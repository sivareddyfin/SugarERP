<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-02T13:19:38.389000000"/>
	<meta name="changed" content="2016-06-02T15:42:18.433000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>SAMPLE
RULES MASTER</b></u></font></font></font></h2>
<p align="center" style="font-weight: normal; text-decoration: none"><br/>
<br/>

</p>
<ul>
	<li/>
<p align="left" style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-1057-935b-4fb6-c30f5bf26987"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen defines the rules to generate the sample cards.</span></span></span></span></font></span>
	</font></font>
	</p>
	<li/>
<p align="left" style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-1066-7d8a-ae26-75e96bc5a9c8"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Provision
	is there <span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">to
	record land sizes and the possible sample cards.</span></span></span></span></font></span>
	</font></font>
	</p>
	<p align="left" style="margin-bottom: 0in; line-height: 100%"></p>
</ul>
</body>
</html>