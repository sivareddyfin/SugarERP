<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T14:59:35.322000000"/>
	<meta name="changed" content="2016-06-02T15:42:46.449000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p align="center" style="margin-bottom: 0in; line-height: 100%"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>SEASON
MASTER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b53-d874-714e-1aaa9c5eddb7"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">This
	screen is to define different seasons. </span></font></font></font>
	</p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b54-7737-46cb-e8962ba2046a"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">When
	the season year is defined all functional tables will be created.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Screen
	id will start with 1 and incremented by 1 by the system
	automatically.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-90a6824d-0b5b-4195-1356-bbfd36c40df6"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">From
	Year &ndash; this should not exist in the system in previous season
	data, To year can not be greater than 1 year from From Year.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Based
	on the selected From year and To year, season year will generate.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">All
	the added season will be shown in the grid.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">There
	is a provision to set status for a season as Active/Inactive.</span></font></font></font></p>
</ul>
<p style="line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><br/>
</font></font><br/>
<br/>

</p>
<p style="line-height: 100%"><br/>
<br/>
<br/>

</p>
</body>
</html>