<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T15:40:12.942000000"/>
	<meta name="changed" content="2016-06-02T15:53:08.382000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>VILLAGE
MASTER</b></u></font></font></font></h2>
<p align="left" style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Village
	id starts with 1 and are added by 1 which is done by the system
	only.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Names
	of the Villages are defined.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Mandal
	names in drop down will load from the mandal master.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added Villages are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">By
	clicking on the action button we can modify the given values and
	also make any Villages inactive when not in use.</font></font></font></p>
	<li/>
<p align="left" style="font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-style: normal">And
	the inactive </span><span style="font-style: normal">Villages</span><span style="font-style: normal">
	can be seen in only this screen, </span><span style="font-style: normal">it</span><span style="font-style: normal">
	does not appear else where.</span></font></font></font></p>
</ul>
</body>
</html>