<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T11:33:39.794000000"/>
	<meta name="changed" content="2016-06-02T15:50:13.046000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p align="center" style="margin-bottom: 0in; line-height: 100%"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>USER
MASTER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0a9d-811c-e541-9b9e40ff38a3"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen records the details of different employees.</span></span></span></span></font></span>
	</font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">User
	id will start with 1 and added by 1 by the system automatically.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0ab7-152b-32ee-8016f35d8fc1"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">This
	screen link the user to the respective role.</span></span></span></span>
	</font></font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">User
	name and password is required to login the s/w.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">User
	id should be unique for every user.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">For
	every user password is &ldquo;finsol&rdquo;.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Provision
	is there to set the password as Active/Inactive.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Can
	modify the added user details if necessary.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Designation,
	Department and roles will be loaded here from the respective master
	screens.</font></p>
	<p style="margin-bottom: 0in; line-height: 100%"></p>
</ul>
</body>
</html>