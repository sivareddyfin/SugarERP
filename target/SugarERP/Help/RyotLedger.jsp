<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-31T14:17:48.947000000"/>
	<meta name="changed" content="2016-06-02T15:40:47.029000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">                    
                                                <font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>RYOT
LEDGER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">This
	screen generates the ledger of an individual ryot for a particular
	season within the given dates.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">This
	screen calculates the advances, loans, Seedling tray charges if any,
	transport expenses, harvesting expenses, CDC, UC given to ryots.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Debit(DR)
	is the amount which ryot has to give to a company.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Credit(CR)
	is the amount which company has to give to the ryot.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Running
	balance =Credit &ndash; Debit.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Available
	balance in Debit(DR) is the amount which ryot has to give to a
	company.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Available
	balance in Credit(CR) is the amount which company has to give to the
	ryot.</font></font></p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>