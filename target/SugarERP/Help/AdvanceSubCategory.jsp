<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-02T12:34:00.760000000"/>
	<meta name="changed" content="2016-06-02T15:20:01.240000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
		a:link { so-language: zxx }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>ADVANCE
SUB-CATEGORY MASTER</b></u></font></font></font></h2>
<p align="left" style="font-weight: normal; text-decoration: none"><br/>
<br/>

</p>
<p align="left" style="font-weight: normal; text-decoration: none"><br/>
<br/>

</p>
<ul>
	<li/>
<p align="left" style="font-style: normal"><font color="#000000">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Sub-category
	code starts with 1 and are added by 1 which is done by the system
	only.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; font-weight: normal; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Names
	of the sub-category are defined.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; font-weight: normal; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here,
	advances, categories will load from the respective master screens
	master.</font></font></font></p>
	<li/>
<p align="left" style="text-decoration: none"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added sub-categories are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left"><font color="#000000"><font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt">By
	clicking on the action button we can modify the given values and
	also make any sub-category inactive when not in use.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; font-weight: normal; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="2" style="font-size: 11pt">And
	the inactive sub-category can be seen in only this screen, it does
	not appear else where.</font></font></font></p>
</ul>
<p align="center" style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>