<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T10:39:10.577000000"/>
	<meta name="changed" content="2016-06-02T15:32:13.128000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">   					<font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>DESIGNATION
MASTER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0a66-01fa-d2fe-5f7a20f56be5"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen gives the complete information of the designations of the
	employee like cane manager, field officer, field assistant</span></span></span></span></font></span><font color="#0066ff"><u><b>
	</b></u></font></font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Designation
	id will start with 1 and incremented by 1 by the system
	automatically.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Added
	designations will added to the grid.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Have
	provision to set the status as Active, Inactive.</font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Action
	button will used to modify the necessary data in a particular
	record.</font></font></font></p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>