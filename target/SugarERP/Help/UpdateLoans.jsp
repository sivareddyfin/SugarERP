<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-31T10:26:05.077000000"/>
	<meta name="changed" content="2016-06-02T15:46:15.867000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-f9c3c6e6-0532-c62e-5d04-a3a6ccc7be5b"></a>
                                                     <span style="font-variant: normal"><font color="#0066ff"><span style="text-decoration: none"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><span style="font-style: normal"><u><b><span style="background: transparent">Update
Loans</span></b></u></span></font></font></span></font></span><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt">
</font></font>
</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0532-27ea-a813-ae0c6ffff42d"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen is to update the Loan Account No., Disbursed Amt and
	Disbursed Date for recommended loans.</span></span></span></span></font></span>
	</font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Select
	season, bank branch and then click on get updated loan details then
	you get the details from prepare tie up loans screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Have
	to give the details manually for Loan account number, Date of
	disbursement and disbursed amount.</font></font></p>
	<p style="margin-bottom: 0in; line-height: 100%"></p>
</ul>
</body>
</html>