<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T01:17:25.096000000"/>
	<meta name="changed" content="2016-06-02T15:39:26.521000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h1 { margin-bottom: 0.08in }
		h1.western { font-family: "Liberation Sans", sans-serif; font-size: 18pt }
		h1.cjk { font-family: "Microsoft YaHei"; font-size: 18pt }
		h1.ctl { font-family: "Mangal"; font-size: 18pt }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h1 class="western" align="center" style="font-style: normal; line-height: 100%">
<font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u>Region
Master</u></font></font></font></h1>
<ul>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Region
	id starts with 1 and are added by 1 which is done by the system
	only.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Names
	of the regions are defined.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added regions are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">By
	clicking on the action button we can modify the given values and
	also make any regions inactive when not in use.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-style: normal"><span style="text-decoration: none">And
	the inactive </span></span><span style="font-style: normal"><span style="text-decoration: none">regions</span></span><span style="font-style: normal"><span style="text-decoration: none">
	can be seen in only this screen, </span></span><span style="font-style: normal"><span style="text-decoration: none">it</span></span><span style="font-style: normal"><span style="text-decoration: none">
	does not appear else where I.e in </span></span><span style="font-style: normal"><span style="text-decoration: none">zone</span></span><span style="font-style: normal"><span style="text-decoration: none">
	screen only active </span></span><span style="font-style: normal"><span style="text-decoration: none">region</span></span><span style="font-style: normal"><span style="text-decoration: none">s
	are displayed.</span></span></font></font></font></p>
</ul>
</body>
</html>