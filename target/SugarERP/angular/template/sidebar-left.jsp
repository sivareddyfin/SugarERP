<!--ng-style="{'background-color': mactrl.bgCol,'color': fontColor}"-->
<div class="sidebar-inner c-overflow" ng-style="{'background-color': mactrl.bgCol,'color': fontColor}" ng-init="getUserPermissions();setPoolingSession();">    
    <div class="profile-menu">
        <a href="" toggle-submenu>
            <div class="profile-pic">
                <!--<img src="img/profile-pics/1.jpg" alt="">-->
				<img src="img/profile-pics/images.png" />
            </div>

            <div class="profile-info">
                {{UserName}}
                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>

        <ul class="main-menu">
            <li>
                <a data-ui-sref="pages.profile.profile-about" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a data-ui-sref="Dashboard" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-input-antenna"></i> DashBoard</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
            <li>
                <a href="#" id="logout" data-ng-click="mactrl.Logout();"><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>
        </ul>
    </div>
	
    <ul class="main-menu">        
        <!------------Admin and Security------------------->
		<!--ng-style="{'color':mactrl.fontColor}"-->
                <li class="sub-menu adminsec" data-ng-class="{ 'active toggled': mactrl.$state.includes('admin') }" data-ui-sref-active="active" ng-disabled="mactrl.displayMenu=='1'">
                    <a href="" toggle-submenu ><i class="zmdi zmdi-account-circle"></i> Administration and Security</a>
						<ul style="margin-left:1px;">
						   <!--------Security-------------->
                        	<li class="sub-menu admaster" data-ng-class="{ 'active toggled': mactrl.$state.includes('admin.security') }" data-ui-sref-active="active">
								<a href="" toggle-submenu><i class="zmdi zmdi-lock-open"></i> Masters</a>					
									<ul style="background-color: #f7f7f7;padding-left:12px;" class="adminmas">
										<li style="padding:3px;">
											<a data-ui-sref-active="active" data-ui-sref="admin.security.CompanyMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Master</a>  							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active" data-ui-sref="admin.security.ScreenMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Screens Master</a> 	 						
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active" data-ui-sref="admin.security.RoleMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Roles Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active" data-ui-sref="admin.security.Designation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Designation Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.DepartmentMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Department Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.LoginUserMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> User Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.SetupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Setup Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.ScreenFields" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Screen Fields Master</a> 	 						
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.Season" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Season Master</a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="admin.security.MigrationMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Migration Master</a> 							
										</li>
										
									</ul>
								 </li>
						   <!--------MIS Reports----------->
						    <li class="sub-menu adminmis" data-ng-class="{ 'active toggled': mactrl.$state.includes('admin.reports') }" data-ui-sref-active="active">
								<a href="" toggle-submenu ng-style="{'color':mactrl.fontColor}"><i class="zmdi zmdi-account-circle"></i> MIS Reports</a>					
									<ul style="background-color: #f7f7f7;padding-left:12px;" class="adminrep">
										<li style="padding:3px;">
											<a data-ui-sref-active="active" data-ui-sref="admin.reports.AuditTrail" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Audit Trail</a>
										</li>																																																																							
									</ul>
							</li>
						</ul>										
				</li>					 					
		
		<!-----------Agriculture and Harvesting----->
		
		<li class="sub-menu harvestmodl" data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting') }" data-ui-sref-active="active">
			<a href="" toggle-submenu><i class="zmdi zmdi-menu"></i> Agriculture and Harvesting</a>					
            <ul style="margin-left:20px;">				
				<!----------------Masters----------->
                <li class="sub-menu harmaster" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.masters') }" data-ui-sref-active="active">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Masters</a>
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="harmas">
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.LandType" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Land Type Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.CaneManager" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Manager Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.FieldOfficer" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Field Officer Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.FieldAssistant" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Field Assistant Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Region" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Region Master</a> 							
						</li>			
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Zone" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Zone Master</a> 							
						</li>	
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="harvesting.masters.Mandal" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Mandal Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="harvesting.masters.Village" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Village Master</a> 							
						</li>	
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Circle" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Circle Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Bank" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Bank Master</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Variety" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Variety Master</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Advance" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Advance Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="harvesting.masters.AdvanceOrder" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Order Advances</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.AdvanceCategory" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Advance Category Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.AdvanceSubCategory" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Advance Subcategory Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.RyotMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Ryot Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="harvesting.masters.FamilyGroup" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Family Group Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.SeedlingTray" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Seedling Tray Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.SampleRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Sample Rules</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="harvesting.masters.PermitRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Permit Rules</a> 							
						</li>
					</ul>
				</li>
				<!-----------Transaction Screens---->
				<li class="sub-menu hartrans" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.transaction') }" data-ui-sref-active="active">
                  <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Transaction Screens</a>
				  <ul style="background-color: #f7f7f7;padding-left:12px;" class="hartran">
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.RyotExtentDetails" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Extent Details for Soil Test</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.soilTestResults" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Soil Test Results</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.OfferMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Agreement</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.CompanyAdvancestoRyot" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Advances to Ryots</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.SeedlingTrayCharges" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Seedling Tray Charges</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.PrepareLoan" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Prepare Tieup Loans</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.RyotLoans" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Search Ryot Loans by Ryot Code</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.UpdateEstimatedQty" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Estimated Quantity</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GeneratePrograms" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Programs</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GenerateSampleCards" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Sample Cards</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.LabCCSRatings" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Lab CCS Rating</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.AvgCCSRatings" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Average CCS Rating</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GenerateRanking" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Ranking</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GeneratePermit" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Permits</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.PrintPermits" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Print Permits</a> 							
						</li>
					</ul>							
				</li>
				<!-----------MIS Reports------------>
				<li class="sub-menu harreport" data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.Reports') }" data-ui-sref-active="active" style="margin-left:-20px;">
								<a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> MIS Reports</a>					
									<ul style="background-color: #f7f7f7;padding-left:12px;" class="harrep">
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.ProgramDetails" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Program Details</a> 							
										</li>								
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementCheckList" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> AgreementCheckList </a>							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementCircleWiseCheckList" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> AgreementCircleWiseCheckList </a> 						
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementMasterListRyotWiseInVillage" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Agreement MasterListRyotWiseInVillage</a>  							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.GrowersRegisterRyotwiseReport" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> GrowersRegister Ryot wise Report </a> 							
										</li>
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.GrowerListByVillageMandalSummary" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> GrowerList By Village & Mandal Summary</a>  							
										</li>												
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.BankOrder" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Bank Order</a>  							
										</li>						
										<li style="padding:3px;">
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.LoanMasterListReport" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Loans Master List Report</a> 							
										</li>																
										
									</ul>
							</li>
			</ul>
			
					
		</li>		
		
		<!-----------Cane Receipts-------------->
		<li class="sub-menu caneReceipt" data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts') }" data-ui-sref-active="active">
			<a href="" toggle-submenu><i class="zmdi zmdi-menu"></i> Cane Receipts</a>			
            <ul style="margin-left:20px;">				
				<!---------------Masters-------->
                <li class="sub-menu recptmaster" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.masters') }" data-ui-sref-active="active">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Masters</a>
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="recptmas">
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.masters.CaneWeightMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Weight Master</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.masters.ShiftMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Shift Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.masters.WeightBridgemaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Weigh Bridge Master</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.masters.UnloadingContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Unloading Contractor Master</a> 							
						</li>
					</ul>
				</li>
				<!--------Transaction Screens--->
				<li class="sub-menu recpttrans" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.transaction') }" data-ui-sref-active="active">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Transaction Screens</a>
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="recpttran">
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.CaneWeighment" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Weighment</a> 	 						
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.TestWeighment" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Test Weighment</a> 							
						</li>
					</ul>
				</li>
				<!--------MIS Reports----------->
				<li class="sub-menu recptreport" data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.Reports') }" data-ui-sref-active="active" style="margin-left:-20px;">
					<a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> MIS Reports</a>					
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="recptrept">
					</ul>
				</li>
			</ul>			
		</li>
		
		<!----------Cane Accounting------------>
		<li class="sub-menu caneAccounting" data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting') }" data-ui-sref-active="active">
			<a href="" toggle-submenu><i class="zmdi zmdi-menu"></i> Cane Accounting</a>			
            <ul style="margin-left:20px;">	
				<!-------------Masters--------->			
				<li class="sub-menu accountmaster" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.masters') }" data-ui-sref-active="active">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Masters</a>
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="accountmas">
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="accounting.masters.Deductions" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Standard Deductions</a>							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="accounting.masters.DeductionsOrder" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Order Deductions</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.TieupLoanInterest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Tieup Loan Interest Master </a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.AccountGroupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Account Group Master </a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref="accounting.masters.SubGroupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Account Subgroup Master</a>							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.HarvestContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Harvesting Contractor Master </a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.TransportContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Transport Contractor Master </a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.CaneAccountingRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Accounting Rules</a>							
						</li>
					</ul>
				</li>
				<!-----------Transactions------>
				<li class="sub-menu accounttransaction" style="margin-left:-20px;" data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.transaction') }" data-ui-sref-active="active">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Transaction Screens</a>
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="accounttrans">
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateLoans" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Loans </a> 							
						</li>
						<!--<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref-active="active" data-ui-sref="accounting.transaction.UpdateLoanInterest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Loan Interest</a>							
						</li>-->
						<!--<li style="padding:3px;">
							<a data-ui-sref-active="active" data-ui-sref-active="active" data-ui-sref="accounting.transaction.UpdateCompanyAdvanceInterest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Compnay Advance Interest</a>							
						</li>-->
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateHarvestCharges" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Harvest Rates</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateTransportCharges" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Transport Rates</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.PerformCaneAccountingCalculation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Perform Cane Accounting Calculation-Dates</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.PostSeasonCalculation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Post Season Calculation</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateRyotAccounts" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Ryot Accounts</a> 							
						</li>						
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.SearchRyotLoansbySurvey" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Search Ryot Loans by Survey Number</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.RyotLedger" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Ryot Ledger</a> 							
						</li>
						<li style="padding:3px;">
							<a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.TrailBalance" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Trail Balance</a> 							
						</li>
						
					</ul>
				</li>
				<!-----------MIS Reports------->
				<li class="sub-menu accountReport" data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.Reports') }" data-ui-sref-active="active" style="margin-left:-20px;">
					<a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> MIS Reports</a>					
					<ul style="background-color: #f7f7f7;padding-left:12px;" class="accountRep">
					</ul>
				</li>
			</ul>
		</li>
		
		
		
    </ul>
</div>
