materialAdmin
    // =========================================================================
    // Base controller for common functions
    // =========================================================================


    .controller('materialadminCtrl', function($timeout, $state, $scope, growlService, $http){
		var asd = "Welcome back "+ sessionStorage.getItem('username')+" !";
        //Welcome Message
        growlService.growl(asd, 'inverse')
       	$scope.UserName = sessionStorage.getItem('username');
		
		
		//--------Pooling Session----
		$scope.setPoolingSession = function()
		{
			$scope.value = 1;

			var polling = function() {
			var value = $http({
			method : 'GET',
			url : '/SugarERP/pollToCheckSession.html'
			});

			value.success(function(data, status, headers, config) {
				if(data==false)
					window.location.assign('/SugarERP');
			//alert(data);
			});

			$timeout(function() {
			$scope.value++;
			polling();
			}, 80000);
			};

			//Call function polling()
			polling();
		};
		//Logout
		//================
		//this.Logout = function()
//		{
//				//sessionStorage.setItem('username',null);
//				//window.location.assign('/SugarERP');
//				
//				var value = $http({
//			method : 'GET',
//			url : '/SugarERP/logout.html'
//			});
//
//			value.success(function(data, status, headers, config) {
//				window.location.assign(data);
//			});
//		}

		this.Logout = function()
		{
				//sessionStorage.setItem('username',null);
				//window.location.assign('/SugarERP');
				
				var value = $http({
			method : 'GET',
			url : '/SugarERP/logout.html'
			});
			window.location.assign('/SugarERP');
			//value.success(function(data, status, headers, config) {
			//	window.location.assign(data);
			//});
		}
		//=======================================
			//User Management
		//=======================================
		
		$scope.getUserPermissions = function()
		{
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllRoleScreensByEmployeeId.html",
					data: {},
			     }).success(function(data, status) 
				 {	
					if(data != '' && data != 'null')
					{
						$("a").each(function()
						{
							for(var i=0;i<data.length;i++)
							{
								if($(this).text().toUpperCase().trim() ==data[i].screenname)
								{
									
									if($(this).text().toUpperCase().trim() == data[i].screenname && data[i].canadd==false && data[i].canmodify==false && data[i].candelete==false && data[i].canread==false)
									{					
										$(this).remove();  							   
									}
								}
							}
							//if($(this).text().toUpperCase().trim() == data[i].screenname)
	//						{
	//							
	//							if($(this).text().toUpperCase().trim() == data[i].screenname && data[i].canadd==false && data[i].canmodify==false && data[i].candelete==false && data[i].canread==false)
	//				   			{					
	//							   $(this).remove();  							   
	//							}
	//							i++;
	//							//--------hide masters module in Administration and Security---
	//							var numrelated=$('.adminmas > li > a').length;
	//							if(numrelated=='0') { $('.admaster').hide(); }				
	//							//--------hide MIS Reports module in Administration and Security---
	//							var adminmisrp = $('.adminrep > li > a').length;
	//							if(adminmisrp=='0') { $('.adminmis').hide(); }					  
	//							//--------hide Administration and Security---
	//							if(adminmisrp=='0' && numrelated=='0') { $('.adminsec').hide(); }
	//							//--------hide masters module in Agriculture and Harvesting---
	//							var harCount=$('.harmas > li > a').length;
	//							if(harCount=='0') { $('.harmaster').hide(); }				
	//							//--------hide Transaction module in Agriculture and Harvesting---
	//							var harTransCount = $('.hartran > li > a').length;
	//							if(harTransCount=='0') { $('.hartrans').hide(); }					  
	//							//--------hide MIS Reports module in Agriculture and Harvesting---
	//							var harRepCount = $('.harrep > li > a').length;
	//							if(harRepCount=='0') { $('.harreport').hide(); }					  
	//							//--------hide Agriculture and Harvesting  module---
	//							if(harCount=='0' && harTransCount=='0' && harRepCount=='0') { $('.harvestmodl').hide(); }
	//							//--------hide masters module in Cane Receipts---
	//							var recptMasCount=$('.recptmas > li > a').length;
	//							if(recptMasCount=='0') { $('.recptmaster').hide(); }				
	//							//--------hide Transaction module in Cane Receipts---
	//							var recptTransCount=$('.recpttran > li > a').length;
	//							if(recptTransCount=='0') { $('.recpttrans').hide(); }				
	//							//--------hide MIS Reports module in Cane Receipts---
	//							var recptReportCount=$('.recptrept > li > a').length;
	//							if(recptReportCount=='0') { $('.recptreport').hide(); }			
	//							//--------hide Cane Receipts module---
	//							if(recptMasCount=='0' && recptTransCount=='0' && recptReportCount=='0') { $('.caneReceipt').hide(); }														
	//							//--------hide Master module in Cane Accounting---
	//							var accountMasCount=$('.accountmas > li > a').length;
	//							if(accountMasCount=='0') { $('.accountmaster').hide(); }				
	//							//--------hide Transaction module in Cane Accounting---
	//							var accountTransCount=$('.accounttrans > li > a').length;
	//							if(accountTransCount=='0') { $('.accounttransaction').hide(); }				
	//							//--------hide MIS Report module in Cane Accounting---
	//							var accountReportCount=$('.accountRep > li > a').length;
	//							if(accountReportCount=='0') { $('.accountReport').hide(); }				
	//							//--------hide Cane Accounting Module---							
	//							if(accountMasCount=='0' && accountTransCount=='0' && accountReportCount=='0') { $('.caneAccounting').hide(); }				
	//							
							//}
					   });
					}
					
																
				});			
		};
		
		
        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');
        
        // For Mainmenu Active Class
        this.$state = $state;    
        
        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
				
                this.sidebarToggle.left = false;
            }
        }
        
        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;
        
        this.lvSearch = function() {
            this.listviewSearchStat = true; 
        }
        
        //Listview menu toggle in small screens
        this.lvMenuStat = false;
        
        //Blog
        this.wallCommenting = [];
        
        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';
		this.displayMenu = '1';
		//this.bgCol = 'lightblue';
		
        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
			
			
        ]

        this.skinSwitch = function (color) 
		{		
			//alert(color);
            this.currentSkin = color;
			this.bgCol = color;
			this.fontColor = "white";
        }
		
    })

     
    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function($timeout, messageService){

		
        // Top Search
        this.openSearch = function(){
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        }

        this.closeSearch = function(){
            angular.element('#header').removeClass('search-toggled');
        }
        
        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        this.messageResult = messageService.getMessage(this.img, this.user, this.text);


        //Clear Notification
        this.clearNotification = function($event) {
            $event.preventDefault();
            
            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();
            
            angular.element($event.target).parent().fadeOut();
            
            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;
            
            y.each(function(){
                var z = $(this);
                $timeout(function(){
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                        z.remove();
                    });
                }, w+=150);
            })
            
            $timeout(function(){
                angular.element('#notifications').addClass('empty');
            }, (z*150)+200);
        }
        
        // Clear Local Storage
        this.clearLocalStorage = function() {
            
            //Get confirmation, if confirmed clear the localStorage
            swal({   
                title: "Are you sure?",   
                text: "All your saved localStorage values will be removed",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#F44336",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success"); 
            });
            
        }
        
        //Fullscreen View
        this.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }
    
    })



    // =========================================================================
    // Best Selling Widget
    // =========================================================================

    .controller('bestsellingCtrl', function(bestsellingService){
        // Get Best Selling widget Data
        this.img = bestsellingService.img;
        this.name = bestsellingService.name;
        this.range = bestsellingService.range; 
        
        this.bsResult = bestsellingService.getBestselling(this.img, this.name, this.range);
    })

 
    // =========================================================================
    // Todo List Widget
    // =========================================================================

    .controller('todoCtrl', function(todoService){
        
        //Get Todo List Widget Data
        this.todo = todoService.todo;
        
        this.tdResult = todoService.getTodo(this.todo);
        
        //Add new Item (closed by default)
        this.addTodoStat = false;
    })


    // =========================================================================
    // Recent Items Widget
    // =========================================================================

    .controller('recentitemCtrl', function(recentitemService){
        
        //Get Recent Items Widget Data
        this.id = recentitemService.id;
        this.name = recentitemService.name;
        this.parseInt = recentitemService.price;
        
        this.riResult = recentitemService.getRecentitem(this.id, this.name, this.price);
    })


    // =========================================================================
    // Recent Posts Widget
    // =========================================================================
    
    .controller('recentpostCtrl', function(recentpostService){
        
        //Get Recent Posts Widget Items
        this.img = recentpostService.img;
        this.user = recentpostService.user;
        this.text = recentpostService.text;
        
        this.rpResult = recentpostService.getRecentpost(this.img, this.user, this.text);
    })


    //=================================================
    // Profile
    //=================================================

    .controller('profileCtrl', function(growlService){
        
        //Get Profile Information from profileService Service
        
        //User
        this.profileSummary = "Sed eu est vulputate, fringilla ligula ac, maximus arcu. Donec sed felis vel magna mattis ornare ut non turpis. Sed id arcu elit. Sed nec sagittis tortor. Mauris ante urna, ornare sit amet mollis eu, aliquet ac ligula. Nullam dolor metus, suscipit ac imperdiet nec, consectetur sed ex. Sed cursus porttitor leo.";
    
        this.fullName = "Mallinda Hollaway";
        this.gender = "female";
        this.birthDay = "23/06/1988";
        this.martialStatus = "Single";
        this.mobileNumber = "00971123456789";
        this.emailAddress = "malinda.h@gmail.com";
        this.twitter = "@malinda";
        this.twitterUrl = "twitter.com/malinda";
        this.skype = "malinda.hollaway";
        this.addressSuite = "44-46 Morningside Road";
        this.addressCity = "Edinburgh";
        this.addressCountry = "Scotland";

        //Edit
        this.editSummary = 0;
        this.editInfo = 0;
        this.editContact = 0;
    
        
        this.submit = function(item, message) {            
            if(item === 'profileSummary') {
                this.editSummary = 0;
            }
            
            if(item === 'profileInfo') {
                this.editInfo = 0;
            }
            
            if(item === 'profileContact') {
                this.editContact = 0;
            }
            
            growlService.growl(message+' has updated Successfully!', 'inverse'); 
        }

    })
	
	//================================================
    	// CALENDAR
    //=================================================
    
    .controller('calendarCtrl', function($modal){
    
        //Create and add Action button with dropdown in Calendar header. 
        this.month = 'month';
    
        this.actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                            '<li class="dropdown" dropdown>' +
                                '<a href="" dropdown-toggle><i class="zmdi zmdi-more-vert"></i></a>' +
                                '<ul class="dropdown-menu dropdown-menu-right">' +
                                    '<li class="active">' +
                                        '<a data-calendar-view="month" href="">Month View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicWeek" href="">Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaWeek" href="">Agenda Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicDay" href="">Day View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaDay" href="">Agenda Day View</a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                        '</li>';

            
        //Open new event modal on selecting a day
        this.onSelect = function(argStart, argEnd) {            
            var modalInstance  = $modal.open({
                templateUrl: 'addEvent.html',
                controller: 'addeventCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    calendarData: function() {
                        var x = [argStart, argEnd];
                        return x;
                    }
                }
            });
        }
    })

    //Add event Controller (Modal Instance)
    .controller('addeventCtrl', function($scope, $modalInstance, calendarData){
        
        //Calendar Event Data
        $scope.calendarData = {
            eventStartDate: calendarData[0],
            eventEndDate:  calendarData[1]
        };
    
        //Tags
        $scope.tags = [
            'bgm-teal',
            'bgm-red',
            'bgm-pink',
            'bgm-blue',
            'bgm-lime',
            'bgm-green',
            'bgm-cyan',
            'bgm-orange',
            'bgm-purple',
            'bgm-gray',
            'bgm-black',
        ]
        
        //Select Tag
        $scope.currentTag = '';
        
        $scope.onTagClick = function(tag, $index) {
            $scope.activeState = $index;
            $scope.activeTagColor = tag;
        } 
        
        //Add new event
        $scope.addEvent = function() {
            if ($scope.calendarData.eventName) {

                //Render Event
                $('#calendar').fullCalendar('renderEvent',{
                    title: $scope.calendarData.eventName,
                    start: $scope.calendarData.eventStartDate,
                    end:  $scope.calendarData.eventEndDate,
                    allDay: true,
                    className: $scope.activeTagColor

                },true ); //Stick the event

                $scope.activeState = -1;
                $scope.calendarData.eventName = '';     
                $modalInstance.close();
            }
        }
        
        //Dismiss 
        $scope.eventDismiss = function() {
            $modalInstance.dismiss();
        }
    })

    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', function(){
    
        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;
    
        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';
    })


    // =========================================================================
    // PHOTO GALLERY
    // =========================================================================

    .controller('photoCtrl', function(){
        
        //Default grid size (2)
        this.photoColumn = 'col-md-2';
        this.photoColumnSize = 2;
    
        this.photoOptions = [
            { value: 2, column: 6 },
            { value: 3, column: 4 },
            { value: 4, column: 3 },
            { value: 1, column: 12 },
        ]
    
        //Change grid
        this.photoGrid = function(size) {
            this.photoColumn = 'col-md-'+size;
            this.photoColumnSize = size;
        }
    
    })


    // =========================================================================
    // ANIMATIONS DEMO
    // =========================================================================
    .controller('animCtrl', function($timeout){
        
        //Animation List
        this.attentionSeekers = [
            { animation: 'bounce', target: 'attentionSeeker' },
            { animation: 'flash', target: 'attentionSeeker' },
            { animation: 'pulse', target: 'attentionSeeker' },
            { animation: 'rubberBand', target: 'attentionSeeker' },
            { animation: 'shake', target: 'attentionSeeker' },
            { animation: 'swing', target: 'attentionSeeker' },
            { animation: 'tada', target: 'attentionSeeker' },
            { animation: 'wobble', target: 'attentionSeeker' }
        ]
        this.flippers = [
            { animation: 'flip', target: 'flippers' },
            { animation: 'flipInX', target: 'flippers' },
            { animation: 'flipInY', target: 'flippers' },
            { animation: 'flipOutX', target: 'flippers' },
            { animation: 'flipOutY', target: 'flippers'  }
        ]
         this.lightSpeed = [
            { animation: 'lightSpeedIn', target: 'lightSpeed' },
            { animation: 'lightSpeedOut', target: 'lightSpeed' }
        ]
        this.special = [
            { animation: 'hinge', target: 'special' },
            { animation: 'rollIn', target: 'special' },
            { animation: 'rollOut', target: 'special' }
        ]
        this.bouncingEntrance = [
            { animation: 'bounceIn', target: 'bouncingEntrance' },
            { animation: 'bounceInDown', target: 'bouncingEntrance' },
            { animation: 'bounceInLeft', target: 'bouncingEntrance' },
            { animation: 'bounceInRight', target: 'bouncingEntrance' },
            { animation: 'bounceInUp', target: 'bouncingEntrance'  }
        ]
        this.bouncingExits = [
            { animation: 'bounceOut', target: 'bouncingExits' },
            { animation: 'bounceOutDown', target: 'bouncingExits' },
            { animation: 'bounceOutLeft', target: 'bouncingExits' },
            { animation: 'bounceOutRight', target: 'bouncingExits' },
            { animation: 'bounceOutUp', target: 'bouncingExits'  }
        ]
        this.rotatingEntrances = [
            { animation: 'rotateIn', target: 'rotatingEntrances' },
            { animation: 'rotateInDownLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInDownRight', target: 'rotatingEntrances' },
            { animation: 'rotateInUpLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInUpRight', target: 'rotatingEntrances'  }
        ]
        this.rotatingExits = [
            { animation: 'rotateOut', target: 'rotatingExits' },
            { animation: 'rotateOutDownLeft', target: 'rotatingExits' },
            { animation: 'rotateOutDownRight', target: 'rotatingExits' },
            { animation: 'rotateOutUpLeft', target: 'rotatingExits' },
            { animation: 'rotateOutUpRight', target: 'rotatingExits'  }
        ]
        this.fadeingEntrances = [
            { animation: 'fadeIn', target: 'fadeingEntrances' },
            { animation: 'fadeInDown', target: 'fadeingEntrances' },
            { animation: 'fadeInDownBig', target: 'fadeingEntrances' },
            { animation: 'fadeInLeft', target: 'fadeingEntrances' },
            { animation: 'fadeInLeftBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInRight', target: 'fadeingEntrances'  },
            { animation: 'fadeInRightBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInUp', target: 'fadeingEntrances'  },
            { animation: 'fadeInBig', target: 'fadeingEntrances'  }
        ]
        this.fadeingExits = [
            { animation: 'fadeOut', target: 'fadeingExits' },
            { animation: 'fadeOutDown', target: 'fadeingExits' },
            { animation: 'fadeOutDownBig', target: 'fadeingExits' },
            { animation: 'fadeOutLeft', target: 'fadeingExits' },
            { animation: 'fadeOutLeftBig', target: 'fadeingExits'  },
            { animation: 'fadeOutRight', target: 'fadeingExits'  },
            { animation: 'fadeOutRightBig', target: 'fadeingExits'  },
            { animation: 'fadeOutUp', target: 'fadeingExits'  },
            { animation: 'fadeOutUpBig', target: 'fadeingExits'  }
        ]
        this.zoomEntrances = [
            { animation: 'zoomIn', target: 'zoomEntrances' },
            { animation: 'zoomInDown', target: 'zoomEntrances' },
            { animation: 'zoomInLeft', target: 'zoomEntrances' },
            { animation: 'zoomInRight', target: 'zoomEntrances' },
            { animation: 'zoomInUp', target: 'zoomEntrances'  }
        ]
        this.zoomExits = [
            { animation: 'zoomOut', target: 'zoomExits' },
            { animation: 'zoomOutDown', target: 'zoomExits' },
            { animation: 'zoomOutLeft', target: 'zoomExits' },
            { animation: 'zoomOutRight', target: 'zoomExits' },
            { animation: 'zoomOutUp', target: 'zoomExits'  }
        ]

        //Animate    
        this.ca = '';
    
        this.setAnimation = function(animation, target) {
            if (animation === "hinge") {
                animationDuration = 2100;
            }
            else {
                animationDuration = 1200;
            }
            
            angular.element('#'+target).addClass(animation);
            
            $timeout(function(){
                angular.element('#'+target).removeClass(animation);
            }, animationDuration);
        }
    
    })

//===============================================================================================================================================================================

	
  //=================================================
   		 // LOGIN
  //=================================================

    .controller('loginCtrl', function($scope, $http){
		
        this.login = 1;
        this.register = 0;
        this.forgot = 0;
		
			$scope.capslock=function(e){
			
	 var isShiftPressed = false;
            var isCapsOn = null;
            $("#txtName").bind("keydown", function (e) {
                var keyCode = e.keyCode ? e.keyCode : e.which;
                if (keyCode == 16) {
                    isShiftPressed = true;
                }
            });
            $("#txtName").bind("keyup", function (e) {
                var keyCode = e.keyCode ? e.keyCode : e.which;
                if (keyCode == 16) {
                    isShiftPressed = false;
                }
                if (keyCode == 20) {
                    if (isCapsOn == true) {
                        isCapsOn = false;
                        $("#error").hide();
                    } else if (isCapsOn == false) {
                        isCapsOn = true;
                        $("#error").show();
                    }
                }
            });
            $("#txtName").bind("keypress", function (e) {
				
                var keyCode = e.keyCode ? e.keyCode : e.which;
                if (keyCode >= 65 && keyCode <= 90 && !isShiftPressed) {
                    isCapsOn = true;
                    $("#error").show();
                } else {
                    $("#error").hide();
                }
            });
        };

$scope.loadyears=function(){
		$scope.loginYears=[{'id':1,'text':"16-17"},{'id':2,'text':"17-18"}			   
						   
						   ]
		
		$scope.login={'finYear':"16-17"};
};
		
		//-------login form---------		
		$scope.loginform = function(login) 
		{	
		$scope.Addsubmitted = true;
			//$scope.submitted = true;
			//alert($scope.details.$valid);
			//if($scope.details.$valid) 
			//{
					if(login!=undefined){
				var data = JSON.stringify(login);
// alert("data"+data);				
				$scope.loading = true;
				$.ajax({
					   url:"login.html",
					   processData:true,
					   type:'POST',
					   contentType:'Application/json',
					   data:data,
					   success:function(responce) 
					   {
						  //alert("responce"+responce);
					
						   if(responce=="angular/login.jsp")
						  {
							 
							sessionStorage.setItem('username',login.name);
							window.location.assign(responce);
						  }
						  else
						  {
							  $('#errorMsg').show();
						  }
						 
							//sessionStorage.setItem('username',login.name);
							//window.location.assign(responce);
					   }, error: function(responce) 
					   { 
					
							$('#errorMsg').show();
							//$scope.login = {'errorMSG':'sdasd'};
							//$scope.errorMsg="username is incorrcet";
				
                  
                }
					});
					}
			//}
		};
		//------login form end------		
    })

	
	//=================================================
			//SetUp Master
	//=================================================
	
	.controller('SetupMaster',function($scope,$http)
	 {
	 	
		$scope.AddSetup = {'lrmVal':0,'atrVal':0,'arntVal':0,'mrntVal':0,'sdntVal':0,'ddntVal':0,'tdnpVal':0,'acpnpVal':0,'lrmConfig':'lrm','atrConfig':'atr','arntConfig':'arnt','mrntConfig':'mrnt','sdntConfig':'sdnt','ddntConfig':'ddnt','tdnpConfig':'tdnp','acpnpConfig':'acpnp','lrmName':'Lab Results Mandatory','atrName':'Audit Trail Required','arntName':'Add records need to be tracked','mrntName':'Modify records need to be tracked','sdntName':'Same Data also need to be tracked','ddntName':'Delete Data need to be tracked','tdnpName':'Transport Data need to be posted','acpnpName':'Additional cane price need to be posted','lrmDesc':'Lab Results Mandatory','atrDesc':'Audit Trail Required','arntDesc':'Add records need to be tracked','mrntDesc':'Modify records need to be tracked','sdntDesc':'Same Data also need to be tracked','ddntDesc':'Delete Data need to be tracked','tdnpDesc':'Transport Data need to be posted','acpnpDesc':'Additional cane price need to be posted','lrmId':'','atrId':'','arntId':'','mrntId':'','sdntId':'','ddntId':'','tdnpId':'','acpnp':''};
		
		var setupFinalObj = [];
		 
		 $scope.SaveSetupMaster = function(AddSetup)
		 {
			 			 			 
			 var mainKey = ['lrm','atr','arnt','mrnt','sdnt','ddnt','tdnp','acpnp'];
			 var subKey = ['Id','Config','Name','Val','Desc'];
			 var finalKey = ['id','configSc','name','value','description'];
			 
			 var objectLength = Object.keys(AddSetup).length;
			 var loppCount = Number(objectLength)/Number(4);
			 setupFinalObj = [];
			 for(var k=0;k<mainKey.length;k++)
			 {
				 var obj = {};
				 for(var i=0;i<subKey.length;i++)
				 {
					 var arrayKey = mainKey[k]+subKey[i];					 
				     obj[finalKey[i]]= AddSetup[arrayKey];					 
				 }
				 setupFinalObj.push(obj);					 					 
			 }
			 $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveSetupDetails.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(setupFinalObj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success!", 'Setup Details Added Successfully' , "success");
							$('.btn-hide').attr('disabled',false);

							$scope.loadAddedSetupDetails();
								
						}
						else
						{
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						}
					}
				});				 
		 };
		 
		 $scope.loadAddedSetupDetails = function()
		 {

				var loadMainKey = ['lrm','atr','arnt','mrnt','sdnt','ddnt','tdnp','acpnp'];
				var loadSubKey = ['Id','Config','Name','Val','Desc'];
				var loadFinalKey = ['id','configSc','name','value','description'];

				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllSetupDetails.html",
				data: {}
				}).success(function(data, status) 
				{
					var getMainobj = {};
					for(var a=0;a<data.length;a++)
					{	
						for(s=0;s<loadFinalKey.length;s++)
						{
							var loadConactKey = loadMainKey[a]+loadSubKey[s];
							getMainobj[loadConactKey]= data[a][loadFinalKey[s]];							
						}
					}
					$scope.AddSetup = getMainobj;
				}); 
		 };
	 })


	//=================================================
  		//Department Master start by Ganesh on 14-03-2016
    //=================================================

	 .controller('DepartmentMaster', function($scope,$http)
	 {
		var maxid = new Object();
		maxid.tablename = "DepartmentMaster";
		var jsonTable= JSON.stringify(maxid);		
		
		$scope.isEdit = true;
		$scope.master = {};
		
		$scope.validateDup = function()
		  {
			  //alert(123);
				for(var d =0;d<JSON.stringify($scope.DepartmentsData.length);d++)
				{
					var firstString = $scope.Departments.department.toUpperCase();
					var secondString = $scope.DepartmentsData[d].department.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  $scope.validateDuplicate = function(desigName,id)
		  {
			 for(var i =0;i<JSON.stringify($scope.DepartmentsData.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				var str2 = $scope.DepartmentsData[i].department.toUpperCase();
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };		
		
		//---------Space Between Two Words----------
  		$scope.spacebtw = function(b)
		{
			var twoSpace =$scope.Departments[b];
			twoSpace =twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.Departments[b] =twoSpace;	
		};

		//------------Space Between Two Words (Grid)----
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.DepartmentsData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.DepartmentsData[id][b] = twoSpace;
		};

		$scope.loadDepartments=function()
	 	{
			var httpRequest = $http({
				method: 'POST',
				//Modified by DMurty on 17-03-2017
				url : "/SugarERP/getMaxDeptCode.html",
				data: jsonTable,
	 		}).success(function(data) 
			{						
				$scope.Departments={'deptCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Department Master'};	
			});
		};
		
	    $scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					$scope.DepartmentsData=data;
											
				}); 
				
			};
		 
		 /*---------------------Add New Department------------*/
		 // $scope.DepartmentsData = [];		  
		  $scope.AddDepartments = function(Departments,form)
		  {
			  //alert(JSON.stringify(Departments));
			   //var DepartmensData = JSON.stringify(Departments);   /*------------for getting data from form-----------*/
			   //$scope.Addsubmitted = true;
			  //$scope.submitted = true;
			  if($scope.DepartmentForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveDepartmentMaster.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(Departments),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						//alert(response);
						if(response==true)
						{
							swal("Success!", 'Departments added Successfully' , "success");
							$('.btn-hide').attr('disabled',false);
							$scope.Departments = angular.copy($scope.master);
							$scope.loadDepartments();
							form.$setPristine(true);
							
								//---------get all Deductions--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllDepartments.html",
										data: {}
									   }).success(function(data, status) 
										{
											
											$scope.DepartmentsData=data;
											
										});  	   
									//---------End--------
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.Departments={'deptCode':data.id,'status':'0'};			
										        });	   	   */
								  //-----------end--------
								  
							  }
							 else
							  {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);								  
							  }
							}
						});					
				}
			else
			{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
			}					
					
	};
		  
		  /*----------------Update Departments------------------*/
		  $scope.updateDepartment = function(Department,id)
		  {		
		  	  			  
			 		  delete Department["$edit"];
			 		 var UpdatedData = angular.toJson(Department);
			 		 $scope.isEdit = true;
					$scope.submitted = true;
					if($scope.UpdateDepartmentForm.$valid)
					{
						$scope.isEdit = false;
						$('.btn-hide').attr('disabled',true);
			  	$.ajax({
				    url:"/SugarERP/saveDepartmentMaster.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
						if(response==true)
						{
			  				swal("Success", 'Updated Successfully!', "success");
							//$('.btn-hide').attr('disabled',false);

						//---------get all Seasons--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllDepartments.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.DepartmentsData=data;
							 			});  	   
							//---------End--------
						 	}
						    else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
					 	  }
						});			
				}
			//-----------end----------			
			};	
			
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
					$scope.Departments = angular.copy($scope.master);
					$scope.loadDepartments();
				    form.$setPristine(true);			    								
		   };
			
		  
	 })
	
	 //=================================================
		//Field  Officer Master
    //=================================================

	 .controller('OfficerMaster', function($scope,$http)
	 {
	 
		var obj = new Object();
		obj.tablename = "FieldOfficer";
		var jsonString= JSON.stringify(obj);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "CaneManager";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);

		var FieldOffDownLoad = new Object();
		FieldOffDownLoad.dropdownname = "Employees";
		var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);


		$scope.isEdit = true;
		$scope.master = {};
		//------------Space Between Two Words --------
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedFieldOfficer[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedFieldOfficer[b] = twoSpace;				
		};
		//-----------Space Between Two Words (Grid)---
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.OfficerData[id][b];			
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();				
			$scope.OfficerData[id][b] = twoSpace;
				
		};		
		
		//-----------Load officer maxid-------	 
		$scope.loadFieldOfficerId = function()
		{				
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
		 	   { 
					$scope.AddedFieldOfficer={'fieldOfficerId':data.id,'status':'0','modifyFlag':"No",'screenName':'Field Officer Master'};						
	           });	   	   
	    };
  		//----------Load Field Officers------
	  	$scope.loadAllFieldOfficers = function()
		{		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllFieldOfficers.html",
					data: {}
			   }).success(function(data, status) 
			   {
					$scope.OfficerData = data; 		//-------for load the data in to added departments grid---------
					//$scope.AddedCane = {}; 
				});
		};
  	   //---------Load Cane Managers---------
		$scope.loadCaneManagerNames = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {		
					$scope.mgrNames=data;
	 		   });  	   
		};
  	   //---------Load Field Officer DropDown---------
		$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
					$scope.FieldOffNames=data;
	 		   });  	   
		};


		//-------------Add Field Officer------
		
		$scope.fieldofficersubmit = function(AddedFieldOfficer,form)
		{
		  	//$scope.OfficerData=[];	
			//$scope.Addsubmitted = true;
			  
			//var AddedFieldOfficer = JSON.stringify(AddedFieldOfficer);   /*------------for getting data from form-----------*/

			if($scope.FieldOfficerMaster.$valid) 
			{	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveFieldOfficer.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(AddedFieldOfficer),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", 'FieldOfficer Added Successfully!', "success");  //-------For showing success message-------------	
							$('.btn-hide').attr('disabled',false);
							$scope.AddedFieldOfficer = angular.copy($scope.master);
							$scope.loadFieldOfficerId();
							form.$setPristine(true);

							//---------Load Added Officers------
					        var httpRequest = $http({
				            		method: 'POST',
						            url : "/SugarERP/getAllFieldOfficers.html",
						            data: {}
			    		       }).success(function(data, status) 
							   {	
						            $scope.OfficerData = data; 		//-------for load the data in to added departments grid---------
									//$scope.AddedFieldOfficer = {};    		//-------for clear the form-------------------------------------						
					        	});
							 //------Load Max id-------------
							 /*var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getMaxId.html",
									data: jsonString,
								   }).success(function(data, status) 
							 	   { 
										$scope.AddedFieldOfficer={'fieldOfficerId':data.id,'status':'0'};
						           });	   	   							 */
							 //--------------end---------------
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
					}
		      });
			  }			
			 else
			  {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}				  
			  }
		  };
		  
		  //-----------------Update Field Officer-----------
		  $scope.filedofficerupdate = function(Officer,id)
		  {		
		  	  			  
			  delete Officer["$edit"];
			  var UpdatedData = angular.toJson(Officer);
			  $scope.isEdit = true;
			  $scope.submitted = true;
			 
			  if($scope.FieldoOfiicerMasterEdit.$valid) 
			  {
				  $scope.isEdit = false;
				  $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveFieldOfficer.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
			  			if(response==true)
						{
							swal("Good job!", 'FieldOfficer Updated Successfully!', "success");  //-------For showing success message-------------	
							$('.btn-hide').attr('disabled',false);

							//---------Load Added Officers------
					        var httpRequest = $http({
				            		method: 'POST',
						            url : "/SugarERP/getAllFieldOfficers.html",
						            data: {}
			    		       }).success(function(data, status) 
							   {	
						            $scope.OfficerData = data; 		//-------for load the data in to added departments grid---------
									//$scope.AddedFieldOfficer = {};    		//-------for clear the form-------------------------------------						
					        	});
							//-----------End----------------------
						}
					   else
					    {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
							
						}
					}
				});	
			  }
		  };
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedFieldOfficer = angular.copy($scope.master);
				$scope.loadFieldOfficerId();
			    form.$setPristine(true);			    											   
		   };
		  
	 })
	 //=================================================
  		//FIeld  Assistant Master
    //=================================================

	 .controller('fieldAssistantMaster', function($scope,$http)
	 {
		var obj = new Object();
		obj.tablename = "FieldAssistant";
		var jsonString= JSON.stringify(obj);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "FieldOfficer";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		var CaneDropDownLoad = new Object();
		CaneDropDownLoad.dropdownname = "Employees";
		var CaneStringDropDown= JSON.stringify(CaneDropDownLoad);		
		
		$scope.isEdit = true;
		$scope.master = {};
		
		//----------Space Between Two Words ------
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedFieldAssistant[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedFieldAssistant[b] = twoSpace;				
		};
		//------------Space Between Two Words (Grid)---
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.FieldAssistantData[id][b];			
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();				
			$scope.FieldAssistantData[id][b] = twoSpace;				
		};		
		
		//-------------Get Maxid--------------
		$scope.loadFieldAssistantId = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
				{
					$scope.AddedFieldAssistant={'fieldAssistantId':data.id,'status':'0','modifyFlag':"No",'screenName':'FieldAssistantMaster'};
		        });	   	   
		};
  		//------------Load Officer Names--------
		$scope.loadFieldOfficerNames = function()
		{
			$scope.officerNames=[];
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDropDown,
				   }).success(function(data, status) 
				   {
						$scope.officerNames=data;
					});  	   
	    };
  		//------------Load Field Assistants Names--------
		$scope.loadFieldAssistantsNames = function()
		{
			$scope.officerNames=[];
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: CaneStringDropDown,
				   }).success(function(data, status) 
				   {
						$scope.AssistantNames=data;
					});  	   
	    };		
		//----------Load Field Assistants-------								
		$scope.loadAllFieldAssistants = function()
		{		
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllFieldAssistants.html",
						data: {}
				   }).success(function(data, status) 
				   {
						$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				   });
		};
		//-----------Add Field Assistants---------
		$scope.fieldoassistantfficersubmit = function(AddedFieldAssistant,form)
		{
			  //$scope.Addsubmitted = true;			  
			  var AddedFieldAssistant = JSON.stringify(AddedFieldAssistant);   /*------------for getting data from form-----------*/
			  if($scope.FieldAssistantMaster.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveFieldAssistant.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedFieldAssistant,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", "Field Assistant Added Successfully!", "success");  //-------For showing success message-------------						
							$('.btn-hide').attr('disabled',false);
							$scope.AddedFieldAssistant = angular.copy($scope.master);
							$scope.loadFieldAssistantId();
							form.$setPristine(true);

							
							//--------Get All field Assistants----
					        var httpRequest = $http({
				            			method: 'POST',
							            url : "/SugarERP/getAllFieldAssistants.html",
							            data: {}
						           }).success(function(data, status) 
								   {
				        			    $scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
										//$scope.AddedFieldAssistant = {};    		//-------for clear the form-------------------------------------						
						        	});
						   //---------Get max id------------------
							/*var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getMaxId.html",
										data: jsonString,
								   }).success(function(data, status) 
									{	
										$scope.AddedFieldAssistant={'fieldAssistantId':data.id,'status':'0'};
							        });							   */
							//---------end------------------------
						}
						else
						{
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						}
						
					}
		        });
			  }
			 else
			  {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}				  
			  }
		  };
		//-----------Update Field Assistants---------
		$scope.fieldassistantupdate = function(FieldAssistant,id)
		{
			
			  delete FieldAssistant["$edit"];
			  
			  var AddedFieldAssistant = angular.toJson(FieldAssistant);

			  $scope.isEdit = true;
			  $scope.submitted = true;			  
			 // alert($scope.FieldAssistantMasterEdit.$valid);
			  if($scope.FieldAssistantMasterEdit.$valid) 
			  {	
			  	$scope.isEdit = false;
				//$('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveFieldAssistant.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedFieldAssistant,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
							//alert(response);
						if(response==true)
						{
							swal("Good job!", 'Field Assistant Updated Successfully!', "success");  //-------For showing success message-------------						
							//$('.btn-hide').attr('disabled',false);

							
							//--------Get All field Assistants----
					        var httpRequest = $http({
				            			method: 'POST',
							            url : "/SugarERP/getAllFieldAssistants.html",
							            data: {}
						           }).success(function(data, status) 
								   {
				        			    $scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
						        	});
						 }
						else
						{
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   //$('.btn-hide').attr('disabled',false);
						}

					}
		        });
			  }
		  };	
		  
		  //$scope.selectFieldOfficer=function(employeeId,fieldOfficerId){
//			if($scope.AddedFieldAssistant.employeeId!=null  && $scope.AddedFieldAssistant.fieldOfficerId!=null){
//			$scope.FieldAssistantData.forEach(function(FieldAssistant){
//		if(FieldAssistant.employeeId == $scope.AddedFieldAssistant.employeeId &&  FieldAssistant.fieldOfficerId == $scope.AddedFieldAssistant.fieldOfficerId){
//			alert("if");
//		$('.btn-hide').attr('disabled',true);
//		$('#errorMsg').show();
//		}
//		else if(FieldAssistant.employeeId != $scope.AddedFieldAssistant.employeeId &&  FieldAssistant.fieldOfficerId != $scope.AddedFieldAssistant.fieldOfficerId)
//		{
//			alert("else");
//		$('.btn-hide').attr('disabled',false);
//		$('#errorMsg').hide();
//		}
//
//});
//  
// }
//};
//		  $scope.selectFieldOfficer=function(employeeId,fieldOfficerId){
			$scope.selectFieldOfficer=function(employeeId,fieldOfficerId)
			{
				if($scope.AddedFieldAssistant.employeeId!=null  && $scope.AddedFieldAssistant.fieldOfficerId!=null)
				{
			//$scope.FieldAssistantData.forEach(function(FieldAssistant){
					for(var i=0;i<$scope.FieldAssistantData.length;i++)
					{
				//alert($scope.AddedFieldAssistant.employeeId == $scope.FieldAssistantData[i].employeeId &&  $scope.AddedFieldAssistant.fieldOfficerId ==  $scope.FieldAssistantData[i].fieldOfficerId)	
						if($scope.AddedFieldAssistant.employeeId == $scope.FieldAssistantData[i].employeeId &&  $scope.AddedFieldAssistant.fieldOfficerId ==  $scope.FieldAssistantData[i].fieldOfficerId){
						$('#errorMsg').show();
						$('.btn-hide').attr('disabled',true);
						return false;
						}
						else
						{
						$('#errorMsg').hide();
						$('.btn-hide').attr('disabled',false);
						}
					}

				}
		};

		  
		  //---------Reset Form----
		   $scope.reset = function(form)
		   {
				$('#errorMsg').hide();
				$('.btn-hide').attr('disabled',false);
				$scope.AddedFieldAssistant = angular.copy($scope.master);
				$scope.loadFieldAssistantId();
			    form.$setPristine(true);			    											   
		   };
		  
	 })
	 
	//=================================================
		  //Cane Manager Master
    //=================================================

	 .controller('CaneManagerMaster', function($scope,$http)
	 {	 
		$scope.mgrNames=[];  
		var obj = new Object();
		obj.tablename = "CaneManager";
		var jsonString= JSON.stringify(obj);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Employees";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		$scope.master = {};
		//-------------Space Between Two Words --------
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedCane[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedCane[b] = twoSpace;				
		};
		//------------Space Between Two Words (Grid)----
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.CaneData[id][b];			
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();			
			$scope.CaneData[id][b] = twoSpace;				
		};		
		
		//------Load Cane Manager Names--------
		$scope.loadCaneManagerNames = function()
		{		
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDropDown,
				   }).success(function(data, status) 
					{	
						$scope.mgrNames=data;			
					});  	   
	   };
	   //-------------Load Added Cane Managers---	  	  
		$scope.loadAllCaneManagers = function()
		{		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllCaneManagers.html",
					data: {}
			   }).success(function(data, status) 
			   {
					$scope.CaneData = data; 		//-------for load the data in to added departments grid---------		
			   });
  	   };
	   //-------------Get Max id-----------	 
		$scope.loadCaneManagerId = function()
		{	
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
				{
					$scope.AddedCane={'caneManagerId':data.id,'status':'0','modifyFlag':"No",'screenName':'CANE MANAGER MASTER'};						
		        });	   	   
	    };
	 	//---------------Add Cane Managers-----	 
		 $scope.CaneData=[];
		 $scope.canesubmit = function(AddedCane,form)
		  {
			  //$scope.submitted = true;
			  var AddedCane = JSON.stringify(AddedCane);   /*------------for getting data from form-----------*/

			  if($scope.CaneMaster.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveCaneManager.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedCane,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", 'Cane Manager Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.AddedCane = angular.copy($scope.master);
							$scope.loadCaneManagerId();
							form.$setPristine(true);

							//-------Load All Added Cane Managers-------------						
					        var httpRequest = $http({
				            		method: 'POST',
						            url : "/SugarERP/getAllCaneManagers.html",
						            data: {}
			    		       }).success(function(data, status) 
							   {	
						            $scope.CaneData = data; 		//-------for load the data in to added departments grid-----									                                    
	  			        	   });
								//-------get Max id-------
								/*var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getMaxId.html",
											data: jsonString,
									   }).success(function(data, status) 
										{
											$scope.AddedCane={'caneManagerId':data.id,'status':'0'};										
								        });	   	   */
							 //--------------end---------
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };
		  //---------Update Added Cane Managers----
		  $scope.updateCaneMaster = function(Cane,id)
		  {		  	  			  
			  delete Cane["$edit"];
			  var UpdatedData = angular.toJson(Cane);
			  //alert(UpdatedData);
			  $.ajax({
				    url:"/SugarERP/saveCaneManager.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						
						if(response==true)
						{
			  				swal("Good job!", 'Cane Manager Added Successfully!', "success");  //-------For showing success message-------------		
							//-------Load All Added Cane Managers-------------						
					        var httpRequest = $http({
				            		method: 'POST',
						            url : "/SugarERP/getAllCaneManagers.html",
						            data: {}
			    		       }).success(function(data, status) 
							   {	
						            $scope.CaneData = data; 		//-------for load the data in to added departments grid---------									                                    //$scope.AddedCane = {}; 
	  			        	   });
							//---------end---------
						}
					   else
					    {
							 sweetAlert("Oops...", "Something went wrong!", "error");
						}
					}
				});			  
		  };
		  
		  //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedCane = angular.copy($scope.master);
				$scope.loadCaneManagerId();
			    form.$setPristine(true);			    											   
		   };
		  
	 })
	//=================================================
 		 //Variety Master
    //=================================================

	 .controller('varietyMaster', function($scope,$http)
	 {
	 
	 	var obj = new Object();
		obj.tablename = "Variety";
		var jsonTable= JSON.stringify(obj);

		$scope.master = {};
		//$scope.isRequired = true;
		$scope.isEdit = true;
		
		//----Duplicate String----
		$scope.validateDup = function()
			  {
					for(var d =0;d<JSON.stringify($scope.varietyData.length);d++)
					{
						var firstString = $scope.AddedVarietyMaster.variety.toUpperCase();
						var secondString = $scope.varietyData[d].variety.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.varietyData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.varietyData[i].variety.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedVarietyMaster[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedVarietyMaster[b] = twoSpace;				
		};
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.varietyData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.varietyData[id][b] = twoSpace;				
		};		
		
	 	//---------Load Max id-----------
		$scope.loadVarietyId = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   }).success(function(data, status) 
				{
					$scope.AddedVarietyMaster={'varietyCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Variety Master'};
		        });  	   
	    };
  		//-----------Load all Added Varities----   
  	    $scope.loadAllVarieties = function()
		{		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllVarietyMasters.html",
					data: {}
			    }).success(function(data, status) 
			    {
					$scope.varietyData = data; 		//-------for load the data in to added departments grid---------
				});
	   };
       //---------Add Variety--------------
		$scope.varietyData=[];
		$scope.varietymastersubmit = function(AddedVarietyMaster,form)
		{
			//$scope.isRequired = true;			
			//$scope.Addsubmitted = true;			  
			var AddedVarietyMaster = JSON.stringify(AddedVarietyMaster);   /*------------for getting data from form-----------*/
			
			if($scope.VarietyMaster.$valid) 
			{	
				$('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveVarietyMaster.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedVarietyMaster,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", 'Variety Added Successfully!', "success");  //-------For showing success message-------------							
							$('.btn-hide').attr('disabled',false);
							$scope.AddedVarietyMaster = angular.copy($scope.master);
							$scope.loadVarietyId();
							form.$setPristine(true);
							
							//------Load Added Varieties----
					        var httpRequest = $http({
							            method: 'POST',
							            url : "/SugarERP/getAllVarietyMasters.html",
				        			    data: {}
						           }).success(function(data, status) 
								   {									  
				        			    $scope.varietyData = data;										
										
					        	   });
							 //------Load Maxid-------
							/*var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getMaxId.html",
									data: jsonTable,	
							      }).success(function(data, status) 
								   {
 										$scope.AddedVarietyMaster={'varietyCode':data.id,'status':'0'};
										//$scope.isRequired = false;																					
   								  });*/
							//--------End------------
						  }
						 else
						  {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);							  
						  }
  				    }
		         });				 	
			  }	
			 else
			  {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
			  }
		  };
	 
	 //-------------Update Variety----------------
	 
	 $scope.updateVarietyMaster = function(variety,id)
	 {		  	  			  
			  delete variety["$edit"];
			  var UpdatedData = angular.toJson(variety);
			  
			  $scope.isEdit = true;
			  
			  $scope.submitted = true;
			  if($scope.varietymasteredit.$valid) 
			  {
			  	$scope.isEdit = false;
			  $.ajax({
				    url:"/SugarERP/saveVarietyMaster.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
						if(response==true)
						{
							swal("Good job!", 'Variety Updated Successfully!', "success");  //-------For showing success message-------------													
							//------Load Added Varieties----
					        var httpRequest = $http({
							            method: 'POST',
							            url : "/SugarERP/getAllVarietyMasters.html",
				        			    data: {}
						           }).success(function(data, status) 
								   {
				        			    $scope.varietyData = data; 										
										//$scope.AddedVarietyMaster = {}; 
					        	   });			
							//------End------------
						}		
					   else
					    {
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
					}
				});		
			  }
		  };
		  //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedVarietyMaster = angular.copy($scope.master);
				$scope.loadVarietyId();
			    form.$setPristine(true);			    											   
		   };
		  
	 })

	//================================================
		//Land Type Master ---Added By Suresh
	//================================================
		.controller('RyotTypeMaster', function($scope,$http)
		{
			//============Get MAx id on Page onload=========

			 var obj = new Object();
			obj.tablename = "LandType";
			var jsonTable= JSON.stringify(obj);
						
			$scope.isEdit = true;
			$scope.master = {};
			
			$scope.loadRyotTypeId = function()
			{		
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
				//data: {}
			   }).success(function(data, status) 
				{
					$scope.AddRyot={'landTypeId':data.id,'status':'0','modifyFlag':"No",'screenName':'LandType'};			
		        });	   	   
		    };
			//-----Duplicate String----
			$scope.validateDup = function()
		  {
			//alert(JSON.stringify($scope.AddedRyot));
				for(var d =0;d<$scope.AddedRyot.length;d++)
				{
					 //alert($scope.SeasonData);
					var firstString = $scope.AddRyot.landType.toUpperCase();
					var secondString = $scope.AddedRyot[d].landType.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  	$scope.validateDuplicate = function(desigName,id)
		  {
			 for(var i =0;i<JSON.stringify($scope.AddedRyot.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				var str2 = $scope.AddedRyot[i].landType.toUpperCase();
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };			
			
			//-------------Space Between Two Words -----------
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddRyot[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddRyot[b] = twoSpace;				
			};
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.AddedRyot[id][b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedRyot[id][b] = twoSpace;
				
			};			
			
	//--------
			
		    $scope.model = { selected: {} };

		    // gets the template to ng-include for a table row / item
		    $scope.getTemplate = function (EditAddedRyots) {
				//alert($scope.model.selected.id);
				//alert(EditAddedRyots.id);
        		if (EditAddedRyots.id === $scope.model.selected.id) return 'edit';
		        else return 'display';
		    };
	
    		$scope.editContact = function (contact) {
		        $scope.model.selected = angular.copy(contact);
		    };

		  //=======Load All Ryot Types on page onload======
			$scope.loadAllRyotTypes = function()
			{		
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllLandTypes.html",
					//data: jsonTable,
					data: {}
			   }).success(function(data, status) 
				{
					$scope.AddedRyot=data;				
		        });	   	   
		    };		  
		  
		  $scope.statusok = true;
         //=====================Add Ryots==================
		$scope.AddRyotType = function(AddRyot,form)
		{
			$scope.statusok = true;
			//$scope.Addsubmitted = true;
			//setTimeout(function()
			//{
				if($scope.RyotTypeForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
				    $.ajax({
					    url:"/SugarERP/saveLandType.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddRyot),
						beforeSend: function(xhr) 
						{
			    	        xhr.setRequestHeader("Accept", "application/json");
			        	    xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
						
							if(response==true)
							{
								swal("Success!", 'LandType Added Successfully!', "success");  //-------For showing success message-------------	
								$('.btn-hide').attr('disabled',false);
								$scope.AddRyot = angular.copy($scope.master);
								$scope.loadRyotTypeId();
								form.$setPristine(true);
								
								//===========Load Max ID===========
								/*var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getMaxId.html",
										data: jsonTable,
								     }).success(function(data, status) 
									 {
										$scope.AddRyot={'landTypeId':data.id,'status':'0'};				
							        });*/	 
								//============load All Added Ryots==
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllLandTypes.html",
										data: {}
									 }).success(function(data, status) 
									 {
										$scope.AddedRyot=data;				
							        });	   	   
								$scope.statusok = false;
								$scope.RyotTypeForm.$invalid = false;

							 //===========End===================
						   }
						  else
						   {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);							   
						   }
					}				
		        });
			 }
			else
			 {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}				 
			 }
		//},1);	  
	};
	//------------------Reset From-----------
	//---------Reset Form----
	$scope.reset = function(form)
	{			  
		$scope.AddRyot = angular.copy($scope.master);
		$scope.loadRyotTypeId();
	    form.$setPristine(true);			    											   
	};
/*	$scope.setEditValue = function(Value)
	{
		//alert(Value);
		$scope.EditPrevValue = $scope.EditValue;	
		if($scope.EditPrevValue==null) { $scope.EditPrevValue=Value;}
		//$('#changeIcon'+$scope.EditPrevValue).removeClass('active');
		//$('#changeIcon'+$scope.EditPrevValue).addClass('active:false');*/
		//$scope.EditAddedRyots.$edit = false;
	/*	$scope.EditValue = Value;
	};*/
	//==========================Update Ryot Types==============
		$scope.UpdateRyotTypes = function(EditAddedRyots,id)
		{
			  $scope.isEdit = true;
			  delete EditAddedRyots["$edit"];
			  var UpdatedRyotTypeData = angular.toJson(EditAddedRyots);
			  
			  $scope.submitted=true;
			  if($scope.UpdateLandTypeForm.$valid) 
			  {
			    $scope.isEdit = false;
			    $.ajax({
				    url:"/SugarERP/saveLandType.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedRyotTypeData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success!", 'LandType Updated Successfully!', "success");  //-------For showing success message-------------
							//============load All Added Ryots==
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllLandTypes.html",
										data: {}
									 }).success(function(data, status) 
									 {										
										$scope.AddedRyot=data;		
										$scope.model = { selected: {} };
										//$scope.AddedRyot[id] = angular.copy($scope.model.selected);
							        });	   	   
							//===========End===================							
						}
					   else 
					    {
							 sweetAlert("Oops...", "Something went wrong!", "error")
						}
					}
			     });			  
			  }
		};
	//======================End================================
   })
		
		
	
	//=========================================================
			//Zone Master  ----Added by Suresh
	//=========================================================
	
	.controller('ZoneMaster', function($scope,$http)
	{
			 var obj = new Object();
			obj.tablename = "Zone";
			var jsonTable= JSON.stringify(obj);

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Region";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);

			var fieldDropDown = new Object();
			fieldDropDown.dropdownname = "FieldOfficer";
			var StringDropDown= JSON.stringify(fieldDropDown);
			
			$scope.isEdit = true;
			$scope.master = {};

			//----Duplicate String-----
			$scope.validateDup = function()
			  {				  
					for(var d =0;d<JSON.stringify($scope.AddedZones.length);d++)
					{
						var firstString = $scope.AddZone.zone.toUpperCase();
						var secondString = $scope.AddedZones[d].zone.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			   $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AddedZones.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AddedZones[i].zone.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };


			//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddZone[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddZone[b] = twoSpace;				
			};
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.AddedZones[id][b];				
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();				
				$scope.AddedZones[id][b] = twoSpace;				
			};

			//============Get MAx id on Page onload=========
			$scope.loadZoneCode = function()
			{		
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   }).success(function(data, status) 
				{
					$scope.AddZone={'zoneCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Zone Master'};					
		        });	   	   
		    };
		   //==========Field Officer Names=========
			$scope.loadFieldOfficerNames = function()
			{
				$scope.officerNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
				   }).success(function(data, status) 
					{
						$scope.officerNames=data;
					});  	   
		  };		  
		  //===========get Region Names==========
			$scope.loadRegionNames = function()
			{
				$scope.officerNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						$scope.regionNames=data;
					});  	   
		  };		  
		  //-----------Get all Added zones-------
			$scope.loadAddedZones = function()
			{
				$scope.AddedZones=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllZoneMasters.html",
					data: {}
				   }).success(function(data, status) 
					{
						$scope.AddedZones=data;
					});  	   
		  };
		  
		   
		   
		//--------Save zones---------	
			$scope.AddZones = function(AddZone,form)
			{
				//$scope.Addsubmitted = true;
				if($scope.ZoneMasterForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveZoneMaster.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddZone),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", "Zone Added Successfully!", "success");
									$('.btn-hide').attr('disabled',false);

									$scope.AddZone = angular.copy($scope.master);
									$scope.loadZoneCode();
									form.$setPristine(true);

									//---------get all Zones--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllZoneMasters.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.AddedZones=data;
											//$scope.AddZone={};
										});  	   
									//---------End--------
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddZone={'zoneCode':data.id,'status':'0'};				
										        });	   	   */
								  //-----------end--------
									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);
									
								}
							}
						});					
				}
			  else
			   {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}				   
			   }
			};
		//---------end---------------
		//---------Update Zones------
		
		$scope.UpdateAddedZones = function(UpdateZones,id)
		{
			delete UpdateZones["$edit"];
			delete UpdateZones["caneManagerId"];
			
			$scope.isEdit = true;
			$scope.submitted = true;
			if($scope.EditZoneMasterFrom.$valid)
			{
				$scope.isEdit = false;
			  $.ajax({
			   		url:"/SugarERP/saveZoneMaster.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:angular.toJson(UpdateZones),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
					},
					success:function(response) 
					{	
						if(response==true)
						{
							swal("Success!", "Zone Updated Successfully!", "success");
							//---------get all Zones--------									
							var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllZoneMasters.html",
									data: {}
							 }).success(function(data, status) 
							 {
								$scope.AddedZones=data;
							 });  	   
							//---------End--------
						 }
						else
						 {
							 sweetAlert("Oops...", "Something went wrong!", "error");
						 }
					 }
				});			
			}
			//-----------end----------			
		};		
		//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddZone = angular.copy($scope.master);
				$scope.loadZoneCode();
			    form.$setPristine(true);			    											   
		   };
		
	})
	
	//=================================================
  		//Region Master
    //=================================================

	 .controller('RegionMaster', function($scope,$http)
	 {
		var obj = new Object();
		obj.tablename = "Region";
		var jsonTable= JSON.stringify(obj);
		$scope.isEdit = true;
		$scope.master = {};
		
		//-------Duplicate Entry----
		$scope.validateDup = function()
		  {
				for(var d =0;d<JSON.stringify($scope.regionData.length);d++)
				{
					 //alert($scope.SeasonData);
					var firstString = $scope.AddedRegionMaster.region.toUpperCase();
					var secondString = $scope.regionData[d].region.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  $scope.validateDuplicate = function(desigName,id)
		  {
			 for(var i =0;i<JSON.stringify($scope.regionData.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				var str2 = $scope.regionData[i].region.toUpperCase();
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };		
		
		//--------Space Between Two Words -------
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedRegionMaster[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedRegionMaster[b] = twoSpace;				
		};
		//--------Space Between Two Words (Grid)---
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.regionData[id][b];				
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();				
			$scope.regionData[id][b] = twoSpace;				
		};		
		
		//----------laod Region id----------
		$scope.loadRegionId = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxId.html",
			data: jsonTable,
		   }).success(function(data, status) 
			{
			$scope.AddedRegionMaster={'regionCode':data.id,'status':'0','modifyFlag':"No",'screenName':'RegionMaster'};
			
			});	   	   
		};
				
		//--------------load added regions-----------

		$scope.loadAddedRegions = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllRegions.html",
			data: jsonTable,
		   }).success(function(data, status) 
			{
				$scope.regionData=data;			
			});	   	   
		};

		 //--------------end-----------------
		
  
		$scope.regionData=[];
		$scope.RegionmasterSubmit = function(AddedRegionMaster,form)
		{
			//$scope.Addsubmitted = true;			  
			var AddedRegionMaster = JSON.stringify(AddedRegionMaster);   /*------------for getting data from form-----------*/
			if($scope.RegionMasterForm.$valid) 
			{	
			   $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveRegion.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedRegionMaster,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", "Region Added Successfully!", "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.AddedRegionMaster = angular.copy($scope.master);
							$scope.loadRegionId();
							form.$setPristine(true);
							
							
					        var httpRequest = $http({
				            method: 'POST',
				            url : "/SugarERP/getAllRegions.html",
				            data: {}
			           }).success(function(data, status) 
					   {
				            $scope.regionData = data; 	
							//-------for load the data in to added departments grid---------
							//$scope.AddedRegionMaster = {}; 
							
							/*var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getMaxId.html",
							data: jsonTable,
						   }).success(function(data, status) 
							{
								$scope.AddedRegionMaster={'regionCode':data.id,'status':'0'};	
								//$scope.AddedFieldOfficer = {};
								//$scope.FieldOfficerMaster.$setPristine();
								//$scope.FieldOfficerMaster.$setUntouched();										
								
							});*/
							//-------for clear the form-------------------------------------						
			        	});
						}
					  else
			           {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);				   
  			           }
				}
			 
		      });
			  }
		   	 else
			  {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}				  
			  }
		  };
		  
		$scope.regionUpdate = function(region,id)
		{
			delete region["$edit"];
			var UpdatedData = angular.toJson(region);
			$scope.submitted = true;
			$scope.isEdit = true;
			if($scope.regionmasteredit.$valid) 
			{
				$scope.isEdit = false;
			 $.ajax({
				    url:"/SugarERP/saveRegion.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },

				    success:function(response) 
					{
						if(response==true)
						{
			  				swal("Success", 'Region Updated Successfully!', "success");
					        var httpRequest = $http({
					            method: 'POST',
					            url : "/SugarERP/getAllRegions.html",
					            data: {}
					           }).success(function(data, status) 
							   {
				            		$scope.regionData = data; 	
							   });							
						}
					  else
					   {
						   sweetAlert("Oops...", "Something went wrong!", "error");
					   }

					}

				});		
			}

		  };  
		  //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedRegionMaster = angular.copy($scope.master);
				$scope.loadRegionId();
			    form.$setPristine(true);			    											   
		   };		  
	 })
	 
	 //===========================================================
	 		//Mandal Master
	 //===========================================================
	 .controller('MandalMaster', function($scope,$http)
	 {
		var obj = new Object();
		obj.tablename = "Mandal";
		var jsonTable= JSON.stringify(obj);		 
		 
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Zone";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		 
		$scope.isEdit = true;
		$scope.master = {};
		
		//----Duplicate String----
		$scope.validateDup = function()
			  {
					for(var d =0;d<JSON.stringify($scope.AddedMandals.length);d++)
					{
						var firstString = $scope.AddMandal.mandal.toUpperCase();
						var secondString = $scope.AddedMandals[d].mandal.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AddedMandals.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AddedMandals[i].mandal.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddMandal[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddMandal[b] = twoSpace;				
		};
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AddedMandals[id][b];			
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();			
			$scope.AddedMandals[id][b] = twoSpace;			
		};

		 //-------get Max mandal id-----------------
		 	$scope.loadMaxMandalId = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.AddMandal={'mandalCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Mandal Master'};														
						});								
			};
		 //----------end---------------------------
		 
		 //-----------Load Zones names------------
		 	$scope.loadZoneNames = function()
			{
				var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: jsonStringDropDown,
							 }).success(function(data, status) 
							 {
								$scope.ZoneData=data;
								//alert(JSON.stringify(data));
							 });  	   
			};		 
		 //--------------end-----------------------
		 //--------------Load Added Mandals--------
		   $scope.loadAddedMandals = function()
		   {
		 	   var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getAllMandals.html",
								data: {},
								}).success(function(data, status) 
								{
									$scope.AddedMandals = data;
								});																																										
		  }
		 //--------------End-----------------------
		 
		 //---------------Add Mandals----------------
		 
		 	$scope.SubmitMandals = function(AddMandal,form)
		 	{
				//$scope.Addsubmitted = true;
				if($scope.MandalMasterForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveMandal.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddMandal),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							
							if(response==true)
							{
								swal("Success", 'Mandal Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								$scope.AddMandal = angular.copy($scope.master);
								$scope.loadMaxMandalId();
								form.$setPristine(true);
								
								//-------load Max ID-------
								/*var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getMaxId.html",
										data: jsonTable,
									   }).success(function(data, status) 
										{
											$scope.AddMandal={'mandalCode':data.id,'status':'0'};							
										});		*/																						
								//---------End---------------------
								//--------Load Added Mandals-------
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllMandals.html",
										data: {},
									   }).success(function(data, status) 
										{
											$scope.AddedMandals = data;
										});																																								
								//----------End--------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};
		 //--------------End-------------------------
		 //--------------Update Mandals--------------
		 $scope.UpdateAddedMandals = function(UpdateMandal,id)
		 {
				delete UpdateMandal["$edit"];
				var UpdatedMandalData = angular.toJson(UpdateMandal);
				$scope.submitted = true;
				$scope.isEdit = true;
				if($scope.UpdateMandalMasterForm.$valid) 
				{	
					$scope.isEdit = false;
					
					$.ajax({
				    	url:"/SugarERP/saveMandal.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:UpdatedMandalData,
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Mandal Updated Successfully!', "success");
								//--------Load Added Mandals-------
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllMandals.html",
										data: {},
									   }).success(function(data, status) 
										{
											$scope.AddedMandals = data;
										});																																								
								//----------End--------------------
								
							}
							else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});
					
					//----------load Added Mandals-------										
				}			 
		 };		 
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddMandal = angular.copy($scope.master);
				$scope.loadMaxMandalId();
			    form.$setPristine(true);			    								
			   
		   };		 
	 })
	 
	 //=======================================================
	 	//Village Master
	 //=======================================================
	 
	 .controller('VillageMaster', function($scope,$http)
	 {
 		$scope.AddVillage={'status':'0','modifyFlag':"No",'screenName':'Village Master','start':'Sarvaraya Sugars'};
		//var obj = new Object();
		//obj.tablename = "Village";
		//var jsonTable= JSON.stringify(obj);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Mandal";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		var directionDisplay;
		//alert(directionDisplay);
		var directionsService = new google.maps.DirectionsService();
		//alert(directionsService);
		var map;
		//alert(map);
		$scope.isEdit=true;
		$scope.master = {};
		
		$scope.getInitialize = function()
		{
			directionsDisplay = new google.maps.DirectionsRenderer();
			//alert(directionDisplay);
			var melbourne = new google.maps.LatLng(-37.813187, 144.96298);
			//alert(melbourne);
			var myOptions = {
				zoom:12,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: melbourne
			}

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			
			directionsDisplay.setMap(map);
			//alert("map"+map);
			//
		};
		
		$scope.calcRoute = function()
		{
			var start = $("#start").val();
			//alert(start);
			var end = $("#villageName").val();
			//alert(end);
			var distanceInput = document.getElementById("distance");
			//alert("distance"+distanceInput);
			
			var request = {
				origin:start, 
				destination:end,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};
				//alert("Request"+request);
			directionsService.route(request, function(response, status) {
													  //alert("status"+status);
													  //alert(google.maps.DirectionsStatus.OK);
				if (status == google.maps.DirectionsStatus.OK) {
						//if (status == REQUEST_DENIED) {
					directionsDisplay.setDirections(response);
					distanceInput.value = response.routes[0].legs[0].distance.value / 1000;
					//alert("Km"+distanceInput.value);
				}
			});
			//alert(distanceInput.value);
		};
		//---------Validtae Duplicate Entrys----
		 $scope.validateDup = function()
		  {
				for(var d =0;d<JSON.stringify($scope.AddedVillages.length);d++)
				{
					var firstString = $scope.AddVillage.villageName.toUpperCase();
					var secondString = $scope.AddedVillages[d].villageName.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  //=============================	Get Distance ======================
		  
		
		  
		  //==================================================================
		  
		  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AddedVillages.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AddedVillages[i].villageName.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		
		//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddVillage[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddVillage[b] = twoSpace;				
			};
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.AddedVillages[id][b];				
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();				
				$scope.AddedVillages[id][b] = twoSpace;				
			};
			
			/*$scope.decimalpointgrid = function(b,id)
			{
				var decmalPoint = $scope.AddedVillages[id][b];				
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				$scope.AddedVillages[id][b] = getdecimal;								
			};*/
						
			//=============Amounts in Decimals ==================
			$scope.decimalpoint = function(b)
			{
				var decmalPoint = $scope.AddVillage[b];				
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				$scope.AddVillage[b] = getdecimal;								
			};
		 
		 //-------get Max village id-----------------
		 	/*$scope.loadMaxVillageId = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.AddVillage={'villageCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Village Master'};																				
						});								
			};*/
		 //----------end---------------------------
		 //-------------Load Mandals---------------
		 	$scope.loadMandals  = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDropDown,
					   }).success(function(data, status) 
						{
							$scope.MandalData=data;							
						});														
			};
		 
		 //----------Set new village code on mandal onchange---
		 $scope.setNewVillageCode = function(mandalCode)
		 {
			// alert(2);
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadMaxIdForVillage.html",
						data: mandalCode,
					   }).success(function(data, status) 
						{

							$scope.AddVillage.villageCode=data[0].id;	
						});				 
		 };
		 //--------Set new village code on mandal code onchange update----
		 $scope.setNewVillageCodeUpdate = function(mandalCode,id)
		 {
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadMaxIdForVillage.html",
						data: mandalCode,
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data[0].id));
							$scope.AddedVillages[id].villageCode=data[0].id;	
						});				 			 
		 };
		 //---------------end----------------------		 
		 //--------load all Aded Villages----------
		  $scope.LoadAddedVillages = function()
		  {
		    	var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getAllVillages.html",
								data: {},
			  			    }).success(function(data, status) 
							{
								//alert(JSON.stringify(data));
								$scope.AddedVillages=data;							
							});															  
		  }
	   //----------------end---------------------
		 
		 
		 //-----------Add Village----------
		 $scope.AddVillages = function(AddVillage,form)
		 {			 
			//$scope.Addsubmitted = true;
			if($scope.VillageMasterForm.$valid) 
			{
				$('.btn-hide').attr('disabled',true);
				delete AddVillage['start'];
					$.ajax({
				    	url:"/SugarERP/saveVillage.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddVillage),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							//alert(response);
							if(response==true)
							{
								swal("Success", 'Village Added Successfully!', "success");				
								$('.btn-hide').attr('disabled',false);	
								
								$scope.AddVillage = angular.copy($scope.master);
								//$scope.loadMaxVillageId();
			    				form.$setPristine(true);			    								

							    //-------get Max mandal id-----------------
								/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddVillage={'villageCode':data.id,'status':'0'};		
														//$scope.master = {'villageCode':data.id,'status':'0'};		
													});								*/
						 	  //----------end---------------------------
							  //--------load all Aded Villages----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllVillages.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedVillages=data;							
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			}		
			else
			{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
			}
		 };
		 //-----------------Update Village--------------------------
		   $scope.UpdateAddedVillages = function(UpdateVillage,id)
		   {
				delete UpdateVillage["$edit"];
				var UpdateVillageData = angular.toJson(UpdateVillage);
				$scope.submitted = true;
				$scope.isEdit=true;
				if($scope.UpdateVillageMaster.$valid) 
				{	
					$scope.isEdit=false;
					$.ajax({
				    	url:"/SugarERP/saveVillage.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:UpdateVillageData,
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Village Updated Successfully!', "success");				
								
							    //-------get Max mandal id-----------------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddVillage={'status':'0'};			
																	
													});								
						 	  //----------end---------------------------
							  //--------load all Aded Villages----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllVillages.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedVillages=data;							
													});															  
							  //----------------end---------------------
							}
						   else
						    {
								 sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});									
				}			   
		   };		 		 
		   
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddVillage = angular.copy($scope.master);
				//$scope.loadMaxVillageId();
			    form.$setPristine(true);			    								
		   };
		   
		   

		   
		   
	 })
	 
	 //===============================================
	 		//Circle Master
	 //===============================================
	 
	 .controller('CircleMaster', function($scope,$http)
	 {
		 
		var obj = new Object();
		obj.tablename = "Circle";
		var jsonTable= JSON.stringify(obj);		
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);

		var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
		var zoneDropDown = new Object();
		zoneDropDown.dropdownname = "Zone";
		var zoneStringDropDown= JSON.stringify(zoneDropDown);
		
		$scope.isEdit=true;
		$scope.master = {};
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddCircle[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddCircle[b] = twoSpace;				
		};
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AddedCircles[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedCircles[id][b] = twoSpace;				
		};

		//---------Validtae Duplicate Entrys----
		 $scope.validateDup = function()
		  {
				for(var d =0;d<JSON.stringify($scope.AddedCircles.length);d++)
				{
					var firstString = $scope.AddCircle.circle.toUpperCase();
					var secondString = $scope.AddedCircles[d].circle.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };	
		
		$scope.validateDuplicate = function(desigName,id)
		{
				 for(var i =0;i<JSON.stringify($scope.AddedCircles.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AddedCircles[i].circle.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
						$(".duplicate"+ id).show();
						return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
		 };
		


		 //-------------Load Max Circle Code-------
		 $scope.loandMaxCircleCode = function()
		 {
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.AddCircle={'circleCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Circle Master'};																				
						});											 
		 };
		 //-------------Load Field Assistants-------
		$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};		 
		 //-------------Load Villages-------
		$scope.loadVillageNames = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.VillagesNamesData = data; 		//-------for load the data in to added departments grid---------
				});
		};		 
		//----------Load Added Circles--------------
		$scope.loadAddedCircles = function()
		{
			var httpRequest = $http({
							method: 'POST',
			 			    url : "/SugarERP/getAllCircles.html",
							data: {},
						}).success(function(data, status) 
						{
							$scope.AddedCircles=data;							
						});															  
			
		};
		//-----------CIRCLE ZONES--------------
		$scope.getZonesByVillage = function(villageCode)
			{
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getZonesForVillage.html",
				data: villageCode,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data[0].id));
				//alert("xx"+JSON.stringify(data[0])+""+JSON.stringify(data));
				//$scope.AddCircle.zone=data[0];
					$scope.AddCircle.zone = data[0].id;
				});
			};
			//----onload Zones dropdown---
			$scope.loadZoneNames = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: zoneStringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.zoneNames = data; 	

				});
		};	


		//-------------Add Circles------------------
		
		$scope.AddCircles = function(AddCircle,form)
		{	
			//alert(90);
			//$scope.Addsubmitted = true;
			if($scope.CircleMasterForm.$valid) 
			{
				//alert(123);
				$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveCircle.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddCircle),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Circle Added Successfully!', "success");				
								 $('.btn-hide').attr('disabled',false);
								 $scope.AddCircle = angular.copy($scope.master);
								 $scope.loandMaxCircleCode();
								 form.$setPristine(true);
								 
							    //-------get Max mandal id-----------------
								/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddCircle={'circleCode':data.id,'status':'0'};								
													});								*/
						 	  //----------end---------------------------
							  //--------load all Aded Villages----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllCircles.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedCircles=data;							
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			}			
		   else
			{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			
		};
		//------------------Update Circle-------------		
		$scope.UpdateAddedCircles = function(UpdateCircle,id)
		{
				delete UpdateCircle["$edit"];
				var UpdateCircleData = angular.toJson(UpdateCircle);
				$scope.submitted = true;
				$scope.isEdit=true;
				if($scope.UpdateCircleForm.$valid) 
				{	
					$scope.isEdit=false;
					$.ajax({
				    	url:"/SugarERP/saveCircle.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:UpdateCircleData,
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Circle Updated Successfully!', "success");				
								
							    //-------get Max mandal id-----------------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddCircle={'circleCode':data.id,'status':'0'};							
													});								
						 	  //----------end---------------------------
							  //--------load all Aded Circles----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllCircles.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedCircles=data;							
													});															  
							  //----------------end---------------------
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});									
				}							
		};		
		//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				 $scope.AddCircle = angular.copy($scope.master);
				 $scope.loandMaxCircleCode();
			     form.$setPristine(true);			    											   
		   };
		
	 })
	 
	 //==================================================================
	 		//Bank Master
	 //==================================================================
	 .controller('BankMaster', function($scope,$http)
	 {
		 //-----------Add Banks--------
		 $scope.data = []; 
		 var gridData = [];
		 var obj = new Object();
		 
		 $index=0;
		 
		 var maxid = new Object();
		 maxid.tablename = "Bank";
		 var jsonTable= JSON.stringify(maxid);
		
		var DropDownLoad = new Object();
		DropDownLoad.tablename = "Bank";
		DropDownLoad.columnName = "bankcode";
		DropDownLoad.columnName1 = "bankname";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);	
		 
		 var DropDistrict = new Object();
		 DropDistrict.targetdropdownname = "District";
		 DropDistrict.columnname = "statecode";
		 
		 

		 var DropState = new Object();
		 DropState.dropdownname = "State";
		 var jsonStringState= JSON.stringify(DropState);
		 
		 var DropDistrictUpdate = new Object();
		 DropDistrictUpdate.dropdownname = "District";
		 var jsonStringDistrictUpdate= JSON.stringify(DropDistrictUpdate);
		 
		 $scope.master = {};
		 
		 //------for Reste------
		$scope.setRowAfterReset = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxId.html",
				data: jsonTable,
		     }).success(function(data, status) 
			 {
				$scope.AddBank={'bankCode':data.id,'modifyFlag':"No"};							
			    $index = 1;
				$scope.data = [{'id':$index,'status':'0'}]	
							
			});								
		};	
		 
		 //----------Validtae Duplicate Bank Name------
		 
		 $scope.validateDup = function(bankCode)		 
		 {
				for(var i =0;i<JSON.stringify($scope.AddedBankDropdown.length);i++)
				{
					var firstString = $scope.AddBank.bankName.toUpperCase();
					var secondString = $scope.AddedBankDropdown[i].bankname.toUpperCase();
					if(firstString!=null)
					{
						if(bankCode!=i+1)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);
								$('.duplicate').show();
								return false;
							}
						   else
						    {
								$('.btn-hide').attr('disabled',false);
								$('.duplicate').hide();
							}
						}
					}
				}
		  };
			  $scope.validateDuplicate = function(bName,id)
		  {
			  
		
				 /* var loadBranch = JSON.stringify($scope.data);*/
				
				 for(var i =0;i<$scope.data.length;i++)
				 {
					var str1 = bName.toUpperCase();
			
					var str2 = $scope.data[i].branchName.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hide').attr('disabled',true);						
						$(".duplicate"+ id).show();
						return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hide').attr('disabled',false);	
					 }
					}
				 }
			  };	 
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddBank[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddBank[b] = twoSpace;				
		};


		 //---------Load State Dropdown-----
		 $scope.loadStateDropDown  = function()
		 {
			 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringState,
				   }).success(function(data, status) 
					{
						$scope.AddStates=data;							
					});											 
		 };
		//---------Load District DropDown--------
		 $scope.loadDistrictDropDownUpdate  = function()
		 {
			 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDistrictUpdate,
				   }).success(function(data, status) 
					{
						$scope.AddDistricts=data;							
					});											 
		 };
		
		 //---------Load District Dropdown-----
		 $scope.loadDistrictDropDown  = function(StateCode)
		 {
			 
			DropDistrict.value = StateCode;
			var jsonDistrictDropDown= JSON.stringify(DropDistrict);

			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValues.html",
					data: jsonDistrictDropDown,
				   }).success(function(data, status) 
					{
						$scope.AddDistricts=data;
						for(var i=0;i<$scope.data.length;i++)
						{
							for(var j=0;j<$scope.AddStates.length;j++)
							{
								if($scope.data[i].stateCode==$scope.AddStates[j].id)
								{
									$scope.stateCodeTooltip = $scope.AddStates[j].state;
								}
							}
						}
					});								
		 };
		 	  //---------Load District Dropdown-----
		 $scope.loadDistrictsDropDown  = function(districtCode)
		 {
			


		
						for(var i=0;i<$scope.data.length;i++)
						{
							for(var j=0;j<$scope.AddDistricts.length;j++)
							{
								
								if($scope.data[i].districtCode==$scope.AddDistricts[j].id)
								{
									$scope.districtCodeTooltip = $scope.AddDistricts[j].district;
								}
							}
						}
										
		 };
		 
		 
		 //--------Set Tooltip Status----------
		 $scope.setTooltipStatus = function(status)
		 {
			 if(status==0) { $scope.statusTooltip = "Active"; } else { $scope.statusTooltip = "Inactive"; }
		 };
		 //---------get MAxId------------------
		 $scope.loandMaxBankCode = function()
		 {
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.AddBank={'bankCode':data.id,'modifyFlag':"No"};							
						});											 
		 };	
		 //---------get Added Banks---------------
		 $scope.loadAddedBanks = function()
		 {
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadAddedDropDownsForMasters.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.AddedBankDropdown=data;							
					});											 
		 };	
		 
		//-----------Add Row for Branch------
		$scope.addFormField = function() 
		{
			$index++;
		    $scope.data.push({'id':$index,'status':'0'})
		};		 		 
		 //---------Get all data from table---
		 $scope.AddBanks = function(AddBank,form)
		 {
				//$scope.submitted = true;
				if($scope.BankMasterForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (BranchData) 
					  {
						  delete BranchData['statusTooltip'];
					 delete BranchData['stateCodeTooltip'];

	            		 gridData.push(angular.toJson(BranchData));														 
				      });
					  obj.gridData = gridData;
				      obj.formData = angular.toJson(AddBank);
				      delete obj["$index"];
					  
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveBankMaster.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Bank Added Successfully!', "success");	
								$('.btn-hide').attr('disabled',false);
								$scope.AddBankDrop = angular.copy($scope.master);
								$scope.AddBank = angular.copy($scope.master);
								$scope.setRowAfterReset();
								form.$setPristine(true);
								$scope.loadAddedBanks();
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };
		 //------------end---------------------
		 //---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {			   	
		   	   if(modifyId!=null)
			   {
					$scope.AddBankDrop = angular.copy($scope.master);
					$scope.AddBank = angular.copy($scope.master);					
					$scope.setRowAfterReset();
					$scope.AddBank.modifyFlag = "Yes";
					$scope.loadAddedBanks();
				    form.$setPristine(true);			    								

			   }
			  else
			   {				   
					$scope.AddBankDrop = angular.copy($scope.master);
					$scope.AddBank = angular.copy($scope.master);					
					$scope.setRowAfterReset();
					$scope.AddBank.modifyFlag = "Yes";
					$scope.loadAddedBanks();
				    form.$setPristine(true);			    								
			   }
		   };
		 
		 //------------Delete Row from table---
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };		 
		//-------------Added Banks DropDown Onchange--------------------		 
		
		$scope.loadAddedBanksonChange = function()
		{
			if($scope.AddBankDrop.AddedBankName!=null)
			{
					
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllBanks.html",
					data: JSON.stringify($scope.AddBankDrop.AddedBankName),
				   }).success(function(data, status) 
				   {
					    $scope.loadDistrictDropDownUpdate();
						$scope.AddBank = data.bank;
						$scope.data=data.branch;
						$scope.AddBank.modifyFlag = "Yes";
						$scope.updateFlag = 'ok';
						$scope.updateLength = data.branch.length;
						$index = data.branch.length;
						
						
						for(var i=0;i<data.branch.length;i++)
						{
							for(var j=0;j<$scope.AddStates.length;j++)
							{
								if($scope.data[i].stateCode==$scope.AddStates[j].id)
								{
									$scope.data[i].stateCodeTooltip = $scope.AddStates[j].state;
								}
							}

							if($scope.data[i].status==0) { $scope.data[i].statusTooltip = "Active"; } else { $scope.data[i].statusTooltip = "Inactive"; }
						}
						
				   });								
			}
		  else
		   { 
		   		//$scope.AddBank = {};
				//$index = 1;
				//$scope.data = [{'id':$index,'status':'0'}]
					$scope.AddBankDrop = angular.copy($scope.master);
					$scope.AddBank = angular.copy($scope.master);					
					$scope.setRowAfterReset();
					$scope.AddBank.modifyFlag = "No";
					$scope.loadAddedBanks();
				    $scope.BankMasterForm.$setPristine(true);			    								
				
				///$scope.data.push({'id':$index,'status':'0'})
				
		   }
		};
	 })
	 
	 //==========================================
	 		//Sample Rules Master
	 //==========================================	 
	 .controller('SampleRulesMaster', function($scope,$http)
	  {
		 	 $index = 0;
		     $scope.data = [];
		     var gridData = [];
			 var gridDataSave = [];
			 var submittedOk = "";
			 $scope.master = {};
			//var gridLastValue = [];
			//$scope.data.push({'id':$index,'sizeFrom':0})
		  //------Validation---------
		  $scope.setValidation = function(id)
		  {
			  var prvId="";
			  if(id>1)
			  {
				  prvId = Number(id) - Number(1);
				  var currentVal =  $('#from'+ id).val();
				  var PrevVal =  $('#to'+prvId).val();
				  var SameVal =  $('#to'+id).val();
				  
				  if(currentVal<PrevVal || currentVal==SameVal) 
				  { 
				  	$('#error'+ id).show();
					$('#saveButton').attr("disabled", true);
				  } 
				 else 
				  { 
				  	$('#error'+ id).hide();
					$('#errorto'+ PrvId).hide();
					$('#saveButton').attr("disabled", false);
				  }
			  }			  
		  };
		  $scope.setoValidation = function(id)
		  {
			  	  
			  	  var nextId = Number(id) + Number(1);	
				  var currentVal =  $('#from'+ id).val();
				  var nextVal =  $('#from'+nextId).val();
				  var SameVal =  $('#to'+id).val();			  	
				  if(id<=$index)
				  {
					  if(SameVal==currentVal || SameVal>nextVal)
					  {
						//$('#errorto'+ id).removeClass('Errorto');
					  	$('#error'+ nextId).show();
						$('#saveButton').attr("disabled", true);					  
					  }
					 else
					  {
						//$('#errorto'+ id).addClass('Errorto');  
					  	$('#error'+ id).hide();
						$('#saveButton').attr("disabled", false);					  					  
					  }
				  }
		  };
		  
			
		  //---------Add Row---------
		  $scope.addSampleCard = function()
		  {			  
			  $index++;			  
			  gridData = [];
			 $scope.data.forEach(function (SampleRulesData) 
			  {
	             gridData.push(angular.toJson(SampleRulesData));														 
		      });
			 
			  //var asdf  = jQuery.unique(gridData);
			  //$scope.data.push({'id':$index})
			  var uniqGridData = [];
		      for (var i = 0; i < gridData.length; i++) 
			  {
		        if (uniqGridData.indexOf(gridData[i]) == -1) 
				{
		            uniqGridData.push(gridData[i]);
		        }
		    }
			  			  			  			  
			 $scope.data.push({'id':$index})
			 /*if($index!='1')
			 {
			 	
				$scope.data.push({'id':$index})
				if(uniqGridData.length==1)
				{
					var Modelname = new Object();
					for(var s=0;s<=uniqGridData.length;s++)
					{
						var nextVal = Number(s)+Number(2);
											
						var comArr = $.parseJSON(uniqGridData[s]);
						var textname = "hg"+nextVal;
						Modelname[textname] = comArr['sizeTo'];
						$scope.asd= Modelname;				  
					}				  				  
				}
				else
				{
					
						var comArr = $.parseJSON(uniqGridData[uniqGridData.length-1]);
						var textname = "hg"+$index+1;
						alert(textname);
						alert(JSON.stringify(comArr));
						Modelname[textname] = comArr['sizeTo'];
						$scope.asd= Modelname;				  					
					
				}
			}	
		   else
		    {
				$scope.data.push({'id':$index,'sizeFrom':0})
			}*/
			  gridData = [];
			 // $scope.submitted=true;
		  };
		  //--------Delete Row-------
		  $scope.removeRow = function(id)
		  {
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === id ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;	
				
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);	
				}				
		  };
		  
		  //-------Save Sample Rules----
		  
		  $scope.AddSamples = function(form)
		  {
			  var sampleRulesObj = new Object();
			 //$scope.submitted=true;
			 if($scope.AddSampleRulesForm.$valid)
			 {
				 gridDataSave = [];
				 $scope.data.forEach(function (SampleRulesData) 
				  {
	        	     gridDataSave.push(angular.toJson(SampleRulesData));														 
			      });
				 	
					sampleRulesObj.samplerules = gridDataSave;
					delete sampleRulesObj["id"];
	//alert(JSON.stringify(sampleRulesObj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveSampleCardRules.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(sampleRulesObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Sample Card Rules Added Successfully!', "success");	
								$('.btn-hide').attr('disabled',false);
								$scope.SampleRulesData = angular.copy($scope.master);
								$scope.LoadAddedSamples();
								form.$setPristine(true);
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});																
			 }
			else 
		     {
			 	var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
						firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}
			  }				 
		  };
		  //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.SampleRulesData = angular.copy($scope.master);
				$scope.LoadAddedSamples();
			    form.$setPristine(true);			    											   
		   };
		  
		  //-----------Load All Added sample Card Rules-------
		  $scope.LoadAddedSamples = function()
		  {			  
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllSampleCardRules.html",
					data: {}
				   }).success(function(data, status) 
					{
						if(data!='')
						{
							$scope.data=data;
							$index = data.length;
						}
					});											 			  			  
		  };		  		  
	  })
	 
	//===========================================
	 		//Permit Rules Master
	 //===========================================
	 .controller('PermitRulesMaster', function($scope,$http)
	 {
		  $scope.master = {};
		  //$scope.AddPermit={'status':0};
		  $scope.loadPermitStatus = function()
		  {
			  //alert(1);
			   //$scope.AddPermit.status=0;
			   $scope.AddPermit={'status':'0'};
		  };
		  
		  
		  
		  $scope.changeStatus = function(permitType)
		  {
			  if(permitType=="0")
			  {
				  $("#peracre").show();
				  $("#permitsperacre").hide();
				  $("#tonspermit").show();
			  }
			  else
			  {
				  $("#permitsperacre").show();
				   $("#peracre").hide();
				   $("#tonspermit").hide();
			  }
		  };
		  
		  
		 //---------Save Permits------------------
		 $scope.AddPermitRules = function(AddPermit,form)
		 {
			 
			 $scope.changeStatus = function(status)
			{
				
				if(status=='1') { $scope.validationRequired = true; } else { $scope.validationRequired = false; }
			};


 // $scope.AddPermit ={'permitBased':0};
			// $scope.submitted=true;

			 if($scope.PermitRulesForm.$valid)
			 {
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/savePermitRules.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddPermit),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'PermitRules Added Successfully!', "success");	
								$('.btn-hide').attr('disabled',false);
								
								$scope.AddPermit = angular.copy($scope.master);
								$scope.loadAddedPermits();
								form.$setPristine(true);
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});																
			 }
			else
			 {
				 var field = null, firstError = null;
				 for (field in form) 
				 {
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}			 			 
		   }
		 };
		 //-----------Load All Added Permits---------		 
		 $scope.loadAddedPermits = function()
		 {
			 //$scope.AddPermit = [];
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllPermitRules.html",
					data: {}
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
							$scope.AddPermit = data;
							//$scope.AddPermit={'status':'0'};	
							// $scope.AddPermit ={'permitType':0};
					});											 			  			  
		 };
		 
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddPermit = angular.copy($scope.master);
				$scope.loadAddedPermits();
			    form.$setPristine(true);			    											   
		   };
		 
	 })

	 //======================================
			//Designation Master
	//=====================================
	
	.controller('DesignationMaster',function($scope,$http)
	{
		
		var maxid = new Object();
		maxid.tablename = "Designation";
		var jsonTable= JSON.stringify(maxid);		
		
		$scope.isEdit = true;
		$scope.master = {};
		
		//--------Duplicate Entry------
		$scope.validateDup = function()
		  {			 
				for(var d =0;d<JSON.stringify($scope.AddedDesignations.length);d++)
				{
					var firstString = $scope.AddDesignation.designation.toUpperCase();
					var secondString = $scope.AddedDesignations[d].designation.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  $scope.validateDuplicate = function(desigName,id)
		  {
			 for(var i =0;i<JSON.stringify($scope.AddedDesignations.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				var str2 = $scope.AddedDesignations[i].designation.toUpperCase();
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					// alert("Enter Name already Exits");
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };		
		
		$scope.loadDesignationCode= function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
	   			}).success(function(data, status) 
				{			
					$scope.AddDesignation={'designationCode':data.id,'status':'0','modifyFlag':"NO",'screenName':'Designation Master'};																	
       			});	
		};	
		
		//------------Space Between Two Words -----------
  		$scope.spacebtw = function(b)
		{
				var twoSpace =$scope.AddDesignation[b];
				twoSpace =twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddDesignation[b] =twoSpace;	
		};

		//-----------Space Between Two Words (Grid)---------------
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AddedDesignations[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedDesignations[id][b] = twoSpace;
		};
			
	    //--------load all Designations ----------
	 	$scope.loadAllDesginations= function()
		 {
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllDesignations.html",
					data: {},
			 	}).success(function(data, status) 
				{
					$scope.AddedDesignations=data;
				});															  
		 };
		 //----------------end--------------------
	
		$scope.AddDesignations = function(AddDesignation,form)
		{
			//$scope.Addsubmitted = true;
			if($scope.DesignationMasterForm.$valid)
			{
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				    	url:"/SugarERP/saveDesignation.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddDesignation),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
						
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Designation Added Successfully!', "success");				
								$('.btn-hide').attr('disabled',false);	
								$scope.AddDesignation = angular.copy($scope.master);
								$scope.loadDesignationCode();
							    form.$setPristine(true);			    								
							  //--------load all Designations ----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllDesignations.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedDesignations=data;
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			}
			else
			{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
			}
		};
	
		//-------------------Update Designations--------------------
		$scope.UpdateAddedDesignations = function(UpdateDesignations,id)
		{
					$scope.isEdit = true;
					delete UpdateDesignations["$edit"];
			  		var UpdatedData = angular.toJson(UpdateDesignations);
					$scope.submitted=true;
					if($scope.EditDesignationMasterFrom.$valid)
					{
						$scope.isEdit = false;
				  		$.ajax({
				    		url:"/SugarERP/saveDesignation.html",
				    		processData:true,
				    		type:'POST',
				    		contentType:'Application/json',
				    		data:UpdatedData,
							beforeSend: function(xhr) 
							{
			            		xhr.setRequestHeader("Accept", "application/json");
			            		xhr.setRequestHeader("Content-Type", "application/json");
			        		},
				    		success:function(response) 
							{			  
								if(response==true)
								{
			  						swal("Success", 'Updated Successfully!', "success");
									
		 							//--------load all Advance SubCategory ----------
									var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllDesignations.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedDesignations=data;
													});															  
								  //----------------end---------------------									
								}			
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");									
								}
							}
						});	
					}
		};
		
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
					$scope.AddDesignation = angular.copy($scope.master);
					$scope.loadDesignationCode();
				    form.$setPristine(true);			    								
		   };
		
		
		
	 })

	//============================================
		//Family Group Master
	//============================================
	.controller('FamilyGroupMaster',function($scope,$http)
	{
	
			var obj = new Object();
			obj.tablename = "FamilyGroup";
			var jsonTable= JSON.stringify(obj);

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Zone";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);

			var fieldDropDown = new Object();
			fieldDropDown.dropdownname = "Circle";
			var StringDropDown= JSON.stringify(fieldDropDown);
			
			$scope.isEdit = true;
			$scope.master = {};
			
			//--------Duplicate Entry-------------
			$scope.validateDup = function()
			  {
				 // alert($scope.advance);
					for(var d =0;d<JSON.stringify($scope.FamilyGroupData.length);d++)
					{
						var firstString = $scope.AddedFamilyGroup.familyGroupName.toUpperCase();
						var secondString = $scope.FamilyGroupData[d].familyGroupName.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			   $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.FamilyGroupData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.FamilyGroupData[i].familyGroupName.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };			
			
			
			//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddedFamilyGroup[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedFamilyGroup[b] = twoSpace;				
			};
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.FamilyGroupData[id][b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.FamilyGroupData[id][b] = twoSpace;
				
			};			

			//============Get MAx id on Page onload=========
			$scope.loadFamilyGroupCode = function()
			{	
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   		}).success(function(data, status) 
					{
						$scope.AddedFamilyGroup={'familyGroupCode':data.id,'status':'0','modifyFlag':"No"};					
		        	});	   	   
		    };
			
			//===========get Zone Names==========
			$scope.loadZoneNames = function()
			{
				$scope.zoneNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						$scope.zoneNames=data;
					});  	   
		  	};		  
		 	//===========get Circle Names==========
			  $scope.loadCircleNames = function()
			  {
				$scope.circleNames=[];
		 		var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						$scope.circleNames=data;
			  	   });  	   
			  };
			
			 //===========get All Family Groups==========
			 $scope.loadAddedFamilyGroups = function()
			 {
				$scope.FamilyGroupData=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllFamilyGroups.html",
					data: {}
				   }).success(function(data, status) 
					{
						$scope.FamilyGroupData=data;
					});
			 };
			//===========Add Family Groups==========
				  
			$scope.AddFamilyGroupSubmit = function(AddedFamilyGroup,form)
			{
				//$scope.Addsubmitted = true;
				if($scope.FamilyGroupMasterForm.$valid) 
				{	
				//alert(JSON.stringify(AddedFamilyGroup));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveFamilyGroup.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedFamilyGroup),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Family Group Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									
									$scope.AddedFamilyGroup = angular.copy($scope.master);
									$scope.loadFamilyGroupCode();
									form.$setPristine(true);
									
									//---------get all FamilyGroups--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllFamilyGroups.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.FamilyGroupData=data;
										});  	   
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddedFamilyGroup={'familyGroupCode':data.id,'status':'0'};				
										        });	   	*/   								
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}			
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedFamilyGroup = angular.copy($scope.master);
				$scope.loadFamilyGroupCode();
			    form.$setPristine(true);			    											   
		   };
	
			//--------------Update Familygroup-----------				
			$scope.UpdateFamilyGroup = function(FamilyGroup,id)
			{
				delete FamilyGroup["$edit"];
			var UpdatedData = angular.toJson(FamilyGroup);
				//alert(UpdatedData);
				$scope.isEdit = true;
				$scope.submitted = true;
				//alert(data.familyGroupMembers));
				//alert($scope.EditFamilyGroupMasterFrom.$valid);
				if($scope.EditFamilyGroupMasterFrom.$valid)
				{
						$scope.isEdit = false;
			  		$.ajax({
			   			url:"/SugarERP/saveFamilyGroup.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:UpdatedData,
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Family Group Updated Successfully!', "success");
								//---------get all FamilyGroups--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllFamilyGroups.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.FamilyGroupData=data;
							 			});  	   
							//---------End--------
						 		}
							   else
								{
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
					 		}
						});			
				}			
			};
	})

	//=================================================
	  //Advance Category
    //=================================================
	 .controller('AdvanceCategory', function($scope,$http)
	{
		
		var maxid = new Object();
		maxid.tablename = "AdvanceCategory";
		var jsonTable= JSON.stringify(maxid);		
		
		var CategoryNames = new Object();
		CategoryNames.dropdownname = "CompanyAdvance";
		var CategoryNamesString= JSON.stringify(CategoryNames);		
		$scope.master = {};
		
		//----Duplicate String----
		$scope.validateDup = function()
		{
					for(var d =0;d<JSON.stringify($scope.AdvanceCategoryData.length);d++)
					{
						var firstString = $scope.AddedAdvanceCategory.advanceCategoryName.toUpperCase();
						var secondString = $scope.AdvanceCategoryData[d].advanceCategoryName.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
		 };		
		$scope.validateDuplicate = function(desigName,id)
		{
			for(var i =0;i<JSON.stringify($scope.AdvanceCategoryData.length);i++)
			{
				var str1 = desigName.toUpperCase();
				var str2 = $scope.AdvanceCategoryData[i].advanceCategoryName.toUpperCase();
				if(id!=i)
				{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
						$(".duplicate"+ id).show();
						return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
				}
			 }
		 };		 
		//=============Space Between Two Words ==================
	  	$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedAdvanceCategory[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedAdvanceCategory[b] = twoSpace;				
		};
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AdvanceCategoryData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AdvanceCategoryData[id][b] = twoSpace;			
		};		
		
		//--------------LoadCategoryCode--------------------
		$scope.loadAdvanceCategoryCode = function()
		{			
				var httpRequest = $http
				({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
	   			}).success(function(data, status) 
				{			
					$scope.AddedAdvanceCategory={'advanceCategoryCode':data.id,'status':'0','modifyFlag':"No",'screenName':'Advance Category Master'};						
       			});	
			};

		//--------------LoadCategoryCode--------------------
		$scope.loadCompanyAdvances = function()
		{			
				var httpRequest = $http
				({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: CategoryNamesString,
	   			}).success(function(data, status) 
				{			
					//alert(JSON.stringify(data));
					$scope.GetAdvancesData=data;						
       			});	
			};


			$scope.loadAllDetails = function()
			{				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllAdvanceCategories.html",
				data: {},
			   }).success(function(data, status) 
				{
						//alert(JSON.stringify(data));
					$scope.AdvanceCategoryData=data;
				});		
			};
		//--------------------LoadCategoryCodeEnd---------------
			$scope.advanceCategorysubmit = function(AddedAdvanceCategory,form)
			{
				//$scope.Addsubmitted = true;
				
				if($scope.AdvanceCategoryForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveAdvanceCategory.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddedAdvanceCategory),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
						
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'AdvanceCategory Added Successfully!', "success");				
								$('.btn-hide').attr('disabled',false);
								$scope.AddedAdvanceCategory = angular.copy($scope.master);
								$scope.loadAdvanceCategoryCode();
								form.$setPristine(true);
								
							    //-------get Max AdvanceCategory id-----------------
								/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddedAdvanceCategory={'advanceCategoryCode':data.id,'status':'0'};								
													});	*/							
						 	  //----------end---------------------------
							  //--------load all Advance Category ----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllAdvanceCategories.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AdvanceCategoryData=data;
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
				  }
				 else
				  {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					  
				  }
				  
			   };
				
				//--------------------Update Advance Category---------
				$scope.advanceCategoryUpdate = function(AdvCategory,id)
				{
					
					delete AdvCategory["$edit"];
			  		var UpdatedData = angular.toJson(AdvCategory);
					
			  		$.ajax({
				    		url:"/SugarERP/saveAdvanceCategory.html",
				    		processData:true,
				    		type:'POST',
				    		contentType:'Application/json',
				    		data:UpdatedData,
							beforeSend: function(xhr) 
							{
			            		xhr.setRequestHeader("Accept", "application/json");
			            		xhr.setRequestHeader("Content-Type", "application/json");
			        		},
				    		success:function(response) 
							{			  
								
								if(response==true)
								{
			  						swal("Success", 'Updated Successfully!', "success");
								}
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
							}

						});	
				};
				
             //---------Reset Form----
			 $scope.reset = function(form)
		     {			  
				$scope.AddedAdvanceCategory = angular.copy($scope.master);
				$scope.loadAdvanceCategoryCode();
			    form.$setPristine(true);			    											   
		    };
	})
	 //=======================================
	 		//Advance Sub Category
	 //=======================================
	.controller('AdvanceSubCategory', function($scope,$http)
	{


		var maxid = new Object();
		maxid.tablename = "AdvanceSubCategory";
		var jsonTable= JSON.stringify(maxid);	
		
		var AdvancesDrop = new Object();
		AdvancesDrop.dropdownname = "AdvanceCategory";
		var StringDropDown= JSON.stringify(AdvancesDrop);

		var CompanyAdvancesDrop = new Object();
		CompanyAdvancesDrop.dropdownname = "CompanyAdvance";
		var StringCompanyAdvanceDrop= JSON.stringify(CompanyAdvancesDrop);


		$scope.isEdit = true;
		$scope.master = {};
		
		//----Duplicate String----
		$scope.validateDup = function()
			  {
					for(var d =0;d<JSON.stringify($scope.AdvanceSubCategoryData.length);d++)
					{
						var firstString = $scope.AddedAdvanceSubCategory.name.toUpperCase();
						var secondString = $scope.AdvanceSubCategoryData[d].name.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AdvanceSubCategoryData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AdvanceSubCategoryData[i].name.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
								$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedAdvanceSubCategory[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedAdvanceSubCategory[b] = twoSpace;				
		};
		
		$scope.getcategoryCode = function(categorycode)
		{

			$scope.AddedAdvanceSubCategory.subCategoryCode=$scope.AddedAdvanceSubCategory.subCategoryCode;
	
		};		
		
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AdvanceSubCategoryData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AdvanceSubCategoryData[id][b] = twoSpace;			
		};


		$scope.loadAdvanceSubCategoryCode = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxId.html",
			data: jsonTable,

	   			}).success(function(data, status) 
				{
			
					$scope.AddedAdvanceSubCategory={'subCategoryCode':data.id,'status':'0','modifyFlag':"No"};				
       			});	
		};
		$scope.loadCategory = function()
		{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.Category=data;
				});  	   
		};
		$scope.loadCompanyAdvances = function()
		{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringCompanyAdvanceDrop,
			   }).success(function(data, status) 
				{
					//alert(data);
					$scope.CompanyAdvanceNames=data;
				});  	   
		};
		
		$scope.loadAllAddedSubCategories = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllAdvanceSubCategories.html",
			data: {},
		   }).success(function(data, status) 
			{
				$scope.AdvanceSubCategoryData=data;
			});		
		};

		$scope.advanceSubCategorysubmit = function(AddedAdvanceSubCategory,form)
		{
			//$scope.Addsubmitted = true;
			if($scope.AdvanceSubCategoryForm.$valid) 
			{
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				    	url:"/SugarERP/saveAdvanceSubCategory.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddedAdvanceSubCategory),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },						
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'AdvanceSubCategory Added Successfully!', "success");				
								 $('.btn-hide').attr('disabled',false);
								 
								 $scope.AddedAdvanceSubCategory = angular.copy($scope.master);
								 $scope.loadAdvanceSubCategoryCode();
								 form.$setPristine(true);
								 
							    //-------get Max AdvanceSubCategory id-----------------
								/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
												   }).success(function(data, status) 
													{
														$scope.AddedAdvanceSubCategory={'subCategoryCode':data.id,'status':'0'};								
													});		*/						
						 	  //----------end---------------------------
							  //--------load all Advance SubCategory ----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllAdvanceSubCategories.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AdvanceSubCategoryData=data;
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								   
							}
						}
					});					
			}	
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				
			}
		};

		//--------Update Advance Subcategory-----
		$scope.advanceSubCategoryUpdate = function(AdvSubCategory,id)
		{
					$scope.isEdit = true;
					delete AdvSubCategory["$edit"];
			  		var UpdatedData = angular.toJson(AdvSubCategory);
					
					$scope.submitted=true;
					if($scope.AdvanceSubCategoryEdit.$valid)
					{
					$scope.isEdit = false;

			  		$.ajax({
				    		url:"/SugarERP/saveAdvanceSubCategory.html",
				    		processData:true,
				    		type:'POST',
				    		contentType:'Application/json',
				    		data:UpdatedData,
							beforeSend: function(xhr) 
							{
			            		xhr.setRequestHeader("Accept", "application/json");
			            		xhr.setRequestHeader("Content-Type", "application/json");
			        		},
				    		success:function(response) 
							{	
								
								if(response==true)
								{
			  						swal("Success", 'Updated Successfully!', "success");
									
 							//--------load all Advance SubCategory ----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getAllAdvanceSubCategories.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AdvanceSubCategoryData=data;
													});															  
							  //----------------end---------------------									
								}
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
							}
						});	
					}
			};				
			 //---------Reset Form----
		    $scope.reset = function(form)
		    {			  
				$scope.AddedAdvanceSubCategory = angular.copy($scope.master);
				$scope.loadAdvanceSubCategoryCode();
			    form.$setPristine(true);			    											   
		    };
			
		})

	//============================================
			//Ryot Master
	//============================================
	.controller('RyotMaster', function($scope,$http)
	 {
		
		$scope.data = [];
		$scope.AddRyot = [];
		$index = 0;
		$scope.master = {};
		var gridData = [];
		$scope.Salutation = [];
		$scope.Relation = [];
		$scope.BranchData = [];
		var AddRyotObj = new Object();
		//$scope.aadhaarImagePath = '9';
		
		
		var DropDownLoadSeason = new Object();
		DropDownLoadSeason.dropdownname = "Season";
		var jsonStringSeasonDropDown= JSON.stringify(DropDownLoadSeason);		
		
		var DropDownLoad = new Object();
		DropDownLoad.targetdropdownname = "Village";
		DropDownLoad.columnname = "mandalcode";
		//DropDownLoad.value = "mandalcode";
		//var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		var BranchDropDown = new Object();
		BranchDropDown.dropdownname = "Branch";
		var jsonBranchDropDown= JSON.stringify(BranchDropDown);

		var VillageDropDown = new Object();
		VillageDropDown.dropdownname = "Mandal";
		var jsonStringDropDown= JSON.stringify(VillageDropDown);
		
		var VillageDropDownUpdate = new Object();
		VillageDropDownUpdate.dropdownname = "Village";
		var jsonStringVillage= JSON.stringify(VillageDropDownUpdate);

		var CircleDropDownUpdate = new Object();
		CircleDropDownUpdate.dropdownname = "Circle";
		var jsonStringCircle= JSON.stringify(CircleDropDownUpdate);


		var CircleDropDown = new Object();
		CircleDropDown.targetdropdownname = "Circle";
		CircleDropDown.columnname = "villagecode";


		var SetIFSCCode = new Object();
		SetIFSCCode.tablename = "Branch";
		SetIFSCCode.columnname = "ifsccode";
		SetIFSCCode.wherecolumnname = "branchcode";


		var maxid = new Object();
		maxid.tablename = "Ryot";
		var jsonTable= JSON.stringify(maxid);	

		var AddedRyotDropDown = new Object();
		AddedRyotDropDown.tablename = "Ryot";
		AddedRyotDropDown.columnName = "ryotcode";
		AddedRyotDropDown.columnName1 = "ryotname";
		var jsonAddedRyotDropDown= JSON.stringify(AddedRyotDropDown);	

		$scope.vmDropDownDisabled = false;
		//$scope.vmDropDownDisabledv = false;

		//-------Load Season Dropdown------------
		$scope.loadseasonNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
			  }).success(function(data, status) 
			  {
				$scope.seasonNames=data;
			  });
		};		
			
		//=============Space Between Two Words ==================
	  	$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddRyot[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddRyot[b] = twoSpace;				
		};

		
		//----------Set Village Code on Village On Change---
		
		$scope.setRyotCodeonvillageChange = function(villageCode)
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/GetMaxRyotCodeByVillage.html",
				data: villageCode,
			  }).success(function(data, status) 
			  {
				 $scope.AddRyot.ryotCode = data[0].ryotCode;
			  });			
			
		};


		//-----------Get MAX-ID--------------
		$scope.loadRyotCode = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxId.html",
				data: jsonTable,
	   		  }).success(function(data, status) 
			  {			
			  	var ryotDefaultCode = 0;
				if(data.id<10) { ryotDefaultCode = "000"+data.id; }
				else if(data.id>=10 && data.id<100) { ryotDefaultCode = "00"+data.id; }
				else if(data.id>=10 && (data.id>=100 && data.id<1000)) { ryotDefaultCode = "0"+data.id; } else { ryotDefaultCode = data.id; }
			 //alert(ryotDefaultCode);
			     var ryotDefaultCodeet = "0000"+ryotDefaultCode;
				 $scope.AddRyot = {'ryotCode':ryotDefaultCodeet,'status':'0','aadhaarImagePath':'0','ryotPhotoPath':'0','circleCode':'0','screenName':'Ryot Master','ryotType':'0'};	
				 $scope.TempRyotCode = data.id;
				 $scope.AddRyot.ryotCodeSequence = data.id;
       		  });	
		};		
		
		//----------set New RyotCode on village and manal onchage-------
		
		$scope.setNewRyotCode = function(value)
		{
			var mandalCode = 0000;
			var villageCode = 0000;
			var ryotCode = 00;
			if(value=="") { value=0;}			
			//if($scope.AddRyot.mandalCode<10) { mandalCode = "0"+$scope.AddRyot.mandalCode; } else { mandalCode = $scope.AddRyot.mandalCode; }
			if(value!=undefined)
			{
				if(value<10) { villageCode = "0"+value; } else { villageCode = value }
			}
		   else
		    {
				villageCode = "00";
			}
			if(mandalCode==undefined) { mandalCode = "00"; }
			
			if($scope.TempRyotCode<10) { ryotCode = "000"+$scope.TempRyotCode; }
			else if($scope.TempRyotCode>=10 && $scope.TempRyotCode<100) { ryotCode = "00"+$scope.TempRyotCode; }
			else if($scope.TempRyotCode>=10 && ($scope.TempRyotCode>=100 && $scope.TempRyotCode<1000)) { ryotCode = "0"+$scope.TempRyotCode; } else { ryotCode = $scope.TempRyotCode; } 
						
			//var newRyotCode = mandalCode+''+villageCode+''+ryotCode;			
			var newRyotCode = villageCode+''+ryotCode;			
			//$scope.AddRyot.ryotCode = newRyotCode;
			//alert(villageCode);
		};
		//--------Load Added Ryot Dropdown------
		/*
		$scope.loadAddedRyotDropdowns = function()
		{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadAddedDropDownsForMasters.html",
				data: jsonAddedRyotDropDown,
			   }).success(function(data, status) 
				{
					$scope.AddedRyotsData=data;
				});  	   
		};		
		*/
	$scope.loadAddedRyotDropdowns = function(AddedRyot)
		{
			//alert(AddedRyot);
			//filterRyot.ryotcode = $scope.AddedRyot;
			//alert(JSON.stringify(filterRyot));
			//alert(AddedRyot);
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllRyots.html",
				data: AddedRyot,
				}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.AddRyot=data.ryot;
					$scope.data=data.branch;
				});   
		};		
		//-----------Add Row for Branch------
		$scope.addFormField = function() 
		{
			$index++;
		    $scope.data.push({'id':$index,'accountType':'Select Accoount Type'})
		};		 		 		
		//------------Delete Row from table---
		$scope.removeRow = function(id)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === id ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
  	    };				
		//------Load  Salutation & Relation DropDowns------
		$scope.Salutation = [{ Id: 'SRI', Name: 'SRI' },{ Id: 'SMT', Name: 'SMT' },{ Id: 'KUM ', Name: 'KUM ' }];
		$scope.relation = [{ relationId: 'FATHER', Name: 'FATHER' }, { relationId: 'GUARDIAN ', Name: 'GUARDIAN ' } , { relationId: 'SPOUSE', Name: 'SPOUSE' }];
		$scope.RelationName = 'Relative';
		$scope.suretyRyotData = [{ Id: 'Suresh', Name: 'Suresh' },{ Id: 'Ramesh', Name: 'Ramesh' }];

		
		$scope.AccountTypeData = [{ Id: '0', Name: 'Savings A/C' },{ Id: '1', Name: 'Loan A/C' }];
		
		//------------Relation Name Dropdown Onchange-----
		
		$scope.changeRelationName = function()
		{
			$scope.RelationName = $scope.AddRyot.relation;	
		};
		//--------Load Mandal DropDown-------------------
		$scope.loadMandalDropDown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.MandalData=data;							
					});								
		};

		//--------Load Village DropDown-------------------
		$scope.setVillageDropDown = function(mandalCode)
		{
			//alert(mandalCode);
			DropDownLoad.value = mandalCode;
			var jsonVillageDropDown= JSON.stringify(DropDownLoad);
						
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValuesForVillage.html",
					data: jsonVillageDropDown,
				   }).success(function(data, status) 
					{
						//alert('Mandal------'+JSON.stringify(data));
						$scope.VillageData=data;							
					});								
		};
		//--------Load Circle DropDown-------------------
		$scope.setCircleDropDown = function(villageCode)
		{
			CircleDropDown.value = villageCode;
			var jsonCircleDropDown= JSON.stringify(CircleDropDown);
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValues.html",
					data: jsonCircleDropDown,
				   }).success(function(data, status) 
					{
						$scope.CircleData=data;							
					});								
		};

		//----------Load Bank Branch Dropdown---
		$scope.loadBankBranchesDropDown = function()
		{
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonBranchDropDown,
				   }).success(function(data, status) 
					{
						//console.log(data);
						$scope.BranchDropData=data;							
					});												
		};
		
		//-------------Load Village in Update---
		$scope.loadVillageDropDownUpdate = function()
		{
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNamesForVillage.html",
					data: jsonStringVillage,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.VillageData=data;							
					});												
		};		
		//-------------Load Circle in Update---
		$scope.loadCircleDropDownUpdate = function()
		{
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringCircle,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.CircleData=data;							
					});												
		};		
		
		
		//---------Add Ryots-----------
		$scope.AddedRyots = function(AddRyot,form)
		{			 
			// $scope.submitted=true;			  
						//alert(1);			  
			  if($scope.AddRyot.aadhaarImagePath=='0') { $('#Aadhar').show(); } else { $('#Aadhar').hide(); } 
			  if($scope.AddRyot.ryotPhotoPath=='0') { $('#image').show(); } else { $('#image').hide(); } 
			  
			  if($scope.AddRyot.aadhaarImagePath!='0' && $scope.AddRyot.ryotPhotoPath!='0')
			  {			  
			  //alert($scope.RyotMasterForm.$valid);
			   if($scope.RyotMasterForm.$valid)
			   {
				    gridData = [];
			 		$scope.data.forEach(function (BranchData) 
				    {
			             gridData.push(angular.toJson(BranchData));														 
				    });		
			 
					  var SuretyCode = $scope.AddRyot['suretyRyotCode'];
					  if(SuretyCode!=undefined)
					  {				 
						  var SuretyString = "";
						  for(var s=0;s<SuretyCode.length;s++)
						  {
							  SuretyString +=SuretyCode[s]+",";
						  }	
						  SuretyString = SuretyString.substring(0,SuretyString.length-1);
					  }
					 else
					  {
						  SuretyString = "";
					  }			  

					  delete AddRyot['suretyRyotCode'];				  
					  AddRyotObj.gridData = gridData;			  
					  AddRyot.suretyRyotCode = SuretyString;			  
				  	  AddRyotObj.formData = JSON.stringify(AddRyot);	
					  $scope.modifyFlag="No";
					  AddRyotObj.modifyFlag=$scope.modifyFlag;
					
					
				 // alert(JSON.stringify(AddRyotObj));
				  $('.btn-hide').attr('disabled',true);
		 		  $.ajax({
				    	url:"/SugarERP/saveRyotMaster.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddRyotObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },						
					    success:function(response) 
						{			
						    //alert(response);
							if(response==true)
							{		
								swal("Success", 'Ryot Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								//alert(1);
								$scope.AddRyot = angular.copy($scope.master);
								//alert(2);
								$('#Aadhar').hide();
								//alert(3);
								$('#image').hide();				
								//alert(4);
								$scope.loadRyotCode();
								//alert(5);
						    	$scope.RyotMasterForm.$setPristine(true);			
								//alert(6);
								$scope.data = {};
								//alert(7);
								$scope.AddedRyot = {};
								//alert(8);
								$scope.AddRyot.aadhaarImagePath="0";
								//alert(9);
								$scope.AddRyot.ryotPhotoPath="0";					
								$scope.loadAddedRyotDropdowns();
								//alert(10);

								$index=1;
								$scope.data = [{'id':$index,'accountType':'Select Accoount Type'}]
								
								var alertok = document.querySelector(".sweet-alert"),
								okButton = alertok.getElementsByTagName("button")[1];
								$(okButton).click(function() 
								{  								
									location.reload();
								});								
								//alert(11);
							}			
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
				
			  }	
			 else 
		     {
			 	var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
						firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}
			  }				 
		   }
		 else if($scope.AddRyot.aadhaarImagePath=='0' && $scope.AddRyot.ryotPhotoPath=='0')
		  {
			var field = null, firstError = null;
			for (field in form) 
			{
				if (field[0] != '$') 
				{
					if (firstError === null && !form[field].$valid) 
					{
						firstError = form[field].$name;
					}
					if (form[field].$pristine) 
					{
						form[field].$dirty = true;
					}
				}	
			}
		  }		   
		};	
		//--------Set ifsc Code on branch onchange-------
		$scope.setifscCode = function(id,value)
		{	
			
			SetIFSCCode.value = value;
			var jsonIFSCCode= JSON.stringify(SetIFSCCode);
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeFieldValues.html",
					data: jsonIFSCCode,
				   }).success(function(data, status) 
					{
						$scope.data[id-1].ifscCode = data.ifsccode;
						//$scope.VillageData=data;							
					});								
		};
		
		//--------------Upload Images-----------

		$scope.uploadAadharFile = function(files) 
		{
			
		    var fd = new FormData();
			var Path = "aadhar_"+ $scope.AddRyot.ryotCode;
		    fd.append("file", files[0]);
			fd.append("path", Path);
			
		    $http.post("/SugarERP/saveImage.html", fd, {
		        withCredentials: true,
		        headers: {'Content-Type': undefined },
        		transformRequest: angular.identity
			  }).success(function(data, status) 
		 	   { 
			   		$('#Aadhar').hide();
					//alert(JSON.stringify(data));
					$scope.AddRyot.aadhaarImagePath = data.toLowerCase();
	           });	   	   
		};		
		
		$scope.uploadRyotImage = function(files) 
		{
		    var fd = new FormData();
			var Path = "ryotimage_"+ $scope.AddRyot.ryotCode;
		    fd.append("file", files[0]);
			fd.append("path", Path);
			
		    $http.post("/SugarERP/saveImage.html", fd, {
		        withCredentials: true,
		        headers: {'Content-Type': undefined },
        		transformRequest: angular.identity
			  }).success(function(data, status) 
		 	   { 
			   		$('#image').hide();	
					$scope.AddRyot.ryotPhotoPath = data.toLowerCase();
	           });	   	   
		};		
		
		//----------Update Ryot Master----------------
		$scope.UpdateRyotData = function(value)
		{			
			if(value!=null)
			{
				$scope.vmDropDownDisabled = true;
				$scope.loadVillageDropDownUpdate();
				$scope.loadCircleDropDownUpdate();	
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllRyots.html",
					data: JSON.stringify(value),
				   }).success(function(data, status) 
					{		
						//alert(JSON.stringify(data.ryot));
						$scope.vmDropDownDisabled = "true";
						//$scope.setVillageDropDown(data.ryot.mandalCode);
						$scope.AddRyot = data.ryot;	
						
						$scope.data = data.branch;
						$scope.AddRyot.screenName ="Ryot Master";
						
						for(var i=0;i<data.branch.length;i++)
						{
							//alert($scope.data[i].ryotBankBranchCode);
							if($scope.data[i].ryotBankBranchCode==0) { $scope.data[i].ryotBankBranchCode = null; }
						}
						
					});	
					
				
			}
		   else
		    {
					$scope.AddRyot = angular.copy($scope.master);
					$('#Aadhar').hide();
					$('#image').hide();				
					$scope.loadRyotCode();
			    	$scope.RyotMasterForm.$setPristine(true);			
					$scope.data = {};
					$scope.AddRyot.aadhaarImagePath="0";
					$scope.AddRyot.ryotPhotoPath="0";					

					$index=1;
					$scope.data = [{'id':$index,'accountType':'Select Accoount Type'}]								
			}
		};
		//---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {	
		   		
		   		if(modifyId!=null)
				{
					$scope.AddRyot = angular.copy($scope.master);
					$('#Aadhar').hide();
					$('#image').hide();				
					$scope.loadRyotCode();
			    	form.$setPristine(true);		
					$scope.vmDropDownDisabled = false;
					$scope.data = {};
					$scope.AddedRyot = "";
					$scope.AddRyot.aadhaarImagePath="0";
					$scope.AddRyot.ryotPhotoPath="0";
					$scope.loadAddedRyotDropdowns();
					$index=1;
					$scope.data = [{'id':$index,'accountType':'Select Accoount Type'}]
					$('.ryotDup').hide();
					$('.btn-hide').attr('disabled',false);

				}
			   else
			   {
					
					$scope.AddRyot = angular.copy($scope.master);
					$('#Aadhar').hide();
					$('#image').hide();				
					$scope.loadRyotCode();
			    	form.$setPristine(true);
					$scope.vmDropDownDisabled = false;
					$scope.data = {};
					$scope.AddedRyot = "";
					$scope.AddRyot.aadhaarImagePath="0";
					$scope.AddRyot.ryotPhotoPath="0";
					$scope.loadAddedRyotDropdowns();
					$index=1;
					$scope.data = [{'id':$index,'accountType':'Select Accoount Type'}]	
					$('.ryotDup').hide();
					$('.btn-hide').attr('disabled',false);
				}
		   };
		   
		   //--------Ryot Code Validation----
		   //10030491
		   /*$scope.ryotCodeValidation = function(ryotCode)
		   {
			  // alert(ryotCode);
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/ValidateRyotCode.html",
					data: ryotCode,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						if(data==true)
						{
							$('.ryotDup').show();
						 	$('.btn-hide').attr('disabled',true);	
						}
						else
						{
							$('.ryotDup').hide();
						 	$('.btn-hide').attr('disabled',false);								
						}
						//$scope.VillageData=data;							
					});					   
		   
		   */
		
	 })


	//=============================================
			//Season Master
	//=============================================
	.controller('SeasonMaster',function($scope,$http)
	{			
			$scope.master = {};
			
			var obj = new Object();
			obj.tablename = "Season";
			var jsonTable= JSON.stringify(obj);
			
		   $scope.fromYear = [{ fromYear: '2014', fromYear: '2014' }, { fromYear: '2015', fromYear: '2015' }, { fromYear: '2016', fromYear: '2016' },{ fromYear: '2017', fromYear: '2017' }, { fromYear: '2018', fromYear: '2018' }, { fromYear: '2019', fromYear: '2019' }, { fromYear: '2020', fromYear: '2020' }];
			$scope.isEdit = true;
			$scope.toYrList = [];
		//---------Validtae Duplicate Entrys----
		 $scope.validateDup = function()
		  {
			  //alert($scope.SeasonData.length);
				for(var i =0;i<JSON.stringify($scope.SeasonData.length);i++)
				{					
					var firstString = $scope.AddedSeasons.season;
					//alert(firstString);
					var secondString = $scope.SeasonData[i].season;
					//alert(secondString);
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							//alert(2);
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };	
		  
		 $scope.validateDuplicate = function(season,id)
			  {
				
				 for(var i =0;i<JSON.stringify($scope.SeasonData.length);i++)
				 {
					var str1 = season.toUpperCase();
					
					var str2 = $scope.SeasonData[i].season.toUpperCase();
					
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						
						$('.btn-hide').attr('disabled',true);								
					$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						$(".duplicate"+ id).hide();
						$('.btn-hide').attr('disabled',false);		
					 }
					}
				 }
			  };			
			
			//------Load Season ID--------
			$scope.loadSeasonId = function()
			{	
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   		}).success(function(data, status) 
					{
						$scope.AddedSeasons={'seasonId':data.id,'status':'0','modifyFlag':"No"};								
		        	});	   	   
		    };

			//------------Space Between Two Words ------------
  			$scope.spacebtw = function(b)
			{
					var twoSpace =$scope.AddedSeasons[b];
					twoSpace =twoSpace.replace(/ {2,}/g,' ');
					twoSpace=twoSpace.trim();
					$scope.AddedSeasons[b] =twoSpace;	
			};

			//----------Space Between Two Words (Grid)--------
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.SeasonData[id][b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.SeasonData[id][b] = twoSpace;
			};

			
			//-------Load Added Season Details---
			
			$scope.loadSeasonDetails = function()
			{
				//---------get all Seasons--------									
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllSeasons.html",
				data: {}
				}).success(function(data, status) 
				{
					$scope.SeasonData=data;
				});  	   
							//---------End--------
			};
			//-------Load From Year--------
			
			$scope.loadFromYear = function()
			{
				var fromYearNames = $scope.AddedSeasons.fromYear;
				var FromYear = Number(fromYearNames)+1;
				 $scope.toYrList=[{'toYear':FromYear}];			
			};
			//--------Load To Year-------
			$scope.loadToYear = function()
			{
				var toYearNames = $scope.AddedSeasons.toYear;
				var fromYearNames = $scope.AddedSeasons.fromYear;
				var FromYear = Number(fromYearNames)+1;
				var Season = fromYearNames+"-"+FromYear;
				$scope.AddedSeasons.season = Season;
			};
			
			//--------Save Season--------------------
			
			$scope.SeasonSubmit = function(AddedSeasons,form)
			{
				
				//$scope.Addsubmitted = true;
				if($scope.SeasonMasterForm.$valid) 
				{	
				   $('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveSeason.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedSeasons),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", "Season Added Successfully!", "success");
									$('.btn-hide').attr('disabled',false);
									
									$scope.AddedSeasons = angular.copy($scope.master);
									$scope.loadSeasonId();
									form.$setPristine(true);
									
									//---------get all Seasons--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllSeasons.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.SeasonData=data;
										});  	   
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddedSeasons={'seasonId':data.id,'status':'0'};				
										        });	 */  	   
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}				
			};
			
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedSeasons = angular.copy($scope.master);
				$scope.loadSeasonId();
			    form.$setPristine(true);
				$('.btn-hide').attr('disabled',false);
		   };

			//--------Load From Year DropDown--------------------
			$scope.loadFromYearEdit = function(Season,id)
			{				
				var frmyr = Season;
				var toYr = Number(Season)+1;
				var EditSeasonYear = frmyr+"-"+toYr;

				$scope.SeasonData[id].season = EditSeasonYear;
				$scope.SeasonData[id].toYear = toYr;
					$scope.validateDuplicate(EditSeasonYear,id);
			};
			
			//------------------------Update----------------				
			$scope.UpdateSeasons = function(Season,id)
			{
					
					delete Season["$edit"];
					$scope.isEdit = true;
					$scope.submitted = true;
					
					if($scope.EditSeasonMasterForm.$valid)
					{
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/saveSeason.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(Season),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", "Season Updated Successfully!", "success");
								//---------get all Seasons--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllSeasons.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.SeasonData=data;
											
							 			});  	   
							//---------End--------
						 		}
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
					 		}
						});			
				}
			//-----------end----------			
			};
		})

		//=============================================
				//Account Group Master
		//=============================================
		.controller('AccountGroupMaster',function($scope,$http)
		{
		
			var obj = new Object();
			obj.tablename = "AccountGroup";
			var jsonTable= JSON.stringify(obj);
			$scope.isEdit = true;
			$scope.master = {};
			
			//-----Duplicate String---
			$scope.validateDup = function()
			  {
				 // alert($scope.advance);
					for(var d =0;d<JSON.stringify($scope.AccountGroupData.length);d++)
					{
						var firstString = $scope.AddedAccountGroup.accountGroup.toUpperCase();
						var secondString = $scope.AccountGroupData[d].accountGroup.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			   $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AccountGroupData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AccountGroupData[i].accountGroup.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
								$(".btn-hideg").prop('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $(".btn-hideg").prop('disabled',false);	
					 }
					}
				 }
			  };			

		   //=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddedAccountGroup[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedAccountGroup[b] = twoSpace;				
			};
			
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.AccountGroupData[id][b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AccountGroupData[id][b] = twoSpace;
			};

			//============Get MAx id on Page onload=========
			$scope.loadAccountGroupCode = function()
			{	
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   		}).success(function(data, status) 
					{
						$scope.AddedAccountGroup={'accountGroupCode':data.id,'status':'0','modifyFlag':"No"};				
		        	});	   	   
		    };
			
			//---------get all AccountGroup--------
			$scope.loadAllAccountGroup = function()
			{	
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllAccountGroups.html",
				data: {}
				}).success(function(data, status) 
				{
				$scope.AccountGroupData=data;
											
				});
			  };	
			
			//---------Save Account Groups---------
			
			$scope.AddAcountGroupSubmit = function(AddedAccountGroup,form)
			{
				
				//$scope.Addsubmitted = true;			
				if($scope.AccountGroupMasterForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveAccountGroup.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedAccountGroup),							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Account Group Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedAccountGroup = angular.copy($scope.master);
									$scope.loadAccountGroupCode();
									form.$setPristine(true);
									
									//---------get all FamilyGroups--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllAccountGroups.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.AccountGroupData=data;
										});  	   
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddedAccountGroup={'accountGroupCode':data.id,'status':'0'};				
										        });	  */ 	   
								  //-----------end--------
									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}			
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};
			
			//----------Update Account Group-----------
			$scope.UpdateAccountGroup = function(Group,id)
				{					
					delete Group["$edit"];
					$scope.isEdit = true;
					$scope.submitted = true;
					
					if($scope.EditAccountGroupMasterFrom.$valid)
					{
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/saveAccountGroup.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(Group),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
						success:function(response) 
						{	
							if(response==true)
							{
								swal("Success!", "Account Group Updated Successfully!", "success");
								//---------get all Seasons--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllAccountGroups.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.AccountGroupData=data;											
							 			});  	   
							//---------End--------
						 		}
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
					 		}
						});			
				}
			//-----------end----------			
			};			
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedAccountGroup = angular.copy($scope.master);
				$scope.loadAccountGroupCode();
			    form.$setPristine(true);			    											   
		   };
			
		})

		//===================================================
				//Account Sub Group Master
		//===================================================
		.controller('AccountSubGroupMaster',function($scope,$http)
		{
			
			var obj = new Object();
			obj.tablename = "AccountSubGroup";
			var jsonTable= JSON.stringify(obj);
			
			$scope.isEdit = true;
			$scope.master = {};

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "AccountGroup";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			//---Duplicate String----
			$scope.validateDup = function()
			  {
					for(var d =0;d<JSON.stringify($scope.AccountSubGroupData.length);d++)
					{
						var firstString = $scope.AddedAccountSubGroup.accountSubGroup.toUpperCase();
						var secondString = $scope.AccountSubGroupData[d].accountSubGroup.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.AccountSubGroupData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.AccountSubGroupData[i].accountSubGroup.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };			
			
			//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddedAccountSubGroup[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedAccountSubGroup[b] = twoSpace;				
			};
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.AccountSubGroupData[id][b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AccountSubGroupData[id][b] = twoSpace;
			};			
			
			//============Get MAx id on Page onload=========
			$scope.loadAccountSubGroupCode = function()
			{								
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   		}).success(function(data, status) 
					{
						$scope.AddedAccountSubGroup={'accountSubGroupCode':data.id,'status':'0','modifyFlag':"No",'screenName':"Account Subgroup Master"};					
		        	});	   	   
		    };
			//------------Load Account Group Names----
			$scope.loadaccountGroupNames = function()
			{
				$scope.accountGroupNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						$scope.accountGroupNames=data;
					}); 
			};
			//---------get all AccountSubGroup--------
			$scope.loadAllAccountSubGroup = function()
			{	
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllAccountSubGroups.html",
				data: {}
				}).success(function(data, status) 
				{
				$scope.AccountSubGroupData=data;
											
				});
			  };	
			  
			//-----------Save Account Sub Groups--------
			
			$scope.AddAcountSubGroupSubmit = function(AddedAccountSubGroup,form)
			{
				//$scope.Addsubmitted = true;
				
				if($scope.AccountSubGroupMasterForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveAccountSubGroup.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedAccountSubGroup),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Account SubGroup Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedAccountSubGroup = angular.copy($scope.master);
									$scope.loadAccountSubGroupCode();
									form.$setPristine(true);			    								
									
									//---------get all FamilyGroups--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllAccountSubGroups.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.AccountSubGroupData=data;
										});  	   
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddedAccountSubGroup={'accountSubGroupCode':data.id,'status':'0'};				
										        });	 */  	   
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
				
			};
			
			//-----------Update Account Subgroup Master----------
			
			$scope.UpdateSubAccountGroup = function(Group,id)
			{					
					delete Group["$edit"];
					
					$scope.isEdit = true;
					
					$scope.submitted = true;
					
					if($scope.EditAccountSubGroupMasterFrom.$valid)
					{
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/saveAccountSubGroup.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(Group),

						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Account SubGroup Updated Successfully!', "success");
								//---------get all Seasons--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllAccountSubGroups.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.AccountSubGroupData=data;											
							 			});  	   
							//---------End--------
						 		}
							    else
								 {
									  sweetAlert("Oops...", "Something went wrong!", "error");
								 }
					 		}
						});			
				}
			//-----------end----------			
			};				
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedAccountSubGroup = angular.copy($scope.master);
				$scope.loadAccountSubGroupCode();
			    form.$setPristine(true);			    											   
		   };
			
		})


	  //==========================================
	 		//Seedling Tray Master
	 //==========================================
	 .controller('SeedlingTrayMaster', function($scope,$http)
	 {
		 
		var maxid = new Object();
		maxid.tablename = "SeedlingTray";
		var jsonTable= JSON.stringify(maxid);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "SeedlingTray";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		 $scope.data = [];

		$scope.isEdit = true;
		$scope.master = {};
		 $index=0;
		  var gridData = [];
		 var obj = new Object();
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddSeedlig[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddSeedlig[b] = twoSpace;				
		};
		//=================Space Between Two Words (Grid)================
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.AddedSeedlingTraysData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedSeedlingTraysData[id][b] = twoSpace;			
		};	
		
		
		$scope.decimalpointgrid=function(b,id)
			{
				//alert(90);
				var decmalPoint = $scope.data[id][b];			
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				$scope.data[id][b] = getdecimal;	
				 if(getdecimal=="NaN")
					{
						 $scope.data[id][b]="";
					}
			};
		
		
		
		
		
		
		
		//-----------Get MAX-ID--------------
		$scope.loadTrayCode = function()
		{
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxId.html",
				data: jsonTable,
	   		  }).success(function(data, status) 
			  {						  
				 $scope.AddSeedlig = {'trayCode':data.id,'status':'0','trayType':'0','screenName':'Seedling Tray Master','modifyFlag':'No'};															
       		  });	
		};	
		
		$scope.addFormField = function() 
		{
			
			$index++;
		    $scope.data.push({'id':$index})
		};
		
		//----------Load Added Seedling Trays---
		
		$scope.loadAddedSeedlingTrays = function()
		{
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllSeedlingTrays.html",
					data: JSON.stringify($scope.seedlingTray),
				   }).success(function(data, status) 
				   {
						$scope.data = data.charge;
						$scope.AddSeedlig = data.tray;
						$scope.AddSeedlig.screenName="Seedling Tray Master";
						$scope.AddSeedlig.modifyFlag="Yes";
				   });	
							
		};
		$scope.loadSeedlingTrayNames = function()
		{
			//alert(JSON.stringify(jsonStringDropDown));
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringDropDown,
			  }).success(function(data, status) 
			{
				$scope.seedlingTrayNames=data;
			}); 
		};
		 
		//----------Save Seedling Trays-----
		$scope.AddSeedlings = function(AddSeedlig,form)
		{
			//$scope.Addsubmitted=true;
			

		if($scope.SeedlingTrayMasterForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (SeedlingData) 
					  {
	            		 gridData.push(angular.toJson(SeedlingData));														 
				      });
					  obj.gridData = gridData;
				      obj.formData = angular.toJson(AddSeedlig);
				      delete obj["$index"];
					 // alert(JSON.stringify(obj));
					
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveSeedlingTray.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Seedling Tray Added Successfully!', "success");	
								$('.btn-hide').attr('disabled',false);
								$scope.AddSeedlig = angular.copy($scope.master);
								$scope.loadTrayCode();
								$scope.loadSeedlingTrayNames();
								$scope.seedlingTray = "";
								$index=1;
								$scope.data = [{'id':$index}]
							    form.$setPristine(true);			    											   
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
				}
		};
		
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddSeedlig = angular.copy($scope.master);
				$scope.loadTrayCode();
				$index=1;
				$scope.seedlingTray={};
				$scope.data = [{'id':$index}]				
			    form.$setPristine(true);			    											   
		   };		

		$scope.setValidation = function(id)
		  	{
			  var prvId="";
			  
			  if(id>1)
			  {
				  prvId = Number(id) - Number(1);
				 
				  var currentVal =  $('#from'+ id).val();
				  
				  var PrevVal =  $('#to'+prvId).val();
				   
				  var SameVal =  $('#to'+id).val();
			
				  if(currentVal<PrevVal || currentVal==SameVal || currentVal==PrevVal) 
				  { 
				  	$('#error'+ id).show();
					$('#saveButton').attr("disabled", true);
				  } 
				 else 
				  { 
				  	$('#error'+ id).hide();
					$('#errorto'+ PrvId).hide();
					$('#saveButton').attr("disabled", false);
				  }
			  }			  
		  };
		  $scope.setoValidation = function(id)
		  {
			  	  
			  	  var nextId = Number(id) + Number(1);	
				  var currentVal =  $('#from'+ id).val();
				  var nextVal =  $('#from'+nextId).val();
				  var SameVal =  $('#to'+id).val();			  	
				 
					 if(Number(SameVal)== Number(currentVal))
					 {
						 	$('#errorto'+ id).show();
						 	$('#saveButton').attr("disabled", true);
					 }
					 else
					 {
						 $('#errorto'+ id).hide();
						 	$('#saveButton').attr("disabled", false);
					 }
				 				  
				  
				  if(id<=$index)
				  {
					  if(SameVal==currentVal || SameVal>nextVal)
					  {
						
					  	$('#error'+ nextId).show();
						$('#saveButton').attr("disabled", true);					  
					  }
					 else
					  {
						 
					  	$('#error'+ id).hide();
						$('#saveButton').attr("disabled", false);					  					  
					  }
				  }
		  };
	 })


	 //==========================================
	 		//Unloading Contractor Master
	 //==========================================

	.controller('UnloadContractorMaster',function($scope,$http)
	{
		
		var maxid = new Object();
		maxid.tablename = "UnloadingContractor";
		var jsonTable= JSON.stringify(maxid);		
		$scope.isEdit = true;
		$scope.master = {};
		
		//------------Space Between Two Words --------
		$scope.spacebtw = function(b)
		{
			var twoSpace =$scope.AddedUnload[b];
			twoSpace =twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedUnload[b] =twoSpace;	
		};
		
	   //-----------Space Between Two Words (Grid)----
	   $scope.spacebtwgrid = function(b,id)
	   {
			var twoSpace = $scope.UnloadData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.UnloadData[id][b] = twoSpace;
	   };		
		
		
		//--------Get MAXID-------------
		$scope.loadUnloadContractorCode=function()
    	{
			var httpRequest = $http
			({
				method: 'POST',
				url : "/SugarERP/getMaxId.html",
				data: jsonTable,

	 			}).success(function(data) 
				{
					$scope.AddedUnload={'ucCode':data.id,'status':'0','modifyFlag':"NO"};
       			});
		};
		//-------------Get All Contractors-----
		$scope.getAllUnload=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllUnloadingContractor.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.UnloadData=data;											
			}); 
		};
		//------------Save Unloading Contractors
     	$scope.UnloadSubmit=function(AddedUnload,form)
		{
				//$scope.Addsubmitted = true;
				if($scope.Unloadcontractorform.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({						  
				    		url:"/SugarERP/saveUnloadingContractor.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedUnload),
							beforeSend: function(xhr) 
							{
								xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },							
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", "Unloading Contractor Added Successfully!", "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedUnload = angular.copy($scope.master);
									$scope.loadUnloadContractorCode();
									form.$setPristine(true);
									
									//-------------load Added all Contractors----		
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllUnloadingContractor.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.UnloadData=data;
											
										});  	   
									//-------get Maxid----------
									/*var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getMaxId.html",
									data: jsonTable,
									}).success(function(data, status) 
									{
										$scope.AddedUnload={'ucCode':data.id,'status':'0'};				
									});	   	 */  
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
					}		
				   else
				   {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					   
				   }
		};
		//-----------------update Contractors------
		$scope.updateUnloadMaster = function(Unload,id)
		{
				delete Unload["$edit"];
		  		var UpdatedData = angular.toJson(Unload);
				$scope.submitted = true;
				$scope.isEdit = true;
				
				if($scope.editUnloadForm.$valid)
				{				
					$scope.isEdit = false;
				  	$.ajax({
				   		url:"/SugarERP/saveUnloadingContractor.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:UpdatedData,
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			  
							if(response==true)
							{
		  						swal("Success", 'Unloading Contractor Updated Successfully!', "success");

								//-------------load Added all Contractors----		
								var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllUnloadingContractor.html",
									data: {}
								   }).success(function(data, status) 
									{
										$scope.UnloadData=data;										
									});  	   
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});	
				 }
         };
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedUnload = angular.copy($scope.master);
				$scope.loadUnloadContractorCode();
			    form.$setPristine(true);			    								
			   
		   };
		 
	})

	 //===========================================
	 	   //Weigh Bridge Master
	 //===========================================
	 
	.controller('WeightBridgeMaster',function($scope,$http)
	{
		
	  	var maxid = new Object();
		maxid.tablename = "WeighBridge";
		var jsonTable= JSON.stringify(maxid);		
		$scope.isEdit = true;
		$scope.master = {};
		
		//-----Duplicate String----
		$scope.validateDup = function()
			  {
				 // alert($scope.advance);
					for(var d =0;d<JSON.stringify($scope.WeightData.length);d++)
					{
						var firstString = $scope.AddedWeight.bridgeName.toUpperCase();
						var secondString = $scope.WeightData[d].bridgeName.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.WeightData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.WeightData[i].bridgeName.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		
		//------------Space Between Two Words ---------
  		$scope.spacebtw = function(b)
		{
			var twoSpace =$scope.AddedWeight[b];
			twoSpace =twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedWeight[b] =twoSpace;	
		};

		//--------Space Between Two Words (Grid)----
		$scope.spacebtwgrid = function(b,id)
		{
			var twoSpace = $scope.WeightData[id][b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.WeightData[id][b] = twoSpace;
		};
		
		
		//--------Get Max ID---------
	   	$scope.loadWeightCode=function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			 	}).success(function(data) 
				{			
					$scope.AddedWeight={'bridgeId':data.id,'status':'0','modifyFlag':"NO"};											
	       		});	
		};
	
		//-------Get All Added Weights-----
		$scope.getAllWeight=function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllWeighBridge.html",
					data: {}
				}).success(function(data, status) 
				{
					$scope.WeightData=data;											
				}); 
		};
		
		//---------Save Weight-------
		$scope.WeightSubmit=function(AddedWeight,form)
		{
			//$scope.Addsubmitted = true;
			if($scope.WeightBridgeForm.$valid) 
			{	
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				   		url:"/SugarERP/saveWeighBridge.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddedWeight),
						beforeSend: function(xhr) 
						{
				            xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
			    	    },
					    success:function(response) 
						{	
							if(response==true)
							{
								swal("Success!", 'WeighBridge Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								$scope.AddedWeight = angular.copy($scope.master);
								$scope.loadWeightCode();
								form.$setPristine(true);
								
								//--------get all Weight Bridge--------									
								var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllWeighBridge.html",
									data: {}
								   }).success(function(data, status) 
									{
										$scope.WeightData=data;											
									});  	   
								//-------get Maxid----
								/*var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getMaxId.html",
										data: jsonTable,
								   }).success(function(data, status) 
									{
											$scope.AddedWeight={'bridgeId':data.id,'status':'0'};				
							        });	  */ 	   
							   //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}		
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
					
				}
			};

		//-------------Update Weights---------

			$scope.updateWeight = function(Weight,id)
			{
					delete Weight["$edit"];
			  		var UpdatedData = angular.toJson(Weight);
					
					$scope.submitted=true;
					$scope.isEdit = true;
					
					if($scope.editWeightBridgeForm.$valid)
					{
						$scope.isEdit = false;
						
				  		$.ajax({
				    		url:"/SugarERP/saveWeighBridge.html",
				    		processData:true,
				    		type:'POST',
				    		contentType:'Application/json',
				    		data:UpdatedData,
							beforeSend: function(xhr) 
							{
			            		xhr.setRequestHeader("Accept", "application/json");
			            		xhr.setRequestHeader("Content-Type", "application/json");
			        		},
				    		success:function(response) 
							{			  
								if(response==true)
								{
			  						swal("Success", 'WeighBridge Updated Successfully!', "success");
									
									//--------get all Weight Bridge--------									
									var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getAllWeighBridge.html",
											data: {}
										   }).success(function(data, status) 
										   {
										      $scope.WeightData=data;											
  									       });  	   									
									//------End--------------
								}
							   else
							    {
									sweetAlert("Oops...", "Something went wrong!", "error");
								}
							}
						});	
					}
             };
			 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedWeight = angular.copy($scope.master);
				$scope.loadWeightCode();
			    form.$setPristine(true);			    								
			   
		   };
			 
		})	 
	
	  //==========================================
	  		//STANDARD DEDUCTION MASTER
	  //==========================================
	  .controller('DeductionMaster',function($scope,$http)
	  {
	
			var maxid = new Object();
			
			maxid.tablename = "StandardDeductions";
			var jsonTable= JSON.stringify(maxid);		
			$scope.isEdit = true;
			$scope.master = {};
		
			$scope.validateDup = function()
			  {
				 // alert($scope.advance);
					for(var d =0;d<JSON.stringify($scope.DeductionData.length);d++)
					{
						var firstString = $scope.AddedDeduction.name.toUpperCase();
						var secondString = $scope.DeductionData[d].name.toUpperCase();
						if(firstString!=null)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
							{
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
					
			  };
			  
			  $scope.validateDuplicate = function(desigName,id)
			  {
				 for(var i =0;i<JSON.stringify($scope.DeductionData.length);i++)
				 {
					var str1 = desigName.toUpperCase();
					var str2 = $scope.DeductionData[i].name.toUpperCase();
					if(id!=i)
					{
					 if(str1 == str2)
					 {
						$('.btn-hideg').attr('disabled',true);						
								$(".duplicate"+ id).show();
								return false;
					 }
					 else
					 {
						 $(".duplicate"+ id).hide();
						 $('.btn-hideg').attr('disabled',false);	
					 }
					}
				 }
			  };		
		//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddedDeduction[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedDeduction[b] = twoSpace;				
			};
			
			//=================Space Between Two Words (Grid)================
			$scope.spacebtwgrid = function(b,id)
			{
				var twoSpace = $scope.DeductionData[id][b];
				
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				
				$scope.DeductionData[id][b] = twoSpace;
				
			};
			//=============Amounts in Decimals ==================
			$scope.decimalpoint = function(b)
			{
				var decmalPoint = $scope.AddedDeduction[b];
				
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				 $scope.AddedDeduction[b] = getdecimal;
				 if(getdecimal=="NaN")
				{
					 $scope.AddedDeduction[b]="";
				}
			};
			
			$scope.decimalpointgrid = function(b,id)
			{
				var decmalPoint = $scope.DeductionData[id][b];
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
			    $scope.DeductionData[id][b] = getdecimal;
				 if(getdecimal=="NaN")
				{
					 $scope.DeductionData[id][b]="";
				}
			};		
		
		
			//-------Get MAxid------------
	 	   $scope.loadDeductionMasterCode=function()
		   {
			   
				 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
				 	}).success(function(data) 
					{			
						$scope.AddedDeduction={'stdDedCode':data.id,'status':'0','amountValidity':'0','modifyFlag':"NO",'screenName':'Standard Deductions'};	
       				});
		   };
		   //--------Get All Added Deductions--
		   $scope.getAllDeduction=function()
		   {
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllStandardDeductions.html",
						data: {}
					}).success(function(data, status) 
					{
						$scope.DeductionData=data;											
					}); 
		   };
		  //----------Save Deductions----------
		  
		  $scope.DeductionSubmit=function(AddedDeduction,form)
	  	  {
			  
				//$scope.Addsubmitted = true;
				
				if($scope.DeductionMasterForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveStandardDeductions.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedDeduction),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{								
								if(response==true)
								{
									swal("Success!", 'Deductions Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedDeduction = angular.copy($scope.master);
									$scope.loadDeductionMasterCode();
									form.$setPristine(true);			    								
									
									//---------get all Deductions--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllStandardDeductions.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.DeductionData=data;
										});  	   
									//---------End--------
									//-------get Maxid----
									/*var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getMaxId.html",
													data: jsonTable,
											   }).success(function(data, status) 
												{
													$scope.AddedDeduction={'stdDedCode':data.id,'status':'0','amountValidity':'0'};			
										        });	   	*/   
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);
									
								}
							}
						});					
				}	
			  else
			   {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}				   
			   }
		};
		//-----------update Deductions-----
		$scope.UpdateDeductionMaster = function(Deduction,id)
		{
				delete Deduction["$edit"];
				$scope.submitted=true;
				$scope.isEdit = true;
		  		var UpdatedData = angular.toJson(Deduction);
				
				if($scope.EditDeductionMasterForm.$valid)
				{
					$scope.isEdit = false;
				  	$.ajax({
				   		url:"/SugarERP/saveStandardDeductions.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:UpdatedData,
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			  
							if(response==true)
							{
		  						swal("Success", 'Deductions Updated Successfully!', "success");
								
								//---------get all Deductions--------									
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllStandardDeductions.html",
										data: {}
									  }).success(function(data, status) 
									  {
										$scope.DeductionData=data;
									  });  	   
							    //---------End--------								
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});	
				}
         };
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedDeduction = angular.copy($scope.master);
				$scope.loadDeductionMasterCode();
			    form.$setPristine(true);			    											   
		   };
		 
	})	
			
	  //==========================================
	  		//Order of Deductions
	  //==========================================
	  
	  .controller('orderDeductionMaster', function($scope,$http)
	  {
		  
		  var finalObj = [];		  
		  var Deduction = new Object();
		  
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "StandardDeductions";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);

		  //-------Load Added Deductions-------
		  $scope.LoadAddedDeductions = function()
		  {
				//$scope.AddOrderDeductionData=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						$scope.AddOrderDeductionData=data;
					}); 
		  };
		  //----------Save Deductions Order--------
		  $scope.SaveOrderDeductions = function(AddOrderDeduction)
		  {
				var opts = $('.form-control1')[0].options;
				var getDropDownValues = $.map(opts, function(elem) 
				{
				    return (elem.value || elem.text);
				});	
				
				for(var i=0;i<getDropDownValues.length;i++)
				{
					//obj = [];
					var obj = new Object();
					var splitedValues = getDropDownValues[i].split("-");
					obj.stdDedCode = splitedValues[0];
					obj.name = splitedValues[1];
					obj.deductionOrder = Number(i)+Number(1);
					
					finalObj.push(obj);
					
				}
					Deduction.StandardDeduction = finalObj;
					$('.btn-hide').attr('disabled',true);
					
				  	$.ajax({
				   		url:"/SugarERP/saveStandardDeductionsOrder.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:JSON.stringify(finalObj),
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			  
							if(response==true)
							{
		  						swal("Success", 'Deductions Updated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);								
								
								finalObj = [];
								//---------get all Deductions--------									
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/loadDropDownNames.html",
										data: jsonStringDropDown,
								   }).success(function(data, status) 
									{
										$scope.AddOrderDeductionData=data;
									}); 
								  
							    //---------End--------								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});					
		  };		  
	  })
	  	  
	  //=========================================
	  		//Harvesting Contractor Master
	  //=========================================
	   .controller('HarvestingContractorMaster', function($scope,$http)
		{
			$index=0;
			$scope.data = [];
			var HarSuretyData = [];
			var harObj = new Object();
			
			var HarvestedID = new Object();
			HarvestedID.tablename = "HarvestingContractors";
			var jsonHarTable= JSON.stringify(HarvestedID);	
			
			var HarDropDownLoad = new Object();
			HarDropDownLoad.dropdownname = "Bank";
			var jsonHarStringDropDown= JSON.stringify(HarDropDownLoad);
			
			var HarBranchDropDown = new Object();
			HarBranchDropDown.dropdownname = "Branch";
			var jsonHarBranchDropDown= JSON.stringify(HarBranchDropDown);
			
			var HarAllDropDown = new Object();
			HarAllDropDown.tablename = "HarvestingContractors";
			HarAllDropDown.columnName = "harvestercode";
			HarAllDropDown.columnName1 = "harvester";
			var jsonHarAllDropDown= JSON.stringify(HarAllDropDown);
			
			var DropDownLoad = new Object();
			DropDownLoad.targetdropdownname = "Branch";
			DropDownLoad.columnname = "bankcode";			

			$scope.master = {};
			
			//---------Validtae Duplicate Entrys----
		 $scope.validateDup = function()
		  {
				for(var d =0;d<JSON.stringify($scope.GetAllAddedHarvesters.length);d++)
				{
					var firstString = $scope.AddHarvester.harvester.toUpperCase();
					var secondString = $scope.GetAllAddedHarvesters[d].harvester.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };			
			//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddHarvester[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddHarvester[b] = twoSpace;				
			};		
		
			//-------Get MAxid------------
		 	$scope.loadHarvesterCode=function()
		    {		
				 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonHarTable,
				 	}).success(function(data) 
					{	
						$scope.AddHarvester={'harvesterCode':data.id,'status':'0','modifyFlag':"No"};		
       				});
		    };
			//-------Load Bank DropDown-----------
			 $scope.loadHarvesterBanks = function()
			 {
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadDropDownNames.html",
						  data: jsonHarStringDropDown,
					   }).success(function(data, status) 
						{							
							$scope.GetBankDropdownData=data;							
						});											 
			 };	
			//-------Set Branch Dropdown on bank change------
			$scope.getBankbranchdata = function(bankdrop)
			{
				if(bankdrop!=null)
				{
					DropDownLoad.value = bankdrop;
					var jsonBranchDropDown= JSON.stringify(DropDownLoad);
				
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/onChangeDropDownValues.html",
							data: jsonBranchDropDown,
						}).success(function(data, status) 
						{		
							$scope.GetBranchDropdownData=data;
						}); 		
				}
			   else
			    {
					$scope.GetBranchDropdownData={};
				}
				
			};			
			
			//--------Added Harvestors Data in dropdown------
			$scope.LoadAddedHarvestorsDropDown = function()
			{
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadAddedDropDownsForMasters.html",
						  data: jsonHarAllDropDown,
					   }).success(function(data, status) 
						{		
							$scope.GetAllAddedHarvesters=data;							
						});											 				
				
			};
			
			//--------Set ifsc Code on branch onchange-------
			$scope.setHarvesterifscCode = function(value)
			{			
				$scope.AddHarvester.ifscCode = value;
			};
										
			//-------------Add Surety Details-------------
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};		 		 
			 //------------Delete Row from table---
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
  	    	};				
			
			//-----------Save Contractor--------
			$scope.SaveHarvestingContractor = function(AddHarvester,form)
			{
				
				
				//$scope.Addsubmitted=true;
				if($scope.HarvestingContractorForm.$valid)
				{
					HarSuretyData = [];
				    $scope.data.forEach(function (harvesterData) 
				    {
	    	           HarSuretyData.push(angular.toJson(harvesterData));														 
		    	    });
					
					harObj.gridData = HarSuretyData;
					harObj.formData = angular.toJson(AddHarvester);
					
					$('.btn-hide').attr('disabled',true);
				  	$.ajax({
				   		url:"/SugarERP/saveHarvestingContractor.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:JSON.stringify(harObj),
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			 
							if(response==true)
							{
		  						swal("Success", 'Harvesting Contractor Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								//location.reload();
								$scope.AddHarvester = angular.copy($scope.master);
								$scope.loadHarvesterCode();
								$scope.AddedHarvesters = "";
								form.$setPristine(true);
								
								$index=1;
								$scope.data = [{'id':$index}]				
								
								//--------Get All Added Harvesters-----
								 var httpRequest = $http({
									 	  method: 'POST',
										  url : "/SugarERP/loadDropDownNames.html",
										  data: jsonHarAllDropDown,
									   }).success(function(data, status) 
										{		
											$scope.GetAllAddedHarvesters=data;							
											
										});											 				
								//------End-----------								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});										
				}
			   else
			   {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}				   
			   }
			};
			
			//---------Load Added Data--------
			$scope.loadAllAddedHarvesterData = function(AddedHarvesters)
			{
				if(AddedHarvesters!=null)
				{
					 var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllHarvestingContractors.html",
							data: JSON.stringify(AddedHarvesters),
					     }).success(function(data, status) 
				    	 {  
							$scope.AddHarvester = data.formData;
							$scope.AddHarvester.modifyFlag = "Yes";
							$scope.data=data.gridData;
							$index = data.gridData.length;
					     });											 			 								
				}
			   else
			    {
					$scope.AddHarvester = angular.copy($scope.master);
					$scope.loadHarvesterCode();
					$scope.AddedHarvesters = "";
					$index=1;
					$scope.data = [{'id':$index}]								
				    $scope.HarvestingContractorForm.$setPristine(true);			    											   					
				}
			};
			//---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {	
		   		if(modifyId!=null)
				{
					$scope.AddHarvester = angular.copy($scope.master);
					$scope.loadHarvesterCode();
					$scope.AddedHarvesters = "";
					$index=1;
					$scope.data = [{'id':$index}]								
				    form.$setPristine(true);			    											   					

				}
			   else
			    {
					$scope.AddHarvester = angular.copy($scope.master);
					$scope.loadHarvesterCode();
					$scope.AddedHarvesters = "";
					$index=1;
					$scope.data = [{'id':$index}]								
				    form.$setPristine(true);			    											   					
				}
		   };			
		})


	 //===========================================
	 		//Transport Contractor Master
	 //===========================================	 
	 .controller('TransportContractorMaster', function($scope,$http)
	 {
		 
			$index=0;
			$scope.data = [];
			var TarSuretyData = [];
			var TarObj = new Object();
			
			var TransporeteID = new Object();
			TransporeteID.tablename = "TransportingContractors";
			var jsonTarTable= JSON.stringify(TransporeteID);	
			
			var TarDropDownLoad = new Object();
			TarDropDownLoad.dropdownname = "Bank";
			var jsonTarStringDropDown= JSON.stringify(TarDropDownLoad);
			
			/*var TarBranchDropDown = new Object();
			TarBranchDropDown.dropdownname = "Branch";
			var jsonTarBranchDropDown= JSON.stringify(TarBranchDropDown);*/
			
			var TarAllDropDown = new Object();
			TarAllDropDown.tablename = "TransportingContractors";
			TarAllDropDown.columnName = "transportercode";
			TarAllDropDown.columnName1 = "transporter";
			var jsonTarAllDropDown= JSON.stringify(TarAllDropDown);
			
			var DropDownLoad = new Object();
			DropDownLoad.targetdropdownname = "Branch";
			DropDownLoad.columnname = "bankcode";	
			$scope.master = {};
			
			//---------Validtae Duplicate Entrys----
			$scope.validateDup = function()
		   {
				for(var d =0;d<JSON.stringify($scope.GetAllAddedHarvesters.length);d++)
				{
					var firstString = $scope.AddTransporter.transporter.toUpperCase();
					var secondString = $scope.GetAllAddedTransporters[d].transporter.toUpperCase();
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };		 
		 
		 
			//=============Space Between Two Words ==================
		  	$scope.spacebtw = function(b)
			{
				var twoSpace = $scope.AddTransporter[b];
				twoSpace = twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddTransporter[b] = twoSpace;				
			};		 
		 
			//-------Get MAxid------------
		 	$scope.loadTransporterCode=function()
		    {	
				 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTarTable,
				 	}).success(function(data) 
					{							
						$scope.AddTransporter={'transporterCode':data.id,'status':'0','modifyFlag':"No"};		
       				});
		    };
			//-------Load Bank DropDown-----------
			 $scope.loadTransporterBanks = function()
			 {
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadDropDownNames.html",
						  data: jsonTarStringDropDown,
					   }).success(function(data, status) 
						{							
							$scope.GetBankDropdownData=data;							
						});											 
			 };	
			//-------Load Branch DropDown-----------
			/* $scope.loadTransporterBranchs = function()
			 {
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadDropDownNames.html",
						  data: jsonTarBranchDropDown,
					   }).success(function(data, status) 
						{				
							$scope.GetBranchDropdownData=data;							
						});											 
			 };*/	
		 	
			//----------display branches on Bank Dropdown onchange---
			$scope.getBankbranchdata = function(bankdrop)
			{
				if(bankdrop!=null)
				{
					DropDownLoad.value = bankdrop;
					var jsonBranchDropDown= JSON.stringify(DropDownLoad);
				
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/onChangeDropDownValues.html",
							data: jsonBranchDropDown,
						}).success(function(data, status) 
						{		
							$scope.GetBranchDropdownData=data;
						}); 		
				}
			   else
			    {
					$scope.GetBranchDropdownData = {};
				}
				
			};			
			
			
			//--------Added Harvestors Data in dropdown------
			$scope.LoadAddedTransportersDropDown = function()
			{
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadAddedDropDownsForMasters.html",
						  data: jsonTarAllDropDown,
					   }).success(function(data, status) 
						{		
							$scope.GetAllAddedTransporters=data;							
						});											 				
				
			};
			
			//--------Set ifsc Code on branch onchange-------
			$scope.setTransporterifscCode = function(value)
			{			
				$scope.AddTransporter.ifscCode = value;
			};		 
			//-------------Add Surety Details-------------
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};		 		 
			 //------------Delete Row from table---
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
  	    	};				
			
			//-----------Save Contractor--------
			$scope.SaveTransportContractor = function(AddTransporter,form)
			{
								
				//$scope.Addsubmitted=true;
				//alert($scope.TransportContractorForm.$valid);
				if($scope.TransportContractorForm.$valid)
				{
					TarSuretyData = [];
				    $scope.data.forEach(function (TransporterData) 
				    {
	    	           TarSuretyData.push(angular.toJson(TransporterData));														 
		    	    });
					
					TarObj.gridData = TarSuretyData;
					TarObj.formData = angular.toJson(AddTransporter);
					
					$('.btn-hide').attr('disabled',true);
				  	$.ajax({
				   		url:"/SugarERP/saveTransportingContractor.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:JSON.stringify(TarObj),
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			 
							if(response==true)
							{
		  						swal("Success", 'Transporting Contractor Added Successfully!', "success");
								 $('.btn-hide').attr('disabled',false);
								 
								$scope.AddTransporter = angular.copy($scope.master);
								$scope.loadTransporterCode();
								form.$setPristine(true);								 								 
							 	$scope.AddTransporter = {};
								$index=1;
								$scope.data = [{'id':$index}]				   
								
								//--------Get All Added Harvesters-----
								 var httpRequest = $http({
									 	  method: 'POST',
										  url : "/SugarERP/loadDropDownNames.html",
										  data: jsonTarAllDropDown,
									   }).success(function(data, status) 
										{		
											$scope.GetAllAddedTransporters=data;							
										});											 				
								//------End-----------								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});										
				}
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					
				}
				
			};			
			//---------Load Added Data--------
			$scope.loadAllAddedTransportersData = function(AddedTransporters)
			{
				if(AddedTransporters!=null)
				{
					 var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllTransportingContractors.html",
							data: JSON.stringify(AddedTransporters),
					     }).success(function(data, status) 
				    	 {  
							$scope.AddTransporter = data.formData;
							$scope.AddTransporter.modifyFlag = "Yes";
							$scope.data=data.gridData;
							$index = data.gridData.length;
					     });
				}
			  else
			   {
					$scope.AddHarvester = angular.copy($scope.master);
					$scope.loadTransporterCode();
				 	$scope.AddTransporter = {};
					$index=1;
					$scope.data = [{'id':$index}]				   				
				    $scope.TransportContractorForm.$setPristine(true);			    								
			   }

			};
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddHarvester = angular.copy($scope.master);
				$scope.loadTransporterCode();
			 	$scope.AddTransporter = {};
				$index=1;
				$scope.data = [{'id':$index}]				   				
			    form.$setPristine(true);			    								
			   
		   };
			
	 })

	  //==========================================
	  		//Tie-Up Loan Interest Master
	  //==========================================
	  
	  .controller('TieupLoanInterestMaster', function($scope,$http)
	  {
		
			$index=0;
			$scope.data = [];
			var TieupDataObj = [];
			var TieupObj = new Object();
			var filterObj = new Object();
			
			var LoanBankDrop = new Object();
			LoanBankDrop.dropdownname = "Branch";
			var jsonLoanBankDrop= JSON.stringify(LoanBankDrop);
			
			var LoanSeasonDrop = new Object();
			LoanSeasonDrop.dropdownname = "Season";
			var jsonLoanSeasonDrop= JSON.stringify(LoanSeasonDrop);			
			//$scope.AddTieupLoan={'modifyFlag':"No"};
			$scope.master = {};
			$scope.setRowAfterReset = function()
			{
							$scope.data = {};
							$index = 1;
							$scope.data = [{'id':$index}]
							//$scope.AddTieupLoan={'modifyFlag':"No"};
			};	
			
			//-------Load Bank DropDown-----------
			 $scope.loadTieupLoanBanks = function()
			 {
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadDropDownNames.html",
						  data: jsonLoanBankDrop,
					   }).success(function(data, status) 
						{			
							
							$scope.GetLoanBankDropdownData=data;							
						});											 
			 };	
			//-------Load Season DropDown-----------
			 $scope.loadTieupLoanSeasons = function()
			 {
				 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/loadDropDownNames.html",
						  data: jsonLoanSeasonDrop,
					   }).success(function(data, status) 
						{			
							$scope.GetLoanSeasonDropdownData=data;							
						});											 
			 };	
		    //--------------Load Tieup Loans Filter----------------
      		$scope.loadTieupLoansByFilters = function(AddTieupLoan) 
		    {
				if(AddTieupLoan.season!='' && AddTieupLoan.branchCode!='') 
				{ 					
					var seasonSplit = AddTieupLoan.season.split("-");
					var fromYear = Number(seasonSplit[0])-Number(1);
					var toYear = Number(seasonSplit[1])-Number(1);
					var seasonyear = fromYear+"-"+toYear;
					
					filterObj.season = seasonyear;
					filterObj.branchcode = $scope.AddTieupLoan.branchCode;
					
					 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/getAllTieUpInterests.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{														
							$scope.AddTieupLoan.payableDate=data.formData.payableDate;
							//$scope.AddTieupLoan.modifyFlag = "No";
							$scope.data=data.gridData;						
							$index = data.gridData.length;					
						});											 										
				} 
				else
				{
					
					//$('.btn-hide').attr('disabled',false);
				}        		
	        };
			
		 $scope.seasonOnChange=function()
   		{
	  		 if($scope.AddTieupLoan.season!=null  && $scope.AddTieupLoan.branchCode!=null)
			 {
					filterObj.season = $scope.AddTieupLoan.season;
					filterObj.branchcode = $scope.AddTieupLoan.branchCode;

				var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/getAllTieUpInterests.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{

							$scope.data=data.gridData;
							$scope.AddTieupLoan=data.formData;
							$index = data.gridData.length;
							//$scope.AddTieupLoan.modifyFlag = "Yes";
							//$('.btn-hide').attr('disabled',true);
		  			}).error(function(data,status)
			 		 {
 		 				$scope.data = {};
						$scope.AddTieupLoan.payableDate= "";
						//$scope.AddTieupLoan.modifyFlag = "No";
						$index = 1;
						$scope.data = [{'id':$index}]
						$('.btn-hide').attr('disabled',false);
					});
				 }
				 else
				 {
					$scope.data = [{'id':$index}]
				 }
	   		};			
			
			//----------Save Tieup Loans------------
			$scope.AddedTieupLoans = function(AddTieupLoan,form)
			{
				//$scope.Filtersubmitted = true;
				//$scope.submitted = true;
				$scope.modifyFlag="No";
				
				if($scope.TieupLoanInterestMasterForm.$valid)
				{
					TieupDataObj = [];
				    $scope.data.forEach(function (TieupData) 
				    {
    		           TieupDataObj.push(angular.toJson(TieupData));														 
	    		    });
					TieupObj.formData = AddTieupLoan;
					TieupObj.gridData = TieupDataObj;
					TieupObj.modifyFlag = $scope.modifyFlag;
					
					
					//alert(JSON.stringify(TieupObj));
					$('.btn-hide').attr('disabled',true);
				  	$.ajax({
				   		url:"/SugarERP/saveTieUpLoanInterest.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:JSON.stringify(TieupObj),
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			 
							//alert(response);
							if(response==true)
							{
		  						swal("Success", 'Tieup Loan Interests Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								 $scope.loadTieupLoanBanks();
								$scope.AddTieupLoan = angular.copy($scope.master);
								$scope.addTielUpField();
								$scope.setRowAfterReset();
							    form.$setPristine(true);

							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});										
				}		
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
					
				}
			};
			
				//---------Reset Form----
		   $scope.reset = function(form)
		   {		
				$scope.AddTieupLoan = angular.copy($scope.master);
				$scope.addTielUpField();
				$scope.setRowAfterReset();
			    form.$setPristine(true);
				$('.btn-hide').attr('disabled',false);
		   };
			
			//--------Add Loan Interest Rates-------
			$scope.addTielUpField = function() 
			{
				$index++;				
		    	$scope.data.push({'id':$index})
			};		 		 
			 //------------Delete Row from table---
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
  	    	};				
			
		  //------Validation---------
		  $scope.setTieupFromValidation = function(id)
		  {
			  var prvId="";
			 // alert(id);
			  if(id>1)
			  {
				  prvId = Number(id) - Number(1);
				  var currentVal =  $('#from'+ id).val();
				  var PrevVal =  $('#to'+prvId).val();
				  var SameVal =  $('#to'+id).val();
				 // alert("currentVal===="+currentVal);
				 // alert("PrevVal========"+PrevVal);
				  if(currentVal<=PrevVal || currentVal==SameVal) 
				  { 
				  	//$('#saveButton').attr("disabled", true);
					$scope.TieupLoanInterestMasterForm.$invalid = true;
				  	$('#error'+ id).show();					
				  } 
				 else 
				  { 
					//$('#saveButton').attr("disabled", false);				  
					$scope.TieupLoanInterestMasterForm.$invalid = false;
				  	$('#error'+ id).hide();
					$('#errorto'+ PrvId).hide();					
				  }
			  }			  
		  };
		  //----------Validation-------------
		  $scope.setTieupToValidation = function(id)
		  {
			  	  
			  	  var nextId = Number(id) + Number(1);	
				  var prevId = Number(id) - Number(1);	
				  var currentVal =  $('#from'+ id).val();
				  var nextVal =  $('#from'+nextId).val();
				  var SameVal =  $('#to'+id).val();
				  var PrevVal =  $('#to'+prvId).val();
				  
				  if(id<=$index)
				  {
					  if(SameVal==currentVal || SameVal>=nextVal || SameVal<nextVal)
					  {
						//$('#saveButton').attr("disabled", true);
						$scope.TieupLoanInterestMasterForm.$invalid = true;
					  	$('#error'+ nextId).show();
											  
					  }
					 else
					  {
						//$('#saveButton').attr("disabled", false);					  					  
						$scope.TieupLoanInterestMasterForm.$invalid = false;
					  	$('#error'+ id).hide();						
					  }
					  
					  if(currentVal<=PrevVal || currentVal==SameVal) 
					  { 
					  	$('#error'+ id).show();
						//$('#saveButton').attr("disabled", true);
						$scope.TieupLoanInterestMasterForm.$invalid = true;
					  } 
					 else 
					  { 
					  	$('#error'+ id).hide();
						$('#errorto'+ PrvId).hide();
						//$('#saveButton').attr("disabled", false);
						$scope.TieupLoanInterestMasterForm.$invalid = false;
					  }					  
					  
					  
				  }
		  };			
		  
		  $scope.hideButton = function(id)
		  {
			 // alert(id);
		  };
	  })
	
 	 //=========================================
	 		//Cane Weight Master
	 //=========================================

	  .controller('CaneWeightMaster', function($scope,$http)
	  {
		  	var maxid = new Object();			
			maxid.tablename = "CaneWeight";
			var jsonTable= JSON.stringify(maxid);	
			
		    var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Season";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			$scope.master = {};
			
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			//-----------Save Cane Weight Master--------
			
			$scope.CaneWeightSubmit = function(AddedCaneWeight,form)
			{				
			
				//$scope.submitted = true;				
				if($scope.CaneWeightForm.$valid) 
				{	
				   $('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveCaneWeight.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedCaneWeight),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{									
								if(response==true)
								{
									swal("Success!", 'Cane Weight Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);		
									$scope.AddedCaneWeight = angular.copy($scope.master);
									form.$setPristine(true);
									
									//---------get all Cane Weights--------									
									var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/loadDropDownNames.html",
											data: jsonStringDropDown,
										  }).success(function(data, status) 
										  {
												$scope.seasonNames=data;
							 			  }); 									
									 //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}	
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};

			//---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {			  
		   		if(modifyId!=null)
				{
					$scope.getAddedWeights(modifyId);
				}
			   else
			    {					
					$scope.AddedCaneWeight = angular.copy($scope.master);
				    form.$setPristine(true);			    											   
				}
		   };

			
			//-------------Get Added Cane Weights on Dropdown change--
		  
		   $scope.getAddedWeights = function(Weight)
		   {
			   var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getCaneWeightBySeason.html",
						data:Weight,
					  }).success(function(data, status) 
					  {
						    //alert(data.status);
							$scope.AddedCaneWeight = data;
					  }).error(function(data,status)
					  {
						  //alert(data);
					  });			  
		   };		 
		   
		   //-------Reset Form-------
		   //$scope.resetForm = function(FormName) 
		   //{

			    //$scope.AddedCaneWeight.bmPercentage = "";				
				//$scope.AddedCaneWeight.season = "";
     			//$scope.CaneWeightForm.$setPristine();
			//};		   		   
	  })

	 //==========================================
	 		//Shift Master
	 //==========================================
	 .controller('ShiftMaster', function($scope,$http)
	  {
		
			$index=0;
			$scope.data = [];
			var ShiftDataObj = [];
			var ShiftObj = new Object();
			var TotalHrsObj = new Object();
			$scope.master = {};
			
			//-------Time Picker--
			$scope.displayTime = function(id,name)
			{
				//$('.time').bootstrapMaterialDatePicker({ date: false, shortTime: true, format: 'HH:mm:00' });
				$('.time').timepicker({ 'scrollDefault': 'now', format: 'HH:mm:ss'});
				var StoreseassionId = sessionStorage.setItem('sessionId',id);					
			};
			
			$scope.$applyAsync(function()
			{
				$('.time').timepicker({ 'scrollDefault': 'now', format: 'HH:mm:ss' });
			})
			
			$scope.CalculateShiftHrs = function()			
			{
				//alert(1);
				var sessionId = sessionStorage.getItem('sessionId');
				//alert(sessionId);
				if(sessionId!='')
				{
				    var startTime = $('#from'+ sessionId).val();
					
				    var endTime = $('#to'+ sessionId).val();
					if(startTime!='' && endTime!='')
					{
	        	        var startTimeArray = startTime.split(":");
    	        	    var startInputHrs = parseInt(startTimeArray[0]);
        	        	var startInputMins = parseInt(startTimeArray[1]);

						var endTimeArray = endTime.split(":");
						var endInputHrs = parseInt(endTimeArray[0]);
			        	var endInputMins = parseInt(endTimeArray[1]);

					    var startMin = startInputHrs*60 + startInputMins;
   						var endMin = endInputHrs*60 + endInputMins;

					    var result;
	
					   if (endMin < startMin) 
					   {
					       var minutesPerDay = 24*60; 
				    	   result = minutesPerDay - startMin;  // Minutes till midnight
					       result += endMin; // Minutes in the next day
					   } 
					  else 
					   {
				    	  result = endMin - startMin;
					   }
	
					   var minutesElapsed = result % 60;
					   var hoursElapsed = (result - minutesElapsed) / 60;

					 // $scope.data[sessionId-1].shiftHrs = hoursElapsed +':'+ (minutesElapsed < 10 ? '0'+minutesElapsed : minutesElapsed)+":"+"00";
					  
					  if(endTime.length>3)
					 {
						  $scope.data[sessionId-1].shiftHrs = hoursElapsed +':'+ (minutesElapsed < 10 ? '0'+minutesElapsed : minutesElapsed)+":"+"00";
					}
					else
					{

						$scope.data[sessionId-1].shiftHrs="";
					}
					  //var getDayFlag = endTime.split(" ");
					  //if(getDayFlag[1]=='am') { $scope.data[sessionId-1].dayFlag = 1 } else { $scope.data[sessionId-1].dayFlag = 0 }					  
					  
					}
			   }
			  
				   	var sumHrs = 0;
				    var sumMins = 0;

				   for(var k=0;k<$index;k++)
				   {
					   var m=Number(k)+Number(1);
					   //var hrs = $('#hrs'+ m).val();
					   var hrs = $scope.data[k].shiftHrs
					   if(hrs!='')
					   {
					   	   var hrsh = hrs.split(":");				   				   
						   var hours = hrsh[0];
						   var mins =  hrsh[1];
				   
						   sumHrs += +parseInt(hours);
						   sumMins += +parseInt(mins);
					   }
				   }
			   		
					if(sumMins>=60){
						sumHrs=sumHrs+1;
					sumMins=sumMins-60;
						
						
						
					}
				   for(var m=0;m<$index;m++)
				   {
					   $scope.data[m].totalHrs = (sumHrs < 10 ? '0'+sumHrs : sumHrs) +":"+ (sumMins < 10 ? '0'+sumMins : sumMins)+":"+"00";
				   }
			   		//alert((sumHrs < 10 ? '0'+sumHrs : sumHrs) +":"+ (sumMins < 10 ? '0'+sumMins : sumMins)+":"+"00");
				   $scope.totalHrs = (sumHrs < 10 ? '0'+sumHrs : sumHrs) +":"+ (sumMins < 10 ? '0'+sumMins : sumMins)+":"+"00";
				  
			  
				   
		};	
			
			
		
			//--------Add Shifts-------
			$scope.addShiftField = function() 
			{
				$index++;				
		    	$scope.data.push({'shiftId':$index})
			};		 		 
			 //------------Delete Shifts---
			$scope.removeShiftRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].shiftId === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].shiftId = Number(s)+Number(1);
				}
				
			   var sumHrs = 0;
			   var sumMins = 0;

			   for(var k=0;k<$index;k++)
			   {
				   var m=Number(k)+Number(1);
				   var hrs = $('#hrs'+ m).val();
				   if(hrs!='')
				   {
				   	   var hrsh = hrs.split(":");				   				   
					   var hours = hrsh[0];
					   var mins =  hrsh[1];
				   
					   sumHrs += +parseInt(hours);
					   sumMins += +parseInt(mins);
				   }
			   }
			   $scope.totalHrs = (sumHrs < 10 ? '0'+sumHrs : sumHrs) +":"+ (sumMins < 10 ? '0'+sumMins : sumMins) +":"+"00";
			   for(var m=0;m<$index;m++)
			   {
				   $scope.data[m].totalHrs = (sumHrs < 10 ? '0'+sumHrs : sumHrs) +":"+ (sumMins < 10 ? '0'+sumMins : sumMins) +":"+"00";
			   }
				
				
				
  	    	};	
			
			//---------Save Shift Master-------
			
			$scope.SaveShiftMAster = function(totalHrs,form)
			{
				//$scope.submitted=true;
				//alert($scope.ShiftMasterForm.$valid);
				if($scope.ShiftMasterForm.$valid)
				{
					ShiftDataObj = [];
					TotalHrsObj.totalHrs = totalHrs;
				    $scope.data.forEach(function (ShiftData) 
				    {
						//alert(ShiftData);
   		    	       ShiftDataObj.push(angular.toJson(ShiftData));
					   
    		    	});

					ShiftObj.gridData = ShiftDataObj;
					ShiftObj.formData = TotalHrsObj;
		
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/SaveShiftMaster.html",
						    processData:true,
						    type:'POST',
					    	contentType:'Application/json',
						    data:JSON.stringify(ShiftObj),						
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
		    			    },
					    	success:function(response) 
							{				
								if(response==true)
								{
									swal("Success!", 'Shift Added Successfully!', "success");
									 $('.btn-hide').attr('disabled',false);
									 $scope.loadAllAddedShifts();									 
									//location.reload();
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						 });				
				}
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					
				}
			};
			//-------Load All Added Shifts--------
			$scope.loadAllAddedShifts = function()
			{
				   var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllShifts.html",
							data:{},
						  }).success(function(data, status) 
						  {
								if(data.Shifts!='')
								{
									$scope.data = data.Shifts;
									$scope.totalHrs = data.Shifts[0].totalHrs;
									//alert(data.Shifts[0].totalHrs);
									$index = data.Shifts.length;
								}
						  }).error(function(data,status)
						  {
							  //alert(data);
						  });			  								
			};
			
			//-----------Shift Validation------
			
			$scope.setValidation = function(id)
		  	{
					//CalculateShiftHrs();
					var prvId="";
					if(id>1)
				  	{
						 prvId = Number(id) - Number(1); 
						  
				  		 var currentVal =  $('#from'+ id).val();
						 var startTimeArray = currentVal.split(":");
    	        	   	 var startInputHrs = parseInt(startTimeArray[0]);
        	        	 var startInputMins = parseInt(startTimeArray[1]);
						 var currentValstartMin = startInputHrs*60 + startInputMins;
						 //alert("Current Value---------------"+currentValstartMin);
						 var PrevVal =  $('#to'+prvId).val();
						 
						 
						 var pstartTimeArray = PrevVal.split(":");
    	        	   	 var pstartInputHrs = parseInt(pstartTimeArray[0]);
        	        	 var pstartInputMins = parseInt(pstartTimeArray[1]);
						 var pcurrentValstartMin = pstartInputHrs*60 + pstartInputMins;
						// alert("Current Value---------------"+currentValstartMin);
						// alert("Previous Value-------------------"+pcurrentValstartMin);
				  		
						 
						
						 if(currentValstartMin < pcurrentValstartMin) 
				  		 { 
							//alert("Invalid");
							//$('#saveButton').attr("disabled", true);
							$scope.ShiftMasterForm.$invalid = true;
							$('#error'+ id).show();
				  		 } 
					    else 
				  	     {	 
				  			$('#error'+ id).hide();
							$('#errorto'+ PrvId).hide();
							//$('#saveButton').attr("disabled", false);
							$scope.ShiftMasterForm.$invalid = false;
				  	     }					  
			 		}
					//CalculateShiftHrs();
		  	};			
			
			//--------Shift Name Validation------
			$scope.setValidationForName = function(shiftId)
			{
				var PreId = Number(shiftId)-Number(1);
				var currentName = $('#name'+ shiftId).val();
				var prevName = $('#name'+ PreId).val();
				
				if(shiftId>1)
				{
					if(currentName==prevName)
					{
						$('#errorName'+ shiftId).show();
						//$('#saveButton').attr("disabled", true);
						$scope.ShiftMasterForm.$invalid = true;
					}
				   else
				    {
						$('#errorName'+ shiftId).hide();
						//$('#saveButton').attr("disabled", false);						
						$scope.ShiftMasterForm.$invalid = false;
					}
				
				}
				 //CalculateShiftHrs();
			};
	  })
	 
		//============================================
	 		//Cane Accounting Rules
		//============================================
	  	  
		.controller('CaneAccountingRules',function($scope,$http)
		{

			var CaneAccountingRuleObj = new Object();
			var filterObj = new Object();

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Season";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			$scope.AddedCaneAccount = {'isTAApplicable':'0','isPercentOrAmt':'0','modifyFlag':'No'};

			$scope.isEdit = true;
			$scope.master = {};
			
			var obj = new Object();
			obj.tablename = "Village";
			var jsonTable= JSON.stringify(obj);
			
			var objectOne = new Object();
			var objectTwo = new Object();
			var objectThree = new Object();
			var objectFour = new Object();
			var objectFive = new Object();
			var objectFormdata = new Object();
		 
			var caneAccountingArray = [];
			var finalCaneObject = new Object();
			var filterSeasonObj = new Object();
			
			$scope.setRowAfterReset = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxId.html",
				data: jsonTable,
				   }).success(function(data, status) 
				{
				$scope.AddedCaneAccount = {'isTAApplicable':'0','isPercentOrAmt':'0'};	
				});	
			};
			
			
			$scope.decimalpoint = function(b)
			{
				var decmalPoint = $scope.AddedCaneAccount[b];				
				var getdecimal = parseFloat(decmalPoint).toFixed(3);
				$scope.AddedCaneAccount[b] = getdecimal;
				if(getdecimal=="NaN")
				{
					$scope.AddedCaneAccount[b]="";
				}
			};			
			//==========Season Names=========
			$scope.loadSeasonNames = function()
			{
				$scope.seasonNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						$scope.seasonNames=data;
				
				});  	   
		  };
		  
		  //===========Season Onchange Added Dates==============
		  $scope.AddedDatesBySeason = function()
		  {
			  filterSeasonObj = $scope.AddedCaneAccountFilter.season;
				 var httpRequest = $http({
					  method: 'POST',
				  url : "/SugarERP/getDatesFromCaneAccRules.html",
				  data: JSON.stringify(filterSeasonObj),
				   }).success(function(data, status) 
				{
				//alert(JSON.stringify(data));
				$scope.calcruledate=data;
				});	
		  };
		  //------Calculations----------
		  /*$scope.getCanePrice = function()
		  {
			  var frp = $scope.AddedCaneAccount.frp;
			  var icp = $scope.AddedCaneAccount.icp;
			  var acp = $scope.AddedCaneAccount.additionalCanePrice;
			  
			  if(frp==null) { frp = 0; }
			  if(icp==null) { icp = 0; }
			  if(acp==null) { acp = 0; }
			  
			  var netCane = Number(frp)+Number(icp)+Number(acp);
			  $scope.AddedCaneAccount.netCanePrice=netCane;
		  };*/
		  $scope.getTotalFRP = function()
		  {
			//alert("STATUS"+status);
			var frp = $scope.AddedCaneAccount.frp;
			var frpSubsidy = $scope.AddedCaneAccount.frpSubsidy;
			
		    if(frp==null) { frp = 0; }
			if(frpSubsidy==null) { frpSubsidy = 0; }
			
			var netFRP = Number(frp)-Number(frpSubsidy);
			$scope.AddedCaneAccount.netFrp = parseFloat(netFRP).toFixed(3);
			
			
			 $scope.AddedCaneAccount.totalPrice = $scope.AddedCaneAccount.netFrp;
			 //alert("total price"+$scope.AddedCaneAccount.totalPrice);
			 $scope.AddedCaneAccount.postSeason =  $scope.AddedCaneAccount.totalPrice;
			 
			 if(status==1)
			 {
				 
			 }
			 
			// alert("postSeason price"+$scope.AddedCaneAccount.postSeason);
		  };
		  
		  
		  //alert(JSON.stringify($scope.AddedCaneAccount));
		  
		 // $scope.removePurchaseTax = function(tickStatus,value)
//		  {
//			  
//			  	 var postSeason =  $scope.AddedCaneAccount.postSeason;
//				 var getVal = 0;
//				 if(value == "" || value==null){getVal = 0;}else{ getVal = value;}
//				 if(!tickStatus)
//				 {
//					alert(getVal +" if ");
//					$scope.AddedCaneAccount.postSeason = Number(postSeason)-Number(getVal);
//				 }
//				 else
//				 {
//					 alert(getVal + " else ");
//					 $scope.AddedCaneAccount.postSeason = Number(postSeason)+Number(getVal);
//				 }
//			  /*if(value==undefined)
//				 {
//					 
//					 getVal = 0;
//					 if(tickStatus==0 || tickStatus==undefined)
//					 {
//						 alert(tickStatus + " 1 ");
//						 $scope.AddedCaneAccount.postSeason = Number(postSeason)+Number(getVal);
//					 }
//					 else
//					 {
//						 alert(tickStatus + " 2 ");
//						 $scope.AddedCaneAccount.postSeason = Number(postSeason)-Number(getVal);
//					 }
//				 }
//				 else
//				 {
//					 alert(value + " else ");
//					 getVal = value;
//					 if(tickStatus==0 || tickStatus==undefined)
//					 {
//						 alert(tickStatus + " 4 ");
//						 $scope.AddedCaneAccount.postSeason = Number(postSeason)-Number(getVal);
//					 }
//					 else
//					 {
//						 alert(tickStatus + " 5");
//						 $scope.AddedCaneAccount.postSeason = Number(postSeason)+Number(getVal);
//					 }
//					  //$scope.AddedCaneAccount.postSeason = Number(postSeason)-Number(getVal);
//				 }*/
//			  /*if(tickStatus==0 || tickStatus==undefined)
//			  {
//				  alert("minus avvaali");
//				 alert(getVal);
//				$scope.AddedCaneAccount.postSeason = Number(postSeason)-Number(getVal);
//			  }
//			  else if(tickStatus==1)
//			  {
//				  alert("add avvaali");
//				  alert(postSeason);
//				   alert(getVal);
//				     $scope.AddedCaneAccount.postSeason = Number(postSeason)+Number(getVal);
//				
//			  }*/
//		  };
		  
		  $scope.removePurchaseTax = function(status,Value,afterStatus)
		  {
			//alert(status);
			 //alert("accrued and Due"+$scope.AddedCaneAccount.postSeason);
			
				
			  if(status==0 || status==undefined)
			  {
				 
				  var postSeason = parseFloat($scope.AddedCaneAccount.postSeason)-parseFloat(Number(afterStatus));
			$scope.AddedCaneAccount.postSeason =parseFloat(postSeason).toFixed(2)
			$scope.righptaxStatus = true;
			$scope.beforefield = false;
				
			  }
			    else if(status==1 )
			  { 
				   var postSeason = parseFloat($scope.AddedCaneAccount.postSeason)+parseFloat(Number(afterStatus));
					$scope.AddedCaneAccount.postSeason =parseFloat(postSeason).toFixed(2)
					$scope.righptaxStatus = false;
					$scope.beforefield = true;
			  }
		};
		
		
		
		//
		 $scope.removePurchaseTax1 = function(ptaxStatus,Value,status)
		  {
			
			  $scope.AddedCaneAccount.postSeason;
			//alert("accrued and due"+$scope.AddedCaneAccount.postSeason);
		var totalPrice  = $scope.AddedCaneAccount.totalPrice;
			var butnotDue = $scope.AddedCaneAccount.seasonPrice;
			if(butnotDue==undefined || butnotDue==null)
			{
				butnotDue=0;
			}
			var asd  = Number(totalPrice)-Number(butnotDue);
			  if(ptaxStatus==1 )
			  {
				  var postSeason = parseFloat(asd)-parseFloat(Number(Value));
				//alert(postSeason);
				  if(postSeason<0)
				  {
					  postSeason=0;
				  }
			$scope.AddedCaneAccount.postSeason =parseFloat(postSeason).toFixed(2);
			  //alert("postSeason"+postSeason);
			$scope.checkStatus = true;
				
			  }
			  
			   
		};
		//$scope.AlreadytickFunction =  function(status,Status,Value)
//		{
//			alert(status);
//			
//			if(status==0 || status==undefined)
//			{
//				//alert($scope.AddedCaneAccount.postSeason);
////				alert(Value);
//				//var postSeason = parseFloat($scope.AddedCaneAccount.postSeason)+parseFloat(Number(Value));
//				//$scope.AddedCaneAccount.postSeason = postSeason;
//			}
//			if(status==1)
//			{
//				
//				//var postSeason = parseFloat($scope.AddedCaneAccount.postSeason)-parseFloat(Number(Value));
//				//$scope.AddedCaneAccount.postSeason = postSeason;
//			}
//		};
		  
		  $scope.getTotalPrice = function(status,Value,ptaxCheckStatus)
		  {
			//alert(ptaxCheckStatus);
			
			//if(ptaxCheckStatus==1)
			//{
				//$scope.AddedCaneAccount.postSeason  = $scope.AddedCaneAccount.postSeason;
				//alert($scope.AddedCaneAccount.postSeason);
			//}
			//alert(ptaxCheckStatus);
			  var netFrp = $scope.AddedCaneAccount.netFrp;
			  var harvestingSub = $scope.AddedCaneAccount.harvestingSub;
			  var purchaseTax = $scope.AddedCaneAccount.purchaseTax;
			  var additionalPrice = $scope.AddedCaneAccount.additionalPrice;
			  var tansportSub = $scope.AddedCaneAccount.tansportSub;
			  
			  if(netFrp==null) { netFrp = 0; }
			  if(harvestingSub==null) { harvestingSub = 0; }
			  if(purchaseTax==null) { purchaseTax = 0; }
			  if(additionalPrice==null) { additionalPrice = 0; }
			  if(tansportSub==null) { tansportSub = 0; }
			  
			  
			  
				  var TotalCanePrice = Number(netFrp)+Number(harvestingSub)+Number(purchaseTax)+Number(additionalPrice)+Number(tansportSub);
			 	 $scope.AddedCaneAccount.totalPrice = parseFloat(TotalCanePrice).toFixed(3); 
			 
			//alert(TotalCanePrice);
			  
			  
			 // alert(status);
			  if(status==1) 
			  {
				 
				  var isFrpSeaon = $scope.AddedCaneAccount.isFrpSeaon;
				  var isHarvestingSeaon = $scope.AddedCaneAccount.isHarvestingSeaon;
				  var isIcpSeaon = $scope.AddedCaneAccount.isIcpSeaon;
				  var isAdditionalSeaon = $scope.AddedCaneAccount.isAdditionalSeaon;
				  var isTransportSeaon = $scope.AddedCaneAccount.isTransportSeaon;
				  
				  if(isFrpSeaon==0) { netFrp = $scope.AddedCaneAccount.netFrp;} else { netFrp = 0; }
				  if(isHarvestingSeaon==0) { harvestingSub = $scope.AddedCaneAccount.harvestingSub;} else { harvestingSub = 0; }
				  if(isIcpSeaon==0) { purchaseTax = $scope.AddedCaneAccount.purchaseTax;} else { purchaseTax = 0; }
				  if(isAdditionalSeaon==0) { additionalPrice = $scope.AddedCaneAccount.additionalPrice;} else { additionalPrice = 0; }
				  if(isTransportSeaon==0) { tansportSub = $scope.AddedCaneAccount.tansportSub;} else { tansportSub = 0; }

			  var totalSeasonPrice = Number(netFrp)+Number(harvestingSub)+Number(purchaseTax)+Number(additionalPrice)+Number(tansportSub);
		

        $scope.AddedCaneAccount.seasonPrice = $scope.AddedCaneAccount.totalPrice- $scope.AddedCaneAccount.postSeason;
		
		
			  }
			  
			  else
			  {
				 
				 
				  if($scope.AddedCaneAccount.seasonPrice==null)
				  { 
				  var totalPricediv = "0";
				  } 
				  else 
				  { 
				  var totalPricediv = $scope.AddedCaneAccount.seasonPrice;
				  }
				
				 $scope.AddedCaneAccount.postSeason = parseFloat(Number($scope.AddedCaneAccount.totalPrice)-Number(totalPricediv)).toFixed(3);
				
				 
				  //$scope.removePurchaseTax1(ptaxCheckStatus,Value,status);
				 //$scope.removePurchaseTax(status,Value);
				
				// $scope.removePurchaseTax1(ptaxCheckStatus,Value,status);
			  }
		  };
		  $scope.CheckLoanWithPercent = function(chkboxstatus)
		  {
			  //alert(chkboxstatus);
			  if(chkboxstatus==0)
			  {
				  if($scope.AddedCaneAccount.percentForDed<=100)
				  {
				  $scope.AddedCaneAccount.loanRecovery = 100-Number($scope.AddedCaneAccount.percentForDed);
				  }
				  else
				  {
					  $scope.AddedCaneAccount.percentForDed = 0;
					  $scope.AddedCaneAccount.loanRecovery = 0;
					  
				  }
			  }
		  };
		  $scope.clearValues = function()
		  {
			 $scope.AddedCaneAccount.percentForDed ="";
			 $scope.AddedCaneAccount.loanRecovery ="";
			 $Sscope.AddedCaneAccount.amtForDed ="";
			  $Sscope.AddedCaneAccount.amtForLoans ="";
			
		  };
		  $scope.chkAmountForLoanDeductions = function(chkboxstatus)
		  {
			  
			  if(chkboxstatus==1)
			  {
				  $scope.AddedCaneAccount.amtForLoans = Number($scope.AddedCaneAccount.seasonPrice) - Number($scope.AddedCaneAccount.amtForDed);
			  }
		  };
		  
		  $scope.getSeaonCanePrice = function(status,curValue)
		  {
			 //alert("Current Value"+curValue);
			
			  var seasonPrice = $scope.AddedCaneAccount.seasonPrice;
			  var postSeasonPrice="";
			  if(curValue==null || curValue=="") { curValue = 0; }
			  
			  if(seasonPrice==null || seasonPrice=="" ) {  seasonPrice= 0; } else { seasonPrice=seasonPrice; }			  
			  
			  if(status==1) 
			  { 
			  
			  
			  
			  		seasonPrice = Number(seasonPrice)+Number(curValue);
					
					
					 //alert("Acrued But not Due"+seasonPrice);
					
					
			  } 
			  else
			  {
				  seasonPrice = Number(seasonPrice)-Number(curValue);
				 
			  }
			  $scope.AddedCaneAccount.seasonPrice = parseFloat(seasonPrice).toFixed(3);
			  
			    
			  
			  var postSeasonPrice = Number($scope.AddedCaneAccount.totalPrice)-Number($scope.AddedCaneAccount.seasonPrice);
			 
			
			  $scope.AddedCaneAccount.postSeason = parseFloat(Math.abs(postSeasonPrice)).toFixed(3);
			
			
		  };
		  
		  $scope.getPostSeasonAmt = function()
		  {
			  var netCane = $scope.AddedCaneAccount.netCanePrice;
			  //alert("netCane---------------"+netCane);
			  var priceForSeason = $scope.AddedCaneAccount.seasonCanePrice;
			  // alert("priceForSeason---------------"+priceForSeason);
			  var netCaneAmt = Number(netCane);
			  var priceForSeasonAmt = Number(priceForSeason);
			  var postSeasonPrice = netCaneAmt-priceForSeasonAmt;
			 // alert(postSeasonPrice);
			  $scope.AddedCaneAccount.postSeasonCanePrice = Math.abs(postSeasonPrice);
			 
		  };
		 //----------Save CaneAccounting-------- 
		 

		 $scope.CaneAccountingSubmit = function(AddedCaneAccount,form)
		 {
				if($scope.CaneAccountingRuleSeasonForm.$valid) 
				{
			 
			    caneAccountingArray = [];
				
			   if(AddedCaneAccount.isFrpTax!=null) { var taxOne = AddedCaneAccount.isFrpTax; } else { var taxOne = "0"; }
			   if(AddedCaneAccount.isHarTax!=null) { var taxTwo = AddedCaneAccount.isHarTax; } else { var taxTwo = "0"; }
			   if(AddedCaneAccount.isIcpTax!=null) { var taxThree = AddedCaneAccount.isIcpTax; } else { var taxThree = "0"; }
			   if(AddedCaneAccount.isAdditionalTax!=null) { var taxFour = AddedCaneAccount.isAdditionalTax; } else { var taxFour = "0"; }
			   if(AddedCaneAccount.isTransportTax!=null) { var taxFive = AddedCaneAccount.isTransportTax; } else { var taxFive = "0"; }

			   if(AddedCaneAccount.isFrpSeaon!=null) { var seasonOne = AddedCaneAccount.isFrpSeaon; } else { var seasonOne = "0"; }
			   if(AddedCaneAccount.isHarvestingSeaon!=null) { var seasonTwo = AddedCaneAccount.isHarvestingSeaon; } else { var seasonTwo = "0"; }
			   if(AddedCaneAccount.isIcpSeaon!=null) { var seasonThree = AddedCaneAccount.isIcpSeaon; } else { var seasonThree = "0"; }
			   if(AddedCaneAccount.isAdditionalSeaon!=null) { var seasonFour = AddedCaneAccount.isAdditionalSeaon; } else { var seasonFour = "0"; }
			   if(AddedCaneAccount.isTransportSeaon!=null) { var seasonFive = AddedCaneAccount.isTransportSeaon; } else { var seasonFive = "0"; }

               if(AddedCaneAccount.calcid1!=null) { var calc = AddedCaneAccount.calcid1; } else { var calc = 0; }
			   if(AddedCaneAccount.calcid2!=null) { var calc1 = AddedCaneAccount.calcid2; } else { var calc1 = 0; }
			   if(AddedCaneAccount.calcid3!=null) { var calc2 = AddedCaneAccount.calcid3; } else { var calc2 = 0; }
			   if(AddedCaneAccount.calcid4!=null) { var calc3 = AddedCaneAccount.calcid4; } else { var calc3 = 0; }
			   if(AddedCaneAccount.calcid5!=null) { var calc4 = AddedCaneAccount.calcid5; } else { var calc4 = 0; }

				objectOne.isThisPurchaseTax = taxOne;
				objectOne.priceName = "FRP";
				objectOne.amount = AddedCaneAccount.frp;
				objectOne.subsidy = AddedCaneAccount.frpSubsidy;
				objectOne.netAmt = AddedCaneAccount.netFrp;
				objectOne.considerForSeasonActs = seasonOne;
				
				
				objectOne.calcid =calc;
			


				objectTwo.isThisPurchaseTax = taxTwo;
				objectTwo.priceName = "Harvesting Subsidy";
				objectTwo.amount = AddedCaneAccount.harvestingSub;
				objectTwo.subsidy = "0.00";
				objectTwo.netAmt = "0.00";
				objectTwo.considerForSeasonActs = seasonTwo;
					objectTwo.calcid = calc1;
				

				objectThree.isThisPurchaseTax = taxThree;
				objectThree.priceName = "Purchase Tax / ICP";
				objectThree.amount = AddedCaneAccount.purchaseTax;
				objectThree.subsidy = "0.00";
				objectThree.netAmt = "0.00";
				objectThree.considerForSeasonActs = seasonThree;
				objectThree.calcid = calc2;
				

				objectFour.isThisPurchaseTax = taxFour;
				objectFour.priceName = "Additional Cane Price";
				objectFour.amount = AddedCaneAccount.additionalPrice;
				objectFour.subsidy = "0.00";
				objectFour.netAmt = "0.00";
				objectFour.considerForSeasonActs = seasonFour;
				objectFour.calcid = calc3;
				

				objectFive.isThisPurchaseTax = taxFive;
				objectFive.priceName = "Transport Subsidy";
				objectFive.amount = AddedCaneAccount.tansportSub;
				objectFive.subsidy = "0.00";
				objectFive.netAmt = "0.00";
				objectFive.considerForSeasonActs = seasonFive;
				objectFive.calcid = calc4;

				caneAccountingArray.push(objectOne);
				caneAccountingArray.push(objectTwo);
				caneAccountingArray.push(objectThree);
				caneAccountingArray.push(objectFour);
				caneAccountingArray.push(objectFive);
				
				if($scope.AddedCaneAccount.isPercentOrAmt==0) 
				{ 
					var dedPercent = $scope.AddedCaneAccount.percentForDed;
					var dedLoans = $scope.AddedCaneAccount.loanRecovery;
					var dedAmount = "0.00";
					var dedLoan = "0.00";
				}
			   else
			    {
					var dedAmount = $scope.AddedCaneAccount.amtForDed;
					var dedLoanInAmt = $scope.AddedCaneAccount.amtForLoans;										
					var dedPercent = "0";
					var dedLoans = "0";										
				}
				
				if($scope.AddedCaneAccount.isTAApplicable==0) 
				{ 
					var noOfKms = $scope.AddedCaneAccount.noOfKms;
					var allowancePerKmPerTon = $scope.AddedCaneAccount.allowancePerKmPerTon;
				}
			   else
			    {
					var noOfKms = "0";
					var allowancePerKmPerTon = "0";
				}
				
				
				
				objectFormdata.calcid = "";
				objectFormdata.season = $scope.AddedCaneAccountFilter.season;
				objectFormdata.caneaccruleDate = $scope.AddedCaneAccountFilter.date;
				objectFormdata.netCanePrice = $scope.AddedCaneAccount.totalPrice;
				objectFormdata.seasonCanePrice = $scope.AddedCaneAccount.seasonPrice;
				objectFormdata.postSeasonCanePrice = $scope.AddedCaneAccount.postSeason;
				objectFormdata.isPercentOrAmt = $scope.AddedCaneAccount.isPercentOrAmt;
				objectFormdata.amtForDed = dedAmount;
				objectFormdata.amtForLoans = dedLoanInAmt;
				objectFormdata.percentForLoans = dedPercent;
				objectFormdata.percentForDed = dedLoans;
				objectFormdata.loanRecovery = $scope.AddedCaneAccount.loanRecovery;
				objectFormdata.isTAApplicable = $scope.AddedCaneAccount.isTAApplicable;
				objectFormdata.noOfKms = noOfKms;
				objectFormdata.allowancePerKmPerTon = allowancePerKmPerTon;
				objectFormdata.modifyFlag = $scope.AddedCaneAccount.modifyFlag;
				
				
				finalCaneObject.gridData = caneAccountingArray;
				finalCaneObject.formData = objectFormdata;
				
				//caneAccountingArray.push(objectFormdata);
				//alert(JSON.stringify(finalCaneObject));
				//$scope.Addsubmitted = true;
				//$scope.Filtersubmitted = true;
				//alert(JSON.stringify(finalCaneObject));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveCaneAccountingRulesFroSeason.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(finalCaneObject),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", "Cane Accounting Rules Added Successfully!", "success");	
									$('.btn-hide').attr('disabled',false);
									$scope.AddedCaneAccount = angular.copy($scope.master);
									$scope.AddedCaneAccountFilter = angular.copy($scope.master);
									$scope.CaneAccountingFilterForm = angular.copy($scope.master);
									$scope.setRowAfterReset();
										form.$setPristine(true);	
									
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};

			//--------------Load Cane Accounting Filter----------------
      		$scope.loadCaneAccountDataByFilter = function(AddedCaneAccount,form) 
		    {
				//$scope.Filtersubmitted = true;
				//alert(AddedCaneAccount.season);
				//alert(AddedCaneAccount.date);
				if(AddedCaneAccount.season!='' && AddedCaneAccount.addedDate!='') 
				{ 
					//alert('in');
					filterObj.season = $scope.AddedCaneAccountFilter.season;
					filterObj.date = $scope.AddedCaneAccountFilter.addedDate;
					
					
					//alert(JSON.stringify(filterObj));

					 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/getCaneAccountingRules.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{			
							//alert(JSON.stringify(data));
							console.log(data);
						$scope.AddedCaneAccount = {'totalPrice':data.rules.netCanePrice,'seasonPrice':data.rules.seasonCanePrice,'postSeason':data.rules.postSeasonCanePrice,'isPercentOrAmt':data.rules.isPercentOrAmt,'percentForDed':data.rules.percentForLoans,'loanRecovery':data.rules.percentForDed,'amtForLoans':data.rules.amtForLoans,'amtForDed':data.rules.amtForDed,'isTAApplicable':data.rules.isTAApplicable,'noOfKms':data.rules.noOfKms,'allowancePerKmPerTon':data.rules.allowancePerKmPerTon,'season':data.rules.season,'caneaccruleDate':data.rules.caneaccruleDate,'frp':data.accounts[0].amount,'frpSubsidy':data.accounts[0].subsidy,'netFrp':data.accounts[0].netAmt,'isFrpSeaon':data.accounts[0].considerForSeasonActs,'harvestingSub':data.accounts[1].amount,'calcid1':data.accounts[0].calcid,'calcid2':data.accounts[1].calcid,'calcid3':data.accounts[2].calcid,'calcid4':data.accounts[3].calcid,'calcid5':data.accounts[4].calcid,'purchaseTax':data.accounts[2].amount,'additionalPrice':data.accounts[3].amount,'tansportSub':data.accounts[4].amount,'isFrpTax':data.accounts[0].isThisPurchaseTax,'isHarTax':data.accounts[1].isThisPurchaseTax,'isIcpTax':data.accounts[2].isThisPurchaseTax,'isAdditionalTax':data.accounts[3].isThisPurchaseTax,'isTransportTax':data.accounts[4].isThisPurchaseTax,'isFrpSeaon':data.accounts[0].considerForSeasonActs,'isHarvestingSeaon':data.accounts[1].considerForSeasonActs,'isIcpSeaon':data.accounts[2].considerForSeasonActs,'isAdditionalSeaon':data.accounts[3].considerForSeasonActs,'isTransportSeaon':data.accounts[4].considerForSeasonActs,'modifyFlag':data.rules.modifyFlag};	
						$scope.AddedCaneAccountFilter = {'date':data.rules.caneaccruleDate,'season':data.rules.season};
							
							
							
							//'isFrpTax':data.rules.isThisPurchaseTax,'frp':data.rules.amount,'frpSubsidy':data.rules.subsidy,'netFrp':data.rules.netAmt,'isFrpSeaon':data.rules.considerForSeasonActs,'isHarTax':data.rules.isThisPurchaseTax,'harvestingSub':data.rules.amount,'isHarvestingSeaon':data.rules.considerForSeasonActs,'isIcpTax':data.rules.isThisPurchaseTax,'purchaseTax':data.rules.amount,'isIcpSeaon':'','isAdditionalTax':'','additionalPrice':'','isAdditionalSeaon':'','isTransportTax':'','tansportSub':'','isTransportSeaon':'',
							
							//$scope.AddedCaneAccount=data.caneAccountingRules;														
						});											 										
				} 
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}										
				}
        		
	        };
			//---------Reset Form----
		    $scope.reset = function(form)
			{	   
				$scope.AddedCaneAccount = angular.copy($scope.master);
				$scope.AddedCaneAccountFilter = angular.copy($scope.master);
				$scope.CaneAccountingFilterForm = angular.copy($scope.master);
				$scope.setRowAfterReset();
					form.$setPristine(true);	     	
   
			};
			
		})
	  
	   //===========================================
	 		//Company Advance Master
	  //===========================================
	  
	  
	 .controller('CompanyAdvanceMaster', function($scope,$http)
	  {
		  
		 
		  		$scope.data = []; 
				$scope.Seedlingdata = [];
				
			 	var gridData = [];
				var gridData1 = [];
		 		var obj = new Object();
			 	$index=0;
				$indexSeed=0;
				
				var DropDownLoad = new Object();
				DropDownLoad.tablename = "CompanyAdvance";
				DropDownLoad.columnName = "advancecode";
				DropDownLoad.columnName1 = "advance";
				var jsonStringDropDown= JSON.stringify(DropDownLoad);	
				
				obj.tablename = "CompanyAdvance";
				var jsonTable= JSON.stringify(obj);
				$scope.isEdit = true;
				$scope.requiredstatus = true;
				$scope.interestvalid = true;
				
				$scope.decimalpointgrid= function(b,id)
			    {
				
					var decmalPoint=$scope.data[id][b];
					var getdecimal=parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				    $scope.data[id][b]=getdecimal;
					 if(getdecimal=="NaN")
					{
						 $scope.data[id][b]="";
					}
				};	
				
					$scope.decimalpointseed= function(b,id)
			    {
				
					var decmalPointseed=$scope.Seedlingdata[id][b];
					var getdecimalseed=parseFloat(Math.round(decmalPointseed * 100) / 100).toFixed(2);
				    $scope.Seedlingdata[id][b]=getdecimalseed;
					 if(getdecimalseed=="NaN")
					{
						 $scope.Seedlingdata[id][b]="";
					}
				};	

				
				//---------Validtae Duplicate Entrys----
		 	$scope.validateDup = function(advanceCode)
		 	{
				for(var d =0;d<JSON.stringify($scope.advanceNames.length);d++)
				{
					var firstString = $scope.AddedCompanyAdvance.advance.toUpperCase();
					var secondString = $scope.advanceNames[d].advance.toUpperCase();
					if(firstString!=null)
					{
						if(advanceCode!=d+1)
						{
							if(firstString==secondString)
							{
								$('.btn-hide').attr('disabled',true);						
								$(".duplicate").show();
								return false;
							}					
						   else
						    {
								$(".duplicate").hide();
								$('.btn-hide').attr('disabled',false);						
							}
						}
					}
				}
				
		  };				
		 //=============Amounts in Decimals ==================
		$scope.decimalpoint = function(b)
		{
			var decmalPoint = $scope.AddedCompanyAdvance[b];			
			var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
			$scope.AddedCompanyAdvance[b] = getdecimal;								
		};
			
		//=============Space Between Two Words ==================
		$scope.spacebtw = function(b)
		{
			var twoSpace = $scope.AddedCompanyAdvance[b];
			twoSpace = twoSpace.replace(/ {2,}/g,' ');
			twoSpace=twoSpace.trim();
			$scope.AddedCompanyAdvance[b] = twoSpace;				
		};			
		     
		//============Get MAx id on Page onload=========
		$scope.loadCompanyAdvanceCode = function()
		{	
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			   		}).success(function(data, status) 
					{
						$scope.AddedCompanyAdvance={'advanceCode':data.id,'status':'0','advanceMode':'0','advanceFor':'0','isSeedlingAdvance':'0','subsidyPart':'0','interestApplicable':'0','isAmount':'0', 'modifyFlag':'No','onloadIsKindorAmt':'5'};
						if($scope.AddedCompanyAdvance.isAmount=='0') { $scope.placeholder='Percentage (%)'; } else { $scope.placeholder='Amount'; }
					
						
		        	});	   	   
		};
			$scope.loadAdvanceNames = function()
			{
				//$scope.advanceNames=[];
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadAddedDropDownsForMasters.html",
							data: jsonStringDropDown,
						 }).success(function(data, status) 
						 {
							 //alert(JSON.stringify(data));
							$scope.advanceNames=data;
						 }); 
			};
			
			$scope.ChangePlaceholder = function(fname)
			{
				if(fname=='0') { $scope.placeholder='Percentage (%)'; } else { $scope.placeholder='Amount'; }
			};
			//requiredstatus
			$scope.ChangeRequiredStatus = function(status)
			{
					if(status=='0') { $scope.requiredstatus = true; } else { $scope.requiredstatus = false; }
			};
			
			$scope.tillDateHide = function(dateValid)
			{
				//alert(dateValid)
					if(dateValid=='0') { $scope.interestvalid = true; } else { $scope.interestvalid = false; }
			};
			$scope.seedlingValid = function(seedlingRowValidation)
			{
			 	if(seedlingRowValidation=='0') { $scope.SeedlingAmtRow = true; } else { $scope.SeedlingAmtRow = false; }
			};
			
			
			$scope.CompanyAdvanceSubmit = function(AddedCompanyAdvance,form)
			{


			 // alert( angular.toJson(AddedCompanyAdvance));
			  //$scope.submitted = true;
			  //$scope.Addsubmitted = true;

				if($scope.AdvanceMasterForm.$valid) 
				{	
					gridData = [];
			 		 $scope.data.forEach(function (CompanyAdvancesData) 
					  {
		    		      gridData.push(angular.toJson(CompanyAdvancesData));														 
				      });
					 gridData1 = [];
			 		 $scope.Seedlingdata.forEach(function (CompanyAdvancesSeedlingData) 
					  {
		    		      gridData1.push(angular.toJson(CompanyAdvancesSeedlingData));														 
				      });
					  obj.gridData = gridData;
					   obj.gridData1 = gridData1;
					  obj.formData = angular.toJson(AddedCompanyAdvance);
					  delete obj["$index"];
					   delete obj["$indexSeed"];
				
				//alert(JSON.stringify(obj));
				    $('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveCompanyMaster.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Company Advance Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								$scope.AddedCompanyAdvance = angular.copy($scope.master);
								$scope.advance = angular.copy($scope.master);
								$scope.loadCompanyAdvanceCode();
								$index=1;
								$indexSeed=1;
								$scope.data = [{'id':$index}]
								$scope.Seedlingdata = [{'id':$indexSeed}]
								form.$setPristine(true);								
								//location.reload();
							}
							else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
								$('.btn-hide').attr('disabled',false);
							}
						}
					});	
				}
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
			 };
			
			 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedCompanyAdvance = angular.copy($scope.master);
				$scope.advance = angular.copy($scope.master);
				$scope.loadCompanyAdvanceCode();
				$index=1;
				$indexSeed=1;
				$scope.data = [{'id':$index}]
				$scope.Seedlingdata = [{'id':$indexSeed}]
			    form.$setPristine(true);			    								
			   
		   };
			
			
			$scope.addCompanyAdvanceAmount = function(CompanyAdvancesData) 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};
			$scope.addCompanySeedlingAdvanceAmount = function()
			{
				//alert($indexSeed);
				$indexSeed++;
		    	$scope.Seedlingdata.push({'id':$indexSeed})
				//alert($indexSeed);
			};
			
			
			 $scope.setValidation = function(id)
		  	{
				
			  var prvId="";
			  if(id>1)
			  {
				  prvId = Number(id) - Number(1);
				
				  var currentVal =  $('#from'+ id).val();
				  
				  var PrevVal =  $('#to'+prvId).val();
				   
				  var SameVal =  $('#to'+id).val();
			
				  if(currentVal<PrevVal || currentVal==SameVal || currentVal==PrevVal) 
				  { 
				  	$('#error'+ id).show();
					$('#saveButton').attr("disabled", true);
				  } 
				 else 
				  { 
				  	$('#error'+ id).hide();
					$('#errorto'+ PrvId).hide();
					$('#saveButton').attr("disabled", false);
				  }
			  }			  
		  };
		  
		  $scope.setSeedValidation = function(id)
		  	{
			  var prvIdseed="";
			  if(id>1)
			  {
				  prvIdseed = Number(id) - Number(1);
				 
				  var currentValseed =  $('#fromDist'+ id).val();
				  
				  var PrevValseed =  $('#toDist'+prvIdseed).val();
				   
				  var SameValseed =  $('#toDist'+id).val();
		
			
				  if(currentValseed<PrevValseed || currentValseed==SameValseed || currentValseed==PrevValseed) 
				  { 
				  	$('#errormessage'+ id).show();
					$('#saveButton').attr("disabled", true);
				  } 
				 else 
				  { 
			
				  	$('#errormessage'+ id).hide();
					$('#error'+ prvIdseed).hide();
					$('#saveButton').attr("disabled", false);
				  }
			  }			  
		  };
		  $scope.setoValidation = function(id)
		  {
			  	  
			  	  var nextId = Number(id) + Number(1);	
				  var currentVal =  $('#from'+ id).val();
				  var nextVal =  $('#from'+nextId).val();
				  var SameVal =  $('#to'+id).val();			  	
				 //if(id==1)
				  //{
					 if(Number(SameVal)== Number(currentVal))
					 {
						 	$('#errorto'+ id).show();
						 	$('#saveButton').attr("disabled", true);
					 }
					 else
					 {
						 $('#errorto'+ id).hide();
						 	$('#saveButton').attr("disabled", false);
					 }
				  //}				  
				  
				  if(id<=$index)
				  {
					  if(SameVal==currentVal || SameVal>nextVal)
					  {
						//$('#errorto'+ id).removeClass('Errorto');
					  	$('#error'+ nextId).show();
						$('#saveButton').attr("disabled", true);					  
					  }
					 else
					  {
						//$('#errorto'+ id).addClass('Errorto');  
					  	$('#error'+ id).hide();
						$('#saveButton').attr("disabled", false);					  					  
					  }
				  }
		  };
		  	$scope.setSeedtoValidation = function(id)
			  {
			  var nextId = Number(id) + Number(1);	
			  var currentVal =  $('#fromDist'+ id).val();
			  var nextVal =  $('#fromDist'+nextId).val();
			  var SameVal =  $('#toDist'+id).val();	   	
			 //if(id==1)
			  //{
			 if(Number(SameVal)== Number(currentVal))
			 {
				$('#errorto'+ id).show();
				$('#saveButton').attr("disabled", true);
			 }
			 else
			 {
			 $('#errorto'+ id).hide();
				$('#saveButton').attr("disabled", false);
			 }
			  //}	   
			  
			  if(id<=$indexSeed)
			  {
			  if(SameVal==currentVal || SameVal>nextVal)
			  {
			//$('#errorto'+ id).removeClass('Errorto');
				$('#error'+ nextId).show();
			$('#saveButton').attr("disabled", true);	   
			  }
			 else
			  {
			//$('#errorto'+ id).addClass('Errorto');  
				$('#error'+ id).hide();
			$('#saveButton').attr("disabled", false);	   	   
			  }
			  }
  		};
		  
		   $scope.removeRow = function(id)
		  {
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === id ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;	
				
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);	
				}				
		  };
		  $scope.removeSeedlingRow = function(id)
		  {
			  
			  var Index = -1;		
				var comArr = eval( $scope.Seedlingdata );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === id ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.Seedlingdata.splice( Index, 1 );
				$index = comArr.length;	
				
				for(var s=0;s<$index;s++)
				{
					$scope.Seedlingdata[s].id = Number(s)+Number(1);	
				}		
		  };
		  
		  //-------------Added Advance Names DropDown Onchange--------------------		 
		
				$scope.loadAddedAdvancessonChange = function(advance)
		{
			//alert(advance);
			if(advance!=null)
			{
				
				 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllCompanyAdvances.html",
						data: JSON.stringify(advance),
					   }).success(function(data, status) 
					   {	 
					   		//alert(JSON.stringify(data));
							$scope.AddedCompanyAdvance = data.companyadvance;
							$scope.AddedCompanyAdvance.onloadIsKindorAmt = data.companyadvance.isSeedlingAdvance;
							//$scope.AddedCompanyAdvance.onloadIsKindorAmt=15;
							$scope.data=data.rates;
							
							$scope.AddedCompanyAdvance.screenName ="Company Advance Master";
							$index = data.rates.length;
						
							if(data.rates.length==0)
							{
							
								$index = 1;
								$scope.data = [{'id':$index}]
							}
							$indexSeed = data.cost.length;
							//alert(data.cost.length);
							//alert(data.rates);
							//alert("datarates"+data.rates.length);
							
							
							
							if(data.cost=="")
							{
								$indexSeed = 1;
								
								$scope.Seedlingdata = [{'id':$indexSeed}]
							}
							else
							{
								$scope.Seedlingdata=data.cost;
							}
							//alert($indexSeed);
					   });											 			 
			}
		   else
		    {
				$scope.AddedCompanyAdvance = angular.copy($scope.master);
				$scope.advance = angular.copy($scope.master);
				$scope.loadCompanyAdvanceCode();
				$index=1;
				$indexSeed=1;
				$scope.data = [{'id':$index}]
				$scope.Seedlingdata = [{'id':$indexSeed}]
			    form.$setPristine(true);			    												
			}
		};
			
	  })

	//=============================================
			//Order Advances Begin
	//=============================================
	
	  .controller('orderAdvanceMaster',function($scope,$http)
		{
			var finalObj = [];		  
		  	var Advance = new Object();
			
			$scope.master = {};
			 var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var jsonStringDropDownSeason=JSON.stringify(DropDownLoad);
			
			$scope.loadseasonNames=function()
			{
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDropDownSeason,
					  }).success(function(data, status) 
					  {
						   $scope.seasonNames=data;
					  }); 
			};			
			
			//----------Get All Advances--------	
			  $scope.getAdvancesOrder = function(getSelectedDate)
			  {
				  //alert(90);
				  $scope.Addsubmitted = true;
				 //alert('getSelectedDate');
				   var getdate = getSelectedDate.season;
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getCompanyAdvancesForOrder.html",
							data: getdate,
					   }).success(function(data, status) 
						{
								//alert(JSON.stringify(data));
							$scope.AddOrderAdvancesData=data;
						});
			  };

			 //-----Save Advances Order---------
			  $scope.SaveOrderAdvances = function(AddOrderAdvances,form)
			  {
				  //alert(90);
				 	$scope.Addsubmitted = true;
				  if($scope.orderAdvanceMaster.$valid)
				  {
					var opts = $('.form-control1')[0].options;
					var getDropDownValues = $.map(opts, function(elem) 
					{
					    return (elem.value || elem.text);
					});	
					
					for(var i=0;i<getDropDownValues.length;i++)
					{
						//obj = [];
						var obj = new Object();
						//var finalObject = new Object();
						var splitedValues = getDropDownValues[i].split("-");
					
						obj.advanceCode = splitedValues[0];
						obj.id = splitedValues[2];
						//obj.advanceName = splitedValues[1];
						obj.AdvanceOrder = Number(i)+Number(1);
						
						finalObj.push(obj);
					
					}
					$('.btn-hide').attr('disabled',true);
				  	$.ajax({
				   		url:"/SugarERP/saveOrderForCompanyAdvances.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:JSON.stringify(finalObj),
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{									
							if(response==true)
							{
		  						swal("Success", 'Advances Updated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);								
								$scope.AddOrderAdvancesData = angular.copy($scope.master);								
								$scope.AddOrderAdvances = angular.copy($scope.master);
								$scope.loadseasonNames();
								$scope.AddOrderAdvances.season={};
								$scope.getAdvancesOrder();
								form.$setPristine(true);

							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});	
				  }
				 else
				  {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					  
				  }
			  };
			  //---------Reset Form----
		   		$scope.reset = function(form)
			   {			  
					$scope.AddOrderAdvancesData = angular.copy($scope.master);					
					$scope.AddOrderAdvances = angular.copy($scope.master);				
					$scope.loadseasonNames();
					$scope.AddOrderAdvances.season={};
					$scope.AddOrderAdvances.inputDate={};
					$scope.getAdvancesOrder();
				    form.$setPristine(true);			    											   
			   };
			  
		})
	  	  
	  //========================================
	  		//Company Master
	  //========================================
	 .controller('CompanyMaster',function($scope,$http)
	 {

			$index=0;
			$scope.data = [];
			var maxid = new Object();
			var CompanyGridData = [];
			var CompanyDataObj = new Object();
			
			maxid.tablename ="CompanyDetails";
			var jsonTable= JSON.stringify(maxid);
			
			var DropCountryDownLoad = new Object();
			DropCountryDownLoad.dropdownname ="Country";
			var jsonStringDropDown= JSON.stringify(DropCountryDownLoad);

			var DropStateGrid = new Object();
			DropStateGrid.dropdownname ="State";
			var jsonStateGrid= JSON.stringify(DropStateGrid);

			var StateDropDown = new Object();
			StateDropDown.dropdownname ="State";
			var jsonStateNames= JSON.stringify(StateDropDown);

			var DistrictDropDown = new Object();
			DistrictDropDown.dropdownname ="District";
			var jsonDistrictNames= JSON.stringify(DistrictDropDown);

			
			var DropStateDownLoad = new Object();
			DropStateDownLoad.targetdropdownname = "State";
			DropStateDownLoad.columnname = "countrycode";
			
						
			var DropDistrictDownLoad = new Object();
			DropDistrictDownLoad.targetdropdownname = "District";
			DropDistrictDownLoad.columnname = "statecode";
			
			
			$scope.master = {};
			
			
			//--------------Space Between Two Words -----------
  			$scope.spacebtw = function(b)
			{
					var twoSpace =$scope.AddedComany[b];
					twoSpace =twoSpace.replace(/ {2,}/g,' ');
					twoSpace=twoSpace.trim();
					$scope.AddedComany[b] =twoSpace;	
			};
			
			
			//-------Add Company Branch Details--------
			$scope.addComapnyBranchField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};
			
			//------Delete Company Branch Details-------
			$scope.removeCompanyBranch = function(BranchId) 
			{
					var Index = -1;
					
					var comArr = eval($scope.data);
					for( var i = 0; i < comArr.length; i++ ) 
					{	
						if( comArr[i].id === BranchId ) 
						{
							Index = i;
							break;
						}				
					}
					$scope.data.splice( Index, 1 );
					$index = comArr.length;
					for(var s=0;s<$index;s++)
					{
						$scope.data[s].id = Number(s)+Number(1);
					}
					
			};
			
			
			$scope.loadCountryNames = function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.countryNames=data;
					}); 
			};
			
			$scope.loadStateNamesUpdate = function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStateNames,
				   }).success(function(data, status) 
					{
						$scope.stateNames=data;
					}); 
			};

			$scope.loadDistrictNamesUpdate = function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonDistrictNames,
				   }).success(function(data, status) 
					{
						$scope.districtNames=data;
					}); 
			};


			$scope.loadGridStateNames = function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStateGrid,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.gridStateNames=data;
					}); 
			};

			
			$scope.loadStateNames = function(countryCode)
			{
					DropStateDownLoad.value = countryCode;
					var JSONStateString = JSON.stringify(DropStateDownLoad);

					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValues.html",
					data: JSONStateString,
				   }).success(function(data, status) 
					{
						$scope.stateNames=data;
					}); 
			};
			
			
			$scope.loadDistrictNames = function(stateCode)
			{
					DropDistrictDownLoad.value = stateCode;
					var jsonStringDistrict= JSON.stringify(DropDistrictDownLoad);
					
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValues.html",
					data: jsonStringDistrict,
				   }).success(function(data, status) 
					{
						$scope.districtNames=data;
					}); 
			};

			$scope.loadGridDistrictNames = function(stateCode)
			{
					DropDistrictDownLoad.value = stateCode;
					var jsonStringDistrict= JSON.stringify(DropDistrictDownLoad);
					
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/onChangeDropDownValues.html",
					data: jsonStringDistrict,
				   }).success(function(data, status) 
					{
						$scope.gridDistrictNames=data;
					}); 
			};

			$scope.loadGridDistrictNamesUpdate = function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonDistrictNames,
				   }).success(function(data, status) 
					{
						$scope.gridDistrictNames=data;
					}); 
			};


			$scope.loadAddedCompanyDetails = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllCompanyDetails.html",
						data: {},
				    }).success(function(data, status) 
					{
						if(data!=null)
						{
							$scope.loadStateNamesUpdate();
							$scope.loadDistrictNamesUpdate();
							$scope.loadGridDistrictNamesUpdate();
							$scope.AddedComany=data.company;
							$scope.data=data.branch;
							$index = data.branch.length;
						}
					}); 
			};
			
			
			
			//-----------Save Company Master--------
			
			$scope.Addcompanies = function(AddedComany,form)
			{
				
				//$scope.Addsubmitted = true;
				
				if($scope.CompanyForm.$valid)
				{
					CompanyGridData = [];
				    $scope.data.forEach(function(CompanyData) 
				    {
			             CompanyGridData.push(angular.toJson(CompanyData));														 
		    	    });									
								
					CompanyDataObj.formData = JSON.stringify(AddedComany);
					CompanyDataObj.gridData = CompanyGridData;
					
					//alert(JSON.stringify(CompanyDataObj));
					
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveCompanyDetails.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(CompanyDataObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Company Details Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);								
								$scope.AddedComany = angular.copy($scope.master);
								$index=1;
								$scope.data = [{'id':$index}]	
								
								$scope.loadAddedCompanyDetails();
								form.$setPristine(true);

							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
							
							
						}
					});						
				}
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}
			};
			
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedComany = angular.copy($scope.master);
				$scope.loadAddedCompanyDetails();
				$index=1;
				$scope.data = [{'id':$index}]	
			    form.$setPristine(true);			    								
			   
		   };
		})	  
	  
	   //====================================
	   		//Screens Master
	   //====================================
	  
	.controller('screensmaster',function($scope,$http)
	{
			$scope.isEdit = true;
			$scope.master = {};	

			var obj = new Object();
			obj.tablename = "Village";
			var jsonTable= JSON.stringify(obj);
			
		 	$scope.loadScreensmaster = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.AddedScreens={'status':'0','isAuditTrailRequired':'0','canAdd':false,'canModify':false,'canRead':true,'canDelete':false,'modifyFlag':"No"};	
						});								
			};
			
	
		//-------------Space Between Two Words -----------
  		$scope.spacebtw = function(b)
		{
				var twoSpace =$scope.AddedScreens[b];
				twoSpace =twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedScreens[b] =twoSpace;	
		};
		
			//-------Duplicate Screen Name---
			$scope.validateDup = function(screenName)
			{
				for(var i=0; i<JSON.stringify($scope.ScreenData.length);i++)
				{
					var str1 = screenName.toUpperCase();
					var str2 = $scope.ScreenData[i].screenName.toUpperCase();
						if(str1==str2)
						{
							 $('.btn-hide').attr('disabled',true);						
							 $(".duplicate").show();
							 return false;
						}
						 else
						{
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
				}
			};
			
			$scope.validateDuplicate = function(desigName,id)
		  {
			 for(var i =0;i<JSON.stringify($scope.ScreenData.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				var str2 = $scope.ScreenData[i].screenName.toUpperCase();
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };
		
		
		//-------------Get All Screens-----
		$scope.getAllScreensMaster=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllScreens.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.ScreenData=data;											
			}); 
		};
	
	   $scope.AddScreensSubmit = function(AddedScreens,form)
	   {	
				if($scope.ScreenForm.$valid) 
				{
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveScreens.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedScreens),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{				
								//alert(response);
								if(response==true)
								{
									
									swal("Success!", 'Screens Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									//-------------load Added all Screens----
									$scope.AddedScreens = angular.copy($scope.master);
									$scope.loadScreensmaster();
								    form.$setPristine(true);			    								

									
								}
								else
								{
									sweetAlert("Oops...", "Something went wrong!", "error");
									$('.btn-hide').attr('disabled',false);
								}
							}
						});					
				}	
			   else
				{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
				}			
			};
			
  			
						
						
			$scope.updateScreenMaster = function(Screen,id)
			{
				delete Screen["$edit"];
		  		var UpdatedData = angular.toJson(Screen);
				$scope.submitted = true;
				$scope.isEdit = true;
				
				if($scope.editScreenForm.$valid)
				{
					//alert(345);
					$scope.isEdit = false;
				  	$.ajax({
				   		url:"/SugarERP/saveScreens.html",
				   		processData:true,
			    		type:'POST',
			    		contentType:'Application/json',
			    		data:UpdatedData,
						beforeSend: function(xhr) 
						{
		            		xhr.setRequestHeader("Accept", "application/json");
		            		xhr.setRequestHeader("Content-Type", "application/json");
		        		},
			    		success:function(response) 
						{			  
							if(response==true)
							{
		  						swal("Success", 'Updated Successfully!', "success");

								//-------------load Added all Contractors----		
								var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllScreens.html",
									data: {}
								   }).success(function(data, status) 
									{
										$scope.ScreenData=data;										
									});
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
						}
					});	
				 }
         };
		 
		   //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
					$scope.AddedScreens = angular.copy($scope.master);
					$scope.loadScreensmaster();
				    form.$setPristine(true);			    								
		   };
		 
		 
		
	})	  
	  
	//======================================
		//Role Master
	//======================================
	  
	.controller('RoleMaster',function($scope,$http)
	{
		
			$scope.data = [];
			var RoleGridData = [];
			$scope.master = {};
			
			var RoleGridDataObj = new Object();
			$scope.ScreenNamesGrid1 = [];
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname ="Screens";
			var jsonStringDropDownSscreen= JSON.stringify(DropDownLoad);
			
			//----------Modified By Phani--
			
			var DropRoleDownLoad = new Object();
			DropRoleDownLoad.tablename = "Roles";
			DropRoleDownLoad.columnName = "roleid";
			DropRoleDownLoad.columnName1 = "role";
			var jsonStringRoleDropDown= JSON.stringify(DropRoleDownLoad);
									
			$scope.isEdit = true;
			$scope.AddedRoles={'status':'0','modifyFlag':"No"};
			
								
			//--------------Space Between Two Words -------
  			$scope.spacebtw = function(b)
			{
					var twoSpace =$scope.AddedRoles[b];
					twoSpace =twoSpace.replace(/ {2,}/g,' ');
					twoSpace=twoSpace.trim();
					$scope.AddedRoles[b] =twoSpace;	
			};
			//-------------drop down--------------
			$scope.loadRoleUsers=function()
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadAddedDropDownsForMasters.html",
					data: jsonStringRoleDropDown,
				   }).success(function(data, status) 
					{
						$scope.RoleNames=data;
						$scope.AddedRoles={'status':'0','modifyFlag':"No"};
					}); 
			};
			
			//-----------load Screen Names-----
			$scope.loadScreenNames=function()
			{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllScreens.html",
							data: {}
					   }).success(function(data, status) 
						{
							$scope.ScreenNamesGrid=data;							
						})			
			};
			$scope.loadScreens = function()
			{
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDownSscreen,
				   }).success(function(data, status) 
					{
						$scope.ScreenNames=data;
					});  
			}						
	
			//=============================Save===================================	
			$scope.AddRolesSubmit = function(AddedRoles,form)
			{	
				//$scope.Addsubmitted = true;
										
				if($scope.RoleForm.$valid) 
				{
					RoleGridData = [];
				    $scope.ScreenNamesGrid.forEach(function(Role) 
				    {
						 delete Role["$edit"];
			             RoleGridData.push(angular.toJson(Role));														 
		    	    });									
						
					RoleGridDataObj.role = AddedRoles.role;
					RoleGridDataObj.roleId = AddedRoles.roleid;
					RoleGridDataObj.description=AddedRoles.description;
					RoleGridDataObj.status=AddedRoles.status;
					RoleGridDataObj.roleScreensBean = RoleGridData;
					//alert(JSON.stringify(RoleGridDataObj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveRoles.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(RoleGridDataObj),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{				
								if(response==true)
								{
									swal("Success!", 'Roles Added Successfully!', "success");									
									$('.btn-hide').attr('disabled',false);									
									$scope.AddedRoles = angular.copy($scope.master);
									$scope.loadRoleUsers();
									form.$setPristine(true);		
									$scope.loadScreenNames();
									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}		
			   else
			    {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}					
				}
			};

		   //---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {	
		   		if(modifyId!=null)
				{
					$scope.AddedRoles = angular.copy($scope.master);
					$scope.loadRoleUsers();					
					$scope.loadScreenNames();
				    form.$setPristine(true);			    											   					

				}
			   else
			    {
					$scope.AddedRoles = angular.copy($scope.master);
					$scope.loadRoleUsers();
					$scope.loadScreenNames();
				    form.$setPristine(true);			    											   					
				}
		   };

			//=========onchange dropdown=========================				
									
			$scope.loadRolessOnChange = function()
			{
				if($scope.AddedRoles.roleid!=null)
				{
					    
					 var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllRolesOnChange.html",
							data: JSON.stringify($scope.AddedRoles.roleid),
						 }).success(function(data, status) 
					     { 
					  		$scope.AddedRoles.role=data.roles.role;
							$scope.AddedRoles.description=data.roles.description;
							$scope.AddedRoles.status=data.roles.status;
							$scope.AddedRoles.modifyFlag = "Yes";
							$scope.ScreenNamesGrid=data.roles.roleScreensBean;
					    });								
				}
			  else
			   { 
					//$scope.loadScreenNames();
					//$scope.AddedRoles = {};	
					//$scope.AddedRoles={'status':'0'};
					$scope.AddedRoles = angular.copy($scope.master);
					$scope.loadRoleUsers();
					$scope.loadScreenNames();
				    $scope.RoleForm.$setPristine(true);			    											   										
		  	   }
		  };
	  })	  
	
	
	//============================================
			//Screen Fields Master
	//============================================
	
		.controller('screenfieldmaster',function($scope,$http)
		{
			//alert(123);
			$index=0;
			$scope.data = [];
			$scope.master = {};
			$scope.Addedscreens={'modifyFlag':"No"};
			
			var maxid = new Object();
			var screenGridData = [];
			var screenDataObj = new Object();
			
			maxid.tablename ="ScreenFields";
			var jsonTable= JSON.stringify(maxid);
			
			
			 var DropDownLoad = new Object();
			DropDownLoad.dropdownname ="Screens";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			
			var obj = new Object();
			obj.tablename = "Village";
			var jsonTable= JSON.stringify(obj);
			
			$scope.setRowAfterReset = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							$scope.data = {};
							$scope.Addedscreens={'modifyFlag':"No"};
							$index = 1;
							$scope.data = [{'id':$index,'isAuditTrail':'0'}]					
						});								
			};			
			
			//--------------------Dropdown  added Screens---------------------
			$scope.loadScreenfieldNames = function()
			{
				//alert(jsonStringDropDown);
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.ScreenFieldNames=data;
					}); 
			};
			
			
			//-------Add screen fields Details--------
			$scope.addScreenBranchField = function() 
			{
				//alert(90);
				$index++;
				
		    	$scope.data.push({'id':$index,'isAuditTrail':'0'})
				//alert($index);
			};
			
			
			//------Delete screen fields Details-------
			$scope.removeScreenBranch = function(ScreenId) 
			{
				
					var Index = -1;
					
					var scrArr = eval($scope.data);
					for( var i = 0; i < scrArr.length; i++ ) 
					{	
						if(scrArr[i].id === ScreenId ) 
						{
							Index = i;
							break;
						}				
					}
					$scope.data.splice( Index, 1 );
					$index = scrArr.length;
					//alert($index);
			};
			
			
			
			//-----------Save Screen fields Master--------
			
			$scope.Addscreenfields = function(Addedscreens,form)
			{
				  delete Addedscreens["$edit"];
				  //delete Screendata["$index"];
				//$scope.Addsubmitted = true;
				
				if($scope.screenForm.$valid)
				{
					//alert(34);
					screenGridData = [];
				    $scope.data.forEach(function(ScreenData) 
				    {
			             screenGridData.push(angular.toJson(ScreenData));														 
		    	    });									
								
						
						screenDataObj.formData = JSON.stringify(Addedscreens);
					   	screenDataObj.gridData = screenGridData;
						$('.btn-hide').attr('disabled',true);
						
					$.ajax({
				    	url:"/SugarERP/saveScreenFields.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(screenDataObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{							
							if(response==true)
							{
								swal("Success", 'Screen Fields Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								$scope.Addedscreens = angular.copy($scope.master);	
								$scope.setRowAfterReset();
								form.$setPristine(true);	
								$scope.loadScreenfieldNames();
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});						
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
			};
			
			
			
		//-------------Get Added screen fields on Dropdown change--
  
   		$scope.screensOnChange = function()
   		{
	  		 if($scope.Addedscreens.screenId!=null)
			 {
  			  	 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllScreenFields.html",
						data:JSON.stringify($scope.Addedscreens.screenId),
	  				}).success(function(data, status) 
  					{
						$scope.data=data;
						$scope.Addedscreens.modifyFlag = "Yes";
						$index = data.length;				
		  			}).error(function(data,status)
			 		 {
 		 				$scope.data = {};
						$index = 1;
						$scope.data = [{'id':$index,'isAuditTrail':'0'}]						
  					});
   
       		 }
			else
			 {
						$scope.Addedscreens = angular.copy($scope.master);
						$scope.setRowAfterReset();
						$scope.Addedscreens.modifyFlag = "No";
					    $scope.screenForm.$setPristine(true);			    											   				 
			 }
	    };
		 //---------Reset Form----
		 $scope.reset = function(form,modifyId)
		 {			  
		 		if(modifyId!=null)
				{
					$scope.Addedscreens = angular.copy($scope.master);
					$scope.setRowAfterReset();
				    form.$setPristine(true);			    											   

				}
			   else
			    {
					$scope.Addedscreens = angular.copy($scope.master);
					$scope.setRowAfterReset();
				    form.$setPristine(true);			    											   
				}
		};

	})
			
	
	
	  
	//===================================================================Functional Screens========================================================================================
		
		//========================================
				//Extent Details for Soil Tests
		//========================================
		
		.controller('RyotExtentDetails',function($scope,$http)
		{

				var LoadRyotDropDown = new Object();
				LoadRyotDropDown.dropdownname = "ExtentDetails";
				var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);

			 	var LoadVarietyName = new Object();
				LoadVarietyName.dropdownname = "Variety";
				var VarietyNameString= JSON.stringify(LoadVarietyName);

								
				$index=0;
				$scope.data = [];
				var ExtentgridData = [];
				var sendExtentDetails = new Object();
				$scope.master = {};

				//--------Load Ryot Code Dropdown------
				$scope.loadRyotCodeDropDown = function()
				{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							$scope.ryotNames = data;
							$scope.data = {};
							$index = 1;
							$scope.data = [{'id':$index,'plotSlNumber':$index}]	
							$scope.AddedExtent = {'screenName':'Extent Details for Soil Test'};
							
						});  	   
				};		
				
				//----------Load Variety Names---------
				$scope.loadVarietyNames = function()
				{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
				};
				
				//-----------Add Row for Branch------
				$scope.addExtentField = function() 
				{
					$index++;
				    $scope.data.push({'id':$index,'plotSlNumber':$index})
				};		 		 		
				//------------Delete Row from table---
				$scope.removeExtent = function(id)
				{				
					var Index = -1;		
					var comArr = eval( $scope.data );
					for( var i = 0; i < comArr.length; i++ ) 
					{	
						if( comArr[i].id === id ) 
						{
							Index = i;
							break;
						}				
					}
					$scope.data.splice( Index, 1 );
					for(var s=0;s<$index;s++)
					{
						$scope.data[s].plotSlNumber = Number(s)+Number(1);
					}
		  	    };				
				
				$scope.setRowAfterReset = function()
				{
							$scope.data = {};
							$index = 1;
							$scope.data = [{'id':$index,'plotSlNumber':$index}]					
				};

				
				//-----------Save Extent Details--------
				
				$scope.AddExtentDetails = function(Extent,form)
				{
						//$scope.submitted=true;
						if($scope.RyotExtentDetails.$valid)
						{
							ExtentgridData = [];
						    $scope.data.forEach(function (ExtentData) 
						    {
					             ExtentgridData.push(angular.toJson(ExtentData));														 
					        });									

							sendExtentDetails.formData = JSON.stringify(Extent);
							sendExtentDetails.gridData = ExtentgridData;
							
							//alert(JSON.stringify(sendExtentDetails));
							$('.btn-hide').attr('disabled',true);
							$.ajax({
						    	url:"/SugarERP/SaveExtentDetails.html",
							    processData:true,
					    		type:'POST',
							    contentType:'Application/json',
							    data:JSON.stringify(sendExtentDetails),
								beforeSend: function(xhr) 
								{
					            	xhr.setRequestHeader("Accept", "application/json");
						            xhr.setRequestHeader("Content-Type", "application/json");
						        },
							    success:function(response) 
								{
									if(response==true)
									{										
										swal("Success", 'Ryot Extent Details Added Successfully!', "success");
										$('.btn-hide').attr('disabled',false);										
										//$scope.loadRyotCodeDropDown();
										//$scope.ResetExtent();
										$scope.AddedExtent = angular.copy($scope.master);
										$scope.ryotcode = "";
										$scope.loadRyotCodeDropDown();
										$scope.setRowAfterReset();
										form.$setPristine(true);
										
										//location.reload();																				
									}				
								   else
								    {
									   sweetAlert("Oops...", "Something went wrong!", "error");
									   $('.btn-hide').attr('disabled',false);										
									}
								}								
							});															
						}
					   else
					    {
							var field = null, firstError = null;
							for (field in form) 
							{
								if (field[0] != '$') 
								{
									if (firstError === null && !form[field].$valid) 
									{
										firstError = form[field].$name;
									}
									if (form[field].$pristine) 
									{
										form[field].$dirty = true;
									}
								}	
							}						
						}
				};
				
				//-----------Ryot Code DropDown Onchange------
				$scope.getRyotDetails = function(RyotCode)
				{
					if(RyotCode!=null)
					{
						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getExtentDetailsForSoilTest.html",
							data: RyotCode,
						   }).success(function(data, status) 
						   {	
									$scope.AddedExtent = data[0];
									$scope.AddedExtent.ryotName = data[0].ryotName;
									$scope.data = data;
									if(data[0].varietyCode!='') { $scope.data[0].plotSlNumber = data[0].id; }
									$index = data.length;
									$scope.AddedExtent.screenName='Extent Details for soil Test';
						   });  	   															
					}
				   else
				    {
						$scope.AddedExtent = angular.copy($scope.master);
						$scope.setRowAfterReset();
						$scope.loadRyotCodeDropDown();
					    $scope.RyotExtentDetails.$setPristine(true);			    											   						
					}
				};
				 //---------Reset Form----
			   $scope.reset = function(form)
			   {			  
					$scope.AddedExtent = angular.copy($scope.master);
					$scope.ryotcode = "";
					$scope.setRowAfterReset();
					$scope.loadRyotCodeDropDown();
				    form.$setPristine(true);			    											   
			   };
				
		     //-----------Ryot Code  Onchange------
				$scope.getRyotUpdate = function(ryotCode)
				{
						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/GetRyotNameForCode.html",
							data: ryotCode,
						   }).success(function(data, status) 
						   {	
						  		 //alert(JSON.stringify(data));
								 //alert(JSON.stringify(data[0].ryotName));
									$scope.AddedExtent = data[0];
									$scope.AddedExtent.ryotName = data[0].ryotName;
									
									
						});  	   															
				};
				
				
		})
		
		//======================================
				//Login User Master
		//======================================
		 .controller('LoginUserMaster', function($scope,$http)
	  	{
		  	var maxid = new Object();
			maxid.tablename ="Employees";
			var jsonTable= JSON.stringify(maxid);	
		    var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "DepartmentMaster";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Designation";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoadUser = new Object();
			DropDownLoadUser.tablename = "Employees";
			DropDownLoadUser.columnName = "employeeid";
			DropDownLoadUser.columnName1 = "employeename";
			var jsonStringDropDownUser= JSON.stringify(DropDownLoadUser);			
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname ="Roles";
			var jsonStringDropDownRoles= JSON.stringify(DropDownLoad);	
		var	loginIDObj = new Object();
			
		
		     $scope.maritalStatus = [{ maritalStatusId: '0', maritalStatus: 'Single' }, { maritalStatusId: '1', maritalStatus: 'Married' }];
			 $scope.empStatus = [{ empStatusId: '0', empStatus: 'Request' }, { empStatusId: '1', empStatus: 'Authorise' }];
	 		 $scope.genderData = [{ id: '0', gender: 'Male' }, { id: '1', gender: 'Female' }];
	
			 $scope.master = {};
			//-----Login Id Ajax Request--------
			$scope.duplicateId = function(loginId)
{
loginIDObj.loginId = loginId;

if(loginId!="null")
{	  
var httpRequest = $http
({
method: 'POST',

url : "/SugarERP/getLoginIdFromEmployeeId.html",
data: JSON.stringify(loginIDObj),
}).success(function(data) 
{

if(data==true)
{

$('.btn-hide').attr('disabled',true);
$('.duplicate').show();
    return false;
}
else
{
    $('.btn-hide').attr('disabled',false);
    $('.duplicate').hide();
}

});

 	     	
    } 
};
			
			//--------------Login User Duplicate Names--------------------
			
			$scope.validateLoginUser = function()		 
		 	{
				//alert(90);
				for(var a =0;a<JSON.stringify($scope.employeeNames.length);a++)
				{
					//alert('ganesh');
					var LoginUser = $scope.AddedLoginUser.employeeName.toUpperCase();
					var AddedLoginUser = $scope.employeeNames[a].employeename.toUpperCase();
					if(LoginUser!=null)
					{
						
							if(LoginUser==AddedLoginUser)
							{
								$('.btn-hide').attr('disabled',true);
								$('.duplicate').show();
								return false;
							}
						   else
						    {
								$('.btn-hide').attr('disabled',false);
								$('.duplicate').hide();
							}
						}
				}
		  };
			
			
			
			
			
			
			//---------load Role Names-------------------
			$scope.loadRolesNames=function()
			{
				//$scope.employeeNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDownRoles,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.RoleNames=data;
					}); 
			};			
			//---------Space Between Two Words ----------
  			$scope.spacebtw = function(b)
			{
				var twoSpace =$scope.AddedLoginUser[b];
				twoSpace =twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddedLoginUser[b] =twoSpace;	
			};
	
	
			$scope.loadLoginUserId=function()
			{
				 var httpRequest = $http
				({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,
			 	}).success(function(data) 
				{			
					//$scope.AddedLoginUser={'loginID':data.id,'status':'0'};	
					//$scope.AddedLoginUser={'status':'0'};	
					$scope.AddedLoginUser={'status':'0','screenName':'User Master'};
				});
			};
			$scope.loaddepartmentNames = function()
			{
				//alert(123);
				$scope.departmentNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						
						$scope.departmentNames=data;
					}); 
			};
									
			$scope.loadDesignationNames = function()
			{
				
				$scope.designationNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.designationNames=data;
					}); 
			};
			$scope.loadUserNames = function()
			{
				$scope.employeeNames=[];
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadAddedDropDownsForMasters.html",
					data: jsonStringDropDownUser,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.employeeNames=data;
					}); 
			};
			
			//-----------Save Login User Master--------
			
			$scope.LoginUserSubmit = function(AddedLoginUser,form)
			{	
					//$scope.submitted = true;
					$scope.isEdit = true;
				if($scope.LoginUserMasterForm.$valid) 
				{
					//alert(JSON.stringify(AddedLoginUser));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveEmployees.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedLoginUser),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{				
							//alert(response);
								if(response==true)
								{									
									swal("Success!", 'Login User Master Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedLoginUser = angular.copy($scope.master);
									$scope.loadLoginUserId();
									form.$setPristine(true);
									$scope.loaddepartmentNames();
									$scope.loadUserNames();
								}
							   else
							    {
									   sweetAlert("Oops...", "Something went wrong!", "error");
									   $('.btn-hide').attr('disabled',false);																			
								}
							}
						});					
				}	
			   else
			   {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
				}				
			};

			
			
		////-------------Added Users DropDown Onchange--------------------		 
		
		$scope.loadUsersOnChange = function(LoginUser)
		{			
			if(LoginUser!=null)
			{
				 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllEmployeesForUser.html",
					data:LoginUser,
					  }).success(function(data, status) 
					  {							
							$scope.AddedLoginUser = data.employees;
							$scope.AddedLoginUser.screenName="User Master";
							if(data.employees.gender=='0')
							{
								$scope.AddedLoginUser.gender='0';
							}
						   else
						    {
								$scope.AddedLoginUser.gender='1';
							}
							if(data.employees.maritalStatus=='0')
							{
								$scope.AddedLoginUser.maritalStatus='0';
							}
						   else
						    {
								$scope.AddedLoginUser.maritalStatus='1';
							}
															
					  }).error(function(data,status)
					  {
						  //alert(data);
					  });	
			}
		   else
		    {
					$scope.AddedLoginUser = angular.copy($scope.master);
					$scope.loadLoginUserId();
				    $scope.LoginUserMasterForm.$setPristine(true);			    											   					
			}
		};

		 //---------Reset Form----
		   $scope.reset = function(form,modifyId)
		   {			  
		   		if(modifyId!=null)
				{
					$scope.AddedLoginUser = angular.copy($scope.master);
					$scope.loadLoginUserId();
				    form.$setPristine(true);			    											   					
				}
			   else
			    {
					$scope.AddedLoginUser = angular.copy($scope.master);
					$scope.loadLoginUserId();
				    form.$setPristine(true);			    											   					
				}
		   };



	})
		
	//==============================================
			//Update Soil Test Results
	//==============================================
	
	.controller('UpdateSoilTestResults',function($scope,$http)
	{
		$scope.data = [];
		$scope.Recommendation = [];
		var RyotIDs = new Object();
		
		var RecommendatioData = new Object();
		$scope.enteredBy=sessionStorage.getItem('username');
		RyotIDs.dropdownname = "ExtentDetails";
		var jsonRyotIDs= JSON.stringify(RyotIDs);	
		
		var UpdateExtentgridData = [];
				
		var mainExtentDetails = new Object();		
		var RyotCodeData = new Object();		
		var recommendationObj = new Object();
		
		$scope.master = {};
		
		$scope.loadRyotExtentNames = function()
		{			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonRyotIDs,
			   }).success(function(data, status) 
			   {
					$scope.ryotCodeFilterData=data;
					
					$scope.enteredBy=sessionStorage.getItem('username');
			   }); 
					

		};		
		
		
		//var dialog = document.getElementById('window');  		
		$scope.showDialog = function(linkok,surveyno,ryotCode)
		{
			
			recommendationObj.plotno = linkok;
			recommendationObj.surveyno = surveyno;
			recommendationObj.ryotcode = ryotCode;
			
						
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getRecomendationsFromTemp.html",
					data: JSON.stringify(recommendationObj),
			   }).success(function(data, status) 
			   {				
					$scope.Recommendation.recommendation = data[0].recommendation;	
			   }); 
			$scope.Recommendation.plotNo = linkok;
			$scope.Recommendation.surveyNo = surveyno;				
			//dialog.show(); 
			$('#popup_box').fadeIn("slow");
			$('#recom').focus();
			
		};
		
		$scope.CloseDialog = function () 
		{
			//dialog.close();  
			$('#popup_box').fadeOut("slow");
		};
		
		window.onkeydown = function( event ) 
		{
		    if ( event.keyCode === 27 ) 
			{
        		$('#popup_box').fadeOut("slow");
		    }
		};		
		
		//----------Load Ryot Details--------
		$scope.getRyotDetailsUpdate = function(RyotCode)
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getExtentDetailsForSoilTest.html",
					data: RyotCode,
			   }).success(function(data, status) 
			   {	
						$scope.ryotName = data[0].ryotCode;
			   });  	   															
		};
		
		
		//----------Ryot Details--------
		$scope.getRyotDetailsUpdate = function(ryotCode)
		{
			//alert(89);
			//alert(ryotCode);
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetRyotNameForCode.html",
					data: ryotCode,
			   }).success(function(data, status) 
			   {
				  // alert(JSON.stringify(data[0].ryotName));
				  // $scope.Recommendation = data[0];
					//$scope.ryotName = data[0].ryotName;
					$scope.ryotName = data[0].ryotName;
					
				 // $scope.ryotCode=data;
				   
				   //alert(90);
				   //$scope.ryotCode=ryotName;
				   //alert($scope.ryotName);
						//var asd=$scope.ryotName;
						//$scope.ryotName=asd;
						
			   });  	   															
		};
		
		
		
		
		
		//-----------Load Extent Details-----
		$scope.loadExtentDetails = function(RyotCode,form)
		{
			//$scope.submitted = true;
			if(RyotCode!='')
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getUpdateSoilTestResults.html",
						data: RyotCode,
				   }).success(function(data, status) 
				   {		
				   			if(typeof data[0].varietyCode== 'undefined')
							{ 
								$scope.data = {};	
							} 
						   else 
						    { 								
								$scope.data = data;
								$scope.Recommendation.rCode = data[0].ryotCode; 
								$scope.Recommendation.rName = data[0].ryotName;	
								$scope.soilTestStatus = data[0].soilTestStatus;
								$scope.enteredBy = sessionStorage.getItem('username');
								//$scope.Recommendation.recommendation = data[0].recommendation;
								//$('.cultivateyes:first').focus();
							} 
				   }).error(function(data,status)
				   {
						  $scope.data = {};
				   });  	   															
			}
		   else
		    {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}				
			}
		};		
		
		//----------------Save Recommendation Popup--
		$scope.SaveRecommendations = function(Recommendation)
		{
			
			$scope.Resubmitted = true;

			if(Recommendation.recommendation!=null)
			{
				var recommendation = Recommendation.recommendation;
				var rCode = Recommendation.rCode;
				var rName = Recommendation.rName;
				var surveyNo = Recommendation.surveyNo;
				var plotNo = Recommendation.plotNo;
				
				RecommendatioData.rCode = rCode;
				RecommendatioData.recommendation = recommendation;
				RecommendatioData.rName = rName;
				RecommendatioData.surveyNo = surveyNo;
				RecommendatioData.plotNo = plotNo;
			
				$('.btn-hide').attr('disabled',true);
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/saveUpdateSoilTestPopUpDetails.html",
						data:JSON.stringify(RecommendatioData),
						  }).success(function(data, status) 
						  {
								
								if(data==true)
								{
									swal("Success!", 'Recommendations Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);								   
									
									$('#popup_box').fadeOut("slow");
								}
							  else
							   {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);								   
							   }
								
					  }).error(function(data,status)
					  {
						  //alert(data);
					  });			
			}
		};
		
		//-------------------Save Recommendations Main Form-
		
		$scope.saveMainRecommendationsForm = function(ryotCode,form,enteredBy)
		{
			
			//$scope.Addsubmitted = true;
			if($scope.UpdateSoilTestForm.$valid)
			{
				UpdateExtentgridData = [];
			    $scope.data.forEach(function (UpdateExtentData) 
			    {					
	    	         UpdateExtentgridData.push(angular.toJson(UpdateExtentData));
	        	});									
				RyotCodeData.ryotCode = ryotCode;
				RyotCodeData.enteredBy = enteredBy;
				//$scope.enteredBy = enteredBy;
				
				//alert("entered By-------"+enteredBy);
				mainExtentDetails.formData = RyotCodeData;
				mainExtentDetails.gridData = UpdateExtentgridData;
				
				$('.btn-hide').attr('disabled',true);	
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveUpdateSoilTestResults.html",
					data:JSON.stringify(mainExtentDetails),
					  }).success(function(data, status) 
					  {
							if(data==true)
							{
								swal("Success!", 'LabTestResults Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);								   								
								$scope.UpdateExtentData = angular.copy($scope.master);
								form.$setPristine(true);
								$scope.data = {};
								$scope.ryotCode = "";
								$scope.ryotName = "";
								$scope.enteredBy = sessionStorage.getItem('username');
							}
						   else
						    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);								   								
							}
					  }).error(function(data,status)
					  {
					  });			
			}
		   else
		    {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}				
			}
		};
		
		//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.UpdateExtentData = angular.copy($scope.master);
				$scope.ryotName = "";
				$scope.ryotCode = "";
				$scope.data = {};
				$scope.enteredBy=sessionStorage.getItem('username');

			    form.$setPristine(true);			    								
			   
		   };
	})

//--------------------------------------------------------------Harvesting MIS Reports---------------------------------------------------------------------------------------

	//=============================================
		//BranchDetailsReport
	//=============================================
	.controller('BranchDetailsReport',function($scope,$http)
	{
		$scope.Bchecklist = {'mode':"HTML"};
	})
		

	//==============================================
			//Branch Details Checklist Circle Wiese
	//==============================================
	.controller('BranchDetailsCircleWiseReport',function($scope,$http)
	{
		$scope.checklist = {'mode':"HTML"};
	})
	
	//=========================================
			//Report Growers Register Ryotwise
	//=========================================
	  .controller('GrowersRegisterVillagewiseReport', function($scope,$http)
	  {
		  
			$scope.AddedMandal={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Mandal";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Village";
			var StringDropDownUser= JSON.stringify(DropDownLoad);
		
			//------------load Mandal Nemes----
			$scope.loadMandalNames=function()
		    {			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: StringDropDown,
				   }).success(function(data, status) 
				   {
						$scope.mandalNames=data;
				   }); 
			};	
			
			//--------------load Village Names----------
			$scope.loadVillageNames=function()
		    {			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: StringDropDownUser,
				   }).success(function(data, status) 
			   		{
						$scope.villageNames=data;
				   }); 
			};	
		})	  	
	  
	  //=======================================
	  		//Report Mandal Date Wise Summary
	  //=======================================
	  
	  .controller('AgreementMasterListRyotWiseInVillage',function($scope,$http)
	  {
			$scope.AddedMandal={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
						
						
			//----------Load Season Names-------
			
		   $scope.loadseasonNames=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   });
				
		   };
		   
		})
	  
	   //===============================================
	   		//GrowerListBy Village Mandal Summary
	   //===============================================
	   
	  .controller('GrowerListByVillageMandalSummary',function($scope,$http)
	  {
		
			$scope.AddedMandal={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Mandal";
			var StringDropDownMandal= JSON.stringify(DropDownLoad);
			
			
			//---------------loand Season Names-------
			 $scope.loadseasonNames=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   });
				
		   };
		  
		    $scope.loadMandalNames=function()
			
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownMandal,
			   }).success(function(data, status) 
			   {
				   
				    data.push({"mandal":"all","id":"ALL"});
					$scope.mandalNames=data;
			   });
				
		   };
									
		})
		
		//==========================================
			 //Growers Register Ryot Wise Report
		//==========================================

	    .controller('GrowerRegister',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Village' }, { GROWER: '1', GROWER: 'Mandal' }];		
		})

		
		
	  //=============================================
	  		//Bank Order
	  //=============================================
	  .controller('BankOrder',function($scope,$http)
		{
			$scope.AddedBranch={'status':'pdf'};

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Branch";
			var StringDropDown= JSON.stringify(DropDownLoad);

			var DropSeasonDownLoad = new Object();
			DropSeasonDownLoad.dropdownname="Season";
			var StringSeasonDropDown= JSON.stringify(DropSeasonDownLoad);

									
			$scope.loadBranchNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.branchNames=data;
			   }); 
		};	

			$scope.loadseasonNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringSeasonDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   }); 
		};	

		})
		
		//========================================
				//Agreement Checklist
		//========================================
	    .controller('AgreementCheckList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Loan Master List Report
		//========================================
	    .controller('LoanMasterListReport',function($scope,$http)
		{
			
			$scope.AddedSeason={'status':'pdf'};
			//alert(123);	
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
	  
		})

	//===============================
		//RyotLoanForwardingReport
	//===============================
		  
.controller('RyotLoanForwardingReport',function($scope,$http)
{
			var reportObj = new Object();
			$scope.AddedRyotName={'status':'pdf'};



	var DropDownLoad = new Object();
		DropDownLoad.dropdownname="Season";
		var StringDropDown2= JSON.stringify(DropDownLoad);


			var DropDownLoad = new Object();
 			DropDownLoad.dropdownname = "Branch";
 				var jsonStringBranch= JSON.stringify(DropDownLoad);


			//var DropDownLoad = new Object();
			//DropDownLoad.dropdownname="loanRyotcode";
			//var LoanDropdownBranch= JSON.stringify(DropDownLoad);

			var DropLoanDownLoad = new Object();
			DropLoanDownLoad.tablename = "LoanDetails";
			DropLoanDownLoad.columnname = "loannumber";
			DropLoanDownLoad.wherecolumnname = "Season";	
			DropLoanDownLoad.wherecolumnname1 = "ryotcode";
				//var reportObj = new Object();


 

			$scope.loadBranchNames = function()
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringBranch,
			  }).success(function(data, status) 
			{
			//alert(JSON.stringify(data));
			$scope.branchNames=data;
			}); 
			};




			  $scope.loadSeasons=function()
			   {	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: StringDropDown2,
			   }).success(function(data, status) 
			   {
			$scope.seasons=data;
			   }); 
			};	

 

			$scope.loadLoanNos = function(AddedRyotName)
			{
			
				reportObj.season = $scope.AddedRyotName.season;
			
			reportObj.branchcode = $scope.AddedRyotName.branchCode;
			//alert(JSON.stringify(reportObj));
			if(reportObj.season!='' && reportObj.branchcode!='') 
			{
			
			 var httpRequest = $http({
				  method: 'POST',
			  url : "/SugarERP/getLoanRyotCode.html",
			  data: JSON.stringify(reportObj),
			   }).success(function(data, status) 
			{
			//alert(JSON.stringify(data));
			$scope.loanRyotcode=data;
			});
			 
			}
			
			};	

		//window.parent.location.reload();
	})

	   
	 //=============================================
	  		//Agriculture Loan App
	  //=============================================
	  .controller('AgricultureLoanAppReport',function($scope,$http)
		{
			//alert(2);
			$scope.AddedBranch={'status':'pdf'};

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Branch";
			var StringDropDown= JSON.stringify(DropDownLoad);

			var DropSeasonDownLoad = new Object();
			DropSeasonDownLoad.dropdownname="Season";
			var StringSeasonDropDown= JSON.stringify(DropSeasonDownLoad);
			
			
									
			$scope.loadBranchNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {	
					//data.push({"branchname":"all","branchname":"ALL"});
					$scope.branchNames=data;
			   }); 
		};	
	
		$scope.getBatchnumbers=function(season,branchcode)
		    {	
			var DropDownLoadForBatch = new Object();
			DropDownLoadForBatch.season=season;
			DropDownLoadForBatch.branch=parseInt(branchcode);
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getBranchBatchNo.html",
					data: JSON.stringify(DropDownLoadForBatch),
			   }).success(function(data, status) 
			   {	
					$scope.batchnumbers=data;
			   }); 
		};
	
			$scope.loadseasonNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringSeasonDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   }); 
		};	

		})   


	 //=============================================
	  		//SeedSeedlingLoansReport
	  //=============================================
		
		.controller('SeedSeedlingLoansReport',function($scope,$http)
		{
			
			$scope.AddedSeason={'status':'PDF'};
			//alert(123);	
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadseasonNames=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
	  
		})
		
		//==========================================
			 //VarietywiseSummary Report
		//==========================================

	    .controller('VarietywiseSummaryReport',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Circle' }, { GROWER: '1', GROWER: 'Region' }, { GROWER: '2', GROWER: 'Mandal' }, { GROWER: '3', GROWER: 'Village' }, { GROWER: '4', GROWER: 'Zone' },{ GROWER: '5', GROWER: 'Variety' }];		
		})
		

	//========================================
				//Circle Plant Ratoon Wise RyotReport
		//========================================
	    .controller('CirclPlantRatoonWiseRyotReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})		
		
		//---------------------------------------Bank Order End ---------------------------------------

	  //===================================
	  		//Migration Master
	  //===================================
		 .controller('MigrationMaster',function($scope,$http)
		{
			//migrateCircle()
				
			$scope.migrateCircle=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateCircleData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Circle Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}
			   }); 
			};
			
			$scope.migrateVillage=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateVillageData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Village Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}

			   }); 
			};
			
			$scope.migrateBank=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateBankData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Bank Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}

			   }); 
			};
			
			
			//
			$scope.migrateAgreement=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateAgreementData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Agreement Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}

			   }); 
			};
			$scope.getMigrateLoanData=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateLoanData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Loan Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}

			   }); 
			};
			
			$scope.getOldRyotDataData=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateOldRyotData.html",
					data: '',
			   }).success(function(data, status) 
			   {
					if(data==true)
					{
						swal("Success!", 'Loan Data Migrated Successfully!', "success");
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
					}

			   }); 
			};
			
			
			$scope.updateRyotNames=function()
		   {			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateRyotNames.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Loan Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				
				$scope.Migrate1516Accounts=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/transportingMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'transporting Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};

				$scope.Migrate1314Accounts=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/Migrate1314Accounts.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'transporting Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};

				$scope.Migrate1415Accounts=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/Migrate1415Accounts.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'transporting Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};

				$scope.advancesMigation=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/SeedAndSeedAdvanceMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'transporting Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				$scope.regionMigration=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/regionMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Region Data Migrated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				$scope.AdvancesToAccounts=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/AdvancesToAccounts.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Advances migrated to accounts Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};		
				$scope.UpdateLoanDate=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateLoandetialsForDate.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Advances migrated to accounts Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	

				$scope.UpdateLoanPrinciplesDate=function()
				{			
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMigrateLoanPrinciplesForDate.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Advances migrated to accounts Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	

				$scope.UpdateloanSummary=function()
				{		//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/ReconsileLoansAndAdvances.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Update Loan Summary Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	
				
				$scope.UpdateZones=function()
				{		
					alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateZonesMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Update Loan Summary Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	




				$scope.UpdateAccounAdvances=function()
				{		
					alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateAdvancesData.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Update Advance Data Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	


				$scope.UpdateloanDetails=function()
				{		
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateLoanData.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Update Loan Data Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	
				$scope.caneAccUpdationCheckLIst=function()
				{		
					//alert("hai");
					//alert("data");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getCaneUpdationCheckList.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'cane updation check list generation Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	
				$scope.caneAccCumulationCheckLIst=function()
				{		
					//alert("hai");
					//alert("data");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getCaneAccCumulativeCheckList.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'cane updation check list generation Migration Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	

				$scope.UpdateAdvanceSummary=function()
				{		
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateAdvancesSummaryData.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Advance Summary Data Updated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				
				
				//UpdateMobileNos
				$scope.UpdateMobileNos=function()
				{		
					//alert('1');
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateMobileNumbers.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Ryot Mobile Numbers Updated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};


				$scope.migrateRyotCategory=function()
				{		
					//alert('1');
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateRyotCategory.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Ryot Category Updated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				$scope.PurchaseTaxOfWeighmentMigration=function()
				{		
					alert("data");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/CanePurchargeTaxAccountMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Cane Purchase Tax Updated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};
				$scope.DedsCaStatusMigration=function()
				{		
					alert("Migrating Castatus");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/DedsCaStatusMigration.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'CA Status to Deds updated Successfully!', "success");
						}
					   else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
	
				   }); 
				};	

				$scope.updateHarvesterCode=function()
				{		
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateHarvesterCode.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Harvesting Contractor codes updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				$scope.updateTransporterCode=function()
				{		
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateTransporterCode.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Transport Contractor codes updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};

				//updateSeedSupplierData
				$scope.updateSeedSupplierData=function()
				{	
					var DropDownLoad = new Object();
					DropDownLoad.dropdownname="2016-2017";
					var StringDropDown= JSON.stringify(DropDownLoad);		

					
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateSeedSupplierAccounts.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Seed Supplier Accounts updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				$scope.updateSeedSupplierAccountsToCr=function()
				{	
					var DropDownLoad = new Object();
					DropDownLoad.dropdownname="2016-2017";
					var StringDropDown= JSON.stringify(DropDownLoad);		

					
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateSeedSupplierAccountsToCr.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Seed Supplier Accounts updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				$scope.updateWeighmentAccounts=function()
				{		
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateWeighmentAccounts.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Weighment Account codes updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
			//  Added by sahadeva   15-02-2017  
				$scope.updateCaneReceiptEndDates=function()
				{		
					//alert("data");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateWeighmentCaneReceiptDates.html",
					data: '',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Weighment Dates updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				$scope.updateLoanPrinciplesForAccountedRyots=function()
				{	
					var DropDownLoad = new Object();
					DropDownLoad.dropdownname="2016-2017";
					var StringDropDown= JSON.stringify(DropDownLoad);		

					
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateLoanPrinciplesForAccountedRyots.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Seed Supplier Accounts updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				//Added by DMurty on 22-02-2017
				$scope.updateOldLoans=function()
				{	
					var DropDownLoad = new Object();
					DropDownLoad.dropdownname="2016-2017";
					//DropDownLoad.suppliedOrNot="Supplied";
					DropDownLoad.suppliedOrNot="Not Supplied";
					var StringDropDown= JSON.stringify(DropDownLoad);		

					
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/updateOldLoans.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Old Loans updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				//Added by DMurty on 24-02-2017
				$scope.loanReconsilation=function()
				{	
					var DropDownLoad = new Object();
					DropDownLoad.dropdownname="2016-2017";
					var StringDropDown= JSON.stringify(DropDownLoad);		

					
					//alert("data")	;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loanReconsilation.html",
					data: StringDropDown,
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Old Loans updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				$scope.ssupplierMigration=function()
				{	
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/seedSupplierSummaryMigration.html",
					data:'',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Seed Supplier Summary Migration updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				//For SeedSource Previous Accounting posting as per season of ryot catagory wise posting
				$scope.migrateAccountsForSeedSupplierPerSeason=function()
				{	
				alert("hello");
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateAccountsForSeedSupplierPerSeason.html",
					data:'',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Seed migrateAccountsForSeedSupplierPerSeason Migration updated Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				$scope.PinCodeMigration=function()
				{	
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migratePincodeInVillageMaster.html",
					data:'',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Pincode Migrated  Successfully In Village Master !', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
				$scope.migrateBatchesForSeedSupplierPerSeason=function()
				{	
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateBatchesForSeedSupplierPerSeason.html",
					data:'',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'Pincode Migrated  Successfully In Village Master !', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				$scope.migrateBatchesInSeedSourceRecords=function()
				{	
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/migrateBatchesInSeedSourceRecords.html",
					data:'',
				   }).success(function(data, status) 
				   {
						if(data==true)
						{
							swal("Success!", 'migrateBatchesInSeedSourceRecords Migrated  Successfully In Village Master !', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				   }); 
				};
				
			
		})
	  
	  //================================
	  		//Audit Trail
	  //================================
		.controller('AuditTrail',function($scope,$http)
		{
			$scope.loadAudittrail=function(addaudit,form)
			{
				if(form.$valid)
				{
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAuditTrail.html",
						data:JSON.stringify(addaudit),
				   }).success(function(data, status) 
				   {
						$scope.data=data;
				   }); 
				}
			   else
			    {
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}									
				}
			};
		}) 
		
		//========================================
				//Login User
		//========================================
	    .controller('LoginUser',function($scope,$http)
		{			
			$scope.AddLogin={'status':'pdf'};
			$scope.asd=function()
			{
			};						
		})
		
		
		
		
		//========================================
				//VarietyWise CaneCrushing ShiftsWise
		//========================================
	    .controller('VarietyWiseCaneCrushingShiftsWise',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Variety Wise Cane Crushing Report (Datewise)
		//========================================
	    .controller('VarietyWiseCaneCrushingReport(Datewise)',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Variety Wise Cane Crushing Cumulative Report 
		//========================================
	    .controller('VarietyWiseCaneCrushingCumulativeReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
		//========================================
				//Vehicle TypeWise CaneCrushing ShiftsWise Report 
		//========================================
	    .controller('VehicleTypeWiseCaneCrushingShiftsWise',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
		//==========================================
			 //VehicleTypeWise CaneCrushing Cumulative Report
		//==========================================

	    .controller('VehicleTypeWiseCaneCrushingCumulative',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Circle' }, { GROWER: '1', GROWER: 'Region' }, { GROWER: '2', GROWER: 'Mandal' }, { GROWER: '3', GROWER: 'Village' }, { GROWER: '4', GROWER: 'Zone' }];		
		})
				
		
		
		
		//========================================
				//Daily Log Sheet
		//========================================
	    .controller('DailyLogSheet',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
			//========================================
				//Grower Wise Hourly Crushing Report
		//========================================
	    .controller('GrowerWiseHourlyCrushingReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Transport Deductions CheckList
		//========================================
	    .controller('TransportDeductionsCheckList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Transport Deductions For Cane
		//========================================
	    .controller('TransportDeductionsForCane',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		 $scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		
		})
		
		
		
		
		
		
		
		//========================================
		//Transport Recovery Summary For Cane
		//========================================
	    .controller('TransportRecoverySummaryForCane',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		 $scope.loadAccountingDates=function(season)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
				//alert(data);
				// alert(JSON.stringify(data));
				$scope.ftDates = data;		
			}); 
		};
		})
		
		
		//========================================
				//Village Wise Transport SubsidyList
		//========================================
	    .controller('VillageWiseTransportSubsidyList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
				//========================================
				//Harvesting Contractor Service Provider Rates
		//========================================
	    .controller('HarvestingContractorServiceProviderRates',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
		
		
		//===================Cane Ledger===================
		.controller('CaneLedgerController',function($scope,$http)
		{
			
									
			$index=0;
			var DropDownLoad = new Object();
			var loadLedgerObj=new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			var seasonRyot=new Object();
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {	
					$scope.seasons=data;
			   }); 
		};
		$scope.data = [];
		$scope.addFormField = function() 
		{
			$index++;
		    $scope.data.push({'id':$index})
			
		};
		
		
		$scope.getSeasonRyotCode=function(season)
		{
			
			seasonRyot.season=season;
			
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getCaneWeighmentRyots.html",
					data: JSON.stringify(seasonRyot),
			   }).success(function(data, status) 
			   {
				  
				  
					$scope.ryotCodes=data;
			   }); 
			
		};
		
		
		$scope.CaneLedgerFormSubmit = function(AddCaneLedger,form)
		{
			caneLedgerObj  = new Object();
			caneLedgerObj.season =  $scope.AddCaneLedger.season;
			caneLedgerObj.ryotCode =  $scope.AddCaneLedger.ryotCode;
			
			var supplyQty=0;
			var amountPayableTotal=0;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getCaneLedger.html",
					data:JSON.stringify(caneLedgerObj),
			   }).success(function(data, status) 
			   {
				    $("#dispCaneLedger").show();
				
				 $scope.AddCaneLedger = data.formData[0];
				$scope.AddCaneLedger.ryotCode = data.formData[0].ryotcode;
				//alert(JSON.stringify(data['gridData']))
				
				  $scope.data = data.gridData;
				// alert(data.gridData.length);
				 for(var g=0;g<data.gridData.length;g++)
				 {
					 supplyQty +=data.gridData[g].supQty;
				 }
				   //alert(supplyQty);
				   
				   $scope.AddCaneLedger.supQtyTotal = supplyQty;
				   
				   for(var m=0;m<data.gridData.length;m++)
				 {
					 amountPayableTotal +=data.gridData[m].amountPayable;
				 }
				   //alert(supplyQty);
				   
				   $scope.AddCaneLedger.AmountTotalpayable = amountPayableTotal;
				  
				   
					
			   });
			
			
			//var CaneLedgerDataObj = new Object();
//			CaneLedgerDataObj.formData = AddCaneLedger;
//			
//			
//				generateAddRowData = [];
//			$scope.data.forEach(function (summary) 
//			{
//	    		generateAddRowData.push(angular.toJson(summary));														 
//			});			
//		
//			 CaneLedgerDataObj.gridData = generateAddRowData;
//			 
//			 alert(JSON.stringify(CaneLedgerDataObj));
		
			};
		})		
			
		
		
				
		//========================================
				//Cane Samples Check List
		//========================================
	    .controller('CaneSamplesCheckList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			var ProgramDropDownLoad = new Object();
			ProgramDropDownLoad.dropdownname = "ProgramSummary";
			var StringProgramDropDownSeason= JSON.stringify(ProgramDropDownLoad);
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadProgram = function(seasonCode)
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: seasonCode,
	  		}).success(function(data, status) 
			{
				$scope.ProgramNames=data;
				
			}); 
		};

		})	

		//========================================
				//Ranking Statement Report
		//========================================
	    .controller('RankingStatementReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//Cane Growers List Circle ProgramWise
		//========================================
	    .controller('CaneGrowersListCircleProgramWise',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad1 = new Object();
			DropDownLoad1.dropdownname="Circle";
			var StringDropDown1= JSON.stringify(DropDownLoad1);
		
			var ProgramDropDownLoad = new Object();
			ProgramDropDownLoad.dropdownname = "ProgramSummary";
			var StringProgramDropDownSeason= JSON.stringify(ProgramDropDownLoad);
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadProgram = function(seasonCode)
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: seasonCode,
	  		}).success(function(data, status) 
			{
				$scope.ProgramNames=data;
				
			}); 
		};
		$scope.loadCircle=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown1,
			   }).success(function(data, status) 
			   {	
					data.push({"circle":"all","id":"ALL"});
					$scope.circles=data;
			   }); 
		};

		})


		//========================================
		//Inactive Modified AgreementsList
		//========================================
	    .controller('InactiveModifiedAgreementsList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'status':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadseasonNames=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
		//Harvesting Deductions For Cane AC Period
		//========================================
	    .controller('HarvestingDeductionsForCaneACPeriod',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
		$scope.loadAccountingDates=function(season)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		
		})
		
		//========================================
		//Harvest Recovery Summary For Cane AC 
		//========================================
	    .controller('HarvestRecoverySummaryForCaneAC',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		 $scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		
		
		//========================================
				//Transport Subsidy List For The Crushing
		//========================================
	    .controller('TransportSubsidyListForTheCrushing',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
				//TransportContractorServiceProviderRates
		//========================================
	    .controller('TransportContractorServiceProviderRates',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})	
		
		//========================================
		//Advance Check List Report
//========================================
	    .controller('AdvanceCheckListReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var AdvanceTypeObj = new Object();
			AdvanceTypeObj.dropdownname = "CompanyAdvance";
			var jsonAdvanceTypeString= JSON.stringify(AdvanceTypeObj);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadCompanyAdvance=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonAdvanceTypeString,
			   }).success(function(data, status) 
			   {
				   data.push({"advance":"all","id":"ALL"});
					$scope.advanceType=data;
			   }); 
		};
		
		})
		//========================================
		//Check List For SB Account Portions
		//========================================
	    .controller('CheckListForSBAccountPortions',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			 alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		
		})
		
		//========================================
		//Cane Supply Payment Statement For Cane AC portion
		//========================================
	    .controller('CaneSupplyPaymentStatementForCaneAC',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		

		//========================================
		//VillageWiseIncentiveCanePricePaymentSummary
		//========================================
	    .controller('VillageWiseIncentiveCanePricePaymentSummary',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		//========================================
		//Incentive CanePrice Payment Statement
		//========================================
	    .controller('IncentiveCanePricePaymentStatement',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
											
		//=============================================
	  		//Verifying Loans AsPer Branch
		//=============================================
	  .controller('VerifyingLoansAsPerBranch',function($scope,$http)
		{
			$scope.AddedBranch={'status':'pdf'};

			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Branch";
			var StringDropDown= JSON.stringify(DropDownLoad);

			var DropSeasonDownLoad = new Object();
			DropSeasonDownLoad.dropdownname="Season";
			var StringSeasonDropDown= JSON.stringify(DropSeasonDownLoad);

									
			$scope.loadBranchNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.branchNames=data;
			   }); 
		};	

			$scope.loadseasonNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringSeasonDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   }); 
		};	

		})
		
		
		//========================================
				//CanToFinancialAccounting
		//========================================
	    .controller('CanToFinancialAccounting',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
			
		//=============================================
	  		//Purchase Tax Transfer
	//=============================================	
		
	.controller('purchaseTaxTransfer',function($scope,$http)
	{
		$scope.purchaseTaxOption = function()
		{
			$scope.AddedPurchaseTax={'pTax':'0'};
		};
	
		$scope.PurchaseTaxSubmit = function(AddedPurchaseTax,form)
		{
			//alert(JSON.stringify(AddedPurchaseTax));
			
			if($scope.purchaseTaxTransferForm.$valid)
			{
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				    	url:"/SugarERP/getPurchaseTaxTransfer.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddedPurchaseTax),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
						
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Purchase Tax Transferred!', "success");				
								$('.btn-hide').attr('disabled',false);	
								$scope.AddedPurchaseTax = angular.copy($scope.master);
								$scope.purchaseTaxOption();
							    form.$setPristine(true);			    								
							  //--------load all PurchaseTaxOptions ----------
								var httpRequest = $http({
													method: 'POST',
													url : "/SugarERP/getPurchaseTaxTransfer.html",
													data: {},
												   }).success(function(data, status) 
													{
														$scope.AddedPurchaseTax=data;
													});															  
							  //----------------end---------------------
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			}
			else
			{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
			}
		};
	
	
})	
	//========================================
				//BruntCaneReport
		//========================================
	    .controller('BruntCaneReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
		
		//========================================
				//Cane Suppliers Report
		//========================================
	    .controller('CaneSuppliersReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
			//========================================
				//AccountingClouser
		//========================================
	    .controller('AccountingClouser',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
		
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
					$scope.AddedSeason={'mode':'0'};
			   }); 
			   
		};
		
		$scope.AccountingClouserFormSubmit = function(AddedSeason,form)
		{
			//alert($scope.purchaseTaxTransferForm.$valid);
			
			if($scope.purchaseTaxTransferForm.$valid)
			{
				//alert(JSON.stringify(AddedSeason));
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				    	url:"/SugarERP/getPurchaseTaxTransfer.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddedSeason),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
						
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Accounting Clouser Done!', "success");				
								$('.btn-hide').attr('disabled',false);	
								$scope.AddedSeason = angular.copy($scope.master);
								$scope.loadSeason();
							    form.$setPristine(true);			    								
							  
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			}
			else
			{
					var field = null, firstError = null;
	                for (field in form) 
					{
                    	if (field[0] != '$') 
						{
                        	if (firstError === null && !form[field].$valid) 
							{
                            	firstError = form[field].$name;
	                        }
	                        if (form[field].$pristine) 
							{
                            	form[field].$dirty = true;
	                        }
    	                }	
					}
			}
		};
	
		})
		
		
			//========================================
				//VehicleNumWiseTrptDtls
		//========================================
	    .controller('VehicleNumWiseTrptDtls',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadNumberbyVehicle=function(season)
		 {
			 var loadVehicleFilterObj = new Object();
			 loadVehicleFilterObj.season = season;
			// alert(JSON.stringify(loadVehicleFilterObj));
			 
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getVehicleNumbers.html",
					data: JSON.stringify(loadVehicleFilterObj),
			   }).success(function(data, status) 
			   {
				   $scope.vehiclenums=data;
				
			   });
		};
		



		
		})
			
		//========================================
				//Zonewise weighment Report
		//========================================
	    .controller('zonewiseweighmentController',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var seasonStringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Zone";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
		
			var fieldDropDown = new Object();
			fieldDropDown.dropdownname = "Circle";
			var StringDropDown= JSON.stringify(fieldDropDown);
			
			var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			

		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: seasonStringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadZoneNames = function()
			{
				var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: jsonStringDropDown,
							 }).success(function(data, status) 
							 {
								$scope.ZoneData=data;
								 data.push({"id":0,"zone":"ALL ZONES"});
								 $scope.AddedSeason.zoneCode = 0;  //MODIFIED BY SAHADEVA
								
							 });  	   
			};
			
			$scope.loadVarietyNames = function()
		 {
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: VarietyNameString,
					}).success(function(data, status) 
					{			
					
						$scope.varietyNames = data;
						data.push({"id":0,"variety":"ALL VARIETY"});
						 $scope.AddedSeason.varietyOfCane = 0; //MODIFIED BY SAHADEVA
						
					});  	   					
		};
		

			  
			  $scope.loadRyotCodeDropDown = function()
				{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							
							//$scope.ryotNames = data;
							
							//data.push({"id":0,"ryotname":"ALL"});
							
							
							
						});  	   
				};
				
				$scope.getCircleNmae=function(zoneid){
					var zoneObj=new Object();
					zoneObj.zone=zoneid
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getCirclesByZone.html",
							data: JSON.stringify(zoneObj),
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							 data.push({"circlecode":0,"circle":"ALL CIRCLES"});
							$scope.circleNames = data;
							 $scope.AddedSeason.circle = 0; //MODIFIED BY SAHADEVA
							
						});  
					
				}
				
				$scope.loadCircleCodeDropDown = function(circleid)
				{
					var circleObj=new Object();
					circleObj.circle=circleid;
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getRyotFromCircle.html",
							data: JSON.stringify(circleObj),
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							
							data.push({"ryotcode":0,"ryotname":"ALL RYOTS"});
							$scope.ryotNames = data;
							 $scope.AddedSeason.ryotcode = 0; //MODIFIED BY SAHADEVA
							
							
							
						});  	   
				};
		})

		//========================================
		//Shift Wise Cane Crushed 
		//========================================
	    .controller('ShiftWiseCaneCrushed',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		

//========================================
		//HarvestContractorWiseRyot 
		//========================================
	    .controller('HarvestContractorWiseRyot',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			
			var DropDownLoadContractor = new Object();
			DropDownLoadContractor.dropdownname="HarvestingContractors";
			var StringDropDownContractor= JSON.stringify(DropDownLoadContractor);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   { 
			      
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadHarvestingContractors=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownContractor,
			   }).success(function(data, status) 
			   {
				   data.push({"id":0,"harvester":"ALL"});
				   $scope.contractorNames = data;
				   //alert(JSON.stringify(data));
					//$scope.seasons=data;
			   }); 
		};
		})
		
	//==========================================
	// Area Wise Pending Report
	//==========================================

	    .controller('AreaWisePendingReport',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};	
			
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Circle' },{ GROWER: '4', GROWER: 'Zone' }];		
		})
	
			
		//=====================Change Password=======================//
		 .controller('resetPasswordController',function($scope,$http)
		{
			$scope.master = {};
			
		
			$scope.addedChangeresetSubmit = function(AddedChange,form)
			{
				 if($scope.resetpasswordForm.$valid) 
			  	{	
			
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/resetPassword.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(AddedChange),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", 'Password reset Successfully!', "success"); 
							$('.btn-hide').attr('disabled',false);
							form.$setPristine(true);
						 }
						else
						 {
						   sweetAlert("Oops...", "Please check login id and try again!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
			  }
			  else
				{
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
				}
			};
			
			
			  $scope.reset = function(form)
		   	{			
		
				$scope.AddedChange = angular.copy($scope.master);
					//$scope.resetusername();
				form.$setPristine(true);
				
				$(".btn-hide").attr('disabled',false);
		   	};
		})
		 
		 //=====================Change Password=======================//
		 .controller('changePasswordController',function($scope,$http)
		{
			$scope.loginUsername = function()
			{
				var userlogin = sessionStorage.getItem('username');
				
				//$scope.AddedChange.username = userlogin;
				$scope.AddedChange = {'username':userlogin};
			};
			$scope.master = {};
			
		
			$scope.addedChangePasswordSubmit = function(AddedChange,form)
			{
				//alert(JSON.stringify(AddedChange));
				 if($scope.changepasswordForm.$valid) 
			  	{	
			
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/changePassword.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(AddedChange),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Good job!", 'Password changed Successfully!', "success"); 
							$('.btn-hide').attr('disabled',false);
							form.$setPristine(true);
							location.reload(1000);
						 }
						else
						 {
						   sweetAlert("Oops...", "Please check login id and try again!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
			  }
			  else
				{
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
				}
			};
			
			
			  //$scope.reset = function(form)
//		   	{			
//		
//				$scope.AddedChange = angular.copy($scope.master);
//					//$scope.resetusername();
//				form.$setPristine(true);
//				
//				$(".btn-hide").attr('disabled',false);
//				var userloginame = sessionStorage.getItem('username');
//				$scope.AddedChange = {'username':userloginame};
//				alert(userloginame);
//				$scope.$apply();
//				//$scope.loginUsername();
//		   	};
		})
	
		//========================================
		//TransportContractorWiseRyot 
		//========================================
	    .controller('TransportContractorWiseRyot',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			
			var DropDownLoadContractor = new Object();
			DropDownLoadContractor.dropdownname="TransportingContractors";
			var StringDropDownContractor= JSON.stringify(DropDownLoadContractor);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   { 
			      
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadTransporterContractors=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownContractor,
			   }).success(function(data, status) 
			   {
				   data.push({"id":0,"transporter":"ALL"});
				   $scope.TransporterNames = data;
				   //alert(JSON.stringify(data));
					//$scope.seasons=data;
			   }); 
		};
			
		})

		//========================================
				//DeductionCheckList
		//========================================
	    .controller('DeductionCheckList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		
		})
	



//========================================
		//Loan Recovery Report
	//========================================
	    .controller('LoanRecoveryReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad1 = new Object();
 			DropDownLoad1.dropdownname = "Branch";
			var jsonStringBranch= JSON.stringify(DropDownLoad1);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		$scope.loadBranchNames = function()
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringBranch,
			  }).success(function(data, status) 
			{
			//alert(JSON.stringify(data));
			$scope.branchNames=data;
			data.push({"branchname":"ALL","id":"ALL"});
				
			}); 
			};
		
		})
		


//========================================
		//Cane Price Payments Statement
	//========================================
	    .controller('CanePricePaymentsStatement',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad1 = new Object();
 			DropDownLoad1.dropdownname = "Branch";
			var jsonStringBranch= JSON.stringify(DropDownLoad1);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		$scope.loadBranchNames = function()
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringBranch,
			  }).success(function(data, status) 
			{
			//alert(JSON.stringify(data));
			$scope.branchNames=data;
			data.push({"branchname":"ALL","id":"ALL"});
			}); 
			};
		
		})
		

//========================================
		//CanePaymentVoucher
		//========================================
	    .controller('CanePaymentVoucher',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		

	//========================================
	//Bank Wise Growers Loan Collection For Cane AC
	//========================================
	    .controller('BankWiseGrowersLoanCollectionForCaneAC',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		
		
		//========================================
		//Bank Wise Payment To Ryot SB ACs For Cane AC
		//========================================
	    .controller('BankWisePaymentToRyotSBACsForCaneAC',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
	
				//========================================
		//CanePaymentReport
		//========================================
		.controller('CanePaymentReport',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		
		
		
		//========================================
		//CaneAccountCheckList
		//========================================
		.controller('CaneAccountCheckList',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
			$scope.loadAccountingDates=function(season)
			{
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getCaneAccountingPeriods.html",
				data: season,
				
				}).success(function(data, status) 
				{	
				//alert(data);
				// alert(JSON.stringify(data));
					$scope.ftDates = data;
								
				}); 
			};
		})

		//========================================
		//CaneAccountCheckListSample
		//========================================
		.controller('CaneAccountCheckListSample',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
			$scope.loadAccountingDates=function(season)
			{
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getCaneAccountingPeriodsTemp.html",
				data: season,
				
				}).success(function(data, status) 
				{	
				//alert(data);
				// alert(JSON.stringify(data));
					$scope.ftDates = data;
								
				}); 
			};
		})
				//========================================
				//CaneToFinancialAccounting
		//========================================
	    .controller('CaneToFinancialAccounting',function($scope,$http)
		{
			
			$scope.AddedSeason={'mode':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad1 = new Object();
 			DropDownLoad1.dropdownname = "Branch";
			var jsonStringBranch= JSON.stringify(DropDownLoad1);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			
				$scope.ftDates = data;
							
			}); 
		};
		$scope.loadBranchNames = function()
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringBranch,
			  }).success(function(data, status) 
			{
			
			$scope.branchNames=data;
			}); 
			};
			
			$scope.getcanetoFa = function(season,ftDate)
			{
				var getcanetoFaObj = new Object();
				getcanetoFaObj.season = season;
				getcanetoFaObj.ftDate = ftDate;
				//alert(JSON.stringify(getcanetoFaObj));
				
				var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneToFA.html",
			data: JSON.stringify(getcanetoFaObj),
			
			}).success(function(data, status) 
			{	
			$("#hidecanetofatable").show();
			 //alert(JSON.stringify(data));
				$scope.canetofArray = data;
							
			}); 
			};
			$scope.exportcanetoFaexcel = function()
			{
				alasql('SELECT TCOMP,TDB,TDOC,TSERIAL,TDT,TDES,TGEN,TPARTY,TAMOUNT,TDC,TQTY,TUNIT_PRIC,TADB,TCON,TPROD,TPROD,TREF_NO,REMARKS INTO XLSX("CanetoFA.xlsx",{headers:true}) FROM ?',[$scope.canetofArray]);
			};
		})
		
		
		//========================================
		//PaymentCheckList
		//========================================
	    .controller('PaymentCheckList',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
		//RyotWiseWeighmentDetail
		//========================================
	    .controller('RyotWiseWeighmentDetail',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
			$scope.loadryotCode = function(season)
			{				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadWeighmentRyots.html",
						data: season,
					  }).success(function(data, status) 
					  {
						$scope.ryotCodeData=data;
						//data.push({"ryotcode":"ALL","id":"ALL"});
					  }); 
	  	    };
		
		})	
		


		
		//========================================
		//TransportContractorRyotCumulative 
		//========================================
	    .controller('TransportContractorRyotCumulative',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			
			var DropDownLoadContractor = new Object();
			DropDownLoadContractor.dropdownname="TransportingContractors";
			var StringDropDownContractor= JSON.stringify(DropDownLoadContractor);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   { 
			      
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadTransporterContractors=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownContractor,
			   }).success(function(data, status) 
			   {
				   data.push({"id":0,"transporter":"ALL"});
				   $scope.TransporterNames = data;
				   //alert(JSON.stringify(data));
					//$scope.seasons=data;
			   }); 
		};
			
		})
		
		//========================================
		//CaneSupplyCheckListForWeek
		//========================================
	    .controller('CaneSupplyCheckListForWeek',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		
		
		//========================================
		//CaneLedgerReport
		//========================================
		.controller('CaneLedgerReport',function($scope,$http)
		{
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		$scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		
		//========================================
		//CaneToFAReport
		//========================================
	    .controller('CaneToFAReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		 $scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//alert(data);
			// alert(JSON.stringify(data));
				$scope.ftDates = data;
							
			}); 
		};
		})
		//========================================
		//HarvestContractorWiseRyotCumulative 
		//========================================
	    .controller('HarvestContractorWiseRyotCumulative',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			
			var DropDownLoadContractor = new Object();
			DropDownLoadContractor.dropdownname="HarvestingContractors";
			var StringDropDownContractor= JSON.stringify(DropDownLoadContractor);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   { 
			      
					$scope.seasons=data;
			   }); 
		};
		
		$scope.loadHarvestingContractors=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownContractor,
			   }).success(function(data, status) 
			   {
				   data.push({"id":0,"harvester":"ALL"});
				   $scope.contractorNames = data;
				   //alert(JSON.stringify(data));
					//$scope.seasons=data;
			   }); 
		};
		})
		
		//CaneSupplyPaymentStatement
		//========================================
	    .controller('CaneSupplyPaymentStatement',function($scope,$http)
		{
			$scope.UpdateLoanData = [];
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'Excel'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		$scope.generateReport = function(season)
		{
			 //alert(season);
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/caneSupplyAndPaymentStatementarray.html",
					data: season,
			   }).success(function(data, status) 
			   {
				   alert(JSON.stringify(data));
					$scope.UpdateLoanData=data;
					
					$scope.exportData();
					
			   });
			
		};
		
		$scope.exportData = function()
		{
			alasql('SELECT Ryotcode,Ryotname,zone,circle,village,Canesupply,Canevalue,ul,cdc,transport,harvesting,otherded,Lprinciple,finalinterest,advance,paidamt,advisedamt,sbacTransfer,sbpaidamt,balancedue,Payamantresourcebank INTO XLSX("CaneSupplyPaymentStatement.xlsx",{headers:true}) FROM ?',[$scope.UpdateLoanData]);
		};
		})
		
		
		//  BankLoanDetailsController
		
		 .controller('bankLoanDetailsController',function($scope,$http)
		{
			$scope.bankLoanData=[];
			$scope.master = {};
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.SeasonData=data;
					//$scope.AddedBankLoanDetails = {'modifyFlag':'No'};
			   }); 
			};
			
			$scope.loadLoanDetails = function()
			{
			        $scope.loanDetailss=[{'id':1,'loanDetails':'ALL'},{'id':2,'loanDetails':'LOAN' },{'id':3,'loanDetails':'ADVANCE' }]
			
			};
				
			$scope.updateData=function(AddedBankLoanDetails){
				
	
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAdvanceAndLoanData.html",
						data: JSON.stringify(AddedBankLoanDetails),
						beforeSend: function(xhr) 
				{
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				}
				
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$("#hidetable").show("slow");
						$scope.bankLoanData = data;
						
					});
			
		
		 };

		$scope.exportData = function()
		{
			alasql('SELECT ryotCode,ryotName,village,Quantity,loanNo,loanREfNo,TotalAmount,intrestAmt,TotalRecovery,balance INTO XLSX("BankLoanDetails.xlsx",{headers:true}) FROM ?',[$scope.bankLoanData]);
		};		 
				
			
		})
		//==========================================
			 //Bank Loan Data Pdf Formate
		//==========================================

	    .controller('BankLoanDataPdfFormate',function($scope,$http)
		{
		
			$scope.AddedMandal={'status':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadseasonNames=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasonNames=data;
			   }); 
		};
			//$scope.GROWER = [{ GROWER: '1', GROWER: 'Advance' },{ GROWER: '2', GROWER: 'Loan' }];		
			$scope.GROWER = [{ GROWER: '0', GROWER: 'All' }, { GROWER: '1', GROWER: 'Advance' },{ GROWER: '2', GROWER: 'Loan' }];		
		})
		
		
		//========================================
				//AuthorisationAckDetailsReport
		//========================================
	    .controller('AuthorisationAckDetailsReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//ProcurementReport
		//========================================
	    .controller('ProcurementReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
			};
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Seedling Nursary' }, { GROWER: '1', GROWER: 'Seed Supply' }];	
		})
		//========================================
				//SeedDistrubution
		//========================================
	    .controller('SeedDistrubution',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//SeedProductionReport
		//========================================
	    .controller('SeedProductionReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//SeedlingProcessReport
		//========================================
	    .controller('SeedlingProcessReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//Yearly Ledger Report
		//========================================
	    .controller('YearlyLedgerReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//UnloadContractor Unloading Report
		//========================================
	    .controller('UnloadContractorReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		})
		
		//========================================
				//SeedUtilizationReport
		//========================================
	    .controller('SeedUtilizationReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		})
		
		//========================================
				//SeedDevlopmentReport
		//========================================
	    .controller('SeedDevlopmentReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		})
		//========================================
				//VegetableFrootsProductionReport
		//========================================
	    .controller('VegetableFrootsProductionReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		})
		//========================================
				//SeedUtilizationReportZoneWise
		//========================================
	    .controller('SeedUtilizationReportZoneWise',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
		
		})
			
		//==========================================
			 //Pending Loans And Advances  Report
		//==========================================
	/*
	    .controller('PendingLoansAndAdvances',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};
			$scope.PendingType = [{ PendingType: 'ADVANCES', PendingType: 'ADVANCES' }, { PendingType: 'LOANS', PendingType: 'LOANS' }];		
		})
		*/
		
		//========================================
		//UpdatePayment Report
		//========================================
	    .controller('UpdatePaymentsReport',function($scope,$http)
		{
			//alert(123);
			//var filterObj = new Object();
			$scope.AddedSeason={'mode':'PDF'};
				
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
				   
					$scope.seasons=data;
					
			   }); 
		   };
		   $scope.loadAccountingDates=function(season)
		{
		
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneAccountingPeriods.html",
			data: season,
			
			}).success(function(data, status) 
			{	
			//$scope.ftDates={"id":0,"foCode":"All"};
			 data.push({"id":0,"foCode":"All"});
				$scope.ftDates = data;
							
			}); 
		};
		})
	  //=================================
	  		//Floating Lables Demo
	  //=================================
	  
	 .directive('withFloatingLabel', function () 
	 {
	    return {
    	    restrict: 'A',
        	link: function ($scope, $element, attrs) {
            var template = '<div class="floating-label">' + attrs.placeholder +'</div>';

            //append floating label template
            $element.after(template);

            //remove placeholder
            $element.removeAttr('placeholder');

            //hide label tag assotiated with given input
            document.querySelector('label[for="' +  attrs.id +  '"]').style.display = 'none';

            $scope.$watch(function () {
                if($element.val().toString().length < 1) {
                    $element.addClass('empty');
                } else {
                    $element.removeClass('empty');
                }
            });
	        }
    	};
	});	  
	
	
	  
	  
	  
	  
	  
	  
	  

	  
	