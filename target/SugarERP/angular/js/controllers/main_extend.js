materialAdmin

	//==================================
			//Generate Agreement
	//==================================	
	.controller('GenerateAgreement',function($scope,$http)
	{
								
		 var SerialNumberObj = new Object();
		 
			
		 SerialNumberObj.tablename = "AgreementSummary";
		 SerialNumberObj.col1 = "agreementseqno";
		 SerialNumberObj.col2 = "seasonyear";	 	 
		 
		 var jsonSerialNumberString= JSON.stringify(SerialNumberObj);

		$scope.master = {};
        $scope.setRowAfterReset = function()
		{
		    $scope.data = {};
			$index=1;
			$scope.data = [{'plotSeqNo':$index,'plotNumber':$index,'screenName':'GENERATE AGREEMENT'}]
			//$scope.AddAgreement = {'hasFamilyGroup':0,'status':0,'modifyFlag':'No'};
		};
		
		
		 //--------Load Server Date--------
			$scope.loadServerDate = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						//alert(JSON.stringify(data));	
						$scope.AddAgreement = {'agreementNumber':data.id,'hasFamilyGroup':0,'status':0,'modifyFlag':'No','screenName':'GENERATE AGREEMENT','typeOfAgreement':'1'};
						$scope.AddAgreement.agreementDate = data.serverdate;
					    $scope.data = {};
						$index=1;
						$scope.data = [{'plotSeqNo':$index,'plotNumber':$index,'screenName':'GENERATE AGREEMENT'}]
						//alert($scope.AddAgreement.typeOfAgreement);
					var typeofAgreement = $scope.AddAgreement.typeOfAgreement;
				
					
					


				 	 });	
			};
		
		//-----------Load Cane Accounting Serial Number--------	
		$scope.loadAgreementSeqNumberBySeason = function(season)
		{	
			var Agreementnumber="";
		//if type = 0, tablename = RandDAgreementSummary, 1 = AgreementSummary
			SerialNumberObj.value = season;
			//alert("onload"+$scope.AddAgreement.typeOfAgreement);
			if($scope.AddAgreement.typeOfAgreement=='0')
			{
			SerialNumberObj.tablename = "RandDAgreementSummary";
			}
			else
			{
				SerialNumberObj.tablename = "AgreementSummary";
			}
			
			//alert(JSON.stringify(SerialNumberObj));
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxColumnValue.html",
			data: JSON.stringify(SerialNumberObj),
			}).success(function(data, status) 
			{
				$scope.tempAgreement = data.id;
				var seasonSplit = season.split("-");
				var year1 = seasonSplit[0].slice(-2);
				var year2 = seasonSplit[1].slice(-2);
				var Seasonyear = year1+"-"+year2;
				if($scope.AddAgreement.typeOfAgreement=='0' )
				{//alert('t3');
					 Agreementnumber = "T"+data.id+"/"+Seasonyear;
				}
				else
				{//alert('4');
					 Agreementnumber = data.id+"/"+Seasonyear;
				}
				//alert(Agreementnumber);
				$scope.AddAgreement.agreementNumber = Agreementnumber;

			}); 
		};
				
		$index = 0;		
		$scope.data = [];
		$scope.AddAgreement = [];
		var agreementGridData = [];
		var AddAgreementObj = new Object();
		var LoadAgreementObj = new Object();
		var RatoonObj = new Object();
		var RatoonChangeObj = new Object();
		
		

		var seasonObj = new Object();
		seasonObj.dropdownname = "Season";
		var jsonSeasonString= JSON.stringify(seasonObj);

		var ryotExtObj = new Object();
		ryotExtObj.dropdownname = "ExtentDetails";
		var jsonRyotExtString= JSON.stringify(ryotExtObj);
		
		var ryotObj = new Object();
		ryotObj.dropdownname = "Ryot";
		var jsonRyotString= JSON.stringify(ryotObj);
		
		var setUpDetails = new Object();
		setUpDetails.tablename = "SetupDetails";
		setUpDetails.columnname = "value";
		setUpDetails.wherecolumnname = "configsc";
		setUpDetails.value = 'lrm';
		var jsonsetUpDetails= JSON.stringify(setUpDetails);		
		

		var bankObj = new Object();
		bankObj.dropdownname = "Branch";
		var jsonBankString= JSON.stringify(bankObj);

		var varietyObj = new Object();
		varietyObj.dropdownname = "Variety";
		var jsonVarietyString= JSON.stringify(varietyObj);

		var landTypeObj = new Object();
		landTypeObj.dropdownname = "LandType";
		var jsonLandTypeString= JSON.stringify(landTypeObj);

		var cropTypeObj = new Object();
		cropTypeObj.dropdownname = "CropTypes";
		var jsonCropTypeString= JSON.stringify(cropTypeObj);

		var CircleDropDown = new Object();
		CircleDropDown.dropdownname = "Circle";
		var jsonCircleDropDown= JSON.stringify(CircleDropDown);

		var MandalDropDown = new Object();
		MandalDropDown.dropdownname = "Mandal";
		var jsonMandalDropDown= JSON.stringify(MandalDropDown);
		
		var VillageDropDown = new Object();
		VillageDropDown.dropdownname = "Village";
		var jsonVillageDropDown= JSON.stringify(VillageDropDown);
		

		var FamilyDropDown = new Object();
		FamilyDropDown.dropdownname = "FamilyGroup";
		var jsonFamilyDropDown= JSON.stringify(FamilyDropDown);

		var AddedAgreementDropDown = new Object();
		AddedAgreementDropDown.tablename = "AgreementSummary";
		AddedAgreementDropDown.columnName = "agreementseqno";
		AddedAgreementDropDown.columnName1 = "agreementnumber";
		var jsonAddedAgreementDropDown= JSON.stringify(AddedAgreementDropDown);

		var agreementNumber = new Object();
		agreementNumber.tablename = "AgreementSummary";
		var jsonAgreementString= JSON.stringify(agreementNumber);
		
		
		var FieldOffDownLoad = new Object();
		FieldOffDownLoad.dropdownname = "FieldOfficer";
		var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);


			
		var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
		var directionDisplay;
		//alert(directionDisplay);
		var directionsService = new google.maps.DirectionsService();
		//alert(directionsService);
		var map;
		//alert(map);
		
		//-------------Load Field Assistants-------
		$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};	
		$scope.loadFieldOfficerDropdown = function()
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: FieldOffDropDown,
			  		 }).success(function(data, status) 
			   			{	
							$scope.FieldOffNames=data;
	 		   			});  	   
					};
		
			$scope.addAgreemntRows = function()
		{
			$index++;
			$scope.data.push({'plotSeqNo':$index,'plotNumber':$index,'screenName':'GENERATE AGREEMENT'});			
		};
		
		$scope.getInitialize = function()
		{
			
			directionsDisplay = new google.maps.DirectionsRenderer();
			//alert(directionDisplay);
			var melbourne = new google.maps.LatLng(-37.813187, 144.96298);
			//alert(melbourne);
			var myOptions = {
				zoom:12,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: melbourne
			}

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			
			directionsDisplay.setMap(map);
			//alert("map"+map);
			
			
			
		};
		/*
		$scope.calcRoute = function(fromdist,distance,id)
		{
			
			var sugars = "Sarvaraya Sugars";
			var start = $("#start"+id).val(sugars);
			//alert("start"+sugars);
			
			var end = $("#villageName"+id).val();
			//alert("end"+end);
			//var distanceInput = document.getElementById("distance");
			//alert("distance"+distanceInput);
			var distanceInput = distance;
			//alert("distance"+distanceInput);
			
			var request = {
				origin:sugars, 
				destination:end,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};
				//alert("Request"+request);
			directionsService.route(request, function(response, status) {
													  //alert("status"+status);
													  //alert(google.maps.DirectionsStatus.OK);
				//if (status == google.maps.DirectionsStatus.OK) {
						//if (status == REQUEST_DENIED) {
					directionsDisplay.setDirections(response);
					//alert("directionDisplay"+directionsDisplay.setDirections(response));
					//alert("response"+response.routes[0].legs[0].distance.value / 1000);
					$scope.data[id].plotDistance = response.routes[0].legs[0].distance.value / 1000;
					$scope.$apply();
					//alert("Km"+distanceInput.value);
				//}
			});
			//alert(distanceInput.value);
		};
		*/
		//--------set Surety Ryot Rquired Status---
		$scope.setSuretyRequired = function(id)
		{
			if(id!=0)
			{
				$scope.suretyStatus = true;
			}
		   else
		    {
				$scope.suretyStatus = false;
			}
		};
		
		//---Change FamilyGroup Status---------
		$scope.changeFamilyGroupStatus = function(familyValue)
		{
		     if(familyValue == '1')
			 {
				 $scope.familyGroupValid = false;
		     }else
			 {
			     $scope.familyGroupValid = true;
			 }
		};
		$scope.plotIdentity = [{ id: 0, plotIdentity: 'Commercial' }, { id: 1, plotIdentity: 'Seed Supply' }];
		
		$scope.ShowDatePicker = function()
		{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		$scope.$applyAsync(function()
		{
	     	$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function(){ setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); }});
			
		})


		

			$scope.updateAgmtNobytypeOfAgrmt = function(season,agreementType)
			{
				var Agreementnumber="";
				SerialNumberObj.value = season;
			//alert("onload"+$scope.AddAgreement.typeOfAgreement);
			if($scope.AddAgreement.typeOfAgreement=='0')
			{
			SerialNumberObj.tablename = "RandDAgreementSummary";
			}
			else
			{
				SerialNumberObj.tablename = "AgreementSummary";
			}
			
			//alert(JSON.stringify(SerialNumberObj));
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxColumnValue.html",
			data: JSON.stringify(SerialNumberObj),
			}).success(function(data, status) 
			{
				$scope.tempAgreement = data.id;
				var seasonSplit = season.split("-");
				var year1 = seasonSplit[0].slice(-2);
				var year2 = seasonSplit[1].slice(-2);
				var Seasonyear = year1+"-"+year2;
				if($scope.AddAgreement.typeOfAgreement=='0' && $scope.AddAgreement.seasonYear!=null)
				{//alert('t1');
				 Agreementnumber = "T"+data.id+"/"+Seasonyear;
				}
				else
				{//alert('2');
				 Agreementnumber = data.id+"/"+Seasonyear;
				}
				//alert(Agreementnumber);
				$scope.AddAgreement.agreementNumber = Agreementnumber;

			});
			};
			


		
		

		//--------Load setup Details---
		
		$scope.loadSetupDetails = function()
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/onChangeFieldValues.html",
				data: jsonsetUpDetails,
			   }).success(function(data, status) 
				{
					$scope.setupStatus = data.value;
				}); 
		};

		//--------Load Added Agreement Details---
		
		


		//--------Load Village DropDown---
		
		$scope.loadVillageNames = function()
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.villageNames=data;
				}); 
		};

		//--------Load Circle Dropdown----
		$scope.loadCircleNames = function()
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCircleDropDown,
			   }).success(function(data, status) 
				{
					$scope.circleNames=data;
					//$scope.dropDisabled = true;
				}); 
		};
	//added by naidu on 31-03-2017
	
		$scope.loadZoneData = function(circlecode)
		{
			alert(circlecode);
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getZonesForCircle.html",
				data: circlecode,
			   }).success(function(data, status) 
				{
					alert(data[0].id+""+JSON.stringify(data));
					$scope.AddAgreement.zoneCode=data[0].id;
					$scope.AddAgreement.zoneName=data[0].zone;
					
				}); 
			};
		
		//--------Load Circle Dropdown----
		$scope.loadFamilyNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonFamilyDropDown,
			   }).success(function(data, status) 
				{
					$scope.FamilyNames=data;
				}); 
			};

		//--------Load Mandal Dropdown----
		$scope.loadMandalNames = function()
		{
			$scope.dropDisabled = "true";
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonMandalDropDown,
			   }).success(function(data, status) 
				{
					$scope.mandalNames=data;
					
				}); 
				
			};

		//--------Load Season Dropdown----
		$scope.loadSeasonNames = function()
		{
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonSeasonString,
			   }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
					
				}); 
			};
			
		//--------Load RyotCode Dropdown----
		$scope.loadRyotNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonRyotString,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.ryotNames=data;
				}); 
			};
		//--------Load Surety Ryot Code Dropdown----
		$scope.loadSuretyRyotNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForSurietyRyot.html",
				data: jsonRyotString,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.suretyRyotNames=data;
				}); 
			};
			
			
		//--------Load RyotCode From Extent Dropdown----
		$scope.loadRyotExtentNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonRyotExtString,
			   }).success(function(data, status) 
				{
					
					$scope.ryotNamesExtent=data;
				}); 
			};
			
			
		//--------Load Bank Dropdown----
		$scope.loadBankNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonBankString,
			   }).success(function(data, status) 
				{
					$scope.bankNames=data;
				}); 
			};

		//--------Load Variety Dropdown----
		$scope.loadVarietyNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonVarietyString,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.varietyNames=data;
				}); 
		};
		//--------Load LandType Dropdown----
		$scope.loadLandType = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonLandTypeString,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.landTypeNames=data;
				}); 
		};
		//--------Load CropType Dropdown----
		$scope.loadCropType = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCropTypeString,
			   }).success(function(data, status) 
				{			
					$scope.cropTypeNames=data;
				}); 
		};
		
		

		//-----------Load Agreement Number---
		
		$scope.AddAgreement = {'hasFamilyGroup':0,'status':0,'modifyFlag':'No'};
		
		//---------Add Agreement Row------
	
		//---------Delete Agreement Row------
		$scope.removeAgreemntRows = function(id)
		{
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{	
				if( comArr[i].plotSeqNo === id ) 
				{
					Index = i;
					break;
				}				
			}
			
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].plotSeqNo = Number(s)+Number(1);
				$scope.data[s].plotNumber = Number(s)+Number(1);
			}
		};
		//---------Show Survey Dialog------
		$scope.showSurveyDialog = function(plotSeqNo,plantOrRatoon)
		{
			if($scope.setupStatus=='0')
			{
				if(plantOrRatoon!=null)
				{
					$('.popupbox').fadeIn("slow");
					if($scope.AddAgreement.ryotCode!="")
					{
						$scope.ryotCodePop = $scope.AddAgreement.ryotCode;
						$scope.loadAddedExtentDetails($scope.AddAgreement.ryotCode);
					}
					$scope.PlotNumberPop = plotSeqNo;
				}
			}
		};
		
		//--------------Show Ratoon Dialog------
		$scope.showRatoonDialog = function(season,ryotCode,ratoon,index)
		{
			if(ratoon!='1' && ratoon!=null)
			{
				RatoonObj.season = season;
				RatoonObj.ryotCode = ryotCode;
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getPreviousAgreementsForRatoon.html",
						data: JSON.stringify(RatoonObj),
					 }).success(function(data, status) 
					 {	
						$scope.ryotCodeRatoon = ryotCode;
						$scope.ryotNameRatoon = ryotCode;
						$scope.currentIndex = index;
						$('.popupbox_ratoon').fadeIn("slow");
						$scope.RatoonExtentData = data;
						
					 }); 																	
			}
		};
		//---------Close Dialog Ratoon------		
		$scope.CloseDialogRatoon = function()
		{
			$('.popupbox_ratoon').fadeOut("slow");
		};		
		//---------Close Survey Dialog------
		$scope.CloseDialog = function()
		{
			$('.popupbox').fadeOut("slow");
		};
		//-----Close Survey Dialog Esc------
		window.onkeydown = function( event ) 
		{
		    if ( event.keyCode === 27 ) 
			{
        		$('.popupbox').fadeOut("slow");
		    }
		};		
		//-----Close Ratoon Dialog Esc------
		window.onkeydown = function( event ) 
		{
		    if ( event.keyCode === 27 ) 
			{
        		$('.popupbox_ratoon').fadeOut("slow");
		    }
		};		
		
		//---------------Get Ratoon details on radion checked--------
		$scope.getRatoonDetails = function(meanDate)
		{
			$scope.data[$scope.currentIndex].cropDate = meanDate;
		};
		
		//--------------load Added Ratoon Details on Ryot Code onchange in popup---
		
		$scope.loadAddedRatoonDetails = function(ryotCode)
		{
			RatoonChangeObj.season = $scope.AddAgreement.seasonYear;
			RatoonChangeObj.ryotCode = ryotCode;
			
			$scope.ryotNameRatoon = ryotCode;

			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getPreviousAgreementsForRatoon.html",
					data: JSON.stringify(RatoonChangeObj),
				 }).success(function(data, status) 
				 {							 	
					$scope.RatoonExtentData = data;					
				}); 																	

		};
		
		
		//---------Load Ryot Details on RyotCode Change--
		
		$scope.loadTyotDetails = function(ryotCode)
		{			
			var season = $scope.AddAgreement.seasonYear;
			var agreementNo = $scope.AddAgreement.agreementNumber;
			if(season==null) { season = ""; }
			
			sessionStorage.setItem('season',season);
			sessionStorage.setItem('agreementNo',agreementNo);
			
			$scope.dropDisabled = 'true';
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllRyots.html",
				data: ryotCode,
			   }).success(function(data, status) 
				{
					
					$scope.AddAgreement.mandalCode = data.ryot.mandalCode;
					$scope.AddAgreement.salutation = data.ryot.salutation;
					$scope.AddAgreement.relativeName = data.ryot.relativeName;
				
					

					if(sessionStorage.getItem('season')!='')
					{
						$scope.AddAgreement.seasonYear = sessionStorage.getItem('season');
					}
					
					//$scope.AddAgreement.agreementNumber = sessionStorage.getItem('agreementNo');
					
				}); 						
		};	
		
		
		
		
		
		
		
		
		
		
		
		//---------Load Ryot Details on RyotCode Change--
		
		$scope.loadRyotDet = function(RyotCode)
		{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/GetRyotNameForCode.html",
			data: RyotCode,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.AddAgreement.ryotName = data[0].ryotName;
			});
			
			
		};
		
		//----------Set Agreement Number-----
		/*$scope.setAgreementNumber = function(seasonYear)
		{
			if(seasonYear!=undefined)
			{
				sessionStorage.setItem('season',seasonYear);
				var Agreementnumber = $scope.tempAgreement+"/"+seasonYear;
				$scope.AddAgreement.agreementNumber = Agreementnumber;
			}
		   else
		    {
				var Agreementnumber = $scope.tempAgreement;
				$scope.AddAgreement.agreementNumber = Agreementnumber;				
			}
			//alert(Agreementnumber);
		};*/

		
		$scope.setAddAgreement = function(seasonYear,typeOfAgreement)
		{
			var addAgreementObj = new Object();
				addAgreementObj.season = seasonYear;
			var tablename="";
			seasonYearValue = seasonYear;		
			if($scope.AddAgreement.typeOfAgreement=='0')
			{
				tablename = "RandDAgreementSummary";
			}
			else
			{
			     tablename = "AgreementSummary";
			}
				addAgreementObj.typeOfAgreement = typeOfAgreement;	
				addAgreementObj.tablename = tablename;
			
			var jsonAgreementDropDown= JSON.stringify(seasonYearValue);
			//alert(jsonAgreementDropDown);
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAgreementNumbersForSeason.html",
					data: JSON.stringify(addAgreementObj),
				
				   }).success(function(data, status) 
					{
					  $scope.AddedAgreements=data;
						
					});
		};
		
		/*$scope.setAgreementnumber = function(seasonYear)
		{
			
			SerialNumberObj.value = seasonYear;
			
			//alert(JSON.stringify(SerialNumberObj));
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxColumnValue.html",
			data: JSON.stringify(SerialNumberObj),
			}).success(function(data, status) 
			{
				sessionStorage.setItem('season',seasonYear);
				var seasonSplit = seasonYear.split("-");
				var year1 = seasonSplit[0].slice(-2);
				var year2 = seasonSplit[1].slice(-2);
				var Seasonyear = year1+"-"+year2;
				var Agreementnumber = data.id+"/"+Seasonyear;
				$scope.AddAgreement.agreementNumber = Agreementnumber;
			}); 
		};*/
		
		
		
		
		//----------Generate Agreement-------
		$scope.GenerateAgreement = function(AddAgreement,form)
		{			
			//$scope.submitted=true;
			//alert($scope.GenerateAgreementForm.$valid);
			if($scope.GenerateAgreementForm.$valid)
			{
				agreementGridData = [];
				$scope.data.forEach(function (agreementData) 
				{
					delete agreementData['landVillageCodeTooltip'];
					delete agreementData['plotIdentityTooltip'];
					delete agreementData['varietyCodeTooltip'];
					delete agreementData['circleCodeTooltip'];
					delete agreementData['plotIdentityTooltip'];
					delete agreementData['plantorRatoonTooltip'];
					delete agreementData['landTypeCodeTooltip'];
					
					
	        		agreementGridData.push(angular.toJson(agreementData));														 
				});		
				
				/*var SuretyCode = $scope.AddAgreement['suretyRyotCode'];
				if(SuretyCode!=undefined)
				{				 
					  var SuretyString = "";
					  for(var s=0;s<SuretyCode.length;s++)
					  {
						  SuretyString +=SuretyCode[s]+",";
					  }	
					  //SuretyString = SuretyString.substring(0,SuretyString.length-1);
				}
			   else
				{
				      SuretyString = "";
				}					
				delete AddAgreement['suretyRyotCode'];	
				AddAgreement.suretyRyotCode = SuretyString;*/
			
				AddAgreementObj.formData = JSON.stringify(AddAgreement);
				AddAgreementObj.gridData = agreementGridData;
				$('.btn-hide').attr('disabled',true);																
				$.ajax({
				    	url:"/SugarERP/saveAgreement.html",
					    processData:true,
				    	type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(AddAgreementObj),
						beforeSend: function(xhr) 
						{
			    	       	xhr.setRequestHeader("Accept", "application/json");
				    	    xhr.setRequestHeader("Content-Type", "application/json");
					    },	
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Agreement Generated Successfully!', "success");		
								$('.btn-hide').attr('disabled',false);	
								//$scope.suretyRyotNames = {};
								$scope.AddAgreement = angular.copy($scope.master);
								$scope.setRowAfterReset();
								$scope.loadServerDate();								
								form.$setPristine(true);
								$scope.AddedAgreements = {};
								$scope.loadAgreementNumber();
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);																
							}
						}
					});			
			}
		   else
		    {
				var field = null, firstError = null;
	            for (field in form) 
				{
                   	if (field[0] != '$') 
					{
                       	if (firstError === null && !form[field].$valid) 
						{
                           	firstError = form[field].$name;
	                    }
	                   if (form[field].$pristine) 
				  	   {  
                           	form[field].$dirty = true;
	                   }
    	           }	
				}				
			}
		};
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddAgreement = angular.copy($scope.master);
				$scope.setRowAfterReset();
				$scope.loadServerDate();				
				$scope.AddedAgreements = {};
				form.$setPristine(true);
				$(".btn-hide").html('Save'); 
				$(".btn-hide").attr('disabled',false);
				$scope.loadAgreementNumber();				
		   };
		
		//--------------Load Extent Details in Sueveyno Popup---
		$scope.loadAddedExtentDetails = function(ryotCodePop)
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getUpdateSoilTestResultsForPopup.html",
					data: ryotCodePop,
				  }).success(function(data, status) 
				  {		
				  		$scope.ryotNamePop = data[0].ryotCode;						
				  		$scope.PopupExtentData = data;
				  }).error(function(data,status)
				  {
				  });		
		};
		
		
		//------------Set Survey Number-----
		$scope.setSurveyNumber = function(surveyNo,plotNo,orgPlotNo)
		{

			$scope.seySurveyNumber = surveyNo;
			//$scope.data[$scope.PlotNumberPop-1].plotNumber = plotNo;
			$scope.data[$scope.PlotNumberPop-1].surveyNumber = surveyNo;						
			
		};
		
		//-------------Get Survey Number----
		$scope.getSurveyNumber = function()
		{
			$scope.Popsubmitted = true;
			if($scope.agreementpopup.$valid)
			{
				if($scope.seySurveyNumber!='')
				{
					$('.popupbox').fadeOut("slow");			
				}
			}
		};
		
		//-------------Upload Ryot Extent Image----
		$scope.uploadExtentImage = function(files,id) 
		{
			var agreemtnNum =  $scope.AddAgreement.agreementNumber.replace("/", "_");
		    var fd = new FormData();
			var Path = "Extent_"+ agreemtnNum+ "_"+ $scope.data[id].plotNumber;
		    fd.append("file", files[0]);
			fd.append("path", Path);
		    $http.post("/SugarERP/saveImage.html", fd, {
		        withCredentials: true,
		        headers: {'Content-Type': undefined },
        		transformRequest: angular.identity
			  }).success(function(data, status) 
		 	   { 
					$scope.data[id].extentImage = data.toLowerCase();
	           });	   	   
		};		
		
		$scope.getScreenPreveliges = function()
		{			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getRoleScreensByEmployeeId.html",
					data: {},
				  }).success(function(data, status) 
				  {	
				  	$scope.preveliges = data;
					if(data.canadd==false) { $(".btn-hide").html('No Access'); $(".btn-hide").attr('disabled',true); }
				  });					
		};
		
		//------------Space Between Two Words ------------
  		$scope.spacebtw = function(b)
		{
				var twoSpace =$scope.AddAgreement[b];
				twoSpace =twoSpace.replace(/ {2,}/g,' ');
				twoSpace=twoSpace.trim();
				$scope.AddAgreement[b] =twoSpace;	
		};
		
		//----------Load Added Agreement Details---------
		
		$scope.getAllAddedDetails = function(agreementNumber,Season)
		{
			var aggtype="";
			if(agreementNumber!=null || Season!=null)
			{
				LoadAgreementObj.agreementnumber = agreementNumber;
				LoadAgreementObj.season = Season;
				if($scope.AddAgreement.typeOfAgreement=='0')
				{
					aggtype="0";
				}
				else
				{
					 aggtype="1";
				}
				LoadAgreementObj.aggType = aggtype;

				$scope.dropDisabled = 'true';
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllAgreementDetails.html",
							data: JSON.stringify(LoadAgreementObj),
						  }).success(function(data, status) 
						  {		
						  
								var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/GetRyotNameForCode.html",
								data: data.formData.ryotName,
								}).success(function(data, status) 
								{
									//alert(JSON.stringify(data));
									$scope.AddAgreement.ryotName = data[0].ryotName;
								});
								$scope.AddAgreement = data.formData;
								$scope.AddAgreement.screenName = "GENERATE AGREEMENT";
								
								$scope.AddAgreement.foCode =data.gridData[0].foCode;
								$scope.AddAgreement.faCode=data.gridData[0].faCode;
								
								if($scope.AddAgreement.branchCode==0) { $scope.AddAgreement.branchCode=null; }
								if($scope.AddAgreement.mandalCode==0) { $scope.AddAgreement.mandalCode=null; }
								
								$scope.data = data.gridData;
								$scope.AddAgreement.agreementAddNumber = agreementNumber;
									for(var s=0;s<data.gridData.length;s++)
								{
									$scope.data[s].screenName = "GENERATE AGREEMENT";
								}
								for(var i=0;i<data.gridData.length;i++)
								{
									$scope.data[i].plantorRatoon = parseInt(data.gridData[i].plantorRatoon);
									if(data.gridData[i].landTypeCode==0) { data.gridData[i].landTypeCode = null; }
									if(data.gridData[i].landVillageCode==0) { data.gridData[i].landVillageCode = null; }
									if(data.gridData[i].varietyCode==0) { data.gridData[i].varietyCode = null; }
									
									for(var j=0;j<$scope.plotIdentity.length;j++)
									{
										if(data.gridData[i].plotIdentity==$scope.plotIdentity[j].id)
										{
											$scope.data[i].plotIdentityTooltip = $scope.plotIdentity[j].plotIdentity;
										}
									}
									for(var k=0;k<$scope.varietyNames.length;k++)
									{
										if(data.gridData[i].varietyCode==$scope.varietyNames[k].id)
										{
											$scope.data[i].varietyCodeTooltip = $scope.varietyNames[k].variety;
										}
									}
									for(var l=0;l<$scope.circleNames.length;l++)
									{
										if(data.gridData[i].circleCode==$scope.circleNames[l].id)
										{
											$scope.data[i].circleCodeTooltip = $scope.circleNames[l].circle;
										}
									}
									/*
									for(var l=0;l<$scope.FieldOffNames.length;l++)
									{
										if(data.gridData[i].foCode==$scope.FieldOffNames[l].id)
										{
											$scope.data[i].circleCodeTooltip = $scope.circleNames[l].circle;
										}
									}
									for(var l=0;l<$scope.FieldAssistantData.length;l++)
									{
										if(data.gridData[i].faCode==$scope.FieldAssistantData[l].id)
										{
											$scope.data[i].circleCodeTooltip = $scope.circleNames[l].circle;
										}
									}
									*/
									for(var m=0;m<$scope.cropTypeNames.length;m++)
									{
										if(data.gridData[i].plantorRatoon==$scope.cropTypeNames[m].id)
										{
											$scope.data[i].plantorRatoonTooltip = $scope.cropTypeNames[m].croptype;
										}
									}
									for(var n=0;n<$scope.villageNames.length;n++)
									{
										if(data.gridData[i].landVillageCode==$scope.villageNames[n].id)
										{
											$scope.data[i].landVillageCodeTooltip = $scope.villageNames[n].village;
										}
									}
									for(var o=0;o<$scope.landTypeNames.length;o++)
									{
										if(data.gridData[i].landTypeCode==$scope.landTypeNames[o].id)
										{
											$scope.data[i].landTypeCodeTooltip = $scope.landTypeNames[o].landtype;
										}
									}
									
								}
								
								if($scope.preveliges.canmodify==false) 
								{ 
									$(".btn-hide").html('No Access'); $(".btn-hide").attr('disabled',true); 
								}
							   else
							    {
									$(".btn-hide").html('Save'); $(".btn-hide").attr('disabled',false); 
								}
								/*if(data.formData.suretyRyotCode!='')
								{
									var SuretySplit = data.formData.suretyRyotCode.split(",");
									var suretyRyotCodeNew = [];
									for(var s=0;s<SuretySplit.length;s++)
									{
										suretyRyotCodeNew.push(SuretySplit[s]);							
									}
									$scope.AddAgreement.suretyRyotCode = suretyRyotCodeNew;									
								}*/																					
						  }).error(function(data,status)
						  {
						  });				
			}
		   else
		    {
				$scope.AddAgreement = angular.copy($scope.master);
				$scope.setRowAfterReset();
				$scope.loadServerDate();
				$scope.GenerateAgreementForm.$setPristine(true);
				$scope.loadAgreementNumber();
			}
		};		

		//---------Set Tooltip-------
		
		$scope.setVarietyTooltip = function(varietyCode,id)
		{
				for(var k=0;k<$scope.varietyNames.length;k++)
				{
					if(varietyCode==$scope.varietyNames[k].id)
					{
						$scope.data[id].varietyCodeTooltip = $scope.varietyNames[k].variety;
					}
				}
		};
		$scope.setCircleTooltip = function(circleCode,id)
		{
				for(var l=0;l<$scope.circleNames.length;l++)
				{
					if(circleCode==$scope.circleNames[l].id)
					{
						$scope.data[id].circleCodeTooltip = $scope.circleNames[l].circle;
					}
				}
		};
		
		$scope.setplantorRatoonTooltip = function(cropTypeCode,id)
		{
			 	for(var m=0;m<$scope.cropTypeNames.length;m++)
				{
					if(cropTypeCode==$scope.cropTypeNames[m].id)
					{
						$scope.data[id].plantorRatoonTooltip = $scope.cropTypeNames[m].croptype;
					}
				}			
		};
		
		$scope.setLandVillageCodeTooltip = function(villageCode,id)
		{
				for(var n=0;n<$scope.villageNames.length;n++)
				{
					if(villageCode==$scope.villageNames[n].id)
					{
						$scope.data[id].landVillageCodeTooltip = $scope.villageNames[n].village;
					}
				}			
		};
		
		$scope.setLandTypeCodeTooltip = function(landTypeCode,id)
		{
				for(var o=0;o<$scope.landTypeNames.length;o++)
				{
					if(landTypeCode==$scope.landTypeNames[o].id)
					{
						$scope.data[id].landTypeCodeTooltip = $scope.landTypeNames[o].landtype;
					}
				}			
		};
	})

	 
	 
	  //==========================================
			 //PostSeason Calculation
		//==========================================
		.controller('PostSeasonCalculationController',function($scope,$http)
		{
			var obj = new Object();
		obj.tablename = "PostSeasonPaymentSummary";
		obj.columnname="pssino";
		var jsonString= JSON.stringify(obj);
		
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
	
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Circle";
		var jsonStringcircleDropDown= JSON.stringify(DropDownLoad);

		
		var UpdateSeasonDropDown=new Object();
		UpdateSeasonDropDown.dropdownname="Season";
		var UpdateDropDownSeason= JSON.stringify(UpdateSeasonDropDown);
		var PostSeasonObj=new Object();
		var PostSeasonSaveObj=new Object();
		
		$scope.loadPostSeasonNames=function()
		{
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: UpdateDropDownSeason,
		 	  }).success(function(data, status) 
			  {		
		  
					$scope.seasonNames=data;
			  }); 
		};		
		
		
		$scope.loadCircleNames = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringcircleDropDown,
			}).success(function(data, status) 
			{
				$scope.circleNames=data;
			}); 
			
		};

		//-------------Load Villages-------
		$scope.loadVillageNames = function()
		{	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.VillagesNamesData = data; 		//-------for load the data in to added departments grid---------
				});
		};
		
			$scope.loadPostSeasonAccountingDate=function()
				{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getServerDate.html",
				data: {},
				}).success(function(data, status) 
				{
			
					var serverDate = data.serverdate;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
				{
					$scope.PostSeason={'seedAccDate':serverDate,'seedActSlno':data.id};
		        });	
					
			  }); 
		};
		
		
	
		
		$scope.getPostPopUp=function(){
			
			 $('#popup_box').fadeIn("slow");
		}
	
	
			$scope.getPostSeasonDetails = function(form)
		{
       		

			if(form.$valid) 
			{
				
	  		  $scope.getPostPopUp();
			  $scope.PostSeasonHeaderData= [];	
				
				PostSeasonObj.season = $scope.PostSeason.season;
				PostSeasonObj.villageCode = $scope.PostSeason.villageCode;
				
				
				//alert(JSON.stringify(PostSeasonObj));
 				var httpRequest = $http({
 	 				 method: 'POST',
 					 url : "/SugarERP/getAllAccruedAndDueRyotsAccounting.html",
 						data: PostSeasonSaveObj,
   				  }).success(function(data, status) 
				  {
						//alert(JSON.stringify(data));
						//alert("data length"+data.length);
						
							$scope.PostSeasonHeaderData = data;		
			  			 $("#buttonshow").show();
 }); 
		  }
		  else
		   {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}			   
		   }
        };
		
		//save Post Season
		
		 var SavePostSeasonCalculationData = [];
		  $scope.PostSeasonSave = function(){
			
			
			SavePostSeasonCalculationData = [];
			$scope.PostSeasonHeaderData.forEach(function(headerData){
						SavePostSeasonCalculationData.push(angular.toJson(headerData));							 
						});
				PostSeasonObj.season = $scope.PostSeason.season;
				PostSeasonObj.villageCode = $scope.PostSeason.villageCode;
		
				
				PostSeasonSaveObj.SavePostSeasonCalculationData=SavePostSeasonCalculationData;
				PostSeasonSaveObj.formData=angular.toJson(PostSeasonObj);
				
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAllAccruedAndDueRyotsAccounting.html",
				data: PostSeasonSaveObj,
			  }).success(function(data, status) 
			  {	
			  
			  if(data==true)	
				 {
					swal("Success!", ' Added Successfully!', "success");
					}
					else
				 {
					
					 sweetAlert("Oops...", "Something went wrong!", "error");
				 }
			  });
			
			
			
			
			
			
			
			  
		  };
		  
		  ////change date 
		  $scope.ChangeRepaymentDate = function(principal,rate,issueDate,repaymentDate,seedActSlno,id)
		{
			
			
		var issueDate = issueDate+"-";
		var repaymentDate = repaymentDate+"-";
		var issueSplit = issueDate.split("-");
		
		
		var repaymentDateSplit = repaymentDate.split("-");
		
		
		var date2 = new Date(repaymentDateSplit[2], repaymentDateSplit[1], repaymentDateSplit[0]);
		var date1 = new Date(issueSplit[2], issueSplit[1], issueSplit[0]);

  


		var date1_unixtime = parseInt(date1.getTime() / 1000);
		var date2_unixtime = parseInt(date2.getTime() / 1000);
	
		// This is the calculated difference in seconds
		var timeDifference = date2_unixtime - date1_unixtime;

		// in Hours
		var timeDifferenceInHours = timeDifference / 60 / 60;

		// and finaly, in days :)
		var timeDifferenceInDays = timeDifferenceInHours  / 24;

		var timeDifferenceInDays1= timeDifferenceInDays  / 30;
		var timeDifferenceInDays12=timeDifferenceInDays1+".";
		var timeDifferenceInDays1Split= timeDifferenceInDays12.split(".");
			//alert(timeDifferenceInDays1Split[0]);	

		var ptr = (principal*timeDifferenceInDays1Split[0]*rate)/100;

		$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].interestAmount=ptr;
 


	var netpayable=(ptr+principal);

	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].totalPayable=netpayable;
	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].paidAmount=netpayable

	
	$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable=0;
	
		for( var i=0;i<$scope.SeedACCHeaderData.length;i++){
		//alert(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable+=parseInt(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid=$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable
				$scope.SeedACCHeaderData[seedActSlno-1].paidToSBAC=($scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid)-($scope.SeedACCHeaderData[seedActSlno-1].payingAmount);
	}
		

	
		
		
		
		
		};
		
			
		})
	 //==========================================
			 //Update Seed  Accounting
		//==========================================
			    .controller('SeedAccountingController',function($scope,$http){
						var obj = new Object();
						obj.tablename = "SeedAccountingSummary";
						obj.columnname="seedActSlno";
						var jsonString= JSON.stringify(obj);
						
					var SaveSeedObj=new Object();
					
					var UpdateSeasonDropDown=new Object();
					UpdateSeasonDropDown.dropdownname="Season";
					var UpdateDropDownSeason= JSON.stringify(UpdateSeasonDropDown);
					
					
					var CaneAccountedDate=new Object();
					CaneAccountedDate.dropdownname="SeedAccountingSummary";
					var CaneAccountedDates=JSON.stringify(CaneAccountedDate);
					
					var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
					
					var seedAccountingObj=new Object();	
					
					//--------------SEASONdropDown------------------
					$scope.loadPostSeasonNames=function()
					{
						var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: UpdateDropDownSeason,
					}).success(function(data, status) 
					{		
					$scope.seasonNames=data;
					}); 
					};	

			
				//------------------ LoadSeedAccountingDate-------------------
					$scope.loadSeedAccountingDate=function()
				{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getServerDate.html",
				data: {},
				}).success(function(data, status) 
				{
			
					var serverDate = data.serverdate;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
				{
					$scope.caneaccounting={'seedAccDate':serverDate,'seedActSlno':data.id,'modifyFlag':'Yes'};
		        });	
					
			  }); 
		};
		
		
		//------------------LoadSeedAccountedDate------------
			$scope.loadSeedAccountedDates=function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: CaneAccountedDates,
		 	  }).success(function(data, status) 
			  {
					$scope.seedAccDates=data;
					
					
			  }); 
		};
		
		//-------------Load Villages-------
		$scope.loadVillageNames = function()
		{	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.VillagesNamesData = data; 		//-------for load the data in to added departments grid---------
				});
		};
		
		//-----------Display Pop window---------------------------
		$scope.getPopUp=function(){
			
			 $('#popup_box').fadeIn("5000");
			 $("#form1").hide();
			 //$("#popupbox").show();
			
		}
				
		//-----------LoadSeedAccounting details button----------------------
		$scope.getSeedAccountingDetails = function(form)
		{
			//alert("enterd");
       		$scope.submitted=true;

			if(form.$valid) 
			{
				
				
				$scope.SeedACCHeaderData= [];	
				
				seedAccountingObj.season = $scope.caneaccounting.season;
				seedAccountingObj.seedActSlno = $scope.caneaccounting.seedActSlno;
				seedAccountingObj.AccountingDate = $scope.caneaccounting.AccountingDate;
				seedAccountingObj.seedAccDate = $scope.caneaccounting.seedAccDate;
				seedAccountingObj.villageCode = $scope.caneaccounting.villageCode;
				
				
				
				
				
				//alert(JSON.stringify(seedAccountingObj));
 				var httpRequest = $http({
 	 				 method: 'POST',
 					 url : "/SugarERP/getAllSeedSupplierRyotsAccounting.html",
 					 data:seedAccountingObj,
   				  }).success(function(data, status) 
				  {
					
						$scope.getPopUp();
							
							$scope.SeedACCHeaderData = data;		
						
							$("#buttonshow").show();
							$("#buttonshow1").show();
						$("#buttonshow12").show();
						$("#loadButton").show();
						
						
						
						
						
					
				  }); 		  
		  }
		  else
		   {
			  
			
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}			   
		   }
        };
		
	/////////////
				$scope.ChangeSBAcc=function(amount,id){
			
					var amount=amount;
			var totalAmount=$scope.SeedACCHeaderData[id].paidToSBAC;
				if(totalAmount<0){
			var getamount=Number(totalAmount)+Number(amount);
		getamount=Math.abs(getamount)
			}else{
				var getamount=Number(amount)-Number(totalAmount);
		getamount=Math.abs(getamount)
				
			}
	$scope.SeedACCHeaderData[id].paidToSBAC=getamount;
	}
		//////////////
		$scope.changeTotalamount=function(rate,seedTotal,id){
	$scope.SeedACCHeaderData[id].totalAmount="";
	
	var Rate=0;
	var TotalSeedAmount=0;
	if(rate==null) { Rate=0; }else{ Rate=rate;}
	if(seedTotal==null) { TotalSeedAmount=0; }else{ TotalSeedAmount=seedTotal;}
var TotalAmount=(Number(Rate)*Number(TotalSeedAmount)).toFixed(2);
$scope.SeedACCHeaderData[id].totalAmount=TotalAmount;
	
}
/////////////////
	$scope.LoadRate = function(rate,extentSize)
			{
				
				
				var gridlength = $scope.SeedACCHeaderData.length;
				var Rate = $scope.caneaccounting.Rate;
				
			
				var totalplant=0;
				var totalratoon=0;
				for(var s=0;s<gridlength;s++)
				{
					//alert("sd");
					
					if(Rate==null)
					{
						$scope.SeedACCHeaderData[s].Rate = 0;
					
					}
					else
					{
						$scope.SeedACCHeaderData[s].seedRate = Rate;
						var rate=$scope.SeedACCHeaderData[s].seedRate;
						var extentsize=$scope.SeedACCHeaderData[s].extentSize;
				$scope.SeedACCHeaderData[s].totalAmount=(Number(rate)* Number(extentsize)).toFixed(2);
						
						
						
					}
			
					 
				}
				
			
			};
//
		$scope.ChangeDate = function(principal,rate,issueDate,repaymentDate,seedActSlno,id)
		{
			
			
		var issueDate = issueDate+"-";
		var repaymentDate = repaymentDate+"-";
		var issueSplit = issueDate.split("-");
		
		
		var repaymentDateSplit = repaymentDate.split("-");
		
		
		var date2 = new Date(repaymentDateSplit[2], repaymentDateSplit[1], repaymentDateSplit[0]);
		var date1 = new Date(issueSplit[2], issueSplit[1], issueSplit[0]);

  


		var date1_unixtime = parseInt(date1.getTime() / 1000);
		var date2_unixtime = parseInt(date2.getTime() / 1000);
	
		// This is the calculated difference in seconds
		var timeDifference = date2_unixtime - date1_unixtime;

		// in Hours
		var timeDifferenceInHours = timeDifference / 60 / 60;

		// and finaly, in days :)
		var timeDifferenceInDays = timeDifferenceInHours  / 24;

		var timeDifferenceInDays1= timeDifferenceInDays  / 30;
		var timeDifferenceInDays12=timeDifferenceInDays1+".";
		var timeDifferenceInDays1Split= timeDifferenceInDays12.split(".");
			//alert(timeDifferenceInDays1Split[0]);	

		var ptr = (principal*timeDifferenceInDays1Split[0]*rate)/100;

		$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].interestAmount=ptr;
 


	var netpayable=(ptr+principal);

	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].totalPayable=netpayable;
	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].paidAmount=netpayable

	
	$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable=0;
	
		for( var i=0;i<$scope.SeedACCHeaderData.length;i++){
		//alert(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable+=parseInt(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid=$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable
				$scope.SeedACCHeaderData[seedActSlno-1].paidToSBAC=($scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid)-($scope.SeedACCHeaderData[seedActSlno-1].payingAmount);
	}
		

	
		
		
		
		
		};
		 //-----------Save Calculation Tab-------
		 var SaveSeedCalculationData = [];
		  $scope.SeedAccountingSave = function(){
			
			//  if($scope.SeedAccountingCalculation.$valid)

			
			SaveSeedCalculationData = [];
			$scope.SeedACCHeaderData.forEach(function(headerData){
						SaveSeedCalculationData.push(angular.toJson(headerData));							 
						});
				seedAccountingObj.season = $scope.caneaccounting.season;
				seedAccountingObj.seedActSlno = $scope.caneaccounting.seedActSlno;
				seedAccountingObj.AccountingDate = $scope.caneaccounting.AccountingDate;
				seedAccountingObj.seedAccDate = $scope.caneaccounting.seedAccDate;
				seedAccountingObj.villageCode = $scope.caneaccounting.villageCode;
				
				SaveSeedObj.SaveSeedCalculationData=SaveSeedCalculationData;
				SaveSeedObj.formData=angular.toJson(seedAccountingObj);
				
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAllSeedSupplierRyotsAccounting.html",
				data: JSON.stringify(SaveSeedObj),
			  }).success(function(data, status) 
			  {	
			  
			  if(data==true)	
				 {
					swal("Success!", ' Added Successfully!', "success");
					}
					else
				 {
					
					 sweetAlert("Oops...", "Something went wrong!", "error");
				 }
			  });
			
			
			
			
			
			
			//}else{
				
			//};
			  
		  };
		
		
				})


	 
	 
	
		
		 //==========================================
			 //Update Seed  Accounting
		//==========================================
			    .controller('SeedAccountingController',function($scope,$http){
						var obj = new Object();
						obj.tablename = "SeedAccountingSummary";
						obj.columnname="seedActSlno";
						var jsonString= JSON.stringify(obj);
						
					var SaveSeedObj=new Object();
					
					var UpdateSeasonDropDown=new Object();
					UpdateSeasonDropDown.dropdownname="Season";
					var UpdateDropDownSeason= JSON.stringify(UpdateSeasonDropDown);
					
					
					var CaneAccountedDate=new Object();
					CaneAccountedDate.dropdownname="SeedAccountingSummary";
					var CaneAccountedDates=JSON.stringify(CaneAccountedDate);
					
					var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
					
					var seedAccountingObj=new Object();	
					
					//--------------SEASONdropDown------------------
					$scope.loadPostSeasonNames=function()
					{
						var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: UpdateDropDownSeason,
					}).success(function(data, status) 
					{		
					$scope.seasonNames=data;
					}); 
					};	

			
				//------------------ LoadSeedAccountingDate-------------------
				$scope.loadSeedAccountingDate=function()
				{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getServerDate.html",
				data: {},
				}).success(function(data, status) 
				{
			
					var serverDate = data.serverdate;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonString,
			   }).success(function(data, status) 
				{
					$scope.caneaccounting={'seedAccDate':serverDate,'seedActSlno':data.id,'modifyFlag':'Yes'};
		        });	
					
			  }); 
		};
		
		//------------------LoadSeedAccountedDate------------
			$scope.loadSeedAccountedDates=function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: CaneAccountedDates,
		 	  }).success(function(data, status) 
			  {
					$scope.seedAccDates=data;
					
					
			  }); 
		};
		
		//-------------Load Villages-------
		$scope.loadVillageNames = function()
		{	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.VillagesNamesData = data; 		//-------for load the data in to added departments grid---------
				});
		};
		
		//-----------Display Pop window---------------------------
		$scope.getPopUp=function(){
			
			 $('#popup_box').fadeIn("5000");
			 $("#form1").hide();
			 //$("#popupbox").show();
			
		}
				
		//-----------LoadSeedAccounting details button----------------------
		$scope.getSeedAccountingDetails = function(form)
		{
			//alert("enterd");
       		$scope.submitted=true;

			if(form.$valid) 
			{
				
				$scope.getPopUp();
				$scope.SeedACCHeaderData= [];	
				
				seedAccountingObj.season = $scope.caneaccounting.season;
				seedAccountingObj.seedActSlno = $scope.caneaccounting.seedActSlno;
				seedAccountingObj.AccountingDate = $scope.caneaccounting.AccountingDate;
				seedAccountingObj.seedAccDate = $scope.caneaccounting.seedAccDate;
				seedAccountingObj.villageCode = $scope.caneaccounting.villageCode;
				
				//alert(JSON.stringify(seedAccountingObj));
 				var httpRequest = $http({
 	 				 method: 'POST',
 					 url : "/SugarERP/getAllSeedSupplierRyotsAccounting.html",
 					 data: JSON.stringify(seedAccountingObj),
   				  }).success(function(data, status) 
				  {
					
					//alert(JSON.stringify(data));
							
							$scope.SeedACCHeaderData = data;		
							
							$("#buttonshow").show();
						
						
						
						
						
						
					
				  }); 		  
		  }
		  else
		   {
			  
			
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}			   
		   }
        };
		
		$scope.ShowDatePicker = function()
		{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
	
		$scope.ChangeSBAcc=function(amount,id){
			
			var amount=amount;
	var totalAmount=$scope.SeedACCHeaderData[id].paidToSBAC;
			if(totalAmount<0){
	var getamount=Number(totalAmount)+Number(amount);
getamount=Math.abs(getamount)
			}else{
				var getamount=Number(amount)-Number(totalAmount);
getamount=Math.abs(getamount)
				
			}
	$scope.SeedACCHeaderData[id].paidToSBAC=getamount;
	
		
			
		}
		
$scope.changeTotalamount=function(rate,seedTotal,id){
	$scope.SeedACCHeaderData[id].totalAmount="";
	
	var Rate=0;
	var TotalSeedAmount=0;
	if(rate==null) { Rate=0; }else{ Rate=rate;}
	if(seedTotal==null) { TotalSeedAmount=0; }else{ TotalSeedAmount=seedTotal;}
var TotalAmount=Number(Rate)*Number(TotalSeedAmount);
$scope.SeedACCHeaderData[id].totalAmount=TotalAmount;
	
}
		$scope.ChangeDate = function(principal,rate,issueDate,repaymentDate,seedActSlno,id)
		{
			
			
		var issueDate = issueDate+"-";
		var repaymentDate = repaymentDate+"-";
		var issueSplit = issueDate.split("-");
		
		
		var repaymentDateSplit = repaymentDate.split("-");
		
		
		var date2 = new Date(repaymentDateSplit[2], repaymentDateSplit[1], repaymentDateSplit[0]);
		var date1 = new Date(issueSplit[2], issueSplit[1], issueSplit[0]);

  


		var date1_unixtime = parseInt(date1.getTime() / 1000);
		var date2_unixtime = parseInt(date2.getTime() / 1000);
	
		// This is the calculated difference in seconds
		var timeDifference = date2_unixtime - date1_unixtime;

		// in Hours
		var timeDifferenceInHours = timeDifference / 60 / 60;

		// and finaly, in days :)
		var timeDifferenceInDays = timeDifferenceInHours  / 24;

		var timeDifferenceInDays1= timeDifferenceInDays  / 30;
		var timeDifferenceInDays12=timeDifferenceInDays1+".";
		var timeDifferenceInDays1Split= timeDifferenceInDays12.split(".");
			//alert(timeDifferenceInDays1Split[0]);	

		var ptr = (principal*timeDifferenceInDays1Split[0]*rate)/100;

		$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].interestAmount=ptr;
 


	var netpayable=(ptr+principal);

	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].totalPayable=netpayable;
	$scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[id].paidAmount=netpayable

	
	$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable=0;
	
		for( var i=0;i<$scope.SeedACCHeaderData.length;i++){
		//alert(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable+=parseInt(JSON.stringify($scope.SeedACCHeaderData[seedActSlno-1].advanceAndLoans[i].totalPayable));
				$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid=$scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPayable
				$scope.SeedACCHeaderData[seedActSlno-1].paidToSBAC=($scope.SeedACCHeaderData[seedActSlno-1].loansAndAdvancesPaid)-($scope.SeedACCHeaderData[seedActSlno-1].payingAmount);
	}
		

	
		
		
		
		
		};
		 //-----------Save Calculation Tab-------
		 var SaveSeedCalculationData = [];
		  $scope.SeedAccountingSave = function(){
			
			//  if($scope.SeedAccountingCalculation.$valid)
		//{
			
			SaveSeedCalculationData = [];
			$scope.SeedACCHeaderData.forEach(function(headerData){
						SaveSeedCalculationData.push(angular.toJson(headerData));							 
						});
				seedAccountingObj.season = $scope.caneaccounting.season;
				seedAccountingObj.seedActSlno = $scope.caneaccounting.seedActSlno;
				seedAccountingObj.AccountingDate = $scope.caneaccounting.AccountingDate;
				seedAccountingObj.seedAccDate = $scope.caneaccounting.seedAccDate;
				seedAccountingObj.villageCode = $scope.caneaccounting.villageCode;
				
				SaveSeedObj.SaveSeedCalculationData=SaveSeedCalculationData;
				SaveSeedObj.formData=angular.toJson(seedAccountingObj);
				//alert(JSON.stringify(SaveSeedObj));
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAllSeedSupplierRyotsAccounting.html",
				data: JSON.stringify(SaveSeedObj),
			  }).success(function(data, status) 
			  {	
			  //alert(JSON.stringify(data));
			  if(data==true)	
				 {
					swal("Success!", ' Added Successfully!', "success");
					}
					else
				 {
					
					 sweetAlert("Oops...", "Something went wrong!", "error");
				 }
			  });
			
			
			
			
			
			
			//}else{
				
			//};
			  
		  };
		
		
				})



		
		
		





	

	//=====================================
			//Seedling Tray Charges
	//=====================================
	  .controller('SeedlingTrayCharges',function($scope,$http)
		{
			

			$scope.data = []; 
		 	var gridData = [];
		 	var obj = new Object();
			var totalGrid = new Object();
			$index = 0;
			var seedlingGrid = new Object();
			
			var maxid = new Object();
			maxid.tablename = "SeedlingTrayChargeSummary";
			var jsonTable= JSON.stringify(maxid);	
		
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Season";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "ExtentDetails";
			var StringDropDownExtent= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "SeedlingTray";
			var StringDropDownTray= JSON.stringify(DropDownLoad);
			var filterObj = new Object();
			
			var DropRyotDownLoad = new Object();
			DropRyotDownLoad.tablename = "AgreementSummary";
			DropRyotDownLoad.columnname = "ryotcode";
			DropRyotDownLoad.wherecolumnname = "seasonyear";			
			
			
			var VillageDropDownUpdate = new Object();
			VillageDropDownUpdate.dropdownname = "Village";
			var jsonStringVillage= JSON.stringify(VillageDropDownUpdate);
			var ryotSeasonObj = new Object();
			
			$scope.StatusNames = [{id: 0, status: 'Damaged' }, {id: 1, status: 'Lost' }];
			
			$scope.master = {};
			
			$scope.setRowAfterReset = function()
			{
				$scope.StatusNames = [{id: 0, status: 'Damaged' }, {id: 1, status: 'Lost' }];
				$scope.data = {};
				$index='1';
				$scope.data = [{'status':'','id':$index,'screenName':'SEEDLING TRAY CHARGES'}]										
			};
			
			$scope.setVillageDropDown = function()
			{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringVillage,
			   }).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
			$scope.VillageData=data;	
			});	
			};


			//----------Ryot Name Filter by Season----
		
			
				$scope.getseasonRyots = function(seasoncode)
				{
					ryotSeasonObj.season = seasoncode;
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSeedlingAdvanceRyots.html",
					data: JSON.stringify(ryotSeasonObj),
					}).success(function(data, status) 
					{	 
						$scope.ryotCodeData=data;
					}); 	
				
			};

			$scope.decimalpointgrid= function(b,id)
			{
				//alert(90);
				var decmalPoint=$scope.data[id][b];
				//alert(decmalPoint);
				var getdecimal=parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				//alert(getdecimal);
			    $scope.data[id][b]=getdecimal;
				 if(getdecimal=="NaN")
				{
					//alert(67);
					 $scope.data[id][b]="";
				}
			};
			$scope.loadSeedlingGridValues = function(id,villageCode,trayType)
			{
				//alert('seedlingGrid.villagecode');
				seedlingGrid.villagecode = villageCode;
				seedlingGrid.traycode = trayType;
				if(seedlingGrid.villagecode!='' && seedlingGrid.traycode!='') 
				{
					//alert(seedlingGrid.villagecode);
					//alert(seedlingGrid.traycode);
					//alert(JSON.stringify(seedlingGrid));
					 var httpRequest = $http({
						  method: 'POST',
						  url : "/SugarERP/getTrayChangesByDistance.html",
						  data: JSON.stringify(seedlingGrid),
					   }).success(function(data, status) 
						{
							//alert("id-----------"+id);
							//alert(JSON.stringify(data));
							for(var r=0;r<id;r++)
							{
								
								//$scope.data.cost[r] = data;
								$scope.data[id-1].cost = data;
							}
						});						 
				}
				
			};

			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			
			/*$scope.loadryotCode = function()
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDownExtent,
				  }).success(function(data, status) 
				{
					$scope.ryotCodeData=data;
				}); 
			};*/
			$scope.loadTrayType = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data:StringDropDownTray,
				  }).success(function(data,status) 
				{
					//alert(JSON.stringify(data));					
					$scope.trayTypeNames=data;
					
				}); 
			};
			
			//-------Display Ryot Names by Season-----
		 	/*$scope.getseasonRyots = function(seasoncode)
			{
					DropDownLoad.value = seasoncode;
					var jsonRyotDropDown= JSON.stringify(DropDownLoad);
				
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/onChangeDropDownValues.html",
							data: jsonRyotDropDown,
						}).success(function(data, status) 
						{		
							alert(JSON.stringify(data));
							$scope.ryotCodeData=data;
						}); 		
				
			};*/			
			
			
			//-----------Add Row for SeedlingTrayCharges------
		$scope.addFormField = function() 
		{
			
			$index++;
		    $scope.data.push({'status':'','id':$index,'screenName':'SEEDLING TRAY CHARGES'})
		};
		
		 //------------Delete Row from SeedlingTrayCharges---
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}

            var totalAmount = 0;
			for(var k=0;k<$index;k++)
			{
				//alert($scope.data[k].totalCost);
			     totalAmount += parseInt($scope.data[k].totalCost);
				 
			}
				  
			$scope.total = parseFloat(Math.round(totalAmount)).toFixed(2);
			$scope.AddedSeedlingTray.totalAmount = parseFloat(Math.round(totalAmount)).toFixed(2);
			
			
			
			
  	    };
		
		 $scope.getotalCost = function(id,tray,cost)
		  {
			  if(cost!=null)
			  {
				var toalamount = Number(tray)*Number(cost);
				
				 $scope.data[id-1].totalCost=parseFloat(Math.round(toalamount)).toFixed(2);
				if(id==1)
				  {
					 $scope.total = parseFloat(Math.round(toalamount)).toFixed(2);
					 $scope.AddedSeedlingTray.totalAmount = parseFloat(Math.round(toalamount)).toFixed(2);
				  }
				 else
				  {
					  var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += parseInt($scope.data[s].totalCost);
					  }
					  $scope.total = parseFloat(Math.round(totalAmount)).toFixed(2);
					  $scope.AddedSeedlingTray.totalAmount = parseFloat(Math.round(totalAmount)).toFixed(2);
				  }
			  }
			  
			  
		  };
		  
		  $scope.loadreturnId=function()
	 			{
					$scope.total;
					
				var httpRequest = $http
					({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: jsonTable,

	 			}).success(function(data) 
				{
					$scope.AddedSeedlingTray={'returnId':data.id,'loginId':'makella','modifyFlag':'No','screenName':'SEEDLING TRAY CHARGES'};		
				});
	};
		
			//----------SeedlingTrayCharges Submit--------------------------
			$scope.AddSeedlingTrayChargesSubmit = function(AddedSeedlingTray,form)
		   {

				//$scope.Addsubmitted = true;
				//$scope.submitted = true;
				
				if($scope.SeedlingTrayChargesForm.$valid) 
				{	
					  gridData = [];
					  $scope.data.forEach(function (SeedlingData) 
					  {
	            		 gridData.push(angular.toJson(SeedlingData));														 
				      });
					  obj.gridData = gridData;
				      obj.formData = angular.toJson(AddedSeedlingTray);
					  obj.total=$scope.total;
										  
				      delete obj["$index"];
					//alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveSeedlingTrayCharges.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Seedling Tray Charges Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								
								$scope.loadseasonNames();
								$scope.AddedSeedlingTray = angular.copy($scope.master);
								$scope.SeedlingData = angular.copy($scope.master);
								$scope.setRowAfterReset();								
								$scope.total = "";
								$scope.ryotCodeData = {};
							    form.$setPristine(true);			    											   
								//$scope.AddedSeedlingTray = {};
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
					 
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
		 };

			 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedSeedlingTray = angular.copy($scope.master);
				$scope.SeedlingData = angular.copy($scope.master);
				$scope.setRowAfterReset();
				$scope.loadseasonNames();
				$scope.total = "";
				$scope.ryotCodeData = {};
			    form.$setPristine(true);			    											   
		   };
		 
			//--------------Load Seedling TrayFilter----------------
			//--------------Load Seedling TrayFilter----------------
      		$scope.loadSeedlingTrayFilter = function(AddedSeedlingTray,form) 
		    {
				
				//$scope.Addsubmitted = true;

				
				if($scope.AddedSeedlingTray.season!='' && $scope.AddedSeedlingTray.ryotCode!='') 
				{ 
					//alert($scope.AddedSeedlingTray.ryotCode);
					filterObj.season = $scope.AddedSeedlingTray.season;
					filterObj.ryotCode = $scope.AddedSeedlingTray.ryotCode;
					
					

					 var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/getAllSeedlingTrayCharges.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{			
							
							//alert(JSON.stringify(data));
							 if($scope.data!="")
							{
								 
								$scope.data=data.gridData;
								//alert(JSON.stringify(data));

								$scope.total = data.formData.totalAmount;
								
								$scope.AddedSeedlingTray.modifyFlag = "Yes";
								//$scope.AddedSeedlingTray.returnId;

								var Amt = data.formData.totalAmount;
								$scope.AddedSeedlingTray.totalAmount = Amt;
								//alert('$scope.AddedSeedlingTray.totalAmount========'+$scope.AddedSeedlingTray.totalAmount);


								var val = data.formData.returnId;
								$scope.AddedSeedlingTray.returnId = val;
								$scope.AddedSeedlingTray.screenName = "SEEDLING TRAY CHARGES";
								for(var s=0;s<data.gridData.length;s++)
								{
									$scope.data[s].screenName = "SEEDLING TRAY CHARGES";
								}
								
								
							}
						}).error(function(data, status)
						{
							if(status=='500')
							{
								//alert(status);
								$scope.AddedSeedlingTray.returnId;
								//$scope.AddedSeedlingTray={};
								$scope.data = {};
								$index = 1;
								$scope.data =[{'status':'','id':$index}]
								$scope.total = '0';								
								$scope.AddedSeedlingTray.modifyFlag = "No";
								//$scope.ryotCodeData = {};
								//$scope.ryotCodeData = [];
								//alert($scope.AddedSeedlingTray.id);
								//$scope.AddedSeedlingTray.returnId = returnId;
								
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getMaxId.html",
										data: jsonTable,
	 								}).success(function(data) 
									{
										$scope.AddedSeedlingTray.returnId  = data.id;
									});
							}								
						});					
				} 
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}
			};
		})
	
		//=================================
				//Prepare Tie Up Loans
		//=================================
	  	  
	   .controller('PrepareLoanMaster', function($scope,$http)
		{
			var obj = new Object();
			obj.tablename = "LoanDetails";
			var jsonTable= JSON.stringify(obj);
			
			var PrepareLoanAddObj = new Object();
			
			var getAllLoansObj = new Object();
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Season";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "ExtentDetails";
			var StringDropDownExtent= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
		 	DropDownLoad.dropdownname = "Branch";
		 	var jsonStringDropDownBank= JSON.stringify(DropDownLoad);
			var DropDownLoad = new Object();
		 	DropDownLoad.dropdownname = "Branch";
		 	var jsonStringDropDownBank= JSON.stringify(DropDownLoad);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "LoanDetails";
			var StringDropDownAddedLoan= JSON.stringify(DropDownLoad);
			
			
			var DropAgreement= new Object();
			DropAgreement.targetdropdownname = "AgreementSummary";
			DropAgreement.columnname = "seasonYear";
			
			var DropRyotDownLoad = new Object();
			DropRyotDownLoad.tablename = "AgreementSummary";
			DropRyotDownLoad.columnname = "ryotcode";
			DropRyotDownLoad.wherecolumnname = "seasonyear";
			
			var DropLoanLoad = new Object();
			DropLoanLoad.tablename = "LoanDetails";
			DropLoanLoad.columnname = "loannumber";
			DropLoanLoad.wherecolumnname = "Season";			
			
			var agreementRyotSeason = new Object();
			
			$scope.master = {};
			var loanCodeObj = new Object();
			
			//-------Set Loan number on branch onchange----
			$scope.setMaxLoanNumber = function(branchCode,season)
			{
				var maxLoan = new Object();
			maxLoan.branchCode = parseInt(branchCode);
			maxLoan.season = season;
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxLoanNo.html",
					data: JSON.stringify(maxLoan),
				  }).success(function(data, status) 
  				  {
					$scope.AddedPrepareLoan.loanNumber=data;		
 				  }); 
				
			};
			
			
			 $scope.decimalpointgrid= function(b,id)
			{
				//alert(90);
				var decmalPoint=$scope.data[id][b];
				//alert(decmalPoint);
				var getdecimal=parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				//alert(getdecimal);
			    $scope.data[id][b]=getdecimal;
				 if(getdecimal=="NaN")
				{
					//alert(67);
					 $scope.data[id][b]="";
				}
			};			
			
			$scope.loadAddedSeasonloan = function(season)
			{
				DropLoanLoad.value = season;
				var jsonaddedloanDropDown= JSON.stringify(DropLoanLoad);
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getLoansForSeason.html",
				data: jsonaddedloanDropDown,
				  }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.loanNames=data;
				}); 
			};			
			
			//----------Ryot Name Filter by Season----
			$scope.getseasonRyots = function(seasoncode)
			{
				
					DropRyotDownLoad.value = seasoncode;
					var jsonRyotDropDown= JSON.stringify(DropRyotDownLoad);
					//alert(jsonRyotDropDown);
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getRyotsFromAgreementSummary.html",
							data: jsonRyotDropDown,
						}).success(function(data, status) 
						{		//alert(JSON.stringify(data));
							//alert(JSON.stringify(jsonRyotDropDown));
							$scope.ryotCodeData=data;
							//$scope.AddedPrepareLoan.ryotCode = data;
						}); 		
				
			};			
		 //--------Load Server Date--------
			$scope.loadServerDate = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(dataDate, status) 
					 {
						//alert(JSON.stringify(data));	
						/*var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getMaxId.html",
								data: jsonTable,
					   		}).success(function(data, status) 
							{*/
								//alert(JSON.stringify(data));
								$scope.AddedPrepareLoan={"recommendedDate":dataDate.serverdate,'screenName':'Prepare Tieup Loans'};				
				        	/*});	*/   	   													//$scope.AddedPrepareLoan.recommendedDate = dataDate.serverdate;
						
				 	 });	
			};			
			
						
			$scope.loadseasonNames = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
				});
			};
			
			$scope.loadAgreements = function(season)
			{
				DropAgreement.value = season;
				var jsonagreement  = JSON.stringify(DropAgreement);
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/onChangeDropDownValues.html",
						data: jsonagreement,
					 }).success(function(data, status) 
					 {					
						$scope.agreementNames=data;
						//alert(JSON.stringify(data));					
					});
			};
			$scope.getagreement = function(ryotcode,season)
			{
				agreementRyotSeason.ryotcode = ryotcode;
				agreementRyotSeason.season = season;
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAgreementsRyotsBySeason.html",
						data: JSON.stringify(agreementRyotSeason),
					 }).success(function(data, status) 
					 {					
						$scope.agreementNames=data;
											
					});
			};
			
			$scope.loadryotCode = function()
			{				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: StringDropDownExtent,
					  }).success(function(data, status) 
					  {
						$scope.ryotCodeData=data;
					  }); 
	  	    };
			
			$scope.loadBranchNames = function()
			{				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDownBank,
				  }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.branchNames=data;
				}); 
			};
			
			$scope.loadPrepareLoanCode = function(season,branchCode)
			{	
				
				loanCodeObj.season = season;
				loanCodeObj.branchCode = branchCode;
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getLoanNumbersForBranch.html",
					data: JSON.stringify(loanCodeObj),
			   		}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.loanNames=data;
						//$scope.AddedPrepareLoan={'loanNumber':data.id};		
						//alert(asd);
		        	});	   	   
		    };
			
			/*$scope.loadAddedLoanNames = function()
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDownAddedLoan,
				  }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.loanNames=data;
				}); 
			};*/
			
			$scope.getRyotCode = function(RyotCode)
			{
				//alert(90);
				//alert(JSON.stringify(RyotCode));
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetRyotNameForCode.html",
					data: RyotCode,
			   }).success(function(data, status) 
			   {
				   //alert(JSON.stringify(data));
				  
				   //alert(JSON.stringify(
				   //alert($scope.AddedPrepareLoan.ryotCode);
				   //$scope.AddedGenerateProgram={'typeOfExtent':RyotCode};
				    //$scope.AddedPrepareLoan.ryotCode":ryotCode,"ryotName":"dssdf"};
				  //$scope.AddedPrepareLoan = data[0];
				  
					$scope.AddedPrepareLoan.ryotName = data[0].ryotName;
					
					
				   //alert(90);
					//alert($scope.AddedPrepareLoan.ryotCode);
					
				var ryotCodeforName = $scope.AddedPrepareLoan.ryotCode;
				//$scope.AddedPrepareLoan.ryotName = ryotCodeforName;
				var ln = $scope.AddedPrepareLoan.loanNumber;
				//alert(ln+"-"+ryotCodeforName);
				$scope.AddedPrepareLoan.addedLoan = ln+"-"+ryotCodeforName;
					
						
			   }); 
				
			};
			
			
			
			
			$scope.getRyotName=function(ryotcode,season)
			{
				//alert(123);
				//alert(JSON.stringify(ryotcode));
				var ryotObj=new Object();
				ryotObj.season=season;
				ryotObj.ryotcode=ryotcode;
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAgreementNosForRyot.html",
					data: ryotObj,
			   }).success(function(data, status) 
			   {
				  
				  
				  var dataSplit = data.split(",");
				  var setAgreementNums = [];
				  for(var s=0;s<dataSplit.length-1;s++)
				  {
						setAgreementNums.push(dataSplit[s]);							
				  }
				  $scope.AddedPrepareLoan.agreementNumber = setAgreementNums;
				  var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getLoanDetails.html",
						data: setAgreementNums,
					  }).success(function(data, status) 
					  {	
					  	  var surveyNo = "";
						  var plantExtent = "";
						  var ratoonExtent = "";
						  for(var s=0;s<data.length;s++)
						  {
							  surveyNo +=data[s][1]+",";
							  //plantExtent +=data[s][0]+",";
							  if(data[s][0]!=1)
							  {
								  ratoonExtent +=data[s][2]+",";
								  plantExtent +='0'+",";
							  }
							 else
							  {
								  ratoonExtent +='0'+",";
								  plantExtent +=data[s][2]+",";								  
							  }
						  }							
							
						  $scope.AddedPrepareLoan.surveyNumber = surveyNo;
						  $scope.AddedPrepareLoan.plantExtent = plantExtent;
						  $scope.AddedPrepareLoan.ratoonExtent = ratoonExtent;
					  });
				  				  	
			   }); 
				
			};
			
			
									
			
			$scope.PrepareLoanSubmit = function(AddedPrepareLoan,form)
		 	{
				//$scope.PrepareLoanForm.$submitted = true;
				//$scope.Addsubmitted = true;				
				//$scope.Filtersubmitted = true;
								
				if($scope.PrepareLoanForm.$valid) 
				{

				    var agreementNo = $scope.AddedPrepareLoan['agreementNumber'];
					var AgreementString = "";
					for(var s=0;s<agreementNo.length;s++)
					{
						  AgreementString +=agreementNo[s]+",";
					}	
					delete AddedPrepareLoan['agreementNumber'];
					
					PrepareLoanAddObj.formData = AddedPrepareLoan;
					PrepareLoanAddObj.agreementNumber = AgreementString;
					
					//alert(JSON.stringify(PrepareLoanAddObj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/savePrepareTieUpLoans.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(PrepareLoanAddObj),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response[0].issuccess=="true")
								{
									
									swal({
										   	 title: "Prepare TieUp Loan Added Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#DD6B55",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 function(){
											//window.open("http://response[0].serverip:8080/SugarERP/generateRyotLoanForwardingReportForReport.html?season="+response[0].season+"&branchCode="+response[0].branchCode+"&loanRyotcode="+response[0].loanRyotcode+"&status="+response[0].status, "CNN_WindowName");
										
											window.open("http://"+response[0].serverip+":8080/SugarERP/generateRyotLoanForwardingReportForReport.html?season="+response[0].season+"&branchCode="+response[0].branchCode+"&loanRyotcode="+response[0].loanRyotcode+"&status="+response[0].status, "CNN_WindowName");
										});
									
									
									
									//swal("Success!", 'Prepare TieUp Loan Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);									
									
									$scope.AddedPrepareLoan = angular.copy($scope.master);
				                    $scope.loadServerDate();
									$scope.agreementNames = [];
									$scope.ryotCodeData = [];
									$scope.loanNames = [];
									
									form.$setPristine(true);
									$scope.loadPrepareLoanCode();
								   							
								}
							  	else if(response.issuccess=="false")
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									   
								}
							}
						});					
				}		
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}					
				}
		 	};
			
			
			$scope.getSurveuNumbers = function(adreementNo)
			{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLoanDetails.html",
							data: adreementNo,
					  }).success(function(data, status) 
					  {
						  //alert(JSON.stringify(data));
						  var surveyNo = "";
						  var plantExtent = "";
						  var ratoonExtent = "";
						  for(var s=0;s<data.length;s++)
						  {
							  surveyNo +=data[s][1]+",";
							  //plantExtent +=data[s][0]+",";
							  if(data[s][0]!=1)
							  {
								  ratoonExtent +=data[s][2]+",";
								  plantExtent +='0'+",";
							  }
							 else
							  {
								  ratoonExtent +='0'+",";
								  plantExtent +=data[s][2]+",";								  
							  }
						  }							
							
						  $scope.AddedPrepareLoan.surveyNumber = surveyNo;
						  $scope.AddedPrepareLoan.plantExtent = plantExtent;
						  $scope.AddedPrepareLoan.ratoonExtent = ratoonExtent;
							
					  });
			};
			
			$scope.getAllPrepareTieup = function(loanNo,seasonYear,form,branchCode)
			{
					//$scope.Filtersubmitted = true;
					
					if(loanNo!=null && seasonYear!=null)
					{
					    getAllLoansObj.season = seasonYear;
						getAllLoansObj.loannumber = loanNo;
						getAllLoansObj.branchCode = branchCode;
						$scope.hiddenLoanNo = loanNo;

						
						var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getAllLoanDetails.html",
								data: JSON.stringify(getAllLoansObj),
						  }).success(function(data, status) 
						  {
							  //alert(JSON.stringify(data));
							 // alert("Our ryotCode--------------------"+data[0].ryotCode);
							 var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/GetRyotNameForCode.html",
								data: data[0].ryotCode,
								}).success(function(data, status) 
								{
									//alert(JSON.stringify(data));
									$scope.AddedPrepareLoan.ryotName = data[0].ryotName;
								}); 
							  
							  
							  $scope.AddedPrepareLoan = data[0];
							  $scope.AddedPrepareLoan.ryotName = data[0].ryotCode;
							  $scope.AddedPrepareLoan.addedLoan = $scope.hiddenLoanNo;
						  						  
							  var SuretySplit = data[0].agreementNumber.split(",");
							  var suretyRyotCodeNew = [];
							  for(var s=0;s<SuretySplit.length-1;s++)
							  {
									suretyRyotCodeNew.push(SuretySplit[s]);							
							  }
							  //alert(suretyRyotCodeNew);
							  $scope.AddedPrepareLoan.agreementNumber = suretyRyotCodeNew;
							  $scope.AddedPrepareLoan.screenName = "Prepare Tieup Loans";
						  });				
					}
				   else
				    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}						
					}
			};
			
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedPrepareLoan = angular.copy($scope.master);
				$scope.loadServerDate();
				$scope.agreementNames = [];
				$scope.ryotCodeData = [];
				$scope.loanNames = [];
				
			    form.$setPristine(true);			    								
			   
		   };			
			
		})  
	  
	  	//=================================================
				//update lab css rating
		//=================================================
		
		.controller('UpdateLabCCSRating',function($scope,$http)
		{
				
				var DropDownLoad=new Object();
				DropDownLoad.dropdownname="Season";
				var DropDownSeason= JSON.stringify(DropDownLoad);
				
				var getDetailsObj = new Object();
				var loadDetailsObj = new Object();
				var submitObj = new Object();
				//$scope.AddedLabCSS.sampleCardCount = '1';
				$scope.AddedLabCSS = {'sampleCardCount':100};
				$scope.master = {};
												
				$scope.loadSeasonNames=function()
				{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: DropDownSeason,
				  		}).success(function(data, status) 
						{							
							$scope.SeasonData=data;
						}); 
				};
				
				$scope.loadProgramNames = function(seasonCode)
				{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProgramnNumbersBySeason.html",
						data: seasonCode,
				  		}).success(function(data, status) 
						{
							$scope.ProgramNames=data;
						}); 
			    };
				$scope.calculatePurity = function(pol,brix)
				{
					var purity = (pol/brix)*100;
					var ccsrating = Number(1.125*pol)-Number(0.4*brix);  
					$scope.AddedLabCSS.purity =Number(Math.round(purity+'e2')+'e-2');
					$scope.AddedLabCSS.ccsRating = Number(Math.round(ccsrating+'e2')+'e-2');
				};
					
					
				//---------Space Between Two Words ----------
		  		$scope.spacebtw = function(b)
				{
						var twoSpace =$scope.AddedLabCSS[b];
						twoSpace =twoSpace.replace(/ {2,}/g,' ');
						twoSpace=twoSpace.trim();
						$scope.AddedLabCSS[b] =twoSpace;	
				};
					
				//------Get All Details--------------------
					
				$scope.getLabCCsDetails = function(form)
				{					
					//$scope.Filtersubmitted=true;
					
					if($scope.AddedLabCSS.season!=null && $scope.AddedLabCSS.program!=null)
					{						
						getDetailsObj.season = $scope.AddedLabCSS.season;
						getDetailsObj.programno = $scope.AddedLabCSS.program;
						getDetailsObj.samplecard = 0;
						getDetailsObj.Flag = 'No';
						getDetailsObj.pageName = 'Update';
					
					
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getSampleCards.html",
						data: JSON.stringify(getDetailsObj),
						}).success(function(data, status) 
						{
							//alert(JSON.stringify(data[0]));
							$scope.AddedLabCSS=data[0];
						}); 												
					}
				   else
				    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}										
					}
					
				};
					
				//------Update Lab CCS Rating------------
					
				$scope.SubmitLabCCSRating = function(AddedLabCSS,form)
				{
					//$scope.submitted=true;
					if($scope.UpdateLabCCSRating.$valid)
					{
						submitObj.formData = JSON.stringify(AddedLabCSS);
						$('.btn-hide').attr('disabled',true);
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/saveLabCcsRating.html",
						data: submitObj,
						}).success(function(data, status) 
						{
							if(data==true)
							{
								swal("Success!", 'CSS Ratings Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);								
								
								$scope.AddedLabCSS = angular.copy($scope.master);
								$scope.getLabCCsDetails();
								$scope.AddedLabCSS.season = "";
								$scope.AddedLabCSS.program = "";
								$scope.AddedLabCSS = {'sampleCardCount':100};
								$scope.ProgramNames = {};
								form.$setPristine(true);
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
								$('.btn-hide').attr('disabled',false);								
							}
						}); 												
						
					}
				   else
				    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}							
						getDetailsObj.season = $scope.AddedLabCSS.season;
						getDetailsObj.programno = $scope.AddedLabCSS.program;
						getDetailsObj.samplecard = 0;
						getDetailsObj.Flag = 'No';
						getDetailsObj.pageName = 'Update';						
					}
				};
				
				 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedLabCSS = angular.copy($scope.master);
				$scope.getLabCCsDetails();
				$scope.AddedLabCSS.season = "";
				$scope.AddedLabCSS.program = "";
				$scope.AddedLabCSS = {'sampleCardCount':100};
				$scope.ProgramNames = {};
				form.$setPristine(true);
			   
		   };
				
			   //------------------Load Sample Cards on Sample Card onblur----
			   $scope.loadSampleCards = function(samplecard)
			   {
				   //alert(samplecard);
					loadDetailsObj.season = $scope.AddedLabCSS.season;
					loadDetailsObj.programno = $scope.AddedLabCSS.program;
					loadDetailsObj.samplecard = parseInt(samplecard);
					loadDetailsObj.Flag = 'Yes';
					loadDetailsObj.pageName = 'Update';
					
					sessionStorage.setItem('sseason',$scope.AddedLabCSS.season);
					sessionStorage.setItem('sprogramnumber',$scope.AddedLabCSS.program);
					sessionStorage.setItem('ssamplecard',samplecard);
					
					if($scope.AddedLabCSS.season!=null && $scope.AddedLabCSS.program!=null)
					{
						
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getSampleCards.html",
						data: JSON.stringify(loadDetailsObj),
						}).success(function(data, status) 
						{
							//alert(JSON.stringify(data[0]));
							$scope.AddedLabCSS=data[0];
						}).error(function(data, status)
						{
							//alert(sessionStorage.getItem('sprogramnumber'));
							$scope.AddedLabCSS = {};
							$scope.AddedLabCSS.season = sessionStorage.getItem('sseason');
							$scope.AddedLabCSS.program = sessionStorage.getItem('sprogramnumber');
							$scope.AddedLabCSS.samplecard = sessionStorage.getItem('ssamplecard');
						});
						
					}
				   else
				    {
						$scope.Filtersubmitted=true;
					}
			   };
			   
			   //--------Load Samples on Next Button Click-----
			   
			   $scope.loadNextSamples = function(SampleCard,status,form)
			   {
				   //$scope.submitted=true;
				   
				   var statusok = status.trim();
				   if($scope.UpdateLabCCSRating.$valid)
					{
						submitObj.formData = JSON.stringify($scope.AddedLabCSS);
						
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/saveLabCcsRating.html",
						data: submitObj,
						}).success(function(data, status) 
						{
							if(data==true)
							{
									//swal("Success!", 'CSS Ratings Added Successfully!', "success");
							
									if(statusok=="plus")
									{
										var nextSample = Number(SampleCard)+Number(1);
									}
								   else
								    {
										var nextSample = Number(SampleCard)-Number(1);
									}
									//alert(nextSample);
									$scope.loadSampleCards(nextSample);															
							}
						}); 												
						
					}				
				  else
				   {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}												   
				   }
				   
			   };
				
			})
				
		//========================================
				//Company Advances to Ryot		
		//========================================
		.controller('CompanyAdvancestoRyot', function($scope,$http)
		{
			$index=0;
			$scope.data = [];
			var CompanyAdvanceGridData = [];
			var CompanyAdvanceObj = new Object();
						
			var ryotExtObj = new Object();
			ryotExtObj.dropdownname = "ExtentDetails";
			var jsonRyotCompString= JSON.stringify(ryotExtObj);
			
			var seasonCompanyObj = new Object();
			seasonCompanyObj.dropdownname = "Season";
			var jsonSeasonCompString= JSON.stringify(seasonCompanyObj);
			
			var varietyCompanyObj = new Object();
			varietyCompanyObj.dropdownname = "Variety";
			var jsonVarietyCompString= JSON.stringify(varietyCompanyObj);
			
			var CategoryObj = new Object();
			CategoryObj.dropdownname = "AdvanceCategory";
			var jsonCompanyCategoryString= JSON.stringify(CategoryObj);		
			
			/*var agreementNumberObj = new Object();
			agreementNumberObj.dropdownname = "AgreementSummary";
			var jsonCompanyAgreementString= JSON.stringify(agreementNumberObj);*/
	
			var AdvanceTypeObj = new Object();
			AdvanceTypeObj.dropdownname = "CompanyAdvance";
			var jsonAdvanceTypeString= JSON.stringify(AdvanceTypeObj);
	
		    var AdvanceSubCategoryObj = new Object();
			AdvanceSubCategoryObj.dropdownname = "AdvanceSubCategory";
			var jsonAdvanceSubCatString= JSON.stringify(AdvanceSubCategoryObj);	
	
			var trayTypeObj = new Object();
			trayTypeObj.dropdownname = "SeedlingTray";
			var jsonTrayTypeString= JSON.stringify(trayTypeObj);

			var addedadvanceObj = new Object();
			addedadvanceObj.dropdownname = "SeedlingTray";
			var jsonAdvanceTypeStringaddedAdvance= JSON.stringify(addedadvanceObj);
			
			var dropDownObj = new Object();
			var loadDetailsObj = new Object();
			
			var DropRyotDownLoad = new Object();
			DropRyotDownLoad.tablename = "AgreementSummary";
			DropRyotDownLoad.columnname = "ryotcode";
			DropRyotDownLoad.wherecolumnname = "seasonyear";			
			
			var agreeementExtentObj = new Object();
			
			var getagreementsObj = new Object();
			var getVarietyObj = new Object();
			var getTotalSeedlingsObj = new Object();
			
			$scope.maxAdvanceAmt = 100000;
			$scope.master = {};
			//$scope.AddAdvancestoRyot = {'modifyFlag':'No','transactionCode':'0','loginId':'makella','isItSeedling':'0','onloadSeedlingStatus':'5'};
	
			//-------Load RyotCode DropDowns-----
			/*$scope.loadRyotCodes = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonRyotCompString,
			   		}).success(function(data, status) 
					{					
						$scope.ryotCodes=data;
					}); 
			};*/
			  $scope.setRowAfterReset = function()
			 {
				    
				    $scope.data = {};					
					$index=1;
					$scope.data = [{'id':$index}]
			};

			$scope.decimalpoint = function(b)
			{
				var decmalPoint = $scope.AddAdvancestoRyot[b];			
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
				$scope.AddAdvancestoRyot[b] = getdecimal;	
				 if(getdecimal=="NaN")
					{
						 $scope.AddAdvancestoRyot[b]="";
					}
			};
			
			//----------load Ryot Names on Season Change----
			$scope.getseasonRyots = function(seasoncode)
			{
				if(seasoncode!=null)
				{
					//DropDownLoad.value = seasoncode;
					DropRyotDownLoad.value = seasoncode;
					var jsonRyotDropDown= JSON.stringify(DropRyotDownLoad);
					//alert(jsonRyotDropDown)
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getRyotsFromAgreementSummary.html",
							data: jsonRyotDropDown,
						}).success(function(data, status) 
						{		
							
							$scope.ryotCodeData=data;
						}); 		
				}
			   else
			    {
					$scope.ryotCodeData={};
				}
				
			};				
			
			//--------------Amount validation on advance onchange----
			
			$scope.getAdvanceAmount = function(advanceType)
			{				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getadvanceAmounttoValidate.html",
						data: advanceType,
			   		}).success(function(data, status) 
					{		
						$scope.maxAdvanceAmt=data[0].maxAdvanceAmt;
						//alert($scope.maxAdvanceAmt);
					}); 				
			};
			
			

			$scope.getAgreementNosForRyot = function(ryotcode,season)
			{
				getagreementsObj.season = season;
				getagreementsObj.ryotcode = ryotcode;
				//alert(JSON.stringify(getagreementsObj));
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAgreementsRyotsBySeason.html",
							data: JSON.stringify(getagreementsObj),
						}).success(function(data, status) 
						{	
						//alert(JSON.stringify(data));
							$scope.agreementsNames=data;
							
						});
			};
			$scope.getVarietyforRyot = function(ryotcode,season)
			{
				getVarietyObj.season = season;
				getVarietyObj.ryotCode = ryotcode;
				//alert(JSON.stringify(getVarietyObj));
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getVariety.html",
							data: JSON.stringify(getVarietyObj),
						}).success(function(data, status) 
						{	
						//alert(JSON.stringify(data));
							//$scope.agreementsNames=data;
							$scope.varietyNames=data;
							
						});
			};
			$scope.getTotalCostbyTotalSeedlings = function(ryotcode,season,seedlings,loanAmount,advanceCode)
			{
				
					getTotalSeedlingsObj.season = season;
					getTotalSeedlingsObj.ryotCode = ryotcode;
					getTotalSeedlingsObj.seedlingsQuantity = seedlings;
					getTotalSeedlingsObj.advanceCode=advanceCode;
					
						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getSeedsCost.html",
							data: JSON.stringify(getTotalSeedlingsObj),
						}).success(function(data, status) 
						{	
						//alert(JSON.stringify(data));
						$scope.AddAdvancestoRyot.totalCost = data[0].totalCost;
						$scope.AddAdvancestoRyot.ryotPayable = data[0].totalCost-loanAmount;
							//$scope.agreementsNames=data;
							
							
						});
			};
			
			$scope.getTotalcost = function(amount,ryotpayable)
			{
				if(amount=="")
				{
					amount=0;
				}
				if(ryotpayable=="")
				{
					ryotpayable=0;
				}
				
				var totalcost  = Number(amount)+Number(ryotpayable);
				$scope.AddAdvancestoRyot.totalCost = totalcost;
			};
			
			$scope.getExtentSize = function(agreement,season)
			{
				 agreeementExtentObj.agreementnumber = agreement;
				  agreeementExtentObj.season = season;
				//added by naidu on 23-01-2017
				var aggtype="1";
				agreeementExtentObj.aggType = aggtype;
				var httpRequest = $http({
							method: 'POST',
							//url : "/SugarERP/getAllAgreementDetails.html",
							url : "/SugarERP/getExtraColumnAgrExtent.html",
							data: JSON.stringify(agreeementExtentObj),
						}).success(function(data, status) 
						{	
						/*
						 var j;
						 var p=0;
						  	for(var s=0;s<data.gridData.length;s++)
							{
								j= data.gridData[s].extentSize;
						 	//	p=j+p;
								p = Number(parseFloat(Math.round(j * 100) / 100).toFixed(2))+ Number(parseFloat(Math.round(p * 100) / 100).toFixed(2));
							p= parseFloat(Math.round(p * 100) / 100).toFixed(2);
							}	
							$scope.AddAdvancestoRyot.extentSize = p;
							
							*/
						$scope.AddAdvancestoRyot.extentSize = data.noOfAcr.toFixed(2);
						var k=data.individualExtent.split(",");
						$scope.AddAdvancestoRyot.plantExtent =parseFloat(k[0]).toFixed(2);
						$scope.AddAdvancestoRyot.ratoonExtent =parseFloat(k[1]).toFixed(2);
						
						
						});
				
			};
			
			//-----------Amount Validation on amount key up--
			$scope.setAmountValidation = function(curAmount)
			{
				//alert($scope.maxAdvanceAmt);
				var curAmount = $('#Amount').val();
				//alert($scope.maxAdvanceAmt);
				var difAmount = Number(curAmount)-Number($scope.maxAdvanceAmt);
				//alert(difAmount);
				$scope.difAmount = difAmount;
			};
			
			//---------Set Ryot Code----------
			$scope.setRyotCode = function(RyotCode,advanceCode,season)
			{
				$scope.AddAdvancestoRyot.ryotName = RyotCode;
				loadDetailsObj.ryotcode = RyotCode;
				loadDetailsObj.advanceCode = advanceCode;
				loadDetailsObj.season = season;
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllRyotAdvanceDetails.html",
						data: JSON.stringify(loadDetailsObj),
			   		}).success(function(data, status) 
					{
					//alert(data.formData.seedlingsQuantity);	
					//alert(data.formData.ryotPayable);
					
						//$scope.ryotCodes=data;
						alert("ryotadvanceonload"+JSON.stringify(data));
						$scope.AddAdvancestoRyot = data.formData;
						$scope.AddAdvancestoRyot.ryotName = data.formData.ryotName;
						$scope.data = data.gridData;
						//alert(JSON.stringify(data.gridData));
						if(data.gridData=="")
						{
							$index = 1;//data.gridData.length;
							$scope.data = [{'id':$index}]
						}
						$scope.isSeedlingAdvance = data.formData.isSeedlingAdvance;
						$scope.AddAdvancestoRyot.onloadSeedlingStatus = data.formData.isItSeedling;
						$scope.AddAdvancestoRyot.seedlingsQuantity = data.formData.seedlingsQuantity;
						$scope.AddAdvancestoRyot.ryotPayable = data.formData.ryotPayable;
						
					});				
			};
			
			
			
	                $scope.companyadvdet=function(RyotCode)
					{
						//alert(RyotCode);
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/GetRyotNameForCode.html",
						data: RyotCode,
			   		}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						
						$scope.AddAdvancestoRyot.ryotCode=RyotCode;
						//alert(JSON.stringify(data[0].ryotName));
//						
//							$scope.AddAdvancestoRyot=data[0];
						$scope.AddAdvancestoRyot.ryotName=data[0].ryotName;
//						
//						//var ryotCodeforName = $scope.AddedPrepareLoan.ryotCode;
//						
//						$scope.AddAdvancestoRyot.ryotCode=data[0].ryotName;
//						$scope.AddAdvancestoRyot.season=data[0].ryotName;
					});
				};
			
			
			
			
			
			//-------Load Season DropDowns-----
			$scope.loadSeasonDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonSeasonCompString,
			   		}).success(function(data, status) 
					{					
						$scope.seasonNames=data;
						$scope.AddAdvancestoRyot = {'modifyFlag':'No','transactionCode':'0','loginId':'makella','isItSeedling':'0','onloadSeedlingStatus':'5'};
					}); 
			};
			////-------Load Variety DropDowns-----
//			$scope.loadVarietyDropDown = function(seasonYear)
//			{	
//			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getVarietyForSeason.html",
//						data: seasonYear,
//			   		}).success(function(data, status) 
//					{	
//						alert(JSON.stringify(data));
//						$scope.varietyNames=data;
//					}); 
//			};
			//-------Load Variety DropDowns-----
			$scope.loadCategoryDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonCompanyCategoryString,
			   		}).success(function(data, status) 
					{									
						$scope.categoryNames=data;
					}); 
			};
			//-------Load Agreements DropDowns on Season Change-----
			$scope.loadAgreementsDropDown = function(seasonYear)
			{	

			var addAgreementObj = new Object();
				addAgreementObj.season = seasonYear;
				//added by naidu on 23-01-2017
			var tablename="AgreementSummary";
				addAgreementObj.tablename = tablename;
			
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAgreementNumbersForSeason.html",
							data: JSON.stringify(addAgreementObj),				
					   }).success(function(data, status) 
						{
							$scope.agreementsNames=data;						
						});						
			};
			//-------Load Advance Type DropDowns-----
			$scope.loadAdvanceTypeDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonAdvanceTypeString,
			   		}).success(function(data, status) 
					{	
						$scope.advanceTypeNames=data;
					}); 
			};
			$scope.loadAddedAdvanceDropDown = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonAdvanceTypeStringaddedAdvance,
			   		}).success(function(data, status) 
					{	
						$scope.addedAdvanceNames=data;
					}); 
			};
			
			//-------Load Advance SubCategory DropDowns-----
			$scope.loadAdvanceSubCategoryDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonAdvanceSubCatString,
			   		}).success(function(data, status) 
					{							
						$scope.advanceSubCatNames=data;
					}); 
			};
			//-------Load Tray Type DropDowns-----
			$scope.loadTrayTypeDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonTrayTypeString,
			   		}).success(function(data, status) 
					{	
						$scope.trayTypeNames=data;
					}); 
			};
			//
			$scope.loadSeedSuppliersDropDown = function()
			{			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonRyotCompString,
			   		}).success(function(data, status) 
					{	
						$scope.supplierNames=data;
					}); 
			};

			//---------get is this Seedling Advance Or not satus on advance type onchange---

			$scope.getSeedlingAdvanceStatus = function(advanceType)
			{
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/isSeedlingAdvance.html",
						data: advanceType,
			   		}).success(function(data, status) 
					{	
					
						$scope.isSeedlingAdvance=data;
						//alert(data);
						$scope.AddAdvancestoRyot.isSeedlingAdvance = data;
						$index=1;
						$scope.data = [{'id':$index}]			
					}); 				
			};
					
			//---------Add Agreement Row------
			$scope.addCompanyAdvanceRows = function()
			{
				$index++;
				$scope.data.push({'id':$index});			
			};

			//---------Delete Agreement Row------
			$scope.removeCompanyAdvanceRows = function(id)
			{
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{	
					if( comArr[i].id === id ) 
					{
						Index = i;
						break;
					}				
				}
				
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			};
			
			//----------Save Advances to ryot-----
			$scope.AddedAdvancestoRyot = function(AddAdvancestoRyot,form)
			{		
				//alert(AddAdvancestoRyot.isItSeedling);			
				if(AddAdvancestoRyot.isItSeedling=='0')
				{
					//$scope.Addsubmitted=true;
					//$scope.Addsubmittedok=true;
					//$scope.submitted=false;
				}
			  else
			   {
				   //$scope.Addsubmitted=true;
				   //$scope.Addsubmittedok=false;
				  // $scope.submitted=true;				   
			   }
				//alert($scope.AdvancestoRyot.$valid);
				if($scope.AdvancestoRyot.$valid)
				{
					CompanyAdvanceGridData = [];
					$scope.data.forEach(function (SeedlingGridData) 
					{
	        			CompanyAdvanceGridData.push(angular.toJson(SeedlingGridData));														 
					});
                     	delete AddAdvancestoRyot['availableBalance'];					
					//alert(JSON.stringify(CompanyAdvanceGridData));
					CompanyAdvanceObj.formData = JSON.stringify(AddAdvancestoRyot);
					CompanyAdvanceObj.gridData = CompanyAdvanceGridData;
							
					$('.btn-hide').attr('disabled',true);
					//alert(JSON.stringify(CompanyAdvanceObj));
					$.ajax({
					    	url:"/SugarERP/saveRyotAdvances.html",
						    processData:true,
					    	type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(CompanyAdvanceObj),
							beforeSend: function(xhr) 
							{
				    	       	xhr.setRequestHeader("Accept", "application/json");
					    	    xhr.setRequestHeader("Content-Type", "application/json");
						    },	
						    success:function(response) 
							{
							
								if(response==true)
								{
									swal("Success", 'Advances Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);	
									
									$scope.AddAdvancestoRyot = angular.copy($scope.master);
									$scope.loadSeasonDropDown();
									$scope.setRowAfterReset();
									$scope.isSeedlingAdvance = '1';
									$scope.addedAdvanceNames = {};
									$scope.ryotCodeData = {};
									form.$setPristine(true);
									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);	
								}
							}
						});			
					}
				   else
				    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}						
					}
			};
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddAdvancestoRyot = angular.copy($scope.master);
				$scope.loadSeasonDropDown();
				$scope.setRowAfterReset();
				$scope.isSeedlingAdvance = '1';
				$scope.addedAdvanceNames = {};
				$scope.ryotCodeData = {};				
			    form.$setPristine(true);			    											   
		   };
			
			//-------------Load added Advances Dropdown------
			$scope.getAddedAdvanceNames = function(seasonCode)
			{
			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getRyotAdvances.html",
						data: seasonCode,
			   		}).success(function(data, status) 
					{							
						$scope.addedAdvanceNames=data;
					}); 
			};
			
			//--------Load Ryot Name Dropdown on season change-----
			$scope.loadRyotNames = function(advanceCode,seasonCode)
			{
				if(advanceCode!=null)
				{
					dropDownObj.season = seasonCode;
					dropDownObj.advanceCode = advanceCode;
					//alert(JSON.stringify(dropDownObj));
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getRyotNamesByAdvances.html",
							data: JSON.stringify(dropDownObj),
			   			}).success(function(data, status) 
						{					
					
							$scope.ryotCodeData=data;
								//$index = data.length;
								
								//alert($index);
								//$scope.data = [{'id':$index}]
							//$scope.data = [{'id':$index}];
							
						}).error(function(data, status)
						{
							$scope.loadRyotCodes();
						});
				}
			  else
			   {
				   $scope.getseasonRyots(seasonCode);
			   }								
			};
			
			//--------Load default agreement number in dropdown---
			
			$scope.getRyotName = function(ryotCode)
			{
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAgreementNosForRyot.html",
						data: ryotCode,
				   }).success(function(data, status) 
				   {
					    var agreementNumberNew = data.split(",");
						
						$scope.AddAdvancestoRyot.agreementNumber = agreementNumberNew[0];
			   	   });				
			};
			
			
		var availableBalObj = new Object();
		$scope.getAvailableBalance = function(ryotCode,season,advanceType)
		{
		availableBalObj.ryotcode = ryotCode;
		availableBalObj.season = season;
		availableBalObj.advanceType = advanceType;
		//alert(JSON.stringify(availableBalObj));
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getSumOfAdvanceAmount.html",
		data: JSON.stringify(availableBalObj),
			 }).success(function(data, status) 
		{
		//alert(JSON.stringify(data));
		$scope.AddAdvancestoRyot.availableBalance=data;

		});	
		};
			
			
		})
		
		////========================================
//				//Company Advances to Ryot		
//		//========================================
//		.controller('CompanyAdvancestoRyot', function($scope,$http)
//		{
//			$index=0;
//			$scope.data = [];
//			var CompanyAdvanceGridData = [];
//			var CompanyAdvanceObj = new Object();
//						
//			var ryotExtObj = new Object();
//			ryotExtObj.dropdownname = "ExtentDetails";
//			var jsonRyotCompString= JSON.stringify(ryotExtObj);
//			
//			var seasonCompanyObj = new Object();
//			seasonCompanyObj.dropdownname = "Season";
//			var jsonSeasonCompString= JSON.stringify(seasonCompanyObj);
//			
//			var varietyCompanyObj = new Object();
//			varietyCompanyObj.dropdownname = "Variety";
//			var jsonVarietyCompString= JSON.stringify(varietyCompanyObj);
//			
//			var CategoryObj = new Object();
//			CategoryObj.dropdownname = "AdvanceCategory";
//			var jsonCompanyCategoryString= JSON.stringify(CategoryObj);		
//			
//			/*var agreementNumberObj = new Object();
//			agreementNumberObj.dropdownname = "AgreementSummary";
//			var jsonCompanyAgreementString= JSON.stringify(agreementNumberObj);*/
//	
//			var AdvanceTypeObj = new Object();
//			AdvanceTypeObj.dropdownname = "CompanyAdvance";
//			var jsonAdvanceTypeString= JSON.stringify(AdvanceTypeObj);
//	
//		    var AdvanceSubCategoryObj = new Object();
//			AdvanceSubCategoryObj.dropdownname = "AdvanceSubCategory";
//			var jsonAdvanceSubCatString= JSON.stringify(AdvanceSubCategoryObj);	
//	
//			var trayTypeObj = new Object();
//			trayTypeObj.dropdownname = "SeedlingTray";
//			var jsonTrayTypeString= JSON.stringify(trayTypeObj);
//
//			var addedadvanceObj = new Object();
//			addedadvanceObj.dropdownname = "SeedlingTray";
//			var jsonAdvanceTypeStringaddedAdvance= JSON.stringify(addedadvanceObj);
//			
//			var dropDownObj = new Object();
//			var loadDetailsObj = new Object();
//			
//			var DropRyotDownLoad = new Object();
//			DropRyotDownLoad.tablename = "AgreementSummary";
//			DropRyotDownLoad.columnname = "ryotcode";
//			DropRyotDownLoad.wherecolumnname = "seasonyear";			
//			
//			$scope.maxAdvanceAmt = 100000;
//			$scope.master = {};
//			//$scope.AddAdvancestoRyot = {'modifyFlag':'No','transactionCode':'0','loginId':'makella','isItSeedling':'0','onloadSeedlingStatus':'5'};
//	
//			//-------Load RyotCode DropDowns-----
//			/*$scope.loadRyotCodes = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonRyotCompString,
//			   		}).success(function(data, status) 
//					{					
//						$scope.ryotCodes=data;
//					}); 
//			};*/
//			  $scope.setRowAfterReset = function()
//			 {
//				    
//				    $scope.data = {};					
//					$index=1;
//					$scope.data = [{'id':$index}]
//			};
//
//			$scope.decimalpoint = function(b)
//			{
//				var decmalPoint = $scope.AddAdvancestoRyot[b];			
//				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(2);
//				$scope.AddAdvancestoRyot[b] = getdecimal;	
//				 if(getdecimal=="NaN")
//					{
//						 $scope.AddAdvancestoRyot[b]="";
//					}
//			};
//			
//			//----------load Ryot Names on Season Change----
//			$scope.getseasonRyots = function(seasoncode)
//			{
//				if(seasoncode!=null)
//				{
//					//DropDownLoad.value = seasoncode;
//					DropRyotDownLoad.value = seasoncode;
//					var jsonRyotDropDown= JSON.stringify(DropRyotDownLoad);
//					//alert(jsonRyotDropDown)
//					var httpRequest = $http({
//							method: 'POST',
//							url : "/SugarERP/getRyotsFromAgreementSummary.html",
//							data: jsonRyotDropDown,
//						}).success(function(data, status) 
//						{		
//							
//							$scope.ryotCodeData=data;
//						}); 		
//				}
//			   else
//			    {
//					$scope.ryotCodeData={};
//				}
//				
//			};				
//			
//			//--------------Amount validation on advance onchange----
//			
//			$scope.getAdvanceAmount = function(advanceType)
//			{				
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getadvanceAmounttoValidate.html",
//						data: advanceType,
//			   		}).success(function(data, status) 
//					{		
//						$scope.maxAdvanceAmt=data[0].maxAdvanceAmt;
//						//alert($scope.maxAdvanceAmt);
//					}); 				
//			};
//			
//			//-----------Amount Validation on amount key up--
//			$scope.setAmountValidation = function(curAmount)
//			{
//				//alert($scope.maxAdvanceAmt);
//				var curAmount = $('#Amount').val();
//				//alert($scope.maxAdvanceAmt);
//				var difAmount = Number(curAmount)-Number($scope.maxAdvanceAmt);
//				//alert(difAmount);
//				$scope.difAmount = difAmount;
//			};
//			
//			//---------Set Ryot Code----------
//			$scope.setRyotCode = function(RyotCode,advanceCode,season)
//			{
//				$scope.AddAdvancestoRyot.ryotName = RyotCode;
//				loadDetailsObj.ryotcode = RyotCode;
//				loadDetailsObj.advanceCode = advanceCode;
//				loadDetailsObj.season = season;
//				
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getAllRyotAdvanceDetails.html",
//						data: JSON.stringify(loadDetailsObj),
//			   		}).success(function(data, status) 
//					{		
//						//$scope.ryotCodes=data;
//						$scope.AddAdvancestoRyot = data.formData;
//						$scope.AddAdvancestoRyot.ryotName = data.formData.ryotName;
//						$scope.data = data.gridData;
//						$scope.isSeedlingAdvance = data.formData.isSeedlingAdvance;
//						$scope.AddAdvancestoRyot.onloadSeedlingStatus = data.formData.isItSeedling;
//						
//					});				
//			};
//			
//			
//			
//	                $scope.companyadvdet=function(RyotCode)
//					{
//						//alert(RyotCode);
//						var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/GetRyotNameForCode.html",
//						data: RyotCode,
//			   		}).success(function(data, status) 
//					{
//						//alert(JSON.stringify(data));
//						
//						$scope.AddAdvancestoRyot.ryotCode=RyotCode;
//						//alert(JSON.stringify(data[0].ryotName));
////						
////							$scope.AddAdvancestoRyot=data[0];
//						$scope.AddAdvancestoRyot.ryotName=data[0].ryotName;
////						
////						//var ryotCodeforName = $scope.AddedPrepareLoan.ryotCode;
////						
////						$scope.AddAdvancestoRyot.ryotCode=data[0].ryotName;
////						$scope.AddAdvancestoRyot.season=data[0].ryotName;
//					});
//				};
//			
//			
//			
//			
//			
//			//-------Load Season DropDowns-----
//			$scope.loadSeasonDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonSeasonCompString,
//			   		}).success(function(data, status) 
//					{					
//						$scope.seasonNames=data;
//						$scope.AddAdvancestoRyot = {'modifyFlag':'No','transactionCode':'0','loginId':'makella','isItSeedling':'0','onloadSeedlingStatus':'5'};
//					}); 
//			};
//			//-------Load Variety DropDowns-----
//			$scope.loadVarietyDropDown = function(seasonYear)
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getVarietyForSeason.html",
//						data: seasonYear,
//			   		}).success(function(data, status) 
//					{	
//						//alert(JSON.stringify(data));
//						$scope.varietyNames=data;
//					}); 
//			};
//			//-------Load Variety DropDowns-----
//			$scope.loadCategoryDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonCompanyCategoryString,
//			   		}).success(function(data, status) 
//					{									
//						$scope.categoryNames=data;
//					}); 
//			};
//			//-------Load Agreements DropDowns on Season Change-----
//			$scope.loadAgreementsDropDown = function(seasonYear)
//			{								
//					var jsonAgreementsList= JSON.stringify(seasonYear);
//					var httpRequest = $http({
//							method: 'POST',
//							url : "/SugarERP/getAgreementNumbersForSeason.html",
//							data: jsonAgreementsList,				
//					   }).success(function(data, status) 
//						{
//							$scope.agreementsNames=data;						
//						});						
//			};
//			//-------Load Advance Type DropDowns-----
//			$scope.loadAdvanceTypeDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonAdvanceTypeString,
//			   		}).success(function(data, status) 
//					{	
//						$scope.advanceTypeNames=data;
//					}); 
//			};
//			$scope.loadAddedAdvanceDropDown = function()
//			{
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonAdvanceTypeStringaddedAdvance,
//			   		}).success(function(data, status) 
//					{	
//						$scope.addedAdvanceNames=data;
//					}); 
//			};
//			
//			//-------Load Advance SubCategory DropDowns-----
//			$scope.loadAdvanceSubCategoryDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonAdvanceSubCatString,
//			   		}).success(function(data, status) 
//					{							
//						$scope.advanceSubCatNames=data;
//					}); 
//			};
//			//-------Load Tray Type DropDowns-----
//			$scope.loadTrayTypeDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonTrayTypeString,
//			   		}).success(function(data, status) 
//					{	
//						$scope.trayTypeNames=data;
//					}); 
//			};
//			//
//			$scope.loadSeedSuppliersDropDown = function()
//			{			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/loadDropDownNames.html",
//						data: jsonRyotCompString,
//			   		}).success(function(data, status) 
//					{	
//						$scope.supplierNames=data;
//					}); 
//			};
//
//			//---------get is this Seedling Advance Or not satus on advance type onchange---
//
//			$scope.getSeedlingAdvanceStatus = function(advanceType)
//			{
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/isSeedlingAdvance.html",
//						data: advanceType,
//			   		}).success(function(data, status) 
//					{	
//						$scope.isSeedlingAdvance=data;
//						$scope.AddAdvancestoRyot.isSeedlingAdvance = data;
//						$index=1;
//						$scope.data = [{'id':$index}]			
//					}); 				
//			};
//					
//			//---------Add Agreement Row------
//			$scope.addCompanyAdvanceRows = function()
//			{
//				$index++;
//				$scope.data.push({'id':$index});			
//			};
//
//			//---------Delete Agreement Row------
//			$scope.removeCompanyAdvanceRows = function(id)
//			{
//				var Index = -1;		
//				var comArr = eval( $scope.data );
//				for( var i = 0; i < comArr.length; i++ ) 
//				{	
//					if( comArr[i].id === id ) 
//					{
//						Index = i;
//						break;
//					}				
//				}
//				
//				$scope.data.splice( Index, 1 );
//				$index = comArr.length;
//				for(var s=0;s<$index;s++)
//				{
//					$scope.data[s].id = Number(s)+Number(1);
//				}
//			};
//			
//			//----------Save Advances to ryot-----
//			$scope.AddedAdvancestoRyot = function(AddAdvancestoRyot,form)
//			{		
//				//alert(AddAdvancestoRyot.isItSeedling);			
//				if(AddAdvancestoRyot.isItSeedling=='0')
//				{
//					//$scope.Addsubmitted=true;
//					//$scope.Addsubmittedok=true;
//					//$scope.submitted=false;
//				}
//			  else
//			   {
//				   //$scope.Addsubmitted=true;
//				   //$scope.Addsubmittedok=false;
//				  // $scope.submitted=true;				   
//			   }
//				//alert($scope.AdvancestoRyot.$valid);
//				if($scope.AdvancestoRyot.$valid)
//				{
//					CompanyAdvanceGridData = [];
//					$scope.data.forEach(function (SeedlingGridData) 
//					{
//	        			CompanyAdvanceGridData.push(angular.toJson(SeedlingGridData));														 
//					});		
//					//alert(JSON.stringify(CompanyAdvanceGridData));
//					CompanyAdvanceObj.formData = JSON.stringify(AddAdvancestoRyot);
//					CompanyAdvanceObj.gridData = CompanyAdvanceGridData;
//							
//					$('.btn-hide').attr('disabled',true);
//					//alert(JSON.stringify(CompanyAdvanceObj));
//					$.ajax({
//					    	url:"/SugarERP/saveRyotAdvances.html",
//						    processData:true,
//					    	type:'POST',
//						    contentType:'Application/json',
//						    data:JSON.stringify(CompanyAdvanceObj),
//							beforeSend: function(xhr) 
//							{
//				    	       	xhr.setRequestHeader("Accept", "application/json");
//					    	    xhr.setRequestHeader("Content-Type", "application/json");
//						    },	
//						    success:function(response) 
//							{
//							
//								if(response==true)
//								{
//									swal("Success", 'Advances Added Successfully!', "success");
//									$('.btn-hide').attr('disabled',false);	
//									
//									$scope.AddAdvancestoRyot = angular.copy($scope.master);
//									$scope.loadSeasonDropDown();
//									$scope.setRowAfterReset();
//									$scope.isSeedlingAdvance = '1';
//									$scope.addedAdvanceNames = {};
//									$scope.ryotCodeData = {};
//									form.$setPristine(true);
//									
//								}
//							   else
//							    {
//								   sweetAlert("Oops...", "Something went wrong!", "error");
//								   $('.btn-hide').attr('disabled',false);	
//								}
//							}
//						});			
//					}
//				   else
//				    {
//						var field = null, firstError = null;
//						for (field in form) 
//						{
//							if (field[0] != '$') 
//							{
//								if (firstError === null && !form[field].$valid) 
//								{
//									firstError = form[field].$name;
//								}
//								if (form[field].$pristine) 
//								{
//									form[field].$dirty = true;
//								}
//							}	
//						}						
//					}
//			};
//			//---------Reset Form----
//		   $scope.reset = function(form)
//		   {			  
//				$scope.AddAdvancestoRyot = angular.copy($scope.master);
//				$scope.loadSeasonDropDown();
//				$scope.setRowAfterReset();
//				$scope.isSeedlingAdvance = '1';
//				$scope.addedAdvanceNames = {};
//				$scope.ryotCodeData = {};				
//			    form.$setPristine(true);			    											   
//		   };
//			
//			//-------------Load added Advances Dropdown------
//			$scope.getAddedAdvanceNames = function(seasonCode)
//			{
//			
//				var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getRyotAdvances.html",
//						data: seasonCode,
//			   		}).success(function(data, status) 
//					{							
//						$scope.addedAdvanceNames=data;
//					}); 
//			};
//			
//			//--------Load Ryot Name Dropdown on season change-----
//			$scope.loadRyotNames = function(advanceCode,seasonCode)
//			{
//				if(advanceCode!=null)
//				{
//					dropDownObj.season = seasonCode;
//					dropDownObj.advanceCode = advanceCode;
//				
//					var httpRequest = $http({
//							method: 'POST',
//							url : "/SugarERP/getRyotNamesByAdvances.html",
//							data: JSON.stringify(dropDownObj),
//			   			}).success(function(data, status) 
//						{					
//							//alert(JSON.stringify(data));
//							$scope.ryotCodeData=data;
//						}).error(function(data, status)
//						{
//							$scope.loadRyotCodes();
//						});
//				}
//			  else
//			   {
//				   $scope.getseasonRyots(seasonCode);
//			   }								
//			};
//			
//			//--------Load default agreement number in dropdown---
//			
//			$scope.getRyotName = function(ryotCode)
//			{
//					var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getAgreementNosForRyot.html",
//						data: ryotCode,
//				   }).success(function(data, status) 
//				   {
//					    var agreementNumberNew = data.split(",");
//						
//						$scope.AddAdvancestoRyot.agreementNumber = agreementNumberNew[0];
//			   	   });				
//			};
//			
//			
//		})
		
	//=============================================
			//Update Extimated Quantity
	//=============================================
		  
	.controller('UpdateEstimatedQtyController', function($scope,$http)
	{
		var filterObj = new Object();
		var obj = new Object();
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "Circle";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
		$scope.master = {};
		var saveEstimatedQtyDataObj = new Object();
									
		$scope.loadSeasonNames = function()
		{
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: jsonStringDropDown,
				 	}).success(function(data, status) 
					{
						$scope.SeasonData=data;
					});  	   
		};
				
		$scope.loadCircleNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
		     }).success(function(data, status) 
			 {
					$scope.circleNames=data;
			 });  	   
		};
		
		$scope.getEstimatedQtyDetails = function(form)
		{
			//$scope.Filtersubmitted = true;
					
				//alert($scope.AddedEstimatedQty.season);
				
			if(form.$valid) 
			{						
				$scope.EstimateQtyData=[];
				filterObj.season = $scope.AddedEstimatedQty.season;
				filterObj.circle = $scope.AddedEstimatedQty.circle;
						
				var httpRequest = $http({
					  method: 'POST',
					  url : "/SugarERP/getAllEstimatedQty.html",
					  data: JSON.stringify(filterObj),
					}).success(function(data, status) 
					{			
						$scope.EstimateQtyData=data;
					});						 
		  	}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{						
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}				
			}
	  };

	  $scope.updateEstimate = function(UpdateEstimate,id)
	  {
			 var UpdatedData = angular.toJson(UpdateEstimate);
			 obj.gridData = UpdatedData;
			 
			 $scope.flag="Single";
			 obj.flag=$scope.flag;			 
			 
			//$('.btn-hide').attr('disabled',true);							
			 
			$.ajax({
			    url:"/SugarERP/SaveEstimateQty.html",
			    processData:true,
			    type:'POST',
			    contentType:'Application/json',
			    data:JSON.stringify(obj),
				beforeSend: function(xhr) 
				{
		            xhr.setRequestHeader("Accept", "application/json");
		            xhr.setRequestHeader("Content-Type", "application/json");
		        },
			    success:function(response) 
				{					
					if(response==true)
					{
			  			swal("Success!", 'Estimated Quantity Updated Successfully!', "success");  //-------For showing success message-------------						
						//$('.btn-hide').attr('disabled',false);							
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
						//$('.btn-hide').attr('disabled',false);							
					}
				}
			});	
	 };			  
	 //-------------------Save Update Estimate Quantity-----
	 $scope.saveEstimatedQtyDetails = function(form)
	 {
		 
		if(form.$valid)
		{
			saveEstimatedQtyData = [];
			$scope.EstimateQtyData.forEach(function (UpdateEstimate) 
			{
	    		saveEstimatedQtyData.push(angular.toJson(UpdateEstimate));														 
			});			
		
			 saveEstimatedQtyDataObj.gridData = saveEstimatedQtyData;
			 $('.btn-hide').attr('disabled',true);		
			$.ajax({
				    url:"/SugarERP/SaveEstimateQty.html",
				    processData:true,
				    type:'POST',
			    	contentType:'Application/json',
				    data:JSON.stringify(saveEstimatedQtyDataObj),
					beforeSend: function(xhr) 
					{
		    	        xhr.setRequestHeader("Accept", "application/json");
		        	    xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{					
						if(response==true)
						{
			  				swal("Success!", 'Estimated Quantity Updated Successfully!', "success");  //-------For showing success message-------------						
							$('.btn-hide').attr('disabled',false);	
							
							$scope.loadCircleNames();
							//$scope.AddedEstimatedQty = angular.copy($scope.master);
							$scope.AddedEstimatedQty.season = "";
							$scope.AddedEstimatedQty.circle = "";
							$scope.EstimateQtyData = {};
							$scope.UpdateEstimatedQty.$setPristine(true);
							form.$setPristine(true);
						}
					   else
					    {
							sweetAlert("Oops...", "Something went wrong!", "error");
							$('.btn-hide').attr('disabled',false);							
						}
					}
				});			 
		}
	   else
	    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{						
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}				
		}
	 };	 
  })	
	
  //==================================
  		//Search Ryot Loans
  //==================================	  
	.controller('SearchRyotLoanController', function($scope,$http)
	{
		var filterObj = new Object();
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "ExtentDetails";
		var StringDropDownExtent= JSON.stringify(DropDownLoad);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			
		$scope.loadryotCode = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownExtent,
			  }).success(function(data, status) 
			  {
				$scope.ryotNames=data;
			  }); 
		};
						
		$scope.loadSeasonNames = function()
		{				
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				}).success(function(data, status) 
				{
					$scope.SeasonNames=data;
				}); 
		};
						
		$scope.getRyotCode = function(RyotCode)
		{
			//alert(90);
			if(RyotCode!=null)
					{
																	
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetRyotNameForCode.html",
					data: RyotCode,
			   }).success(function(data,status) 
			   {
				  // alert(JSON.stringify(data[0].ryotName));
				   //alert(JSON.stringify(data));
				  // alert(565);
				   //$scope.AddedSearchLoan = data[0];
					$scope.AddedSearchLoan.ryotCode = data[0].ryotName;
					
					//var asd=$scope.AddedSearchLoan.ryotCode;
					
					//$scope.AddedSearchLoan.ryot=asd;
					
					//$scope.data = data;
					
				   //alert($scope.AddedSearchLoan.ryotName);
				   //$scope.AddedSearchLoan.ryotName =ryotCode;
				  // var ryotCode = $scope.AddedSearchLoan.ryotName;	
				  // alert(ryotCode);
					//$scope.AddedSearchLoan.ryotCode= ryotCode;
						//$scope.AddedSearchLoan.ryotCode =ryotName;
						
			  		}); 
				 }
	    };
		
		$scope.getRyotDetails = function(AddedSearchLoan)
		{
			$scope.SearRyotLoanData= [];
			$scope.Filtersubmitted = true;
		 	filterObj.season = $scope.AddedSearchLoan.season;

			filterObj.ryotcode = $scope.AddedSearchLoan.ryotName;
			if(filterObj.season!='' && filterObj.ryotcode!='') 
			{
				 var httpRequest = $http({
				 	  method: 'POST',
					  url : "/SugarERP/searchRyotLoans.html",
					  data: JSON.stringify(filterObj),
				   }).success(function(data, status) 
					{
							$scope.SearRyotLoanData=data;
					});						 
			}
		};						
	})
	//=================================
			//Generate Programs
	//=================================
	
	.controller('GeneratePrograms', function($scope,$http)
	{
		var filterObj = new Object();
		
		var maxid = new Object();		
		maxid.tablename = "ProgramSummary";
		var jsonTable= JSON.stringify(maxid);
		 
		var LoadVarietyName = new Object();
		LoadVarietyName.dropdownname = "Variety";
		var VarietyNameString= JSON.stringify(LoadVarietyName);
		
		var LoadDateofPlanting = new Object();
		LoadDateofPlanting.dropdownname = "Period";
		var StringDatePlanting= JSON.stringify(LoadDateofPlanting);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var jsonStringDropDownSeason= JSON.stringify(DropDownLoad);
			
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Month";
		var jsonStringDropDownMonth= JSON.stringify(DropDownLoad);
		
		$scope.data = []; 
		var griddata = [];
		var obj = new Object();
		$scope.master = {};
		 $index=0;
		 
		$scope.getGenerateProgramNumber = function(season)
		{
		  filterObj.season = $scope.AddedGenerateProgram.season;
		 
		  var httpRequest = $http({
		  method: 'POST',
		  url : "/SugarERP/getMaxProgramNoBySeason.html",
		  data: JSON.stringify(filterObj),
		   }).success(function(data, status) 
			{
				$scope.AddedGenerateProgram.programNumber=data[0].programNumber;
			});
	};
	
	$scope.getAddedProgramNumber = function(season)
		{
		  
		  var httpRequest = $http({
		  method: 'POST',
		  url : "/SugarERP/getProgramNoForGenerateProgram.html",
		  data:season,
		   }).success(function(data, status) 
			{
				
				$scope.programnumberNames=data;
			});
	};
	$scope.addFormField = function() 
		{
			
		$index++;
		$scope.data.push({'id':$index,'typeOfExtent':'0'})
		};
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
	
	$scope.getModifyGridDetails = function(season,addedProgramNumber,AddedGenerateProgram)
	{
		filterObj.season = season;
		filterObj.programNumber = addedProgramNumber;
		 $scope.generateprogramData = [] ;
		 $scope.data = [] ;
		 $scope.AddedGenerateProgram.programnumber = addedProgramNumber;
		//alert(JSON.stringify(filterObj));
		 var httpRequest = $http({
		  method: 'POST',
		  url : "/SugarERP/getAllAgreementDetailsForGenerateProgramsByProgramNo.html",
		  data: JSON.stringify(filterObj),
		   }).success(function(data, status) 
			{
				//alert(JSON.stringify(data.formData));
				$scope.AddedGenerateProgram = data.formData;
				$scope.data = data.middleData;
				//alert("data"+JSON.stringify($scope.data) );
				$scope.generateprogramData  = data.gridData;
				$scope.getProgramNumber();
				
				
				for(var s=0;s<data.middleData.length;s++)
				{
					//alert('data[s].middleData.periodId---'+data.middleData[s].periodId);
					//var i = s+1;
					$scope.AddedGenerateProgram.yearOfPlanting=data.middleData[s].yearOfPlanting;
					$scope.AddedGenerateProgram.typeOfExtent=data.middleData[s].typeOfExtent;
					$scope.AddedGenerateProgram.periodId=data.middleData[s].periodId;
					
					//alert(data.middleData[s].varietyOfCane);		
					var dataSplit = data.middleData[s].varietyOfCane.split(",");
					var varietyOfCaneNew = [];	
					for(var a=0;a<dataSplit.length;a++)
					{
						varietyOfCaneNew.push(parseInt(dataSplit[a]));								
					}
					//alert(varietyOfCaneNew);
					$scope.AddedGenerateProgram.varietyOfCane[s]=varietyOfCaneNew;
					
					
					
					
					
					/*alert($scope.varietyNames.length);
					for(var k=0;k<$scope.varietyNames.length;k++)
					{
						alert('2222');
						var dataSplit = data.middleData[s].varietyOfCane.split(",");
						for(var a=0;a<dataSplit.length;a++)
						{
							if(dataSplit[a]==$scope.varietyNames[k].id)
							{
								alert('11444');
								//$scope.data[i].varietyCodeTooltip = $scope.varietyNames[k].variety;
								//$scope.AddedGenerateProgram.varietyOfCane.id=dataSplit[a];
								varietyOfCaneNew.push(parseInt(dataSplit[a]));								

								
							}


						}					
						
						
					}*/
					
					$scope.AddedGenerateProgram.varietyOfCane[s]=varietyOfCaneNew;

					
					

					//alert('====='+varietyOfCaneNew);
					//$scope.AddedGenerateProgram.varietyOfCane=varietyOfCaneNew;

					
					
					
					/*var monthconct = 0;
					if(data.middleData[s].monthId<10) 
					{
						monthconct = "0"+ data.middleData[s].monthId; 
					}
					else 
					{
						monthconct = data.middleData[s].monthId ;
					}
					alert('monthconct--'+monthconct);
					$scope.AddedGenerateProgram.monthId=monthconct.toString();*/


					
				
					//$scope.AddedGenerateProgram.modifyFlag=data[s].middleData.modifyFlag;
					
					
				}
				
				
				
				
					  
				  
				  
				//$scope.AddedGenerateProgram.varietyOfCane = setVariety;
				 
				
				/*$scope.AddedGenerateProgram.programNumber=addedProgramNumber;
				$scope.generateprogramData=data.gridData;
				$scope.getProgramNumber();
				
				
				//$scope.caneAccountGridData=data;

				for(var s=0;s<data.middleData.length;s++)
				{
					$scope.AddedGenerateProgram[s].typeOfExtent=data.middleData[s].typeOfExtent;
								
				}*/
					
				
				
			});
	};
		 
		 $scope.loadProgramCode = function()
		 {
			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: jsonTable,
					   }).success(function(data, status) 
						{
							//alert(data.id);
							$scope.AddedGenerateProgram={'typeOfExtent':"0","modifyFlag":"No"};							
						});											 
		 };
		 
		 $scope.loadVarietyNames = function()
		 {
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: VarietyNameString,
					}).success(function(data, status) 
					{							
						$scope.varietyNames = data;
						data.push({"id":0,"variety":"ALL"});
					});  	   					
		};
				
		$scope.loadPeriodNames = function()
		{
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDatePlanting,
				}).success(function(data, status) 
				{
					
					$scope.periodNames = data;
				});						
		};
		$scope.loadmonthNames = function()
				{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDownMonth,
					}).success(function(data, status) 
					{				
						$scope.monthNames = data;
						//alert(JSON.stringify(data));
						$scope.AddedGenerateProgram={'typeOfExtent':"0"};
					});  	   					
		};
		$scope.loadSeasonNames = function()
		{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDownSeason,
				  }).success(function(data, status) 
				{
					$scope.SeasonNames=data;
				}); 
		};
		
		$scope.year = [ { yearId: 2014, year: 2014 }, { yearId: 2015, year: 2015 }, { yearId: 2016, year: 2016 }, { yearId: 2017, year: 2017 }, { yearId: 2018, year: 2018 }, { yearId: 2019, year: 2019 }, { yearId: 2020, year: 2020 }];
		 
			var ProgramGenerateData = new Object();
			
			$scope.applyProgram = function(form)
			{
				//alert("get details");
				
					//$scope.Filtersubmitted = true;
					
					//$scope.generateprogramData=[];
					//alert(JSON.stringify(filterObj));
					//if($scope.AddedGenerateProgram.programNumber!=null && $scope.AddedGenerateProgram.season!=null) 
					//{
						//alert($scope.AddedGenerateProgram.programNumber);
						if($scope.AddedGenerateProgram.programNumber!="" && $scope.AddedGenerateProgram.season!=null)
						{
							//alert('1');
							$scope.AddedGenerateProgram.modifyFlag="No";
						}
						else
						{
							$scope.AddedGenerateProgram.modifyFlag="Yes";
						}
						
						filterObj.season = $scope.AddedGenerateProgram.season;
						filterObj.programnumber = parseInt($scope.AddedGenerateProgram.programNumber);
						filterObj.modifyFlag = $scope.AddedGenerateProgram.modifyFlag;
						ProgramGenerateData.formData = JSON.stringify(filterObj);
						
						generateAddRowData = [];
						$scope.data.forEach(function (ProgramData) 
						{
							generateAddRowData.push(angular.toJson(ProgramData));														 
						});			
					
						ProgramGenerateData.middleData = generateAddRowData;
			 
						//alert(JSON.stringify(ProgramGenerateData));
								var httpRequest = $http({
						 	  method: 'POST',
							  url : "/SugarERP/getAllAgreementDetailsForGeneratePrograms.html",
							  data: JSON.stringify(ProgramGenerateData),
					 	  }).success(function(data, status) 
							{
								//alert(JSON.stringify(data.gridData));
								$scope.generateprogramData  = data.gridData;
								//$scope.$apply();
								
								
								//$scope.generateprogramData  = data.gridData;
										
																
							});
			 
			 
			// for(var s=0;s<dataSplit.length-1;s++)
//			 {
//				 
//			 }
//console.log(generateAddRowData);
//alert(generateAddRowData.length);
//for(var i=0;i<generateAddRowData.length;i++)
//{
//	var jjj=new Object();
//	jjj=generateAddRowData[i];
//	alert(jjj.id);
//	console.log(jjj.);
//
//}
			 
		//	 var dataSplit = data.formData[0]['varietyOfCane'].split(",");
//				  var setVariety = [];
//				  for(var s=0;s<dataSplit.length-1;s++)
//				  {
//						setVariety.push(parseInt(dataSplit[s]));							
//				  }
//				  //alert(data);
//				 $scope.AddedGenerateProgram.varietyOfCane = setVariety;
			 
			 
			
			 //alert(JSON.stringify(ProgramGenerateData));
						//filterObj.year = $scope.AddedGenerateProgram.yearOfPlanting;
						//filterObj.varietycode = ($scope.AddedGenerateProgram.varietyOfCane).toString();
//						filterObj.typeOfExtent  = $scope.AddedGenerateProgram.typeOfExtent;
//						filterObj.period  = $scope.AddedGenerateProgram.periodId;
//						filterObj.month  = $scope.AddedGenerateProgram.monthId;
//						filterObj.modifyFlag = $scope.AddedGenerateProgram.modifyFlag;
						
						//alert($scope.AddedGenerateProgram.modifyFlag);
						//alert($scope.AddedGenerateProgram.programNumber);
						
						
						//$scope.generateprogramData=[];
						//alert("load grid Details");
						
						
						
						// var httpRequest = $http({
//					 	  method: 'POST',
//						  url : "/SugarERP/getAllAgreementDetailsForGeneratePrograms.html",
//						  data: JSON.stringify(filterObj),
//					   }).success(function(data, status) 
//						{			
//							//alert(JSON.stringify(data));
//							$scope.generateprogramData=data.gridData;
//							var monthconct = 0;
//							if(data.formData[0].monthId<10) { monthconct = "0"+ data.formData[0].monthId; } else { monthconct = data.formData[0].monthId ;}
//							$scope.AddedGenerateProgram.monthId=monthconct.toString();
//							$scope.AddedGenerateProgram.varietyOfCane=data.formData[0].varietyOfCane;
//							$scope.AddedGenerateProgram.modifyFlag=data.formData[0].modifyFlag;
//							$scope.AddedGenerateProgram.yearOfPlanting=data.formData[0].yearOfPlanting;
//							$scope.AddedGenerateProgram.periodId=data.formData[0].periodId;
//							$scope.AddedGenerateProgram.typeOfExtent=data.formData[0].typeOfExtent;
//							$scope.getProgramNumber();
//							
//						});
//				//	}
////				  else
////				   {
//						var field = null, firstError = null;
//						for (field in form) 
//						{
//							if (field[0] != '$') 
//							{
//								if (firstError === null && !form[field].$valid) 
//								{
//									firstError = form[field].$name;
//								}
//								if (form[field].$pristine) 
//								{
//									form[field].$dirty = true;
//								}
//							}	
//					    }					   
				   //}
			};
			
			//$scope.applyonBlurProgram = function()
//			{
//					$scope.Filtersubmitted = true;
//					if($scope.AddedGenerateProgram.season!=null)
//					{
//						$scope.onblurprog = true;
//					}
//					if($scope.AddedGenerateProgram.programNumber==null)
//					{
//					   $scope.AddedGenerateProgram={'typeOfExtent':"0","modifyFlag":"No"};	
//					}
//					else
//					{
//						//alert('1');
//						filterObj.season = $scope.AddedGenerateProgram.season;
//						filterObj.programnumber = parseInt($scope.AddedGenerateProgram.programNumber);
//						filterObj.modifyFlag = $scope.AddedGenerateProgram.modifyFlag;
//						//alert(JSON.stringify(filterObj));
//						if(filterObj.programnumber!='' && filterObj.season!='') 
//						{
//							$scope.generateprogramData=[];
//							 var httpRequest = $http({
//						 	  method: 'POST',
//							  url : "/SugarERP/getAllAgreementDetailsForGeneratePrograms.html",
//							  data: JSON.stringify(filterObj),
//					 	  }).success(function(data, status) 
//							{
//								//alert(JSON.stringify(data.gridData));		
//								$scope.generateprogramData=data.gridData;
//								var monthconct = 0;
//								if(data.formData[0].monthId<10) { monthconct = "0"+ data.formData[0].monthId; } else { monthconct = data.formData[0].monthId ;}
//								$scope.AddedGenerateProgram.monthId=monthconct.toString();
//								$scope.AddedGenerateProgram.varietyOfCane=data.formData[0].varietyOfCane;
//								$scope.AddedGenerateProgram.modifyFlag=data.formData[0].modifyFlag;
//								$scope.AddedGenerateProgram.yearOfPlanting=data.formData[0].yearOfPlanting;
//								$scope.AddedGenerateProgram.periodId=data.formData[0].periodId;
//								$scope.AddedGenerateProgram.typeOfExtent=data.formData[0].typeOfExtent;								
//							});
//						}
//					}
//				};		
			
			$scope.SaveProgramData = function(form)
			{
				if($scope.AddedGenerateProgram.programNumber!="" && $scope.AddedGenerateProgram.season!=null)
						{
							//alert('1');
							$scope.AddedGenerateProgram.modifyFlag="No";
						}
						else
						{
							$scope.AddedGenerateProgram.modifyFlag="Yes";
						}
						
						filterObj.season = $scope.AddedGenerateProgram.season;
						filterObj.programNumber = parseInt($scope.AddedGenerateProgram.programNumber);
						filterObj.modifyFlag = $scope.AddedGenerateProgram.modifyFlag;
						filterObj.totalPlant = parseInt($scope.AddedGenerateProgram.totalPlant);
						filterObj.totalRatoon = parseInt($scope.AddedGenerateProgram.totalRatoon);
						filterObj.totalExtent = parseInt($scope.AddedGenerateProgram.totalExtent);
						ProgramGenerateData.formData = JSON.stringify(filterObj);
						
						generateAddRowData = [];
			$scope.data.forEach(function (ProgramData) 
			{
	    		generateAddRowData.push(angular.toJson(ProgramData));														 
			});	
			
			gridData = [];	
					  $scope.generateprogramData.forEach(function (generateProgram) 
					  {
	            		 gridData.push(angular.toJson(generateProgram));														 
				      });
					  ProgramGenerateData.gridData = gridData;
			 
			 //alert(JSON.stringify(ProgramGenerateData));
			 
			  var httpRequest = $http({
						 	  method: 'POST',
							  url : "/SugarERP/saveGenaratePrograms.html",
							  data: JSON.stringify(ProgramGenerateData),
					 	  }).success(function(data, status) 
							{
								if(data==true)
								{
									
										swal("Success", 'Generate Programs Updated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);	
								$index=1;
					
									$scope.data = [{'id':$index,'typeOfExtent':'0'}];
									$scope.generateprogramData = [];
									//$("#showProgramgrid").hide();
									$scope.AddedGenerateProgram={};
									$scope.AddedGenerateProgram = angular.copy($scope.master);
				 
									form.$setPristine(true);
								
								   // $scope.AddedGenerateProgram = angular.copy($scope.master);
									//form.$setPristine(true);
								}
								else
								{
									sweetAlert("Oops...", "Something went wrong!", "error");
									$('.btn-hide').attr('disabled',false);									
								}								
																
							});
			 
			 
			 
			};
			//$scope.SaveProgramDetails = function(AddedGenerateProgram,form)
//			{
//				//alert(JSON.stringify(AddedGenerateProgram));
//				if(form.$valid)
//				{
//					griddata = []; 	  			
// 					$scope.generateprogramData.forEach(function (generateProgram) 
//					{
//		    		griddata.push(angular.toJson(generateProgram));														 
//					});
//					delete AddedGenerateProgram['programnumber'];
//					
//					var varietymul = $scope.AddedGenerateProgram.varietyOfCane;
//					
//					var VarietyString = "";
//					for(var s=0;s<varietymul.length;s++)
//					{
//						  VarietyString +=varietymul[s]+",";
//					}	
//					
//				//	alert(VarietyString);
//					
//					//delete AddedGenerateProgram['agreementNumber'];					
//					delete AddedGenerateProgram['varietyOfCane'];
//				//AddedGenerateProgram.push({'variety':VarietyString});
//					//alert(angular.toJson(AddedGenerateProgram));
//					obj.gridData = griddata;
//					obj.formData = angular.toJson(AddedGenerateProgram);
//					obj.variety = VarietyString;
//				//	alert(JSON.stringify(obj));
//					$('.btn-hide').attr('disabled',true);								
//					$.ajax({
//					    	url:"/SugarERP/saveGenaratePrograms.html",
//						    processData:true,
//					    	type:'POST',
//						    contentType:'Application/json',
//						    data:JSON.stringify(obj),
//							beforeSend: function(xhr) 
//							{
//			    	        	xhr.setRequestHeader("Accept", "application/json");
//				    	        xhr.setRequestHeader("Content-Type", "application/json");
//				        	},
//						    success:function(response) 
//							{
//								//alert(response);
//								if(response==true)
//								{
//									swal("Success", 'Generate Programs Updated Successfully!', "success");
//									$('.btn-hide').attr('disabled',false);	
//									$scope.loadmonthNames();
//								    $scope.AddedGenerateProgram = angular.copy($scope.master);
//							    	$scope.generateprogramData = {};
//								    form.$setPristine(true);									
//									//location.reload();
//								}
//							   else
//							    {
//									   sweetAlert("Oops...", "Something went wrong!", "error");
//									   $('.btn-hide').attr('disabled',false);								
//								}
//							}
//						});	
//				}
//			   else
//			    {
//						var field = null, firstError = null;
//						for (field in form) 
//						{
//							if (field[0] != '$') 
//							{
//								if (firstError === null && !form[field].$valid) 
//								{
//									firstError = form[field].$name;
//								}
//								if (form[field].$pristine) 
//								{
//									form[field].$dirty = true;
//								}
//							}	
//					    }					   
//				}
//				
//			};
			
			$scope.getProgramNumber = function()
			{
				//alert(JSON.stringify($scope.generateprogramData));
				var gridlength = $scope.generateprogramData.length;
				var programNumber = $scope.AddedGenerateProgram.programNumber;
			//alert("program Number"+programNumber);
				
				//$scope.generateprogramData[0].program = programNumber;
				//alert("program Number"+programNumber);
				var totalplant=0;
				var totalratoon=0;
				for(var s=0;s<gridlength;s++)
				{
					//alert("sd");
					
					if(programNumber==null)
					{
						$scope.generateprogramData[s].programNumber = 0;
					}
					else
					{
						$scope.generateprogramData[s].programNumber = programNumber;
					}
					
					 totalplant += $scope.generateprogramData[s].plantAcres;
					 totalratoon += $scope.generateprogramData[s].ratoonAcres;
				}
				
				$scope.AddedGenerateProgram.totalPlant =  parseFloat(Math.round(totalplant * 100) / 100).toFixed(2);
				$scope.AddedGenerateProgram.totalRatoon = parseFloat(Math.round(totalratoon * 100) / 100).toFixed(2);
				$scope.AddedGenerateProgram.totalExtent = Number($scope.AddedGenerateProgram.totalPlant)+Number($scope.AddedGenerateProgram.totalRatoon);
				var totalacres = parseFloat(Math.round($scope.AddedGenerateProgram.totalExtent * 100) / 100).toFixed(2);
				
				$scope.AddedGenerateProgram.totalExtent = totalacres;
				
			};
			
			
			 //---------Reset Form----
				   $scope.reset = function(form)
				   {	   
						$scope.data = [{'id':$index,'typeOfExtent':'0'}];
						$scope.generateprogramData = [];
						//$("#showProgramgrid").hide();
						$scope.AddedGenerateProgram={};
						$scope.AddedGenerateProgram = angular.copy($scope.master);
	 
						form.$setPristine(true);  	    
				   };
	})	
	
	
	
	
	
   //==================================
   		//Program Details
   //==================================
	
	.controller('programDetailsController', function($scope,$http)
	{
		$scope.data = []; 
		 var gridData = [];
		 var filterObj = new Object();
		 
		var LoadVarietyName = new Object();
		LoadVarietyName.dropdownname = "ProgramSummary";
		var VarietyNameString= JSON.stringify(LoadVarietyName);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var StringDropDownSeason= JSON.stringify(DropDownLoad);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Zone";
		var jsonStringzoneDropDown= JSON.stringify(DropDownLoad);

		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Circle";
		var jsonStringcircleDropDown= JSON.stringify(DropDownLoad);
		

		 $scope.loadProgram = function()
				{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: VarietyNameString,
					}).success(function(data, status) 
					{							
						$scope.programNames = data;
					});  	   					
				};
				
		$scope.loadSeasonNames = function()
		{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDownSeason,
				  }).success(function(data, status) 
				{
					$scope.SeasonNames=data;
				}); 
		};
		
		$scope.loadZoneNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringzoneDropDown,
				}).success(function(data, status) 
				{
				$scope.ZoneData=data;
				}); 
		};
		
		$scope.loadCircleNames = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: jsonStringcircleDropDown,
			}).success(function(data, status) 
			{
				$scope.CircleData=data;
			}); 
			
		};
		
		
				
		$scope.getDetails = function(AddedprogramDetail)
		{
				//alert('get');
				$scope.programData= [];
				filterObj.season = $scope.season;
				filterObj.programnumber = $scope.programNumber;
				
				if(filterObj.season!='' && filterObj.programNumber!='') 
				{
					   var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/getAllProgramDetails.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{
							$scope.AddedprogramDetail=data.formData;
							$scope.programData=data.gridData;
						});
				}										
		};		
	})
	
	
  
   //===================================
  		//Cane Weighment
  //===================================
  
  .controller('CaneWeighment', function($scope,$http)
  {
	  
	 $scope.AddCaneWeight ={'weighmentorder':0,'burntCane':1,'vehicle':1,'vehicleType':0,'partLoad':1,'recieptWeightOneForShiftOne':0,'recieptWeightOneForShiftTwo':0,'recieptWeightOneForShiftThree':0,'recieptWeightTwoForShiftOne':0,'recieptWeightTwoForShiftTwo':0,'recieptWeightTwoForShiftThree':0,'totalWeightForShiftOne':0,'totalWeightForShiftTwo':0,'totalWeightForShiftThree':0};	 
	
	 var seasonObj = new Object();
	 seasonObj.dropdownname = "Season";
	 var jsonSeasonString= JSON.stringify(seasonObj);
	 
	 var CaneWeightSeasonObj = new Object();
	 CaneWeightSeasonObj.dropdownname = "WeighBridge";
	 var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);	 
	 
	 var UnloadingObj = new Object();
	 UnloadingObj.dropdownname = "UnloadingContractor";
	 var jsonUnloadingString= JSON.stringify(UnloadingObj);	 
	 
	 var serialNumber = new Object();
	 serialNumber.tablename = "WeighmentDetails";
	 var serialNumberString= JSON.stringify(serialNumber);
	 
	 $scope.GrossTareData = [{'id':'0','grossTare':'Gross'},{'id':'1','grossTare':'Tare'}]
	 
	 $scope.weightStatus = false;
	// $scope.readonlyStatus = true;
	 $scope.partloadStatus = false;
	$scope.readonlyStatusSecond = false;
	$scope.readonlygrossTare = true;
	$scope.readonlyStatus = true;
	 
	 var SaveCaneWeighment = new Object();
	 $scope.AddCaneWeight.grossTare = '0'; 
	 $scope.AddCaneWeight.operatorName = sessionStorage.getItem('username');
	 
	 $scope.master = {};
	 $scope.GetCaneWeightbyServlet=function()
	  {
			 var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneweightment.html",
			data: {},
		}).success(function(data, status)
		{
						
			var  servletCane = Number(data[0].DATA);
			
			$scope.AddCaneWeight.totalWeight = servletCane;
			
								//alert(data[0].DATA)

		});

	  };
	
	$scope.validateSecondPermitNumber = function(permitNo)
	{
	 var permitValidateObj = new Object();
		var npermitNo = parseInt(permitNo);
		permitValidateObj.permitNo = npermitNo;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getValidatePermitNo.html",
					data: permitNo,
					}).success(function(data, status)
					{
						if(data==true)
						{
							sweetAlert("Oops...", "Permit is already closed for the given permit number. Please enter valid permit number and try again!", "error");
							$scope.AddCaneWeight.secondPermitNumber = '';
							$scope.AddCaneWeight.vehicleNumber = '';
							$scope.AddCaneWeight.vehicle = '1';
						}
						else
						{
						var permitValidateObj = new Object();
		var npermitNo = parseInt($scope.AddCaneWeight.secondPermitNumber);
		var serialNumber = parseInt($scope.AddCaneWeight.serialNumber);

		permitValidateObj.permitNo = npermitNo;
		permitValidateObj.serialNumber = serialNumber;

		//alert("FE"+$scope.AddCaneWeight.permitNumber);

		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/validateHarvestDateForPermitForWeighment.html",
			data: JSON.stringify(permitValidateObj),
		   }).success(function(data, status) 
			{
				$scope.items = data;
				var val = JSON.stringify(data);
				validateFlag = val;
				
				//alert("val"+val);
				if(val==3)
				{
					sweetAlert("Oops...", "Permit Number is expired. Please regenreate permit and  try again!", "error");
					
					$scope.AddCaneWeight.secondPermitNumber ='';
					$scope.AddCaneWeight.ryotCode='';
					$scope.AddCaneWeight.ryotName='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.varietyCode='';
					$scope.AddCaneWeight.village='';
					$scope.AddCaneWeight.circle='';
					$scope.AddCaneWeight.villageCode='';
					$scope.AddCaneWeight.circleCode='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.extentSize='';
					$scope.AddCaneWeight.agreementNumber='';
					$scope.AddCaneWeight.plantOrRatoon = '';
					$scope.AddCaneWeight.plotNumber = '';
					$scope.AddCaneWeight.zoneCode ='';
					$scope.AddCaneWeight.programNumber = '';
					$scope.AddCaneWeight.landVillageCode = '';
					$scope.AddCaneWeight.relativeName = '';
					if(data.plantOrRatoon=='1')
						{
							$scope.AddCaneWeight.plant='';
						}
					   else
						{
							$scope.AddCaneWeight.plant='';
						}
						$scope.AddCaneWeight.weighmentorder =0;
					
					
					
				}
				else if(val==2)
				{
					sweetAlert("Oops...", "Harvesting Date is not given for this permit. Please enter valid permit number and  try again!", "error");
					
					$scope.AddCaneWeight.secondPermitNumber ='';
					$scope.AddCaneWeight.ryotCode='';
					$scope.AddCaneWeight.ryotName='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.varietyCode='';
					$scope.AddCaneWeight.village='';
					$scope.AddCaneWeight.circle='';
					$scope.AddCaneWeight.villageCode='';
					$scope.AddCaneWeight.circleCode='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.extentSize='';
					$scope.AddCaneWeight.agreementNumber='';
					$scope.AddCaneWeight.plantOrRatoon = '';
					$scope.AddCaneWeight.plotNumber = '';
					$scope.AddCaneWeight.zoneCode ='';
					$scope.AddCaneWeight.programNumber = '';
					$scope.AddCaneWeight.landVillageCode = '';
					$scope.AddCaneWeight.relativeName = '';
					if(data.plantOrRatoon=='1')
						{
							$scope.AddCaneWeight.plant='';
						}
					   else
						{
							$scope.AddCaneWeight.plant='';
						}
						$scope.AddCaneWeight.weighmentorder =0;
				
					
					
				}
					})			
						}
				
			
					})
	
	};
	$scope.validateSerialNumber = function(slnoForPermit)
	{
		//alert(slnoForPermit);
	var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getWeigmentDetailsBySerialNumber.html",
				data: slnoForPermit,
			}).success(function(data, status) 
			{
				
					//alert(JSON.stringify(data));								
				//var pNumber  = $scope.AddCaneWeight.permitNumber;
//				var bck = data.permitNumber;
//				//alert("FE"+$scope.AddCaneWeight.permitNumber);
//				//alert("BackEnd"+data.permitNumber);
//				var bck = data.permitNumber;
					//if(bck==pNumber)
//					{
					$scope.AddCaneWeight.varietyCode = data.varietyCode;
				$scope.AddCaneWeight.villageCode = data.villageCode;
				$scope.AddCaneWeight.circleCode = data.circleCode;
				$scope.AddCaneWeight.agreementNumber = data.agreementNumber;
				$scope.AddCaneWeight.plantOrRatoon = data.plantorratoon;
				if(data.plantOrRatoon=='1')
				{
					$scope.AddCaneWeight.plant="Plant";
				}
			   else
			    {
					$scope.AddCaneWeight.plant="Ratoon";
				}
				//alert(data.extentSize);
				$scope.AddCaneWeight.extentSize=data.extentSize;
				
				$scope.AddCaneWeight.ryotCode = data.ryotCode;
				$scope.AddCaneWeight.ryotName = data.ryotName;
				
				$scope.AddCaneWeight.variety = data.variety;
				
				$scope.AddCaneWeight.relativeName = data.relativeName;
				
				$scope.AddCaneWeight.circle = data.circle;
				$scope.AddCaneWeight.village = data.village;
								
				$scope.AddCaneWeight.landVillageCode = data.landVillageCode;
				
				$scope.AddCaneWeight.burntCane = data.burntCane;
				$scope.AddCaneWeight.vehicle = data.vehicle;
				
				$scope.AddCaneWeight.vehicleType = data.vehicleType;
				
				$scope.AddCaneWeight.vehicleNumber = data.vehicleNo;
				
				$scope.AddCaneWeight.partLoad = data.partLoad;
				$scope.AddCaneWeight.secondPermitNumber = data.secondPermitNo;
				$scope.AddCaneWeight.unloadingContractor = data.ucCode;
				
				$scope.AddCaneWeight.particulars = data.varietycode;
				$scope.AddCaneWeight.operatorName = data.loginId;
				//$scope.AddCaneWeight.permitNumber = data.permitNumber;
				$scope.AddCaneWeight.particulars  = data.ucCode;
				if(data.loginId==null || data.loginId=="")
				{
					$scope.AddCaneWeight.operatorName = sessionStorage.getItem('username');
				}
				else
				{
					$scope.AddCaneWeight.operatorName = data.loginId;
				}
				
				$scope.AddCaneWeight.season=data.season;
				$scope.AddCaneWeight.weighBridgeId=data.weighBridgeSlNo;	
				$scope.AddCaneWeight.plotNumber = data.plotNumber;
			    $scope.AddCaneWeight.serialNumber = slnoForPermit;
				$scope.AddCaneWeight.weighmentorder=1;
				$scope.AddCaneWeight.grossTare='1';
					
				$scope.AddCaneWeight.permitNumber = data.permitNumber;

				
				
					//$scope.AddCaneWeight.grossTare = '0';
			});
	};
	
	$scope.serialValidation = function(serialNumber)
	{
		var serialnumber = parseInt(serialNumber);
		
		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getValidateSerialNo.html",
			data: serialnumber,
		}).success(function(data, status)
		{
						if(data==true)
						{
							sweetAlert("Oops...", "Permit is already closed for the given permit number. Please enter valid permit number and try again!", "error");
							$scope.AddCaneWeight.permitNumber = '';
							$scope.AddCaneWeight.serialNumber='';
							$scope.AddCaneWeight.secondPermitNumber ='';
					$scope.AddCaneWeight.ryotCode='';
					$scope.AddCaneWeight.ryotName='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.varietyCode='';
					$scope.AddCaneWeight.village='';
					$scope.AddCaneWeight.circle='';
					$scope.AddCaneWeight.villageCode='';
					$scope.AddCaneWeight.circleCode='';
					$scope.AddCaneWeight.variety='';
					$scope.AddCaneWeight.extentSize='';
					$scope.AddCaneWeight.agreementNumber='';
					$scope.AddCaneWeight.plantOrRatoon = '';
					$scope.AddCaneWeight.plotNumber = '';
					$scope.AddCaneWeight.zoneCode ='';
					$scope.AddCaneWeight.programNumber = '';
					$scope.AddCaneWeight.landVillageCode = '';
					$scope.AddCaneWeight.relativeName = '';
					$scope.AddCaneWeight.vehicleNumber = '';
							$scope.AddCaneWeight.vehicle = '1';
					if(data.plantOrRatoon=='1')
						{
							$scope.AddCaneWeight.plant='';
						}
					   else
						{
							$scope.AddCaneWeight.plant='';
						}
						$scope.AddCaneWeight.weighmentorder =0;
							
							
							
							
						}
						else
						{
							$scope.validateSerialNumber(serialNumber);
						}
			
			
								//alert(data[0].DATA)

		});
		
	};
	
	$scope.validatePermitNumber = function(permitNo)
	 {
											
		//alert('1');		
		var permitValidateObj = new Object();
		var npermitNo = parseInt(permitNo);
		permitValidateObj.permitNo = npermitNo;
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getValidatePermitNo.html",
		data: permitNo,
		}).success(function(data, status)
		{
			if(data==true)
			{
				sweetAlert("Oops...", "Permit is already closed for the given permit number. Please enter valid permit number and try again!", "error");
				$scope.AddCaneWeight.permitNumber = '';
				$scope.AddCaneWeight.vehicleNumber = '';
				$scope.AddCaneWeight.vehicle = '1';
			}
			else
			{
				
				
				var permitValidateObj = new Object();
				var npermitNo = parseInt($scope.AddCaneWeight.permitNumber);
				var serialNumber = parseInt($scope.AddCaneWeight.serialNumber);

				permitValidateObj.permitNo = npermitNo;
				permitValidateObj.serialNumber = serialNumber;

				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/validatePermitCheckInForWeighment.html",
				//url : "/SugarERP/validateHarvestDateForPermitForWeighment.html",
				data: JSON.stringify(permitValidateObj),
				}).success(function(response) 
				{
					
					if(response==false)
					{
						sweetAlert("Oops...", "Permit Number is not CheckedIn. Please CheckIn the permit and try again!", "error");
						
						$scope.AddCaneWeight.permitNumber ='';
						$scope.AddCaneWeight.ryotCode='';
						$scope.AddCaneWeight.ryotName='';
						$scope.AddCaneWeight.variety='';
						$scope.AddCaneWeight.varietyCode='';
						$scope.AddCaneWeight.village='';
						$scope.AddCaneWeight.circle='';
						$scope.AddCaneWeight.villageCode='';
						$scope.AddCaneWeight.circleCode='';
						$scope.AddCaneWeight.variety='';
						$scope.AddCaneWeight.extentSize='';
						$scope.AddCaneWeight.agreementNumber='';
						$scope.AddCaneWeight.plantOrRatoon = '';
						$scope.AddCaneWeight.plotNumber = '';
						$scope.AddCaneWeight.zoneCode ='';
						$scope.AddCaneWeight.programNumber = '';
						$scope.AddCaneWeight.landVillageCode = '';
						$scope.AddCaneWeight.relativeName = '';
						if(data.plantOrRatoon=='1')
							{
								$scope.AddCaneWeight.plant='';
							}
						   else
							{
								$scope.AddCaneWeight.plant='';
							}
							$scope.AddCaneWeight.weighmentorder =0;
						
						
						
					}
					else
					{
					
						//=============Get Details//================
						//alert("setTimeOutStart"+permitNo);
						var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getPermitDetailsByPermitNumber.html",
					data: permitNo,
				}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					var slnoForPermit = data.slno;
					
					
					if(data.slno==0)
					{
					
						var pNumber  = $scope.AddCaneWeight.permitNumber;
						//alert("FE"+$scope.AddCaneWeight.permitNumber);
						//alert("BackEnd"+data.permitNumber);
						var bck = data.permitNumber;
						if(bck==pNumber)
						{
						$scope.AddCaneWeight.ryotCode=data.ryotCode;
						$scope.AddCaneWeight.ryotName=data.ryotName;
						$scope.AddCaneWeight.variety=data.variety;
						$scope.AddCaneWeight.varietyCode=data.varietyCode;
						$scope.AddCaneWeight.village=data.village;
						$scope.AddCaneWeight.circle=data.circle;
						$scope.AddCaneWeight.villageCode=data.villageCode;
						$scope.AddCaneWeight.circleCode=data.circleCode;
						$scope.AddCaneWeight.variety=data.variety;
						$scope.AddCaneWeight.extentSize=data.extentSize;
						$scope.AddCaneWeight.agreementNumber=data.agreementNumber;
						$scope.AddCaneWeight.plantOrRatoon = data.plantOrRatoon;
						$scope.AddCaneWeight.plotNumber = data.plotNumber;
						$scope.AddCaneWeight.zoneCode = data.zoneCode;
						$scope.AddCaneWeight.programNumber = data.programNumber;
						$scope.AddCaneWeight.vehicleNumber = data.vehicleNumber;
						
						$scope.AddCaneWeight.vehicle = data.vehicle;
						if($scope.AddCaneWeight.vehicle==0)
						{
							//naidu today
							$scope.AddCaneWeight.unloadingContractor = 1;
						}
						if(data.plantOrRatoon=='1')
						{
							$scope.AddCaneWeight.plant="Plant";
						}
					   else
						{
							$scope.AddCaneWeight.plant="Ratoon";
						}
						$scope.AddCaneWeight.relativeName=data.relativeName;
						$scope.AddCaneWeight.landVillageCode=data.landVillageCode;
						
						var perNumber  = $scope.AddCaneWeight.permitNumber;
						perNumber = parseInt(perNumber);
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getSecondPermitNo.html",
						data: perNumber,
					   }).success(function(data, status) 
					  {
						  //alert(data);
						  if(data>0)
						  {
							 $scope.AddCaneWeight.secondPermitNumber = data;
							 $scope.AddCaneWeight.partLoad = 0;
							 $scope.AddCaneWeight.unloadingContractor = 1;
							 $scope.AddCaneWeight.particulars = 1;
						  }
					  });
						
						
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getMaxId.html",
						data: serialNumberString,
					   }).success(function(data, status) 
					  {
						 $scope.AddCaneWeight.serialNumber = data.id;
					  });
						}
					}
					else if(data.slno>0)
					{
						
						//sahu ajax
						//alert(slnoForPermit);
						var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getWeigmentDetailsBySerialNumber.html",
					data: slnoForPermit,
				}).success(function(data, status) 
				{
														
					var pNumber  = $scope.AddCaneWeight.permitNumber;
					//alert("FE"+$scope.AddCaneWeight.permitNumber);
					//alert("BackEnd"+data.permitNumber);
					var bck = data.permitNumber;
						if(bck==pNumber)
						{
						$scope.AddCaneWeight.varietyCode = data.varietyCode;
					$scope.AddCaneWeight.villageCode = data.villageCode;
					$scope.AddCaneWeight.circleCode = data.circleCode;
					$scope.AddCaneWeight.agreementNumber = data.agreementNumber;
					$scope.AddCaneWeight.plantOrRatoon = data.plantorratoon;
					if(data.plantOrRatoon=='1')
					{
						$scope.AddCaneWeight.plant="Plant";
					}
				   else
					{
						$scope.AddCaneWeight.plant="Ratoon";
					}
					//alert(data.extentSize);
					$scope.AddCaneWeight.extentSize=data.extentSize;
					
					$scope.AddCaneWeight.ryotCode = data.ryotCode;
					$scope.AddCaneWeight.ryotName = data.ryotName;
					
					$scope.AddCaneWeight.variety = data.variety;
					
					$scope.AddCaneWeight.relativeName = data.relativeName;
					
					$scope.AddCaneWeight.circle = data.circle;
					$scope.AddCaneWeight.village = data.village;
									
					$scope.AddCaneWeight.landVillageCode = data.landVillageCode;
					
					$scope.AddCaneWeight.burntCane = data.burntCane;
					$scope.AddCaneWeight.vehicle = data.vehicle;
					
					$scope.AddCaneWeight.vehicleType = data.vehicleType;
					
					$scope.AddCaneWeight.vehicleNumber = data.vehicleNo;
					
					$scope.AddCaneWeight.partLoad = data.partLoad;
					$scope.AddCaneWeight.secondPermitNumber = data.secondPermitNo;
					$scope.AddCaneWeight.unloadingContractor = data.ucCode;
					
					$scope.AddCaneWeight.particulars = data.varietycode;
					$scope.AddCaneWeight.operatorName = data.loginId;
					//$scope.AddCaneWeight.permitNumber = data.permitNumber;
					$scope.AddCaneWeight.particulars  = data.ucCode;
					if(data.loginId==null || data.loginId=="")
					{
						$scope.AddCaneWeight.operatorName = sessionStorage.getItem('username');
					}
					else
					{
						$scope.AddCaneWeight.operatorName = data.loginId;
					}
					
					$scope.AddCaneWeight.season=data.season;
					$scope.AddCaneWeight.weighBridgeId=data.weighBridgeSlNo;	
					$scope.AddCaneWeight.plotNumber = data.plotNumber;
					$scope.AddCaneWeight.serialNumber = slnoForPermit;
						$scope.AddCaneWeight.weighmentorder=1;
						$scope.AddCaneWeight.grossTare='1';
						
						}
					
					
						//$scope.AddCaneWeight.grossTare = '0';
				}); 
					}
				
					
				});
						
						
					//alert("else lo code end");
					
						
					}
				});
			}
		});
		//alert("else lo code end");
		
		//}, 1000);
										
	};
	
	
	
	 
	  $scope.setRowAfterReset = function()
	  {
		 // alert(2);
		  var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxId.html",
			data: serialNumberString,
		 }).success(function(data, status) 
		 {
			// $scope.data = data;
			 //alert(JSON.stringify(data.id));
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(dataDate, status) 
				 {
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerTime.html",
						data: {},
					 }).success(function(dataTime, status) 
					 {
						 //$scope.AddCaneWeight.season = '2016-2017';
				//$scope.AddCaneWeight.weighBridgeId = 2;
						 $scope.AddCaneWeight ={'weighmentorder':0,'burntCane':1,'vehicle':1,'vehicleType':0,'partLoad':1,'recieptWeightOneForShiftOne':0,'recieptWeightOneForShiftTwo':0,'recieptWeightOneForShiftThree':0,'recieptWeightTwoForShiftOne':0,'recieptWeightTwoForShiftTwo':0,'recieptWeightTwoForShiftThree':0,'totalWeightForShiftOne':0,'totalWeightForShiftTwo':0,'totalWeightForShiftThree':0,'serialNumber':data.id,'caneWeighmentDate':dataDate.serverdate,'season':'2016-2017','weighBridgeId':2,'caneWeighmentTime':dataTime.servertime,'shiftTime':dataTime.servertime,'operatorName':sessionStorage.getItem('username')};
						$scope.loadShiftBridgeDetails();
						$scope.getShiftId(dataTime.servertime);
						$scope.AddCaneWeight.grossTare = '0';
						 $scope.weightStatus = false;
						 $scope.readonlyStatus = true;
						 $scope.partloadStatus = false;
						 //$scope.grossTareDisable = true;
						 $scope.readonlyStatusSecond = false;
						 $scope.readonlyStatusforPermit =false;
						
					 });								 
			 	 });			 		 
		 });
			
	  };
	 
	 //------------------Get Shift Id--------------		
	 $scope.getShiftId = function(caneWeighmentTime)
	 {
	 	var httpRequest = $http({
	 			method: 'POST',
				url : "/SugarERP/getShiftIdNew.html",
				data: caneWeighmentTime,
		     }).success(function(data, status) 
			 {
				 
				//alert(data.ShiftId);
				$scope.AddCaneWeight.shiftId=data.ShiftId;
				if(data.ShiftId=='C')
				{
					 $(".applybgColorGreen").css("color", "blue");
					 $(".applybgColorGreen").css("background-color", "yellow");
					  $(".applybgColorGreen").css("font-weight", "bold");
				}
				else if(data.ShiftId=='B')
				{
					  $(".applybgColorBlue").css("color", "blue");
					   $(".applybgColorBlue").css("background-color", "yellow");
					    $(".applybgColorBlue").css("font-weight", "bold");
				}
				else if(data.ShiftId=='A')
				{
					  $(".applybgColorRed").css("color", "blue");
					   $(".applybgColorRed").css("background-color", "yellow");
					     $(".applybgColorRed").css("font-weight", "bold");
				}
		 	 });
		};		
	
	
	//--------Load Server Date--------
	 $scope.loadServerDate = function()
	 {
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(data, status) 
				 {
					$scope.AddCaneWeight.caneWeighmentDate = data.serverdate;
					$scope.loadShiftBridgeDetails();
			 	 });	
	};
	
	//-----------Load Server Time-------------
	var ntime = '';
	$scope.loadServerTime = function()
	{
		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getServerTime.html",
			data: {},
		 }).success(function(data, status) 
		 {
			// alert(JSON.stringify(data));
			$scope.AddCaneWeight.caneWeighmentTime = data.servertime;
			$scope.AddCaneWeight.shiftTime = data.servertime;
			ntime = data.servertime;
			//alert(data.servertime);
			$scope.getShiftId(data.servertime);
		 });			
	};
	
	//-----------Load MaxId-------------------
	
	$scope.loadCaneWeighMaxId = function()
	{
		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxId.html",
			data: serialNumberString,
		 }).success(function(data, status) 
		 {
			 $scope.AddCaneWeight.serialNumber = data.id;
		 });			
	};
		
	//-----------Load Season Drop Down--------
	
	$scope.loadCaneWeightSeasonNames = function()
	{		
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonSeasonString,
			}).success(function(data, status) 
			{		//alert("1");		
				$scope.seasonNames=data;
				$scope.AddCaneWeight.season = '2016-2017';
				$scope.dataok = data;
				$("#grossTare").prop("disabled", true);
				//alert("2");
			}); 
	};
	
	//----------Load Weigh Bridge Names-------
	
	$scope.loadWeighBridgeNames = function()
	{
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddCaneWeight.weighBridgeId = 2;
				
				
			}); 		
	};
	
	//---------Load Unloading ContractorNames---
	$scope.loadUCNames = function()
	{
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonUnloadingString,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.UCNames=data;
				//$scope.AddCaneWeight.unloadingContractor = 1;
				
			}); 		
	};
	$scope.loadUCC = function(vehicle)
	{
		if(vehicle==0)
		{
			$scope.AddCaneWeight.unloadingContractor = 1;
		}
		else
		{
			$scope.AddCaneWeight.unloadingContractor = "";
		}
	}
	
	
	//-------Get Ryot Details By Serial Number---------
	$scope.getRyotDetailsBySerial = function(serialNumber)
	{
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getWeigmentDetailsBySerialNumber.html",
				data: serialNumber,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				//alert(data.vehicleType);
				$scope.AddCaneWeight.varietyCode = data.varietyCode;
				$scope.AddCaneWeight.villageCode = data.villageCode;
				$scope.AddCaneWeight.circleCode = data.circleCode;
				$scope.AddCaneWeight.agreementNumber = data.agreementNumber;
				$scope.AddCaneWeight.plantOrRatoon = data.plantorratoon;
				if(data.plantOrRatoon=='1')
				{
					$scope.AddCaneWeight.plant="Plant";
				}
			   else
			    {
					$scope.AddCaneWeight.plant="Ratoon";
				}
				//alert(data.extentSize);

				$scope.AddCaneWeight.extentSize=data.extentSize;

				$scope.AddCaneWeight.ryotCode = data.ryotCode;
				$scope.AddCaneWeight.ryotName = data.ryotName;
				
				$scope.AddCaneWeight.variety = data.variety;
				
				$scope.AddCaneWeight.relativeName = data.relativeName;
				
				$scope.AddCaneWeight.circle = data.circle;
				$scope.AddCaneWeight.village = data.village;
				
				
				$scope.AddCaneWeight.landVillageCode = data.landVillageCode;
				
				$scope.AddCaneWeight.burntCane = data.burntCane;
				$scope.AddCaneWeight.vehicle = data.vehicle;
				
				$scope.AddCaneWeight.vehicleType = data.vehicleType;
				
				$scope.AddCaneWeight.vehicleNumber = data.vehicleNo;
				
				$scope.AddCaneWeight.partLoad = data.partLoad;
				$scope.AddCaneWeight.secondPermitNumber = data.secondPermitNo;
				$scope.AddCaneWeight.unloadingContractor = data.ucCode;
				
				$scope.AddCaneWeight.particulars = data.varietycode;
				$scope.AddCaneWeight.operatorName = data.loginId;
				$scope.AddCaneWeight.permitNumber = data.permitNumber;
				$scope.AddCaneWeight.particulars  = data.ucCode;
				if(data.loginId==null || data.loginId=="")
				{
					$scope.AddCaneWeight.operatorName = "makella";
				}
				else
				{
					$scope.AddCaneWeight.operatorName = data.loginId;
				}
				
				$scope.AddCaneWeight.season=data.season;
				$scope.AddCaneWeight.weighBridgeId=data.weighBridgeSlNo;	
				$scope.AddCaneWeight.plotNumber = data.plotNumber;				
			}); 
	};
	
	//-------------Set Contractor Code in Particulars textbox--
	$scope.setContractorCode = function(contCode)
	{
		$scope.AddCaneWeight.particulars = contCode;
	};
	
	//-----------load Weights by shifts wise ----------
	$scope.loadShiftBridgeDetails = function()
	{	
		var shiftDates = new Object();
		var today = new Date();
		var tdate=today.getDate();
		var tmonth=today.getMonth()+1;
		if(tdate<10)
		{
			tdate='0'+tdate;
		} 
		if(tmonth<10)
		{
			tmonth='0'+tmonth
		} 
		
		shiftDates.caneWeighmentDate= tdate+"-"+tmonth+"-"+today.getFullYear();
		shiftDates.caneWeighmentTime=today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
		
		//alert(JSON.stringify(shiftDates));
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getWeigmentDetailsByShiftByDate.html",
				data: JSON.stringify(shiftDates),
			}).success(function(data, status) 
			{		
					
					var Quantity2=0;
				var Quantity3=0;
				var k=	data.length;
				//alert("datalength"+k);
				for(var i=0;i<=k;i++)
				{
					//alert(JSON.stringify(data));
					if(data[i].shiftid1==1)
					{

						
						$scope.AddCaneWeight.recieptWeightOneForShiftOne = data[i].rsc1;
						$scope.AddCaneWeight.recieptWeightTwoForShiftOne = data[i].rsc2;
						$scope.AddCaneWeight.totalWeightForShiftOne =data[i].netwt.toFixed(3);
					}
					else if(data[i].shiftid1==2)
					{
						 
						 $scope.AddCaneWeight.recieptWeightOneForShiftTwo = data[i].rsc1;
						 $scope.AddCaneWeight.recieptWeightTwoForShiftTwo = data[i].rsc2;
						 $scope.AddCaneWeight.totalWeightForShiftTwo =  data[i].netwt.toFixed(3);
					}
					else
					{
						 $scope.AddCaneWeight.recieptWeightOneForShiftThree = data[i].rsc1;
						 $scope.AddCaneWeight.recieptWeightTwoForShiftThree = data[i].rsc2;
						 $scope.AddCaneWeight.totalWeightForShiftThree =  data[i].netwt.toFixed(3);
					}
				}
			
								
			});		
	};
	
	//------Change Serial Number Readonly Status-------
	$scope.changeSerialNumStat = function(weightStatus)
	{
		if(weightStatus=='1')
		{
			$scope.weightStatus=true;
			$scope.readonlyStatus = false;
			//$scope.readonlyStatus = true;
			$scope.AddCaneWeight.grossTare = '1';
			$scope.readonlyStatusforPermit = true;
			$scope.AddCaneWeight.serialNumber = "";
			//$scope.readonlyStatusSecond = true;
			//$scope.readonlyStatusSecond2 = true;
		}
	   else
	    {
			$scope.weightStatus=true;
			$scope.readonlyStatusforPermit = false;
			$scope.readonlyStatus = true;
			//$scope.readonlyStatus = true;
			$scope.AddCaneWeight.grossTare = '0';
			$scope.loadCaneWeighMaxId();
			//$scope.readonlyStatusSecond = false;
			//$scope.readonlyStatusSecond2 = false;
		}
	};
	
	//---------Change PartLoad Validation Status-------
	$scope.changePartloadStatus = function(PartloadStatus)
	{
		if(PartloadStatus=='0')
		{
			$scope.partloadStatus = true;
			$scope.AddCaneWeight.unloadingContractor = 1;
		}
	   else
	    {
			$scope.partloadStatus = false;
			$scope.AddCaneWeight.unloadingContractor = "";
		}
	};
	
	
	//---------Save Cane Weighment---------------------
	  var printSeasonSerial = new Object();
	$scope.SaveCaneWeighment = function(AddCaneWeight,form)
	{
	
		var shiftName = $scope.AddCaneWeight.shiftId;
			var shiftID="";
			
			if(shiftName == 'B')
			{
				
				shiftID=parseInt(2);
				//alert("vachindi");

			}
			else if(shiftName == 'A')
			{
				
				shiftID=parseInt(1);
				

			}
			else if(shiftName == 'C')
			{
				
				shiftID=parseInt(3);
				

			}
		
		if($scope.CaneWeighmentForm.$valid)
		{
			$scope.AddCaneWeight.shiftId = shiftID;
			delete AddCaneWeight['plant'];
			SaveCaneWeighment.formData = AddCaneWeight;
			$('.btn-hide').attr('disabled',true);
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveWeighmentDetails.html",
					data: JSON.stringify(SaveCaneWeighment),
				}).success(function(response, status) 
				{
					var Repseason = response[0].season;
					var RepserailNumber = response[0].serailNumber;
					var RepLorryStatus = response[0].lorryStatus;
					printSeasonSerial.season = Repseason;
					printSeasonSerial.serailNumber = RepserailNumber;
					printSeasonSerial.lorryStatus = RepLorryStatus;
					//printSeasonSerial.lorryStatus = 
					var RepisTare = response[0].isTare;
					
					if(Repseason!='' && RepserailNumber!='' && RepisTare=='1')
					{
						$('.btn-hide').attr('disabled',false);
											$scope.AddCaneWeight = angular.copy($scope.master);				
											form.$setPristine(true);
											$scope.setRowAfterReset();
											$scope.loadServerDate();
											$scope.loadServerTime();
												$('.btn-hide').attr('disabled',false);
												
												
												var httpRequest = $http({
									  method: 'POST',
									  url : "/SugarERP/printCaneWeighment.html",
									  data: JSON.stringify(printSeasonSerial),
								   }).success(function(data, status) 
								   {	
								   //alert(JSON.stringify(data));
								   var grossWeight = data[0].grossweight.toFixed(3);
									var lorryWeight = data[0].lorrywt.toFixed(3);
									var grossWt = data[0].grossWt.toFixed(3);
									var netWt = data[0].netWt.toFixed(3);
								    //alert("second submit"+JSON.stringify(data));
											//var diffnetWt =  data[0].grossweight-data[0].lorrywt;
											// data[0].netwt = diffnetWt;
											//alert("lorryWeight"+data[0].lorrywt);
								  var popupWin = window.open('', '_blank', 'width=300,height=300');
  									popupWin.document.open();
 



popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2.7%;font-size:18px;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].systemdate+'</span><span style="position:fixed; left:25%; top:10%;">Weightment : '+data[0].weighBridge+'</span><span style="position:fixed; left:67%; top:9%;font-size:18px;">'+data[0].actualSlNo+'</span></div><div><span style="position:fixed; left:25%; top:14%;">REF : '+data[0].serialnumber+'</span><span style="position:fixed; left:7%; top:15%;">'+data[0].shiftid+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:21%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:20%; top:39%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:41.5%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:58%; top:55%;">Unloader No.: '+data[0].ucCode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">'+grossWeight+'</span><span style="position:absolute; left:39%; top:71%;">'+lorryWeight+'</span><span style="position:absolute; left:55.5%; top:71%;">'+grossWt+'</span></div><div><span style="position:fixed; left:22%; top:73.5%;"></span><span style="position:absolute; left:34%; top:73.5%;">Binding Material</span><span style="position:absolute; left:56.5%; top:73.5%;">'+data[0].bindingMaterial+'</span></div><div><span style="position:fixed; left:22%; top:75%;"></span><span style="position:absolute; left:39%; top:75%;"></span><span style="position:absolute; left:55.5%; top:75%;">------------------</span></div><div><span style="position:fixed; left:22%; top:77%;"></span><span style="position:absolute; left:39%; top:77%;"></span><span style="position:absolute; left:55.5%; top:77%;">'+netWt+'</span></div><div><span style="position:fixed; left:22%; top:79%;"></span><span style="position:absolute; left:39%; top:79%;"></span><span style="position:absolute; left:55.5%; top:79%;">------------------</span></div><div><span style="position:fixed; left:22%; top:84%;"></span><span style="position:absolute; left:39%; top:84%;"></span><span style="position:absolute; left:55.5%; top:84%;">Permit No.: '+data[0].permitNo+'</span><span style="position:absolute; left:5%; top:90%;">'+data[0].login+'</span></div></body></html>');
popupWin.document.close();


//popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2%;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].systemdate+'</span><span style="position:fixed; left:25%; top:9.3%;">Weightment : '+data[0].weighBridge+'</span><span style="position:fixed; left:67%; top:9%;">'+data[0].serialnumber+'</span></div><div><span style="position:fixed; left:25%; top:14%;"></span><span style="position:fixed; left:7%; top:16%;">'+data[0].shiftid+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:22%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:9%; top:40.5%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35.5%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45.75%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45.75%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:42%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51.75%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:50%; top:51.75%;">Unloader No.: '+data[0].ucCode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">'+data[0].grossweight+'</span><span style="position:absolute; left:39%; top:71%;"></span><span style="position:absolute; left:55.5%; top:71%;"></span></div><div><span style="position:fixed; left:22%; top:73.5%;"></span><span style="position:absolute; left:34%; top:73.5%;"></span><span style="position:absolute; left:55.5%; top:73.5%;"></span></div><div><span style="position:fixed; left:22%; top:75%;"></span><span style="position:absolute; left:39%; top:75%;"></span><span style="position:absolute; left:55.5%; top:75%;"></span></div><div><span style="position:fixed; left:22%; top:77%;"></span><span style="position:absolute; left:39%; top:77%;"></span><span style="position:absolute; left:55.5%; top:77%;"></span></div><div><span style="position:fixed; left:22%; top:79%;"></span><span style="position:absolute; left:39%; top:79%;"></span><span style="position:absolute; left:55.5%; top:79%;"></span></div><div><span style="position:fixed; left:22%; top:84%;"></span><span style="position:absolute; left:39%; top:84%;"></span><span style="position:absolute; left:55.5%; top:84%;">Permit No.: '+data[0].permitNo+'</span></div></body></html>');
										//$scope.data;
								   }).error(function(data,status)
								   {
									
								   });
										//swal({
//											 
//										   	 title: "Weighment Details Added Successfully!",
//										   	 text: " Do You want to take a print out?",
//											 type: "success",
//											 showCancelButton: true,
//											timer: 5000,
//											 //confirmButtonColor: "#000000",
//   											 confirmButtonText: "Print",
//   											 closeOnConfirm: false}, 
									//	function(){
//										
//										
//										
//										});
						//	$('.btn-hide').attr('disabled',false);
//								$scope.AddCaneWeight = angular.copy($scope.master);				
//								form.$setPristine(true);
//								$scope.setRowAfterReset();
//								$scope.loadServerDate();
//								$scope.loadServerTime();
//								$('.btn-hide').attr('disabled',false);	
//
//						$('.btn-hide').attr('disabled',false);
						//location.reload();
						

					}
					else if(Repseason!='' && RepserailNumber!='' && RepisTare=='0')
					{
						//swal("Success!", 'Weighment Details Added Successfully!', "success");									
//							$('.btn-hide').attr('disabled',false);
//						$scope.AddCaneWeight = angular.copy($scope.master);				
//					    form.$setPristine(true);
//						$scope.setRowAfterReset();
//						$scope.loadServerDate();
//						$scope.loadServerTime();
//					    	$scope.getLoginUser();
//							$('.btn-hide').attr('disabled',false);	



$('.btn-hide').attr('disabled',false);
											$scope.AddCaneWeight = angular.copy($scope.master);				
											form.$setPristine(true);
											$scope.setRowAfterReset();
											$scope.loadServerDate();
											$scope.loadServerTime();
												$('.btn-hide').attr('disabled',false);
												
												
												var httpRequest = $http({
									  method: 'POST',
									  url : "/SugarERP/printCaneWeighment.html",
									  data: JSON.stringify(printSeasonSerial),
								   }).success(function(data, status) 
								   {	
								   //alert("first submit"+JSON.stringify(data));
											//alert("gross"+ data[0].grossweight);
											var lorryWt = 0;
											var grossWeight = data[0].grossweight.toFixed(3);
											// data[0].lorrywt = lorryWt
											//var diffnetWt =  data[0].grossweight-data[0].lorrywt;
//											 data[0].netwt = diffnetWt;
//											alert("lorryWeight"+data[0].lorrywt);
											
											
								  var popupWin = window.open('', '_blank', 'width=300,height=300');
  									popupWin.document.open();
 


popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2.7%;font-size:18px;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].systemdate+'</span><span style="position:fixed; left:25%; top:10%;">Weightment : '+data[0].weighBridge+'</span><span style="position:fixed; left:67%; top:9%;font-size:18px;">'+data[0].serialnumber+'</span></div><div><span style="position:fixed; left:25%; top:14%;"></span><span style="position:fixed; left:7%; top:15%;">'+data[0].shiftid+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:21%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:20%; top:39%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:41.5%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:58%; top:55%;">Unloader No.: '+data[0].ucCode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">'+grossWeight+'</span><span style="position:absolute; left:39%; top:71%;"></span><span style="position:absolute; left:55.5%; top:71%;"></span></div><div><span style="position:fixed; left:22%; top:73.5%;"></span><span style="position:absolute; left:34%; top:73.5%;"></span><span style="position:absolute; left:55.5%; top:73.5%;"></span></div><div><span style="position:fixed; left:22%; top:75%;"></span><span style="position:absolute; left:39%; top:75%;"></span><span style="position:absolute; left:55.5%; top:75%;"></span></div><div><span style="position:fixed; left:22%; top:77%;"></span><span style="position:absolute; left:39%; top:77%;"></span><span style="position:absolute; left:55.5%; top:77%;"></span></div><div><span style="position:fixed; left:22%; top:79%;"></span><span style="position:absolute; left:39%; top:79%;"></span><span style="position:absolute; left:55.5%; top:79%;"></span></div><div><span style="position:fixed; left:22%; top:84%;"></span><span style="position:absolute; left:39%; top:84%;"></span><span style="position:absolute; left:55.5%; top:84%;">Permit No.: '+data[0].permitNo+'</span><span style="position:absolute; left:5%; top:90%;">'+data[0].login+'</span></div></body></html>');
popupWin.document.close();






popupWin.document.close();
										//$scope.data;
								   }).error(function(data,status)
								   {
									
								   });
			
						
					}
				
				   else
				    {
					   sweetAlert("Oops...", "Something went wrong!", "error");
					   $('.btn-hide').attr('disabled',false);						
					}
					//$scope.AddCaneWeight=data;
				}); 			
		}	
	   else
	    {
			
			
			var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }			
		}
	};
	 //---------Reset Form----
		   $scope.reset = function(form)		   
		   {			  
		   $('.btn-hide').attr('disabled',false);
				$scope.AddCaneWeight = angular.copy($scope.master);				
			    form.$setPristine(true);
				
				$scope.setRowAfterReset();
				
				$scope.loadServerDate();
				$scope.loadServerTime();

		   };	
  })
  
  
  //==========================================
			 //Cane Crushing Report
		//==========================================

	    .controller('CaneCrushingReport',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};	
			
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Circle' }, { GROWER: '1', GROWER: 'Region' }, { GROWER: '2', GROWER: 'Mandal' }, { GROWER: '3', GROWER: 'Village' }, { GROWER: '4', GROWER: 'Zone' }];		
		})
		
		
		//==========================================
		 //Cumulative CaneCrushing Report
	//==========================================

	    .controller('CumulativeCaneCrushingReport',function($scope,$http)
		{
		
			$scope.AddedSeason={'status':'pdf'};
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
		
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.seasons=data;
			   }); 
		};	
			
			$scope.GROWER = [{ GROWER: '0', GROWER: 'Circle' }, { GROWER: '1', GROWER: 'Region' }, { GROWER: '2', GROWER: 'Mandal' }, { GROWER: '3', GROWER: 'Village' }, { GROWER: '4', GROWER: 'Zone' }];		
		})
				
  
  
  //============================================
  		//Generate Sample Cards
  //============================================
  
	.controller('generateSampleCards', function($scope,$http)
	{

		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var StringDropDownSeason= JSON.stringify(DropDownLoad);

		var ProgramDropDownLoad = new Object();
		ProgramDropDownLoad.dropdownname = "ProgramSummary";
		var StringProgramDropDownSeason= JSON.stringify(ProgramDropDownLoad);

		var filterObj = new Object();
		var generateSampleCardData = [];
		var SampleCardObj = new Object();
		var SampleCardFormObj = new Object();
		$scope.master = {};
		
		$scope.loadSeason = function()
		{
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownSeason,
				  }).success(function(data, status) 
				  {
					 
					 	$scope.SeasonNames=data;
				  }); 
		};
		
		$scope.loadProgram = function(seasonCode)
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: seasonCode,
	  		}).success(function(data, status) 
			{
				$scope.ProgramNames=data;
			}); 
		};
		
		
		//---------Load Sample Card Details----------------
			
		$scope.getSampleCardsDetails = function(form)
		{
			//$scope.Filtersubmitted = true;
			//alert($scope.GenerateCard.season);	
			//if($scope.GenerateCard.season!=null && $scope.GenerateCard.program!=null) 
			if(form.$valid)
			{

				filterObj.season = $scope.GenerateCard.season;
				filterObj.programno = $scope.GenerateCard.program;
				
				var httpRequest = $http({
				 	  method: 'POST',
					  url : "/SugarERP/getAllAgrmntDetails.html",
					  data: JSON.stringify(filterObj),
				   }).success(function(data, status) 
				   {	
						$scope.generateData=data;
				   }).error(function(data,status)
				   {
					   $scope.generateData = [];
				   });
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				
			}
		};
		
		//--------Update Sample Card Details--------------
		
		$scope.GenerateSampleSubmit = function(form)
		{		
				//$scope.Addsubmitted = true;
				
				if($scope.generatesample.$valid)
				{
					generateSampleCardData = [];
					$scope.generateData.forEach(function (generate) 
					{
	        			generateSampleCardData.push(angular.toJson(generate));														 
					});								
				
					SampleCardObj.gridData = generateSampleCardData;
					
					SampleCardFormObj.season = $scope.GenerateCard.season;
					SampleCardFormObj.programno = $scope.GenerateCard.program;
				
					SampleCardObj.formData = SampleCardFormObj;
					//alert(SampleCardObj));
					$('.btn-hide').attr('disabled',true);
					var httpRequest = $http({
					 	  method: 'POST',
						  url : "/SugarERP/saveSampleCards.html",
						  data: JSON.stringify(SampleCardObj),
					   }).success(function(response, status) 
					   {	
							if(response[0].issuccess=="true")
							{
								var samplecardVal = response[0].sampleCard.split(":");
										swal({
											 
										   	 title: "Sample Cards Added Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 timer: 5000,
											 //confirmButtonColor: "#000000",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 function(){
												 
											//window.open("http://localhost:8080/SugarERP/generateCaneSampleCardReport.html?sampleCard="+samplecardVal[1], "CNN_WindowName");
											window.open("http://"+response[0].serverip+":8080/SugarERP/generateCaneSampleCardReport.html?sampleCard="+samplecardVal[1], "CNN_WindowName");
					
									});
										
										
									$scope.GenerateCard = angular.copy($scope.master);
								    $scope.generateData = {};
								   $scope.ProgramNames = {};
								  	form.$setPristine(true);	
								$('.btn-hide').attr('disabled',false);
								
								

							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);														
							}
					   });						 
				}
			   else
			    {
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}									
				}
		};
		
		//-------Assign Sample Cards---------------------
		
		$scope.assignSampleCards = function()
		{
			//alert('1');
			//alert('$scope.generateData.length--------'+$scope.generateData.length);
			var nextVal=0;
			for(var s=0;s<$scope.generateData.length;s++)
			{
				//alert(s);
				//alert('$scope.generateData[s].sampleCard---'+$scope.generateData[s].sampleCard);
				var assignSampleCard = "";
				for(var k=0;k<$scope.generateData[s].sampleCard;k++)
				{
					//alert(k);
					var sampleCard = Number(nextVal)+Number(k)+Number(1);
					assignSampleCard +=sampleCard+",";
				}
				nextVal=sampleCard;
				$scope.generateData[s].season = $scope.GenerateCard.season;
				$scope.generateData[s].programNumber = $scope.GenerateCard.program;
				$scope.generateData[s].sampleCardsId = assignSampleCard.substring(0,assignSampleCard.length-1);				
			}
		};
	})   


   //============================================
   		//Update Average CCS Rating
   //============================================
   .controller('UpdateAvgCCSRating',function($scope,$http)
	{
		
		var seasonAvgDropDownLoad = new Object();
		seasonAvgDropDownLoad.dropdownname = "Season";
		var StringAvgDropDownSeason= JSON.stringify(seasonAvgDropDownLoad);
		
		var avgFilterObj = new Object();
		var AvgCCSRatingByFg = [];
		var AvgCCSRatingByFgObj = new Object();
		var AvgSampleObj = new Object();
		
		$scope.display=1;
		$scope.master = {};

		//-------------Load Season Year Deop Down--
		$scope.loadAvgCCSSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringAvgDropDownSeason,
				  }).success(function(data, status) 
				  {
					  //alert(JSON.stringify(
					 	$scope.AvgCCSSeasonNames=data;
				  }); 
		};
		
		//----------Load Program numbers based on season year-----
		
		$scope.loadAvgCCSProgram = function(seasonCode)
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: seasonCode,
	  		}).success(function(data, status) 
			{
				$scope.AvgCCSProgramNames=data;
			}); 
		};	
		
		//-----------load Sample Card Details----
		
		$scope.AvgCSSFilterSubmit = function(AvgCCSfilter,form)
		{
			//$scope.Filtersubmitted = true;
			
			if($scope.AvgCCSFilterForm.$valid)
			{
				
				avgFilterObj.season = AvgCCSfilter.season;
				avgFilterObj.programno = AvgCCSfilter.program;
				avgFilterObj.pageName = "Average";
				avgFilterObj.samplecard = 0;	 
				avgFilterObj.Flag = "NA";				
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSampleCards.html",
					data: JSON.stringify(avgFilterObj),
		  		}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.AvgCCSSampleCards=data;
					$scope.display=0;
				}); 
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}
			}
		};
		
		//----------------Calculate Average CCS Rating-------------------
		$scope.CalculateAvgCCS = function()
		{
				$scope.display=0;
		};
		
		//----------------Calculate Average CCs Rating By Family Group----
		
		$scope.CalculateAvgbyFg = function()
		{
			AvgCCSRatingByFg = [];
			$scope.AvgCCSSampleCards.forEach(function (AvgSampleCard) 
			{
        		AvgCCSRatingByFg.push(angular.toJson(AvgSampleCard));														 
			});							
			AvgCCSRatingByFgObj.gridData = AvgCCSRatingByFg;
			
			AvgSampleObj.season = $scope.AvgCCSfilter.season;
			AvgSampleObj.programno = $scope.AvgCCSfilter.program;
			AvgSampleObj.pageName = "Average";
			AvgSampleObj.samplecard = 0;	 
			AvgSampleObj.Flag = "NA";				
			
			AvgCCSRatingByFgObj.formData = AvgSampleObj;
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAverageCcsRatingsTemp.html",
				data: JSON.stringify(AvgCCSRatingByFgObj),
		  	}).success(function(data, status) 
			{
				$scope.AvgCCSSampleCards=data;
			}); 
		};
		
		//-----------------Update average CCS Ratings----------
		
		$scope.SaveCCSRatings = function(form)
		{
			
			//$scope.Mainsubmitted = true;
			
			if($scope.UpdateAvgCCSRatingForm.$valid)
			{			
				AvgCCSRatingByFg = [];
				$scope.AvgCCSSampleCards.forEach(function (AvgSampleCard) 
				{
        			AvgCCSRatingByFg.push(angular.toJson(AvgSampleCard));														 
				});							
				AvgCCSRatingByFgObj.gridData = AvgCCSRatingByFg;
			
				AvgSampleObj.season = $scope.AvgCCSfilter.season;
				AvgSampleObj.programno = $scope.AvgCCSfilter.program;
				AvgSampleObj.pageName = "Average";
				AvgSampleObj.samplecard = 0;	 
				AvgSampleObj.Flag = "NA";				
			
				AvgCCSRatingByFgObj.formData = AvgSampleObj;
									
				//alert(JSON.stringify(AvgCCSRatingByFgObj));
				$('.btn-hide').attr('disabled',true);
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveUpdateAverageCcsRatings.html",
					data: JSON.stringify(AvgCCSRatingByFgObj),
			  	}).success(function(data, status) 
				{
					if(data==true)
					{
						swal("Success!", 'Average CCS Ratings Updated Successfully!', "success");
						$('.btn-hide').attr('disabled',false);
						
						$scope.AvgCCSfilter = angular.copy($scope.master);
						$scope.AvgSampleCard = angular.copy($scope.master);
						form.$setPristine(true);
						$scope.AvgCCSFilterForm.$setPristine(true);
						$scope.loadAvgCCSSeason();
						$scope.AvgCCSSampleCards = {};
						$scope.AvgCCSProgramNames = {};
						$scope.display=1;						
					}
					else
					{
						sweetAlert("Oops...", "Something went wrong!", "error");
						$('.btn-hide').attr('disabled',false);																				
					}
				});
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				
			}
			
		};		
		
	})
					  
  //=============================================
  		//Generate Ranking
  //=============================================
  
  .controller('GenerateRanking',function($http,$scope)
   {
		var seasonRankingDropDownLoad = new Object();
		seasonRankingDropDownLoad.dropdownname = "Season";
		var StringRankingDropDownSeason= JSON.stringify(seasonRankingDropDownLoad);	  
		
		var SaveRankings = [];
		$scope.master = {};
		var SaveRankingsObj = new Object();
		//$scope.RankDisplay = 1;
		$scope.RankingFilter= {pageName:'Ranking'};
		
		//-------------Load Season Year Deop Down--
		
		$scope.loadRankingSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringRankingDropDownSeason,
				  }).success(function(data, status) 
				  {					  
					 	$scope.GenerateRankingeasonNames=data;
				  }); 
		};
		
		//-----load Programnumber dropdown on season on change-
		
		$scope.loadRankingProgramBySeason = function(rankingSeason)
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: rankingSeason,
	  		}).success(function(data, status) 
			{
				$scope.GenerateRankingProgramNames=data;
			}); 			
		};	   
		
		//--------Load RankingDetails on Get Details button clik----
		
		$scope.loadRankingDetails = function(RankingFilter,form)
		{
			//$scope.Filtersubmitted = true;
			if($scope.GenerateRankingFilterForm.$valid)
			{
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAgreementDetailsForRanking.html",
					data: JSON.stringify(RankingFilter),
	  			  }).success(function(data, status) 
				  {
						$scope.GenerateRankingData=data;
				  }); 				
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }												
			}
		};
		
		//------------Assign Rankings----------------
		
		$scope.AssignRankings = function()
		{
				//$scope.RankDisplay = 0;
			
				SaveRankings = [];
				$scope.GenerateRankingData.forEach(function (RankingData) 
				{
        			SaveRankings.push(angular.toJson(RankingData));														 													
				});		
				SaveRankingsObj.formData = $scope.RankingFilter;
				SaveRankingsObj.gridData = SaveRankings;
				
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveGenerateRankingTemp.html",
					data: JSON.stringify(SaveRankingsObj),
	  			  }).success(function(data, status) 
				  {					  
						$scope.GenerateRankingData=data;
				  }); 			
			
		};
		
		//-----------Save Generate Rankings----------
		
		$scope.SaveGenerateRankings = function(form)
		{
			//$scope.submitted=true;
			
			if($scope.GenerateRankingGrid.$valid)
			{
				SaveRankings = [];
				$scope.GenerateRankingData.forEach(function (RankingData) 
				{
        			SaveRankings.push(angular.toJson(RankingData));														 													
				});		
				SaveRankingsObj.formData = $scope.RankingFilter;
				SaveRankingsObj.gridData = SaveRankings;
				
				$('.btn-hide').attr('disabled',true);							
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveRanking.html",
					data: JSON.stringify(SaveRankingsObj),
	  			  }).success(function(data, status) 
				  {
						if(data==true)
						{
								swal("Success!", 'Ranks Updated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);	
								$scope.RankingFilter = angular.copy($scope.master);							    								
								//$scope.loadRankingSeason();
								$scope.GenerateRankingData = {};								
								form.$setPristine(true);
								$scope.GenerateRankingFilterForm.$setPristine(true);
								$scope.RankingFilter= {pageName:'Ranking'};
								$scope.GenerateRankingProgramNames = {};
						}			
					   else
					    {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							
						}
				  }); 					
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }								
			}
		};
   })
  
  
  //=============================================
  		//Generate Permits
  //=============================================
  
  .controller('GeneratePermits',function($http,$scope)
  {
		var seasonPermitDropDownLoad = new Object();
		seasonPermitDropDownLoad.dropdownname = "Season";
		var StringPermitDropDownSeason= JSON.stringify(seasonPermitDropDownLoad);	  
		
		$scope.PermitFilter = {pageName:'Permit'};
		var SavePermits = [];
		var SavePermitObj = new Object();
		
		$scope.master = {};
		
		//-------------Load Season Year Deop Down--
		
		$scope.loadPermitSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringPermitDropDownSeason,
				  }).success(function(data, status) 
				  {					  
					 	$scope.GeneratePermiteasonNames=data;
				  }); 
		};
		
		
		
		//-----load Programnumber deopdown on season on change-
		
		$scope.loadProgramBySeason = function(permitSeason)
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: permitSeason,
	  		}).success(function(data, status) 
			{
				$scope.GeneratePermitProgramNames=data;
			}); 			
		};
		
		//---------Get Permit Rules-----------------
		$scope.getPermitRules = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getPermitRules.html",
				data: {},
	  		}).success(function(data, status) 
			{
				$scope.PermitRules = data[0];
			}); 						
		};		
		
		var getPermitDetailsObj  = new Object();
		//--------Get All Permit Details-------------
		$scope.loadPermitDetails = function(season,programno,pageName,fromCcsRating,toCcsRating)
		{

		//
		//	 //$scope.Filtersubmitted = true;	
			getPermitDetailsObj.season = season;
			getPermitDetailsObj.programno = programno;
			getPermitDetailsObj.pageName = pageName;
			getPermitDetailsObj.fromCcsRating=fromCcsRating;
			getPermitDetailsObj.toCcsRating=toCcsRating;
			//alert(JSON.stringify(getPermitDetailsObj));
		//	
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAgreementDetailsForRanking.html",
				data: JSON.stringify(getPermitDetailsObj),
					 }).success(function(data, status) 
				{
				//alert(JSON.stringify(data));
				$scope.GeneratePermitData=data;
				}); 	
		};

		 $scope.ShowDatePicker = function()
		 {
					//alert('ashok');
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		//var maxNumberBySeason = new Object();
		 $scope.loadPermitMaxNumber=function(seasonYear)
	 			{
				
					//alert(season);
					//salert(seasonYear);
				var httpRequest = $http
					({
					method: 'POST',
					url : "/SugarERP/getMaxPermitNum.html",
					data: seasonYear,

	 			}).success(function(data) 
				{
					$scope.PermitFilter.maxNumber =data;
					//alert(JSON.stringify(data));
							
				});
	};
	$scope.getNewPermitRules = function()
		 {
			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getPermitRulesNew.html",
						data: {},
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
						//alert(data[0].permittype);
							//alert(data.id);
							$scope.permitType=data[0].permittype;
							$scope.noofpermitperacre = data[0].noofpermitperacre;
						});											 
		 };
		
		//--------Generate Permit Numbers-----
		$scope.GeneratePermits = function(maxid)
		{	
		//alert(maxid);
			var nextPermit=maxid;
			//alert(nextPermit);
			//alert("Permit Type"+$scope.permitType);
			//alert("No. of permits Per Acre"+$scope.noofpermitperacre);
			
			for(var s=0;s<$scope.GeneratePermitData.length;s++)
			{         
			        
					if($scope.permitType=="1")
					{
					//alert("PlotSize"+$scope.GeneratePermitData[s].plotSize);
					//alert("No of permit per acre"+$scope.noofpermitperacre);
					var permitCount = $scope.GeneratePermitData[s].plotSize*$scope.noofpermitperacre;
				
					}
					else
					{
						var permitCount = $scope.GeneratePermitData[s].plotSize*$scope.PermitRules.noOfTonsPeraAcre/$scope.PermitRules.noOfTonsPerEachPermit;
					
					}
					
					permitCount = Math.ceil(permitCount);
					//alert("permitCount"+permitCount);
					var assignPermitNum = "";
					for(var gp=1;gp<=permitCount;gp++)
					{
												
						var permitNum = Number(nextPermit)+Number(gp);						
							assignPermitNum += permitNum+",";
						
					}
					nextPermit = permitNum;
					
					$scope.GeneratePermitData[s].permitNumber = assignPermitNum.substring(0,assignPermitNum.length-1);
					//alert($scope.GeneratePermitData[s].permitNumber);
					
			}
		};
		
		//---------Save Permit Numbers--------
		$scope.SavePermitNumbers = function(form)
		{
			//$scope.submitted = true;
			if($scope.GeneratePermitGridForm.$valid)
			{
				SavePermits = [];
				$scope.GeneratePermitData.forEach(function (PermtiData) 
				{
        			SavePermits.push(angular.toJson(PermtiData));														 													
				});	
				
				SavePermitObj.formData = $scope.PermitFilter;
				SavePermitObj.gridData = SavePermits;
				
				$('.btn-hide').attr('disabled',true);							
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/saveRanking.html",
						data: JSON.stringify(SavePermitObj),
			  		}).success(function(data, status) 
					{
						if(data==true)
						{
							swal("Success!", 'Permits Updated Successfully!', "success");
							$('.btn-hide').attr('disabled',false);	
							
							$scope.PermitFilter = angular.copy($scope.master);
							$scope.PermtiData = angular.copy($scope.master);
							$scope.GeneratePermitData = {};
						    form.$setPristine(true);
							$scope.GeneratePermitFilterForm.$setPristine(true);
							$scope.GeneratePermitProgramNames = {};
							
						}
					   else
					    {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							
						}
					}); 				
				
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }				
			}
		};
		
  })
  
  
 //=============================================
  		//Perform Cane Accounting Calculation
  //=============================================
  
  .controller('performCaneAccountingCalculation', function($scope,$http)														  
  {
	
	 var accountSeasonObj = new Object();
	 accountSeasonObj.dropdownname = "Season";
	 var jsonAccountSeasonString= JSON.stringify(accountSeasonObj);
	 
	 var dateDivisionObj = new Object();
		 
	 var performDivisionObj = new Object();
	 var submitPerformDivisionData = [];
	 
	 var deductionsAndAcTabObj = new Object();
	 
	 var nullObj = new Object();
	
	 var SerialNumberObj = new Object();
	 SerialNumberObj.tablename = "CaneAccountSmry";
	 SerialNumberObj.col1 = "caneacslno";
	 SerialNumberObj.col2 = "season";

	var VillageDropDown = new Object();
	VillageDropDown.dropdownname = "Village";
	var jsonVillageDropDown= JSON.stringify(VillageDropDown);


	// Added by DMurty on 30-12-2016
	$scope.getRyotCategory = function()
	{
		$scope.getDetailsData={'ryotCategory':'0','advOrLoan':'0','supplyType':'0'};
	};
	 
	 var jsonSerialNumberString= JSON.stringify(SerialNumberObj);
	 
	 //-----------Load Season Drop Down--------	
	$scope.loadCaneAccountSeasonNames = function()
	{		
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonAccountSeasonString,
			}).success(function(data, status) 
			{
				$scope.accountSeasonNames=data;
			}); 
	};
	 //-----------Load Village Drop Down--------	
	$scope.loadVillageNames = function()
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.villageNames=data;
					data.push({"id":"0","village":"ALL"});
				}); 
		};

	//-----------Load Cane Accounting Serial Number--------	
	$scope.loadCaneAccountSerialNumber = function(season)
	{		
		SerialNumberObj.value = season;
		//alert(JSON.stringify(SerialNumberObj));
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxColumnValue.html",
				data: JSON.stringify(SerialNumberObj),
			}).success(function(data, status) 
			{
				//alert('$scope.getDetailsData.caneAccSlno----'+$scope.getDetailsData.caneAccSlno);
				$scope.getDetailsData.caneAccSlno = data.id;
				
			}); 
	};


	
	
	//-----------load Details for Define Dates-----
	$scope.getDetailsFilter = function()
	{
		$scope.filterSubmitted = true;
		if($scope.FirstTabFilter.$valid)
		{
			dateDivisionObj.season = $scope.getDetailsData.season;
			dateDivisionObj.fromdate = $scope.getDetailsData.dateFrom;
			dateDivisionObj.todate = $scope.getDetailsData.dateTo;
			dateDivisionObj.villageCode = $scope.getDetailsData.villageCode;
			//Added by DMurty on 30-12-2016
			var ryotCat = $scope.getDetailsData.ryotCategory
			dateDivisionObj.ryotCategory = parseInt(ryotCat);
			
			
			var advanceOrLoan = $scope.getDetailsData.advOrLoan;
			var suppType = $scope.getDetailsData.supplyType;
			
			dateDivisionObj.advOrLoan = parseInt(advanceOrLoan);
			dateDivisionObj.supplyType = parseInt(suppType);
			

		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getCaneAccRulesWeighmentDtlssForCACalculation.html",
					data: JSON.stringify(dateDivisionObj),
				}).success(function(data, status) 
				{
					
						$scope.caneAccountGridData=data;
					
						if($scope.getDetailsData.supplyType == 0)
						{
							for(var s=0;s<$scope.caneAccountGridData.length;s++)
							{
								$scope.caneAccountGridData[s].totalValue = parseFloat(Math.round(data[s].seasonCanePrice*data[s].netWeight*100)/100).toFixed(2);
								$scope.caneAccountGridData[s].fromDate = $scope.getDetailsData.dateFrom;
								$scope.caneAccountGridData[s].todate = $scope.getDetailsData.dateTo;
								$scope.caneAccountGridData[s].caneAccSlno = $scope.getDetailsData.caneAccSlno;
							}
						}
						else if($scope.getDetailsData.supplyType == 1)
						{
							
							for(var s=0;s<$scope.caneAccountGridData.length;s++)
							{
							
								$scope.caneAccountGridData[s].totalValue = data[s].amtForDed;
								$scope.caneAccountGridData[s].forSbac = data[s].amtForDed;
								$scope.caneAccountGridData[s].forLoans =  parseFloat(0).toFixed(2);
							}
							
						}
						else
						{
							for(var s=0;s<$scope.caneAccountGridData.length;s++)
							{
							
								$scope.caneAccountGridData[s].totalValue = data[s].amtForDed;
								$scope.caneAccountGridData[s].forSbac = data[s].amtForDed;
								$scope.caneAccountGridData[s].forLoans =  parseFloat(0).toFixed(2);
							}
						}
					
					
					
				}); 
		
		
			
		}
	};
	$scope.secondTabwithOutSupply = function(getDetailsData,form)
	{
		
		//alert("hello");
		var gridData = [];
	    var obj = new Object();
		var fobj = new Object();
		  var advanceOrLoan = $scope.getDetailsData.advOrLoan;
		  var suppType = $scope.getDetailsData.supplyType;
		  fobj.advOrLoan = parseInt(advanceOrLoan);
		  fobj.supplyType = parseInt(suppType);
			
//			alert("hello");
//			alert($scope.PerformDivision.$valid);


if($scope.PerformDivision.$valid)
{
	
	 $scope.caneAccountGridData.forEach(function (accountGridData) 
		 {
			if(accountGridData.selectData==true)
			{
				delete accountGridData['amtForDed'];
				delete accountGridData['isPercentOrAmt'];
				delete accountGridData['percentForDed'];
				delete accountGridData['id'];
				
				var repayDate = $scope.getDetailsData.allRepayDate;
				//alert('repayDate-----'+repayDate);
				accountGridData.allRepayDate = repayDate;
				
				var caneAccSlno = $scope.getDetailsData.caneAccSlno;
				accountGridData.caneAccSlno = parseInt(caneAccSlno);
					
				accountGridData.fromDate = $scope.getDetailsData.dateFrom;
				accountGridData.todate = $scope.getDetailsData.dateTo;
				
				accountGridData.todate = $scope.getDetailsData.dateTo;
					
				 gridData.push(angular.toJson(accountGridData));
			}
			
		  });
			
			//obj.formData = getDetailsData;
			
			
			
			obj.formData = JSON.stringify(fobj);
			obj.gridData = gridData;
			
			
			$.ajax({
				url:"/SugarERP/saveCaneValueSplitupDetails.html",
				processData:true,
				type:'POST',
				contentType:'Application/json',
				data:JSON.stringify(obj),
				beforeSend: function(xhr) 
				{
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success:function(response) 
				{
					if(response==true)
					{
						swal("Success", 'Division updated Successfully!', "success");
						$('.btn-hide').attr('disabled',false);
						//alert("dsfd");
						//$(".popup_box").fadeOut();
						
			
						//-----------load Fourth Tab Data----
					deductionsAndAcTabObj.season = $scope.getDetailsData.season;
					deductionsAndAcTabObj.caneAccSlno = $scope.getDetailsData.caneAccSlno;
					deductionsAndAcTabObj.fromDate = $scope.getDetailsData.dateFrom;
					deductionsAndAcTabObj.toDate = $scope.getDetailsData.dateTo;
					
					var advanceOrLoan = $scope.getDetailsData.advOrLoan;
					var suppType = $scope.getDetailsData.supplyType;
					
					deductionsAndAcTabObj.advOrLoan = parseInt(advanceOrLoan);
					deductionsAndAcTabObj.supplyType = parseInt(suppType);
					
					
					//Present we need to change this before CA and after CA.
					//Based on this date, Intrest amount will calculate in frontend.
					//deductionsAndAcTabObj.AllRepayDate = '23-12-2016';

					//
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllAdvancesDetails.html",
							data: JSON.stringify(deductionsAndAcTabObj),
						  }).success(function(data, status) 
						  {	
						  $("#thirTabForm").hide();
						//$('.finalPopupBox').show("slow");
						
						$('.finalPopupBox').fadeIn("slow");
						  if($scope.getDetailsData.supplyType==1)
						  {
							$scope.fourthTabHeaderData = data;					
							var k=0;
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData) 
							{
								$scope.fourthTabHeaderData[k].ryotLoansbac = $scope.fourthTabHeaderData[k].ForLoans;
								$scope.fourthTabHeaderData[k].id = k;
							    $scope.fourthTabHeaderData[k].season = $scope.getDetailsData.season;
								$scope.fourthTabHeaderData[k].caneAccSlno = $scope.getDetailsData.caneAccSlno;
								
								k++;
							});					
					
							var z=0;
					
							
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData)
							{
								var finalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac;
								if($scope.fourthTabHeaderData[z].caneacslno.length==0)
								{
							
									$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
							
								}
								for(var l=0;l<$scope.fourthTabHeaderData[z].caneacslno.length;l++)
								{
									//alert($scope.fourthTabHeaderData[z].caneacslno[l].totalamount);
									//$scope.fourthTabHeaderData[z].caneacslno[l].totalamount =  parseFloat(Math.round(Number(100)+Number(l))).toFixed(2);
									$scope.fourthTabHeaderData[z].caneacslno[l].subventedInterestRate = 0;
									$scope.fourthTabHeaderData[z].caneacslno[l].netRate = $scope.fourthTabHeaderData[z].caneacslno[l].advanceinterest;
									$scope.fourthTabHeaderData[z].caneacslno[l].tempTotal =  $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									//alert("totalAMount"+$scope.fourthTabHeaderData[z].caneacslno[l].totalamount);
									
									$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = $scope.fourthTabHeaderData[z].caneacslno[l].intamount;
									//alert("loanpayable"+$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable);
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount=="" )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].netRate==null || $scope.fourthTabHeaderData[z].caneacslno[l].netRate=="")
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].netRate = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].interestAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].interestAmount=="")
									{
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable=='' )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = parseFloat(0).toFixed(2);
									}
									
																
									var totalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac - $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable;	
															
									if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)>Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
									{
										//alert('if 1');
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);																			
										finalLoanAmount = Number($scope.fourthTabHeaderData[z].ryotLoansbac)-Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable);
										
										$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
										$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);

										
									}
									//alert('the pending amt'+$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount);	
								   else if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)<Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
									{			
										
										//$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(Math.abs(Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable)-Number($scope.fourthTabHeaderData[z].ryotLoansbac)))).toFixed(2);						
										 $scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);	
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount)).toFixed(2);
										$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
										$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
										finalLoanAmount = 0;	
									}
									
								   else
									{	//alert('if 3');
										finalLoanAmount = 0;
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);						
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount)).toFixed(2);
										$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
										$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
									}
									
								 }
								 
								z++;
							});	
						  }
						 else if($scope.getDetailsData.supplyType==2)
						  {
							  	
							$scope.fourthTabHeaderData = data;					
							var k=0;
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData) 
							{
								$scope.fourthTabHeaderData[k].ryotLoansbac = $scope.fourthTabHeaderData[k].ForLoans;
								$scope.fourthTabHeaderData[k].id = k;
							    $scope.fourthTabHeaderData[k].season = $scope.getDetailsData.season;
								$scope.fourthTabHeaderData[k].caneAccSlno = $scope.getDetailsData.caneAccSlno;
								
								k++;
							});					
					
							var z=0;
							
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData)
							{
								var finalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac;
								if($scope.fourthTabHeaderData[z].caneacslno.length==0)
								{
							
									$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
							
								}
								for(var l=0;l<$scope.fourthTabHeaderData[z].caneacslno.length;l++)
								{
									//$scope.fourthTabHeaderData[z].caneacslno[l].totalamount =  parseFloat(Math.round(Number(100)+Number(l))).toFixed(2);
									$scope.fourthTabHeaderData[z].caneacslno[l].subventedInterestRate = 0;
									$scope.fourthTabHeaderData[z].caneacslno[l].netRate = $scope.fourthTabHeaderData[z].caneacslno[l].advanceinterest;
									$scope.fourthTabHeaderData[z].caneacslno[l].tempTotal =  $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = $scope.fourthTabHeaderData[z].caneacslno[l].intamount;
									alert($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount);
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount=="" )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].netRate==null || $scope.fourthTabHeaderData[z].caneacslno[l].netRate=="")
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].netRate = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].interestAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].interestAmount=="")
									{
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable=='' )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = parseFloat(0).toFixed(2);
									}
									
														
									var totalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac - $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable;	
															
									//if(finalLoanAmount>0)
									//{
										if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)>Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
										{
											//alert('if 1');
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);																			
											finalLoanAmount = Number($scope.fourthTabHeaderData[z].ryotLoansbac)-Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable);
											
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);

											
										}
									   else if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)<Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
									    {
											//alert('if 2');				

											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(Math.abs(Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable)-Number($scope.fourthTabHeaderData[z].ryotLoansbac)))).toFixed(2);						
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount) - Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
											finalLoanAmount = 0;									
										}
									   else
									    {	//alert('if 3');
											finalLoanAmount = 0;
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);						
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount) - Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
										}
							
								 }
								z++;
							});
						  }
					  });
						form.$setPristine(true);
			
						$('.btn-hide').attr('disabled',false);	
						
						form.$setPristine(true);			    											   
						
					}
				   else
					{
					   sweetAlert("Oops...", "Something went wrong!", "error");
					   $('.btn-hide').attr('disabled',false);								
					}
				}
				
			});
	
	

}
else
{
	
}
	
	 


//			if($scope.PerformDivision.$valid)
//			{
//			
//			}
//			else
//			{
//			var field = null, firstError = null;
//				for (field in form) 
//				{
//					if (field[0] != '$') 
//						{
//							if (firstError === null && !form[field].$valid) 
//								{
//									firstError = form[field].$name;
//								}
//								if (form[field].$pristine) 
//								{
//									form[field].$dirty = true;
//								}
//						}	
//			    }	
//			}
//			
			
		
		//if(accountGridData.selectData)
//		{
//			alert("dfdsfsdfsdf");
//		}
		
	};
	
	//--------------Apply Cancel Proportion-----------
	$scope.applyProportion = function(index,loans,sbAc,status)
	{
		
		if(loans!=null || sbAc!=null)
		{
			
			if(status==0)
			{
			//alert("status"+status);	
				var FinalLoans = Number(loans)+Number(sbAc);	
		
			
				$scope.caneAccountGridData[index].forLoans = parseFloat(Math.round(FinalLoans)).toFixed(2);
				$scope.caneAccountGridData[index].forSbac = '0.00';
			}
		   else if(status==1)
		    {
				//alert("status"+status);	
			
				if($scope.caneAccountGridData[index].isPercentOrAmt==1)
				{
				//alert("percentorAmt"+$scope.caneAccountGridData[index].isPercentOrAmt);
					$scope.caneAccountGridData[index].forLoans =  parseFloat($scope.caneAccountGridData[index].amtForDed*$scope.caneAccountGridData[index].netWeight).toFixed(2);
					$scope.caneAccountGridData[index].forSbac =  parseFloat(Math.abs($scope.caneAccountGridData[index].amtForDed*$scope.caneAccountGridData[index].netWeight-$scope.caneAccountGridData[index].totalValue)).toFixed(2);
				}
			   else
			    {
			//alert("percentorAmt"+$scope.caneAccountGridData[index].isPercentOrAmt);
					$scope.caneAccountGridData[index].forLoans =  parseFloat($scope.caneAccountGridData[index].totalValue*$scope.caneAccountGridData[index].percentForDed/100).toFixed(2);
					$scope.caneAccountGridData[index].forSbac =  parseFloat(Math.abs($scope.caneAccountGridData[index].totalValue*$scope.caneAccountGridData[index].percentForDed/100-$scope.caneAccountGridData[index].totalValue)).toFixed(2);				
				}		
			}
		}
	};
	
	//-----------Perform Cane Calculations-----------
	$scope.performCaneCalculations = function()
	{
		
		for(var s=0;s<$scope.caneAccountGridData.length;s++)
		{			
	
			if($scope.caneAccountGridData[s].cancelProportion=='1' || $scope.caneAccountGridData[s].cancelProportion==null)
			{
				//alert($scope.caneAccountGridData[s].isPercentOrAmt);
				if($scope.caneAccountGridData[s].isPercentOrAmt==1)
				{
					$scope.caneAccountGridData[s].forSbac =  parseFloat(Math.round(Math.abs($scope.caneAccountGridData[s].amtForDed*$scope.caneAccountGridData[s].netWeight)*100)/100).toFixed(2);
//alert($scope.caneAccountGridData[s].forSbac);
					$scope.caneAccountGridData[s].forLoans =  parseFloat(Math.round(Math.abs($scope.caneAccountGridData[s].totalValue-$scope.caneAccountGridData[s].forSbac)*100)/100).toFixed(2);
				}
			   else if($scope.caneAccountGridData[s].isPercentOrAmt!=1)
			    {
					
					$scope.caneAccountGridData[s].forLoans =  parseFloat(Math.round($scope.caneAccountGridData[s].totalValue*$scope.caneAccountGridData[s].percentForDed/100)).toFixed(2);
					$scope.caneAccountGridData[s].forSbac =  parseFloat(Math.round(Math.abs($scope.caneAccountGridData[s].totalValue*$scope.caneAccountGridData[s].percentForDed/100-$scope.caneAccountGridData[s].totalValue))).toFixed(2);				
				}
			}
		   else
		    {
				//alert("else");
				$scope.caneAccountGridData[s].forLoans  = $scope.caneAccountGridData[s].totalValue;
				$scope.caneAccountGridData[s].forSbac = '0.00';
				
			}
		}
	};
	
	
	
	
	//-------------Submit Perform Division Data--------
	
	$scope.performDivSubmit = function()
	{
		//alert("dfdsfsdf");
		
		//alert(2345678);
			$scope.Tab1Submitted = true;
			//alert("$scope.PerformDivision.$valid"+$scope.PerformDivision.$valid);
			if($scope.PerformDivision.$valid)
			{
				submitPerformDivisionData = [];
				$scope.caneAccountGridData.forEach(function(accountGridData) 
				{
					delete accountGridData['percentForDed'];
					delete accountGridData['amtForDed'];
					delete accountGridData['id'];
					delete accountGridData['isPercentOrAmt'];
					
					var repayDate = $scope.getDetailsData.allRepayDate;
					accountGridData.allRepayDate = repayDate;
										
						
		    	 	submitPerformDivisionData.push(angular.toJson(accountGridData));														 
				});		
				//alert(submitPerformDivisionData);
				performDivisionObj.gridData = submitPerformDivisionData;
				
				
				//Added by Dmurty on 02-03-2017
				var fobj = new Object();
				var advanceOrLoan = $scope.getDetailsData.advOrLoan;
			    var suppType = $scope.getDetailsData.supplyType;
			    fobj.advOrLoan = parseInt(advanceOrLoan);
		 	    fobj.supplyType = parseInt(suppType);
				performDivisionObj.formData = JSON.stringify(fobj);
				
				//alert("sending secondtab"+JSON.stringify(performDivisionObj));
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/saveCaneValueSplitupDetails.html",
						data: performDivisionObj,
					}).success(function(data, status) 
					{ 
						//alert("data"+data);
						if(data==true)
						{
							swal("Success!", 'Divisions Added Successfully!', "success");
							var alertok = document.querySelector(".sweet-alert"),
							okButton = alertok.getElementsByTagName("button")[1];
							$(okButton).click(function() 
							{  		
								$("#thirTabForm").show();
								
							//alert($scope.getDeductionsandAcData());
								//alert($scope.showDeductionsPopup());
								$scope.showDeductionsPopup();
								
								$scope.getDeductionsandAcData();
								
							});
						}
						else
						{
							 sweetAlert("Oops...", "Something went wrong!", "error");
						}
					});				
			}
	};
	
	
		
	 
	 //-----------Display Deduction Division Popup-------
	 $scope.showDeductionsPopup = function()
	 {
		 $('#popup_box').fadeIn("slow");
		 
		// $('#finalPopupBox').fadeIn("slow");
	 };
	 
	 //--------------load Third Tab Grid Data------------
	 $scope.getDeductionsandAcData = function()
	 {
		 deductionsAndAcTabObj.season = $scope.getDetailsData.season;
		 deductionsAndAcTabObj.caneAccSlno = $scope.getDetailsData.caneAccSlno;
		 deductionsAndAcTabObj.fromDate = $scope.getDetailsData.dateFrom;
		 deductionsAndAcTabObj.toDate = $scope.getDetailsData.dateTo;
		 
		 var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getDeductionValues.html",
				data: JSON.stringify(deductionsAndAcTabObj),
			  }).success(function(newPayableData, status) 
			  {	
			  		//alert("New Payable Data"+JSON.stringify(newPayableData));
			  		//console.log(newPayableData);
					//alert(JSON.stringify(newPayableData));
				   var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllCaneValuesSplitupDetails.html",
						data: JSON.stringify(deductionsAndAcTabObj),
					  }).success(function(data, status) 
					  {	
							//alert(JSON.stringify(data));
						    $scope.thirdTabHeaderData = data;
                          							
							 var j=0;
							for(var t=0;t<newPayableData.length;t++)
							{
								
								
								for(var s=0;s<newPayableData[t].caneacslno.length;s++)
								{
									//$scope.thirdTabHeaderData[t].caneacslno[s].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno[s].amount)).toFixed(2);
									$scope.thirdTabHeaderData[t].caneacslno[s].newPayable = 0;
									
									//alert($scope.thirdTabHeaderData[t].caneacslno[s].newPayable);
									$scope.thirdTabHeaderData[t].caneacslno[s].oldPending = parseFloat(Math.round(0)).toFixed(2);
								}
//								alert(newPayableData[t].caneacslno[2]);
//								alert(newPayableData[t].caneacslno[3]);
//								alert(newPayableData[t].caneacslno[4]);
								
								 // $scope.thirdTabHeaderData[t].caneacslno[0].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.cdcamount)).toFixed(2);
								 //  alert(0);
								 // $scope.thirdTabHeaderData[t].caneacslno[1].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.icpamount)).toFixed(2);
								 // alert(1);
								 // $scope.thirdTabHeaderData[t].caneacslno[2].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.hcamount)).toFixed(2);
								   //alert(2);
								  //$scope.thirdTabHeaderData[t].caneacslno[3].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.tcamount)).toFixed(2);
								   //alert(3);
								   
								   //alert('--------'+ newPayableData[t].caneacslno.ucamount);
								  // if(newPayableData[t].caneacslno.ucamount > 0)
								 	// $scope.thirdTabHeaderData[t].caneacslno[4].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.ucamount)).toFixed(2);
								  
								  //$scope.thirdTabHeaderData[t].caneacslno[5].newPayable = parseFloat(Math.round(newPayableData[t].caneacslno.ucamount)).toFixed(2);
								 // alert($scope.thirdTabHeaderData[t].caneacslno[4].newPayable);
								 // alert(4);
								  
								  
								//  $scope.thirdTabHeaderData[t].caneacslno[0].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.cdcpayable)).toFixed(2);
								//  $scope.thirdTabHeaderData[t].caneacslno[1].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.icppayable)).toFixed(2);
								 // $scope.thirdTabHeaderData[t].caneacslno[2].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.hcpayable)).toFixed(2);
								 // $scope.thirdTabHeaderData[t].caneacslno[3].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.tcpayable)).toFixed(2);
								 // $scope.thirdTabHeaderData[t].caneacslno[4].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.ucpayable)).toFixed(2);
								  //$scope.thirdTabHeaderData[t].caneacslno[5].oldPending = parseFloat(Math.round(newPayableData[t].forsbacs.ucpayable)).toFixed(2);
								 
								/*  if(j<=$scope.thirdTabHeaderData[t].caneacslno.length)
								  {
									  for(var i=j;i<i+1;i++)
									  {
										  alert(i);
									  }
									  j=i;
								  }*/
								  //j=i;
							}							  
							

						  //----------Display Old,new and Net Amounts------
						  var k=0;
						  $scope.thirdTabHeaderData.forEach(function(headerData) 
						  {
							  $scope.thirdTabHeaderData[k].ryotsbac = $scope.thirdTabHeaderData[k].forsbacs;
							  $scope.thirdTabHeaderData[k].id = k;
							  $scope.thirdTabHeaderData[k].season = $scope.getDetailsData.season;
							  $scope.thirdTabHeaderData[k].caneAccSlno = $scope.getDetailsData.caneAccSlno;
							  
						  	  k++;
						  });
				  
						  var i=0;				  
						  $scope.thirdTabHeaderData.forEach(function(headerData) 
						  {
								var total = 0;
								var finalAmount = $scope.thirdTabHeaderData[i].forsbacs;
								
								var totalDedAmount = 0;
								
								//alert("length"+$scope.thirdTabHeaderData[i].caneacslno.length);
							 	for(var s=0;s<$scope.thirdTabHeaderData[i].caneacslno.length;s++)
								{	
									//alert($scope.thirdTabHeaderData[i].caneacslno[s].canesupplied);
									totalDedAmount = Number($scope.thirdTabHeaderData[i].caneacslno[s].amount)*Number($scope.thirdTabHeaderData[i].caneacslno[s].canesupplied);
									
									$scope.thirdTabHeaderData[i].caneacslno[s].totalextent = newPayableData[i].caneacslno[s].wextent;
									//alert("forloop"+totalDedAmount);
									
									//$scope.thirdTabHeaderData[i].caneacslno[s].oldPending = parseFloat(Math.round(Number(58000)+Number(s))).toFixed(2);
									//$scope.thirdTabHeaderData[i].caneacslno[s].netPayable = parseFloat(Math.round(Number($scope.thirdTabHeaderData[i].caneacslno[s].oldPending)+Number($scope.thirdTabHeaderData[i].caneacslno[s].newPayable)+Number(totalDedAmount))).toFixed(2);
									//$scope.thirdTabHeaderData[i].caneacslno[s].netPayable = Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)
									var ntPayable ="";
									ntPayable = $scope.thirdTabHeaderData[i].caneacslno[s].netPayable;
									Math.round(ntPayable);
									$scope.thirdTabHeaderData[i].caneacslno[s].netPayable = ntPayable;
									//alert('netpayable - ' + ntPayable);
									
									var netPayableForLoop = Math.round($scope.thirdTabHeaderData[i].caneacslno[s].netPayable * 100)/100;
									//alert("netPayable in forloop"+ netPayableForLoop);
						  			//Math.round($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)
									//$scope.thirdTabHeaderData[i].caneacslno[s].amount = netPayableForLoop;
									//alert("netPayable in forloop"+ netPayableForLoop);
									
									$scope.thirdTabHeaderData[i].caneacslno[s].netPayable = netPayableForLoop;
									
									//alert('before total - ' + $scope.thirdTabHeaderData[i].ryotsbac);
									//var totalAmount = parseFloat(Math.round(Math.abs($scope.thirdTabHeaderData[i].ryotsbac - $scope.thirdTabHeaderData[i].caneacslno[s].netPayable))).toFixed(2);
									var totalAmount = parseFloat(Math.round(Math.abs($scope.thirdTabHeaderData[i].ryotsbac - $scope.thirdTabHeaderData[i].caneacslno[s].netPayable))).toFixed(2);
									
									//alert("totalAmount"+totalAmount);
									
									var pendingamount = 0.00;																				
									//alert("sb"+ $scope.thirdTabHeaderData[i].ryotsbac);
									//alert("sb1"+ $scope.thirdTabHeaderData[i].caneacslno[s].netPayable);
									//if(finalAmount>0)
									//{
										
										if(Number($scope.thirdTabHeaderData[i].ryotsbac)>Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable))
										{
											//alert("RyotSB A/C"+$scope.thirdTabHeaderData[i].ryotsbac);
											//alert("inside if");
											//$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount = parseFloat(Math.round(Math.abs(Number($scope.thirdTabHeaderData[i].caneacslno[s].ryotsbac)-Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)))).toFixed(2);
											$scope.thirdTabHeaderData[i].amount = parseFloat(Math.round(Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)*100)/100).toFixed(2);								
											$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount = parseFloat(Math.round(0)).toFixed(2);
											finalAmount = parseFloat(Math.round(Number($scope.thirdTabHeaderData[i].ryotsbac)-Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)*100)/100).toFixed(2);
											$scope.thirdTabHeaderData[i].ryotsbac = totalAmount;
										}
									   else if(Number($scope.thirdTabHeaderData[i].ryotsbac)<Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable))
									    {
										   //alert('inside else 1');
											//alert("RyotSB A/C"+$scope.thirdTabHeaderData[i].ryotsbac);
											$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount = parseFloat(Math.round(Math.abs(Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable)-Number($scope.thirdTabHeaderData[i].ryotsbac)))).toFixed(2);						
											$scope.thirdTabHeaderData[i].ryotsbac = parseFloat(0).toFixed(2);

											$scope.thirdTabHeaderData[i].caneacslno[s].amount = parseFloat(Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable) - Number($scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount)).toFixed(2);
										//alert("netP"+$scope.thirdTabHeaderData[i].caneacslno[s].netPayable); alert("pending"+$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount);	alert('amount - ' + Number($scope.thirdTabHeaderData[i].amount));
											//finalAmount = 0;
										}
									   else if(Number($scope.thirdTabHeaderData[i].ryotsbac)==Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable))
										{
											//alert("RyotSB A/C"+$scope.thirdTabHeaderData[i].ryotsbac);
											//alert("sbeq");
											finalAmount = 0;
											$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount = parseFloat(Math.round(0)).toFixed(2);
										}
									//}
								   //else
									//{
									  // alert('final amount < 0');
									//	//alert("RyotSB A/C"+$scope.thirdTabHeaderData[i].ryotsbac);
									//	$scope.thirdTabHeaderData[i].caneacslno[s].pendingAmount = $scope.thirdTabHeaderData[i].caneacslno[s].netPayable;
									//}

									finalAmount = finalAmount - Number($scope.thirdTabHeaderData[i].caneacslno[s].netPayable);
									//$scope.thirdTabHeaderData[i].ryotsbac = totalAmount;			
									//alert("Total Amount"+totalAmount);
								}
							 i++;
						  });				  
					  });
				   });		  
	 };
	 
	 
	 //-----------Net Payable Adjustments-------
	 $scope.ryotPayableAdjustment = function(netAmount,id,gridId)
	 {		 
		 var newTotalAmount = 0;
		 var caneAmountCal = parseFloat(Math.round(Number($scope.thirdTabHeaderData[id].caneacslno[gridId].amount)*Number($scope.thirdTabHeaderData[id].caneacslno[gridId].canesupplied))).toFixed(2);
		 
		 
		 $scope.thirdTabHeaderData[id].caneacslno[gridId].pendingAmount = parseFloat(Math.round(Math.abs(Number($scope.thirdTabHeaderData[id].caneacslno[gridId].oldPending)+Number($scope.thirdTabHeaderData[id].caneacslno[gridId].newPayable)+Number(caneAmountCal)-Number($scope.thirdTabHeaderData[id].caneacslno[gridId].netPayable)))).toFixed(2);

		var oneErroramount = parseFloat(Math.round(Math.abs(Number($scope.thirdTabHeaderData[id].caneacslno[gridId].oldPending)+Number($scope.thirdTabHeaderData[id].caneacslno[gridId].newPayable)+Number(caneAmountCal)))).toFixed(2);
		

		 if(Number($scope.thirdTabHeaderData[id].caneacslno[gridId].netPayable)>Number(oneErroramount))
		 {
			 $('#cError'+ id + gridId).show();
			 $('.third').attr("disabled", true);			 
		 }
		else
		 {
			 $('#cError'+ id + gridId).hide();
			 $('.third').attr("disabled", false);
		 }

		 
		 for(var s=0;s<$scope.thirdTabHeaderData[id].caneacslno.length;s++)
		 {
			 /*var newPendingAmountTemp = parseFloat(Math.round(Number($scope.thirdTabHeaderData[id].caneacslno[s].oldPending)+Number($scope.thirdTabHeaderData[id].caneacslno[s].newPayable)+Number($scope.thirdTabHeaderData[id].caneacslno[s].amount)));
			 var newNetPayable = parseFloat(Math.round($scope.thirdTabHeaderData[id].caneacslno[s].netPayable)).toFixed(2);	
			 var newPendingAmount = newPendingAmountTemp-newNetPayable;
			 $scope.thirdTabHeaderData[id].caneacslno[s].pendingAmount = parseFloat(Math.round(Math.abs(newPendingAmount))).toFixed(2);*/
			 //newTotalAmount +=$scope.thirdTabHeaderData[id].caneacslno[s].netPayable;
			  newTotalAmount += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].netPayable);
		 }
		 $scope.thirdTabHeaderData[id].ryotsbac = parseFloat(Math.round($scope.thirdTabHeaderData[id].forsbacs-newTotalAmount)).toFixed(2);	
		 //alert($scope.thirdTabHeaderData[id].ryotsbac);
	 };
	 
	 
	 $scope.pendingAmountChange = function(netPayable,DedPaidAmt,id,gridId)
	 {
		 //alert("DedPaidAmt"+DedPaidAmt);
		 if(DedPaidAmt == '' || DedPaidAmt == null )
		 {
			 var amountZero=0;
			 var amountfixzero = amountZero.toFixed(2);

			 DedPaidAmt = 0;
			 var totalDed =0;
			 //alert("value Empty");
			 var totalEmptyDedAmtinGrid=0;
			 $scope.thirdTabHeaderData[id].caneacslno[gridId].amount = amountfixzero;
			 
			 
			 //var totalDed += $scope.thirdTabHeaderData[id].caneacslno[gridId].amount;
			// alert(totalDed);
			 // for(var s=0;s<$scope.thirdTabHeaderData[id].caneacslno.length;s++)
			 // {
				  // parseInt($scope.thirdTabHeaderData[id].caneacslno[s].amount);
				 
				  //$scope.thirdTabHeaderData[id].caneacslno[s].amount = 0;
				  //totalinGrid += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].pendingAmount);
				  //totalinGrid += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].pendingAmount);
				  //totalEmptyDedAmtinGrid += emptyVal; 
			 // }
			 // alert(totalEmptyDedAmtinGrid);
			  //$scope.thirdTabHeaderData[id].ryotsbac = parseFloat($scope.thirdTabHeaderData[id].forsbacs) - parseFloat(Math.round(totalDedAmtinGrid)).toFixed(2);
			 
		 }
		 
			 
		  var totalDedAmtinGrid=0;
		  var pamt = Number(netPayable)-Number(DedPaidAmt);
		  $scope.thirdTabHeaderData[id].caneacslno[gridId].pendingAmount = parseFloat(pamt).toFixed(2);
		  //alert('1');
		  //alert("DedPaidAmt"+DedPaidAmt);
		  
			  for(var s=0;s<$scope.thirdTabHeaderData[id].caneacslno.length;s++)
			  {
				  //totalinGrid += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].pendingAmount);
				  //totalinGrid += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].pendingAmount);
				  totalDedAmtinGrid += parseInt($scope.thirdTabHeaderData[id].caneacslno[s].amount); 
			  }
			  //alert('totalDedAmtinGrid------'+totalDedAmtinGrid);
			  
			  var amt = parseFloat($scope.thirdTabHeaderData[id].forsbacs) - parseFloat(Math.round(totalDedAmtinGrid)).toFixed(2);
		  	  $scope.thirdTabHeaderData[id].ryotsbac = parseFloat(amt).toFixed(2);
			  
			  //alert("total"+$scope.thirdTabHeaderData[id].ryotsbac);
		 
		 
	 };
	 
	 
	 
	
	 //secondTabwithOutSupply
	 
	 
	 
	 
	 
	 //-----------Save Deductions tab-------
	var thirdTabFinalData = [];	
	 $scope.thirdTabDeductionsSave = function()
	 {		
	 	$scope.Tab3Submitted = true;
		if($scope.ThirdTabDeduction.$valid)
		{
			thirdTabFinalData = [];
			$scope.thirdTabHeaderData.forEach(function(headerData) 
			{
				thirdTabFinalData.push(angular.toJson(headerData));
			});
			
			$('.third').attr('disabled',true);
			//alert(JSON.stringify(thirdTabFinalData));
			//delete [gridData.newPayable]
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveDeductionsAndSBDetails.html",
				data: thirdTabFinalData,
			  }).success(function(data, status) 
			  {	
				// alert(data);
			  		
				 if(data==true)	
				 {					 
					swal("Success!", 'Deductions Added Successfully!', "success");
					var alertok = document.querySelector(".sweet-alert"),
					//alert(alertok);
					okButton = alertok.getElementsByTagName("button")[1];
					
					$(okButton).click(function() 
					{  	
					//alert("third tab go");
					
					$('#popup_box').hide("slow");
						 //$('#popup_box').fadOut("slow");
//						 alert("real third tab go ");
						 $('.finalPopupBox').show("slow");
//						 alert("fourth tab come");
					//-----------load Fourth Tab Data----
					deductionsAndAcTabObj.season = $scope.getDetailsData.season;
					deductionsAndAcTabObj.caneAccSlno = $scope.getDetailsData.caneAccSlno;
					deductionsAndAcTabObj.fromDate = $scope.getDetailsData.dateFrom;
					deductionsAndAcTabObj.toDate = $scope.getDetailsData.dateTo;
					deductionsAndAcTabObj.supplyType = parseInt($scope.getDetailsData.supplyType);
					deductionsAndAcTabObj.advOrLoan = parseInt($scope.getDetailsData.advOrLoan);
					
					//Present we need to change this before CA and after CA.
					//Based on this date, Intrest amount will calculate in frontend.
					deductionsAndAcTabObj.AllRepayDate = '23-12-2016';

					//
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllAdvancesDetails.html",
							data: JSON.stringify(deductionsAndAcTabObj),
						  }).success(function(data, status) 
						  {	
						  
							$scope.fourthTabHeaderData = data;					
							var k=0;
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData) 
							{
								$scope.fourthTabHeaderData[k].ryotLoansbac = $scope.fourthTabHeaderData[k].ForLoans;
								$scope.fourthTabHeaderData[k].id = k;
							    $scope.fourthTabHeaderData[k].season = $scope.getDetailsData.season;
								$scope.fourthTabHeaderData[k].caneAccSlno = $scope.getDetailsData.caneAccSlno;
								
								k++;
							});					
					
							var z=0;
							
							$scope.fourthTabHeaderData.forEach(function(fourthHeaderData)
							{
								var finalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac;
								if($scope.fourthTabHeaderData[z].caneacslno.length==0)
								{
							
									$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
							
								}
								for(var l=0;l<$scope.fourthTabHeaderData[z].caneacslno.length;l++)
								{
									//$scope.fourthTabHeaderData[z].caneacslno[l].totalamount =  parseFloat(Math.round(Number(100)+Number(l))).toFixed(2);
									$scope.fourthTabHeaderData[z].caneacslno[l].subventedInterestRate = 0;
									$scope.fourthTabHeaderData[z].caneacslno[l].netRate = $scope.fourthTabHeaderData[z].caneacslno[l].advanceinterest;
									$scope.fourthTabHeaderData[z].caneacslno[l].tempTotal =  $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = $scope.fourthTabHeaderData[z].caneacslno[l].totalamount;
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = $scope.fourthTabHeaderData[z].caneacslno[l].intamount;
									alert($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount);
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount=="" )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].netRate==null || $scope.fourthTabHeaderData[z].caneacslno[l].netRate=="")
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].netRate = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].interestAmount==null || $scope.fourthTabHeaderData[z].caneacslno[l].interestAmount=="")
									{
									$scope.fourthTabHeaderData[z].caneacslno[l].interestAmount = parseFloat(0).toFixed(2);
									}
									if($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable==null || $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable=='' )
									{
										$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable = parseFloat(0).toFixed(2);
									}
									
									/*var principal = $scope.fourthTabHeaderData[z].caneacslno[l].advanceamount;
									var advancerepayDate = $scope.fourthTabHeaderData[z].caneacslno[l].advancerepaydate;
									var advanceDate = $scope.fourthTabHeaderData[z].caneacslno[l].advancedate;
									var intRate = $scope.fourthTabHeaderData[z].caneacslno[l].advanceinterest;
									var subVented = $scope.fourthTabHeaderData[z].caneacslno[l].subventedInterestRate;
									
									var rate = intRate;
									var loanPayable =  $scope.fourthTabHeaderData[z].caneacslno[l].advanceamount;
									var totalAmount = loanPayable;*/
								
									//alert("abcd");
									//$scope.getnetRate(principal,intRate,subVented,rate,advanceDate,advancerepayDate,loanPayable,l,z,totalAmount);
									
																
									var totalLoanAmount = $scope.fourthTabHeaderData[z].ryotLoansbac - $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable;	
															
									//if(finalLoanAmount>0)
									//{
										if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)>Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
										{
											//alert('if 1');
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);																			
											finalLoanAmount = Number($scope.fourthTabHeaderData[z].ryotLoansbac)-Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable);
											
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);

											
										}
									   else if(Number($scope.fourthTabHeaderData[z].ryotLoansbac)<Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount))
									    {
											//alert('if 2');				

											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(Math.abs(Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPayable)-Number($scope.fourthTabHeaderData[z].ryotLoansbac)))).toFixed(2);						
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount) - Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
											finalLoanAmount = 0;									
										}
									   else
									    {	//alert('if 3');
											finalLoanAmount = 0;
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);						
											$scope.fourthTabHeaderData[z].caneacslno[l].loanPayable =  parseFloat(Number($scope.fourthTabHeaderData[z].caneacslno[l].totalamount) - Number($scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount)).toFixed(2);
											$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
											$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
										}
							
									//}
								   //else
								    //{
										//alert('if 4');

									//	$scope.fourthTabHeaderData[z].caneacslno[l].loanPendingAmount = $scope.fourthTabHeaderData[z].caneacslno[l].loanPayable;
									//}
									//$scope.fourthTabHeaderData[z].ryotLoansbac = parseFloat(Math.round(totalLoanAmount)).toFixed(2);
									//$scope.fourthTabHeaderData[z].tempLoansbac = parseFloat(Math.round(totalLoanAmount)).toFixed(2);
							
								 }
								z++;
							});					
					  });
					});					 
				 }
				else
				 {
					 $('.third').attr('disabled',false);
					 //sweetAlert("Oops...", "Something went wrong!", "error");
				 }
			  });
			}
		 };
		 
		  $scope.ShowDatePicker = function()
		 {
				
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
	 
		//----------Fourth Tab Advances Save----
		var FourthTabSubmitData = [];
		$scope.fourthTabDeductionsSave = function()
		{
			$('.fourth').attr('disabled',true);
			FourthTabSubmitData = [];
			
			$scope.fourthTabHeaderData.forEach(function(fourthHeaderData) 
			{
				//Added by DMurty on 25-12-2016
				fourthHeaderData.AllRepayDate = '23-12-2016';
				
				
				var advanceOrLoan = $scope.getDetailsData.advOrLoan;
				var suppType = $scope.getDetailsData.supplyType;
				
				///alert('advanceOrLoan--------'+advanceOrLoan);
				//alert('suppType--------'+suppType);
				
				fourthHeaderData.advOrLoan = parseInt(advanceOrLoan);
				fourthHeaderData.supplyType = parseInt(suppType);
				
				
							
				FourthTabSubmitData.push(fourthHeaderData);
			});	
					//alert(JSON.stringify(FourthTabSubmitData));		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAdvancesAndLoansDetails.html",
				data: FourthTabSubmitData,
			  }).success(function(data, status) 
			  {	
			  		//alert(data);
					if(data==true)
					{
						 swal("Success!", 'Loans Added Successfully!', "success");	
						 var alertok = document.querySelector(".sweet-alert"),
						okButton = alertok.getElementsByTagName("button")[1];
						$(okButton).click(function() 
						{  
							location.reload();
						});
					}
				   else
					{
						sweetAlert("Oops...", "Something went wrong!", "error");
					}
			  });
		};
		
		
		
		var FourthTabSubmitData = [];
	$scope.getAdvancesAndLoansCheckList = function()
	{
			//$('.fourth').attr('disabled',true);
			FourthTabSubmitData = [];
			$scope.fourthTabHeaderData.forEach(function(fourthHeaderData) 
			{
				FourthTabSubmitData.push(fourthHeaderData);
			});	
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/saveAdvancesAndLoansDetailsForVerify.html",
				data: FourthTabSubmitData,
			  }).success(function(data, status) 
			  {	
			  		alert(data);
					if(data==true)
					{
						alert('success');
						 //swal("Success!", 'Loans Added Successfully!', "success");	
						// var alertok = document.querySelector(".sweet-alert"),
						//okButton = alertok.getElementsByTagName("button")[1];
						//$(okButton).click(function() 
						//{  
						//	location.reload();
						//});
					}
				   else
					{
						//sweetAlert("Oops...", "Something went wrong!", "error");
					}
			  }); 

	};
	
	
	
	
		
		$scope.changeTotalinGridByInterestAmt = function(interestAmount,totalamount,gridId,colId)
		{
			var totalamount = Number(interestAmount)+Number(totalamount);
			// are you there? unna 
			//ok if you user number which datatype ill get to backend?
			//int or double..?nee datatype emi maaradhu asit is ga alaaaney untundi
			
			//ok eskallent
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount = totalamount;
			//deeni batti raaskuntaava 
		};
		
	//=================Interest Calculations=======================//
		
		
	 	$scope.getnetRate = function(principal,intRate,subVented,rate,issueDate,repaymentDate,loanPayable,gridId,colId,totalAmount)
		{
			var netRatePercent = Number(intRate)-Number(subVented);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].netRate = netRatePercent;
			
			var issueDateSplt = issueDate.split("00:");
			var issueDate = issueDateSplt[0];
			var issueDate = issueDate+"-";
			var repaymentDate = repaymentDate+"-";
			var issueSplit = issueDate.split("-");
			var repaymentDateSplit = repaymentDate.split("-");
			var RepaymentdateMonth = repaymentDateSplit[1]-1;

			var date2 = new Date(repaymentDateSplit[2], RepaymentdateMonth, repaymentDateSplit[0]);
			
			var IssueDateMonth = issueSplit[1] - 1;
			var date1 = new Date(issueSplit[2], IssueDateMonth, issueSplit[0]);
			
			
			var date1_unixtime = parseInt(date1.getTime()/1000);
			var date2_unixtime = parseInt(date2.getTime()/1000);
	
		//	// This is the calculated difference in seconds
			var timeDifference = date2_unixtime - date1_unixtime;
			//alert("timeDifference"+timeDifference);
			var timeDifferenceInHours = timeDifference/60/60;
			//alert("timeDifferenceinHours"+timeDifferenceInHours);
			var timeDifferenceInDays = timeDifferenceInHours/24;
			//alert("timeDifferenceInDays"+timeDifferenceInDays);
			var ActualTimeDiffInDays = Math.abs(timeDifferenceInDays)+1; //to include the end date

			//var timeDifferenceInDays1= timeDifferenceInDays/30;
			 //var timeInMonths =  timeDifferenceInDays1.toFixed(2);
			//alert("timeDifferenceInMonths"+timeDifferenceInDays1);
			//var timeDifferenceInDays12=timeDifferenceInDays1+".";
			//var timeDifferenceInDays1Split= timeDifferenceInDays12.split(".");
			//var ptr = (principal*timeInMonths*netRatePercent)/1200;
			
			//var ptr = (principal*timeInMonths*netRatePercent)*30/100*365;
			netRatePercent = netRatePercent/100;
			//var ptr = (principal*timeInMonths*netRatePercent)/1200;
			var intrestPerDay = principal*netRatePercent/365;
			var ptr = parseFloat(Math.abs(intrestPerDay*ActualTimeDiffInDays*100)/100).toFixed(2);
			//alert("ptr"+ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].interestAmount = ptr;
			//alert("Interest Amt"+ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount  = Number(principal)+Number(ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable = Number(principal)+Number(ptr);
			//alert('FinalPendingAmount-------3'+FinalPendingAmount);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPendingAmount =$scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount-$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable;
			
			
			
			
			if(loanPayable==null) { loanPayable = 0; }
		
		if(Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable)>Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount))
		{
			$('#c4Error'+ gridId+ colId).show();
			$('.fourth').attr('disabled',true);			
		}
	   else
	    {
			$('#c4Error'+ gridId+ colId).hide();
			$('.fourth').attr('disabled',false);
		}
		
		var FinalPendingAmount = Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount)-Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable);
		//alert('FinalPendingAmount-----2'+FinalPendingAmount);
		$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPendingAmount = parseFloat(Math.round(FinalPendingAmount)).toFixed(2);
		var FourthTabTotal = 0;
		for(var c=0;c<$scope.fourthTabHeaderData[gridId].caneacslno.length;c++)
		{
			FourthTabTotal +=parseInt($scope.fourthTabHeaderData[gridId].caneacslno[c].loanPayable); 
		}
		//alert("FourthTabTotal"+FourthTabTotal);
		//alert("For Loans"+$scope.fourthTabHeaderData[gridId].ForLoans);
		var FinalRyotLoanAc = Number($scope.fourthTabHeaderData[gridId].ForLoans)-Number(FourthTabTotal);
		$scope.fourthTabHeaderData[gridId].ryotLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
		$scope.fourthTabHeaderData[gridId].tempLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
	 
			
			
			
			};
			
			
			$scope.ChangeDate = function(principal,intRate,subVented,rate,issueDate,repaymentDate,loanPayable,gridId,colId,totalAmount)
		{
			var netRatePercent = Number(intRate)-Number(subVented);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].netRate = netRatePercent;
			
			var issueDateSplt = issueDate.split("00:");
			var issueDate = issueDateSplt[0];
			var issueDate = issueDate+"-";
			var repaymentDate = repaymentDate+"-";
			var issueSplit = issueDate.split("-");
			var repaymentDateSplit = repaymentDate.split("-");

			var RepaymentdateMonth = repaymentDateSplit[1]-1;
			var date2 = new Date(repaymentDateSplit[2], RepaymentdateMonth, repaymentDateSplit[0]);
			
			var IssueDateMonth = issueSplit[1] - 1;			
			var date1 = new Date(issueSplit[2], IssueDateMonth, issueSplit[0]);
			
			var date1_unixtime = parseInt(date1.getTime()/1000);
			var date2_unixtime = parseInt(date2.getTime()/1000);

			// This is the calculated difference in seconds
			var timeDifference = date2_unixtime - date1_unixtime;
			var timeDifferenceInHours = timeDifference/60/60;
			var timeDifferenceInDays = timeDifferenceInHours/24;
			var ActualTimeDiffInDays = Math.abs(timeDifferenceInDays)+1; //to include the end date

			//alert('timeDifferenceInDays------'+timeDifferenceInDays);
			
			netRatePercent = netRatePercent/100;
			var intrestPerDay = principal*netRatePercent/365;
			var ptr = parseFloat(Math.abs(intrestPerDay*ActualTimeDiffInDays*100)/100).toFixed(2);
		
			//alert("principal"+principal); alert("time"+timeDifferenceInDays1); alert("rate"+netRatePercent);

			$scope.fourthTabHeaderData[gridId].caneacslno[colId].interestAmount = ptr;
			//alert("Interest Amt"+ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount  = Number(principal)+Number(ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable = Number(principal)+Number(ptr);
			$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPendingAmount =$scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount-$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable;
		
			if(loanPayable==null) { loanPayable = 0; }
		
		if(Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable)>Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount))
		{
			$('#c4Error'+ gridId+ colId).show();
			$('.fourth').attr('disabled',true);			
		}
	   else
	    {
			$('#c4Error'+ gridId+ colId).hide();
			$('.fourth').attr('disabled',false);
		}
		
		var FinalPendingAmount = Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount)-Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable);
		//alert('FinalPendingAmount-------------1'+FinalPendingAmount);
		$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPendingAmount = parseFloat(Math.round(FinalPendingAmount)).toFixed(2);
		var FourthTabTotal = 0;
		for(var c=0;c<$scope.fourthTabHeaderData[gridId].caneacslno.length;c++)
		{
			FourthTabTotal +=parseInt($scope.fourthTabHeaderData[gridId].caneacslno[c].loanPayable); 
		}
		//alert("FourthTabTotal"+FourthTabTotal);
		//alert("For Loans"+$scope.fourthTabHeaderData[gridId].ForLoans);
		var FinalRyotLoanAc = Number($scope.fourthTabHeaderData[gridId].ForLoans)-Number(FourthTabTotal);
		$scope.fourthTabHeaderData[gridId].ryotLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
		$scope.fourthTabHeaderData[gridId].tempLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
			
			
			
			};
			
	 //--------------Calculate Total Loan Amount by Subvented Interest Rate---------
	 $scope.calculateTotalLoanAmount = function(subventedInterestRate,tableId,colId)
	 {
			if(subventedInterestRate==null) { subventedInterestRate = 0; }
			 var subventedInterest = Number(subventedInterestRate)*Number($scope.fourthTabHeaderData[tableId].caneacslno[colId].tempTotal)/100;
			 $scope.fourthTabHeaderData[tableId].caneacslno[colId].totalamount = parseFloat(Math.round(Number($scope.fourthTabHeaderData[tableId].caneacslno[colId].tempTotal)-Number(subventedInterest))).toFixed(2);
			 $scope.fourthTabHeaderData[tableId].ryotLoansbac = parseFloat(Math.round(Number($scope.fourthTabHeaderData[tableId].tempLoansbac)+Number(subventedInterest))).toFixed(2);
			 $scope.fourthTabHeaderData[tableId].caneacslno[colId].loanPayable = $scope.fourthTabHeaderData[tableId].caneacslno[colId].totalamount;
		
	 };
	 
	 //-------------Fourth Tab Payable Amount Calculations------
	 $scope.fourthTabPayableAdjust = function(currentPayable,totalamount,gridId,colId)
	 {
	
		if(currentPayable==null) { currentPayable = 0; }
		
		if(Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable)>Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount))
		{
		
			$('#c4Error'+ gridId+ colId).show();
			$('.fourth').attr('disabled',true);			
		}
	   else
	    {
		
			$('#c4Error'+ gridId+ colId).hide();
			$('.fourth').attr('disabled',false);
		}
		
		var FinalPendingAmount = Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].totalamount)-Number($scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPayable);
		//alert('FinalPendingAmount-------'+FinalPendingAmount);
		$scope.fourthTabHeaderData[gridId].caneacslno[colId].loanPendingAmount = parseFloat(Math.round(FinalPendingAmount)).toFixed(2);
		var FourthTabTotal = 0;
		for(var c=0;c<$scope.fourthTabHeaderData[gridId].caneacslno.length;c++)
		{
			FourthTabTotal +=parseInt($scope.fourthTabHeaderData[gridId].caneacslno[c].loanPayable); 
		}
		var FinalRyotLoanAc = Number($scope.fourthTabHeaderData[gridId].ForLoans)-Number(FourthTabTotal);
		//added by naidu on 08-03-2017
		//var FinalRyotLoanAc = Number($scope.fourthTabHeaderData[gridId].totalamount)-Number(FourthTabTotal);
		//alert($scope.fourthTabHeaderData[gridId].ForLoans);
		$scope.fourthTabHeaderData[gridId].ryotLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
		$scope.fourthTabHeaderData[gridId].tempLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
	 };

	  //===================================Over All Date///=====================================
	$scope.ChangeOverallDateForLoans=function(repaymentDate)
	{
		for(var d=0;d<$scope.fourthTabHeaderData.length;d++)
		{
			$scope.fourthTabHeaderData[d].ryotLoansbac = $scope.fourthTabHeaderData[d].ForLoans;
			for(var c=0;c<$scope.fourthTabHeaderData[d].caneacslno.length;c++)
			{
				//alert("length of the grid row"+$scope.fourthTabHeaderData[d].caneacslno.length+" slno"+c+" gdrono"+d);
				$scope.fourthTabHeaderData[d].caneacslno[c].advancerepaydate = repaymentDate;
				var netRatePercent = Number($scope.fourthTabHeaderData[d].caneacslno[c].advanceinterest)-Number($scope.fourthTabHeaderData[d].caneacslno[c].subventedInterestRate);
				$scope.fourthTabHeaderData[d].caneacslno[c].netRate = netRatePercent;
				//alert("issued date="+$scope.fourthTabHeaderData[d].caneacslno[c].advancedate);
				var issueDateSplt = $scope.fourthTabHeaderData[d].caneacslno[c].advancedate.split("00:");
				var issueDate = issueDateSplt[0];
				var issueDate = issueDate+"-";
				//var repaymentDate = repaymentDate+"-";
				var repDate= repaymentDate+"-";
				var issueSplit = issueDate.split("-");
				var repaymentDateSplit = repDate.split("-");
				var date1lomonth = issueSplit[1]-1;
				var date2lomonth  = repaymentDateSplit[1]-1;
				
				var date2 = new Date(repaymentDateSplit[2], date2lomonth, repaymentDateSplit[0]);
				
				var date1 = new Date(issueSplit[2], date1lomonth, issueSplit[0]);
				var date1_unixtime = parseInt(date1.getTime()/1000);
				var date2_unixtime = parseInt(date2.getTime()/1000);
				// This is the calculated difference in seconds
				var timeDifference = date2_unixtime - date1_unixtime;
				var timeDifferenceInHours = timeDifference/60/60;
				
				var timeDifferenceInDays = timeDifferenceInHours/24;
				
				var includeEndDate = 1;
				var ActualDifferenceinDays;
				ActualDifferenceinDays = Math.abs(timeDifferenceInDays);
				var timeDiffinDays = ActualDifferenceinDays+1;
				
				//var timeDifferenceInDays1= timeDifferenceInDays/30;
				//var timeInMonths =  timeDifferenceInDays1.toFixed(2);
				//var timeDifferenceInDays12=timeDifferenceInDays1+".";
				//var timeDifferenceInDays1Split= timeDifferenceInDays12.split(".");
				//var ptr = ($scope.fourthTabHeaderData[d].caneacslno[c].advanceamount*timeInMonths*netRatePercent)/10000;
				
				var ptr =  ($scope.fourthTabHeaderData[d].caneacslno[c].advanceamount*timeDiffinDays*netRatePercent)/36500;
				var ptr1  = Math.round(ptr*100)/100;
				ptr  = ptr1.toFixed(2);
				$scope.fourthTabHeaderData[d].caneacslno[c].interestAmount = ptr;
				$scope.fourthTabHeaderData[d].caneacslno[c].totalamount  = Number($scope.fourthTabHeaderData[d].caneacslno[c].advanceamount)+Number(ptr);

				var finalLoanAmount = 0; 
				if (Number($scope.fourthTabHeaderData[d].ryotLoansbac) > Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount))
				{
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPayable = $scope.fourthTabHeaderData[d].caneacslno[c].totalamount;
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount =parseFloat(Math.round(0)).toFixed(2);
					finalLoanAmount = Number($scope.fourthTabHeaderData[d].ryotLoansbac)-Number($scope.fourthTabHeaderData[d].caneacslno[c].loanPayable);
					$scope.fourthTabHeaderData[d].ryotLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
					$scope.fourthTabHeaderData[d].tempLoansbac = parseFloat(Math.round(finalLoanAmount)).toFixed(2);
				}
				else if(Number($scope.fourthTabHeaderData[d].ryotLoansbac) < Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount))
				{
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPayable = Number($scope.fourthTabHeaderData[d].ryotLoansbac);
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount = Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount) - Number($scope.fourthTabHeaderData[d].caneacslno[c].loanPayable);
					$scope.fourthTabHeaderData[d].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
					$scope.fourthTabHeaderData[d].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
					finalLoanAmount = 0;									
				}
				else
				{	
					finalLoanAmount = 0;
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount = parseFloat(Math.round(0)).toFixed(2);						
					$scope.fourthTabHeaderData[d].caneacslno[c].loanPayable =  Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount) - Number($scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount).toFixed(2);
					$scope.fourthTabHeaderData[d].ryotLoansbac = parseFloat(Math.round(0)).toFixed(2);
					$scope.fourthTabHeaderData[d].tempLoansbac = parseFloat(Math.round(0)).toFixed(2);
				}

				//alert("Interest Amt"+ptr);

				//$scope.fourthTabHeaderData[d].caneacslno[c].loanPayable = Number($scope.fourthTabHeaderData[d].caneacslno[c].advanceamount)+Number(ptr);
				//$scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount =$scope.fourthTabHeaderData[d].caneacslno[c].totalamount-$scope.fourthTabHeaderData[d].caneacslno[c].loanPayable;
				
				//if($scope.fourthTabHeaderData[d].caneacslno[c].loanPayable==null) { loanPayable = 0; }
			
				if(Number($scope.fourthTabHeaderData[d].caneacslno[c].loanPayable)>Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount))
				{
					$('#c4Error'+ d+ c).show();
					$('.fourth').attr('disabled',true);			
				}
				else
				{
					$('#c4Error'+ d+ c).hide();
					$('.fourth').attr('disabled',false);
				}
			
				//var FinalPendingAmount = Number($scope.fourthTabHeaderData[d].caneacslno[c].totalamount)-Number($scope.fourthTabHeaderData[d].caneacslno[c].loanPayable);
				//alert('FinalPendingAmount-------------1'+FinalPendingAmount);
				//$scope.fourthTabHeaderData[d].caneacslno[c].loanPendingAmount = parseFloat(Math.round(FinalPendingAmount)).toFixed(2);
				
				var FourthTabTotal = 0;
				for(var h=0;h<$scope.fourthTabHeaderData[d].caneacslno.length;h++)
				{
					FourthTabTotal +=parseInt($scope.fourthTabHeaderData[d].caneacslno[h].loanPayable); 
				}
				
				//alert("For Loans"+$scope.fourthTabHeaderData[gridId].ForLoans);
				//var FinalRyotLoanAc = Number($scope.fourthTabHeaderData[d].ForLoans)-Number(FourthTabTotal);
				//$scope.fourthTabHeaderData[d].ryotLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
				//$scope.fourthTabHeaderData[d].tempLoansbac = parseFloat(Math.round(FinalRyotLoanAc)).toFixed(2);
					
				//}
				
		
		}

	} //end of first for()
};


		












  })

  //=========================================
  		//Update Ryot Accounts
  //=========================================
   
   .controller('UpdateRyotAccounts',function($scope,$http)
	{
		var UpdateVillageDropDown=new Object();
		UpdateVillageDropDown.dropdownname="Village";
		var UpdateDropDownVillage= JSON.stringify(UpdateVillageDropDown);
		
		var UpdateSeasonDropDown=new Object();
		UpdateSeasonDropDown.dropdownname="Season";
		var UpdateDropDownSeason= JSON.stringify(UpdateSeasonDropDown);
		
		var CompanyAdvanceData = [];
		var CompanyAdvanceSaveObj = new Object();
		
		var CompanyLoansData = [];
		var CompanyLoansSaveObj = new Object();
		
		var CompanyHarvestingData = [];
		var CompanyHarvestingSaveObj = new Object();
		
		var CompanyTransportingData = [];
		var CompanyTransportingSaveObj = new Object();
		
		var RyotSbAcData = [];
		var RyotSbAcDataObj = new Object();
		
		//---------Load Season Dropdpwns---
		$scope.loadUpdateRyotSeasonNames=function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: UpdateDropDownSeason,
		 	  }).success(function(data, status) 
			  {				
					$scope.UpdateSeasonNames=data;
			  }); 
		};		
		

		//----------Load Zone DropDown----		
		$scope.loadUpdateRyotVillageNames=function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: UpdateDropDownVillage,
		 	  }).success(function(data, status) 
			  {
					$scope.UpdateVillageNames=data;
			  }); 
		};		
		//----------Load Circle DropDown----		
		$scope.loadServerDate = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(data, status) 
				 {
					$scope.UpdateFilterData = {caneaccdate:data.serverdate};
			 	 });	
		};
		
		//-------get Update Ryot Account Details---
		
		$scope.getUpdateRyotAccountDetails = function(UpdateFilterData)
		{
			$scope.FilterSubmitted=true;			
	
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllAdvancePayableDetailsForUpdateRyotAccounts.html",
					data: JSON.stringify(UpdateFilterData),
			     }).success(function(data, status) 
				 {
					 //alert(JSON.stringify(data));
					 //console.log(data);
					$scope.AdvanceTab = data;
					$scope.loadLoanDetails();
					$scope.loadHarvestingCharges();
					$scope.loadTransporterData();
					$scope.loadTransporterData();
					$scope.loadSBAcData ();
			 	 });	
			
		};
		
		//----------Load Loan Details---------
		$scope.loadLoanDetails = function()
		{	
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllLoanPayableDetailsForUpdateRyotAccounts.html",
					data: JSON.stringify($scope.UpdateFilterData),
			     }).success(function(data, status) 
				 {
					 //console.log(data);
					$scope.LoansTab = data;
			 	 });	
		};
		
		//----------Load Harvesting Charges----
		$scope.loadHarvestingCharges = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllHarvesterDetailsForUpdateRyotAccounts.html",
					data: JSON.stringify($scope.UpdateFilterData),
			     }).success(function(data, status) 
				 {
					 //console.log(data);
					$scope.HarvesterTab = data;
			 	 });				
		};
		//----------Load Tramsporting Charges----
		$scope.loadTransporterData = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllTransporterDetailsForUpdateRyotAccounts.html",
					data: JSON.stringify($scope.UpdateFilterData),
			     }).success(function(data, status) 
				 {
					 //console.log(data);
					$scope.TransporterTab = data;
			 	 });				
		};
		//----------Load SB Account Charges----
		$scope.loadSBAcData = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllSBAccountDetailsForUpdateRyotAccounts.html",
					data: JSON.stringify($scope.UpdateFilterData),
			     }).success(function(data, status) 
				 {
					console.log(data);
					$scope.BankAcTab = data;
			 	 });				
		};
		
		//-----------Save company Advances----		
		$scope.saveCompanyAdvances = function()
		{
			$scope.AdvanceSubmitted = true;
			if($scope.CompanyAdvanceSubmit.$valid)
			{
				CompanyAdvanceData = [];
				$scope.AdvanceTab.forEach(function(AdvanceData) 
				{
				    CompanyAdvanceData.push(angular.toJson(AdvanceData));														 
			    });	
				CompanyAdvanceSaveObj.formData = $scope.UpdateFilterData;
				CompanyAdvanceSaveObj.gridData = CompanyAdvanceData;	

				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/updateAdvancePaymentDetails.html",
						data: JSON.stringify(CompanyAdvanceSaveObj),
				     }).success(function(data, status) 
					 {
						if(data==true)
						{
							swal("Success!", 'Advance Payment Details Added Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}	
				 	 });										
			}
		};
		//-----------Save company Loans----		
		$scope.saveCompanyLoans = function()
		{
			$scope.LoanSubmitted = true;
			if($scope.CompanyLoanSubmit.$valid)
			{
				CompanyLoansData = [];
				$scope.LoansTab.forEach(function(LoanData) 
				{
				    CompanyLoansData.push(angular.toJson(LoanData));														 
			    });	
				CompanyLoansSaveObj.formData = $scope.UpdateFilterData;
				CompanyLoansSaveObj.gridData = CompanyLoansData;	
			
			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/updateLoanPaymentDetails.html",
						data: JSON.stringify(CompanyLoansSaveObj),
				     }).success(function(data, status) 
					 {
						if(data==true)
						{
							swal("Success!", 'Loan Payment Details Added Successfully!', "success");
						}
					    else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}					
				 	 });										
			}
		};
		//-----------Save Harvesting Charges----		
		$scope.saveHarvestingCharges = function()
		{
			$scope.HarvestingSubmitted = true;
			if($scope.HarvestingChargesSubmit.$valid)
			{
				CompanyHarvestingData = [];
				$scope.HarvesterTab.forEach(function(HarvestingData) 
				{
				    CompanyHarvestingData.push(angular.toJson(HarvestingData));														 
			    });	
				CompanyHarvestingSaveObj.formData = $scope.UpdateFilterData;
				CompanyHarvestingSaveObj.gridData = CompanyHarvestingData;	
						
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/updateHarvestingContractorPaymentDetails.html",
						data: JSON.stringify(CompanyHarvestingSaveObj),
			    	 }).success(function(data, status) 
					 {
						if(data==true)
						{
							swal("Success!", 'Harvesting Contractor Payment Details Added Successfully!', "success");
						}
				    	else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				 	 });										
			}
		};
		
		//-----------Save Transporting Charges----		
		$scope.saveTransportingCharges = function()
		{
			$scope.TransportingSubmitted = true;
			if($scope.TransportingChargesSubmit.$valid)
			{
				CompanyTransportingData = [];
				$scope.TransporterTab.forEach(function(TransporterData) 
				{
				    CompanyTransportingData.push(angular.toJson(TransporterData));														 
			    });	
				CompanyTransportingSaveObj.formData = $scope.UpdateFilterData;
				CompanyTransportingSaveObj.gridData = CompanyTransportingData;	
						
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/updateTransportContractorPaymentDetails.html",
						data: JSON.stringify(CompanyTransportingSaveObj),
			    	 }).success(function(data, status) 
					 {
						if(data==true)
						{
							swal("Success!", 'Transport Contractor Payment Details Added Successfully!', "success");
						}
				    	else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}
				 	 });										
			}
		};
		//-----------Save Ryot SB A/C----		
		$scope.saveRyotSBACDetails = function()
		{
			$scope.SBSubmitted = true;
			if($scope.SBAccountSubmit.$valid)
			{
				RyotSbAcData = [];
				$scope.BankAcTab.forEach(function(BankData) 
				{
				    RyotSbAcData.push(angular.toJson(BankData));														 
			    });	
				RyotSbAcDataObj.formData = $scope.UpdateFilterData;
				RyotSbAcDataObj.gridData = RyotSbAcData;	
			
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/updateSBPaymentDetails.html",
						data: JSON.stringify(RyotSbAcDataObj),
			    	 }).success(function(data, status) 
					 {
						if(data==true)
						{
							swal("Success!", 'SB Account Payment Details Added Successfully!', "success");
						}
				    	else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						}					
				 	 });										
			}
		};
		
		//------------Calculate Pending Amount----
		$scope.calculatePendingAmount = function(paidAmount,id,commonName)
		{
			if(paidAmount!=null && paidAmount<$scope[commonName][id].payableamount)
			{
				var PendingAmount = Number(paidAmount)-Number($scope[commonName][id].payableamount);			
				$scope[commonName][id].pendingamount = parseFloat(Math.round(Math.abs(PendingAmount))).toFixed(2);
			}
		   else
		    {
				$scope[commonName][id].pendingamount = parseFloat(Math.round(Math.abs(0))).toFixed(2);
			}
		};
		
		
	})


  //=========================================
  		//Update Loans
  //=========================================
  
  .controller('Updateloandetails',function($scope,$http)
   {
		var LoanGridData = [];
		var LoanDataObj = new Object();
		
		var filterObj = new Object();
		var DropDownLoad=new Object();
		DropDownLoad.dropdownname="Season";
		var DropDownSeason= JSON.stringify(DropDownLoad);
			
			
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname ="Branch";
		var DropDownbBank= JSON.stringify(DropDownLoad);
		
		$scope.master = {};
		
		$scope.loadSeasonNames=function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: DropDownSeason,
		 	  }).success(function(data, status) 
			  {
					$scope.SeasonNames=data;
			  }); 
		};
										
		$scope.loadBranchNames=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: DropDownbBank,
			}).success(function(data, status) 
			{
				$scope.BankNames=data;
			}); 
		};
					
		$scope.getUpdateLoanDetails = function(form)
		{
       		//$scope.Filtersubmitted=true;

			if(form.$valid) 
			{
				
	  			$scope.UpdateLoanData= [];			
				filterObj.season = $scope.AddedUpdateLoan.season;
				filterObj.branchcode = $scope.AddedUpdateLoan.branchcode;
				
 				var httpRequest = $http({
 	 				 method: 'POST',
 					 url : "/SugarERP/getAllUpdateLoans.html",
 					 data: JSON.stringify(filterObj),
   				  }).success(function(data, status) 
				  {
						$scope.UpdateLoanData=data;
				  }); 		  
		  }
		  else
		   {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}			   
		   }
        };
		 		 
		 $scope.ShowDatePicker = function()
		 {
					//alert('ashok');
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		$scope.$applyAsync(function()
		{
	     	$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function(){ setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); }});
			
		})
		
		//============UPdate Loan FOrm Submit Row wise======================
		
			$scope.updateLoanRowWise = function(UpdateLoan,id,AddedUpdateLoan)
			{
				var updateLoanRowWise = new Object();
				$scope.flag="Single";

				updateLoanRowWise.formData = JSON.stringify(AddedUpdateLoan);
				//updateLoanRowWise.griddata = JSON.stringify(UpdateLoan);
				updateLoanRowWise.griddata = angular.toJson(UpdateLoan);
				 
				 updateLoanRowWise.flag=$scope.flag;
				//alert(JSON.stringify(updateLoanRowWise)); 	
				//delete updateLoanRowWise['$$hashKey'];

				//"$$hashKey"				
			 
			$.ajax({
			    url:"/SugarERP/saveUpdateLoans.html",
			    processData:true,
			    type:'POST',
			    contentType:'Application/json',
			    data:JSON.stringify(updateLoanRowWise),
				beforeSend: function(xhr) 
				{
		            xhr.setRequestHeader("Accept", "application/json");
		            xhr.setRequestHeader("Content-Type", "application/json");
		        },
			    success:function(response) 
				{					
					if(response==true)
					{
			  			swal("Success!", 'Update Loans  Added Successfully!', "success");  						
						//$('.btn-hide').attr('disabled',false);							
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
						//$('.btn-hide').attr('disabled',false);							
					}
				}
			});
			
			};
		
		//====================================
					
		$scope.Updateloansubmit=function(AddedUpdateLoan,form)
	  	{
			//$scope.submitted = true;
					
			if($scope.UpdateLoanForm.$valid) 
			{						
				LoanGridData = [];
				$scope.UpdateLoanData.forEach(function(UpdateLoan) 
				{
			         LoanGridData.push(angular.toJson(UpdateLoan));														 
		    	});									
				$scope.flag="Multiple";								
				LoanDataObj.formData = JSON.stringify(AddedUpdateLoan);
				LoanDataObj.griddata = LoanGridData;
				LoanDataObj.flag=$scope.flag;
				

				$('.btn-hide').attr('disabled',true);								
				$.ajax({
				   		url:"/SugarERP/saveUpdateLoans.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(LoanDataObj),						
						beforeSend: function(xhr) 
						{
					           xhr.setRequestHeader("Accept", "application/json");
					           xhr.setRequestHeader("Content-Type", "application/json");
			    		},
						success:function(response) 
						{				
							if(response==true)
							{																		
								swal("Success!", 'Update Loans  Added Successfully!', "success");									
								$('.btn-hide').attr('disabled',false);	
								$scope.loadSeasonNames();
								$scope.AddedUpdateLoan = angular.copy($scope.master);
								$scope.AddedUpdateLoan = {};
								$scope.UpdateLoanData = {};
								form.$setPristine(true);								
								//$scope.loadBranchNames();
									//-------------load Added all Screens----		
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});					
				}										
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}			   					
				}
			};					
			 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedUpdateLoan = angular.copy($scope.master);
				$scope.UpdateLoanData = {};
			    form.$setPristine(true);			    								
			   
		   };
		   
		   $scope.exportData = function () {

        alasql('SELECT * INTO XLSX("LoanData.xlsx",{headers:true}) FROM ?',[$scope.UpdateLoanData]);
    };

		   
			
		})  
		
		.directive("fileread", [function () {
				return {
    $scope: {
      UpdateLoanData: '=method'
	  
    },
    link: function ($scope, $elm, $attrs) {
		
      $elm.on('change', function (changeEvent) {
        var reader = new FileReader();
        
        reader.onload = function (evt) {
          $scope.$apply(function () {
            var data = evt.target.result;
          
            var workbook = XLSX.read(data, {type: 'binary'});
            
            var headerNames = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
            
          var data = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]]);
           // alert(JSON.stringify(data));
			 
             $scope.UpdateLoanData= [];
             data.forEach(function (h) {
	
             $scope.UpdateLoanData.push(h);
            }); 
		
			//alert(JSON.stringify($scope.UpdateLoanData));  
        $scope.UpdateLoanData.data=data;
			
		  
            $elm.val(null);
          });
        };
      
        reader.readAsBinaryString(changeEvent.target.files[0]);
      });
    }
  }
}])


   //========================================
   		//Print Permits
   //========================================
   
   .controller("PrintPermits",function($http,$scope)
   {
	   $scope.PrintPermit = {printMode:"Bulk"};
	   
	   var PrintPerSeasonObj = new Object();
  	   PrintPerSeasonObj.dropdownname = "Season";
 	   var jsonPrintPerSeasonString= JSON.stringify(PrintPerSeasonObj);
	   
	   //----------Load Print Permit Season Names-----
	   $scope.loadPrintPermitSeasons=function()
	   {
		   var httpRequest = $http({
			 	method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonPrintPerSeasonString,
		 	  }).success(function(data, status) 
			  {
					$scope.PrintSeasonNames=data;
			  }); 
		};
		
		//------load Program number by season drop onchange--
		
		$scope.loadPrintProgramNumber = function(printSeason)
		{
			var httpRequest = $http({
			 	method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: printSeason,
		 	  }).success(function(data, status) 
			  {
					$scope.PrintProgramNum=data;
			  }); 			
		};
	   
	   
   })
   
	//========================================
		//Update Harvest Charges
	//========================================
		
	.controller('UpdateHarvestCharges',function($scope,$http)
	{
				
		$scope.UpdateharvestData=[];
	    var HarvestGridData = [];
	    var HarvestDataObj=new Object();
				
		$scope.AddedCharges={'harvestingMode':'0'};
				
		var filterObj = new Object();
				
		var DropDownLoad=new Object();
		DropDownLoad.dropdownname="Season";
		var DropDownSeasonUser= JSON.stringify(DropDownLoad);
				
		var DropDownLoad=new Object();
		DropDownLoad.dropdownname="Circle";
		var DropDownSeason= JSON.stringify(DropDownLoad);
				
				
		var DropDownLoad=new Object();
		DropDownLoad.dropdownname="HarvestingContractors";
		var DropDownSeasonharvest=JSON.stringify(DropDownLoad);
				
		$scope.master = {};
		
		$scope.setRowAfterReset = function()
		{
			$scope.AddedCharges={'harvestingMode':'0'};					
		};
		
		//-----------load Season Names---------	
		$scope.loadSeasonNames=function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: DropDownSeasonUser,
			 }).success(function(data, status) 
			 {
				$scope.SeasonNames=data;
				$scope.AddedCharges={'harvestingMode':'0'};	
			 }); 
		};
		
		//---------load Circle Names---------
				
		$scope.loadCircleNames=function()
		{
				
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: DropDownSeason,
			 }).success(function(data, status) 
			 {
					$scope.circleNames=data;
			 }); 
		};
					
		//---------Load Harvesting Charges---------
		
		$scope.loadHarvestNames=function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: DropDownSeasonharvest,
			 }).success(function(data, status) 
			 {
				$scope.harvestNames=data;
			 }); 
		};
		
		//------------Get Harvesting Charges Details--------
		$scope.getCaneSuppliedRyotDetails=function(form)
		{
			
			if(form.$valid) 
			{
			    $scope.UpdateharvestData= [];			
				//$scope.filterSubmitted = true;
				filterObj.season = $scope.AddedCharges.season;
				filterObj.circleCode = $scope.AddedCharges.circleCode;
				filterObj.date = $scope.AddedCharges.effectiveDate;

				var httpRequest = $http({
						 method: 'POST',
						  url : "/SugarERP/getRyotsBySeasonandCircle.html",
						  data: JSON.stringify(filterObj),
					   }).success(function(data, status) 
						{
							$scope.UpdateharvestData=data;
						});						 
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				
			}
		};
					
		//---------Save Harvesting Charges---------
		$scope.UpdateHarvestSubmit=function(AddedCharges,form)
	  	{
			//$scope.submitted =true;
			
			if($scope.UpdateHarvestChargesForm.$valid) 
			{
				HarvestGridData = [];
			    $scope.UpdateharvestData.forEach(function(Updateharvest) 
			    {
				    HarvestGridData.push(angular.toJson(Updateharvest));														 
		        });									
														
				HarvestDataObj.formData = JSON.stringify(AddedCharges);
				HarvestDataObj.gridData =HarvestGridData;

				$('.btn-hide').attr('disabled',true);
				$.ajax({
				   		url:"/SugarERP/saveUpdateHarvestingCharges.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(HarvestDataObj),						
						beforeSend: function(xhr) 
						{
				            xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
			   		    },
					    success:function(response) 
						{				
							if(response==true)
							{																		
								swal("Success!", 'Harvesting Charges Updated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								
								$scope.AddedCharges = angular.copy($scope.master);
								form.$setPristine(true);
								$scope.loadSeasonNames();
								$scope.UpdateharvestData = {};
							}
						  else
						   {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);							   
						   }
						}
					});					
				}				
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}									
				}
			};
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedCharges = angular.copy($scope.master);
				form.$setPristine(true);
				$scope.loadSeasonNames();
				$scope.UpdateharvestData = {};				
		   };
			
		})
		
	//=====================================
		//Update Transport Rates
	//=====================================
	
	.controller('UpdateTransporterCharges', function($scope,$http)
	{
		
		var transportSeasonObj = new Object();
		transportSeasonObj.dropdownname = "Season";
		var jsonTransportSeasonString= JSON.stringify(transportSeasonObj);				

		var transportCircleObj = new Object();
		transportCircleObj.dropdownname = "Circle";
		var jsonTransportCircleString= JSON.stringify(transportCircleObj);				

		var transportDropObj = new Object();
		transportDropObj.dropdownname = "TransportingContractors";
		var jsonTransportDropString= JSON.stringify(transportDropObj);				

		$scope.master = {};
		
		var TransportGridData = new Object();
		var SaveTransportObj = new Object();
		var EffectiveDateObj = new Object();
		
		//--------Load Seaon DropDown----------
		$scope.loadTransportContractors = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonTransportDropString,
				}).success(function(data, status) 
				{
					$scope.getTransporterNames=data;
				}); 			
		};		
		
		//--------Load Seaon DropDown----------
		$scope.loadTransportSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonTransportSeasonString,
				}).success(function(data, status) 
				{
					$scope.getTransportSeasons=data;
				}); 			
		};
		//--------Load Seaon DropDown----------
		$scope.loadTransportCircle = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonTransportCircleString,
				}).success(function(data, status) 
				{
					$scope.getTransportCircles=data;
				}); 			
		};
		
		//--------------Get Trasport Details----
		$scope.transportFilterSubmit = function(transportFilter,form)
		{
			//$scope.filterSubmitted = true;
			//alert(JSON.stringify(transportFilter));
			if($scope.TransportChargesForm.$valid)
			{
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getRyotsBySeasonandCircleForTransportCharges.html",
					data: JSON.stringify(transportFilter),
				}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.UpdateTransporterData=data;
				});
			}
		   else
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				 
			 }
		};
		
		//-------------Save Transporting Rates---------
		
		$scope.SaveTransportingRates = function(form)
		{
			//$scope.Submitted = true;
			
			if($scope.TransporterChargesSubmit.$valid)
			{				
				TransportGridData = [];
				$scope.UpdateTransporterData.forEach(function(TransporteData) 
				{
				    TransportGridData.push(angular.toJson(TransporteData));														 
		    	});		
				
				EffectiveDateObj.effectiveDate = $scope.inputData.effectiveDate;
				EffectiveDateObj.toDate = $scope.inputData.effectiveDate;
				EffectiveDateObj.season = $scope.transportFilter.season;
				EffectiveDateObj.circleCode = $scope.transportFilter.circleCode;
				
				SaveTransportObj.formData = EffectiveDateObj;
				SaveTransportObj.gridData = TransportGridData;
				
				$('.btn-hide').attr('disabled',true);							   						
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/saveUpdateTransportCharges.html",
					data: JSON.stringify(SaveTransportObj),
				}).success(function(data, status) 
				{
					if(data==true)
					{
						swal("Success!", 'Transport Charges Updated Successfully!', "success");
						$('.btn-hide').attr('disabled',false);							   						
						
						$scope.transportFilter = angular.copy($scope.master);
						//$scope.inputData.effectiveDate = "";
						$scope.UpdateTransporterData = {};					
						form.$setPristine(true);	
						$scope.TransporterChargesSubmit.$setPristine(true);	
						$scope.loadTransportSeason();						
					}
				   else
				    {
						sweetAlert("Oops...", "Something went wrong!", "error");
						$('.btn-hide').attr('disabled',false);							   						
					}
				});								
			}
		   else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				}				 				
			}
		};
		 //---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.transportFilter = angular.copy($scope.master);
				$scope.loadTransportSeason();				
				$scope.UpdateTransporterData = {};
			    form.$setPristine(true);			    								
				$scope.TransporterChargesSubmit.$setPristine(true);	
			   
		   };
		
	})


		
	//========================================
			//Test Weighment
	//========================================
	.controller('TestWeighment', function($scope,$http)	
	{
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "WeighBridge";
		var jsonBridge= JSON.stringify(DropDownLoad);						
		$scope.master = {};
		//-------Get Server Date-------------
		
		$scope.loadServerDate = function()
		{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						//$scope.Addedtestweighment={'testWeighmentDate':data.serverdate};
						var serverDate = data.serverdate;
						
						var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getServerTime.html",
								data: {},
							 }).success(function(data, status) 
							 {
 								$scope.Addedtestweighment={'testWeighmentDate':serverDate,'testWeighmentTime':data.servertime,'bridgeName':2};
								 $scope.getShiftId(data.servertime);
								  $scope.Addedtestweighment.operatorName=sessionStorage.getItem('username');
							 });			
				 	 });				
		};
		
		$scope.GetCaneWeightbyServlet=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCaneweightment.html",
			data: {},
			}).success(function(data, status)
			{			
				var  servletCane = Number(data[0].DATA);
				$scope.Addedtestweighment.totalWeight = servletCane;
			});
		};
		
		//-------Get Bridge Names DropDown---
		
		$scope.loadWeighBridgeNames=function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonBridge,
				}).success(function(data, status) 
				{
//alert(JSON.stringify(data));

					$scope.WeighBridgeNames=data;
					$scope.Addedtestweighment.bridgename = 2;
					//$scope.Addedtestweighment.bridgeName=2;
				}); 
		};
		
		//-----------------------------Save--------------------------------
		$scope.testweighment = function(Addedtestweighment,form)
	  	{
			//$scope.submitted = true;
					
			if($scope.TestWeighmentForm.$valid) 
			{
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				  		url:"/SugarERP/saveWeighBridgeTestData.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(Addedtestweighment),							
						beforeSend: function(xhr) 
						{
					           xhr.setRequestHeader("Accept", "application/json");
					           xhr.setRequestHeader("Content-Type", "application/json");
			    		},
						success:function(data) 
						{	
						
						
							//alert(JSON.stringify(data));
							//alert(response);
						//	alert(data[0].isInsertSuccess);
							if(data[0].isInsertSuccess==true)
							
							{																		
								//swal("Success!", 'Tests Added Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								$scope.Addedtestweighment = angular.copy($scope.master);
								$scope.getShiftId();
								$scope.loadServerDate();
								form.$setPristine(true);
								$scope.loadServerDate();
								$scope.Addedtestweighment.bridgeName = {};
								$scope.Addedtestweighment.operatorName = {};
								$scope.Addedtestweighment.totalWeight = {};		
							//	alert(data[0].netwt);
								if(data[0].vehicletype=='' || data[0].vehicletype=='NA')
								{
									data[0].vehicletype='';
								}
								if(data[0].canereceipttime=='' || data[0].canereceipttime=='NA')
								{
									data[0].canereceipttime='';
								}
								if(data[0].vehicleno=='' || data[0].vehicleno=='NA')
								{
									data[0].vehicleno='';
								}
								if(data[0].ryotcode=='' || data[0].ryotcode=='NA')
								{
									data[0].ryotcode='';
								}
								if(data[0].ryotname=='' || data[0].ryotname=='NA')
								{
									data[0].ryotname='';
								}
								if(data[0].relativeName=='' || data[0].relativeName=='NA')
								{
									data[0].relativeName='';
								}
								if(data[0].cropName=='' || data[0].cropName=='NA')
								{
									data[0].cropName='';
								}
								if(data[0].villagecode=='' || data[0].villagecode=='NA')
								{
									data[0].villagecode='';
								}
								if(data[0].villageName=='' || data[0].villageName=='NA')
								{
									data[0].villageName='';
								}
								if(data[0].varietyName=='' || data[0].varietyName=='NA')
								{
									data[0].varietyName='';
								}
								if(data[0].landvilcode=='' || data[0].landvilcode=='NA')
								{
									data[0].landvilcode='';
								}
								if(data[0].circlecode=='' || data[0].circlecode=='NA')
								{
									data[0].circlecode='';
								}
								if(data[0].circleName=='' || data[0].circleName=='NA')
								{
									data[0].circleName='';
								}
								if(data[0].grossweight=='' || data[0].grossweight=='NA')
								{
									data[0].grossweight='';
								}
								if(data[0].lorrywt=='' || data[0].lorrywt=='NA')
								{
									data[0].lorrywt='';
								}
								if(data[0].netwt=='' || data[0].netwt=='NA')
								{
									data[0].netwt='';
								}
								
								var shift = data[0].shiftid;
								if(shift == 1)
								{
									shift = 'A';
								}
								else if(shift == 2)
								{
									shift = 'B';
								}
								else
								{
									shift = 'C';
								}
								//alert('shift-------'+shift);
								
								var lorryWeight = data[0].lorrywt.toFixed(3);
								var popupWin = window.open('', '_blank', 'width=300,height=300');
  									popupWin.document.open();
 


popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2%;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].canereceiptdate+'</span><span style="position:fixed; left:67%; top:9%;"></span></div><div><span style="position:fixed; left:7%; top:16%;">'+shift+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:22%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:9%; top:40.5%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35.5%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45.75%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45.75%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:42%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51.75%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">Test Weight</span><span style="position:absolute; left:39%; top:71%;">'+lorryWeight+'</span><span style="position:absolute; left:55.5%; top:71%;"></span><span style="position:absolute; left:5%; top:90%;">'+data[0].login+'</span></div></body></html>');
popupWin.document.close();
								
							}
						   else
						    {
								sweetAlert("Oops...", "Something went wrong!", "error");
								$('.btn-hide').attr('disabled',false);							   														
							}
						}
					});					
			}		
		   else
		    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
									{
										firstError = form[field].$name;
									}
									if (form[field].$pristine) 
									{
										form[field].$dirty = true;
									}
							}	
					}
			}
		 };
		
		//------------------Get Shift Id--------------		
		$scope.getShiftId = function(caneWeighmentTime)
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getShiftId.html",
					data: caneWeighmentTime,
			     }).success(function(data, status) 
				 {
					$scope.Addedtestweighment.shift=data.ShiftId;
			 	 });
		};		
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.Addedtestweighment = angular.copy($scope.master);
				$scope.getShiftId();
				$scope.loadServerDate();
			    form.$setPristine(true);			    								
			   
		   };
		
	})
	
	//============================================
			//Ryot Ledger
	//============================================
	.controller('RyotLedger', function($scope,$http)										  
	{
		var ledgerSeasonObj = new Object();
		ledgerSeasonObj.dropdownname = "Season";
		var jsonLedgerSeasonString= JSON.stringify(ledgerSeasonObj);		
		
		var LedgerRyotDownLoad = new Object();
		LedgerRyotDownLoad.tablename = "AgreementSummary";
		LedgerRyotDownLoad.columnname = "ryotcode";
		LedgerRyotDownLoad.wherecolumnname = "seasonyear";
		var generateRyotLedger = new Object();
		
		$scope.loadRyotDatainSession = function(selectedRyot)
		{
			selectedRyot = selectedRyot.trim();
			//alert(selectedRyot);
			if(selectedRyot != '' || selectedRyot !='null')
			{
				if(selectedRyot.length>=3)
				{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAccountNames.html",
					data: selectedRyot,
					}).success(function(data, status) 
					{	
						//alert(JSON.stringify(data));
						$scope.accName=data;
					}); 
					
				}
				
			}
			
			//alert(selectedRyot);
		};


		//--------Load Seaon DropDown----------
		$scope.loadTransportSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonLedgerSeasonString,
				}).success(function(data, status) 
				{
					$scope.getLedgerSeasons=data;
				}); 			
		};
		
		$scope.getAgreedRyots = function(season)
		{
			LedgerRyotDownLoad.value = season;
			var jsonLedgerRyotDropDown= JSON.stringify(LedgerRyotDownLoad);

			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getRyotsFromAgreementSummary.html",
					data: jsonLedgerRyotDropDown,
				}).success(function(data, status) 
				{		
				
					$scope.ledgerRyotCodeData=data;
				}); 		
		};
		
		var d = new Date();
		var dd = d.getDate();
		var mm = d.getMonth()+1;
		var yy = d.getFullYear();
		$scope.currentDate = dd+"-"+mm+"-"+yy;
		
		
		$scope.generateLedger = function(ryotLedger,form)
		{
			//alert(form.$valid);
			
			//alert(JSON.stringify(ryotLedger));
			generateRyotLedger.season = $scope.ryotLedger.season;
			$scope.ryotLedger.ryotcode;
			
			var n = $scope.ryotLedger.ryotcode.lastIndexOf("-");
			//alert(n);
			var ryotcodeval = $scope.ryotLedger.ryotcode.substring(n+1,$scope.ryotLedger.ryotcode.length);
			//alert(ryotcodeval);
	       generateRyotLedger.ryotcode = ryotcodeval;
			generateRyotLedger.fromdate = $scope.ryotLedger.fromdate;
			generateRyotLedger.todate = $scope.ryotLedger.todate;
			//alert(JSON.stringify(generateRyotLedger));
			//$scope.FilterSubmitted=="true";
			
			if(form.$valid)
			{
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getRyotLedger.html",
						data: JSON.stringify(generateRyotLedger),
					}).success(function(data, status) 
					{		
					
					//alert(JSON.stringify(data));
						console.log(data);
						$("#dispLedger").show(2000);
						$scope.loadLedgerData=data;
						$("#printButton").show();
						
						var i=0;
						$scope.loadLedgerData.forEach(function(ledgerData) 
						{
							var runningBal=$scope.loadLedgerData[i].runningbalance;
							//added by naidu on 14-02-2017
							var bcr=$scope.loadLedgerData[i].cr;
							var bdr=$scope.loadLedgerData[i].dr;
							$scope.loadLedgerData[i].cr=parseFloat(Math.abs(bcr)).toFixed(2);
							$scope.loadLedgerData[i].dr=parseFloat(Math.abs(bdr)).toFixed(2);
							$scope.loadLedgerData[i].Runbalance = parseFloat(Math.abs(runningBal)).toFixed(2);
								if($scope.loadLedgerData[i].runningbalance>0)
								{
									$scope.loadLedgerData[i].transType = "Cr";
								}
								else
								{
									$scope.loadLedgerData[i].transType = "Dr";
								}
							
					
							i++;
						});
						//var i=0;
							
						//$scope.availableBalance = $scope.loadLedgerData[$scope.loadLedgerData.length-1].Runbalance+$scope.loadLedgerData[$scope.loadLedgerData.length-1].transType;
						
						 var crTotal =0;
		   var drTotal =0;
		   for(var u=0;u<($scope.loadLedgerData.length);u++)
		   {
			   //added by naidu on 14-02-2017
			   crTotal += parseFloat($scope.loadLedgerData[u].cr);
			   drTotal += parseFloat($scope.loadLedgerData[u].dr);
		   }
		
		 $scope.debitTotal = drTotal.toFixed(2);
		 $scope.creditTotal = crTotal.toFixed(2);
					
					}); 					
			}
		   else
		   {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
									{
										firstError = form[field].$name;
									}
									if (form[field].$pristine) 
									{
										form[field].$dirty = true;
									}
							}	
					}			   
		   }
		   
		   //alert(JSON.stringify(generateRyotLedger));
		   var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getRyotLedgerData.html",
					data: JSON.stringify(generateRyotLedger),
				}).success(function(data, status) 
				{		
					//alert(JSON.stringify(data));
				$scope.ryotLedgers=data;
				/*
				var username = sessionStorage.getItem('username');
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/checkinLoginRole.html",
					data: username,
				}).success(function(data, status) 
				{	
				if(data==14)
				{
					$("#printButton").hide();
				}
					*/
				}); 
		   
		   
		   
		   
		   
		};
		
		$scope.ryotLedgerPrint = function()
		{
			var prtContent = document.getElementById("dispLedger");
			var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
			WinPrint.document.write(prtContent.innerHTML);
			WinPrint.document.close();
			WinPrint.focus();
			WinPrint.print();
			WinPrint.close();
			$scope.loadLedgerData =[];

			$("#printButton").hide();
			$scope.ryotLedger = angular.copy($scope.master);
			$("#dispLedger").hide();
				 form.$setPristine(true);			    								
			   
		};
	})
	
	
	  //================= Re Print Permits============================
	.controller('ReprintPermitController',function($scope,$http)
	{
		var permitGridObj = new Object();
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		var printPermitObj = new Object();
		
		 //-------------Load Villages-------
		$scope.loadVillageNames = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.VillagesNamesData = data;
					$scope.Reprint={'status':'0'};
					
				});
		};
		$scope.getPrintOption = function(printOption)
		{
			if(printOption==1)
			{
				
				$("#singlePrint").hide();
				$("#multiplePrint").show();
				$("#singlePermitPrint").hide();	
			}
			else
			{
				$scope.items = [];
				$("#multiplePrint").hide();
				$("#singlePrint").show();
				$("#singlePermitPrint").show();
			}
		};
		
		//--------------Show Popup Permit  Dialog------
		$scope.displayGridDialogBox = function()
		{
			
			permitGridObj.ryotCode = $scope.Reprint.ryotCode;
			permitGridObj.agreementNumber = $scope.Reprint.agreementNumber;
			permitGridObj.villageCode = $scope.Reprint.villageCode;
			//alert(JSON.stringify(permitGridObj));
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSearchingData.html",
					data: JSON.stringify(permitGridObj),
				 }).success(function(data, status) 
				 {	
					
					$('.popupbox_ratoon').fadeIn("slow");
					$scope.data = data;
					
				 }); 
				
			
		};
		
		//---------Close Dialog  Popup Permit------		
		$scope.CloseDialogRatoon = function()
		{
			$('.popupbox_ratoon').fadeOut("slow");
		};
		
		
		
		$scope.getPermitNumber = function(permit)
		{
			$scope.CloseDialogRatoon();
			$scope.permitNumber  = permit;
			
				var permitObj = new Object();
				permitObj.permitNumber = permit;
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/generatePermitPrint.html",
				data: JSON.stringify(permitObj),
			   }).success(function(data, status) 
				{
					$scope.items = data;
					
				}); 
		};
		var validateFlag = '';
		$scope.validateHarvestDateForPermit = function(permitNo)
		{
			var permitValidateObj = new Object();
			var npermitNo = parseInt(permitNo);
			permitValidateObj.permitNo = npermitNo;
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/validateHarvestDateForPermit.html",
				data: JSON.stringify(permitValidateObj),
			   }).success(function(data, status) 
				{
					$scope.items = data;
					var val = JSON.stringify(data);
					validateFlag = val;
					if(val == 'false')
					{
						sweetAlert("Oops...", "Harvesting Date is not given for this permit. Please enter valid permit number and try again!", "error");
						$scope.permitNumber = '';	
					}
					
				});
			
		}
		
		$scope.getdirectPermitPrint = function(permitNo)
		{
			//var permitGridObj  = new Object();
			var permitDirectObj = new Object();
			var npermitNo = parseInt(permitNo);
			permitDirectObj.permitNo = npermitNo;
			
			var permitDirectObj1 = new Object();
			var npermitNo = parseInt(permitNo);
			permitDirectObj1.permitNumber = npermitNo;
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/validateHarvestDateForPermit.html",
				data: JSON.stringify(permitDirectObj),
			   }).success(function(data, status) 
				{
					var val = JSON.stringify(data);
					validateFlag = val;
					//alert(val);
					if(val == 'false')
					{
						sweetAlert("Oops...", "Harvesting Date is not given for this permit. Please enter valid permit number and try again!", "error");
						$scope.permitNumber = '';	
					}
					else if(val == 'true')
					{
						//alert("chiru");
						//alert(JSON.stringify(permitDirectObj));
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/generatePermitPrint.html",
						data: JSON.stringify(permitDirectObj1),
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							document.getElementById("1234").style.display="none";
							$scope.items = data;
						});
					}
					
				});
		};
		
		$scope.getPrintbtwDates = function(fromDate,toDate)
		{
			if(fromDate!=null && toDate!=null)
			{
				var DirectObjPrint = new Object();
				DirectObjPrint.fromdate = fromDate;
				DirectObjPrint.todate = toDate;
				if(fromDate!='' && toDate!='')
				{
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getPermitDetailsByHarvestingDates.html",
				data: JSON.stringify(DirectObjPrint),
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.items = data;
				});
				}
			}
		};
		
		$scope.getPermitPrintByHarvestDate =  function()
		{
			var fromDate = $scope.Reprint.fromdate;
			var toDate = $scope.Reprint.todate;
			
			//alert('fromDate-------'+fromDate);
			//alert('toDate-------'+toDate);
			var permitByHarvestObj = new Object();

			permitByHarvestObj.fromdate = fromDate;
			permitByHarvestObj.todate = toDate;
			

			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getPermitDetailsByHarvestingDates.html",
				data: JSON.stringify(permitByHarvestObj),
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					 //document.getElementById("1234").style.display='block';
					$scope.items = data;
					
					var prtContent = document.getElementById("1234");
					var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
					
					
				});

			
		};
		
		$scope.PrintrePrintReport = function(permitNo)
		{
			
			var permitDirectObj = new Object();
			permitDirectObj.permitNumber = permitNo;
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/generatePermitPrint.html",
				data: JSON.stringify(permitDirectObj),
			   }).success(function(data, status) 
				{
					$scope.items = data;
					//document.getElementById("1234").style.display="block";	
					var prtContent = document.getElementById("1234");
					var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
					
				});
		};
		
	})
  
    //=================Re Generate Print Permits============================
  .controller('ReGenerateprintPermitController',function($scope,$http)
	{
		var permitGridObj = new Object();
		var DropDownLoad = new Object();
		var seasonFilter = new Object();
		var AddedRegenerateObj = new Object();
		DropDownLoad.dropdownname = "Village";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		$scope.master = {};
		 //-------------Load Villages-------
		$scope.loadVillageNames = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.VillagesNamesData = data; 		
				});
		};
		
		
		 $scope.ShowDatePicker = function()
		 {
					//alert('ashok');
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		//--------------Show Popup Permit  Dialog------
		$scope.displayGridDialogBox = function()
		{
			
				permitGridObj.ryotCode = $scope.Reprint.ryotCode;
				permitGridObj.agreementNumber = $scope.Reprint.agreementNumber;
				permitGridObj.villageCode = $scope.Reprint.villageCode;
				
				var permitNumber = $scope.Reprint.permitNumber;
				if(permitNumber == '' || permitNumber==null)
				{
					permitNumber = 0;
				}
				
				permitGridObj.permitNumber = parseInt(permitNumber);
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getSearchingData.html",
				data: JSON.stringify(permitGridObj),
				}).success(function(data, status) 
				{	
				
					$('.popupbox_ratoon').fadeIn("slow");
					$scope.data = data;
					
				}); 
				
			
		};
		
		
		//---------Close Dialog  Popup Permit------		
		$scope.CloseDialogGridPopup = function()
		{
			$('.popupbox_ratoon').fadeOut("slow");
		};
		

		$scope.getPermitNumber = function(permit,ryotcode,ryotName,agreementno,mandalName,landvilcode,season,plotNo)
		{
			$scope.CloseDialogGridPopup();
			$("#poupDetails").show();
			$scope.AddedRegenerate = {"permitNumber":permit};
			$scope.AddedRegenerate.ryotCode  = ryotcode;
			$scope.AddedRegenerate.ryotName  = ryotName;
			$scope.AddedRegenerate.agreementNo  = agreementno;
			$scope.AddedRegenerate.mandalName  = mandalName;
			$scope.AddedRegenerate.villageCode  = landvilcode;
			$scope.AddedRegenerate.season  = season;
			 $scope.AddedRegenerate.plotNo = plotNo;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(data, status) 
				 {
					$scope.AddedRegenerate.date = data.serverdate;
			 	 });
			seasonFilter.season = season;
			
			
			//alert(JSON.stringify(seasonFilter));
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxPermitNum.html",
					data: season,
			     }).success(function(data, status) 
				 {
					 //alert(JSON.stringify(data));
					$scope.AddedRegenerate.newpermitNumber = data;
					
			 	 });
			
			
		};
		
		$scope.permitRegenerateSubmit = function(AddedRegenerate,form)
		{
			  //var AddedRegenerate = JSON.stringify(AddedRegenerate);
			  AddedRegenerateObj.formData = AddedRegenerate;
			 
			   delete AddedRegenerate['mandalName'];
			   //alert($scope.permitRegenerateForm.$valid);
			   //alert(JSON.stringify(AddedRegenerateObj));
			  if($scope.permitRegenerateForm.$valid) 
				{	
					//$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveReGeneratePermit.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedRegenerateObj),
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	 
								//alert(response[0].issuccess);
								if(response[0].issuccess=="true")
								{
									//alert(JSON.stringify(response));
									
				
									//swal("Success!", 'Regenerate Permit Details Added Successfully!', "success");
									$scope.itemData = response;
									$scope.$apply();
									$("#poupDetails").hide();
									//document.getElementById("dispGeneratePrint").style.display='block';
									
									swal({
										   	 title: "Permit Regenerated Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 function(){
											var prtContent = document.getElementById("dispGeneratePrint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											
										});	
								}
								else
								{
									 sweetAlert("Oops...", "Something went wrong!", "error");
									
								}
															
								
							}
						});					
				}					
			
		};
		
		$scope.printGeneratePermit = function()
		{
			var prtContent = document.getElementById("dispGeneratePrint");
			var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
			WinPrint.document.write(prtContent.innerHTML);
			WinPrint.document.close();
			WinPrint.focus();
			WinPrint.print();
			WinPrint.close();
		};
		
		$scope.CloseDialogRatoon = function()
		{
			$("#gridpopup_box").fadeOut();
		};
		
	})
  
	
	 //==================================
			//Special Permit
	//==================================	
	.controller('specialPermitController',function($scope,$http)
	{
	
		 var SerialNumberObj = new Object();
		 SerialNumberObj.tablename = "AgreementSummary";
		 SerialNumberObj.col1 = "agreementseqno";
		 SerialNumberObj.col2 = "seasonyear";	 	 
		 
		 var jsonSerialNumberString= JSON.stringify(SerialNumberObj);

		$scope.master = {};
      
		
		//-----------Load Cane Accounting Serial Number--------	
	
				
		$index = 0;		
		//$scope.data = [];
		$scope.AddAgreement = [];
		//var agreementGridData = [];
		var AddAgreementObj = new Object();
		
		

		var seasonObj = new Object();
		seasonObj.dropdownname = "Season";
		var jsonSeasonString= JSON.stringify(seasonObj);

		var ryotExtObj = new Object();
		ryotExtObj.dropdownname = "ExtentDetails";
		var jsonRyotExtString= JSON.stringify(ryotExtObj);
		
		var ryotObj = new Object();
		ryotObj.dropdownname = "Ryot";
		var jsonRyotString= JSON.stringify(ryotObj);
		
		var AddedAgreementDropDown = new Object();
		AddedAgreementDropDown.tablename = "AgreementSummary";
		AddedAgreementDropDown.columnName = "agreementseqno";
		AddedAgreementDropDown.columnName1 = "agreementnumber";
		var jsonAddedAgreementDropDown= JSON.stringify(AddedAgreementDropDown);

		var agreementNumber = new Object();
		agreementNumber.tablename = "AgreementSummary";
		var jsonAgreementString= JSON.stringify(agreementNumber);
		
		
		
	
		 //--------Load Server Date--------
			$scope.loadServerDate = function()
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						 
						$scope.AddAgreement.permitDate = data.serverdate;
						$scope.AddAgreement.harvestingDate = data.serverdate;
						//$scope.permitSelection = //modified by umanath 29-11-2016
						$scope.AddAgreement = {'permitDate' : data.serverdate,'harvestingDate':data.serverdate,'permitSelection' : '0'};
						
						//$scope.AddAgreement.harvestingDate = 
						
				 	 });	
			};

			
			$scope.loadProgramBySeason = function(permitSeason)
		{			
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: permitSeason,
	  		}).success(function(data, status) 
			{
				
				$scope.GeneratePermitProgramNames=data;
				
				
			}); 			
		};
		
		$index=0;
			$scope.SpecialPermitData=[];
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.SpecialPermitData.push({'id':$index})
			
			};
		
		$scope.removeRow = function(name,maxid,permitSelection)
			{
					//modified by umanath 29-11-2016
					 $scope.SpecialPermitData.splice(name, 1);
				 
				 $scope.generateSpecialPermit(maxid,permitSelection);
				 
					
  	    	};
		
		$scope.getNewPermitRules = function()
		 {
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getPermitRulesNew.html",
						data: {},
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data[0]));
					//	alert(data[0].permittype);
						//alert(data[0].noofpermitperacre);
							//alert(data.id);
							$scope.permitType=data[0].nooftonspereachpermit;
							$scope.noofpermitperacre = data[0].nooftonsperacre;
						});											 
		 };
		 
		 //---------Get Permit Rules-----------------
		$scope.getPermitRules = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getPermitRules.html",
				data: {},
	  		}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.PermitRules = data[0];
			}); 						
		};	

		


		//--------Load Season Dropdown----
		$scope.loadSeasonNames = function()
		{
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonSeasonString,
			   }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
					
				}); 
			};
		//--------Load RyotCode Dropdown----
		$scope.loadRyotNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonRyotString,
			   }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.ryotNames=data;
				}); 
			};
		
		//--------Load RyotCode From Extent Dropdown----
		$scope.loadRyotExtentNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonRyotExtString,
			   }).success(function(data, status) 
				{
					
					$scope.ryotNamesExtent=data;
				}); 
			};
			
			$scope.setAddAgreement = function(seasonYear)
		   {
			   var addAgreementObj = new Object();
				addAgreementObj.season = seasonYear;
			//added by naidu on 23-01-2017
			var tablename="AgreementSummary";
				addAgreementObj.tablename = tablename;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAgreementNumbersForSeason.html",
					data: JSON.stringify(addAgreementObj),
				   }).success(function(data, status) 
					{
						$scope.AddedAgreements=data;							
					});
		};
		
			
			$scope.getAllAddedDetails = function(season,agreementNumber)
			{
			var specialPermitObjDetails =  new Object();
			specialPermitObjDetails.season = season;
			specialPermitObjDetails.agreementnumber = agreementNumber;
			//added by naidu on 23-01-2017
				var aggtype="1";
				
				specialPermitObjDetails.aggType = aggtype;
						
			
				if(season!=null || season!='' && agreementNumber!=null || agreementNumber!='')
				{
					//alert(JSON.stringify(specialPermitObjDetails));
					//alert(JSON.stringify(specialPermitObjDetails));
					//alert(JSON.stringify(specialPermitObjDetails));
				var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAllAgreementDetails.html",
										data: JSON.stringify(specialPermitObjDetails),
									  }).success(function(data, status) 
									  {	

									  
									 var getrytotName = data.formData.ryotCode;
									  $scope.AddAgreement.ryotCode = data.formData.ryotCode;
									  $scope.SpecialPermitData = data.gridData;
									
									  
									  
									  var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/GetRyotNameForCode.html",
									data:getrytotName,
									}).success(function(data, status) 
									{
										var ryotName  = data[0].ryotName;
										$scope.AddAgreement.ryotName = ryotName;
									});

							  });
				}
			
				
			};
			
			 $scope.loadPermitMaxNumber=function(seasonYear)
	 			{
				
					
				var httpRequest = $http
					({
					method: 'POST',
					url : "/SugarERP/getMaxPermitNum.html",
					data: seasonYear,

	 			}).success(function(data) 
				{
					$scope.AddAgreement.maxNumber =data-Number(1);
					
							
				});
	};
			
			
		$scope.changePermitSelection = function(maxid,permitSelection)
		{
			
			$scope.generateSpecialPermit(maxid,permitSelection);
		}
		
		$scope.generateSpecialPermit = function(maxid,permitSelection)
		{	
		//alert(maxid);
		
			var nextPermit=maxid;
			
			//alert("Permit Type"+$scope.permitType);
			//alert("No. of permits Per Acre"+$scope.noofpermitperacre);
			
			for(var s=0;s<$scope.SpecialPermitData.length;s++)
			{         
			        
					if($scope.permitType=="1")
					{
					//alert("PlotSize"+$scope.GeneratePermitData[s].plotSize);
					//alert("No of permit per acre"+$scope.noofpermitperacre);
					var permitCount = $scope.SpecialPermitData[s].extentSize*$scope.noofpermitperacre;
				
					}
					else
					{
						var permitCount = $scope.SpecialPermitData[s].extentSize*$scope.noofpermitperacre/$scope.permitType;
					
					}
					
					permitCount = Math.ceil(permitCount);
					//alert("permitCount"+permitCount);
					var assignPermitNum = "";
					if(permitSelection==1)
					{
						for(var gp=1;gp<=permitCount;gp++)
						{
													
							var permitNum = Number(nextPermit)+Number(gp);						
							assignPermitNum += permitNum+",";
							
						}
						
					}
					
					else if(permitSelection==0)
					{
						
						for(var gp=1;gp<=1;gp++)
						{
													
							var permitNum = Number(nextPermit)+Number(gp);						
							assignPermitNum += permitNum+",";
							
						}
						
							//var permitNum = Number(nextPermit);						
							//assignPermitNum += permitNum+",";
							
						
					}
					
					nextPermit = permitNum;
					
					$scope.SpecialPermitData[s].permitNumber = assignPermitNum.substring(0,assignPermitNum.length-1);
					//alert($scope.GeneratePermitData[s].permitNumber);
					
			}
		};
		
	
		
		//---------Load Ryot Details on RyotCode Change--
		
		$scope.loadRyotDet = function(RyotCode)
		{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/GetRyotNameForCode.html",
			data: RyotCode,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.AddAgreement.ryotName = data[0].ryotName;
			});
			
			
		};
		
	
		//--------------Load Extent Details in Sueveyno Popup---
		$scope.loadAddedExtentDetails = function(ryotCodePop)
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getUpdateSoilTestResultsForPopup.html",
					data: ryotCodePop,
				  }).success(function(data, status) 
				  {		
				  		$scope.ryotNamePop = data[0].ryotCode;						
				  		$scope.PopupExtentData = data;
				  }).error(function(data,status)
				  {
				  });		
		};
		
		
		$scope.printGeneratePermit = function()
			{
				
				var prtContent = document.getElementById("dispGeneratePrint");
				var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
				WinPrint.document.write(prtContent.innerHTML);
				WinPrint.document.close();
				WinPrint.focus();
				WinPrint.print();
				WinPrint.close();
			};
		
		$scope.specialPermitFormSubmit = function(season,ryotCode,permitDate,agreementAddNumber,programno,ryotName,harvestingDate,remarks,form)
		{
			var SubmitObj = new Object(); 
			var obj = new Object();
			SubmitObj.season = season;
			SubmitObj.ryotCode = ryotCode;
			SubmitObj.permitDate = permitDate;
			SubmitObj.agreementAddNumber = agreementAddNumber;
			SubmitObj.programno = programno;
			SubmitObj.ryotName = ryotName;
			SubmitObj.harvestingDate = harvestingDate;
			SubmitObj.remarks = remarks;
		//	SubmitObj.permitSelection = permitSelection;
			//alert(JSON.stringify(SubmitObj));
			
			
			
			var specialPermitGridData = [];
				$scope.SpecialPermitData.forEach(function (PermtiData) 
				{
					delete PermtiData['extentImage'];
					delete PermtiData['plantorRatoon'];
					delete PermtiData['landVillageCode'];
					delete PermtiData['landTypeCode'];
					delete PermtiData['agreedQty'];
					delete PermtiData['circleCode'];
					delete PermtiData['cropDate'];
					delete PermtiData['plotIdentity'];
					delete PermtiData['plotDistance'];
					delete PermtiData['surveyNumber'];
					delete PermtiData['address'];
					delete PermtiData['slno'];
					delete PermtiData['plotSeqNo'];
					delete PermtiData['varietyCode'];
					
					
					
					
	        		specialPermitGridData.push(angular.toJson(PermtiData));														 
				});	
			
			obj.formData = JSON.stringify(SubmitObj);
			obj.gridData = specialPermitGridData;
			//alert(gridData.permitNumber);
			
			if($scope.specialPermitForm.$valid)
			{
				
				$('.btn-hide').attr('disabled',true);																
				$.ajax({
				    	url:"/SugarERP/saveSpecialPermit.html",
					    processData:true,
				    	type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			    	       	xhr.setRequestHeader("Accept", "application/json");
				    	    xhr.setRequestHeader("Content-Type", "application/json");
					    },	
					    success:function(response) 
						{
							
							if(response[0].issuccess=="true")
							{
								//alert(JSON.stringify(response));
								
								//alert($scope.SpecialPermitData.length);
								$scope.itemData = response;
									$scope.$apply();
									swal({
										   	 title: "Harvesting Date Updated Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 function(){
											var prtContent = document.getElementById("dispGeneratePrint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											
										});
								
								
								//swal("Success", 'Special Permit Generated Successfully!', "success");		
								$('.btn-hide').attr('disabled',false);	
							 $scope.AddAgreement = angular.copy($scope.master);
							
								 $scope.SpecialPermitData = [];
								 
								// $scope.loadSeasonNames();
								 $scope.loadServerDate();
								 form.$setPristine(true);
								 
								 
								 
								  
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);																
							}
						}
					});
			}
			
			else
		    {
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }				
			}
		
			
			
			
			
		};
		//------------Set Survey Number-----
		
	})


	//======================Reprint Receipt Controller =====================
  //======================================================================
		.controller('reprintReceiptController',function($scope,$http)
	 {
			var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);//
			
			 var seasonObj = new Object();
		 seasonObj.dropdownname = "Season";
	 	var jsonSeasonString= JSON.stringify(seasonObj);
	 
			
			$scope.master = {};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
					//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddReprintReceipt = {'lorryStatus':0};
				
			}); 		
		};
		
		$scope.loadCaneWeightSeasonNames = function()
		{		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonSeasonString,
				}).success(function(data, status) 
				{		//alert("1");		
					$scope.seasonNames=data;
                    $scope.AddReprintReceipt.season='2016-2017';
					
					//alert("2");
				}); 
		};
	
		
		$scope.getPermitWiseRyotDetails = function(fromDate,toDate,shiftName,lorryStatus)
		{
			
			var getPermitWiseRyDetObj = new Object();
 

		if((fromDate!=undefined) &&  (toDate!=undefined) &&  (shiftName!=undefined ))
		{
			
			getPermitWiseRyDetObj.fromDate = fromDate;
			getPermitWiseRyDetObj.toDate = toDate;
			getPermitWiseRyDetObj.shift = shiftName;
			var nlorryStatus = parseInt(lorryStatus);
			getPermitWiseRyDetObj.lorryStatus = nlorryStatus;

			//
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/GetReprintReceiptDetails.html",
				data: JSON.stringify(getPermitWiseRyDetObj),
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
                if(JSON.stringify(data)!='' || JSON.stringify(data)==null)
				{
					$('.popupbox_ratoon').fadeIn("slow");
					$scope.data = data;
				}
			
				//$scope.data = data;
				
			}); 
			
		}
		};
		
		$scope.displayGridDialogBox = function()
		{
			
			permitGridObj.ryotCode = $scope.Reprint.ryotCode;
			permitGridObj.agreementNumber = $scope.Reprint.agreementNumber;
			permitGridObj.villageCode = $scope.Reprint.villageCode;
			//alert(JSON.stringify(permitGridObj));
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSearchingData.html",
					data: JSON.stringify(permitGridObj),
				 }).success(function(data, status) 
				 {	
					
					$('.popupbox_ratoon').fadeIn("slow");
					$scope.data = data;
					
				 }); 
				
			
		};
		
		//---------Close Dialog  Popup Permit------		
		$scope.CloseDialogRatoon = function()
		{
			$('.popupbox_ratoon').fadeOut("slow");
		};

		$scope.getPermitNumber = function(permit,serialnumber)
		{
			$scope.CloseDialogRatoon();
			$("#permitNumberprint").show();
		//	$scope.permitNumber  = permit;
			
				var permitObj = new Object();
				permitObj.permitNumber = parseInt(permit);
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/generatePermitPrint.html",
				data: JSON.stringify(permitObj),
			   }).success(function(data, status) 
				{
					
					$scope.AddReprintReceipt.permitNumber = permit;
					$scope.AddReprintReceipt.permitNumber = permit;
					$scope.AddReprintReceipt.serialNumber = parseInt(serialnumber);
					//$scope.items = data;

					

					
				}); 
		};
		
		
		
		$scope.generatePrint = function(season,permitNumber,status)
		{
		
			var printSeasonSerial = new Object();
			printSeasonSerial.serailNumber = parseInt(permitNumber);
			printSeasonSerial.season = season;
			var nstatus = parseInt(status);
			printSeasonSerial.lorryStatus = nstatus;
		//alert(JSON.stringify(printSeasonSerial));
		//$("#permitNumberprint").hide();
		
			if(status==1)
			{
				 	var httpRequest = $http({
									  method: 'POST',
									  url : "/SugarERP/printCaneWeighment.html",
									  data: JSON.stringify(printSeasonSerial),
								   }).success(function(data, status) 
								   {	
									//  alert("second submit"+JSON.stringify(data));
									var bindingMaterial = data[0].bindingMaterial;
									var grossWeight = data[0].grossweight.toFixed(3);
									var lorryWeight = data[0].lorrywt.toFixed(3);
									var grossWt = data[0].grossWt.toFixed(3);
									var netWt = data[0].netWt.toFixed(3);

									//alert('bindingMaterial-----'+bindingMaterial);
											//var diffnetWt =  data[0].grossweight-data[0].lorrywt;
											// data[0].netwt = diffnetWt;
											//alert("lorryWeight"+data[0].lorrywt);
								  var popupWin = window.open('', '_blank', 'width=300,height=300');
  									popupWin.document.open();
 



								popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2.7%;font-size:18px;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].systemdate+'</span><span style="position:fixed; left:25%; top:10%;">Weightment : '+data[0].weighBridge+'</span><span style="position:fixed; left:67%; top:9%;font-size:18px;">'+data[0].actualSlNo+'</span></div><div><span style="position:fixed; left:25%; top:14%;">REF : '+data[0].serialnumber+'</span><span style="position:fixed; left:7%; top:15%;">'+data[0].shiftid+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:21%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:20%; top:39%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:41.5%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:58%; top:55%;">Unloader No.: '+data[0].ucCode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">'+grossWeight+'</span><span style="position:absolute; left:39%; top:71%;">'+lorryWeight+'</span><span style="position:absolute; left:55.5%; top:71%;">'+grossWt+'</span></div><div><span style="position:fixed; left:22%; top:73.5%;"></span><span style="position:absolute; left:35%; top:73.5%;">Binding Material</span><span style="position:absolute; left:56.5%; top:73.5%;">'+data[0].bindingMaterial+'</span></div><div><span style="position:fixed; left:22%; top:75%;"></span><span style="position:absolute; left:39%; top:75%;"></span><span style="position:absolute; left:55.5%; top:75%;">------------</span></div><div><span style="position:fixed; left:22%; top:77%;"></span><span style="position:absolute; left:39%; top:77%;"></span><span style="position:absolute; left:55.5%; top:77%;">'+netWt+'</span></div><div><span style="position:fixed; left:22%; top:79%;"></span><span style="position:absolute; left:39%; top:79%;"></span><span style="position:absolute; left:55.5%; top:79%;">------------</span></div><div><span style="position:fixed; left:22%; top:84%;"></span><span style="position:absolute; left:39%; top:84%;"></span><span style="position:absolute; left:55.5%; top:84%;">Permit No.: '+data[0].permitNo+'</span><span style="position:absolute; left:5%; top:90%;">'+data[0].login+'</span></div></body></html>');

								popupWin.document.close();
										//$scope.data;
								   }).error(function(data,status)
								   {
									
								   });
			}
			else
			{
				
				var httpRequest = $http({
									  method: 'POST',
									  url : "/SugarERP/printCaneWeighment.html",
									  data: JSON.stringify(printSeasonSerial),
								   }).success(function(data, status) 
								   {	
								 
											//alert("gross"+ data[0].grossweight);
											
											var lorryWt = 0;
											var grossWeight = data[0].grossweight.toFixed(3);
											// data[0].lorrywt = lorryWt
											//var diffnetWt =  data[0].grossweight-data[0].lorrywt;
//											 data[0].netwt = diffnetWt;
//											alert("lorryWeight"+data[0].lorrywt);
											
											
								  var popupWin = window.open('', '_blank', 'width=300,height=300');
  									popupWin.document.open();
 


popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print()"><div><span style="position:fixed; left:67%; top:2.7%;font-size:18px;">'+data[0].weighbridgeslno+'</span></div><div><span style="position:fixed; left:7%; top:9.3%;">'+data[0].systemdate+'</span><span style="position:fixed; left:25%; top:10%;">Weightment : '+data[0].weighBridge+'</span><span style="position:fixed; left:67%; top:9%;font-size:18px;">'+data[0].serialnumber+'</span></div><div><span style="position:fixed; left:25%; top:14%;"></span><span style="position:fixed; left:7%; top:15%;">'+data[0].shiftid+'</span><span style="position:fixed; left:67%; top:15%;">'+data[0].vehicletype+'</span></div><div><span style="position:fixed; left:7%; top:21%;">'+data[0].canereceipttime+'</span><span style="position:fixed; left:67%; top:20.5%;">'+data[0].vehicleno+'</span></div><div><span style="position:fixed; left:9%; top:33%;">'+data[0].ryotcode+'</span><span style="position:fixed; left:20%; top:33%;">'+data[0].ryotname+'</span></div><div><span style="position:fixed; left:6%; top:17%;"></span><span style="position:fixed; left:20%; top:39%;">'+data[0].relativeName+'</span><span style="position:fixed; left:69%; top:35%;">'+data[0].cropName+'</span></div><div><span style="position:fixed; left:9%; top:45%;">'+data[0].villagecode+'</span><span style="position:fixed; left:20%; top:45%;">'+data[0].villageName+'</span><span style="position:fixed; left:69%; top:41.5%;">'+data[0].varietyName+'</span></div><div><span style="position:fixed; left:9%; top:51%;">'+data[0].landvilcode+'</span><span style="position:fixed; left:58%; top:55%;">Unloader No.: '+data[0].ucCode+'</span><span style="position:fixed; left:20%; top:23%;"></span></div><div><span style="position:fixed; left:9%; top:57%;">'+data[0].circlecode+'</span><span style="position:fixed; left:20%; top:57%;">'+data[0].circleName+'</span></div><div><span style="position:fixed; left:22%; top:71%;">'+grossWeight+'</span><span style="position:absolute; left:39%; top:71%;"></span><span style="position:absolute; left:55.5%; top:71%;"></span></div><div><span style="position:fixed; left:22%; top:73.5%;"></span><span style="position:absolute; left:34%; top:73.5%;"></span><span style="position:absolute; left:55.5%; top:73.5%;"></span></div><div><span style="position:fixed; left:22%; top:75%;"></span><span style="position:absolute; left:39%; top:75%;"></span><span style="position:absolute; left:55.5%; top:75%;"></span></div><div><span style="position:fixed; left:22%; top:77%;"></span><span style="position:absolute; left:39%; top:77%;"></span><span style="position:absolute; left:55.5%; top:77%;"></span></div><div><span style="position:fixed; left:22%; top:79%;"></span><span style="position:absolute; left:39%; top:79%;"></span><span style="position:absolute; left:55.5%; top:79%;"></span></div><div><span style="position:fixed; left:22%; top:84%;"></span><span style="position:absolute; left:39%; top:84%;"></span><span style="position:absolute; left:55.5%; top:84%;">Permit No.: '+data[0].permitNo+'</span><span style="position:absolute; left:5%; top:90%;">'+data[0].login+'</span></div></body></html>');
popupWin.document.close();
										//$scope.data;
								   }).error(function(data,status)
								   {
									
								   });
				
				
				
				
				
			}
				
			
			
				
			//}
			//else if(status==1)
//			{
			
				
			 //$scope.AddReprintReceipt = angular.copy($scope.master);
				 
								 $scope.permitData = [];
								 $scope.loadShift();
								 $scope.loadCaneWeightSeasonNames();
								 $scope.AddReprintReceipt.fromDate = "";
								 $scope.AddReprintReceipt.toDate="";
								 $scope.AddReprintReceipt.shift ="";
								 $scope.AddReprintReceipt.serialNumber = "";
								// form.$setPristine(true);
			//var prtContent = document.getElementById("1234");
//					var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
//					WinPrint.document.write(prtContent.innerHTML);
//					WinPrint.document.close();
//					WinPrint.focus();
//					WinPrint.print();
//					WinPrint.close();
         
				
								 

		};
		
	 })	
 	
	
	//=========================================
			//Trail Balance
	//=========================================
	
	/*.controller('TrailBalance',function($scope,$http)
	{
		
		var trailSeasonObj = new Object();
		trailSeasonObj.dropdownname = "Season";
		var jsonTrailSeasonString= JSON.stringify(trailSeasonObj);		
		
		
		$scope.loadTrailSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonTrailSeasonString,
				}).success(function(data, status) 
				{
					$scope.getTrailSeasons=data;
				}); 						
		};
		
		$scope.getTrailBalance  = function(TrailBalance)
		{
			$scope.Addsubmitted = true;
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getTrialBalance.html",
					data: JSON.stringify(TrailBalance),
				}).success(function(data, status) 
				{		
					$scope.loadTrailBalanceData = data;
				});
			
		};
	})*/
	.controller('TrailBalance',function($scope,$http)
	{
		
		var trailSeasonObj = new Object();
		trailSeasonObj.dropdownname = "Season";
		var jsonTrailSeasonString= JSON.stringify(trailSeasonObj);		
		
		
		$scope.loadTrailSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonTrailSeasonString,
				}).success(function(data, status) 
				{
					$scope.getTrailSeasons=data;
				}); 						
		};
		
		$scope.getTrailBalance  = function(TrailBalance)
		{
			$scope.Addsubmitted = true;
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getTrialBalance.html",
					data: JSON.stringify(TrailBalance),
				}).success(function(data, status) 
				{		
					//alert(JSON.stringify(data));
					$scope.loadTrailBalanceData = data;
					var i=0;
					var crrunningBalance=0;
					var drrunningBalance=0;
					$scope.loadTrailBalanceData.forEach(function() 
					{

							if($scope.loadTrailBalanceData[i].balancetype=='Cr')
							{	
									crrunningBalance = Number(crrunningBalance)+Number($scope.loadTrailBalanceData[i].runningbalance);								
							}
							else
							{
								drrunningBalance = Number(drrunningBalance)+Number($scope.loadTrailBalanceData[i].runningbalance);
							}	
							i++;
						
		    		});					
					$scope.availableBalance=parseFloat(Math.round(Math.abs(crrunningBalance-drrunningBalance))).toFixed(2);
				
					
					
				});
			
		};
	})

 //===============================================
  		//Schedule Permit
 //===============================================		
  .controller('SchedulePermits',function($http,$scope)
	 {	
		
		var seasonPermitDropDownLoad = new Object();
		seasonPermitDropDownLoad.dropdownname = "Season";
		var StringPermitDropDownSeason= JSON.stringify(seasonPermitDropDownLoad);
		var DropDownLoad=new Object();
		DropDownLoad.dropdownname="Circle";
		var DropDownSeason= JSON.stringify(DropDownLoad);
		
		var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			
		$scope.master = {};
		
		
		$scope.loadPermitSeason = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringPermitDropDownSeason,
				  }).success(function(data, status) 
				  {					  
					 $scope.ScheduleFilter = {'permitDateFlag':'0'};
						$scope.seasonNames = data;
				  }); 
		};
		
		$scope.loadProgramBySeason = function(permitSeason)
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProgramnNumbersBySeason.html",
				data: permitSeason,
	  		}).success(function(data, status) 
			{
				$scope.ScheduleProgramNumber=data;
			}); 			
		};
		
		/*$scope.loadRyotCodeDropDown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: ExtentRyotCode,
			   }).success(function(data, status) 
				{
					$scope.ryotCode = data;
				});  	   
		};*/
		
		
		$scope.master = {};
		$scope.loadCircleNames=function()
		{
				
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: DropDownSeason,
			 }).success(function(data, status) 
			 {
				 $scope.CircleNames=data;
				 data.push({"id":0,"circle":"ALL"});
					
			 }); 
		};
		$scope.getScheduleDetails = function(season,programNumber,fromRank,toRank,circle,unschedule,harvestingDate)
		{
			//alert('circle----'+circle);
			var ScheduleObj = new Object();
			ScheduleObj.season = season;
			ScheduleObj.programNumber = programNumber;
			ScheduleObj.fromRank = parseInt(fromRank);
			ScheduleObj.toRank = parseInt(toRank);
			ScheduleObj.circle = circle;
			ScheduleObj.permitDateFlag = parseInt(unschedule);
			ScheduleObj.HarvestDate = harvestingDate;
			//alert(JSON.stringify(ScheduleObj));
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getHarvestingSchedulePermits.html",
				data: JSON.stringify(ScheduleObj),
			 }).success(function(data, status) 
			 {
				// alert(JSON.stringify(data));
				 $scope.SchedulepermitData = data;
			 }); 
			
		};
		
		$scope.appendHarvestDate = function(hrvDateForPermit)
		{
	 		for(var s=0;s<$scope.SchedulepermitData.length;s++)
			{	
				if($scope.SchedulepermitData[s].hrvFlag)
				{
					$scope.SchedulepermitData[s].harvestDate = hrvDateForPermit;
				}
			}
  		};
		
		
		
		
		
		//---------Save Schedule Permits---------
		$scope.saveSchedulepermitData=function(ScheduleFilter,form)
	  	{
			
			//$scope.submitted =true;
			/*
			if($scope.SchedulepermitData.$valid) 
			{*/
				
				ScheduleGridData = [];
			    $scope.SchedulepermitData.forEach(function(ScheduleData) 
			    {
				    ScheduleGridData.push(angular.toJson(ScheduleData));														 
		        });									
				//alert(JSON.stringify(ScheduleGridData));
				$('.btn-hide').attr('disabled',true);
				$.ajax({
				   		url:"/SugarERP/savePermitDetailsPerHarvestingDate.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(ScheduleGridData),						
						beforeSend: function(xhr) 
						{
				            xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
			   		    },
					    success:function(response) 
						{
							
							if(response[0].issuccess=="true")
							{
									
								 $scope.ScheduleFilter = angular.copy($scope.master);
								 $scope.SchedulepermitData = [];
								 form.$setPristine(true);
								  $scope.loadPermitSeason();
								
									
									$scope.itemData = response;
									$scope.$apply();
									swal({
										   	 title: "Harvesting Date Updated Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 function(){
											var prtContent = document.getElementById("dispGeneratePrint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											
										});
									
									
									$('.btn-hide').attr('disabled',false);									

						  }
						  else
						   {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);							   
						   }
						}
					});					
				/*
				}				
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}									
				}*/
			};
			//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				 $scope.ScheduleFilter = angular.copy($scope.master);
				 $scope.isAllSelected = false;
								 $scope.SchedulepermitData = [];
								 form.$setPristine(true);
								  $scope.loadPermitSeason();			
		   };
		
		/*
		$scope.SchedulepermitData = [
    {value:'Option1', hrvFlag:true}, 
    {value:'Option2', selected:false}
  ];
  */
			$scope.printGeneratePermit = function()
			{
				
				var prtContent = document.getElementById("dispGeneratePrint");
				var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
				WinPrint.document.write(prtContent.innerHTML);
				WinPrint.document.close();
				WinPrint.focus();
				WinPrint.print();
				WinPrint.close();
			};
  
		$scope.optionToggled = function(){
			
    $scope.ScheduleData.hrvFlag = $scope.SchedulepermitData.every(function(itm){ 
	//alert(JSON.stringify(itm));
	return itm.hrvFlag; })
  };
   $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
	 
     angular.forEach($scope.SchedulepermitData, function(itm){ itm.hrvFlag = toggleStatus; });
   
  };
  
  
  

  
	})
	
//==================================
  		//Ryot Permit Details
  //==================================	  
	.controller('ryotPermmitDetailsController', function($scope,$http)
	{
		var filterObj = new Object();
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "ExtentDetails";
		var StringDropDownExtent= JSON.stringify(DropDownLoad);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Season";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			
		$scope.loadryotCode = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDownExtent,
			  }).success(function(data, status) 
			  {
				$scope.ryotNames=data;
			  }); 
		};
						
		$scope.loadSeasonNames = function()
		{				
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDown,
				}).success(function(data, status) 
				{
						//alert(JSON.stringify(data));
				
					$scope.SeasonNames=data;
					//$scope.AddedRyotPermit.season ='2016-2017';
				
				}); 
		};
						
		$scope.getRyotCode = function(RyotCode)
		{
			
			if(RyotCode!=null)
					{
																	
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetRyotNameForCode.html",
					data: RyotCode,
			   }).success(function(data,status) 
			   {
				  	$scope.AddedRyotPermit.ryotname = data[0].ryotName;
					
						
			  		}); 
				 }
	    };
		
		$scope.getRyotPermitDetails = function(AddedRyotPermit)
		{
			$scope.RyotPermmitData= [];
			$scope.Filtersubmitted = true;
		 	filterObj.season = $scope.AddedRyotPermit.season;

			filterObj.ryotcode = $scope.AddedRyotPermit.ryotCode;
			if(filterObj.season!='' && filterObj.ryotcode!='') 
			{
				//alert(JSON.stringify(filterObj));
				 var httpRequest = $http({
				 	  method: 'POST',
					  url : "/SugarERP/getPermitDetailsByRyot.html",
					  data: JSON.stringify(filterObj),
				   }).success(function(data, status) 
					{
							$scope.RyotPermmitData=data;
					});						 
			}
		};						
	})
	
	//===================================
  		//Permit Checkin 
  //===================================
   .controller('PermitCheckinController', function($scope,$http)
  {
	  $scope.getLoginUser = function()
	 {
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getLoginUserForCheckIn.html",
					data: {},
			     }).success(function(data, status) 
				 {
					$scope.AddPermitCheckin.operatorName = data;
			 	 });	
	};
	 $scope.AddPermitCheckin ={'vehicle':1};	 
	
	 var seasonObj = new Object();
	 seasonObj.dropdownname = "Season";
	 var jsonSeasonString= JSON.stringify(seasonObj);
	 
	 var CaneWeightSeasonObj = new Object();
	 CaneWeightSeasonObj.dropdownname = "WeighBridge";
	 var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);	 
	 
	 var UnloadingObj = new Object();
	 UnloadingObj.dropdownname = "UnloadingContractor";
	 var jsonUnloadingString= JSON.stringify(UnloadingObj);	 
	 
	 var serialNumber = new Object();
	 serialNumber.tablename = "WeighmentDetails";
	 var serialNumberString= JSON.stringify(serialNumber);
	 var today = new Date();
	 var dd = today.getDate();
	 var mm = today.getMonth()+1; //January is 0!
	 var yyyy = today.getFullYear();
	 if(dd<10){
     dd='0'+dd
	 } 
	 if(mm<10){
     mm='0'+mm
	 } 
	 var vserialNumber = new Object();
	 vserialNumber.tablename = "PermitDetails";
	 vserialNumber.vehicleInDate=dd+'-'+mm+'-'+yyyy;
	 vserialNumber.vehicleInTime=today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
	 var vserialNumberString= JSON.stringify(vserialNumber)
	
	 
	 $scope.weightStatus = false;
	$scope.readonlyStatusSecond = false;
	$scope.readonlyStatus = true;
	 
	 //$scope.AddPermitCheckin.operatorName = sessionStorage.getItem('username');
	 
	 $scope.master = {};
	
	$scope.validatePermitNumber = function(permitNo)
	 {
		
		 	//alert("else lo code start");
					//setTimeout(function(){
		 var permitValidateObj = new Object();
		var npermitNo = parseInt(permitNo);
		permitValidateObj.permitNo = npermitNo;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getValidatePermitNoForCheckIn.html",
					data: permitNo,
					}).success(function(data, status)
					{
						
						if(data==true)
						{
							sweetAlert("Oops...", "Permit is already Checked In for the given permit number. Please enter valid permit number and try again!", "error");
							$scope.AddPermitCheckin.permitNumber = '';
							$scope.AddCaneWeight.vehicleNumber = '';
							$scope.AddCaneWeight.vehicle = '1';
							$scope.loadVehicleMaxId();$scope.loadServerDate();$scope.loadCaneWeightSeasonNames();$scope.loadWeighBridgeNames();$scope.loadServerTime();$scope.loadCaneWeighMaxId();$scope.loadVehicleDetailsByShifts();
						}
						else
						{
							
							
			var permitValidateObj = new Object();
			var npermitNo = parseInt($scope.AddPermitCheckin.permitNumber);
			var serialNumber = parseInt($scope.AddPermitCheckin.serialNumber);

			permitValidateObj.permitNo = npermitNo;
			//permitValidateObj.serialNumber = serialNumber;

		
		

		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/validateHarvestDateForPermitForWeighment.html",
			data: JSON.stringify(permitValidateObj),
		   }).success(function(data, status) 
			{
				$scope.items = data;
				var val = JSON.stringify(data);
				validateFlag = val;
				
				//alert("val"+val);
				if(val==3)
				{
					sweetAlert("Oops...", "Permit Number is expired. Please regenreate permit and  try again!", "error");
					
					$scope.AddPermitCheckin.permitNumber ='';
					$scope.AddPermitCheckin.ryotCode='';
					$scope.AddPermitCheckin.ryotName='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.varietyCode='';
					$scope.AddPermitCheckin.village='';
					$scope.AddPermitCheckin.circle='';
					$scope.AddPermitCheckin.villageCode='';
					$scope.AddPermitCheckin.circleCode='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.extentSize='';
					$scope.AddPermitCheckin.agreementNumber='';
					$scope.AddPermitCheckin.plantOrRatoon = '';
					$scope.AddPermitCheckin.plotNumber = '';
					$scope.AddPermitCheckin.zoneCode ='';
					$scope.AddPermitCheckin.programNumber = '';
					$scope.AddPermitCheckin.landVillageCode = '';
					$scope.AddPermitCheckin.relativeName = '';
					if(data.plantOrRatoon=='1')
						{
							$scope.AddPermitCheckin.plant='';
						}
					   else
						{
							$scope.AddPermitCheckin.plant='';
						}
					
					$scope.loadVehicleMaxId();$scope.loadServerDate();$scope.loadCaneWeightSeasonNames();$scope.loadWeighBridgeNames();$scope.loadServerTime();$scope.loadCaneWeighMaxId();$scope.loadVehicleDetailsByShifts();
					
					
				}
				else if(val==2)
				{
					sweetAlert("Oops...", "Harvesting Date is not given for this permit. Please enter valid permit number and  try again!", "error");
					
					$scope.AddPermitCheckin.permitNumber ='';
					$scope.AddPermitCheckin.ryotCode='';
					$scope.AddPermitCheckin.ryotName='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.varietyCode='';
					$scope.AddPermitCheckin.village='';
					$scope.AddPermitCheckin.circle='';
					$scope.AddPermitCheckin.villageCode='';
					$scope.AddPermitCheckin.circleCode='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.extentSize='';
					$scope.AddPermitCheckin.agreementNumber='';
					$scope.AddPermitCheckin.plantOrRatoon = '';
					$scope.AddPermitCheckin.plotNumber = '';
					$scope.AddPermitCheckin.zoneCode ='';
					$scope.AddPermitCheckin.programNumber = '';
					$scope.AddPermitCheckin.landVillageCode = '';
					$scope.AddPermitCheckin.relativeName = '';
					
					$scope.AddPermitCheckin.plant='';
					
					
				$scope.loadVehicleMaxId();$scope.loadServerDate();$scope.loadCaneWeightSeasonNames();$scope.loadWeighBridgeNames();$scope.loadServerTime();$scope.loadCaneWeighMaxId();$scope.loadVehicleDetailsByShifts();
					
					
				}
				else
				{
				
					//=============Get Details//================
					//alert("setTimeOutStart"+permitNo);
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getPermitDetailsByPermitNumber.html",
				data: permitNo,
			}).success(function(data, status) 
			{
			
				var slnoForPermit = data.slno;
				
				
				if(data.slno==0)
				{
				
					var pNumber  = $scope.AddPermitCheckin.permitNumber;
				
					var bck = data.permitNumber;
					if(bck==pNumber)
					{
					$scope.AddPermitCheckin.ryotCode=data.ryotCode;
					$scope.AddPermitCheckin.ryotName=data.ryotName;
					$scope.AddPermitCheckin.variety=data.variety;
					$scope.AddPermitCheckin.varietyCode=data.varietyCode;
					$scope.AddPermitCheckin.village=data.village;
					$scope.AddPermitCheckin.circle=data.circle;
					$scope.AddPermitCheckin.villageCode=data.villageCode;
					$scope.AddPermitCheckin.circleCode=data.circleCode;
					$scope.AddPermitCheckin.variety=data.variety;
					$scope.AddPermitCheckin.extentSize=data.extentSize;
					$scope.AddPermitCheckin.agreementNumber=data.agreementNumber;
					$scope.AddPermitCheckin.plantOrRatoon = data.plantOrRatoon;
					$scope.AddPermitCheckin.plotNumber = data.plotNumber;
					$scope.AddPermitCheckin.zoneCode = data.zoneCode;
					$scope.AddPermitCheckin.programNumber = data.programNumber;
					
					if(data.plantOrRatoon=='1')
					{
						$scope.AddPermitCheckin.plant="Plant";
					}
				   else
					{
						$scope.AddPermitCheckin.plant="Ratoon";
					}
					$scope.AddPermitCheckin.relativeName=data.relativeName;
					$scope.AddPermitCheckin.landVillageCode=data.landVillageCode;
					/*
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxId.html",
					data: serialNumberString,
				   }).success(function(data, status) 
				  {
					 $scope.AddPermitCheckin.serialNumber = data.id;
				  });
				  */
					}
				}
				else if(data.slno>0)
				{sweetAlert("Oops...", "The Given PermitNumber weighment had completed. Please consult ERP Department and  try again!", "error");
					
					$scope.AddPermitCheckin.permitNumber ='';
					$scope.AddPermitCheckin.ryotCode='';
					$scope.AddPermitCheckin.ryotName='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.varietyCode='';
					$scope.AddPermitCheckin.village='';
					$scope.AddPermitCheckin.circle='';
					$scope.AddPermitCheckin.villageCode='';
					$scope.AddPermitCheckin.circleCode='';
					$scope.AddPermitCheckin.variety='';
					$scope.AddPermitCheckin.extentSize='';
					$scope.AddPermitCheckin.agreementNumber='';
					$scope.AddPermitCheckin.plantOrRatoon = '';
					$scope.AddPermitCheckin.plotNumber = '';
					$scope.AddPermitCheckin.zoneCode ='';
					$scope.AddPermitCheckin.programNumber = '';
					$scope.AddPermitCheckin.landVillageCode = '';
					$scope.AddPermitCheckin.relativeName = '';
					if(data.plantOrRatoon=='1')
						{
							$scope.AddPermitCheckin.plant='';
						}
					   else
						{
							$scope.AddPermitCheckin.plant='';
						}
					$scope.loadVehicleMaxId();$scope.loadServerDate();$scope.loadCaneWeightSeasonNames();$scope.loadWeighBridgeNames();$scope.loadServerTime();$scope.loadCaneWeighMaxId();$scope.loadVehicleDetailsByShifts();
					
				}
			
				
			});
			
				}
					
				
				
			});
					}
					});
						
	};
	
	
	  $scope.setRowAfterReset = function()
	  {
		 // alert(2);
		  var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxIdForVehicleCheckSlno.html",
			data: serialNumberString,
		 }).success(function(data, status) 
		 {
			
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(dataDate, status) 
				 {
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerTime.html",
						data: {},
					 }).success(function(dataTime, status) 
					 {
						 
				$scope.getShiftId(dataTime.servertime);
						 $scope.AddPermitCheckin ={'vehicle':1,'serialNumber':data.id,'caneWeighmentDate':dataDate.serverdate,'season':'2016-2017','weighBridgeId':2,'caneWeighmentTime':dataTime.servertime,'shiftTime':dataTime.servertime};
						//$scope.loadShiftBridgeDetails();
						$scope.loadVehicleDetailsByShifts();
					 	
						 $scope.readonlyStatusSecond = false;
						 $scope.readonlyStatusforPermit =false;
						
					 });								 
			 	 });			 		 
		 });
			
	  };
	 
	 //------------------Get Shift Id--------------		
	 $scope.getShiftId = function(caneWeighmentTime)
	 {
	 	var httpRequest = $http({
	 			method: 'POST',
				url : "/SugarERP/getShiftIdNew.html",
				data: caneWeighmentTime,
		     }).success(function(data, status) 
			 {
				 
				//alert(data.ShiftId);
				$scope.AddPermitCheckin.shiftId=data.ShiftId;
				if(data.ShiftId=='C')
				{
					 $(".applybgColorGreen").css("color", "blue");
					 $(".applybgColorGreen").css("background-color", "yellow");
					  $(".applybgColorGreen").css("font-weight", "bold");
				}
				else if(data.ShiftId=='B')
				{
					  $(".applybgColorBlue").css("color", "blue");
					   $(".applybgColorBlue").css("background-color", "yellow");
					    $(".applybgColorBlue").css("font-weight", "bold");
				}
				else if(data.ShiftId=='A')
				{
					  $(".applybgColorRed").css("color", "blue");
					   $(".applybgColorRed").css("background-color", "yellow");
					     $(".applybgColorRed").css("font-weight", "bold");
				}
		 	 });
		};		
	
	
	//--------Load Server Date--------
	 $scope.loadServerDate = function()
	 {
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getServerDate.html",
					data: {},
			     }).success(function(data, status) 
				 {
					$scope.AddPermitCheckin.caneWeighmentDate = data.serverdate;
					$scope.loadShiftBridgeDetails();
			 	 });	
	};
	
	//-----------Load Server Time-------------
	var ntime = '';
	$scope.loadServerTime = function()
	{
		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getServerTime.html",
			data: {},
		 }).success(function(data, status) 
		 {
			// alert(JSON.stringify(data));
			$scope.AddPermitCheckin.caneWeighmentTime = data.servertime;
			$scope.AddPermitCheckin.shiftTime = data.servertime;
			ntime = data.servertime;
			//alert(data.servertime);
			$scope.getShiftId(data.servertime);
		 });			
	};
	//-----------load loadVehicleDetails by shifts wise ----------
	$scope.loadVehicleDetailsByShifts = function()
	{	
		//alert("hello welcome");
		var shiftDates = new Object();
		var today = new Date();
		var tdate=today.getDate();
		var tmonth=today.getMonth()+1;
		if(tdate<10)
		{
			tdate='0'+tdate;
		} 
		if(tmonth<10)
		{
			tmonth='0'+tmonth
		} 
		
		shiftDates.caneWeighmentDate= tdate+"-"+tmonth+"-"+today.getFullYear();
		shiftDates.caneWeighmentTime=today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
		//--------------shiftwise vehicle totals
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getTotalVehiclesByShiftAndDate.html",
				data: JSON.stringify(shiftDates),
			}).success(function(data, status) 
			{	
			//alert(JSON.stringify(data));
			var k=	data.length;
				for(var i=0;i<=k;i++)
				{
					if(data[i].shiftid==1)
					{		
						$scope.AddPermitCheckin.shiftAVehicle = data[i].vtotal;
						$scope.AddPermitCheckin.shiftAVehiclewaiting=data[i].wstotal;
					}
					else if(data[i].shiftid==2)
					{	 
						 $scope.AddPermitCheckin.shiftBVehicle = data[i].vtotal;
						 $scope.AddPermitCheckin.shiftBVehiclewaiting=data[i].wstotal;
					}
					else
					{
						 $scope.AddPermitCheckin.shiftCVehicle = data[i].vtotal;	
						 $scope.AddPermitCheckin.AllshiftTotalVehicle=data[i].stotal;
						 $scope.AddPermitCheckin.shiftCVehiclewaiting=data[i].wstotal;
						 $scope.AddPermitCheckin.AllshiftwaitingTotal=data[i].wtotal;
					}
				}
			
								
			});		
	};
	//-----------Load MaxId-------------------
	
	$scope.loadVehicleMaxId = function()
	{
		var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxIdForVehicleCheckSlno.html",
			data: vserialNumberString,
		 }).success(function(data, status) 
		 {
			 $scope.AddPermitCheckin.serialNumber = data.id;
		 });			
	};
		
	//-----------Load Season Drop Down--------
	
	$scope.loadCaneWeightSeasonNames = function()
	{		
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonSeasonString,
			}).success(function(data, status) 
			{	
				$scope.seasonNames=data;
				$scope.AddPermitCheckin.season = '2016-2017';
				$scope.dataok = data;
				
				//alert("2");
			}); 
	};
	
	//----------Load Weigh Bridge Names-------
	
	$scope.loadWeighBridgeNames = function()
	{
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddPermitCheckin.weighBridgeId = 2;
				
				
			}); 		
	};
	

	
	
	
	//---------SavepermitCheckin---------------------
	  var printSeasonSerial = new Object();
	$scope.SavepermitCheckin = function(AddPermitCheckin,form)
	{
		
		var shiftName = $scope.AddPermitCheckin.shiftId;
			var shiftID="";
			
			if(shiftName == 'B')
			{
				
				shiftID=parseInt(2);

			}
			else if(shiftName == 'A')
			{
				
				shiftID=parseInt(1);
				

			}
			else if(shiftName == 'C')
			{
				
				shiftID=parseInt(3);
				

			}
		
		if($scope.Permitcheckinform.$valid)
		{
			var SavePermitCheckin = new Object();
			$scope.AddPermitCheckin.shiftId = shiftID;
			$scope.AddPermitCheckin.vehicle = parseInt($scope.AddPermitCheckin.vehicle);
			delete AddPermitCheckin['plant'];
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/savePermitCheckInDetails.html",
					data: JSON.stringify(AddPermitCheckin),
					//data: JSON.stringify(AddPermitCheckin),
				}).success(function(response, status) 
				{
					
						
					if(response==true)
					{
						swal("Success!", 'Permit check in for weighment Successfully!', "success");
						
						$scope.AddPermitCheckin = angular.copy($scope.master);				
						form.$setPristine(true);
						$scope.loadVehicleDetailsByShifts();
						$scope.setRowAfterReset();
						$scope.loadServerDate();
						$scope.loadServerTime();
						$scope.getLoginUser();
						$scope.AddPermitCheckin.vehicle = '1';

						$scope.loadVehicleMaxId();$scope.loadCaneWeightSeasonNames();$scope.loadWeighBridgeNames();$scope.loadCaneWeighMaxId();
					}
				   else
				    {
					   sweetAlert("Oops...", "Something went wrong!", "error");
					   $('.btn-hide').attr('disabled',false);						
					}
				}); 			
		}	
	   else
	    {
			
			
			var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
						}	
			    }			
		}
	};
	 //---------Reset Form----
		   $scope.reset = function(form)		   
		   {			  
		   $('.btn-hide').attr('disabled',false);
				$scope.AddPermitCheckin = angular.copy($scope.master);				
			    form.$setPristine(true);
				
				$scope.setRowAfterReset();
				
				$scope.loadServerDate();
				$scope.loadServerTime();

		   };	
  })		
	//===============Permit CheckIn Report =============================
	
	
	.controller('permitCheckinReportController',function($scope,$http)
		{
			var shiftDates = new Object();
		var today = new Date();
		var tdate=today.getDate();
		var tmonth=today.getMonth()+1;
		if(tdate<10)
		{
			tdate='0'+tdate;
		} 
		if(tmonth<10)
		{
			tmonth='0'+tmonth
		} 
		
		shiftDates.sdate= tdate+"-"+tmonth+"-"+today.getFullYear();
		shiftDates.stime=today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
		
			$scope.loadCheckinDetails=function()
			{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getVehiclesWaitingReport.html",
			data: JSON.stringify(shiftDates),
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.permitCheckinData=data;
				
			}); 
			};
		})
	
	//===============Update Payments===============================
		.controller('updatePaymentFormController',function($scope,$http)
		{
			$index = 1;
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname ="Branch";
			var DropDownbBank= JSON.stringify(DropDownLoad);
			
			var seasonObj = new Object();
			seasonObj.dropdownname = "Season";
			var jsonSeasonString= JSON.stringify(seasonObj);
			
		
		
		
		
		$scope.loadBranchNames=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: DropDownbBank,
			}).success(function(data, status) 
			{
				
				$scope.BankNames=data;
				//Modified by DMurty on 10-02-2017
				data.push({"id":0,"branchname":"ALL"});
				
				$scope.AddupdatePayment = {'bankPayment':'0','paymentToRyot':'0'};
			}); 
		};
		
		$scope.paidFrom = [{ paidFromId: 'ICICI', paidFrom: 'ICICI' }, { paidFromId: 'SBH', paidFrom: 'SBH' }, { paidFromId: 'Company Funds', paidFrom: 'Company Funds' }];
		$scope.isDirect = [{ isDirectId: 'Yes', isDirect: 'Yes' }, { isDirectId: 'No', isDirect: 'No' }];
		
	$scope.loadCaneWeightSeasonNames = function()
	{		
		var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonSeasonString,
			}).success(function(data, status) 
			{				
				$scope.seasonNames=data;
				//$scope.AddupdatePayment.season = '2016-2017';
				//loadAccountingDates();
			}); 
	};
	
	
	$scope.loadAccountingDates=function()
	{
		///alert('1');
		var DatesLoad = new Object();
		DatesLoad.season = '2016-2017';//$scope.AddupdatePayment.season;
		var DropDownSeason= JSON.stringify(DatesLoad);
		//alert(DropDownSeason);
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getCaneAccountingPeriodsForPayment.html",
		
		data: DropDownSeason,
		
		}).success(function(data, status) 
		{	
			//alert(JSON.stringify(data));
			$scope.FieldOffNames=data;
			
		}); 
	};
	
	
	//AddupdatePayment.season,AddupdatePayment.paymentToRyot,AddupdatePayment.branch,AddupdatePayment.foCode,AddupdatePayment.bankPayment

	$scope.getPaymentDetails = function(AddupdatePayment,updatePaymentForm)
	{
		
		var DataObj = new Object();
		DataObj.season = $scope.AddupdatePayment.season;
		DataObj.paymentToRyot = parseInt($scope.AddupdatePayment.paymentToRyot);
		DataObj.branch = parseInt($scope.AddupdatePayment.branch);
		DataObj.foCode = parseInt($scope.AddupdatePayment.foCode);
		DataObj.bankPayment = parseInt($scope.AddupdatePayment.bankPayment);
		
		
		var GetDetails = JSON.stringify(DataObj);
		
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getDataForUpdatePayments.html",
		data: GetDetails,
		
		}).success(function(data, status) 
		{	
		
		
		
		if($scope.AddupdatePayment.bankPayment==0)
		{
			$("#sbgridDetails").show();
			$("#hideloan").show();
			$("#loadSbselectAll").show();
			//$scope.updatesbacPaymentArraysb=[];
			$scope.updatesbacPaymentArray = data;

			var datalength = data.length;
			
			for(var i=0;i<datalength;i++)
			{
				//Modified by DMurty on 02-01-2017
				$scope.updatesbacPaymentArray[i].paidFrom='ICICI';
				$scope.updatesbacPaymentArray[i].isDirect='Yes';
				$scope.updatesbacPaymentArray[i].chkFlag='false';
			}
			
		}
		else
		{ $("#loangridDetails").show();
		$("#hideloan").show();
		$("#loadSbselectAll").hide();
		//$scope.updatesbacPaymentArraysbLoan=[];
			$scope.updateloanAmtPaymentArray = data;
			var datalength = data.length;
			
			for(var i=0;i<datalength;i++)
			{
				$scope.updateloanAmtPaymentArray[i].paidFrom='ICICI';
				$scope.updateloanAmtPaymentArray[i].isDirect='Yes';
				$scope.updateloanAmtPaymentArray[i].chkFlag='false';
			}
		}
		
		}); 
	};
	
	//----------sahu ajax call
	
	$scope.getAccountedDate =  function(season,foCode)
	{
		var accDateObj = new Object();
		accDateObj.season = season;
		accDateObj.foCode = parseInt(foCode);
		//alert(JSON.stringify(accDateObj));
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getDateByCaneslno.html",
		
		data: JSON.stringify(accDateObj),
		
		}).success(function(data, status) 
		{	
			$scope.AddupdatePayment.repaymentDate=data[0].canedate;
			$scope.AddupdatePayment.supplyType=data[0].acctype;
			
		}); 
		
		
	};
	
	$scope.accountedAompletedRyots = function(season,caneAccSlNo,loanOrSb)
	{
		var caneaccountingRyotObj = new Object();
		if(loanOrSb==0)
		{
			var loanorSbName = "SB";
		}
		else
		{
			loanorSbName = "Loan";
		}
		
		caneaccountingRyotObj.season = season;
		caneaccountingRyotObj.loanOrSb = loanorSbName;
		caneaccountingRyotObj.caneAccSlNo = parseInt(caneAccSlNo);
		
		
		//alert(JSON.stringify(caneaccountingRyotObj));
		if(season!=null || season!='' && loanOrSb !=null || loanOrSb!='' &&  caneAccSlNo!=null && caneAccSlNo!='' )
		
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAccountedRyots.html",
			
			data: JSON.stringify(caneaccountingRyotObj),
			
			}).success(function(data, status) 
			{	
			$scope.ryotCodeData = data;
			}); 
		
		}
		
		
	};
	$scope.getAccountNumbers = function(season,ryotCode,loanOrSb,caneaccSlNo,index)
	{
		
		var index = this.$index;
		var accObj = new Object();
		
		
		if(loanOrSb==0)
		{
			var loanorSbName = "SB";
		}
		else
		{
			loanorSbName = "Loan";
		}
		
		accObj.season = season;
		accObj.ryotCode = ryotCode;
		accObj.loanOrSb = loanorSbName;
		accObj.caneAccSlNo = parseInt(caneaccSlNo);
		
		var httpRequest = $http({
		method: 'POST',
		url : "/SugarERP/getAccountNumbers.html",
		
		data: JSON.stringify(accObj),
		
		}).success(function(data, status) 
		{	
		
		$scope.sbacPopData[index].accountNum = data[0].accountNum;
		$scope.sbacPopData[index].loanamt =data[0].pendingAmt;
		
		}); 
		
		
	};
	
	

	
	

	
	
	
	
	
	//$scope.getAccountLoanNumbers = function(season,ryotCode,loanOrSb,caneaccSlNo,index)
//	{
//		
//		var index = this.$index;
//		var LoanObj = new Object();
//		
//		
//		if(loanOrSb==0)
//		{
//			var loanorSbName = "SB";
//		}
//		else
//		{
//			loanorSbName = "Loan";
//		}
//		
//		LoanObj.season = season;
//		LoanObj.ryotCode = ryotCode;
//		LoanObj.loanOrSb = loanorSbName;
//		LoanObj.caneAccSlNo = parseInt(caneaccSlNo);
//		//alert(JSON.stringify(LoanObj));
//		var httpRequest = $http({
//		method: 'POST',
//		url : "/SugarERP/getAccountNumbers.html",
//		
//		data: JSON.stringify(LoanObj),
//		
//		}).success(function(data, status) 
//		{	
//		//alert(JSON.stringify(data));
//		$scope.loanPopData[index].accountNum = data[0].accountNum;
//		$scope.loanPopData[index].totalAmount =data[0].pendingAmt;
//		}); 
//		
//		
//	};
	/*$scope.optionToggled = function(){
			
    $scope.updatesbacData.chkFlag = $scope.updatesbacPaymentArray.every(function(itm){ 
	//alert(JSON.stringify(itm));
	return itm.chkFlag;
		
	
	})
  };
  	$scope.optionToggledLoan = function(){
			
    $scope.updateloanData.chkFlag = $scope.updateloanAmtPaymentArray.every(function(itm){ 
	
	return itm.chkFlag;
		
	
	})
  };*/

  $scope.optionToggled = function(){
			
    $scope.updateloanData.chkFlag = $scope.updateloanAmtPaymentArray.every(function(itm){ 
	//alert(JSON.stringify(itm));
	return itm.chkFlag; })
  };
   $scope.toggleAll = function() {
     var toggleStatus = $scope.isAllSelected;
	 
     angular.forEach($scope.updateloanAmtPaymentArray, function(itm){ itm.chkFlag = toggleStatus; });
   
  };
   
   $scope.toggleAllSb = function() {
     var toggleStatus = $scope.isAllSelectedSb;
	 
     angular.forEach($scope.updatesbacPaymentArray, function(itm){ itm.chkFlag = toggleStatus; });
   
  };
  $scope.optionToggledSb = function(){
	   $scope.updatesbacData.chkFlag = $scope.updatesbacPaymentArray.every(function(itm){ 
	
	return itm.chkFlag; })
  };
   
 
		
		$scope.loadRyotName = function(ryotCode,id,index)
		{
			
			 var index = this.$index;
			
			 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/GetRyotNameForCode.html",
						data:ryotCode,
						}).success(function(data, status) 
						{
							
							$scope.sbacPopData[index].ryotName = data[0].ryotName;
							
							
										
						});
		};
		
		$scope.loadLoanRyotName = function(ryotCode,id,index)
		{
			
			 var index = this.$index;
			 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/GetRyotNameForCode.html",
						data:ryotCode,
						}).success(function(data, status) 
						{
							$scope.loanPopData[index].ryotName = data[0].ryotName;
						});
		};
		
	//$scope.getPopupSbContent = function(isDirect,checkbox,ryotCode,branchcode,loanamt,accountNum,paidtoRyot,paidFrom,isDirect,index)
//	{
//		//alert($scope.updatesbacPaymentArraysb.length);
//		
//		for(var i=0;i<=$scope.updatesbacPaymentArraysb.length;i++)
//		{
//			if($scope.updatesbacPaymentArraysb[i]!=null)
//			{
//				if($scope.updatesbacPaymentArraysb[i].ryotCode==ryotCode)
//				{
//					delete $scope.updatesbacPaymentArraysb[i];
//				}
//			}
//		}
//		//$scope.AddupdatePayment.mainRyot = ryotCode;
//		$scope.AddupdatePayment.amount1=paidtoRyot;
//		var indexVal = index+Number(1);
//		
//		$scope.sbacPopData=[];
//		if($scope.AddupdatePayment.bankPayment==0)
//		{
//			if(isDirect=="Yes")
//			{
//			
//				$('.popupbox_ratoon').fadeIn("slow");
//				$("#popup1").show();
//			
//			
//			 var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/GetRyotNameForCode.html",
//						data:ryotCode,
//						}).success(function(data, status) 
//						{
//							$scope.sbacPopData.push({"ryotCode":ryotCode,"ryotName":data[0].ryotName,"branchcode":branchcode,"loanamt":loanamt,"accountNum":accountNum,"paidtoRyot":paidtoRyot,"paidFrom":paidFrom,"isDirect":isDirect,"id":1});
//										
//						});
//			$("#popup2").hide();
//				
//			
//			
//		}
//	
//		}
//		else
//		{
//			$('.popupbox_ratoon').fadeIn("slow");
//			$("#popup1").hide();
//			$("#popup2").show();
//		}
//		
//	};
	
	$scope.getValidationforBifAmt = function(loanamt,bifAmt,id)
	{
		if(bifAmt >= loanamt)
		{ 
		$("#showMsg"+id).show();
			
		}
	};
	
	
	
		  
	
	$indexPop = 1;
	$scope.addFormField = function() 
			{
				$indexPop++;
		    	$scope.sbacPopData.push({'id':$indexPop})
			
				
			};
			
			$indexPopLoan = 1;
	$scope.addFormFieldLoan = function() 
			{
				$indexPopLoan++;
		    	$scope.loanPopData.push({'id':$indexPopLoan})
			
				
			};
			
			
			
			
			$scope.removeRow = function(name)
			{				
				var Index = 0;		
				var comArr = eval( $scope.sbacPopData );
				for( var i = 1; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.sbacPopData.splice( Index, 1 );
				$indexPop = comArr.length;
				for(var s=0;s<$indexPop;s++)
				{
					$scope.sbacPopData[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
			
			$scope.removeRowLoan = function(name)
			{				
				var Index = 0;		
				var comArr = eval( $scope.loanPopData );
				for( var i = 1; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.loanPopData.splice( Index, 1 );
				$indexPopLoan = comArr.length;
				for(var s=0;s<$indexPop;s++)
				{
					$scope.loanPopData[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
			
			
			$scope.SBpopupSave = function(AddupdatePayment,form)
			{
							
				var gridSaveSbData = [];
				var gridData = [];
				
				 var obj = new Object();
				 $scope.updatesbacPaymentArray.forEach(function (updatesbacData) 
					 {
						  var bifAmt = updatesbacData.bifAmt;
						var paidtoRyot = updatesbacData.paidtoRyot;
						var loanamt = updatesbacData.loanamt;
						var pendingAmt = updatesbacData.pendingAmt;
						var paidAmt = updatesbacData.paidAmt;

						delete updatesbacData['bifAmt'];
						delete updatesbacData['paidtoRyot'];
						delete updatesbacData['loanamt'];
						delete updatesbacData['pendingAmt'];
						delete updatesbacData['paidAmt'];
					  
						updatesbacData.bifAmt =  bifAmt.toString();
						updatesbacData.paidtoRyot = paidtoRyot.toString();
						updatesbacData.loanamt =   loanamt.toString();
						updatesbacData.pendingAmt =   pendingAmt.toString();
						updatesbacData.paidAmt =   paidAmt.toString();
					
					//	  alert(updatesbacData.chkFlag);
						  if(updatesbacData.chkFlag==true)
							{
	            		 		gridData.push(angular.toJson(updatesbacData));
							}
				      });
				 
				obj.season = $scope.AddupdatePayment.season;
				obj.foCode = parseInt($scope.AddupdatePayment.foCode);
				obj.branch = parseInt($scope.AddupdatePayment.branch);
				obj.paymentToRyot = parseInt($scope.AddupdatePayment.paymentToRyot);
				obj.bankPayment = parseInt($scope.AddupdatePayment.bankPayment);
				obj.systemDate = $scope.AddupdatePayment.systemDate;
				obj.acctype= parseInt($scope.AddupdatePayment.supplyType);
				  obj.gridData = gridData;
				  alert(JSON.stringify(obj));
					
				  
					
				  
				  $.ajax({
				    	url:"/SugarERP/updatePayments.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'SB Amount Charges Bifurcated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								
											$scope.AddupdatePayment = angular.copy($scope.master);
					$scope.updatesbacPaymentArray = [];
					
						//alert("dsfd");
					$("#loadSbselectAll").hide();
					$("#sbgridDetails").hide();
					$scope.AddupdatePayment = {'bankPayment':'0','paymentToRyot':'0'};
					
					form.$setPristine(true);
					//$scope.loadseasonNames();
					$('.btn-hide').attr('disabled',false);	
								
							    form.$setPristine(true);			    											   
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
			};
			
			$scope.LoanpopupSave = function()
			{
				var obj = new Object();
				 obj.season = $scope.AddupdatePayment.season;
				obj.foCode = parseInt($scope.AddupdatePayment.foCode);
				obj.branch = parseInt($scope.AddupdatePayment.branch);
				obj.paymentToRyot = parseInt($scope.AddupdatePayment.paymentToRyot);
				obj.bankPayment = parseInt($scope.AddupdatePayment.bankPayment);
				obj.systemDate = $scope.AddupdatePayment.systemDate;
				//on 22-03-2017
				obj.acctype=$scope.AddupdatePayment.supplyType;

				//
				  var gridData = [];
				 $scope.updateloanAmtPaymentArray.forEach(function (updateloanData) 
					  {
	            		
						 	var prin = updateloanData.principle;
						 	var totalAmt = updateloanData.totalAmount;
						 	var addnalInt = updateloanData.additionalInt;
						 	var bifAmt = updateloanData.bifAmt;
						 	var amount = updateloanData.amount;

							delete updateloanData['principle'];
							delete updateloanData['totalAmount'];
							delete updateloanData['additionalInt'];
							delete updateloanData['bifAmt'];
							delete updateloanData['amount'];

						
							updateloanData.principle =  prin.toString();
							updateloanData.totalAmount = totalAmt.toString();
							updateloanData.additionalInt =   addnalInt.toString();
							updateloanData.bifAmt =   bifAmt.toString();
							updateloanData.amount =   amount.toString();

							if(updateloanData.chkFlag==true)
							{
							gridData.push(angular.toJson(updateloanData));
							}		 
									
						 
				      });
				   
				   
				     obj.gridData = gridData;
					
					 alert(JSON.stringify(obj));
					 $.ajax({
				    	url:"/SugarERP/updatePayments.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Loan Amount Charges Bifurcated Successfully!', "success");
								$('.btn-hide').attr('disabled',false);
								
											$scope.AddupdatePayment = angular.copy($scope.master);
					$scope.updateloanAmtPaymentArray = [];
					
						
					$("#hideloanselectall").hide();
					$("#loangridDetails").hide();
					$("#hideloan").hide();
					$scope.AddupdatePayment = {'bankPayment':'0','paymentToRyot':'0'};
					//alert("dsfdsf");
					form.$setPristine(true);
					//$scope.loadseasonNames();
					$('.btn-hide').attr('disabled',false);
								
							    form.$setPristine(true);			    											   
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);								
							}
						}
					});
					 
					 
				  
				  
				 // alert(JSON.stringify(objLoan));
			};
	
				

	
		$scope.CalculateAdditionalInterest = function()
		{
			
		
			if($scope.AddupdatePayment.bankPayment=="1")
			{
			var ptr = 0;
					//alert($scope.AddupdatePayment.repaymentDate)
			var repaymentDate = $scope.AddupdatePayment.repaymentDate+"-";
			//var issueSplit = issueDate.split("-");
			var repaymentDateSplit = repaymentDate.split("-");
			var date2lomonth  = repaymentDateSplit[1]-1;
			var date2 = new Date(repaymentDateSplit[2], date2lomonth, repaymentDateSplit[0]);
			/*var todayDate = new Date();
			var dd = todayDate.getDate();
			var mm = todayDate.getMonth();
			var yy = todayDate.getFullYear();
			var date1 = new Date(yy, mm, dd);*/

			var todayDate = $scope.AddupdatePayment.systemDate+"-";
			var todaySplit = todayDate.split("-");
			var date1lomonth  = todaySplit[1]-1;
			var date1 = new Date(todaySplit[2], date1lomonth, todaySplit[0]);
			
			
			
			//alert(date1);
			var ActualtimeDifferenceInDays;
			var date1_unixtime = parseInt(date1.getTime()/1000);
			var date2_unixtime = parseInt(date2.getTime()/1000);
	
		//	// This is the calculated difference in seconds
			var timeDifference = date2_unixtime - date1_unixtime;
			//alert("timeDifference"+timeDifference);
			var timeDifferenceInHours = timeDifference/60/60;
			//alert("timeDifferenceinHours"+timeDifferenceInHours);
			// and finaly, in days :)
			var timeDifferenceInDays = timeDifferenceInHours/24;
			timeDifferenceInDays = 	Math.abs(timeDifferenceInDays);
			var includeEndDate = 1;
			ActualtimeDifferenceInDays = timeDifferenceInDays;
			//alert("time in Days"+ActualtimeDifferenceInDays);
			
			
			
			
					for(var s=0;s<$scope.updateloanAmtPaymentArray.length;s++)
						{         
							
							var principle = $scope.updateloanAmtPaymentArray[s].principle;
							
							var rate = $scope.updateloanAmtPaymentArray[s].intrestRate;
							ptr = principle*rate*ActualtimeDifferenceInDays;
							var ptrFul = ptr/36500;
							
							var ptr1  = Math.round(ptrFul*100)/100;
							ptr  = ptr1.toFixed(2);
							
							
							
							$scope.updateloanAmtPaymentArray[s].additionalInt = ptr;
							
								
									
						}
					
					}
			
		};

		$scope.resetSB = function()
		{
			//alert("dsfd");
			$scope.AddupdatePayment = angular.copy($scope.master);
					$scope.updatesbacPaymentArray = [];
					
						//alert("dsfd");
					$("#loadSbselectAll").hide();
					$("#sbgridDetails").hide();
					$scope.AddupdatePayment = {'bankPayment':'0','paymentToRyot':'0'};
					
					form.$setPristine(true);
					//$scope.loadseasonNames();
					$('.btn-hide').attr('disabled',false);	
		};
		
		$scope.resetLoan = function()
		{
			//alert("dsfd");
			$scope.AddupdatePayment = angular.copy($scope.master);
					$scope.updateloanAmtPaymentArray = [];
					
						
					$("#hideloanselectall").hide();
					$("#loangridDetails").hide();
					$("#hideloan").hide();
					$scope.AddupdatePayment = {'bankPayment':'0','paymentToRyot':'0'};
					//alert("dsfdsf");
					form.$setPristine(true);
					//$scope.loadseasonNames();
					$('.btn-hide').attr('disabled',false);	
		};
	
	
		})
