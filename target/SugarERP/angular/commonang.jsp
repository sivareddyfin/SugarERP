<% String contextPath = request.getContextPath(); %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<style type="text/css" title="currentStyle" media="screen">
		@import "<%=contextPath%>/angular/vendors/bower_components/animate.css/animate.min.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/angular-loading-bar/src/loading-bar.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css";
		
		@import "<%=contextPath%>/angular/css/app.min.1.css";
		@import "<%=contextPath%>/angular/css/app.min.2.css";	
		@import "<%=contextPath%>/angular/css/demo.css";
		</style>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">		
		<style type="text/css" title="currentStyle" media="screen">
			@import "<%=contextPath%>/angular/css/bootstrap-material-datetimepicker.css";
		</style>	
		
		
		
		
		        <!-- Core -->

        <script src="<%=contextPath%>/angular/vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<script src="<%=contextPath%>/angular/js/jquery-1.10.2.js"></script>
		<script src="<%=contextPath%>/angular/js/jquery-ui.js"></script>
		<script src="<%=contextPath%>/angular/js/sweetalert-dev.js"></script>
		
		<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<%=contextPath%>/angular/js/bootstrap-material-datetimepicker.js"></script>

        
        <!-- Angular -->
		<!--<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>-->
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular/angular.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-animate/angular-animate.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-resource/angular-resource.min.js"></script>
        
        <!-- Angular Modules -->
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-loading-bar/src/loading-bar.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

        <!-- Common Vendors -->
        <script src="<%=contextPath%>/angular/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/ng-table/dist/ng-table.min.js"></script>		
		
		
<!-- Using below vendors in order to avoid misloading on resolve -->
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="<%=contextPath%>/angular/vendors/bower_components/angular-nouislider/src/nouislider.min.js"></script>
        
        
        <!-- App level -->
        <script src="<%=contextPath%>/angular/js/app.js"></script>
        <script src="<%=contextPath%>/angular/js/config.js"></script>
        <script src="<%=contextPath%>/angular/js/controllers/main.js"></script>
        <script src="<%=contextPath%>/angular/js/services.js"></script>
        <script src="<%=contextPath%>/angular/js/templates.js"></script>
        <script src="<%=contextPath%>/angular/js/controllers/ui-bootstrap.js"></script>
        <script src="<%=contextPath%>/angular/js/controllers/table.js"></script>
		<script src="<%=contextPath%>/angular/js/controllers/tableok.js"></script>
		


        <!-- Template Modules -->
        <script src="<%=contextPath%>/angular/js/modules/template.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/ui.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/charts/flot.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/charts/other-charts.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/form.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/media.js"></script>
        <!--<script src="js/modules/components.js"></script>-->
        <script src="<%=contextPath%>/angular/js/modules/calendar.js"></script>
        <script src="<%=contextPath%>/angular/js/modules/demo.js"></script>
		<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>-->
		<script src="<%=contextPath%>/angular/js/jquery.geocomplete.js"></script>
		<script src="<%=contextPath%>/angular/js/logger.js"></script>
		
		<style type="text/css" title="currentStyle" media="screen">
			@import "<%=contextPath%>/angular/css/sweetalert.css";
		</style>	
		
		
</script>
</body>
</html>