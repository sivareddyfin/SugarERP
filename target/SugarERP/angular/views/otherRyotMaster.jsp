		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	

	<!--<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>-->
	<section id="main" class='bannerok'>    
<!--	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>-->

    	<section id="content" data-ng-controller="otherRyotMaster" data-ng-init="loadStatus();loadBanks();loadBranchNames();" >      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Other Ryot Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="otherRyotMaster" novalidate ng-submit="otherRyotSubmit(Addedryots,otherRyotMaster);">
					
				
					<div class="row">
						<div class="col-sm-4">						
							<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : otherRyotMaster.ryotname.$invalid && (otherRyotMaster.ryotname.$dirty || submitted)}"  >
							        <div class="fg-line">
									
									<input type="text" class="form-control autofocus" placeholder="Ryot Name" data-ng-required='true'  autofocus  maxlength="25" tabindex="1" name="ryotname" data-ng-model='Addedryots.ryotname' />														 
				                	</div>
									<p ng-show="otherRyotMaster.ryotname.$error.required && (otherRyotMaster.ryotname.$dirty || submitted)" class="help-block">Ryot Name is required</p>	     				
								</div>
					         </div>
							</div>
							</div>	
							
							<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : otherRyotMaster.village.$invalid && (otherRyotMaster.village.$dirty || submitted)}" >
							        <div class="fg-line">
									
									<input type="text" class="form-control" placeholder="City/Village" data-ng-required='true'   maxlength="25" tabindex="4" name="village" data-ng-model='Addedryots.village' />														 
				                	</div>
									<p ng-show="otherRyotMaster.village.$error.required && (otherRyotMaster.village.$dirty || submitted)" class="help-block">City/Village is required</p>	     								
								</div>
					         </div>
							</div>
							</div>	
						
						</div>
						
					<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : otherRyotMaster.ryotcode.$invalid && (otherRyotMaster.ryotcode.$dirty || submitted)}">
							        <div class="fg-line">
									
									<input type="text" class="form-control autofocus" placeholder="Ryot Code" data-ng-required='true'    maxlength="10" tabindex="2" name="ryotcode" data-ng-model='Addedryots.ryotcode' />														 
				                	</div>
									<p ng-show="otherRyotMaster.ryotcode.$error.required && (otherRyotMaster.ryotcode.$dirty || submitted)" class="help-block">RyotCode is required</p>				
								</div>
					         </div>
						</div>
						</div>	

						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : otherRyotMaster.PhoneNumber.$invalid && (otherRyotMaster.PhoneNumber.$dirty || submitted)}">
							        <div class="fg-line">
									
									<input type="text" class="form-control autofocus" placeholder="Phone Number" data-ng-pattern='/^[7890]\d{9,10}$/'   maxlength="10" tabindex="5" name="PhoneNumber" data-ng-model='Addedryots.PhoneNumber' />														 
				                	</div>
									<p ng-show="otherRyotMaster.PhoneNumber.$error.pattern && (otherRyotMaster.PhoneNumber.$dirty || submitted)" class="help-block">Enter valid PhoneNumber</p>	     
									<p ng-show="otherRyotMaster.PhoneNumber.$error.required && (otherRyotMaster.PhoneNumber.$dirty || submitted)" class="help-block">PhoneNumber is required</p>                     <p ng-show="otherRyotMaster.PhoneNumber.$error.minlength && (otherRyotMaster.PhoneNumber.$dirty || submitted)" class="help-block"> Too Short</p>								
								</div>
					         </div>
						</div>
						</div>	
					</div>
						
						<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" >
							        <div class="fg-line">
									
									<input type="text" class="form-control autofocus" placeholder="Address"    maxlength="30" tabindex="3" name="address" data-ng-model='Addedryots.address' />														 
				                	</div>
													
								</div>
					         </div>
						</div>
						</div>						
						</div>
					</div>
					
					<div class='row'>
					 <div class="col-sm-4">
				          <div class="input-group">
            				 										
								<h4 style="font-weight:lighter;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Bank Details :- </u></h4>								
			                 </div>			                    
				      </div>
					  </div>
					
				<div class="row">	
					<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper"  >
							        <div class="fg-line">
									 <select chosen class="w-100" name="bank"  data-ng-model="Addedryots.bank" data-ng-required='true'  ng-options="bank.id as bank.bankname for bank in banknames">
                                      <option value=''>Bank</option>
                                     </select>														 
				                	</div>
													
								</div>
					         </div>
						</div>
						</div>						
					</div>
					
					
					<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" >
							        <div class="fg-line">
									 <select chosen class="w-100" name="branchName"  data-ng-model="Addedryots.branchName" data-ng-required='true'  ng-options="branchName.id as branchName.branchname for branchName in branchNames" >
                                      <option value=''>Branch</option>
                                     </select>														 
				                	</div>
													
								</div>
					         </div>
						</div>
						</div>						
					</div>
					
					<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : otherRyotMaster.accountNumber.$invalid && (otherRyotMaster.accountNumber.$dirty || submitted)}" >
							        <div class="fg-line">
									
									<input type="text" class="form-control autofocus" placeholder="AccountNumber"    maxlength="16" tabindex="6" name="accountNumber" data-ng-model='Addedryots.accountNumber'  data-ng-pattern="/^[0-9\s]*$/" data-ng-minlength="7" data-ng-required='true' />														 
				                	</div>
										<p ng-show="otherRyotMaster.accountNumber.$error.required && (otherRyotMaster.accountNumber.$dirty || submitted)" class="help-block"> Account Number is required</p>
<p ng-show="otherRyotMaster.accountNumber.$error.pattern && (otherRyotMaster.accountNumber.$dirty || submitted)" class="help-block"> Invalid</p>																																																
<p ng-show="otherRyotMaster.accountNumber.$error.minlength && (otherRyotMaster.accountNumber.$dirty || submitted)" class="help-block"> Too short</p>																																																
			
								</div>
					         </div>
						</div>
						</div>						
						</div>
						
						
				</div>
				
				<div class='row'>
					<div class="col-sm-4">							
						<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								
								 <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="Addedryots.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="Addedryots.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
					         </div>
						</div>
						</div>						
						</div>
					
				</div>
				<br/>
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="reset" class="btn btn-primary" ng-click='reset(otherRyotMaster);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
					</form>
						
 					 									 
				   			
				        </div>
						
			    	</div>
					
					
							</div>
					
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
