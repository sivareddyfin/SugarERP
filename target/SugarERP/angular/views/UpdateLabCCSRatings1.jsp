	<style type="text/css">

		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.input-group.input-group-unstyled input.form-control 
		{
	    -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
	}
	.input-group-unstyled .input-group-addon 
	{
    	border-radius: 4px;
	    border: 0px;
    	background-color: transparent;
	}	
		
</style>
	
<!--<div class="col-sm-6">
				                    <div class="input-group">
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;">
					                       <i class="glyphicon glyphicon-chevron-left"></i>
					                    </span>													 
										<input type="text" class="form-control" placeholder="Sample Card" id="inputsamplecard"/>
					                    <span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;">
							               <i class="glyphicon glyphicon-chevron-right"></i>
					                    </span>	
				                  </div>
								  </div>-->
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Lab CCS Ratings [ Alternate Design ]</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="row">
				                <div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_Programno">
												<option value="0">Select Program Number</option>
												<option value="0">Program 1</option>
												<option value="0">Program 2</option>
												<option value="0">Program 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_Programno">
												<option value="0">Select Zone</option>
												<option value="0">Program 1</option>
												<option value="0">Program 2</option>
												<option value="0">Program 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_Programno">
												<option value="0">Select Circle</option>
												<option value="0">Program 1</option>
												<option value="0">Program 2</option>
												<option value="0">Program 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-3">
				                    <div class="input-group">
			        	                <div class="fg-line">
											<a href="#/CompanyAdvanceIntrest" class="btn btn-primary btn-sm m-t-10">Get Details</a>
			                	        </div>
			                    	</div>			                    
				                  </div>      
    	        			 </div><br />
							 <hr />
							 
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;">
					                       <i class="glyphicon glyphicon-chevron-left"></i>
					                    </span>													 
										<input type="text" class="form-control" placeholder="Sample Card" id="inputsamplecard"/>
					                    <span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;">
							               <i class="glyphicon glyphicon-chevron-right"></i>
					                    </span>	
				                  </div>
								  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Ryot Code [ Ryot Name ]' readonly class="form-control"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Offer Number [ Plot Number ]' readonly class="form-control"/>
			                	        </div>
			                    	</div>			                    
				                  </div>  
							</div><br /> 	
													 
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Land Village'  readonly class="form-control"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Variety' id="input_ryotcode" readonly class="form-control"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Plant Ratoon' id="input_ryotcode" readonly class="form-control"/>
			                	        </div>
			                    	</div>			                    
				                  </div>								    
							</div><br />
							
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Plant Ratoon Extent'  readonly class="form-control" id="input_ryotname"/>
			                	        </div>
			                    	</div>			                    
				                  </div>  
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Brix %' id="input_puritypercent" class="form-control" maxlength="4"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Pol %' class="form-control" id="input_ccspercent" maxlength="4"/>
			                	        </div>
			                    	</div>			                    
				                  </div>								  
							</div><br />
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Purity %' class="form-control" id="input_ccspercent" maxlength="4"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='CCS %' class="form-control" id="input_ccspercent" maxlength="4"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
							</div>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/LabCCSRatings" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/LabCCSRatings" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					
        			           			
				        </div>
			    	</div>
					
													
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
