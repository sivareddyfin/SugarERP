	<script type="text/javascript">
		$('#samplecard').focus();
	</script>
	<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateLabCCSRating" ng-init="loadSeasonNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Lab CCS Ratings</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					    <form name="UpdateLabCCSRating" ng-submit="SubmitLabCCSRating(AddedLabCSS,UpdateLabCCSRating);" novalidate>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            	            					<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : UpdateLabCCSRating.season.$invalid && (UpdateLabCCSRating.season.$dirty || Filtersubmitted)}">
        	                						<div class="fg-line">
            	           								<select chosen class="w-100" name="season" data-ng-model="AddedLabCSS.season" ng-options="season.season as season.season for season in SeasonData | orderBy:'-season':true" ng-required='true' ng-change="loadProgramNames(AddedLabCSS.season);">
																<option value=""><b>Select Season</b></option>
	 														</select>
                	        						</div>
													<p ng-show="UpdateLabCCSRating.season.$error.required && (UpdateLabCCSRating.season.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>
												</div>
                    						</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            	            					<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : UpdateLabCCSRating.program.$invalid && (UpdateLabCCSRating.program.$dirty || Filtersubmitted)}">
        	                						<div class="fg-line">
            	           								<select chosen class="w-100" name="program" data-ng-model="AddedLabCSS.program" ng-options="program.programnumber as program.programnumber for program in ProgramNames | orderBy:'-programnumber':true" ng-required='true' >
														<option value=""><b>Select Program</b></option>												
													</select>
													
                	        						</div>
													<p ng-show="UpdateLabCCSRating.program.$error.required && (UpdateLabCCSRating.program.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>
												</div>
                    						</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<button type="button" class="btn btn-primary" ng-click="getLabCCsDetails(UpdateLabCCSRating);">Get Details</button>
									
									<input type="hidden" name="sampleCardCount" ng-model="AddedLabCSS.sampleCardCount" />
								</div>
							</div>
						    <hr />
   						 	<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;" ng-click="loadNextSamples(AddedLabCSS.samplecard,'minus');" ng-if="AddedLabCSS.samplecard>'1'">
					                       <i class="glyphicon glyphicon-chevron-left"></i>
					                    </span>
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer; opacity:0.5;"  ng-if="AddedLabCSS.samplecard=='1' || AddedLabCSS.samplecard==null">
					                       <i class="glyphicon glyphicon-chevron-left"></i>
					                    </span>										
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.samplecard.$invalid && (UpdateLabCCSRating.samplecard.$dirty || submitted)}">														
											<label for="samplecard">Sample Card</label>											 
											<input type="text" class="form-control" placeholder="Sample Card" name="samplecard" data-ng-model="AddedLabCSS.samplecard" id="samplecard" with-floating-label maxlength="15"   ng-blur="spacebtw('samplecard');" style="width:210px;" data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/" />
			<p ng-show="UpdateLabCCSRating.samplecard.$error.required && (UpdateLabCCSRating.samplecard.$dirty || submitted)" class="help-block">Sample Card is required.</p>
			<p ng-show="UpdateLabCCSRating.samplecard.$error.pattern && (UpdateLabCCSRating.samplecard.$dirty || submitted)" class="help-block">Enter Valid Sample Card.</p>
										</div>
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:10px; background-color: #999999; cursor: pointer;" ng-click="loadSampleCards(AddedLabCSS.samplecard);">
					                       <i class="glyphicon glyphicon-search"></i>
					                    </span>											
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;opacity:0.5;" ng-if="AddedLabCSS.samplecard==AddedLabCSS.sampleCardCount">
					                       <i class="glyphicon glyphicon-chevron-right"></i>
					                    </span>											
										<span class="input-group-addon hoverok" style="color: #FFFFFF; font-size:12px; background-color:#0099FF; cursor: pointer;" ng-click="loadNextSamples(AddedLabCSS.samplecard,'plus',form);" ng-if="AddedLabCSS.samplecard<AddedLabCSS.sampleCardCount || AddedLabCSS.samplecard==null">
					                       <i class="glyphicon glyphicon-chevron-right"></i>
					                    </span>											
										
				                  	</div>
								  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										    <div class="form-group floating-label-wrapper">
			        	                		<div class="fg-line">
												  <label for="ryotcode">Ryot Code</label>
												<input type="text" placeholder='Ryot Code' readonly class="form-control" name="ryotCode" data-ng-model="AddedLabCSS.ryotCode" id="ryotcode" with-floating-label />
												</div>
			                	        	</div>
			                    		</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										      <div class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
													<label for="ryotname">Ryot Name</label>
											<input type="text" placeholder='Ryot Name'  readonly class="form-control" name="ryotName" data-ng-model="AddedLabCSS.ryotName" id="ryotname" with-floating-label />
			                	        	</div>
										</div>
			                    	</div>			                    
				                </div>  
							</div> 	
													 
						 	<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										      <div class="form-group floating-label-wrapper">
			        	                		<div class="fg-line">
												<label for="agreementnumber">Agreement Number</label>
											<input type="text" placeholder='Agreement Number' readonly class="form-control" name="agreementNumber" data-ng-model="AddedLabCSS.agreementNumber" id="agreementnumber" with-floating-label />
			                	        	</div>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										 	<div class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
														<label for="plotnumber">Plot Number</label>
														<input type="text" placeholder='Plot Number' readonly class="form-control" name="plotNumber" data-ng-model="AddedLabCSS.plotNumber" id="plotnumber" with-floating-label />
											</div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
			        	                		<div class="fg-line">
												<label for="landvillage">Land Village</label>
											<input type="text" placeholder='Land Village'  readonly class="form-control" name="village" data-ng-model="AddedLabCSS.village"  id="landvillage" with-floating-label />
											</div>
			                	        </div>
			                    	</div>			                    
				                  </div>  
							</div>
							
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										    <div class="form-group floating-label-wrapper">
			        	                        <div class="fg-line">
												<label for="variety">Variety</label>
											<input type="text" placeholder='Variety' readonly class="form-control" name="variety" data-ng-model="AddedLabCSS.variety"  id="variety" with-floating-label />
			                	       		 </div>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
													<label for="plantratoon">Plant/Ratoon</label>
											<input type="text" placeholder='Plant/Ratoon'  readonly class="form-control" name="plantOrRatoon" data-ng-model="AddedLabCSS.plantOrRatoon"  id="plantratoon" with-floating-label />
			                	        	</div>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										 	<div class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
													<label for="plantratoonextent">Plant/Ratoon Extent</label>
											<input type="text" placeholder='Plant/Ratoon Extent'  readonly class="form-control" name="extentSize" data-ng-model="AddedLabCSS.extentSize"  id="plantratoonextent" with-floating-label />
			                	       		 </div>
										</div>
			                    	</div>			                    
				                  </div>  
							</div>
							
							
						 	<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.brix.$invalid && (UpdateLabCCSRating.brix.$dirty || submitted)}">
			        	                		<div class="fg-line">
												<label for="brix">Brix</label>
											<input type="text" placeholder='Brix'  class="form-control" maxlength="5" name="brix" data-ng-model="AddedLabCSS.brix"  id="brix" with-floating-label data-ng-required='true'  ng-blur="spacebtw('brix');" data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="calculatePurity(AddedLabCSS.pol,AddedLabCSS.brix);" autofocus tabindex="1"/>
			                	        	</div>
				<p ng-show="UpdateLabCCSRating.brix.$error.required && (UpdateLabCCSRating.brix.$dirty || submitted)" class="help-block">Brix % is required</p>
				<p ng-show="UpdateLabCCSRating.brix.$error.pattern && (UpdateLabCCSRating.brix.$dirty || submitted)" class="help-block">Enter Valid Brix %.</p>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" >
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.pol.$invalid && (UpdateLabCCSRating.pol.$dirty || submitted)}">											
			        	               				 <div class="fg-line">
														 <label for="pol">Pol</label>
														<input type="text" placeholder='Pol' class="form-control"  maxlength="5" name="pol" data-ng-model="AddedLabCSS.pol"  id="pol" with-floating-label  ng-blur="spacebtw('pol');loadNextSamples(AddedLabCSS.samplecard,'plus',form);" data-ng-required="true"  data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="calculatePurity(AddedLabCSS.pol,AddedLabCSS.brix);" tabindex="2"  />
			        		        	        	</div>
				<p ng-show="UpdateLabCCSRating.pol.$error.required && (UpdateLabCCSRating.pol.$dirty || submitted)" class="help-block">Pol % is required</p>
				<p ng-show="UpdateLabCCSRating.pol.$error.pattern && (UpdateLabCCSRating.pol.$dirty || submitted)" class="help-block">Enter Valid Pol %.</p>
												</div>
											</div>
			                    	</div>			                    
					            </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.purity.$invalid && (UpdateLabCCSRating.purity.$dirty || submitted)}">	
			        	                			<div class="fg-line">
														<label for="purity">Purity</label>
														<input type="text" placeholder='Purity' class="form-control"   name="purity" data-ng-model="AddedLabCSS.purity"  id="purity" with-floating-label  ng-blur="spacebtw('purity');" data-ng-required="true"  data-ng-pattern="/^[0-9.\s]*$/" readonly="readonly"/>
				    	            	       		 </div>	
				<p ng-show="UpdateLabCCSRating.purity.$error.required && (UpdateLabCCSRating.purity.$dirty || submitted)" class="help-block">Purity % is required</p>
				<p ng-show="UpdateLabCCSRating.purity.$error.pattern && (UpdateLabCCSRating.purity.$dirty || submitted)" class="help-block">Enter Valid Purity %.</p>													 											
												</div>
											</div>
			                    	</div>			                    
				                  </div>
							</div>
							
						 	<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										    <div class="form-group floating-label-wrapper">
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.ccsRating.$invalid && (UpdateLabCCSRating.ccsRating.$dirty || submitted)}">
			        	               			 	<div class="fg-line">
														 <label for="ccs">CCS %</label>
														<input type="text" placeholder='CCS %' class="form-control"  name="ccsRating" data-ng-model="AddedLabCSS.ccsRating"  id="ccs" with-floating-label  ng-blur="spacebtw('ccsRating');" data-ng-required="true" data-ng-pattern="/^[0-9.\s]*$/" readonly="readonly"/>
			        		        	        	</div>
				<p ng-show="UpdateLabCCSRating.ccsRating.$error.required && (UpdateLabCCSRating.ccsRating.$dirty || submitted)" class="help-block">CCS % is required</p>
				<p ng-show="UpdateLabCCSRating.ccsRating.$error.pattern && (UpdateLabCCSRating.ccsRating.$dirty || submitted)" class="help-block">Enter Valid CCS %.</p>
												</div>
										   </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										    <div class="form-group floating-label-wrapper">
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateLabCCSRating.sampleDate.$invalid && (UpdateLabCCSRating.sampleDate.$dirty || submitted)}">
			        	               			 	<div class="fg-line">
														 <label for="sampleDate">Sample Date</label>
														<input type="text" placeholder='Sample Date' class="form-control datepicker"  name="sampleDate" data-ng-model="AddedLabCSS.sampleDate"  id="sampleDate" with-floating-label  ng-blur="spacebtw('sampleDate');" data-ng-required="true"  readonly="readonly"/>
			        		        	        	</div>
				<p ng-show="UpdateLabCCSRating.sampleDate.$error.required && (UpdateLabCCSRating.sampleDate.$dirty || submitted)" class="help-block">Sample Date is required</p>
				<p ng-show="UpdateLabCCSRating.sampleDate.$error.pattern && (UpdateLabCCSRating.sampleDate.$dirty || submitted)" class="help-block">Enter Sample Date</p>
												</div>
										   </div>
			                    	</div>			                    
				                  </div>
							</div>
						 	<br  />
						 	<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(UpdateLabCCSRating)">Reset</button>
											</div>
										</div>						 	
							 		</div>												
						</form>   			
				     </div>
			    </div>
			 </div>
	     </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
