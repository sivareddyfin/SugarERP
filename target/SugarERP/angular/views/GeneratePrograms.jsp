		<script type="text/javascript">		
		$('#Programnumber').focus();		
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GeneratePrograms"  ng-init="loadProgramCode();loadVarietyNames();loadPeriodNames();loadSeasonNames();loadmonthNames();addFormField();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Programs</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="GenerateProgramForm" ng-submit="SaveProgramDetails(AddedGenerateProgram,GenerateProgramForm);" novalidate>					
    	        			<div class="row">
								<div class="col-sm-4">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : GenerateProgramForm.season.$invalid && (GenerateProgramForm.season.$dirty || Filtersubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 								
					
					<select chosen class="w-100" name="season" data-ng-model="AddedGenerateProgram.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" ng-required='true' ng-change="getGenerateProgramNumber(AddedGenerateProgram.season); getAddedProgramNumber(AddedGenerateProgram.season);" >
<option value="">Season</option>

                    </select>
                	            	   </div>
                	        </div>
							<p ng-show="GenerateProgramForm.season.$error.required && (GenerateProgramForm.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
			
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateProgramForm.programnumber.$invalid && (GenerateProgramForm.programnumber.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
											<div class="select">
                	     	 								
					
					<select chosen class="w-100" name="programnumber"  data-ng-model="AddedGenerateProgram.programnumber" ng-options="programnumber.programnumber as programnumber.programnumber for programnumber in programnumberNames | orderBy:'-programnumber':true"  ng-change="getModifyGridDetails(AddedGenerateProgram.season,AddedGenerateProgram.programnumber,AddedGenerateProgram);">
<option value="">Added Program No.</option>

                    </select>
                	            	   </div>
			                	        </div>
										<p ng-show="GenerateProgramForm.programnumber.$error.required && (GenerateProgramForm.programnumber.$dirty || Filtersubmitted)" class="help-block">Added Program Number Code is required</p>
										</div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateProgramForm.programNumber.$invalid && (GenerateProgramForm.programNumber.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
										<label for="Programnumber">Program No.</label>
            					          <input type="text" class="form-control autofocus" placeholder="Program No." name="programNumber"  data-ng-model="AddedGenerateProgram.programNumber" autofocus id="Programnumber" with-floating-label  ng-required="onblurprog" readonly="readonly"/>
			                	        </div>
										<p ng-show="GenerateProgramForm.programNumber.$error.required && (GenerateProgramForm.programNumber.$dirty || Filtersubmitted)" class="help-block">Program Code is required</p>
										</div>
			                    	</div>			                    
				                  </div>								  
								<input type="hidden" data-ng-model="AddedGenerateProgram.modifyFlag"  />
								  
								  
							</div><br />
					<div ng-repeat='ProgramData in data'>
							<div class="row"  >
							
							<div class="col-sm-5">
							
				                    <div class="input-group">
									<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="ProgramData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(Program.id);' ng-if="ProgramData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>

            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : GenerateProgramForm.varietyOfCane.$invalid && (GenerateProgramForm.varietyOfCane.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
										  
                				    		<select chosen class="w-100" multiple="multiple"  data-placeholder="Variety" name="varietyOfCane{{ProgramData.id}}" data-ng-model='ProgramData.varietyOfCane' ng-options="varietyOfCane.id as varietyOfCane.variety for varietyOfCane in varietyNames | orderBy:'-variety':true">
												<option value="">Variety</option>
						                       											
				        		            </select>
                	            		  </div>
			                	        </div>
										<p ng-show="GenerateProgramForm.varietyOfCane.$error.required && (GenerateProgramForm.varietyOfCane.$dirty || Filtersubmitted)" class="help-block">Select Variety </p>
										</div>
										
			                    	</div>			                    
				                  </div>
							<div class="col-sm-3">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : GenerateProgramForm.periodId.$invalid && (GenerateProgramForm.periodId.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"  tabindex="2"  data-ng-model='ProgramData.periodId' name="periodId{{ProgramData.id}}" ng-options="periodId.id as periodId.period for periodId in periodNames">
															<option value="" selected="selected">Period</option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="GenerateProgramForm.periodId.$error.required && (GenerateProgramForm.periodId.$dirty || Filtersubmitted)" class="help-block"> Select Period</p>
										</div>
			                    	</div>			                    
				                  </div>
							<div class="col-sm-2">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : GenerateProgramForm.monthId.$invalid && (GenerateProgramForm.monthId.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"    data-ng-model='ProgramData.monthId' name="monthId{{ProgramData.id}}" ng-options="monthId.id as monthId.month for monthId in monthNames">
															<option value="" selected="selected">Month </option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="GenerateProgramForm.month.$error.required && (GenerateProgramForm.month.$dirty || Filtersubmitted)" class="help-block"> Select Month</p>
										</div>
			                    	</div>			                    
				                  </div>
								
							<div class="col-sm-2">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : GenerateProgramForm.year.$invalid && (GenerateProgramForm.year.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"    data-ng-model='ProgramData.yearOfPlanting' name="year{{ProgramData.id}}" ng-options="year.yearId as year.year for year in year">
															<option value="">Year</option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="GenerateProgramForm.year.$error.required && (GenerateProgramForm.year.$dirty || Filtersubmitted)" class="help-block"> Select Year</p>
										</div>
			                    	</div>			                    
				                  </div>
							
								  	
								   
    	        			 </div>
							 <div class="row">
							 	<div class="col-sm-4">
				                    <div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon" title="Extent Type">Extent:</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="typeOfExtent{{ProgramData.id}}" data-ng-model='ProgramData.typeOfExtent' value="0">
			    			            	<i class="input-helper"></i>Plant
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="typeOfExtent{{ProgramData.id}}" data-ng-model='ProgramData.typeOfExtent' value="1">
			    				            <i class="input-helper"></i>Ratoon
					  		              </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="typeOfExtent{{ProgramData.id}}" data-ng-model='ProgramData.typeOfExtent' value="2">
			    				            <i class="input-helper"></i>Both
					  		              </label>										  
			                	        </div>
			                    	</div>			                    
				                  </div>
							 </div>
						</div>
							 
							 <div class="row">
							 
							 
							
							
									
								  <input type="hidden" name="datecount" ng-model="datecount" />
								   <div class="col-sm-2">
								  	<div class="input-group">
										<div class="fg-line">
										<button type="button" class="btn btn-primary" ng-click="applyProgram(GenerateProgramForm);">Get Details </button>
											
										</div>
									</div>
									
								  </div>
								   <div class="col-sm-2">
								  	<div class="input-group">
										<div class="fg-line">
										<button type="button" class="btn btn-primary" ng-click="getProgramNumber();">Apply Program </button>
											
										</div>
									</div>
								  </div>
							 </div>													
						
							<!-- </form>-->
							<br /><br />
							<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
										<!--<form name="GenerateProgramform" novalidate >	-->			
							        <table ng-table="GeneratePrograms.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>Ryot<br /> Code</span></th>
													<th><span>Ryot<br /> Name</span></th>
													<th><span>Agreement<br />No.</span></th>
													<th><span>Plot<br /> No.<br /></span></th>
													<th><span>plant<br /> Acres</span></th>
													<th><span>Ratoon<br /> Acres</span></th>
													<th><span>Program</span></th>													
												</tr>
												
											</thead>
										<tbody>
								        <tr ng-repeat="generateProgram in generateprogramData"  ng-class="{ 'active': generateProgram.$edit }">
                		    				<td>
											<input type="text" class="form-control1" ng-model="generateProgram.ryotCode" name="ryotCode{{$index}}" readonly/>
					            		      </td>
		                    				  
							                  <td>
											  <input type="text" class="form-control1" ng-model="generateProgram.ryotName" name="ryotName{{$index}}" readonly/>
							                  </td>
											  <td>
											  	  <input type="text" class="form-control1" ng-model="generateProgram.agreementNo" name="agreementNumber{{$index}}" readonly/>
							                    
							                  </td>
											  <td>
											  	  <input type="text" class="form-control1" ng-model="generateProgram.plotNo" name="plotNo{{$index}}" readonly/>
							                  </td>
											  <td>
											  	  <input type="text" class="form-control1" ng-model="generateProgram.plantAcres" name="plantAcres{{$index}}" readonly/>
							                   </td>
											   <td>
											   	  <input type="text" class="form-control1"  ng-model="generateProgram.ratoonAcres" name="ratoonAcres{{$index}}" readonly/>
							                  </td>
											  <td style="width:200px;">
												<div class="input-group input-group-unstyled">
                   									<input type="text" class="form-control"  size="5" placeholder="Program"  ng-model="generateProgram.programNumber" placeholder='Program' maxlength="50" name="program{{$index}}" readonly="readonly"/>
								                </div>												
											</td>
											  											  
							             </tr>
										 </tbody>
								         </table>
										 </div>
										</section>  
																 
							     </div>
							 <hr />
							 <div class="row">
							 	<div class="col-md-4">
									<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateProgramForm.totalPlant.$invalid && (GenerateProgramForm.totalPlant.$dirty )}">
			        	                <div class="fg-line">
										<label for="totalplant">Total Plant Acres</label>
										<input type="text" class="form-control"  readonly placeholder="Total Plant Acres" name="totalPlant"  data-ng-model="AddedGenerateProgram.totalPlant"  maxlength="13" id="totalplant" with-floating-label ng-required="true"/>
										</div>
										<p ng-show="GenerateProgramForm.totalPlant.$error.required && (GenerateProgramForm.totalPlant.$dirty)" class="help-block"> Total Plant Acres Required</p>
			                	        </div>
			                    	</div>
								</div>
								<div class="col-md-4">
								    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateProgramForm.totalRatoon.$invalid && (GenerateProgramForm.totalRatoon.$dirty )}">
			        	                <div class="fg-line">
										<label for="totalratoon">Total Ratoon Acres</label>
            					          <input type="text" class="form-control" placeholder="Total Ratoon Acres" name="totalRatoon" data-ng-model="AddedGenerateProgram.totalRatoon"  maxlength="13" id="totalratoon" with-floating-label  ng-required="true" readonly/>
										  </div>
										  <p ng-show="GenerateProgramForm.totalRatoon.$error.required && (GenerateProgramForm.totalRatoon.$dirty)" class="help-block"> Total Ratoon Acres Required</p>
			                	        </div>
			                    	</div>
								</div>
								<div class="col-md-4">
								    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateProgramForm.totalExtent.$invalid && (GenerateProgramForm.totalExtent.$dirty )}">
			        	                <div class="fg-line">
										<label for="totalacres">Total Acres</label>
            					          <input type="text" class="form-control" placeholder="Total Acres"  maxlength="13" name="totalExtent" data-ng-model="AddedGenerateProgram.totalExtent" id="totalacres" with-floating-label  ng-required="true" readonly/>
										  </div>
										  <p ng-show="GenerateProgramForm.totalExtent.$error.required && (GenerateProgramForm.totalExtent.$dirty)" class="help-block"> Total Acres Required</p>
			                	        </div>
			                    	</div>
								</div>
							 </div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									<br />
										<button type="button" class="btn btn-primary btn-hide" ng-click="SaveProgramData(GenerateProgramForm);">Save</button>
											<button type="reset" class="btn btn-primary" ng-click="reset(GenerateProgramForm);">Reset</button>
									</div>
								</div>						 	
							 </div>
							 </form>							 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->
					 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section>  
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
