	<style>
.styletr {border-bottom: 1px solid grey;}
#styletr:nth-child(odd) {
    background: #E9E9E9;
}

#styletr:nth-child(even) {
    background: #FFFFFF;
}
.changeFontColor
{
color:#FFFFFF;
}
</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	 // $(function() 
//	  {
//	     $( ".date" ).datepicker({
//    		  changeMonth: true,
//		      changeYear: true,
//			  dateFormat: 'dd-mm-yy'
//	     });	
//		  $( "#date" ).datepicker({
//    		  changeMonth: true,
//		      changeYear: true,
//			  dateFormat: 'dd-mm-yy'
//	     });		 	
//		 	 		 
//		 $('#Time').timepicker({ 'scrollDefault': 'now' });
//		$("th").addClass("changeFontColor"); 
//		
//	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SaleInvoice" ng-init="addFormField();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Consumption</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 <div class="row">
					 	<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group">
									<div class="fg-line">
										<select  chosen class="w-100">
											<option value="">Added Consumptions</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					 </div>
					 <br />
					 	<form name="consumptionForm" novalidate >
							<div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : consumptionForm.consumptionNo.$invalid && (consumptionForm.consumptionNo.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="consumptionNo" data-ng-model="AddedConsumption.consumptionNo" placeholder="Consumption No." tabindex="1"  maxlength="15" ng-disabled='true' />
									</div>
								<p ng-show="consumptionForm.consumptionNo.$error.required && (consumptionForm.consumptionNo.$dirty || Addsubmitted)" class="help-block">Consumption No. is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
													<input type="text" class="form-control date" tabindex="2" name="consumptionDate" ng-required='true' data-ng-model="AddedConsumption.consumptionDate" placeholder="Date" maxlength="10" data-input-mask="{mask: '00-00-0000'}"/>
												</div>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : consumptionForm.department.$invalid && (consumptionForm.department.$dirty || submitted)}">
											 	<div class="fg-line">
													<select chosen class="w-100" ng-required='true' data-ng-model="AddedConsumption.department" name="department" tabindex="3" >
														<option value="">Department</option>
													</select>
													
												</div>
												<p ng-show="consumptionForm.department.$error.required && (consumptionForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 
											 </div>
										</div>
									</div>
								</div>															
							</div>
							
							
							
								
						 </div>
							<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{'has-error':consumptionForm.reason.$invalid && (consumptionForm.reason.$dirty || submitted )}">
													<div class="fg-line">
														<input type="text" class="form-control" data-ng-model="AddedConsumption.reason" name="reason" tabindex="4" placeholder='Reason'  ng-required='true' />
													</div>
													<p ng-show='consumptionForm.reason.$error.required &&(consumptionForm.reason.$dirty || submitted)' class="help-block">Reason is required</p>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
								</div>
								
							</div>
						 
						 <br />
							<div class="table-responsive">
							 <table style="width:100%;"  style="border-collapse:collapse;" class="table table-stripped">
								<tr class="styletr" style="font-weight:bold;background-color:#2298f2;">
									<th>Action</th>
									<th>Product Code</th>
									<th >Product Name</th>
									<th>UOM</th>
									<th>Batch</th>
									<th>Exp Dt</th>
									<th>MRP</th>
									<th>Stock In Date</th>
									<th >Quantity</th>
								</tr>
								<tr id="styletr"   ng-repeat="stockData in stockRequestData" style="height:50px;">
									<td><button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();'ng-if="stockData.id=='1'" ><i class="glyphicon glyphicon-plus-sign"></i>
										<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(stockData.id);'ng-if="stockData.id!='1'" ><i class="glyphicon glyphicon-remove-sign"></i>
										</td>
									
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.productCode{{$index}}.$invalid && (consumptionForm.productCode{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Code" maxlength="20" name="productCode{{$index}}" ng-model="stockData.productCode" ng-required="true" ng-pattern="/^[a-zA-Z0-9?=.*!@#$%^&*_\-\s]+$/" readonly>							
										<p ng-show="consumptionForm.productCode{{$index}}.$error.required && (consumptionForm.productCode{{$index}}.$dirty || submitted)" class="help-block">Required</p>
										<p ng-show="consumptionForm.productCode{{$index}}.$error.pattern && (consumptionForm.productCode{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.productName{{$index}}.$invalid && (consumptionForm.productName{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Name" maxlength="20" name="productName{{$index}}" ng-model="stockData.productName" ng-required="true" readonly>							
										<p ng-show="consumptionForm.productName{{$index}}.$error.required && (consumptionForm.productName{{$index}}.$dirty || submitted)" class="help-block">Required</p>	
										<p ng-show="consumptionForm.productName{{$index}}.$error.pattern && (consumptionForm.productName{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.uom{{$index}}.$invalid && (consumptionForm.uom{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="UOM" maxlength="10" name="uom{{$index}}" ng-model="stockData.uom" ng-required="true" readonly>							
										<p ng-show="consumptionForm.uom{{$index}}.$error.required && (consumptionForm.uom{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
								
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.batch{{$index}}.$invalid && (consumptionForm.batch{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Batch" maxlength="10" name="batch{{$index}}" ng-model="stockData.batch" ng-required="false" readonly>							
										<p ng-show="consumptionForm.batch{{$index}}.$error.required && (consumptionForm.batch{{$index}}.$dirty || submitted)" class="help-block">Required</p>							
										<p ng-show="consumptionForm.batch{{$index}}.$error.pattern && (consumptionForm.batch{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.expDate{{$index}}.$invalid && (consumptionForm.expDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Exp Dt" maxlength="10" name="expDate{{$index}}" ng-model="stockData.expDate" ng-required="false" readonly>							
										<p ng-show="consumptionForm.expDate{{$index}}.$error.required && (consumptionForm.expDate{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.mrp{{$index}}.$invalid && (consumptionForm.mrp{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="MRP" maxlength="10" name="mrp{{$index}}" ng-model="stockData.mrp" ng-required="false" readonly>							
										<p ng-show="consumptionForm.mrp{{$index}}.$error.required && (consumptionForm.mrp{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.stockInDate{{$index}}.$invalid && (consumptionForm.stockInDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="StockInDate" maxlength="10" name="stockInDate{{$index}}" ng-model="stockData.StockInDate" ng-required="false" readonly>							
										<p ng-show="consumptionForm.stockInDate{{$index}}.$error.required && (consumptionForm.stockInDate{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.qty{{$index}}.$invalid && (consumptionForm.qty{{$index}}.$dirty || submitted)}">
										<input type="number" class="form-control1" placeholder="Cause" maxlength="50" name="qty{{$index}}" ng-model="stockData.qty" ng-required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" readonly>							
										<p ng-show="consumptionForm.qty{{$index}}.$error.required && (consumptionForm.qty{{$index}}.$dirty || submitted)" class="help-block">Required</p>							
										<p ng-show="consumptionForm.qty{{$index}}.$error.pattern && (consumptionForm.qty{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									

									
									
									
									
									
									
								</tr>
							</table>
							
							</div>
							
						 <br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(consumptionForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>
						
						
						<form name="GridPopup" novalidate style="display:;">
					   <div class="card popupbox_ratoon" id="gridpopup_box">						  
						  	  						
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
								   <table style="width:100%;"  style="border-collapse:collapse;" id="tab_logic">
								
							 		
							 			<thead>
								        	<tr class="styletr" style="font-weight:bold;background-color:#FFFFFF;">
											<th>Action</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>UOM</th>
									
									<th>Batch</th>
									<th>Exp Dt</th>
									<th>MRP</th>
									<th>Stock In Date</th>
									<th>Quantity</th>
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData" style="height:50px;">
									<td><button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();'ng-if="stockData.id=='1'" ><i class="glyphicon glyphicon-plus-sign"></i>
										<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(stockData.id);'ng-if="stockData.id!='1'" ><i class="glyphicon glyphicon-remove-sign"></i>
										</td>
									
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.productCode{{$index}}.$invalid && (consumptionForm.productCode{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Code" maxlength="10" name="productCode{{$index}}" ng-model="stockData.productCode" ng-required="true" readonly>							
										<p ng-show="consumptionForm.productCode{{$index}}.$error.required && (consumptionForm.productCode{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.productName{{$index}}.$invalid && (consumptionForm.productName{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Name" maxlength="10" name="productName{{$index}}" ng-model="stockData.productName" ng-required="true" readonly>							
										<p ng-show="consumptionForm.productName{{$index}}.$error.required && (consumptionForm.productName{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.uom{{$index}}.$invalid && (consumptionForm.uom{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="UOM" maxlength="10" name="uom{{$index}}" ng-model="stockData.uom" ng-required="true" readonly>							
										<p ng-show="consumptionForm.uom{{$index}}.$error.required && (consumptionForm.uom{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.issuedQty{{$index}}.$invalid && (consumptionForm.issuedQty{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Issued Qty" maxlength="10" name="issuedQty{{$index}}" ng-model="stockData.issuedQty" ng-required="true" readonly>							
										<p ng-show="consumptionForm.issuedQty{{$index}}.$error.required && (consumptionForm.issuedQty{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.acceptedQty{{$index}}.$invalid && (consumptionForm.acceptedQty{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Accepted Qty" maxlength="10" name="acceptedQty{{$index}}" ng-model="stockData.acceptedQty" ng-required="true" readonly>							
										<p ng-show="consumptionForm.acceptedQty{{$index}}.$error.required && (consumptionForm.acceptedQty{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.rejectedQty{{$index}}.$invalid && (consumptionForm.rejectedQty{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Rejected Qty" maxlength="10" name="rejectedQty{{$index}}" ng-model="stockData.rejectedQty" ng-required="true" readonly>							
										<p ng-show="consumptionForm.rejectedQty{{$index}}.$error.required && (consumptionForm.rejectedQty{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.batch{{$index}}.$invalid && (consumptionForm.batch{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Batch" maxlength="10" name="batch{{$index}}" ng-model="stockData.batch" ng-required="true">							
										<p ng-show="consumptionForm.batch{{$index}}.$error.required && (consumptionForm.batch{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.expDate{{$index}}.$invalid && (consumptionForm.expDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Exp Dt" maxlength="10" name="expDate{{$index}}" ng-model="stockData.expDate" ng-required="true" readonly>							
										<p ng-show="consumptionForm.expDate{{$index}}.$error.required && (consumptionForm.expDate{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.mrp{{$index}}.$invalid && (consumptionForm.mrp{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="MRP" maxlength="10" name="mrp{{$index}}" ng-model="stockData.mrp" ng-required="true">							
										<p ng-show="consumptionForm.mrp{{$index}}.$error.required && (consumptionForm.mrp{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.stockInDate{{$index}}.$invalid && (consumptionForm.stockInDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="StockInDate" maxlength="10" name="stockInDate{{$index}}" ng-model="stockData.stockInDate" ng-required="true">							
										<p ng-show="consumptionForm.stockInDate{{$index}}.$error.required && (consumptionForm.stockInDate{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':consumptionForm.qty{{$index}}.$invalid && (consumptionForm.qty{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Quantity" maxlength="50" name="qty{{$index}}" ng-model="stockData.qty" ng-required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/">							
										<p ng-show="consumptionForm.qty{{$index}}.$error.required && (consumptionForm.qty{{$index}}.$dirty || submitted)" class="help-block">Required</p>							
										<p ng-show="consumptionForm.qty{{$index}}.$error.pattern && (consumptionForm.qty{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>									
										</div>
									</td>
									

									
									
									
									
									
									
								</tr>
								</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="closeGRNDetails();">Close</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
