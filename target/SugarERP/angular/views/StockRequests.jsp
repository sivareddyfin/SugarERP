	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="StockRequestsController" ng-init="getAllDepartments();userLoginDept();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Requests</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StockRequestForm" novalidate ng-submit="stockRequestSubmit(AddedStockRequest,StockRequestForm);">

							<div class="row" style="display:none;">
							<div class="col-sm-3">
							 <select chosen class="w-100" name="userDept" tabindex="3"   data-ng-model="AddedStockRequest.userDept" ng-options="userDept.deptCode as userDept.department for userDept  in DepartmentsData  | orderBy:'-deptCode':true" ng-required='true'>
							<option value=''>Department</option>
							 </select>	</div>
							</div>

						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockRequestForm.fromDate.$invalid && (StockRequestForm.fromDate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date " data-ng-required='true'  name="fromDate" data-ng-model="AddedStockRequest.fromDate" placeholder="From Date" tabindex="1"  maxlength="15" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  id="fromDate" with-floating-label/>
									</div>
								<p ng-show="StockRequestForm.fromDate.$error.required && (StockRequestForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 
								<p ng-show="StockRequestForm.fromDate.$error.pattern && (StockRequestForm.fromDate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
											 
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockRequestForm.todate.$invalid && (StockRequestForm.todate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="todate">To Date</label>
										<input type="text" class="form-control date " data-ng-required='true'  name="todate" data-ng-model="AddedStockRequest.todate" placeholder="To Date" tabindex="1"  maxlength="15" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  id="todate" with-floating-label/>
									</div>
								<p ng-show="StockRequestForm.todate.$error.required && (StockRequestForm.todate.$dirty || Addsubmitted)" class="help-block">To Date is required</p>		 
								<p ng-show="StockRequestForm.todate.$error.pattern && (StockRequestForm.todate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
								</div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : StockRequestForm.addDepartment.$invalid && (StockRequestForm.addDepartment.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100" name="addDepartment" tabindex="3"   data-ng-model="AddedStockRequest.addDepartment" ng-options="addedDepartment.deptCode as addedDepartment.department for addedDepartment  in DepartmentsData  | orderBy:'-deptCode':true" ng-required='true'>
														<option value=''>Department</option>
													 </select>	
												</div>
												<p ng-show="StockRequestForm.addDepartment.$error.required && (StockRequestForm.addDepartment.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton" ng-click='getStockRequestDetails(AddedStockRequest.fromDate,AddedStockRequest.todate,AddedStockRequest.addDepartment);'>Get Details</button>
											 	
												
											 
										</div>
									</div>
								</div>
								
							
							</div>
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive' id='hidetable' style="display:none;">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>&nbsp;&nbsp; &nbsp;&nbsp; Modify</th>
									
									<th>Stock Request</th>
									<th>Date Of Request</th>
									<th>Requested Items</th>
									<th>Department</th>
									<th>View</th>
									<th>&nbsp;&nbsp; &nbsp;Issue</th>
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData" ng-if="stockData.status=='0'" style="background-color:orange;">
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										<a  href="#/store/transaction/StockIndent"  >
										 <img ng-hide="AddedStockRequest.userDept!=stockData.deptid" src="../icons/edit.png" style="height:30px;" ng-click="UpdateStockRequest(stockData.stockReqNo);" />
										</a>
									</td>
									
									<td style="display:none;"><input type="hidden" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
									<input type="hidden" ng-model="stockData.deptid" name="deptid{{$index}}"  />
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Stock Request" name="stockRequest{{$index}}" ng-model="stockData.stockReqNo"  readonly>
									</div>
									</td>
									
									<td>
									<div class="form-group">
									<input type="text" class="form-control1 datepicker" placeholder="Date of Request" maxlength="10" name="dateOfRequest{{$index}}" ng-model="stockData.reqDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<div class="form-group" >
									<input type="text" class="form-control1" placeholder="Requested Items" readonly maxlength="20" name="requestItems{{$index}}" ng-model="stockData.Items">									
															
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									<td>&nbsp;&nbsp;<a href="#">
          <span style="font-size:20px;" ng-hide="AddedStockRequest.userDept!=stockData.deptid"  class="glyphicon glyphicon-eye-open" title="View"></span>
        </a></td>
									<td>
									</td>
									
									
									
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData" ng-if="stockData.status=='1'" style="background-color: #66CC66;">
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										<a  href="#/store/transaction/StockIndent"  >
										 <img ng-hide="AddedStockRequest.userDept!=stockData.deptid"   src="../icons/edit.png" style="height:30px; background-color:none;" ng-click="UpdateStockRequest(stockData.stockReqNo);" />
										</a>
									</td>
									
									<td style="display:none;"><input type="text" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
									<div class="form-group">
									<input type="hidden" ng-model="stockData.deptid" name="deptid{{$index}}"  />
									<input type="text" class="form-control1" placeholder="Stock Request" name="stockRequest{{$index}}" ng-model="stockData.stockReqNo"  readonly>
									</div>
									</td>
									
									<td>
									<div class="form-group">
									<input type="text" class="form-control1 datepicker" placeholder="Date of Request" maxlength="10" name="dateOfRequest{{$index}}" ng-model="stockData.reqDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<div class="form-group" >
									<input type="text" class="form-control1" placeholder="Requested Items" maxlength="20" name="requestItems{{$index}}" ng-model="stockData.Items">									
															
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									<td>&nbsp;&nbsp;<a href="#">
          <span style="font-size:20px;" ng-hide="AddedStockRequest.userDept!=stockData.deptid"  class="glyphicon glyphicon-eye-open" title="View"></span>
        </a></td>
									<td> <a  href="#/store/transaction/StoreStockIssue"  >
										 <img src="../icons/edit.png" style="height:30px;" ng-click="updateToStockIssue(stockData.stockReqNo);"/>
										</a></td>
									
									
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData" ng-if="stockData.status=='2'" style="background-color:yellow;">
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										<a  href="#/store/transaction/StockIndent"  >
										 <img  ng-hide="AddedStockRequest.userDept!=stockData.deptid"   src="../icons/edit.png" style="height:30px;" ng-click="UpdateStockRequest(stockData.stockReqNo);" />
										</a>
									</td>
									<td><a href="#">
          <span style="font-size:20px;" ng-hide="AddedStockRequest.userDept!=stockData.deptid"   class="glyphicon glyphicon-eye-open" title="View"></span>
        </a></td>
									<td style="display:none;"><input type="text" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
									<div class="form-group">
									<input type="hidden" ng-model="stockData.deptid" name="deptid{{$index}}"  />
									<input type="text" class="form-control1" placeholder="Stock Request" name="stockRequest{{$index}}" ng-model="stockData.stockReqNo"  readonly>
									</div>
									</td>
									
									<td>
									<div class="form-group">
									<input type="text" class="form-control1 datepicker" placeholder="Date of Request" maxlength="10" name="dateOfRequest{{$index}}" ng-model="stockData.reqDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<div class="form-group" >
									<input type="text" class="form-control1" readonly placeholder="Requested Items" maxlength="20" name="requestItems{{$index}}" ng-model="stockData.Items">									
															
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" readonly  class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly >									
															
									</div>
									</td>
									<td>&nbsp;&nbsp;<a href="#">
          <span style="font-size:20px;" ng-hide="AddedStockRequest.userDept!=stockData.deptid"  class="glyphicon glyphicon-eye-open" title="View"></span>
        </a></td>
									<td> <a  href="#/store/transaction/StoreStockIssue"  >
										 <img src="../icons/edit.png" style="height:30px;" ng-click="updateToStockIssue(stockData.stockReqNo);"/>
										</a></td>									
									
									
									
								</tr>
							</table>
							
							</div>
							
						 <br>
						 					<p style='text-align:right;font-weight:bold;'>Requested : <input type='text' size='3' style='background-color:orange;' readonly> &nbsp;&nbsp; Authorize : 
<input type='text' size='3' readonly style='background-color:green;'>&nbsp;&nbsp;&nbsp; Pending : <input type='text' size='3' style='background-color:yellow;' readonly>&nbsp;&nbsp; </p>
							<!--<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(StockRequestForm)">Reset</button>
									</div>
								</div>						 	
							 </div>-->				
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
