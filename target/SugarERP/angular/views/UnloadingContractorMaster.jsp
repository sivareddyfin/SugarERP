	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UnloadContractorMaster" ng-init="loadUnloadContractorCode();getAllUnload();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Unloading Contractor Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
    	        			  
							
					<form name="Unloadcontractorform" ng-submit="UnloadSubmit(AddedUnload,Unloadcontractorform)" novalidate>
							 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group floating-label-wrapper">
			                        	<div class="fg-line">
										<label for="uccode">UCCode</label>
										
											<input type="text" class="form-control" placeholder="UCCode"   readonly name="ucCode"  data-ng-model="AddedUnload.ucCode"  tabindex="1"  maxlength="25"  id="uccode" with-floating-label  />  
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									   <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Unloadcontractorform.contactNo.$invalid && (Unloadcontractorform.contactNo.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="contactnumber">Contact Number</label>
									
			<input type="text" class="form-control" placeholder="Contact Number"  name="contactNo"  data-ng-model="AddedUnload.contactNo"  tabindex="2" data-ng-minlength="10" maxlength="10" data-ng-required="true" data-ng-pattern='/^[7890]\d{9,10}$/' id="contactnumber" with-floating-label  ng-blur="spacebtw('contactNo');" />
			                        	</div>
										<p ng-show="Unloadcontractorform.contactNo.$error.required && (Unloadcontractorform.contactNo.$dirty || Addsubmitted)" class="help-block">Contact Number is required</p>
								<p ng-show="Unloadcontractorform.contactNo.$error.pattern && (Unloadcontractorform.contactNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Mobile Number starting with 7,8,9 series</p>
	                    <p ng-show="Unloadcontractorform.contactNo.$error.minlength && (Unloadcontractorform.contactNo.$dirty || Addsubmitted)" class="help-block">Contact Number is Too Short</p>
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>



							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Unloadcontractorform.name.$invalid && (Unloadcontractorform.name.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="name">Name</label>
										
											<input type="text" class="form-control autofocus" placeholder="Name" maxlength="25" autofocus  name="name"  data-ng-model="AddedUnload.name"  tabindex="1" data-ng-required='true'  data-ng-pattern="/^[a-zA-Z.\s]*$/" id="name" with-floating-label  ng-blur="spacebtw('name');" />
			                        	</div>
										<p ng-show="Unloadcontractorform.name.$error.required && (Unloadcontractorform.name.$dirty || Addsubmitted)" class="help-block">Name is required</p>
										<p ng-show="Unloadcontractorform.name.$error.pattern && (Unloadcontractorform.name.$dirty || Addsubmitted)" class="help-block">Enter Valid Name</p>
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"    data-ng-model="AddedUnload.status">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"   data-ng-model="AddedUnload.status">
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
							   
							    
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedUnload.modifyFlag"  />	
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(Unloadcontractorform);">Reset</button>
									
										
									</div>
								</div>						 	
							 </div>	
							 </form>	
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!----------table grid----------------->
<div class="block-header" style="margin-bottom:0px; positiontatic; margin-top:-30px; padding:8px;"><h2><b>Added Unloading contractor Managers</b></h2></div>
<div class="card">	
        <div class="card-body card-padding">
				<div class="table-responsive">	
					 <section class="asdok">
						<div class="container1">

						<form name="editUnloadForm" novalidate> 

       				 			<table   ng-table="UnloadContractorMaster.tableEdit"  class="table table-striped table-vmiddle" >
		
													<thead>
											        <tr>
											           <th><span>Action</span></th>
													   <th><span>UcCode</span></th>
													   <th><span>Name</span></th>
													   <th><span>ContactNumber</span></th>
													   <th><span>Status</span></th>
											        </tr>
											      </thead>
											      <tbody>		

       				 <tr ng-repeat="Unload in UnloadData"  ng-class="{ 'active': Unload.$edit }">
                	     	 <td>
    	                <button type="button" class="btn btn-default" ng-if="!Unload.$edit" ng-click="Unload.$edit = true;Unload.modifyFlag='Yes'"><i class="zmdi zmdi-edit"></i></button>
            	        <button type="submit" class="btn btn-success" ng-if="Unload.$edit" data-ng-click='updateUnloadMaster(Unload,$index);Unload.$edit = isEdit;'><i class="zmdi zmdi-check"></i></button>
						<input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Unload.modifyFlag"  />
                    	  </td>
               					 <td>
                	     	 <span ng-if="!Unload.$edit">{{Unload.ucCode}}</span>
    	                 <div ng-if="Unload.$edit">
							<div class="form-group">
					<input class="form-control" type="text" data-ng-model="Unload.ucCode"  name="ucCode{{$index}}" maxlength="25" placeholder='Uccode' readonly/>
					</div>
				</div>
            	       			</td>
                    	   <td>
                     <span ng-if="!Unload.$edit">{{Unload.name}}</span>
                     	<div ng-if="Unload.$edit">
            					<div class="form-group" ng-class="{ 'has-error' : editUnloadForm.name{{$index}}.$invalid && (editUnloadForm.name{{$index}}.$dirty || submitted)}">												
							<input class="form-control" type="text" data-ng-model="Unload.name"  name="name{{$index}}" maxlength="25" placeholder='Name'  data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('name',$index)"/>
				<p ng-show="editUnloadForm.name{{$index}}.$error.required && (editUnloadForm.name{{$index}}.$dirty || submitted)" class="help-block">Required</p>
				<p ng-show="editUnloadForm.name{{$index}}.$error.pattern  && (editUnloadForm.name{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>										
						</div>
					</div>
                  </td>
                  <td>
                      <span ng-if="!Unload.$edit">{{Unload.contactNo}}</span>
  						<div ng-if="Unload.$edit"> 
							<div class="form-group" ng-class="{ 'has-error' : editUnloadForm.contactNo{{$index}}.$invalid && (editUnloadForm.contactNo{{$index}}.$dirty || submitted)}">												
  	<input class="form-control" type="text" data-ng-model="Unload.contactNo" name="contactNo{{$index}}" maxlength="10" data-ng-minlength="10" placeholder='Contact Number' data-ng-required="true" data-ng-pattern='/^[7890]\d{9,10}$/' ng-blur="spacebtwgrid('contactNo',$index)"/>
				<p ng-show="editUnloadForm.contactNo{{$index}}.$error.required && (editUnloadForm.contactNo{{$index}}.$dirty || submitted)" class="help-block">Required</p>
				<p ng-show="editUnloadForm.contactNo{{$index}}.$error.pattern  && (editUnloadForm.contactNo{{$index}}.$dirty || submitted)" class="help-block">Should Start with 7,8,9 series</p>
					<p ng-show="editUnloadForm.contactNo{{$index}}.$error.minlength  && (editUnloadForm.contactNo{{$index}}.$dirty || submitted)" class="help-block">Too Short</p>
																		
  														 </div>	
													</div>             	       
 															 </td>
   														<td>
							                      <span ng-if="!Unload.$edit">
												<span ng-if="Unload.status=='0'">Active</span>
													<span ng-if="Unload.status=='1'">Inactive</span>
														</span>
        		            					  <div ng-if="Unload.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" data-ng-model='Unload.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='Unload.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>											
                 	     	          		 </tr>
										 </tbody>
         								</table>
  									</form>
								</div>
							</section>
							</div>
 						</div>
					</div>	
<!----------table grid end------------->

				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



