	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.textbox
		{
			width:100px;	
		}
		.spacing-table {
  
   
    border-collapse: separate;
    border-spacing: 0 5px; /* this is the ultimate fix */
}
.spacing-table th {
    text-align: left;
    
}
.spacing-table td {
    border-width: 1px 0;
  
   
    
}
.spacing-table td:first-child {
    border-left-width: 3px;
    border-radius: 5px 0 0 5px;
}
.spacing-table td:last-child {
    border-right-width: 3px;
    border-radius: 0 5px 5px 0;
}
	</style>

	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="PostSeasonCalculationController" ng-init="loadVillageNames();loadPostSeasonAccountingDate();loadPostSeasonNames();loadCircleNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> Post Season Calculation </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		
					 	<form name="postSeasonForm" ng-submit="UpdatePostsubmit(PostSeason,postSeasonForm)" novalidate>
						<div class="row">
				          <div class="col-sm-3">
						 
								
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
											<div  class="form-group"  ng-class="{ 'has-error' : postSeasonForm.season.$invalid && (postSeasonForm.season.$dirty || submitted)}">				
				        	               <div class="fg-line">
										
										
								<select chosen class="w-100" name="season" data-ng-model="PostSeason.season" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true" data-ng-required='true'>
									<option value="">Season</option>
									</select>
				                	    </div>
							
		<p data-ng-show="postSeasonForm.season.$error.required && (postSeasonForm.season.$dirty || submitted)"  class="help-block">Select Season</p>																								
											</div>
									    </div>
								
								
							</div>
							<div class="col-sm-3">
							
						
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group floating-label-wrapper">
			                        	<div class="fg-line nulvalidate">
											<label for="ppslno">PP Sl No</label>
											<input type="text" class="form-control" placeholder="PP Sl No"  name="ppslno"  data-ng-model="PostSeason.ppslno" readonly id="ppslno" with-floating-label> 
			                        	</div>
										
										
										
										
			                
			                 			                    
			                </div>
							</div>
							</div>
							   <div class="col-sm-3">
						
							
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
											      <div  class="form-group"  ng-class="{ 'has-error' : postSeasonForm.circleCode.$invalid && (postSeasonForm.circleCode.$dirty || submitted)}">				
				        	               <div class="fg-line">
										
										
								<select chosen class="w-100" name="circleCode" data-ng-model="PostSeason.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-circle':true" data-ng-required='true'>
									<option value="">Circle</option>
									</select>
				                	    </div>
							
		<p data-ng-show="postSeasonForm.circleCode.$error.required && (postSeasonForm.circleCode.$dirty || submitted)" class="help-block">Select Village</p>																								
											</div>
									  
									
								</div>
							</div>
							  <div class="col-sm-3">
                                      
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                                    <!--<div class="form-group">-->
                                                    <div class="form-group" >
                                                        <div class="fg-line">


                                                              <input type="text" class="form-control datepicker autofocus"  name="seedAccDate" placeholder="Date" data-ng-model="PostSeason.seedAccDate" data-input-mask="{mask: '00-00-0000'}"  id="AgreementDate" with-floating-label   readonly/>


                                                        </div>

                                                   
                                            </div>
                                        </div>
                                    </div>
							   <!---<div class="col-sm-3">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getPostSeasonDetails(postSeasonForm)">Load Details</button>

													</div>
												</div>
  										</div>-->
							</div>
							<div class="row">
						
                                  
									
									   <div class="col-sm-3">
						  <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
											      <div  class="form-group"  >				
				        	               <div class="fg-line">
										
										
								<select chosen class="w-100" name="circleCode"  ng-options="" data-ng-required='true'>
									<option value="">Completed Ones</option>
									</select>
				                	    </div>
							
											</div>
									    </div>
									</div>	
								</div>
							</div>
							
							
								<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : postSeasonForm.villageCode.$invalid && (postSeasonForm.villageCode.$dirty || Addsubmitted)}">																																																											
					        	                <div class="fg-line">
                						    		<select chosen class="w-100" name="villageCode" data-ng-model='PostSeason.villageCode' data-ng-required='true' ng-options="villageCode.id as villageCode.village for villageCode in VillagesNamesData | orderBy:'-village':true">
														<option value="">Select Village</option>
				        		        		    </select>
					                	        </div>
	<p ng-show="postSeasonForm.villageCode.$error.required && (postSeasonForm.villageCode.$dirty || Addsubmitted)" class="help-block">Select Village</p>																								
</div>												
					                    	</div>		
										</div>
							</div>
							
							 <div class="row" align="center">
                                   
                                        <div class="input-group">
                                            <div class="fg-line">
                                              
                                            <button type="button"   class="btn btn-primary"	 ng-click="getPostSeasonDetails(postSeasonForm)">Load Details</button>
                                            
											</div>
                                        </div>
                                   
									
                                </div>
							
							
						
				 		
						
						<br />
						 </form>
						<!------table data start--------->
								
						    <form name="SeedAccountingCalculation"  style=" width:95%; margin-left:-6%;   margin-top:-6%;" id="popup_box">
                      
                            <div style="height:100%; background-color:#FFFFFF;" >
                                <div style="height:400px; overflow-y:auto;">
                                


                                        <table style="width:100%; position:relative;"  class="spacing-table"  cellpadding="5"  cellspacing="10"  ng-repeat="headerData in PostSeasonHeaderData" >

                                            <tr style="background-color: #0099FF;font-weight:bold;">
                                                <th  colspan="1" style=" color:#FFFFFF;">Ryot Code</th>
													
                                                <th   colspan="2" style=" color:#FFFFFF;">Ryot Name</th>
									
										
                                                <th  colspan="2" style=" color:#FFFFFF;">Accrued & Due Amount </th>
											
                                                <th  colspan="2"  style=" color:#FFFFFF;">Paying Amount</th>
														
                                                <th  colspan="2"  style=" color:#FFFFFF;">Advances &  Loans </th>
														
												<th  colspan="2"  style=" color:#FFFFFF;">SB AC/ Amount</th>
                                               	
												

                                            </tr>
											
											<tr>
											
											<td colspan="1">
										
											
                                                   {{headerData.seedSupplierCode}}  <input type="hidden" name="seedSupplierCode{{$index}}" ng-model="headerData.seedSupplierCode" readonly />
                                             </td>
											<td colspan="2">
                                                   {{headerData.seedSupplierName}}  <input type="hidden" name="seedSupplierName{{$index}}" ng-model="headerData.seedSupplierName" readonly />
                                                </td>
												
										
											<td colspan="2" >
											
                                                   {{headerData.totalAmount}} <input type="hidden"  name="totalAmount{{$index}}" ng-model="headerData.totalAmount" readonly />
                                                </td>
												<td colspan="2" >
												
                                                  <input type="text"  class="textbox" name="payingAmount{{$index}}" ng-model="headerData.payingAmount"  />
                                                </td>
											<td colspan="2" >
											
                                                   {{headerData.loansAndAdvancesPayable}} <input type="hidden" name="loansAndAdvancesPayable{{$index}}" ng-model="headerData.loansAndAdvancesPayable" readonly />
                                                </td>
											<td colspan="2">
											
                                                   {{headerData.loansAndAdvancesPaid}} <input type="hidden" name="loansAndAdvancesPaid{{$index}}" ng-model="headerData.loansAndAdvancesPaid" readonly />
                                                </td>
										
											</tr>
											
                                            
											
                                            <tr style="background-color: #c0c5ce;font-weight:bold;">
                                                <th><span>Advance</span></th>
                                                <th><span>Advance<br/> Code</span></th>
                                                <th><span>Advance<br/>Amount</span></th>
												 <th><span>Interest<br/>Rate</span></th>
                                                <th><span>Date of<br/> Principle</span></th>
                                                <th><span>Repayment<br/>Date</span></th>
                                               
                                                <th colspan="2"><span>Interest<br/> Amount</span></th>
                                                <th colspan="2"><span>Total<br/>Amount</span></th>
                                                <th colspan="2"><span>Paying <br/>Amount</span></th>


                                            </tr>
                                            <tr ng-repeat="gridData in headerData.advanceAndLoans" >
											
                                                <td>
                                                  {{gridData.advance}}   <input type="hidden" name="advance{{$index}}" ng-model="gridData.advance" readonly />
                                                </td>
                                                <td>
                                                   {{gridData.advanceCode}}  <input type="hidden" name="advanceCode{{$index}}" ng-model="gridData.advanceCode" readonly />
                                                </td>
                                                <td>
                                                   {{gridData.principle}}  <input type="hidden" name="principle{{$index}}" ng-model="gridData.principle" readonly />
                                                </td>
                                                <td>
                                                 {{gridData.interestRate}}   <input type="text" class="hidden" name="interestRate{{$index}}" ng-model="gridData.interestRate"  readonly />
                                                </td>
                                                <td>
												<div>
												{{gridData.issuingDate}}   <input type="hidden"  name="issuingDate{{$index}}" ng-model="gridData.issuingDate" readonly   />
                                                </div>
												</td>
                                                <td>
                                                     <input type="text" class="textbox" name="repaymentDate{{headerData.seedActSlno}}{{$index}}" ng-model="gridData.repaymentDate" data-input-mask="{mask: '00-00-0000'}"  ng-blur="ChangeRepaymentDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" />
                                                </td>
                                                <td colspan="2">
												{{gridData.interestAmount}}   <input type="hidden"  name="interestAmount{{headerData.seedActSlno}}{{$index}}" ng-model="gridData.interestAmount" ng-blur="ChangeRepaymentDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" readonly />
                                                </td>
                                                <td colspan="2">
												{{gridData.totalPayable}}<input type="hidden"  name="totalPayable{{headerData.seedActSlno}}{{$index}}" ng-model="gridData.totalPayable" ng-blur="ChangeRepaymentDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" readonly />
                                                </td>
                                                <td colspan="2">
                                                 
													 <input type="text" class="textbox" name="paidAmount{{$index}}" ng-model="gridData.paidAmount"  readonly />
                                                </td>
												
                                            </tr>

						
					<!--	<div class="row" align="center">
                                   
                                        <div class="input-group">
                                            <div class="fg-line">
                                              
                                            <button type="button"   class="btn btn-primary"	 ng-click="getSeedAccountingDetails(SeedAccountingForm);">Update</button>
                                            
											</div>
                                        </div>
                                   
									
                                </div>-->



                                        </table>
                                  
                                </div>
								<div style="text-align:center;display:none; background-color:#FFFFFF;" id="buttonshow">
						<button type="button"   class="btn btn-primary"	ng-disabled="" ng-click="PostSeasonSave();">Update</button></div>
                            </div>
                       
                        <p></p>
                        <!--<button class="btn btn-primary previous" id="Third" ng-click="setNullArray();"><i class="zmdi zmdi-arrow-back"></i></button>-->
                        <%--   <button class="btn btn-primary next third" id="third" ng-disabled="ThirdTabDeduction.$invalid" ng-click="thirdTabDeductionsSave();"><i class="zmdi zmdi-arrow-forward"></i></button>					
					   <button type="button" class="btn btn-primary third" ng-click="thirdTabDeductionsSave();" ng-disabled="ThirdTabDeduction.$invalid">Update</button>--%>
                            <!--</div>	-->
                            <!--</div>		-->
                    </form>
						
					<!----------end----------------------->										
					
				</div>
				</div>
			</div>		
	    </section> 
	</section>
	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
