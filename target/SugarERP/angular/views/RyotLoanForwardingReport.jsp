	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" ></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RyotLoanForwardingReport" ng-init="loadBranchNames();loadSeasons();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Ryot Loan Forwarding Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>	
					 <form method="POST" action="/SugarERP/generateRyotLoanForwardingReport.html" target="_blank">
					 
					<div class="row">
						<div class="col-sm-12">
						   <div class="row">
						     <div class="col-sm-4">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="season"  data-ng-model="AddedRyotName.season" ng-options="season.season as season.season for season in seasons  | orderBy:'-season':true" tabindex="1">
                                      <option value=''>Select Season</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
						  
						  
						  <div class="col-sm-4">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                        <select chosen class="w-100"  tabindex="1"  data-ng-model='AddedRyotName.branchCode' name="branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in branchNames | orderBy:'-branchname':true" ng-change="loadLoanNos();">
										<option value="">Select Branch</option>
						        		</select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
						  
						  
						   <!--<div class="col-sm-3">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="ryotname"  data-ng-model="AddedRyotName.ryotname" ng-options="ryotname.id as ryotname.id for ryotname in ryotNames  | orderBy:'-id':true" tabindex="1" ng-change="loadLoanNos(AddedRyotName.season,AddedRyotName.ryotname);">
                                      <option value=''>Select RyotCode</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>-->
						  
						    <div class="col-sm-4">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="loanRyotcode"  data-ng-model="AddedRyotName.loanRyotcode" ng-options="loanRyotcode.loanRyotcode as loanRyotcode.loanRyotcode for loanRyotcode in loanRyotcode  | orderBy:'-loanRyotcode':true" tabindex="1" >
                                      <option value=''>Select LoanNumber</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
											<!--<div class="col-sm-3">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
											<input type="text" data-ng-model="AddedRyotName.ryotCode" name="ryotCode" class="form-control">
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>-->						  
						   
                
                    </div>
					
					</div>
					
					       <div class="row">
						                  <div class="col-sm-12">
                    			             <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Choose Format :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="pdf"   data-ng-model='AddedRyotName.status' />
			    					    				<i class="input-helper"></i>PDF
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="html"   data-ng-model='AddedRyotName.status'>
					    								<i class="input-helper"></i>HTML
							  		 				</label>
													<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="lpt"   data-ng-model='AddedRyotName.status'>
					    								<i class="input-helper"></i>LPT
							  		 				</label>						 							 
												</div>
											</div>
						  </div>
                
                    </div>
				     	
				
						
			  
			  <br />
			  
			  
			             
			              <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit"  class="btn btn-primary btn-sm m-t-10" ng-click="asd();">Generate Report</button>
										
									</div>
								</div>
							</div>
							
											</form>
										</div>
									</div>
								</div>
							</section>
						</section>
							
						
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



