	<script type="text/javascript">
		$('#PermitNumber').focus();		
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="PrintPermits" ng-init="loadPrintPermitSeasons();">    
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Print Permits</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
						<form name="PrintPermitsForm">
					 		<div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100" placeholder='Select Season' name="season" ng-model="PrintPermit.season" ng-options="season.season as season.season for season in PrintSeasonNames | orderBy:'-season':true" ng-change="loadPrintProgramNumber(PrintPermit.season);">
												<option value="">Select Season</option>
				        		            </select>
                	            		  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Print Mode :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="printMode" value="Bulk" ng-model="PrintPermit.printMode">
			    			            	<i class="input-helper"></i>Bulk
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="printMode" value="Single" ng-model="PrintPermit.printMode">
			    				            <i class="input-helper"></i>Single
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>								  
							 </div><br />
							 <div class="row">
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100" placeholder='Select Program' name="programNumber" ng-model="PrintPermit.programNumber" ng-options="programNumber.programnumber as programNumber.programnumber for programNumber in PrintProgramNum | orderBy:'-programnumber':true">
												<option value="">Select Program</option>
				        		            </select>
                	            		  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>								
								  <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper">	
			    	    	                <div class="fg-line">
											  <label for="PermitNumber">Permit Number</label>
            						          <input type="text" class="form-control" placeholder="Permit Number" maxlength="10" id="PermitNumber" name="permitNumber" ng-model="PermitNumber" with-floating-label ng-model="PrintPermit.permitNumber">											
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>
    	        			 </div><br />		
							 <div class="row" align="center">            			
								<div class="input-group">
								  <div class="fg-line">
								 <button type="submit" class="btn btn-primary btn-sm m-t-10">Print</button>
								 <button type="button" class="btn btn-primary btn-sm m-t-10">Cancel</button>
								</div>
								</div>						 	
							 </div>	
						</form>		
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
