<style>
.styletr {border-bottom: 1px solid grey;}


</style>
	<script type="text/javascript">		
		$('#autofocus').focus();				
	</script>
	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="AcknowledgementController" ng-init="loadRyotCodeDropDown();loadFieldOfficerDropdown();loadFieldAssistants();loadseasonNames();loadVarietyNames();loadFieldOfficerDropdown();loadTrayType();onloadFunction();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Acknowledgement</b></h2></div>
			    <div class="card">
			        <div  class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					    <form name="Ackform" novalidate ng-submit="AckSubmit(AddedAcknowledgementForm,Ackform);">
							 <div class="row">
									<div class="col-sm-3">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : Ackform.season.$invalid && (Ackform.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedAcknowledgementForm.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="loadDetails(AddedAcknowledgementForm.season,AddedAcknowledgementForm.dispatchNo);">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="Ackform.season.$error.required && (Ackform.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
											</div>
											<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Ackform.dispatchNo.$invalid && (Ackform.dispatchNo.$dirty || Addsubmitted)}">
											  <div class="fg-line">
												<label for="dispatchNo">Dispatch No.</label>
													<input type="text" placeholder="Dispatch No." name="dispatchNo" class="form-control" ng-model="AddedAcknowledgementForm.dispatchNo" data-ng-required="true" ng-blur="loadDetails(AddedAcknowledgementForm.season,AddedAcknowledgementForm.dispatchNo);"  id="dispatchNo" with-floating-label />
												
											 </div>
								<p ng-show="Ackform.dispatchNo.$error.required && (Ackform.dispatchNo.$dirty || Addsubmitted)" class="help-block">Dispatch No. is required</p>										
										   </div>
									   </div>
										
									</div>
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : Ackform.ryotcode.$invalid && (Ackform.ryotcode.$dirty || Addsubmitted)}">
										  
										  <div class="fg-line">
												<div class="select">
												<select chosen class="w-100" name="ryotcode" ng-model="AddedAcknowledgementForm.ryotcode"  ng-options="ryotcode.id as ryotcode.id for ryotcode in ryotCode | orderBy:'-id':true" ng-required='true' ng-readonly='true' style="background-color:#FFFFFF;" >
													<option value="">Ryot Code</option>
												</select>													  
												</div>
			    		            		</div>
											
											
											 
								<p ng-show="Ackform.ryotcode.$error.required && (Ackform.ryotcode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>										
										   </div>
									   </div>
										
									</div>
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : Ackform.foCode.$invalid && (Ackform.foCode.$dirty || Addsubmitted)}">
										  
										  
										  <div class="fg-line">
														 <select chosen class="w-100" name="foCode" data-ng-model='AddedAcknowledgementForm.foCode' data-ng-required='true' ng-options="foCode.id as foCode.fieldOfficer for foCode in FieldOffNames  | orderBy:'-fieldOfficer':true" ng-change="loadIndents(AddedAcknowledgementForm.season,AddedAcknowledgementForm.ryotcode,AddedAcknowledgementForm.foCode)" ng-readonly='true' style="background-color:#FFFFFF;">
														<option value="">Field Officer</option>
				        				            </select>
				    		            	        </div>
										  <!--<select chosen class="w-100" ng-model="AddAckform.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true">
														<option value="">Field Assistant</option>
														
														</select>-->
											 
								<p ng-show="Ackform.foCode.$error.required && (Ackform.foCode.$dirty || Addsubmitted)" class="help-block">Select Field Officer</p>										
										   </div>
									   </div>
										
									</div>
									
								</div>
								
								
								
							
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper">
										<div class="form-group">
						        	         <div class="fg-line">
											 <label for="indents">Indents</label>
											 <input type='text' class="form-control" name="indents" ng-model="AddedAcknowledgementForm.indentNumbers" placeholder="Indents" id="indents" with-floating-label ng-readonly='true' style="background-color:#FFFFFF;">
												
													
												
											 </div>
										</div>
										</div>
			            		    </div>
										</div>

									



									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Ackform.faCode.$invalid && (Ackform.faCode.$dirty || Addsubmitted)}">
												<div class="fg-line">
														 <select chosen class="w-100" ng-model="AddedAcknowledgementForm.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;">
														<option value="">Field Assistant</option>
														
														</select>
				    		            	        </div>
														
				    		            	        
					<p ng-show="Ackform.faCode.$error.required && (Ackform.faCode.$dirty || Addsubmitted)" class="help-block">Field Assistant is Required.</p>
																						
												</div>
			            		        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
												
												<div class="fg-line">
												
													<input type="text" placeholder="Acknowledged Date" class="form-control date" name="dateOfAcknowledge" ng-model="AddedAcknowledgementForm.dateOfAcknowledge" data-ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;"  />
												
											 </div>
												
												
						        	              
					
																						
												</div>
			            		        	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Ackform.ryotName.$invalid && (Ackform.ryotName.$dirty || Addsubmitted)}">
						        	      <div class="fg-line">
    	    		    					 <label for="ryotname">Ryot Name</label> 
											 <input type="text" class="form-control" placeholder="Ryot Name" ng-model="AddedAcknowledgementForm.ryotName" id="ryotname" with-floating-label ng-readonly='true' style="background-color:#FFFFFF;" >	
											
					    	             </div>
						 <p ng-show="Ackform.ryotName.$error.required && (Ackform.ryotName.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>
									   </div>
					        	    </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
											 		<div class="form-group" ng-class="{ 'has-error' : Ackform.driverName.$invalid && (Ackform.driverName.$dirty || Addsubmitted)}">									
						        	                	<div class="fg-line">
														  <label for="drivername">Driver Name</label>  		
    		    		    					        <input type="text" class="form-control" placeholder="Driver Name" data-ng-model="AddedAcknowledgementForm.driverName" id="drivername" with-floating-label name="driverName" data-ng-required="true"  ng-readonly='true' style="background-color:#FFFFFF;"  />
					    		            	        </div>
							<p ng-show="Ackform.driverName.$error.required && (Ackform.driverName.$dirty || Addsubmitted)" class="help-block">Driver Name is Required.</p>																											
							<p ng-show="Ackform.driverName.$error.pattern && (Ackform.driverName.$dirty || Addsubmitted)" class="help-block">Enter Valid Name.</p>
																												
													</div>							
												  </div>
			            		        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
											 		<div class="form-group" ng-class="{ 'has-error' : Ackform.vehicle.$invalid && (Ackform.vehicle.$dirty || Addsubmitted)}">
						        	                	<div class="fg-line">
														  <label for="Vehicle">Vehicle No.</label>  		
    		    		    					          <input type="text" class="form-control" placeholder='Vehicle No.' maxlength="10" id="Vehicle" with-floating-label name="vehicle" ng-model="AddedAcknowledgementForm.vehicle " data-ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;" />
					    		            	        </div>
							<p ng-show="Ackform.vehicle.$error.required && (Ackform.vehicle.$dirty || Addsubmitted)" class="help-block">Vehicle No. is Required.</p>																											
							<p ng-show="Ackform.vehicle.$error.pattern && (Ackform.vehicle.$dirty || Addsubmitted)" class="help-block">Enter Valid Vehicle No..</p>
																												
													</div>							
												  </div>
			            		        	</div>
										</div>
									</div>
									
									<div class="row" style="display:none;">
										<div class="col-sm-12">
											<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Soil & Water Analysis</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="soilwateranalysis" value="0"   data-ng-model="AddedAcknowledgementForm.soilWaterAnalyis" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="soilwateranalysis" value="1"   data-ng-model='AddedAcknowledgementForm.soilWaterAnalyis'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>	
										</div>
									</div>
									
									
								</div>
								
								
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
														<label for="phone">Ryot Phone Number</label>
														<input type="text" class="form-control" placeholder="Ryot Phone Number"  id="phone" with-floating-label name="phone" ng-model="AddedAcknowledgementForm.ryotPhoneNumber" data-ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;"  />
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper">
														<div class="form-group" ng-class="{ 'has-error' : Ackform.drPhoneNo.$invalid && (Ackform.drPhoneNo.$dirty || Addsubmitted)}">									
															<div class="fg-line">
															  <label for="drPhoneNo">Driver Phone Number</label>  		
															<input type="text" class="form-control" placeholder="Driver Phone Number" data-ng-model="AddedAcknowledgementForm.drPhoneNo" id="drPhoneNo" with-floating-label name="drPhoneNo" data-ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;"  />
															</div>
								<p ng-show="Ackform.drPhoneNo.$error.required && (Ackform.drPhoneNo.$dirty || Addsubmitted)" class="help-block">Driver Phone Number is Required.</p>																											
								<p ng-show="Ackform.drPhoneNo.$error.pattern && (Ackform.drPhoneNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Driver Phno.</p>
																													
														</div>							
													  </div>
												</div>
										</div>
									</div>
									<div class="row" style="display:none;">
									<div class="col-sm-12">
										<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Land Survey</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="landSurvey" value="0"   data-ng-model="AddedAcknowledgementForm.landSurvey" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="landSurvey" value="1"   data-ng-model='AddedAcknowledgementForm.landSurvey'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>
									</div>
									</div>
									<div class="row" style="display:none;">
										<div class="col-sm-12">	
											<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />	
									 					 <label class="radio radio-inline m-r-20"><b>Agreement Signed</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="agreementSign" value="0"   data-ng-model="AddedAcknowledgementForm.agreementSign" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="agreementSign" value="1"   data-ng-model='AddedAcknowledgementForm.agreementSign'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Ackform.seedlingQty.$invalid && (Ackform.seedlingQty.$dirty || Addsubmitted)}">										
				        	                	<div class="fg-line">
												<label for="SeedlingQty">Seedlings sent by</label> 
	    	        					         <input type="text" class="form-control" ng-model="AddedAcknowledgementForm.seedlingQty" name="seedlingQty" placeholder="Seedlings sent by" data-ng-required="true" id="SeedlingQty" with-floating-label  ng-readonly='true' style="background-color:#FFFFFF;" />
			        	        	    	    </div>
		<p ng-show="Ackform.seedlingQty.$error.required && (Ackform.seedlingQty.$dirty || Addsubmitted)" class="help-block">SeedlingQty is Required.</p>																									
											</div>
										</div>
			                    	</div>
										</div>
									</div>
								</div>
							</div>
							 
							 <hr />
			                
								<div class="table-responsive"  ng-if="data!=0" id='hidetable' style="display:none;">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table style="width:100%;">								                  
													<thead>
														<tr  class="styletr" style="font-weight:bold;" >
															<!--<th><span>Action</span></th>-->
															<th><span>Indent <br />No.</span></th>
															<th><span>Variety</span></th>
															
															<th><span>No of <br />Seedlings.</span></th>
															<th><span>Cost</span></th>
															<th><span>Total <br />Cost</span></th>
															<th><span>Batch No.</span></th>
															<th><span>No. of <br /> Trays</span></th>
															<th><span>Tray Type</span></th>
															<th><span>Tray Cost</span></th>
															<th><span>Total</span></th>
															
														</tr>
														
													</thead>
													<tbody>
													
													<tr ng-repeat="dispatchData in data"  class="styletr" style="height:60px;">
													
												
													

                                             <!--<td>
											
												
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow($index);" ><i class="glyphicon glyphicon-remove-sign"></i></button>
												<input  type="hidden" ng-model="dispatchData.id" name="dispatchData{{dispatchData.id}}" />												
											</td>-->


													<td><input type="text" class="form-control1"  placeholder="Indent" size="3" name="indentNo{{dispatchData.id}}" data-ng-model="dispatchData.indentNo"  ng-readonly='true' style="background-color:#FFFFFF;"  /></td>
													<td>
													 <select class="form-control1" name="variety{{dispatchData.id}}" ng-model="dispatchData.variety"  ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true" readonly="readonly">
													<option value="">Variety</option>
												</select>
													</td>




													
													<td><input type="text" class="form-control1"  placeholder="Seedlings No." size="5" name="noOfSeedlings{{dispatchData.id}}" data-ng-model="dispatchData.noOfSeedlings"   ng-readonly='true' style="background-color:#FFFFFF;"/></td>
													<td><input type="text" class="form-control1"  placeholder="Cost" size="5" name="cost{{dispatchData.id}}" data-ng-model="dispatchData.costOfEachItem"  ng-readonly='true' style="background-color:#FFFFFF;" /></td>
													<td><input type="text" class="form-control1"  placeholder="TotalCost" size="5" name="totalCost{{dispatchData.id}}" data-ng-model="dispatchData.totalCost"  ng-readonly='true' style="background-color:#FFFFFF;" /></td>

<td><!--<select class="form-control1" name="batchNo{{dispatchData.id}}" data-ng-model="dispatchData.batchNo"><option value="">Batch</option></select>-->      <input list="stateList" placeholder="Batch No" id="ryotcodeorname" style="width:90px;"  class="form-control1"  name="batchSeries{{dispatchData.id}}" placeholder ="Batch No" ng-model="dispatchData.batchNo" ng-change="loadBatchDetails(dispatchData.batchNo,dispatchData.id);"  ng-readonly='true' style="background-color:#FFFFFF;"> 
											<datalist id="stateList"  ng-readonly='true' style="background-color:#FFFFFF;">
											<select class="form-control1">
										 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
										</select>    

											</datalist>    </td> 


													<td><input type="text" class="form-control1"  placeholder="Trays" size="5" name="noOfTrays{{dispatchData.id}}" data-ng-model="dispatchData.noOfTrays" ng-readonly='true' style="background-color:#FFFFFF;" /></td>
													
 <td>
                                                  <select class="form-control1" name="trayType{{dispatchData.id}}" ng-model="dispatchData.trayType"  ng-options="trayType.id as trayType.TrayType for trayType in TrayTypeNames | orderBy:'TrayType':true" readonly="readonly" ng-change="loadTrayCost(dispatchData.trayType,AddedAcknowledgementForm.ryotcode,dispatchData.id);"  ng-readonly='true' style="background-color:#FFFFFF;">
													<option value="">TrayType</option>
												</select>
                                                </td>
													
														<td><input type="text" class="form-control1"  placeholder="Tray Cost" style="width:100px;" name="trayCost{{dispatchData.id}}" data-ng-model="dispatchData.trayCost"  ng-readonly='true' style="background-color:#FFFFFF;" /></td>

													<td><input type="text" class="form-control1"  placeholder="Tray Total Cost" size="5" name="trayTotalCost{{dispatchData.id}}" data-ng-model="dispatchData.trayTotalCost"  ng-readonly='true' style="background-color:#FFFFFF;"/></td>

												
													<!--<td><input type="text" class="form-control1"  placeholder="Total Tray Cost"  size="10" /></td>-->
													
													
													</tr>
																						
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			           <br />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(Ackform)">Reset</button>
										<br /><br />
									</div>
								</div>						 	
							 </div>	
						  </form>	
						  
						  					 							 
						  
						  
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
