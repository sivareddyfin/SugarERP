	<%@ include file="DisableCache.jsp" %>
	<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
	<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
	<%@ taglib uri="/components.hms.com" prefix="hc" %>
	<%@ page import="com.hms.vo.*, java.util.Map"%>	
<%
	ApplicationSession applSession=null;
	String userName="";
	//String storeNameId="";
	if(session.getAttribute("ApplicationSession")!=null)
	{
		applSession = (ApplicationSession)session.getAttribute("ApplicationSession");
		userName = applSession.getUserName();
		//storeNameId=applSession.getStoreNameID();
	}
	else
	{
		System.out.println("Application session is null");
	}
%>	
	<style type="text/css">
	.btnok
	{
		width:80px;
		height:30px;
		color:#FFFFFF;
		font-weight:bold;
		border:none;
		background-color:#2196F3;
	}
	</style>	
<script>
</script>	
	<script type="text/javascript">
		
		var LoginUser = '<%=userName %>';		
		
		$(document).ready(function()
		{
			//alert($('#header').text());
			loadDefaults();
		});
	 	function loadDefaults()
	    {
			//document.forms[0]["EMPDETAILS:EMP_STATUS"].value = "A";
			//document.forms[0]["LOGINUSER:LOGIN_NAME"].focus();
			document.forms[0]["LOGINUSER:ITEM_SELECTED"].value="Contract";
			document.forms[0]["LOGINUSER:EMP_SALARY"].value="0.00";
			document.forms[0]["LOGINUSER:EMP_LEAVES"].value="0";

			// code changing by Bala for bug no 132 on 14-03-2011
			document.forms[0]["LOGINUSER:EMP_TYPE"].value="Other";
			document.forms[0]["LOGINUSER:BIRTH_DATE"].value="1985/01/21";
			document.forms[0]["LOGINUSER:EMPGENDER"].value="M";
			document.forms[0]["LOGINUSER:EMP_DEPT_NAME"].value="IT";
			document.forms[0]["LOGINUSER:EMP_ROLE_NAME"].value="Administrator";
			document.forms[0]["LOGINUSER:EMPSTATUS"].value="A";
	    }
	
	
		function formSubmit()
		{
			var username=$('#Empname').val();
			if(username=="")
			{
				$('#Empname').focus();
				return false;
			}
			var loginid=$('#LoginID').val();
			if(loginid=="")
			{
				$('#LoginID').focus();
				return false;
			}
			checkLoginId();
			return false;		
		}
		
		function checkLoginId()
		{
			var loginid=$('#LoginID').val();
			//ajax call here 
		    var action = "EmpLogin";
			var url = "/BILLINGSTORE/OfferingsServlet?action="+action+"&obj="+loginid;
			//sending url to the servelet by passing parameters 
			if(window.XMLHttpRequest)
			{
				req = new XMLHttpRequest();
			}
		   else
			if(window.ActiveXObject)
			{
				req = new ActiveXObject("Microsoft.XMLHTTP");
			}
			req.open("GET",url,false);
			req.onreadystatechange = LoginCallback;		
			req.send(null);
		}
		function LoginCallback()
		{
			alert(req.readyState);
			if(req.readyState == 4)
		    {
				if(req.status == 200)
				{
					var userDetailsFields=new Array("LOGIN_ID");
					var offerings = req.responseXML.getElementsByTagName('value');
					alert(offerings.length);
					if(offerings.length != 0)
					{ 
						var str=unescape(offerings.item(0).childNodes.item(0).nodeValue.replace(/\+/g,  " "));
						str=str.toLowerCase();
						strLogin=str;
						alert('strlogin-----'+strLogin);
					}
				}
			}				
		}	
		
		function asdfg()
		{
			$('.asd').addClass('has-warning');
			$('#inputWarning1').focus();
		}
			
	</script>
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#inputWarning1').blur(function()
			{
				if($('#inputWarning1').val()!='')
				{
					$('.asd').removeClass('has-warning');	
				}
			    else
				{
			$('.asd').addClass('has-warning');
			$('#inputWarning1').focus();
					
				}
			});
		});
	</script>
<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.html'" data-ng-controller="headerCtrl as hctrl"></header>
<section id="main">  
    <aside id="sidebar" data-ng-include="'template/sidebar-left.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>

    <aside id="chat" data-ng-include="'template/chat.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    <section id="content"> 

        <div class="container" data-ng-controller="tableCtrl as tctrl">
            <div class="block-header">
                <h2>Dashboard</h2>
            </div>

            <div class="card">
                <div class="card-header">
                    <h2>Login User</h2>			
					<p></p>				
                    <!--------Form Design----------->
					<f:view>
						<hc:hmsForm id="LOGINUSER">	
					
	                    <div class="input-group">
							<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
        	                <div class="fg-line">
            	                <div class="select">
                				    <select chosen class="w-100" id="LognUser">
										<option value="0">Select User</option>
				                        <option value="United States">United States</option>
                				        <option value="United Kingdom">United Kingdom</option>
				                        <option value="Afghanistan">Afghanistan</option>
				                        <option value="Aland Islands">Aland Islands</option>
                				        <option value="Albania">Albania</option>
				                        <option value="Algeria">Algeria</option>
                				        <option value="American Samoa">American Samoa</option>
				                    </select>
                	            </div>
                    	    </div>
	                    </div>
						<p></p>
						<div class="input-group">
            	            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                	        <div class="fg-line">
                                <input type="text" class="form-control" placeholder="Employee Name" id="Empname">
                    	    </div>
	                    </div>
						<p></p>
						<div class="input-group">
            	            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                	        <div class="fg-line">
                                <input type="text" class="form-control" placeholder="Login ID" id="LoginID">
                    	    </div>
	                    </div>
						<p></p>
						<div class="input-group">
							<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
        	                <div class="fg-line">
            	                <div class="select">
                				    <select chosen class="w-100" id="EmployeeRole">
										<option value="0">Select User</option>
				                        <option value="United States">United States</option>
                				        <option value="United Kingdom">United Kingdom</option>
				                        <option value="Afghanistan">Afghanistan</option>
				                        <option value="Aland Islands">Aland Islands</option>
                				        <option value="Albania">Albania</option>
				                        <option value="Algeria">Algeria</option>
                				        <option value="American Samoa">American Samoa</option>
				                    </select>
                	            </div>
                    	    </div>
	                    </div>
						<p></p>
						<!-----------Date Picker----->
						<div class="input-group">						
					        <div class="card-body" data-ng-controller="DatepickerDemoCtrl">								
									<div class="fg-line">
    				                	<div class="date-picker input-group dp-blue" ng-class="{ 'is-opened': opened2 == true }">
											<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
					                        <div class="fg-line" ng-class="{ 'fg-toggled': opened2 == true }">
    	            				            <input ng-click="open($event, 'opened2')" type="text" class="form-control" uib-datepicker-popup="{{format}}" show-weeks="false" ng-model="dtPopup2" is-open="opened2" min-date="minDate" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="Select Date" id="tdydate"/>
					                        </div>
								     	</div>				
									</div>	                                                
							 	</div>
						 </div>
						<!-------end------------------>						
						<!--------Checkbox----------->
						<div class="input-group">
							<div class="checkbox">
								<div class="fg-line">
	    	                        <label>
    	    	                        <input type="checkbox" value="">
        	    	                    <i class="input-helper"></i>
            	    	                Remember me
                	    	        </label>
								</div>
                        	</div>
						</div>
						<!---------end-------------->
						<!----------Radio buttons--------->
						<label class="radio radio-inline m-r-20">
				            <input type="radio" name="inlineRadioOptions" value="option1">
			                <i class="input-helper"></i>1
			            </label>
			            <label class="radio radio-inline m-r-20">
            			    <input type="radio" name="inlineRadioOptions" value="option2">
			                <i class="input-helper"></i>2
			            </label>						
						<!------------End-------->
						<p></p>
						<!----------Toggle Switch---------->
						<div class="col-sm-4 m-b-20">
                    <div class="toggle-switch" data-ts-color="blue">
                        <label for="ts3" class="ts-label">Toggle Swith Blue</label>
                        <input id="ts3" type="checkbox" hidden="hidden">
                        <label for="ts3" class="ts-helper"></label>
                    </div>
                </div>
						<!------------End-------->	
						<p></p>						
						<!-----------File upload---->
						<div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file m-r-10">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="...">
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                    </div>												
						<!------------end------------>
						<p></p>
						<!------------Success and failure alerts--------->
						<div class="card">
					        <div class="card-header">
					            <h2>Dialog <small>A beautiful replacement for Javascript's boring Alert</small></h2>
					        </div>
					        <div class="card-body card-padding">
					            <div class="row dialog">
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">Basic Example</p>
					                    <button class="btn btn-info" data-swal-basic>Click me</button>
					                </div>
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">A title with a text under</p>
					                    <button class="btn btn-info" data-swal-text>Click me</button>
					                </div>
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">A success message!</p>
					                    <button class="btn btn-info" data-swal-success>Click me</button>
					                </div>
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">A warning message, with a function attached to the "Confirm"-button...</p>
					                    <button class="btn btn-info" data-swal-warning>Click me</button>
					                </div>
					            </div>
					            <br/><br/>
					            <div class="row dialog">
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">By passing a parameter, you can execute something else for "Cancel".</p>
					                    <button class="btn btn-info" data-swal-params>Click me</button>
					                </div>
					                <div class="col-sm-3">
					                    <p class="f-500 c-black m-b-20">A message with custom Image Header</p>
					                    <button class="btn btn-info" data-swal-img>Click me</button>
					                </div>
					                <div class="col-sm-3">
						                <p class="f-500 c-black m-b-20">A message with auto close timer</p>
					                    <button class="btn btn-info" data-swal-timer>Click me</button>
					                </div>
					            </div>
					        </div>
					    </div>						
						<!-----------end--------------------------------->
						<p></p>
						<!-------Editable table contents-->        
					    <div class="card">
					        <div class="card-header">
						         <h2>Editable <small>Click on Pencil icon to edit a row.</small></h2>
					        </div>						
							<div class="card-body">
					            <div class="table-responsive">
					                <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
						                <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                        					<td data-title="'ID'">
					                            <span ng-if="!w.$edit">1</span>
                    					        <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.id" /></div>
					                        </td>
					                        <td data-title="'Name'">
                    					        <span ng-if="!w.$edit">Suresh</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.name" /></div>
					                        </td>
                    					    <td data-title="'Email'">
					                            <span ng-if="!w.$edit">A@a.com</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.email" /></div>
					                        </td>
					                        <td data-title="'Username'">
					                            <span ng-if="!w.$edit">makella</span>
                    					        <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.username" /></div>
					                        </td>
					                        <td data-title="'Contact'">
                    					        <span ng-if="!w.$edit">9010567112</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.contact" /></div>
					                        </td>
					                        <td data-title="'Contact1'">
                    					        <span ng-if="!w.$edit">2</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.contact" /></div>
					                        </td>					
					                        <td data-title="'Contact'">
                    					        <span ng-if="!w.$edit">4</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.contact" /></div>
					                        </td>
					                        <td data-title="'Contact'">
                    					        <span ng-if="!w.$edit">6</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.contact" /></div>
					                        </td>
					                        <td data-title="'Contact'">
                    					        <span ng-if="!w.$edit">7</span>
					                            <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.contact" /></div>
					                        </td>
																	
                    					    <td data-title="'Actions'">
					                            <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					                            <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false"><i class="zmdi zmdi-check"></i></button>
                    					    </td>
					                    </tr>
					                </table>
					            </div>
					        </div>
						</div>
						<!----------end------------>	
						<!-----Preloaders---------->
					    <div class="card">
					        <div class="card-body card-padding">
					            <div class="preloader pl-xxl">
					                <svg class="pl-circular" viewBox="25 25 50 50">
					                    <circle class="plc-path" cx="50" cy="50" r="20" />
					                </svg>
					            </div>
					            <div class="preloader pl-xl">
					                <svg class="pl-circular" viewBox="25 25 50 50">
					                    <circle class="plc-path" cx="50" cy="50" r="20"/>
					                </svg>
					            </div>
					            <div class="preloader pl-lg">
					                <svg class="pl-circular" viewBox="25 25 50 50">
					                    <circle class="plc-path" cx="50" cy="50" r="20" />
					                </svg>
					            </div>
					            <div class="preloader">
					                <svg class="pl-circular" viewBox="25 25 50 50">
					                    <circle class="plc-path" cx="50" cy="50" r="20" />
					                </svg>
					            </div>
					            <div class="preloader pl-sm">
					                <svg class="pl-circular" viewBox="25 25 50 50">
					                    <circle class="plc-path" cx="50" cy="50" r="20" />
					                </svg>
					            </div>
					            <div class="preloader pl-xs">
					                <svg class="pl-circular" viewBox="25 25 50 50">
				    	                <circle class="plc-path" cx="50" cy="50" r="20" />
					                </svg>
					            </div>
					        </div>
					    </div>    
						<!-------End---------->
						<!--------Tabs-------->
						<div class="card">	
					        <div class="card-body card-padding" data-ng-controller="TabsDemoCtrl">
					            <tabset>
					                <tab heading="Home" active="tab.active" disable="tab.disabled">
					                  Home
					                </tab>
					                <tab heading="Profile" disable="tab.disabled">
					                  Profile
					                </tab>
					                <tab heading="Settings" disable="tab.disabled">
					                  Settings
					                </tab>									
					            </tabset>
					        </div>
					    </div>
						<!-------End---------->	
						<!---------Validations-->
						<div class="form-group asd">
			                <div class="fg-line">
            			        	<label class="control-label" for="inputWarning1">Input with warning</label>
				                    <input type="text" class="form-control" id="inputWarning1" required>
                			</div>
							<input type="button" value="asd" onclick="asdfg();" />
			            </div>			
						<!-------End----------->	
						<p></p>						
						<div class="input-group">
							<div class="fg-line">
								<a href="#" class="btn btn-primary btn-sm m-t-10">
									<h:commandButton type="submit" value='Update' actionListener="#{formBean.businessActionHandler.processAction}" action="#{formBean.doAction}" onclick="return formSubmit()" styleClass='btnok'/>
								</a>
							</div>
						</div>					
						<!---------hidden variables------------->						
						<hc:hmsJavaScriptComponent id="javascript"/>
						<h:inputHidden id="ID" value="#{formBean.fieldsMap.ID}"/>
						<h:inputHidden id="TABLENAME" value="#{formBean.fieldsMap.TABLENAME}"/>
						<h:inputHidden id="ADMINTYPE" value="#{formBean.fieldsMap.ADMINTYPE}"/>
						<h:inputHidden id="PageName" value="#{formBean.fieldsMap.PageName}" />

						<h:inputHidden id="ITEM_SELECTED" value="#{formBean.fieldsMap.ITEM_SELECTED}" />
						<h:inputHidden id="EMP_SALARY" value="#{formBean.fieldsMap.EMP_SALARY}" />
						<h:inputHidden id="EMP_LEAVES" value="#{formBean.fieldsMap.EMP_LEAVES}" />

						<h:inputHidden id="EMP_TYPE" value="#{formBean.fieldsMap.EMP_TYPE}" />
						<h:inputHidden id="BIRTH_DATE" value="#{formBean.fieldsMap.BIRTH_DATE}" />
						<h:inputHidden id="EMPGENDER" value="#{formBean.fieldsMap.EMPGENDER}" />
						<h:inputHidden id="EMP_DEPT_NAME" value="#{formBean.fieldsMap.EMP_DEPT_NAME}" />
						<h:inputHidden id="EMP_ROLE_NAME" value="#{formBean.fieldsMap.EMP_ROLE_NAME}" />
						<h:inputHidden id="EMPSTATUS" value="#{formBean.fieldsMap.EMPSTATUS}" />
						<h:inputHidden id="UPDATEFLAG" value="#{formBean.fieldsMap.UPDATEFLAG}" />
				<!---------end of hidden variables------------->												
														    			
</hc:hmsForm>
</f:view>
					<!-----end-->
                </div>
            </div>
        </div>
    </section>
</section>

<footer id="footer" data-ng-include="'template/footer.html'"></footer>
