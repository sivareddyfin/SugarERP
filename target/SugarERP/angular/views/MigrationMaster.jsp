	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="MigrationMaster">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Migration Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					     <form name="MigrationMasterForm" novalidate >
							
							 <!------<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateCircle();" >Migrate Circle Data</button>
													

												</div>
											</div>
								</div>
							</div>---->
							<br>
							 <!------<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateVillage();" >Migrate Village Data</button>

												</div>
											</div>
								</div>
							</div>--->
							<br>
							<!----<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateBank();" >Migrate Bank Data</button>

												</div>
											</div>
								</div>
								</div>
									<br/>
								<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateAgreement();" >Migrate Agreement Data</button>

												</div>
											</div>
								</div>
							</div>
							<br/>
							
								<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="getMigrateLoanData();" >Migrate Loan Data</button>

												</div>
											</div>
								</div>
							</div>
							<br/>
							
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="getOldRyotDataData();" >Migrate Old Ryot Data</button>

												</div>
											</div>
								</div>
							</div>
							
							
							<br/>
							
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateRyotNames();" >Update Ryot Names</button>

												</div>
											</div>
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="transportingMigration();" >Transpoting Migration</button>

												</div>
											</div>
								</div>
							</div>
							
							<br />
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="advancesMigation();">Migrate Advances</button>

												</div>
											</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="regionMigration();" >Region Migration</button>

												</div>
											</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="AdvancesToAccounts();" >Advances To Accounts Migration</button>

												</div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateLoanDate();" >Update Loan DisburseDate</button>

												</div>
									</div>
								</div>
							</div>	

							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateLoanPrinciplesDate();" >Update Loan Principle Date</button>

												</div>
									</div>
								</div>
							</div>		
							
							
							 <div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="AccountingMig();" >Accounting  Migration</button>

												</div>
									</div>
								</div>
							</div>	
							
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateloanSummary();" >Reconsile Loans And Advances</button>

												</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateZones();" >Update Zones</button>

												</div>
									</div>
								</div>
							</div>	---->

							<div class="row">
								
								
								<!--<div class="col-sm-6" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateAccounAdvances();" >Advance Accounting</button>
												</div>
									</div>
								</div>-->
								
								
								<!--<div class="col-sm-6" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="caneAccUpdationCheckLIst();" >Cane AccUpdation CheckLIst</button>
												</div>
									</div>
								</div>
								
								<div class="col-sm-6" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateloanDetails();" >Loan Migration</button>
												</div>
									</div>
								</div>
							
							</div>		

							
							
							<br /><br /><br />

							<div class="row">
								<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateAdvanceSummary();" >Update AdvanceSummary</button>

												</div>
									</div>
								</div>
							</div>--->
							
							<!--
							<div class="col-sm-6" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="UpdateMobileNos();" >Update Ryot Mobile Numbers</button>
												</div>
									</div>
							</div>
						
							<br /><br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateRyotCategory();" >Update ICICI Ryot</button>
										</div>
									</div>
							</div>
								
							<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="PurchaseTaxOfWeighmentMigration();" >PurchaseTax Of Weighment Migration</button>
												</div>
									</div>
								</div>
								
							</br></br>
							
							<div class="col-sm-12" align="left">
									<div class="input-group">
												<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="DedsCaStatusMigration();" >Update CaStatus in Deds</button>
												</div>
									</div>
								</div>
								
								<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateHarvesterCode();" >Update Harvesting Contractors</button>
										</div>
									</div>
							</div>
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateTransporterCode();" >Update Transport Contractors</button>
										</div>
									</div>
							</div>
							
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateSeedSupplierData();" >Update Seed Supplier Accounts</button>
										</div>
									</div>
							</div>
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateWeighmentAccounts();" >Update Weighment Accounts</button>
										</div>
									</div>
							</div>
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateCaneReceiptEndDates();" >Update Weighment CaneReceipt Dates</button>
										</div>
									</div>
							</div>
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateLoanPrinciplesForAccountedRyots();" >Update Loans For Accounted Ryots</button>
										</div>
									</div>
							</div>
							
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateOldLoans();" >Update Old Loans</button>
										</div>
									</div>
							</div>
							
							
							<br /><br />
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="loanReconsilation();" >Reconsile Loans</button>
										</div>
									</div>
							</div>
							<br /><br />
							-->
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="ssupplierMigration();" >SeedSupplierMigration</button>
										</div>
									</div>
							</div>
							
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateAccountsForSeedSupplierPerSeason();" >migrateAccountsForSeedSupplierPerSeason</button>
										</div>
									</div>
							</div>
							
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="PinCodeMigration();" >PinCodeMigration</button>
										</div>
									</div>
							</div>
							
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateBatchesForSeedSupplierPerSeason();" >migrateBatchesForSeedSupplierPerSeason</button>
										</div>
									</div>
							</div>
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="migrateBatchesInSeedSourceRecords();" >migrateBatchesInSeedSourceRecords</button>
										</div>
									</div>
							</div>
							<div class="col-sm-12" align="left">
									<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="updateSeedSupplierAccountsToCr();" >migrateUpdateSeedSupplierAccountsToCr</button>
										</div>
									</div>
							</div>
							
							</div>
						 </form>				
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
								        
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
