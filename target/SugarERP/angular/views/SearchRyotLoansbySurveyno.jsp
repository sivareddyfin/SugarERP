	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Search Ryot Loans by Survey Number</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="row">
								<div class="col-sm-2"></div>
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_surveynum">
												<option value="0">Select Survey Number</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-3"><a href="#/admin/admindata/masterdata/SearchRyotLoansbySurvey" class="btn btn-primary btn-sm m-t-10">Get Details</a></div>								
    	        			 </div><br /><hr />
							 <div class="block-header"><h5><b><u>Loan Details</u> : </b></h5></div>					 		
							 <div  class="table-responsive">
							 <table class="table table-striped">
							 		<thead>
								        <tr>
											<th>Season Year</th>
                		    				<th>Ryot No</th>
							                <th>Loan No</th>
											<th>Offer No</th>
		                    				<th>Bank Details</th>
											<th>Date of Loan</th>
											<th>Amount</th>
											<th>A/C No</th>
											<th>Status</th>
											
            							</tr>											
									</thead>
									<tbody>
										<tr>
											<td>2015-2016</td>
                		    				<td>123</td>
											<td>5</td>
							                <td>405</td>
		                    				<td>BOB</td>
											<td>11-01-2016</td>
											<td>100000</td>
											<td>12345</td>

											<td>Pending</td>
            							</tr>
										<tr>
											<td>2015-2016</td>
                		    				<td>123</td>
											<td>6</td>
							                <td>406</td>
		                    				<td>Axis Bank</td>
											<td>11-01-2016</td>
											<td>100000</td>
											<td>786954</td>
											
											<td>Pending</td>
            							</tr>
										<tr>
											<td>2015-2016</td>
                		    				<td>123</td>
											<td>7</td>
							                <td>407</td>
		                    				<td>SBI</td>
											<td>11-01-2016</td>
											<td>100000</td>
											<td>356795</td>
											
											<td>Pending</td>
            							</tr>
									</tbody>		
							 </table>
						  </div>
							 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section>
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
