<script type="text/javascript">
	var data = [{id:'1',desc:'Description',name:'Ramarao',burned:'Yes',newrate:'20',cane:'100',deduction:'2000'},{id:'2',desc:'Description',name:'Prasad Rao',burned:'No',newrate:'0',cane:'0',deduction:'2000'}];
	sessionStorage.setItem('data1',JSON.stringify(data));
</script>

	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
</style>
<script type="text/javascript">
	$('#autofocus').focus();
</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Other Deductions - Post Season</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>	
					 
					 		 <div class="row">
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="From Date"  maxlength="10"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="To Date"  maxlength="10"/>
			                	        </div>
			                    	</div>			                    
				                  </div>   															  
    	        			 </div><br />	
							 <div class='row'>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<div class="select">
												<select chosen class="w-100" id="input_ryotcode">
													<option value="0">Select Ryot Code</option>
													<option value="Ryot1">Ryot1</option>
													<option value="Ryot2">Ryot2</option>
												</select>
											</div>
			                	        </div>
			                    	</div>			                    
				                  </div>
									<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Name'  id="input_ryotname" maxlength="25" id="autofocus"/>
			                	        </div>
			                    	</div>			                    
				                  </div> 
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<select chosen class="w-100" id="input_ryotcode">
													<option value="0">Select Added Deductions</option>
													<option value="Ryot1">Ryot1</option>
													<option value="Ryot2">Ryot2</option>
											</select>
			                	        </div>
			                    	</div>			                    
				                  </div> 
							 </div><br>
							 <div class="row">
				                <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Serial Number"   maxlength="10"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Description' class="form-control" />
			                	        </div>
			                    	</div>			                    
				                  </div>
<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" placeholder='Deduction Amount' id="input_deductionvalue" class="form-control" maxlength="5"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								   															  
    	        			 </div><br />
							 
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/OtherDeductions" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/OtherDeductions" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Deductions</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Serial No.'">
                		    					<span ng-if="!w.$edit">{{w.id}}</span>
					    		                <div ng-if="w.$edit">
													<input type="text" class="form-control1" readonly />
												</div>
					            		      </td>
		                    				  <td data-title="'Description'">
							                     <span ng-if="!w.$edit">{{w.desc}}</span>
							                     <div ng-if="w.$edit">
												  <input class="form-control1" type="text" placeholder='Description' maxlength="25"/>
												 </div>
							                  </td>
							                  <td data-title="'Name'">
							                      <span ng-if="!w.$edit">{{w.name}}</span>
        		            					  <div ng-if="w.$edit">
												   <input class="form-control1" type="text" placeholder='Name' readonly maxlength="25"/>
												   </div>
							                  </td>
											  
											  <td data-title="'Deduction Amount'" style="text-align:center;">
							                      <span ng-if="!w.$edit">{{w.deduction}}</span>
        		            					  <div ng-if="w.$edit">
												  	<input class="form-control1" type="email" ng-model="w.country" placeholder='Deduction Value' id='input_dedctngrd' maxlength="5"/>
												  </div>
							                  </td>
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>						
														
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
