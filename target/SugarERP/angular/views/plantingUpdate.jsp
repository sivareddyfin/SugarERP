	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="plantUpdateController" ng-init="loadseasonNames();loadFieldOfficerDropdown();addFormField();loadRyotCodeDropDown();loadVarietyNames();loadusername();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Planting Update </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->				
					 <!-------body start------>						 	
					 	<form name="plantingForm"  novalidate ng-submit="PlantUpdateSubmit(plantupdate,plantingForm);">
							  <input type="hidden" name="modifyFlag" ng-model="plantupdate.modifyFlag" />
							  <div class="row">
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  data-ng-model='plantupdate.season' name="season" ng-options="season.season as season.season for season in seasonNames" data-ng-required="true " maxlength="10" >
																		<option value="">Season</option>
																	</select>	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  name="fieldOfficer{{$index}}" data-ng-model='plantupdate.fieldOfficer' ng-options="fieldOfficer.id as fieldOfficer.employeename for fieldOfficer in FieldOffNames | orderBy:'-employeename':true">
																	<option value="">Field Officers</option>																	</select>	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
														<label for="date">Date Of Entry</label>
														<input type="text" class="form-control" placeholder='Date Of Entry' id="date" with-floating-label  name="dateOfEntry" ng-model="plantupdate.dateOfEntry" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" />	
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>
										 
							
							  <br />
							
							 
							 
							   							
							 	 					 	
							 		
									
									
										
								
							
							
							 
							
								<div class="table-responsive">
							 	<table class="table">
							 		<thead>
								        <tr>
											<th>Action</th>
											
							                <th>Variety</th>
		                    				
											<th>Ryot Code</th>
											<th>No Of Seedlings </th>
											<th>Date Of Update</th>
											
            							</tr>											
									</thead>
									
									<tbody>
										<tr ng-repeat="plantData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="plantData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(plantData.id);" ng-if="plantData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>
												<input  type="hidden" ng-model="plantData.id" name="plantData{{plantData.id}}" />												
											</td>
											
													
													
													<td>
	
		
		<select class="form-control1" name="village" ng-model="plantData.variety" tabindex="14" ng-required="true" ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true">
													<option value="">Variety</option>
												</select>
		
								</td>
								
													
												<td>
								
																																																																																																																									
		
		
		<div class="fg-line">
														
														<select class="form-control1" name="ryotCode" data-ng-model='plantData.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-ryotCode':true" >
															<option value="">Ryot Code</option>
														</select>
				                			        </div>
												
													</td>     
											
											<td>
										<div class="form-group" ng-class="{ 'has-error' : plantingForm.noOfSeedling{{plantData.id}}.$invalid && (plantingForm.noOfSeedling{{plantData.id}}.$dirty || submitted)}">																																																																																																																																													
												<input type="text" class="form-control1"  placeholder="No of Seedlings"  name="noOfSeedling{{plantData.id}}" ng-model="plantData.noOfSeedling"  ng-pattern="/^[0-9]+$/" />
																						
							<p ng-show="plantingForm.noOfSeedling{{plantData.id}}.$error.pattern && (plantingForm.noOfSeedling{{plantData.id}}.$dirty || Addsubmitted)" class="help-block">Only Numbers</p>						
</div>	
											
											</td>
											
												<td style="border:none;"><div class="form-group"><input type="text" class="form-control1 datepicker" ng-model="plantData.dateofPlanting" name="dateofPlanting{{dateofPlanting.id}}" placeholder="Date" ng-mouseover="ShowDatePicker();" /></div>
											
											</td>
											
										
											
											</tr>
									</tbody>
							 	</table>
							</div>
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(plantingForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
