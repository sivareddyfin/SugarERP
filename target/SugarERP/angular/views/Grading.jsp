	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GradingController" ng-init="addFormField();loadVarietyNames();loadVillageNames();loadseasonNames();loadShift();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Grading</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="gradingForm" novalidate ng-submit="GradingSubmit(AddedGrading,gradingForm);">
							  <input type="hidden" ng-model="AddedGrading.modifyFlag" name="modifyFlag" />
							  <div class="row">
							 	<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.season.$invalid && (gradingForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedGrading.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="getAddedGradingList(AddedGrading.season,AddedGrading.gradingDate,AddedGrading.shift)">
															<option value="">Season</option>
						        			            </select>	
				        		        	        </div>
													<p ng-show="gradingForm.season.$error.required && (gradingForm.season.$dirty || submitted)" class="help-block">Season</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.gradingDate.$invalid && (gradingForm.gradingDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="gradingDate" ng-model="AddedGrading.gradingDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-blur="getAddedGradingList(AddedGrading.season,AddedGrading.gradingDate,AddedGrading.shift)"/>	
				        		        	        </div>
													<p ng-show="gradingForm.gradingDate.$error.required && (gradingForm.gradingDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : gradingForm.shift.$invalid && (gradingForm.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift" ng-required="true" data-placeholder="Shift" ng-model="AddedGrading.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames | orderBy:'-shiftName':true" ng-change="getAddedGradingList(AddedGrading.season,AddedGrading.gradingDate,AddedGrading.shift)" >
															<option value="">Shift</option>
															
														</select>
				                			        </div>
													<p ng-show="gradingForm.shift.$error.required && (gradingForm.shift.$dirty || submitted)" class="help-block">Select Shift</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								
							  </div>
							
							  <div class="row">
							  	<div class="col-sm-4">
									<div class="row">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourcharges" value="0" ng-model="AddedGrading.labourSpplr" ng-click="getLabourContractorDetails();" >
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedGrading.labourSpplr" ng-click="getLabourContractorDetails();">
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
								</div>

								 <div class='col-sm-4'>
								<div class="row">
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
														<select chosen class="w-100" name='lc'  ng-model="AddedGrading.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							</div>
</div>


							 </div>
							  </div>
							
							
							 
							  							
							 	<div ng-show="AddedGrading.labourSpplr=='0'">						
							  <div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
									</div>
								</div>
								
							 	<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Men</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : gradingForm.noOfMen.$invalid && (gradingForm.noOfMen.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfMen">No. of Men</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfMen" ng-model="AddedGrading.noOfMen" ng-keyup="LoadManTotal(AddedGrading.noOfMen,AddedGrading.manCost);getGrandTotalforGrading(AddedGrading.menTotal,AddedGrading.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfMen" with-floating-label />
												</div>
												
												<p ng-show="gradingForm.noOfMen.$error.pattern && (gradingForm.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : gradingForm.manCost.$invalid && (gradingForm.manCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="manCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost"  name="manCost" ng-model="AddedGrading.manCost" ng-keyup="LoadManTotal(AddedGrading.noOfMen,AddedGrading.manCost);getGrandTotalforGrading(AddedGrading.menTotal,AddedGrading.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="manCost" with-floating-label />
												</div>
												<p ng-show="gradingForm.manCost.$error.pattern && (gradingForm.manCost.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="menTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total"  name="menTotal" ng-model="AddedGrading.menTotal" readonly id="menTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : gradingForm.noOfWomen.$invalid && (gradingForm.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												   <label for="noOfWomen">No. of People</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfWomen" ng-model="AddedGrading.noOfWomen" ng-keyup="LoadWoManTotal(AddedGrading.noOfWomen,AddedGrading.womenCost);getGrandTotalforGrading(AddedGrading.menTotal,AddedGrading.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfWomen" with-floating-label />
												</div>
												
												<p ng-show="gradingForm.noOfWomen.$error.pattern && (gradingForm.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : gradingForm.womenCost.$invalid && (gradingForm.womenCost.$dirty || submitted)}">
												 <div class="fg-line">
												   <label for="womenCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="womenCost" ng-model="AddedGrading.womenCost" ng-keyup="LoadWoManTotal(AddedGrading.noOfWomen,AddedGrading.womenCost);getGrandTotalforGrading(AddedGrading.menTotal,AddedGrading.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="womenCost" with-floating-label />
												</div>
												
												<p ng-show="gradingForm.womenCost.$error.pattern && (gradingForm.womenCost.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="womenTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" ng-model="AddedGrading.womenTotal" readonly id="womenTotal" with-floating-label />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								<div class="row">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalLabourCost">Grand Total</label>
														<input type="text" class="form-control" placeholder="Grand Total" name="totalLabourCost" ng-model="AddedGrading.totalLabourCost"  readonly id="totalLabourCost" with-floating-label/>
												</div>
												</div>
												</div>
									</div>
									
								</div>
								</div>
								<div  ng-show="AddedGrading.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : gradingForm.costPerTon.$invalid && (gradingForm.costPerTon.$dirty || submitted)}">
							<div class="fg-line">
							  <label for="costPerTon">Cost Per Ton</label>
							<input type="text" placeholder="Cost Per Ton" ng-model="AddedGrading.costPerTon" class="form-control" name="costPerTon" ng-keyup="getContractTotalCost(AddedGrading.costPerTon,AddedGrading.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="costPerTon" with-floating-label  />
							</div>
							<p ng-show="gradingForm.costPerTon.$error.pattern && (gradingForm.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : gradingForm.totalTons.$invalid && (gradingForm.totalTons.$dirty || submitted)}">
							<div class="fg-line">
							 <label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons" ng-model="AddedGrading.totalTons" class="form-control" name="totalTons" ng-keyup="getContractTotalCost(AddedGrading.costPerTon,AddedGrading.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" id="totalTons" with-floating-label />
							</div>
							<p ng-show="gradingForm.totalTons.$error.pattern && (gradingForm.totalTons.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
									<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="fg-line">
									 <label for="totalContractAmt">Grand Total</label>
									<input type="text" placeholder="Grand Total" ng-model="AddedGrading.totalContractAmt" class="form-control" name="totalContractAmt" readonly id="totalContractAmt" with-floating-label />
									</div>
									</div>
									</div>
								</div>
								</div>	
									
									
									
										
									
							  
							
								<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
											<th>Action</th>
											<th>Batch Series</th>
							                <th>Variety</th>
		                    				<th>Supplier</th>
											<th>Village</th>
											<th>Total No.of Buds</th>
											<th>Transfer to tray filling</th>
											<th>Sent back to sprouting</th>
											<th>Disposed to rejection</th>
											<th>Germination % <span style="display:none;">(6/4*100)</span></th>
											<th>Remarks</th>
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data">
											<td>
											
											
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											
											<td style="display:none;">
											<input type="hidden" ng-model="batchData.id" name="batchid{{batchData.id}}" />
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.batchSeries{{batchData.id}}.$invalid && (gradingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
											<input list="stateList" placeholder="Batch Series" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{batchData.id}}" placeholder ="Batch No" ng-model="batchData.batch" ng-change="loadBatchDetails(batchData.batch,batchData.id);"> 
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
       						    </select>    
   									</datalist>

<p ng-show="gradingForm.batchSeries{{batchData.id}}.$error.required && (gradingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.batchSeries{{batchData.id}}.$error.pattern && (gradingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
										
</div>	


						</td>
						<td style='display:none;'>
						
<div class="form-group" ng-class="{ 'has-error' : gradingForm.variety{{batchData.id}}.$invalid && (gradingForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="variety{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.variety' name="variety" readonly>
													
<p ng-show="gradingForm.variety{{batchData.id}}.$error.required && (gradingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.variety{{batchData.id}}.$error.pattern && (gradingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.seedSupr{{batchData.id}}.$invalid && (gradingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="seedSupr{{batchData.id}}" ng-model="batchData.seedSupr"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="gradingForm.seedSupr{{batchData.id}}.$error.required && (gradingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.seedSupr{{batchData.id}}.$error.pattern && (gradingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.landVillageCode{{batchData.id}}.$invalid && (gradingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="landVillageCode{{batchData.id}}" placeholder="Village" data-ng-model='batchData.landVillageCode' name="landVillageCode" readonly/>
<p ng-show="gradingForm.landVillageCode{{batchData.id}}.$error.required && (gradingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.landVillageCode{{batchData.id}}.$error.pattern && (gradingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>


											<td>
						
<div class="form-group" ng-class="{ 'has-error' : gradingForm.varietyName{{batchData.id}}.$invalid && (gradingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="varietyName{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.varietyName' name="varietyName" readonly>
													
<p ng-show="gradingForm.varietyName{{batchData.id}}.$error.required && (gradingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.varietyName{{batchData.id}}.$error.pattern && (gradingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.ryotName{{batchData.id}}.$invalid && (gradingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="ryotName{{batchData.id}}" ng-model="batchData.ryotName"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="gradingForm.ryotName{{batchData.id}}.$error.required && (gradingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.ryotName{{batchData.id}}.$error.pattern && (gradingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.lvName{{batchData.id}}.$invalid && (gradingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="lvName{{batchData.id}}" placeholder="Village" data-ng-model='batchData.lvName'  readonly/>
<p ng-show="gradingForm.lvName{{batchData.id}}.$error.required && (gradingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.lvName{{batchData.id}}.$error.pattern && (gradingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.tubWt{{batchData.id}}.$invalid && (gradingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Total No.of buds" maxlength='10' name="tubWt{{batchData.id}}" ng-model="batchData.totalNoOfBuds" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/"  ng-keyup="getGermination(batchData.transferToTrayFilling,batchData.totalNoOfBuds,batchData.id);getdisposedtoRejection(batchData.totalNoOfBuds,batchData.transferToTrayFilling,batchData.sentBackToSprouting,batchData.id)"/>
<p ng-show="gradingForm.tubWt{{batchData.id}}.$error.required && (gradingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.tubWt{{batchData.id}}.$error.pattern && (gradingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.transferToTrayFilling{{batchData.id}}.$invalid && (gradingForm.transferToTrayFilling{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Transfer tray to filling"  name="transferToTrayFilling{{batchData.id}}" ng-model="batchData.transferToTrayFilling" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getGermination(batchData.transferToTrayFilling,batchData.totalNoOfBuds,batchData.id);getdisposedtoRejection(batchData.totalNoOfBuds,batchData.transferToTrayFilling,batchData.sentBackToSprouting,batchData.id);" />
<p ng-show="gradingForm.transferToTrayFilling{{batchData.id}}.$error.required && (gradingForm.transferToTrayFilling{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.transferToTrayFilling{{batchData.id}}.$error.pattern && (gradingForm.transferToTrayFilling{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.sentBackToSprouting{{batchData.id}}.$invalid && (gradingForm.sentBackToSprouting{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Sent back"  name="sentBackToSprouting{{batchData.id}}" ng-model="batchData.sentBackToSprouting" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getdisposedtoRejection(batchData.totalNoOfBuds,batchData.transferToTrayFilling,batchData.sentBackToSprouting,batchData.id)";  />
<p ng-show="gradingForm.sentBackToSprouting{{batchData.id}}.$error.required && (gradingForm.sentBackToSprouting{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.sentBackToSprouting{{batchData.id}}.$error.pattern && (gradingForm.sentBackToSprouting{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.disposedToRejection{{batchData.id}}.$invalid && (gradingForm.disposedToRejection{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Disposed to rejection"  name="disposedToRejection{{batchData.id}}" ng-model="batchData.disposedToRejection" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" />
<p ng-show="gradingForm.disposedToRejection{{batchData.id}}.$error.required && (gradingForm.disposedToRejection{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.disposedToRejection{{batchData.id}}.$error.pattern && (gradingForm.disposedToRejection{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.germinationPerc{{batchData.id}}.$invalid && (gradingForm.germinationPerc{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Germination"  name="germinationPerc{{batchData.id}}" ng-model="batchData.germinationPerc" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" />
<p ng-show="gradingForm.germinationPerc{{batchData.id}}.$error.required && (gradingForm.germinationPerc{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.germinationPerc{{batchData.id}}.$error.pattern && (gradingForm.germinationPerc{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.remarks{{batchData.id}}.$invalid && (gradingForm.remarks{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Remarks"  name="remarks{{batchData.id}}" ng-model="batchData.remarks" data-ng-required='true'  data-ng-pattern="/^[a-z A-Z 0-9.\s]*$/" />
<p ng-show="gradingForm.remarks{{batchData.id}}.$error.required && (gradingForm.remarks{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.remarks{{batchData.id}}.$error.pattern && (gradingForm.remarks{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											
											</tr>
									</tbody>
							 	</table>
							</div>
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(gradingForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
