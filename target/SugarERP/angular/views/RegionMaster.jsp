	<script type="text/javascript">	
		$('.autofocus').focus();				
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RegionMaster" ng-init='loadRegionId();loadAddedRegions();'>    
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Region Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					  	<form name="RegionMasterForm" ng-submit="RegionmasterSubmit(AddedRegionMaster,RegionMasterForm)"  novalidate>
						<input type="hidden" name="screenName" ng-model="AddedRegionMaster.screenName" />
							 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	<div class="fg-line">
										<label for="regionCode">Region Code</label>
											<input type="text" class="form-control autofocus" placeholder="Region Code"   readonly="readonly" name="regionCode"  data-ng-model="AddedRegionMaster.regionCode"       tabindex="1"  maxlength="25" id="regionCode" with-floating-label  >  
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        	<div class="fg-line">
										<label for="description">Description</label>
											<input type="text" class="form-control" placeholder="Description" maxlength="50"  name="description"  data-ng-model="AddedRegionMaster.description"       tabindex="2" ng-blur="spacebtw('description');" id="description" with-floating-label >
			                        	</div>
										

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>



							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" ng-class="{ 'has-error' : RegionMasterForm.region.$invalid && (RegionMasterForm.region.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="regionName">Region Name</label>
											<input type="text" class="form-control autofocus" placeholder="Region Name" maxlength="25" name="region"  data-ng-model="AddedRegionMaster.region"   data-ng-required="true"     tabindex="1"  data-ng-pattern="/^[a-zA-Z\s]*$/" autofocus ng-blur="spacebtw('region');validateDup();" id="regionName" with-floating-label > 
			                        	</div>
										<p ng-show="RegionMasterForm.region.$error.required && (RegionMasterForm.region.$dirty || Addsubmitted)" class="help-block"> Region Name is required</p>
										<p ng-show="RegionMasterForm.region.$error.pattern && (RegionMasterForm.region.$dirty || Addsubmitted)" class="help-block"> Valid Region Name is required</p>
										<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedRegionMaster.region!=null">Region Name Already Exist.</p>
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedRegionMaster.status" ng-init="AddedRegionMaster.status='Active'"    >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"   data-ng-model='AddedRegionMaster.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
									
								
			                        	</div>
										</div>

			                
			                    </div>			                    
			                </div>
							   
							    
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedRegionMaster.modifyFlag"  />
							 
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(RegionMasterForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
							
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Regions</b></h2></div>
					<div class="card">						
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 	
										<form name="regionmasteredit" novalidate>					
											<table ng-table="RegionMaster.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
											<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Region Code</span></th>
																<th><span>Region Name</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
												<tbody>
												<tr ng-repeat="Region in regionData"  ng-class="{ 'active': region.$edit }">
													<td>
													   <button type="button" class="btn btn-default" ng-if="!Region.$edit" ng-click="Region.$edit = true;Region.modifyFlag='Yes';Region.screenName='Region Master';"><i class="zmdi zmdi-edit"></i></button>
													   <button type="submit" class="btn btn-success btn-hideg" ng-if="Region.$edit" ng-click="regionUpdate(Region,$index);Region.$edit = isEdit" ><i class="zmdi zmdi-check"></i></button>
													   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Region.modifyFlag"  />
													   <input type="hidden" name="screenName{{$index}}" ng-model="Region.screenName" />
													 </td>
													 <td>
														<span ng-if="!Region.$edit">{{Region.regionCode}}</span>
														<div ng-if="Region.$edit">
															<div class="form-group">
																<input class="form-control" type="text" ng-model="Region.regionCode" placeholder='Region Code' maxlength="10" readonly/>
															</div>
														</div>
													  </td>
													  <td>
														 <span ng-if="!Region.$edit">{{Region.region}}</span>
														 <div class="form-group" ng-class="{ 'has-error' : regionmasteredit.region.$invalid && (regionmasteredit.region.$dirty || submitted)}">
														 <div ng-if="Region.$edit">
															<input class="form-control" type="text" ng-model="Region.region" placeholder='Region Name' maxlength="25" name="region" data-ng-pattern="/^[a-zA-Z\s]*$/" data-ng-required='true' ng-blur="spacebtwgrid('region',$index);validateDuplicate(Region.region,$index);"/>
															<p ng-show="regionmasteredit.region.$error.required && (regionmasteredit.region.$dirty || submitted)" class="help-block"> Region Name is required</p>
															<p ng-show="regionmasteredit.region.$error.pattern && (regionmasteredit.region.$dirty || submitted)" class="help-block"> Valid Region Name is required</p>
															<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Region.region!=null">Region Name Already Exist.</p>
															</div>
														 </div>
													  </td>
													  <td>
														  <span ng-if="!Region.$edit">{{Region.description}}</span>
														  <div ng-if="Region.$edit">
															<div class="form-group">
																<input class="form-control" type="text" ng-model="Region.description" placeholder='Description' maxlength="50" name="description" ng-blur="spacebtwgrid('description',$index)"/>
															  </div>
															 </div>
													  </td>
													  <td>
														  <span ng-if="!Region.$edit">
															<span ng-if="Region.status=='0'">Active</span>
															<span ng-if="Region.status=='1'">Inctive</span>
		
														  </span>
														  <div ng-if="Region.$edit">
															<div class="form-group">
															<label class="radio radio-inline m-r-20">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model="Region.status">
																<i class="input-helper"></i>Active
															</label>
															<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="Region.status">
																<i class="input-helper"></i>Inactive
															</label>				
															</div>		 							 												  
														  </div>
													  </td>
												 </tr>
												 </tbody>
												 </table>
												 </form>
									</div>
								</section>				 							 
							     </div>
							</div>
						</div>										
					</div>
					
				
	    </section>
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
