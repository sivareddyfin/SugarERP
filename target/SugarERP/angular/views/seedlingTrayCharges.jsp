	
<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeedlingTrayCharges" data-ng-init="loadseasonNames(); addFormField(); loadreturnId();loadTrayType();setVillageDropDown();" >   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Tray Charges</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<form name="SeedlingTrayChargesForm" ng-submit='AddSeedlingTrayChargesSubmit(AddedSeedlingTray,SeedlingTrayChargesForm);' novalidate>
					 		<div class="row">
															
							</div><br />
							<div class="row">
								
								<div class="col-sm-6">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.season.$invalid && (SeedlingTrayChargesForm.season.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingTray.season' name="season" ng-options="season.season as season.season for season in seasonNames" ng-change="getseasonRyots(AddedSeedlingTray.season);">
															<option value="">Select Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingTrayChargesForm.season.$error.required && (SeedlingTrayChargesForm.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>	
										 <input type="text" class="form-control" placeholder="Return ID"  maxlength="10" readonly name="returnId" tabindex="1" data-ng-model='AddedSeedlingTray.returnId' style="display:none;">
										 
										 <input type="text" class="form-control" placeholder="Login ID"  maxlength="10" readonly name="loginId" tabindex="1" data-ng-model='AddedSeedlingTray.loginId' style="display:none;">

										 <input type="text" name="totalAmount" data-ng-model="AddedSeedlingTray.totalAmount" style="display:none;"/>
										  <input type="text" name="modifyFlag" data-ng-model="AddedSeedlingTray.modifyFlag" style="display:none;"/>
										  <input type="hidden" name="screenName" data-ng-model="AddedSeedlingTray.screenName"/>

											


				                  </div>
								 <div class="col-sm-6">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.ryotCode.$invalid && (SeedlingTrayChargesForm.ryotCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
														<select chosen class="w-100"  tabindex="3" data-ng-required='true'  data-ng-model='AddedSeedlingTray.ryotCode' name="ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodeData  | orderBy:'-ryotCode':true" ng-change="loadSeedlingTrayFilter()">
															<option value="">Select Ryot Code</option>
								        	             </select>															
													</div>
		<p ng-show="SeedlingTrayChargesForm.ryotCode.$error.required && (SeedlingTrayChargesForm.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>		 
											  </div>
			                    	      </div>
										</div>			                    
				                  </div> 							
							</div><br />							
					 		<div class="row">
								<div class="table-responsive">
									<table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th>Action</th>
												<th>LVCode</th>
												<th>Tray Type</th>
												<th>No. of Trays</th>
												<th>Cost</th>
												<th>Total Cost</th>
												<th>Status</th>
												
											</tr>
										</thead>										
										<tbody>
											<tr ng-repeat='SeedlingData in data'>
												<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="SeedlingData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
<input type="hidden" name="screenName" data-ng-model="SeedlingData.screenName"/>		
												</td>
												
												<td>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.villageCode{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.villageCode{{SeedlingData.id}}.$dirty || submitted)}">
														<select  class="form-control1" name="villageCode{{SeedlingData.id}}" data-ng-model="SeedlingData.villageCode" data-ng-required="true" ng-options="villageCode.id as villageCode.village for villageCode in VillageData | orderBy:'-village':true" data-ng-required='true'>
															<option value="">Village Code</option>
														</select>
														<p ng-show="SeedlingTrayChargesForm.villageCode{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.villageCode{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required.</p>
													</div>
</td>

												<td>												
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.trayType{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.trayType{{SeedlingData.id}}.$dirty || submitted)}">
														<select  class="form-control1" name="trayType{{SeedlingData.id}}" data-ng-model="SeedlingData.trayTypeCode" data-ng-required="true" ng-options="trayTypeCode.id as trayTypeCode.tray for trayTypeCode in trayTypeNames" ng-change="loadSeedlingGridValues(SeedlingData.id,SeedlingData.villageCode,SeedlingData.trayTypeCode);" data-ng-required='true'>
															<option value="">Tray Type</option>
														</select>
														<p ng-show="SeedlingTrayChargesForm.trayType{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.trayType{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required.</p>
													</div>
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$dirty || submitted)}">																																																											
														<input type="text" class="form-control1" placeholder="No. of Trays" name="noofTrays{{SeedlingData.id}}"  data-ng-model="SeedlingData.noofTrays" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" maxlength="3" ng-keyup="getotalCost(SeedlingData.id,SeedlingData.noofTrays,SeedlingData.cost);"/>
														<p ng-show="SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
														<p ng-show="SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayChargesForm.noofTrays{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																				 													</div>												 
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
														<input type="text" class="form-control1" placeholder="Cost" name="cost{{SeedlingData.id}}" data-ng-model="SeedlingData.cost" data-ng-required='true'  maxlength="10" ng-keyup="getotalCost(SeedlingData.id,SeedlingData.noofTrays,SeedlingData.cost);" style="text-align:right;" data-ng-pattern="/^[0-9]{1,7}(\.[0-9]+)?$/" ng-blur="decimalpointgrid('cost',$index)" >
														<p ng-show="SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
														<p ng-show="SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayChargesForm.cost{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																
													</div>												 
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$dirty || submitted)}">																																																																																			
														<input type="text" class="form-control1" placeholder="Total Cost" name="totalCost{{SeedlingData.id}}" data-ng-model="SeedlingData.totalCost" data-ng-required='true'  maxlength="10" ng-keyup="getotalCost(SeedlingData.id,SeedlingData.noofTrays,SeedlingData.cost);" readonly="readonly" style="text-align:right;" ng-blur="decimalpointgrid('totalCost',$index)"/>
														<p ng-show="SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
														<p ng-show="SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayChargesForm.totalCost{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																
													</div>												  
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayChargesForm.status{{SeedlingData.id}}.$invalid && (SeedlingTrayChargesForm.status{{SeedlingData.id}}.$dirty || submitted)}">																																																																																																																																																																																			
														<select class="form-control1" name="status{{SeedlingData.id}}" data-ng-model="SeedlingData.status" data-ng-required='true' ng-options="status.id as status.status for status in StatusNames">
															<option value="">Status</option>								
														</select>
														<p ng-show="SeedlingTrayChargesForm.status{{SeedlingData.id}}.$error.required && (SeedlingTrayChargesForm.status{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>														
													</div>											
												</td>
											</tr>
											<tr>
										<td colspan='6' style='text-align:right;'><b>Sum :</b></td>
										<td>																																																																																			
											<input type="text" class="form-control1" placeholder="Sum" name="total" data-ng-model="total" data-ng-required='true'  maxlength="10" readonly style="text-align:right;" ng-blur="decimalpointgrid('total',$index)"/>												  
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-1"></div>
					</div>
					<div class="row" align="center">            			
						<div class="input-group">
							<div class="fg-line">
								<button type="submit" class="btn btn-primary btn-hide">Save</button>
								<button type="reset" class="btn btn-primary" ng-click="reset(SeedlingTrayChargesForm)">Reset</button>
							</div>
						</div>						 	
					</div>
				</form>							 							 
				<!------------------form end---------->							 
				<!-------from-end--------------------->		 						           			
			</div>
		</div>
	</div>
  </section>  
</section> 

<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
