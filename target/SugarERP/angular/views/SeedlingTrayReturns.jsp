	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Tray Returns</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_addedadvances">
												<option value="0">Select Ryot Code</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
    	        			 </div><br />
					 		<div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          	<div class="select">
										  	<select chosen class="w-100">
												<option value="0">Select Tray Type</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <input type="text" placeholder='Trays in Pending' class="form-control" maxlength="3">
			                	        </div>
			                    	</div>			                    
				                  </div>								  
    	        			 </div><br />
							<div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <input type="text" class="form-control"  placeholder='Returning Trays' maxlength="3"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <input type="text"  class="form-control" placeholder='Pending Trays'  disabled="disabled" maxlength="3"/>
			                	        </div>
			                    	</div>			                    
				                  </div>								  
    	        			 </div><br /> 							
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/TrayReturns" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/TrayReturns" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>							 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
