	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="trimmingController" ng-init="addFormField();addFormFieldBud();loadVarietyNames();loadVillageNames();loadseasonNames();ShiftDropDownLoad();loadShift();loadTrayType();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Trimming</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->				
					 <!-------body start------>						 	
					 	<form name="hardeningForm"  novalidate ng-submit="HardeningSubmit(AddedHardening,hardeningForm);">
							  <input type="hidden" name="modifyFlag" ng-model="AddedHardening.modifyFlag" />
							  <div class="row">
							  <div class="col-sm-6">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : hardeningForm.season.$invalid && (hardeningForm.season.$dirty || submitted)}">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  data-ng-model='AddedHardening.season' name="season" ng-options="season.season as season.season for season in seasonNames" data-ng-required="true " maxlength="10" ng-change ="getHardeningList(AddedHardening.season,AddedHardening.date,AddedHardening.shift);">
																		<option value="">Season</option>
																	</select>	
																</div>
																<p ng-show="hardeningForm.season.$error.required && (hardeningForm.season.$dirty || submitted)" class="help-block">Season</p>													
															</div>
														</div>
													</div>
										</div>
										
										<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.shift.$invalid && (hardeningForm.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift"  data-placeholder="Shift" ng-model="AddedHardening.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames |  orderBy:'-shiftName':true" ng-change ="getHardeningList(AddedHardening.season,AddedHardening.date,AddedHardening.shift);">
															<option value="">Shift</option>
															
														</select>
				                			        </div>
											<p ng-show="hardeningForm.shift.$error.required && (hardeningForm.shift.$dirty || submitted)" class="help-block">Select Shift</p>													
												</div>
					                    	</div>
										</div>
								</div>
										
										
										</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : hardeningForm.date.$invalid && (hardeningForm.date.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date"> Date</label>
														<input type="text" class="form-control" placeholder=' Date ' id="date" with-floating-label  name="caneWeighmentDate" ng-model="AddedHardening.date" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-blur ="getHardeningList(AddedHardening.season,AddedHardening.date,AddedHardening.shift);"/>	
				        		        	        </div>
													<p ng-show="hardeningForm.date.$error.required && (hardeningForm.date.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
									<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourcharges" value="0" ng-model="AddedHardening.labourSpplr" ng-click="getLabourContractorDetails();" >
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedHardening.labourSpplr" ng-click="getLabourContractorDetails();">
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>
											</div>
										</div>
								</div>
								
								
								
								
							  </div>
							
							  <div class="row">
								<div class="col-sm-6">
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
														<select chosen class="w-100" name='lc'  ng-model="AddedHardening.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							
								</div>
								
								
								</div>
								
							  </div>
							
							 
							 
							   							
							 	<div ng-show="AddedHardening.labourSpplr=='0'">						
							  <div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
									</div>
								</div>
								
							 	<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Men</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.noOfMen.$invalid && (hardeningForm.noOfMen.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfMen">No. of People</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfMen" ng-model="AddedHardening.noOfMen" ng-keyup="getMenTotal(AddedHardening.noOfMen,AddedHardening.manCost);getLabourGrandTotal(AddedHardening.menTotal,AddedHardening.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfMen" with-floating-label   />
												</div>
												
												<p ng-show="hardeningForm.noOfMen.$error.pattern && (hardeningForm.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.manCost.$invalid && (hardeningForm.manCost.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="manCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost"  name="manCost" ng-model="AddedHardening.manCost" ng-keyup="getMenTotal(AddedHardening.noOfMen,AddedHardening.manCost);getLabourGrandTotal(AddedHardening.menTotal,AddedHardening.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"/ id="manCost" with-floating-label >
												</div>
												<p ng-show="hardeningForm.manCost.$error.pattern && (hardeningForm.manCost.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="menTotal">No. of People</label>
														<input type="text" class="form-control" placeholder="Total" name="menTotal" ng-model="AddedHardening.menTotal" readonly="readonly" id="menTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
										<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.noOfWomen.$invalid && (hardeningForm.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="noOfWomen">No. of People</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfWomen" ng-model="AddedHardening.noOfWomen" ng-keyup="getWomanTotal(AddedHardening.noOfWomen,AddedHardening.womenCost);getLabourGrandTotal(AddedHardening.menTotal,AddedHardening.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfWomen" with-floating-label />
												</div>
												
												<p ng-show="hardeningForm.noOfWomen.$error.pattern && (hardeningForm.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
										<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.womenCost.$invalid && (hardeningForm.womenCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="womenCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="womenCost" ng-model="AddedHardening.womenCost" ng-keyup="getWomanTotal(AddedHardening.noOfWomen,AddedHardening.womenCost);getLabourGrandTotal(AddedHardening.menTotal,AddedHardening.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="womenCost" with-floating-label />
												</div>
												<p ng-show="hardeningForm.womenCost.$error.pattern && (hardeningForm.womenCost.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
										<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="womenTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" ng-model="AddedHardening.womenTotal" readonly="readonly" id="womenTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<input type="text" class="form-control" name="totalLabourCost" placeholder="Grand Total" ng-model="AddedHardening.totalLabourCost" readonly="readonly"   />
												</div>
												</div>
												</div>
									</div>
									
								</div>
								
								</div>
								<div  ng-show="AddedHardening.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.costPerTon.$invalid && (hardeningForm.costPerTon.$dirty || submitted)}">
							<div class="fg-line">
							<label for="costPerTon">Cost Per Ton</label>
							<input type="text" placeholder="Cost Per Ton" ng-model="AddedHardening.costPerTon" class="form-control" name="costPerTon" ng-keyup="getContractTotal(AddedHardening.costPerTon,AddedHardening.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="costPerTon" with-floating-label />
							</div>
							<p ng-show="hardeningForm.costPerTon.$error.pattern && (hardeningForm.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : hardeningForm.totalTons.$invalid && (hardeningForm.totalTons.$dirty || submitted)}">
							<div class="fg-line">
							<label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons" ng-model="AddedHardening.totalTons" name="totalTons" class="form-control" ng-model="AddedHardening.totalTons" ng-keyup="getContractTotal(AddedHardening.costPerTon,AddedHardening.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" id="totalTons" with-floating-label />
							</div>
							
							<p ng-show="hardeningForm.totalTons.$error.pattern && (hardeningForm.totalTons.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
									<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="fg-line">
									<label for="totalContractAmt">Total Tons</label>
									<input type="text" placeholder="Total Cost" ng-model="AddedHardening.totalContractAmt" class="form-control"  name="totalContractAmt" readonly="readonly" id="totalContractAmt" with-floating-label />
									</div>
									</div>
									</div>
								</div>
								
								</div>	
												 					 	
							 		
									
									
										
								
							
							
							 
							
								<div class="table-responsive">
							 	<!--<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
											<th>Action</th>
											<th>Batch</th>
							                <th>Variety</th>
		                    				
											<th>Tray Type In</th>
											<th>No Of TraysIn </th>
											<th>No of Cavities Per tray in</th>
											<th>Total Seedlings In</th>
											<th>Tray TypeOut</th>
											<th>No Of Trays Out</th>
											<th>No of Cavities Per tray Out</th>
											<th>Total SeedlingsOut</th>
											
            							</tr>											
									</thead>-->
									
									<table   border="1" style=" border-bottom-style:none;">
							 		<thead>
								        <tr style="color: #333333; font-weight:bold; outline: dotted #000000;">
                		    				<td rowspan="2">Action</td>
											<td rowspan="2">Batch</td>
							                <td rowspan="2">Variety</td>
		                    				
											
											
											<td colspan="4" align="center">Trays In</td>
											<td colspan="4" align="center">Trays Out</td>
												
            							</tr>
										
										<tr style="font-weight:bold; ">
											<td >Tray Type</td><td>No. Of Trays</td><td>No of Cavities Per tray</td><td>Total Seedlings</td><td>Tray Type</td><td>No. of Trays</td><td>No of Cavities Per tray</td><td>Total Seedlings</td>
										</tr>
																				
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td style="display:none;">
													<input type="hidden" ng-model="batchData.id" name="batchid{{batchData.id}}" />
													</td>
													<td>
		<div class="form-group" ng-class="{ 'has-error' : hardeningForm.batchSeries{{batchData.id}}.$invalid && (hardeningForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
													<input list="stateList" placeholder="Batch No" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{batchData.id}}" placeholder ="Batch No" ng-model="batchData.batch" ng-change="loadBatchDetails(batchData.batch,batchData.id);"> 
											<datalist id="stateList">
											<select class="form-control1">
										 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
										</select>    

											</datalist>
		
		<p ng-show="hardeningForm.batchSeries{{batchData.id}}.$error.required && (hardeningForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="hardeningForm.batchSeries{{batchData.id}}.$error.pattern && (hardeningForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
												
		</div>	
		
		
								</td>
								
													
												<td style='display:none;'>
								
		<div class="form-group" ng-class="{ 'has-error' : hardeningForm.variety{{batchData.id}}.$invalid && (hardeningForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
		<input type='text' class="form-control1" name="variety{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.variety' name="variety" readonly>
															
		<p ng-show="hardeningForm.variety{{batchData.id}}.$error.required && (hardeningForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="hardeningForm.variety{{batchData.id}}.$error.pattern && (hardeningForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		</div>												
													</td>
													
													<td>
								
		<div class="form-group" ng-class="{ 'has-error' : hardeningForm.varietyName{{batchData.id}}.$invalid && (hardeningForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
		<input type='text' class="form-control1" name="varietyName{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.varietyName' name="variety" readonly>
															
		<p ng-show="hardeningForm.varietyName{{batchData.id}}.$error.required && (hardeningForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="hardeningForm.varietyName{{batchData.id}}.$error.pattern && (hardeningForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		</div>												
													</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.trayTypeIn{{batchData.id}}.$invalid && (hardeningForm.trayTypeIn{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<!--<input type="text" class="form-control1"  placeholder="TrayType In"  name="trayTypeIn{{batchData.id}}" ng-model="batchData.trayTypeIn"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/"/>-->
												
												<select class="form-control1" name="trayTypeIn{{batchData.id}}" ng-model="batchData.trayTypeIn"  ng-options="trayTypeIn.id as trayTypeIn.TrayType for trayTypeIn in TrayTypeNames">
												<option value="">Tray Type</option>
												</select>
<p ng-show="hardeningForm.trayTypeIn{{batchData.id}}.$error.required && (hardeningForm.trayTypeIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.trayTypeIn{{batchData.id}}.$error.pattern && (hardeningForm.trayTypeIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.noOfTraysIn{{batchData.id}}.$invalid && (hardeningForm.noOfTraysIn{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="No.of Trays In"  name="noOfTraysIn{{batchData.id}}" ng-model="batchData.noOfTraysIn"   data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getTotalSeedlingsIn(batchData.noOfTraysIn,batchData.noOfCavitiesPertrayin,batchData.id)"/>
<p ng-show="hardeningForm.noOfTraysIn{{batchData.id}}.$error.required && (hardeningForm.noOfTraysIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.noOfTraysIn{{batchData.id}}.$error.pattern && (hardeningForm.noOfTraysIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$invalid && (hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="CavitiesPertrayIn"  name="noOfCavitiesPertrayin{{batchData.id}}" ng-model="batchData.noOfCavitiesPertrayin"   data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getTotalSeedlingsIn(batchData.noOfTraysIn,batchData.noOfCavitiesPertrayin,batchData.id)"/>
<p ng-show="hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$error.required && (hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$error.pattern && (hardeningForm.noOfCavitiesPertrayin{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.totalSeedlingsIn{{batchData.id}}.$invalid && (hardeningForm.totalSeedlingsIn{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="TotalSeedlingsIn"  name="totalSeedlingsIn{{batchData.id}}" ng-model="batchData.totalSeedlingsIn"   data-ng-pattern="/^[0-9.\s]*$/" readonly/>
<p ng-show="hardeningForm.totalSeedlingsIn{{batchData.id}}.$error.required && (hardeningForm.totalSeedlingsIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.totalSeedlingsIn{{batchData.id}}.$error.pattern && (hardeningForm.totalSeedlingsIn{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.trayTypeOut{{batchData.id}}.$invalid && (hardeningForm.trayTypeOut{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<!--<input type="text" class="form-control1"  placeholder="TrayTypeOut" name="trayTypeOut{{batchData.id}}" ng-model="batchData.trayTypeOut"   data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/"/>-->
												
												<select class="form-control1" name="trayTypeOut{{batchData.id}}" ng-model="batchData.trayTypeOut"  ng-options="trayTypeOut.id as trayTypeOut.TrayType for trayTypeOut in TrayTypeNames">
												<option value="">Tray Type</option>
												</select>
<p ng-show="hardeningForm.trayTypeOut{{batchData.id}}.$error.required && (hardeningForm.trayTypeOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.trayTypeOut{{batchData.id}}.$error.pattern && (hardeningForm.trayTypeOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.noOfTraysOut{{batchData.id}}.$invalid && (hardeningForm.noOfTraysOut{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="No. of Trays Out" name="noOfTraysOut{{batchData.id}}" ng-model="batchData.noOfTraysOut"   data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getTotalSeedlingsOut(batchData.noOfTraysOut,batchData.noOfCavitiesPertrayOut,batchData.id)"/>
<p ng-show="hardeningForm.noOfTraysOut{{batchData.id}}.$error.required && (hardeningForm.noOfTraysOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.noOfTraysOut{{batchData.id}}.$error.pattern && (hardeningForm.noOfTraysOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$invalid && (hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="CavitiesPertrayOut" name="noOfCavitiesPertrayOut{{batchData.id}}" ng-model="batchData.noOfCavitiesPertrayOut"   data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getTotalSeedlingsOut(batchData.noOfTraysOut,batchData.noOfCavitiesPertrayOut,batchData.id)"/>
<p ng-show="hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$error.required && (hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$error.pattern && (hardeningForm.noOfCavitiesPertrayOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : hardeningForm.totalSeedlingsOut{{batchData.id}}.$invalid && (hardeningForm.totalSeedlingsOut{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="TotalSeedlingsOut" name="totalSeedlingsOut{{batchData.id}}" ng-model="batchData.totalSeedlingsOut"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" readonly/>
<p ng-show="totalSeedlingsOut.totalSeedlingsOut{{batchData.id}}.$error.required && (hardeningForm.totalSeedlingsOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="totalSeedlingsOut.totalSeedlingsOut{{batchData.id}}.$error.pattern && (hardeningForm.totalSeedlingsOut{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											</tr>
									</tbody>
							 	</table>
							</div>
							<br />
							
							<div class="row">
										<div class="col-sm-3">
										</div>
										 <div class="col-sm-5">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<input type="text" class="form-control" placeholder="Grand Total In" name="grandTotalIn" ng-model="AddedHardening.grandTotalIn" readonly  />
												</div>
												</div>
												</div>
									</div>
									
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<input type="text" class="form-control" placeholder="Grand Total Out" name="grandTotalOut" ng-model="AddedHardening.grandTotalOut" readonly  />
												</div>
												</div>
												</div>
									</div>
								</div>
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(hardeningForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
