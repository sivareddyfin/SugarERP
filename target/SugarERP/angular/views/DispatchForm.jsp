<script type="text/javascript">		
		$('.autofocus').focus();				
	</script>
<style>
.styletr {border-bottom: 1px solid grey;}


</style>
	
	
	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( ".date" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		 
		 $( "#deliveryDate" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy',
			   minDate: 0
			 
	     });
		 
		 $('.Time').timepicker({ 'scrollDefault': 'now' });
		 	 	
		 	 		 
		
	  });
	  
	  
	
    </script>	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="DispatchFormController" ng-init="loadRyotCodeDropDown();loadFieldOfficerDropdown();loadFieldAssistants();loadseasonNames();loadVarietyNames();loadKindType();loadTrayType();getTransportContratorDetails();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Dispatch Advise</b></h2></div>
			    <div class="card">
			        <div  class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 
					 
					 
					 
					    <form name="DispatchForm" novalidate ng-submit="AddedDispatchSubmit(AddedDispatchForm,DispatchForm);">
							 <div class="row">
									<div class="col-sm-2">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : DispatchForm.season.$invalid && (DispatchForm.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedDispatchForm.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="loadGetMaxDispatchNo(AddedDispatchForm.season);">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="DispatchForm.season.$error.required && (DispatchForm.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
											</div>
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : DispatchForm.dispatchNo.$invalid && (DispatchForm.dispatchNo.$dirty || Addsubmitted)}">
											  <div class="fg-line">
												
													<input type="text" placeholder="Dispatch Advise No." name="dispatchNo" class="form-control autofocus" ng-model="AddedDispatchForm.dispatchNo" data-ng-required="true" readonly  tabindex="2" />
												
											 </div>
								<p ng-show="DispatchForm.dispatchNo.$error.required && (DispatchForm.dispatchNo.$dirty || Addsubmitted)" class="help-block">Dispatch No. is required</p>										
										   </div>
									   </div>
										
									</div>
									<div class="col-sm-3">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group floating-label-wrapper" >
												
												<div class="fg-line">
												
													<input type="text"  placeholder="Date of Dispatch" class="form-control date " id="dateOfDispatch" name="dateOfDispatch" ng-model="AddedDispatchForm.dateOfDispatch" readonly="readonly" data-ng-required="true"  tabindex="3"  />
												
											 </div>
												
												
						        	              
					
																						
												</div>
			            		        	</div>
										</div>
										<div class="col-sm-4">
								      <div class="input-group">
													<div class="form-group" >
														<div class="fg-line nulvalidate">
										<br />
															 <label class="radio radio-inline m-r-20"><b>Is Main Agreement</b></label>	
															 <label class="radio radio-inline m-r-20">
																<input type="radio" name="agrType" value="0"   data-ng-model="AddedDispatchForm.agrType" >
																<i class="input-helper"></i>Yes
															</label>
															<label class="radio radio-inline m-r-20">
															<input type="radio" name="agrType" value="1"   data-ng-model="AddedDispatchForm.agrType">
															<i class="input-helper"></i>No
															</label>
										
														</div>
												</div>
										</div>		
								</div>
								</div>
								
								<div class="row">
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : DispatchForm.ryotcode.$invalid && (DispatchForm.ryotcode.$dirty || Addsubmitted)}">
										  
										  <div class="fg-line">
												<div class="select">
												<select chosen class="w-100" name="ryotcode" ng-model="AddedDispatchForm.ryotcode"  ng-options="ryotcode.id as ryotcode.id for ryotcode in ryotCode | orderBy:'-id':true" ng-required='true' ng-change="loadRyotDet(AddedDispatchForm.ryotcode);loadIndents(AddedDispatchForm.season,AddedDispatchForm.ryotcode);getaggrementDetails(AddedDispatchForm.season,AddedDispatchForm.ryotcode,AddedDispatchForm.agrType);"  tabindex="4">
													<option value="">Ryot Code</option>
												</select>													  
												</div>
			    		            		</div>
										
								<p ng-show="DispatchForm.ryotcode.$error.required && (DispatchForm.ryotcode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>										
										   </div>
									   </div>
										
									</div>
									<div class="col-sm-3">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
											<div class="form-group" ng-class="{ 'has-error' : DispatchForm.agreementnumber.$invalid && (DispatchForm.agreementnumber.$dirty || Addsubmitted)}">										
				        	                	<div class="fg-line">
	    	        					         <select  chosen class="w-100" name='agreementnumber' ng-model="AddedDispatchForm.agreementnumber" ng-options="agreementnumber.agreementnumber as agreementnumber.agreementnumber for agreementnumber in agreementnumberNames" ng-change="getSquantityForAgreement(AddedDispatchForm.season,AddedDispatchForm.agreementnumber,AddedDispatchForm.agrType);"  tabindex="5">
												 <option value="">Agreement Number</option>
												 </select>
			        	        	    	    </div>
		<p ng-show="DispatchForm.agreementnumber.$error.required && (DispatchForm.agreementnumber.$dirty || Addsubmitted)" class="help-block">Select Agreement Number</p>																									
											</div>
										</div>
			                    	</div>



										</div>
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : DispatchForm.foCode.$invalid && (DispatchForm.foCode.$dirty || Addsubmitted)}">
										 <div class="fg-line">
														 <select chosen class="w-100" name="foCode" data-ng-model='AddedDispatchForm.foCode' data-ng-required='true' ng-options="foCode.id as foCode.fieldOfficer for foCode in FieldOffNames  | orderBy:'-fieldOfficer':true"   tabindex="6">
														<option value="">Field Officer</option>
				        				            </select>
				    		            	        </div>
										 
											 
								<p ng-show="DispatchForm.foCode.$error.required && (DispatchForm.foCode.$dirty || Addsubmitted)" class="help-block">Select Field Officer</p>										
										   </div>
									   </div>
										
									</div>
									<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DispatchForm.faCode.$invalid && (DispatchForm.faCode.$dirty || Addsubmitted)}">
												<div class="fg-line">
														 <select chosen class="w-100" name='faCode' ng-model="AddedDispatchForm.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true" tabindex="7">
														<option value="">Field Assistant</option>
														
														</select>
				    		            	        </div>
														
				    		            	        
					<p ng-show="DispatchForm.faCode.$error.required && (DispatchForm.faCode.$dirty || Addsubmitted)" class="help-block">Field Assistant is Required.</p>
																						
												</div>
			            		        	</div>
										</div>
									
									
								</div>
								
							
							<div class="row">
								<div class="col-sm-4">
									
									
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group" ng-class="{ 'has-error' : DispatchForm.ryotName.$invalid && (DispatchForm.ryotName.$dirty || Addsubmitted)}">
						        	      <div class="fg-line">
    	    		    					
											 <input type="text" class="form-control" placeholder="Ryot Name" readonly="readonly" ng-model="AddedDispatchForm.ryotName" tabindex="8" />	
											
					    	             </div>
						 <p ng-show="DispatchForm.ryotName.$error.required && (DispatchForm.ryotName.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>
									   </div>
					        	    </div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
														<label for="phone">Ryot Phone Number</label>
														<input type="text" class="form-control" placeholder="Ryot Phone Number"  id="phone" with-floating-label name="phone" ng-model="AddedDispatchForm.ryotPhoneNumber" data-ng-required="true"  tabindex="11"  />
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
											<div class="form-group" ng-class="{ 'has-error' : DispatchForm.seedlingQty.$invalid && (DispatchForm.seedlingQty.$dirty || Addsubmitted)}">										
				        	                	<div class="fg-line">
	    	        					         <input type="text" class="form-control" ng-model="AddedDispatchForm.seedlingQty" name="seedlingQty" placeholder="Allowed Seedling Qty" data-ng-required="true" readonly tabindex="14"/>
			        	        	    	    </div>
		<p ng-show="DispatchForm.seedlingQty.$error.required && (DispatchForm.seedlingQty.$dirty || Addsubmitted)" class="help-block">SeedlingQty is Required.</p>																									
											</div>
										</div>
			                    	</div>
								</div>
							</div>
									
									
									
									<div class="row">
										<!--Dispatch Date-->
									</div>
									
									
									<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Land Survey</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="landSurvey" value="0"   data-ng-model="AddedDispatchForm.landSurvey" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="landSurvey" value="1"   data-ng-model='AddedDispatchForm.landSurvey'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>
									</div>
									</div>
									
									
								</div>
								
								<div class="col-sm-4">
									
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
						        	         <div class="fg-line">
												<select chosen class="w-100" ng-model="AddedDispatchForm.indents" ng-options="indents.IndentNo as indents.IndentNo for indents in IndentData" data-ng-required="true" ng-change="loadIndentPopup(AddedDispatchForm.indents);" tabindex="9">
														<option value="">Indents</option>
														
														</select>
													
												
											 </div>
										</div>
			            		    </div>
										</div>
									</div>
									<div class="row">
										
										<div class="col-sm-12">
											<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper">
														<div class="form-group">									
															<div class="fg-line">
															  <select chosen class="w-100" ng-model="AddedDispatchForm.lc" name='lc' ng-options='lc.lcCode as lc.lcName for lc in lcNames' tabindex="12">
															  <option value="">Transport Contractor</option>
															  </select>
															</div>
								
																													
														</div>							
													  </div>
												</div>
										</div>
										
									</div>
									<div class="row">
									
										
										<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
											 		<div class="form-group" ng-class="{ 'has-error' : DispatchForm.driverName.$invalid && (DispatchForm.driverName.$dirty || Addsubmitted)}">									
						        	                	<div class="fg-line">
														  <label for="drivername">Driver Name</label>  		
    		    		    					        <input type="text" class="form-control" placeholder="Driver Name" data-ng-model="AddedDispatchForm.driverName" id="drivername" with-floating-label name="driverName" data-ng-required="true" tabindex="15" />
					    		            	        </div>
							<p ng-show="DispatchForm.driverName.$error.required && (DispatchForm.driverName.$dirty || Addsubmitted)" class="help-block">Driver Name is Required.</p>																											
							<p ng-show="DispatchForm.driverName.$error.pattern && (DispatchForm.driverName.$dirty || Addsubmitted)" class="help-block">Enter Valid Name.</p>
																												
													</div>							
												  </div>
			            		        	</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper">
														<div class="form-group" ng-class="{ 'has-error' : DispatchForm.drPhoneNo.$invalid && (DispatchForm.drPhoneNo.$dirty || Addsubmitted)}">									
															<div class="fg-line">
															  <label for="drPhoneNo">Driver Phone Number</label>  		
															<input type="text" class="form-control" placeholder="Driver Number" data-ng-model="AddedDispatchForm.drPhoneNo" id="drPhoneNo" with-floating-label name="drPhoneNo" data-ng-required="true" tabindex="16" maxlength="10" data-ng-pattern="/^[0-9\s]*$/" />
															</div>
								<p ng-show="DispatchForm.drPhoneNo.$error.required && (DispatchForm.drPhoneNo.$dirty || Addsubmitted)" class="help-block">Driver Phone Number is Required.</p>																											
								<p ng-show="DispatchForm.drPhoneNo.$error.pattern && (DispatchForm.drPhoneNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Driver Phno.</p>
																													
														</div>							
													  </div>
												</div>
										</div>
										
									</div>
									
									<div class="row">
										<div class="col-sm-12">	
											<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />	
									 					 <label class="radio radio-inline m-r-20"><b>Agreement Signed</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="agreementSign" value="0"   data-ng-model="AddedDispatchForm.agreementSign" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="agreementSign" value="1"   data-ng-model='AddedDispatchForm.agreementSign'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>
										</div>
									</div>
									

							
									
								</div>


								
								
								
								<div class="col-sm-4">
								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
						        	         <div class="fg-line">
												<input type='text' class='form-control' name="indentNumbers" ng-model="AddedDispatchForm.indentNumbers" placeholder="Indent Numbers"  tabindex="10">
													
												
											 </div>
										</div>
			            		    </div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
											 		<div class="form-group" ng-class="{ 'has-error' : DispatchForm.vehicle.$invalid && (DispatchForm.vehicle.$dirty || Addsubmitted)}">
						        	                	<div class="fg-line">
														  <label for="Vehicle">Vehicle No.</label>  		
    		    		    					          <input type="text" class="form-control" placeholder='Vehicle No.' maxlength="10" id="Vehicle" with-floating-label name="vehicle" ng-model="AddedDispatchForm.vehicle " data-ng-required="true" tabindex="13"/>
					    		            	        </div>
							<p ng-show="DispatchForm.vehicle.$error.required && (DispatchForm.vehicle.$dirty || Addsubmitted)" class="help-block">Vehicle No. is Required.</p>																											
							<p ng-show="DispatchForm.vehicle.$error.pattern && (DispatchForm.vehicle.$dirty || Addsubmitted)" class="help-block">Enter Valid Vehicle No..</p>
																												
													</div>							
												  </div>
			            		        	</div>
										</div>
									</div>
									
									<div class="row">
										
										<div class="col-sm-6">
											<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
													<div class="form-group floating-label-wrapper">
														<div class="form-group" ng-class="{ 'has-error' : DispatchForm.deliveryDate.$invalid && (DispatchForm.deliveryDate.$dirty || Addsubmitted)}">									
															<div class="fg-line">
															  <label for="deliveryDate">Delivery Date</label>  		
															<input type="text" class="form-control" placeholder="Delivery Date" data-ng-model="AddedDispatchForm.deliveryDate" id="deliveryDate" with-floating-label name="deliveryDate" data-ng-required="true" tabindex="17"/>
															</div>
								<p ng-show="DispatchForm.deliveryDate.$error.required && (DispatchForm.deliveryDate.$dirty || Addsubmitted)" class="help-block">deliveryDate is Required.</p>																											
								<p ng-show="DispatchForm.deliveryDate.$error.pattern && (DispatchForm.deliveryDate.$dirty || Addsubmitted)" class="help-block">Enter Valid deliveryDate.</p>
																													
														</div>							
													  </div>
												</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
													<div class="form-group floating-label-wrapper">
														<div class="form-group" ng-class="{ 'has-error' : DispatchForm.deliveryTime.$invalid && (DispatchForm.deliveryTime.$dirty || Addsubmitted)}">									
															<div class="fg-line">
															  <label for="deliveryTime">Delivery Time</label>  		
															<input type="text" class="form-control Time" placeholder="Delivery Time" data-ng-model="AddedDispatchForm.deliveryTime" id="deliveryTime" with-floating-label name="deliveryTime" data-ng-required="true" tabindex="17" />
															</div>
								<p ng-show="DispatchForm.deliveryTime.$error.required && (DispatchForm.deliveryTime.$dirty || Addsubmitted)" class="help-block">DeliveryTime is Required.</p>																											
								<p ng-show="DispatchForm.deliveryTime.$error.pattern && (DispatchForm.deliveryTime.$dirty || Addsubmitted)" class="help-block">Enter Valid DeliveryTime.</p>
																													
														</div>							
													  </div>
												</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Soil & Water Analysis</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="soilwateranalysis" value="0"   data-ng-model="AddedDispatchForm.soilWaterAnalyis" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="soilwateranalysis" value="1"   data-ng-model='AddedDispatchForm.soilWaterAnalyis'>
			    				            			<i class="input-helper"></i>No
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>	
										</div>
									</div>
									
									
									
								</div>
							</div>
							 
							 <hr />
			                
								<div class="table-responsive"  ng-if="data!=0" id='hidetable'>
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table style="border-collapse:collapse; width:100%;">								                  
													<thead>
														<tr class="styletr" style="font-weight:bold;" >
															<th><span>Action</span></th>
															<th><span></span></th>
															<th><span>Indent No.</span></th>
															<th><span>Variety</span></th>
															<th><span>Batch</span></th>
															<th><span>Pending Qty(Batch)</span></th>
															<th><span>Cost</span></th>
															<th><span>Indent qty</span></th>
															<th><span>Dis. <br />Seedlings.</span></th>
												
															
															<th><span>Total <br />Cost</span></th>
															<th><span>No. of <br /> Trays</span></th>
															<th><span>Tray <br />Type</span></th>
															<th><span>Tray <br />Cost</span></th>
															<th><span>Total</span></th>
															<th><span>Status</span></th>
														</tr>
														
													</thead>
													<tbody>
													
													<tr ng-repeat="dispatchData in data"  class="styletr" style="height:60px;"  >
												
											 <td>
											   <!--<button type="button" id="right_All_1" class="btn btn-primary" ng-click='duplicateField(dispatchData.indentNumbers,dispatchData.variety,dispatchData.seedlingsVal,dispatchData.noOfSeedlings,dispatchData.costOfEachItem,dispatchData.totalCost);'  ng-show=$index== 0"><span class="glyphicon glyphicon-repeat"></span></button>-->
											  <button type="button" id="right_All_1" class="btn btn-primary" ng-click='duplicateField(dispatchData.indentNumbers,dispatchData.variety,dispatchData.seedlingsVal,dispatchData.noOfSeedlings,dispatchData.costOfEachItem,dispatchData.totalCost,dispatchData.id);' ng-if="dispatchData.id == '1'"><i class="glyphicon glyphicon-repeat"></i></button>												

											   <button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(dispatchData.id);" ><i class="glyphicon glyphicon-remove-sign"></i></button>
											   
												<input type="hidden" ng-model="dispatchData.id" name="id{{$index}}" />
											 
											 </td>
											 <td>
												
											   												
											</td>


													<td><input type="text" class="form-control1"  placeholder="Indent"  name="indentNumbers{{index}}" data-ng-model="dispatchData.indentNumbers" size="11" /></td>
													<td>
													 <select class="form-control1" name="variety{{$index}}" ng-model="dispatchData.variety"  ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true" readonly="readonly">
													<option value="">Variety</option>
												</select>
													</td>
													
													<td><input  list="stateList" placeholder="Batch No" id="ryotcodeorname" style="width:90px;"  class="form-control1"  name="batchNo{{dispatchData.id}}"  ng-model="dispatchData.batchNo" ng-change="loadBatchDetails(dispatchData.batchNo,$index,dispatchData.variety);"> 
											<datalist id="stateList">
											<select class="form-control1">
										 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
										</select>    

											</datalist>    </td>
											
<td><input type="text" class="form-control1" placeholder="Pending Qty" name="pendingQty" data-ng-model="dispatchData.pendingQty"  ng-keyup="getdispatchTotalCost(dispatchData.noOfSeedlings,dispatchData.costOfEachItem,$index,dispatchData.pendingQty,dispatchData.batchNo);"size="5" readonly /></td>
											
											<td><input type="text" class="form-control1"  placeholder="Cost" size="5" name="cost{{$index}}" data-ng-model="dispatchData.costOfEachItem" ng-keyup="getdispatchTotalCost(dispatchData.noOfSeedlings,dispatchData.costOfEachItem,$index,dispatchData.pendingQty,dispatchData.batchNo);" /></td>
													

												<td><input type="text" class="form-control1"  placeholder="Indented Qty" size="5" name="seedlingsVal{{$index}}"  data-ng-model="dispatchData.seedlingsVal" readonly/ ></td>


													
													<td><input type="text" class="form-control1"  placeholder="Dis. Seedlings" size="5" name="noOfSeedlings{{$index}}" data-ng-model="dispatchData.noOfSeedlings"    ng-keyup="getdispatchTotalCost(dispatchData.noOfSeedlings,dispatchData.costOfEachItem,$index,dispatchData.pendingQty,dispatchData.batchNo);"  />
													<p id="validationSeedling{{$index}}"   style="display:none; color:#FF0000;">Invalid</p>
													</td>
													
													<td><input type="text" class="form-control1"  placeholder="TotalCost" size="5" name="totalCost{{$index}}" data-ng-model="dispatchData.totalCost" readonly /></td>

 


													<td><input type="text" class="form-control1"  placeholder="Trays" size="5" name="noOfTrays{{$index}}" data-ng-model="dispatchData.noOfTrays" /></td>
													
 <td>
                                                  <select class="form-control1" name="trayType{{$index}}" ng-model="dispatchData.trayType"  ng-options="trayType.id as trayType.TrayType for trayType in TrayTypeNames | orderBy:'TrayType':true" readonly="readonly" ng-change="loadTrayCost(dispatchData.trayType,AddedDispatchForm.ryotcode,$index,dispatchData.noOfTrays);" style='width:100px;'>
													<option value="">TrayType</option>
												</select>
                                                </td>

													<td><input type="text" class="form-control1"  placeholder="Tray Cost" style="width:100px;" name="trayCost{{$index}}" data-ng-model="dispatchData.trayCost" /></td>

													<td><input type="text" class="form-control1"  placeholder="Tray Total Cost" size="5" name="trayTotalCost{{$index}}" data-ng-model="dispatchData.trayTotalCost" /></td>
													
													<!--<td><input type="text" class="form-control1"  placeholder="Total Tray Cost"  size="10" /></td>-->
												<td><label class="checkbox checkbox-inline m-r-20" style="margin-left:20px;" >
<input type="checkbox"  class="checkbox" name="loadFlag{{dispatchData.id}}" data-ng-model='dispatchData.loadFlag' ng-true-value="'1'" ng-false-value="'0'"     ><i class="input-helper" ></i>
																
															</label>
															
															</td>
													
													</tr>
																						
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			           <br />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(DispatchForm)">Reset</button>
										<br /><br />
									</div>
								</div>						 	
							 </div>	
						  </form>	
						  
						  		<form name="GridPopup" novalidate style="display:;">
					   <div class="card popupbox_ratoon" id="gridpopup_box">						  
						  	  						
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
											<th></th>	
												<th>Indent</th>
													
                                                <th >Date of Indent</th>
									
										
                                                <th>Date of Delivery </th>
											
                                                <th>Kind</th>
														
                                                <th>Variety </th>
														
												<th>Quantity</th>
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="gridData in IndentNumberData" >
											
                                                <td>
                                                  <div class="checkbox">
<label class="checkbox checkbox-inline m-r-20">
<input type="checkbox" class="checkbox" id="checkbox" name="hrvFlag"  ng-model="gridData.hrvFlag" ng-click="optionToggled(gridData.hrvFlag,gridData.IndentNo,gridData.kind,gridData.quantity,gridData.variety,gridData.totalCost,gridData.costOfEachItem,AddedDispatchForm.indentNumbers)"><i class="input-helper"></i>
</label>
</div>
                                                </td>
                                                <td>
                                                   <input type="text"  class="form-control1" name="IndentNo{{$index}}" ng-model="gridData.IndentNo"  readonly="readonly" />
                                                </td>
                                                <td>
                                                 <input type="text"  class="form-control1" name="dateofindent{{$index}}" ng-model="gridData.dateofindent" readonly="readonly"  />
                                                </td>
                                                <td>
                                               <input type="text"  class="form-control1"  name="dateofdeliver{{$index}}" ng-model="gridData.dateofdeliver"  data-input-mask="{mask: '00-00-0000'}" readonly />
                                                </td>
                                                <td>
												
											<select class="form-control1" name="kind{{$index}}" data-ng-model='gridData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
														<option value="" > Kind Type</option>	
														</select>
                                               
												</td>
                                               

												
													 <td>
                                                  <select class="form-control1" name="variety{{$index}}" ng-model="gridData.variety"  ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true" readonly="readonly">
													<option value="">Variety</option>
												</select>
                                                </td>


                                                <td colspan="2">
												  <input type="text"  class="form-control1"  name="quantity{{$index}}" ng-model="gridData.quantity" readonly/>
                                                </td>
                                               
												
                                            </tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>			 							 
						    
						<form name='dispatchPrintForm'>
						  		<div id="authprint" style="display:none;">
									<table style='width:100%;position:absolute;top:11.3%;' border='0' >
									
					<tr>
					<td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;">{{dispatchPrnt.RyotName}}</div><!--<input type='hidden' name='RyotName' ng-model='dispatchPrnt.RyotName'>{{dispatchPrnt.RyotName}}</div>--></td>
				<td style="float:right;" colspan='2'><div style="font-size:18px;"><input type='hidden' name='dispatchNo' ng-model='dispatchPrnt.dispatchNo'><span style="margin-right:160px;">{{dispatchPrnt.dispatchNo}}</span></div></td></tr>
<tr><td  style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='RyotCode' ng-model='dispatchPrnt.RyotCode'>{{dispatchPrnt.RyotCode}}</div></td>
<td style="float:right;" colspan='2'><div style="text-align:right;font-size:18px;"><span style="margin-right:160px;">{{dispatchPrnt.DispatchDate}}</span></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Village' ng-model='dispatchPrnt.Village'>
{{dispatchPrnt.Village}}</div></td>
<td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:18px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Circle' ng-model='dispatchPrnt.Circle'>{{dispatchPrnt.Circle}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:14px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Zone' ng-model='dispatchPrnt.Zone'>{{dispatchPrnt.Zone}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:18px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:214px;font-size:14px;"><br /><input type='hidden' name='dispatchNo' ng-model='dispatchPrnt.dispatchNo'>{{dispatchPrnt.IndentNo}}</div></td><td style="float:left;" colspan='2'><div style="margin-left:50px;text-align:left;font-size:14px;"><br />{{dispatchPrnt.IndentDate}}</div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:120px;font-size:14px;"><input type='hidden' name='Vehical' ng-model='dispatchPrnt.Vehical'>{{dispatchPrnt.Vehical}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:14px;"></div></td></tr>

									</table>
						
									<div>
									
												
									
					
									<div style='position:absolute;top:42%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr ng-repeat="printGrid in gridDetails">
										<td ><div style="margin-left:30px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.varietyName}}</div></td>
<td ><div style="margin-left:150px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofTrays}}</div></td>
<td ><div style="margin-left:200px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofSeedlings}}</div></td>
<td ><div style="margin-left:165px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.TrayType}}</div></td>

</tr>	
									</table>			

					
										<table border='0' style='width:100%;font-size:5px;' >
										<tr><td align='right'><p  style='margin-right:60px;height:4px;'><br></p></td></tr>
										<tr><td align='right'><p style='margin-right:60px;height:4px;'></p></td></tr>
										</table>
									<br>
									</div>
												

					
									<div style='position:absolute;top:85%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr ng-repeat="printGrid in gridDetails">
										<td ><div style="margin-left:30px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.varietyName}}</div></td><td ><div style="margin-left:150px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter; ">{{printGrid.NoofTrays}}</div></td>
<td ><div style="margin-left:200px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofSeedlings}}</div></td>
<td ><div style="margin-left:130px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.TrayType}}</div></td>
</tr>	
									</table>																				
									
								</div>
								
	</div>
				<!--below grid-->
							

									<div style='position:absolute;top:98%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr>
										<td ><div style="margin-left:30px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;height:30px;">{{dispatchPrnt.dispatchNo}}</div></td>
<td ><div style="margin-left:150px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;height:30px;">{{dispatchPrnt.DispatchDate}}</div></td>
</tr>	
									</table>			

					
										<table border='0' style='width:100%;font-size:5px;' >
										<tr><td align='right'><p  style='margin-right:60px;height:4px;'><br></p></td></tr>
										<tr><td align='right'><p style='margin-right:60px;height:4px;'></p></td></tr>
										</table>
									<br>
									</div>

<!--end grid----->

								</div>
					 
					 			 					

		 
						  </form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
