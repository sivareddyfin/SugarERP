	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" ></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="AgreementMasterListRyotWiseInVillage" ng-init="loadMandalNames();loadVillageNames();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Agreement MasterList RyotWise In Village Report Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>	
					 <form method="POST" action="/SugarERP/generateAgreementMasterListRyotWiseInVillage.html" target="_blank">
					 
					<div class="row">
						<div class="col-sm-6">
						   <div class="row">
						     <div class="col-sm-12">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="mandal"  data-ng-model="AddedMandal.mandal" ng-options="mandal.id as mandal.mandal for mandal in mandalNames  | orderBy:'-mandal':true" tabindex="1">
                                      <option value=''>Select Mandal</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
                
                    </div>
					       <div class="row">
						                  <div class="col-sm-12">
                    			             <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Choose Format :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="pdf"   data-ng-model='AddedMandal.status' />
			    					    				<i class="input-helper"></i>PDF
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="html"   data-ng-model='AddedMandal.status'>
					    								<i class="input-helper"></i>HTML
							  		 				</label>
													<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="lpt"   data-ng-model='AddedMandal.status'>
					    								<i class="input-helper"></i>LPT
							  		 				</label>						 							 
												</div>
											</div>
						  </div>
                
                    </div>
				     	</div>
				
						<div class="col-sm-6">
						   <div class="row">
						      <div class="col-sm-12">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                           <div class="form-group" >
                        	                   <div class="fg-line">	
                                         <select chosen class="w-100" name="village" data-ng-model="AddedMandal.village" ng-options="village.id as village.village for village in villageNames  | orderBy:'-village':true" tabindex="2">
                                      <option value=''>Select Village</option>
                                 </select>	
                        	                      </div>
                         
						                </div>
							        </div>
							   </div>
						</div>	                     
                  		</div>
			 		</div>
			  
			  <br />
			  
			              <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit"  class="btn btn-primary btn-sm m-t-10">Get Report</button>
										
									</div>
								</div>
							</div>
							
											</form>
										</div>
									</div>
								</div>
							</section>
						</section>
							
							
								

					
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 				

							 
							 
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



