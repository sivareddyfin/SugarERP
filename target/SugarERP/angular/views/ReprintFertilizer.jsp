		
	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.textbox
		{
			width:100px;	
		}
		.spacing-table {
  
   
    border-collapse: separate;
    border-spacing: 0 5px; /* this is the ultimate fix */
}
.spacing-table th {
    text-align: left;
    
}
.spacing-table td {
    border-width: 1px 0;
  
   
    
}
.spacing-table td:first-child {
    border-left-width: 3px;
    border-radius: 5px 0 0 5px;
}
.spacing-table td:last-child {
    border-right-width: 3px;
    border-radius: 0 5px 5px 0;
}
	</style>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="reprintcontroller" ng-init="loadFieldOfficerDropdown();loadRyotCodeDropDown();loadusername();loadVarietyNames();loadKindType();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Authorised Fertilizers </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->				
						<form name="reprintForm"  novalidate ng-submit="authorisationSubmit(authorized,authorisationForm);">
							  <input type="hidden" name="modifyFlag" ng-model="plantupdate.modifyFlag" />
							  <div class="row">
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  name="fieldOfficer{{$index}}" data-ng-model='authorized.fieldOfficer' ng-options="fieldOfficer.id as fieldOfficer.employeename for fieldOfficer in FieldOffNames | orderBy:'-employeename':true" ng-change="getIndentnumbers(authorized.fieldOfficer,authorized.ryotCode);">
																	<option value="">Field Officer</option>																	</select>	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
									
								</div>	
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
				<select chosen class="w-100"  name="ryotCode" data-ng-model='authorized.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-ryotCode':true"  ng-change="getIndentnumbers(authorized.fieldOfficer,authorized.ryotCode);loadRyotDet(authorized.ryotCode);"  >
															<option value="">Ryot Code</option>
														</select>
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class=""></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																<label for="ryotname">RyotName</label>
														<input type="text" class="form-control" placeholder='RyotName' id="ryotname" with-floating-label  name="ryotName" ng-model="authorized.ryotName" readonly="readonly"  readonly  />	
														
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>
								<div class="row">
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
														<label for="date">Date Of Entry</label>
														<input type="text" class="form-control" placeholder='Date Of Entry' id="date" with-floating-label  name="authorisationDate" ng-model="authorized.authorisationDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" />	
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  ">
																<div class="fg-line">
			
														<select  chosen class="w-100"  placeholder='IndentNo' id="indent"   name="indentNo" ng-model="authorized.indentNo"   ng-options="indentNo.IndentNo as indentNo.IndentNo for indentNo in indentNoNames"  ng-change="loadPopup(authorized.indentNo,authorized.ryotCode);"  >
														<option value="">Indent Number</option>
														</select>	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class=""></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																<label for="Indents">Indents</label>
														<input type="text" class="form-control" placeholder='Indents' id="Indents" with-floating-label  name="indentNumbers" ng-model="authorized.indentNumbers"   />	
														
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>		 
							<div class="row">
							  		
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
				<label for="authorized">Authorized</label>
														<input type="text" class="form-control" placeholder='Authorized' id="authorized" with-floating-label  name="authorisedBy" ng-model="authorized.authorisedBy"   readonly  />	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class=""></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
																<label for="phonenum">Phone Number</label>
														<input type="text" class="form-control" placeholder='Phone Number' id="phonenum" with-floating-label  name="phoneNo" ng-model="authorized.phoneNo" readonly="readonly"   readonly  />	
														
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>
							  <br />
							
							 
							 
							   							
							 	 					 	
							 		
									
									
										
								
							
							
							 
							
								<div class="table-responsive" ng-if="authorizedData!=0" id="hidetable">
							 	<table class="table">
							 		<thead>
								        <tr>
											<th>Action</th>
											
							              
		                    				
											<th>Indent NO</th>
											<th>Item Type</th>
											<th>Qty</th>
											<th>Cost Of Each Item</th>
											<th>Total Cost</th>
											
											
            							</tr>											
									</thead>
									
									<tbody>
										<tr ng-repeat="plantData in authorizedData">
											<td>
											
												
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow($index);" ><i class="glyphicon glyphicon-remove-sign"></i></button>
												<input  type="hidden" ng-model="plantData.id" name="plantData{{plantData.id}}" />												
											</td>
											
								<td>
																																																																																																																																																						
												<input type="text" class="form-control1"  placeholder=""  name="indentNumbers{{plantData.id}}" ng-model="plantData.indentNumbers"  ng-pattern="/^[0-9]+$/" />
																						
													

											
											</td>
													
												<td>
																																																																																																																																																						
												<select class="form-control1" name="kind{{$index}}" data-ng-model='plantData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
														<option value="" > Kind Type</option>	
														</select>									
												

											
											</td>     
											
											<td>
																																																																																																																																																						
												<input type="text" class="form-control1"  placeholder=""  name="quantity{{plantData.id}}" ng-model="plantData.quantity"  ng-pattern="/^[0-9]+$/" />
																						
							
											
											</td>
											
												<td ><input type="text" class="form-control1 " ng-model="plantData.costofEachItem" name="costofEachItem{{plantData.id}}" placeholder=""  />
											
											</td>
											
										
											<td>
																																																																																																																																																						
												<input type="text" class="form-control1"  placeholder=""  name="totalCost{{plantData.id}}" ng-model="plantData.totalCost"  ng-pattern="/^[0-9]+$/" />
																						
													

											
											</td>
											
											</tr>
									</tbody>
							 	</table>
							</div>
								<div class="row" align="right" ng-if="authorizedData!=0">            			
								<div class="input-group">
									<div class="fg-line">
									<input type="text" class="form-control"  placeholder="Grand Total"  name="grandTotal" ng-model="authorized.grandTotal"  />
									</div>
									</div>
									</div>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(authorisationForm)">Reset</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="ajaxCallcome();" >AJAX</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>				 	
					 		 
													 
					<!------------------form end---------->							 
					<!-------from-end--------------------->	
					
					
					 	 
					 
					 
					 
					 
					 
					 
					 	 						           			
				        </div>
			    	</div>
					
					
				</div>
				<!--<div id="authprint" style="display:;">
					 
					<table style="width:100%;" border="0">
						<tr><td colspan="4"><div align="center" style="height:30px;">Sri Sarvaraya </div></td></tr>
						<tr><td colspan="4"><div align="center" style="height:30px;">Sri Sarvaraya </div></td></tr>
						<tr><td colspan="4"><div align="center" style="height:30px;">Sri Sarvaraya </div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:100px;">Naveena</div></td></td><td style="float:right;"><div style="margin-right:130px;text-align:right;">Ref No</div></td></tr>
										<tr><td style="float:left;"><div style="margin-left:100px;"></div></td></td><td style="float:right;"><div style="margin-right:130px;text-align:right;">Ref No</div></td></tr>
									<tr style="height:150px;"><td style="position:top;">10001111000111</td></tr>	
										<tr><td ><div style="margin-left:250px;">Ryotr Name</div></td></tr>	
											<tr><td ><div style="margin-left:250px;">fsdfsdf</div></td></tr>	
												<tr><td ><div style="margin-left:250px;">Village Name</div></td></tr>	
													<tr><td ><div style="margin-left:250px;">Mandal Name</div></td></tr>	
															<tr style="height:100px;"><td ><div style="margin-left:350px;">dsfdsfdsf</div></td></tr>	
										
									
										
					</table>
					
					 
				</div>
				<button type="button" ng-click="printfunction();">dasdasd</button>-->
	    </section> 
	</section>
	
	

	<!--<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>-->
