	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>

	<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	<script>
		$(function()
		{
			$(".timepicker").timepicker({
				date: false,
				shortTime: true,
				format: 'HH:mm:ss'
			});

		});
		(function () {
    function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
        document.getElementById('time1').value = h + ":" + m + ":" + s;
		//$scope.AddCaneWeight.caneWeighmentTime = h + ":" + m + ":" + s;
		//alert( h + ":" + m + ":" + s);
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
})();
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="TestWeighment" ng-init="loadWeighBridgeNames();loadServerDate();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Test Weighment</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>		
					
					<!--------Form Start----->
					<form name="TestWeighmentForm" data-ng-submit="testweighment(Addedtestweighment,TestWeighmentForm);" novalidate>
    	        		<div class="row">
				          <div class="col-sm-6">
						  	<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  		<div  class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : TestWeighmentForm.testWeighmentDate.$invalid && (TestWeighmentForm.testWeighmentDate.$dirty || submitted)}">
				        	            			<div class="fg-line">
														<label for="date">Date</label>
    	        					     <input type="text" readonly class="form-control datepicker autofocus" placeholder='Test Weighment Date'  tabindex="1" name="testWeighmentDate" ng-model="Addedtestweighment.testWeighmentDate" data-ng-required='true' data-input-mask="{mask: '00-00-0000'}"  id="date" with-floating-label  maxlength="15" />
				                	   		 </div>
				<p data-ng-show="TestWeighmentForm.testWeighmentDate.$error.required && (TestWeighmentForm.testWeighmentDate.$dirty || submitted)" class="help-block">Date is required</p>	 
										</div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div class="form-group floating-label-wrapper">
									           <div class="fg-line">
													<label for="shift">Shift</label>
    	        					                    <input type="text" readonly class="form-control" placeholder='Shift'  name="shift" data-ng-model="Addedtestweighment.shift" id="shift" with-floating-label  maxlength="10"  />
				                	    	</div>
										</div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  		<div class="form-group floating-label-wrapper">
                  <!--<div  class="form-group"  ng-class="{ 'has-error' : DepartmentForm.location.$invalid && (DepartmentForm.location.$dirty || Addsubmitted)}">-->																					
				        	            			<div class="fg-line">
														<label for="operatorname">Operator Name</label>
    	        													<input type="text" class="form-control" placeholder='Operator Name'  name="operatorName" data-ng-model="Addedtestweighment.operatorName"   maxlength="25"   id="operatorname" with-floating-label  readonly/>
				                	      </div>
							
			 <!--<p data-ng-show="DepartmentForm.location.$error.required && (DepartmentForm.location.$dirty || Addsubmitted)" class="help-block">Dept. Location Name is required</p>-->																								
											<!--</div>-->										
										</div>
			                    	</div>
								</div>	
							</div>
						</div>
						
				          <div class="col-sm-6">
						  	<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									         <div  class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : TestWeighmentForm.testWeighmentTime.$invalid && (TestWeighmentForm.testWeighmentTime.$dirty || submitted)}">
				        	            <div class="fg-line">
										<label for="time1">Time</label>
										
    	        					       <!--<input id="time1" type="text" readonly  class="form-control timepicker" placeholder="Test Weighment Time" tabindex="2" name="testWeighmentTime" data-ng-model="Addedtestweighment.testWeighmentTime" data-ng-required='true'  with-floating-label data-input-mask="{mask: '00:00:00'}"  maxlength="12" ng-change="getShiftId(Addedtestweighment.testWeighmentTime);"/>-->
										   <input id="time1" type="text" readonly  class="form-control timepicker" placeholder="Test Weighment Time" tabindex="2" name="testWeighmentTime" data-ng-model="Addedtestweighment.testWeighmentTime" data-ng-required='true'  with-floating-label data-input-mask="{mask: '00:00:00'}"  maxlength="12" ng-change="getShiftId(Addedtestweighment.testWeighmentTime);">
				                	    </div>
					 <p data-ng-show="TestWeighmentForm.testWeighmentTime.$error.required && (TestWeighmentForm.testWeighmentTime.$dirty || submitted)" class="help-block">Time is required</p>													 
									  </div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
									<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : TestWeighmentForm.bridgeName.$invalid && (TestWeighmentForm.bridgeName.$dirty || submitted)}">
						        	                <div class="fg-line">
														<!--<label for="WeighBridge">Weigh Bridge</label>-->
														<!--<input type="text" class="form-control" placeholder='Weigh Bridge' id="WeighBridge" readonly with-floating-label name="weighBridge" ng-model="AddCaneWeight.weighBridge"/>-->
														<select chosen class="w-100" name="bridgeName" ng-model="Addedtestweighment.bridgeName" ng-required="true" ng-options="bridgeName.id as bridgeName.bridgename for bridgeName in WeighBridgeNames" disabled="isDisabled">
															<option value="">Select Bridge</option>
														</select>
														
														
														
				                			        </div>
													<p ng-show="TestWeighmentForm.bridgeName.$error.required && (TestWeighmentForm.bridgeName.$dirty || submitted)" class="help-block">Weigh Bridge is Required.</p>
												</div>		     
												 
												 
									    </div>
									</div>	
								</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div  class="form-group floating-label-wrapper" ng-class="{ 'has-error' : TestWeighmentForm.totalWeight.$invalid && (TestWeighmentForm.totalWeight.$dirty || submitted)}">													
				        	           				 <div class="fg-line">
															<label for="totalWeight">Total Weight</label>
    	        					     <input type="text"  class="form-control" placeholder='Total Weight'  name="totalWeight" data-ng-model="Addedtestweighment.totalWeight"   maxlength="25"   id="totalWeight" with-floating-label ng-required="true"  ng-pattern="/^[0-9.\s]*$/"/>
				                	    </div>
		<p data-ng-show="TestWeighmentForm.totalWeight.$error.pattern  && (TestWeighmentForm.totalWeight.$dirty || Addsubmitted)" class="help-block">Enter a valid Total Weight</p>					
		<p data-ng-show="TestWeighmentForm.totalWeight.$error.required && (TestWeighmentForm.totalWeight.$dirty || Addsubmitted)" class="help-block">Total Weight is required</p>																																
											</div>										
									  </div>
			                    	</div>
									
									
									
									
									<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="GetCaneWeightbyServlet()">Get Weight</button>
														
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								
								
								</div>	
							
					
							
							<br /><br />
									<div class="row" align="left">            			
											<div class="input-group">
									           <div class="fg-line">
										              <button type="submit" class="btn btn-primary btn-hide">Save</button>
													  <button type="reset" class="btn btn-primary" ng-click="reset(TestWeighmentForm)">Reset</button>
									                   </div>
								                  </div>						 	
							                </div> 		 							 
						  	          	</div>					  
						          </div>
						
						
															
					 <!-------body start------>					    	        			
						  </form>											
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
