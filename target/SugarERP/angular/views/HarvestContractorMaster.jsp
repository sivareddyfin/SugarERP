	<script type="text/javascript">		
		$('#autofocus').focus();
	</script>


	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="HarvestingContractorMaster" ng-init="addFormField();loadHarvesterBanks();loadHarvesterCode();loadHarvesterBranchs();LoadAddedHarvestorsDropDown();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Harvesting Contractor Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 <div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<select chosen class='w-100' name="AddedHarvesters" ng-model='AddedHarvesters' ng-change='loadAllAddedHarvesterData(AddedHarvesters);' ng-options="harvesters.id as harvesters.harvester for harvesters in GetAllAddedHarvesters | orderBy:'-harvester':true">
												<option value=''>Added Harvesting Contractor</option>
											</select>
			                	        </div>
			                    	</div>			                    
				                  </div>
						</div></br>
						<form name="HarvestingContractorForm" novalidate ng-submit="SaveHarvestingContractor(AddHarvester,HarvestingContractorForm);">							
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						        	                <div class="fg-line">
													<label for="c">Harvester Code</label>
														<input type="text" class="form-control" placeholder="Harvester Code" maxlength="10" readonly name="harvesterCode" ng-model="AddHarvester.harvesterCode" id="c" with-floating-label/>
				            		    	        </div>
			                    			</div>
										</div>
									</div><br />
									<input type="hidden"  name="modifyFlag" data-ng-model="AddHarvester.modifyFlag"  />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.contactNumber.$invalid && (HarvestingContractorForm.contactNumber.$dirty || Addsubmitted)}">												
						        	                <div class="fg-line">
													<label for="Contact">Contact No.</label>
														<input type="text" class="form-control" placeholder="Contact No." maxlength="10" tabindex="3" name="contactNumber" ng-model="AddHarvester.contactNumber" data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-required='true' ng-minlength='10' ng-blur="spacebtw('contactNumber');"  id="Contact" with-floating-label/>
				            		    	        </div>
<p ng-show="HarvestingContractorForm.contactNumber.$error.required && (HarvestingContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Contact Number Required</p>																									
<p ng-show="HarvestingContractorForm.contactNumber.$error.pattern && (HarvestingContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Contact Number starting with 7,8,9 series</p>																									
<p ng-show="HarvestingContractorForm.contactNumber.$error.minlength && (HarvestingContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																									
</div>													
			                    			</div>						                    			                    											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																								
					        	                <div class="fg-line">
												<label for="ifsc">IFSC Code</label>
													<input type="text" class="form-control" placeholder="IFSC Code"  maxlength="10" tabindex="6" name="ifscCode" ng-model="AddHarvester.ifscCode" readonly ng-blur="spacebtw('ifscCode');"  id="ifsc"   with-floating-label>
			    		            	        </div>
<p></p>												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.panNumber.$invalid && (HarvestingContractorForm.panNumber.$dirty || Addsubmitted)}">																																				
					        	                <div class="fg-line">
												<label for="pan">PAN No.</label>
													<input type="text" class="form-control" placeholder="PAN No."  maxlength="10" tabindex="9" name="panNumber" ng-model="AddHarvester.panNumber" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  ng-blur="spacebtw('panNumber');" ng-minlength='10' id="pan"   with-floating-label / >
			            		    	        </div>
<p ng-show="HarvestingContractorForm.panNumber.$error.required && (HarvestingContractorForm.panNumber.$dirty || Addsubmitted)" class="help-block">PAN Number Required</p>												
<p ng-show="HarvestingContractorForm.panNumber.$error.pattern && (HarvestingContractorForm.panNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid PAN Number</p>
<p ng-show="HarvestingContractorForm.panNumber.$error.minlength && (HarvestingContractorForm.panNumber.$dirty || submitted)" class="help-block"> Too Short</p>												
</div>												
			                    			</div>																					
										</div>
									</div>
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.harvester.$invalid && (HarvestingContractorForm.harvester.$dirty || Addsubmitted)}">																																																
					        	                <div class="fg-line">
												<label for="hname">Harvester Name</label>
													<input type="text" class="form-control autofocus" placeholder="Harvester Name" maxlength="25" tabindex="1"  name="harvester" ng-model="AddHarvester.harvester" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/"   ng-blur="spacebtw('harvester');validateDup();"  id="hname"   with-floating-label/>
			            		    	        </div>
<p ng-show="HarvestingContractorForm.harvester.$error.required && (HarvestingContractorForm.harvester.$dirty || Addsubmitted)" class="help-block">Harvester Name Required</p>																			
<p ng-show="HarvestingContractorForm.harvester.$error.pattern && (HarvestingContractorForm.harvester.$dirty || Addsubmitted)" class="help-block">Enter Valid Harvester Name</p>	
<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddHarvester.harvester!=null">Harvester Name Already Exist.</p>																			
</div>												
			                    			</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.bankCode.$invalid && (HarvestingContractorForm.bankCode.$dirty || Addsubmitted)}">																																																												
			    		    	                <div class="fg-line">
												
													<select chosen class="w-100" tabindex="4" name="bankCode" ng-model="AddHarvester.bankCode" data-ng-required='true' ng-options="bank.id as bank.bankname for bank in GetBankDropdownData | orderBy:'-bankname':true" ng-change="getBankbranchdata(AddHarvester.bankCode);">
														<option value="">Select Bank</option>
													</select>
			                			        </div>
<p ng-show="HarvestingContractorForm.bankCode.$error.required && (HarvestingContractorForm.bankCode.$dirty || Addsubmitted)" class="help-block">Bank Name Required</p>																															
</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.accountNumber.$invalid && (HarvestingContractorForm.accountNumber.$dirty || Addsubmitted)}">																																																																								
					        	                <div class="fg-line">
												<label for="acc">Account No.</label>
													<input type="text" class="form-control" placeholder="Account No."  maxlength="16" tabindex="7" name="accountNumber" ng-model="AddHarvester.accountNumber" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-minlength='7'  ng-blur="spacebtw('accountNumber');"  id="acc"   with-floating-label/>
			    		            	        </div>
<p ng-show="HarvestingContractorForm.accountNumber.$error.required && (HarvestingContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Account Number Required</p>
<p ng-show="HarvestingContractorForm.accountNumber.$error.pattern && (HarvestingContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Account Number</p>																																											
<p ng-show="HarvestingContractorForm.accountNumber.$error.minlength && (HarvestingContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											

</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Status : </span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="status" value="0" ng-model='AddHarvester.status'>
			    					            	<i class="input-helper"></i>Active
										          </label>
				 			            		  <label class="radio radio-inline m-r-20">
						            			    <input type="radio" name="status" value="1" ng-model='AddHarvester.status'>
					    				            <i class="input-helper"></i>Inactive
							  		              </label>										
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				          		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.address.$invalid && (HarvestingContractorForm.address.$dirty || Addsubmitted)}">																																																																																				
					        	                <div class="fg-line">
												<label for="address">Address</label>
													<input type="text" class="form-control" placeholder="Address" maxlength="50" tabindex="2" name="address" ng-model="AddHarvester.address" data-ng-required='true'/ ng-blur="spacebtw('address');"  id="address"   with-floating-label>
					                	        </div>
<p ng-show="HarvestingContractorForm.address.$error.required && (HarvestingContractorForm.address.$dirty || Addsubmitted)" class="help-block">Address Required</p>												
</div>												
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">																																																																																																
			    		    	                <div class="fg-line">
													<select chosen class="w-100" tabindex="5" name="branchCode" ng-model="AddHarvester.branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in GetBranchDropdownData | orderBy:'-branchname':true" ng-change="setHarvesterifscCode(AddHarvester.branchCode);">
														<option value="">Select Branch</option>
													</select>
			                			        </div>
											</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.aadhaarNumber.$invalid && (HarvestingContractorForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
													<label for="aadhaar">Aadhar No.</label>
													<input type="text" class="form-control" placeholder="Aadhar No." maxlength="14" tabindex="8" name="aadhaarNumber" ng-model="AddHarvester.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}" data-ng-pattern="/^[0-9\s]*$/"  ng-blur="spacebtw('aadhaarNumber');" id="aadhaar"   with-floating-label />
			    		            	        </div>
<p ng-show="HarvestingContractorForm.aadhaarNumber.$error.required && (HarvestingContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number Required</p>
<p ng-show="HarvestingContractorForm.aadhaarNumber.$error.pattern && (HarvestingContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="HarvestingContractorForm.aadhaarNumber.$error.minlength && (HarvestingContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>									
								</div>
							</div><br />

							<p><b>Surety Persons Details :</b></p>
							<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Actions</th>
											<th>Name</th>
							                <th>Contact No.</th>
		                    				<th>Aadhar No.</th>
											<th>PAN No.</th>
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="harvesterData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="harvesterData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(harvesterData.id);" ng-if="harvesterData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.surety{{harvesterData.id}}.$invalid && (HarvestingContractorForm.surety{{harvesterData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Name" maxlength='8' name="surety{{harvesterData.id}}" ng-model="harvesterData.surety" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" />
<p ng-show="HarvestingContractorForm.surety{{harvesterData.id}}.$error.required && (HarvestingContractorForm.surety{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="HarvestingContractorForm.surety{{harvesterData.id}}.$error.pattern && (HarvestingContractorForm.surety{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.contactNo{{harvesterData.id}}.$invalid && (HarvestingContractorForm.contactNo{{harvesterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Contact No." maxlength='10' name="contactNo{{harvesterData.id}}" data-ng-model='harvesterData.contactNo' data-ng-required='true' ng-minlength='10' data-ng-pattern='/^[789]\d{9,10}$/'/>
<p ng-show="HarvestingContractorForm.contactNo{{harvesterData.id}}.$error.required && (HarvestingContractorForm.contactNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="HarvestingContractorForm.contactNo{{harvesterData.id}}.$error.pattern && (HarvestingContractorForm.contactNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>												
<p ng-show="HarvestingContractorForm.contactNo{{harvesterData.id}}.$error.minlength && (HarvestingContractorForm.contactNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																								
</div>												
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$invalid && (HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Aadhar No." maxlength='14' name="aadhaarNumber{{harvesterData.id}}" ng-model="harvesterData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$error.required && (HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$error.pattern && (HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$error.minlength && (HarvestingContractorForm.aadhaarNumber{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : HarvestingContractorForm.panNo{{harvesterData.id}}.$invalid && (HarvestingContractorForm.panNo{{harvesterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="PAN No." maxlength='10' name="panNo{{harvesterData.id}}" ng-model="harvesterData.panNo" data-ng-required='true' ng-minlength='10' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
<p ng-show="HarvestingContractorForm.panNo{{harvesterData.id}}.$error.required && (HarvestingContractorForm.panNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="HarvestingContractorForm.panNo{{harvesterData.id}}.$error.pattern && (HarvestingContractorForm.panNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="HarvestingContractorForm.panNo{{harvesterData.id}}.$error.minlength && (HarvestingContractorForm.panNo{{harvesterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
												
</div>												
											</td>
											</tr>
									</tbody>
							 	</table>
							</div>							 
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(HarvestingContractorForm,AddedHarvesters);" ng-if="AddedHarvesters==null">Reset</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(HarvestingContractorForm,AddedHarvesters);" ng-if="AddedHarvesters!=null">Reset</button>										
									</div>
								</div>						 	
							</div>																	 							 
						</form>	 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
									
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
