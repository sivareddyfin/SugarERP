	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
$(document).ready(function(){

$( ".date" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
    $("#fromDate").datepicker({
        numberOfMonths: 1,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
        onSelect: function(selected) {
          $("#toDate").datepicker("option","minDate", selected)
		  
        }
    });

    $("#toDate").datepicker({
        numberOfMonths: 1,
		changeMonth: true,
		 changeYear: true,
		dateFormat: 'dd-mm-yy',
        onSelect: function(selected) {
           $("#fromDate").datepicker("option","maxDate", selected)
        }
    }); 
});
</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="printAuthorisationController" ng-init="loadMode();loadseasonNames();">       
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>REPRINT AUTHORISATION </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
    	        			  
							
							 	<form name="printDispatch" method="post">
								
							 <div class="row">
							<div class="col-sm-12">
							<div class="row">
							
							<div class="col-sm-3">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : printDispatch.season.$invalid && (printDispatch.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedDispatchForm.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="printDispatch.season.$error.required && (printDispatch.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
											</div>
							
							  
							  
							  	<div class="col-sm-2">
								
											  	<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : printDispatch.dateFrom.$invalid && (printDispatch.dateFrom.$dirty || filterSubmitted)}">
													  	<div class="fg-line">
														
											<input type="text" placeholder='From Date'  class="form-control date" name="dateFrom" ng-model="AddedDispatchForm.dateFrom"  id="fromDate"/>
														</div>
														<p ng-show="printDispatch.dateFrom.$error.required && (printDispatch.dateFrom.$dirty || submitted)" class="help-block">From Date is Required</p>
													</div>
												</div>
											  </div>
											 									
								<div class="col-sm-2">
											   	  <div class="input-group">
												  	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>	
													<div class="form-group" ng-class="{ 'has-error' : printDispatch.dateTo.$invalid && (printDispatch.dateTo.$dirty || filterSubmitted)}">
													   	<div class="fg-line">
														
															<input type="text" placeholder='To Date' id="toDate" class="form-control date" name="dateTo" ng-model="AddedDispatchForm.dateTo" ng-required="true"  />	
														</div>
														<p ng-show="printDispatch.dateTo.$error.required && (printDispatch.dateTo.$dirty || submitted)" class="help-block">To Date is Required</p>
													</div>
												  </div>
											   </div>
							 
							<div class="col-sm-2"><button type="button" class="btn btn-primary" ng-click='getDispatchDetails(AddedDispatchForm.season,AddedDispatchForm.dateFrom,AddedDispatchForm.dateTo);'>Get Details</button>
							</div> 
							  
							</div>
							
							</div>
							
							
							 </div>
							 
							 
							<!--<div class="row">
							
						<div class="col-sm-3">
							<div class="input-group">								       									
            			            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
							<div class="fg-line">
								<label for="caneManagerid">Dispatch Number</label>
                    			 <input type="text" class="form-control" placeholder="Dispatch Number"    name='DispatchNo'  ng-model="AddedDispatchForm.DispatchNo" data-ng-required="true" ng-blur="printDispatchForm(AddedDispatchForm.DispatchNo)" id="caneManagerid" with-floating-label />
							</div>
        		            
		              								
			                    </div>			                    
			                  </div>
							<div class="col-sm-3"><button type="button" class="btn btn-primary" ng-click='generatePrintDispatch(AddedDispatchForm.DispatchNo);'>Generate Print</button>
							</div>
							</div>-->
							
							<br />
							 	<div class="table-responsive" id="showTable" style="display:none;">
								
										<section class="asdok">
										<div class="container1">
								<table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>SNO</span></th>
															<th><span>Authorisation SeqNo.</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
															<th><span>STORE Location</span></th>
															<th><span>Print</span></th>
															
															
															
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="dispatchDetails in dispatchData">
															<td>
												{{dispatchDetails.id}}
												<input type="hidden" name="sno" ng-model="dispatchDetails.id" />
											</td>
							    	    	<td>
												{{dispatchDetails.authorisationSeqNo}}
												<input type="hidden" name="authorisationSeqNo" ng-model="dispatchDetails.authorisationSeqNo" />
											</td>
											<td>
												{{dispatchDetails.ryotCode}}
												<input type="hidden" name="ryotCode" ng-model="dispatchDetails.ryotCode" />
											</td>
											<td>
												{{dispatchDetails.ryotName}}
												<input type="hidden" name="ryotName" ng-model="dispatchDetails.ryotName" />
											</td>
											<td>
												{{dispatchDetails.storeCity}}
												<input type="hidden" name="storeCity" ng-model="dispatchDetails.storeCity" />
											</td>
											<td ng-hide="dispatchDetails.printFlag=='1'">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click='generatePrintDispatch(dispatchDetails.authorisationSeqNo);'>Print</button>
											</td>
												
																						
														</tr>											
													</tbody>
												</table>
												</div>
									</section>
								</div>
								<div>
												<input type="hidden" name="storeLocation" ng-model="storeLocation" />
												<input type="hidden" name="storeCode" ng-model="storeCode" />
											</div>
								
							 </form>	
							 
							<div id="authprint" style="display:none;">
					<div ng-repeat="item in items"  class="row"> 
					<table style="width:100%;position:absolute;top:11.7%" border="0"  >
						<tr><td style="float:left;"><div style="margin-left:100px;"></div></td><td style="float:right;"><div style="margin-right:200px;text-align:right;font-size:15px"><b>{{item.authSeqNo}}</b></div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">THE RETAILE STORES MANAGER,</div></td><td style="float:right;"><div style="margin-right:200px;text-align:right;font-size:15px"><b>{{item.AuthDate}}</b></div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">{{storeLocation}},</div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">EAST GODAVARI,</div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">STORE CODE : {{storeCode}}.</div></td></tr>
						</table>
						<table style="width:100%;position:absolute;top:42.5%" border="0"  style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
										<tr><td ><div style="margin-left:170px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.RyotCode}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:170px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.RyotName}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:170px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.village}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:170px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.mandal}}</b></div></td>
										<td ><div style="font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>AaadharNo : {{item.ryotAadharNo}}</b></div></td></tr>	
						</table>			
																
						
					</div>
					
								<div style="position:absolute;top:60%"> 
					<table style="width:100%;" border="0" style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
						
													<tr ng-repeat="itemgrid in itemsGrid" >
													<td ><div style="margin-left:228px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{itemgrid.Fertiliser}}</div></td>
													<td ><div style="margin-left:140px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{itemgrid.NoOfBags}}</div></td>
<!--
													<td ><div style="margin-left:210px;font-size:15px;position:static"><b  style="color:#0000FF;">{{itemgrid.Fertiliser}}</b></div></td>
													<td ><div style="margin-right:10px;font-size:15px;position:static"><b  style="color:#0000FF;">{{itemgrid.NoOfBags}}</b></div></td>
													-->
													</tr>	
													
					</table>
					
					</div>
				
					<div   ng-repeat="itemTotal in itemsTotal"> 
					<table style="width:100%;position:absolute;top:75.5%" border="0" style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
						
													
													<tr><td style="float:left;"><div style="margin-left:100px;"></div></td><td style="float:right;"><div style="margin-right:500px;text-align:right;font-size:15px"><b>{{itemTotal.totalBags}}</b></div></td></tr>
													<!--
													<tr>
													<td><div style="margin-right:210px;font-size:15px;"><b  style="color:#0000FF;">{{itemTotal.totalBags}}</b></div></td>
													
													</tr>	
																
										-->
									
										
					</table>
					
					</div>
					
				</div>
			</div>
								</div>						 	
							 </div>
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				      
			    	
					<!----------table grid----------------->
					
							
<!----------table grid end------------->

				
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



