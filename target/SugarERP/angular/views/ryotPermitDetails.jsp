

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	   <section id="content" data-ng-controller="ryotPermmitDetailsController" ng-init="loadryotCode();loadSeasonNames();">  
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Ryot Permit Details</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="ryotPermitDetailsForm">
 								<div class="row">
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : ryotPermitDetailsForm.season.$invalid && (ryotPermitDetailsForm.season.$dirty || Filtersubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddedRyotPermit.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" ng-required='true'>
<option value="">Select Season</option>

                    </select>
                	            	   </div>
                	        </div>
							<p ng-show="ryotPermitDetailsForm.season.$error.required && (ryotPermitDetailsForm.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-3">

											<div class="row">
													<div class="col-sm-12">
														<div class="input-group">
            	             									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group" ng-class="{ 'has-error' : ryotPermitDetailsForm.ryotCode.$invalid && (ryotPermitDetailsForm.ryotCode.$dirty || Filtersubmitted)}">
        	               						 <div class="fg-line">
            	          							 <div class="select">
                	     	 						<select chosen class="w-100" name="ryotCode" data-ng-model="AddedRyotPermit.ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotNames | orderBy:'-id':true" ng-required='true' ng-change="getRyotCode(AddedRyotPermit.ryotCode);">
<option value="">Select Ryot Code</option>

                    						</select>
                	            	   </div>
                	        </div>
							<p ng-show="ryotPermitDetailsForm.ryotCode.$error.required && (ryotPermitDetailsForm.ryotCode.$dirty || Filtersubmitted)" class="help-block">Select Ryot Code</p>
								</div>
                    	</div>
				</div>

			</div>
	</div>
			<div class="col-sm-3">

				<div class="row">
					<div class="col-sm-12">
								<div class="input-group">
            	             <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper" ng-class="{ 'has-error' : ryotPermitDetailsForm.ryotname.$invalid && (ryotPermitDetailsForm.ryotname.$dirty || Filtersubmitted)}">	
        	               						 <div class="fg-line">
												 <label for="ryotname">Ryot Name</label>
            	          
                	     	 <input type="text" class="form-control" placeholder="Ryot Name" name="ryotname" data-ng-model="AddedRyotPermit.ryotname" readonly="readonly"  id="ryotname" with-floating-label />
                	            	  
                	        </div>
					<p ng-show="ryotPermitDetailsForm.ryotname.$error.required && (ryotPermitDetailsForm.ryotname.$dirty || Filtersubmitted)" class="help-block">Ryot Name is required</p>
								</div>
                    	</div>
					</div>
				</div>
			</div>
								<div class="col-sm-3">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getRyotPermitDetails();">Get Details</button>

													</div>
												</div>
  										</div>
									</div>
 								</form>
 							</div>

					
					
					<div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
										<form name="ryotPermitDetailsFormForm" novalidate >				
							        <table ng-table="ryotPermitDetailsFormController.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													
													<th><span>PERMIT NO.</span></th>
													<th><span>HARVESTING DATE</span></th>
													
													<th><span>OLD PERMIT NO.</span></th>
													<th><span>RANK</span></th>
													<th><span>PROGRAM NO.</span></th>

													<th><span>STATUS</span></th>
													
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="ryotPermitDt in RyotPermmitData"  ng-class="{ 'active': ryotPermitDt.$edit }">
                		    				<td>
                		    					{{ryotPermitDt.permitnumber}}
					    		               
					            		      </td>
		                    				  
							                  <td>
							                     {{ryotPermitDt.harvestDt}}
							                  </td>
											  <td>
							                     {{ryotPermitDt.oldpermitnumber}}
							                  </td>
											  <td>
							                     {{ryotPermitDt.rank}}
							                  </td>
											  <td>
							                     {{ryotPermitDt.programNo}}
							                  </td>
											  <td>
							                      {{ryotPermitDt.status}}
        		            					 
							                  </td>
											   
											   									  
							           </tr>
									</tbody>
								 </table>
										 </form>	
										 </div>
										</section> 
																 
							     </div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
				 </div>
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
