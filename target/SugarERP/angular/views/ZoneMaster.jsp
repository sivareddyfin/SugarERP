	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="ZoneMaster" data-ng-init='loadZoneCode();loadFieldOfficerNames();loadAddedZones();loadRegionNames();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Zone Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="ZoneMasterForm" ng-submit='AddZones(AddZone,ZoneMasterForm);' novalidate>
						 <input type="hidden" name="screenName" ng-model="AddZone.screenName" />
					 		<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="zoneCode">Zode Code</label>
        		    					          <input type="text" class="form-control" placeholder="Zone Code"  maxlength="10" readonly name="zoneCode" data-ng-model='AddZone.zoneCode' id="zoneCode" with-floating-label>
			    		            	        </div>
			                    			</div>
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ZoneMasterForm.fieldOfficerId.$invalid && (ZoneMasterForm.fieldOfficerId.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddZone.fieldOfficerId' name="fieldOfficerId" ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in officerNames | orderBy:'-fieldOfficer':true">
															<option value="">Select Field Officer</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="ZoneMasterForm.fieldOfficerId.$error.required && (ZoneMasterForm.fieldOfficerId.$dirty || Addsubmitted)" class="help-block">Select Filed Officer</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="fg-line">
													<label for="Description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="4" name="description" data-ng-model='AddZone.description' ng-blur="spacebtw('description');" id="Description" with-floating-label>
			                	        			</div>
			                    				</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ZoneMasterForm.zone.$invalid && (ZoneMasterForm.zone.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="zoneName">Zone Name</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Zone Name"  maxlength="25" tabindex="1" name="zone" data-ng-model='AddZone.zone' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtw('zone');" id="zoneName" with-floating-label>														 
				                				    </div>
								<p ng-show="ZoneMasterForm.zone.$error.required && (ZoneMasterForm.zone.$dirty || Addsubmitted)" class="help-block">Zone is required</p>
								<p ng-show="ZoneMasterForm.zone.$error.pattern  && (ZoneMasterForm.zone.$dirty || Addsubmitted)" class="help-block">Enter a valid Zone.</p>	
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddZone.zone!=null">Zone Name Already Exist.</p>												
												</div>
					                    	</div>
										</div>
									</div>								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ZoneMasterForm.regionCode.$invalid && (ZoneMasterForm.regionCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100" tabindex="3" name="regionCode" data-ng-required='true' data-ng-model='AddZone.regionCode' ng-options="regionCode.id as regionCode.region for regionCode in regionNames | orderBy:'-region':true">
															<option value="">Select Region</option>
				        					            </select>
					                	        	</div>
								<p ng-show="ZoneMasterForm.regionCode.$error.required && (ZoneMasterForm.regionCode.$dirty || Addsubmitted)" class="help-block">Select Region</p>																									
												</div>							
					                    	</div>
										</div>
									</div>																		
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0" checked="checked" tabindex="5"  data-ng-model='AddZone.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1" tabindex="6"  data-ng-model='AddZone.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>																											
								</div>
							</div><br />			
							<input type="hidden"  name="modifyFlag" data-ng-model="AddZone.modifyFlag"  />			
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(ZoneMasterForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Zones</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
									<div class="container1">	
										<form name="EditZoneMasterFrom" novalidate>					
												<table ng-table="ZoneMaster.tableEdit" class="table table-striped table-vmiddle">
												<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Zone Code</span></th>
																<th><span>Zone Name</span></th>
																<th><span>Field Officer Name</span></th>
																<th><span>Region Name</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
													<tbody>
													<tr ng-repeat="UpdateZones in AddedZones"  ng-class="{ 'active': UpdateZones.$edit }">
														<td>
														   <button type="button" class="btn btn-default" ng-if="!UpdateZones.$edit" ng-click="UpdateZones.$edit = true;UpdateZones.modifyFlag='Yes';UpdateZones.screenName='Zone Master';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="UpdateZones.$edit" ng-click="UpdateAddedZones(UpdateZones,$index);UpdateZones.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="UpdateZones.modifyFlag"  />
														   <input type="hidden" name="screenName{{$index}}" ng-model="UpdateZones.screenName" />
														 </td>
														 <td>
															<span ng-if="!UpdateZones.$edit">{{UpdateZones.zoneCode}}</span>
															<div ng-if="UpdateZones.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="UpdateZones.zoneCode" placeholder='Zone Code' maxlength="10" readonly/>
																</div>
															</div>
														  </td>
														  <td>
															 <span ng-if="!UpdateZones.$edit">{{UpdateZones.zone}}</span>
															 <div ng-if="UpdateZones.$edit">
			<div class="form-group" ng-class="{ 'has-error' : EditZoneMasterFrom.zone{{$index}}.$invalid && (EditZoneMasterFrom.zone{{$index}}.$dirty || Addsubmitted)}">												
			<input class="form-control" type="text" ng-model="UpdateZones.zone" placeholder='Zone Name' maxlength="25" name="zone{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('zone',$index);validateDuplicate(UpdateZones.zone,$index);"/>
						<p ng-show="EditZoneMasterFrom.zone{{$index}}.$error.required && (EditZoneMasterFrom.zone{{$index}}.$dirty || submitted)" class="help-block">Zone is required</p>
						<p ng-show="EditZoneMasterFrom.zone{{$index}}.$error.pattern  && (EditZoneMasterFrom.zone{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Zone.</p>					
						<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="UpdateZones.zone!=null">Zone Name Already Exist.</p>								
			
			</div>
															 </div>
														  </td>
														  <td >
															  <span ng-if="!UpdateZones.$edit" ng-repeat="OfficerN in officerNames">
																<span ng-if="OfficerN.id==UpdateZones.fieldOfficerId">{{OfficerN.fieldOfficer}}</span>																									  	
															  </span>
															  <div ng-if="UpdateZones.$edit">
																<div class="form-group">
																	<select class="form-control1"  ng-model='UpdateZones.fieldOfficerId' ng-options="fieldOfficerId.id as fieldOfficerId.fieldOfficer for fieldOfficerId in officerNames | orderBy:'-fieldOfficer':true"></select>
																</div>
															  </div>
														  </td>
														  <td>
															  <span ng-if="!UpdateZones.$edit" ng-repeat="RegionNames in regionNames">
																<span ng-if="RegionNames.id==UpdateZones.regionCode">{{RegionNames.region}}</span>											  	
															  </span>
															  <div ng-if="UpdateZones.$edit">
																<div class="form-group">
																	<select class="form-control1" ng-model='UpdateZones.regionCode' ng-options="regionCode.id as regionCode.region for regionCode in regionNames | orderBy:'-region':true"></select>
																</div>
															  </div>
														  </td>
														  <td>
															  <span ng-if="!UpdateZones.$edit">{{UpdateZones.description}}</span>
															  <div ng-if="UpdateZones.$edit">
																<div class="form-group">
																 <input class="form-control" type="text" ng-model="UpdateZones.description" placeholder='Description' maxlength="50"  ng-blur="spacebtwgrid('description',$index)" />
																</div>
															  </div>
														  </td>
														  <td style="width:235px;">
															  <span ng-if="!UpdateZones.$edit">
																<span ng-if="UpdateZones.status=='0'">Active</span>
																<span ng-if="UpdateZones.status=='1'">Inactive</span>
															  </span>
															  <div ng-if="UpdateZones.$edit">
																<div class="form-group">
																<label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" data-ng-model='UpdateZones.status'>
																	<i class="input-helper"></i>Active
																</label>
																<label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='UpdateZones.status'>
																	<i class="input-helper"></i>Inactive
																</label>				
																</div>		 							 												  
															  </div>
														  </td>											  
													 </tr>
													 </tbody>
													 </table>	
												</form>
									</div>
									</section>									 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
