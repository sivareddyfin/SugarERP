

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" ></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CumulativeCaneCrushingReport" ng-init="loadSeason()">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cumulative Cane Crushing Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>	
					 <form method="POST" action="/SugarERP/generateCumulativeCaneCrushingReport.html" target="_blank">
					 
					<div class="row">
					
						  <div class="col-sm-2">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

										<div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="season"  data-ng-model="AddedSeason.season" ng-options="season.season as season.season for season in seasons  | orderBy:'-season':true" tabindex="1">
                                       <option value=''>Season</option>
									 </select>	
                        	            </div>
                                     
										</div>
						        </div>
						  </div>
							
						    <div class="col-sm-3">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                        <select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedSeason.GROWER' name="GROWER" ng-options="GROWER.GROWER as GROWER.GROWER for GROWER in GROWER">
															<option value="" selected="selected">Area</option>
				        					            </select>
					                	 </div>	
                        	          </div>
                                </div>
							</div>
							<div class="col-sm-5">
                    			             <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Choose Format :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="pdf"   data-ng-model='AddedSeason.status' />
			    					    				<i class="input-helper"></i>PDF
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="html"   data-ng-model='AddedSeason.status'>
					    								<i class="input-helper"></i>HTML
							  		 				</label>
													<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="lpt"   data-ng-model='AddedSeason.status'>
					    								<i class="input-helper"></i>LPT
							  		 				</label>						 							 
												</div>
											</div>
											
											
											
						  </div>
						  <div class="col-sm-1">
						  <div class="input-group">
									<div class="fg-line">
										<button type="submit"  class="btn btn-primary btn-sm m-t-10">Generate Report <br></button>
										
									</div>
								</div>
						  </div>
							
					</div>
                
							
											</form>
										</div>
									</div>
								</div>
							</section>
						</section>
							
							
								

					
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 
					 				
<!--<form method="POST" action="/SugarERP/generateReport.html" target="_blank">
 
        <table id="reptbl" width="350px" border="1">

        <tr>
            <td>
                 <input type="text" id="agreementnumber" name="Agreement Number" />                
            </td>			
        </tr>
		<tr>
            <td>
                <input type="text" id="ryotcode" name="ryot_code"/>                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="name of cane" name="NAME OF THE CANE GROWER/F_H_G_NAME/MOBILE NO" />                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="village code" name="LAND VILLAGE CODE" />                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="Bank Code" name="BANK CODE  / S.B.AC.NO NAME OF THE BANK/ VILLAGE CODE & NAME/AADHAR NO" />                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="variety" name="VARIETY" />                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="plot no" name="PLOT NO DATE SUR_NO" />                
            </td>			
        </tr>
		<tr>
            <td>
                 <input type="text" id="extent" name="EXTENT" />                
            </td>			
        </tr>
        <tr>
            <td>
				  <input type="radio" value="html" name="type" checked="checked"/> :HTML
                  <input type="radio" value="pdf" name="type"/> :PDF
                 <input type="radio" value="lpt" name="type"/> :LPT 
            </td>			
		</tr>
		<tr>
			<td><input type="submit"  value="Get Branch List"  /></td>
		</tr>
 
         </table>  
 
    </form>-->
							 
							 
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



