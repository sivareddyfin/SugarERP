
		
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="permitCheckinReportController" ng-init="loadCheckinDetails();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Permit CheckIn Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="permitCheckinReportForm" novalidate>
						
						
							<table class="table table-stripped">
							<thead>
								        <tr>
											
											<th>CheckIn Date</th>
							                <th>CheckIn Time</th>
											<th>Shift</th>
											<th>Serial No</th>
											<th>Permit No</th>
											<th>Vehicle Type</th>
											<th>Veihcle No</th>
											<th>Ryot Code</th>
											<th>Ryot Name</th>
											
										
											
            							</tr>
										<tr ng-repeat="permitCheck in permitCheckinData">
										
										<td>{{permitCheck.checkindate}}</td>
										<td>{{permitCheck.checkintime}}</td>
										<td>{{permitCheck.shiftid}}</td>
										<td>{{permitCheck.vehicleslno}}</td>
										<td>{{permitCheck.permitnumber}}</td>
										<td>{{permitCheck.vehicletype}}</td>
										<td>{{permitCheck.vehicleno}}</td>
										<td>{{permitCheck.ryotcode}}</td>
										<td>{{permitCheck.ryotname}}</td>
									
										</tr>
										
																				
									</thead>
							</table>
							
							
						
						
						
						
					</form>	
						  
							 							 
						  
						  
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				
	    </section> 
	</section>
	
	
	
	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
