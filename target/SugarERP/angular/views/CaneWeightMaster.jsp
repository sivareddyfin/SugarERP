	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>      
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneWeightMaster"  ng-init="loadseasonNames();">        
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Weight Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 <form name="CaneWeightForm" ng-submit="CaneWeightSubmit(AddedCaneWeight,CaneWeightForm)" novalidate>
					 		<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : CaneWeightForm.season.$invalid && (CaneWeightForm.season.$dirty || submitted)}">												
			        	                 		<div class="fg-line">
            					         			 <select chosen class="w-100" name="season" data-ng-model="AddedCaneWeight.season" tabindex="1" ng-options="season.season as season.season for season in seasonNames"  data-ng-required="true" ng-change="getAddedWeights(AddedCaneWeight.season);">
													 <option value="">Select Season</option>
										  			</select>
													</div>
									<p ng-show="CaneWeightForm.season.$error.required && (CaneWeightForm.season.$dirty || submitted)" class="help-block">Select Season.</p>
										</div>
			                    	</div>			                    
				                  </div>								
    	        			 </div>		
    	        			<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										     <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneWeightForm.bmPercentage.$invalid && (CaneWeightForm.bmPercentage.$dirty || submitted)}">
											 		
			        	                           <div class="fg-line">
												   <label for="exampleInputEmail1">Binding Material(%)</label>
            	<input type="text" class="form-control" placeholder="Binding Material(%)"  maxlength="3" name="bmPercentage" data-ng-model="AddedCaneWeight.bmPercentage" tabindex="2"  data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/" id="exampleInputEmail1" with-floating-label>
			                	                    </div>
								<p ng-show="CaneWeightForm.bmPercentage.$error.required && (CaneWeightForm.bmPercentage.$dirty || submitted)" class="help-block">Binding Material is required.</p>
								<p ng-show="CaneWeightForm.bmPercentage.$error.pattern && (CaneWeightForm.bmPercentage.$dirty || submitted)" class="help-block">Enter Valid Binding Material.</p> 
										</div>
			                    	</div>			                    
				                  </div>								
    	        			 </div><br /> 
							  
    	        			<div class="row" align="center">
				                <div class="col-sm-12">
				                    <div class="input-group">            				            
			        	                <div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									     <button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(CaneWeightForm,AddedCaneWeight.season);" ng-if="AddedCaneWeight.season==null">Reset</button>
										 <button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(CaneWeightForm,AddedCaneWeight.season);" ng-if="AddedCaneWeight.season!=null">Reset</button>
									   </div>
			                    	</div>			                    
				                  </div>								
    	        			 </div><br /> 
							 </form> 


   

					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
