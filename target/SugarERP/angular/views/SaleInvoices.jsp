

	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SaleInvoices" ng-init="addFormField(); loadStatus(); loadDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Sale Invoices</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="SaleInvoicesForm" novalidate  ng-submit=" SaleInvoicesSubmit(AddedSaleInvoices,SaleInvoicesForm)">
						   <input type="hidden" ng-model="AddedSaleInvoices.modifyFlag" name="modifyFlag"  />
						   
						   <div class="row">
							 <div class="col-sm-6">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SaleInvoicesForm.fromDate.$invalid && (SaleInvoicesForm.fromDate.$dirty || submitted)}" >												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date" autofocus tabindex="1" name="fromDate" ng-required='true' data-ng-model="AddedSaleInvoices.fromDate" placeholder="From Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="fromDate" with-floating-label/>
									</div>
								<p ng-show="SaleInvoicesForm.fromDate.$error.required && (SaleInvoicesForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-6">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : SaleInvoicesForm.toDate.$invalid && (SaleInvoicesForm.toDate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="toDate">To Date </label>
										<input type="text" class="form-control date" tabindex="2" name="toDate " ng-required='true' data-ng-model="AddedSaleInvoices.toDate" placeholder="To Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="toDate" with-floating-label/>
									</div>
								<p ng-show="SaleInvoicesForm.toDate.$error.required && (SaleInvoicesForm.toDate.$dirty || Addsubmitted)" class="help-block">To Date is required</p>
								</div>
								</div>			                    
								</div>
							</div>	
							</div></div>
							
							<div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoicesForm.department.$invalid && (SaleInvoicesForm.department.$dirty || submitted)}" >												
									<div class="fg-line">
										 <select chosen class="w-100 form-control " tabindex="3"  maxlength="20" data-ng-model='AddedSaleInvoices.department' name="department"  placeholder="Department"  ng-options="department.department as department.department for department in departments"  >
														
						        			            </select>
									</div>
								<p ng-show="SaleInvoicesForm.department.$error.required && (SaleInvoicesForm.department.$dirty || Addsubmitted)" class="help-block">Department is required</p>
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-5">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								
								<div class="form-group" >												
									<div class="fg-line">
									   <label class="radio radio-inline m-r-20"><b>Status</b></label>
										<label class="radio radio-inline m-r-20">
									<input type="radio" name="inlineRadioOptions" value="0" maxlength="10" checked="checked" data-ng-model='AddedSaleInvoices.status' tabindex="4" >
			    					    				<i class="input-helper"></i>Valid
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            						<input type="radio" name="inlineRadioOptions" value="1"  maxlength="10" data-ng-model='AddedSaleInvoices.status' tabindex="4" >
					    								<i class="input-helper"></i>Cancelled
							  		 				</label>
													<label class="radio radio-inline m-r-20">
													<input type="radio" name="inlineRadioOptions" value="2" maxlength="10"  data-ng-model='AddedSaleInvoices.status' tabindex="4" >
					    								<i class="input-helper"></i>All
							  		 				</label>				
									</div>
								
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group"  >												
									<div class="fg-line">
										<input type='text' size='3' style='background-color:orange;' readonly>Valid &nbsp;&nbsp;
												<input type='text' size='3' readonly style='background-color:green;'>Cancelled &nbsp;&nbsp;&nbsp;
												<input type='text' size='3' style='background-color:yellow;' readonly> All &nbsp;&nbsp;
									</div>
								
								</div>
								</div>			                    
								</div>
							</div>	
							</div></div>
							
							
							<br/>
							
							<div class="table-responsive">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>Options</th>
									<th>Sale Invoice</th>
									<th>Date</th>
									<th>Department</th>
									<th>DC No</th>
									<th>Customer</th>
									<th>Contact No</th>
									
									
								</tr>
								
								<tr class="styletr"    ng-class="{ 'active': AddedSaleInvoices.$edit }" ng-repeat="SaleInvoices in SaleInvoicesData" >
								   
									<td>
														   <button type="button" class="btn btn-default" ng-if="!AddedSaleInvoices.$edit" ng-click="AddedSaleInvoices.$edit = true; AddedSaleInvoices.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="AddedSaleInvoices.$edit" ng-click="UpdateSaleInvoices(AddedSaleInvoices,$index);AddedSaleInvoices.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AddedSaleInvoices.modifyFlag"  />	
														   
														 </td>
									
									
									<td>
									  <div class="form-group" ng-class="{'has-error':SaleInvoicesForm.saleInvoice{{$index}}.$invalid&&(SaleInvoicesForm.saleInvoice{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Sale Invoice" maxlength="10" name="saleInvoice{{$index}}" ng-model="saleData.productCode" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.saleInvoice{{$index}}.$error.required && (SaleInvoicesForm.saleInvoice{{$index}}.$dirty || submitted)" class="help-block">Sale Invoice is Required</p>
									</div></td>
									
									<td>
								  <div class="form-group" ng-class="{'has-error':SaleInvoicesForm.date{{$index}}.$invalid&&(SaleInvoicesForm.date{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Date" maxlength="10" name="date{{$index}}" ng-model="saleData.date" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.date{{$index}}.$error.required && (SaleInvoicesForm.depardatetment{{$index}}.$dirty || submitted)" class="help-block">Date is Required</p>
									</div>
									
									</td>
									   
									<td>
									    
										<div class="form-group" ng-class="{'has-error':SaleInvoicesForm.department{{$index}}.$invalid&&(SaleInvoicesForm.department{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Department" maxlength="20" name="department{{$index}}" ng-model="saleData.department" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.department{{$index}}.$error.required && (SaleInvoicesForm.department{{$index}}.$dirty || submitted)" class="help-block">Department is Required</p>
									</div></td>
									
									<td>
									    
										<div class="form-group" ng-class="{'has-error':SaleInvoicesForm.dcNo{{$index}}.$invalid&&(SaleInvoicesForm.dcNo{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="DC No" maxlength="10" name="dcNo{{$index}}" ng-model="saleData.dcNo" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.dcNo{{$index}}.$error.required && (SaleInvoicesForm.dcNo{{$index}}.$dirty || submitted)" class="help-block">DC No is Required</p>
									</div></td>
									
									<td>
									    
										<div class="form-group" ng-class="{'has-error':SaleInvoicesForm.customer{{$index}}.$invalid&&(SaleInvoicesForm.customer{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Customer" maxlength="20" name="customer{{$index}}" ng-model="saleData.customer" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.customer{{$index}}.$error.required && (SaleInvoicesForm.customer{{$index}}.$dirty || submitted)" class="help-block">Customer is Required</p>
									</div></td>
									
									<td>
									    
										<div class="form-group" ng-class="{'has-error':SaleInvoicesForm.contact{{$index}}.$invalid&&(SaleInvoicesForm.contact{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Contact No" maxlength="12" name="contact{{$index}}" ng-model="saleData.contact" ng-required="true" >							
										<p ng-show="SaleInvoicesForm.contact{{$index}}.$error.required && (SaleInvoicesForm.contact{{$index}}.$dirty || submitted)" class="help-block">Contact No is Required</p>
									</div></td>
									   
									
									</tr>
									
									</table>
									
									</div>
									
									<br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SaleInvoicesForm)">Reset</button>
									</div>
								</div>						 	
							 </div>		
									
									</form>		
									
									
									<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>			 