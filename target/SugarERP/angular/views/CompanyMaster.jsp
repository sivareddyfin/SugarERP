		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	

	<!--<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>-->
	<section id="main" class='bannerok'>    
<!--	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>-->

    	<section id="content" data-ng-controller="CompanyMaster" ng-init="loadCountryNames();addComapnyBranchField();loadGridStateNames();loadAddedCompanyDetails();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Company Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
 					 	<form name="CompanyForm" ng-submit="Addcompanies(AddedComany,CompanyForm);" novalidate>	
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.companyName.$invalid && (CompanyForm.companyName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="companyname">Company Name</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Company Name"  autofocus  maxlength="25" tabindex="1" name="companyName" data-ng-model='AddedComany.companyName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" id="companyname" with-floating-label  ng-blur="spacebtw('companyName');" />														 
				                				    </div>
						<p ng-show="CompanyForm.companyName.$error.required && (CompanyForm.companyName.$dirty || Addsubmitted)" class="help-block">Company Name is required</p>
						<p ng-show="CompanyForm.companyName.$error.pattern  && (CompanyForm.companyName.$dirty || Addsubmitted)" class="help-block">Enter Valid Company Name</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.address2.$invalid && (CompanyForm.address2.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="addressline2">Address Line 2</label>
    	        								         <input type="text" class="form-control" placeholder="Address Line 2(Optional)"  maxlength="100" tabindex="4" name="address2" data-ng-model='AddedComany.address2'  id="addressline2" with-floating-label  ng-blur="spacebtw('address2');" />														 
				                				    </div>
							
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">										  
										  <div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CompanyForm.stateCode.$invalid && (CompanyForm.stateCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="6" data-ng-required='true' data-ng-model='AddedComany.stateCode' name="stateCode" ng-options="stateCode.id as stateCode.state for stateCode in stateNames | orderBy:'-state':true" ng-change="loadDistrictNames(AddedComany.stateCode);">
															<option value="">Select State</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="CompanyForm.stateCode.$error.required && (CompanyForm.stateCode.$dirty || Addsubmitted)" class="help-block">Select State</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.phoneNo.$invalid && (CompanyForm.phoneNo.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="phonenumber">Phone Number</label>
    	        								         <input type="text" class="form-control" placeholder="Phone Number(Optional)"  maxlength="12" tabindex="10" name="phoneNo" data-ng-model='AddedComany.phoneNo'   data-ng-pattern="/^[0-9\s]*$/" id="phonenumber" with-floating-label  ng-blur="spacebtw('phoneNo');companyphone('phoneNo');" />														 
				                				    </div>
						
					<p ng-show="CompanyForm.phoneNo.$error.pattern  && (CompanyForm.phoneNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Phone Number</p>
					<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedComany.phoneNo!=null">Enter Valid 10 digit phone Number</p>
						
				<!--<p ng-show="CompanyForm.phoneNo.$error.minlength  && (CompanyForm.phoneNo.$dirty || Addsubmitted)" class="help-block">Phone Number is Too Short</p>-->																									
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.faxNo.$invalid && (CompanyForm.faxNo.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="faxnumber">Fax Number</label>
    	        								         <input type="text" class="form-control" placeholder="Fax Number(Optional)"  maxlength="11" tabindex="13" name="faxNo" data-ng-model='AddedComany.faxNo'  data-ng-pattern="/^[0-9\s]*$/" id="faxnumber" with-floating-label  ng-blur="spacebtw('faxNo');" />														 
				                				    </div>
								
								<p ng-show="CompanyForm.faxNo.$error.pattern  && (CompanyForm.faxNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Fax Number</p>
								
																					
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.registrationNo.$invalid && (CompanyForm.registrationNo.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="registernumber">Registration Number</label>
    	        								         <input type="text" class="form-control" placeholder="Registration Number"  maxlength="6" tabindex="16" name="registrationNo" data-ng-model='AddedComany.registrationNo'  data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" id="registernumber" with-floating-label   ng-blur="spacebtw('registrationNo');" />														 
				                				    </div>
	<p ng-show="CompanyForm.registrationNo.$error.required && (CompanyForm.registrationNo.$dirty || Addsubmitted)" class="help-block">Registration Number is required</p>
<p ng-show="CompanyForm.registrationNo.$error.pattern  && (CompanyForm.registrationNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Registration Number</p>
													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.pin.$invalid && (CompanyForm.pin.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="pinnumber">Pin Number</label>
													<input type="text" class="form-control" placeholder="Pin Number(Optional)"  maxlength="15" tabindex="19" name="pin" data-ng-model='AddedComany.pin'    data-ng-pattern="/^[0-9\s]*$/" id="pinnumber" with-floating-label   ng-blur="spacebtw('pin');" />	
													
    	            						    		
		                	            			  </div>
					
					<p ng-show="CompanyForm.pin.$error.pattern && (CompanyForm.pin.$dirty || Addsubmitted)" class="help-block">Enter Valid Pin Number</p>
									 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.serviceTaxRegnNo.$invalid && (CompanyForm.serviceTaxRegnNo.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="servicetax">Service Tax Registration Number</label>
									<input type="text" class="form-control" placeholder="Service Tax Registration Number(Optional)"  maxlength="15" tabindex="22" name="serviceTaxRegnNo" data-ng-model='AddedComany.serviceTaxRegnNo'  data-ng-pattern="/^[a-z A-Z 0-9\s]*$/"  id="servicetax" with-floating-label   ng-blur="spacebtw('serviceTaxRegnNo');" />	
													
    	            						    		
		                	            			  </div>
					
					<p ng-show="CompanyForm.serviceTaxRegnNo.$error.pattern && (CompanyForm.serviceTaxRegnNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Service Tax Registration Number</p>	
								 		 
											  </div>
			                    	      </div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.website.$invalid && (CompanyForm.website.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="companywebsite">Company Website</label>
    	        								         <input type="text" class="form-control" placeholder="Company Website(Optional)"  maxlength="25" tabindex="2" name="website" data-ng-model='AddedComany.website' id="companywebsite" with-floating-label   ng-blur="spacebtw('website');" />														 
				                				    </div>
								
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.city.$invalid && (CompanyForm.city.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="city">City</label>
    	        								         <input type="text" class="form-control" placeholder="City"  maxlength="25" tabindex="5" name="city" data-ng-model='AddedComany.city' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" id="city" with-floating-label   ng-blur="spacebtw('city');" />														 
				                				    </div>
								<p ng-show="CompanyForm.city.$error.required && (CompanyForm.city.$dirty || Addsubmitted)" class="help-block">City is required</p>
								<p ng-show="CompanyForm.city.$error.pattern  && (CompanyForm.city.$dirty || Addsubmitted)" class="help-block">Enter Valid City</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CompanyForm.districtCode.$invalid && (CompanyForm.districtCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="7" data-ng-required='true' data-ng-model='AddedComany.districtCode' name="districtCode" ng-options="districtCode.id as districtCode.district for districtCode in districtNames| orderBy:'-district':true">
															<option value="">Select District</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="CompanyForm.districtCode.$error.required && (CompanyForm.districtCode.$dirty || Addsubmitted)" class="help-block">Select District</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.mobileNo.$invalid && (CompanyForm.mobileNo.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="mobilenumber">Mobile Number</label>
    	        								         <input type="text" class="form-control" placeholder="Mobile Number" maxlength="11" tabindex="11" name="mobileNo" data-ng-model='AddedComany.mobileNo' data-ng-required='true'  data-ng-pattern="/^[0-9\s]*$/" id="mobilenumber" with-floating-label   ng-blur="spacebtw('mobileNo');commobile('mobileNo');" />	
													<!--data-ng-pattern='/^[7890]\d{9,10}$/'-->	 												 
				                				    </div>
							<p ng-show="CompanyForm.mobileNo.$error.required && (CompanyForm.mobileNo.$dirty || Addsubmitted)" class="help-block">Mobile Number is required</p>
							<p ng-show="CompanyForm.mobileNo.$error.pattern  && (CompanyForm.mobileNo.$dirty || Addsubmitted)" class="help-block">Enter Valid Mobile Number starting with 7,8,9 series </p>	
							<!--<p ng-show="CompanyForm.mobileNo.$error.minlength  && (CompanyForm.mobileNo.$dirty || Addsubmitted)" class="help-block">Mobile Number is Too Short</p>-->
							<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedComany.mobileNo!=null">Enter Valid 10 digit Mobile Number</p>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.cin.$invalid && (CompanyForm.cin.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="corporateidentificationnumber">Corporate Identification Number</label>
    	        								         <input type="text" class="form-control" placeholder="Corporate Identification Number"  maxlength="25" tabindex="14" name="cin" data-ng-model='AddedComany.cin' data-ng-required='true'  data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="corporateidentificationnumber" with-floating-label   ng-blur="spacebtw('cin');" />														 
				                				    </div>
								<p ng-show="CompanyForm.cin.$error.required && (CompanyForm.cin.$dirty || Addsubmitted)" class="help-block">Corporate Identification Number is required</p>
								<p ng-show="CompanyForm.cin.$error.pattern  && (CompanyForm.cin.$dirty || Addsubmitted)" class="help-block">Enter Valid Corporate Identification Number</p>	
								
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.gst.$invalid && (CompanyForm.gst.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="gst">GST</label>
    	        								         <input type="text" class="form-control" placeholder="GST(Optional)"  maxlength="15" tabindex="17" name="gst" data-ng-model='AddedComany.gst'   data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="gst" with-floating-label   ng-blur="spacebtw('gst');" />														 
				                				    </div>
								
								<p ng-show="CompanyForm.gst.$error.pattern  && (CompanyForm.gst.$dirty || Addsubmitted)" class="help-block">Enter Valid GST</p>	
																					
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.panNo.$invalid && (CompanyForm.panNo.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="pannumber">PAN Number</label>
    	        								         <input type="text" class="form-control" placeholder="PAN Number"  maxlength="15" tabindex="20" name="panNo" data-ng-model='AddedComany.panNo' data-ng-required='true' data-ng-minlength="10" data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="pannumber" with-floating-label   ng-blur="spacebtw('panNo');" />														 
				                				    </div>
								<p ng-show="CompanyForm.panNo.$error.required && (CompanyForm.panNo.$dirty || Addsubmitted)" class="help-block">PAN Number is required</p>
								<p ng-show="CompanyForm.panNo.$error.pattern && (CompanyForm.panNo.$dirty || Addsubmitted)" class="help-block">Enter Valid PAN Number</p>
								<p ng-show="CompanyForm.panNo.$error.minlength && (CompanyForm.panNo.$dirty || Addsubmitted)" class="help-block">PAN Number is Too Short</p>
																				
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.address1.$invalid && (CompanyForm.address1.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="addressline1">Address Line 1</label>
    	        								         <input type="text" class="form-control" placeholder="Address Line 1"  maxlength="100" tabindex="3" name="address1" data-ng-model='AddedComany.address1' data-ng-required='true' id="addressline1" with-floating-label   ng-blur="spacebtw('address1');" />														 
				                				    </div>
					<p ng-show="CompanyForm.address1.$error.required && (CompanyForm.address1.$dirty || Addsubmitted)" class="help-block">Address Line 1 is required</p>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CompanyForm.countryCode.$invalid && (CompanyForm.countryCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
    	        								         <select chosen class="w-100" name="countryCode" tabindex="8" data-ng-model="AddedComany.countryCode" ng-options="countryCode.id as countryCode.country for countryCode in countryNames | orderBy:'-country':true" ng-change="loadStateNames(AddedComany.countryCode);">
													<option value="">Select Country</option>
												  </select>													 
				                				    </div>
						<p ng-show="CompanyForm.countryCode.$error.required && (CompanyForm.countryCode.$dirty || Addsubmitted)" class="help-block">Select Country</p>
													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.pinCode.$invalid && (CompanyForm.pinCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="pincode">PIN Code</label>
    	        								         <input type="text" class="form-control" placeholder="PIN Code"  maxlength="6" tabindex="9" name="pinCode" data-ng-model='AddedComany.pinCode' data-ng-required='true' data-ng-minlength="6" data-ng-pattern="/^[0-9\s]*$/" id="pincode" with-floating-label   ng-blur="spacebtw('pinCode');" />														 
				                				    </div>
								<p ng-show="CompanyForm.pinCode.$error.required && (CompanyForm.pinCode.$dirty || Addsubmitted)" class="help-block">PIN Code is required</p>
								<p ng-show="CompanyForm.pinCode.$error.pattern  && (CompanyForm.pinCode.$dirty || Addsubmitted)" class="help-block">Enter Valid PIN Code</p>
								<p ng-show="CompanyForm.pinCode.$error.minlength  && (CompanyForm.pinCode.$dirty || Addsubmitted)" class="help-block">PIN Code is Too Short</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.emailId.$invalid && (CompanyForm.emailId.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="emailid">Email ID</label>
    	        								         <input type="email" class="form-control" placeholder="Email ID"  maxlength="50" tabindex="12" name="emailId" data-ng-model='AddedComany.emailId' data-ng-required='true' id="emailid" with-floating-label />														 
				                				    </div>
								<p ng-show="CompanyForm.emailId.$error.required && (CompanyForm.emailId.$dirty || Addsubmitted)" class="help-block">Email ID is required</p>
								<p ng-show="CompanyForm.emailId.$error.email  && (CompanyForm.emailId.$dirty || Addsubmitted)" class="help-block">Enter Valid Email ID</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.roc.$invalid && (CompanyForm.roc.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="roc">ROC</label>
    	        								         <input type="text" class="form-control" placeholder="ROC"  maxlength="10" tabindex="15" name="roc" data-ng-model='AddedComany.roc' data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9-\s]*$/"  id="roc" with-floating-label   ng-blur="spacebtw('roc');" />														 
				                				    </div>
								<p ng-show="CompanyForm.roc.$error.required && (CompanyForm.roc.$dirty || Addsubmitted)" class="help-block">ROC is required</p>
								<p ng-show="CompanyForm.roc.$error.pattern  && (CompanyForm.roc.$dirty || Addsubmitted)" class="help-block">Enter Valid ROC</p>
								
																					
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.tin.$invalid && (CompanyForm.tin.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="tin">TIN</label>
    	        								         <input type="text" class="form-control" placeholder="TIN"  maxlength="15" tabindex="18" name="tin" data-ng-model='AddedComany.tin' data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" id="tin" with-floating-label   ng-blur="spacebtw('tin');" />														 
				                				    </div>
								<p ng-show="CompanyForm.tin.$error.required && (CompanyForm.tin.$dirty || Addsubmitted)" class="help-block">TIN is required</p>
								<p ng-show="CompanyForm.tin.$error.pattern  && (CompanyForm.tin.$dirty || Addsubmitted)" class="help-block">Enter Valid TIN</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CompanyForm.professionalTaxRegn.$invalid && (CompanyForm.professionalTaxRegn.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="professionaltax">Professional Tax</label>
    	            						    		 <input type="text" class="form-control" placeholder="Professional Tax(Optional)"  maxlength="25" tabindex="21" name="professionalTaxRegn" data-ng-model='AddedComany.professionalTaxRegn'  data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="professionaltax" with-floating-label   ng-blur="spacebtw('professionalTaxRegn');" />
		                	            			  </div>
							 
											  </div>
			                    	      </div>
										</div>
									</div>
								</div>
							</div>	
												
																 				
												
					
					
					<!--<div class="table-responsive"> 
									<table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th>Action</th>
												<th>Branch</th>
												<th>Location</th>
												<th>City</th>
												<th>State</th>
												<th>District</th>
												<th>Phone No.</th>
												<th>mobile No.</th>
												<th>Other No.</th>
												<th>Fax No.</th>												
												<th>Email-ID</th>
												
											</tr>
										</thead>										
										<tbody>
											<tr ng-repeat='CompanyData in data'>
												<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addComapnyBranchField();' ng-if="CompanyData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeCompanyBranch(CompanyData.id);' ng-if="CompanyData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="CompanyData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="CompanyData.branchCode"/>
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.name{{CompanyData.id}}.$invalid && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																											
<input type="text" class="form-control1" placeholder="Branch Name" name="name{{CompanyData.id}}"  data-ng-model="CompanyData.name" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.name{{CompanyData.id}}.$error.required && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.name{{CompanyData.id}}.$error.pattern  && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																					 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.location{{CompanyData.id}}.$invalid && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Location" name="location{{CompanyData.id}}" data-ng-model="CompanyData.location" data-ng-required='true' data-ng-pattern="/^[a-z  A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.location{{CompanyData.id}}.$error.required && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.location{{CompanyData.id}}.$error.pattern  && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.city{{CompanyData.id}}.$invalid && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																			
							<input type="text" class="form-control1" placeholder="City" name="city{{CompanyData.id}}" data-ng-model="CompanyData.city" data-ng-required='true' data-ng-pattern="/^[a-z  A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.city{{CompanyData.id}}.$error.required && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.city{{CompanyData.id}}.$error.pattern  && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>												  
												</td>
												<td style="width:100px;">

<div class="form-group" ng-class="{ 'has-error' : CompanyForm.stateCode{{CompanyData.id}}.$invalid && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)}">	
 <select class="form-control1"  placeholder="State"  name="stateCode{{CompanyData.id}}" data-ng-model="CompanyData.stateCode" data-ng-required='true' ng-options="state.id as state.state for state in gridStateNames | orderBy:'-state':true" ng-change="loadGridDistrictNames(CompanyData.stateCode);">	
												 	<option value="">Select</option>
													<option value="">{{CompanyData.stateCode}}</option>
												 </select>
	<p ng-show="CompanyForm.stateCode{{CompanyData.id}}.$error.required && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.stateCode{{CompanyData.id}}.$error.pattern  && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>
												</td>
												<td style="width:100px;">
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.districtCode{{CompanyData.id}}.$invalid && (CompanyForm.districtCode{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
 <select class="form-control1"  placeholder="District"  name="districtCode{{CompanyData.id}}" data-ng-model="CompanyData.districtCode" data-ng-required='true' ng-options="district.id as district.district for district in gridDistrictNames | orderBy:'-district':true">	
												 	<option value="">Select</option>
													<option value="">{{CompanyData.districtCode}}</option>
												 	</select>
	<p ng-show="CompanyForm.districtCode{{CompanyData.id}}.$error.required && (CompanyForm.districtCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.district{{CompanyData.id}}.$error.pattern  && (CompanyForm.district{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>											
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.phoneNo{{CompanyData.id}}.$invalid && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																			
									<input type="text" class="form-control1" placeholder="Phone Number" name="phoneNo{{CompanyData.id}}" data-ng-model="CompanyData.phoneNo" maxlength="12" data-ng-minlength="12" data-ng-pattern="/^[0-9\s]*$/"/>
	<p ng-show="CompanyForm.contactPerson{{CompanyData.id}}.$error.required && (CompanyForm.contactPerson{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.phoneNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
	<p ng-show="CompanyForm.phoneNo{{CompanyData.id}}.$error.minlength  && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																																															
												  
</div>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.mobileNo{{CompanyData.id}}.$invalid && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																															
					<input type="text" class="form-control1" placeholder="mobileNo" name="mobileNo{{CompanyData.id}}" data-ng-model="CompanyData.mobileNo" data-ng-required='true'     maxlength="10" data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-minlength="10" />
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.required && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.minlength  && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																																																																	 
</div>												 
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.otherNo{{CompanyData.id}}.$invalid && (CompanyForm.otherNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="Other Number" name="otherNo{{CompanyData.id}}" data-ng-model="CompanyData.otherNo" maxlength="10" data-ng-pattern="/^[0-9\s]*$/" />
	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.required && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.otherNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.otherNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.fax{{CompanyData.id}}.$invalid && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="Fax" name="fax{{CompanyData.id}}" data-ng-model="CompanyData.fax" maxlength="12" data-ng-pattern="/^[0-9\s]*$/" />
	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.required && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>											
	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.pattern  && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.email{{CompanyData.id}}.$invalid && (CompanyForm.email{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="email" class="form-control1" placeholder="E-mail" name="email{{CompanyData.id}}" maxlength="25" data-ng-model="CompanyData.email" />
	<p ng-show="CompanyForm.email{{CompanyData.id}}.$error.required && (CompanyForm.email{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.email{{CompanyData.id}}.$error.email  && (CompanyForm. email{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
											</tr>

										</tbody>
									</table>
								</div>
					<br />
					-->
					
					<tabset justified="true">
			                <tab  heading="Company Basic Information">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Action</span></th>
															<th><span>Branch</span></th>
															<th><span>Location</span></th>
															<th><span>City</span></th>
															<th><span>State</span></th>
															<th><span>District</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat='CompanyData in data'>
															<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addComapnyBranchField();' ng-if="CompanyData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeCompanyBranch(CompanyData.id);' ng-if="CompanyData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="CompanyData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="CompanyData.branchCode"/>
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.name{{CompanyData.id}}.$invalid && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																											
<input type="text" class="form-control1" placeholder="Branch Name" name="name{{CompanyData.id}}"  data-ng-model="CompanyData.name" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.name{{CompanyData.id}}.$error.required && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.name{{CompanyData.id}}.$error.pattern  && (CompanyForm.name{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																					 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.location{{CompanyData.id}}.$invalid && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Location" name="location{{CompanyData.id}}" data-ng-model="CompanyData.location" data-ng-required='true' data-ng-pattern="/^[a-z  A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.location{{CompanyData.id}}.$error.required && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.location{{CompanyData.id}}.$error.pattern  && (CompanyForm.location{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.city{{CompanyData.id}}.$invalid && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																			
							<input type="text" class="form-control1" placeholder="City" name="city{{CompanyData.id}}" data-ng-model="CompanyData.city" data-ng-required='true' data-ng-pattern="/^[a-z  A-Z\s]*$/" maxlength="25"/>
	<p ng-show="CompanyForm.city{{CompanyData.id}}.$error.required && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.city{{CompanyData.id}}.$error.pattern  && (CompanyForm.city{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>												  
												</td>
												<td style="width:100px;">

<div class="form-group" ng-class="{ 'has-error' : CompanyForm.stateCode{{CompanyData.id}}.$invalid && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)}">	
 <select class="form-control1"  placeholder="State"  name="stateCode{{CompanyData.id}}" data-ng-model="CompanyData.stateCode" data-ng-required='true' ng-options="state.id as state.state for state in gridStateNames | orderBy:'-state':true" ng-change="loadGridDistrictNames(CompanyData.stateCode);">	
												 	<option value="">Select</option>
													<option value="">{{CompanyData.stateCode}}</option>
												 </select>
	<p ng-show="CompanyForm.stateCode{{CompanyData.id}}.$error.required && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.stateCode{{CompanyData.id}}.$error.pattern  && (CompanyForm.stateCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>
												</td>
												<td style="width:100px;">
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.districtCode{{CompanyData.id}}.$invalid && (CompanyForm.districtCode{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
 <select class="form-control1"  placeholder="District"  name="districtCode{{CompanyData.id}}" data-ng-model="CompanyData.districtCode" data-ng-required='true' ng-options="district.id as district.district for district in gridDistrictNames | orderBy:'-district':true">	
												 	<option value="">Select</option>
													<option value="">{{CompanyData.districtCode}}</option>
												 	</select>
	<p ng-show="CompanyForm.districtCode{{CompanyData.id}}.$error.required && (CompanyForm.districtCode{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.district{{CompanyData.id}}.$error.pattern  && (CompanyForm.district{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																

</div>											
												</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
                			<tab  heading="Additional Information">
								<div class="table-responsive">
								
									<section class="asdok">
										<div class="container1">																			
							                  <table class="table table-striped" style="width:100%;">								                  
												 <thead>
													<tr>
													
														<th><span>Action</span></th>
														<th><span>Phone No.</span></th>
														<th><span>Mobile No.</span></th>
														<th><span>Other No.</span></th>
														<th><span>Fax No.</span></th>
														<th><span>Email-ID</span></th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat='CompanyData in data'>
													<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addComapnyBranchField();' ng-if="CompanyData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeCompanyBranch(CompanyData.id);' ng-if="CompanyData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="CompanyData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(CompanyData.id);' ng-if="CompanyData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="CompanyData.branchCode"/>
												</td>
													<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.phoneNo{{CompanyData.id}}.$invalid && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																			
									<input type="text" class="form-control1" placeholder="Phone Number" name="phoneNo{{CompanyData.id}}" data-ng-model="CompanyData.phoneNo" maxlength="12" data-ng-minlength="12" data-ng-pattern="/^[0-9\s]*$/"/>
	<p ng-show="CompanyForm.contactPerson{{CompanyData.id}}.$error.required && (CompanyForm.contactPerson{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.phoneNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
	<p ng-show="CompanyForm.phoneNo{{CompanyData.id}}.$error.minlength  && (CompanyForm.phoneNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																																															
												  
</div>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.mobileNo{{CompanyData.id}}.$invalid && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																															
					<input type="text" class="form-control1" placeholder="mobileNo" name="mobileNo{{CompanyData.id}}" data-ng-model="CompanyData.mobileNo" data-ng-required='true'     maxlength="10" data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-minlength="10" />
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.required && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>
	<p ng-show="CompanyForm.mobileNo{{CompanyData.id}}.$error.minlength  && (CompanyForm.mobileNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																																																																	 
</div>												 
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.otherNo{{CompanyData.id}}.$invalid && (CompanyForm.otherNo{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="Other Number" name="otherNo{{CompanyData.id}}" data-ng-model="CompanyData.otherNo" maxlength="10" data-ng-pattern="/^[0-9\s]*$/" />
	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.required && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.otherNo{{CompanyData.id}}.$error.pattern  && (CompanyForm.otherNo{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.fax{{CompanyData.id}}.$invalid && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="Fax" name="fax{{CompanyData.id}}" data-ng-model="CompanyData.fax" maxlength="12" data-ng-pattern="/^[0-9\s]*$/" />
	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.required && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>											

	<p ng-show="CompanyForm.fax{{CompanyData.id}}.$error.pattern  && (CompanyForm.fax{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : CompanyForm.email{{CompanyData.id}}.$invalid && (CompanyForm.email{{CompanyData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																																							
	<input type="email" class="form-control1" placeholder="E-mail" name="email{{CompanyData.id}}" maxlength="25" data-ng-model="CompanyData.email" />
	<p ng-show="CompanyForm.email{{CompanyData.id}}.$error.required && (CompanyForm.email{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
	<p ng-show="CompanyForm.email{{CompanyData.id}}.$error.email  && (CompanyForm. email{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
													</tr>										
												</tbody>
											</table>											
									</div>
								 </section>
								 									
								 
							  </div>
            			    </tab>
			                
            			    
            			    				
			            </tabset>
						<br />
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="reset" class="btn btn-primary" ng-click='reset(CompanyForm);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
				
				</form>								 
				   			
				        </div>
			    	</div>
							</div>
					
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
