	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		 $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	
	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.textbox
		{
			width:100px;	
		}
		.spacing-table {
  
   
    border-collapse: separate;
    border-spacing: 0 5px; /* this is the ultimate fix */
}
.spacing-table th {
    text-align: left;
    
}
.spacing-table td {
    border-width: 1px 0;
  
   
    
}
.spacing-table td:first-child {
    border-left-width: 3px;
    border-radius: 5px 0 0 5px;
}
.spacing-table td:last-child {
    border-right-width: 3px;
    border-radius: 0 5px 5px 0;
}
	</style>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="authorisationAcknowledgementController" ng-init="loadFieldOfficerDropdown();loadRyotCodeDropDown();loadusername();loadVarietyNames();loadKindType();loadseasonNames();loadFieldAssistants();loadStoreCode();onloadFunction();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Authorisation Acknowledgement</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->				
						<form name="AckForm"  novalidate ng-submit="authorisationAckSubmit(AddedAcknowledgementForm,AckForm);">
							<!--  <input type="hidden" name="modifyFlag" ng-model="plantupdate.modifyFlag" />-->
							  <div class="row">
							  <div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.season.$invalid && (AckForm.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedAcknowledgementForm.season'  name="season" ng-options="season.season as season.season for season in seasonNames" data-ng-required="true " ng-change="getauthorisationAckData(AddedAcknowledgementForm.season,AddedAcknowledgementForm.authorisationNo);">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="AckForm.season.$error.required && (AckForm.season.$dirty || submitted)" class="help-block">Select Season</p>													
													</div>
															
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
								<div class="col-sm-5">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : AckForm.authorisationNo.$invalid && (AckForm.authorisationNo.$dirty || Addsubmitted)}">
											  <div class="fg-line">
												<label for="authorisationNo">Authorisation No.</label>
													<input type="text" placeholder="Authorisation No." name="authorisationNo" class="form-control" ng-model="AddedAcknowledgementForm.authorisationNo" data-ng-required="true" ng-blur="getauthorisationAckData(AddedAcknowledgementForm.season,AddedAcknowledgementForm.authorisationNo);"  id="authorisationNo" with-floating-label />
												
											 </div>
								<p ng-show="AckForm.authorisationNo.$error.required && (AckForm.authorisationNo.$dirty || Addsubmitted)" class="help-block">Authorisation No. is required</p>										
										   </div>
									   </div>
										
									</div>
								
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.ryotCode.$invalid && (AckForm.ryotCode.$dirty || submitted)}">
																<div class="fg-line">
				<select chosen class="w-100" data-ng-required="true"  name="ryotCode" data-ng-model='AddedAcknowledgementForm.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-id':true"  ng-change="loadRyotDet(AddedAcknowledgementForm.ryotCode);getaggrementDetails(AddedAcknowledgementForm.season,AddedAcknowledgementForm.ryotCode);getIndentnumbers(AddedAcknowledgementForm.season,AddedAcknowledgementForm.ryotCode);">
															<option value="">Ryot Code</option>
														</select >
																</div>
															<p ng-show="AckForm.ryotCode.$error.required && (AckForm.ryotCode.$dirty || submitted)" class="help-block">RyotCode is required</p>											
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
							</div>
							
							<div class="row">
								
								
								<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.agreementnumber.$invalid && (AckForm.agreementnumber.$dirty || submitted)}">
																<div class="fg-line">
				<select chosen class="w-100" data-ng-required="true"   name='agreementnumber' ng-model="AddedAcknowledgementForm.agreementnumber" ng-options="agreementnumber.agreementnumber as agreementnumber.agreementnumber for agreementnumber in agreementnumberNames" ng-change="getfofaseedlingQty(AddedAcknowledgementForm.agreementnumber,AddedAcknowledgementForm.season);"  >
															<option value="">Agreement Number</option>
														</select>
																</div>
																<p ng-show="AckForm.agreementnumber.$error.required && (AckForm.agreementnumber.$dirty || submitted)" class="help-block">Agreement Number is required</p>						
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.fieldOfficer.$invalid && (AckForm.fieldOfficer.$dirty || submitted)}">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  name="fieldOfficer" data-ng-required="true" data-ng-model='AddedAcknowledgementForm.fieldOfficer' ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in FieldOffNames | orderBy:'fieldOfficer':true">
																	<option value="">Field Officer</option>																	</select>	
																</div>
																<p ng-show="AckForm.fieldOfficer.$error.required && (AckForm.fieldOfficer.$dirty || submitted)" class="help-block">Field Officer is required</p>	
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
							<div class="col-sm-4">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : AckForm.faCode.$invalid && (AckForm.faCode.$dirty || submitted)}">
												<div class="fg-line">
														 <select chosen class="w-100" name='faCode' ng-model="AddedAcknowledgementForm.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true">
														<option value="">Field Assistant</option>
														
														</select>
				    		            	        </div>
														
				    		            	        
					<p ng-show="AckForm.faCode.$error.required && (AckForm.faCode.$dirty || submitted)" class="help-block">Field Assistant is required.</p>
																						
												</div>
			            		        	</div>
										</div>
							</div>
							
								<div class="row">
								
								<!--<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  >
																<div class="fg-line">
			
														<select  chosen class="w-100"  placeholder='IndentNo' id="indent"   name="indentNumbers" ng-model="AddedAcknowledgementForm.indentNumbers"   ng-options="indentNumbers.IndentNo as indentNumbers.IndentNo for indentNumbers in indentNoNames" >
														<option value="">Indent Number</option>
														</select>	
																</div>
															
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>-->
								
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
														<label for="entrydate">Date Of Entry</label>
														<input type="text" class="form-control" placeholder='Date Of Entry' id="entrydate" with-floating-label  name="authorisationDate" ng-model="AddedAcknowledgementForm.authorisationDate"   tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true"  ng-readonly='true' style="background-color:#FFFFFF;" />	
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								<div class="col-sm-5">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.ryotName.$invalid && (AckForm.ryotName.$dirty || submitted)}">
																<div class="fg-line">
																<label for="ryotname">RyotName</label>
														<input type="text" class="form-control" placeholder='RyotName' id="ryotname" with-floating-label  name="ryotname" ng-model="AddedAcknowledgementForm.ryotName" ng-readonly='true' style="background-color:#FFFFFF;"     />	
														
				        		        	        </div>
																<p ng-show="AckForm.ryotName.$error.required && (AckForm.ryotName.$dirty || submitted)" class="help-block">Ryot Name is required.</p>	
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper" >
																<div class="fg-line">
																<label for="Indents">Indents</label>
														<input type="text" class="form-control" placeholder='Indents' id="Indents" with-floating-label  name="indentNumbers" ng-model="AddedAcknowledgementForm.indentNumbers" ng-required="true" ng-readonly='true' style="background-color:#FFFFFF;"    />	
														
				        		        	        </div>
															<!--<p ng-show="AckForm.indentNumbers.$error.required && (AckForm.indentNumbers.$dirty || submitted)" class="help-block">Indent Numbers required</p>				-->									
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>		 
							<div class="row">
							  		
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
				<label for="AddedAcknowledgementForm">Authorised By</label>
														<input type="text" class="form-control" placeholder='Authorised By' id="AddedAcknowledgementForm" with-floating-label  name="authorisedBy" ng-model="AddedAcknowledgementForm.authorisedBy"   ng-readonly='true' style="background-color:#FFFFFF;"   />	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.phoneNo.$invalid && (AckForm.phoneNo.$dirty || submitted)}">
																<div class="fg-line">
																<label for="phonenum">Phone Number</label>
														<input type="text" class="form-control" placeholder='Phone Number' id="phonenum" with-floating-label  name="phoneNo" ng-model="AddedAcknowledgementForm.phoneNo"    ng-readonly='true' style="background-color:#FFFFFF;"   data-ng-required="true" />	
														
				        		        	        </div>
																<p ng-show="AckForm.phoneNo.$error.required && (AckForm.phoneNo.$dirty || submitted)" class="help-block">Phone Number is required</p>		
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-2">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.noOfAcr.$invalid && (AckForm.noOfAcr.$dirty || submitted)}">
																<div class="fg-line">
																<label for="noOfAcr">No. of Acres</label>
														<input type="text" class="form-control" placeholder='No. of Acres' id="noOfAcr" with-floating-label  name="noOfAcr" ng-model="AddedAcknowledgementForm.noOfAcr" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  ng-readonly='true' style="background-color:#FFFFFF;"   data-ng-required="true" />	
														
				        		        	        </div>
																<p ng-show="AckForm.noOfAcr.$error.required && (AckForm.noOfAcr.$dirty || submitted)" class="help-block">No.of Acres is required</p>		
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
								<div class="col-sm-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : AckForm.Sauthorization.$invalid && (AckForm.Sauthorization.$dirty || submitted)}">
										<div class="fg-line">
										 <select chosen class="w-100" name="Sauthorization" data-ng-model='AddedAcknowledgementForm.storeCode' data-ng-required='true' ng-options="Sauthorization.storeCode as Sauthorization.city for Sauthorization in storeCodeData  | orderBy:'-storeCode':true" >
										   <option value="">Store Code</option>
				        				 </select>
				    		            </div>										 
										<p ng-show="AckForm.Sauthorization.$error.required && (AckForm.Sauthorization.$dirty || Addsubmitted)" class="help-block">Select Store Code</p>										
									</div>
								</div>
										
								</div>
							</div>
							<div class="row">
							
							  	<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
															<div class="form-group  floating-label-wrapper" ng-class="{ 'has-error' : AckForm.billRefDate.$invalid && (AckForm.billRefDate.$dirty || Addsubmitted)}">
																<div class="fg-line">
														<label for="billRefDate">Bill RefDate</label>
														<input type="text" class="form-control date" placeholder='Bill RefDate' id="billRefDate" with-floating-label  name="billRefDate" ng-model="AddedAcknowledgementForm.billRefDate"   readonly tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true"  style="background-color:#FFFFFF;" />	
				        		        	        </div>
														<p ng-show="AckForm.billRefDate.$error.required && (AckForm.billRefDate.$dirty || Addsubmitted)" class="help-block">billRefDate is Required.</p>		
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : AckForm.billRefNum.$invalid && (AckForm.billRefNum.$dirty || submitted)}">
																<div class="fg-line">
																<label for="billRefNum">Bill RefNumber</label>
														<input type="text" class="form-control" placeholder='bill RefNumber' id="billRefNum" with-floating-label  name="billRefNum" ng-model="AddedAcknowledgementForm.billRefNum" style="background-color:#FFFFFF;"     />	
														
				        		        	        </div>
																<p ng-show="AckForm.billRefNum.$error.required && (AckForm.billRefNum.$dirty || submitted)" class="help-block">Ryot Name is required.</p>	
															</div>
														</div>
													</div>
										</div>
							  		
								</div>
							</div>
							
							
							
							  <br/>
							
							 
							 
							   							
							 	 					 	
							 		
									
									
										
								
							
							
							 
							
								<div class="table-responsive" ng-if="authorizedData!=0" id="hidetable">
							 	<table class="table">
							 		<thead>
								        <tr>
											<th>Action</th>
											
							              
		                    				
											<th>Indent NO</th>
											<th>Item Type</th>
											<th>Qty</th>
											
											
											
											<th>Ryot Taken</th>
											<th>Cost Of Each Item</th>
											<th>Total Cost</th>
											<th>Ryot Amt</th>
											<th>Supplier Amt</th>
											
											
											
            							</tr>											
									</thead>
									
									<tbody>
										<tr ng-repeat="plantData in authorizedData">
											<td>
											
											
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow($index,plantData.indentNumbers);" ><i class="glyphicon glyphicon-remove-sign"></i></button>
												<input  type="hidden" ng-model="plantData.id" name="plantData{{$index}}" />												
											</td>


											
								<td>
									<input type="text" class="form-control1"  placeholder=""  name="indentNumbers{{$index}}" ng-model="plantData.indentNumbers" readonly   />
									</td>
									<td>
									<select class="form-control1" name="kind{{$index}}" data-ng-model='plantData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
														<option value="" > Kind Type</option>	
														</select>									
												

											
											</td>     
											
											<td>
										<input type="text" class="form-control1"  placeholder=""  name="quantity{$index}}" ng-model="plantData.quantity" ng-keyup="updatePendingAmtGrid(plantData.quantity,plantData.indent,plantData.allowed,$index);updategridTotalCost(plantData.quantity,plantData.costofEachItem,$index);" readonly  />
										</td>
											
											
											
											
											<td>
											<div ng-class="{ 'has-error' : AckForm.taken{{$index}}.$invalid && (AckForm.taken{{$index}}.$dirty || submitted)}"><input type="text" id='taken{{$index}}' name="taken{{$index}}" ng-model="plantData.taken" class="form-control1" placeholder="Taken" ng-blur="validTakenQty(plantData.quantity,plantData.taken,$index);" ng-keyup="updateConsumerAmt(plantData.taken,plantData.costofEachItem,plantData.quantity,$index);updateRyotTotal(AddedAcknowledgementForm.totalCost,AddedAcknowledgementForm.grandConsumerTotal);" ng-required='true'   />
											<p ng-show="AckForm.taken{{$index}}.$error.required && (AckForm.taken{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>
											
											</div></td>
											<td ><input type="text" class="form-control1 " ng-model="plantData.costofEachItem" name="costofEachItem{{$index}}" placeholder="" ng-keyup="updategridTotalCost(plantData.quantity,plantData.costofEachItem,$index);updateConsumerAmt(plantData.taken,plantData.costofEachItem,plantData.quantity,$index); updateRyotTotal(AddedAcknowledgementForm.totalCost,AddedAcknowledgementForm.grandConsumerTotal);"   /></td>
											<td><input type="text" class="form-control1"  placeholder=""  name="totalCost{{$index}}" ng-model="plantData.totalCost" readonly   /></td>
											
											<td><input type="text" class="form-control1"  placeholder=""  name="ryotAmt{{$index}}" ng-model="plantData.ryotAmt" readonly   />
											</td>
											
											<td><input type="text" class="form-control1"  placeholder=""  name="consumerAmt{{$index}}" ng-model="plantData.consumerAmt" readonly   />
											</td>
											
											</tr>
											<tr ng-if="authorizedData!=0">
											<td colspan="6"></td><td>
									<input type="text" class="form-control1"  placeholder="Grand  Total"  name="totalCost" ng-model="AddedAcknowledgementForm.totalCost" ng-disabled="true" style="color:#000000; width:120px;"    />
									</td><td>
									<input type="text" class="form-control1"  placeholder="Grand Total"  name="grandRyotTotal" ng-model="AddedAcknowledgementForm.grandRyotTotal" ng-disabled="true" style="color:#000000; width:120px;"    />
									</td>
									<td>
									
								<input type="text" class="form-control1"  placeholder="Grand Total"  name="grandConsumerTotal" ng-model="AddedAcknowledgementForm.grandConsumerTotal" ng-disabled="true" style="color:#000000;width:100px;"    />
									</td>
											
											</tr>
									</tbody>
							 	</table>
							</div>
								
									
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Authorise</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(AckForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>				 	
					 		 
													 
					<!------------------form end---------->							 
					<!-------from-end--------------------->	
					
					
					 	 
					 
					 
					 
					 
					 
					 
					 	 						           			
				        </div>
			    	</div>
					
					
				</div>
			
							
	    </section> 
	</section>
	
	
	
	
	

	<!--<footer id="footer" data-ng-include="'template/footer.jsp'"></foo