	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script> 
	

	<!-- <header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header> -->
	<section id="main" class='bannerok'>    
	 <!--   <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside> -->

    	<section id="content" data-ng-controller="labourContractorController" data-ng-init="loadStatus();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Labour Contractor Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="labourContractorForm" novalidate ng-submit="labourContractorSubmit(AddedLabourContractor,labourContractorForm)">
					   <div class="row">
					     <div class="col-sm-12">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                            <div class="form-group floating-label-wrapper" ng-class="{'has-error' : labourContractorForm.contractor.$invalid && (labourContractorForm.contractor.$dirty || Addsubmitted)}">
										  <div class="fg-line">
										    <label for="contractor">Contractor</label>
										     <input type="text" class="form-control " autofocus placeholder="Contractor"    name="contractor" ng-model="AddedLabourContractor.contractor"   data-ng-required='true' tabindex="1" data-ng-pattern= "/^[a-z 0-9 A-Z\s]*$/"  maxlength="25" id="contractor" with-floating-label/>
											 </div>
									<p ng-show = "labourContractorForm.contractor.$error.required &&(labourContractorForm.contractor.$dirty || Addsubmitted)" class="help-block"> Contractor name is required</p>
									<p ng-show = "labourContractorForm.contractor.$error.pattern &&(labourContractorForm.contractor.$dirty || Addsubmitted)" class="help-block"> Enter Valid Contractor name </p>
									
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
					     <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                            <div class="form-group floating-label-wrapper" ng-class="{'has-error' : labourContractorForm.contractorCode.$invalid && (labourContractorForm.contractorCode.$dirty || Addsubmitted)}">
										  <div class="fg-line">
										    <label for="contractorCode">Contractor Code</label>
										     <input type="text" class="form-control" placeholder="Contractor Code"  name="contractorCode" ng-model="AddedLabourContractor.contractorCode"   data-ng-required='true' tabindex="2" data-ng-pattern= "/^[ 0-9 \s]*$/"  maxlength="10" readonly="readonly" id="contractorCode" with-floating-label/>
											 </div>
									<p ng-show = "labourContractorForm.contractorCode.$error.required && (labourContractorForm.contractorCode.$dirty || Addsubmitted)" class="help-block"> Contractor Code is required</p>
									<p ng-show = "labourContractorForm.contractorCode.$error.pattern && (labourContractorForm.contractorCode.$dirty || Addsubmitted)" class="help-block"> Enter Valid Contractor Code </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						 <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                            <div class="form-group floating-label-wrapper">
										  <div class="fg-line">
										     <label for="description">Description</label>
										     <input type="text" class="form-control" placeholder="Description"  name="description" ng-model="AddedLabourContractor.description"    tabindex="3" data-ng-pattern= "/^[a-z 0-9 A-Z\s]*$/"  maxlength="25" id="description" with-floating-label/>
											 </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						 <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group" style="margin-top:10px;">
		            				    <span class="input-group-addon">Status :</span>
			                            <div class="form-group">
										  <div class="fg-line">
										    <label class="radio radio-inline m-r-20">
													<input type="radio" name="inlineRadioOptions" value="0" checked="checked" data-ng-model='AddedLabourContractor.status' tabindex="4" data-ng-required='true'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            						<input type="radio" name="inlineRadioOptions" value="1"  data-ng-model='AddedLabourContractor.status' tabindex="4" data-ng-required='true'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>				
											 </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
					 <br />	
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="5">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(labourContractorForm);" tabindex="6">Reset</button>
									</div>
								</div>
								</div>							
							</div>
                 </form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
						<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Labour Contractors</b></h2></div>
					
					<div class="card">						
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 	
								<form name="labourContractorEdit" novalidate>					
							        <table  class="table table-striped table-vmiddle" >
										<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Contractor</span></th>
																<th><span>Contractor Code</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																
																</tr>
														</thead>
														<tbody>
								        <tr ng-repeat="labourContractor in labourContractorData"  ng-class="{ 'active': labourContractor.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!labourContractor.$edit" ng-click="labourContractor.$edit = true;labourContractor.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="labourContractor.$edit" ng-click="updatelabourContractor(kind,$index);labourContractor.$edit = isEdit"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="labourContractor.modifyFlag"  />
											   
		                    				 </td>
							                  <td>
                		    					<span ng-if="!labourContractor.$edit">{{labourContractor.contractor}}</span>
					    		                <div ng-if="labourContractor.$edit">
									<div class="form-group" ng-class="{'has-error' :labourContractorEdit.contractor{{$index}}.$invalid && (labourContractorEdit.contractor{{index}}.$dirty || submitted)}">
														<input class="form-control" type="text" ng-model="labourContractor.contractor" name="contractor{{$index}}" placeholder='Contractor' maxlength="10" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
										<p ng-show="labourContractorrEdit.contractor{{index}}.$error.required && (labourContractorEdit.contractor{{index}}.$dirty || submitted)" class="help-block">Contractor name required.</p>
													<p ng-show="labourContractorEdit.contractor{{index}}.$error.pattern && (labourContractorEdit.contractor{{index}}.$dirty || submitted)" class="help-block"> Enter valid contractor name </p>
													</div>
												</div>
					            		      </td>
											  
		                    				  <td>
							                     <span ng-if="!labourContractor.$edit">{{labourContractor.contractorCode}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : labourContractorEdit.contractorCode{{index}}.$invalid && (labourContractorEdit.contractorCode{{index}}.$dirty || submitted)}">
							                     <div ng-if="labourContractor.$edit">
												 	<input class="form-control" type="text" ng-model="labourContractor.contractorCode" placeholder='Contractor Code' maxlength="25" name="contractorCode{{index}}" data-ng-pattern="/^[0-9 \s]*$/" ng-required='true'/>
													<p ng-show="labourContractorrEdit.contractorCode{{index}}.$error.required && (labourContractorEdit.contractorCode{{index}}.$dirty || submitted)" class="help-block">Contractor code is required.</p>
													<p ng-show="labourContractorEdit.contractorCode{{index}}.$error.pattern && (labourContractorEdit.contractorCode{{index}}.$dirty || submitted)" class="help-block"> Enter valid contractor code</p>
													
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!labourContractor.$edit">{{labourContractor.description}}</span>
        		            					  <div ng-if="labourContractor.$edit">
												  	<div class="form-group">
													  	<input class="form-control" type="text" ng-model="labourContractor.description" placeholder='Description' maxlength="50" name="description" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!labourContractor.$edit">
												  	<span ng-if="labourContractor.status=='0'">Active</span>
													<span ng-if="labourContractor.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="labourContractor.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model="labourContractor.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="labourContractor.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
											 
											  
							             </tr>
										 </tbody>
										 
								         </table>
										 
										 </form>
										 </div>
										 </section>							 
							     </div>
							</div>
						</div>
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
