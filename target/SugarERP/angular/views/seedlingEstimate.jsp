
<style>
.styletr {border-bottom: 1px solid grey;}
#styletr:nth-child(odd) {
    background: #E9E9E9;
}

#styletr:nth-child(even) {
    background: #FFFFFF;
}

</style>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="SeedlingEstimateController" ng-init="loadSeason();loadFieldOfficerDropdown();loadVarietyNames();loadmonthNames();loadPeriodNames(); getAllSeedlingEstimate();" >   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Estimate</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="SeedlingEstimate" ng-submit="seedlingEstimateSubmit(AddedSeedlingEstimateQty,SeedlingEstimate)" novalidate>
					 		<div class="row">
										<input type="hidden" ng-model="AddedSeedlingEstimateQty.modifyFlag" />
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.season' name="season" ng-options="season.season as season.season for season in SeasonData">
															<option value="">Select Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.employeeId.$invalid && (SeedlingEstimate.employeeId.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1"   name="employeeId"  ng-model="AddedSeedlingEstimateQty.employeeId"  data-ng-required="true"  ng-options="fieldofficername.id as fieldofficername.employeename for fieldofficername in FieldOffNames  | orderBy:'-employeename':true" >
															<option value="">Select Field Officer</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.employeeId.$error.required && (SeedlingEstimate.employeeId.$dirty || Filtersubmitted)" class="help-block">Select Field Officer</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.varietyCode.$invalid && (SeedlingEstimate.varietyCode.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.varietyCode' name="varietyCode" ng-options="varietyCode.id as varietyCode.variety for varietyCode in varietyNames | orderBy:'-variety':true">
															<option value="">Select Variety</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.varietyCode.$error.required && (SeedlingEstimate.varietyCode.$dirty || Filtersubmitted)" class="help-block">Select Variety</p>		 
											  </div>
			                    	      </div>
										</div>
											<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.quantity.$invalid && (SeedlingEstimate.quantity.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
													<input type="text" class="form-control" data-ng-required="true" placeholder="Quantity" name="quantity" data-ng-model="AddedSeedlingEstimateQty.quantity" data-ng-pattern="/^[0-9.\s]*$/" maxlength="5" />
    	            						    		<!--<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.circle' name="circle" ng-options="circle.id as circle.circle for circle in circleNames">
															<option value="">Quantity</option>
						        			            </select-->
		                	            			  </div>
					<p ng-show="SeedlingEstimate.quantity.$error.required && (SeedlingEstimate.quantity.$dirty || Filtersubmitted)" class="help-block">Quantity is required</p>	
					<p ng-show="SeedlingEstimate.quantity.$error.pattern && (SeedlingEstimate.quantity.$dirty || Filtersubmitted)" class="help-block">Quantity is invalid</p>		 
											  </div>
			                    	      </div>
										</div>									
							</div>
							
							
							
										<div class="row">
											
										
								  
								  <div class="col-sm-4">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.year.$invalid && (SeedlingEstimate.year.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"  data-ng-required="true"   data-ng-model='AddedSeedlingEstimateQty.yearOfPlanting' name="year" ng-options="year.yearId as year.year for year in year">
															<option value="">Year</option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="SeedlingEstimate.year.$error.required && (SeedlingEstimate.year.$dirty || Filtersubmitted)" class="help-block"> Select Year</p>
										</div>
			                    	</div>			                    
				                  </div>
								  
										<!--<div class="col-sm-4">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.monthId.$invalid && (SeedlingEstimate.monthId.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"  data-ng-required="true"    data-ng-model='AddedSeedlingEstimateQty.monthId' name="monthId" ng-options="monthId.id as monthId.month for monthId in monthNames">
															<option value="">Month </option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="SeedlingEstimate.month.$error.required && (SeedlingEstimate.month.$dirty || Filtersubmitted)" class="help-block"> Select Month</p>
										</div>
			                    	</div>			                    
				                  </div>-->
								  
								  <div class="col-sm-4">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.monthId.$invalid && (SeedlingEstimate.monthId.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"  data-ng-required="true"   data-ng-model='AddedSeedlingEstimateQty.monthId' name="monthId" ng-options="monthId.id as monthId.month for monthId in monthNames">
															<option value="">Month</option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="SeedlingEstimate.monthId.$error.required && (SeedlingEstimate.monthId.$dirty || Filtersubmitted)" class="help-block"> Select Month</p>
										</div>
			                    	</div>			                    
				                  </div>
								  
								  
										<div class="col-sm-4">
				                    <div class="input-group">										
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.periodId.$invalid && (SeedlingEstimate.periodId.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"  data-ng-required="true"  tabindex="2"  data-ng-model='AddedSeedlingEstimateQty.periodId' name="periodId" ng-options="periodId.id as periodId.period for periodId in periodNames">
															<option value="">Period</option>
				        				    </select>
                	            		  </div>
			                	        </div>
										<p ng-show="SeedlingEstimate.periodId.$error.required && (SeedlingEstimate.periodId.$dirty || Filtersubmitted)" class="help-block"> Select Period</p>
										</div>
			                    	</div>			                    
				                  </div>										
										
										</div>
										<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SeedlingEstimate);">Reset</button>
									</div>
								</div>						 	
							</div>	
					</form>		
							
							<br /><br />		
											 		
						 
							
							<!----------table grid design--------->
								<div class="table-responsive">
								<form name="seedlingEstimateEdit">

        							<!--<table   ng-table="CaneManagerMaster.tableEdit" style="width:100%;">-->
									<table style="width:100%;"  style="border-collapse:collapse;">
			
												<thead>
													<tr class="styletr" style="font-weight:bold;">
														<th><span>ACTION</span></th>
														<th><span>SEASON</span></th>
														<th><span>FIELD OFFICER</span></th>
														<th><span>VARIETY</span></th>	
														<th><span>QUANTITY</span></th>
														
														<th><span>YEAR</span></th>
														<th><span>MONTH</span></th>
														<th><span>PERIOD</span></th>
														</tr>
												</thead>
												<br>
											<tbody>
											

													<tr id="styletr" ng-repeat="Estimate in EstimateData"  ng-class="{ 'active': Estimate.$edit }"   class="styletr"  style="height:50px;">
																		 <td data-title="'Actions'">
																	<button type="button" class="btn btn-default" ng-if="!Estimate.$edit" ng-click="Estimate.$edit = true;Estimate.modifyFlag='Yes'"><i class="zmdi zmdi-edit"></i></button>
																	<button type="submit" class="btn btn-success" ng-if="Estimate.$edit" data-ng-click='updateSeedlingEstimate(Estimate,$index);'><i class="zmdi zmdi-check"></i></button>
																	
																	  </td>
																	  <td style="display:none"><input type="hidden" ng-model="Estimate.id" name="id{{$index}}" /></td>
															<td>
																		 <span ng-if="!Estimate.$edit">{{Estimate.season}}</span>
																	 <div ng-if="Estimate.$edit">
																		<div class="form-group">
																			<select class='form-control1' ng-model="Estimate.season" name="season{{$index}}"  ng-options="season.season as season.season for season in SeasonData" >	</select>
																		</div>
																	</div>
																   </td>
																	   <td>
																	   <span ng-if="!Estimate.$edit" ng-repeat='employee in FieldOffNames'>
  																		<span ng-if="employee.id==Estimate.employeeId">{{employee.employeename}}</span>
																		</span>
																 
																 <div ng-if="Estimate.$edit">
																	<div class="form-group">
																		<select class='form-control1' ng-model="Estimate.employeeId" name="employeeId{{$index}}"  ng-options="employeeId.id as employeeId.employeename for employeeId in FieldOffNames  | orderBy:'-employeename':true" >	</select>
																	</div>
																</div>
															  </td>
															  <td>
																	<span ng-if="!Estimate.$edit" ng-repeat='variety in varietyNames'>
  																		<span ng-if="variety.id==Estimate.varietyCode">{{variety.variety}}</span>
																		</span>

																		 
																		 <div ng-if="Estimate.$edit">
																			<div class="form-group">
																				<select class='form-control1' ng-model="Estimate.varietyCode" name="varietyCode{{$index}}" ng-options="varietyCode.id as varietyCode.variety for varietyCode in varietyNames | orderBy:'-variety':true" >	</select>
																			</div>
																		</div>
																	  </td>
															  <td data-title="'Estimate'">
																  <span ng-if="!Estimate.$edit">{{Estimate.quantity}}</span>
																  <div ng-if="Estimate.$edit">
																	<div class="form-group">
																	<input class="form-control1" type="text" ng-model="Estimate.quantity" name="quantity{{$index}}"  placeholder='quantity'/>
																	  
																	</div>
																  </div>	             	       
																	</td>
																			 <td>
																		 <span ng-if="!Estimate.$edit">{{Estimate.yearOfPlanting}}</span>
																		 <div ng-if="Estimate.$edit">
																			<div class="form-group">
																				<select class='form-control1' ng-model="Estimate.yearOfPlanting" name="year{{$index}}"  ng-options="year.yearId as year.year for year in year" >	</select>
																			</div>
																		</div>
																	  </td>
																	  <td>
																		 
																		 <span ng-if="!Estimate.$edit" ng-repeat='month in monthNames'>
  																		<span ng-if="month.id==Estimate.monthId">{{month.month}}</span>
																		</span>
																		 <div ng-if="Estimate.$edit">
																			<div class="form-group">
																				<select class='form-control1' ng-model="Estimate.monthId" name="monthId{{$index}}"  ng-options="monthId.id as monthId.month for monthId in monthNames" >	</select>
																			</div>
																		</div>
																	  </td>
																	  <td>
																	  <span ng-if="!Estimate.$edit" ng-repeat='period in periodNames'>
  																		<span ng-if="period.id==Estimate.periodId">{{period.period}}</span>
																		</span>
																		 
																		 <div ng-if="Estimate.$edit">
																			<div class="form-group">
																				<select class='form-control1' ng-model="Estimate.periodId" name="periodId{{$index}}"  ng-options="periodId.id as periodId.period for periodId in periodNames">	</select>
																			</div>
																		</div>
																	  </td>
																							
														 </tr>
			 								</tbody>
         								</table>
		 
 </form>
								</div>			
					        
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
