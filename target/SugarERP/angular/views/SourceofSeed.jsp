	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
	  $(function() 
	  {
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		
	  });
    </script>			
	<script>
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
				 
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
	</script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SourceOfSeedController" ng-init="loadRyotCodeDropDown();loadCircleNames();loadVillageNames();loadCircleNames();loadVarietyNames();loadseasonNames(); getAllSeedSources(); getOtherRyot();">       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Source of Seed</b></h2></div>
			    <div class="card"> 
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 		
						<div class="row">
							<!--<div class="col-sm-2">
								<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="fg-line">
													<select chosen class="w-100"  data-ng-required="true"   data-ng-model='AddSource.addedYear' name="addedYear" ng-options="addedYear.addedYear as addedYear.addedYear for addedYear in addedYearNames" ng-change="getAddedBatchNo(AddSource.addedYear,AddSource.seedSrCode,AddSource.otherRyot)">
															<option value="">Year</option>
				        				    </select>
											</div>
											</div>
								</div>-->
								<div class="col-sm-4">
								<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="fg-line">
												
												<select chosen class="w-100" data-ng-model="addedBatch" name="addedBatch"  ng-options="addedBatch.addedBatch as addedBatch.addedBatch for addedBatch in addedBatchNames"  ng-change="getBacthNo(addedBatch,AddSource.addedYear);">
												<option value="">Added Batch No.</option>
												</select>
				                    
											
    								
										</div>
												
								</div>	
								</div>
								<div class="col-sm-4">
								      <div class="input-group">
													<div class="form-group" >
														<div class="fg-line nulvalidate">
										<br />
															 <label class="radio radio-inline m-r-20"><b>Update Type</b></label>	
															 <label class="radio radio-inline m-r-20">
																<input type="radio" name="updateType" value="0"   data-ng-model="AddSource.updateType" >
																<i class="input-helper"></i>New
															</label>
															<label class="radio radio-inline m-r-20">
															<input type="radio" name="updateType" value="1"   data-ng-model='AddSource.updateType' ng-click="getAddedSorceDropDown(addedBatch);">
															<i class="input-helper"></i>Change
															</label>
										
														</div>
												</div>
										</div>		
								</div>
								<div class="col-sm-4"  ng-hide="AddSource.updateType=='0'">
									
										<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="fg-line">
													<select chosen class="w-100" name="Dates" data-ng-model='AddSource.dates' ng-options="Dates.Dates as Dates.Dates for Dates in batchDates">
													<option value=''>Select</option>
													</select>
													</div></div></div>		
												
								  </div>
								  
								
											
					 		</div>	
					  <form name="SourceofSeedForm" novalidate ng-submit="SourceofSeedSubmit(AddSource,SourceofSeedForm);" enctype="multipart/form-data">
					  
							<br /><br />
					  <input type="hidden" ng-model="AddSource.modifyFlag" />
					  <input type="hidden" ng-model="AddSource.modifyFlag1" />
					    <input type="hidden" ng-model="AddSource.transactionCode" />
						 <input type="hidden" ng-model="AddSource.suptransactionCode" />
						<input type="hidden" ng-model="AddSource.prvConsumer" />
								<div class="row">
								 <div class="col-sm-2">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.season.$invalid && (SourceofSeedForm.season.$dirty || submitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100 autofocus"   data-ng-required='true' data-ng-model='AddSource.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="loadbatchNo(AddSource.seedType,AddSource.season);">
															<option value="">Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SourceofSeedForm.season.$error.required && (SourceofSeedForm.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
							 	 <div class="col-sm-3">
				                    <div class="input-group">
            				           <span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
										
										<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.date.$invalid && (SourceofSeedForm.date.$dirty || submitted)}">																					
				        	                <div class="fg-line">
											  	<input type="text" placeholder="Date" data-ng-required='true'  class="form-control date autofocus" name="date" ng-model="AddSource.date" readonly maxlength="10" tabindex="1"/>
				                	        </div>
				<p ng-show="SourceofSeedForm.date.$error.required && (SourceofSeedForm.date.$dirty || submitted)" class="help-block">Date required.</p>
										</div>
										
			                    	</div>			                    
				                  </div>
								 <div class="col-sm-5">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Seed Type</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="seedType" value="0"   data-ng-model="AddSource.seedType" ng-change="loadbatchNo(AddSource.seedType,AddSource.season);" >
			    			            					<i class="input-helper"></i>Sugarcane
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="seedType" value="1"   data-ng-model='AddSource.seedType' ng-change="loadbatchNo(AddSource.seedType,AddSource.season);">
			    				            			<i class="input-helper"></i>Vegetables
					  		              				</label>
														<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="seedType" value="2"   data-ng-model='AddSource.seedType' ng-change="loadbatchNo(AddSource.seedType,AddSource.season);">
			    				            			<i class="input-helper"></i>Fruits
					  		              				</label>
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
								 <div class="col-sm-2">
								<div class="input-group">
								<div class="fg-line">
									<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="getAddedSeedSource(AddSource.addedYear,addedBatch,AddSource.dates);" ng-disabled="AddSource.updateType=='0'">Get Details</button>
									
								</div>
							</div>	
								</div>
								 </div> 
								<div class="row">
								  <div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.circleCode.$invalid && (SourceofSeedForm.circleCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												   <select chosen class="w-100"  data-ng-required='changdpStatus'   name="circleCode" ng-model="AddSource.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-cirlce':true" ng-disabled="isDisabled">
													<option value="">Circle</option>
												</select>
					                	        </div>
<p ng-show="SourceofSeedForm.circleCode.$error.required && (SourceofSeedForm.circleCode.$dirty || submitted)" class="help-block">Select Circle</p>	
	
</div>												
				                    	</div>
									</div>
								  <div class="col-sm-5">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Purpose</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="purpose" value="0"   data-ng-model="AddSource.purpose" >
			    			            					<i class="input-helper"></i>Seedling Nursery
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="purpose" value="1"   data-ng-model='AddSource.purpose'>
			    				            			<i class="input-helper"></i>Set Plantation
					  		              				</label>
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
								<div ng-hide="AddSource.purpose=='0'">
								<div class="col-sm-3">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="fg-line">
													<select chosen class="w-100" name="consumerCode" ng-model="AddSource.consumerCode" ng-options="consumerCode.id as consumerCode.id for consumerCode in ryotCode | orderBy:'-id':true"  ng-change="getAgreementNumberByConsumerCode(AddSource.season,AddSource.consumerCode);">
														<option value="">Consumer</option>
													</select>
												</div>
											</div>
										</div>			                    
									  </div>
									  </div>
								  </div>
								<div class="row">
									  

									
									
									<div class="col-sm-3">
											
									        <div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="fg-line">
				                    <label for="batchNo">Batch Series</label>
									<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.batchNo.$invalid && (SourceofSeedForm.batchNo.$dirty || submitted)}">									
												<div class="fg-line">
													<input type="text"   placeholder="Batch Series"  id="batchNo" class="form-control autofocus "  name="batchNo" ng-model= "AddSource.batchNo" with-floating-label ng-change="loadbatchNo(AddSource.batchNo);" maxlength="10" readonly tabindex="2">
												</div>
										<p ng-show="SourceofSeedForm.batchNo.$error.required && (SourceofSeedForm.batchNo.$dirty || submitted)" class="help-block">Batch No. required</p>	
										</div>		
    										</div>		
												</div>
												
												
												
												
												
												
									</div>
									
									
									<div class="col-sm-3">

									        <div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
													<label for="vehicleNo">Vehicle No.</label>
												  	<input type="text" class="form-control" placeholder="Vehicle No."  tabindex="2" name="vehicleNo" ng-model="AddSource.vehicleNo" id="vehicleNo" with-floating-label   />
													
												  </div>
			                		        </div>
			                    		</div>
												
									</div>		                    
									
									
									<div class="col-sm-3">
									 <div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												<a href="" ng-click="getAllWeightmentDetails(AddSource.batchNo);" ><br />Weighment Details</a>
												</div>
			                		        </div>
			                    		</div>
												
									</div>		
									
									<div class="col-sm-3">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
													<select chosen class="w-100" name="agreementNumber" ng-model="AddSource.aggrementNo" ng-options="agreementnumber.agreementnumber as agreementnumber.agreementnumber for agreementnumber in agreementnumberNames"  ng-change="loadAgrPlantQuantity(AddSource.aggrementNo,AddSource.season);">
														<option value="">Agreement Number</option>
													</select>
												</div>
											</div>
										</div>			                    
									  </div>
									
									
									
									
									
									
									

								  </div>
								<div class="row">
										<div class="col-sm-4">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.seedSrCode.$invalid && (SourceofSeedForm.seedSrCode.$dirty || submitted)}">
												<div class="fg-line">
													<select chosen class="w-100" name="seedSrCode"  ng-model="AddSource.seedSrCode" ng-model='seedSrCode' ng-options="seedSrCode.id as seedSrCode.id for seedSrCode in ryotCode | orderBy:'-id':true"  ng-change="loadRyotDet(AddSource.seedSrCode);getAddedBatchNo(AddSource.addedYear,AddSource.seedSrCode,AddSource.otherRyot);getdisabledStatus(AddSource.seedSrCode);loadRyotCircleVillage(AddSource.seedSrCode,AddSource.season)" ng-disabled="isDisabled">
														<option value="">Seed Supplier Code</option>
													</select>
												</div>
											<p ng-show="SourceofSeedForm.seedSrCode.$error.required && (SourceofSeedForm.seedSrCode.$dirty || submitted)" class="help-block">Select Seed Supplier Code</p>	
											</div>
										</div>			                    
									  </div>
										<div class="col-sm-4">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.ryotName.$invalid && (SourceofSeedForm.ryotName.$dirty || submitted)}">
												<div class="fg-line">
												 <label for="ryotname">Ryot Name</label> 
												<input type="text" class="form-control"  data-ng-model="AddSource.ryotName" placeholder="Ryot Name" readonly="readonly"  id="ryotname" with-floating-label  tabindex="3"/>
												
												  
												</div>
												<p ng-show="SourceofSeedForm.ryotName.$error.required && (SourceofSeedForm.ryotName.$dirty || submitted)" class="help-block">Ryot Name required</p>
											</div>
										</div>			                    
									  </div> 
									
										<div class="col-sm-4">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.otherRyot.$invalid && (SourceofSeedForm.otherRyot.$dirty || submitted)}">
													<div class="fg-line">
													<select name="otherRyot" ng-model="AddSource.otherRyot"  chosen class="w-100" ng-options="otherRyot.Otherryotcode as otherRyot.Otherryotname for otherRyot in otherRyotNames | orderBy:'-id':true" ng-change="getOtherRyotName(AddSource.otherRyot,changdpStatus);getAddedBatchNo(AddSource.addedYear,AddSource.seedSrCode,AddSource.otherRyot); getotherRyotenableStatus(AddSource.otherRyot);" ng-disabled="disabled" >
													<option value="">Other Ryot</option>
													</select>
														
														
													  </div>
													<p ng-show="SourceofSeedForm.otherRyot.$error.required && (SourceofSeedForm.otherRyot.$dirty || submitted)" class="help-block">Ryot Name required</p>	  
													  
												</div>
											</div>										
										</div>
									 </div>							 				  
								<div class="row">
										<div class="col-sm-4">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group">
							<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.varietyCode.$invalid && (SourceofSeedForm.varietyCode.$dirty || submitted)}">											
														<div class="fg-line">
															<select chosen class="w-100"   data-ng-required="true" data- data-ng-model='AddSource.varietyCode' name="varietyCode" ng-options="varietyCode.id as varietyCode.variety for varietyCode in varietyNames | orderBy:'-variety':true">
																	<option value="">Variety</option>
																</select>
														</div>
						<p ng-show="SourceofSeedForm.varietyCode.$error.required && (SourceofSeedForm.varietyCode.$dirty || submitted)" class="help-block">Select Variety</p>												
						</div>												
													</div>
												</div>
											</div>
										<div class="col-sm-4">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.villageCode.$invalid && (SourceofSeedForm.villageCode.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  data-ng-required="changdpStatus" data- data-ng-model='AddSource.villageCode' name="villageCode" ng-options="villageCode.id as villageCode.village for villageCode in villageNames | orderBy:'-village':true" ng-disabled="isDisabled">
																	<option value="">Village</option>
																</select>
															
														  
														</div>
														<p ng-show="SourceofSeedForm.villageCode.$error.required && (SourceofSeedForm.villageCode.$dirty || submitted)" class="help-block">Select Village</p>
													</div>
												</div>
											</div>
											<div class="col-sm-4" id="otherRyotVillage" style="display:none;">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.otherRyotVillage.$invalid && (SourceofSeedForm.otherRyotVillage.$dirty || submitted)}" >
														<div class="fg-line">
														<input type="text" class="form-control" ng-model="AddSource.otherRyotVillage" name="otherRyotVillage" placeholder="Village" data-ng-required="otherRyotVillageMandatory"  />
														
															
														  
														</div>
														<p ng-show="SourceofSeedForm.otherRyotVillage.$error.required && (SourceofSeedForm.otherRyotVillage.$dirty || submitted)" class="help-block">Select Village</p>
														
													</div>
												</div>
											</div>
										
										</div>
										<hr />
										<div class="row">
								 
							 	 <div class="col-sm-3">
				                    <div class="input-group">
            				           <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
										<h5 style="font-weight:lighter;"><u>Weighment Details :- </u></h5>
										
			                    	</div>			                    
				                  </div>
								
								 </div>
										
										
										<br />
								<!--<div class="row" ng-hide="AddSource.acreage=='0'">-->
								<div class="row" >
									<div class="col-sm-3">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.caneWeight.$invalid && (SourceofSeedForm.caneWeight.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="caneWei">Load Weight(tons)</label>
												  	<input type="text"    class="form-control" placeholder="Load Weight(tons)" data-ng-pattern="/^[0-9.]+$/" data-ng-required="changeStatus"    name="caneWeight" ng-model="AddSource.caneWeight" id="caneWei" with-floating-label ng-keyup="getNetWeight(AddSource.caneWeight,AddSource.lorryWeight);" ng-blur="getBm1();decimalpoint('caneWeight');" tabindex="4" ng-focus="updateEmpty(AddSource.caneWeight,'caneWeight')" />
													
												  </div>
												<p ng-show="SourceofSeedForm.caneWeight.$error.required && (SourceofSeedForm.caneWeight.$dirty || submitted)" class="help-block">Cane Weight required</p>
												<p ng-show="SourceofSeedForm.caneWeight.$error.pattern && (SourceofSeedForm.caneWeight.$dirty || submitted)" class="help-block">Enter Valid Cane Weight</p>	  
												  
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-2">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.lorryWeight.$invalid && (SourceofSeedForm.lorryWeight.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="lorryWeight">Lorry Weight (tons)</label>
												  	<input type="text"    class="form-control" data-ng-required="changeStatus"  data-ng-pattern="/^[0-9.]+$/" placeholder="Lorry Weight(tons)"  name="lorryWeight" ng-model="AddSource.lorryWeight" id="lorryWeight" with-floating-label ng-keyup="getNetWeight(AddSource.caneWeight,AddSource.lorryWeight);getBm(AddSource.netweight,AddSource.bm);" ng-blur="getBm(AddSource.netWeight,AddSource.bm);decimalpoint('lorryWeight');" tabindex="5" ng-focus="updateEmpty(AddSource.lorryWeight,'lorryWeight')" />
													
												  </div>
												 <p ng-show="SourceofSeedForm.lorryWeight.$error.required && (SourceofSeedForm.lorryWeight.$dirty || submitted)" class="help-block">Lorry Weight required</p>
												  <p ng-show="SourceofSeedForm.lorryWeight.$error.pattern && (SourceofSeedForm.lorryWeight.$dirty || submitted)" class="help-block">Enter Valid Lorry Weight</p>	   
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-3">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.netWeight.$invalid && (SourceofSeedForm.netWeight.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="netweighta">Net Weight (tons)</label>
												  	<input type="text" class="form-control" data-ng-required="changeStatus" data-ng-pattern="/^[0-9.]+$/" placeholder="Net Weight(tons)"  tabindex="6" name="netWeight"  ng-model="AddSource.netWeight" id="netweighta" with-floating-label ng-blur="decimalpoint('lorryWeight');"  ng-focus="updateEmpty(AddSource.netWeight,'netWeight')"     />
													
												  </div>
												<p ng-show="SourceofSeedForm.netWeight.$error.required && (SourceofSeedForm.netWeight.$dirty || submitted)" class="help-block">Net Weight required</p>
												<p ng-show="SourceofSeedForm.netWeight.$error.pattern && (SourceofSeedForm.netWeight.$dirty || submitted)" class="help-block">Enter Valid Net Weight</p>	     
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-2">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
													<label for="bindingMaterial">Binding Material</label>
												  	<input type="text" class="form-control" placeholder="Binding Material (%)" readonly  tabindex="14" name="bindingMaterial" ng-model="AddSource.bm" id="bindingMaterial" with-floating-label maxlength="7"   />
													
												  </div>
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-2">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.finalWeight.$invalid && (SourceofSeedForm.finalWeight.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="finalweight">Final Weight(tons)</label>
												  	<input type="text"    class="form-control" data-ng-required="changeStatus" data-ng-pattern="/^[0-9.]+$/" placeholder="Final Weight(tons)"  tabindex="8" name="finalWeight" ng-model="AddSource.finalWeight" id="finalweight" with-floating-label ng-blur="decimalpoint('lorryWeight');"/ >
													
												  </div>
												  <p ng-show="SourceofSeedForm.finalWeight.$error.required && (SourceofSeedForm.finalWeight.$dirty || submitted)" class="help-block">Final Weight required</p>	     
												   <p ng-show="SourceofSeedForm.finalWeight.$error.pattern && (SourceofSeedForm.finalWeight.$dirty || submitted)" class="help-block">Enter Valid Final Weight</p>	     
			                		        </div>
			                    		</div>										
									</div>
								</div>
								<hr />
								<div class="row">
								<div class="col-sm-3">
				                    <div class="input-group">
            				           <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
										<h5 style="font-weight:lighter;"><u>Seed Cost Details :- </u></h5>
										
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-4">
												<div class="input-group">
													<div class="form-group" >
														<div class="fg-line nulvalidate">
										<br />
															 <label class="radio radio-inline m-r-20"><b>Option</b></label>	
															 <label class="radio radio-inline m-r-20">
																<input type="radio" name="Tonnage" value="0"   data-ng-model="AddSource.acreage" ng-click="updateSSValidation(AddSource.acreage,changeStatus);" >
																<i class="input-helper"></i>Acreage
															</label>
															<label class="radio radio-inline m-r-20">
															<input type="radio" name="Tonnage" value="1"   data-ng-model='AddSource.acreage' ng-click="updateSSValidation(AddSource.acreage,changeStatus2);">
															<i class="input-helper"></i>Tonnage
															</label>
										
														</div>
												</div>
										</div>			                    
									</div>
								  </div>
								 
								<div class="row">
								<!--<div class="col-sm-3">
				                    <div class="input-group">
            				           <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
										<h5 style="font-weight:lighter;"><u>Seed Cost Details :- </u></h5>
										
			                    	</div>			                    
				                  </div>-->
								  
								  
								  
									<div class="col-sm-6" ng-hide="AddSource.acreage=='0'">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.seedCostPerTon.$invalid && (SourceofSeedForm.seedCostPerTon.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="seedCostPerTon">Seed Cost Per Ton</label>
												  	<input type="text"    class="form-control"  data-ng-required="changeStatus" data-ng-pattern="/^[0-9.]+$/" placeholder="Seed Cost Per Ton"  tabindex="9" name="seedCostPerTon" ng-model="AddSource.seedCostPerTon" id="seedCostPerTon" with-floating-label / ng-keyup="updateTotalCost(AddSource.seedCostPerTon,AddSource.finalWeight);" ng-blur="decimalpointRupees('seedCostPerTon');">
													
												  </div>
												  
												   <p ng-show="SourceofSeedForm.seedCostPerTon.$error.required && (SourceofSeedForm.seedCostPerTon.$dirty || submitted)" class="help-block">Seed Cost Per Ton required</p>	    
												   <p ng-show="SourceofSeedForm.seedCostPerTon.$error.pattern && (SourceofSeedForm.seedCostPerTon.$dirty || submitted)" class="help-block">Enter Valid Seed Cost Per Ton</p>	     
			                		        </div>
			                    		</div>										
									</div>
									
									<div class="col-sm-3" ng-hide="AddSource.acreage=='1'">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.noofacre.$invalid && (SourceofSeedForm.noofacre.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="noofacre">No. of Acre</label>
												  	<input type="text"    class="form-control"  data-ng-pattern="/^[0-9.]+$/" placeholder="No. of Acre"  tabindex="10" name="noofacre" ng-model="AddSource.noofacre" id="noofacre" with-floating-label / maxlength="5" data-ng-required="changeStatus2" ng-keyup="updateTotalCostByAcreage(AddSource.noofacre,AddSource.costperacre,AddSource.acreage);">
													
												  </div>
												  
												   	    
												   <p ng-show="SourceofSeedForm.noofacre.$error.pattern && (SourceofSeedForm.noofacre.$dirty || submitted)" class="help-block">Enter Valid Acres</p>	     
												    <p ng-show="SourceofSeedForm.noofacre.$error.required && (SourceofSeedForm.noofacre.$dirty || submitted)" class="help-block">No. of Acre is required</p>	     
			                		        </div>
			                    		</div>										
									</div>
									
									<div class="col-sm-3" ng-hide="AddSource.acreage=='1'">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.costperacre.$invalid && (SourceofSeedForm.costperacre.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="costperacre">Cost Per Acre</label>
												  	<input type="text"    class="form-control"  data-ng-pattern="/^[0-9.]+$/" placeholder="Cost Per Acre"  tabindex="11" name="costperacre" ng-model="AddSource.costperacre" id="costperacre" with-floating-label / data-ng-required="changeStatus2" maxlength="5" ng-keyup="updateTotalCostByAcreage(AddSource.noofacre,AddSource.costperacre,AddSource.acreage);">
													
												  </div>
												  
												   
												   <p ng-show="SourceofSeedForm.costperacre.$error.pattern && (SourceofSeedForm.costperacre.$dirty || submitted)" class="help-block">Enter Valid Cost Per Acre</p>	     
												    <p ng-show="SourceofSeedForm.costperacre.$error.required && (SourceofSeedForm.costperacre.$dirty || submitted)" class="help-block">Enter Valid Cost Per Acre</p>	     
			                		        </div>
			                    		</div>										
									</div>
									
									
									<div class="col-sm-6">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.totalCost.$invalid && (SourceofSeedForm.totalCost.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  <label for="totalcost">Total Cost(Rs)</label>
    		        					          <input type="text" class="form-control" data-ng-required="true" placeholder="Total Cost(Rs)"  tabindex="12" name="totalCost" ng-model="AddSource.totalCost"   data-ng-pattern="/^[0-9.]+$/" id="totalcost" with-floating-label  readonly  />
					                	        </div>
<p ng-show="SourceofSeedForm.totalCost.$error.required && (SourceofSeedForm.totalCost.$dirty || submitted)" class="help-block">Total Cost Required.</p>												
<p ng-show="SourceofSeedForm.totalCost.$error.pattern && (SourceofSeedForm.totalCost.$dirty || submitted)" class="help-block">Enter Valid Total Cost</p>
												
</div>												
				                    	</div>										
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.ageOfCrop.$invalid && (SourceofSeedForm.ageOfCrop.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="grossweight">Age of Crop(months)</label>
												  	<input type="text"  data-ng-pattern="/^[0-9.\s]*$/" class="form-control" placeholder="Age of Crop(months)"   tabindex="13" name="ageOfCrop" ng-model="AddSource.ageOfCrop" id="grossweight" with-floating-label  / maxlength="5">
													
												  </div>
												  
											<p ng-show="SourceofSeedForm.ageOfCrop.$error.required && (SourceofSeedForm.ageOfCrop.$dirty || submitted)" class="help-block">Age of Crop Required.</p>		  
											<p ng-show="SourceofSeedForm.ageOfCrop.$error.pattern && (SourceofSeedForm.ageOfCrop.$dirty || submitted)" class="help-block">Enter Valid Age of Crop.</p>		  
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.avgBudsPerCane.$invalid && (SourceofSeedForm.avgBudsPerCane.$dirty || submitted)}">
					        	                <div class="fg-line">
													<label for="avgBudsPerCane">Avg. Buds Per Cane</label>
												  	<input type="text" class="form-control" data-ng-pattern="/^[0-9.\s]*$/"  placeholder="Avg. Buds Per Cane"  tabindex="14" name="avgBudsPerCane" ng-model="AddSource.avgBudsPerCane" id="avgBudsPerCane" with-floating-label maxlength="5">
													
												  </div>
												 <p ng-show="SourceofSeedForm.avgBudsPerCane.$error.required && (SourceofSeedForm.avgBudsPerCane.$dirty || submitted)" class="help-block">AverageBudsperCane Required</p>
												 <p ng-show="SourceofSeedForm.avgBudsPerCane.$error.pattern && (SourceofSeedForm.avgBudsPerCane.$dirty || submitted)" class="help-block">Enter Valid AverageBudsperCane</p>		   
			                		        </div>
			                    		</div>										
									</div>
									<div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.physicalCondition.$invalid && (SourceofSeedForm.physicalCondition.$dirty || submitted)}">
					        	                <div class="fg-line">
												 <select chosen class="w-100" name="physicalCondition" ng-model="AddSource.physicalCondition" ng-options="physicalCondition.id as physicalCondition.pc for physicalCondition in physicalConditionNames" tabindex="15">
												<option value="">Physical Condition</option>
												</select>
					                	        </div>
											 <p ng-show="SourceofSeedForm.physicalCondition.$error.required && (SourceofSeedForm.physicalCondition.$dirty || submitted)" class="help-block">Physical Condtion Required.</p>	
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.seedCertifiedBy.$invalid && (SourceofSeedForm.seedCertifiedBy.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  <input type="text"    placeholder="Seed Certified by"  name="seedCertifiedBy" class="form-control" ng-model="AddSource.seedCertifiedBy" tabindex="16" maxlength="20"  />
					                	        </div>
<p ng-show="SourceofSeedForm.seedCertifiedBy.$error.required && (SourceofSeedForm.seedCertifiedBy.$dirty || submitted)" class="help-block">Seed Certified is Required.</p>	
	
</div>												
				                    	</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SourceofSeedForm.certDate.$invalid && (SourceofSeedForm.certDate.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  <input type="text"  name="certDate"   placeholder="Certified Date" class="form-control date" ng-model="AddSource.certDate" readonly  tabindex="17" />
					                	        </div>
<p ng-show="SourceofSeedForm.certDate.$error.required && (SourceofSeedForm.certDate.$dirty || submitted)" class="help-block">Cettified Date is Required.</p>	
<p ng-show="SourceofSeedForm.certDate.$error.pattern && (SourceofSeedForm.certDate.$dirty || submitted)" class="help-block">Enter Valid Cettified Date</p>	
</div>												
				                    	</div>
									</div>
									<div class="co-sm-4">
									<input type="hidden" data-ng-model='AddSource.fileLocation' name="fileLocation" />
									
									<div class="form-group">																												   
									 <div class="fileinput fileinput-new" data-provides="fileinput">
						                <div class="fileinput-preview thumbnail" data-trigger="fileinput">	
										<!--<img src="img/{AddSource.fileLocation}}" id="blah" />-->
											<img id="blah" src="" alt=""   ng-if="AddSource.fileLocation!='0'"   />
										</div>
						               <div>
                					    <span class="btn btn-info btn-file">
		                        			<span class="fileinput-new" id="freshryotimage"><i class="glyphicon glyphicon-picture"></i>ATTACH FILE</span>
			        		                <span class="fileinput-new" id="ryotchangeimage" style="display:none;">UPLOAD FILE</span>
    			            		    	  <input type='file' id="imgInp"  name="file" onchange="angular.element(this).scope().uploadUserFile(this.files)"  multipart ng-model="AddSource.fileLocation"  tabindex="18"/>
        
												
					                	</span>
        					           
            	    				</div>
<p class="help-block" style="color:#FF0000; display:none;" id="image">UPLOAD FILE</p>
</div>												
									
					            	</div>
									</div>
						</div>
								<div class="row" style="margin-top:-90px;">
									<div class="col-sm-4">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.harvestingCharges.$invalid && (SourceofSeedForm.harvestingCharges.$dirty || submitted)}">																																																																																																												
					        	                <div class="fg-line">
													<label for="Harvestingcharges">Harvesting Charges</label>
													<input type="text"    class="form-control"  placeholder="Harvesting Charges(Rs)" maxlength="5" tabindex="19" name="harvestingCharges" ng-model="AddSource.harvestingCharges"   data-ng-pattern="/^[0-9.\s]*$/"  id="Harvestingcharges"   with-floating-label />
			    		            	        </div>
<p ng-show="SourceofSeedForm.harvestingCharges.$error.required && (SourceofSeedForm.harvestingCharges.$dirty || submitted)" class="help-block">Harvesting Charges Required</p>
<p ng-show="SourceofSeedForm.harvestingCharges.$error.pattern && (SourceofSeedForm.harvestingCharges.$dirty || submitted)" class="help-block">Enter Valid Harvesting Charges</p>
								
												</div>												
											</div>
			    		               	</div>
										
									</div>
									<div class="col-sm-4">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="form-group" ng-class="{ 'has-error' : SourceofSeedForm.transCharges.$invalid && (SourceofSeedForm.transCharges.$dirty || submitted)}">																																																																																																												
					        	                <div class="fg-line">
													<label for="transportGroses">Transport Charges</label>
													<input type="text" class="form-control"  placeholder="Transport Charges(Rs)" maxlength="5" tabindex="20" name="transCharges" ng-model="AddSource.transCharges"     data-ng-pattern="/^[0-9.\s]*$/"  id="transportGroses"   with-floating-label   />
			    		            	        </div>
<p ng-show="SourceofSeedForm.transCharges.$error.required && (SourceofSeedForm.transCharges.$dirty || submitted)" class="help-block">Transport Charges Required</p>
<p ng-show="SourceofSeedForm.transCharges.$error.pattern && (SourceofSeedForm.transCharges.$dirty || submitted)" class="help-block">Enter Valid Transport Charges</p>																																											
												</div>												
											</div>
			    		               	</div>
										
									</div>
								</div>
							<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SourceofSeedForm)">Reset</button>
								</div>
							</div>						 	
					   </div>
						
					    
					</form>	
					
					
					<form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">						  
						  	  						
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th>Date</th>	
                		    					<th>Final Weight</th>
												<th>Total Cost</th>
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="weighment in weightmentData" >
												<td>{{weighment.Date}}</td><td>{{weighment.finalweight}}</td><td>{{weighment.totalcost}}</td>
																								
											</tr>
											
											<tr>
											<td colspan="2"></td><td><input type="text" style="width:150px; height:40px; text-align:left; border: 0;
  outline: 0;background: transparent;border-bottom: 1px solid black;" ng-model="GridTotal"  name="GridTotal" readonly /></td>
											</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>				  							 		 							 
					<!------------------form end------------------------------>							 
					<!----------Popup box on surveynumber for plant----------->
						
					 
					<!-----------Popup box on croptype dropdown on change----->
					 	
					<!-------from-end--------------------->		 						           			
				        </div>
					  
					  </form>
					  		   
						
					
					<!----------table grid design--------->
					
					<!----------end----------------------->										
					
				</div> 
	    </section>   
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
