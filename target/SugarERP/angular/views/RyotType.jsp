	<script type="text/javascript">	
		$('#autofocus').focus();	
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="tableCtrl as tctrl">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Ryot Type Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 		<div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Ryot ID" readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Ryot Type" id="autofocus" maxlength="25"/>
			                	        </div>
			                    	</div>			                    
				                  </div>								  
    	        			 </div><br />
						     <div class="row">
							 	<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Description" id="input_description" maxlength="50"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-6">
				                    <div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="Active" checked="checked">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="Inactive">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>													       							
    	        			 </div>							 
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/RyotType" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/RyotType" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Ryot Types</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Ryot Type'">
                		    					<span ng-if="!w.$edit">{{w.did}}</span>
					    		                <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.did" maxlength="25" placeholder='Ryot Type'/></div>
					            		      </td>
		                    				  <td data-title="'Description'">
							                     <span ng-if="!w.$edit">{{w.ddesc}}</span>
							                     <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.ddesc" maxlength="50" placeholder='Description'/></div>
							                  </td>
											  <td data-title="'Status'">
							                      <span ng-if="!w.$edit">{{w.dname}}</span>
        		            					  <div ng-if="w.$edit">	
			            					          <label class="radio radio-inline m-r-20">
								            				<input type="radio" name="inlineRadioOptionsgrid" value="Active" checked="checked">
			    			            					<i class="input-helper"></i>Active
													  </label>
				 			              			  <label class="radio radio-inline m-r-20">
															<input type="radio" name="inlineRadioOptionsgrid" value="Inactive">
			    				                            <i class="input-helper"></i>Inactive
					  		                          </label>
											      </div>
										      </td>
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
