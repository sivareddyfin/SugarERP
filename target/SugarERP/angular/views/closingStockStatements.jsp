<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="SeedlingEstimateController">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Closing Stock Statements</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="SeedlingEstimate">
					 		<div class="row">
										
										  
										  
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true'>
															<option value="">Select Store Names</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true'>
															<option value="">Select Item Category</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true'>
															<option value="">Order By</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
										<button type="button" class="btn btn-primary btn-sm m-t-10">Download Report</button>
										</div>
																				
							</div>
							
							
							
							
							
					</form>		
							
							<br />
											 		
						 
							
							<!----------table grid design--------->
					
											
					        
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
