	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="DispatchBhuShaktiController" ng-init="loadRyotCodeDropDown();getAllDepartments();loadFieldOfficerDropdown();loadseasonNames();loadZoneNames();addGrid();" >    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Dispatch BhuShakti</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="DispatchBhuShaktiForm" novalidate >
						 <div class="row">
							<div class="col-sm-4">
							  <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : DispatchBhuShaktiForm.season.$invalid && (DispatchBhuShaktiForm.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedBhuShakti.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="loadIndents(AddedBhuShakti.season,AddedBhuShakti.ryotCode);">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="DispatchBhuShaktiForm.season.$error.required && (DispatchBhuShaktiForm.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
									</div>			
							  </div>			
							  <div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 <div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.department.$invalid && (DispatchBhuShaktiForm.department.$dirty || submitted)}">
													<div class="fg-line">
														 <select chosen class="w-100" name="department" tabindex="3"   data-ng-model="AddedBhuShakti.department" ng-options="department.deptCode as department.department for department  in DepartmentsData" ng-required='true'>
															<option value=''>Department</option>
														 </select>	
													</div>
													<p ng-show="DispatchBhuShaktiForm.department.$error.required && (DispatchBhuShaktiForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 
													
												 </div>
											</div>
										</div>
									</div>
							  <div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 <div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.fieldOfficer.$invalid && (DispatchBhuShaktiForm.fieldOfficer.$dirty || submitted)}">
													<div class="fg-line">
														 <select chosen class="w-100" name="department" tabindex="3"   data-ng-model="AddedBhuShakti.fieldOfficer" ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in FieldOffNames  | orderBy:'-fieldOfficer':true"  ng-required='true'>
															<option value=''>Field Officer</option>
														 </select>	
													</div>
													<p ng-show="DispatchBhuShaktiForm.fieldOfficer.$error.required && (DispatchBhuShaktiForm.fieldOfficer.$dirty || Addsubmitted)" class="help-block">Select Field Officer</p>		 
													
												 </div>
											</div>
										</div>
									</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
												<div class="input-group">
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="dispatchNo">Dispatch No.</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="dispatchNo" data-ng-model="AddedBhuShakti.dispatchNo" placeholder="Dispatch No." tabindex="1"  id="dispatchNo" with-floating-label readonly  />
													</div>
													
													
															  </div>
														  </div>			                    
										</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
												<div class="input-group">
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DispatchBhuShaktiForm.ryotCode.$invalid && (DispatchBhuShaktiForm.ryotCode.$dirty || submitted)}">
													<div class="fg-line">
													<select chosen class="w-100"  tabindex="6" data-ng-required='true' data-ng-model='AddedBhuShakti.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData" ng-change="updateRyotNamebyCode(AddedBhuShakti.ryotCode);loadIndents(AddedBhuShakti.season,AddedBhuShakti.ryotCode);">
																			<option value=""> Ryot Code</option>
																		</select>
													</div>
													
													<p ng-show="DispatchBhuShaktiForm.ryotCode.$error.required && (DispatchBhuShaktiForm.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Stock Date</p>
															  </div>
														  </div>			                    
										</div>
								</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 <div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.selectedIndent.$invalid && (DispatchBhuShaktiForm.selectedIndent.$dirty || submitted)}">
													<div class="fg-line">
														 <select chosen class="w-100" name="selectedIndent" tabindex="3"   data-ng-model="AddedBhuShakti.selectedIndent" ng-options="selectedIndent.IndentNo as selectedIndent.IndentNo for selectedIndent  in IndentData" ng-required='true'>
															<option value=''>Indent</option>
														 </select>	
													</div>
													<p ng-show="DispatchBhuShaktiForm.selectedIndent.$error.required && (DispatchBhuShaktiForm.selectedIndent.$dirty || Addsubmitted)" class="help-block">Select Indent</p>		 
													
												 </div>
											</div>
										</div>
									</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
									<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DispatchBhuShaktiForm.date.$invalid && (DispatchBhuShaktiForm.date.$dirty || submitted)}">											
										<div class="fg-line">
										<label for="date">Date</label>
											<input type="text" class="form-control date "  name="date" data-ng-model="AddedBhuShakti.date" placeholder="Date" tabindex="2"  maxlength="15" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/" id="date" with-floating-label />
										</div>
										
										<p ng-show="DispatchBhuShaktiForm.date.$error.required && (DispatchBhuShaktiForm.date.$dirty || Addsubmitted)" class="help-block">Select Date</p>
										<p ng-show="DispatchBhuShaktiForm.date.$error.pattern && (DispatchBhuShaktiForm.date.$dirty || Addsubmitted)" class="help-block">Enter Valid Date</p>
											 
									</div>
									</div>			                    
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DispatchBhuShaktiForm.ryotName.$invalid && (DispatchBhuShaktiForm.ryotName.$dirty || submitted)}">											
										<div class="fg-line">
										<label for="ryotName">Ryot Name</label>
											<input type="text" class="form-control"  name="ryotName" data-ng-model="AddedBhuShakti.ryotName" placeholder="Ryot Name" tabindex="2"  maxlength="50"  id="ryotName" with-floating-label />
										</div>
										
										<p ng-show="DispatchBhuShaktiForm.ryotName.$error.required && (DispatchBhuShaktiForm.ryotName.$dirty || Addsubmitted)" class="help-block">Enter Ryot Name</p>
										
											 
									</div>
									</div>			                    
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
												<div class="input-group">
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DispatchBhuShaktiForm.zone.$invalid && (DispatchBhuShaktiForm.zone.$dirty || submitted)}">
													<div class="fg-line">
													<select chosen class="w-100"  tabindex="6" data-ng-required='true' data-ng-model='AddedBhuShakti.zone' name="zone" ng-options="zone.id as zone.zone for zone in ZoneData">
																			<option value="">Zone</option>
																		</select>
													</div>
													
													<p ng-show="DispatchBhuShaktiForm.zone.$error.required && (DispatchBhuShaktiForm.zone.$dirty || Addsubmitted)" class="help-block">Select Zone</p>
															  </div>
														  </div>			                    
										</div>
								</div>	
							</div>
						 </div>
						 <br />
							<div class='table-responsive'>
							<table style="width:100%;"  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>Indent No</th>
											<th>Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Open</th>
											<th>&nbsp;&nbsp; Batch ID</th>
											<th>Batch</th>
											<th>Exp Date.</th>
											<th>Price</th>
											<th>Indent Qty</th>
											<th>Dispatch Qty</th>
											<th>Total Cost</th>
											
											
										
											
            							</tr>
										<tr class="styletr" style="height:70px;" ng-repeat='stockinData in data'>
										<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addGrid();' ng-if="stockinData.id == '1'" style="height:45px;"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='removeRow(stockinData.id);' ng-if="stockinData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
												</td>
											<td>
											<input type='hidden'  name="batchFlag{{$index}}" ng-model="stockinData.batchFlag" >
											<input type='hidden'  name="batchSeqNo{{$index}}" ng-model="stockinData.batchSeqNo" >
											<input type='hidden'  name="batchId{{$index}}" ng-model="stockinData.batchId" >

											
											
											
											
											<br />
											<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.productCode{{$index}}.$invalid && (DispatchBhuShaktiForm.productCode{{$index}}.$dirty || submitted)}">
											
											<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="stockinData.productCode" ng-change="loadProductCode(stockinData.productCode,$index);getproductIdforPopup(stockinData.productCode,$index);" style="height:40px;">
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
       						    </select>    
   									</datalist>
											
											
											
											
											<p ng-show="DispatchBhuShaktiForm.productCode{{$index}}.$error.required && (DispatchBhuShaktiForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											<p ng-show="DispatchBhuShaktiForm.productCode{{$index}}.$error.pattern && (DispatchBhuShaktiForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.productName{{$index}}.$invalid && (DispatchBhuShaktiForm.productName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="productName{{$index}}"  ng-model="stockinData.productName" data-ng-required='true' placeholder="Product Name"  style="height:40px;" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" maxlength="30" readonly />
												<p ng-show="DispatchBhuShaktiForm.productName{{$index}}.$error.required && (DispatchBhuShaktiForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="DispatchBhuShaktiForm.productName{{$index}}.$error.pattern && (DispatchBhuShaktiForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.uom{{$index}}.$invalid && (DispatchBhuShaktiForm.uom{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uom{{$index}}"  ng-model="stockinData.uom" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="DispatchBhuShaktiForm.uom{{$index}}.$error.required && (DispatchBhuShaktiForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="DispatchBhuShaktiForm.uom{{$index}}.$error.pattern && (DispatchBhuShaktiForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												<td> <button type="button" class="btn btn-default btn-sm" style="height:38px;" ng-click="getbatchPopup(stockinData.productCode,$index);">
          <span class="glyphicon glyphicon-open"></span> Open
        </button></td>
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.batchId{{$index}}.$invalid && (DispatchBhuShaktiForm.batchId{{$index}}.$dirty || submitted)}">
											<input type="text" class="form-control1" name="batchId{{$index}}"   ng-model="stockinData.batchId" placeholder="Batch ID" ng-required='true' style="height:40px;" maxlength="10" ng-blur="duplicateProductCode(stockinData.batch,$index);" readonly />
											<p ng-show="DispatchBhuShaktiForm.batchId{{$index}}.$error.required && (DispatchBhuShaktiForm.batchId{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.batch{{$index}}.$invalid && (DispatchBhuShaktiForm.batch{{$index}}.$dirty || submitted)}">
											<input type="text" class="form-control1" name="batch{{$index}}"   ng-model="stockinData.batch" placeholder="Batch" ng-required='true' style="height:40px;" maxlength="10" ng-blur="duplicateProductCode(stockinData.batch,$index);" readonly />
											<p ng-show="DispatchBhuShaktiForm.batch{{$index}}.$error.required && (DispatchBhuShaktiForm.batch{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.expDate{{$index}}.$invalid && (DispatchBhuShaktiForm.expDate{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1 datepicker" name="expDate{{$index}}"  ng-model="stockinData.expDt"  placeholder="Exp Date" style="height:40px;" maxlength="10"  data-ng-required='true' data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$" readonly />
											
											
											
											<p ng-show="DispatchBhuShaktiForm.expDate{{$index}}.$error.required && (DispatchBhuShaktiForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="DispatchBhuShaktiForm.expDate{{$index}}.$error.pattern && (DispatchBhuShaktiForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
												</div></td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.indentQty{{$index}}.$invalid && (DispatchBhuShaktiForm.indentQty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="indentQty{{$index}}"  ng-model="stockinData.indentQty"  placeholder="Qty" ng-required='true'  style="height:40px;" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" maxlength="10"  />
											<p ng-show="DispatchBhuShaktiForm.indentQty{{$index}}.$error.required && (DispatchBhuShaktiForm.indentQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="DispatchBhuShaktiForm.indentQty{{$index}}.$error.pattern && (DispatchBhuShaktiForm.indentQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.dispatchQty{{$index}}.$invalid && (DispatchBhuShaktiForm.dispatchQty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="dispatchQty{{$index}}"  ng-model="stockinData.dispatchQty"  placeholder="Qty" ng-required='true'  style="height:40px;" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" maxlength="10"  />
											<p ng-show="DispatchBhuShaktiForm.dispatchQty{{$index}}.$error.required && (DispatchBhuShaktiForm.dispatchQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="DispatchBhuShaktiForm.dispatchQty{{$index}}.$error.pattern && (DispatchBhuShaktiForm.dispatchQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : DispatchBhuShaktiForm.mrp{{$index}}.$invalid && (DispatchBhuShaktiForm.totalCost{{$index}}.$dirty || submitted)}">		<input type="text" class="form-control1" id name="totalCost{{$index}}"  ng-model="stockinData.totalCost"  placeholder="Total Cost" ng-required='true'  style="height:40px;" maxlength="12"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="totalCost{{$index}}" /><p ng-show="DispatchBhuShaktiForm.totalCost{{$index}}.$error.required && (DispatchBhuShaktiForm.totalCost{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>	<p ng-show="DispatchBhuShaktiForm.totalCost{{$index}}.$error.pattern && (DispatchBhuShaktiForm.totalCost{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											
										</tr>
										
										
														
									</thead>
							</table>
							
							</div>
							
						 <br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(DispatchBhuShaktiForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
