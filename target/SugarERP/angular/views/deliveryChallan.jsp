<style>
.styletr {border-bottom: 1px solid grey;}


</style>
<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="deliveryChallanController" ng-init="addFormField();getAllDepartments();onloadFunction();getdiscType();">      
        	<div class="container">	
						
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>DELIVERY CHALLAN </b></h2></div>
				 
																	
			     <div  class = "card">
			        <div class = "card-body card-padding" >
					      
					<!--------Form Start----->				
					 <!-------body start------>						 	
					 	<form name="deliveryChallanForm"  novalidate ng-submit="deliveryChallanSubmit(deliveryChallan,deliveryChallanForm);">
							<div class='row'>
							<div class="col-sm-6">
								 <div class="row">
									<div class="col-sm-12">
												<div class="input-group">
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																<div class="form-group "  ng-class="{ 'has-error' : deliveryChallanForm.department.$invalid && (deliveryChallanForm.department.$dirty || submitted)}">
													<div class="fg-line">
														<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='deliveryChallan.department' name="department" ng-change="getMaxDcNo(deliveryChallan.department);" ng-options="department.deptCode as department.department for department in DepartmentsData">
																			<option value="">Department</option>
																		</select>
													</div>
													<p ng-show="deliveryChallanForm.department.$error.required && (deliveryChallanForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>	 
															  </div>
														  </div>			                    
										</div>
								</div>	
								</div>
							<div class="col-sm-6">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
												
													<input type="text" id='dcnumber' class="form-control autofocus" placeholder='DC NO' id="dcnumber"   name="dcnumber" ng-model="deliveryChallan.dcnumber"   readonly="readonly" tabindex="1" maxlength='50'  ng-required="true"  />
																		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>	
							</div>
							
							
							<div class="row">
								<div class="col-sm-6">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : deliveryChallanForm.dcdate.$invalid && (deliveryChallanForm.dcdate.$dirty || submitted)}">
												<div class="fg-line">
												
													<input type="text" class="form-control date" placeholder='DC Date' id="dcdate"   name="dcdate" ng-model="deliveryChallan.dcdate"    tabindex="2"  ng-required="true" maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="dcdate" with-floating-label data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  / >
																		
												</div>
												<p ng-show="deliveryChallanForm.dcdate.$error.required && (deliveryChallanForm.dcdate.$dirty || Addsubmitted)" class="help-block">Select Stock Date</p>
													<p ng-show="deliveryChallanForm.dcdate.$error.pattern && (deliveryChallanForm.dcdate.$dirty || Addsubmitted)" class="help-block">Valid Stock Date is required</p>
											</div>
										</div>
									</div>
								</div>
								
								
								</div>
								
								</div>
								<div class="col-sm-6">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{'has-error': deliveryChallanForm.customername.$invalid && deliveryChallanForm.customername.$dirty || submitted}">
												<div class="fg-line">
												
													<input type="text" class="form-control" placeholder='Customer Name' id="customername"   name="customername" ng-model="deliveryChallan.customername"    tabindex="3"  ng-pattern="/^[a-zA-Z0-9?=.*!@#$%^&*_\-\s]+$/"  ng-required="true" maxlength="25"  />
																		
												</div>
												<p ng-show ="deliveryChallanForm.customername.$error.required && (deliveryChallanForm.customername.$dirty||Addsubmitted) " class="help-block">     Customer Name Required</p>
												<p ng-show ="deliveryChallanForm.customername.$error.pattern && (deliveryChallanForm.customername.$dirty||Addsubmitted) " class="help-block">    Only Names   </p>
											</div>
										</div>
									</div>
								</div>
								
								
								</div>
								
							
								</div>
							<div class='row'>
								<div class="col-sm-6">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{'has-error': deliveryChallanForm.address.$invalid && deliveryChallanForm.address.$dirty || submitted}">
												<div class="fg-line">
												
													<input type="text" class="form-control" placeholder='Address' id="address"   name="address" ng-model="deliveryChallan.address"    tabindex="4"   ng-required="true" ng-pattern="/^[a-zA-Z0-9?=.*!@#$%^&|+*<>_\-\s]+$/" maxlength="50"  />
																		
												</div>
												<p ng-show ="deliveryChallanForm.address.$error.required && (deliveryChallanForm.address.$dirty||Addsubmitted) " class="help-block">          Address Required</p>
												<p ng-show ="deliveryChallanForm.address.$error.pattern && (deliveryChallanForm.address.$dirty||Addsubmitted) " class="help-block">        Only  Address</p>
											</div>
										</div>
									</div>
								</div>
								
								
								</div>
								<div class="col-sm-6">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{'has-error': deliveryChallanForm.contactnumber.$invalid && deliveryChallanForm.contactnumber.$dirty || submitted}">
												<div class="fg-line">
												
													<input type="text" class="form-control " placeholder='Contact No'   name="contactnumber" ng-model="deliveryChallan.contactnumber"    tabindex="5"  ng-pattern="/^[0-9]+$/" ng-required="true" maxlength="15"  />
																		
												</div>
												<p ng-show="deliveryChallanForm.contactnumber.$error.required && (deliveryChallanForm.contactnumber.$dirty || Addsubmitted)" class="help-block"> Contact Number Required</p>
												<p ng-show="deliveryChallanForm.contactnumber.$error.pattern &&  (deliveryChallanForm.contactnumber.$dirty || Addsubmitted)" class="help-block">Only Numbers</p>
											</div>
										</div>
									</div>
								</div>
								
								<input type='hidden'  name="dcSeqNo" ng-model="deliveryChallan.dcSeqNo" >
								<input type='hidden'  name="modifyFlag" ng-model="deliveryChallan.modifyFlag" >
								<input type='hidden'  name="saleInvoiceNo" ng-model="deliveryChallan.saleInvoiceNo" >

																			
								</div>
							</div>
							
							
							<br />
							
							<div class="col-sm-6">
								<div class='row'>
							<label class="radio radio-inline m-r-20"><b>Discount For</b></label>	
									<label class="radio radio-inline m-r-20">
										<input type="radio" name="discType" value="0" checked="checked"  data-ng-model="deliveryChallan.discType" >
										<i class="input-helper"></i>Items
									 </label>
									 <label class="radio radio-inline m-r-20">
										 <input type="radio" name="discType" value="1"   data-ng-model='deliveryChallan.discType'>
										<i class="input-helper"></i>Invoice
									 </label>													
								</div>
							</div>
							
							<br />
							<br />
							<div class="table-responsive">
							<table style="width:100%;"  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>&nbsp;&nbsp;&nbsp;Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Open Batch</th>
											<th>&nbsp;&nbsp;&nbsp; Batch</th>
											<th>Exp Date.</th>
											<th>MRP</th>
											<th>StockIn Date</th>
											<th>Quantity</th>
											<th>Total Cost</th>
											
											<th>Disc. Amount</th>
											<th>Net Amount</th>
											
            							</tr>
										<tr class="styletr" style="height:70px;" ng-repeat='stockinData in data'>
										<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="stockinData.id == '1'" style="height:45px;"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='removeRow(stockinData.id);' ng-if="stockinData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
												</td>
											<td>
											<input type='hidden'  name="batchFlag{{$index}}" ng-model="stockinData.batchFlag" >
											<input type='hidden'  name="batchSeqNo{{$index}}" ng-model="stockinData.batchSeqNo" >
											<input type='hidden'  name="batchId{{$index}}" ng-model="stockinData.batchId" >
											<input type='hidden'  name="cp{{$index}}" ng-model="stockinData.cp" >

																					
											
											
											<br />
											<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.productCode{{$index}}.$invalid && (deliveryChallanForm.productCode{{$index}}.$dirty || submitted)}">
											
											<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="stockinData.productCode" ng-change="loadProductCode(stockinData.productCode,$index);getproductIdforPopup(stockinData.productCode,$index);" style="height:40px;">
    								
											<datalist id="stateList">
												<select class="form-control1">
												<option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
												</select>    
											</datalist>
											
											
											
											
											<p ng-show="deliveryChallanForm.productCode{{$index}}.$error.required && (deliveryChallanForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											<p ng-show="deliveryChallanForm.productCode{{$index}}.$error.pattern && (deliveryChallanForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.productName{{$index}}.$invalid && (deliveryChallanForm.productName{{$index}}.$dirty || submitted)}">
												<input type="text" readonly class="form-control1" name="productName{{$index}}"  ng-model="stockinData.productName" data-ng-required='true' placeholder="Product Name"  style="height:40px;" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" maxlength="30" readonly />
												<p ng-show="deliveryChallanForm.productName{{$index}}.$error.required && (deliveryChallanForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="deliveryChallanForm.productName{{$index}}.$error.pattern && (deliveryChallanForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											
											<td style="display:none;">
											<br />
											
											<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.uom{{$index}}.$invalid && (deliveryChallanForm.uom{{$index}}.$dirty || submitted)}">
												<input type="text" readonly class="form-control1" name="uom{{$index}}"  ng-model="stockinData.uom" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="deliveryChallanForm.uom{{$index}}.$error.required && (deliveryChallanForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="deliveryChallanForm.uom{{$index}}.$error.pattern && (deliveryChallanForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												
												<td>
											<br />
											
											<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.uomName{{$index}}.$invalid && (deliveryChallanForm.uomName{{$index}}.$dirty || submitted)}">
												<input type="text" readonly class="form-control1" name="uomName{{$index}}"  ng-model="stockinData.uomName" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="deliveryChallanForm.uomName{{$index}}.$error.required && (deliveryChallanForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="deliveryChallanForm.uomName{{$index}}.$error.pattern && (deliveryChallanForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												
												
												
												
												
												<td> <button type="button" class="btn btn-default btn-sm" style="height:38px;" ng-click="getbatchPopup(stockinData.productCode,$index,deliveryChallan.department);">
          <span class="glyphicon glyphicon-open"></span> Open
        </button></td>
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.batch{{$index}}.$invalid && (deliveryChallanForm.batch{{$index}}.$dirty || submitted)}">
											<input type="text" readonly class="form-control1" name="batch{{$index}}"   ng-model="stockinData.batch" placeholder="Batch" ng-required='true' style="height:40px;" maxlength="10" ng-blur="duplicateProductCode(stockinData.batch,$index);" readonly />
											<p ng-show="deliveryChallanForm.batch{{$index}}.$error.required && (deliveryChallanForm.batch{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.expDate{{$index}}.$invalid && (deliveryChallanForm.expDate{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1 datepicker" name="expDate{{$index}}"  ng-model="stockinData.expDt"  placeholder="Exp Date" style="height:40px;" maxlength="10"  data-ng-required='true' data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$" readonly />
											
											
											
											<p ng-show="deliveryChallanForm.expDate{{$index}}.$error.required && (deliveryChallanForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="deliveryChallanForm.expDate{{$index}}.$error.pattern && (deliveryChallanForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
												</div></td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.mrp{{$index}}.$invalid && (deliveryChallanForm.mrp{{$index}}.$dirty || submitted)}">		
											<input type="text" readonly class="form-control1" id name="mrp{{$index}}"  ng-model="stockinData.mrp"  placeholder="Mrp" ng-required='true'  style="height:40px;" maxlength="12"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="getMRPid{{$index}}" /><p ng-show="deliveryChallanForm.mrp{{$index}}.$error.required && (deliveryChallanForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>	<p ng-show="deliveryChallanForm.mrp{{$index}}.$error.pattern && (deliveryChallanForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.stockInDate{{$index}}.$invalid && (deliveryChallanForm.stockInDate{{$index}}.$dirty || submitted)}">		
											<input type="text" readonly class="form-control1" id name="stockInDate{{$index}}"  ng-model="stockinData.stockInDate"  placeholder="StockIn Date" ng-required='true'  style="height:40px;" maxlength="12"  id="getMRPid{{$index}}" /><p ng-show="deliveryChallanForm.stockInDate{{$index}}.$error.required && (deliveryChallanForm.stockInDate{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>	<p ng-show="deliveryChallanForm.stockInDate{{$index}}.$error.pattern && (deliveryChallanForm.stockInDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.qty{{$index}}.$invalid && (deliveryChallanForm.qty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="qty{{$index}}"  ng-model="stockinData.quantity" ng-keyup="updateTotal(stockinData.quantity,stockinData.mrp,stockinData.discountAmount,$index);"  placeholder="Qty" ng-required='true'  style="height:40px;" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" maxlength="10"  />
											<p ng-show="deliveryChallanForm.qty{{$index}}.$error.required && (deliveryChallanForm.qty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="deliveryChallanForm.qty{{$index}}.$error.pattern && (deliveryChallanForm.qty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											
											
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.totalCost{{$index}}.$invalid && (deliveryChallanForm.totalCost{{$index}}.$dirty || submitted)}">		
											<input type="text" readonly class="form-control1" name="totalCost{{$index}}"  ng-model="stockinData.totalCost" placeholder="Total Cost" style="height:40px;" maxlength="12"   ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" /><p ng-show="deliveryChallanForm.totalCost{{$index}}.$error.required && (deliveryChallanForm.totalCost{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="deliveryChallanForm.totalCost{{$index}}.$error.pattern && (deliveryChallanForm.totalCost{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.discountAmount{{$index}}.$invalid && (deliveryChallanForm.discountAmount{{$index}}.$dirty || submitted)}">		
											<input type="text" class="form-control1" name="discountAmount{{$index}}"  ng-model="stockinData.discountAmount" placeholder="Discount" style="height:40px;" maxlength="12" ng-keyup="updateNetAmount(stockinData.quantity,stockinData.mrp,stockinData.discountAmount,$index);"   ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" /><p ng-show="deliveryChallanForm.discountAmount{{$index}}.$error.required && (deliveryChallanForm.discountAmount{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="deliveryChallanForm.discountAmount{{$index}}.$error.pattern && (deliveryChallanForm.discountAmount{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : deliveryChallanForm.netAmount{{$index}}.$invalid && (deliveryChallanForm.netAmount{{$index}}.$dirty || submitted)}">		
											<input type="text" readonly class="form-control1" name="netAmount{{$index}}"  ng-model="stockinData.netAmount" placeholder="Net Amount" style="height:40px;" maxlength="12" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" /><p ng-show="deliveryChallanForm.netAmount{{$index}}.$error.required && (deliveryChallanForm.netAmount{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="deliveryChallanForm.netAmount{{$index}}.$error.pattern && (deliveryChallanForm.netAmount{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
											
											
											
										</tr>
										
										
														
									</thead>
							</table>
							</div> 
							<br />
							
							<div class="row" align="left">
							<div class="col-sm-3">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
													<label for="totalCost">Total Cost</label>
													<input type="text" readonly id='totalCost' class="form-control autofocus" placeholder='Total Cost' id="totalCost"   name="totalCost" ng-model="deliveryChallan.totalCost"   readonly="readonly" tabindex="1" maxlength='50'  ng-required="true" id="totalCost" with-floating-label  />
																		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							
							
							<div class="col-sm-3">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
												<label for="discountPercent">Discount Percent</label>
													<input type="text" readonly id='discountPercent' class="form-control autofocus" placeholder='Disc. Percentage' id="discountPercent"   name="discountPercent" ng-model="deliveryChallan.discountPercent"   readonly="readonly" tabindex="1" maxlength='50'  ng-required="true" with-floating-label />
																		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-sm-3">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
												<label for="discountAmount">Discount Amount</label>
													<input type="text" readonly id='discountAmount' class="form-control autofocus" placeholder='Disc. Amount' id="discountAmount"   name="discountAmount" ng-model="deliveryChallan.discountAmount"   readonly="readonly" tabindex="1" maxlength='50'  ng-required="true" with-floating-label />
																		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-sm-3">
								<div class='row'>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" >
												<div class="fg-line">
												<label for="netAmount">Net Amount</label>
													<input type="text" readonly id='netAmount' class="form-control autofocus" placeholder='Net Amount' id="netAmount"   name="netAmount" ng-model="deliveryChallan.netAmount"   readonly="readonly" tabindex="1" maxlength='50'  ng-required="true" with-floating-label />
																		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							</div> 	
							
							
							
							
							
							
							
							<br />
								<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(deliveryChallanForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 		
						</form>
						<footer data-ng-include="'template/batchDetailsPopup.jsp'"  style='font-weight:bold;'></footer>
					</div>
				</div>
		</section>
	</section>
<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>					 