<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RyotWiseWeighmentDetail" ng-init="loadSeason();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Ryot Wise Weighment Detail</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
					<form method="POST" action="/SugarERP/generateRyotWiseWeighmentDetail.html" target="_blank">
						<div class="row">
							<div class="col-sm-4">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="season"  data-ng-model="AddedSeason.season" ng-options="season.season as season.season for season in seasons  | orderBy:'-season':true"  ng-change='loadryotCode(AddedSeason.season)' tabindex="1" >
													<option value=''>Select Season</option>
												</select>	
											</div>				
										</div>
			                    </div>
							</div>
							<div class="col-sm-4">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
													<select chosen class="w-100"  name="ryotCode" ng-model="AddedSeason.ryotCode" ng-options="ryotCode.ryotCode as ryotCode.ryotCode for ryotCode in ryotCodeData | orderBy:'-id':true">
														<option value="">Select Ryot Code</option>
													</select>	
											</div>				
										</div>
			                    </div>
							</div>
							<!--
							<div class="col-sm-4">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<label for="fromDate">From Date</label>
												<input type="text" class="form-control datepicker autofocus" placeholder='From Date'  tabindex="1" name="fromDate"  data-input-mask="{mask: '00-00-0000'}"  id="fromDate" with-floating-label  maxlength="10" ng-model="fromDate"/>
											</div>				
										</div>
			                    </div>
							</div>
							<div class="col-sm-4">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<label for="toDate">To Date</label>
												<input type="text" class="form-control datepicker autofocus" placeholder='To Date'  tabindex="1" name="toDate"  data-input-mask="{mask: '00-00-0000'}"  id="toDate" with-floating-label  maxlength="10" ng-model="toDate"/>
											</div>				
										</div>
			                    </div>
							</div>
							-->
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="input-group">            			            
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">									
											<label class="radio radio-inline m-r-20"><b>Print Mode</b></label>	
            					            <label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="HTML" data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>HTML
											</label>
											<label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="PDF"   data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>PDF
											</label>																																					
											<label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="LPT"   data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>LPT
											</label>																																					
			                        	</div>																														
									</div>			                
			                    </div>
							</div>
						</div>
						<!--<div class="row">
							<div class="col-sm-6">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<label for="toDate">Extra Field</label>
												<input type="text" class="form-control autofocus" placeholder='To Date'  tabindex="1" name="toDate"  data-input-mask="{mask: '00-00-0000'}"  id="toDate" with-floating-label  maxlength="10" ng-model="toDate"/>
											</div>				
										</div>
			                    </div>
							</div>
						</div>-->
						<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary">Generate Report</button>
								<!--	<button type="reset" class="btn btn-primary">Reset</button>  -->
								</div>
							</div>						 	
						</div>	
						
					</form>
		      
		
		
		
		
		
		
		<!--<table id="reptbl" width="350px" border="1">

        <tr>
            <td>
                Enter Bank Id <input type="text" id="noofYears" name="bankid"/>                
            </td>			
        </tr>
        <tr>
            <td>
				  <input type="radio" value="html" name="type" checked="checked"/> :HTML
                  <input type="radio" value="pdf" name="type"/> :PDF
                 <input type="radio" value="lpt" name="type"/> :LPT 
            </td>			
		</tr>
		<tr>
			<td><input type="submit"  value="Get Branch List"  /></td>
		</tr>
 
         </table>-->  
 
    					 
							 
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



