<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	<style>
	table {
        display: block;
        overflow-x: auto;
    }
	</style>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneSupplyPaymentStatement" ng-init="loadSeason();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Supply And Payment Statement</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
					<form method="POST">
						<div class="row">
							<div class="col-sm-6">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="season"  data-ng-model="AddedSeason.season" ng-options="season.season as season.season for season in seasons  | orderBy:'-season':true" tabindex="1">
													<option value=''>Select Season</option>
												</select>	
											</div>				
										</div>
			                    </div>
							</div>
							
							
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary" ng-click="generateReport(AddedSeason.season);">Generate Report</button>
									
									
								 <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file m-r-10">
                            <span class="fileinput-new">Export Excel</span>
                            <span class="fileinput-exists">Change</span>
                          <input type="button" ng-click="exportData()"/>
 
 
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                    </div>
								</div>
							</div>
						</div>
						<!--<div class="row">
							<div class="col-sm-12">
								<div class="input-group">            			            
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">									
											<label class="radio radio-inline m-r-20"><b>Print Mode</b></label>	
            					            <label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="HTML" data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>HTML
											</label>
											<label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="PDF"   data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>PDF
											</label>																																					
											<label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="LPT"   data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>LPT
											</label>
											<label class="radio radio-inline m-r-20">
												<input type="radio" name="mode" value="Excel"   data-ng-model='AddedSeason.mode'>
												<i class="input-helper"></i>Excel
											</label>
			                        	</div>																														
									</div>			                
			                    </div>
							</div>
						</div>-->
						
						
						<div>
						<table style="width:100%;text-align:left;" border="1" >
						<tr style='background-color:rgb(34, 152, 242);color:#FFFFFF;'>
						<td colspan='7'></td>
						<td colspan="5"><-------DEDUCTIONS--------></td>
						<td colspan="3"><---------LOANS&ADVANCES----------></td>
						<td colspan="2"><-----LOAN-----></td>
						<td colspan="2"><-----SB-----></td>
						<td colspan='2'></td>
						<tr style='background-color:rgb(34, 152, 242);color:#FFFFFF;'>
						<td>RYOT CODE</td><td>RYOT NAME</td><td>ZONE</td><td>CIRCLE</td><td>VILLAGE</td><td>CANE SUPPLY</td><td>CANE VALUE</td><td>UL</td><td>CDC</td><td>TRANSPORT</td><td>HARVESTING</td><td>OTHER DED</td><td>LOAN PRINCIPLE</td><td>LOAN INTEREST</td><td>ADVANCES</td><td>TRANSFER</td><td>PAID</td><td>TRANSFER</td><td>PAID</td><td>BALANCE DUE</td><td>PAYMENT RESOURCE/BANK</td>
						</tr>
						<tr  ng-repeat="caneSupplyPayment in UpdateLoanData">
						<td align="left">{{caneSupplyPayment.Ryotcode}}</td>
						<td align="left">{{caneSupplyPayment.Ryotname}}</td>
						<td align="left">{{caneSupplyPayment.zone}}</td>
						<td align="left">{{caneSupplyPayment.circle}}</td>
						<td align="left">{{caneSupplyPayment.village}}</td>
						<td align="right">{{caneSupplyPayment.Canesupply}}</td>
						<td align="right">{{caneSupplyPayment.Canevalue}}</td>
						<td align="right">{{caneSupplyPayment.ul}}</td>
						<td align="right">{{caneSupplyPayment.cdc}}</td>
						<td align="right">{{caneSupplyPayment.transport}}</td>
						<td align="right">{{caneSupplyPayment.harvesting}}</td>
						<td align="right">{{caneSupplyPayment.otherded}}</td>
						<td align="right">{{caneSupplyPayment.Lprinciple}}</td>
						<td align="right">{{caneSupplyPayment.finalinterest}}</td>
						<td align="right">{{caneSupplyPayment.advance}}</td>
						<td align="right">{{caneSupplyPayment.paidamt}}</td>
						<td align="right">{{caneSupplyPayment.advisedamt}}</td>
						<td align="right">{{caneSupplyPayment.sbacTransfer}}</td><td align="right">{{caneSupplyPayment.sbpaidamt}}</td><td align="right">{{caneSupplyPayment.balancedue}}</td>
						<td align="left">{{caneSupplyPayment.Payamantresourcebank}}</td>
						
						</tr>
						</table>
						</div>
						
					</form>
		      
		
		
		
		
		
		
		<!--<table id="reptbl" width="350px" border="1">

        <tr>
            <td>
                Enter Bank Id <input type="text" id="noofYears" name="bankid"/>                
            </td>			
        </tr>
        <tr>
            <td>
				  <input type="radio" value="html" name="type" checked="checked"/> :HTML
                  <input type="radio" value="pdf" name="type"/> :PDF
                 <input type="radio" value="lpt" name="type"/> :LPT 
            </td>			
		</tr>
		<tr>
			<td><input type="submit"  value="Get Branch List"  /></td>
		</tr>
 
         </table>-->  
 
    					 
							 
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



