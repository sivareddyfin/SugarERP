	<style>
.styletr {border-bottom: 1px solid grey;}


</style>

	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="StockRequestController" ng-init="addFormField();getAllDepartments();getUserDeptStatus();onloadFunction();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Request</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StockRequestForm" novalidate  ng-submit="stockIndentReqSubmit(AddedStockRequest,StockRequestForm);">
						<input type="hidden" ng-model="AddedStockRequest.modifyFlag"  name="modifyFlag"/>	
						 <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockRequestForm.requestNo.$invalid && (StockRequestForm.requestNo.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="requestNo">Request No.</label>
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="requestNo" data-ng-model="AddedStockRequest.stockReqNo" placeholder="Request Number" tabindex="1"  maxlength="15" id="requestNo"  with-floating-label readonly/>
									</div>
								<p ng-show="StockRequestForm.requestNo.$error.required && (StockRequestForm.requestNo.$dirty || Addsubmitted)" class="help-block">Stock Request No. is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	

							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockRequestForm.employeeName.$invalid && (StockRequestForm.employeeName.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="employeeName">Employee Name</label>
										<input type="text" class="form-control" data-ng-required='true' tabindex="4"   name="employeeName" data-ng-model="AddedStockRequest.authorisedUser" placeholder="Employee Name" maxlength="20" id="employeeName"  with-floating-label readonly />
									</div>
								<p ng-show="StockRequestForm.employeeName.$error.required && (StockRequestForm.employeeName.$dirty || Addsubmitted)" class="help-block">Employee Name is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							 

							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : StockRequestForm.department.$invalid && (StockRequestForm.department.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100" name="department" tabindex="2"   data-ng-model="AddedStockRequest.deptID " ng-options="department.deptCode as department.department for department in DepartmentsData" data-ng-required='true' ng-change="getMaxStockReqid(AddedStockRequest.deptID)">
														<option value=''>Department</option>
													 </select>	
												</div>
												<p ng-show="StockRequestForm.department.$error.required && (StockRequestForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>		
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockRequestForm.date.$invalid && (StockRequestForm.date.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="date">Date</label>
													<input type="text" class="form-control date" tabindex="3" name="date" ng-required='true' data-ng-model="AddedStockRequest.reqDate" placeholder="Stock Date" maxlength="10"  data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/" id="date"  with-floating-label />
												</div>
												<p ng-show="StockRequestForm.date.$error.required && (StockRequestForm.date.$dirty || Addsubmitted)" class="help-block">Date is  required</p>
												<p ng-show="StockRequestForm.date.$error.pattern && (StockRequestForm.date.$dirty || Addsubmitted)" class="help-block">Valid Date is  required</p>
											 </div>
										</div>
									</div>
								</div>
							
							</div>
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive'>
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>Action</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>UOM</th>
									<th>Purpose</th>
									<th>Requesting Qty</th>
									<th>Stock in Dept.</th>
									<th>Stock in CS.</th>
									<th>Consumption For</th>
									
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData">
									<td><button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();'ng-if="stockData.id=='1'" ><i class="glyphicon glyphicon-plus-sign"></i>
										<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(stockData.id);'ng-if="stockData.id!='1'" ><i class="glyphicon glyphicon-remove-sign"></i>
										</td>
									
									<td>
									<div class="form-group" ng-class="{'has-error':StockRequestForm.productCode{{$index}}.$invalid && (StockRequestForm.productCode{{$index}}.$dirty || submitted)}">
									
									<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="stockData.productCode" ng-change="loadProductCode(stockData.productCode,$index);duplicateProductCode(stockData.productCode,$index,stockData.id);" >
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
       						    </select>    
   									</datalist>
									
									<p ng-show="StockRequestForm.productCode{{$index}}.$error.required && (StockRequestForm.productCode{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									</td>
									<td>
								
									<div class="form-group" ng-class="{'has-error':StockRequestForm.productName{{$index}}.$invalid && (StockRequestForm.productName{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Product Name" maxlength="30" name="productName{{$index}}" ng-model="stockData.productName" ng-required="true" readonly>
									
									<p ng-show="StockRequestForm.productName{{$index}}.$error.required && (StockRequestForm.productName{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									
									</td>
									<td>
										<input type="hidden" ng-model="stockData.uom" name="uomid{{$index}}" />
									<div class="form-group" ng-class="{'has-error':StockRequestForm.uom{{$index}}.$invalid && (StockRequestForm.uom{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="UOM" data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/"  maxlength="10" name="uom{{$index}}" ng-model="stockData.uomName" ng-required="true" readonly>
									
									<p ng-show="StockRequestForm.uom{{$index}}.$error.required && (StockRequestForm.uom{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									<p ng-show="StockRequestForm.uom{{$index}}.$error.pattern && (StockRequestForm.uom{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
									
									</div>
									</td><td>
									<div class="form-group" ng-class="{'has-error':StockRequestForm.purpose{{$index}}.$invalid && (StockRequestForm.purpose{{$index}}.$dirty || submitted)}">
									<input type="text" class="form-control1" placeholder="Purpose"  maxlength="20" name="purpose{{$index}}" ng-model="stockData.purpose" ng-required="true">
									
									<p ng-show="StockRequestForm.purpose{{$index}}.$error.required && (StockRequestForm.purpose{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									</td><td>
									<div class="form-group" ng-class="{'has-error':StockRequestForm.reqstQty{{$index}}.$invalid && (StockRequestForm.reqstQty{{$index}}.$dirty || submitted)}">
									<input type="number" class="form-control1" placeholder="Requesting Qty." maxlength="10" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/"  name="reqstQty{{$index}}" ng-model="stockData.quantity" ng-required="true">
									
									<p ng-show="StockRequestForm.reqstQty{{$index}}.$error.required && (StockRequestForm.reqstQty{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									<p ng-show="StockRequestForm.reqstQty{{$index}}.$error.pattern && (StockRequestForm.reqstQty{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
									
									</div>
									</td>
									<td>
									<div class="form-group" ng-class="{'has-error':StockRequestForm.stockinDept{{$index}}.$invalid && (StockRequestForm.stockinDept{{$index}}.$dirty || submitted)}">
									<input type="number" class="form-control1" placeholder="Stock in Dept" maxlength="10" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/"  name="stockinDept{{$index}}" ng-model="stockData.deptStock" ng-required="true" readonly>
									
									<p ng-show="StockRequestForm.stockinDept{{$index}}.$error.required && (StockRequestForm.stockinDept{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									<p ng-show="StockRequestForm.stockinDept{{$index}}.$error.pattern && (StockRequestForm.stockinDept{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									</td><td>
									<div class="form-group" ng-class="{'has-error':StockRequestForm.stockinCs{{$index}}.$invalid && (StockRequestForm.stockinCs{{$index}}.$dirty || submitted)}">
									<input type="number" class="form-control1" placeholder="Stock in CS" maxlength="12" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/"  name="stockinCs{{$index}}" ng-model="stockData.csStock" ng-required="true" readonly>
									
									<p ng-show="StockRequestForm.stockinCs{{$index}}.$error.required && (StockRequestForm.stockinCs{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									<p ng-show="StockRequestForm.stockinCs{{$index}}.$error.pattern && (StockRequestForm.stockinCs{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									</td>
									<td>
									
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
											 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="consumption{{$index}}" value="0"   data-ng-model="stockData.consumption" >
			    			            					<i class="input-helper"></i>Revenue
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20" style="margin-left:1px;">
				            			    			<input type="radio" name="consumption{{$index}}" value="1"   data-ng-model='stockData.consumption'>
			    				            			<i class="input-helper"></i>Capital
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	
									
									
									</td>
								</tr>
							</table>
							
							</div>
							
						 
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(StockRequestForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
