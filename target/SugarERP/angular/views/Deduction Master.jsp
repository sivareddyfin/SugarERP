	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="DeductionMaster" ng-init="loadDeductionMasterCode();getAllDeduction();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Standard Deduction Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="DeductionMasterForm" ng-submit="DeductionSubmit(AddedDeduction,DeductionMasterForm);" novalidate>
						  <input type="hidden" name="screenName" ng-model="AddedDeduction.screenName" />
					 		 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	<div class="fg-line">
										<label for="Deduction">Deduction Code</label>
											 <input type="text" class="form-control" placeholder="Deduction Code"   maxlength="5" name='stdDedCode'  data-ng-model="AddedDeduction.stdDedCode" readonly/ id="Deduction" with-floating-label >
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : DeductionMasterForm.amount.$invalid && (DeductionMasterForm.amount.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
											<label for="amount">Deduction Amount</label>
											<input type="text" name="amount" class="form-control" data-ng-model="AddedDeduction.amount" placeholder="Deduction Amount"  maxlength="10" tabindex="2" data-ng-required="true" ng-blur="spacebtw('amount'); decimalpoint('amount'); "  id="amount" with-floating-label style="text-align:right;" data-ng-pattern="/^[0-9.\s]*$/">
			                        	</div>
										<p ng-show="DeductionMasterForm.amount.$error.required && (DeductionMasterForm.amount.$dirty || Addsubmitted)" class="help-block">Deduction Amount is required.</p>
										<p ng-show="DeductionMasterForm.amount.$error.pattern && (DeductionMasterForm.amount.$dirty || Addsubmitted)" class="help-block">Enter Valid Deduction Amount.</p>
								
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>



							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" ng-class="{ 'has-error' : DeductionMasterForm.name.$invalid && (DeductionMasterForm.name.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="dName">Deduction Name</label>
											<input type="text" class="form-control autofocus" placeholder="Deduction Name" maxlength="25" autofocus name="name"  data-ng-model="AddedDeduction.name"  tabindex="1" data-ng-required='true'  ng-blur="spacebtw('name');decimalpoint('amount');validateDup();"   data-ng-pattern="/^[a-zA-Z\s]*$/"  id="dName" with-floating-label >
											</div>
										<p ng-show="DeductionMasterForm.name.$error.required && (DeductionMasterForm.name.$dirty || Addsubmitted)" class="help-block">Deduction Name is required.</p>
										<p ng-show="DeductionMasterForm.name.$error.pattern && (DeductionMasterForm.name.$dirty || Addsubmitted)" class="help-block"> Enter Valid Deduction Name</p>
										<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedDeduction.name!=null">Deduction Name Already Exist.</p>
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						
							<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Amount Validity :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="amountValidity" value="0"    data-ng-model="AddedDeduction.amountValidity">
			    					    				<i class="input-helper"></i>Single Amount
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="amountValidity" value="1"   data-ng-model="AddedDeduction.amountValidity">
					    								<i class="input-helper"></i>Per Ton
							  		 				</label>						 							 
												</div>
											</div>
										</div>
							   
							    
							
							
							
							
							</div>
							</div>
							
							 </div>
							 
							 <div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"    data-ng-model="AddedDeduction.status">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"   data-ng-model="AddedDeduction.status">
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
    	        			  
							 
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedDeduction.modifyFlag"  />
							 <br />
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(DeductionMasterForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Deductions</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
									<div class="container1">	
								<form name="EditDeductionMasterForm" novalidate>					
							        <table ng-table="DeductionMaster.tableEdit" class="table table-striped table-vmiddle">
									<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Deduction Code</span></th>
													<th><span>Deduction Name</span></th>
													<th><span>Deduction Amount</span></th>
													<th><span>Amount Validity</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
										
								        <tr ng-repeat="Deduction in DeductionData"  ng-class="{ 'active': Deduction.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Deduction.$edit" ng-click="Deduction.$edit = true;Deduction.modifyFlag='Yes';Deduction.screenName='Standard Deductions';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Deduction.$edit" data-ng-click="UpdateDeductionMaster(Deduction,$index);Deduction.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Deduction.modifyFlag"  />
											    <input type="hidden" name="screenName{{$index}}" ng-model="Deduction.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Deduction.$edit">{{Deduction.stdDedCode}}</span>
					    		                <div ng-if="Deduction.$edit"><input class="form-control" type="text"  ng-model="Deduction.stdDedCode" placeholder='Deduction Code' maxlength="10" name="stdDedCode{{$index}}" readonly /></div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!Deduction.$edit">{{Deduction.name}}</span>
							                     <div ng-if="Deduction.$edit">
                                                 <div class="form-group" ng-class="{ 'has-error' : EditDeductionMasterForm.name{{$index}}.$invalid && (EditDeductionMasterForm.name{{$index}}.$dirty || submitted)}">												
<input class="form-control" type="text" ng-model="Deduction.name" placeholder='Deduction Name' maxlength="25" name="name{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('name',$index);validateDuplicate(Deduction.name,$index);"/>
			<p ng-show="EditDeductionMasterForm.name{{$index}}.$error.required && (EditDeductionMasterForm.name{{$index}}.$dirty || submitted)" class="help-block">Deduction Name is required.</p>
			<p ng-show="EditDeductionMasterForm.name{{$index}}.$error.pattern  && (EditDeductionMasterForm.name{{$index}}.$dirty || submitted)" class="help-block">Enter  valid Deduction Name.</p>				
			<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Deduction.name!=null">Deduction Name Already Exist.</p>									

                                                 </div>
												 </div>
							                  </td>
							                  
											  <td>
							                       <span ng-if="!Deduction.$edit">{{Deduction.amount}}</span>
							                     <div ng-if="Deduction.$edit">
                                                 <div class="form-group" ng-class="{ 'has-error' : EditDeductionMasterForm.amount{{$index}}.$invalid && (EditDeductionMasterForm.amount{{$index}}.$dirty || submitted)}">												
<input class="form-control" type="text" ng-model="Deduction.amount" placeholder='Deduction Amount' maxlength="25" name="amount{{$index}}"    data-ng-required='true'  ng-blur="decimalpointgrid('amount',$index)"/>
			<p ng-show="EditDeductionMasterForm.amount{{$index}}.$error.required && (EditDeductionMasterForm.amount{{$index}}.$dirty || submitted)" class="help-block">Deduction Amount is required.</p>
																

                                                 </div>
												 </div>
							                  </td>
											   <td style="width:235px;">
							                     
												  <span ng-if="!Deduction.$edit">
												  		<span ng-if="Deduction.amountValidity=='0'">Single Amount</span>
														<span ng-if="Deduction.amountValidity=='1'">Per Ton</span>
												  </span>
        		            					  <div ng-if="Deduction.$edit">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="amountValidity{{$index}}" value="0"  data-ng-model='Deduction.amountValidity'>
			    					    				<i class="input-helper"></i>Single Amount
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="amountValidity{{$index}}" value="1" data-ng-model='Deduction.amountValidity'>
			    										<i class="input-helper"></i>Per Ton
							  		 				</label>						 							 												  
												  </div>
							                  </td>										
											  <td style="width:235px;">
							                     
												  <span ng-if="!Deduction.$edit">
												  		<span ng-if="Deduction.status=='0'">Active</span>
														<span ng-if="Deduction.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Deduction.$edit">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status{{$index}}" value="0"  data-ng-model='Deduction.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status{{$index}}" value="1" data-ng-model='Deduction.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>						 							 												  
												  </div>
							                  </td>											  
							             </tr>
										 </tbody>
								         </table>	
									</form>
									</div>
								</section>					 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
