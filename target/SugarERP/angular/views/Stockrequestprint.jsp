<script type="text/javascript">
    $(".autofocus").focus();
</script>

<style>

   tr,th,td{ 
    text-align:center;
	
	 }
	 
	#asdf {
  font-family: "Gothic 57";
  Gothic 57 Normal.ttf) format("truetype");
}
h1 { font-family: "Gothic 57", sans-serif }   
	 
	
</style>

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
	<section id="content" data-ng-controller="StockrequestprintController"  ng-init='requestPrint(); updatePrintData();'>       
        	<div class="container">				
    			<div class="block-header"><h2><b>Stock Request Print</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding">
				  
				  <!--------Form Start----->
					 <!-------body start------>
					 
					
							
			
			 
			 <div id="stockRequestPrint" style="color:#0099CC;display:none;">
			<table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
					<tr style="text-align:center; font-size:18px;"><td><h1 style="color:#0099CC;font-family: 'Gothic 57', sans-serif;">Sri Sarvaraya Sugars Limited</h1></td></tr><br/>
					<tr style="text-align:center; font-size:14px;"><td><strong style="color:#0099CC;">CHELLURU</strong></td></tr><br/>
					<tr style="text-align:center; font-size:14px;"><td><strong style="color:#0099CC;">REQUISITION FROM ZONAL OFFICER</strong></td></tr><br/>
					
			</table>
			
			<div class="row" style="font-family:Georgia, Helvetica, sans-serif; border:1px solid #0099CC; border-radius:5px ; margin-bottom:2px;">
			<div class="col-sm-12">
			<br />
					
						<table width="100%" border="0" style="color:#0099CC; " >
<tr><td style="text-align:left; width:50%; font-family:Calibry, Helvetica, sans-serif;">Sl.No :<span style="font-size:13px;">&nbsp;&nbsp;{{AddedStockRequest.stockReqNo}}</span>  </td><td></td><td style="font-family:Calibry, Helvetica, sans-serif;">Date : &nbsp;&nbsp;<span style="text-align:center; font-family:Calibry, Helvetica, sans-serif; font-size:13px;" > 20-03-2017<%--{{AddedStockRequest.reqDate}}--%> </span> </td></tr>
						<tr><td style="text-align:left;">To  </td><td style="float:left;">From</td><td></td></tr>
						<tr><td style="text-align:left;">The Asst. Manager (Stores & Purchase)</td><td style="float:left;"> Zonal Officer of </td><td></td></tr>
						<tr><td style="text-align:left;">Sri Sarvaraya Sugars Ltd.,</td><td></td><td></td></tr>
						<tr><td style="text-align:left;">Chelluru </td><td></td><td></td></tr>
						<tr><td style="text-align:right; font-size:16px; font-weight: 300;"><u>Through Sr. General Manager</u></td><td colspan="2"></td></tr>
						<tr><td style="text-align:left; font-size:14" colspan="3"><br />We require the following items inorder to distribute  the same to our Sugar Cane growers in our zone</td></tr>
						</table>
						
						
					
				<br/>
				
					<table style="width:100%; border-collapse:collapse;" border="1"  bordercolor="#0099CC"  >
					<thead>
				   <tr style="color:#0099CC" >
				   
				      <th style="font-size:14px;">S.No</th>  <th style="font-size:14px;"> Name of the item  </th>  <th style="font-size:14px;">Quantity(Nos.)</th>   <th style="font-size:14px;"> No. of Acres  </th>   <th style="font-size:14px;"> Remarks (If any)  </th>
					</tr></thead>
				    <tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ng-repeat="stockRequestPrintdata in stockRequuest">
					<td style="font-family:Calibry, Helvetica, sans-serif; text-align:center; font-size:13px;">{{$index+1}}</td>
					<td style="text-align:center;font-size:13px;">{{stockRequestPrintdata.itemName}} </td> 
					<td style="text-align:center; font-family:Calibry, Helvetica, sans-serif;font-size:13px;" >{{stockRequestPrintdata.quantity}}</td>
					<td style="text-align:center; font-family:Calibry, Helvetica, sans-serif;font-size:13px;">{{stockRequestPrintdata.noOfAcres}}</td> 
					<td style="text-align:center;font-size:13px;">{{stockRequestPrintdata.remarks}}</td>
				    </tr>
					
				</table> 
				<br> 
				
			</div>
			</div>
			 
			<div class="row" style="font-family:Georgia, Helvetica, sans-serif; border:1px solid #0099CC; border-radius:5px ; ">
			<div class="col-sm-12">
			     <div class="row" >
					<div class="col-xs-12">	
						<table width="100%">
						  <tr style="color:#0099CC">
						  <td style="text-align:left;">
						  <br />	
						   <p></p>
							 <span> <strong>Zonal Officer </strong></span>
						</td>
						<td style="text-align:center;"> <br />	
							<p></p>
							<span><strong> AGM(Cane)</strong></span>
						</td>
						<td style="text-align:right;"> <br />	
							<p></p>
							<span><strong> Sr. General Manager</strong></span>
						</td></tr></table>
						
						</div>
					
				</div>
			</div>
			</div>
			 </div> 
			
			<div style="text-align:center;"><button class="btn btn-primary btn-sm m-t-10" ng-click="printStockReq();" >Print</button></div>
				
				</div>
				</div>
				</div>
	</section>
</section>>
				  
				  