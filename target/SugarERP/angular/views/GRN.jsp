	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>

<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GRNController" ng-init="getuserLoginId();onloadFunction();getAllDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Goods Receipt Note</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="GrnForm" novalidate ng-submit="GRNSubmit(AddedGRN,GrnForm)" >
						<input type="hidden" ng-model="AddedGRN.status" name="status" />
<input type="hidden" ng-model="AddedGRN.modifyFlag" name="modifyFlag" />
						 <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GrnForm.grnNo.$invalid && (GrnForm.grnNo.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="grnNo">Grn No</label>
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="grnNo" data-ng-model="AddedGRN.grnNo" placeholder="GRN No" tabindex="1"   readonly="readonly" id="grnNo" with-floating-label />
									</div>
								<p ng-show="GrnForm.grnNo.$error.required && (GrnForm.grnNo.$dirty || Addsubmitted)" class="help-block">GRN No is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GrnForm.grnDate.$invalid && (GrnForm.grnDate.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="grnDate">Grn Date</label>
													<input type="text" class="form-control date" tabindex="2" name="grnDate" ng-required='true' data-ng-model="AddedGRN.grnDate" placeholder="GRN Date" maxlength="10" data-input-mask="{mask: '00-00-0000'}" data-ng-required="true" id="grnDate" with-floating-label  data-input-mask="{mask: '00-00-0000'}"  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/" />
												</div>
												<p ng-show="GrnForm.grnDate.$error.required && (GrnForm.grnDate.$dirty || Addsubmitted)" class="help-block">GRN Date is required</p>
												<p ng-show="GrnForm.grnDate.$error.pattern && (GrnForm.grnDate.$dirty || Addsubmitted)" class="help-block">GRN Date is required</p>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GrnForm.stockIssueNo.$invalid && (GrnForm.stockIssueNo.$dirty || submitted)}">
											 	<div class="fg-line">
											
												<label for="stockIssueNo">Stock Issue No</label>
													 <input type="text" class="form-control " tabindex="3" name="stockIssueNo" ng-required='true' data-ng-model="AddedGRN.stockIssueNo" placeholder="Stock Issue No." maxlength="20" readonly="readonly" id="stockIssueNo" with-floating-label ng-readonly="{{checkValue}}" ng-blur="getGRNSummaryDetails(AddedGRN.stockIssueNo);"/>	
												</div>
									<p ng-show="GrnForm.stockIssueNo.$error.required && (GrnForm.stockIssueNo.$dirty || Addsubmitted)" class="help-block">GRN Stock Issue No is required</p>	 
											 </div>
										</div>
									</div>
								</div>															
							</div>
						
							
							
								
						 </div>
						
						<div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GrnForm.loginId.$invalid && (GrnForm.loginId.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="loginId">Login ID</label>
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="loginId" data-ng-model="AddedGRN.loginId" placeholder="Login User" tabindex="1"   readonly="readonly" id="loginId" with-floating-label />
									</div>
								<p ng-show="GrnForm.loginId.$error.required && (GrnForm.loginId.$dirty || Addsubmitted)" class="help-block">Login User is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : GrnForm.deptId.$invalid && (GrnForm.deptId.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100"  tabindex="3" data-ng-required='true' data-ng-model='AddedGRN.deptId' name="deptId" ng-options="deptId.deptCode as deptId.department for deptId in DepartmentsData" ng-required='true' ng-change="getMaxStockReqid(AddedGRN.deptId)">
																		<option value="">Department</option>
																	</select>	
												</div>
												<p ng-show="GrnForm.deptId.$error.required && (GrnForm.deptId.$dirty || Addsubmitted)" class="help-block">Select Department</p>
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							
						
							
							
								
						 </div>
						
						 
						 <br />
							<div id="hideTable" style="display:none;">
							<table style="width:100%;"  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Issued Qty</th>
											<th>Accepted Qty</th>
											<th>Rejected Qty</th>
											<th>&nbsp;&nbsp; Batch</th>
											<th>MRP</th>
											<th>Mfg Dt</th>
											<th>Cause</th>
											
											
										
											
            							</tr>
										<tr class="styletr" style="height:70px;" ng-repeat='grnData in data'>
										<td>
											
<button type="button" id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='removeRow(grnData.id);'><i class="glyphicon glyphicon-remove-sign"></i></button>
												</td>
											<td>
											

											
											
											
											
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.productCode{{$index}}.$invalid && (GrnForm.productCode{{$index}}.$dirty || submitted)}">
											
											<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="grnData.productCode" ng-change="loadProductCode(grnData.productCode,$index);getproductIdforPopup(grnData.productCode,$index);" style="height:40px;" ng-required='true'>
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
       						    </select>    
   									</datalist>
											
											
											
											
											<p ng-show="GrnForm.productCode{{$index}}.$error.required && (GrnForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											<p ng-show="GrnForm.productCode{{$index}}.$error.pattern && (GrnForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.productName{{$index}}.$invalid && (GrnForm.productName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="productName{{$index}}"  ng-model="grnData.productName" data-ng-required='true' placeholder="Product Name"  style="height:40px;" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" maxlength="30" readonly />
												<p ng-show="GrnForm.productName{{$index}}.$error.required && (GrnForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.productName{{$index}}.$error.pattern && (GrnForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											<td style="display:none;">
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.uom{{$index}}.$invalid && (GrnForm.uom{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uom{{$index}}"  ng-model="grnData.uom" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="GrnForm.uom{{$index}}.$error.required && (GrnForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.uom{{$index}}.$error.pattern && (GrnForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.uomName{{$index}}.$invalid && (GrnForm.uomName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uomName{{$index}}"  ng-model="grnData.uomName" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="GrnForm.uomName{{$index}}.$error.required && (GrnForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.uomName{{$index}}.$error.pattern && (GrnForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.issuedQty{{$index}}.$invalid && (GrnForm.issuedQty{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="issuedQty{{$index}}"  ng-model="grnData.issuedQty" placeholder="Issued" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[0-9.\s]*$/" maxlength="10" readonly/>
												<p ng-show="GrnForm.issuedQty{{$index}}.$error.required && (GrnForm.issuedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.issuedQty{{$index}}.$error.pattern && (GrnForm.issuedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.acceptedQty{{$index}}.$invalid && (GrnForm.acceptedQty{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="acceptedQty{{$index}}"  ng-model="grnData.acceptedQty" placeholder="Accepted" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[0-9.\s]*$/" maxlength="10" ng-keyup="updateRejectedQty(grnData.issuedQty,grnData.acceptedQty,$index);"/>
												<p ng-show="GrnForm.acceptedQty{{$index}}.$error.required && (GrnForm.acceptedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.acceptedQty{{$index}}.$error.pattern && (GrnForm.acceptedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.rejectedQty{{$index}}.$invalid && (GrnForm.rejectedQty{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="rejectedQtyvvvvvv"  ng-model="grnData.rejectedQty" placeholder="Rejected" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[0-9.\s]*$/" maxlength="10" readonly/>
												<p ng-show="GrnForm.rejectedQty{{$index}}.$error.required && (GrnForm.rejectedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.rejectedQty{{$index}}.$error.pattern && (GrnForm.rejectedQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : GrnForm.batchId{{$index}}.$invalid && (GrnForm.batchId{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="batchId{{$index}}"  ng-model="grnData.batchId" placeholder="BatchId" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="GrnForm.batchId{{$index}}.$error.required && (GrnForm.batchId{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="GrnForm.batchId{{$index}}.$error.pattern && (GrnForm.batchId{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											
											
											
											
											
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : GrnForm.mrp{{$index}}.$invalid && (GrnForm.mrp{{$index}}.$dirty || submitted)}" ><input type="text" class="form-control1" id name="mrp{{$index}}"  ng-model="grnData.mrp"  placeholder="MRP"  style="height:40px;" maxlength="12"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="getMRPid{{$index}}"    /><p ng-show="GrnForm.mrp{{$index}}.$error.required && (GrnForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>	<p ng-show="GrnForm.mrp{{$index}}.$error.pattern && (GrnForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : GrnForm.mfgDate{{$index}}.$invalid && (GrnForm.mfgDate{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1 datepicker" placeholder='Mfg Date' maxlength="10" tabindex="10" name="mfgDate{{$index}}" ng-model="grnData.mfgDate" data-input-mask="{mask: '00-00-0000'}" ng-mouseover="ShowDatePicker();" style="height:40px;"  ng-required="true"  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  />
											
											
											<p ng-show="GrnForm.mfgDate{{$index}}.$error.required && (GrnForm.mfgDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="GrnForm.mfgDate{{$index}}.$error.pattern && (GrnForm.mfgDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												
											</div></td>
											
											<td><br /><div class="form-group" ng-class="{ 'has-error' : GrnForm.cause{{$index}}.$invalid && (GrnForm.cause{{$index}}.$dirty || submitted)}">		<input type="text" class="form-control1" name="cause{{$index}}"  ng-model="grnData.cause" placeholder="Cause" style="height:40px;" maxlength="50" ng-required="grnData.rejectedQty!=0"  /><p ng-show="GrnForm.cause{{$index}}.$error.required && (GrnForm.cause{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="GrnForm.cause{{$index}}.$error.pattern && (GrnForm.cause{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												
											</div></td>
											
										</tr>
										
										
														
									</thead>
							</table>
							
						 <br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(GrnForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 
							 </div>			
						</form>
						
						
						
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
