	<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" ></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="AgricultureLoanAppReport" ng-init="loadBranchNames();loadseasonNames();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Agriculture Loan AppReport</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>	
					 <form method="POST" action="/SugarERP/generateAgricultureLoanAppReport.html" target="_blank">
					 
					<div class="row">
					
							<div class="col-sm-3">
								<div class="col-sm-12">
								<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="season"  data-ng-model="AddedBranch.season" ng-options="season.season as season.season for season in seasonNames  | orderBy:'-season':true" tabindex="1">
                                      <option value=''>Select Season</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
								
								</div>
							</div>
					
						<div class="col-sm-6">
						
						     <div class="col-sm-12">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="branchCode"  data-ng-model="AddedBranch.branchName" ng-options="branchName.id as branchName.branchname for branchName in branchNames  | orderBy:'-branchname':true" ng-change="getBatchnumbers(AddedBranch.season,AddedBranch.branchName);" tabindex="1">
                                      <option value=''>Select Bank/Branch</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
					
					</div>
					<div class="col-sm-3">
						
						     <div class="col-sm-12">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="batchno"  data-ng-model="batchno" ng-options="batchno.batchno as batchno.batchno for batchno in batchnumbers  | orderBy:'-batchno':true" tabindex="1">
                                      <option value=''>Select BatchNo</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
					
					</div>
					</div>
					       <div class="row">
						                  <div class="col-sm-12">
                    			             <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Choose Format :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="pdf"   data-ng-model='AddedBranch.status' />
			    					    				<i class="input-helper"></i>PDF
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="html"   data-ng-model='AddedBranch.status'>
					    								<i class="input-helper"></i>HTML
							  		 				</label>
													<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="lpt"   data-ng-model='AddedBranch.status'>
					    								<i class="input-helper"></i>LPT
							  		 				</label>
												<label class="radio radio-inline m-r-20">
												<input type="radio" name="status" value="excel"   data-ng-model='AddedBranch.status'>
													<i class="input-helper"></i>EXCEL
												</label>													
												</div>
											</div>
										</div>
                
							</div>
				     	
				
						
			  
			  <br />
			  
			              <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit"  class="btn btn-primary btn-sm m-t-10">Generate Report</button>
										
									</div>
								</div>
							</div>
							
											</form>
										
									</div>
									</div>
									</div>								
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



