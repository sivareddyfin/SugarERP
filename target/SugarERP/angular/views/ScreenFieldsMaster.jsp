	
	<script type="text/javascript">		
		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="screenfieldmaster"  ng-init="loadscreenfieldmaster();addScreenBranchField();loadScreenfieldNames();">        
        	<div class="container"> 
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Screen Fields Master</b></h2></div>
			   		 <div class="card"> 
			        		<div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 				<form name="screenForm" ng-submit="Addscreenfields(Addedscreens,screenForm);" novalidate>							 		
    	        				<div class="row">
								<div class="col-sm-3"></div>
				                <div class="col-sm-5">
				                    <div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : screenForm.screenId.$invalid && (screenForm.screenId.$dirty || Addsubmitted)}">												
			        	                			<div class="fg-line">
														<div class="select">
															<select chosen class="w-100"  tabindex="1"  name="screenId" data-ng-required='true'  ng-change="screensOnChange(Addedscreens.screenId);" data-ng-model='Addedscreens.screenId'  ng-options="screenId.id as screenId.screenname for screenId in ScreenFieldNames | orderBy:'-screenname':true">
											<option value="">Select Added Screens</option>
														</select>
												</div>
										</div>
						<p ng-show="screenForm.screenId.$error.required && (screenForm.screenId.$dirty || Addsubmitted)" class="help-block">Select Added Screens</p>	
			                	        		</div>
			                    			</div>			                    
				                  		</div>								  
								</div>
								
								<!--<div class="select">
										  	<select chosen class="w-100" id="input_addedscreens">
												<option value="0">Select Added Screens</option>
												<option value="Screen1">Screen1</option>
												<option value="Screen2">Screen2</option>
												<option value="Screen3">Screen3</option>
											</select>
										  </div>-->
										  
								<input type="hidden"  name="modifyFlag" data-ng-model="Addedscreens.modifyFlag"  />				
								<div class="table-responsive">
										<section class="asdok">
											<div class="container1">						
							       			 	<table class="table table-striped table-vmiddle">
											<thead>
											<tr>
												<th><span>Action</span></th>
												<th><span>Field ID</span></th>
												<th><span>Field Name</span></th>
												<th><span>Audit Trail Required</span></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat='ScreenData in data'>
											<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addScreenBranchField();' ng-if="ScreenData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeScreenBranch(ScreenData.id);' ng-if="ScreenData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="ScreenData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(ScreenData.id);' ng-if="ScreenData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(ScreenData.id);' ng-if="ScreenData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				
												</td>
											
											
							<!--<td><button type="button" id="right_All_1" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i></button></td>-->
												<td>
												<div class="form-group" ng-class="{ 'has-error' : screenForm.fieldId{{ScreenData.id}}.$invalid && (screenForm.fieldId{{ScreenData.id}}.$dirty || Addsubmitted)}">	
				<input type="text" class="form-control1" placeholder='Field ID' maxlength="10"  name="fieldId{{ScreenData.id}}" data-ng-model="ScreenData.fieldId" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/"/>
			<p ng-show="screenForm.fieldId{{ScreenData.id}}.$error.required && (screenForm.fieldId{{ScreenData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>
			<p ng-show="screenForm.fieldId{{ScreenData.id}}.$error.pattern && (screenForm.fieldId{{ScreenData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
												</div>
												</td>
												<td>
										<div class="form-group" ng-class="{ 'has-error' : screenForm.fieldLabel{{ScreenData.id}}.$invalid && (screenForm.fieldLabel{{ScreenData.id}}.$dirty || Addsubmitted)}">			
		<input type="text" class="form-control1" placeholder='Field Name' maxlength="25" name="fieldLabel{{ScreenData.id}}" data-ng-model="ScreenData.fieldLabel" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/"/>
		<p ng-show="screenForm.fieldLabel{{ScreenData.id}}.$error.required && (screenForm.fieldLabel{{ScreenData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>
		<p ng-show="screenForm.fieldLabel{{ScreenData.id}}.$error.pattern && (screenForm.fieldLabel{{ScreenData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
		                                        </div>
												</td>
												<td>
            								          <label class="radio radio-inline m-r-20">
								    			        <input type="radio"  value="0" name="isAuditTrail{{ScreenData.id}}" data-ng-model="ScreenData.isAuditTrail" />
						    			            	<i class="input-helper"></i>Yes
											          </label>
				 						              <label class="radio radio-inline m-r-20">
				            						    <input type="radio"  value="1" name="isAuditTrail{{ScreenData.id}}" data-ng-model="ScreenData.isAuditTrail" />
						    				            <i class="input-helper"></i>No
								  		              </label>
				   								</td>
											</tr>
										</tbody>
									</table>
								</div>
						</section>							 
				   </div>    								
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">save</button>	
										<button type="reset" class="btn btn-primary" ng-click="reset(screenForm,Addedscreens.screenId)" ng-if="Addedscreens.screenId==null">Reset</button>	
										<button type="reset" class="btn btn-primary" ng-click="reset(screenForm,Addedscreens.screenId)" ng-if="Addedscreens.screenId!=null">Reset</button>	
									</div>
								</div>						 	
							 </div>
							</form> 							 							 
				        </div> 
			    	</div> 
				</div>
		</section>  
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
