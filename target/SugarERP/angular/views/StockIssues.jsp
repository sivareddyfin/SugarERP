	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="StockIssuesController" ng-init="getAllDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Issues</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StockIssuesForm" novalidate ng-submit="stockIssuesSubmit(AddedStockRequest,StockIssuesForm);">


						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockIssuesForm.fromDate.$invalid && (StockIssuesForm.fromDate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date " data-ng-required='true'  name="fromDate" data-ng-model="AddedStockRequest.fromDate" placeholder="From Date" tabindex="1"  maxlength="15" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  id="fromDate" with-floating-label/>
									</div>
								<p ng-show="StockIssuesForm.fromDate.$error.required && (StockIssuesForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 
								<p ng-show="StockIssuesForm.fromDate.$error.pattern && (StockIssuesForm.fromDate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StockIssuesForm.todate.$invalid && (StockIssuesForm.todate.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="toDate">To Date</label>
													<input type="text" class="form-control date" tabindex="2" name="todate" ng-required='true' data-ng-model="AddedStockRequest.todate" placeholder="Todate" maxlength="10" data-input-mask="{mask: '00-00-0000'}"  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"   id="toDate" with-floating-label />
													<p ng-show="StockIssuesForm.todate.$error.required && (StockIssuesForm.todate.$dirty || Addsubmitted)" class="help-block">To Date is required</p>		 <p ng-show="StockIssuesForm.todate.$error.pattern && (StockIssuesForm.todate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
												</div>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : StockIssuesForm.addDepartment.$invalid && (StockIssuesForm.addDepartment.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100" name="addDepartment" tabindex="3"   data-ng-model="AddedStockRequest.addDepartment" ng-options="addedDepartment.deptCode as addedDepartment.department for addedDepartment  in DepartmentsData" ng-required='true'>
														<option value=''>Department</option>
													 </select>	
												</div>
												<p ng-show="StockIssuesForm.addDepartment.$error.required && (StockIssuesForm.addDepartment.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton" ng-click='getStockRequestDetails(AddedStockRequest.fromDate,AddedStockRequest.todate,AddedStockRequest.addDepartment);'>Get Details</button>
											 	
												
											 
										</div>
									</div>
								</div>
								
							
							</div>
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive' id='hidetable' style="display:none;">
							<table style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									
									<th>Stock Issue</th>
									<th>Date Of Issue</th>
									<th>Issued Items</th>
									<th>Department</th>
									<th>	&nbsp;&nbsp;&nbsp;&nbsp;View | Edit | GRN</th>
									<th>Print STA| Print DN </th>
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockIssuesData" ng-if="stockData.status=='1'" style="background-color:yellow;">
									
									<td style="display:none;"><input type="hidden" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
									<br>
									<div class="form-group">
									&nbsp;&nbsp;{{stockData.stockIssueNo}}
									<input type="hidden" class="form-control1" placeholder="Stock Issue" name="stockIssue{{$index}}" ng-model="stockData.stockIssueNo"  readonly>
									</div>
									</td>
									
									<td>
									<br />
									<div class="form-group">
									{{stockData.issueDate}}
									<input type="hidden" class="form-control1 datepicker" placeholder="Date of Issue" maxlength="10" name="dateOfIssue{{$index}}" ng-model="stockData.issueDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<br />
									<div class="form-group" >
									
									<input type="text" class="form-control1" placeholder="Issued Items" readonly maxlength="20" name="issuedItems{{$index}}" ng-model="stockData.items" style="border:none;background-color:yellow;">									
															
									</div>
									</td>
									<td>
									<br />
									<div class="form-group">
									{{stockData.departmentName}}
									<input type="hidden" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									<td>
									&nbsp;&nbsp;&nbsp;&nbsp;		<a href="#">
          <span style="font-size:20px;" class="glyphicon glyphicon-eye-open" title="View"></span>
        </a>  <a  href="#/store/transaction/StoreStockIssue"  >
										 &nbsp; &nbsp;<img src="../icons/edit.png" title="Edit" style="height:25px;" ng-click="getAllStockIssueDetails(stockData.stockIssueNo);" />
										</a>
        </a>   &nbsp;&nbsp; <a href="#/store/transaction/GRN">
          <span style="font-size:20px;" class="glyphicon glyphicon-share" title="GRN" ng-click="getUpdatedGRNfromStockIssueDetails(stockData.stockIssueNo);"></span>
        </a>
										</a>	
									</td>
									<td>
									</td>
									
									
									
									
								</tr>
								
								
								<tr class="styletr"   ng-repeat="stockData in stockIssuesData" ng-if="stockData.status=='2'" style="background-color:#66CC99;">
									<!--<td> 
									<br />
										<a  href="#/store/transaction/StoreStockIssue"  >
										 &nbsp; &nbsp; &nbsp; &nbsp;<img src="../icons/edit.png" style="height:28px;" ng-click="getAllStockIssueDetails(stockData.stockIssueNo);" />
										</a>
									</td>-->
									<td style="display:none;"><input type="hidden" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
									<br>
									<div class="form-group">
										&nbsp;&nbsp;{{stockData.stockIssueNo}}
									<input type="hidden" class="form-control1" placeholder="Stock Issue" name="stockIssue{{$index}}" ng-model="stockData.stockIssueNo"  readonly>
									</div>
									</td>
									
									<td>
									<br />
									<div class="form-group">
									{{stockData.issueDate}}
									<input type="hidden" class="form-control1 datepicker" placeholder="Date of Issue" maxlength="10" name="dateOfIssue{{$index}}" ng-model="stockData.issueDate"  ng-click="ShowDatePicker();" readonly > 
									</div>
									</td>
									<td>
									<br />
									<div class="form-group" >
									
									<input type="text" class="form-control1" placeholder="Issued Items" readonly maxlength="20" name="issuedItems{{$index}}" ng-model="stockData.items" style="border:none;background-color:#66CC99;">									
															
									</div>
									</td>
									<td>
									<br />
									<div class="form-group">
									{{stockData.departmentName}}
									<input type="hidden" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									<td>
								&nbsp;&nbsp;&nbsp;&nbsp;		<a href="#">
          <span style="font-size:20px;" class="glyphicon glyphicon-eye-open" title="View"></span>
        </a>  <a  href="#/store/transaction/StoreStockIssue"  >
										 &nbsp; &nbsp;<img src="../icons/edit.png" title="Edit" style="height:25px;" ng-click="getAllStockIssueDetails(stockData.stockIssueNo);" />
										</a>
        </a>   &nbsp;&nbsp; <a href="#/store/transaction/GRN">
          <span style="font-size:20px;" class="glyphicon glyphicon-share" title="GRN" ng-click="getUpdatedGRNfromStockIssueDetails(stockData.stockIssueNo);"></span>
        </a>
										</a>					</td>
									<td>
									</td>
									
									
									
									
								</tr>
								
								
								<tr class="styletr"   ng-repeat="stockData in stockIssuesData" ng-if="stockData.status=='3'" style="background-color: #FF8A8A;">
								
									<td style="display:none;"><input type="hidden" name="status{{$index}}" ng-model="stockData.status"  readonly /></td>
									<td>
										&nbsp;&nbsp;<br>
									<div class="form-group">
									{{stockData.stockIssueNo}}
									<input type="hidden" class="form-control1" placeholder="Stock Issue" name="stockIssue{{$index}}" ng-model="stockData.stockIssueNo"  readonly>
									</div>
									</td>
									
									<td>
									<br />
									<div class="form-group">
									{{stockData.issueDate}}
									<input type="hidden" class="form-control1 datepicker" placeholder="Date of Issue" maxlength="10" name="dateOfIssue{{$index}}" ng-model="stockData.issueDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<br />
									<div class="form-group" >
									
									<input type="text" class="form-control1" placeholder="Issued Items" readonly maxlength="20" name="issuedItems{{$index}}" ng-model="stockData.items" style="border:none;background-color:#FF8A8A;">									
															
									</div>
									</td>
									<td>
									<br />
									<div class="form-group">
									{{stockData.departmentName}}
									<input type="hidden" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="stockData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									<td>
								&nbsp;&nbsp;&nbsp;&nbsp;		<a href="#">
          <span style="font-size:20px;" class="glyphicon glyphicon-eye-open" title="View"></span>
        </a> |  <a href="#">
          <span style="font-size:20px;" class="glyphicon glyphicon-edit" title="Edit"></span>
        </a> | <a href="#/store/transaction/GRN">
          <span style="font-size:20px;" class="glyphicon glyphicon-share" title="GRN" ng-click="getUpdatedGRNfromStockIssueDetails(stockData.stockIssueNo);"></span>
        </a>
										</a>
									</td>
									<td>
									</td>
									
									
									
									
								</tr>
								
							</table>
							
							</div>
							
						 <br>
						 					<p style='text-align:right;font-weight:bold;'>Issued : <input type='text' size='3' style='background-color:yellow;' readonly> &nbsp;&nbsp; In Process : 
<input type='text' size='3' readonly style='background-color:#66CC99;'>&nbsp;&nbsp;&nbsp; Closed : <input type='text' size='3' style='background-color: #FF8A8A;' readonly>&nbsp;&nbsp; </p>
											
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
