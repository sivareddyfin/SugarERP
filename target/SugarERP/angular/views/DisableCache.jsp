<!--
/**
 * @Title	:	 Disabling the browser caching ability of jsp pages
 * @author	:	Kishore S S 
 * @version	:	1.0
 */
 */
 -->
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
%>