	<style>
.styletr {border-bottom: 1px solid grey;}


</style>
<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="OpeningStockController" ng-init="loadseasonNames();addFormField();getAllDepartments();">      
        	<div class="container">	
						
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Opening Stock </b></h2></div>
				 
																	
			    <div class="card">
			        <div class="card-body card-padding">
					      
					<!--------Form Start----->				
					 <!-------body start------>						 	
					 	<form name="OpeningStockForm"  novalidate ng-submit="OpeningStockSubmit(OpeningStock,OpeningStockForm);">
						
						<div class='row'>
						
							<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-12">
											<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group "  ng-class="{ 'has-error' : OpeningStockForm.department.$invalid && (OpeningStockForm.department.$dirty || submitted)}">
											 	<div class="fg-line">
													<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='OpeningStock.department' name="department" ng-options="department.deptCode as department.department for department in DepartmentsData">
																		<option value="">Department</option>
																	</select>
												</div>
												<p ng-show="OpeningStockForm.department.$error.required && (OpeningStockForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>	 
														  </div>
													  </div>			                    
									</div>
							</div>
							
							
							</div>
							<div class="col-sm-6">
							<div class='row'>
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
											<div class="fg-line">
												
																		<input type="text" class="form-control date" placeholder='Date Of Entry' id="dateofentry" l  name="dateOfEntry" ng-model="OpeningStock.dateOfEntry"   readonly="readonly" tabindex="2"  ng-required="true" maxlength="12"  />
											</div>
										</div>
									</div>
								</div>
							</div>
							
							</div>
							
							<div class="col-sm-1"></div>
						</div>
						
						  <input type="hidden" name="modifyFlag" ng-model="OpeningStk.modifyFlag" />
							
							  <br />
							
							      <div>
							<table style="width:100%;"  style="border-collapse:collapse;" >
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Open Batch</th>
											<th>Batch</th>
											<th>Exp Date.</th>
											<th>MFR Date</th>
											<th>Quantity</th>
											<th>MRP</th>
											<th>CP</th>
										
										
											
            							</tr>
										<tr class="styletr" style="height:50px;" ng-repeat='openingstockData in data'>
										<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="openingstockData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(openingstockData.id);' ng-if="openingstockData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>

<span ng-if="openingstockData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(openingstockData.id);' ng-if="openingstockData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(openingstockData.id);' ng-if="openingstockData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				
												</td>
							                      <td>
												  
												  <input type='hidden'  name="batchFlag{{$index}}" ng-model="openingstockData.batchFlag" >
												<input type='hidden'  name="batchSeqNo{{$index}}" ng-model="openingstockData.batchSeqNo" >
												<input type='hidden'  name="batchId{{$index}}" ng-model="openingstockData.batchId" >
												<input type='hidden'  name="modifyFlag{{$index}}" ng-model="openingstockData.modifyFlag" >
											
											
			<div >									  
			<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}" class="form-control1" name="productCode{{$index}}"  ng-model="openingstockData.productCode" ng-required="true"  ng-change="loadProductCode(openingstockData.productCode,$index);"/>
					<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
       						    </select>    
   									</datalist>						
											
											</div></td>
											<td>
			 <div >   
				<input type="text" class="form-control1" name="productName{{$index}}"  ng-model="openingstockData.productName" placeholder="Product Name"  ng-required="true" readonly/>
				
				
				</div></td>
											<td>	
			<div >								
				<input type="text" class="form-control1" name="uom{{$index}}"  ng-model="openingstockData.uom" placeholder="UOM" ng-required="true" readonly />
				
				
				</div></td>
				
				<!--<td>
											
											<div>
												<input type="text" class="form-control1" name="uomName{{$index}}"  ng-model="openingstockData.uomName" placeholder="UOM" data-ng-required='true'     data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="20" readonly/>
												
												</div>
												</td>-->
												<td> <button type="button" class="btn btn-default btn-sm" style="height:38px;" ng-click="getbatchPopup(openingstockData.productCode,$index,OpeningStock.department);">
												<span class="glyphicon glyphicon-open"></span> Open
												</button></td>
												
												
												
											<td><div >	<input type="text" class="form-control1" name="batch{{$index}}"  ng-model="openingstockData.batch" placeholder="Batch" /></div></td>
											<td><div >	<input type="text" class="form-control1 datepicker" name="expDate{{$index}}" ng-click="ShowDatePicker();" ng-model="openingstockData.expDate"  placeholder="Exp Date" /></div></td>
											<td>	<div><input type="text" class="form-control1 datepicker" name="mfrDate{{$index}}" ng-click="ShowDatePicker();" ng-model="openingstockData.mfrDate"  placeholder="MFR Date" /></div></td>
											<td>
		
		
		<div  ng-class="{'has-error':OpeningStockForm.qty{{$index}}.$invalid && (OpeningStockForm.qty{{$index}}.$dirty || submitted)}">
			
								<input type="text" class="form-control1" name="qty{{$index}}"  ng-model="openingstockData.qty"  ng-pattern=  "/^[0-9]+(\.[0-9]{1,2})?$/"  ng-required="true" placeholder="Qty" />
			<p ng-show="OpeningStockForm.qty{{$index}}.$error.required && (OpeningStockForm.qty{{$index}}.$dirty || submitted)" class="help-block">Required</p>
			<p ng-show="OpeningStockForm.qty{{$index}}.$error.pattern && (OpeningStockForm.qty{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>						
											</div></td>
											<td>
		<div ng-class="{'has-error': OpeningStockForm.mrp{{$index}}.$invalid && (OpeningStockForm.mrp{{$index}}.$dirty || submitted)}">										
											<input type="text" class="form-control1" name="mrp{{$index}}"  ng-model="openingstockData.mrp"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required="true" placeholder="MRP"  />
					 <p ng-show ="OpeningStockForm.mrp{{$index}}.$error.required && (OpeningStockForm.mrp{{$index}}.$dirty||Addsubmitted) " class="help-block">Required</p>
			   <p ng-show ="OpeningStockForm.mrp{{$index}}.$error.pattern && (OpeningStockForm.mrp{{$index}}.$dirty||Addsubmitted) " class="help-block"> Invalid</p>						
											</div></td>
											<td>
		<div ng-class="{'has-error': OpeningStockForm.cp{{$index}}.$invalid && (OpeningStockForm.cp{{$index}}.$dirty || submitted)}">										
											<input type="text" class="form-control1" name="cp{{$index}}"  ng-model="openingstockData.cp"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required="true" placeholder="CP" />
									 <p ng-show ="OpeningStockForm.cp{{$index}}.$error.required && (OpeningStockForm.cp{{$index}}.$dirty||submitted) " class="help-block">   Required</p>
			   <p ng-show ="OpeningStockForm.cp{{$index}}.$error.pattern && (OpeningStockForm.cp{{$index}}.$dirty||submitted) " class="help-block">   Invalid </p>		
											</div></td>
										</tr>
										
										
														
									</thead>
							</table>
							</div> 
							 
							   	
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="resetForm(OpeningStockForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	
						  
						  
						  
						  <form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">
					   				<div class="row">
										<div class="col-sm-4">
											<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="productCodepopup">Product Code</label>
														
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="productCode" data-ng-model="productCode" placeholder="Product Code" tabindex="1"  id="productCodepopup" with-floating-label readonly  />
													</div>
													
													
													
															  </div>
														  </div>
										
										</div>
										<div class="col-sm-4"><input type="hidden" ng-model="productId" name="productId{{$index}}" /></div>
										<div class="col-sm-4"><input type="hidden"  ng-model="popupId"/></div>
									</div>						  
						  	  					
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th> &nbsp;&nbsp;Select</th>
												<th>Batch ID</th>	
												<th>Batch</th>
												<th>Expiry Date</th>
												<th>Cost Price</th>
												<th>MRP</th>
												<th>Stock</th>
												<th>Mfr. Date</th>
												
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="batchDetails in productBatchData">
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label class="checkbox checkbox-inline m-r-20" style="margin-top:-10px;" >
												<input type="checkbox"  class="checkbox" name="batchFlag{{$index}}"  ng-model="batchDetails.batchFlag" ng-click="batchDatapush(batchDetails.batchFlag,batchDetails.batch,batchDetails.expiryDate,batchDetails.mrp,$index,batchDetails.batchSeqNo,batchDetails.costPrice,batchDetails.batchId,batchDetails.mfrDate);"    ><i class="input-helper" ></i></label></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.batchId" name="batchId{{$index}}" /></td>
											<td><input type="text" class="form-control1"  readonly  ng-model="batchDetails.batch" name="batch{{$index}}"/></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.expiryDate" name="expiryDate{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.costPrice" name="costPrice{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mrp" name="mrp{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.stock" name="stock{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mfrDate" name="mfrDate{{$index}}" /></td>
											
											</tr>
											
											
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 	
							 
							 <div class="row">
					   	<div class="col-sm-4">
							<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="newbatch">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="newabatch" data-ng-model="newBatch.batch" placeholder="Batch" tabindex="1"  id="newbatch" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="expdt">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="expdt" data-ng-model="newBatch.expdt" placeholder="Expiry Date" tabindex="2"  data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$"  id="expdt" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="MRP">MRP</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="MRP" data-ng-model="newBatch.mrp" placeholder="MRP" tabindex="1"  id="MRP" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
					   
					   </div>
					   <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseBatchPopup(newBatch);">Load</button>
									</div>
								</div>						 	
							 </div>						 							 
				        </div>
			    	</div>
					   </div>
					   <br />
					   
					   				
					 </form>	 

							
				
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
