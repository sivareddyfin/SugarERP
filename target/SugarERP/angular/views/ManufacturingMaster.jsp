	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="ManufacturingMasterController">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Manufacturing Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="ManufacturingForm" ng-submit='AddAcountSubGroupSubmit(AddedAccountSubGroup,ManufacturingForm);' novalidate>
					 		<div class="row">
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="accountSubGroup">MFR Name</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="MFR Name"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="accountSubGroup" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="addressline1">Address Line1</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Address Line 1"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="addressline1" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="city">City </label>
    	        								         <input type="text" class="form-control autofocus" placeholder="City"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="city" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="PhoneNumber2">Phone Number 2</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Phone Number 2"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="PhoneNumber2" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>		
										
									<br />
																		
																		
								</div>
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="MFRShortCut">MFR ShortCut</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="MFR ShortCut"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="MFRShortCut" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="addressline2">Address Line 2</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Address Line 2"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="addressline2" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : ManufacturingForm.accountSubGroup.$invalid && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="phno">Phone Number </label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Phone Number"  maxlength="25"  name="accountSubGroup" data-ng-model='AddedAccountSubGroup.accountSubGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="2" ng-blur="spacebtw('accountSubGroup');validateDup();" id="phno" with-floating-label>														 
				                				    </div>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.required && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Sub Group Name is required</p>
								<p ng-show="ManufacturingForm.accountSubGroup.$error.pattern  && (ManufacturingForm.accountSubGroup.$dirty || Addsubmitted)" class="help-block"> Valid Sub Group  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountSubGroup.accountSubGroup!=null">Sub Group Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
										
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"   tabindex="5" checked="checked">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"  >
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>	
									<br />
																		
																		
								</div>
								
							</div>
							
							
							<br />	
							<input type="hidden"  name="modifyFlag" data-ng-model="AddedAccountSubGroup.modifyFlag"  />				
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(ManufacturingForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Manufacuturers</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
										<form name="EditAccountSubGroupMasterFrom" novalidate>					
							        <table ng-table="AccountSubGroupMaster.tableEdit" class="table table-striped table-vmiddle">
											<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>MFR Name</span></th>
													<th><span>MFR ShortCut</span></th>
													<!--<th><span>Description</span></th>-->
													<th><span>Address Line1</span></th>
													<th><span>Address Line2</span></th>
													<th><span>City</span></th>
													<th><span>Phone Number</span></th>
													<th><span>Phone Number 2</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="Group in AccountSubGroupData"  ng-class="{ 'active': Group.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Group.$edit" ng-click="Group.$edit = true;Group.modifyFlag='yes'"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Group.$edit" ng-click="UpdateSubAccountGroup(Group,$index);Group.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Group.modifyFlag"  />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Group.$edit">{{Group.accountSubGroupCode}}</span>
					    		                <div ng-if="Group.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="Group.accountGroupCode" placeholder='Group Code' maxlength="10" name="accountSubGroupCode{{$index}}" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                      <span ng-if="!Group.$edit" ng-repeat="AccoutGroupNames in accountGroupNames">
												  	<span ng-if="Group.accountGroupCode==AccoutGroupNames.id">{{AccoutGroupNames.accountgroup}}</span>
												  </span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												 <select class="form-control1"  ng-model='Group.accountGroupCode' ng-options="accountGroupCode.id as accountGroupCode.accountgroup for accountGroupCode in accountGroupNames | orderBy:'-accountgroup':true" name="accountGroupCode{{$index}}"  >	
												 	<option value="">{{Group.accountGroupCode}}</option>
												 </select>
														</div>
														
												  </div>
							                  </td>
											  <td>
							                     <span ng-if="!Group.$edit">{{Group.accountSubGroup}}</span>
							                     <div ng-if="Group.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$invalid && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="Group.accountSubGroup" placeholder='Group Name' maxlength="25" name="accountSubGroup{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('accountSubGroup',$index);validateDuplicate(Group.accountSubGroup,$index);"/>
			<p ng-show="EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$error.required && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || submitted)" class="help-block"> SubGroup Name is required</p>
			<p ng-show="EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$error.pattern  && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || submitted)" class="help-block">Enter a valid  Sub Group Name.</p>
			<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Group.accountSubGroup!=null">SubGroup Name Already Exist.</p>													

</div>
												 </div>
							                  </td>
							                  
											  
											  <!--<td>
							                      <span ng-if="!Group.$edit">{{Group.description}}</span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												  	 <input class="form-control" type="text" ng-model="Group.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
													 </div>
												  </div>
							                  </td>-->
											  <td style="width:235px;">
							                     
												  <span ng-if="!Group.$edit">
												  		<span ng-if="Group.status=='0'">Active</span>
														<span ng-if="Group.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  data-ng-model='Group.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='Group.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>											  
							             </tr>
								         </table>	
									</form>
									</div>
									</section>						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
