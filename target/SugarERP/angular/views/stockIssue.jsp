<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="stockIssueController" ng-init="addFormField();">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Issue</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="SeedlingEstimate">
					 		
							
							<div class="row">
							<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Stock Issue Type </span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
							<input type="radio" name="permitType" checked="checked"  >
			    					    				<i class="input-helper">ThroughOut Stock Indent</i>
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				        <input type="radio" name="permitType" value="1"   >
					    								<i class="input-helper"></i>Direct Issue
							  		 				</label>						 							 
												</div>
											</div>
										</div>
							</div>
							
							
							
							<div class="row">
							<div class="col-sm-3">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Indent No." />
										</div>		
								</div>				
							</div>
							<div class="col-sm-3">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control date" placeholder="Indent Date" />
										</div>		
								</div>				
							</div>
							<div class="col-sm-3">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Issue No." />
										</div>		
								</div>				
							</div>
							<div class="col-sm-3">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control date" placeholder="Issue Date" />
										</div>		
								</div>				
							</div>
							
							
							</div>
							<br />
							<div class="row">
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Department Name" />
										</div>		
								</div>				
							</div>
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Department ID" />
										</div>		
								</div>				
							</div>
							</div>
							<br />
							<div class="row">
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Employee Name" />
										</div>		
								</div>				
							</div>
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Employee ID" />
										</div>		
								</div>				
							</div>
							</div>
							<br />
							<div class="row">
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Recieved Emp. Name" />
										</div>		
								</div>				
							</div>
							<div class="col-sm-6">
							
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="fg-line">
										<input type="text" class="form-control" placeholder="Recieved Emp. ID" />
										</div>		
								</div>				
							</div>
							</div>
							
					</form>		
							
							<br />
							
							  <div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Item Name</th>
							                <th>Manufacturer</th>
		                    				<th>Purpose</th>
											<th>UOM</th>
											<th>Item Code</th>
											<th>Packing Type</th>
											<th>Batch No.</th>
											<th>Qty ln</th>
											<th>CF</th>
											<th>Indent Qty</th>
											<th>Pending Qty</th>
											<th>Issue Qty</th>
											<th>Curnt. Stock</th>
											<th>Vendor</th>
											<th>Invoice No.</th>
											<th>GRN No.</th>
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="chemicalTreatmentData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="chemicalTreatmentData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(chemicalTreatmentData.id);" ng-if="chemicalTreatmentData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Item Name" maxlength='8' name="surety{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.surety" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" />
<p ng-show="SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Manufacturer" maxlength='10' name="contactNo{{chemicalTreatmentData.id}}" data-ng-model='chemicalTreatmentData.contactNo' data-ng-required='true' ng-minlength='10' data-ng-pattern='/^[789]\d{9,10}$/'/>
<p ng-show="SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>												
<p ng-show="SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																								
</div>												
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Purpose" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="UOM" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Item Code" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Packing Type" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Batch No." maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Qty ln" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="CF" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Request Qty" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Curnt. Stock" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (SeedlingEstimate.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											
											
											</tr>
									</tbody>
							 	</table>
							</div>			 		
						 
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">SUBMIT</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10">Reset</button>
									</div>
								</div>						 	
							 </div>
							<!----------table grid design--------->
					
											
					        
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
