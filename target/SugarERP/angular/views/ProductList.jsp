<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		<section id="content" data-ng-controller="ProductListController" ng-init="addFormField(); loadStatus();loadDepartments();loadproducts();">
		  <div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Product List</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="ProductListForm" novalidate ng-submit="ProductListSubmit(AddedProductList,ProductListForm)" >
                            <input type="hidden" ng-model="AddedProductList.modifyFlag" name="modifyFlag"  />
						 <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group " ng-class="{'has-error':ProductListForm.productGroup.$invalid && (ProductListForm.productGroup.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<select chosen class="w-100 form-control " autofocus tabindex="1"   data-ng-model='AddedProductList.productGroup' name="productGroup"  placeholder="Product Group"  ng-options="productGroup.GroupCode as productGroup.Group for productGroup in productGroups | orderBy:'-GroupCode':true" ng-change="AddedProductLists(AddedProductList.productGroup);" >
									<option value="">Added Products</option>
														
						        			            </select>
									</div>
								<p ng-show="ProductListForm.productGroup.$error.required && (ProductListForm.productGroup.$dirty || Addsubmitted)" class="help-block">Product Group is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group " ng-class="{'has-error':ProductListForm.productSubGroup.$invalid && (ProductListForm.productSubGroup.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									
										<select chosen class="w-100 form-control " tabindex="2" data-placeholder='Sub Group'   data-ng-model='AddedProductList.productSubGroup' name="productSubGroup"  placeholder="Product SubGroup"  ng-options="productSubGroup.subGroupCode as productSubGroup.subGroup for productSubGroup in productSubGroups" maxlength="20"  >
														<option value="">Sub Group</option>
						        			            </select>
									</div>
							<p ng-show="ProductListForm.productSubGroup.$error.required && (ProductListForm.productSubGroup.$dirty || Addsubmitted)" class="help-block">Product SubGroup is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"></span>
											 	<div class="fg-line">
												
												 <button type="Button"   name="get"  class="btn btn-primary btn-sm m-t-10" ng-click="getAllProductData(AddedProductList.productGroup,AddedProductList.productSubGroup)">Get details</button>	
												</div>
								 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							</div>
							
							<br/>
							
							<div class="table-responsive">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>&nbsp;&nbsp; &nbsp;&nbsp;Action</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>UOM</th>
									<th>Stock Method</th>
									<th>Status</th>
									
								</tr>
								
								<tr class="styletr" ng-repeat="productList in data "  ng-class="{ 'active': productList.$edit }" >
								   
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
						<a  href="#/store/masters/productMaster" ng-click="UpdateProductList(productList.productCode,$index);"  >																				  <img src="../icons/edit.png" style="height:30px;" ng-click="UpdateProductList(productList,$index);" />
														    
														   
														   </a>
														  
														 	
														   
														 </td>
									
									<td >
										<div class="form-group" ng-class="{'has-error':ProductListForm.productCode{{$index}}.$invalid && (ProductListForm.productCode{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Code" maxlength="15" name="productCode{{$index}}" ng-model="productList.productCode" ng-required="true"  readonly="readonly">							
										<p ng-show="ProductListForm.productCode{{$index}}.$error.required && (ProductListForm.productCode{{$index}}.$dirty || submitted)" class="help-block"> Product Code Required</p>
										
										</div>
									</td>	
									
									<td >
										<div class="form-group" ng-class="{'has-error':ProductListForm.productName{{$index}}.$invalid && (ProductListForm.productName{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Name" maxlength="25" name="productName{{$index}}" ng-model="productList.productName" ng-required="true"  readonly="readonly">							
										<p ng-show="ProductListForm.productName{{$index}}.$error.required && (ProductListForm.productName{{$index}}.$dirty || submitted)" class="help-block"> Product Name Required</p>
										
										</div>
									</td>	
									
									<td >
										<div class="form-group" ng-class="{'has-error':ProductListForm.uom{{$index}}.$invalid && (ProductListForm.uom{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="UOM" maxlength="10" name="uom{{$index}}" ng-model="productList.uom" ng-required="true"  readonly="readonly">							
										<p ng-show="ProductListForm.uom{{$index}}.$error.required && (ProductListForm.uom{{$index}}.$dirty || submitted)" class="help-block"> UOM Required</p>
										
										</div>
									</td>	
									
									<td >
										<div class="form-group" ng-class="{'has-error':ProductListForm.stockMethod{{$index}}.$invalid && (ProductListForm.stockMethod{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Stock Method" maxlength="20" name="stockMethod{{$index}}" ng-model="productList.stockMethod" ng-required="true"  readonly="readonly">							 
										<p ng-show="ProductListForm.stockMethod{{$index}}.$error.required && (ProductListForm.stockMethod{{$index}}.$dirty || submitted)" class="help-block">Stock Method Required</p>
										
										</div>
									</td>	
									
									<td >
										<div class="form-group" ng-class="{'has-error':ProductListForm.status{{$index}}.$invalid && (ProductListForm.status{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Status" maxlength="10" name="status{{$index}}" ng-model="productList.status" ng-required="true"  readonly="readonly">							
										<p ng-show="ProductListForm.status{{$index}}.$error.required && (ProductListForm.status{{$index}}.$dirty || submitted)" class="help-block"> Status Required</p>
										
										</div>
									</td>	
								</tr>
							</table>
						</div>
						
						
				<!--	<br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(ProductListForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	-->
						
						 
							 
						</form>
						
						
						<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>									 