	<script type="text/javascript">	
		$('.autofocus').focus();	
		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="WeightBridgeMaster" ng-init="loadWeightCode();getAllWeight();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Weigh Bridge Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="WeightBridgeForm" ng-submit="WeightSubmit(AddedWeight,WeightBridgeForm)"  novalidate>
    	        			<div class="row">
				                <div class="col-sm-6">
									<div class='col-sm-12'>
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper">						
			        	                <div class="fg-line">
										<label for="bridgenumber">Bridge Number</label>
            					          <input type="text" class="form-control" placeholder="Bridge Number"  ng-model="AddedWeight.bridgeId" name="bridgeId"  maxlength="5" readonly  id="bridgenumber" with-floating-label />																
			                	        </div>
										</div>						
			                    	</div>
						</div>			                    
				                  
						</div>								
						<div class="col-sm-6">
							<div class='col-sm-12'>
 								 <div class="input-group">
            				            			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : WeightBridgeForm.bridgeName.$invalid && (WeightBridgeForm.bridgeName.$dirty || Addsubmitted)}">
			        	                				<div class="fg-line">
														<label for="bridgename">Bridge Name</label>
            					   <input type="text" class="form-control autofocus" placeholder="Bridge Name"  ng-model="AddedWeight.bridgeName" name="bridgeName"  maxlength="25" data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9.-\s]*$/" tabindex="1" autofocus id="bridgename" with-floating-label  ng-blur="spacebtw('bridgeName');validateDup();" />
												

			                	        				</div>
<p ng-show="WeightBridgeForm.bridgeName.$error.required && (WeightBridgeForm.bridgeName.$dirty || Addsubmitted)" class="help-block">Bridge Name is required</p>
<p ng-show="WeightBridgeForm.bridgeName.$error.pattern && (WeightBridgeForm.bridgeName.$dirty || Addsubmitted)" class="help-block">Enter Valid Bridge Name</p>
<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedWeight.bridgeName!=null">Bridge Group Name Already Exist.</p>	
										</div>
			                    				</div>		
							</div>						                  	                    
				     		</div>





	<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"   data-ng-model='AddedWeight.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"   data-ng-model='AddedWeight.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
    	        			 </div><br />  
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedWeight.modifyFlag"  />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" tabindex="5" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(WeightBridgeForm);">Reset</button>
									</div>
								</div>


						 	
							 </div>							 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->
					</form>		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Bridge</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">
							   <div class="table-responsive">							
								 <section class="asdok">
									<div class="container1">
									<form name="editWeightBridgeForm" novalidate>					
							        <table ng-table="WeightBridgeMaster.tableEdit" class="table table-striped table-vmiddle">
									
													<thead>
											        <tr>
											           <th><span>Action</span></th>
													   <th><span>Bridge Number</span></th>
													   <th><span>Bridge Name</span></th>
													  	<th><span>Status</span></th>														
											        </tr>
											      </thead>
											      <tbody>	
									
										
								        <tr ng-repeat="Weight in WeightData"  ng-class="{ 'active': Weight.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Weight.$edit" ng-click="Weight.$edit = true;Weight.modifyFlag='Yes'"><i class="zmdi zmdi-edit"></i></button>
					            		      <button type="submit" class="btn btn-success btn-hideg" ng-if="Weight.$edit" data-ng-click="updateWeight(Weight,$index); Weight.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											  <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Weight.modifyFlag"  />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Weight.$edit">{{Weight.bridgeId}}</span>
					    		                <div ng-if="Weight.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="Weight. bridgeId" placeholder='Bridge Number' maxlength="5" name=" bridgeId{{$index}}"   readonly="readonly"/>
													</div>
												</div>
					            		      </td>
		                    				    <td>
							                     <span ng-if="!Weight.$edit">{{Weight.bridgeName}}</span>
							                     <div ng-if="Weight.$edit"> 
														<div class="form-group" ng-class="{ 'has-error' : editWeightBridgeForm.bridgeName{{$index}}.$invalid && (editWeightBridgeForm.bridgeName{{$index}}.$dirty || submitted)}"> 												
<input class="form-control" type="text" ng-model="Weight.bridgeName" placeholder='Bridge Name' maxlength="25" name="bridgeName{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9-.\s]*$/"  ng-blur="spacebtwgrid('bridgeName',$index);validateDuplicate(Weight.bridgeName,$index);"/>  
		<p ng-show="editWeightBridgeForm.bridgeName{{$index}}.$error.required && (editWeightBridgeForm.bridgeName{{$index}}.$dirty || submitted)" class="help-block">Required</p>
		<p ng-show="editWeightBridgeForm.bridgeName{{$index}}.$error.pattern  && (editWeightBridgeForm.bridgeName{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
		 <p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Weight.bridgeName!=null">Bridge Name Already Exist.</p>													

													</div>
												</div>  											  
							   </td>

 										<td>
							                      <span ng-if="!Weight.$edit">
										<span ng-if="Weight.status=='0'">Active</span>
										<span ng-if="Weight.status=='1'">Inactive</span>
															</span>
        		            					  		<div ng-if="Weight.$edit">
												  			<div class="form-group">
												 		<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" data-ng-model='Weight.status'>
			    					    				<i class="input-helper"></i>Active
									 					</label>
				 			 							<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='Weight.status'>
			    										<i class="input-helper"></i>Inactive
							  		 					</label>				
														</div>		 							 												  
												  </div>
							                 	 </td>	
											  </tr>
											  </tbody>
								         </table>
									</form>							 
							    	</div>
								</section>
								</div>
							</div>
						</div>

					<!----------end----------------------->
        				
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
