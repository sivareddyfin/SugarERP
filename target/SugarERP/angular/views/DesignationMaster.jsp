	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="DesignationMaster" data-ng-init='loadDesignationCode();loadAllDesginations();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Designation Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="DesignationMasterForm" ng-submit='AddDesignations(AddDesignation,DesignationMasterForm);' novalidate>
						 <input type="hidden" name="screenName" ng-model="AddDesignation.screenName" />
					 		<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												  <div class="form-group floating-label-wrapper">
					        	               			 <div class="fg-line">
														 <label for="designationcode">Designation Code</label>
        		    					          <input type="text" class="form-control" placeholder="Designation Code"  maxlength="10" readonly name="designationCode" data-ng-model='AddDesignation.designationCode' id="designationcode" with-floating-label />
			    		            	        	</div>
												</div>
			                    			</div>
										</div>
									</div><br />
																		
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
													<label for="desc">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description(Optional)"  maxlength="50" tabindex="4" name="description" data-ng-model='AddDesignation.description' id="desc" with-floating-label  ng-blur="spacebtw('description');" />
			                	        			</div>
													</div>
			                    				</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DesignationMasterForm.designation.$invalid && (DesignationMasterForm.designation.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="desi">Designation Name</label>
    	        								         <input type="text" class="form-control" placeholder="Designation Name"  maxlength="25" tabindex="1" name="designation" data-ng-model='AddDesignation.designation' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" id="desi" with-floating-label ng-blur="spacebtw('designation');validateDup();" />														 
				                				    </div>
								<p ng-show="DesignationMasterForm.designation.$error.required && (DesignationMasterForm.designation.$dirty || Addsubmitted)" class="help-block">Designation is required.</p>
								<p ng-show="DesignationMasterForm.designation.$error.pattern  && (DesignationMasterForm.designation.$dirty || Addsubmitted)" class="help-block">Enter a valid Designation.</p>			
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddDesignation.designation!=null">Designation Name Already Exist.</p>										
												</div>
					                    	</div>
										</div>
									</div>								
																											
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0" checked="checked" tabindex="5"  data-ng-model='AddDesignation.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1" tabindex="6"  data-ng-model='AddDesignation.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>																											
								</div>
							</div><br />		
							<input type="hidden"  name="modifyFlag" data-ng-model="AddDesignation.modifyFlag"  />			
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(DesignationMasterForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Designations</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">	
								<form name="EditDesignationMasterFrom" novalidate>					
							        <table ng-table="DesignationMaster.tableEdit" class="table table-striped table-vmiddle">
									
									
													<thead>
											        <tr>
											           <th><span>Action</span></th>
													   <th><span>Designation Code</span></th>
													    <th><span>Designation Name</span></th>
													   <th><span>Description</span></th>
													   <th><span>Status</span></th>
											        </tr>
											      </thead>
											      <tbody>
										
								        <tr ng-repeat="UpdateDesignations in AddedDesignations"  ng-class="{ 'active': UpdateDesignations.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateDesignations.$edit" ng-click="UpdateDesignations.$edit = true;UpdateDesignations.modifyFlag='Yes';UpdateDesignations.screenName='Designation Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="UpdateDesignations.$edit" ng-click="UpdateAddedDesignations(UpdateDesignations,$index);UpdateDesignations.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="UpdateDesignations.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="UpdateDesignations.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateDesignations.$edit">{{UpdateDesignations.designationCode}}</span>
					    		                <div ng-if="UpdateDesignations.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="UpdateDesignations.designationCode" placeholder='Designation Code' maxlength="10" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!UpdateDesignations.$edit">{{UpdateDesignations.designation}}</span>
							                     <div ng-if="UpdateDesignations.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditDesignationMasterFrom.designation{{$index}}.$invalid && (EditDesignationMasterFrom.designation{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="UpdateDesignations.designation" placeholder='Designation Name' maxlength="25" name="designation{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('designation',$index);validateDuplicate(UpdateDesignations.designation,$index);"/>
						<p ng-show="EditDesignationMasterFrom.designation{{$index}}.$error.required && (EditDesignationMasterFrom.designation{{$index}}.$dirty || submitted)" class="help-block">Required</p>
					<p ng-show="EditDesignationMasterFrom.designation{{$index}}.$error.pattern  && (EditDesignationMasterFrom.designation{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>		
					<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="UpdateDesignations.designation!=null">Designation Name Already Exist.</p>											

</div>
												 </div>
							                  </td>
							                  
											  
											  <td>
							                      <span ng-if="!UpdateDesignations.$edit">{{UpdateDesignations.description}}</span>
        		            					  <div ng-if="UpdateDesignations.$edit">
												  		<div class="form-group">
												  	 <input class="form-control" type="text" ng-model="UpdateDesignations.description" placeholder='Description' maxlength="50" name="description{{$index}}"  ng-blur="spacebtwgrid('description',$index)" />
													 </div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!UpdateDesignations.$edit">												  
												  <span ng-if="UpdateDesignations.status=='0'">Active</span>
													<span ng-if="UpdateDesignations.status=='1'">Inactive</span>
												  
												  </span>
        		            					  <div ng-if="UpdateDesignations.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  data-ng-model='UpdateDesignations.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='UpdateDesignations.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>		
													</div>				 							 												  
												  </div>
							                  </td>											  
							             	</tr>
										</tbody>
								     </table>	
								</form>
							</div>
						</section>						 
					</div>
				</div>
			</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
