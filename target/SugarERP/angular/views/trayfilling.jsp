<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
<style>
.styletr {border-bottom: 1px solid grey;}
#styletr:nth-child(odd) {
    background: #E9E9E9;
}

#styletr:nth-child(even) {
    background: #FFFFFF;
}

</style>	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 $('.Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="trayfillingController" ng-init="addFormField();addFormFieldBud();loadVarietyNames();loadVillageNames();loadseasonNames();ShiftDropDownLoad();loadTrayType();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Tray Filling</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="trayfillingform" novalidate  ng-submit="SaveTrayFilling(AddedTrayFilling,trayfillingform);">
							  <input type="hidden" ng-model="AddedTrayFilling.modifyFlag" />
							  <div class="row">
								<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : trayfillingform.season.$invalid && (trayfillingform.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100"  tabindex="1"  data-ng-model='AddedTrayFilling.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" data-ng-required="true" ng-change ="getTrayfillingList(AddedTrayFilling.season,AddedTrayFilling.date,AddedTrayFilling.shift);">
															<option value="">Season</option>
						        			            </select>	
				        		        	        </div>
													<p ng-show="trayfillingform.season.$error.required && (trayfillingform.season.$dirty || submitted)" class="help-block">Select Season</p>													
												</div>
					                    	</div>
											
								</div>
								<div class="col-sm-4">
											      <div class="input-group">
												       <a href="#/seedling/transaction/trayfilling" ng-click="loadBatchDetailsPopup();">Batch Details</a>
												  </div>
								</div>
								</div>
								<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : trayfillingform.date.$invalid && (trayfillingform.date.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date ' id="date" with-floating-label  name="date" ng-model="AddedTrayFilling.date" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}"  maxlength="12" data-ng-required="true" ng-blur="getTrayfillingList(AddedTrayFilling.season,AddedTrayFilling.date,AddedTrayFilling.shift);clearBatchgrid(trayfillingform);"/>	
				        		        	        </div>
													<p ng-show="trayfillingform.date.$error.required && (trayfillingform.date.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : trayfillingform.shift.$invalid && (trayfillingform.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift"   ng-model="AddedTrayFilling.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames |  orderBy:'-shiftName':true" ng-change ="getTrayfillingList(AddedTrayFilling.season,AddedTrayFilling.date,AddedTrayFilling.shift);">
															<option value="">Shift</option>
															
														</select>
				                			        </div>
											<p ng-show="trayfillingform.shift.$error.required && (trayfillingform.shift.$dirty || submitted)" class="help-block">Select Shift</p>													
												</div>
					                    	</div>
										</div>
								</div>
								<!--<div class="col-sm-6">
			                    <div class="input-group">
            			            
			                       
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Tray Filling</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0"   data-ng-model="AddedTrayFilling.trayfilling">
			    			            	<i class="input-helper"></i>Sugarcane
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="AddedTrayFilling.trayfilling">
			    				            <i class="input-helper"></i>Vegetables
					  		              </label>
										   <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="2"  data-ng-model="AddedTrayFilling.trayfilling">
			    				            <i class="input-helper"></i>Fruits
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>-->
							
							  </div>
							  <div class="row">
							 
										
									
							  	<div class="col-sm-4">
									<div class="row">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">&nbsp; &nbsp; &nbsp;Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourSpplr" value="0" ng-model="AddedTrayFilling.labourSpplr" ng-change="hideTrayFields(AddedTrayFilling.labourSpplr);" ng-click="getLabourContractorDetails();">
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedTrayFilling.labourSpplr" ng-change="hideTrayFields(AddedTrayFilling.labourSpplr);" ng-click="getLabourContractorDetails();">
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
								</div>
									<div class="col-sm-4"  style="display:none;">
									<div class="row">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Machine or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="machine" value="0" ng-model="AddedTrayFilling.machineOrMen"  >
				    				            	  <i class="input-helper"></i>Machine
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="machine" value="1"  ng-model="AddedTrayFilling.machineOrMen">
			    						            <i class="input-helper"></i>Manual
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
								</div>
								<div class="col-sm-4">
								<div class="row">
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
														<select chosen class="w-100" name='lc'  ng-model="AddedTrayFilling.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							</div>
								</div>
							  </div>
							  
							
							  
							
							 
							 
							  			<div id='hidelabourcharges'  ng-if="AddedTrayFilling.machineOrMen=='1'">				
									  <div class="row">
											<div class="col-sm-12">
												<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
											</div>
										</div>
								
									  <div class="row">
										 
											<div class="col-sm-3">
											<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group floating-label-wrapper">
														 <div class="fg-line">
																<h5 style="font-weight:normal;">Men</h5>
														</div>
														</div>
														</div>
											</div>
											<div class="col-sm-3">
											<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : trayfillingform.noOfMen.$invalid && (trayfillingform.noOfMen.$dirty || submitted)}">
														 <div class="fg-line">
														 <label for="noOfMen">No. of Men</label>
																<input type="text" class="form-control autofocus" placeholder="No. of People" name="noOfMen" data-ng-model="AddedTrayFilling.noOfMen" ng-keyup="getMenTotal(AddedTrayFilling.noOfMen,AddedTrayFilling.manCost);getLabourGrandTotal(AddedTrayFilling.menTotal,AddedTrayFilling.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfMen" with-floating-label tabindex="1"  />
														</div>
														<p ng-show="trayfillingform.noOfMen.$error.pattern && (trayfillingform.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>
														
														
														</div>
														</div>
											</div>
											<div class="col-sm-3">
											<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : trayfillingform.manCost.$invalid && (trayfillingform.manCost.$dirty || submitted)}">
														 <div class="fg-line">
														  <label for="manCost">Cost</label>
																<input type="text" class="form-control" placeholder="Cost" name="manCost" data-ng-model="AddedTrayFilling.manCost" ng-keyup="getMenTotal(AddedTrayFilling.noOfMen,AddedTrayFilling.manCost);getLabourGrandTotal(AddedTrayFilling.menTotal,AddedTrayFilling.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="manCost" with-floating-label  tabindex="2"  />
														</div>
														<p ng-show="trayfillingform.manCost.$error.pattern && (trayfillingform.manCost.$dirty || submitted)" class="help-block">Invalid</p>
														
														</div>
														</div>
											</div>
											<div class="col-sm-3">
											<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group floating-label-wrapper">
														 <div class="fg-line">
														   <label for="menTotal">Total</label>
																<input type="text" class="form-control" placeholder="Total" name="menTotal" data-ng-model="AddedTrayFilling.menTotal" readonly  id="menTotal" with-floating-label tabindex="3" />
														</div>
														</div>
														</div>
											</div>
										
										
										</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : trayfillingform.noOfWomen.$invalid && (trayfillingform.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfWomen">No. of People</label>
														<input type="text" class="form-control" name="noOfWomen" placeholder="No. of People" name="noOfWomen" data-ng-model="AddedTrayFilling.noOfWomen" ng-keyup="getWomenTotal(AddedTrayFilling.noOfWomen,AddedTrayFilling.womenCost); getLabourGrandTotal(AddedTrayFilling.menTotal,AddedTrayFilling.womenTotal);" data-ng-pattern="/^[0-9\s]*$/" id="noOfWomen" with-floating-label  tabindex="4" />
												</div>
												
								<p ng-show="trayfillingform.noOfWomen.$error.pattern && (trayfillingform.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : trayfillingform.womenCost.$invalid && (trayfillingform.womenCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="womenCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="womenCost" data-ng-model="AddedTrayFilling.womenCost" ng-keyup="getWomenTotal(AddedTrayFilling.noOfWomen,AddedTrayFilling.womenCost);getLabourGrandTotal(AddedTrayFilling.menTotal,AddedTrayFilling.womenTotal);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="womenCost" with-floating-label  tabindex="5"  />
												</div>
												<p ng-show="trayfillingform.womenCost.$error.pattern && (trayfillingform.womenCost.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="womenTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" data-ng-model="AddedTrayFilling.womenTotal" readonly  id="womenTotal" with-floating-label  tabindex="6"/>
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								
								</div>
								<div  ng-show="AddedTrayFilling.machineOrMen=='0' ">
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Machines</u></h4>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">No. of Machines</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-9">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												   <label for="noOfMachines">No. of Machines</label>
														<input type="text" class="form-control" placeholder="No. of Machines" name="noOfMachines" data-ng-model="AddedTrayFilling.noOfMachines" id="noOfMachines" with-floating-label />
												</div>
												</div>
												</div>
									</div>
							   </div>
									
								</div>
								
								<div  ng-show="AddedTrayFilling.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : trayfillingform.costPerTon.$invalid && (trayfillingform.costPerTon.$dirty || submitted)}">
							<div class="fg-line">
							<label for="costPerTon">Cost Per Ton</label>
							<input type="text" placeholder="Cost Per Ton" ng-model="AddedTrayFilling.costPerTon" class="form-control" name="costPerTon" ng-keyup="getContractTotal(AddedTrayFilling.costPerTon,AddedTrayFilling.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="costPerTon" with-floating-label  />
							</div>
							
						<p ng-show="trayfillingform.costPerTon.$error.pattern && (trayfillingform.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
	
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : trayfillingform.totalTons.$invalid && (trayfillingform.totalTons.$dirty || submitted)}">
												
							<div class="fg-line">
							<label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons" ng-model="AddedTrayFilling.totalTons" class="form-control" name="totalTons" ng-keyup="getContractTotal(AddedTrayFilling.costPerTon,AddedTrayFilling.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/"  id="totalTons" with-floating-label />
							</div>
							<p ng-show="trayfillingform.totalTons.$error.pattern && (trayfillingform.totalTons.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
									<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="fg-line">
									<label for="totalCost">Total Cost</label>
									<input type="text" placeholder="Total Cost" ng-model="AddedTrayFilling.totalCost" class="form-control"  id="totalCost" with-floating-label/>
									</div>
									</div>
									</div>
								</div>
								
								</div>
								
								
									<div class="row" id="totalLabourCost"  ng-show="AddedTrayFilling.machineOrMen=='1'" >
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalLabourCost">Grand  Labour Total</label>
														<input type="text" class="form-control" placeholder="Grand  Labour Total" ng-model="AddedTrayFilling.totalLabourCost" tabindex="12" readonly id="totalLabourCost" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
									
								</div>
									<div class="row" id="totalContractAmt" style="display:none;">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="totalContractAmt">Grand  Contract Total</label>
														<input type="text" class="form-control" placeholder="Grand  Contract Total" ng-model="AddedTrayFilling.totalContractAmt" tabindex="12" readonly id="totalContractAmt" with-floating-label />
												</div>
												</div>
												</div>
									</div>
									
								</div>				 					 	
							 		
									
									<p style="font-size:18px;">Material for media</p>
									<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Actions</th>
											<th>Item</th>
							                <th>Weight</th>
		                    				
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="trayfillingdata in data">
										
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="trayfillingdata.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(trayfillingdata.id);" ng-if="trayfillingdata.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : trayfillingform.item{{trayfillingdata.id}}.$invalid && (trayfillingform.item{{trayfillingdata.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Item" maxlength='8' name="item{{trayfillingdata.id}}" ng-model="trayfillingdata.item"  data-ng-pattern="/^[a-z A-Z\s]*$/" />
<p ng-show="trayfillingform.item{{trayfillingdata.id}}.$error.required && (trayfillingform.item{{trayfillingdata.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="trayfillingform.item{{trayfillingdata.id}}.$error.pattern && (trayfillingform.item{{trayfillingdata.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : trayfillingform.weight{{trayfillingdata.id}}.$invalid && (trayfillingform.weight{{trayfillingdata.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="number" class="form-control1" placeholder="Weight" maxlength='10' name="weight{{trayfillingdata.id}}" data-ng-model='trayfillingdata.weight' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/"  />
<p ng-show="trayfillingform.weight{{trayfillingdata.id}}.$error.required && (trayfillingform.weight{{trayfillingdata.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>
<p ng-show="trayfillingform.weight{{trayfillingdata.id}}.$error.pattern && (trayfillingform.weight{{trayfillingdata.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>		

</div>												
											</td>  
											
											
											
											</tr>
									</tbody>
							 	</table>
							</div>	
									<p style="font-size:18px;">Seedling Details</p>	
									<div class="table-responsive">
									<table style="width:100%;"  style="border-collapse:collapse;" border="0">
											<thead>
												<tr class="styletr" style="font-weight:bold;">
													<td>Action</td>
													<td>Batch Series</td>
													<td>Batch </td>
													
													
													<!--<th>Village</th>-->
													<td>Variety</td>
													<td>Start Time</td>
													<td>End Time</td>
													<td>Total Time</td>
													
													<td>Tray Type</td>
													
														
															<td>No. of trays</td>
															<td>No. of Cavities</td>
															<td>Seedling Type</td>
															<td>Total No. of Seedlings</td>

													
													
												</tr>											
											</thead>
											<tbody>
												<tr  id="styletr"  ng-repeat="BudData in datagrid" class="styletr"  style="height:50px;">
													<td>
														<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormFieldBud();" ng-if="BudData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
														<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRowBud(BudData.id);" ng-if="BudData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
													</td>
													
													<td style="display:none;">
													<input type="hidden" ng-model="BudData.id" name="batchid{{BudData.id}}" />
													</td>
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.batchSeries{{BudData.id}}.$invalid && (trayfillingform.batchSeries{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
													<input list="stateList" placeholder="Batch Series" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{BudData.id}}" placeholder ="Batch No" ng-model="BudData.batchSeries" ng-change="loadBatchDetails(BudData.batchSeries,BudData.id);"  style="height:40px; margin-top:10px;"> 
											<datalist id="stateList">
											<select class="form-control1">
										 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
										</select>    
											</datalist>
		
		<p ng-show="trayfillingform.batchSeries{{BudData.id}}.$error.required && (trayfillingform.batchSeries{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.batchSeries{{BudData.id}}.$error.pattern && (trayfillingform.batchSeries{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
												
		</div>	
		
		
								</td>
								<td>
					<div class="form-group" ng-class="{ 'has-error' : trayfillingform.batchNo{{BudData.id}}.$invalid && (trayfillingform.batchNo{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
					<input  type="text" placeholder="Batch No"   class="form-control1"  name="batchNo{{BudData.id}}" placeholder ="Batch No" ng-model="BudData.batchNo"  style="height:40px; margin-top:10px;"> 
					<p ng-show="trayfillingform.batchNo{{BudData.id}}.$error.required && (trayfillingform.batchNo{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
					<p ng-show="trayfillingform.batchNo{{BudData.id}}.$error.pattern && (trayfillingform.batchNo{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
															
					</div>	
		
		
								</td>
								
													
												<td style='display:none;'>
								
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.variety{{BudData.id}}.$invalid && (trayfillingform.variety{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
		<input type='text' class="form-control1" name="variety{{BudData.id}}" placeholder="Variety" data-ng-model='BudData.variety' name="variety" readonly  style="height:40px; margin-top:10px;">
															
		<p ng-show="trayfillingform.variety{{BudData.id}}.$error.required && (trayfillingform.variety{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.variety{{BudData.id}}.$error.pattern && (trayfillingform.variety{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		</div>												
													</td> 
													
													<td>
								
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.varietyName{{BudData.id}}.$invalid && (trayfillingform.varietyName{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
		<input type='text' class="form-control1" name="varietyName{{BudData.id}}" placeholder="Variety" data-ng-model='BudData.varietyName' name="varietyName" readonly  style="height:40px; margin-top:10px;">
															
		<p ng-show="trayfillingform.varietyName{{BudData.id}}.$error.required && (trayfillingform.varietyName{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.varietyName{{BudData.id}}.$error.pattern && (trayfillingform.varietyName{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		</div>												
													</td> 
													
													
													
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.fromTime{{BudData.id}}.$invalid && (trayfillingform.fromTime{{BudData.id}}.$dirty || Addsubmitted)}">										
													<input type="text" class="form-control1 time" placeholder="Start Time" maxlength='11' name="fromTime{{BudData.id}}" ng-model='BudData.startTime' ng-mouseover="displayTime(BudData.id,'from');" id="from{{BudData.id}}" ng-focus="displayTime(BudData.id,'from');" ng-change="CalculateShiftHrs();"/  style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.fromTime{{BudData.id}}.$error.required && (trayfillingform.fromTime{{BudData.id}}.$dirty || submitted)" class="help-block">Required</p>
		<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{BudData.id}}">Invalid</p>											
		</div>											
												</td>  
												<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.toTime{{BudData.id}}.$invalid && (trayfillingform.toTime{{BudData.id}}.$dirty || Addsubmitted)}">										
													<input type="text" class="form-control1 time" placeholder="End Time" maxlength='11' name="toTime{{BudData.id}}" ng-model='BudData.endTime' ng-mouseover="displayTime(BudData.id,'to');"   id="to{{BudData.id}}" ng-focus="displayTime(BudData.id,'to');" ng-change="CalculateShiftHrs();"  style="height:40px; margin-top:10px;"/>
		<p ng-show="trayfillingform.toTime{{BudData.id}}.$error.required && (trayfillingform.toTime{{BudData.id}}.$dirty || submitted)" class="help-block">Required</p>	
		<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{BudData.id}}">Invalid</p>										
		</div>											
												</td>
												<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.shiftHrs{{BudData.id}}.$invalid && (trayfillingform.toTime{{BudData.id}}.$dirty || Addsubmitted)}">										
													<input type="text" class="form-control1"  placeholder="Total Time" maxlength='10' name="shiftHrs{{BudData.id}}"  readonly   ng-value='BudData.fromTime' ng-model='BudData.totalTime' id="hrs{{ShiftData.shiftId}}"/  style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.shiftHrs{{BudData.id}}.$error.required && (BudData.shiftHrs{{BudData.id}}.$dirty || submitted)" class="help-block">Required</p>											
		</div>										
			
												</td>
													
													<td>
													<div class="form-group" ng-class="{ 'has-error' : trayfillingform.trayType{{BudData.id}}.$invalid && (trayfillingform.trayType{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
														<select class="form-control1" name="trayType{{BudData.id}}" data-ng-model='BudData.trayType' name="trayType" ng-options="trayType.id as trayType.TrayType for trayType in TrayTypeNames | orderBy:'-variety':true"  style="height:40px; margin-top:10px;">
																	<option value="">Tray Type</option>
																</select>
		<p ng-show="trayfillingform.trayType{{BudData.id}}.$error.required && (trayfillingform.trayType{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.trayType{{BudData.id}}.$error.pattern && (trayfillingform.trayType{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																						
		</div>
													</td>
													
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.noofTrays{{BudData.id}}.$invalid && (trayfillingform.noofTrays{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
														<input type="text" class="form-control1" placeholder="Total trays" maxlength='14' name="noofTrays{{BudData.id}}" ng-model="BudData.totalTrays" data-ng-required='true'  data-ng-pattern="/^[0-9\s]*$/" ng-keyup="updateTotalSeedlings(BudData.totalTrays,BudData.noOfCavitiesPerTray,BudData.id);"/  style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.noofTrays{{BudData.id}}.$error.required && (trayfillingform.noofTrays{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.noofTrays{{BudData.id}}.$error.pattern && (trayfillingform.noofTrays{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																					
		</div>												
													</td>
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.noOfCavity{{BudData.id}}.$invalid && (trayfillingform.noOfCavity{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
														<input type="text" class="form-control1" placeholder="No. of Cavities" maxlength='14' name="noOfCavity{{BudData.id}}" ng-model="BudData.noOfCavitiesPerTray" data-ng-required='true'  data-ng-pattern="/^[0-9\s]*$/" ng-keyup="updateTotalSeedlings(BudData.totalTrays,BudData.noOfCavitiesPerTray,BudData.id);" /   style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.noOfCavity{{BudData.id}}.$error.required && (trayfillingform.noOfCavity{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.noOfCavity{{BudData.id}}.$error.pattern && (trayfillingform.noOfCavity{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																					
		</div>												
													</td>
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.seedlingType{{BudData.id}}.$invalid && (trayfillingform.seedlingType{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
														<input type="text" class="form-control1" placeholder="Seedling Type" maxlength='14' name="seedlingType{{BudData.id}}" ng-model="BudData.seedlingType" data-ng-required='true' readonly/  style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.seedlingType{{BudData.id}}.$error.required && (trayfillingform.seedlingType{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.seedlingType{{BudData.id}}.$error.pattern && (trayfillingform.seedlingType{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																					
		</div>												
													</td>
													<td>
		<div class="form-group" ng-class="{ 'has-error' : trayfillingform.noofSeedlings{{BudData.id}}.$invalid && (trayfillingform.noofSeedlings{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
														<input type="text" class="form-control1" placeholder="Total Seedlings" maxlength='14' name="noofSeedlings{{BudData.id}}" ng-model="BudData.totalNoOfSeedlingss"    data-ng-pattern="/^[0-9\s]*$/" readonly/  style="height:40px; margin-top:10px;">
		<p ng-show="trayfillingform.noofSeedlings{{BudData.id}}.$error.required && (trayfillingform.noofSeedlings{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="trayfillingform.noofSeedlings{{BudData.id}}.$error.pattern && (trayfillingform.noofSeedlings{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		
		</div>												
													</td>
													
													
													</tr>
													
													<tr>
													
														<div ng-show="AddedTrayFilling.trayfilling=='0'"><td  colspan="11" ></td><td align="right"><br /><input type="text" class="form-control1" style="width:100px;" ng-model="AddedTrayFilling.totalNoOfSeedlings" name="totalNoOfSeedlings" placeholder="Total" readonly /></td></div>
														
														
													</tr>
											</tbody>
										</table>
									</div>
							
								
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(trayfillingform)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>


								<br>
								<!------------Batch Details------------->
								
								<form name="BatchDeyailsForm" novalidate style="display:;">
									   <div class="card popupbox_ratoon" id="gridpopup_box">						  
																	
											<div class="row">
											  <div class="col-md-12">		
												  <div class="table-responsive">
													<table class="table table-striped table-vmiddle" id="tab_logic">
														<thead>
															<tr style="background-color:#FFFFFF;">
															    <th>SNo.</th>	
																<th>Batch</th>
																<th>Variety</th>
																<th>Total Seedlings</th>
													
														
																<th>Delivered</th>
															
																<th>Pending</th>
																		
																
															</tr>											
														</thead>
														<tbody>
														    <tr ng-repeat="batchdata in batchdetailsData">
															   <td>{{$index+1}}</td>
															   <td>{{batchdata.batch}}</td>
															   <td>{{batchdata.variety}}</td>
															   <td>{{batchdata.totalSeedlings}}</td>
															   <td>{{batchdata.delivered}}</td>
															   <td>{{batchdata.pending}}</td>
															</tr>
															<!--<tr ng-repeat="gridData in IndentNumberData" >
															
																<td>
																  <div class="checkbox">
				<label class="checkbox checkbox-inline m-r-20">
				<input type="checkbox" class="checkbox" name="hrvFlag"  ng-model="gridData.hrvFlag" ng-click="optionToggled(gridData.hrvFlag,gridData.IndentNo,gridData.kind,gridData.quantity,gridData.variety,gridData.totalCost,gridData.costOfEachItem)"><i class="input-helper"></i>
				</label>
				</div>
																</td>
																<td>
																   <input type="text"  class="form-control1" name="IndentNo{{$index}}" ng-model="gridData.IndentNo"  readonly="readonly" />
																</td>
																<td>
																 <input type="text"  class="form-control1" name="dateofindent{{$index}}" ng-model="gridData.dateofindent" readonly="readonly"  />
																</td>
																<td>
															   <input type="text"  class="form-control1"  name="dateofdeliver{{$index}}" ng-model="gridData.dateofdeliver"  data-input-mask="{mask: '00-00-0000'}" readonly />
																</td>
																<td>
																
															<select class="form-control1" name="kind{{$index}}" data-ng-model='gridData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
																		<option value="" > Kind Type</option>	
																		</select>
															   
																</td>
															   

																
																	 <td>
																  <select class="form-control1" name="variety{{$index}}" ng-model="gridData.variety"  ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true" readonly="readonly">
																	<option value="">Variety</option>
																</select>
																</td>


																<td colspan="2">
																  <input type="text"  class="form-control1"  name="quantity{{$index}}" ng-model="gridData.quantity" readonly/>
																</td>
															   
																
															</tr>-->
														</tbody>	
												 </table>
											</div>	  
										</div>
										<div class="col-md-1"></div>
										</div>
											 <div class="row" align="center">            			
												<div class="input-group">
													<div class="fg-line">
														
														<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogBatch();">Close Dialog</button>
													</div>
												</div>						 	
											 </div>							 							 
										</div>
									</div>
									   </div>				
									 </form>			 				
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
