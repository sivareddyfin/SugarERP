<script type="text/javascript">	
		$('.autofocus').focus();
	</script>
	
	
<script type="text/javascript">
  $(function()
  {
         $( ".date" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
     
  });
  
  </script>	

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		<section id="content" data-ng-controller="authorisationStatusReportController" ng-init="">
		  <div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Authorisation Status Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>		
					 
				<form name="authorisationStatusReportForm" novalidate ng-submit="authorisationStatusReportSubmit(AddedauthorisationStatusReport,authorisationStatusReportForm)" >
				
				     <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group  floating label" ng-class="{'has-error':authorisationStatusReportForm.from.$invalid && (authorisationStatusReportForm.from.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label> 
									  <input type="text" class="form-control   date" autofocus placeholder="From Date" name="from" ng-model="AddedauthorisationStatusReport.from" ng-required="" tabindex="1"  id="fromDate" with-floating-label />
									</div>
						</div>
						       
							    </div>
								</div>
							</div>
							</div>	
							
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group  floating label" ng-class="{'has-error':authorisationStatusReportForm.to.$invalid && (authorisationStatusReportForm.to.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<label for="toDate">To Date</label> 
									  <input type="text" class="form-control   date" placeholder="To Date" name="to" ng-model="AddedauthorisationStatusReport.to" ng-required="" tabindex="2"  id="toDate" with-floating-label />
									</div>
						</div>
						       
							    </div>
								</div>
							</div>
							</div>	
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group ">												
									<div class="fg-line">
									<button type="button"  class="btn btn-primary btn-sm m-t-10" name="getDetails" ng-click="loadData(AddedauthorisationStatusReport);">Get Details </button>
									</div>
						</div>
						       
							    </div>
								</div>
							</div>
							</div>		
					     </div>
						
						<hr />
						<br />
						
						<div class="table-responsive">
							<table class="table table-striped"  style="border-collapse:collapse;width:100%;">
								<tr  style="font-weight:bold;">
								   
								<th>INDENT NO</th>
								<th>DATE</th>
								<th>RYOT CODE</th>
								<th>RYOT NAME</th>
								<th>VILLAGE</th>
								<th>AGREEMENT</th>
								<th>EXTEND P/R</th>
								<th>KIND</th>
								<th>QTY</th>
								<th>COST</th>
								<th>AF DOC NO</th>
								<th>DATE</th>
								<th>VALUE</th>
								<th>STORE LOCATION</th>
								
						
						 </tr>
						 
						 <tr   ng-repeat="authorisationStatusdata in authorisationStatusReport"  >
						 <td>
						       <div class="form-group">
									   {{authorisationStatusdata.indentno}}  
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.indentdate}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.ryotCode}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.ryotName}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.village}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.agrNo}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.extent}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.Kind}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									  {{authorisationStatusdata.qty}}    
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.cost}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.authorisationSeqNo}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.authorisationDate}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.total}} 
									</div>
						 </td>
						 
						 <td>
						       <div class="form-group">
									     {{authorisationStatusdata.storeCity}} 
									</div>
						 </td>
						 </tr>
						 </table>
					</div>	 
					</form>	 
					 </div>
				</div>
		  </div>
		</section>
  </section>>		  			 