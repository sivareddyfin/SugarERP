	<script type="text/javascript">		
		$('#subCategory').focus();	

	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="AdvanceSubCategory" ng-init="loadAdvanceSubCategoryCode();loadCategory();loadAllAddedSubCategories();loadCompanyAdvances();">      
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Advance Sub-Category Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
					 			<form name="AdvanceSubCategoryForm" ng-submit="advanceSubCategorysubmit(AddedAdvanceSubCategory,AdvanceSubCategoryForm)"  novalidate>
							 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : AdvanceSubCategoryForm.advanceCode.$invalid  && (AdvanceSubCategoryForm.advanceCode.$dirty || Addsubmitted)}">
							<div class="fg-line">

							<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedAdvanceSubCategory.advanceCode' name="advanceCode" ng-options="advanceCode.id as advanceCode.advance for advanceCode in CompanyAdvanceNames | orderBy:'-advance':true">
								<option value="">Select Advances</option>
						    </select>
								
								
								<!--<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedAdvanceSubCategory.advanceCode' name="advanceCode" ng-options="advanceCode.id as advanceCode.advanceCode for advanceCode in Advances">
															<option value="">Select Advances</option>
						        			            </select>--->
							</div>
        		            <p ng-show="AdvanceSubCategoryForm.advanceCode.$error.required  && (AdvanceSubCategoryForm.advanceCode.$dirty ||  Addsubmitted)" class="help-block">Select Advance</p>
		                </div>

			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	
										<div class="form-group" ng-class="{ 'has-error' : AdvanceSubCategoryForm.name.$invalid && (AdvanceSubCategoryForm.name.$dirty || Addsubmitted)}">
										<div class="fg-line">
										<label for="subCategory">Sub Category</label>
											<input type="text" class="form-control" placeholder="Sub Category" maxlength="25"  name="name"  data-ng-model="AddedAdvanceSubCategory.name" tabindex="3"  ng-required='true' ng-blur="spacebtw('name');validateDup();"  id="subCategory" with-floating-label> 
			                        	</div>
										
										<p ng-show="AdvanceSubCategoryForm.name.$error.required && (AdvanceSubCategoryForm.name.$dirty || Addsubmitted)" class="help-block"> Sub Category is required</p>
										<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAdvanceSubCategory.name!=null">Sub Category Already Exist.</p>
										</div>
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									
			                        	<div class="fg-line">
										<label for="description">Description</label>
											<input type="text" class="form-control" placeholder="Description" maxlength="50"   name="description"  data-ng-model="AddedAdvanceSubCategory.description" ng-blur="spacebtw('description');"  tabindex="5" id="description" with-floating-label > 
			                        	
										
										<!--<p ng-show="AdvanceCategoryForm.advanceCategoryCode.$error.required && (AdvanceCategoryForm.advanceCategoryCode.$dirty || submitted)" class="help-block"> Valid Field Assistant Name is required.</p>-->
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>



							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : AdvanceSubCategoryForm.advanceCategoryCode.$invalid  && (AdvanceSubCategoryForm.advanceCategoryCode.$dirty || Addsubmitted)}">
							<div class="fg-line">
							<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedAdvanceSubCategory.advanceCategoryCode' name="advanceCategoryCode" ng-options="advanceCategoryCode.id as advanceCategoryCode.name for advanceCategoryCode in Category | orderBy:'-name':true" ng-change="getcategoryCode(AddedAdvanceSubCategory.advanceCategoryCode);">
															<option value="">Select Category</option>
						        			            </select>
							</div>
        		            <p ng-show="AdvanceSubCategoryForm.advanceCategoryCode.$error.required  && (AdvanceSubCategoryForm.advanceCategoryCode.$dirty ||  Addsubmitted)" class="help-block">Select Category</p>
		                </div>

			                
			                    </div>			                    
			                </div>
							
							</div>
							<div class="row">
								<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	<div class="fg-line">
											<label for="subCategoryCode">SubCategory Code</label>
											<input type="text" class="form-control" placeholder="SubCategory Code" maxlength="25"   name="subCategoryCode"  data-ng-model="AddedAdvanceSubCategory.subCategoryCode" tabindex="4"  readonly="readonly" id="subCategoryCode" with-floating-label > 
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedAdvanceSubCategory.status" ng-init="AddedAdvanceSubCategory.status='Active'"   >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"   data-ng-model='AddedAdvanceSubCategory.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
									
			                        	</div>
										
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>
							
							</div>
							
							 </div>
							 
							 <br />
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedAdvanceSubCategory.modifyFlag"  />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide" tabindex="6">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(AdvanceSubCategoryForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Sub Categories</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
										<form name="AdvanceSubCategoryEdit" novalidate>					
											<table ng-table="AdvanceSubCategory.tableEdit" class="table table-striped table-vmiddle" >
											<thead>
																	<tr>
																		<th><span>Action</span></th>
																		<th><span>Advance</span></th>
																		<th><span>Category</span></th>
																		<th><span>Sub Category</span></th>
																		<th><span>Sub Category Code </span></th>
																		<th><span>Description</span></th>
																		<th><span>Status</span></th>
																		</tr>
																</thead>
																<tbody>
																
												
												<tr ng-repeat="AdvSubCategory in AdvanceSubCategoryData"  ng-class="{ 'active': AdvSubCategory.$edit }">
													<td>
													   <button type="button" class="btn btn-default" ng-if="!AdvSubCategory.$edit" ng-click="AdvSubCategory.$edit = true;AdvSubCategory.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
													   <button type="submit" class="btn btn-success btn-hideg" ng-if="AdvSubCategory.$edit" ng-click="advanceSubCategoryUpdate(AdvSubCategory,$index);AdvSubCategory.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
													   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AdvSubCategory.modifyFlag"  />
													 </td>
													
													  <td>
														  <span ng-if="!AdvSubCategory.$edit" ng-repeat='AdvanceNames in CompanyAdvanceNames'>
															<span ng-if='AdvSubCategory.advanceCode==AdvanceNames.id'>{{AdvanceNames.advance}}</span>
														  </span> 
														  <div class="form-group" ng-class="{ 'has-error' : AdvanceSubCategoryEdit.advanceCode.$invalid && (AdvanceSubCategoryEdit.advanceCode.$dirty || submitted)}">
														  <div ng-if="AdvSubCategory.$edit">
														  
					<select class="form-control1" id="input_fieldofficer"  ng-model="AdvSubCategory.advanceCode" name="advanceCode{{$index}}" ng-options="advanceCode.id as advanceCode.advance for advanceCode in CompanyAdvanceNames | orderBy:'-advance':true">  
						<option value="">{{AdvSubCategory.advanceCode}}</option>
					</select>
													
															<p ng-show="AdvanceSubCategoryEdit.advanceCode.$error.required && (AdvanceSubCategoryEdit.advanceCode.$dirty || submitted)" class="help-block"> Select Advance Name.</p>
													</div>
														  </div>
													  </td>
													  <td>
														  <span ng-if="!AdvSubCategory.$edit" ng-repeat="CategoryName in Category">
															<span ng-if="AdvSubCategory.advanceCategoryCode==CategoryName.id">{{CategoryName.name}}</span>
														  </span> 
														  <div class="form-group" ng-class="{ 'has-error' : AdvanceSubCategoryEdit.advanceCategoryCode.$invalid && (AdvanceSubCategoryEdit.advanceCategoryCode.$dirty || submitted)}">
														  <div ng-if="AdvSubCategory.$edit">
														  
															<select class="form-control1" id="input_fieldofficer"  ng-model="AdvSubCategory.advanceCategoryCode" name="advanceCategoryCode{{$index}}" ng-options="advanceCategoryCode.id as advanceCategoryCode.name for advanceCategoryCode in Category | orderBy:'-name':true"> 
																<option value="">{{AdvSubCategory.advanceCategoryCode}}</option>
															</select>
													
															<p ng-show="AdvanceSubCategoryEdit.advanceCategoryCode.$error.required && (AdvanceSubCategoryEdit.advanceCategoryCode.$dirty || submitted)" class="help-block"> Select Advance Name.</p>
													</div>
														  </div>
													  </td>
													  
													  <td>
														   <span ng-if="!AdvSubCategory.$edit">{{AdvSubCategory.name}}</span>
		
														   <div ng-if="AdvSubCategory.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="AdvSubCategory.name" placeholder='SubCategory' maxlength="50" name="name{{$index}}" ng-blur="spacebtwgrid('name',$index);validateDuplicate(AdvSubCategory.name,$index);"/>  
																	<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AdvSubCategory.name!=null">SubCategory Name Already Exist.</p>
																</div>
														   </div>
													  </td>
													  <td data-title="'SubCategory Code'">
														   <span ng-if="!AdvSubCategory.$edit">{{AdvSubCategory.subCategoryCode}}</span>
		
														   <div ng-if="AdvSubCategory.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="AdvSubCategory.subCategoryCode" placeholder='subCategoryCode' maxlength="50" name="subCategoryCode{{$index}}" readonly="readonly"/>  
																</div>
														   </div>
													  </td>
													  <td>
														   <span ng-if="!AdvSubCategory.$edit">{{AdvSubCategory.description}}</span>
		
														   <div ng-if="AdvSubCategory.$edit">
															<div class="form-group">
																<input class="form-control" type="text" ng-model="AdvSubCategory.description" placeholder='Description' maxlength="50" name="description{{$index}}"  ng-blur="spacebtwgrid('description',$index)"/>  
															</div>
														   </div>
													  </td>
													  
													  <td>
														  <span ng-if="!AdvSubCategory.$edit">
															<span ng-if="AdvSubCategory.status=='0'">Active</span>
															<span ng-if="AdvSubCategory.status=='1'">Inactive</span>
														  </span>
														  <div ng-if="AdvSubCategory.$edit">
															<div class="form-group">
															<label class="radio radio-inline m-r-20">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model="AdvSubCategory.status">
																<i class="input-helper"></i>Active
															</label>
															<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="AdvSubCategory.status">
																<i class="input-helper"></i>Inactive
															</label>				
															</div>		 							 												  
														  </div>
													  </td>
													  
													  <!--<!--<td data-title="'Status'">
														   <span ng-if="!FieldAssistant.$edit">{{FieldAssistant.status}}</span>
														   <div ng-if="FieldAssistant.$edit">
																<input class="form-control" type="text" ng-model="FieldAssistant.status" placeholder='Status' maxlength="50"/>
														   </div> 
													  </td>	-->				    		              										  			
												 </tr>
												 </tbody>
												 </table>
												 </form>							 
							     </div>
								</section>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
