 
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RyotExtentDetails" ng-init='loadRyotCodeDropDown();addExtentField();loadVarietyNames();'>      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> Extent Details for Soil Test</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					    <form name="RyotExtentDetails" novalidate ng-submit='AddExtentDetails(AddedExtent,RyotExtentDetails);'>
					 		<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : RyotExtentDetails.ryotcode.$invalid && (RyotExtentDetails.ryotcode.$dirty || submitted)}">										
			        	                <div class="fg-line">
										  	<select chosen class="w-100" name="ryotcode" ng-model='ryotcode' ng-options="ryotcode.id as ryotcode.id for ryotcode in ryotNames | orderBy:'-id':true" ng-required='true' ng-change="getRyotDetails(ryotcode);getRyotUpdate(ryotcode)">
												<option value="">Select Ryot Code</option>
											</select>
			                	        </div>
<p ng-show="RyotExtentDetails.ryotcode.$error.required && (RyotExtentDetails.ryotcode.$dirty || submitted)" class="help-block">Select Ryot Code</p>										
										</div>										
			                    	</div>			                    
				                  </div>
    	        			 </div><br />
							<div class="row">
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper">
				        	                <div class="fg-line">
												<label for="RyotName">Ryot Name</label>
												<input type="text" class="form-control" placeholder='Ryot Name' readonly maxlength="50" name="ryotName" ng-model='AddedExtent.ryotName' with-floating-label id="RyotName"/>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper">
				        	                <div class="fg-line">
												<label for="Village">Village</label>
												<input type="text" class="form-control" placeholder='Village' readonly maxlength="25" name="villageName" ng-model='AddedExtent.villageName' with-floating-label id="Village"/>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div> 
								  <!--<input type="hidden" name="ryotCode" ng-model='AddedExtent.ryotCode' /> -->
								  <input type="hidden" name="villageCode" ng-model='AddedExtent.villageCode' /> 
								  <input type="hidden" name="screenName" ng-model="AddedExtent.screenName" />
    	        			 </div><br /><hr /> 
							 <div  class="table-responsive">	 
						        <table class="table table-striped table-vmiddle">
						          <thead>
							         <tr>
										<th>Action</th>
    	            		    		<th>Plot Sl No</th>										
								        <th>Variety</th>
										<th>Extent Size (Acres)</th>
										<th>Survey No.</th>
            						</tr>	
						          </thead>
						          <tbody>
							        <tr ng-repeat='ExtentData in data'>
										<td>
										   <button type="button" id="right_All_1" class="btn btn-primary" ng-if="ExtentData.id=='1'" ng-click='addExtentField();'><i class="glyphicon glyphicon-plus-sign"></i></button>
										   <button type="button" id="right_All_1" class="btn btn-primary" ng-if="ExtentData.id!='1'" ng-click='removeExtent(ExtentData.id);'><i class="glyphicon glyphicon-remove-sign"></i></button>										   
										</td>  
										<td>
											<div class="form-group">
											<input type="text" class="form-control1" placeholder="Plot Sl No" maxlength="4" readonly name="plotSlNumber" ng-model='ExtentData.plotSlNumber'  ng-disabled="AddedExtent.soilTestStatus=='1'" />
											</div>
										</td>  												
										<td>
<div class="form-group" ng-class="{ 'has-error' : RyotExtentDetails.VarietyCode{{ExtentData.id}}.$invalid && (RyotExtentDetails.VarietyCode{{ExtentData.id}}.$dirty || submitted)}">																				
											<select  class="form-control1" name="VarietyCode{{ExtentData.id}}" ng-model='ExtentData.varietyCode' ng-required='true' ng-options="VarietyCode.id as VarietyCode.variety for VarietyCode in varietyNames | orderBy:'-variety':true">
												<option value="">Select Variety</option>												
											</select>
<p ng-show="RyotExtentDetails.VarietyCode{{ExtentData.id}}.$error.required && (RyotExtentDetails.VarietyCode{{ExtentData.id}}.$dirty || submitted)" class="help-block">Variety required.</p>																					
</div>											
										</td>  
										<td>
<div class="form-group" ng-class="{ 'has-error' : RyotExtentDetails.extentSize{{ExtentData.id}}.$invalid && (RyotExtentDetails.extentSize{{ExtentData.id}}.$dirty || submitted)}">																														
											<input type="text" class="form-control1" placeholder="Extent Size" maxlength="10" name="extentSize{{ExtentData.id}}" ng-model='ExtentData.extentSize' ng-required='true' data-ng-pattern="/^[0-9.\s]*$/" ng-disabled="AddedExtent.soilTestStatus=='1'">
<p ng-show="RyotExtentDetails.extentSize{{ExtentData.id}}.$error.required && (RyotExtentDetails.extentSize{{ExtentData.id}}.$dirty || submitted)" class="help-block">Extent Size required.</p>
<p ng-show="RyotExtentDetails.extentSize{{ExtentData.id}}.$error.pattern && (RyotExtentDetails.extentSize{{ExtentData.id}}.$dirty || submitted)" class="help-block">Invalid Extent Size.</p>											
</div>											
											
										</td>  
										<td>
<div class="form-group" ng-class="{ 'has-error' : RyotExtentDetails.surveyNo{{ExtentData.id}}.$invalid && (RyotExtentDetails.surveyNo{{ExtentData.id}}.$dirty || submitted)}">																														
											<input type="text" class="form-control1"  placeholder="Survey No" maxlength="10" name="surveyNo{{ExtentData.id}}" ng-model='ExtentData.surveyNo' ng-required='true' ng-disabled="AddedExtent.soilTestStatus=='1'"/>
<p ng-show="RyotExtentDetails.surveyNo{{ExtentData.id}}.$error.required && (RyotExtentDetails.surveyNo{{ExtentData.id}}.$dirty || submitted)" class="help-block">Survey No. required.</p>											
</div>											
										</td>
									</tr>           
						          </tbody>
					            </table>
    						</div>

							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-disabled="AddedExtent.soilTestStatus=='1'">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(RyotExtentDetails);">Reset</button>
									</div>
								</div>						 	
							 </div>					
					 </form>	 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>					
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
