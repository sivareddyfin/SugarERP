
<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
<section id="main" class='bannerok'>  
    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>

    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

	


    <!--<section id="content"> 
        <div class="container" data-ng-controller="tableCtrl as tctrl">
		    <div class="banner">
      <div class="container">
        <div class="banner-contentarea">
          <div class="banner-content text-center text-white">
            <h1>Welcome Message</h1>
          </div>
        </div>
      </div>
    </div>
        </div>
    </section>-->
		
	
					
		<section id="content">      
        	<div class="container" data-ng-controller="validation">				
    			<div class="block-header"><h2><b>Bank Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">					
					
					
				
				<form name="userForm" >

                <!-- NAME -->
				<div class="row">
				<div class="col-md-6">
	                <div class="form-group" ng-class="{ 'has-error' : userForm.name.$invalid && (userForm.name.$dirty || submitted)}">
						<div class="fg-line">
	    	                <input type="text" name="name" class="form-control" data-ng-model="user.name" placeholder="Your Name" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]*$/">
						</div>
    	        	        <p ng-show="userForm.name.$error.required && (userForm.name.$dirty || submitted)" class="help-block">You name is required.</p>
							<p ng-show="userForm.name.$error.pattern  && (userForm.name.$dirty || submitted)" class="help-block">Enter a valid Name.</p>					
	                </div>
				</div>
				<div class="col-md-6">
                	<!-- USERNAME -->
	                <div class="form-group" ng-class="{ 'has-error' : userForm.username.$invalid && (userForm.username.$dirty || submitted)}">
						<div class="fg-line">
        		            <input type="text" name="username" class="form-control" data-ng-model="user.username" placeholder="Your Username" ng-minlength="3" ng-maxlength="8" data-ng-required="true">
						</div>
                    	<p ng-show="userForm.username.$error.required && (userForm.username.$dirty || submitted)" class="help-block">You username is required.</p>
	                    <p ng-show="userForm.username.$error.minlength && (userForm.username.$dirty || submitted)" class="help-block">Username is too short.</p>
    	                <p ng-show="userForm.username.$error.maxlength && (userForm.username.$dirty || submitted)" class="help-block">Username is too long.</p>					
	                </div>
				</div>
				</div>
				<div class="row">
					<div class="col-md-6">
		                <!-- PASSWORD -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.password.$invalid && (userForm.password.$dirty || submitted)}">
							<div class="fg-line">
        		    	        <input type="Password" name="password" class="form-control" data-ng-model="user.password" placeholder="Your Password" data-ng-required="true">
							</div>
		                    <p ng-show="userForm.password.$error.required && (userForm.password.$dirty || submitted)" class="help-block">Your password is required.</p>					
		                </div>
					</div>
					<div class="col-md-6">
		                <!-- CONFIRM PASSWORD -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.confirmPassword.$invalid && (userForm.confirmPassword.$dirty || submitted)}">
							<div class="fg-line">
            			        <input type="Password" name="confirmPassword" class="form-control" data-ng-model="user.confirmPassword" placeholder="Confirm Your Password" data-ng-compare="password" ng-required="true">
							</div>
        		            <p ng-show="userForm.confirmPassword.$error.required && (userForm.confirmPassword.$dirty || submitted)" class="help-block">Your confirm password is required.</p>
                		    <p ng-show="userForm.confirmPassword.$error.compare  && (userForm.confirmPassword.$dirty || submitted)" class="help-block">Confirm password doesnot match.</p>
		                </div>	
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
		                <!-- EMAIL -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.email.$invalid && (userForm.email.$dirty || submitted)}">
							<div class="fg-line">
            			        <input type="email" name="email" class="form-control" data-ng-model="user.email" placeholder="Your Email Address" data-ng-required="true">
							</div>
        		            <p ng-show="userForm.email.$error.required && (userForm.email.$dirty || submitted)" class="help-block">Email is required.</p>
                		    <p ng-show="userForm.email.$error.email && (userForm.email.$dirty || submitted)" class="help-block">Enter a valid email.</p>
		                </div>
					</div>
					<div class="col-md-6">
		                <!-- CONTACTNO -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.contactno.$invalid  && (userForm.contactno.$dirty || submitted) }">
						<div class="fg-line">
        		            <input type="text" name="contactno" class="form-control" data-ng-model="user.contactno" placeholder="Your Contact No" data-ng-pattern="/^[789]\d{9}$/" maxlength="10" data-ng-required="true">
						</div>
						<p ng-show="userForm.contactno.$error.required && (userForm.contactno.$dirty || submitted)" class="help-block">contactno is required.</p>	
	                    <p ng-show="userForm.contactno.$error.pattern  && (userForm.contactno.$dirty || submitted)" class="help-block">Enter a valid contactno.</p>
	                </div>
				</div>
				<div class="row">
					<div class="col-md-6">
		                <!-- COUNTRY -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.country.$invalid  && (userForm.country.$dirty || submitted)}">
							<div class="fg-line">
                    			<select chosen  class="w-100"
            		                data-ng-model="user.country"
		                            data-ng-options="country.CountryId as country.Name for country in countryList"
        		                    data-ng-required="true">
                			        <option value=''>Select</option>
			                    </select>
							</div>
        		            <p ng-show="userForm.country.$error.required  && (userForm.country.$dirty || submitted)" class="help-block">Select country.</p>
		                </div>
					</div>
					<div class="col-md-6">				
		                <!-- CITY -->
        		        <div class="form-group" ng-class="{ 'has-error' : userForm.city.$invalid  && (userForm.city.$dirty || submitted)}">
						<div class="fg-line">
        		            <select chosen name="city" class="w-100"
                	            data-ng-model="user.city"
                    	        data-ng-options="city.CityId as city.Name for city in cityList"
                        	    data-ng-required="true">
		                        <option value=''>Select</option>
        		            </select>
						</div>
    	                <p ng-show="userForm.city.$error.required  && (userForm.city.$dirty || submitted)" class="help-block">Select city.</p>
	                </div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-12">
                <!-- TERMS & CONDITIONS -->
                <div class="form-group" ng-class="{ 'has-error' : userForm.terms.$invalid && (userForm.terms.$dirty || submitted)}">
                    <label>Accept Terms & Conditions</label>
                    <input type="checkbox" value="" name="terms" data-ng-model="user.terms" data-ng-required="true" id='chk' my-focus/>
                    <p ng-show="userForm.terms.$error.required && (userForm.terms.$dirty || submitted)" class="help-block">Accept terms & conditions.</p>
                </div>
Selected Value is: {{user.city}}	
</div>
</div>
                <!-- ng-disabled FOR ENABLING AND DISABLING SUBMIT BUTTON -->
                <!--<button type="submit" class="btn btn-primary" ng-disabled="userForm.$invalid">Register</button>-->
                <button type="submit" class="btn btn-primary" data-ng-click="submitForm(user)">Register</button>
            </form>
</div></div></div>		
		
	</section>
</section>

<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
