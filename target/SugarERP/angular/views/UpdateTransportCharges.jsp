	
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
			 
	     });		 		 		 
	  });
    </script>	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateTransporterCharges" ng-init="loadTransportSeason();loadTransportCircle();loadTransportContractors();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Transport Charges</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					    <form name="TransportChargesForm" ng-submit="transportFilterSubmit(transportFilter,TransportChargesForm);" novalidate>
					 		<div class="row">
				                <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : TransportChargesForm.season.$invalid && (TransportChargesForm.season.$dirty || filterSubmitted)}">
				        	                <div class="fg-line">
												<select chosen class="w-100" name="season" ng-model="transportFilter.season" ng-options="season.season as season.season for season in getTransportSeasons | orderBy:'-season':true" ng-required="true">
													<option value="">Select Season</option>
					        		            </select>
			    	            	        </div>
											<p ng-show="TransportChargesForm.season.$error.required && (TransportChargesForm.season.$dirty || filterSubmitted)" class="help-block">Season is Required.</p>
										</div>
			                    	</div>			                    
				                  </div>
								  
								  <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : TransportChargesForm.circleCode.$invalid && (TransportChargesForm.circleCode.$dirty || filterSubmitted)}">
			       	 	                	<div class="fg-line">
												<select chosen class="w-100" name="circleCode" ng-model="transportFilter.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in getTransportCircles | orderBy:'-circle':true" ng-required="true">
													<option value="">Select Circle</option>
					        		            </select>
				                	        </div>
											<p ng-show="TransportChargesForm.circleCode.$error.required && (TransportChargesForm.circleCode.$dirty || filterSubmitted)" class="help-block">Circle is Required.</p>
										</div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-2">
				                    <div class="input-group">
            				            
			        	                <div class="fg-line">
												<button type="submit" class="btn btn-primary btn-sm m-t-10">Get Details</button>
			                	        </div>
			                    	</div>			                    
				                  </div>
    	        			 </div>
						  </form>							 													 
						  <hr />	
						  <form name="TransporterChargesSubmit" ng-submit="SaveTransportingRates(TransporterChargesSubmit);" novalidate>						 
						 	 <div class="row">
				            	 <div class="col-sm-4">
					               <div class="input-group">
    	        				     <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									 <div class="form-group" ng-class="{ 'has-error' : TransporterChargesSubmit.effectiveDate.$invalid && (TransporterChargesSubmit.effectiveDate.$dirty || Submitted)}">
					        	         <div class="fg-line">
											<input type="text" class="form-control input_date" placeholder="From Date" name="effectiveDate" ng-model="inputData.effectiveDate" ng-required="true"/>
				    	            	 </div>
										 <p ng-show="TransporterChargesSubmit.effectiveDate.$error.required && (TransporterChargesSubmit.effectiveDate.$dirty || Submitted)" class="help-block">Season is Required.</p>
									  </div>
			        	           </div>			                    
				        	    </div>
							  </div>
							  <hr />							
							  <div  class="table-responsive">
							     <section class="asdok"> 
									<div class="container1">							  
										 <table ng-table="tctrl.tableEdit" class="table table-striped">
										 	<thead>
												<tr>
													<th><span>Ryot Code</span></th>
													<th><span>Ryot Name</span></th>
													<th><span>Transporting Contr.</span></th>
													<th><span>Sl. No.</span></th>
													<th><span>Rate per Ton</span></th>
												</tr>
											</thead>					
											<tbody>
												<tr ng-repeat="TransporteData in UpdateTransporterData">
													<td>
														{{TransporteData.ryotCode}}
														<input type="hidden" name="ryotCode" ng-model="TransporteData.ryotCode" />
													</td>
													<td>
														{{TransporteData.ryotName}}
														<input type="hidden" name="ryotName" ng-model="TransporteData.ryotName" />
													</td>
													<td>
														<div class="form-group" ng-class="{ 'has-error' : TransporterChargesSubmit.transportContractorCode.$invalid && (TransporterChargesSubmit.transportContractorCode.$dirty || Submitted)}">
															<select class="form-control1" name="transportContractorCode" ng-model="TransporteData.transportContractorCode" ng-options="transportContractorCode.id as transportContractorCode.transporter for transportContractorCode in getTransporterNames | orderBy:'-transporter':true">
																<option value="">Transporting Contr.</option>
															</select>
															<p ng-show="TransporterChargesSubmit.transportContractorCode.$error.required && (TransporterChargesSubmit.transportContractorCode.$dirty || Submitted)" class="help-block">Season is Required.</p>
														</div>
													</td>
													<td>
														<div class="form-group" ng-class="{ 'has-error' : TransporterChargesSubmit.slNo.$invalid && (TransporterChargesSubmit.slNo.$dirty || Submitted)}">
															<input type="text" class="form-control1" placeholder='Sl.No' maxlength="10" name="slNo" ng-model="TransporteData.slNo" />
															<p ng-show="TransporterChargesSubmit.slNo.$error.required && (TransporterChargesSubmit.slNo.$dirty || Submitted)" class="help-block">Season is Required.</p>
														</div>
													</td>
													<td>
														<div class="form-group" ng-class="{ 'has-error' : TransporterChargesSubmit.rate.$invalid && (TransporterChargesSubmit.rate.$dirty || Submitted)}">
															<input type="text" class="form-control1" placeholder='Rate' maxlength="10" name="rate" ng-model="TransporteData.rate" />
															<p ng-show="TransporterChargesSubmit.rate.$error.required && (TransporterChargesSubmit.rate.$dirty || Submitted)" class="help-block">Season is Required.</p>
														</div>
													</td>
												</tr>										
											</tbody>													        
										 </table>
									  </div>									
								</section> 	
								</div>														 
							    <div class="row" align="center">
									<div class="col-sm-12">
						               <div class="input-group">            				            
			        			          <div class="fg-line">
											<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Update</button>
											<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(TransportChargesForm)">Reset</button>
			                	          </div>
					                    </div>			                    
						            </div>
								</div>						
						  </form> 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
