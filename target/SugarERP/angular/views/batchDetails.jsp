<style>
.styletr 
  {
    border-bottom: 1px solid grey;
  }
  
 td
 {
    padding-top:20px;
 } 
 td,th{
  text-align:center;
  font-weight: 5px ;
  } 
  #pop
  {
    padding-top:3px;
  }
  
input[type=text] {
   
    height:40px;
}

#divpop{
   
 height:40px;
 width:40px;
}


</style>
<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="batchDetailsController" ng-init="loadDetails();">      
        	<div class="container">	
						
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Batch Details </b></h2></div>
				 
																	
			     <div class ="card">
			        <div class ="card-body card-padding" >
			       
					 <div id="divpop">			 	
					 	
						<form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">
					   				<div class="row">
										<div class="col-sm-4">
											<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="productCodepopup">Product Code</label>
														
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="productCode" data-ng-model="productCode" placeholder="Product Code" tabindex="1"  id="productCodepopup" with-floating-label readonly  />
													</div>
													
													
													
															  </div>
														  </div>
										
										</div>
										<div class="col-sm-4"><input type="hidden" ng-model="productId" name="productId{{$index}}" /></div>
										<div class="col-sm-4"><input type="hidden"  ng-model="popupId"/></div>
									</div>						  
						  	  					
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th> &nbsp;&nbsp;Select</th>
												<th>Batch ID</th>	
												<th>Batch</th>
												<th>Expiry Date</th>
												<th>Cost Price</th>
												<th>MRP</th>
												<th>Stock</th>
												<th>Mfr. Date</th>
												
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="batchDetails in productBatchData">
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label class="checkbox checkbox-inline m-r-20" style="margin-top:-10px;" >
												<input type="checkbox"  class="checkbox" name="batchFlag{{$index}}"  ng-model="batchDetails.batchFlag" ng-click="batchDatapush(batchDetails.batchFlag,batchDetails.batch,batchDetails.expiryDate,batchDetails.mrp,$index,batchDetails.batchSeqNo,batchDetails.costPrice,batchDetails.batchId);"    ><i class="input-helper" ></i></label></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.batchId" name="batchId{{$index}}" /></td>
											<td><input type="text" class="form-control1"  readonly  ng-model="batchDetails.batch" name="batch{{$index}}"/></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.expiryDate" name="expiryDate{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.costPrice" name="costPrice{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mrp" name="mrp{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.stock" name="stock{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mfrDate" name="mfrDate{{$index}}" /></td>
											
											</tr>
											
											
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 	
							 
							 <div class="row">
					   	<div class="col-sm-4">
							<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="newbatch">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="newabatch" data-ng-model="newBatch.batch" placeholder="Batch" tabindex="1"  id="newbatch" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>

						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="expdt">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="expdt" data-ng-model="newBatch.expdt" placeholder="Expiry Date" tabindex="1"  id="expdt" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="MRP">MRP</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="MRP" data-ng-model="newBatch.mrp" placeholder="MRP" tabindex="1"  id="MRP" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
					   
					   </div>
					   <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseBatchPopup(newBatch);">Load</button>
									</div>
								</div>						 	
							 </div>						 							 
				        </div>
			    	</div>
					   </div>
					   <br />
					   
					   				
					 </form>
						
						
					 	</div>
					 	
					 </div>	
					 	  
				</div>	
				</div>      
		</section>   	 	
			        
			        
			        
			        
			        