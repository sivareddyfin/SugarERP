	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="purchaseTaxTransfer" ng-init="purchaseTaxOption();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Purchase Tax Transfer</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>		
					 	<form name="purchaseTaxTransferForm" novalidate ng-submit="PurchaseTaxSubmit(AddedPurchaseTax,purchaseTaxTransferForm);">
							
							
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
		            				            <span class="input-group-addon">Purchase Tax Status</span>
					        	                <div class="fg-line" style="margin-top:5px;">
            							          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="inlineRadioOptions" value="0" checked="checked" ng-model="AddedPurchaseTax.pTax">
			    			    		        	<i class="input-helper"></i>ICP
								        		  </label>
						 			              <label class="radio radio-inline m-r-20" style="margin-left:-10px;">
						            			    <input type="radio" name="inlineRadioOptions" value="1" ng-model="AddedPurchaseTax.pTax">
			    						            <i class="input-helper"></i>Government
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
									</div>
								</div>
								
						
							
							
					
						</div>
							
							
							
							
													
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Transfer</button>
										<!--<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(purchaseTaxTransferForm)">Reset</button>-->
									</div>
								</div>						 	
							 </div>				
						</form>			 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!---------Table Design Start--------->
					<!--<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Seedling Trays</b></h2></div>-->
					<!--<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
								  	<form name="UpdateSeedlingTrays" novalidate>					
							        <table ng-table="SeedlingTrayMaster.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Tray Name</span></th>
													<th><span>Tray Type</span></th>
													<th><span>Life of Tray</span></th>
													<th><span>Seedlings Per Tray</span></th>
													<th><span>Description</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
										
								        <tr ng-repeat="UpdateTrays in AddedSeedlingTraysData"  ng-class="{ 'active': UpdateTrays.$edit }">
                		    				<td>
											
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateTrays.$edit" ng-click="UpdateTrays.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="UpdateTrays.$edit" ng-click="UpdateAddedSeedlingTrays(UpdateTrays);UpdateTrays.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateTrays.$edit">{{UpdateTrays.trayName}}</span>
					    		                <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.trayName.$invalid && (UpdateSeedlingTrays.trayName.$dirty || submitted)}">																																																
													<input class="form-control1" type="text" ng-model="UpdateTrays.trayName" maxlength="10" placeholder='Tray Name' name="trayName" data-ng-required='true'  ng-blur="spacebtwgrid('trayName',$index)"/>
<p ng-show="UpdateSeedlingTrays.trayName.$error.required && (UpdateSeedlingTrays.trayName.$dirty || submitted)" class="help-block">is required</p>													
</div>													
												</div>
					            		      </td>
							                  <td>
							                      <span ng-if="!UpdateTrays.$edit">
												  	<span ng-if="UpdateTrays.trayType=='0'">Disposable </span>	  	
													<span ng-if="UpdateTrays.trayType=='1'">Non-Disposable </span>	  	
												  </span>
        		            					  <div ng-if="UpdateTrays.$edit">
												  	<div class="form-group">
													
													 <select class="form-control1" name="trayType" data-ng-model='UpdateTrays.trayType'>
													 	<option value="0" ng-selected="UpdateTrays.trayType=='0'">Disposable</option>
														<option value="1" ng-selected="UpdateTrays.trayType=='1'">Non-Disposable</option>
													 </select> 	
													 </div>
												  </div>
							                  </td>
							                  <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.lifeOfTray}}</span>
					            		           <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.lifeOfTray{{$index}}.$invalid && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType=='0'" ng-init="UpdateTrays.lifeOfTray==''">												   													  	
													  <input type="text" class="form-control1" placeholder="Life of Tray(Times)" ng-readonly="UpdateTrays.trayType=='0'" maxlength="2" data-ng-model="UpdateTrays.lifeOfTray" name="lifeOfTray{{$index}}" >
													  </div>													 
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.lifeOfTray{{$index}}.$invalid && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType!='0'">												   													  	
													  <input type="text" class="form-control1" placeholder="Life of Tray(Times)"  maxlength="2" data-ng-model="UpdateTrays.lifeOfTray" name="lifeOfTray{{$index}}" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.lifeOfTray{{$index}}.$error.required && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.lifeOfTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>  													
													  
</div>													  

													</div>
							                  </td>
					    		              <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.noOfSeedlingPerTray}}</span>
					                    		   <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$invalid && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType=='0'">												   
													  <input type="text" class="form-control1" placeholder="No.of Seedlings per tray"  maxlength="2" ng-readonly="UpdateTrays.trayType=='1'" name="noOfSeedlingPerTray{{$index}}" data-ng-model="UpdateTrays.noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.required && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
</div>
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$invalid && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType!='0'">												   
													  <input type="text" class="form-control1" placeholder="No.of Seedlings per tray"  maxlength="2" name="noOfSeedlingPerTray{{$index}}" data-ng-model="UpdateTrays.noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.required && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>  
													  
</div>													  
												    </div>
							                  </td>
											  <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.description}}</span>
					                    		   <div ng-if="UpdateTrays.$edit">
												   	<div class="form-group">
													  <input type="text" class="form-control1" placeholder="Description" maxlength="50" name="description{{$index}}" data-ng-model="UpdateTrays.description"  ng-blur="spacebtwgrid('description',$index)">
													  </div>
												    </div>
							                  </td>
											  <td>
                    							   <span ng-if="!UpdateTrays.$edit">
												   	  <span ng-if="UpdateTrays.status=='0'">Active</span>
													  <span ng-if="UpdateTrays.status=='1'">Inactive</span>
												   </span>
					                    		   <div ng-if="UpdateTrays.$edit">
												   	<div class="form-group">
													   <select class="form-control1" name="status{{$index}}" data-ng-model='UpdateTrays.status'>
													 		<option value="0" ng-selected="UpdateTrays.status=='0'">Active</option>
															<option value="1" ng-selected="UpdateTrays.status=='1'">Inactive</option>
													   </select>
													   </div>
												    </div>
							                  </td>														  			
							             </tr>
										 </tbody>
								         </table>		
									 </form>
								</div>
							</section>					 
							     </div>
							</div>
						</div>-->
					<!---------Table Design End----------->
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
