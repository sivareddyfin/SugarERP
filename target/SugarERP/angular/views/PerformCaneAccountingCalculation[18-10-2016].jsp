	<style type="text/css">
		.table 
		{
		  width: 100%;
		  max-width: 100%;
		  margin-bottom: 18px;
		}

		.table > thead > tr > th,
		.table > tbody > tr > th,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > tbody > tr > td,
		.table > tfoot > tr > td 
		{
		  padding: 4px;
		  line-height: 1.42857143;
		  vertical-align: top;
		  border-top: 1px solid #f0f0f0;
		}

		.table > thead > tr > th 
		{
		  vertical-align: bottom;
		  border-bottom: 2px solid #f0f0f0;
		}

		.table > caption + thead > tr:first-child > th,
		.table > colgroup + thead > tr:first-child > th,
		.table > thead:first-child > tr:first-child > th,
		.table > caption + thead > tr:first-child > td,
		.table > colgroup + thead > tr:first-child > td,
		.table > thead:first-child > tr:first-child > td 
		{
		  border-top: 0;
		}

		.table > tbody + tbody 
		{
		  border-top: 2px solid #f0f0f0;
		}

		.table .table 
		{
		  background-color: #edecec;
		}		
	</style>
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>		
	<script>
	  $(function() 
	  {
			//jQuery time
			var current_fs, next_fs, previous_fs; //fieldsets
			var left, opacity, scale; //fieldset properties which we will animate
			var animating; //flag to prevent quick multi-click glitches

			$(".next").click(function()
			{	
				if($(this).attr('id')=='third')
				{
					$('#popup_box').fadeOut("slow");
					$('.finalPopupBox').fadeIn("slow");
				}
			   else
				{										
					if(animating) return false;
						animating = true;	
						current_fs = $(this).parent();
						next_fs = $(this).parent().next();
							
						//activate next step on progressbar using the index of next_fs
						$("#progressbar li").eq($("form").index(next_fs)).addClass("active");
						//show the next fieldset
						next_fs.show(); 
						//hide the current fieldset with style
						current_fs.animate({opacity: 0}, 
						{
							step: function(now, mx) 
							{
								//as the opacity of current_fs reduces to 0 - stored in "now"
								//1. scale current_fs down to 80%
								scale = 1 - (1 - now) * 0.2;
								//2. bring next_fs from the right(50%)
								left = (now * 50)+"%";
								//3. increase opacity of next_fs to 1 as it moves in
								opacity = 1 - now;
								current_fs.css({'transform': 'scale('+scale+')'});
								next_fs.css({'left': left, 'opacity': opacity});
							}, 
							duration: 800, 
							complete: function()
							{					
								current_fs.hide();
								animating = false;
							}, 
							//this comes from the custom easing plugin
							easing: 'easeInOutBack'
						});
				  }				
			});

			$(".previous").click(function()
			{

				if($(this).attr('id')=='Fourth')
				{
					$('#popup_box').fadeIn("slow");
					$('.finalPopupBox').fadeOut("slow");					
				}
			  else
			   {
			    //$('#popup_box').fadeOut("slow");
				if(animating) return false;
					animating = true;		
					current_fs = $(this).parent();
					previous_fs = $(this).parent().prev();	
					//de-activate current step on progressbar
					$("#progressbar li").eq($("form").index(current_fs)).removeClass("active");
					//show the previous fieldset
					previous_fs.show(); 
					//hide the current fieldset with style
					current_fs.animate({opacity: 0}, 
					{
						step: function(now, mx) 
						{
							//as the opacity of current_fs reduces to 0 - stored in "now"
							//1. scale previous_fs from 80% to 100%
							scale = 0.8 + (1 - now) * 0.2;
							//2. take current_fs to the right(50%) - from 0%
							left = ((1-now) * 50)+"%";
							//3. increase opacity of previous_fs to 1 as it moves in
							opacity = 1 - now;
							current_fs.css({'left': left});
							previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
						}, 
						duration: 800, 
						complete: function()
						{
							current_fs.hide();
							animating = false;
						}, 
						//this comes from the custom easing plugin
						easing: 'easeInOutBack'
					});
					}			  
				});			 
		});
	</script>	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="performCaneAccountingCalculation" ng-init="loadCaneAccountSeasonNames();">       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Perform Cane Accounting Calculation</b></h2></div>
							 					 
					 <div id="msform">
					    <!----------Progress Form------->
							<ul id="progressbar">
								<li class="active">Define Dates</li>
								<li>Perform Division</li>
								<li id="thirdFieldSet">Deductions and SB AC Divisions</li>
								<li>Advances and Loans</li>
								
							</ul>	
										
						<!-----------First Tab--------->
						
							<form name="FirstTabFilter" id="fieldset" style="width:100%;">
								<input type="hidden" name="caneAccSlno" ng-model="getDetailsData.caneAccSlno" />
 								
								<div class="card">
				    				<div class="card-body card-padding">
										<div class="row">
											<div class="col-sm-4">
										  		<div class="input-group">
            				            			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group" ng-class="{ 'has-error' : FirstTabFilter.season.$invalid && (FirstTabFilter.season.$dirty || filterSubmitted)}">												
						        	                 		<div class="fg-line">	
            							         			  <select chosen class="w-100" name="season" ng-model="getDetailsData.season" ng-options="season.season as season.season for season in accountSeasonNames" ng-required="true" ng-change="loadCaneAccountSerialNumber(getDetailsData.season);">
																 <option value="">Select Season</option>
													  		  </select>
															</div>
															<p ng-show="FirstTabFilter.season.$error.required && (FirstTabFilter.season.$dirty || filterSubmitted)" class="help-block">Season is Required</p>
														</div>
			    			                	</div>											
											</div>	
											<div class="col-md-4">
											  	<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : FirstTabFilter.dateFrom.$invalid && (FirstTabFilter.dateFrom.$dirty || filterSubmitted)}">
													  	<div class="fg-line">
															<input type="text" placeholder='From Date' class="form-control input_date" name="dateFrom" ng-model="getDetailsData.dateFrom" ng-required="true"/>
														</div>
														<p ng-show="FirstTabFilter.dateFrom.$error.required && (FirstTabFilter.dateFrom.$dirty || filterSubmitted)" class="help-block">From Date is Required</p>
													</div>
												</div>
											  </div>									
											   <div class="col-md-4">
											   	  <div class="input-group">
												  	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>	
													<div class="form-group" ng-class="{ 'has-error' : FirstTabFilter.dateTo.$invalid && (FirstTabFilter.dateTo.$dirty || filterSubmitted)}">
													   	<div class="fg-line">
															<input type="text" placeholder='To Date' class="form-control input_date" name="dateTo" ng-model="getDetailsData.dateTo" ng-required="true"/>	
														</div>
														<p ng-show="FirstTabFilter.dateTo.$error.required && (FirstTabFilter.dateTo.$dirty || filterSubmitted)" class="help-block">To Date is Required</p>
													</div>
												  </div>
											   </div>
										</div>
									</div>
								</div>
								<button type="button" class="btn btn-primary next" ng-click="getDetailsFilter();" id="first" ng-disabled="FirstTabFilter.$invalid"><i class="zmdi zmdi-arrow-forward"></i></button>
								<p></p>
							</form>
												
						<!---------------Second Tab----------->
					
						    <form name="PerformDivision" novalidate ng-submit="performDivisionSubmit();" id="fieldset" style="width:100%;">
							<div class="card">
							    <div class="card-body card-padding">									
									<div class="row" align="center">
										<div class="col-sm-4">
								          <div class="input-group">            				            
			        	    				 <div class="fg-line">
												<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="performCaneCalculations();">Perform Cane Calculations</button>
												
							                 </div>
			    					        </div>			                    
									      </div>
										</div><hr />
									<div  class="table-responsive">				
										<section class="asdok">
											<div class="container1">		
												<table class="table table-striped">
													<thead>
														<tr>
															<th><span>Cancel<br />Proportion</span></th>															
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
															<th><span>Supply <br />cane Wt. (Tons)</span></th>
															<th><span>Rate (Rs)</span></th>
															<th><span>Total Value (Rs)</span></th>
															<th><span>For loans <br /> advances (Rs)</span></th>
															<th><span>For Ded. & SB A/C</span></th>
														</tr>
													</thead>
													<tbody>
														<tr style="text-align:center" ng-repeat="accountGridData in caneAccountGridData">
															<td>
															
																<div class="checkbox">
													                <label>														
													                    <input type="checkbox" name="cancelProportion{{$index}}" ng-click="applyProportion($index,accountGridData.forLoans,accountGridData.forSbac,accountGridData.cancelProportion);" ng-true-value="'0'" ng-false-value="'1'" ng-model="accountGridData.cancelProportion">															
												    	                <i class="input-helper"></i>
																	</label>
																</div>
															</td>
															<input type="hidden" name="slno" ng-model="accountGridData.slno" ng-value="$index+1"/>
															<td>{{accountGridData.ryotCode}}<input type="hidden" name="ryotCode" ng-model="accountGridData.ryotCode"/></td>
															<td>{{accountGridData.ryotName}}<input type="hidden"  name="ryotName" ng-model="accountGridData.ryotName"/></td>
															<td>{{accountGridData.netWeight}}<input type="hidden" name="netWeight"  ng-model="accountGridData.netWeight"/></td>
															<td>{{accountGridData.seasonCanePrice}}<input type="hidden" name="seasonCanePrice" ng-model="accountGridData.seasonCanePrice" /></td>
															<td>																
																	<input type="text" class="form-control1" placeholder='Total Value' readonly name="totalValue" ng-model="accountGridData.totalValue" style="text-align:right;"/>										
																	<input type="hidden" name="villageCode" ng-model="accountGridData.villageCode" />
																	<input type="hidden" name="fromDate" ng-model="accountGridData.fromDate" />
																	<input type="hidden" name="todate" ng-model="accountGridData.todate" />
																	<input type="hidden" name="caneAccSlno" ng-model="accountGridData.caneAccSlno" />							
																
															</td>
															<td>
															
																<div class="form-group" ng-class="{ 'has-error' : PerformDivision.forLoans{{$index}}.$invalid && (PerformDivision.forLoans{{$index}}.$dirty || Tab1Submitted)}">															
																	<input type="text" class="form-control1" placeholder='For loans & advances' name="forLoans{{$index}}"  ng-model="accountGridData.forLoans" readonly ng-required="true" style="text-align:right;"/>
																	<p ng-show="PerformDivision.forLoans{{$index}}.$error.required && (PerformDivision.forLoans{{$index}}.$dirty || Tab1Submitted)" class="help-block">Required</p>
																</div>
															</td>
															<td>
																<div class="form-group" ng-class="{ 'has-error' : PerformDivision.forSbac{{$index}}.$invalid && (PerformDivision.forSbac{{$index}}.$dirty || Tab1Submitted)}">
																	<input type="text" class="form-control1" placeholder='For Ded. & SB A/C' name="forSbac{{$index}}" ng-model="accountGridData.forSbac" readonly ng-required="true"  style="text-align:right;"/><input type="hidden" name="season" ng-model="accountGridData.season"/>
																	<p ng-show="PerformDivision.forSbac{{$index}}.$error.required && (PerformDivision.forSbac{{$index}}.$dirty || Tab1Submitted)" class="help-block">Required</p>
																</div>
															</td>
														</tr>				
													</tbody>
						  					  </table>
											</div>
										  </section>	
				 					   </div>		
									</div>
								</div>
								
							    <button class="btn btn-primary next" id="second" ng-disabled="PerformDivision.$invalid" type="submit">Update <i class="zmdi zmdi-arrow-forward"></i></button>
							<p></p>
						  </form>
							
							
					
					<!-----------Third Tab--------------->						
						
						    <form name="ThirdTabDeduction" style="width:95%; margin-left:50px; height:85%; margin-top:-5%;" id="popup_box">						
						   <div style="overflow-y:auto;height:470px;"> 
							 <div class="card"> 
			    				<div class="card-body card-padding"> 
									<div  class="table-responsive"> 
										<table style="width:100%; position:relative;">
											<tr>
												<td>
													<table  style="width:100%;position:relative;" class="table" ng-repeat="headerData in thirdTabHeaderData">
														<tr style="background-color: #0099FF;font-weight:bold;">
															<th style=" color:#FFFFFF;">Ryot Name</th>
															<td colspan="2" style=" color:#FFFFFF;">{{headerData.RyotName}}</td>
															<th style=" color:#FFFFFF;">Ryot Code</th>
															<td  colspan="2" style=" color:#FFFFFF;"> {{headerData.ryotcode}}<input type="hidden" name="caneAccSlno" ng-model="headerData.caneAccSlno" /></td>
															<th style=" color:#FFFFFF;">Part Amount </th>
															<td colspan="2" style=" color:#FFFFFF;">{{headerData.forsbacs}}<input type="hidden" name="season" ng-model="headerData.season" /></td>
														</tr>
														<tr>
															<th>Deductions</th>
															<th>StdDedCode</th>
															<th>Cont. Code</th>
															<th>Sup. Cane Wt. (Tons)</th>
															<th>Extent Size (Acres)</th>
															<th style="display:none;">Old Pending Amt (Rs)</th>
															<th style="display:none;">New Payable(Rs)</th>
															<th>Net Payable (Rs)</th>
															<th>Ded. Paid Amt (Rs)</th>
															<th>Ded. Paid Amt(Rs)</th>
															
															<th>Pending Amt (Rs)</th>
														</tr>
														<tr ng-repeat="gridData in headerData.caneacslno">
															<td>{{gridData.name}}<input type="hidden" name="name{{$index}}" ng-model="gridData.name" /></td>
															<td>{{gridData.stdDedCode}}<input type="hidden" name="stdDedCode{{$index}}" ng-model="gridData.stdDedCode" /></td>
															<td>{{gridData.harvestcontcode}}<input type="hidden" name="harvestcontcode{{$index}}" ng-model="gridData.harvestcontcode" /></td>
															<td>{{gridData.canesupplied}}<input type="hidden" name="canesupplied{{$index}}" ng-model="gridData.canesupplied" /></td>
															<td>{{gridData.totalextent}}<input type="hidden" name="totalextent{{$index}}" ng-model="gridData.totalextent" /></td>
															<td style="display:none;">																																													
																	<input type="text"  name="oldPending{{$index}}" ng-model="gridData.oldPending" readonly  style="text-align:right;"/>																	
																
															</td>
															<td style="display:none;">																	
																   <input type="text"  name="newPayable{{$index}}"  ng-model="gridData.newPayable" readonly  style="text-align:right;"/>																	
																
															</td>
															<td>
																<div class="form-group" ng-class="{ 'has-error' : ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$invalid && (ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$dirty || Tab3Submitted)}">
																	<input type="text"  name="netPayable{{headerData.id}}{{$index}}" ng-required="true" ng-model="gridData.netPayable" ng-keyup="ryotPayableAdjustment(gridData.netPayable,headerData.id,$index);"  style="text-align:right;" data-ng-pattern="/^[0-9.-]+$/" readonly="readonly"/>
																	<p ng-show="ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$error.required && (ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$dirty || Tab3Submitted)" class="help-block">Required</p>
																	<p ng-show="ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$error.pattern && (ThirdTabDeduction.netPayable{{headerData.id}}{{$index}}.$dirty || Tab3Submitted)" class="help-block">Invalid</p>																	
																	<p style=" color:#FF0000; display:none;" id="cError{{headerData.id}}{{$index}}">Given Amount Shouldn't be greater than the NetPayable</p>
																</div>
															</td>
															<td style="display:;">
															
															<input type="text" style="width:100px;text-align:right;" name="gridDedAmt{{$index}}" ng-model="gridData.amount" ng-keyup="pendingAmountChange(gridData.netPayable,gridData.amount,headerData.id,$index);" ng-change="pendingAmountChange(gridData.netPayable,gridData.amount,headerData.id,$index);"/>
															
															 </td>
															
															<!--<td>																
																	<input type="text" style="width:100px;text-align:right;" name="dedPaidAmt{{$index}}" ng-model="gridData.dedPaidAmt" ng-keyup="pendingAmountChange(gridData.netPayable,gridData.dedPaidAmt,headerData.id,$index);" ng-change="pendingAmountChange(gridData.netPayable,gridData.dedPaidAmt,headerData.id,$index);"/>																	
																
															</td>-->
															<td>																
																	<input type="text" style="width:150px;text-align:right;" name="pendingAmount{{$index}}" ng-model="gridData.pendingAmount" readonly/>																	
																
															</td>
														</tr>
														<tr style="background-color: #0099FF;font-weight:bold;">
															<th style=" color:#FFFFFF; text-align:right;" colspan="8">Ryot SB A/C</th>
															<th colspan="1">
																<div class="form-group" ng-class="{ 'has-error' : ThirdTabDeduction.ryotsbac{{$index}}.$invalid && (ThirdTabDeduction.ryotsbac{{$index}}.$dirty || Tab3Submitted)}">
																	<input type="text" ng-model="headerData.ryotsbac" readonly style="text-align:right;" name="ryotsbac{{$index}}" data-ng-pattern="/^[0-9.]+$/" ng-required="true"/>
																	<p ng-show="ThirdTabDeduction.ryotsbac{{$index}}.$error.pattern && (ThirdTabDeduction.ryotsbac{{$index}}.$dirty || Tab3Submitted)" class="help-block">Ryot SB A/C shouldn't be Negetive.</p>
																</div>
															</th>
														</tr>
													</table>	
												</td>
											</tr>
											
										</table>
									</div>									
								</div>
							</div>
						</div>
						<p></p>	
					   <button class="btn btn-primary next third" id="third" ng-disabled="ThirdTabDeduction.$invalid" ng-click="thirdTabDeductionsSave();"><i class="zmdi zmdi-arrow-forward"></i></button>					
					   <button type="button" class="btn btn-primary third" ng-click="thirdTabDeductionsSave();" ng-disabled="ThirdTabDeduction.$invalid">Update</button>
					
					</form>
					<!-------------Fourth Tab--------->					
					
						  	<form name="FourthTabLoans" class="finalPopupBox"    style="width:90%;margin-left:-10px;  float:left;height:85%; margin-top:-5%;">						
						<div style="overflow-y:auto;height:470px;">
							<div class="card">
			    				<div class="card-body card-padding">
									<div  class="table-responsive">
										<table style="width:100%; position:relative;">
											<tr>
												<td>
													<table  style="width:100%;position:relative;" class="table" ng-repeat="fourthHeaderData in fourthTabHeaderData">
														<tr style="background-color: #0099FF;font-weight:bold;">
															<th style=" color:#FFFFFF;">Ryot Name</th>
															<td colspan="3" style=" color:#FFFFFF;">{{fourthHeaderData.RyotName}}</td>
															<th style=" color:#FFFFFF;">Ryot Code</th>
															<td  colspan="3" style=" color:#FFFFFF;">{{fourthHeaderData.ryotcode}}<input type="hidden" name="caneAccSlno" ng-model="fourthHeaderData.caneAccSlno" /></td>
															<th style=" color:#FFFFFF;">Part Amount</th>
															<td colspan="3" style=" color:#FFFFFF;">{{fourthHeaderData.ForLoans}}<input type="hidden" name="season" ng-model="fourthHeaderData.season" /></td>
														</tr>
														<tr>
															<th>Advance</th>
															<th>Advance Code</th>
															<th>Advance Date</th>
															<th>Advance Amount (Rs)</th>
															<th>Repayment Date</th>
															<th>Interest Rate</th>
															<th>Subvented Interest Rate (Rs)</th>
															<th>Net Rate (Rs)</th>
															<th>Interest Amount (Rs)</th>
															<th>Total Amount(Rs)</th>
															<th>Payable(Rs)</th>
															<th>Pending Amount (Rs)<input type="hidden" name="id" ng-model="fourthHeaderData.id" /></th>															
														</tr>
														<tr ng-repeat="fourthGridData in fourthHeaderData.caneacslno">
															<td>{{fourthGridData.advance}}</td>
															<td>{{fourthGridData.advancecode}}</td>
															<td>{{fourthGridData.advancedate}}</td>
															<td>{{fourthGridData.advanceamount}}</td>
															<td><input type="text" class="form-controll datepicker" name="advancerepaydate{{fourthHeaderData.id}}{{$index}}"  data-ng-model="fourthGridData.advancerepaydate" ng-mouseover="ShowDatePicker();" data-input-mask="{mask: '0000-00-00'}"  ng-blur="ChangeDate(fourthGridData.advanceamount,fourthGridData.subventedInterestRate,fourthGridData.advancedate,fourthGridData.advancerepaydate,fourthGridData.loanPayable,fourthHeaderData.id,$index,fourthGridData.totalamount);"  /></td>
															<td>{{fourthGridData.advanceinterest}}</td>
															<td>
																<div class="form-group" ng-class="{ 'has-error' : FourthTabLoans.subventedInterestRate{{fourthHeaderData.id}}{{$index}}.$invalid && (FourthTabLoans.subventedInterestRate{{fourthHeaderData.id}}{{$index}}.$dirty || Tab4Submitted)}">
																	<input type="text" name="subventedInterestRate{{fourthHeaderData.id}}{{$index}}" ng-model="fourthGridData.subventedInterestRate" style="text-align:right;"   ng-blur="ChangeDate(fourthGridData.advanceamount,fourthGridData.subventedInterestRate,fourthGridData.advancedate,fourthGridData.advancerepaydate,fourthGridData.loanPayable,fourthHeaderData.id,$index,fourthGridData.totalamount);" />
																	<p ng-show="FourthTabLoans.subventedInterestRate{{fourthHeaderData.id}}{{$index}}.$error.pattern && (FourthTabLoans.subventedInterestRate{{fourthHeaderData.id}}{{$index}}.$dirty || Tab4Submitted)" class="help-block">Invalid.</p>
																</div>
																
															</td>
															<td>{{fourthGridData.advanceinterest}}</td>
															<td>{{fourthGridData.advanceinterest}}</td>
															<td>{{fourthGridData.totalamount}}</td>
															<td>
																<div class="form-group" ng-class="{ 'has-error' : FourthTabLoans.loanPayable{{fourthHeaderData.id}}{{$index}}.$invalid && (FourthTabLoans.loanPayable{{fourthHeaderData.id}}{{$index}}.$dirty || Tab4Submitted)}">
																	<input type="text"   name="loanPayable{{fourthHeaderData.id}}{{$index}}" ng-model="fourthGridData.loanPayable" style="width:100px; text-align:right;" data-ng-pattern="/^[0-9.]+$/" ng-required="true" ng-keyup="fourthTabPayableAdjust(fourthGridData.loanPayable,fourthHeaderData.id,$index);"/>
																	
																	<p ng-show="FourthTabLoans.loanPayable{{fourthHeaderData.id}}{{$index}}.$error.pattern && (FourthTabLoans.loanPayable{{fourthHeaderData.id}}{{$index}}.$dirty || Tab4Submitted)" class="help-block">Invalid.</p>
																	<p style="color:#FF0000; display:none;" id="c4Error{{fourthHeaderData.id}}{{$index}}">Given Amount Shouldn't be greater than the NetPayable</p>
																</div>
															</td>
															<td>
																<input type="text"   name="loanPendingAmount" ng-model="fourthGridData.loanPendingAmount" style="text-align:right;" readonly/>
																<input type="hidden" name="tempTotal" ng-model="fourthGridData.tempTotal" />																
															</td>
														</tr>
														<tr style="background-color: #0099FF;font-weight:bold;">
															<th style=" color:#FFFFFF; text-align:right;" colspan="11">Ryot SB A/C</th>
															<td colspan="1">
																<div class="form-group" ng-class="{ 'has-error' : FourthTabLoans.ryotLoansbac{{fourthHeaderData.id}}.$invalid && (FourthTabLoans.ryotLoansbac{{fourthHeaderData.id}}.$dirty || Tab4Submitted)}">
																	<input type="text" ng-model="fourthHeaderData.ryotLoansbac" style="text-align:right;" readonly name="{{fourthHeaderData.id}}" data-ng-pattern="/^[0-9.]+$/" ng-required="true"/>
																	<p ng-show="FourthTabLoans.ryotLoansbac{{fourthHeaderData.id}}.$error.pattern && (FourthTabLoans.ryotLoansbac{{fourthHeaderData.id}}.$dirty || Tab4Submitted)" class="help-block">Invalid.</p>
																</div>
																	<input type="hidden" name="tempLoansbac" ng-model="fourthGridData.tempLoansbac" />
															</td>
														</tr>																												
													</table>	
																							
												</td>
											</tr>
										</table>
									</div>									
								</div>
							</div>
						</div>
						<p></p>	
  						<!--<button class="btn btn-primary previous" id="Fourth"><i class="zmdi zmdi-arrow-back"></i></button>-->
						<button type="button" class="btn btn-primary fourth" ng-click="fourthTabDeductionsSave();" ng-disabled="FourthTabLoans.$invalid">Update</button>
					</form>
														 
				</div>					
			 </div>
		 	<!----------end----------------------->															
		</div>
	 </section>  
  </section>
  <br />
  <footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
