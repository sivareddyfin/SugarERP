
<script>

$(document).ready(function(){
	

Highcharts.setOptions({
   colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4'],
    lang: {
        drillUpText: 'Back to {series.name}'
    }
});

var options = {

    chart: {
        height: 500
    },
    
    credits: {
        enabled: false
      },
    
    title: {
        text: 'Total Agreements-Field officer wise - 1,423',
        
        
    },

    xAxis: {
        categories: true
    },
    
     legend: {
        enabled: true,
        
       
        },
    
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            },
            shadow: false
        },
        pie: {
            size: '80%',
            showInLegend: true
            
        }
    },
    
    series: [{
        name: 'Agreements',
        colorByPoint: true,
        data: [{
            name: 'S.NARASIMHA RAJU',
            y:312
            
        }, {
            name: 'R.SURYARAO',
            y:97
            
        }, {
            name: 'M.PANASARAM',
            y: 39
        },{
            name: 'K.ANJANEYULU',
            y: 164
        },{
            name: 'G.SURESH NAIDU',
            y: 199
        },
        {
            name: 'M.LOKESH',
            y: 5
        },{
            name: 'M.MURTHYRAJU',
            y: 607
        }]
    }]
};


// Pie
options.chart.renderTo = 'container2';
options.chart.type = 'pie';
var chart2 = new Highcharts.Chart(options);

})


</script>
<script>

$(document).ready(function(){
	

Highcharts.setOptions({
   colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4'],
    lang: {
        drillUpText: 'Back to {series.name}'
    }
});

var options1 = {

    chart: {
        height: 300
    },
    
    credits: {
        enabled: false
      },
    
    title: {
        text: 'Total Agreements - Plant/Ratoon wise - 1,988'
    },

    xAxis: {
        categories: true
    },
    
     legend: {
        enabled: true,
        
        
       
    },
    
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            },
            shadow: false
        },
        pie: {
            size: '70%',
            showInLegend: true
            
        }
    },
    
    series: [{
        name: 'Total',
        colorByPoint: true,
        data: [{
            name: 'Plant',
            y:680
            
        }, {
            name: 'Ratoon1',
            y:681
            
        }, {
            name: 'Ratoon2',
            y: 505
        },{
            name: 'Ratoon3',
            y: 111
        },{
            name: 'Ratoon4',
            y: 11
        }]
    }]
};


//Pie1
options1.chart.renderTo = 'container3';
options1.chart.type = 'pie';
var chart2 = new Highcharts.Chart(options1);


Highcharts.setOptions({
	   colors: ['#FA8072', '#6A5ACD', '#FF6347', '#F5DEB3', '#008080', '', '', ''],
	    lang: {
	        drillUpText: 'Back to {series.name}'
	    }
	});



var options2 = {

	    chart: {
	        height: 300
	    },
	    
	    credits: {
	        enabled: false
	      },
	    
	    title: {
	        text: 'Total Extent Size - Plant/Ratoon wise - 4,653.25 Acres'
	    },

	    xAxis: {
	        categories: true
	    },
	    
	     legend: {
	        enabled: true,
	        
	        
	       
	    },
	    
	    plotOptions: {
	        series: {
	            dataLabels: {
	                enabled: true
	            },
	            shadow: false
	        },
	        pie: {
	            size: '70%',
	            showInLegend: true
	            
	        }
	    },
	    
	    series: [{
	        name: 'Extent',
	        colorByPoint: true,
	        data: [{
	            name: 'Plant',
	            y:1409.73
	            
	        }, {
	            name: 'Ratoon1',
	            y:1678.86
	            
	        }, {
	            name: 'Ratoon2',
	            y:1275.45
	        },{
	            name: 'Ratoon3',
	            y: 269.18
	        },{
	            name: 'Ratoon4',
	            y: 20.03
	        } ]
	    }]
	};


//pie2
options2.chart.renderTo = 'container4';
options2.chart.type = 'pie';
var chart3 = new Highcharts.Chart(options2);


Highcharts.setOptions({
	 colors: ['#FF4500', '#DDA0DD', '	#FF0000', '#4169E1', '#40E0D0', '', '', ''],
	    
	    lang: {
	        drillUpText: 'Back to {series.name}'
	    }
	});


var options3 = {

	    chart: {
	        height: 300
	    },
	    
	    credits: {
	        enabled: false
	      },
	    
	    title: {
	        text: 'Total AGR_QTY - Plant/Ratoon wise - 1,37,715 Tons'
	    },

	    xAxis: {
	        categories: true
	    },
	    
	     legend: {
	        enabled: true,
	        
	        
	       
	    },
	    
	    plotOptions: {
	        series: {
	            dataLabels: {
	                enabled: true
	            },
	            shadow: false
	        },
	        pie: {
	            size: '70%',
	            showInLegend: true
	            
	        }
	    },
	    
	    series: [{
	        name: 'AGR_QTY',
	        colorByPoint: true,
	        data: [{
	            name: 'Plant',
	            y:45931
	            
	        }, {
	            name: 'Ratoon1',
	            y:47650
	            
	        }, {
	            name: 'Ratoon2',
	            y: 35805
	        },{
	            name: 'Ratoon3',
	            y: 7796
	        },{
	            name: 'Ratoon4',
	            y: 533
	        } ]
	    }]
	};




//pie3
options3.chart.renderTo = 'container5';
options3.chart.type = 'pie';
var chart4 = new Highcharts.Chart(options3);

 

})


</script>
<script>

$(document).ready(function(){
	

Highcharts.setOptions({
   colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4'],
    lang: {
        drillUpText: 'Back to {series.name}'
    }
});

var options = {

    chart: {
        height: 300
    },
    
    credits: {
        enabled: false
      },
    
    title: {
        text: 'Total Loans Bank Wise - Rs.16,43,78,000.00/-'
    },

    xAxis: {
        categories: true
    },
    
    drilldown: {
        series: [{
            id: 'ANDHRA BANK',
            name: 'ANDHRA BANK',
            data: [
                ['ANDHRA BANK, KADIAM',9998000 ],
                ['ANDHRA BANK, MIRTHIPADU',16777000 ],
                ['ANDHRA BANK, RAGHUDEVPURAM',1110000],
                ['ANDHRA BANK, RANGAMPETA',100000 ],
                ['ANDHRA BANK, THOKADA',905000],
                ['ANDHRA BANK, VADISILERU',1305000]
                
            ]
        }, {
            id: 'BANK OF BARODA',
            name: 'BANK OF BARODA',
            data: [ 
            ['BANK OF BARODA, HUKUMPETA',340000],
            ['BANK OF BARODA, KATHERU',29400500],
            ['BANK OF BARODA, MURARI',15979000],
            ['BANK OF BARODA, PEDDAPURAM',19367500],
            ['BANK OF BARODA, YEDITHA',1748000]
            ]
        }, {
            id: 'BANK OF INIDA',
            name: 'BANK OF INIDA',
            data: [
                ['BANK OF INIDA, RAVULAPALEM',300000]
                 
            ]
        },{
        	 id: 'CANARA BANK',
             name: 'CANARA BANK',
             data: [
                    
                 ['CANARA BANK, JEGURUPADU',230000 ],
                 ['CANARA BANK, PITAPURAM',530000 ],
                 ['CANARA BANK, RAJAHMUNDRY',1725000 ],
                 ['CANARA BANK, RAVULAPALEM',1536000 ]
                 
             ]
        	
        },{
        	id: 'CHAITANYA G.G.BANK',
            name: 'CHAITANYA G.G.BANK',
            data: [
                ['CHAITANYA G.G.BANK, PITAPURAM',2972000],
                ['CHAITANYA G.G.BANK,CHEBROLU',621000 ],
                ['CHAITANYA G.G.BANK,GEDDANAPALL',4662000]
                 
        ]},{
        	
        	id: 'CORPORATION BANK',
            name: 'CORPORATION BANK',
            data: [
                ['CORPORATION BANK, DULLA',7308000  ],
                ['CORPORATION BANK, GANDEPALLI',2225000 ]
                
        ]
        },{
        	id: 'KARUR VYSYA BANK',
            name: 'KARUR VYSYA BANK',
            data: [
                ['KARUR VYSYA BANK, BOMMURU',100000],
                ['KARUR VYSYA BANK, PEDDAPURAM',2213000 ],
                ['KARUR VYSYA BANK, PITAPURAM',5569000]
                
        ]
        	
        	
        },{
        	
        	
        	id: 'S.B.H',
            name: 'S.B.H',
            data: [
                ['S.B.H., CHEMUDULANKA',525000]
                
                
        ]
        	
        	
        	
        },{
        	id: 'S.B.I',
            name: 'S.B.I',
            data: [
                ['S.B.I., ATREYAPURAM',495000],
                ['S.B.I., KORUKONDA',1888000],
                ['S.B.I., MUGGALLA',400000],
                ['S.B.I., RAJANAGARAM',954500],
                ['S.B.I., THORREDU',33094500]
                
        	]
        	
        	
        }]
    },
    
    legend: {
        enabled: true
    },
    
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            },
            shadow: false
        },
        pie: {
            size: '80%',
            showInLegend: true
        }
    },
    
    series: [{
        name: 'Bank Loan',
        colorByPoint: true,
        data: [{
            name: 'ANDHRA BANK',
            y:164378000,
            drilldown: 'ANDHRA BANK'
        }, {
            name: 'BANK OF BARODA',
            y:66835000 ,
            drilldown: 'BANK OF BARODA'
        }, {
            name: 'BANK OF INIDA',
            y: 300000,
            drilldown: 'BANK OF INIDA'
        },{
        	name: 'CANARA BANK',
            y: 4021000,
            drilldown: 'CANARA BANK'
        },{
        	name: 'CHAITANYA G.G.BANK',
            y:8255000 ,
            drilldown: 'CHAITANYA G.G.BANK'
        	
        },{
        	name: 'CORPORATION BANK',
            y:9533000,
            drilldown: 'CORPORATION BANK'
            },{
            	name: 'KARUR VYSYA BANK',
                y:7882000 ,
                drilldown: 'KARUR VYSYA BANK'
            	},{
            		name: 'S.B.H',
                    y:525000 ,
                    drilldown: 'S.B.H'
            	},{
            		name: 'S.B.I',
                    y:36832000 ,
                    drilldown: 'S.B.I'
            		}]
    }]
};


// Pie
options.chart.renderTo = 'container6';
options.chart.type = 'pie';
var chart2 = new Highcharts.Chart(options);

})


</script>
<script language="JavaScript">
$(document).ready(function() { 
   var data = {
      table: 'datatable'
   };
   var chart = {
      type: 'column'
   };
   var title = {
      text: 'Overview of amount taken and amount cleared'   
   };      
   var yAxis = {
      allowDecimals: false,
      title: {
         text: 'Units'
      }
   };
   var tooltip = {
      formatter: function () {
         return '<b>' + this.series.name + '</b><br/>' +
            this.point.y + ' ' + this.point.name.toLowerCase();
      }
   };
   var credits = {
      enabled: false
   };  
      
   var json = {};   
   json.chart = chart; 
   json.title = title; 
   json.data = data;
   json.yAxis = yAxis;
   json.credits = credits;  
   json.tooltip = tooltip;  
   $('#container').highcharts(json);
   
   
   Highcharts.setOptions({
	   colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4'],
	    lang: {
	        drillUpText: 'Back to {series.name}'
	    }
	});
   
   
   var options = {

		    chart: {
		        height: 300
		    },
		    
		    credits: {
		        enabled: false
		      },
		    
		    title: {
		        text: 'Overview of advances'
		    },

		    xAxis: {
		        categories: true
		    },
		    
		     legend: {
		        enabled: true,
		        
		        
		       
		    },
		    
		    plotOptions: {
		        series: {
		            dataLabels: {
		                enabled: true
		            },
		            shadow: false
		        },
		        pie: {
		            size: '70%',
		            showInLegend: true
		            
		        }
		    },
		    
		    series: [{
		        name: 'Amount',
		        colorByPoint: true,
		        data: [{
		            name: 'Seed Advances',
		            y:20440000
		            
		        }, {
		            name: 'Bio-fertilizers',
		            y:16352000
		            
		        }, {
		            name: 'Land Preparation',
		            y:10220000
		        }]
		    }]
		};


		// Pie
		options.chart.renderTo = 'container7';
		options.chart.type = 'pie';
		var chart2 = new Highcharts.Chart(options);

   
   
});

</script>
<script type="text/javascript">
		$(document).ready(function()
		{
			 var chart = new CanvasJS.Chart("chartContainer8", {
				zoomEnabled: false,
				animationEnabled: true,
				title: {
					text: "OverView of sugarcane production - 1,09,551 Tons"
				},
				axisY2: {
					valueFormatString: "0.0 tones",

					maximum: 10000,
					interval: 0,
					interlacedColor: "#F5F5F5",
					gridColor: "#D7D7D7",
					tickColor: "#D7D7D7"
				},
				axisX: {
					valueFormatString: "MMM",
					interval: 1,
					intervalType: "month"

				},
				theme: "theme2",
				toolTip: {
					shared: true
				},
				legend: {
					verticalAlign: "bottom",
					horizontalAlign: "center",
					fontSize: 15,
					fontFamily: "Lucida Sans Unicode"
				},
				data: [{
					type: "line",
					lineThickness: 3,
					axisYType: "secondary",
					showInLegend: true,
					name: "First Week",
					dataPoints: [
					 { x: new Date("January , 2016"), y:9020  },
					{ x: new Date("November, 2016"), y:9181  },
					{ x: new Date("December, 2016"), y:9000  }
					 
					 
					 
					]
				},
				{
					type: "line",
					lineThickness: 3,
					showInLegend: true,
					name: "Second Week",
					axisYType: "secondary",
					dataPoints: [
					{ x: new Date("January , 2016"), y: 9015},
					{ x: new Date("November, 2016"), y: 9200},
					{ x: new Date("December, 2016"), y: 8999}
					 
					]
				},
				{
					type: "line",
					lineThickness: 3,
					showInLegend: true,
					name: "Third Week",
					axisYType: "secondary",
					dataPoints: [
					{ x: new Date("January , 2016"), y: 9182 },
					{ x: new Date("November, 2016"), y: 9150 },
					{ x: new Date("December, 2016"), y: 9185 }
					 
					]
				},{
					type: "line",
					lineThickness: 3,
					showInLegend: true,
					name: "Fourth Week",
					axisYType: "secondary",
					dataPoints: [
					{ x: new Date("January , 2016"), y: 9179 },
					{ x: new Date("November, 2016"), y: 9250 },
					{ x: new Date("December, 2016"), y: 9190 }
					 
					]
				}
				],
				legend: {
					cursor: "pointer",
					itemclick: function (e) {
						if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
						}
						else {
							e.dataSeries.visible = true;
						}
						chart.render();
					}
				}
			});

			chart.render();
		});
	</script>
<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"> 
		<div class="container">
		    <div class="row">
		       <div class="col-sm-12">
	   				<div id="container2"></div>
			   </div>
		    </div>
			<br />
			
			<div class="row">
				<div  class="col-md-4" id="container3" style="margin: 0 auto"></div>
				<div class="col-md-4" id="container4" style=" margin: 0 auto"></div>
				<div class="col-md-4" id="container5" style=" margin: 0 auto"></div>
			</div>	   
			<br />
			
			<div class="row">
				<div class="col-sm-12">
					<div id="container6" style="height: 300px; width: 600px; margin: 0 auto"></div>
				</div>
			</div>
			<br />
			
			<div class="row">
				<div class="col-sm-6">
					<div id="container7" style="height: 300px; width: 600px; margin: 0 auto"></div>
				</div>
				<div class="col-sm-6">
					<div id="container" style="width: 550px; height: 400px; margin: 0 auto"></div>
				</div>
				<table id="datatable" border=10>
<thead>
<tr><th></th><th>Amount</th><th>Cleared</th></tr>
</thead>
<tbody>
<tr><th>Seed Advances</th><td>20440000</td><td>2044000</td></tr>
<tr><th>Bio-fertilizers</th><td>16352000</td><td>11002000</td></tr>
<tr><th>Land Preparation</th><td>10220000</td><td>10220000</td></tr>
 
</tbody>
</table>
			</div>			
			<br />
			<div class="row">
				<div class="col-sm-12">
					<div id="chartContainer8" style="width: 100%; height: 300px;display: inline-block;"></div>
				</div>
			</div>
		</div>
	</section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>