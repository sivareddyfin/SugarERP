	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 //$('#date').focus();	 		 
		// $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>			
<style type="text/css">
@media print {
    .footer {page-break-after: always;}
}

/*.row:nth-child(3n){
page-break-after: always;
color:#FF0000;
}*/

#start {
/*  margin-bottom:8px;*/
}

.col1 {
  /*margin-bottom: 4px;*/
}

.col-inside {
  border: solid 1px #000000;

}
.col1:first-child {
  padding: 0 5px 5px 0;

}
.col1:nth-child(2) {
  padding: 0 5px 5px;
}

table.test
{
    border-collapse: separate;
    border-spacing: 2px;
}

table.test td
{
     
    padding: 0 0 0 10px;
  /*  border: 1px  #ccc;*/
}
</style>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="reprintReceiptController"  ng-init='loadShift(); loadCaneWeightSeasonNames();'>       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Reprint Receipt</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
						
					  	 <form  target="_blank">							 				  
					  		 <div class="row" id="ftSData">
							<div class="col-sm-12">								
								<div class="row">
								<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="season" ng-required="true" data-placeholder="Season" ng-model="AddReprintReceipt.season" ng-required="true" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true">
															<option value="">Season</option>
															
														</select>
				                			        </div>
																										
												</div>
					                    	</div>
										</div>
									<div class="col-sm-3">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
					        	                <div class="fg-line">
												   <label for="fdate">From Date</label>
    		        					          <input type="text" class="form-control date" placeholder='From Date' id="fdate" with-floating-label name="date" ng-model="AddReprintReceipt.fromDate"  tabindex="1" ng-required="true" data-input-mask="{mask: '00:00:00'}" maxlength="10" readonly="readonly" ng-change="getPermitWiseRyotDetails(AddReprintReceipt.fromDate,AddReprintReceipt.toDate,AddReprintReceipt.shift,AddReprintReceipt.lorryStatus);"/>
					                	        </div>
	
</div>												
				                    
									</div>
									<div class="col-sm-3">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
					        	                <div class="fg-line">
												   <label for="PermitNumber">To Date</label>
    		        					          <input type="text" class="form-control date" placeholder='To Date' id="PermitNumber" with-floating-label name="date" ng-model="AddReprintReceipt.toDate"  tabindex="1" ng-required="true" data-input-mask="{mask: '00:00:00'}" maxlength="10" readonly="readonly" ng-change="getPermitWiseRyotDetails(AddReprintReceipt.fromDate,AddReprintReceipt.toDate,AddReprintReceipt.shift,AddReprintReceipt.lorryStatus);"/>
					                	        </div>
	
</div>												
				                    
									</div>
									
										<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift" ng-required="true" data-placeholder="Shift" ng-model="AddReprintReceipt.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames | orderBy:'-shiftName':true" ng-change="getPermitWiseRyotDetails(AddReprintReceipt.fromDate,AddReprintReceipt.toDate,AddReprintReceipt.shift,AddReprintReceipt.lorryStatus);">
															<option value="">Shift</option>
															
														</select >
				                			        </div>
																										
												</div>
					                    	</div>
										</div>
										
										
									
									
									
									
								</div>
								
								<div class="row">
								<div class="col-sm-4">
										
											<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weighment :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0"  data-ng-model='AddReprintReceipt.lorryStatus' ng-change="getPermitWiseRyotDetails(AddReprintReceipt.fromDate,AddReprintReceipt.toDate,AddReprintReceipt.shift,AddReprintReceipt.lorryStatus);">
			    			            	<i class="input-helper"></i>1
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"  data-ng-model='AddReprintReceipt.lorryStatus' ng-change="getPermitWiseRyotDetails(AddReprintReceipt.fromDate,AddReprintReceipt.toDate,AddReprintReceipt.shift,AddReprintReceipt.lorryStatus);">
			    				            <i class="input-helper"></i>2
					  		              </label>
			                	        </div>
			                    	</div>
										</div>
								
								<div class="col-sm-4">
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">
									<input type="text" class="form-control" placeholder="Serial Number" name="serialNumber" data-ng-model="AddReprintReceipt.serialNumber" />
									</div>
								</div>
								</div>
								
								</div>		
									
									<div class="col-sm-3">
											<div class="input-group">
            				            		
												<div class="form-group">
						        	                <div class="fg-line">
														<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="generatePrint(AddReprintReceipt.season,AddReprintReceipt.serialNumber,AddReprintReceipt.lorryStatus);">Generate Print </button>
														
				                			        </div>
																										
												</div>
					                    	</div>
										</div>	
										</div>
								<div class="row" id="permitNumberprint" style="display:;">
								<div class="col-sm-3" style="display:none;">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">
														<input type="text" class="form-control" ng-model="AddReprintReceipt.permitNumber" readonly>
														
				                			        </div>
																										
												</div>
					                    	</div>
										</div>
								
										
										
										
										
										<!--<div class="col-sm-4">
										
											<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorry Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" data-ng-model='AddAgreement.permitSelection' ng-change="changePermitSelection(AddAgreement.maxNumber,AddAgreement.permitSelection);">
			    			            	<i class="input-helper"></i>Lorry with Load
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"  data-ng-model='AddAgreement.permitSelection'  ng-change="changePermitSelection(AddAgreement.maxNumber,AddAgreement.permitSelection);">
			    				            <i class="input-helper"></i>Empty Lorry
					  		              </label>
			                	        </div>
			                    	</div>
										</div>-->
										</div>
								
																								
							</div>							
						 </div>
						 </form>
						<br />
						 	
					
						<form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">						  
						  	  						
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th>Sl No.</th>	
												<th>Permit No.</th>	
                		    					<th>Ryot Code</th>
												<th>Ryot Name</th>
							                	
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="permitData in data">
											<td>{{permitData.serialnumber}}</td>
												<td><a ng-click="getPermitNumber(permitData.permitnumber,permitData.serialnumber);" style="cursor:pointer;">{{permitData.permitnumber}}</a></td>
												
												<td>{{permitData.ryotcode}}</td>
												<td>{{permitData.ryotname}}</td>
												
																								
											</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        
						</div>
						
						
						
						
			    	</div>
					   </div>				
					 </form>  							 		 							 
					<!------------------form end------------------------------>							 
					<!----------Popup box on surveynumber for plant----------->
						
					 
					<!-----------Popup box on croptype dropdown on change----->
						
					<!-------from-end--------------------->		 						           			
				      </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<!----------end----------------------->										
					
				</div> 
				<!--<div id="1234" style="display:none;">
					<div ng-repeat="item in items" ng-if="$index % 2 == 0" class="row">
							
	        		<div class="col-xs-12 col1" >
			
			<div class="col-inside" style=" border: solid 1px #000000;">
				
				<table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
					<tr style="text-align:center; font-size:14px;"><td><strong>SRI SARVARAYA SUGARS LTD., CHELLURU</strong></td></tr>
					<tr style="text-align:center; font-size:12px;"><td><strong>CANE HARVESTING PERMIT</strong></td></tr>					
				</table>
				<hr style="border: solid 1px #333333;"/>
				<div class="row" style="font-family:Georgia, Helvetica, sans-serif; ">
				<div class="col-xs-4">
				<table width="" align="left" style="font-size:12px;">
				<tr  style="text-align:left" ><td><strong>&nbsp;&nbsp;PERMIT NO.</strong></td>
				<td>:</td>
				<td style="font-family:Calibry, Helvetica, sans-serif;"> &nbsp;&nbsp;{{item.permitnumber}}</td>
				</tr>
				<tr><td> <strong>&nbsp;&nbsp;HARVEST. DATE</strong></td>
				<td>:</td>
				<td style="font-family:Calibry, Helvetica, sans-serif;">&nbsp;&nbsp;{{item.harvestDate}}</td>
				</tr>
				<tr><td > <strong>&nbsp;&nbsp;CIRCLE</strong></td>
				<td>:</td>
				<td> &nbsp; {{item.circle}}</td>
					
					</tr>	
									
					
				</table>
				</div>
				<div class="col-xs-4"  valign='top'>
				<table width="" align="center" style="font-size:12px;">
				<tr  style="text-align:left"><td  ><strong>&nbsp;&nbsp;RYOT CODE</strong></td>
					<td>:</td>
					<td style="font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"> &nbsp;&nbsp;{{item.ryotcode}}</td>
					</tr>
					<tr><td > <strong>&nbsp;&nbsp;VILLAGE</strong></td>
					<td>:</td>
					<td>&nbsp;&nbsp; {{item.villagename}}</td>
				</tr>
				<tr><td > <strong>&nbsp;&nbsp;FATHER</strong></td>
				<td>:</td>
				<td>&nbsp;&nbsp; {{item.fathername}}</td>
				</tr>
				<tr ><td ><strong>&nbsp;&nbsp;RYOT</strong></td>
				<td>:</td>
				<td> &nbsp;&nbsp; {{item.ryotname}}</td>
				</tr>
				
				
				
				</table>
				</div>
			
				<div class="col-xs-4"  rowspan="4"   valign='top' >
				
				
				
				
				<img alt="Embedded Image" data-ng-src="data:image/png;base64,{{item.qrcodedata}}"  align="right" height="60"  style="margin-top:-75px; margin-right:10px;" />
				
				
				</div>
				</div>
                	
                	
                	
                	<div align="center">
						
                    <table border="1" style="width:100%; border-collapse:collapse; margin-top:1%; font-family:Georgia, Helvetica, sans-serif; ">
                    	<tr>
                        	<td style="width:25%;border-left:none;">Plant/Ratoon</td>
                            <td style="width:25%;"> &nbsp;{{item.plantorratoon}}</td>
                            <td style="width:20%;">Admn.No.</td>
                            <td style="width:30%;">&nbsp;  </td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Variety</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.variety}}</td>
                            <td style="width:25%;">Time</td>
                            <td style="width:25%;">&nbsp;</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Circle Code</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.circlecode}}</td>
                            <td style="width:25%;">Shift</td>
                            <td style="width:25%;"><input type="checkbox" >A <input type="checkbox" > B <input type="checkbox" > C</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Land Village Code</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.landvilcode}}</td>
                            <td style="width:25%;">Vehicle Regn.</td>
                            <td style="width:25%;">&nbsp;</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Program & Rank No.</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{item.programno}} & {{item.rank}} </td>
                            <td style="width:25%;">Vehicle No.</td>
                            <td  style="width:25%;">&nbsp;</td>                            
                        </tr>
                        <tr>
                        	<td style="width:25%;border-left:none;">Vehicle Type</td>
                            <td colspan="3"  style="" ><input type="checkbox" >Tractor <input type="checkbox" >Small Lorry <input type="checkbox" >Big Lorry</td>
                        </tr>
                        
                    </table>
                    </div>
					
				<table width="100%"  class="test" style="font-size:14px;font-weight:bold;font-family:Georgia, Helvetica, sans-serif; ">
				<tr  style="text-align:left; width:25%;"><td></td>
					<td style="text-align:left; width:25%;"></td>
					<td  style="text-align:right; width:25%;"></td>
					<td  style="text-align:right; width:25%;"><img src="img/images/Cane Manager Signature.png" style="width:40%; height:30px; margin-right:50px;" /></td>			
					
					</tr>
					<tr  style="text-align:left; width:25%;"><td>En.Clerk</td>
					<td style="text-align:left; width:25%;">W.B.Operator</td>
					<td  style="text-align:right; width:25%;">Agrl.Officer</td>
					<td  style="text-align:right; width:25%;">CANE MANAGER</td>			
					
					</tr>
					
					
														
				</table>
				</div>
			</div>
			<br /><br />
			
			
			
			
			
		<div class="footer" ng-if="items[$index+1].id%3=='0'"></div>	
	    </div>
				
				</div>-->
				
	    </section>   
	</section>
	
	

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
