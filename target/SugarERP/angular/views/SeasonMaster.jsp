	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeasonMaster" data-ng-init='loadSeasonId(); loadSeasonDetails();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Season Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="SeasonMasterForm" ng-submit='SeasonSubmit(AddedSeasons,SeasonMasterForm);' novalidate>
					 		<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper">
					        	               		 <div class="fg-line">
												<label for="seasonid">Season ID</label>
        		    					          <input type="text" class="form-control" placeholder="Season ID"  maxlength="10" readonly name="seasonId" tabindex="1" data-ng-model='AddedSeasons.seasonId' id="seasonid" with-floating-label />
			    		            	       		 </div>
												</div>
			                    			</div>
										</div>
									</div><br />
																		
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group floating-label-wrapper">
			        	                					<div class="fg-line">
															<label for="seasonyear">Season Year</label>
            					          				<input type="text" class="form-control" placeholder="Season Year"  maxlength="50" tabindex="4" name="season" data-ng-model='AddedSeasons.season' readonly="readonly" id="seasonyear" with-floating-label />
			                	        				</div>
														<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedSeasons.season!=null">Season Year Already Exist.</p>
													</div>
			                    				</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-4">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeasonMasterForm.fromYear.$invalid && (SeasonMasterForm.fromYear.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		
														<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedSeasons.fromYear' name="fromYear" ng-options="fromYear.fromYear as fromYear.fromYear for fromYear in fromYear"  ng-change="loadFromYear();">
															<option value="" selected="selected">Select From Year</option>
				        					            </select>
					                	        	</div>
		<p ng-show="SeasonMasterForm.fromYear.$error.required && (SeasonMasterForm.fromYear.$dirty || Addsubmitted)" class="help-block">Select From Year</p>																									
												</div>							
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SeasonMasterForm.description.$invalid && (SeasonMasterForm.description.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="decsription">Description</label>
													
    	        								         <input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="5" name="description" data-ng-model='AddedSeasons.description'  id="decsription" with-floating-label  ng-blur="spacebtw('description');" />														 
				                				    </div>
																					
												</div>
					                    	</div>
										</div>
									</div>								
																											
																																				
								</div>
								<div class="col-sm-4">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeasonMasterForm.toYear.$invalid && (SeasonMasterForm.toYear.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100" tabindex="3" name="toYear"  data-ng-model='AddedSeasons.toYear' ng-change="loadToYear();"     ng-value="validateDup();"  data-ng-options="toyear.toYear as toyear.toYear for toyear in toYrList"  data-ng-required="true">
															<option value="">Select To Year</option>
				        					            </select>
					                	        	</div>
								<p ng-show="SeasonMasterForm.toYear.$error.required && (SeasonMasterForm.toYear.$dirty || Addsubmitted)" class="help-block">Select To Year</p>																									
												</div>							
					                    	</div>
										</div>
									</div>
																	
																											
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"    data-ng-model='AddedSeasons.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"   data-ng-model='AddedSeasons.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>																											
								</div>
							</div><br />		
							<input type="hidden"  name="modifyFlag" data-ng-model="AddedSeasons.modifyFlag"  />							
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(SeasonMasterForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->
							 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Seasons</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">
											<form name="EditSeasonMasterForm" novalidate>				
							        <table ng-table="SeasonMaster.tableEdit" class="table table-striped table-vmiddle">
									
													<thead>
											        <tr>
											           <th><span>Action</span></th>
													   <th><span>Season ID</span></th>
													   <th><span>From Year</span></th>
													   <th><span>To Year</span></th>
													    <th><span>Season Year</span></th>
														 <th><span>Description</span></th>
													   <th><span>Status</span></th>
											        </tr>
											      </thead>
											      <tbody>		
										
								        <tr ng-repeat="Season in SeasonData"  ng-class="{ 'active': Season.$edit }">
                		    				
											<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Season.$edit" ng-click="Season.$edit = true;Season.modifyFlag='Yes'"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="Season.$edit" ng-click="UpdateSeasons(Season,$index);Season.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Season.modifyFlag"  />
		                    				 </td>
											
											 
							                 <td>
                		    					<span ng-if="!Season.$edit">{{Season.seasonId}}</span>
					    		                <div ng-if="Season.$edit"><input class="form-control" type="text" ng-model="Season.seasonId" placeholder='Season ID' maxlength="10" readonly/ name="seasonId{{$index}}" size="5"></div>
					            		      </td>
		                    				  
							                  <td>
							                      <span ng-if="!Season.$edit">{{Season.fromYear}}</span>
        		            					  <div ng-if="Season.$edit">
												 <select class="form-control1"  ng-model='Season.fromYear' ng-options="fromY.fromYear as fromY.fromYear for fromY in fromYear" name="fromYear{{$index}}" ng-change="loadFromYearEdit(Season.fromYear,$index);" >	
												 	<option value="">{{Season.fromYear}}</option>
												 </select>
														
														
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!Season.$edit">{{Season.toYear}}</span>
        		            					  <div ng-if="Season.$edit">
												  	<select class="form-control1" ng-model='Season.toYear' ng-options="toYear.id as toYear.fromYear for toYear in fromYearNames" name="toYear{{$index}}">	
														<option value="">{{Season.toYear}}</option>
													</select>
													
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!Season.$edit">{{Season.season}}</span>
        		            					  <div ng-if="Season.$edit">
												  	 
													 <input class="form-control" type="text"  placeholder='Season Year' maxlength="10" name="season{{$index}}" size="5" readonly="readonly" ng-model="Season.season"  ng-blur=""/>
													 <p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Season.season!=null"> Already Exist.</p>	
												  </div>
												 <p style="display:none;"> Seasonupdyear.seasonyearchange{{$index}}</p>
							                  </td>
											  <td>
							                      <span ng-if="!Season.$edit">{{Season.description}}</span>
        		            					  <div ng-if="Season.$edit">
												  	 <input class="form-control" type="text" ng-model="Season.description" placeholder='Description' maxlength="50" name="description{{$index}}"  ng-blur="spacebtwgrid('description',$index)" />
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!Season.$edit">
												  		<span ng-if="Season.status=='0'">Active</span>
														<span ng-if="Season.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Season.$edit">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  data-ng-model='Season.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='Season.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>						 							 												  
												  </div>
							                  	</td>											  
							             	</tr>
										 </tbody>
								        </table>
									 </form>
								 </div>
							</section>							 
						</div>
					</div>
				</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
