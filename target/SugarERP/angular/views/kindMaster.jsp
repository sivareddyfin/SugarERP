	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="kindMasterController" data-ng-init="loadKindStatus();getallKinds();loadAdvanceNames();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Kind Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="kindForm" ng-submit='AddKindMasterSubmit(AddedKind,kindForm);' novalidate>
					 		<div class="row">
							<input type="hidden" ng-model="AddedKind.modifyFlag">
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : kindForm.kindType.$invalid && (kindForm.kindType.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="categoryName">Kind Type</label>
    	        								         <input type="text" class="form-control autofocus" autofocus placeholder="Kind Type"  maxlength="25"  name="kindType" data-ng-model='AddedKind.kindType' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="1" ng-blur="spacebtw('kindType');validateDup();" id="categoryName" with-floating-label>														 
				                				    </div>
								<p ng-show="kindForm.kindType.$error.required && (kindForm.kindType.$dirty || Addsubmitted)" class="help-block">Kind Type is required</p>
								<p ng-show="kindForm.kindType.$error.pattern  && (kindForm.kindType.$dirty || Addsubmitted)" class="help-block"> Valid Kind Type is required</p>		
																			
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="form-group" ng-class="{ 'has-error' : kindForm.uom.$invalid && (kindForm.uom.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="prefix">UOM</label>
    	        								         <input type="text" class="form-control " placeholder="UOM"    name="uom" data-ng-model='AddedKind.uom' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="3"  id="prefix" with-floating-label>														 
				                				    </div>
								<p ng-show="kindForm.uom.$error.required && (kindForm.uom.$dirty || Addsubmitted)" class="help-block">UOM is required</p>
							
																	
												</div>
			                    				</div>
										</div>
									</div>	
								
																		
																		
								</div>
								<div class="col-sm-6">
																	
											<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="fg-line">
													<label for="description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50"  name="description" data-ng-model='AddedKind.description' tabindex="2"  id="description" with-floating-label>
			                	        			</div>
			                    				</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:27px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  data-ng-model='AddedKind.status' tabindex="5">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"  data-ng-model='AddedKind.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>
																									
																																				
								</div>
							</div>
							
							<div class="row">
						
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : kindForm.limitperAcre.$invalid && (kindForm.limitperAcre.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="limitperAcre">Limit Per acre</label>
    	        								         <input type="number" class="form-control autofocus" autofocus placeholder="Limit Per acre"  maxlength="25"  name="limitperAcre" data-ng-model='AddedKind.limitperAcre' data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" tabindex="4" ng-blur="spacebtw('limitperAcre');" id="limitperAcre" with-floating-label>														 
				                				    </div>
								<p ng-show="kindForm.limitperAcre.$error.required && (kindForm.limitperAcre.$dirty || Addsubmitted)" class="help-block">Limit per acre is required</p>
								<p ng-show="kindForm.limitperAcre.$error.pattern  && (kindForm.limitperAcre.$dirty || Addsubmitted)" class="help-block"> Valid Acre is required</p>		
																			
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="form-group" ng-class="{ 'has-error' : kindForm.costofeachItem.$invalid && (kindForm.costofeachItem.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="costofeachitem">Cost of Each Item</label>
    	        								         <input type="number" class="form-control " placeholder="Cost of Each Item"    name="costofeachItem" data-ng-model='AddedKind.costofeachItem' data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" tabindex="5"  id="costofeachitem" with-floating-label>														 
				                
												    </div>
								<p ng-show="kindForm.costofeachItem.$error.required && (kindForm.costofeachItem.$dirty || Addsubmitted)" class="help-block">Cost is required</p>
							<p ng-show="kindForm.costofeachItem.$error.pattern && (kindForm.costofeachItem.$dirty || Addsubmitted)" class="help-block">Enter Valid Cost</p>
																	
												</div>
			                    				</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="form-group" ng-class="{ 'has-error' : kindForm.plantMaxCount.$invalid && (kindForm.plantMaxCount.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="plant">Plant Max Bag</label>
    	        								         <input type="number" class="form-control " placeholder="Plant Max Bag"    name="plantMaxCount" data-ng-model='AddedKind.plantMaxCount' data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" tabindex="5"  id="plant" with-floating-label>														 
				                
												    </div>
								<p ng-show="kindForm.plantMaxCount.$error.required && (kindForm.plantMaxCount.$dirty || Addsubmitted)" class="help-block">Maxlimit for Plant is required</p>
							<p ng-show="kindForm.plantMaxCount.$error.pattern && (kindForm.plantMaxCount.$dirty || Addsubmitted)" class="help-block">Enter Valid  Cost</p>
																	
												</div>
			                    				</div>
										</div>
									</div>	
									
																		
																		
								</div>
								<div class="col-sm-6">
																	
											<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Rounding to :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="roundingTo" value="0"  data-ng-model='AddedKind.roundingTo' tabindex="5">
			    					    				<i class="input-helper"></i>Always Lower
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="roundingTo" value="1"  data-ng-model='AddedKind.roundingTo'>
					    								<i class="input-helper"></i>Nearest Integer
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : kindForm.advance.$invalid && (kindForm.advance.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedKind.advance' name="advance" ng-options="advance.id as advance.advance for advance in advanceNames | orderBy:'-advance':true" >
															<option value="">Advance</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="kindForm.advance.$error.required && (kindForm.advance.$dirty || Addsubmitted)" class="help-block">Select Advance</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
																									
										<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="form-group" ng-class="{ 'has-error' : kindForm.ratoon.$invalid && (kindForm.ratoon.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="ratoon">Ratoon</label>
    	        								         <input type="number" class="form-control " placeholder="Ratoon Max bag"    name="ratoon" data-ng-model='AddedKind.ratoonMaxCount' data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" tabindex="6"  id="ratoon" with-floating-label>														 
				                
												    </div>
								<p ng-show="kindForm.ratoon.$error.required && (kindForm.ratoon.$dirty || Addsubmitted)" class="help-block">Maxlimit for Ratoon is required</p>
							<p ng-show="kindForm.ratoon.$error.pattern && (kindForm.ratoon.$dirty || Addsubmitted)" class="help-block">Enter Valid  Cost</p>
																	
												</div>
			                    				</div>
										</div>
									</div>																										
								</div>
							</div>
							
							
							<br />	
											
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="button" class="btn btn-primary" ng-click="reset(kindForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Kinds</b></h2></div>
					
					<div class="card">						
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 	
								<form name="kindMasterEdit" novalidate>					
							        <table ng-table="kindMaster.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Kind Type</span></th>
																<th><span>UOM</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																<th><span>Limit per Acre</span></th>
																<th><span>Plant</span></th>
																<th><span>Ratoon</span></th>
																<th><span>Rounding of</span></th>
																<th><span>Each Item Cost</span></th>
																<th><span>Advance</span></th>
																
																</tr>
														</thead>
														<tbody>
								        <tr ng-repeat="kind in kindData"  ng-class="{ 'active': kind.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!kind.$edit" ng-click="kind.$edit = true;kind.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="kind.$edit" ng-click="updateKindMaster(kind,$index);kind.$edit = isEdit"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="kind.modifyFlag"  />
											   
		                    				 </td>
							                  <td>
                		    					<span ng-if="!kind.$edit">{{kind.kindType}}</span>
					    		                <div ng-if="kind.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="kind.kindType" placeholder='KindType' maxlength="10" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!kind.$edit">{{kind.uom}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : kindMasterEdit.uom{{index}}.$invalid && (kindMasterEdit.uom{{index}}.$dirty || submitted)}">
							                     <div ng-if="kind.$edit">
												 	<input class="form-control" type="text" ng-model="kind.uom" placeholder='UOM' maxlength="25" name="uom{{index}}" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" ng-required='true'/>
													<p ng-show="kindMasterEdit.uom{{index}}.$error.required && (kindMasterEdit.uom{{index}}.$dirty || submitted)" class="help-block">UOM is required.</p>
													<p ng-show="kindMasterEdit.uom{{index}}.$error.pattern && (kindMasterEdit.uom{{index}}.$dirty || submitted)" class="help-block"> Valid UOM is required.</p>
													
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!kind.$edit">{{kind.description}}</span>
        		            					  <div ng-if="kind.$edit">
												  	<div class="form-group">
													  	<input class="form-control" type="text" ng-model="kind.description" placeholder='Description' maxlength="50" name="description"/>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!kind.$edit">
												  	<span ng-if="kind.status=='0'">Active</span>
													<span ng-if="kind.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="kind.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  ng-model="kind.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="kind.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
											  
											  <td>
                		    					<span ng-if="!kind.$edit">{{kind.limitperAcre}}</span>
					    		                <div ng-if="kind.$edit">
													<div class="form-group">
														<input class="form-control1" type="number" ng-model="kind.limitperAcre" placeholder='Limit per Acre' maxlength="10" />
													</div>
												</div>
					            		      </td>
											  
											  
											  <td>
							                     <span ng-if="!kind.$edit">{{kind.plantMaxCount}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : kindMasterEdit.plantMaxCount{{index}}.$invalid && (kindMasterEdit.plantMaxCount{{index}}.$dirty || submitted)}">
							                     <div ng-if="kind.$edit">
												 	<input class="form-control1" type="text" ng-model="kind.plantMaxCount" placeholder='Plant' maxlength="25" name="plantMaxCount{{index}}" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required='true'/>
													<p ng-show="kindMasterEdit.plantMaxCount{{index}}.$error.required && (kindMasterEdit.plantMaxCount{{index}}.$dirty || submitted)" class="help-block">Plant Cost is required.</p>
													<p ng-show="kindMasterEdit.plantMaxCount{{index}}.$error.pattern && (kindMasterEdit.plantMaxCount{{index}}.$dirty || submitted)" class="help-block"> Valid Cost is required.</p>
													
													</div>
												 </div>
							                  </td>
											  
											  <td>
							                     <span ng-if="!kind.$edit">{{kind.ratoonMaxCount}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : kindMasterEdit.ratoon{{index}}.$invalid && (kindMasterEdit.ratoon{{index}}.$dirty || submitted)}">
							                     <div ng-if="kind.$edit">
												 	<input class="form-control1" type="text" ng-model="kind.ratoonMaxCount" placeholder='Ratoon' maxlength="25" name="ratoon{{index}}" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required='true'/>
													<p ng-show="kindMasterEdit.ratoon{{index}}.$error.required && (kindMasterEdit.ratoon{{index}}.$dirty || submitted)" class="help-block">Ratoon Cost is required.</p>
													<p ng-show="kindMasterEdit.ratoon{{index}}.$error.pattern && (kindMasterEdit.ratoon{{index}}.$dirty || submitted)" class="help-block"> Valid Cost is required.</p>
													
													</div>
												 </div>
							                  </td>
											  
		                    				  <td>
							                      <span ng-if="!kind.$edit">
												  	<span ng-if="kind.roundingTo=='0'">Always Lower</span>
													<span ng-if="kind.roundingTo=='1'">Nearest Integer</span>
												  </span>
        		            					  <div ng-if="kind.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="roundingTo{{$index}}" value="0"  ng-model="kind.roundingTo">
			    					    				<i class="input-helper"></i>Always Lower
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="roundingTo{{$index}}" value="1" ng-model="kind.roundingTo">
			    										<i class="input-helper"></i>Nearest Integer
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
		                    				  <td>
							                     <span ng-if="!kind.$edit">{{kind.costofeachItem}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : kindMasterEdit.costofeachItem{{index}}.$invalid && (kindMasterEdit.costofeachItem{{index}}.$dirty || submitted)}">
							                     <div ng-if="kind.$edit">
												 	<input class="form-control1" type="text" ng-model="kind.costofeachItem" placeholder='Cost of each Item' maxlength="25" name="costofeachItem{{index}}" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required='true'/>
													<p ng-show="kindMasterEdit.costofeachItem{{index}}.$error.required && (kindMasterEdit.costofeachItem{{index}}.$dirty || submitted)" class="help-block">Cost is required.</p>
													<p ng-show="kindMasterEdit.costofeachItem{{index}}.$error.pattern && (kindMasterEdit.costofeachItem{{index}}.$dirty || submitted)" class="help-block"> Valid Cost is required.</p>
													
													</div>
												 </div>
							                  </td>
							                  
							                     
												  
												
												   
							                  <td>
																	  <span ng-if="!kind.$edit" ng-repeat='period in advanceNames'>
  																		<span ng-if="period.id==kind.advance">{{period.advance}}</span>
																		</span>
																		 
																		 <div ng-if="kind.$edit">
																			<div class="form-group">
																				<select class='form-control1' ng-model="kind.advance" name="advance{{$index}}" ng-options="advance.id as advance.advance for advance in advanceNames | orderBy:'-advance':true">	</select>
																			</div>
																		</div>
																	  </td>
											  
							             </tr>
										 </tbody>
										 
								         </table>
										 
										 </form>
										 </div>
										 </section>							 
							     </div>
							</div>
						</div>
					<!----------table grid design end--------->
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
