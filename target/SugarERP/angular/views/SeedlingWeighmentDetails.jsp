<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeedlingWeighmentController" ng-init="loadRyotCodeDropDown();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Weighment Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
					<form name="SeedlingWeighmentform" novalidate ng-submit=''>
						<div class="row">
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingWeighmentform.fromDate.$invalid && (SeedlingWeighmentform.fromDate.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<label for="fromDate">From Date</label>
												<input type="text" class="form-control datepicker autofocus" placeholder='From Date' data-ng-required='true'  tabindex="1" name="fromDate"  data-input-mask="{mask: '00-00-0000'}"  id="fromDate" with-floating-label  maxlength="10" ng-model="Weighment.fromDate"/>
											</div>
											<p ng-show="SeedlingWeighmentform.fromDate.$error.required && (SeedlingWeighmentform.fromDate.$dirty || submitted)" class='help-block'>From Date is required </p>				
										</div>
			                    </div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingWeighmentform.toDate.$invalid && (SeedlingWeighmentform.toDate.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<label for="toDate">To Date</label>
												<input type="text" class="form-control datepicker" placeholder='To Date'  tabindex="2" name="toDate"  data-input-mask="{mask: '00-00-0000'}"  id="toDate" with-floating-label  maxlength="10" ng-model="Weighment.toDate" data-ng-required='true'/>
											</div>
											<p ng-show="SeedlingWeighmentform.toDate.$error.required && (SeedlingWeighmentform.toDate.$dirty || submitted)" class='help-block'>To Date is required </p>					
										</div>
			                    </div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingWeighmentform.seedSupplier.$invalid && (SeedlingWeighmentform.seedSupplier.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="seedSupplier"  data-ng-model="Weighment.seedSupplier" ng-options="seedSupplier.seedSupplier as seedSupplier.seedSupplier for seedSupplier in seedSupplierNames  | orderBy:'-seedSupplier':false" tabindex="3" data-ng-required='true'>
													<option value=''>Seed Supplier</option>
												</select>	
											</div>
											<p ng-show="SeedlingWeighmentform.seedSupplier.$error.required && (SeedlingWeighmentform.seedSupplier.$dirty || submitted)" class='help-block'>Select Seed Supplier</p>					
										</div>
			                    </div>
							</div>
							
							<div class="col-sm-3">
								<div class="input-group">
								<div class="fg-line">
									<button type="button" class="btn btn-primary" ng-click="generateSeedlingWeighmentDetails(Weighment.fromDate,Weighment.toDate,Weighment.seedSupplier);">Generate Report</button>
								<!--	<button type="reset" class="btn btn-primary">Reset</button>  -->
								</div>
							</div>
							</div>
							
						</div>
							</form>
							
					
					<div id="dispSeedlingWeighmentDetails" style="display:none;">
					<h4 align="center">Source Seed Supplier Details</h4>	
					<table  class="table table-striped table-vmiddle"  style="border-collapse:collapse; width:100%;">
							<tr>
							<th>SNO</th>
							<th>Date</th>
							<th>Ryot Code</th>
							<th>Final Weight</th>
							<th>Vehicle No.</th>
							</tr>
							<tr ng-repeat="weighment in weighmentData">
								<td>{{$index+1}}</td>
								<td>{{weighment.Date}}</td>
								<td>{{weighment.seedSupplier}}</td>
								<td>{{weighment.finalweight}}</td>
								<td>{{weighment.vehicalNo}}</td>
							</tr>
							</table>
		      		</div>
			 			
				</div>		 							 
					<!------------------form end---------->	
					
					
											 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



