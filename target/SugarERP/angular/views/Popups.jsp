	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		input
		{
		color:#000000;
		font-size:18px;
		font-weight:bold;
		}
	</style>

	<script type="text/javascript">
	var rowCount = 0;
	function addMoreRows(frm) 
	{
		rowCount ++;
		$('#tab_logic').append('<tr id="addr'+rowCount+'"></tr>');
		$('#addr'+rowCount).html('<td width="100px;"><button type="button" id="right_All_1" class="btn btn-primary" onclick="removeRow('+ rowCount +');"><i class="glyphicon glyphicon-remove-sign"></i></button></td><td width="150px;"><select class="form-control1"><option value="">Tray Type</option></select></td><td width="150px;"><input type="text" class="form-control1" placeholder="No.of Trays" maxlength="8"/></td><td width="150px;"><input type="text" class="form-control1" placeholder="Cost" maxlength="8"/></td><td width="150px;"><input type="text" class="form-control1"  placeholder="Total Cost" maxlength="5"/></td><td width="150px;"><select class="form-control1"><option value="">Status</option><option value="">Damaged</option><option value="">Lost</option></select></td>');
	}

	function removeRow(removeNum) 
	{	
		jQuery('#addr'+removeNum).remove();
	}	
	</script>
	
	
    
	
	
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Popup in Offer Screen</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
							<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Season' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Code' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Name' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>							
							</div><br />							
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 <table class="table table-striped table-vmiddle" id="tab_logic">
							 		<thead>
								        <tr style="background-color:#FFFFFF;">
                		    				<th>Select</th>
											<th>Agreement No.</th>
							                <th>Plot No.</th>
		                    				<th>Date of Harvesting</th>
            							</tr>											
									</thead>
									<tbody>
										<tr id="addr0">
											<td>
												<div class="checkbox">
									                <label>
									                    <input type="checkbox" value="">
									                    <i class="input-helper"></i>
													</label>
									            </div>
											</td>
											<td>81</td>
											<td>123</td>
											<td>12-02-2015</td>
										</tr>
										<tr id="addr0">
											<td>
												<div class="checkbox">
									                <label>
									                    <input type="checkbox" value="">
									                    <i class="input-helper"></i>
													</label>
									            </div>											
											</td>
											<td>82</td>
											<td>124</td>
											<td>13-02-2015</td>
										</tr>
										<tr id="addr0">
											<td>
												<div class="checkbox">
									                <label>
									                    <input type="checkbox" value="">
									                    <i class="input-helper"></i>
													</label>
									            </div>											
											</td>
											<td>83</td>
											<td>125</td>
											<td>14-02-2015</td>
										</tr>
										
							 </table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/CompanyAdvanceIntrest" class="btn btn-primary btn-sm m-t-10">Add</a>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
				<!---------Recommendations--------->					
				<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Popup for Recommendations</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
						<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Code' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Name' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div> 
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Survey Number' readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>							
							</div><br />
							
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="col-sm-6">
				              <div class="input-group">
            				     <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	         <div class="fg-line">							
									<textarea class="form-control" rows="10" cols="30" autofocus placeholder='Recommendations'></textarea>
								</div>
							  </div>
							</div>
							<div class="col-sm-2"></div>
						</div><br />	
						<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/CompanyAdvanceIntrest" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/CompanyAdvanceIntrest" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>							
					</div>
				</div>
				<!---------Post Season Calculations--------->					
				<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Popup in Post Season Calculations</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
						<div class="table-responsive">
							<table class="table table-striped table-vmiddle">
								<thead>
									<tr>
										<th>Ryot Code</th>
										<th>Ryot Name</th>
										<th>Loan</th>
										<th>Advances</th>
										<th>Total Recovery</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><a href="#/admin/admindata/masterdata/Popup">123</a></td>
										<td>Ramarao</td>
										<td>1200</td>
										<td>1200</td>
										<td>2400</td>
									</tr>
									<tr>
										<td><a href="#/admin/admindata/masterdata/Popup">124</a></td>
										<td>Ramu</td>
										<td>1000</td>
										<td>1000</td>
										<td>2000</td>
									</tr>
									<tr>
										<td><a href="#/admin/admindata/masterdata/Popup">125</a></td>
										<td>Raghu</td>
										<td>2200</td>
										<td>2200</td>
										<td>4400</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</div>
				</div>				
									
			</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
