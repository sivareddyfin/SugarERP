	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CategoryMasterController" data-ng-init="loadCategoryStatus();loadgetAllCategory();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Category Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="CategoryMasterForm" ng-submit='AddCategoryMasterSubmit(AddedCategory,CategoryMasterForm);' novalidate>
					 		<div class="row">
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CategoryMasterForm.categoryName.$invalid && (CategoryMasterForm.categoryName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="categoryName">Category Name</label>
    	        								         <input type="text" class="form-control autofocus" autofocus placeholder="Category Name"  maxlength="25"  name="categoryName" data-ng-model='AddedCategory.categoryName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="1" ng-blur="spacebtw('categoryName');validateDup();" id="categoryName" with-floating-label>														 
				                				    </div>
								<p ng-show="CategoryMasterForm.categoryName.$error.required && (CategoryMasterForm.categoryName.$dirty || Addsubmitted)" class="help-block">Category Name is required</p>
								<p ng-show="CategoryMasterForm.categoryName.$error.pattern  && (CategoryMasterForm.categoryName.$dirty || Addsubmitted)" class="help-block"> Valid Category  Name is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedCategory.categoryName!=null">Category Name Already Exist.</p>											
												</div>
					                    	</div>
										</div>
									</div>
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="form-group" ng-class="{ 'has-error' : CategoryMasterForm.prefix.$invalid && (CategoryMasterForm.prefix.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="prefix">Category prefix</label>
    	        								         <input type="text" class="form-control " placeholder="Category Prefix"  maxlength="25"  name="prefix" data-ng-model='AddedCategory.prefix' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="3" ng-blur="spacebtw('prefix');validateDup();" id="prefix" with-floating-label>														 
				                				    </div>
								<p ng-show="CategoryMasterForm.prefix.$error.required && (CategoryMasterForm.prefix.$dirty || Addsubmitted)" class="help-block">Category prefix is required</p>
								<p ng-show="CategoryMasterForm.prefix.$error.pattern  && (CategoryMasterForm.prefix.$dirty || Addsubmitted)" class="help-block"> Valid Category  prefix is required</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedCategory.prefix!=null">Category prefix Already Exist.</p>											
												</div>
			                    				</div>
										</div>
									</div>	
									<br />
																		
																		
								</div>
								<div class="col-sm-6">
																	
											<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="fg-line">
													<label for="description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50"  name="description" data-ng-model='AddedCategory.description' tabindex="2" ng-blur="spacebtw('description');" id="description" with-floating-label>
			                	        			</div>
			                    				</div>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  data-ng-model='AddedCategory.status' tabindex="5">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"  data-ng-model='AddedCategory.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>
																									
																																				
								</div>
							</div>
							
							
							<br />	
											
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(CategoryMasterForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Categories</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
										<form name="EditCategoryMasterFrom" novalidate>					
							        <table ng-table="CategoryMasterForm.tableEdit" class="table table-striped table-vmiddle">
											<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Category Names</span></th>
													<th><span>Description</span></th>
													<th><span>Category Prefix</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="category in CategoryData"  ng-class="{ 'active': category.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!category.$edit" ng-click="category.$edit = true;"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="category.$edit" ng-click="UpdateCategory(category,$index);category.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											 
		                    				 </td>
							                 <td>
                		    					<span ng-if="!category.$edit">{{category.categoryName}}</span>
					    		                <div ng-if="category.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="category.categoryName" placeholder='Category Name' maxlength="10" name="categoryName{{$index}}" readonly/>
													</div>
												</div>
					            		      </td>
											  
		                    				  <td>
							                     <span ng-if="!category.$edit">{{category.description}}</span>
							                     <div ng-if="category.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditCategoryMasterFrom.description{{$index}}.$invalid && (EditCategoryMasterFrom.description{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="category.description" placeholder='Description' maxlength="25" name="description{{$index}}" />
			<p ng-show="EditCategoryMasterFrom.description{{$index}}.$error.required && (EditCategoryMasterFrom.description{{$index}}.$dirty || submitted)" class="help-block"> Description is required</p>
																

</div>
												 </div>
							                  </td>
											  
											  <td>
							                     <span ng-if="!category.$edit">{{category.prefix}}</span>
							                     <div ng-if="category.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditCategoryMasterFrom.prefix{{$index}}.$invalid && (EditCategoryMasterFrom.prefix{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="category.prefix" placeholder='Category Prefix' maxlength="25" name="prefix{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('prefix',$index);"/>
			<p ng-show="EditCategoryMasterFrom.prefix{{$index}}.$error.required && (EditCategoryMasterFrom.prefix{{$index}}.$dirty || submitted)" class="help-block"> Category Prefix Name is required</p>
			<p ng-show="EditCategoryMasterFrom.prefix{{$index}}.$error.pattern  && (EditCategoryMasterFrom.prefix{{$index}}.$dirty || submitted)" class="help-block">Enter a valid  Category Prefix Name.</p>
			<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="category.prefix!=null">Category Prefix Name Already Exist.</p>													

</div>
												 </div>
							                  </td>
							                  
											  
											  
											  <td style="width:235px;">
							                     
												  <span ng-if="!category.$edit">
												  		<span ng-if="category.status=='0'">Active</span>
														<span ng-if="category.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="category.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  data-ng-model='category.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='category.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>											  
							             </tr>
								         </table>	
									</form>
									</div>
									</section>						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
