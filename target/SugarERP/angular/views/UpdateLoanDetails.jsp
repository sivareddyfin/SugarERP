	<script type="text/javascript">
		$('#season').focus();
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="Updateloandetails" ng-init="loadSeasonNames();loadBranchNames();">        
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Loan Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="UpdateLoanForm" ng-submit="Updateloansubmit(AddedUpdateLoan,UpdateLoanForm)" novalidate>
 								<div class="row">
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : UpdateLoanForm.season.$invalid && (UpdateLoanForm.season.$dirty || Filtersubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddedUpdateLoan.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" data-ng-required='true' id="season">
<option value="">Select Season</option>

                    </select>
                	            	   </div>
                	        </div>
							<p ng-show="UpdateLoanForm.season.$error.required && (UpdateLoanForm.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
			
			
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : UpdateLoanForm.branchcode.$invalid && (UpdateLoanForm.branchcode.$dirty || Filtersubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="branchcode" data-ng-model="AddedUpdateLoan.branchcode" ng-options="branchcode.id as branchcode.branchname for branchcode in BankNames | orderBy:'-branchname':true" data-ng-required='true'>
							<option value="">Select Branch</option>

                   							 </select>
                	            	   </div>
                	        </div>
							        <p ng-show="UpdateLoanForm.branchcode.$error.required && (UpdateLoanForm.branchcode.$dirty || Filtersubmitted)" class="help-block">Select Branch</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
			
			
							<div class="col-sm-3">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getUpdateLoanDetails(UpdateLoanForm);">Get Update Loan Details</button>

													</div>
												</div>
  										</div>
										
										
										
										
										
									</div>
							<div class="row">
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file m-r-10">
                            <span class="fileinput-new">Export Excel</span>
                            <span class="fileinput-exists">Change</span>
                          <input type="button" ng-click="exportData()"/>
 
 
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                    </div>
</div>
<div class="col-md-6">
<div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file m-r-10">
                            <span class="fileinput-new">Import Excel</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" accept=".xls,.xlsx,.ods" fileread=""   UpdateLoanData="" multiple="false"   >
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                    </div>
</div>
</div>
</div>
</div>
					     
			 <hr />	
				<!--<hr />-->
		<input type="hidden"  data-ng-model="flag" />
						
							<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
													
							        <table ng-table="Updateloandetails.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>sl.no</span></th>
													<th><span>Ryot Code</span></th>
													<th><span>Ryot Name</span></th>
													<th><span>Loan Sl. No.</span></th>
													<th><span>Recommended<br /> Date</span></th>
													<th><span>Recommended <br />Amount</span></th>
													<th><span>Loan A/C No.</span></th>
													<th><span>DATE OF<br /> DISBURSEMENT</span></th>
													<th><span>DISBURSED<br /> AMOUNT</span></th>
													
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="UpdateLoan in UpdateLoanData"  ng-class="{ 'active': UpdateLoan.$edit }">
                		    				<td>
                		    					{{$index+1}}
					    		               
					            		      </td>
		                    				  
							                  <td>
							                     {{UpdateLoan.ryotCode}}
        		            					 
							                  </td>
											  <td>
							                      {{UpdateLoan.ryotName}}
        		            					 
							                  </td>
											  <td>
							                      {{UpdateLoan.loanNumber}}
												  
        		            					 
							                  </td>
											  <td>
							                      {{UpdateLoan.recommendedDate}}
        		            					 
							                  </td>
											  <td>
							                      {{UpdateLoan.paidAmount}}
        		            					 
							                  </td>
											  <td>
							                      <div class="form-group" ng-class="{ 'has-error' : UpdateLoanForm.referenceNumber{{$index}}.$invalid && (UpdateLoanForm.referenceNumber{{$index}}.$dirty || submitted)}">
        		            					         <input type="text" class="form-control1" name="referenceNumber{{$index}}" data-ng-model="UpdateLoan.referenceNumber" maxlength="16" >
		<p ng-show="UpdateLoanForm.referenceNumber{{$index}}.$error.required && (UpdateLoanForm.referenceNumber{{$index}}.$dirty || submitted)" class="help-block">Required</p>
		<!--<p ng-show="UpdateLoanForm.referenceNumber{{$index}}.$error.pattern  && (UpdateLoanForm.referenceNumber{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
		<p ng-show="UpdateLoanForm.referenceNumber{{$index}}.$error.minlength  && (UpdateLoanForm.referenceNumber{{$index}}.$dirty || submitted)" class="help-block">Too Short</p>-->
															</div>
												 
							                  </td>
											  <td>
							                     <div class="form-group">
												 <input type="text" class="form-control1 datepicker" name="disbursedDate" data-ng-model="UpdateLoan.disbursedDate"  data-input-mask="{mask: '00-00-0000'}"  ng-mouseover="ShowDatePicker();" />
												 </div>
        		            					
							                  </td>
											   <td>
							                    <div class="form-group" ng-class="{ 'has-error' : UpdateLoanForm.disbursedAmount{{$index}}.$invalid && (UpdateLoanForm.disbursedAmount{{$index}}.$dirty || submitted)}">
						<input type="text" class="form-control1" name="disbursedAmount{{$index}}" data-ng-model="UpdateLoan.disbursedAmount"  />
	<!--	<p ng-show="UpdateLoanForm.disbursedAmount{{$index}}.$error.required && (UpdateLoanForm.disbursedAmount{{$index}}.$dirty || submitted)" class="help-block">Required</p>
		<p ng-show="UpdateLoanForm.disbursedAmount{{$index}}.$error.pattern  && (UpdateLoanForm.disbursedAmount{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>-->
		                                            </div>
						
        		            					
							                  </td>
											  
											  <td>
													 <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign" ng-click='updateLoanRowWise(UpdateLoan,$index,AddedUpdateLoan);'></i>
								                    </span>
								              											
											</td>
											   
											   									  
							           </tr>
									</tbody>
								 </table>
										 		
										 	</div>
										</section> 
																 
							     </div>
								 
								  <div class="row" align="center">            			
								         <div class="input-group">
									             <div class="fg-line">
									                  <button type="submit" class="btn btn-primary btn-hide">Save</button>
									                   <button type="reset" class="btn btn-primary" ng-click="reset(UpdateLoanForm)">Reset</button>
										<!--<a href="#/admin/admindata/masterdata/UpdateLoans" class="btn btn-primary btn-sm m-t-10">Update</a>
										<a href="#/admin/admindata/masterdata/UpdateLoans" class="btn btn-primary btn-sm m-t-10">Reset</a>-->
										 </div>
									 </div>						 	
							 	</div>
							
						
					<!----------end----------------------->	
								</form>
					 		</div>
			    		</div>
			  		</div>
	    	</section>  
		</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>

							
							
							
							 
							 
							 
							 
							 
			
