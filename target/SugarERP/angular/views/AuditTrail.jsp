	<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="AuditTrail" >
        	<div class="container" >				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Audit Trail</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 
					 <form name="AuditTrailForm">
							<div class="row">
								
				                <div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<label for="fromDate">From Date</label>
												<input type="text" class="form-control datepicker autofocus" placeholder='From Date'  tabindex="1" name="fromDate"  data-input-mask="{mask: '00-00-0000'}"  id="fromDate" with-floating-label  maxlength="10" ng-model="addaudit.fromDate"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<label for="toDate">To Date</label>
												<input type="text" class="form-control datepicker autofocus" placeholder='To Date'  tabindex="1" name="toDate"  data-input-mask="{mask: '00-00-0000'}"  id="toDate" with-floating-label  maxlength="10" ng-model="addaudit.toDate"/>
										
			                	        </div>
			                    	</div>			                    
				                  </div>
								  
								  <div class="col-sm-3"><button type="button" class="btn btn-primary btn-sm m-t-10"  ng-click="loadAudittrail(addaudit,AuditTrailForm);">Get Details</button></div>
								  <div class="col-sm-3"> <button type="button" class="btn btn-primary btn-sm m-t-10"><i class="glyphicon glyphicon-print"></i>&nbsp;&nbsp;&nbsp;Print</button></div>
								  </div>								
					</form>
					</div>
					</div>
    	        			<br />
							<!---------Table Design Start--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added audit trails</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">
							 
							 
							 <div  class="table-responsive">
							 <section class="asdok">
										<div class="container1">	
							 <table ng-table="AuditTrail.tableEdit" class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
											<th><span>User Name</span></th>
                		    				<th><span>Date</span></th>
							                <th><span>Time</span></th>
											<th><span>Screen Name</span></th>
		                    				<th><span>Field ID</span></th>
											<th><span>Old Value</span></th>
											<th><span>New Value</span></th>
											<th><span>Action</span></th>
											<th><span>Remarks</span></th>
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="auditdata in data">
											<td>{{auditdata.userid}}</td>
                		    				<td>{{auditdata.audittraildate}}</td>
											<td>{{auditdata.audittrailtime}}</td>
							                <td>{{auditdata.screenname}}</td>
		                    				<td>{{auditdata.fieldid}}</td>
											<td>{{auditdata.oldvalue}}</td>
											<td>{{auditdata.newvalue}}</td>
											<td>{{auditdata.action}}</td>
											<td>{{auditdata.remarks}}</td>
            							</tr>
										<!--<tr>
											<td>makella</td>
                		    				<td>20-01-2016</td>
											<td>7:30 AM</td>
							                <td>MM1(Mandal Master)</td>
		                    				<td>Field ID1</td>
											<td>Mandal</td>
											<td>Mandal Name</td>
											<td>Update</td>
											<td>Change Label Name</td>
            							</tr>-->
									</tbody>		
							 </table>
						  </div>
						  </section>
						  </div>
						  
											           			
				        </div>
			    	</div>
					
					
				</div>
	    </section>
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
