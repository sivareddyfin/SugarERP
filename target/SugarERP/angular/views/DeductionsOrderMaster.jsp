	<style type="text/css">
		.border
		{
			border-bottom: solid 1px  #CCCCCC;
			width:100%;
			font-weight:bold;
		}		
	</style>	
	<script type="text/javascript">            
		function moveUp()
        {
            var ddl = document.getElementById('contentlist');
            //var size = ddl.length;
            //var index = ddl.selectedIndex;
            var selectedItems = new Array();
			var temp = {innerHTML:null, value:null};
            for(var i = 0; i < ddl.length; i++)
                if(ddl.options[i].selected)             
                    selectedItems.push(i);

            if(selectedItems.length > 0)    
                if(selectedItems[0] != 0)
                    for(var i = 0; i < selectedItems.length; i++)
                    {
						temp.innerHTML = ddl.options[selectedItems[i]].innerHTML;
						temp.value = ddl.options[selectedItems[i]].value;
                        ddl.options[selectedItems[i]].innerHTML = ddl.options[selectedItems[i] - 1].innerHTML;
                        ddl.options[selectedItems[i]].value = ddl.options[selectedItems[i] - 1].value;
                        ddl.options[selectedItems[i] - 1].innerHTML = temp.innerHTML; 
                        ddl.options[selectedItems[i] - 1].value = temp.value; 
                        ddl.options[selectedItems[i] - 1].selected = true;
                        ddl.options[selectedItems[i]].selected = false;
                    }
        }

        function moveDown()
        {
            var ddl = document.getElementById('contentlist');
            //var size = ddl.length;
            //var index = ddl.selectedIndex;
            var selectedItems = new Array();
			var temp = {innerHTML:null, value:null};
            for(var i = 0; i < ddl.length; i++)
                if(ddl.options[i].selected)             
                    selectedItems.push(i);

            if(selectedItems.length > 0)    
                if(selectedItems[selectedItems.length - 1] != ddl.length - 1)
                    for(var i = selectedItems.length - 1; i >= 0; i--)
                    {
                        temp.innerHTML = ddl.options[selectedItems[i]].innerHTML;
                        temp.value = ddl.options[selectedItems[i]].value;
                        ddl.options[selectedItems[i]].innerHTML = ddl.options[selectedItems[i] + 1].innerHTML;
                        ddl.options[selectedItems[i]].value = ddl.options[selectedItems[i] + 1].value;
                        ddl.options[selectedItems[i] + 1].innerHTML = temp.innerHTML; 
                        ddl.options[selectedItems[i] + 1].value = temp.value; 
                        ddl.options[selectedItems[i] + 1].selected = true;
                        ddl.options[selectedItems[i]].selected = false;
                    }
        }
			
        </script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="orderDeductionMaster" ng-init="LoadAddedDeductions();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Order Deductions</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="block-header"><b>Added Deductions</b></div>	
							<form name="OrderDeductionForm" ng-submit="SaveOrderDeductions(AddOrderDeduction);">				 		
					 			<div class="row">            			
									<div class="col-md-2"></div>
									<div class="col-md-6">            										 		
										<select name="from[]" id="contentlist" class="form-control1" size="8" multiple="multiple" autofocus ng-model="AddOrderDeduction.OrderDeduction">
											<option ng-repeat="stdDedCode in AddOrderDeductionData" value="{{stdDedCode.id}}-{{stdDedCode.name}}" class="border">{{$index+1}} - {{stdDedCode.name}}</option>
										</select>
									</div>
									<div class="col-md-1" style="vertical-align:middle;">
										<div style=" margin-top:50px;">            			
											<button type="button" id="right_All_1" class="btn btn-primary" onClick="moveUp(); return false;"><i class="glyphicon glyphicon-chevron-up"></i></button><br /><p></p>
											<button type="button" id="right_All_1" class="btn btn-primary" onClick="moveDown(); return false;"><i class="glyphicon glyphicon-chevron-down"></i></button>															
										</div>
									</div>
								</div>							
								<div class="row" align="center">            			
									<div class="input-group">
										<div class="fg-line">																		
											<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
											<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="LoadAddedDeductions();">Reset</button>
										</div>
									</div>						 	
								 </div>							 							 
							</form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
														
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
