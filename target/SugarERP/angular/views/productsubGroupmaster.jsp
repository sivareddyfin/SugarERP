		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	

	<!--<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>-->
	<section id="main" class='bannerok'>    
<!--	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>-->

    	<section id="content" data-ng-controller="productsubGroupMasterController" data-ng-init="loadStatus();loadproducts();" >      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Product Sub Group  Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="productsubGroupMaster" novalidate ng-submit="productSubgroupSubmit(AddedproductSubgroup,productsubGroupMaster);">
					
					<div class="row">
						<div class="col-sm-4">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productsubGroupMaster.groupCode.$invalid && (productsubGroupMaster.groupCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<select chosen class="w-100" autofocus  data-ng-model='AddedproductSubgroup.groupCode' data-ng-required='true' name="groupCode"   ng-options="groupCode.GroupCode as groupCode.Group for groupCode in addedProductCodes" ng-change="getProductsDetailsBygroupCode(AddedproductSubgroup.groupCode);getAllProductSubGroups();">
															<option value="">Product Group</option>
						        			            </select>
													
																					 
				                				    </div>
						<p ng-show="productsubGroupMaster.groupCode.$error.required && (productsubGroupMaster.groupCode.$dirty || Addsubmitted)" class="help-block">Group Code is required</p>
						<p ng-show="productsubGroupMaster.groupCode.$error.pattern  && (productsubGroupMaster.groupCode.$dirty || Addsubmitted)" class="help-block">Enter Valid Group Code</p>													
												</div>
					                    	</div>
											</div>
						</div>
						
						 <input type="hidden" class="form-control " placeholder="Product Group"   name="productGroup" data-ng-model='AddedproductSubgroup.group'  id="productGroup"/>
						 
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
							        	            <div class="fg-line">
													<label for="description">Description</label>
													<input type="text" class="form-control" placeholder="Description"  data-ng-model='AddedproductSubgroup.description'  name="description" id="description" with-floating-label>
													</div>
																
												</div>
					                    	</div>
											</div>
						</div>
						
						</div>
						<div class="col-sm-4">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productsubGroupMaster.subgroupCode.$invalid && (productsubGroupMaster.subgroupCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="subgroupCode">Sub Group Code</label>
															 <input type="text" class="form-control " placeholder="Sub Group Code"    maxlength="4" tabindex="2" name="subgroupCode" data-ng-model='AddedproductSubgroup.subGroupCode' data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" id="subgroupCode" with-floating-label readonly/>														 
				                				    </div>
						<p ng-show="productsubGroupMaster.subgroupCode.$error.required && (productsubGroupMaster.subgroupCode.$dirty || Addsubmitted)" class="help-block">Sub Group Code is required</p>
						<p ng-show="productsubGroupMaster.subgroupCode.$error.pattern  && (productsubGroupMaster.subgroupCode.$dirty || Addsubmitted)" class="help-block">Enter Valid Sub Group Code</p>													
												</div>
					                    	</div>
											</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            
			                        <br />
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedproductSubgroup.status" maxlength="10">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="AddedproductSubgroup.status" maxlength="10">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>
							
							
							
							
</div>
						</div>
						<div class="col-sm-4">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												
												
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productsubGroupMaster.productsubGroup.$invalid && (productsubGroupMaster.productsubGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="productsubGroup">Product Sub Group</label>
															 <input type="text" class="form-control " placeholder="Product Sub Group"    maxlength="25" tabindex="5" name="productsubGroup" data-ng-model='AddedproductSubgroup.subGroup' data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="productsubGroup" with-floating-label />														 
				                				    </div>
						<p ng-show="productsubGroupMaster.productsubGroup.$error.required && (productsubGroupMaster.productsubGroup.$dirty || Addsubmitted)" class="help-block">Product Sub Group Name is required</p>
						<p ng-show="productsubGroupMaster.productsubGroup.$error.pattern  && (productsubGroupMaster.productsubGroup.$dirty || Addsubmitted)" class="help-block">Enter Valid Subgroup Name</p>													
												</div>
					                    	</div>
											</div>
						</div>
						
						
							
						</div>
						
						
					</div>
					<br />
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="reset" class="btn btn-primary" ng-click='reset(productsubGroupMaster);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
					</form>
						
 					 									 
				   			
				        </div>
						
			    	</div>
					
					
					<!----------table grid design--------->
					<div id="hidegrid" style="display:none;">
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Product sub Group</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">		
											  <form name="updateProductSubGroupform" novalidate>
												<table ng-table="RyotTypeMaster.tableEdit" class="table table-striped table-vmiddle" >
												
												<thead>
													<tr>
														<th><span>Action</span></th>
														<th><span>Product Group Code</span></th>
														<th><span>Product Group</span></th>
														<th><span>Sub Group Code</span></th>
														<th><span>Sub Group </span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
			
																						
													<tr  ng-repeat="AddedProductGroup in productData | filter:AddedproductSubgroup.group"   ng-class="{ 'active': AddedProductGroup.$edit }">
														<td>
														   <button type="button" class="btn btn-default" ng-if="!AddedProductGroup.$edit" ng-click="AddedProductGroup.$edit = true;AddedProductGroup.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="AddedProductGroup.$edit" ng-click="UpdateProductGroup(AddedProductGroup,$index);AddedProductGroup.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AddedProductGroup.modifyFlag"  />	
														 
														 </td>
														 <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.groupCode}}</span>
															<div ng-if="AddedProductGroup.$edit">
			<div class="form-group" ng-class="{ 'has-error' : updateProductSubGroupform.groupCode{{$index}}.$invalid && (updateProductSubGroupform.groupCode{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="AddedProductGroup.groupCode" maxlength="25" placeholder='Product Group' name="groupCode{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  ng-blur="getMaxSubGroupsbyGroupName(AddedProductGroup.groupCode,$index);" readonly/ >
					<p ng-show="updateProductSubGroupform.groupCode{{$index}}.$error.required && (updateProductSubGroupform.groupCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="updateProductSubGroupform.groupCode{{$index}}.$error.pattern  && (updateProductSubGroupform.groupCode{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<!--<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedProductGroup.groupCode!=null">Land Type Already Exist.</p>-->
																</div>													
																
															</div>
														  </td>
														 <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.group}}</span>
															<div ng-if="AddedProductGroup.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="AddedProductGroup.group" name="productGroup{{$index}}" maxlength="25" placeholder='Product Group' readonly/>																</div>
															</div>
														  </td>											 
														  
														  <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.subGroupCode}}</span>
															<div ng-if="AddedProductGroup.$edit">
			<div class="form-group" ng-class="{ 'has-error' : updateProductSubGroupform.subgroupCode{{$index}}.$invalid && (updateProductSubGroupform.subgroupCode{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="AddedProductGroup.subGroupCode" maxlength="25" placeholder='SubGroup Code' name="subgroupCode{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" readonly  ng-blur=""/ >
					<p ng-show="updateProductSubGroupform.subgroupCode{{$index}}.$error.required && (updateProductSubGroupform.subgroupCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="updateProductSubGroupform.subgroupCode{{$index}}.$error.pattern  && (updateProductSubGroupform.subgroupCode{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<!--<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedProductGroup.groupCode!=null">Land Type Already Exist.</p>-->
																</div>													
																
															</div>
														  </td>
														  <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.subGroup}}</span>
															<div ng-if="AddedProductGroup.$edit">
			<div class="form-group" ng-class="{ 'has-error' : updateProductSubGroupform.productsubGroup{{$index}}.$invalid && (updateProductSubGroupform.productsubGroup{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="AddedProductGroup.subGroup" maxlength="25" placeholder='Product Group' name="productsubGroup{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  ng-blur=""/ >
					<p ng-show="updateProductSubGroupform.productsubGroup{{$index}}.$error.required && (updateProductSubGroupform.productsubGroup{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="updateProductSubGroupform.groupCode{{$index}}.$error.pattern  && (updateProductSubGroupform.productsubGroup{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<!--<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedProductGroup.groupCode!=null">Land Type Already Exist.</p>-->
																</div>													
																
															</div>
														  </td>
														  <td>
															 <span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.description}}</span>
															 <div ng-if="AddedProductGroup.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="AddedProductGroup.description" maxlength="50" placeholder='Description' ng-blur="spacebtwgrid('description',$index)"/>																</div>
															 </div>
														  </td>
														  <td>
															  <span ng-if="!AddedProductGroup.$edit">
																<span ng-if="AddedProductGroup.status=='0'">Active</span>
																<span ng-if="AddedProductGroup.status=='1'">Inactive</span>
															  </span>
															  <div ng-if="AddedProductGroup.$edit">	
																<div class="form-group">
																  <label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model='AddedProductGroup.status'>
																	<i class="input-helper"></i>Active
																  </label>
																  <label class="radio radio-inline m-r-20">
																		<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model='AddedProductGroup.status'>
																		<i class="input-helper"></i>Inactive
																  </label>
																 </div>
															  </div>
														  </td>
													 </tr>
													 </tbody>
													 </table>	
													</form>	
									 	 </div>
								 	</section>					 
							  	</div>
							</div>
						</div>
						</div>
					<!----------end----------------------->	
					
					
							</div>
					
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
