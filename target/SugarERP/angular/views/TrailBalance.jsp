	<script>
	  $(function() 
	  {
	     $( ".datepickerFrom" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	     $( ".datepickerTo" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
		 
	  });
    </script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">
        	<div class="container" data-ng-controller="TrailBalance" ng-init="loadTrailSeason();">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> Trail Balance</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					     <form name="TrailBalanceForm" novalidate ng-submit="getTrailBalance(TrailBalance);">
					 		<div class="row">
								<div class="col-sm-3"></div>
								<div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : TrailBalanceForm.season.$invalid && (TrailBalanceForm.season.$dirty || Addsubmitted)}">
			        	                	<div class="fg-line">
            					          		<div class="select">
										  			<select chosen class="w-100" name="season" ng-model="TrailBalance.season" ng-options="season.season as season.season for season in getTrailSeasons | orderBy:'-season':true" ng-required="true">
														<option value="">Select Season</option>
													</select>
												  </div>										
			    		           	        </div>							
											<p ng-show="TrailBalanceForm.season.$error.required && (TrailBalanceForm.season.$dirty || Addsubmitted)" class="help-block">Season is Required.</p>
				                    	</div>			                    
									 </div>
				                  </div>
								  <div class="col-sm-3" align="center">
									 	 <button type="submit" class="btn btn-primary btn-sm m-t-10">Generate Report</button>
								<!--<button type="hifdden" class="btn btn-primary btn-sm m-t-10"><i class="glyphicon glyphicon-print"></i>&nbsp;&nbsp;&nbsp;Print</button>-->															
								  </div>								
    	        			 </div>
							 <p></p>
						</form>
						<hr />
								 
						<div  class="table-responsive">
						  <table class="table table-striped">
						 		<thead>
							        <tr>
                		   				<th>Particulars</th>
										<th style="text-align:right;">Dr</th>
						                <th style="text-align:right;">Cr</th>										
            						</tr>											
								</thead>
								<tbody>
									<tr ng-repeat="TrailBalanceData in loadTrailBalanceData">
                		   				<td>{{TrailBalanceData.ryotname}}</td>
							            <td style="text-align:right;">
											<span ng-if="TrailBalanceData.balancetype=='Dr'">{{TrailBalanceData.runningbalance}} /-</span>
											<span ng-if="TrailBalanceData.balancetype!='Dr'">0.00 /-</span>
										</td>
										<td style="text-align:right;">
											<span ng-if="TrailBalanceData.balancetype=='Cr'">{{TrailBalanceData.runningbalance}} /-</span>
											<span ng-if="TrailBalanceData.balancetype!='Cr'">0.00 /-</span>
										</td>
            						</tr>
									<tr style=" color:#000000;">
										<th colspan="2" style="text-align:right">Net Balance</th>
										<th style="text-align:right;">{{availableBalance}} Cr</th>
									</tr>
								</tbody>		
							 </table>
						  </div>							 						           			
				        </div>
			    	</div>
				</div>
	    </section>
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
