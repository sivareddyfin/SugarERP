<script type="text/javascript">		
		$(".autofocus").focus();				
</script>




	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RoleMaster" ng-init="loadScreenNames();loadRoleUsers();loadScreens();loadRule();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Role Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 
					 <!-------body start------>	
					 <form name="RoleForm" ng-submit="AddRolesSubmit(AddedRoles,RoleForm);" novalidate>					
					 		<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<div class="select">
	            					          <select chosen class="w-100 select2"  name="roleid" ng-change="loadRolessOnChange(AddedRoles.roleid);"  tabindex="1" data-ng-model="AddedRoles.roleid"    ng-options="roleid.id as roleid.role for roleid in RoleNames | orderBy:'-role':true">
												<option value="">Added Roles</option>
						                        
				        		            </select>
											</div>
			                	        </div>
			                    	</div>			                    
				                  </div>
    	        			 </div><br />
							   
    	        			<div class="row">
				                <div class="col-sm-6">
									<div class="row">
				                		<div class="col-sm-12">
				                   			 <div class="input-group">
            				           			 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 		<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' :RoleForm.role.$invalid && (RoleForm.role.$dirty || Addsubmitted)}">
			        	               				 		<div class="fg-line">
															<label for="roles">Role Name</label>
            					          <input type="text" class="form-control autofocus" placeholder="Role Name" autofocus  maxlength="25" name="role" data-ng-model="AddedRoles.role"  tabindex="2" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" id="roles" with-floating-label ng-blur="spacebtw('role');validateDup(AddedRoles.roleid);" /> 
			                	       					 		</div>
				 <p ng-show="RoleForm.role.$error.required && (RoleForm.role.$dirty || Addsubmitted)" class="help-block">Role Name is required</p>
				<p ng-show="RoleForm.role.$error.pattern  && (RoleForm.role.$dirty || Addsubmitted)" class="help-block">Enter Valid Role Name</p>		
				<p class="help-block duplicate" style="color:#FF0000; display:none;">Role Already Exist.</p>	
														 </div>
			                    					</div>			                    
				                  				</div>
								  		</div>
										
									
								 
    	        								<div class="row">
													<div class="col-sm-12">
														<div class="input-group" style="margin-top:10px;">
            				            					<span class="input-group-addon">Status :</span>
			        	               						 <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0"  data-ng-model="AddedRoles.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1" data-ng-model="AddedRoles.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        				</div>
			                    					</div>			                    
				                  				</div>
								  			</div>
								  		</div>
								  
								 	
								  
								   <div class="col-sm-6">
								   		<div class="row">
								   			 <div class="col-sm-12">
			    	               				 <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-edit"></i></span>
												<div class="form-group floating-label-wrapper">
			            	            			<div class="fg-line">
													<label for="deccription">Description</label>
            										<input type="text" class="form-control" placeholder="Description (Optional)" name="description" tabindex="3" data-ng-model="AddedRoles.description" maxlength="50" id="deccription" with-floating-label ng-blur="spacebtw('description');" />
			                    	    			</div>
													</div>
				                    			</div>			                    
				                  			</div>
								 		 </div>
									</div>
							 	</div>
						<br />
								  <input type="hidden"  name="modifyFlag" data-ng-model="AddedRoles.modifyFlag"  />
								<div class="row" align="center">            			
									<div class="input-group">
										<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-hide">Save</button>									
									<button type="reset" class="btn btn-primary" ng-click="reset(RoleForm,AddedRoles.roleid)" ng-if="AddedRoles.roleid==null">Reset</button>
									<button type="reset" class="btn btn-primary" ng-click="reset(RoleForm,AddedRoles.roleid)" ng-if="AddedRoles.roleid!=null">Reset</button>
								</div>
							</div>						 	
						 </div>								       							
    	        			 
					<!------------------form end---------->							 
						
					<!-------from-end--------------------->	
								</form>	 						           			
				        </div>
			    	</div>
					<!---------Table Design Start--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Screens</b></h2></div>
					<div class="card"> 												
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								      <section class="asdok">
											<div class="container1">
									<form name="editRoleForm" novalidate> 						
							        <table ng-table="RoleMaster.tableEdit" class="table table-striped table-vmiddle" >
									
									<thead>
										<tr>
											<th><span>Action</span></th>
											<th><span>Screen Name</span></th>
											<th><span>Description</span></th>
											<th><span>Add</span></th>
											<th><span>Modify</span></th>
											<th><span>View</span></th>
											<th><span>Delete</span></th>
												</tr>
											</thead>
										<tbody>
										
								        <tr ng-repeat="Role in ScreenNamesGrid"  ng-class="{ 'active': Role.$edit }" > 
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Role.$edit" ng-click="Role.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="Role.$edit" data-ng-click='updateRoleMaster(Role,$index);Role.$edit = isEdit;'><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Role.$edit">{{Role.screenName}}</span>
													<div ng-if="Role.$edit" >
															<div class="form-group" ng-class="{ 'has-error' :editRoleForm.screenName{{$index}}.$invalid && (editRoleForm.screenName{{$index}}.$dirty || submitted)}" >
											<input class="form-control" type="text" name="screenName" data-ng-model="Role.screenName" maxlength="50" ng-options="screenName.id as screenName.screenname for screenName in ScreenNames  | orderBy:'-screenname':true" readonly />				
					    		                
												<!--<select  class="form-control1" name="screenName"    data-ng-model="Role.screenName" ng-options="screenName.id as screenName.screenname for screenName in ScreenNames  | orderBy:'-screenname':true" >
												<option value="">{{Role.screenName}}</option>
												</select>-->
												
													</div> 
												</div> 
					            		      </td> 
		                    				  <td> 
							                     <span ng-if="!Role.$edit">{{Role.description}}</span> 
							            <div ng-if="Role.$edit"> 
										<input class="form-control" type="text" name="description" data-ng-model="Role.description" maxlength="50" readonly />
															</div> 
							                  </td> 
							                  <td> 
							                      <span ng-if="!Role.$edit"> 
												  <span ng-if="Role.canAdd==true">Yes</span> 
													<span ng-if="Role.canAdd==false">No</span> 
												  </span> 
        		            					  <div ng-if="Role.$edit"> 
												  	<label class="checkbox" style="margin-top:-20px;"> 
													  	<input type="checkbox"    name="canAdd" data-ng-model="Role.canAdd"><i class="input-helper"></i> 
													</label> 
												  </div> 
							                  </td> 
							                  <td> 
                    							   <span ng-if="!Role.$edit"> 
												   <span ng-if="Role.canModify==true">Yes</span> 
													<span ng-if="Role.canModify==false">No</span> 
												   </span> 
					            		           <div ng-if="Role.$edit"> 
													  	<label class="checkbox" style="margin-top:-20px;"> 
														  	<input type="checkbox"    name="canModify" data-ng-model="Role.canModify"><i class="input-helper"></i> 
														</label> 
													</div> 
							                  </td> 
					    		              <td> 
                    							   <span ng-if="!Role.$edit"> 
												   <span ng-if="Role.canRead==true">Yes</span> 
													<span ng-if="Role.canRead==false">No</span> 
												   </span> 
					                    		   <div ng-if="Role.$edit"> 
											  			<label class="checkbox" style="margin-top:-20px;"> 
														  	<input type="checkbox"    name="canRead" data-ng-model="Role.canRead"><i class="input-helper"></i> 
														</label> 
												    </div> 
							                  </td> 					
							                  <td> 
                    							    <span ng-if="!Role.$edit"> 
													<span ng-if="Role.canDelete==true">Yes</span> 
													<span ng-if="Role.canDelete==false">No</span> 
													</span> 
					            		            <div ng-if="Role.$edit"> 
													  	<label class="checkbox" style="margin-top:-20px;" > 
														  	<input type="checkbox"    name="canDelete" data-ng-model="Role.canDelete"><i class="input-helper"></i> 
														</label> 
													</div> 
					            		      	</td>	 		
							             	</tr> 
											</tbody>
								      	</table>
									</form>
								</div>
							</section>							 
						</div> 
					</div> 
				</div> 
					<!---------Table Design End----------->
					
					
				</div> 
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
