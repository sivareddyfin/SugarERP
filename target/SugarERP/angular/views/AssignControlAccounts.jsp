	<script type="text/javascript">		
		$('#autofocus').focus();			
		var data = [{name:'1',country:'789',dis:'Seedling Advance',cir:'Current Asset',desc:'Company Advances'},{name:'2',country:'456',dis:'Fertilizer Advance',cir:'Current Asset',desc:'Company Advances'}];
		sessionStorage.setItem('data1',JSON.stringify(data));
	</script>

	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
	</style>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Assign Control Accounts</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
							<div class="row">
								<div class="col-sm-12">
								<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Account Category :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadiocat" value="Asset" checked="checked">
			    			            	<i class="input-helper"></i>Asset
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadiocat" value="Liability">
			    				            <i class="input-helper"></i>Liability
					  		              </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadiocat" value="Expense">
			    				            <i class="input-helper"></i>Expense
					  		              </label>										  
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadiocat" value="Income">
			    				            <i class="input-helper"></i>Income
					  		              </label>										  										  
			                	        </div>
			                    	</div>
								</div>																			                								  
    	        			 </div><br />
							 <hr />
							 <div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
							                 <td data-title="'Sl.No'">
					    		                <div ng-if="!w.$edit">{{w.name}}</div>
					            		      </td>
		                    				  <td data-title="'A/C Code'">
							                     <div ng-if="!w.$edit">{{w.country}}</div>
							                  </td>
							                  <td data-title="'A/C Name'">
        		            					  <div ng-if="!w.$edit">{{w.dis}}</div>
							                  </td>
							                  <td data-title="'A/C Group'">
        		            					  <div ng-if="!w.$edit">
												  	<select class="form-control1" id="input_circle">
														<option value="0">Select A/C Group</option>
								                        <option value="Current Asset" selected="selected">Current Asset</option>
        		        						        <option value="United Kingdom">United Kingdom</option>
						        		                <option value="Afghanistan">Afghanistan</option>
				        		        		        <option value="Aland Islands">Aland Islands</option>
		                						        <option value="Albania">Albania</option>
						                		        <option value="Algeria">Algeria</option>
		                						        <option value="American Samoa">American Samoa</option>
						        		            </select>
												  </div>
							                  </td>											  
											  <td data-title="'A/C SubGroup'" style="width:300px;">
					    		                <div ng-if="!w.$edit">
												<div class="input-group input-group-unstyled">
                   									<select class="form-control1">
														<option value="Company Advances" selected="selected">Company Advances</option>
													</select>
								                    <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign"></i>
								                    </span>
								                </div>
												</div>
					            		      </td>
							             </tr>
								         </table>							 
							     </div>
						   <div class="row">
								<div class="col-sm-12" align="center">
								  	<div class="input-group">
										<div class="fg-line">
											<a href="#/admin/admindata/masterdata/UpdateCompanyAdvanceInterest" class="btn btn-primary btn-sm m-t-10">Update</a>
											<a href="#/admin/admindata/masterdata/UpdateCompanyAdvanceInterest" class="btn btn-primary btn-sm m-t-10">Reset</a>
										</div>
									</div>
								</div>
						  </div> 						           			
					<!------------------form end---------->							 
						</hc:hmsForm>
					</f:view> 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
