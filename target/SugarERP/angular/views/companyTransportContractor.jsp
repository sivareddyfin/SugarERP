	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="companyTransportContractorController" data-ng-init="loadStatus();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Company Transport Contractor Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="companyTransportContractorForm" novalidate ng-submit="companyTransportContractorSubmit(AddedcompanyTransportContractor,companyTransportContractorForm)">
					   <div class="row">
					     <div class="col-sm-12">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			            <div class="form-group floating-label-wrapper" ng-class="{'has-error' : companyTransportContractorForm.contractor.$invalid && (companyTransportContractorForm.contractor.$dirty || Addsubmitted)}">
										  <div class="fg-line">
										   <label for="contractor">Contractor</label>
										     <input type="text" class="form-control " autofocus tabindex="1"  placeholder="Contractor"    name="contractor" ng-model="AddedcompanyTransportContractor.contractor"   data-ng-required='true'  data-ng-pattern= "/^[a-z 0-9 A-Z\s]*$/"  maxlength="25" id="contractor" with-floating-label/>
											 </div>
									<p ng-show = "companyTransportContractorForm.contractor.$error.required &&(companyTransportContractorForm.contractor.$dirty || Addsubmitted)" class="help-block"> Contractor name is required</p>
									<p ng-show = "companyTransportContractorForm.contractor.$error.pattern &&(companyTransportContractorForm.contractor.$dirty || Addsubmitted)" class="help-block"> Enter Valid Contractor name </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
					     <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			     <div class="form-group floating-label-wrapper" ng-class="{'has-error' : companyTransportContractorForm.contractorCode.$invalid && (companyTransportContractorForm.contractorCode.$dirty || Addsubmitted)}">
										  <div class="fg-line">
										    <label for="contractorCode">contractorCode</label>
							 <input type="text" class="form-control" placeholder="Contractor Code" name="contractorCode" ng-model="AddedcompanyTransportContractor.contractorCode"   data-ng-required='true' tabindex="2" data-ng-pattern= "/^[ 0-9 \s]*$/"  maxlength="10" readonly="readonly" id="contractorCode" with-floating-label/>
											 </div>
											 <p ng-show = "companyTransportContractorForm.contractorCode.$error.required &&(companyTransportContractorForm.contractorCode.$dirty || Addsubmitted)" class="help-block"> Contractor code is required</p>
									<p ng-show = "companyTransportContractorForm.contractorCode.$error.pattern &&(companyTransportContractorForm.contractorCode.$dirty || Addsubmitted)" class="help-block"> Enter Valid Contractor code </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						 <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group">
		            				    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                            <div class="form-group floating-label-wrapper">
										  <div class="fg-line">
										    <label for="description">Description</label>
										     <input type="text" class="form-control" placeholder="Description"  name="description" ng-model="AddedcompanyTransportContractor.description"   data-ng-required='true' tabindex="3" data-ng-pattern= "/^[a-z 0-9 A-Z\s]*$/"  maxlength="25" id="description" with-floating-label/>
											 </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						 <div class="col-sm-4">
						   <div class="row">
								<div class="col-sm-12">
									<div class="input-group" style="margin-top:10px;">
		            				    <span class="input-group-addon">Status :</span>
			                            <div class="form-group">
										  <div class="fg-line">
										    <label class="radio radio-inline m-r-20">
												<input type="radio" name="status" value="0"  checked="checked" data-ng-model='AddedcompanyTransportContractor.status' tabindex="4">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            					<input type="radio" name="status" value="1"  data-ng-model='AddedcompanyTransportContractor.status' tabindex="4">
					    								<i class="input-helper"></i>Inactive
							  		 				</label>				
											 </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
					 <br />	
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="5">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(companyTransportContractorForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
                 </form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
						<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Company Transport Contractor</b></h2></div>
					
					<div class="card">						
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 	
								<form name="companyTransportContractorEdit" novalidate>					
							        <table  class="table table-striped table-vmiddle" >
										<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Contractor</span></th>
																<th><span>Contractor Code</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																
																</tr>
														</thead>
														<tbody>
								        <tr ng-repeat="companyTransportContractor in companyTransportContractorData"  ng-class="{ 'active': companyTransportContractor.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!companyTransportContractor.$edit" ng-click="companyTransportContractor.$edit = true;companyTransportContractor.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="companyTransportContractor.$edit" ng-click="updatecompanyTransportContractor(kind,$index);companyTransportContractor.$edit = isEdit"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="companyTransportContractor.modifyFlag"  />
											   
		                    				 </td>
							                  <td>
                		    					<span ng-if="!companyTransportContractor.$edit">{{companyTransportContractor.contractor}}</span>
					    		                <div ng-if="companyTransportContractor.$edit">
				   <div class="form-group" ng-class="{'has-error' :companyTransportContractorEdit.contractor{{$index}}.$invalid && (companyTransportContractorEdit.contractor{{index}}.$dirty || submitted)}">
														<input class="form-control" type="text" ng-model="companyTransportContractor.contractor" name="contractor{{$index}}" placeholder='Contractor' maxlength="10" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
														<p ng-show="companyTransportContractorEdit.contractor{{index}}.$error.required && (companyTransportContractorEdit.contractor{{index}}.$dirty || submitted)" class="help-block">Contractor name required.</p>
													<p ng-show="companyTransportContractorEdit.contractor{{index}}.$error.pattern && (companyTransportContractorEdit.contractor{{index}}.$dirty || submitted)" class="help-block"> Enter valid contractor name </p>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!companyTransportContractor.$edit">{{companyTransportContractor.contractorCode}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : companyTransportContractorEdit.contractorCode{{index}}.$invalid && (companyTransportContractorEdit.contractorCode{{index}}.$dirty || submitted)}">
							                     <div ng-if="companyTransportContractor.$edit">
												 	<input class="form-control" type="text" ng-model="companyTransportContractor.contractorCode" placeholder='Contractor Code' maxlength="25" name="contractorCode{{index}}" data-ng-pattern="/^[0-9 \s]*$/" ng-required='true'/>
													<p ng-show="companyTransportContractorrEdit.contractorCode{{index}}.$error.required && (companyTransportContractorEdit.contractorCode{{index}}.$dirty || submitted)" class="help-block">Contractor code required.</p>
													<p ng-show="companyTransportContractorEdit.contractorCode{{index}}.$error.pattern && (companyTransportContractorEdit.contractorCode{{index}}.$dirty || submitted)" class="help-block"> Enter valid contractor code</p>
													
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!companyTransportContractor.$edit">{{companyTransportContractor.description}}</span>
        		            					  <div ng-if="companyTransportContractor.$edit">
												  	<div class="form-group">
													  	<input class="form-control" type="text" ng-model="companyTransportContractor.description" placeholder='Description' maxlength="50" name="description" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!companyTransportContractor.$edit">
												  	<span ng-if="companyTransportContractor.status=='0'">Active</span>
													<span ng-if="companyTransportContractor.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="companyTransportContractor.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked"  ng-model="companyTransportContractor.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="companyTransportContractor.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
											 
											  
							             </tr>
										 </tbody>
										 
								         </table>
										 
										 </form>
										 </div>
										 </section>							 
							     </div>
							</div>
						</div>
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
