package com.finsol.bean;

public class TrayUpdateDetailsBean
{
	private Integer trayType;
	private Integer noofTraysReturn;
	private Double traysinGoodCondition;
	private Double damagedTray;
	private String trayDateTaken;
	private Integer id;
	private String season;
	private Integer ryotDamagedTray;
	private Integer damagedTrayTravelling;
	

	public Integer getRyotDamagedTray() {
		return ryotDamagedTray;
	}
	public Integer getDamagedTrayTravelling() {
		return damagedTrayTravelling;
	}
	public void setRyotDamagedTray(Integer ryotDamagedTray) {
		this.ryotDamagedTray = ryotDamagedTray;
	}
	public void setDamagedTrayTravelling(Integer damagedTrayTravelling) {
		this.damagedTrayTravelling = damagedTrayTravelling;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTrayType()
	{
		return trayType;
	}
	
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	public Integer getNoofTraysReturn() {
		return noofTraysReturn;
	}
	
	public String getTrayDateTaken() {
		return trayDateTaken;
	}
	public void setNoofTraysReturn(Integer noofTraysReturn) {
		this.noofTraysReturn = noofTraysReturn;
	}
	
	public void setTrayDateTaken(String trayDateTaken) {
		this.trayDateTaken = trayDateTaken;
	}
	public Double getTraysinGoodCondition() {
		return traysinGoodCondition;
	}
	public Double getDamagedTray() {
		return damagedTray;
	}
	public void setTraysinGoodCondition(Double traysinGoodCondition) {
		this.traysinGoodCondition = traysinGoodCondition;
	}
	public void setDamagedTray(Double damagedTray) {
		this.damagedTray = damagedTray;
	}
	
	
	
	
	
}
