package com.finsol.bean;

import java.util.List;

public class RolesBean 
{
	private Integer roleId;
	private String role;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	private List<RoleScreensBean> roleScreensBean;
	
	
	public Integer getRoleId()
	{
		return roleId;
	}
	public void setRoleId(Integer roleId) 
	{
		this.roleId = roleId;
	}
	public String getRole() 
	{
		return role;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public List<RoleScreensBean> getRoleScreensBean() {
		return roleScreensBean;
	}
	public void setRoleScreensBean(List<RoleScreensBean> roleScreensBean) {
		this.roleScreensBean = roleScreensBean;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	
}
