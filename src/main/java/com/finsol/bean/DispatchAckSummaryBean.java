package com.finsol.bean;

public class DispatchAckSummaryBean 
{
	private Integer dispatchSeq;
	private String dispatchNo;
	private String ryotcode;
	private Integer faCode;
	private Integer foCode;
	private String driverName;
	private String drPhoneNo;
	private String AcknowledgedBy;
	private String modifyFlag;
	private String ryotPhoneNumber;
	private Integer seedlingQty;
	private String indentNumbers;
	private String vehicle;
	private String agreementSign;
	private String soilWaterAnalyis;
	private String landSurvey;
	private String ryotName;
	private Integer id;
	private String season;
	private String dateOfDispatch;
	private String indents;
	private String dateOfAcknowledge;
	private Double totalSeedlings;
	private Double totalSeedlingsAmt;
	
	
	
	public Double getTotalSeedlings() {
		return totalSeedlings;
	}
	public Double getTotalSeedlingsAmt() {
		return totalSeedlingsAmt;
	}
	public void setTotalSeedlings(Double totalSeedlings) {
		this.totalSeedlings = totalSeedlings;
	}
	public void setTotalSeedlingsAmt(Double totalSeedlingsAmt) {
		this.totalSeedlingsAmt = totalSeedlingsAmt;
	}
	public Integer getDispatchSeq() 
	{
		return dispatchSeq;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public Integer getFoCode() {
		return foCode;
	}
	public String getDriverName() {
		return driverName;
	}
	public String getDrPhoneNo() {
		return drPhoneNo;
	}
	public String getAcknowledgedBy() {
		return AcknowledgedBy;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public String getRyotPhoneNumber() {
		return ryotPhoneNumber;
	}
	public Integer getSeedlingQty() {
		return seedlingQty;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public String getVehicle() {
		return vehicle;
	}
	public String getAgreementSign() {
		return agreementSign;
	}
	public String getSoilWaterAnalyis() {
		return soilWaterAnalyis;
	}
	public String getLandSurvey() {
		return landSurvey;
	}
	public String getRyotName() {
		return ryotName;
	}
	public Integer getId() {
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getDateOfDispatch() {
		return dateOfDispatch;
	}
	public String getIndents() {
		return indents;
	}
	public String getDateOfAcknowledge() {
		return dateOfAcknowledge;
	}
	public void setDispatchSeq(Integer dispatchSeq) {
		this.dispatchSeq = dispatchSeq;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public void setDrPhoneNo(String drPhoneNo) {
		this.drPhoneNo = drPhoneNo;
	}
	public void setAcknowledgedBy(String acknowledgedBy) {
		AcknowledgedBy = acknowledgedBy;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public void setRyotPhoneNumber(String ryotPhoneNumber) {
		this.ryotPhoneNumber = ryotPhoneNumber;
	}
	public void setSeedlingQty(Integer seedlingQty) {
		this.seedlingQty = seedlingQty;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public void setAgreementSign(String agreementSign) {
		this.agreementSign = agreementSign;
	}
	public void setSoilWaterAnalyis(String soilWaterAnalyis) {
		this.soilWaterAnalyis = soilWaterAnalyis;
	}
	public void setLandSurvey(String landSurvey) {
		this.landSurvey = landSurvey;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDateOfDispatch(String dateOfDispatch) {
		this.dateOfDispatch = dateOfDispatch;
	}
	public void setIndents(String indents) {
		this.indents = indents;
	}
	public void setDateOfAcknowledge(String dateOfAcknowledge) {
		this.dateOfAcknowledge = dateOfAcknowledge;
	}
	
	

}
