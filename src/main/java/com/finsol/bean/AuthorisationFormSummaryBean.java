package com.finsol.bean;

public class AuthorisationFormSummaryBean 
{
	private Integer id;
	private String season;
	private String authorisationDate;
	private String ryotCode;
	private String ryotName;
	private String fieldAssistant;
	private String fieldOfficer;
	private String indentNumbers;
	private String authorisedBy;
	private String phoneNo;
	private String authorisationSeqNo;
	private Double grandTotal;
	private String modifyFlag;
	private Integer indentStatus;
	private Integer faCode;
	private String  agreementnumber;
	private String noOfAcr;
	private String  storeCode;
	private String individualExtent;
	
	public String getIndividualExtent() {
		return individualExtent;
	}
	public void setIndividualExtent(String individualExtent) {
		this.individualExtent = individualExtent;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	
	public String getNoOfAcr() {
		return noOfAcr;
	}
	public void setNoOfAcr(String noOfAcr) {
		this.noOfAcr = noOfAcr;
	}

	
	public Integer getFaCode() {
		return faCode;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId()
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getAuthorisationDate() {
		return authorisationDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public String getFieldAssistant() {
		return fieldAssistant;
	}
	public String getFieldOfficer() {
		return fieldOfficer;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public String getAuthorisedBy() {
		return authorisedBy;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setAuthorisationDate(String authorisationDate) {
		this.authorisationDate = authorisationDate;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setFieldAssistant(String fieldAssistant) {
		this.fieldAssistant = fieldAssistant;
	}
	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setAuthorisedBy(String authorisedBy) {
		this.authorisedBy = authorisedBy;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	

	

}
