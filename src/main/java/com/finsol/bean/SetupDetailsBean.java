package com.finsol.bean;

public class SetupDetailsBean
{
	private Integer id;
	private String configSc;
	private String name;
	private Byte value;
	private String description;
	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public String getConfigSc()
	{
		return configSc;
	}
	public void setConfigSc(String configSc)
	{
		this.configSc = configSc;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public Byte getValue() 
	{
		return value;
	}
	public void setValue(Byte value) 
	{
		this.value = value;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
}
