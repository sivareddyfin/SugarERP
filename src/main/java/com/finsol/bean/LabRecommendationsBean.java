package com.finsol.bean;

import javax.persistence.Column;

public class LabRecommendationsBean {

	private Integer id;
	private String ryotCode;
	private String plotSlNumber;
	private String surveyNo;
	private Byte suitableForCultivation;
	private Double extentSize;
	private Integer varietyCode;
	private String testDate;
	private String ryotName;
	private String villageName;
	private String villageCode;
	private Byte isSuitableForCultivation;
	private String recommendation;
	private Byte soilTestStatus;
	private String enteredBy;
	
	private String screenName;
	private Integer slno;
	

	public String getScreenName()
	{
		return screenName;
	}
	public void setScreenName(String screenName) 
	{
		this.screenName = screenName;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getEnteredBy()
	{
		return enteredBy;
	}
	public void setEnteredBy(String enteredBy) 
	{
		this.enteredBy = enteredBy;
	}
	public Byte getSoilTestStatus()
	{
		return soilTestStatus;
	}
	public void setSoilTestStatus(Byte soilTestStatus)
	{
		this.soilTestStatus = soilTestStatus;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getRyotCode()
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode)
	{
		this.ryotCode = ryotCode;
	}
		
	
	public Byte getSuitableForCultivation()
	{
		return suitableForCultivation;
	}
	public void setSuitableForCultivation(Byte suitableForCultivation) 
	{
		this.suitableForCultivation = suitableForCultivation;
	}
	
	public Integer getVarietyCode()
	{
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode)
	{
		this.varietyCode = varietyCode;
	}
	public String getPlotSlNumber() 
	{
		return plotSlNumber;
	}
	public void setPlotSlNumber(String plotSlNumber)
	{
		this.plotSlNumber = plotSlNumber;
	}
	public String getSurveyNo()
	{
		return surveyNo;
	}
	public void setSurveyNo(String surveyNo)
	{
		this.surveyNo = surveyNo;
	}
	public String getTestDate()
	{
		return testDate;
	}
	public void setTestDate(String testDate) 
	{
		this.testDate = testDate;
	}
	public String getRyotName() 
	{
		return ryotName;
	}
	public void setRyotName(String ryotName)
	{
		this.ryotName = ryotName;
	}
	public String getVillageName()
	{
		return villageName;
	}
	public void setVillageName(String villageName)
	{
		this.villageName = villageName;
	}
	public String getVillageCode()
	{
		return villageCode;
	}
	public void setVillageCode(String villageCode)
	{
		this.villageCode = villageCode;
	}
	public Double getExtentSize() 
	{
		return extentSize;
	}
	public void setExtentSize(Double extentSize) 
	{
		this.extentSize = extentSize;
	}
	public Byte getIsSuitableForCultivation() 
	{
		return isSuitableForCultivation;
	}
	public void setIsSuitableForCultivation(Byte isSuitableForCultivation)
	{
		this.isSuitableForCultivation = isSuitableForCultivation;
	}
	public String getRecommendation()
	{
		return recommendation;
	}
	public void setRecommendation(String recommendation)
	{
		this.recommendation = recommendation;
	}
	

	
	
	
}
