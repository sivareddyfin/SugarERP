package com.finsol.bean;


public class ExtentDetailsBean {
	
	 private Integer id;	 
	 private Integer plotSlNumber;
	 private Integer varietyCode;
	 private Double extentSize;	 
	 private String surveyNo; 	 
	 private Byte IsSuitableForCultivation;
	 private String testDate;
	 private String ryotCode;
	 private String VillageCode;
	 private String ryotName;
	 private String villageName;
	 private Byte soilTestStatus;
	 private Integer slno;
	 
	 private String screenName;
	
	 
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Byte getSoilTestStatus() {
		return soilTestStatus;
	}
	public void setSoilTestStatus(Byte soilTestStatus) {
		this.soilTestStatus = soilTestStatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public Integer getPlotSlNumber() {
		return plotSlNumber;
	}
	public void setPlotSlNumber(Integer plotSlNumber) {
		this.plotSlNumber = plotSlNumber;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public String getSurveyNo() {
		return surveyNo;
	}
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	public Byte getIsSuitableForCultivation() {
		return IsSuitableForCultivation;
	}
	public void setIsSuitableForCultivation(Byte isSuitableForCultivation) {
		IsSuitableForCultivation = isSuitableForCultivation;
	}
	public String getTestDate() {
		return testDate;
	}
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	
	public String getVillageCode() {
		return VillageCode;
	}
	public void setVillageCode(String villageCode) {
		VillageCode = villageCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	
	 
}
