package com.finsol.bean;

public class AgreementSummaryBean 
{

	private Integer agreementSeqNo;
	private String seasonYear;
	private String agreementNumber;
	private String agreementDate;
	private String ryotCode;
	private String salutation;
	private String ryotName;
	private String relation;
	private String relativeName;
	private Integer mandalCode;
	private Integer circleCode;
	private Integer branchCode;
	private Integer villageCode;
	private Integer zoneCode;
	private String accountNumber;
	private Byte hasFamilyGroup;
	private Integer familyGroupCode;
	private String suretyRyotCode;
	private String remarks;
	private String loginId;
	private Byte status;
	private String modifyFlag;
	private String agreementAddNumber;
	private Byte typeOfAgreement;
	private String zoneName;
	private String surityPersonName;
	private String surityPersonaadhaarNumber;
	
	private String screenName;
	private Integer faCode;
	private Integer foCode;
	
	
	public Integer getFaCode() {
		return faCode;
	}
	public Integer getFoCode() {
		return foCode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	public Byte getTypeOfAgreement() {
		return typeOfAgreement;
	}
	public void setTypeOfAgreement(Byte typeOfAgreement) {
		this.typeOfAgreement = typeOfAgreement;
	}
	public String getSurityPersonName() {
		return surityPersonName;
	}
	public void setSurityPersonName(String surityPersonName) {
		this.surityPersonName = surityPersonName;
	}
	public String getSurityPersonaadhaarNumber() {
		return surityPersonaadhaarNumber;
	}
	public void setSurityPersonaadhaarNumber(String surityPersonaadhaarNumber) {
		this.surityPersonaadhaarNumber = surityPersonaadhaarNumber;
	}
	public String getAgreementAddNumber() {
		return agreementAddNumber;
	}
	public void setAgreementAddNumber(String agreementAddNumber) {
		this.agreementAddNumber = agreementAddNumber;
	}
	public String getModifyFlag()
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getAgreementSeqNo() 
	{
		return agreementSeqNo;
	}
	public void setAgreementSeqNo(Integer agreementSeqNo)
	{
		this.agreementSeqNo = agreementSeqNo;
	}
	public String getSeasonYear() {
		return seasonYear;
	}
	public void setSeasonYear(String seasonYear) {
		this.seasonYear = seasonYear;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getAgreementDate() {
		return agreementDate;
	}
	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getRelativeName() {
		return relativeName;
	}
	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}
	public Integer getMandalCode() {
		return mandalCode;
	}
	public void setMandalCode(Integer mandalCode) {
		this.mandalCode = mandalCode;
	}
	
	public Integer getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(Integer villageCode) {
		this.villageCode = villageCode;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public Integer getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(Integer branchCode) {
		this.branchCode = branchCode;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Byte getHasFamilyGroup() {
		return hasFamilyGroup;
	}
	public void setHasFamilyGroup(Byte hasFamilyGroup) {
		this.hasFamilyGroup = hasFamilyGroup;
	}
	public Integer getFamilyGroupCode() {
		return familyGroupCode;
	}
	public void setFamilyGroupCode(Integer familyGroupCode) {
		this.familyGroupCode = familyGroupCode;
	}
	public String getSuretyRyotCode() {
		return suretyRyotCode;
	}
	public void setSuretyRyotCode(String suretyRyotCode) {
		this.suretyRyotCode = suretyRyotCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}



}
