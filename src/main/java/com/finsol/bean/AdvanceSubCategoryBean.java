package com.finsol.bean;

public class AdvanceSubCategoryBean 
{
	private Integer id;
	private String subCategoryCode;
	private String name;
	private String description;
	private Integer advanceCode;
	private Integer advanceCategoryCode;
	private Byte status;
	private String modifyFlag;
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getSubCategoryCode() 
	{
		return subCategoryCode;
	}
	public void setSubCategoryCode(String subCategoryCode) 
	{
		this.subCategoryCode = subCategoryCode;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Integer getAdvanceCode()
	{
		return advanceCode;
	}
	public void setAdvanceCode(Integer advanceCode)
	{
		this.advanceCode = advanceCode;
	}
	public Integer getAdvanceCategoryCode()
	{
		return advanceCategoryCode;
	}
	public void setAdvanceCategoryCode(Integer advanceCategoryCode)
	{
		this.advanceCategoryCode = advanceCategoryCode;
	}
	public Byte getStatus() 
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
}
