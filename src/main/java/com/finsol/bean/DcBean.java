package com.finsol.bean;

public class DcBean 
{
	private Integer id;
	private String productCode;
	private String productName;
	private Integer uom;
	private String uomName;
	private String batch;
	private String expDt;
	private Double mrp;
	private String stockInDate;
	private Double quantity;
	private Double totalCost;
	private String batchId;
	private String batchFlag;
	private Integer batchSeqNo;
	private Double cp;
	private Double discountAmount;
	private Double netAmount;

	
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public Double getNetAmount() {
		return netAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	public Double getCp() {
		return cp;
	}
	public void setCp(Double cp) {
		this.cp = cp;
	}
	public Integer getId() {
		return id;
	}
	public String getProductCode() {
		return productCode;
	}
	public String getProductName() {
		return productName;
	}
	public Integer getUom() {
		return uom;
	}
	public String getUomName() {
		return uomName;
	}
	public String getBatch() {
		return batch;
	}
	public String getExpDt() {
		return expDt;
	}
	public Double getMrp() {
		return mrp;
	}
	public String getStockInDate() {
		return stockInDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public String getBatchId() {
		return batchId;
	}
	public String getBatchFlag() {
		return batchFlag;
	}
	public Integer getBatchSeqNo() {
		return batchSeqNo;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setUom(Integer uom) {
		this.uom = uom;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public void setStockInDate(String stockInDate) {
		this.stockInDate = stockInDate;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public void setBatchFlag(String batchFlag) {
		this.batchFlag = batchFlag;
	}
	public void setBatchSeqNo(Integer batchSeqNo) {
		this.batchSeqNo = batchSeqNo;
	}
	
}
