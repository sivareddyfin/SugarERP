package com.finsol.bean;

public class GrnSummaryBean 
{
	private Integer id;
	private String modifyFlag;
	private String finYr;
	private String grnDate;
	private String grnTime;
	private Integer seqNo;
	private String grnNo;
	private String stockIssueNo;
	private String loginId;
	private Integer deptId;
	private int stockTransactionNo;

	
	public int getStockTransactionNo() {
		return stockTransactionNo;
	}
	public void setStockTransactionNo(int stockTransactionNo) {
		this.stockTransactionNo = stockTransactionNo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getGrnDate() {
		return grnDate;
	}
	public void setGrnDate(String grnDate) {
		this.grnDate = grnDate;
	}
	public String getGrnTime() {
		return grnTime;
	}
	public void setGrnTime(String grnTime) {
		this.grnTime = grnTime;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getGrnNo() {
		return grnNo;
	}
	public void setGrnNo(String grnNo) {
		this.grnNo = grnNo;
	}
	public String getStockIssueNo() {
		return stockIssueNo;
	}
	public void setStockIssueNo(String stockIssueNo) {
		this.stockIssueNo = stockIssueNo;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	

}
