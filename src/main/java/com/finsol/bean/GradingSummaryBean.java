package com.finsol.bean;

public class GradingSummaryBean 
{
	private Integer id;
	private String season;
	private String gradingDate;
	private Integer shift;
	private Byte labourSpplr;
	private String modifyFlag;
	private Double noOfMen;
	private Double manCost;
	private Double menTotal;
	private Double noOfWomen;
	private Double womenCost;
	private Double womenTotal;
	private Double totalLabourCost;
	private Double costPerTon;
	private Double totalTons;
	private Double totalContractAmt;
	private Integer gridCount;
	private Integer lc;
	
	public Integer getLc() {
		return lc;
	}
	public void setLc(Integer lc) {
		this.lc = lc;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getGradingDate() {
		return gradingDate;
	}
	public void setGradingDate(String gradingDate) {
		this.gradingDate = gradingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public Double getWomenCost() {
		return womenCost;
	}
	public void setWomenCost(Double womenCost) {
		this.womenCost = womenCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public Integer getGridCount() {
		return gridCount;
	}
	public void setGridCount(Integer gridCount) {
		this.gridCount = gridCount;
	}
	
	
	
	
	
	
}
