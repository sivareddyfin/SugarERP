package com.finsol.bean;

public class RyotMasterBean {
	private Integer id;
	private String ryotCode;
	private String salutation;
	private String ryotName;
	private Integer mandalCode;
	private String villageCode;
	private Integer ryotCodeSequence;
	private Integer circleCode;
	private String aadhaarNumber;
	private String aadhaarImagePath;
	private String panNumber;
	private String ryotPhotoPath;
	private String relativeName;
	private String relation;
	private String address;
	private String city;
	private String mobileNumber;
	private String suretyRyotCode;
	private Byte status;
	private String screenName;
	private String modifyFlag;
	private Byte ryotType;
	
	
	public Byte getRyotType() {
		return ryotType;
	}
	public void setRyotType(Byte ryotType) {
		this.ryotType = ryotType;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getMandalCode() {
		return mandalCode;
	}
	public void setMandalCode(Integer mandalCode) {
		this.mandalCode = mandalCode;
	}
	
	
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Integer getRyotCodeSequence() {
		return ryotCodeSequence;
	}
	public void setRyotCodeSequence(Integer ryotCodeSequence) {
		this.ryotCodeSequence = ryotCodeSequence;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public String getAadhaarImagePath() {
		return aadhaarImagePath;
	}
	public void setAadhaarImagePath(String aadhaarImagePath) {
		this.aadhaarImagePath = aadhaarImagePath;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getRyotPhotoPath() {
		return ryotPhotoPath;
	}
	public void setRyotPhotoPath(String ryotPhotoPath) {
		this.ryotPhotoPath = ryotPhotoPath;
	}
	public String getRelativeName() {
		return relativeName;
	}
	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSuretyRyotCode() {
		return suretyRyotCode;
	}
	public void setSuretyRyotCode(String suretyRyotCode) {
		this.suretyRyotCode = suretyRyotCode;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}


}
