package com.finsol.bean;

public class AdvanceInterestRatesBean 
{
	private Integer id;
	private Integer advanceCode;
	private Double fromAmount;
	private Double toAmount;
	private Double interestRate;
	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public Integer getAdvanceCode()
	{
		return advanceCode;
	}
	public void setAdvanceCode(Integer advanceCode)
	{
		this.advanceCode = advanceCode;
	}
	public Double getFromAmount()
	{
		return fromAmount;
	}
	public void setFromAmount(Double fromAmount)
	{
		this.fromAmount = fromAmount;
	}
	public Double getToAmount()
	{
		return toAmount;
	}
	public void setToAmount(Double toAmount)
	{
		this.toAmount = toAmount;
	}
	public Double getInterestRate() 
	{
		return interestRate;
	}
	public void setInterestRate(Double interestRate)
	{
		this.interestRate = interestRate;
	}
}
