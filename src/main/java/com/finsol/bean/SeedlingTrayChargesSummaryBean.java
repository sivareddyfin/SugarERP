package com.finsol.bean;

public class SeedlingTrayChargesSummaryBean 
{
	private Integer returnId;
	private String season;
	private String ryotCode;
	private Double totalAmount;
	private String loginId;
	private String modifyFlag;
	
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getReturnId()
	{
		return returnId;
	}
	public void setReturnId(Integer returnId)
	{
		this.returnId = returnId;
	}
	public String getSeason()
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) 
	{
		this.ryotCode = ryotCode;
	}

	public Double getTotalAmount() 
	{
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount)
	{
		this.totalAmount = totalAmount;
	}
	public String getLoginId() 
	{
		return loginId;
	}
	public void setLoginId(String loginId) 
	{
		this.loginId = loginId;
	}
}
