package com.finsol.bean;

public class SampleCardsBean {
	private String season;
	private int programNumber;
	private String ryotCode;
	private String ryotName;
	private String agreementNumber;
	private String plotNumber;
	private Double plantOrRatooninAcres;
	private String variety;
	private String dateOfPlantorRatoon;
	private String sampleCard;
	private String sampleCardsId;

	
	
	public String getSampleCardsId() {
		return sampleCardsId;
	}
	public void setSampleCardsId(String sampleCardsId) {
		this.sampleCardsId = sampleCardsId;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	
	
	public Double getPlantOrRatooninAcres() {
		return plantOrRatooninAcres;
	}
	public void setPlantOrRatooninAcres(Double plantOrRatooninAcres) {
		this.plantOrRatooninAcres = plantOrRatooninAcres;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getDateOfPlantorRatoon() {
		return dateOfPlantorRatoon;
	}
	public void setDateOfPlantorRatoon(String dateOfPlantorRatoon) {
		this.dateOfPlantorRatoon = dateOfPlantorRatoon;
	}
	
	public String getSampleCard() {
		return sampleCard;
	}
	public void setSampleCard(String sampleCard) {
		this.sampleCard = sampleCard;
	}
	public int getProgramNumber() {
		return programNumber;
	}
	public void setProgramNumber(int programNumber) {
		this.programNumber = programNumber;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode)
	{
		this.ryotCode = ryotCode;
	}
	public String getAgreementNumber()
	{
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) 
	{
		this.agreementNumber = agreementNumber;
	}
	public String getPlotNumber()
	{
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber)
	{
		this.plotNumber = plotNumber;
	}
	

}
