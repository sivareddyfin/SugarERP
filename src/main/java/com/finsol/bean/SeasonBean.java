package com.finsol.bean;

public class SeasonBean
{
	private Integer id;
	private Integer seasonId;
	private String season;
	private Integer fromYear;
	private Integer toYear;
	private String description;
	private String status;
	private String modifyFlag;
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getSeasonId()
	{
		return seasonId;
	}
	public void setSeasonId(Integer seasonId)
	{
		this.seasonId = seasonId;
	}
	public String getSeason()
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public Integer getFromYear() 
	{
		return fromYear;
	}
	public void setFromYear(Integer fromYear)
	{
		this.fromYear = fromYear;
	}
	public Integer getToYear() 
	{
		return toYear;
	}
	public void setToYear(Integer toYear)
	{
		this.toYear = toYear;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}
}
