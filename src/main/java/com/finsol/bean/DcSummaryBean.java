package com.finsol.bean;

import javax.persistence.Column;

public class DcSummaryBean 
{
	private int department;
	private String dateOfEntry;
	private String dcnumber;
	private String dcdate;
	private String customername;
	private String address;
	private String contactnumber;
	private int dcSeqNo;
	private String modifyFlag;
	private int transactioncode;
	private String saleInvoiceNo;
    private double totalCost;
    private double discountAmount;
    private Double discountPercent;
    private double netAmount;
	private int discType;

	public int getDiscType() {
		return discType;
	}
	public void setDiscType(int discType) {
		this.discType = discType;
	}
	public int getDepartment() {
		return department;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public String getDcnumber() {
		return dcnumber;
	}
	public String getDcdate() {
		return dcdate;
	}
	public String getCustomername() {
		return customername;
	}
	public String getAddress() {
		return address;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public int getDcSeqNo() {
		return dcSeqNo;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public int getTransactioncode() {
		return transactioncode;
	}
	public String getSaleInvoiceNo() {
		return saleInvoiceNo;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public double getDiscountAmount() {
		return discountAmount;
	}
	public Double getDiscountPercent() {
		return discountPercent;
	}
	public double getNetAmount() {
		return netAmount;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public void setDcnumber(String dcnumber) {
		this.dcnumber = dcnumber;
	}
	public void setDcdate(String dcdate) {
		this.dcdate = dcdate;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public void setDcSeqNo(int dcSeqNo) {
		this.dcSeqNo = dcSeqNo;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public void setTransactioncode(int transactioncode) {
		this.transactioncode = transactioncode;
	}
	public void setSaleInvoiceNo(String saleInvoiceNo) {
		this.saleInvoiceNo = saleInvoiceNo;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

}
