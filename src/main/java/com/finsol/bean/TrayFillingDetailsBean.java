package com.finsol.bean;

public class TrayFillingDetailsBean
{
	private Integer id;
	private String season;
	private String date;
	private Integer shift;
	private String batchSeries;
	private String variety;
	private String seedlingType;
	private String batchNo;
	private String startTime;
	private String endTime;
	private String totalTime;
	private Integer trayType;
	private Integer noOfCavitiesPerTray;
	private Integer totalTrays;
	private Double totalNoOfSeedlingss;
	
	
	private String varietyName;
	
	
	
	public String getVarietyName() {
		return varietyName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getDate() {
		return date;
	}
	public Integer getShift() {
		return shift;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public String getSeedlingType() {
		return seedlingType;
	}
	
	public String getStartTime() {
		return startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	
	public Integer getTrayType() {
		return trayType;
	}
	public Integer getNoOfCavitiesPerTray() {
		return noOfCavitiesPerTray;
	}
	public Integer getTotalTrays() {
		return totalTrays;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public void setSeedlingType(String seedlingType) {
		this.seedlingType = seedlingType;
	}
	
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	public void setNoOfCavitiesPerTray(Integer noOfCavitiesPerTray) {
		this.noOfCavitiesPerTray = noOfCavitiesPerTray;
	}
	public void setTotalTrays(Integer totalTrays) {
		this.totalTrays = totalTrays;
	}
	public Double getTotalNoOfSeedlingss() {
		return totalNoOfSeedlingss;
	}
	public void setTotalNoOfSeedlingss(Double totalNoOfSeedlingss) {
		this.totalNoOfSeedlingss = totalNoOfSeedlingss;
	}


	
}
