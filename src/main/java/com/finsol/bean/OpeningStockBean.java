package com.finsol.bean;

public class OpeningStockBean
{
	private Integer id;
	private String finYr;
	private String productCode;
	private String productName;
	private String uom;
	private String batch;
	private String expDate;
	private String mfrDate;
	private Double qty;
	private Double mrp;
	private Double cp;
	private String batchFlag;
	private Integer batchSeqNo;
	private String batchId;
	private String modifyFlag;
	
	
	
	public Integer getId() {
		return id;
	}
	public String getFinYr() {
		return finYr;
	}
	public String getProductCode() {
		return productCode;
	}
	public String getProductName() {
		return productName;
	}
	public String getUom() {
		return uom;
	}
	public String getBatch() {
		return batch;
	}
	public String getExpDate() {
		return expDate;
	}
	public String getMfrDate() {
		return mfrDate;
	}
	public Double getQty() {
		return qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public Double getCp() {
		return cp;
	}
	public String getBatchFlag() {
		return batchFlag;
	}
	public Integer getBatchSeqNo() {
		return batchSeqNo;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public void setMfrDate(String mfrDate) {
		this.mfrDate = mfrDate;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public void setCp(Double cp) {
		this.cp = cp;
	}
	public void setBatchFlag(String batchFlag) {
		this.batchFlag = batchFlag;
	}
	public void setBatchSeqNo(Integer batchSeqNo) {
		this.batchSeqNo = batchSeqNo;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
}
