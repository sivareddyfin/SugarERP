package com.finsol.bean;

public class HarvestingContractorSuretyBean {
	
	private Integer id;
	private Integer harvesterCode;
	private String surety;
	private String contactNo;
	private String aadhaarNumber;
	private String panNo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getHarvesterCode() {
		return harvesterCode;
	}
	public void setHarvesterCode(Integer harvesterCode) {
		this.harvesterCode = harvesterCode;
	}
	public String getSurety() {
		return surety;
	}
	public void setSurety(String surety) {
		this.surety = surety;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	
	
}
