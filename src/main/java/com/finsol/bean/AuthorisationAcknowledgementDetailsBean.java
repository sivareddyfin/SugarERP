package com.finsol.bean;

public class AuthorisationAcknowledgementDetailsBean 
{
	private Integer id;
	private String season;
	private String authorisationSeqNo;
	private String indentNumbers;
	private Integer kind;
	private Integer quantity;
	private Double costofEachItem;
	private String totalCost;
	private String dateofAutorisation;
	private String ryotCode;
	private Integer indentStatus;
	private Integer allowed;
	private Integer indent;
	private Integer pending;
	private Integer taken;

	private Double  ryotAmt;
	private Double  consumerAmt;
	
	
	
	
	public Integer getTaken() {
		return taken;
	}
	public void setTaken(Integer taken) {
		this.taken = taken;
	}
	public Double getRyotAmt() {
		return ryotAmt;
	}
	public Double getConsumerAmt() {
		return consumerAmt;
	}
	public void setRyotAmt(Double ryotAmt) {
		this.ryotAmt = ryotAmt;
	}
	public void setConsumerAmt(Double consumerAmt) {
		this.consumerAmt = consumerAmt;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public Integer getKind() {
		return kind;
	}
	public void setKind(Integer kind) {
		this.kind = kind;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getCostofEachItem() {
		return costofEachItem;
	}
	public void setCostofEachItem(Double costofEachItem) {
		this.costofEachItem = costofEachItem;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getDateofAutorisation() {
		return dateofAutorisation;
	}
	public void setDateofAutorisation(String dateofAutorisation) {
		this.dateofAutorisation = dateofAutorisation;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public Integer getAllowed() {
		return allowed;
	}
	public void setAllowed(Integer allowed) {
		this.allowed = allowed;
	}
	public Integer getIndent() {
		return indent;
	}
	public void setIndent(Integer indent) {
		this.indent = indent;
	}
	public Integer getPending() {
		return pending;
	}
	public void setPending(Integer pending) {
		this.pending = pending;
	}
	
	

}
