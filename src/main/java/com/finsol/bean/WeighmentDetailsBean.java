package com.finsol.bean;

public class WeighmentDetailsBean {
	
	private String ryotCode;
	private String ryotName;
	private String relativeName;
	private Integer permitNumber;
	private String villageCode;
	private Integer varietyCode;
	private Integer circleCode;
	private String landVillageCode;
	private String circle;
	private String village;
	private String variety;
	private Integer weighBridgeSlNo;
	private Integer weighmentNo;
	private String agreementNumber;
	private String loginId;
	private Integer ucCode;
	private Byte partLoad;
	private String weightmenType;
	private Byte burntCane;
	private Integer lorryorTruck;
	private Integer vehicleType;
	private String vehicleNo;
	private Integer secondPermitNo;
	private Integer serialNo;
	private Byte plantOrRatoon;
	private Integer vehicle;
	private Double netWeight;
	private String season;
	private String plotNumber;
	//Added by DMurty on 1-08-2016
	private String weighmentDate;
	private Double extentSize;


	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public String getWeighmentDate() {
		return weighmentDate;
	}
	public void setWeighmentDate(String weighmentDate) {
		this.weighmentDate = weighmentDate;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getRelativeName() {
		return relativeName;
	}
	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}
	public Integer getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(Integer permitNumber) {
		this.permitNumber = permitNumber;
	}
	
	
	
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	
	
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Integer getWeighBridgeSlNo() {
		return weighBridgeSlNo;
	}
	public void setWeighBridgeSlNo(Integer weighBridgeSlNo) {
		this.weighBridgeSlNo = weighBridgeSlNo;
	}
	public Integer getWeighmentNo() {
		return weighmentNo;
	}
	public void setWeighmentNo(Integer weighmentNo) {
		this.weighmentNo = weighmentNo;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Integer getUcCode() {
		return ucCode;
	}
	public void setUcCode(Integer ucCode) {
		this.ucCode = ucCode;
	}
	public Byte getPartLoad() {
		return partLoad;
	}
	public void setPartLoad(Byte partLoad) {
		this.partLoad = partLoad;
	}
	public String getWeightmenType() {
		return weightmenType;
	}
	public void setWeightmenType(String weightmenType) {
		this.weightmenType = weightmenType;
	}
	public Byte getBurntCane() {
		return burntCane;
	}
	public void setBurntCane(Byte burntCane) {
		this.burntCane = burntCane;
	}
	public Integer getLorryorTruck() {
		return lorryorTruck;
	}
	public void setLorryorTruck(Integer lorryorTruck) {
		this.lorryorTruck = lorryorTruck;
	}
	public Integer getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public Integer getSecondPermitNo() {
		return secondPermitNo;
	}
	public void setSecondPermitNo(Integer secondPermitNo) {
		this.secondPermitNo = secondPermitNo;
	}
	public Integer getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	public Byte getPlantOrRatoon() {
		return plantOrRatoon;
	}
	public void setPlantOrRatoon(Byte plantOrRatoon) {
		this.plantOrRatoon = plantOrRatoon;
	}
	public Integer getVehicle() {
		return vehicle;
	}
	public void setVehicle(Integer vehicle) {
		this.vehicle = vehicle;
	}
	public Double getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}

	

}
