package com.finsol.bean;

public class PlantingUpdateDetailsBean
{
	private Integer id;
	private String season;
	private String dateOfEntry;
	private String feildOfficer;
	private String variety;
	private String ryotCode;
	private Integer noOfSeedling;
	private String dateofPlanting;
	private String modifyFlag;
	
	
	
	public Integer getNoOfSeedling() {
		return noOfSeedling;
	}
	public void setNoOfSeedling(Integer noOfSeedling) {
		this.noOfSeedling = noOfSeedling;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public String getFeildOfficer() {
		return feildOfficer;
	}
	public String getVariety() {
		return variety;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	
	public String getDateofPlanting() {
		return dateofPlanting;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public void setFeildOfficer(String feildOfficer) {
		this.feildOfficer = feildOfficer;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	
	public void setDateofPlanting(String dateofPlanting) {
		this.dateofPlanting = dateofPlanting;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	
	
	
}
