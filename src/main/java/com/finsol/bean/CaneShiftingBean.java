package com.finsol.bean;

public class CaneShiftingBean
{
		private Integer id;
		private String season;
		private String caneShiftingDate;
		private Integer shift;
		private Byte labourSpplr;
		private String modifyFlag;
		private Double noOfMen;
		private Double manCost;
		private Double menTotal;
		private Double noOfWomen;
		private Double womenCost;
		private Double womenTotal;
		private Double totalLabourCost;
		private Double costPerTon;
		private Double totalTons;
		private Double totalContractAmt;
		private Double wtWithTrash;
		private Double totalCaneWt;
		private Integer gridCount;
		private Double caneWt;
		private Integer lc;
		
		public Integer getLc() {
			return lc;
		}
		public void setLc(Integer lc) {
			this.lc = lc;
		}
		public Double getCaneWt() {
			return caneWt;
		}
		public void setCaneWt(Double caneWt) {
			this.caneWt = caneWt;
		}
		public Integer getGridCount() {
			return gridCount;
		}
		public void setGridCount(Integer gridCount) {
			this.gridCount = gridCount;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getSeason() {
			return season;
		}
		public void setSeason(String season) {
			this.season = season;
		}
		public String getCaneShiftingDate() {
			return caneShiftingDate;
		}
		public void setCaneShiftingDate(String caneShiftingDate) {
			this.caneShiftingDate = caneShiftingDate;
		}
		public Integer getShift() {
			return shift;
		}
		public void setShift(Integer shift) {
			this.shift = shift;
		}
		public Byte getLabourSpplr() {
			return labourSpplr;
		}
		public void setLabourSpplr(Byte labourSpplr) {
			this.labourSpplr = labourSpplr;
		}
		public String getModifyFlag() {
			return modifyFlag;
		}
		public void setModifyFlag(String modifyFlag) {
			this.modifyFlag = modifyFlag;
		}
		public Double getNoOfMen() {
			return noOfMen;
		}
		public void setNoOfMen(Double noOfMen) {
			this.noOfMen = noOfMen;
		}
		public Double getManCost() {
			return manCost;
		}
		public void setManCost(Double manCost) {
			this.manCost = manCost;
		}
		public Double getMenTotal() {
			return menTotal;
		}
		public void setMenTotal(Double menTotal) {
			this.menTotal = menTotal;
		}
		public Double getNoOfWomen() {
			return noOfWomen;
		}
		public void setNoOfWomen(Double noOfWomen) {
			this.noOfWomen = noOfWomen;
		}
		public Double getWomenCost() {
			return womenCost;
		}
		public void setWomenCost(Double womenCost) {
			this.womenCost = womenCost;
		}
		public Double getWomenTotal() {
			return womenTotal;
		}
		public void setWomenTotal(Double womenTotal) {
			this.womenTotal = womenTotal;
		}
		public Double getTotalLabourCost() {
			return totalLabourCost;
		}
		public void setTotalLabourCost(Double totalLabourCost) {
			this.totalLabourCost = totalLabourCost;
		}
		public Double getCostPerTon() {
			return costPerTon;
		}
		public void setCostPerTon(Double costPerTon) {
			this.costPerTon = costPerTon;
		}
		public Double getTotalTons() {
			return totalTons;
		}
		public void setTotalTons(Double totalTons) {
			this.totalTons = totalTons;
		}
		public Double getTotalContractAmt() {
			return totalContractAmt;
		}
		public void setTotalContractAmt(Double totalContractAmt) {
			this.totalContractAmt = totalContractAmt;
		}
		public Double getWtWithTrash() {
			return wtWithTrash;
		}
		public void setWtWithTrash(Double wtWithTrash) {
			this.wtWithTrash = wtWithTrash;
		}
		public Double getTotalCaneWt() {
			return totalCaneWt;
		}
		public void setTotalCaneWt(Double totalCaneWt) {
			this.totalCaneWt = totalCaneWt;
		}
		
		
		
}
