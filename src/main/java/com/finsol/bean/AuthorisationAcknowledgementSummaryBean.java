package com.finsol.bean;

public class AuthorisationAcknowledgementSummaryBean 
{
	private Integer id;
	private String season;
	private String authorisationDate;
	private String ryotCode;
	private String ryotName;
	private String fieldAssistant;
	private String fieldOfficer;
	private String indentNumbers;
	private String authorisedBy;
	private String phoneNo;
	private String authorisationSeqNo;
	private String modifyFlag;
	private Integer indentStatus;
	private Integer faCode;
	private String  agreementnumber;
	private Double noOfAcr;
	private String  storeCode;
	private String authorisationNo;
	private Double grandRyotTotal;
	private Double  grandConsumerTotal;
	private Double  grandTotal;
	private Double  totalCost;
	private String billRefDate;
	private String billRefNum;
	
	

	
	
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getAuthorisationNo() {
		return authorisationNo;
	}
	public void setAuthorisationNo(String authorisationNo) {
		this.authorisationNo = authorisationNo;
	}
	public Double getGrandRyotTotal() {
		return grandRyotTotal;
	}
	public Double getGrandConsumerTotal() {
		return grandConsumerTotal;
	}
	public void setGrandRyotTotal(Double grandRyotTotal) {
		this.grandRyotTotal = grandRyotTotal;
	}
	public void setGrandConsumerTotal(Double grandConsumerTotal) {
		this.grandConsumerTotal = grandConsumerTotal;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getAuthorisationDate() {
		return authorisationDate;
	}
	public void setAuthorisationDate(String authorisationDate) {
		this.authorisationDate = authorisationDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getFieldAssistant() {
		return fieldAssistant;
	}
	public void setFieldAssistant(String fieldAssistant) {
		this.fieldAssistant = fieldAssistant;
	}
	public String getFieldOfficer() {
		return fieldOfficer;
	}
	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public String getAuthorisedBy() {
		return authorisedBy;
	}
	public void setAuthorisedBy(String authorisedBy) {
		this.authorisedBy = authorisedBy;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getBillRefDate() {
		return billRefDate;
	}
	public void setBillRefDate(String billRefDate) {
		this.billRefDate = billRefDate;
	}
	public String getBillRefNum() {
		return billRefNum;
	}
	public void setBillRefNum(String billRefNum) {
		this.billRefNum = billRefNum;
	}
	public Double getNoOfAcr() {
		return noOfAcr;
	}
	public void setNoOfAcr(Double noOfAcr) {
		this.noOfAcr = noOfAcr;
	}
	
	
	

}
