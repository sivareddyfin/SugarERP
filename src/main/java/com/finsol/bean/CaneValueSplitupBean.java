package com.finsol.bean;

public class CaneValueSplitupBean 
{
	private String season;
	private String ryotCode;
	private Integer ryotCodeSequence;
	private String ryotName;
	private Integer caneAccSlno;
	private Byte cancelProportion;
	private Double netWeight;
	private Integer slno;
	private Double seasonCanePrice;
	private Double totalValue;
	private Double forLoans;
	private Double forSbac;	
	private String villageCode;
	private Double harvestingChargesPaid;
	private Double harvestingChargesPayable;
	private Double transportingChargesPaid;
	private Double transportingChargesPayable;
	private String fromDate;
	private String todate;
	private String calcDate;
	private String allRepayDate;
	
	//Added by DMurty on 01-03-2017
	private boolean selectData;
	
	
	
	
	
	
	public boolean isSelectData() {
		return selectData;
	}
	public void setSelectData(boolean selectData) {
		this.selectData = selectData;
	}
	public String getAllRepayDate() {
		return allRepayDate;
	}
	public void setAllRepayDate(String allRepayDate) {
		this.allRepayDate = allRepayDate;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getCaneAccSlno() {
		return caneAccSlno;
	}
	public void setCaneAccSlno(Integer caneAccSlno) {
		this.caneAccSlno = caneAccSlno;
	}
	public Byte getCancelProportion() {
		return cancelProportion;
	}
	public void setCancelProportion(Byte cancelProportion) {
		this.cancelProportion = cancelProportion;
	}

	
	public Double getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	
	public Double getSeasonCanePrice() {
		return seasonCanePrice;
	}
	public void setSeasonCanePrice(Double seasonCanePrice) {
		this.seasonCanePrice = seasonCanePrice;
	}
	public Double getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}
	public Double getForLoans() {
		return forLoans;
	}
	public void setForLoans(Double forLoans) {
		this.forLoans = forLoans;
	}
	public Double getForSbac() {
		return forSbac;
	}
	public void setForSbac(Double forSbac) {
		this.forSbac = forSbac;
	}
	public Integer getRyotCodeSequence() {
		return ryotCodeSequence;
	}
	public void setRyotCodeSequence(Integer ryotCodeSequence) {
		this.ryotCodeSequence = ryotCodeSequence;
	}
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Double getHarvestingChargesPaid() {
		return harvestingChargesPaid;
	}
	public void setHarvestingChargesPaid(Double harvestingChargesPaid) {
		this.harvestingChargesPaid = harvestingChargesPaid;
	}
	public Double getHarvestingChargesPayable() {
		return harvestingChargesPayable;
	}
	public void setHarvestingChargesPayable(Double harvestingChargesPayable) {
		this.harvestingChargesPayable = harvestingChargesPayable;
	}
	public Double getTransportingChargesPaid() {
		return transportingChargesPaid;
	}
	public void setTransportingChargesPaid(Double transportingChargesPaid) {
		this.transportingChargesPaid = transportingChargesPaid;
	}
	public Double getTransportingChargesPayable() {
		return transportingChargesPayable;
	}
	public void setTransportingChargesPayable(Double transportingChargesPayable) {
		this.transportingChargesPayable = transportingChargesPayable;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}
	public String getCalcDate() {
		return calcDate;
	}
	public void setCalcDate(String calcDate) {
		this.calcDate = calcDate;
	}
	
	
	
	

}
