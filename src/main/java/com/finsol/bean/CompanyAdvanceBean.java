package com.finsol.bean;

import java.util.Date;

public class CompanyAdvanceBean
{
	private Integer id;
	private String advance;
	private String description;
	private Integer advanceCode;
	private Double maximumAmount;
	private String glCode;
	private Byte advanceMode;
	private Byte subsidyPart;
	private Byte isAmount;
	private Double amount;
	private Double percent;
	private Integer advanceFor;
	private Byte isSeedlingAdvance;
	private Double seedlingCost;
	private Byte status;
	private Byte interestApplicable;
	private String calculationDate;
	private String modifyFlag;
	
	private String screenName;
	private int onloadIsKindorAmt;

	
	
	
	public int getOnloadIsKindorAmt() {
		return onloadIsKindorAmt;
	}
	public void setOnloadIsKindorAmt(int onloadIsKindorAmt) {
		this.onloadIsKindorAmt = onloadIsKindorAmt;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public String getGlCode()
	{
		return glCode;
	}
	public void setGlCode(String glCode)
	{
		this.glCode = glCode;
	}
	
	public String getAdvance() 
	{
		return advance;
	}
	public void setAdvance(String advance)
	{
		this.advance = advance;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Integer getAdvanceCode() 
	{
		return advanceCode;
	}
	public void setAdvanceCode(Integer advanceCode) 
	{
		this.advanceCode = advanceCode;
	}
	public Double getMaximumAmount()
	{
		return maximumAmount;
	}
	public void setMaximumAmount(Double maximumAmount)
	{
		this.maximumAmount = maximumAmount;
	}
	public Byte getAdvanceMode()
	{
		return advanceMode;
	}
	public void setAdvanceMode(Byte advanceMode)
	{
		this.advanceMode = advanceMode;
	}
	public Byte getSubsidyPart()
	{
		return subsidyPart;
	}
	public void setSubsidyPart(Byte subsidyPart)
	{
		this.subsidyPart = subsidyPart;
	}
	public Byte getIsAmount()
	{
		return isAmount;
	}
	public void setIsAmount(Byte isAmount)
	{
		this.isAmount = isAmount;
	}
	public Double getAmount()
	{
		return amount;
	}
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
	public Double getPercent() 
	{
		return percent;
	}
	public void setPercent(Double percent)
	{
		this.percent = percent;
	}
	public Integer getAdvanceFor()
	{
		return advanceFor;
	}
	public void setAdvanceFor(Integer advanceFor)
	{
		this.advanceFor = advanceFor;
	}
	public Byte getIsSeedlingAdvance()
	{
		return isSeedlingAdvance;
	}
	public void setIsSeedlingAdvance(Byte isSeedlingAdvance)
	{
		this.isSeedlingAdvance = isSeedlingAdvance;
	}
	public Double getSeedlingCost() 
	{
		return seedlingCost;
	}
	public void setSeedlingCost(Double seedlingCost)
	{
		this.seedlingCost = seedlingCost;
	}
	public Byte getStatus() 
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
	public Byte getInterestApplicable() 
	{
		return interestApplicable;
	}
	public void setInterestApplicable(Byte interestApplicable)
	{
		this.interestApplicable = interestApplicable;
	}
	
	public String getCalculationDate() 
	{
		return calculationDate;
	}
	public void setCalculationDate(String calculationDate)
	{
		this.calculationDate = calculationDate;
	}
	
	
}
