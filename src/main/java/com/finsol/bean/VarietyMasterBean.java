package com.finsol.bean;

public class VarietyMasterBean {
	private Integer id;
	private Integer varietyCode;
	private String variety;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}

	
	
}
