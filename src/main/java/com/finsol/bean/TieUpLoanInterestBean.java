package com.finsol.bean;

import java.util.Date;

public class TieUpLoanInterestBean {
	
	private Integer id;
	private String season;
	private Integer branchCode;
	private String payableDate;
	private String modifyFlag;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(Integer branchCode) {
		this.branchCode = branchCode;
	}
	public String getPayableDate() {
		return payableDate;
	}
	public void setPayableDate(String payableDate) {
		this.payableDate = payableDate;
	}
	
	
	

}
