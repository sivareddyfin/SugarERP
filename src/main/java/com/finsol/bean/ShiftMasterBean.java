package com.finsol.bean;


public class ShiftMasterBean
{
	private Integer id;
	private Integer shiftId;
	private String shiftName;
	private String fromTime;
	private String toTime;
	private String shiftHrs;
	private String totalHrs;
	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public Integer getShiftId()
	{
		return shiftId;
	}
	public void setShiftId(Integer shiftId)
	{
		this.shiftId = shiftId;
	}
	public String getShiftName() 
	{
		return shiftName;
	}
	public void setShiftName(String shiftName) 
	{
		this.shiftName = shiftName;
	}
	
	
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public String getShiftHrs()
	{
		return shiftHrs;
	}
	public void setShiftHrs(String shiftHrs) 
	{
		this.shiftHrs = shiftHrs;
	}
	public String getTotalHrs()
	{
		return totalHrs;
	}
	public void setTotalHrs(String totalHrs)
	{
		this.totalHrs = totalHrs;
	}
}
