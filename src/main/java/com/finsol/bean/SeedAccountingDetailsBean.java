package com.finsol.bean;

import java.util.List;

public class SeedAccountingDetailsBean {

	private Integer seedActSlno;
	private String season;
	private String seedSupplierCode;
	private Double extentSize;
	private Double totalAmount;
	private Double payingAmount;
	private Double loansAndAdvancesPayable;
	private Double loansAndAdvancesPaid;
	private Double paidToSBAC;
	private String modifyFlag;
	private String seedSupplierName;
	private Double seedRate;
	private List advanceAndLoans;
	
	
	
	public Integer getSeedActSlno() {
		return seedActSlno;
	}
	public void setSeedActSlno(Integer seedActSlno) {
		this.seedActSlno = seedActSlno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getSeedSupplierCode() {
		return seedSupplierCode;
	}
	public void setSeedSupplierCode(String seedSupplierCode) {
		this.seedSupplierCode = seedSupplierCode;
	}
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getPayingAmount() {
		return payingAmount;
	}
	public void setPayingAmount(Double payingAmount) {
		this.payingAmount = payingAmount;
	}
	public Double getLoansAndAdvancesPayable() {
		return loansAndAdvancesPayable;
	}
	public void setLoansAndAdvancesPayable(Double loansAndAdvancesPayable) {
		this.loansAndAdvancesPayable = loansAndAdvancesPayable;
	}
	public Double getLoansAndAdvancesPaid() {
		return loansAndAdvancesPaid;
	}
	public void setLoansAndAdvancesPaid(Double loansAndAdvancesPaid) {
		this.loansAndAdvancesPaid = loansAndAdvancesPaid;
	}
	public Double getPaidToSBAC() {
		return paidToSBAC;
	}
	public void setPaidToSBAC(Double paidToSBAC) {
		this.paidToSBAC = paidToSBAC;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getSeedSupplierName() {
		return seedSupplierName;
	}
	public void setSeedSupplierName(String seedSupplierName) {
		this.seedSupplierName = seedSupplierName;
	}
	public List getAdvanceAndLoans() {
		return advanceAndLoans;
	}
	public void setAdvanceAndLoans(List advanceAndLoans) {
		this.advanceAndLoans = advanceAndLoans;
	}
	public Double getSeedRate() {
		return seedRate;
	}
	public void setSeedRate(Double seedRate) {
		this.seedRate = seedRate;
	}
	
	
}
