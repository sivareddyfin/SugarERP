package com.finsol.bean;

public class CaneAcctAmountsSeasonBean
{
	public String priceName;
	public Double amount;
	public Double subsidy;
	public Double netAmt;
	public String isThisPurchaseTax;
	public String considerForSeasonActs;
	public Integer calcid;
	
	
	public String getConsiderForSeasonActs() {
		return considerForSeasonActs;
	}
	public void setConsiderForSeasonActs(String considerForSeasonActs) {
		this.considerForSeasonActs = considerForSeasonActs;
	}
	
	public Integer getCalcid() {
		return calcid;
	}
	public void setCalcid(Integer calcid) {
		this.calcid = calcid;
	}
	public String getPriceName() {
		return priceName;
	}
	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getSubsidy() {
		return subsidy;
	}
	public void setSubsidy(Double subsidy) {
		this.subsidy = subsidy;
	}
	public Double getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}
	public String getIsThisPurchaseTax() {
		return isThisPurchaseTax;
	}
	public void setIsThisPurchaseTax(String isThisPurchaseTax) {
		this.isThisPurchaseTax = isThisPurchaseTax;
	}
	
}
