package com.finsol.bean;

public class WeighBridgeBean
{
	private Integer id;
	private Integer bridgeId;
	private String bridgeName;
	private Byte status;
	private String modifyFlag;
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getBridgeId() {
		return bridgeId;
	}
	public void setBridgeId(Integer bridgeId) {
		this.bridgeId = bridgeId;
	}
	public String getBridgeName() {
		return bridgeName;
	}
	public void setBridgeName(String bridgeName) {
		this.bridgeName = bridgeName;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}

	
	
	

}
