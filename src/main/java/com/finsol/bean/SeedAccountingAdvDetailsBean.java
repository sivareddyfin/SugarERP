package com.finsol.bean;

public class SeedAccountingAdvDetailsBean {
	private Integer seedActSlno;
	private String season;
	private String seedSupplierCode;
	private String advance;
	private String advanceCode;
	private Double principle;
	private String issuingDate;
	private String repaymentDate;
	private Double interestRate;
	private Double interestAmount;
	private Double totalPayable;
	private Double paidAmount;
	private String modifyFlag;
	private String advanceFlag;
	public Integer getSeedActSlno() {
		return seedActSlno;
	}
	public void setSeedActSlno(Integer seedActSlno) {
		this.seedActSlno = seedActSlno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getSeedSupplierCode() {
		return seedSupplierCode;
	}
	public void setSeedSupplierCode(String seedSupplierCode) {
		this.seedSupplierCode = seedSupplierCode;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	public String getAdvanceCode() {
		return advanceCode;
	}
	public void setAdvanceCode(String advanceCode) {
		this.advanceCode = advanceCode;
	}
	public Double getPrinciple() {
		return principle;
	}
	public void setPrinciple(Double principle) {
		this.principle = principle;
	}
	public String getIssuingDate() {
		return issuingDate;
	}
	public void setIssuingDate(String issuingDate) {
		this.issuingDate = issuingDate;
	}
	public String getRepaymentDate() {
		return repaymentDate;
	}
	public void setRepaymentDate(String repaymentDate) {
		this.repaymentDate = repaymentDate;
	}
	public Double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}
	public Double getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(Double interestAmount) {
		this.interestAmount = interestAmount;
	}
	public Double getTotalPayable() {
		return totalPayable;
	}
	public void setTotalPayable(Double totalPayable) {
		this.totalPayable = totalPayable;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getAdvanceFlag() {
		return advanceFlag;
	}
	public void setAdvanceFlag(String advanceFlag) {
		this.advanceFlag = advanceFlag;
	}
	
	
}
