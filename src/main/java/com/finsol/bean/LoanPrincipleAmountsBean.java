package com.finsol.bean;

public class LoanPrincipleAmountsBean 
{
	private Integer id;
	private String season;
	private String ryotCode;
	private String ryotName;
	private Integer loanNumber;
	private Double paidAmount;
	private String recommendedDate;
	private String referenceNumber;
	private Double disbursedAmount;  
	private String disbursedDate;  
	private String modifyFlag;
	private Integer transactioncode;

	
	
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season) 
	{
		this.season = season;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) 
	{
		this.ryotCode = ryotCode;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public Double getDisbursedAmount() {
		return disbursedAmount;
	}
	public void setDisbursedAmount(Double disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}
	public String getDisbursedDate() {
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(Integer loanNumber) {
		this.loanNumber = loanNumber;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getRecommendedDate() {
		return recommendedDate;
	}
	public void setRecommendedDate(String recommendedDate) {
		this.recommendedDate = recommendedDate;
	}
	

	
	

}
