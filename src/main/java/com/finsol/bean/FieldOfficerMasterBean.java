package com.finsol.bean;

public class FieldOfficerMasterBean {
	
	private Integer id;
	private Integer fieldOfficerId;
	private String fieldOfficer ;
	private Integer caneManagerId;
	private Integer employeeId;	
	private String description;
	private Byte status;
	private String modifyFlag;
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFieldOfficerId() {
		return fieldOfficerId;
	}
	public void setFieldOfficerId(Integer fieldOfficerId) {
		this.fieldOfficerId = fieldOfficerId;
	}
	public String getFieldOfficer() {
		return fieldOfficer;
	}
	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public Integer getCaneManagerId() {
		return caneManagerId;
	}
	public void setCaneManagerId(Integer caneManagerId) {
		this.caneManagerId = caneManagerId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	

}
