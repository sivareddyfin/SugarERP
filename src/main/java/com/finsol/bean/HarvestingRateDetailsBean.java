package com.finsol.bean;

public class HarvestingRateDetailsBean
{
	private Integer id;
	private String season;
	private String effectiveDate;
	private String ryotCode;
	private String ryotName;
	private Integer harvestContractorCode;
	private Double slNo;
	private Double rate;

		
	
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}


	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public String getEffectiveDate()
	{
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate)
	{
		this.effectiveDate = effectiveDate;
	}

	public String getRyotCode()
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode)
	{
		this.ryotCode = ryotCode;
	}
	
	public Integer getHarvestContractorCode()
	{
		return harvestContractorCode;
	}
	public void setHarvestContractorCode(Integer harvestContractorCode)
	{
		this.harvestContractorCode = harvestContractorCode;
	}
	public Double getSlNo() 
	{
		return slNo;
	}
	public void setSlNo(Double slNo)
	{
		this.slNo = slNo;
	}
	public Double getRate()
	{
		return rate;
	}
	public void setRate(Double rate)
	{
		this.rate = rate;
	}

}
