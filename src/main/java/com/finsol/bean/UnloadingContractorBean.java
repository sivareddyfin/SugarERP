package com.finsol.bean;

public class UnloadingContractorBean
{
	private Integer id;
	private Integer ucCode;
	private String name;
	private String contactNo;
	private Byte status;
	private String modifyFlag;
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUcCode() {
		return ucCode;
	}
	public void setUcCode(Integer ucCode) {
		this.ucCode = ucCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
}
