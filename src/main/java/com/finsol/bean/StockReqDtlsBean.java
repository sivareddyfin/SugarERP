package com.finsol.bean;

public class StockReqDtlsBean 
{
	private Integer id;
	private String finYr;
	private String stockReqNo;
	private String productCode;
	private String productGrpCode;
	private String productSubgroupCode;
	private String uom;
	private String purpose;
	private Double quantity;
	private String issuedQty;
	private Double pendingQty;
	private Double deptStock;
	private Double csStock;
	private Integer status;
	private String uomName;
	private String productName;
	private String consumption;

	
	public String getConsumption() {
		return consumption;
	}
	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}
	public Double getDeptStock() {
		return deptStock;
	}
	public void setDeptStock(Double deptStock) {
		this.deptStock = deptStock;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getStockReqNo() {
		return stockReqNo;
	}
	public void setStockReqNo(String stockReqNo) {
		this.stockReqNo = stockReqNo;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductGrpCode() {
		return productGrpCode;
	}
	public void setProductGrpCode(String productGrpCode) {
		this.productGrpCode = productGrpCode;
	}
	public String getProductSubgroupCode() {
		return productSubgroupCode;
	}
	public void setProductSubgroupCode(String productSubgroupCode) {
		this.productSubgroupCode = productSubgroupCode;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public String getIssuedQty() {
		return issuedQty;
	}
	public void setIssuedQty(String issuedQty) {
		this.issuedQty = issuedQty;
	}
	
	
	public Double getPendingQty() {
		return pendingQty;
	}
	public void setPendingQty(Double pendingQty) {
		this.pendingQty = pendingQty;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Double getCsStock() {
		return csStock;
	}
	public void setCsStock(Double csStock) {
		this.csStock = csStock;
	}
	

}
