package com.finsol.bean;

public class CaneWeighmentBean {
	
	private String  agreementNumber;
	private String season;
	private String caneWeighmentDate;
	private String caneWeighmentTime;
	private String shiftTime;
	private Integer weighBridgeId;
	private Double recieptWeightOneForShiftOne;
	private Double recieptWeightTwoForShiftOne;
	private Double totalWeightForShiftOne;
	private Double recieptWeightOneForShiftTwo;
	private Double recieptWeightTwoForShiftTwo;
	private Double totalWeightForShiftTwo;
	private Double recieptWeightOneForShiftThree;
	private Double recieptWeightTwoForShiftThree;
	private Double totalWeightForShiftThree;
	private Integer weighmentorder;
	private Integer serialNumber;
	private Integer permitNumber;
	private String grossTare;
	private String ryotCode;
	private String ryotName;
	private String relativeName;
	private String villageCode;
	private Integer circleCode;
	private String landVillageCode;
	private Integer varietyCode;
	private Integer zoneCode;
	private Byte plantOrRatoon;
	private Double extentSize;
	private Byte burntCane;
	private Integer vehicle;
	private Integer vehicleType;
	private String vehicleNumber;
	private Byte partLoad;
	private Integer secondPermitNumber;
	private Integer unloadingContractor;
	private String operatorName;
	private Double totalWeight;
	private Integer shiftId;	
	private String variety;
	private String circle;
	private String village;
	private String plotNumber;
	private Integer programNumber;
	private String particulars;
	
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getCaneWeighmentDate() {
		return caneWeighmentDate;
	}
	public void setCaneWeighmentDate(String caneWeighmentDate) {
		this.caneWeighmentDate = caneWeighmentDate;
	}
	public String getCaneWeighmentTime() {
		return caneWeighmentTime;
	}
	public void setCaneWeighmentTime(String caneWeighmentTime) {
		this.caneWeighmentTime = caneWeighmentTime;
	}
	public String getShiftTime() {
		return shiftTime;
	}
	public void setShiftTime(String shiftTime) {
		this.shiftTime = shiftTime;
	}

	
	public Integer getWeighBridgeId() {
		return weighBridgeId;
	}
	public void setWeighBridgeId(Integer weighBridgeId) {
		this.weighBridgeId = weighBridgeId;
	}
	public Double getRecieptWeightOneForShiftOne() {
		return recieptWeightOneForShiftOne;
	}
	public void setRecieptWeightOneForShiftOne(Double recieptWeightOneForShiftOne) {
		this.recieptWeightOneForShiftOne = recieptWeightOneForShiftOne;
	}
	public Double getRecieptWeightTwoForShiftOne() {
		return recieptWeightTwoForShiftOne;
	}
	public void setRecieptWeightTwoForShiftOne(Double recieptWeightTwoForShiftOne) {
		this.recieptWeightTwoForShiftOne = recieptWeightTwoForShiftOne;
	}
	public Double getTotalWeightForShiftOne() {
		return totalWeightForShiftOne;
	}
	public void setTotalWeightForShiftOne(Double totalWeightForShiftOne) {
		this.totalWeightForShiftOne = totalWeightForShiftOne;
	}
	public Double getRecieptWeightOneForShiftTwo() {
		return recieptWeightOneForShiftTwo;
	}
	public void setRecieptWeightOneForShiftTwo(Double recieptWeightOneForShiftTwo) {
		this.recieptWeightOneForShiftTwo = recieptWeightOneForShiftTwo;
	}
	public Double getRecieptWeightTwoForShiftTwo() {
		return recieptWeightTwoForShiftTwo;
	}
	public void setRecieptWeightTwoForShiftTwo(Double recieptWeightTwoForShiftTwo) {
		this.recieptWeightTwoForShiftTwo = recieptWeightTwoForShiftTwo;
	}
	public Double getTotalWeightForShiftTwo() {
		return totalWeightForShiftTwo;
	}
	public void setTotalWeightForShiftTwo(Double totalWeightForShiftTwo) {
		this.totalWeightForShiftTwo = totalWeightForShiftTwo;
	}
	public Double getRecieptWeightOneForShiftThree() {
		return recieptWeightOneForShiftThree;
	}
	public void setRecieptWeightOneForShiftThree(
			Double recieptWeightOneForShiftThree) {
		this.recieptWeightOneForShiftThree = recieptWeightOneForShiftThree;
	}
	public Double getRecieptWeightTwoForShiftThree() {
		return recieptWeightTwoForShiftThree;
	}
	public void setRecieptWeightTwoForShiftThree(
			Double recieptWeightTwoForShiftThree) {
		this.recieptWeightTwoForShiftThree = recieptWeightTwoForShiftThree;
	}
	public Double getTotalWeightForShiftThree() {
		return totalWeightForShiftThree;
	}
	public void setTotalWeightForShiftThree(Double totalWeightForShiftThree) {
		this.totalWeightForShiftThree = totalWeightForShiftThree;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(Integer permitNumber) {
		this.permitNumber = permitNumber;
	}
	public String getGrossTare() {
		return grossTare;
	}
	public void setGrossTare(String grossTare) {
		this.grossTare = grossTare;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getRelativeName() {
		return relativeName;
	}
	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}

	

	public Integer getWeighmentorder() {
		return weighmentorder;
	}
	public void setWeighmentorder(Integer weighmentorder) {
		this.weighmentorder = weighmentorder;
	}
	public Byte getPlantOrRatoon() {
		return plantOrRatoon;
	}
	public void setPlantOrRatoon(Byte plantOrRatoon) {
		this.plantOrRatoon = plantOrRatoon;
	}
	
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}

	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public Byte getBurntCane() {
		return burntCane;
	}
	public void setBurntCane(Byte burntCane) {
		this.burntCane = burntCane;
	}
	
	public Integer getVehicle() {
		return vehicle;
	}
	public void setVehicle(Integer vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public Byte getPartLoad() {
		return partLoad;
	}
	public void setPartLoad(Byte partLoad) {
		this.partLoad = partLoad;
	}
	public Integer getSecondPermitNumber() {
		return secondPermitNumber;
	}
	public void setSecondPermitNumber(Integer secondPermitNumber) {
		this.secondPermitNumber = secondPermitNumber;
	}
	public Integer getUnloadingContractor() {
		return unloadingContractor;
	}
	public void setUnloadingContractor(Integer unloadingContractor) {
		this.unloadingContractor = unloadingContractor;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	
	public Double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}
	public Integer getShiftId() {
		return shiftId;
	}
	public void setShiftId(Integer shiftId) {
		this.shiftId = shiftId;
	}

	
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}
	public Integer getProgramNumber() {
		return programNumber;
	}
	public void setProgramNumber(Integer programNumber) {
		this.programNumber = programNumber;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	
	

}
