package com.finsol.bean;

public class DeductionsbaccforPCABean {

	private Integer id;
	private String name;
 	private Double amount;
	private Integer caneacslno;
	private Double canesupplied;
	private Integer amountValidity;
	private Integer deductionOrder;
	private Integer stdDedCode;
	private Double netPayable;	
	private Integer harvestcontcode;
	private Double newPayable;
	private Double oldPending;
	private Double pendingAmount;
	private Double rate;
	private Double totalextent;
	private Byte status;
	
	//Added by DMurty on 07-09-2016
	private String columnname;
	private String tablename;
	
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getColumnname() {
		return columnname;
	}
	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public Double getCanesupplied() {
		return canesupplied;
	}
	public void setCanesupplied(Double canesupplied) {
		this.canesupplied = canesupplied;
	}
	public Integer getAmountValidity() {
		return amountValidity;
	}
	public void setAmountValidity(Integer amountValidity) {
		this.amountValidity = amountValidity;
	}
	public Integer getDeductionOrder() {
		return deductionOrder;
	}
	public void setDeductionOrder(Integer deductionOrder) {
		this.deductionOrder = deductionOrder;
	}
	public Integer getStdDedCode() {
		return stdDedCode;
	}
	public void setStdDedCode(Integer stdDedCode) {
		this.stdDedCode = stdDedCode;
	}
	public Double getNetPayable() {
		return netPayable;
	}
	public void setNetPayable(Double netPayable) {
		this.netPayable = netPayable;
	}
	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}
	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}
	public Double getNewPayable() {
		return newPayable;
	}
	public void setNewPayable(Double newPayable) {
		this.newPayable = newPayable;
	}
	public Double getOldPending() {
		return oldPending;
	}
	public void setOldPendingt(Double oldPendingt) {
		this.oldPending = oldPending;
	}
	public Double getPendingAmount() {
		return pendingAmount;
	}
	public void setPendingAmount(Double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}

	
	
}
