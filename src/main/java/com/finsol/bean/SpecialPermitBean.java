package com.finsol.bean;

public class SpecialPermitBean
{
	private Integer id;
	private Integer plotNumber;
	private Double extentSize;
	private String permitNumber;
	private Integer faCode;
	private Integer foCode;
	
	public String getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(Integer plotNumber) {
		this.plotNumber = plotNumber;
	}
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public Integer getFoCode() {
		return foCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	
	

}
