package com.finsol.bean;

public class UpdateEstimateQtyBean 
{
	private Integer circleCode;
	private Integer zoneCode;
	private String ryotCode;
	private String ryotName;
	private String fghName;
	private String agreementNumber;
	private String agreementDate;
	private String plotNumber;
	private String surveyNumber;
	private Double extentArea;
	private Double agreedQty;
	private Double estimateQty;
	
	public Double getEstimateQty() {
		return estimateQty;
	}
	public void setEstimateQty(Double estimateQty) {
		this.estimateQty = estimateQty;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getFghName() {
		return fghName;
	}
	public void setFghName(String fghName) {
		this.fghName = fghName;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getAgreementDate() {
		return agreementDate;
	}
	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}
	public String getSurveyNumber() {
		return surveyNumber;
	}
	public void setSurveyNumber(String surveyNumber) {
		this.surveyNumber = surveyNumber;
	}
	public Double getExtentArea() {
		return extentArea;
	}
	public void setExtentArea(Double extentArea) {
		this.extentArea = extentArea;
	}
	public Double getAgreedQty() {
		return agreedQty;
	}
	public void setAgreedQty(Double agreedQty) {
		this.agreedQty = agreedQty;
	}
}
