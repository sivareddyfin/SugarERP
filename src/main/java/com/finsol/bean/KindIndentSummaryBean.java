package com.finsol.bean;

public class KindIndentSummaryBean 
{
	private Integer id;
	private String season;
	private Integer seqNo;
	private String indentNo;
	private String ryotCode;
	private String ryotName;
	private String village;
	private String fatherName;
	private String aadhaarNumber;
	private String phoneNumber;
	private Integer fieldMan;
	private String modifyFlag;
	private Integer indentStatus;
	private Integer zone;
	private Integer foCode;
	private String storeCode;
	private String authorisationDate;
	private String indentedDate;
	private Integer purpose;
	private Double noOfAcr;
	private String agreementNo;
	private String individualExtent;

	public String getIndividualExtent() {
		return individualExtent;
	}
	public void setIndividualExtent(String individualExtent) {
		this.individualExtent = individualExtent;
	}
	public Double getNoOfAcr() {
		return noOfAcr;
	}
	public String getAgreementNo() {
		return agreementNo;
	}
	public void setNoOfAcr(Double noOfAcr) {
		this.noOfAcr = noOfAcr;
	}
	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}
	public Integer getPurpose() {
		return purpose;
	}
	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public String getAuthorisationDate() {
		return authorisationDate;
	}
	public String getIndentedDate() {
		return indentedDate;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public void setAuthorisationDate(String authorisationDate) {
		this.authorisationDate = authorisationDate;
	}
	public void setIndentedDate(String indentedDate) {
		this.indentedDate = indentedDate;
	}
	public Integer getFoCode() {
		return foCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	public Integer getZone()
	{
		return zone;
	}
	public void setZone(Integer zone)
	{
		this.zone = zone;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public String getIndentNo() {
		return indentNo;
	}
	
	public String getRyotName() {
		return ryotName;
	}
	
	public String getFatherName() {
		return fatherName;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getFieldMan() {
		return fieldMan;
	}
	public void setFieldMan(Integer fieldMan) {
		this.fieldMan = fieldMan;
	}


	
}
