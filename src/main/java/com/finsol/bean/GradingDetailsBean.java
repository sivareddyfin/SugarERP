package com.finsol.bean;

public class GradingDetailsBean
{
	
	private Integer id;
	private String season;
	private String gradingDate;
	private Integer shift;
	private String batch;
	private String variety;
	private String seedSupr;
	private String landVillageCode;
	private Double totalNoOfBuds;
	private String transferToTrayFilling;
	private String sentBackToSprouting;
	private String disposedToRejection;
	private Double germinationPerc;
	private String remarks;
	
	private String ryotName;
	private String varietyName;
	private String lvName;
	
	
	public String getRyotName() {
		return ryotName;
	}
	public String getVarietyName() {
		return varietyName;
	}
	public String getLvName() {
		return lvName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public void setLvName(String lvName) {
		this.lvName = lvName;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getGradingDate() {
		return gradingDate;
	}
	public void setGradingDate(String gradingDate) {
		this.gradingDate = gradingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getTotalNoOfBuds() {
		return totalNoOfBuds;
	}
	public void setTotalNoOfBuds(Double totalNoOfBuds) {
		this.totalNoOfBuds = totalNoOfBuds;
	}
	public String getTransferToTrayFilling() {
		return transferToTrayFilling;
	}
	public void setTransferToTrayFilling(String transferToTrayFilling) {
		this.transferToTrayFilling = transferToTrayFilling;
	}
	public String getSentBackToSprouting() {
		return sentBackToSprouting;
	}
	public void setSentBackToSprouting(String sentBackToSprouting) {
		this.sentBackToSprouting = sentBackToSprouting;
	}
	public String getDisposedToRejection() {
		return disposedToRejection;
	}
	public void setDisposedToRejection(String disposedToRejection) {
		this.disposedToRejection = disposedToRejection;
	}
	public Double getGerminationPerc() {
		return germinationPerc;
	}
	public void setGerminationPerc(Double germinationPerc) {
		this.germinationPerc = germinationPerc;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	
	

	
}
