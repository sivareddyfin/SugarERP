package com.finsol.bean;

public class StockInSummaryBean 
{
	private Integer id;
	private String modifyFlag;
	private String finYr;
	private String stockDate;
	private String time;
	private Integer seqNo;
	private String stockInNo;
	private String entryDate;
	private int stockTransactionNo;
	private String loginID;
	private Integer department;
	private Integer productionDepartment;
	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getStockDate() {
		return stockDate;
	}
	public void setStockDate(String stockDate) {
		this.stockDate = stockDate;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getStockInNo() {
		return stockInNo;
	}
	public void setStockInNo(String stockInNo) {
		this.stockInNo = stockInNo;
	}
	
	public int getStockTransactionNo() {
		return stockTransactionNo;
	}
	public void setStockTransactionNo(int stockTransactionNo) {
		this.stockTransactionNo = stockTransactionNo;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	
	public Integer getDepartment() {
		return department;
	}
	public Integer getProductionDepartment() {
		return productionDepartment;
	}
	
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public void setDepartment(Integer department) {
		this.department = department;
	}
	public void setProductionDepartment(Integer productionDepartment) {
		this.productionDepartment = productionDepartment;
	}
	
	
	
	
	
}
