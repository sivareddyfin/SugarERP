package com.finsol.bean;

public class ScreensBean 
{
	
	private Integer screenCode;
	private String description;
	private String screenName;
	private Boolean canAdd;
	private Boolean canModify;
	private Boolean canRead;
	private Boolean canDelete;
	private Byte isAuditTrailRequired;
	private Byte status;
	private String modifyFlag;

	
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getScreenCode() {
		return screenCode;
	}
	public void setScreenCode(Integer screenCode) {
		this.screenCode = screenCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Boolean getCanAdd() {
		return canAdd;
	}
	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}
	public Boolean getCanModify() {
		return canModify;
	}
	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}
	public Boolean getCanRead() {
		return canRead;
	}
	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}
	public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	public Byte getIsAuditTrailRequired() {
		return isAuditTrailRequired;
	}
	public void setIsAuditTrailRequired(Byte isAuditTrailRequired) {
		this.isAuditTrailRequired = isAuditTrailRequired;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	

}
