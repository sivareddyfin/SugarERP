package com.finsol.bean;

public class ScreenFieldsBean 
{
	private Integer id;
	private Integer screenId;
	private String fieldId;
	private String fieldLabel;
	private Byte isAuditTrail;
	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getScreenId() 
	{
		return screenId;
	}
	public void setScreenId(Integer screenId) 
	{
		this.screenId = screenId;
	}
	public String getFieldId() 
	{
		return fieldId;
	}
	public void setFieldId(String fieldId) 
	{
		this.fieldId = fieldId;
	}
	public String getFieldLabel()
	{
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel)
	{
		this.fieldLabel = fieldLabel;
	}
	public Byte getIsAuditTrail() 
	{
		return isAuditTrail;
	}
	public void setIsAuditTrail(Byte isAuditTrail)
	{
		this.isAuditTrail = isAuditTrail;
	}
	
	
	
}
