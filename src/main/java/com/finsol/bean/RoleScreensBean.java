package com.finsol.bean;

public class RoleScreensBean
{
	private Integer id;
	private Integer rollId;
	private Integer screenCode;
	private Boolean canAdd;
	private Boolean canModify;
	private Boolean canRead;
	private Boolean canDelete;
	private Boolean isAuditTrailRequired;
	private Byte status;
	private String screenName;
	private String description;	
	private String modifyFlag;
	
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRollId() 
	{
		return rollId;
	}
	public void setRollId(Integer rollId)
	{
		this.rollId = rollId;
	}
	public Boolean getCanAdd()
	{
		return canAdd;
	}
	public void setCanAdd(Boolean canAdd) 
	{
		this.canAdd = canAdd;
	}
	public Boolean getCanModify() {
		return canModify;
	}
	public void setCanModify(Boolean canModify)
	{
		this.canModify = canModify;
	}
	public Boolean getCanRead()
	{
		return canRead;
	}
	public void setCanRead(Boolean canRead)
	{
		this.canRead = canRead;
	}
	public Boolean getCanDelete() 
	{
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete)
	{
		this.canDelete = canDelete;
	}
	public Integer getScreenCode() {
		return screenCode;
	}
	public void setScreenCode(Integer screenCode) {
		this.screenCode = screenCode;
	}
	public Boolean getIsAuditTrailRequired() {
		return isAuditTrailRequired;
	}
	public void setIsAuditTrailRequired(Boolean isAuditTrailRequired) {
		this.isAuditTrailRequired = isAuditTrailRequired;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	

}
