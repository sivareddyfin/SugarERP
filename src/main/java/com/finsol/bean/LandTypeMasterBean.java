package com.finsol.bean;

public class LandTypeMasterBean {
	
	private Integer id;	
	private Integer landTypeId;
	private String landType;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	private String screenName;
	
	public String getScreenName() 
	{
		return screenName;
	}
	public void setScreenName(String screenName) 
	{
		this.screenName = screenName;
	}
	
	
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getLandTypeId() {
		return landTypeId;
	}
	public void setLandTypeId(Integer landTypeId) {
		this.landTypeId = landTypeId;
	}
	public String getLandType() {
		return landType;
	}
	public void setLandType(String landType) {
		this.landType = landType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	

}
