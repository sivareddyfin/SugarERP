package com.finsol.bean;

public class SeedlingTrayChargesDetailsBean 
{
	private Integer id;
	private Integer trayTypeCode;
	private Integer noofTrays;
	private Double cost;
	private Double totalCost;
	private String villageCode;
	private Byte status;
	
	private Integer slno;
	private String screenName;
	
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public Integer getTrayTypeCode() 
	{
		return trayTypeCode;
	}
	public void setTrayTypeCode(Integer trayTypeCode) 
	{
		this.trayTypeCode = trayTypeCode;
	}
	public Integer getNoofTrays()
	{
		return noofTrays;
	}
	public void setNoofTrays(Integer noofTrays)
	{
		this.noofTrays = noofTrays;
	}
	public Double getCost()
	{
		return cost;
	}
	public void setCost(Double cost) 
	{
		this.cost = cost;
	}
	public Double getTotalCost()
	{
		return totalCost;
	}
	public void setTotalCost(Double totalCost) 
	{
		this.totalCost = totalCost;
	}

	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Byte getStatus()
	{
		return status;
	}
	public void setStatus(Byte status) 
	{
		this.status = status;
	}
}
