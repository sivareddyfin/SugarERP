package com.finsol.bean;

public class DetrashingDetailsBean 
{
	
	private Integer id;
	private String season;
	private String detrashingDetailsDate;
	private Integer detrashingshift;
	private String batchSeries;
	private String modifyFlag;
	private Integer variety;
	private String seedSupr;
	private String landVillageCode;
	private Double wtWithTrash;
	private Double wtWithoutTrash;
	//Added by sahadeva 06-01-2017
	private String ryotName;
	private String varietyName;
	private String lvName;
	private Integer lc;
	//
	
	

	public String getLvName() {
		return lvName;
	}

	public Integer getLc() {
		return lc;
	}

	public void setLc(Integer lc) {
		this.lc = lc;
	}

	public void setLvName(String lvName) {
		this.lvName = lvName;
	}

	public String getVarietyName() {
		return varietyName;
	}
	
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getDetrashingDetailsDate() {
		return detrashingDetailsDate;
	}
	public void setDetrashingDetailsDate(String detrashingDetailsDate) {
		this.detrashingDetailsDate = detrashingDetailsDate;
	}
	
	public Integer getDetrashingshift() {
		return detrashingshift;
	}
	public void setDetrashingshift(Integer detrashingshift) {
		this.detrashingshift = detrashingshift;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getVariety() {
		return variety;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getWtWithTrash() {
		return wtWithTrash;
	}
	public void setWtWithTrash(Double wtWithTrash) {
		this.wtWithTrash = wtWithTrash;
	}
	public Double getWtWithoutTrash() {
		return wtWithoutTrash;
	}
	public void setWtWithoutTrash(Double wtWithoutTrash) {
		this.wtWithoutTrash = wtWithoutTrash;
	}



}
