package com.finsol.bean;

import java.util.Date;

public class ReGeneratePermitBean 
{
	private Integer permitNumber;
	private Integer newpermitNumber;
	private String villageCode;
	private String ryotCode;
	private String ryotNamee;
	private String agreementNo;
	private String mandalName;
	private String season;
	private String plotNo;
	

	private String date;
	
	public Integer getPermitNumber()
	{
		return permitNumber;
	}
	public void setPermitNumber(Integer permitNumber) 
	{
		this.permitNumber = permitNumber;
	}
	public Integer getNewpermitNumber()
	{
		return newpermitNumber;
	}
	public void setNewpermitNumber(Integer newpermitNumber)
	{
		this.newpermitNumber = newpermitNumber;
	}
	public String getVillageCode() 
	{
		return villageCode;
	}
	public void setVillageCode(String villageCode) 
	{
		this.villageCode = villageCode;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode)
	{
		this.ryotCode = ryotCode;
	}
	public String getRyotNamee()
	{
		return ryotNamee;
	}
	public void setRyotNamee(String ryotNamee)
	{
		this.ryotNamee = ryotNamee;
	}
	public String getAgreementNo()
	{
		return agreementNo;
	}
	public void setAgreementNo(String agreementNo)
	{
		this.agreementNo = agreementNo;
	}
	public String getMandalName()
	{
		return mandalName;
	}
	public void setMandalName(String mandalName) 
	{
		this.mandalName = mandalName;
	}
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getPlotNo() {
		return plotNo;
	}
	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	

}
