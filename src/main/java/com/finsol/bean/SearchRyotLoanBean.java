package com.finsol.bean;

public class SearchRyotLoanBean
{
	private Integer loanNumber;
	private Double disbursedAmount;
	private String recommendedDate;
	private String fieldOfficer;
	private String disbursedDate;
	private String referenceNumber;
	
	public Integer getLoanNumber() 
	{
		return loanNumber;
	}
	public void setLoanNumber(Integer loanNumber)
	{
		this.loanNumber = loanNumber;
	}
	public String getRecommendedDate()
	{
		return recommendedDate;
	}
	public void setRecommendedDate(String recommendedDate)
	{
		this.recommendedDate = recommendedDate;
	}
	public String getFieldOfficer() 
	{
		return fieldOfficer;
	}
	public void setFieldOfficer(String fieldOfficer) 
	{
		this.fieldOfficer = fieldOfficer;
	}
	public Double getDisbursedAmount()
	{
		return disbursedAmount;
	}
	public void setDisbursedAmount(Double disbursedAmount)
	{
		this.disbursedAmount = disbursedAmount;
	}
	public String getDisbursedDate() 
	{
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate)
	{
		this.disbursedDate = disbursedDate;
	}
	public String getReferenceNumber() 
	{
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber)
	{
		this.referenceNumber = referenceNumber;
	}
	
	

}
