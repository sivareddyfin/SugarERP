package com.finsol.bean;

public class GreenHouseDetailsBean
{
	
	private Integer id;
	private String season;
	private String date;
	private Integer shift;
	private String batch;
	private String variety;
	private Integer trayTypeIn;
	private double noOfTraysIn;
	private double noOfCavitiesPertrayin;
	private double totalSeedlingsIn;
	private Integer trayTypeOut;
	private double noOfTraysOut;
	private double noOfCavitiesPertrayOut;
	private double totalSeedlingsOut;
	private String varietyName;
	
	
	
	
	public String getVarietyName() {
		return varietyName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public Integer getId() {
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getDate() {
		return date;
	}
	public Integer getShift() {
		return shift;
	}
	public String getBatch() {
		return batch;
	}
	public String getVariety() {
		return variety;
	}
	
	public double getNoOfTraysIn() {
		return noOfTraysIn;
	}
	public double getNoOfCavitiesPertrayin() {
		return noOfCavitiesPertrayin;
	}
	public double getTotalSeedlingsIn() {
		return totalSeedlingsIn;
	}
	
	public double getNoOfTraysOut() {
		return noOfTraysOut;
	}
	public double getNoOfCavitiesPertrayOut() {
		return noOfCavitiesPertrayOut;
	}
	public double getTotalSeedlingsOut() {
		return totalSeedlingsOut;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	
	public void setNoOfTraysIn(double noOfTraysIn) {
		this.noOfTraysIn = noOfTraysIn;
	}
	public void setNoOfCavitiesPertrayin(double noOfCavitiesPertrayin) {
		this.noOfCavitiesPertrayin = noOfCavitiesPertrayin;
	}
	public void setTotalSeedlingsIn(double totalSeedlingsIn) {
		this.totalSeedlingsIn = totalSeedlingsIn;
	}
	
	public Integer getTrayTypeIn() {
		return trayTypeIn;
	}
	public Integer getTrayTypeOut() {
		return trayTypeOut;
	}
	public void setTrayTypeIn(Integer trayTypeIn) {
		this.trayTypeIn = trayTypeIn;
	}
	public void setTrayTypeOut(Integer trayTypeOut) {
		this.trayTypeOut = trayTypeOut;
	}
	public void setNoOfTraysOut(double noOfTraysOut) {
		this.noOfTraysOut = noOfTraysOut;
	}
	public void setNoOfCavitiesPertrayOut(double noOfCavitiesPertrayOut) {
		this.noOfCavitiesPertrayOut = noOfCavitiesPertrayOut;
	}
	public void setTotalSeedlingsOut(double totalSeedlingsOut) {
		this.totalSeedlingsOut = totalSeedlingsOut;
	}
	
	
	
	


}
