package com.finsol.bean;

public class UomBean 
{
	private Integer id;
	private Integer uomCode;
	private String uom;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	public Integer getId()
	{
		return id;
	}
	public Integer getUomCode() {
		return uomCode;
	}
	public String getUom() {
		return uom;
	}
	public String getDescription() {
		return description;
	}
	public Byte getStatus() {
		return status;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUomCode(Integer uomCode) {
		this.uomCode = uomCode;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	
}
