package com.finsol.bean;

public class ZoneMasterBean {
	private Integer id;
	private Integer zoneCode;
	private String zone;
	private Integer caneManagerId;
	private Integer fieldOfficerId;
	private Integer regionCode;	
	private String description;
	private Byte status;
	private String modifyFlag;
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getCaneManagerId() {
		return caneManagerId;
	}
	public void setCaneManagerId(Integer caneManagerId) {
		this.caneManagerId = caneManagerId;
	}
	public Integer getFieldOfficerId() {
		return fieldOfficerId;
	}
	public void setFieldOfficerId(Integer fieldOfficerId) {
		this.fieldOfficerId = fieldOfficerId;
	}
	public Integer getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(Integer regionCode) {
		this.regionCode = regionCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
	
	
}
