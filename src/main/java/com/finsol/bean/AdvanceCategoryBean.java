package com.finsol.bean;

public class AdvanceCategoryBean
{
	private Integer id;
	private Integer advanceCategoryCode;
	private String advanceCategoryName;
	private String description;
	private Integer advanceCode;
	private Byte status;
	private String modifyFlag;
	
	private String screenName;
	
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAdvanceCategoryCode() {
		return advanceCategoryCode;
	}
	public void setAdvanceCategoryCode(Integer advanceCategoryCode) {
		this.advanceCategoryCode = advanceCategoryCode;
	}
	public String getAdvanceCategoryName() {
		return advanceCategoryName;
	}
	public void setAdvanceCategoryName(String advanceCategoryName) {
		this.advanceCategoryName = advanceCategoryName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getAdvanceCode() {
		return advanceCode;
	}
	public void setAdvanceCode(Integer advanceCode) {
		this.advanceCode = advanceCode;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
	
	
	
}
