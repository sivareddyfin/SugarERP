package com.finsol.bean;

public class WeighBridgeTestDataBean {
private Integer shift;
private Integer bridgeName;
private String operatorName;
private Double totalWeight;
private String testWeighmentTime;
private String testWeighmentDate;




public String getTestWeighmentTime() {
	return testWeighmentTime;
}
public void setTestWeighmentTime(String testWeighmentTime) {
	this.testWeighmentTime = testWeighmentTime;
}
public String getTestWeighmentDate() {
	return testWeighmentDate;
}
public void setTestWeighmentDate(String testWeighmentDate) {
	this.testWeighmentDate = testWeighmentDate;
}
public Integer getShift() {
	return shift;
}
public void setShift(Integer shift) {
	this.shift = shift;
}

public Integer getBridgeName() {
	return bridgeName;
}
public void setBridgeName(Integer bridgeName) {
	this.bridgeName = bridgeName;
}


public String getOperatorName() {
	return operatorName;
}
public void setOperatorName(String operatorName) {
	this.operatorName = operatorName;
}
public Double getTotalWeight() {
	return totalWeight;
}
public void setTotalWeight(Double totalWeight) {
	this.totalWeight = totalWeight;
}


	
	
	
}
