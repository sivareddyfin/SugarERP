package com.finsol.bean;

public class HardeningSummaryBean 
{
	private Integer id;
	private String season;
	private String Date;
	private Integer shift;
	private Byte labourSpplr;
	private String modifyFlag;
	private Double noOfMen;
	private Double manCost;
	private Double menTotal;
	private Double noOfWomen;
	private Double womenCost;
	private Double womenTotal;
	private Double totalLabourCost;
	private Double costPerTon;
	private Double totalTons;
	private Double totalContractAmt;
	private Double grandTotalIn;
	private Double grandTotalOut;
	private Integer gridCount;
	private Integer lc;
	
	
	public Integer getLc()
	{
		return lc;
	}
	public void setLc(Integer lc)
	{
		this.lc = lc;
	}
	public Integer getId() {
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getDate() {
		return Date;
	}
	public Integer getShift() {
		return shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public Double getWomenCost() {
		return womenCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public Double getGrandTotalIn() {
		return grandTotalIn;
	}
	public Double getGrandTotalOut() {
		return grandTotalOut;
	}
	public Integer getGridCount() {
		return gridCount;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDate(String date) {
		Date = date;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public void setWomenCost(Double womenCost) {
		this.womenCost = womenCost;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public void setGrandTotalIn(Double grandTotalIn) {
		this.grandTotalIn = grandTotalIn;
	}
	public void setGrandTotalOut(Double grandTotalOut) {
		this.grandTotalOut = grandTotalOut;
	}
	public void setGridCount(Integer gridCount) {
		this.gridCount = gridCount;
	}
	
	
	
}
