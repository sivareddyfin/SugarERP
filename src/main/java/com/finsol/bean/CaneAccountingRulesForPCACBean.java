package com.finsol.bean;

public class CaneAccountingRulesForPCACBean {
	
	
	private Integer id;
	private String season;
	private Double seasonCanePrice;
	private Byte isPercentOrAmt;
	private Double amtForDed;
	private Double percentForDed;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Double getSeasonCanePrice() {
		return seasonCanePrice;
	}
	public void setSeasonCanePrice(Double seasonCanePrice) {
		this.seasonCanePrice = seasonCanePrice;
	}
	public Byte getIsPercentOrAmt() {
		return isPercentOrAmt;
	}
	public void setIsPercentOrAmt(Byte isPercentOrAmt) {
		this.isPercentOrAmt = isPercentOrAmt;
	}
	public Double getAmtForDed() {
		return amtForDed;
	}
	public void setAmtForDed(Double amtForDed) {
		this.amtForDed = amtForDed;
	}
	public Double getPercentForDed() {
		return percentForDed;
	}
	public void setPercentForDed(Double percentForDed) {
		this.percentForDed = percentForDed;
	}


	
}
