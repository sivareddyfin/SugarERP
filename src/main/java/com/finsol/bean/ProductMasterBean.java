package com.finsol.bean;

public class ProductMasterBean 
{
	private String productGroup;
	private String productSubGroup;
	private String productCode;
	private String productName;
	private Integer uom;
	private String description;
	private Byte status;
	private Integer stockMethod;
	private String  modifyFlag;
	
	
	public String getProductSubGroup() {
		return productSubGroup;
	}
	public String getProductCode() {
		return productCode;
	}
	public String getProductName() {
		return productName;
	}
	
	public String getDescription() {
		return description;
	}
	public Byte getStatus() {
		return status;
	}
	public Integer getStockMethod() {
		return stockMethod;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public void setProductSubGroup(String productSubGroup) {
		this.productSubGroup = productSubGroup;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public Integer getUom() {
		return uom;
	}
	public void setUom(Integer uom) {
		this.uom = uom;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public void setStockMethod(Integer stockMethod) {
		this.stockMethod = stockMethod;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	

	
}
