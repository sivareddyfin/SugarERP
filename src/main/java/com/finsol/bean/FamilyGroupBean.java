package com.finsol.bean;

public class FamilyGroupBean
{
	private Integer id;
	private Integer familyGroupCode;
	private String familyGroupName;
	private Integer zoneCode;
	private Integer circleCode;
	private String description;
	private Byte status;
	private String modifyFlag;
	private String familyGroupMembers;
	
	public String getFamilyGroupMembers() {
		return familyGroupMembers;
	}
	public void setFamilyGroupMembers(String familyGroupMembers) {
		this.familyGroupMembers = familyGroupMembers;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFamilyGroupCode() {
		return familyGroupCode;
	}
	public void setFamilyGroupCode(Integer familyGroupCode) {
		this.familyGroupCode = familyGroupCode;
	}
	public String getFamilyGroupName() {
		return familyGroupName;
	}
	public void setFamilyGroupName(String familyGroupName) {
		this.familyGroupName = familyGroupName;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
}
