package com.finsol.bean;

public class CaneAccountingRulesWeighmentDetailsForPCACBean {
	
	
	private Integer id;
	private String season;	
	private Double seasonCanePrice;
	private Byte isPercentOrAmt;
	private Double amtForDed;
	private Double percentForDed;
	private String ryotCode;
	private String ryotName;
	private String villageCode;
	private Double netWeight;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Double getSeasonCanePrice() {
		return seasonCanePrice;
	}
	public void setSeasonCanePrice(Double seasonCanePrice) {
		this.seasonCanePrice = seasonCanePrice;
	}
	public Byte getIsPercentOrAmt() {
		return isPercentOrAmt;
	}
	public void setIsPercentOrAmt(Byte isPercentOrAmt) {
		this.isPercentOrAmt = isPercentOrAmt;
	}
	public Double getAmtForDed() {
		return amtForDed;
	}
	public void setAmtForDed(Double amtForDed) {
		this.amtForDed = amtForDed;
	}
	public Double getPercentForDed() {
		return percentForDed;
	}
	public void setPercentForDed(Double percentForDed) {
		this.percentForDed = percentForDed;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}

	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Double getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}


	
}
