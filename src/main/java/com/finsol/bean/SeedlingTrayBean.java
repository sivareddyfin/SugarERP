package com.finsol.bean;

public class SeedlingTrayBean {
	
	private Integer id;
	private Integer trayCode;
	private String trayName;
	private Integer lifeOfTray;
	private Integer noOfSeedlingPerTray;
	private Byte trayType;
	private String description;
	private Byte status;
	private String modifyFlag;
	private String screenName;
	
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTrayCode() {
		return trayCode;
	}
	public void setTrayCode(Integer trayCode) {
		this.trayCode = trayCode;
	}
	public String getTrayName() {
		return trayName;
	}
	public void setTrayName(String trayName) {
		this.trayName = trayName;
	}
	public Integer getLifeOfTray() {
		return lifeOfTray;
	}
	public void setLifeOfTray(Integer lifeOfTray) {
		this.lifeOfTray = lifeOfTray;
	}
	public Integer getNoOfSeedlingPerTray() {
		return noOfSeedlingPerTray;
	}
	public void setNoOfSeedlingPerTray(Integer noOfSeedlingPerTray) {
		this.noOfSeedlingPerTray = noOfSeedlingPerTray;
	}
	public Byte getTrayType() {
		return trayType;
	}
	public void setTrayType(Byte trayType) {
		this.trayType = trayType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
}
