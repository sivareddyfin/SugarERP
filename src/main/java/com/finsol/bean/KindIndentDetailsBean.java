package com.finsol.bean;

public class KindIndentDetailsBean 
{
	private Integer id;
	private Integer kind;
	private Integer seqNo;
	private String batchDate;
	private Double quantity;
	private String uom;
	private String landVillageCode;
	private String indentNo;
	private String season;
	private Integer status;
	private Double dispatchedQuantity;
	private Double pendingQuantity;
	private Integer variety;
	
	public Double getQuantity() {
		return quantity;
	}
	public Double getDispatchedQuantity() {
		return dispatchedQuantity;
	}
	public Double getPendingQuantity() {
		return pendingQuantity;
	}
	public Integer getVariety() {
		return variety;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public void setDispatchedQuantity(Double dispatchedQuantity) {
		this.dispatchedQuantity = dispatchedQuantity;
	}
	public void setPendingQuantity(Double pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getId() 
	{
		return id;
	}
	
	public Integer getSeqNo() {
		return seqNo;
	}
	public String getBatchDate() {
		return batchDate;
	}
	
	public String getUom() {
		return uom;
	}
	
	public String getIndentNo() {
		return indentNo;
	}
	public String getSeason() {
		return season;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getKind() {
		return kind;
	}
	public void setKind(Integer kind) {
		this.kind = kind;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}
	
	public void setUom(String uom) {
		this.uom = uom;
	}
	
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	public void setSeason(String season) {
		this.season = season;
	}


	
}
