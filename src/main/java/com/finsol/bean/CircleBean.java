package com.finsol.bean;

public class CircleBean 
{
	private Integer id;
	private Integer circleCode;
	private String villageCode;
	private String circle;
	private String description;
	private Integer fieldAssistantId;
	private Integer fieldOfficerId;
	private Integer caneManagerId;
	private Byte status;
	private String modifyFlag;
	private String village;
	private String fieldAssistant;
	private  Integer zone;
	private String zoneName;
	
	

	public String getFieldAssistant() {
		return fieldAssistant;
	}
	public void setFieldAssistant(String fieldAssistant) {
		this.fieldAssistant = fieldAssistant;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getCircleCode()
	{
		return circleCode;
	}
	public void setCircleCode(Integer circleCode)
	{
		this.circleCode = circleCode;
	}
	
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getCircle() 
	{
		return circle;
	}
	public void setCircle(String circle) 
	{
		this.circle = circle;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Integer getFieldAssistantId()
	{
		return fieldAssistantId;
	}
	public void setFieldAssistantId(Integer fieldAssistantId)
	{
		this.fieldAssistantId = fieldAssistantId;
	}
	public Integer getFieldOfficerId()
	{
		return fieldOfficerId;
	}
	public void setFieldOfficerId(Integer fieldOfficerId) 
	{
		this.fieldOfficerId = fieldOfficerId;
	}
	public Integer getCaneManagerId() 
	{
		return caneManagerId;
	}
	public void setCaneManagerId(Integer caneManagerId)
	{
		this.caneManagerId = caneManagerId;
	}
	public Byte getStatus() 
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
	public Integer getZone() {
		return zone;
	}
	public void setZone(Integer zone) {
		this.zone = zone;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	
}
