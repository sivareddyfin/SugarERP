package com.finsol.bean;

public class SeedAdvanceCostBean 
{
	private Integer id;
	public Double costOfEachUnit;	
	public Double fromDistance;	
	public Double toDistance;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getCostOfEachUnit() {
		return costOfEachUnit;
	}
	public void setCostOfEachUnit(Double costOfEachUnit) {
		this.costOfEachUnit = costOfEachUnit;
	}
	public Double getFromDistance() {
		return fromDistance;
	}
	public void setFromDistance(Double fromDistance) {
		this.fromDistance = fromDistance;
	}
	public Double getToDistance() {
		return toDistance;
	}
	public void setToDistance(Double toDistance) {
		this.toDistance = toDistance;
	}
}
