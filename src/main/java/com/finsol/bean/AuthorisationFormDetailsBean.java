package com.finsol.bean;

import javax.persistence.Column;

public class AuthorisationFormDetailsBean 
{
	private Integer id;
	private String season;
	private String authorisationSeqNo;
	private String indentNumbers;
	private Integer kind;
	private Integer quantity;
	private Double costofEachItem;
	private String totalCost;
	private String dateofAutorisation;
	private String ryotCode;
	private Integer indentStatus;
	private Integer allowed;
	private Integer indent;
	private Integer pending;
	
	

	
	public Integer getAllowed() {
		return allowed;
	}
	public Integer getIndent() {
		return indent;
	}
	public Integer getPending() {
		return pending;
	}
	public void setAllowed(Integer allowed) {
		this.allowed = allowed;
	}
	public void setIndent(Integer indent) {
		this.indent = indent;
	}
	public void setPending(Integer pending) {
		this.pending = pending;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	public Double getCostofEachItem() {
		return costofEachItem;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public String getDateofAutorisation() {
		return dateofAutorisation;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	
	public Integer getKind() {
		return kind;
	}
	public void setKind(Integer kind) {
		this.kind = kind;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public void setCostofEachItem(Double costofEachItem) {
		this.costofEachItem = costofEachItem;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public void setDateofAutorisation(String dateofAutorisation) {
		this.dateofAutorisation = dateofAutorisation;
	}
	
}
