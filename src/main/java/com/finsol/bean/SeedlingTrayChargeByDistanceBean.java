package com.finsol.bean;

public class SeedlingTrayChargeByDistanceBean {
	
	private Integer id;
	private Double distanceFrom;
	private Double distanceTo;
	private Double trayCharge;
	private Integer trayCode;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getDistanceFrom() {
		return distanceFrom;
	}
	public void setDistanceFrom(Double distanceFrom) {
		this.distanceFrom = distanceFrom;
	}
	public Double getDistanceTo() {
		return distanceTo;
	}
	public void setDistanceTo(Double distanceTo) {
		this.distanceTo = distanceTo;
	}
	public Double getTrayCharge() {
		return trayCharge;
	}
	public void setTrayCharge(Double trayCharge) {
		this.trayCharge = trayCharge;
	}
	public Integer getTrayCode() {
		return trayCode;
	}
	public void setTrayCode(Integer trayCode) {
		this.trayCode = trayCode;
	}
	

}
