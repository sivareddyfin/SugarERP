package com.finsol.bean;

import javax.persistence.Column;

public class BudCuttingSummaryBean 
{
	
	
	private Integer id;
	private String season;
	private String date;
	private Integer shift;
	private Byte labourSpplr;
	private Double noOfMen;
	private Double manCost;
	private Double menTotal;
	private Double noOfWomen;
	private Double womanCost;
	private Double womenTotal;
	private Integer gridCount;
	private Double totalLabourCost;
	private Double costPerTon;
	private Double totalTons;
	private Double totalContractAmt;
	private Double totalNoOfBuds;
	private String modifyFlag;
	private Double totalCaneWt;
	private Double budGrandTotal;
	private Double noOfMachines;
	
	private Byte machineOrMen;
	
	private Integer lc;
	
	
	public Integer getLc() {
		return lc;
	}
	public void setLc(Integer lc) {
		this.lc = lc;
	}
	public Double getNoOfMachines() {
		return noOfMachines;
	}
	public void setNoOfMachines(Double noOfMachines) {
		this.noOfMachines = noOfMachines;
	}
	public Byte getMachineOrMen() {
		return machineOrMen;
	}
	public void setMachineOrMen(Byte machineOrMen) {
		this.machineOrMen = machineOrMen;
	}
	public Double getBudGrandTotal() {
		return budGrandTotal;
	}
	public void setBudGrandTotal(Double budGrandTotal) {
		this.budGrandTotal = budGrandTotal;
	}
	public Double getTotalCaneWt() {
		return totalCaneWt;
	}
	public void setTotalCaneWt(Double totalCaneWt) {
		this.totalCaneWt = totalCaneWt;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId()
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getGridCount() {
		return gridCount;
	}
	public void setGridCount(Integer gridCount) {
		this.gridCount = gridCount;
	}
	public String getDate() {
		return date;
	}
	public Integer getShift() {
		return shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public Double getWomanCost() {
		return womanCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public Double getTotalNoOfBuds() {
		return totalNoOfBuds;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public void setWomanCost(Double womanCost) {
		this.womanCost = womanCost;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public void setTotalNoOfBuds(Double totalNoOfBuds) {
		this.totalNoOfBuds = totalNoOfBuds;
	}
	
}
