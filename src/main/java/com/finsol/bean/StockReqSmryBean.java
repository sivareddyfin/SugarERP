package com.finsol.bean;

public class StockReqSmryBean
{
	private Integer id;
	private String modifyFlag;
	private String finYr;
	private String reqDate;
	private String reqTime;
	private Integer seqNo;
	private String stockReqNo;
	private Integer deptID;
	private String reqlogninID;
	private Integer status;
	private String authorisedDt;
	private String authorisedTime;
	private String authorisedUser;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getStockReqNo() {
		return stockReqNo;
	}
	public void setStockReqNo(String stockReqNo) {
		this.stockReqNo = stockReqNo;
	}
	public Integer getDeptID() {
		return deptID;
	}
	public void setDeptID(Integer deptID) {
		this.deptID = deptID;
	}
	public String getReqlogninID() {
		return reqlogninID;
	}
	public void setReqlogninID(String reqlogninID) {
		this.reqlogninID = reqlogninID;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getAuthorisedDt() {
		return authorisedDt;
	}
	public void setAuthorisedDt(String authorisedDt) {
		this.authorisedDt = authorisedDt;
	}
	public String getAuthorisedTime() {
		return authorisedTime;
	}
	public void setAuthorisedTime(String authorisedTime) {
		this.authorisedTime = authorisedTime;
	}
	public String getAuthorisedUser() {
		return authorisedUser;
	}
	public void setAuthorisedUser(String authorisedUser) {
		this.authorisedUser = authorisedUser;
	}
	
}
