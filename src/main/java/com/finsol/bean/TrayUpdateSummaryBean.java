package com.finsol.bean;

public class TrayUpdateSummaryBean 
{
	private Integer id;
	private String season;
	private String ryotCode;
	private Integer zone;
	private Integer village;
	private Integer circle;
	private String vehicle;
	private String driverNo;
	private String testedBy;
	private String modifyFlag;
	private String summaryDate;
	
	
	
	public String getVehicle() {
		return vehicle;
	}
	public String getDriverNo() {
		return driverNo;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}
	public String getSummaryDate() {
		return summaryDate;
	}
	public void setSummaryDate(String summaryDate) {
		this.summaryDate = summaryDate;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId()
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	
	public Integer getZone() {
		return zone;
	}
	public Integer getVillage() {
		return village;
	}
	public Integer getCircle() {
		return circle;
	}
	
	public String getTestedBy() {
		return testedBy;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setZone(Integer zone) {
		this.zone = zone;
	}
	public void setVillage(Integer village) {
		this.village = village;
	}
	public void setCircle(Integer circle) {
		this.circle = circle;
	}
	
	public void setTestedBy(String testedBy) {
		this.testedBy = testedBy;
	}
	
	
	

}
