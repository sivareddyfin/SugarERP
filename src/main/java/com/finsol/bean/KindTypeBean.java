package com.finsol.bean;

public class KindTypeBean
{
		private Integer id;
		private String kindType;
		private String description;
		private Byte status;
		private String uom;
		private String modifyFlag;
		
		private Byte roundingTo;
		private Double limitperAcre;
		private Double costofeachItem;
		private Integer advance;

		private Integer plantMaxCount;
		private Integer ratoonMaxCount;

		
		public Integer getPlantMaxCount() {
			return plantMaxCount;
		}
		public Integer getRatoonMaxCount() {
			return ratoonMaxCount;
		}
		public void setPlantMaxCount(Integer plantMaxCount) {
			this.plantMaxCount = plantMaxCount;
		}
		public void setRatoonMaxCount(Integer ratoonMaxCount) {
			this.ratoonMaxCount = ratoonMaxCount;
		}
		public Byte getRoundingTo() {
			return roundingTo;
		}
		public Double getLimitperAcre() {
			return limitperAcre;
		}
		public Double getCostofeachItem() {
			return costofeachItem;
		}
		public Integer getAdvance() {
			return advance;
		}
		public void setRoundingTo(Byte roundingTo) {
			this.roundingTo = roundingTo;
		}
		public void setLimitperAcre(Double limitperAcre) {
			this.limitperAcre = limitperAcre;
		}
		public void setCostofeachItem(Double costofeachItem) {
			this.costofeachItem = costofeachItem;
		}
		public void setAdvance(Integer advance) {
			this.advance = advance;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getModifyFlag() {
			return modifyFlag;
		}
		public void setModifyFlag(String modifyFlag) {
			this.modifyFlag = modifyFlag;
		}
		public String getKindType() {
			return kindType;
		}
		public void setKindType(String kindType) {
			this.kindType = kindType;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Byte getStatus() {
			return status;
		}
		public void setStatus(Byte status) {
			this.status = status;
		}
		public String getUom() {
			return uom;
		}
		public void setUom(String uom) {
			this.uom = uom;
		}
		
	

}
