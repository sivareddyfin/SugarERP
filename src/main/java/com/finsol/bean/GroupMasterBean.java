package com.finsol.bean;

public class GroupMasterBean
{
	private String productGroupCode;
	private Integer groupCode;
	private String group;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	public String getProductGroupCode() 
	{
		return productGroupCode;
	}
	public Integer getGroupCode() {
		return groupCode;
	}
	public String getGroup() {
		return group;
	}
	public String getDescription() {
		return description;
	}
	public Byte getStatus() {
		return status;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setProductGroupCode(String productGroupCode) {
		this.productGroupCode = productGroupCode;
	}
	public void setGroupCode(Integer groupCode) {
		this.groupCode = groupCode;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	
	
}
