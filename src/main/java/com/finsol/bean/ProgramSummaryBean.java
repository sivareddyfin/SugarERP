package com.finsol.bean;

public class ProgramSummaryBean
{
	
	private String season;
	private Integer programNumber;
	private Integer periodId;
	private Integer monthId;
	private Integer yearOfPlanting;
	private String typeOfExtent;
	private String varietyOfCane;
	private Double totalExtent;
	private Double totalPlant;
	private Double totalRatoon;
	private String modifyFlag;

	
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag)
	{
		this.modifyFlag = modifyFlag;
	}
	public String getSeason()
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public Integer getProgramNumber()
	{
		return programNumber;
	}
	public void setProgramNumber(Integer programNumber)
	{
		this.programNumber = programNumber;
	}
	public Integer getPeriodId()
	{
		return periodId;
	}
	public void setPeriodId(Integer periodId) 
	{
		this.periodId = periodId;
	}
	public Integer getMonthId()
	{
		return monthId;
	}
	public void setMonthId(Integer monthId)
	{
		this.monthId = monthId;
	}
	public Integer getYearOfPlanting()
	{
		return yearOfPlanting;
	}
	public void setYearOfPlanting(Integer yearOfPlanting) 
	{
		this.yearOfPlanting = yearOfPlanting;
	}
	public String getTypeOfExtent()
	{
		return typeOfExtent;
	}
	public void setTypeOfExtent(String typeOfExtent)
	{
		this.typeOfExtent = typeOfExtent;
	}
	
	public String getVarietyOfCane() {
		return varietyOfCane;
	}
	public void setVarietyOfCane(String varietyOfCane) {
		this.varietyOfCane = varietyOfCane;
	}
	public Double getTotalExtent() 
	{
		return totalExtent;
	}
	public void setTotalExtent(Double totalExtent) 
	{
		this.totalExtent = totalExtent;
	}
	public Double getTotalPlant() 
	{
		return totalPlant;
	}
	public void setTotalPlant(Double totalPlant)
	{
		this.totalPlant = totalPlant;
	}
	public Double getTotalRatoon()
	{
		return totalRatoon;
	}
	public void setTotalRatoon(Double totalRatoon)
	{
		this.totalRatoon = totalRatoon;
	}
	
	

}
