package com.finsol.bean;

public class RyotAdvanceBean {
	private Integer id;
	private String ryotName;
	private String ryotCode;
	private String season;
	private Double loanAmount;
	private String agreementNumber;
	private Integer advanceCategoryCode;
	private Byte isItSeedling;
	private Integer advanceCode;
	//private Integer varietyCode;
	private Double extentSize;
	private Integer advanceSubCategoryCode;
	private String seedSupplierCode;
	private Integer seedlingsQuantity;
	private Double totalCost;
	private Double ryotPayable;
	private Integer advanceType;
	private String loginId;
	private String modifyFlag;
	private Integer transactionCode;
	private Integer onloadSeedlingStatus;
	private Byte isSeedlingAdvance;
	private Double availableBalance;
	private String advanceDate;
	private Double plantExtent;
	private Double ratoonExtent;

	public Double getAvailableBalance() {
	return availableBalance;
	}
	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}
	public String getAdvanceDate() {
		return advanceDate;
	}
	public void setAdvanceDate(String advanceDate) {
		this.advanceDate = advanceDate;
	}
	public Integer getOnloadSeedlingStatus() {
		return onloadSeedlingStatus;
	}
	public void setOnloadSeedlingStatus(Integer onloadSeedlingStatus) {
		this.onloadSeedlingStatus = onloadSeedlingStatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(Integer transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Integer getAdvanceType() {
		return advanceType;
	}
	public void setAdvanceType(Integer advanceType) {
		this.advanceType = advanceType;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public Integer getAdvanceCategoryCode() {
		return advanceCategoryCode;
	}
	public void setAdvanceCategoryCode(Integer advanceCategoryCode) {
		this.advanceCategoryCode = advanceCategoryCode;
	}
	public Byte getIsItSeedling() {
		return isItSeedling;
	}
	public void setIsItSeedling(Byte isItSeedling) {
		this.isItSeedling = isItSeedling;
	}
	
	public Integer getAdvanceCode() {
		return advanceCode;
	}
	public void setAdvanceCode(Integer advanceCode) {
		this.advanceCode = advanceCode;
	}
/*	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}*/
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public Integer getAdvanceSubCategoryCode() {
		return advanceSubCategoryCode;
	}
	public void setAdvanceSubCategoryCode(Integer advanceSubCategoryCode) {
		this.advanceSubCategoryCode = advanceSubCategoryCode;
	}
	
	public String getSeedSupplierCode() {
		return seedSupplierCode;
	}
	public void setSeedSupplierCode(String seedSupplierCode) {
		this.seedSupplierCode = seedSupplierCode;
	}
	public Integer getSeedlingsQuantity() {
		return seedlingsQuantity;
	}
	public void setSeedlingsQuantity(Integer seedlingsQuantity) {
		this.seedlingsQuantity = seedlingsQuantity;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Double getRyotPayable() {
		return ryotPayable;
	}
	public void setRyotPayable(Double ryotPayable) {
		this.ryotPayable = ryotPayable;
	}
	public Byte getIsSeedlingAdvance() {
		return isSeedlingAdvance;
	}
	public void setIsSeedlingAdvance(Byte isSeedlingAdvance) {
		this.isSeedlingAdvance = isSeedlingAdvance;
	}
	public Double getPlantExtent() {
		return plantExtent;
	}
	public void setPlantExtent(Double plantExtent) {
		this.plantExtent = plantExtent;
	}
	public Double getRatoonExtent() {
		return ratoonExtent;
	}
	public void setRatoonExtent(Double ratoonExtent) {
		this.ratoonExtent = ratoonExtent;
	}

	

}
