package com.finsol.bean;


/**
 * @author Rama Krishna
 *
 */
public class DepartmentMasterBean {

	
	private Integer id;
	private Integer deptCode;
	private String department;
	private String deptShortcut;
	private String description;	
	private String location;
	private String contactPerson;
	private String extension;
	private String directNumber;
	private String status;
	private String modifyFlag;
	
	private String screenName;
	

	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDeptCode() {
		return deptCode;
	}
	public void setDeptCode(Integer deptCode) {
		this.deptCode = deptCode;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDeptShortcut() {
		return deptShortcut;
	}
	public void setDeptShortcut(String deptShortcut) {
		this.deptShortcut = deptShortcut;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getDirectNumber() {
		return directNumber;
	}
	public void setDirectNumber(String directNumber) {
		this.directNumber = directNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
