package com.finsol.bean;

public class UpdateCcsBean 
{
	private String season;
	private Integer samplecard;
	private Integer program;
	private String ryotCode;
	private String agreementNumber;
	private String plotNumber;
	private String plantOrRatoon;
	private Double extentSize;
	private String pol;
	private String brix;
	private String purity;
	private Double ccsRating;
	private Double avgCcsRating;
	private Double fgAvgCcsRating;
	private String ryotName;
	private String variety;
	private String village;
	private Integer sampleCardCount;
	private String pageName;
	
	private String sampleDate;
	
	public String getSampleDate() {
		return sampleDate;
	}
	public void setSampleDate(String sampleDate) {
		this.sampleDate = sampleDate;
		
		
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	
	public Integer getSampleCardCount() {
		return sampleCardCount;
	}
	public void setSampleCardCount(Integer sampleCardCount) {
		this.sampleCardCount = sampleCardCount;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public Integer getSamplecard()
	{
		return samplecard;
	}
	public void setSamplecard(Integer samplecard)
	{
		this.samplecard = samplecard;
	}	
	public Integer getProgram() {
		return program;
	}
	public void setProgram(Integer program) {
		this.program = program;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode)
	{
		this.ryotCode = ryotCode;
	}
	
	
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getPlotNumber()
	{
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber)
	{
		this.plotNumber = plotNumber;
	}
	public String getPlantOrRatoon()
	{
		return plantOrRatoon;
	}
	public void setPlantOrRatoon(String plantOrRatoon)
	{
		this.plantOrRatoon = plantOrRatoon;
		
	}
	public Double getExtentSize() 
	{
		return extentSize;
	}
	public void setExtentSize(Double extentSize)
	{
		this.extentSize = extentSize;
	}
	public String getPol() 
	{
		return pol;
	}
	public void setPol(String pol) 
	{
		this.pol = pol;
	}
	public String getBrix() 
	{
		return brix;
	}
	public void setBrix(String brix) 
	{
		this.brix = brix;
	}
	public String getPurity() 
	{
		return purity;
	}
	public void setPurity(String purity)
	{
		this.purity = purity;
	}
	public Double getCcsRating()
	{
		return ccsRating;
	}
	public void setCcsRating(Double ccsRating) 
	{
		this.ccsRating = ccsRating;
	}
	public Double getAvgCcsRating()
	{
		return avgCcsRating;
		
	}
	public void setAvgCcsRating(Double avgCcsRating)
{
		this.avgCcsRating = avgCcsRating;
	}
	public Double getFgAvgCcsRating()
	{
		return fgAvgCcsRating;
	}
	public void setFgAvgCcsRating(Double fgAvgCcsRating)
	{
		this.fgAvgCcsRating = fgAvgCcsRating;
	}

}
