package com.finsol.bean;

public class ChemicalTreatmentDetailsBean 
{
	private Integer id;
	private String season;
	private Integer shift;
	private String date;
	private String batchSeries;
	private String variety;
	private String seedSupr;
	private String landVillageCode;
	private Double noOfTubs;
	private Double tubWt;
	
	private String ryotName;
	private String varietyName;
	private String lvName;
	
	public String getRyotName() {
		return ryotName;
	}
	public String getVarietyName() {
		return varietyName;
	}
	public String getLvName() {
		return lvName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public void setLvName(String lvName) {
		this.lvName = lvName;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getNoOfTubs() {
		return noOfTubs;
	}
	public void setNoOfTubs(Double noOfTubs) {
		this.noOfTubs = noOfTubs;
	}
	public Double getTubWt() {
		return tubWt;
	}
	public void setTubWt(Double tubWt) {
		this.tubWt = tubWt;
	}
	
	
	
	
	
	
}
