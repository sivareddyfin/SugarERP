package com.finsol.bean;

import javax.persistence.Column;

public class AgreementDetailsBean 
{

	private Integer id;
	private Integer plotSeqNo;
	private String plotNumber;
	private Integer varietyCode;
	private String plantorRatoon;
	private String cropDate;
	private Double agreedQty;
	private Double extentSize;
	private String surveyNumber;
	private String landVillageCode;
	private Integer landTypeCode;
	private Integer circleCode;
	private Byte plotIdentity;
	private String extentImage;
	private Integer slno;
	private String screenName;
	private Double plotDistance;
	private String address;
	private Integer foCode;
	private Integer faCode;
	
	
	public Double getPlotDistance() {
		return plotDistance;
	}
	public void setPlotDistance(Double plotDistance) {
		this.plotDistance = plotDistance;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getExtentImage() {
		return extentImage;
	}
	public void setExtentImage(String extentImage) {
		this.extentImage = extentImage;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPlotSeqNo() {
		return plotSeqNo;
	}
	public void setPlotSeqNo(Integer plotSeqNo) {
		this.plotSeqNo = plotSeqNo;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public String getPlantorRatoon() {
		return plantorRatoon;
	}
	public void setPlantorRatoon(String plantorRatoon) {
		this.plantorRatoon = plantorRatoon;
	}
	public String getCropDate() {
		return cropDate;
	}
	public void setCropDate(String cropDate) {
		this.cropDate = cropDate;
	}
	public Double getAgreedQty() {
		return agreedQty;
	}
	public void setAgreedQty(Double agreedQty) {
		this.agreedQty = agreedQty;
	}
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public String getSurveyNumber() {
		return surveyNumber;
	}
	public void setSurveyNumber(String surveyNumber) {
		this.surveyNumber = surveyNumber;
	}
	
	
	
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Integer getLandTypeCode() {
		return landTypeCode;
	}
	public void setLandTypeCode(Integer landTypeCode) {
		this.landTypeCode = landTypeCode;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public Byte getPlotIdentity() {
		return plotIdentity;
	}
	public void setPlotIdentity(Byte plotIdentity) {
		this.plotIdentity = plotIdentity;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Integer getFoCode() {
		return foCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	

	


}
