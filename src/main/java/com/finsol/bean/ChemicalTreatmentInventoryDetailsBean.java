package com.finsol.bean;

public class ChemicalTreatmentInventoryDetailsBean
{
	private Integer id;
	private String productDate;
	private Integer productShift;
	private String productSeason;
	private String productCode;
	private String productQuantity;
	private String productName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProductDate() {
		return productDate;
	}
	public void setProductDate(String productDate) {
		this.productDate = productDate;
	}
	
	public Integer getProductShift() {
		return productShift;
	}
	public void setProductShift(Integer productShift) {
		this.productShift = productShift;
	}
	public String getProductSeason() {
		return productSeason;
	}
	public void setProductSeason(String productSeason) {
		this.productSeason = productSeason;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductQuantity() {
		return productQuantity;
	}
	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	
	
	
	
	

}
