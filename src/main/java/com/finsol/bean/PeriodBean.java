package com.finsol.bean;

public class PeriodBean
{
	
	private Integer periodId;
	private String period;
	private Integer startDate;
	private Integer endDate;
	
	public Integer getPeriodId() 
	{
		return periodId;
	}
	public void setPeriodId(Integer periodId) 
	{
		this.periodId = periodId;
	}
	public String getPeriod()
	{
		return period;
	}
	public void setPeriod(String period) 
	{
		this.period = period;
	}
	public Integer getStartDate() 
	{
		return startDate;
	}
	public void setStartDate(Integer startDate)
	{
		this.startDate = startDate;
	}
	public Integer getEndDate()
	{
		return endDate;
	}
	public void setEndDate(Integer endDate)
	{
		this.endDate = endDate;
	}
	
	

}
