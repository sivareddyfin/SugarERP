package com.finsol.bean;

import javax.persistence.Column;

public class ShiftedCaneDetailsBean 
{
	private Integer id;
	private String season;
	private String shiftingCaneDate;
	private Integer ShiftingCaneshift;
	private String batchSeries;
	private String modifyFlag;
	private Integer variety;
	private String seedSupr;
	private String landVillageCode;
	private Double caneWt;

	private String ryotName;
	private String varietyName;
	private String lvName;
	
	
	public String getRyotName() {
		return ryotName;
	}
	public String getVarietyName() {
		return varietyName;
	}
	public String getLvName() {
		return lvName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public void setLvName(String lvName) {
		this.lvName = lvName;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getShiftingCaneDate() {
		return shiftingCaneDate;
	}
	public void setShiftingCaneDate(String shiftingCaneDate) {
		this.shiftingCaneDate = shiftingCaneDate;
	}
	public Integer getShiftingCaneshift() {
		return ShiftingCaneshift;
	}
	public void setShiftingCaneshift(Integer shiftingCaneshift) {
		ShiftingCaneshift = shiftingCaneshift;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getVariety() {
		return variety;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getCaneWt() {
		return caneWt;
	}
	public void setCaneWt(Double caneWt) {
		this.caneWt = caneWt;
	}
	
	







	
	
	
	
	
	

	



	
	
}
