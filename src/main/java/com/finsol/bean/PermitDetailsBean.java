package com.finsol.bean;


public class PermitDetailsBean 
{

	private String season;
	private String ryotCode;
	private String ryotName;
	private String relativeName;
	private Integer permitNumber;
	private String villageCode;
	private Byte plantOrRatoon;
	private Integer varietyCode;
	private Double extentSize;
	private Integer circleCode;
	private String landVillageCode;
	private Integer zoneCode;
	private String agreementNumber;
	private String circle;
	private String village;
	private String variety;
	private String plotNumber;
	private Integer programNumber;
	private String permitDate;
	private Integer rank;
	private Integer slno;
	private Byte vehicle;
	private  String vehicleNumber;
	
	public Byte getVehicle() {
		return vehicle;
	}
	public void setVehicle(Byte vehicle) {
		this.vehicle = vehicle;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	private boolean hrvFlag;
	private String harvestDate;
	
	public boolean isHrvFlag() {
		return hrvFlag;
	}
	public void setHrvFlag(boolean hrvFlag) {
		this.hrvFlag = hrvFlag;
	}
	public String getHarvestDate() {
		return harvestDate;
	}
	public void setHarvestDate(String harvestDate) {
		this.harvestDate = harvestDate;
	}
	public String getPermitDate() {
		return permitDate;
	}
	public void setPermitDate(String permitDate) {
		this.permitDate = permitDate;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getRelativeName() {
		return relativeName;
	}
	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}
	public Integer getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(Integer permitNumber) {
		this.permitNumber = permitNumber;
	}

	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public Byte getPlantOrRatoon() {
		return plantOrRatoon;
	}
	public void setPlantOrRatoon(Byte plantOrRatoon) {
		this.plantOrRatoon = plantOrRatoon;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public Double getExtentSize() {
		return extentSize;
	}
	public void setExtentSize(Double extentSize) {
		this.extentSize = extentSize;
	}
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	
	
	
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Integer getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}
	public Integer getProgramNumber() {
		return programNumber;
	}
	public void setProgramNumber(Integer programNumber) {
		this.programNumber = programNumber;
	}
	
	
	
	

}
