package com.finsol.bean;

public class CaneWeightBean 
{
	private Integer id;
	private String season;
	private Double bmPercentage;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Double getBmPercentage() {
		return bmPercentage;
	}
	public void setBmPercentage(Double bmPercentage) {
		this.bmPercentage = bmPercentage;
	}
	
	
	
	

}
