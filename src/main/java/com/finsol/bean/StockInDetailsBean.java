package com.finsol.bean;

public class StockInDetailsBean 
{
	private Integer id;
	private String finYr;
	private String productCode;
	private String uom;
	private String batch;
	private String expDt;
	private Double quantity;
	private Double mrp;
	private Double cp;
	private String productName;
	private String stockInNo ;
	private String productGrpCode;
	private String productSubGrpCode;
	private String batchFlag;
	private String uomName;

	//Added by DMurty on 07-03-2017
	private String batchId;
	private Integer batchSeqNo;
	
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public String getBatchFlag() {
		return batchFlag;
	}
	public void setBatchFlag(String batchFlag) {
		this.batchFlag = batchFlag;
	}
	
	public Integer getBatchSeqNo() {
		return batchSeqNo;
	}
	public void setBatchSeqNo(Integer batchSeqNo) {
		this.batchSeqNo = batchSeqNo;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getStockInNo() {
		return stockInNo;
	}
	public String getProductGrpCode() {
		return productGrpCode;
	}
	public String getProductSubGrpCode() {
		return productSubGrpCode;
	}
	public void setStockInNo(String stockInNo) {
		this.stockInNo = stockInNo;
	}
	public void setProductGrpCode(String productGrpCode) {
		this.productGrpCode = productGrpCode;
	}
	public void setProductSubGrpCode(String productSubGrpCode) {
		this.productSubGrpCode = productSubGrpCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpDt() {
		return expDt;
	}
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCp() {
		return cp;
	}
	public void setCp(Double cp) {
		this.cp = cp;
	}
	
	
	
	
	
}
