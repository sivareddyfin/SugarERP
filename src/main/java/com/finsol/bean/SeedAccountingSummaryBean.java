package com.finsol.bean;

public class SeedAccountingSummaryBean {
	private Integer seedActSlno;
	private String season;
	private String seedAccDate;
	private String loginId;                           
	private String modifyFlag;
	private String villageCode;
	
	
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getSeedActSlno() {
		return seedActSlno;
	}
	public void setSeedActSlno(Integer seedActSlno) {
		this.seedActSlno = seedActSlno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getSeedAccDate() {
		return seedAccDate;
	}
	public void setSeedAccDate(String seedAccDate) {
		this.seedAccDate = seedAccDate;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	
	
}
