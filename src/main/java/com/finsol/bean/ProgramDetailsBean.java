package com.finsol.bean;

public class ProgramDetailsBean 
{
	private String ryotCode;
	private String ryotName;
	private String agreementNo;
	private String plotNo;
	private Double plantAcres;
	private Double ratoonAcres;
	private Integer programNumber;
	private String modifyFlag;
	
	

	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getAgreementNo() {
		return agreementNo;
	}
	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}
	public String getPlotNo() {
		return plotNo;
	}
	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}
	public Double getPlantAcres() {
		return plantAcres;
	}
	public void setPlantAcres(Double plantAcres) {
		this.plantAcres = plantAcres;
	}
	public Double getRatoonAcres() {
		return ratoonAcres;
	}
	public void setRatoonAcres(Double ratoonAcres) {
		this.ratoonAcres = ratoonAcres;
	}
	public Integer getProgramNumber() {
		return programNumber;
	}
	public void setProgramNumber(Integer programNumber) {
		this.programNumber = programNumber;
	}
	
	

}
