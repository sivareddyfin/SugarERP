package com.finsol.bean;

public class PermitRulesBean {

	private Integer id;
	private Integer noOfTonsPerAcre;
	
	private Integer noOfTonsPerEachPermit;
	private Byte permitType;
	private Integer noOfPermitPerAcre;
	
	public Byte getPermitType() 
	{
		return permitType;
	}
	public void setPermitType(Byte permitType) 
	{
		this.permitType = permitType;
	}
	public Integer getNoOfPermitPerAcre()
	{
		return noOfPermitPerAcre;
	}
	public void setNoOfPermitPerAcre(Integer noOfPermitPerAcre)
	{
		this.noOfPermitPerAcre = noOfPermitPerAcre;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNoOfTonsPerAcre() {
		return noOfTonsPerAcre;
	}
	public void setNoOfTonsPerAcre(Integer noOfTonsPerAcre) {
		this.noOfTonsPerAcre = noOfTonsPerAcre;
	}
	public Integer getNoOfTonsPerEachPermit() {
		return noOfTonsPerEachPermit;
	}
	public void setNoOfTonsPerEachPermit(Integer noOfTonsPerEachPermit) {
		this.noOfTonsPerEachPermit = noOfTonsPerEachPermit;
	}
	
	
	
}
