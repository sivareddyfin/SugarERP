package com.finsol.bean;

public class SampleCardRulesBean {
	
	private Integer id;
	private Double sizeFrom;
	private Double sizeTo;
	private Integer noOfSampleCards;
	private String modifyFlag;
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getSizeFrom() {
		return sizeFrom;
	}
	public void setSizeFrom(Double sizeFrom) {
		this.sizeFrom = sizeFrom;
	}
	public Double getSizeTo() {
		return sizeTo;
	}
	public void setSizeTo(Double sizeTo) {
		this.sizeTo = sizeTo;
	}
	public Integer getNoOfSampleCards() {
		return noOfSampleCards;
	}
	public void setNoOfSampleCards(Integer noOfSampleCards) {
		this.noOfSampleCards = noOfSampleCards;
	}
	

}
