package com.finsol.bean;

public class AccountGroupBean 
{
	private Integer id;
	private Integer accountGroupCode;
	private String accountGroup;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}


	
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public Integer getAccountGroupCode() 
	{
		return accountGroupCode;
	}
	public void setAccountGroupCode(Integer accountGroupCode) 
	{
		this.accountGroupCode = accountGroupCode;
	}
	public String getAccountGroup() 
	{
		return accountGroup;
	}
	public void setAccountGroup(String accountGroup)
	{
		this.accountGroup = accountGroup;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description) 
	{
		this.description = description;
	}
	public Byte getStatus()
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
}
