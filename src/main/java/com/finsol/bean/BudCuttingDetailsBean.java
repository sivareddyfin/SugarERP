package com.finsol.bean;

public class BudCuttingDetailsBean 
{

	private Integer id;
	private String Season;
	private Integer Shift;
	private String date;
	private String BatchSeries;
	private String Variety;
	private String SeedSupr;
	private String LandVillageCode;
	private String Machine;
	private Double BudsPerKG;
	private Double NoOfTubs;
	private Double TubWt;
	private Double TotalBuds;
	
	private String ryotName;
	private String varietyName;
	private String lvName;
	
	
	public String getRyotName() {
		return ryotName;
	}
	public String getVarietyName() {
		return varietyName;
	}
	public String getLvName() {
		return lvName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}
	public void setLvName(String lvName) {
		this.lvName = lvName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return Season;
	}
	public void setSeason(String season) {
		Season = season;
	}
	public Integer getShift() {
		return Shift;
	}
	public void setShift(Integer shift) {
		Shift = shift;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBatchSeries() {
		return BatchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		BatchSeries = batchSeries;
	}
	public String getVariety() {
		return Variety;
	}
	public void setVariety(String variety) {
		Variety = variety;
	}
	public String getSeedSupr() {
		return SeedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		SeedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return LandVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		LandVillageCode = landVillageCode;
	}
	public String getMachine() {
		return Machine;
	}
	public void setMachine(String machine) {
		Machine = machine;
	}
	public Double getBudsPerKG() {
		return BudsPerKG;
	}
	public void setBudsPerKG(Double budsPerKG) {
		BudsPerKG = budsPerKG;
	}
	public Double getNoOfTubs() {
		return NoOfTubs;
	}
	public void setNoOfTubs(Double noOfTubs) {
		NoOfTubs = noOfTubs;
	}
	public Double getTubWt() {
		return TubWt;
	}
	public void setTubWt(Double tubWt) {
		TubWt = tubWt;
	}
	public Double getTotalBuds() {
		return TotalBuds;
	}
	public void setTotalBuds(Double totalBuds) {
		TotalBuds = totalBuds;
	}
	
	
	
	

	
}
