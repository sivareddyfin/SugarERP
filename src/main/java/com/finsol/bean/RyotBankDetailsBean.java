package com.finsol.bean;

public class RyotBankDetailsBean {

	private Integer id;
	private String ryotCode;
	private Integer ryotBankBranchCode;
	private String ifscCode;
	private String accountNumber;
	private Byte accountType;
	
	private String season;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public Integer getRyotBankBranchCode() {
		return ryotBankBranchCode;
	}
	public void setRyotBankBranchCode(Integer ryotBankBranchCode) {
		this.ryotBankBranchCode = ryotBankBranchCode;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Byte getAccountType() {
		return accountType;
	}
	public void setAccountType(Byte accountType) {
		this.accountType = accountType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	
	
	
}
