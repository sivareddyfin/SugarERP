package com.finsol.bean;

public class AdvancesLoansForPCABean {
	
	public String advance;	
	public Integer advorder;	
	public Double advanceamount;	
	public Integer advancecode;	
	public String  advancedate;	
	public Double advanceinterest;	
	public String advancerepaydate;	
	public long glcode;	
	public Double loanPayable;	
	public Double loanPendingAmount;	
	public Double tempTotal;	
	public Double totalamount;
	public Integer caneacslno;
	public String subventedInterestRate;
	public String advanceflag;
	
	public Double netRate;
	public Double interestAmount;
	public int branchcode;
	public String referencenumber;
	public Integer loanNo;
	public double intamount;
	
	
	public double getIntamount() {
		return intamount;
	}
	public void setIntamount(double intamount) {
		this.intamount = intamount;
	}
	public Integer getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(Integer loanNo) {
		this.loanNo = loanNo;
	}
	public int getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(int branchcode) {
		this.branchcode = branchcode;
	}
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}
	public Double getNetRate() {
		return netRate;
	}
	public void setNetRate(Double netRate) {
		this.netRate = netRate;
	}
	public Double getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(Double interestAmount) {
		this.interestAmount = interestAmount;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}

	public Double getAdvanceamount() {
		return advanceamount;
	}
	public void setAdvanceamount(Double advanceamount) {
		this.advanceamount = advanceamount;
	}
	public Integer getAdvancecode() {
		return advancecode;
	}
	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}
	public String getAdvancedate() {
		return advancedate;
	}
	public void setAdvancedate(String advancedate) {
		this.advancedate = advancedate;
	}
	public Double getAdvanceinterest() {
		return advanceinterest;
	}
	public void setAdvanceinterest(Double advanceinterest) {
		this.advanceinterest = advanceinterest;
	}
	public String getAdvancerepaydate() {
		return advancerepaydate;
	}
	public void setAdvancerepaydate(String advancerepaydate) {
		this.advancerepaydate = advancerepaydate;
	}
	public Double getLoanPayable() {
		return loanPayable;
	}
	public void setLoanPayable(Double loanPayable) {
		this.loanPayable = loanPayable;
	}
	public Double getLoanPendingAmount() {
		return loanPendingAmount;
	}
	public void setLoanPendingAmount(Double loanPendingAmount) {
		this.loanPendingAmount = loanPendingAmount;
	}
	public Double getTempTotal() {
		return tempTotal;
	}
	public void setTempTotal(Double tempTotal) {
		this.tempTotal = tempTotal;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public String getSubventedInterestRate() {
		return subventedInterestRate;
	}
	public void setSubventedInterestRate(String subventedInterestRate) {
		this.subventedInterestRate = subventedInterestRate;
	}
	public String getAdvanceflag() {
		return advanceflag;
	}
	public void setAdvanceflag(String advanceflag) {
		this.advanceflag = advanceflag;
	}
	
	
	public Integer getAdvorder() {
		return advorder;
	}
	public void setAdvorder(Integer advorder) {
		this.advorder = advorder;
	}
	public long getGlcode() {
		return glcode;
	}
	public void setGlcode(long glcode) {
		this.glcode = glcode;
	}
	
	

}
