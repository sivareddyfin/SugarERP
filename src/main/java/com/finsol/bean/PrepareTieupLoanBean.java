package com.finsol.bean;

public class PrepareTieupLoanBean {
	
	private String season;
	private String addedLoan;
	private String ryotName;
	private Integer branchCode;
	private String plantExtent;
	private String recommendedDate;
	private String ryotCode;
	private String agreementNumber;
	private String ratoonExtent;
	private Integer loanNumber;
	private String surveyNumber;
	private Double recommendedLoan;
	private String screenName;
	private Integer batchno;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getAddedLoan() {
		return addedLoan;
	}
	public void setAddedLoan(String addedLoan) {
		this.addedLoan = addedLoan;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(Integer branchCode) {
		this.branchCode = branchCode;
	}
	public String getPlantExtent() {
		return plantExtent;
	}
	public void setPlantExtent(String plantExtent) {
		this.plantExtent = plantExtent;
	}
	public String getRecommendedDate() {
		return recommendedDate;
	}
	public void setRecommendedDate(String recommendedDate) {
		this.recommendedDate = recommendedDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getRatoonExtent() {
		return ratoonExtent;
	}
	public void setRatoonExtent(String ratoonExtent) {
		this.ratoonExtent = ratoonExtent;
	}
	public Integer getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(Integer loanNumber) {
		this.loanNumber = loanNumber;
	}
	public String getSurveyNumber() {
		return surveyNumber;
	}
	public void setSurveyNumber(String surveyNumber) {
		this.surveyNumber = surveyNumber;
	}
	public Double getRecommendedLoan() {
		return recommendedLoan;
	}
	public void setRecommendedLoan(Double recommendedLoan) {
		this.recommendedLoan = recommendedLoan;
	}
	public Integer getBatchno() {
		return batchno;
	}
	public void setBatchno(Integer batchno) {
		this.batchno = batchno;
	}
	
	
	

}
