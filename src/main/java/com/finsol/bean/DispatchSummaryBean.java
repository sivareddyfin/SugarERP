package com.finsol.bean;

public class DispatchSummaryBean 
{
	private Integer dispatchSeq;
	private String dispatchNo;
	private String ryotcode;
	private Integer faCode;
	private Integer foCode;
	private String driverName;
	private String drPhoneNo;
	private String dispatchBy;
	private String modifyFlag;
	private String ryotPhoneNumber;
	private Integer seedlingQty;
	private String indentNumbers;
	private String vehicle;
	private String agreementSign;
	private String soilWaterAnalyis;
	private String landSurvey;
	private String ryotName;
	private Integer id;
	private String season;
	private String dateOfDispatch;
	private String indents;
	private Integer lc;
	private String agreementnumber;
	private String deliveryDate;
	private String deliveryTime;
	private Integer agrType;
	
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getLc()
	{
		return lc;
	}
	public String getAgreementnumber()
	{
		return agreementnumber;
	}
	public void setLc(Integer lc)
	{
		this.lc = lc;
	}
	public void setAgreementnumber(String agreementnumber)
	{
		this.agreementnumber = agreementnumber;
	}
	public String getIndents() {
		return indents;
	}
	public void setIndents(String indents) {
		this.indents = indents;
	}
	public String getDateOfDispatch() {
		return dateOfDispatch;
	}
	public void setDateOfDispatch(String dateOfDispatch) {
		this.dateOfDispatch = dateOfDispatch;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDispatchSeq() {
		return dispatchSeq;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public Integer getFaCode() {
		return faCode;
	}
	
	public String getDriverName() {
		return driverName;
	}
	public String getDrPhoneNo() {
		return drPhoneNo;
	}
	public String getDispatchBy() {
		return dispatchBy;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public String getRyotPhoneNumber() {
		return ryotPhoneNumber;
	}
	public Integer getSeedlingQty() {
		return seedlingQty;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public String getVehicle() {
		return vehicle;
	}
	public String getAgreementSign() {
		return agreementSign;
	}
	public String getSoilWaterAnalyis() {
		return soilWaterAnalyis;
	}
	public String getLandSurvey() {
		return landSurvey;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setDispatchSeq(Integer dispatchSeq) {
		this.dispatchSeq = dispatchSeq;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	
	public Integer getFoCode() {
		return foCode;
	}
	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public void setDrPhoneNo(String drPhoneNo) {
		this.drPhoneNo = drPhoneNo;
	}
	public void setDispatchBy(String dispatchBy) {
		this.dispatchBy = dispatchBy;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public void setRyotPhoneNumber(String ryotPhoneNumber) {
		this.ryotPhoneNumber = ryotPhoneNumber;
	}
	public void setSeedlingQty(Integer seedlingQty) {
		this.seedlingQty = seedlingQty;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public void setAgreementSign(String agreementSign) {
		this.agreementSign = agreementSign;
	}
	public void setSoilWaterAnalyis(String soilWaterAnalyis) {
		this.soilWaterAnalyis = soilWaterAnalyis;
	}
	public void setLandSurvey(String landSurvey) {
		this.landSurvey = landSurvey;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getAgrType() {
		return agrType;
	}
	public void setAgrType(Integer agrType) {
		this.agrType = agrType;
	}



	

	
	

}
