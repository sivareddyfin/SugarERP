package com.finsol.bean;

public class HarvestingRatesSmryBean 
{
	private Integer id;
	private String season;
	private Integer circleCode;
	private String effectiveDate;
	private String toDate;
	private Byte harvestingMode;
	
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season)
	{
		this.season = season;
	}
	public Integer getCircleCode()
	{
		return circleCode;
	}
	public void setCircleCode(Integer circleCode)
	{
		this.circleCode = circleCode;
	}
	
	public String getEffectiveDate() 
	{
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) 
	{
		this.effectiveDate = effectiveDate;
	}
	public String getToDate()
	{
		return toDate;
	}
	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}
	public Byte getHarvestingMode() 
	{
		return harvestingMode;
	}
	public void setHarvestingMode(Byte harvestingMode)
	{
		this.harvestingMode = harvestingMode;
	}
}
