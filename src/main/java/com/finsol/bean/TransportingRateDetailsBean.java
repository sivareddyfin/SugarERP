package com.finsol.bean;

public class TransportingRateDetailsBean 
{
	private Integer id;
	private String ryotCode;
	private String ryotName;
	private Integer transportContractorCode;
	private Double slNo;
	private Double rate;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public Integer getTransportContractorCode() {
		return transportContractorCode;
	}
	public void setTransportContractorCode(Integer transportContractorCode) {
		this.transportContractorCode = transportContractorCode;
	}
	public Double getSlNo() {
		return slNo;
	}
	public void setSlNo(Double slNo) {
		this.slNo = slNo;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
}
