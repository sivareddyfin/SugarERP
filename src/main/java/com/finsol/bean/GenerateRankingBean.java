package com.finsol.bean;

import java.util.Date;

public class GenerateRankingBean
{
	private String season;
	private int programNumber;
	private String zone;
	private String circle;
	private String ryotCode;
	private String ryotName;
	private String agreementNumber;
	private String plantOrRatoon;
	private String plotNumber;
	private double plotSize;
	private double ccsRating;
	private int rank;
	private String pageName;
	private String permitNumber;
	private String permitDate;
	//private String modifyFlag;
	
	
	/*public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}*/
	public String getPermitDate() {
		return permitDate;
	}
	public void setPermitDate(String permitDate) {
		this.permitDate = permitDate;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}
	public double getPlotSize() {
		return plotSize;
	}
	public void setPlotSize(double plotSize) {
		this.plotSize = plotSize;
	}
	public String getPlantOrRatoon() {
		return plantOrRatoon;
	}
	public void setPlantOrRatoon(String plantOrRatoon) {
		this.plantOrRatoon = plantOrRatoon;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public int getProgramNumber() {
		return programNumber;
	}
	public void setProgramNumber(int programNumber) {
		this.programNumber = programNumber;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getPlotNumber() {
		return plotNumber;
	}
	public void setPlotNumber(String plotNumber) {
		this.plotNumber = plotNumber;
	}
	public double getCcsRating() {
		return ccsRating;
	}
	public void setCcsRating(double ccsRating) {
		this.ccsRating = ccsRating;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
}
