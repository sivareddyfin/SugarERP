package com.finsol.bean;

public class AccountSubGroupBean 
{
	private Integer id;
	private Integer accountGroupCode;
	private Integer accountSubGroupCode;
	private String accountSubGroup;
	private String description;
	private Byte status;
	private String modifyFlag;
	
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}


	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	public Integer getAccountGroupCode()
	{
		return accountGroupCode;
	}
	public void setAccountGroupCode(Integer accountGroupCode)
	{
		this.accountGroupCode = accountGroupCode;
	}
	public Integer getAccountSubGroupCode() 
	{
		return accountSubGroupCode;
	}
	public void setAccountSubGroupCode(Integer accountSubGroupCode)
	{
		this.accountSubGroupCode = accountSubGroupCode;
	}
	public String getAccountSubGroup()
	{
		return accountSubGroup;
	}
	public void setAccountSubGroup(String accountSubGroup)
	{
		this.accountSubGroup = accountSubGroup;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getStatus() 
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
}
