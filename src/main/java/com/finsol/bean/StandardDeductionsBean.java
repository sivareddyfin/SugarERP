package com.finsol.bean;

public class StandardDeductionsBean
{
	private Integer id;
	private Integer stdDedCode;
	private String name;
	private Double amount;
	private Byte amountValidity;
	private Integer deductionOrder;
	private Byte status;
	private String modifyFlag;
	//Added by sahadeva 31/10/2016
	private String screenName;
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStdDedCode() {
		return stdDedCode;
	}
	public void setStdDedCode(Integer stdDedCode) {
		this.stdDedCode = stdDedCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Byte getAmountValidity() {
		return amountValidity;
	}
	public void setAmountValidity(Byte amountValidity) {
		this.amountValidity = amountValidity;
	}
	public Integer getDeductionOrder() {
		return deductionOrder;
	}
	public void setDeductionOrder(Integer deductionOrder) {
		this.deductionOrder = deductionOrder;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
}
