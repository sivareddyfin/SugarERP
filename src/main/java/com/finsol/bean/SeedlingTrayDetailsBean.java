package com.finsol.bean;

public class SeedlingTrayDetailsBean {
	
	private Integer id;
	private Integer varietyCode;
	private Integer suppliedSeeds;
	private Integer trayType;
	private Integer noOfTrays;
	private Integer ryotDamagedTray;
	private Integer damagedTrayTravelling;
	
	
	public Integer getRyotDamagedTray() {
		return ryotDamagedTray;
	}
	public Integer getDamagedTrayTravelling() {
		return damagedTrayTravelling;
	}
	public void setRyotDamagedTray(Integer ryotDamagedTray) {
		this.ryotDamagedTray = ryotDamagedTray;
	}
	public void setDamagedTrayTravelling(Integer damagedTrayTravelling) {
		this.damagedTrayTravelling = damagedTrayTravelling;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public Integer getSuppliedSeeds() {
		return suppliedSeeds;
	}
	public void setSuppliedSeeds(Integer suppliedSeeds) {
		this.suppliedSeeds = suppliedSeeds;
	}
	public Integer getTrayType() {
		return trayType;
	}
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	public Integer getNoOfTrays() {
		return noOfTrays;
	}
	public void setNoOfTrays(Integer noOfTrays) {
		this.noOfTrays = noOfTrays;
	}

	
}
