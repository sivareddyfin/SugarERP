package com.finsol.bean;

public class SourceOfSeedBean 
{
	private Integer id;
	private String date;
	private String season;
	private Integer seedType;
	private Integer circleCode;
	private Integer purpose;
	private String consumerCode;
	private String seedSrCode;
	private String otherRyot;
	private Integer varietyCode;
	private String villageCode;
	private String batchNo;
	private double caneWeight;
	private double lorryWeight;
	private double netWeight;
	private double bm;
	private double finalWeight;
	private double seedCostPerTon;
	private double totalCost;
	private double ageOfCrop;
	private double avgBudsPerCane;
	private String physicalCondition;
	private String seedCertifiedBy;
	private String certDate;
	private String fileLocation;
	private double harvestingCharges;
	private double transCharges;
	private String addedYear;
	private String addedBatch;
	private String modifyFlag;
	private String formData;
	private double noofacre;
	private double costperacre;
	private Byte acreage;
	private Byte updateType;
	private String dates;
	private String modifyFlag1;
	private String otherRyotVillage;
	private String vehicleNo;
	//added on 24-2-2017
	private String aggrementNo;
	private Integer transactionCode;
	private String prvConsumer;
	private Integer suptransactionCode;
	
	public String getAggrementNo() {
		return aggrementNo;
	}
	public void setAggrementNo(String aggrementNo) {
		this.aggrementNo = aggrementNo;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getOtherRyotVillage() {
		return otherRyotVillage;
	}
	public void setOtherRyotVillage(String otherRyotVillage) {
		this.otherRyotVillage = otherRyotVillage;
	}
	public String getModifyFlag1() {
		return modifyFlag1;
	}
	public void setModifyFlag1(String modifyFlag1) {
		this.modifyFlag1 = modifyFlag1;
	}
	
	
	public String getDates()
	{
		return dates;
	}
	public void setDates(String dates)
	{
		this.dates = dates;
	}
	public Byte getUpdateType() {
		return updateType;
	}
	public void setUpdateType(Byte updateType) {
		this.updateType = updateType;
	}
	public double getNoofacre() {
		return noofacre;
	}
	public double getCostperacre() {
		return costperacre;
	}

	
	public Byte getAcreage() {
		return acreage;
	}
	public void setAcreage(Byte acreage) {
		this.acreage = acreage;
	}
	public void setNoofacre(double noofacre) {
		this.noofacre = noofacre;
	}
	public void setCostperacre(double costperacre) {
		this.costperacre = costperacre;
	}
	
	public String getAddedYear() 
	{
		return addedYear;
	}
	public void setAddedYear(String addedYear)
	{
		this.addedYear = addedYear;
	}
	public String getAddedBatch() 
	{
		return addedBatch;
	}
	public void setAddedBatch(String addedBatch)
	{
		this.addedBatch = addedBatch;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	private String ryotName;

	public String getFormData() {
		return formData;
	}
	public void setFormData(String formData) {
		this.formData = formData;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getSeedType() {
		return seedType;
	}
	public void setSeedType(Integer seedType) {
		this.seedType = seedType;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public Integer getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	public Integer getPurpose() {
		return purpose;
	}
	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}
	public String getConsumerCode() {
		return consumerCode;
	}
	public void setConsumerCode(String consumerCode) {
		this.consumerCode = consumerCode;
	}
	public String getSeedSrCode() {
		return seedSrCode;
	}
	public void setSeedSrCode(String seedSrCode) {
		this.seedSrCode = seedSrCode;
	}
	public String getOtherRyot() {
		return otherRyot;
	}
	public void setOtherRyot(String otherRyot) {
		this.otherRyot = otherRyot;
	}
	
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public String getVillageCode() {
		return villageCode;
	}
	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public double getCaneWeight() {
		return caneWeight;
	}
	public void setCaneWeight(double caneWeight) {
		this.caneWeight = caneWeight;
	}
	public double getLorryWeight() {
		return lorryWeight;
	}
	public void setLorryWeight(double lorryWeight) {
		this.lorryWeight = lorryWeight;
	}
	public double getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(double netWeight) {
		this.netWeight = netWeight;
	}
	public double getBm() {
		return bm;
	}
	public void setBm(double bm) {
		this.bm = bm;
	}
	public double getFinalWeight() {
		return finalWeight;
	}
	public void setFinalWeight(double finalWeight) {
		this.finalWeight = finalWeight;
	}
	public double getSeedCostPerTon() {
		return seedCostPerTon;
	}
	public void setSeedCostPerTon(double seedCostPerTon) {
		this.seedCostPerTon = seedCostPerTon;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public double getAgeOfCrop() {
		return ageOfCrop;
	}
	public void setAgeOfCrop(double ageOfCrop) {
		this.ageOfCrop = ageOfCrop;
	}
	public double getAvgBudsPerCane() {
		return avgBudsPerCane;
	}
	public void setAvgBudsPerCane(double avgBudsPerCane) {
		this.avgBudsPerCane = avgBudsPerCane;
	}
	public String getPhysicalCondition() {
		return physicalCondition;
	}
	public void setPhysicalCondition(String physicalCondition) {
		this.physicalCondition = physicalCondition;
	}
	public String getSeedCertifiedBy() {
		return seedCertifiedBy;
	}
	public void setSeedCertifiedBy(String seedCertifiedBy) {
		this.seedCertifiedBy = seedCertifiedBy;
	}
	public String getCertDate() {
		return certDate;
	}
	public void setCertDate(String certDate) {
		this.certDate = certDate;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public double getHarvestingCharges() {
		return harvestingCharges;
	}
	public void setHarvestingCharges(double harvestingCharges) {
		this.harvestingCharges = harvestingCharges;
	}
	public double getTransCharges() {
		return transCharges;
	}
	public void setTransCharges(double transCharges) {
		this.transCharges = transCharges;
	}
	public Integer getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(Integer transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getPrvConsumer() {
		return prvConsumer;
	}
	public void setPrvConsumer(String prvConsumer) {
		this.prvConsumer = prvConsumer;
	}
	public Integer getSuptransactionCode() {
		return suptransactionCode;
	}
	public void setSuptransactionCode(Integer suptransactionCode) {
		this.suptransactionCode = suptransactionCode;
	}
	
}
