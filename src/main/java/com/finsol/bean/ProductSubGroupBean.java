package com.finsol.bean;

public class ProductSubGroupBean 
{
	private String groupCode;
	private String group;
	private String subGroupCode;
	private String subGroup;
	private String description;
	private String modifyFlag;
	private Byte status;
	
	
	public String getGroup() {
		return group;
	}
	public String getSubGroupCode() {
		return subGroupCode;
	}
	public String getSubGroup() {
		return subGroup;
	}
	public String getDescription() {
		return description;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public Byte getStatus() {
		return status;
	}
	
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public void setSubGroupCode(String subGroupCode) {
		this.subGroupCode = subGroupCode;
	}
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
}
