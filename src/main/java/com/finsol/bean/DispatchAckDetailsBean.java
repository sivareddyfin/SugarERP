package com.finsol.bean;

public class DispatchAckDetailsBean 
{
	private Integer id;
	private String season;
	private Integer variety;
	private String indentNo;
	private String dispatchNo;
	private String batchNo;
	private Double noOfSeedlings;
	private Double costOfEachItem;
	private Double totalCost;
	private Double noOfTrays;
	private Integer trayType;
	private Double trayCost;
	private Double trayTotalCost;
	private String kind;
	private String indentNumbers;
	
	public Integer getId()
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getVariety() {
		return variety;
	}
	public String getIndentNo() {
		return indentNo;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public Double getNoOfSeedlings() {
		return noOfSeedlings;
	}
	public Double getCostOfEachItem() {
		return costOfEachItem;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public Double getNoOfTrays() {
		return noOfTrays;
	}
	public Integer getTrayType() {
		return trayType;
	}
	public Double getTrayCost() {
		return trayCost;
	}
	public Double getTrayTotalCost() {
		return trayTotalCost;
	}
	public String getKind() {
		return kind;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public void setNoOfSeedlings(Double noOfSeedlings) {
		this.noOfSeedlings = noOfSeedlings;
	}
	public void setCostOfEachItem(Double costOfEachItem) {
		this.costOfEachItem = costOfEachItem;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public void setNoOfTrays(Double noOfTrays) {
		this.noOfTrays = noOfTrays;
	}
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	public void setTrayCost(Double trayCost) {
		this.trayCost = trayCost;
	}
	public void setTrayTotalCost(Double trayTotalCost) {
		this.trayTotalCost = trayTotalCost;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	
	
	
}
