package com.finsol.bean;

public class GrnDetailsBean 
{
	private Integer id;
	private String finYr;
	private String grnNo;
	private String productCode;
	private String productGrpCode;
	private String productSubgroupCode;
	private String uom;
	private String cause;
	private Double issuedQty;
	private Double acceptedQty;
	private Double rejectedQty;
	private Double mrp;
	private String batchId;
	private String productName;
	private String uomName;
	private String mfgDate;
	private String consumption;
	private Integer batchSeqNo;
	private String batch;

	
	

	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getConsumption() {
		return consumption;
	}
	public Integer getBatchSeqNo() {
		return batchSeqNo;
	}
	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}
	public void setBatchSeqNo(Integer batchSeqNo) {
		this.batchSeqNo = batchSeqNo;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public String getProductName() {
		return productName;
	}
	public String getUomName() {
		return uomName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getGrnNo() {
		return grnNo;
	}
	public void setGrnNo(String grnNo) {
		this.grnNo = grnNo;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductGrpCode() {
		return productGrpCode;
	}
	public void setProductGrpCode(String productGrpCode) {
		this.productGrpCode = productGrpCode;
	}
	public String getProductSubgroupCode() {
		return productSubgroupCode;
	}
	public void setProductSubgroupCode(String productSubgroupCode) {
		this.productSubgroupCode = productSubgroupCode;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public Double getIssuedQty() {
		return issuedQty;
	}
	public void setIssuedQty(Double issuedQty) {
		this.issuedQty = issuedQty;
	}
	public Double getAcceptedQty() {
		return acceptedQty;
	}
	public void setAcceptedQty(Double acceptedQty) {
		this.acceptedQty = acceptedQty;
	}
	public Double getRejectedQty() {
		return rejectedQty;
	}
	public void setRejectedQty(Double rejectedQty) {
		this.rejectedQty = rejectedQty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	
	
}
