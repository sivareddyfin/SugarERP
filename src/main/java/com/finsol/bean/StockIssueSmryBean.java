package com.finsol.bean;

public class StockIssueSmryBean
{
	private Integer id;
	private String modifFlag;
	private String finYr;
	private String issueDate;
	private String issueTime;
	private Integer seqNo;
	private String stockIssueNo;
	private String stockReqNo;
	private Integer deptId;
	private String loginId;
	private Integer status;
	private String grnNo;
	private int stockTransactionNo;
	private int centralStoreId;
	
	public int getCentralStoreId() {
		return centralStoreId;
	}
	public void setCentralStoreId(int centralStoreId) {
		this.centralStoreId = centralStoreId;
	}
	public int getStockTransactionNo() {
		return stockTransactionNo;
	}
	public void setStockTransactionNo(int stockTransactionNo) {
		this.stockTransactionNo = stockTransactionNo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getModifFlag() {
		return modifFlag;
	}
	public void setModifFlag(String modifFlag) {
		this.modifFlag = modifFlag;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getIssueTime() {
		return issueTime;
	}
	public void setIssueTime(String issueTime) {
		this.issueTime = issueTime;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getStockIssueNo() {
		return stockIssueNo;
	}
	public void setStockIssueNo(String stockIssueNo) {
		this.stockIssueNo = stockIssueNo;
	}
	public String getStockReqNo() {
		return stockReqNo;
	}
	public void setStockReqNo(String stockReqNo) {
		this.stockReqNo = stockReqNo;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getGrnNo() {
		return grnNo;
	}
	public void setGrnNo(String grnNo) {
		this.grnNo = grnNo;
	}
	
	
	    
}
