package com.finsol.bean;

public class CaneAccountingRulesSeasonBean 
{
	private Integer calcid;
	private String season;
	private String caneaccruleDate;
	private Double netCanePrice;
	private Double seasonCanePrice;
	private Double postSeasonCanePrice;
	private Byte isPercentOrAmt;
	private Double amtForDed;
	private Double amtForLoans;
	private Double percentForLoans;
	private Double percentForDed;
	private Double loanRecovery;
	private Byte isTAApplicable;
	private Double noOfKms;
	private Double allowancePerKmPerTon;
	private String modifyFlag;
	

	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getCalcid() {
		return calcid;
	}
	public void setCalcid(Integer calcid) {
		this.calcid = calcid;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getCaneaccruleDate() {
		return caneaccruleDate;
	}
	public void setCaneaccruleDate(String caneaccruleDate) {
		this.caneaccruleDate = caneaccruleDate;
	}
	public Double getNetCanePrice() {
		return netCanePrice;
	}
	public void setNetCanePrice(Double netCanePrice) {
		this.netCanePrice = netCanePrice;
	}
	public Double getSeasonCanePrice() {
		return seasonCanePrice;
	}
	public void setSeasonCanePrice(Double seasonCanePrice) {
		this.seasonCanePrice = seasonCanePrice;
	}
	public Double getPostSeasonCanePrice() {
		return postSeasonCanePrice;
	}
	public void setPostSeasonCanePrice(Double postSeasonCanePrice) {
		this.postSeasonCanePrice = postSeasonCanePrice;
	}
	public Byte getIsPercentOrAmt() {
		return isPercentOrAmt;
	}
	public void setIsPercentOrAmt(Byte isPercentOrAmt) {
		this.isPercentOrAmt = isPercentOrAmt;
	}
	public Double getAmtForDed() {
		return amtForDed;
	}
	public void setAmtForDed(Double amtForDed) {
		this.amtForDed = amtForDed;
	}
	public Double getAmtForLoans() {
		return amtForLoans;
	}
	public void setAmtForLoans(Double amtForLoans) {
		this.amtForLoans = amtForLoans;
	}
	public Double getPercentForLoans() {
		return percentForLoans;
	}
	public void setPercentForLoans(Double percentForLoans) {
		this.percentForLoans = percentForLoans;
	}
	public Double getPercentForDed() {
		return percentForDed;
	}
	public void setPercentForDed(Double percentForDed) {
		this.percentForDed = percentForDed;
	}
	public Double getLoanRecovery() {
		return loanRecovery;
	}
	public void setLoanRecovery(Double loanRecovery) {
		this.loanRecovery = loanRecovery;
	}
	public Byte getIsTAApplicable() {
		return isTAApplicable;
	}
	public void setIsTAApplicable(Byte isTAApplicable) {
		this.isTAApplicable = isTAApplicable;
	}
	public Double getNoOfKms() {
		return noOfKms;
	}
	public void setNoOfKms(Double noOfKms) {
		this.noOfKms = noOfKms;
	}
	public Double getAllowancePerKmPerTon() {
		return allowancePerKmPerTon;
	}
	public void setAllowancePerKmPerTon(Double allowancePerKmPerTon) {
		this.allowancePerKmPerTon = allowancePerKmPerTon;
	}
	
	
	
	
	
}
