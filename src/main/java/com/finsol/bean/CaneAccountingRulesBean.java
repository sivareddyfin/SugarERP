package com.finsol.bean;

public class CaneAccountingRulesBean
{
	private Integer id;
	private String season;
	private String date;
	private Double frp;
	private Double frpSubsidyAmt;
	private Double additionalCanePrice;
	private Double icp;
	private Double netCanePrice;
	private Double seasonCanePrice;
	private Double postSeasonCanePrice;
	private Byte isPercentOrAmt;
	private Double amtForDed;
	private Double amtForLoans;
	private Double percentForLoans;
	private Double percentForDed;
	private Double loanRecovery;
	private Byte isTAApplicable;
	private Double noOfKms;
	private Double allowancePerKmPerTon;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getFrp() {
		return frp;
	}
	public void setFrp(Double frp) {
		this.frp = frp;
	}
	public Double getFrpSubsidyAmt() {
		return frpSubsidyAmt;
	}
	public void setFrpSubsidyAmt(Double frpSubsidyAmt) {
		this.frpSubsidyAmt = frpSubsidyAmt;
	}
	public Double getAdditionalCanePrice() {
		return additionalCanePrice;
	}
	public void setAdditionalCanePrice(Double additionalCanePrice) {
		this.additionalCanePrice = additionalCanePrice;
	}
	public Double getIcp() {
		return icp;
	}
	public void setIcp(Double icp) {
		this.icp = icp;
	}
	public Double getNetCanePrice() {
		return netCanePrice;
	}
	public void setNetCanePrice(Double netCanePrice) {
		this.netCanePrice = netCanePrice;
	}
	public Double getSeasonCanePrice() {
		return seasonCanePrice;
	}
	public void setSeasonCanePrice(Double seasonCanePrice) {
		this.seasonCanePrice = seasonCanePrice;
	}
	public Double getPostSeasonCanePrice() {
		return postSeasonCanePrice;
	}
	public void setPostSeasonCanePrice(Double postSeasonCanePrice) {
		this.postSeasonCanePrice = postSeasonCanePrice;
	}
	public Byte getIsPercentOrAmt() {
		return isPercentOrAmt;
	}
	public void setIsPercentOrAmt(Byte isPercentOrAmt) {
		this.isPercentOrAmt = isPercentOrAmt;
	}
	public Double getAmtForDed() {
		return amtForDed;
	}
	public void setAmtForDed(Double amtForDed) {
		this.amtForDed = amtForDed;
	}
	public Double getAmtForLoans() {
		return amtForLoans;
	}
	public void setAmtForLoans(Double amtForLoans) {
		this.amtForLoans = amtForLoans;
	}
	public Double getPercentForLoans() {
		return percentForLoans;
	}
	public void setPercentForLoans(Double percentForLoans) {
		this.percentForLoans = percentForLoans;
	}
	public Double getPercentForDed() {
		return percentForDed;
	}
	public void setPercentForDed(Double percentForDed) {
		this.percentForDed = percentForDed;
	}
	public Double getLoanRecovery() {
		return loanRecovery;
	}
	public void setLoanRecovery(Double loanRecovery) {
		this.loanRecovery = loanRecovery;
	}
	public Byte getIsTAApplicable() {
		return isTAApplicable;
	}
	public void setIsTAApplicable(Byte isTAApplicable) {
		this.isTAApplicable = isTAApplicable;
	}
	public Double getNoOfKms() {
		return noOfKms;
	}
	public void setNoOfKms(Double noOfKms) {
		this.noOfKms = noOfKms;
	}
	public Double getAllowancePerKmPerTon() {
		return allowancePerKmPerTon;
	}
	public void setAllowancePerKmPerTon(Double allowancePerKmPerTon) {
		this.allowancePerKmPerTon = allowancePerKmPerTon;
	}
}

