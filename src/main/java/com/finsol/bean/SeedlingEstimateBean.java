package com.finsol.bean;

public class SeedlingEstimateBean 
{
	private String season;
	private Integer employeeId;
	private Integer varietyCode;
	private String quantity;
	private Integer yearOfPlanting;
	private String monthId;
	private Integer periodId;
	private String modifyFlag;
	private int  id;
	
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getVarietyCode() {
		return varietyCode;
	}
	public void setVarietyCode(Integer varietyCode) {
		this.varietyCode = varietyCode;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Integer getYearOfPlanting() {
		return yearOfPlanting;
	}
	public void setYearOfPlanting(Integer yearOfPlanting) {
		this.yearOfPlanting = yearOfPlanting;
	}
	public String getMonthId() {
		return monthId;
	}
	public void setMonthId(String monthId) {
		this.monthId = monthId;
	}
	public Integer getPeriodId() {
		return periodId;
	}
	public void setPeriodId(Integer periodId) {
		this.periodId = periodId;
	}
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
