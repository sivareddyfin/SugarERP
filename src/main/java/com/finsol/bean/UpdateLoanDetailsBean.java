package com.finsol.bean;

public class UpdateLoanDetailsBean 
{
	private String ryotName;
	private String ryotCode;
	private Integer loanNumber;
	private Double paidAmount;
	private String recommendedDate;
	private String referenceNumber;
	private Double disbursedAmount;  
	private String disbursedDate;
	private Integer transactioncode;
	private String modifyFlag;

	
	public String getModifyFlag() {
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) {
		this.modifyFlag = modifyFlag;
	}
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public String getRyotName() 
	{
		return ryotName;
	}
	public void setRyotName(String ryotName)
	{
		this.ryotName = ryotName;
	}
	public String getRyotCode() 
	{
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) 
	{
		this.ryotCode = ryotCode;
	}
	public Integer getLoanNumber()
	{
		return loanNumber;
	}
	public void setLoanNumber(Integer loanNumber) 
	{
		this.loanNumber = loanNumber;
	}
	public Double getPaidAmount()
	{
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount)
	{
		this.paidAmount = paidAmount;
	}
	public String getRecommendedDate()
	{
		return recommendedDate;
	}
	public void setRecommendedDate(String recommendedDate)
	{
		this.recommendedDate = recommendedDate;
	}
	public String getReferenceNumber()
	{
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) 
	{
		this.referenceNumber = referenceNumber;
	}
	public Double getDisbursedAmount()
	{
		return disbursedAmount;
	}
	public void setDisbursedAmount(Double disbursedAmount)
	{
		this.disbursedAmount = disbursedAmount;
	}
	public String getDisbursedDate() 
	{
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate)
	{
		this.disbursedDate = disbursedDate;
	} 
	
	

}
