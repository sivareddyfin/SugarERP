package com.finsol.bean;

public class MandalBean 
{
	private Integer id;
	private Integer mandalCode;
	private String mandal;
	private String description;
	private Integer zoneCode;
	private Byte status;
	private String modifyFlag;
	
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getMandalCode()
	{
		return mandalCode;
	}
	public void setMandalCode(Integer mandalCode)
	{
		this.mandalCode = mandalCode;
	}

	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description) 
	{
		this.description = description;
	}
	public Integer getZoneCode() 
	{
		return zoneCode;
	}
	public void setZoneCode(Integer zoneCode) 
	{
		this.zoneCode = zoneCode;
	}
	public Byte getStatus()
	{
		return status;
	}
	public void setStatus(Byte status) 
	{
		this.status = status;
	}
	public String getMandal() {
		return mandal;
	}
	public void setMandal(String mandal) {
		this.mandal = mandal;
	}
	
	
}
