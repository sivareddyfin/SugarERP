package com.finsol.bean;

public class StockIssueDtlsBean 
{
	private Integer id;
	private String finYr;
	private String stockIssueNo;
	private String productCode;
	private String productGrpCode;
	private String productSubgroupCode;
	private String uom;
	private Double quantity;
	private Double issuedQty;
	private Double pendingQty;
	private String batchId;
	private Double mrp;
	private Integer status;
	private String uomName;
	private String productName;
	private Double batchQty;
	private Integer batchSeqNo;
	private String batch;

	
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Integer getBatchSeqNo() {
		return batchSeqNo;
	}
	public void setBatchSeqNo(Integer batchSeqNo) {
		this.batchSeqNo = batchSeqNo;
	}
	public Double getBatchQty() {
		return batchQty;
	}
	public void setBatchQty(Double batchQty) {
		this.batchQty = batchQty;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFinYr() {
		return finYr;
	}
	public void setFinYr(String finYr) {
		this.finYr = finYr;
	}
	public String getStockIssueNo() {
		return stockIssueNo;
	}
	public void setStockIssueNo(String stockIssueNo) {
		this.stockIssueNo = stockIssueNo;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductGrpCode() {
		return productGrpCode;
	}
	public void setProductGrpCode(String productGrpCode) {
		this.productGrpCode = productGrpCode;
	}
	public String getProductSubgroupCode() {
		return productSubgroupCode;
	}
	public void setProductSubgroupCode(String productSubgroupCode) {
		this.productSubgroupCode = productSubgroupCode;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getIssuedQty() {
		return issuedQty;
	}
	public void setIssuedQty(Double issuedQty) {
		this.issuedQty = issuedQty;
	}
	public Double getPendingQty() {
		return pendingQty;
	}
	public void setPendingQty(Double pendingQty) {
		this.pendingQty = pendingQty;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	                    
	
	
	
	
	         

}
