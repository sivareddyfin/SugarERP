package com.finsol.bean;

public class RegionBean 
{
	private Integer id;
	private Integer regionCode;
	private String region;
	private String description;
	private Byte status;
	private String modifyFlag;
	private String screenName;
	
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getModifyFlag() 
	{
		return modifyFlag;
	}
	public void setModifyFlag(String modifyFlag) 
	{
		this.modifyFlag = modifyFlag;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getRegionCode()
	{
		return regionCode;
	}
	public void setRegionCode(Integer regionCode)
	{
		this.regionCode = regionCode;
	}
	
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getStatus()
	{
		return status;
	}
	public void setStatus(Byte status)
	{
		this.status = status;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}

	
}
