package com.finsol.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;


import com.finsol.controller.LoginController;
import com.finsol.utils.SessionUtils;
/**
 * @author Rama Krishna
 *
 */
public class LoginInterceptor extends HandlerInterceptorAdapter
{

	private static final Logger logger = Logger.getLogger(LoginInterceptor.class);
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
    	
    	logger.info("==============Inside preHandle() Intercepter=====");
        try
        {     	
             String action = request.getParameter("actionName");
            String uName = request.getParameter("name");
            logger.info("==============Inside preHandle() user Name====="+uName);

            

            if ("signout".equals(action))
            {

                HttpSession session = request.getSession(false);
                session.invalidate();
                response.sendRedirect("index.html");
                return false;
            }
            //response.sendRedirect("loginController.htm?code=1")
           // else
           // {
            	/*
               // UserData userData = (UserData) WebUtils.getSessionAttribute(request, Constants.USERSESSION);
                //if (userData == null && !"login".equals(action))
                {
                     response.sendRedirect("loginController.htm?code=1");
                    return false;
                } else
                {
                    if ("login".equals(action))
                    {
                        String userName = request.getParameter("userName");
                        String password = request.getParameter("password");
                      //  User user = userService.getUserByNameAndPassword(userName, digester.digest(password));
                        if (user == null)
                        {
                            response.sendRedirect("loginController.htm?code=1");
                            return false;
                        } else
                        {
                            String userType = user.getUserType().getName();
                            WebUtils.setSessionAttribute(request, "userType", userType);
                           // SessionUtils.setSessionData(user, language, new UserData(), request);
                            return true;
                        }
                    } else
                    {
                        //SessionUtils.setSessionData(userData.getUser(), userData.getLanguage(), userData, request);
                     }*/
                   // return true;
               // }
            
        
        } catch (Exception e)
        {
           // s_log.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView)
    {
    }

   

    
    

}
