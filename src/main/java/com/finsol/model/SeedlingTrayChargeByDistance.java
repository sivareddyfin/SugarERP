package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SeedlingTrayChargeByDistance {

	private Integer id;	
	@Column
	private Integer traycode;
	@Column
	private Double distancefrom;
	@Column
	private Double distanceto;
	@Column
	private Double traycharge;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTraycode() {
		return traycode;
	}

	public void setTraycode(Integer traycode) {
		this.traycode = traycode;
	}

	public Double getDistancefrom() {
		return distancefrom;
	}

	public void setDistancefrom(Double distancefrom) {
		this.distancefrom = distancefrom;
	}

	public Double getDistanceto() {
		return distanceto;
	}

	public void setDistanceto(Double distanceto) {
		this.distanceto = distanceto;
	}

	public Double getTraycharge() {
		return traycharge;
	}

	public void setTraycharge(Double traycharge) {
		this.traycharge = traycharge;
	}

	public Integer getSlno() {
		return slno;
	}

	public void setSlno(Integer slno) {
		this.slno = slno;
	}	
	
	
	
	
	
}
