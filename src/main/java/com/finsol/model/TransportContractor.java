package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 */
@Entity
public class TransportContractor
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
    	private Integer tcCode;

	@Column(nullable = false)
    	private String tcName;
	
	@Column(nullable = true)
    	private String description;
	
	@Column(nullable = false)
    	private Byte status;

	public Integer getId() {
		return id;
	}

	public Integer getTcCode() {
		return tcCode;
	}

	public String getTcName() {
		return tcName;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTcCode(Integer tcCode) {
		this.tcCode = tcCode;
	}

	public void setTcName(String tcName) {
		this.tcName = tcName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	

	

}
