package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class FieldAssistant {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
    private Integer fieldassistantid;
	
	@Column
    private String fieldassistant;
	
	@Column
    private Integer fieldofficerid;
	
	@Column
    private Integer caneManagerid;
	
	@Column
    private Integer employeeid;	
    
    @Column
    private String description;
    
    @Column
    private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFieldassistantid() {
		return fieldassistantid;
	}

	public void setFieldassistantid(Integer fieldassistantid) {
		this.fieldassistantid = fieldassistantid;
	}

	public String getFieldassistant() {
		return fieldassistant;
	}

	public void setFieldassistant(String fieldassistant) {
		this.fieldassistant = fieldassistant;
	}

	public Integer getFieldofficerid() {
		return fieldofficerid;
	}

	public void setFieldofficerid(Integer fieldofficerid) {
		this.fieldofficerid = fieldofficerid;
	}

	public Integer getCaneManagerid() {
		return caneManagerid;
	}

	public void setCaneManagerid(Integer caneManagerid) {
		this.caneManagerid = caneManagerid;
	}

	public Integer getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(Integer employeeid) {
		this.employeeid = employeeid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
    
	@Override
    public String toString()
    {
		//"ID: " + getUserid() + "/n" +"Userame: " + getUsername() + "/n" +"Password: " + getPassword();
        return new StringBuffer().append('[').append(getClass().getName()).append(' ').append(']').toString();
    }
    

}
