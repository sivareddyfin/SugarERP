package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class ExtentDetails {	

	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer slno;	
	 
	 @Column
	 private Integer id;
	 
	 @Column
	 private String ryotcode;
	 
	 @Column
	 private String villagecode;
	 @Column
	 private Integer plotslnumber;
	 @Column
	 private Integer varietycode;
	 @Column
	 private Double extentsize;	 
	 @Column
	 private String surveyno;	 
	 
	 @Column
	 private Byte IsSuitableForCultivation;	 
	 
	 @Column
	 @Type(type="date")
	 private Date TestDate;
	 
	 @Column
	 private Byte soilteststatus;	
	 
	 

	public Byte getSoilteststatus() 
	{
		return soilteststatus;
	}

	public void setSoilteststatus(Byte soilteststatus)
	{
		this.soilteststatus = soilteststatus;
	}

	public Integer getSlno() {
		return slno;
	}

	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public Integer getPlotslnumber() {
		return plotslnumber;
	}

	public void setPlotslnumber(Integer plotslnumber) {
		this.plotslnumber = plotslnumber;
	}

	public Integer getVarietycode() {
		return varietycode;
	}

	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}

	public Double getExtentsize() {
		return extentsize;
	}

	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}

	public String getSurveyno() {
		return surveyno;
	}

	public void setSurveyno(String surveyno) {
		this.surveyno = surveyno;
	}

	public Byte getIsSuitableForCultivation() {
		return IsSuitableForCultivation;
	}

	public void setIsSuitableForCultivation(Byte isSuitableForCultivation) {
		IsSuitableForCultivation = isSuitableForCultivation;
	}

	public Date getTestDate() {
		return TestDate;
	}

	public void setTestDate(Date testDate) {
		TestDate = testDate;
	}
	 
	 
	 

}
