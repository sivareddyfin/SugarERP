package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class HarvestingRateDetails
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
	
	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
	@Type(type="date")
    private Date effectivedate;
	
	@Column(nullable = false)
    private String ryotCode;
	
	@Column(nullable = false)
    private Integer harvestcontcode;
	
	@Column(nullable = false)
    private double sno;
	
	@Column(nullable = false)
    private double rate;
	
	@Column(nullable = false)
    private Integer circlecode;
	

	public Integer getCirclecode() {
		return circlecode;
	}

	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Date getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(Date effectivedate) {
		this.effectivedate = effectivedate;
	}

	public String getRyotCode() {
		return ryotCode;
	}

	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}

	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}

	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}

	public double getSno() {
		return sno;
	}

	public void setSno(double sno) {
		this.sno = sno;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
}
