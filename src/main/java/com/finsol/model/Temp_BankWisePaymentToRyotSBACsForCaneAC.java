package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity
public class Temp_BankWisePaymentToRyotSBACsForCaneAC 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String branchname;
	@Column 
	private Integer branchcode;
	@Column
	private Long glcode;
	@Column
	private Double amount;
	@Column 
	private String betweendate;
	
	
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public Long getGlcode() {
		return glcode;
	}
	public void setGlcode(Long glcode) {
		this.glcode = glcode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	

}
