package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class SeedlingEstimate 
{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
    private Integer employeeid;
	
	@Column(nullable = false)
    private Integer varietycode;
	
	@Column(nullable = false)
    private String quantity;
	
	@Column(nullable = false)
    private Integer yearofplanting;
	
	@Column(nullable = false)
    private String monthid;
	
	@Column(nullable = false)
    private Integer periodid;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Integer getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(Integer employeeid) {
		this.employeeid = employeeid;
	}

	public Integer getVarietycode() {
		return varietycode;
	}

	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Integer getYearofplanting() {
		return yearofplanting;
	}

	public void setYearofplanting(Integer yearofplanting) {
		this.yearofplanting = yearofplanting;
	}

	public String getMonthid() {
		return monthid;
	}

	public void setMonthid(String monthid) {
		this.monthid = monthid;
	}

	public Integer getPeriodid() {
		return periodid;
	}

	public void setPeriodid(Integer periodid) {
		this.periodid = periodid;
	}

	
	
}
