package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneValueSplitupDetailsByDate {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	//@Column
	//private String ryotcode;
	@Column
	private Integer caneacctslno;
	@Column
	private Double suppliedcanewt;
	@Column
	private Double netprice;
	@Column
	private Double totalvalue;
	@Column
	private Double foradvandloans;
	@Column
	private Double lotaladvpayable;
	@Column
	private Double totalloanpayable;
	@Column
	private Double forsbacs;
	@Column
	private Double cdc;
	@Column
	private Double uc;
	@Column
	private Double otherded;
	@Column
	private Double harvestchargespayable;
	@Column
	private Double transportchargespayable;
	@Column
	private Double dedparttosbacpayable;
	@Column
	private Double totalsbacpayable;	
	@Column
	@Type(type="date")
	private Date caneaccountingdate;
	@Column
	private String login;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	/*public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}*/
	public Integer getCaneacctslno() {
		return caneacctslno;
	}
	public void setCaneacctslno(Integer caneacctslno) {
		this.caneacctslno = caneacctslno;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getNetprice() {
		return netprice;
	}
	public void setNetprice(Double netprice) {
		this.netprice = netprice;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getForadvandloans() {
		return foradvandloans;
	}
	public void setForadvandloans(Double foradvandloans) {
		this.foradvandloans = foradvandloans;
	}
	public Double getLotaladvpayable() {
		return lotaladvpayable;
	}
	public void setLotaladvpayable(Double lotaladvpayable) {
		this.lotaladvpayable = lotaladvpayable;
	}
	public Double getTotalloanpayable() {
		return totalloanpayable;
	}
	public void setTotalloanpayable(Double totalloanpayable) {
		this.totalloanpayable = totalloanpayable;
	}
	public Double getForsbacs() {
		return forsbacs;
	}
	public void setForsbacs(Double forsbacs) {
		this.forsbacs = forsbacs;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getUc() {
		return uc;
	}
	public void setUc(Double uc) {
		this.uc = uc;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getHarvestchargespayable() {
		return harvestchargespayable;
	}
	public void setHarvestchargespayable(Double harvestchargespayable) {
		this.harvestchargespayable = harvestchargespayable;
	}
	public Double getTransportchargespayable() {
		return transportchargespayable;
	}
	public void setTransportchargespayable(Double transportchargespayable) {
		this.transportchargespayable = transportchargespayable;
	}
	public Double getDedparttosbacpayable() {
		return dedparttosbacpayable;
	}
	public void setDedparttosbacpayable(Double dedparttosbacpayable) {
		this.dedparttosbacpayable = dedparttosbacpayable;
	}
	public Double getTotalsbacpayable() {
		return totalsbacpayable;
	}
	public void setTotalsbacpayable(Double totalsbacpayable) {
		this.totalsbacpayable = totalsbacpayable;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getCaneaccountingdate() {
		return caneaccountingdate;
	}
	public void setCaneaccountingdate(Date caneaccountingdate) {
		this.caneaccountingdate = caneaccountingdate;
	}
	
	


}
