package com.finsol.model;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

//import com.sun.jmx.snmp.Timestamp;


/**
 * @author sahadeva
 */
@Entity

public class StockInSummary 
{
	@Id
	@Column(nullable = false)
    private String stockinno;
	@Column(nullable = false)
    private String finyr;
	
	@Column(nullable = false)
	@Type(type="date")
	private Date stockdate;
	
	@Column(nullable = true)
    private Timestamp SummaryTime;
	
	@Column(nullable = false)
    private Integer seqno;
	
	@Column(nullable = false)
	@Type(type="date")
	private Date entrydt;
	
	@Column(nullable = false)
    private int stocktransactionno;
	
	@Column(nullable = false)
    private String loginid;
	
	@Column(nullable = false)
    private Integer todeptid;
	
	@Column(nullable = false)
    private Integer fromdeptid;

	
	public int getStocktransactionno() {
		return stocktransactionno;
	}

	public void setStocktransactionno(int stocktransactionno) {
		this.stocktransactionno = stocktransactionno;
	}

	public String getFinyr() {
		return finyr;
	}

	public Date getStockdate() {
		return stockdate;
	}

	
	public Integer getSeqno() {
		return seqno;
	}

	public String getStockinno() {
		return stockinno;
	}

	public Date getEntrydt() {
		return entrydt;
	}

		

	public String getLoginid() {
		return loginid;
	}

	public Integer getTodeptid() {
		return todeptid;
	}

	public Integer getFromdeptid() {
		return fromdeptid;
	}

	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}

	public void setStockdate(Date stockdate) {
		this.stockdate = stockdate;
	}

	

	public Timestamp getSummaryTime() {
		return SummaryTime;
	}

	public void setSummaryTime(Timestamp summaryTime) {
		SummaryTime = summaryTime;
	}

	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}

	public void setStockinno(String stockinno) {
		this.stockinno = stockinno;
	}

	public void setEntrydt(Date entrydt) {
		this.entrydt = entrydt;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public void setTodeptid(Integer todeptid) {
		this.todeptid = todeptid;
	}

	public void setFromdeptid(Integer fromdeptid) {
		this.fromdeptid = fromdeptid;
	}


	
	
}
