package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Naidu
 *
 */
@Entity
public class Temp_YearlyLedger {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String ryotcode;
	@Column
	private Integer serialnumber;
	@Column
	private Integer weighbridgeslno;
	@Column
	private String shiftname;
	@Column
	private String weighmentDate;
	@Column
	private Double supqty;
	@Column
	private Double supValue;
	@Column
	private Double prgqty;
	@Column
	private Double prgSupValue;
	@Column
	private Double sbac;
	@Column
	private Double otherded;
	@Column
	private Double transportamt;
	@Column
	private Double ulamt;
	@Column
	private Double harvestamt;
	@Column
	private Double laAmt;
	@Column
	private Double cdcamt;
	@Column
	private Double totalDeductions;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getSerialnumber() {
		return serialnumber;
	}
	public void setSerialnumber(Integer serialnumber) {
		this.serialnumber = serialnumber;
	}
	public Integer getWeighbridgeslno() {
		return weighbridgeslno;
	}
	public void setWeighbridgeslno(Integer weighbridgeslno) {
		this.weighbridgeslno = weighbridgeslno;
	}
	
	public String getShiftname() {
		return shiftname;
	}
	public void setShiftname(String shiftname) {
		this.shiftname = shiftname;
	}
	public String getWeighmentDate() {
		return weighmentDate;
	}
	public void setWeighmentDate(String weighmentDate) {
		this.weighmentDate = weighmentDate;
	}
	public Double getSupqty() {
		return supqty;
	}
	public void setSupqty(Double supqty) {
		this.supqty = supqty;
	}
	public Double getSupValue() {
		return supValue;
	}
	public void setSupValue(Double supValue) {
		this.supValue = supValue;
	}
	public Double getPrgqty() {
		return prgqty;
	}
	public void setPrgqty(Double prgqty) {
		this.prgqty = prgqty;
	}
	public Double getPrgSupValue() {
		return prgSupValue;
	}
	public void setPrgSupValue(Double prgSupValue) {
		this.prgSupValue = prgSupValue;
	}
	public Double getSbac() {
		return sbac;
	}
	public void setSbac(Double sbac) {
		this.sbac = sbac;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getTransportamt() {
		return transportamt;
	}
	public void setTransportamt(Double transportamt) {
		this.transportamt = transportamt;
	}
	public Double getUlamt() {
		return ulamt;
	}
	public void setUlamt(Double ulamt) {
		this.ulamt = ulamt;
	}
	public Double getHarvestamt() {
		return harvestamt;
	}
	public void setHarvestamt(Double harvestamt) {
		this.harvestamt = harvestamt;
	}
	public Double getLaAmt() {
		return laAmt;
	}
	public void setLaAmt(Double laAmt) {
		this.laAmt = laAmt;
	}
	public Double getTotalDeductions() {
		return totalDeductions;
	}
	public void setTotalDeductions(Double totalDeductions) {
		this.totalDeductions = totalDeductions;
	}
	public Double getCdcamt() {
		return cdcamt;
	}
	public void setCdcamt(Double cdcamt) {
		this.cdcamt = cdcamt;
	}
	
	
}
