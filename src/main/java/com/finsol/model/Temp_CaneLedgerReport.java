package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_CaneLedgerReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private String village;
	@Column 
	private String agrmntdate;
	@Column
	private String branchcode;
	@Column 
	private String branch;
	@Column 
	private String accountnum;
	@Column 
	private Double plant;
	@Column 
	private Double ratoon;
	@Column 
	private Double frpprice;
	@Column 
	private Double agrqty;
	@Column 
	private String loancode;
	@Column 
	private String pamount;
	@Column 
	private String iamount;
	@Column 
	private Double proqty;
	@Column 
	private Double proamount;
	@Column 
	private Double irec;
	@Column 
	private Double lrec;
	@Column 
	private String wdate;
	@Column 
	private Integer recnum;
	@Column 
	private String shift;
	@Column 
	private Double supqty;
	@Column 
	private Double supvalue;
	@Column 
	private Double unload;     
	@Column 
	private Double transport;
	@Column 
	private Double harvest;
	@Column 
	private Double oded;
	@Column 
	private Double cdc;
	@Column 
	private Double lbal;
	@Column 
	private Double ibal;
	@Column 
	private Double irec1;
	@Column 
	private Double lrec1;
	@Column 
	private String betweendate;
	@Column 
	private Integer actualserialnumber;
	@Column 
	private Double payableamt;
	@Column 
	private String sbaccno;
	@Column 
	private Integer weighbridgeslno;
	@Column 
	private String loanacnos;
	@Column 
	private String bankcodes;
	@Column 
	private String advdates;
	@Column 
	private String advamounts;
	@Column 
	private String advloannos;
	
	
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getLoancode() {
		return loancode;
	}
	public void setLoancode(String loancode) {
		this.loancode = loancode;
	}
	public String getPamount() {
		return pamount;
	}
	public void setPamount(String pamount) {
		this.pamount = pamount;
	}
	public String getIamount() {
		return iamount;
	}
	public void setIamount(String iamount) {
		this.iamount = iamount;
	}
	public String getAdvloannos() {
		return advloannos;
	}
	public void setAdvloannos(String advloannos) {
		this.advloannos = advloannos;
	}
	public String getLoanacnos() {
		return loanacnos;
	}
	public void setLoanacnos(String loanacnos) {
		this.loanacnos = loanacnos;
	}
	public String getBankcodes() {
		return bankcodes;
	}
	public void setBankcodes(String bankcodes) {
		this.bankcodes = bankcodes;
	}
	public String getAdvdates() {
		return advdates;
	}
	public void setAdvdates(String advdates) {
		this.advdates = advdates;
	}
	public String getAdvamounts() {
		return advamounts;
	}
	public void setAdvamounts(String advamounts) {
		this.advamounts = advamounts;
	}
	public Integer getWeighbridgeslno() {
		return weighbridgeslno;
	}
	public void setWeighbridgeslno(Integer weighbridgeslno) {
		this.weighbridgeslno = weighbridgeslno;
	}
	public String getSbaccno() {
		return sbaccno;
	}
	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}
	public Double getPayableamt() {
		return payableamt;
	}
	public void setPayableamt(Double payableamt) {
		this.payableamt = payableamt;
	}
	public Integer getActualserialnumber() {
		return actualserialnumber;
	}
	public void setActualserialnumber(Integer actualserialnumber) {
		this.actualserialnumber = actualserialnumber;
	}
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getAgrmntdate() {
		return agrmntdate;
	}
	public void setAgrmntdate(String agrmntdate) {
		this.agrmntdate = agrmntdate;
	}
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAccountnum() {
		return accountnum;
	}
	public void setAccountnum(String accountnum) {
		this.accountnum = accountnum;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getRatoon() {
		return ratoon;
	}
	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}
	public Double getFrpprice() {
		return frpprice;
	}
	public void setFrpprice(Double frpprice) {
		this.frpprice = frpprice;
	}
	public Double getAgrqty() {
		return agrqty;
	}
	public void setAgrqty(Double agrqty) {
		this.agrqty = agrqty;
	}
	public Double getProqty() {
		return proqty;
	}
	public void setProqty(Double proqty) {
		this.proqty = proqty;
	}
	public Double getProamount() {
		return proamount;
	}
	public void setProamount(Double proamount) {
		this.proamount = proamount;
	}
	public Double getIrec() {
		return irec;
	}
	public void setIrec(Double irec) {
		this.irec = irec;
	}
	public Double getLrec() {
		return lrec;
	}
	public void setLrec(Double lrec) {
		this.lrec = lrec;
	}
	public String getWdate() {
		return wdate;
	}
	public void setWdate(String wdate) {
		this.wdate = wdate;
	}
	public Integer getRecnum() {
		return recnum;
	}
	public void setRecnum(Integer recnum) {
		this.recnum = recnum;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
	public Double getSupqty() {
		return supqty;
	}
	public void setSupqty(Double supqty) {
		this.supqty = supqty;
	}
	public Double getSupvalue() {
		return supvalue;
	}
	public void setSupvalue(Double supvalue) {
		this.supvalue = supvalue;
	}
	public Double getUnload() {
		return unload;
	}
	public void setUnload(Double unload) {
		this.unload = unload;
	}
	public Double getTransport() {
		return transport;
	}
	public void setTransport(Double transport) {
		this.transport = transport;
	}
	public Double getHarvest() {
		return harvest;
	}
	public void setHarvest(Double harvest) {
		this.harvest = harvest;
	}
	public Double getOded() {
		return oded;
	}
	public void setOded(Double oded) {
		this.oded = oded;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getLbal() {
		return lbal;
	}
	public void setLbal(Double lbal) {
		this.lbal = lbal;
	}
	public Double getIbal() {
		return ibal;
	}
	public void setIbal(Double ibal) {
		this.ibal = ibal;
	}
	public Double getIrec1() {
		return irec1;
	}
	public void setIrec1(Double irec1) {
		this.irec1 = irec1;
	}
	public Double getLrec1() {
		return lrec1;
	}
	public void setLrec1(Double lrec1) {
		this.lrec1 = lrec1;
	}
	
	

}
