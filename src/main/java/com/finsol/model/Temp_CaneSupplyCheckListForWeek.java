package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_CaneSupplyCheckListForWeek
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private Integer bkcode;
	@Column 
	private String acnumber;
	@Column 
	private Double canesupplied;
	@Column 
	private Double totalvalue;
	@Column 
	private Double deductions;
	@Column 
	private Double advances;
	@Column 
	private Double loan;
	@Column 
	private Double sb;
	@Column 
	private Double ul;
	@Column 
	private Double cdc;
	@Column 
	private Double hrate;
	@Column 
	private Double trate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Integer getBkcode() {
		return bkcode;
	}
	public void setBkcode(Integer bkcode) {
		this.bkcode = bkcode;
	}
	public String getAcnumber() {
		return acnumber;
	}
	public void setAcnumber(String acnumber) {
		this.acnumber = acnumber;
	}
	public Double getCanesupplied() {
		return canesupplied;
	}
	public void setCanesupplied(Double canesupplied) {
		this.canesupplied = canesupplied;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getDeductions() {
		return deductions;
	}
	public void setDeductions(Double deductions) {
		this.deductions = deductions;
	}
	public Double getAdvances() {
		return advances;
	}
	public void setAdvances(Double advances) {
		this.advances = advances;
	}
	public Double getLoan() {
		return loan;
	}
	public void setLoan(Double loan) {
		this.loan = loan;
	}
	public Double getSb() {
		return sb;
	}
	public void setSb(Double sb) {
		this.sb = sb;
	}
	public Double getUl() {
		return ul;
	}
	public void setUl(Double ul) {
		this.ul = ul;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getHrate() {
		return hrate;
	}
	public void setHrate(Double hrate) {
		this.hrate = hrate;
	}
	public Double getTrate() {
		return trate;
	}
	public void setTrate(Double trate) {
		this.trate = trate;
	}
	
	

}
