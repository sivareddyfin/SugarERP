package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class Branch {

	private static final long serialVersionUID = 1L;
	
	
	@Column  (nullable = false)
	private Integer id;
	
	
	//Removed by DMurty on 01-06-2016 for migration
	@Id
	@Column (nullable = false)
	private Integer branchcode;
	
	@Column
	private Integer bankcode;
	
	@Column
	private String branchname;
	
	@Column
	private String ifsccode;
	
	@Column
	private String micrcode;
	
	@Column
	private Long glcode;
	
	@Column
	private String email;
	
	@Column
	private String address;
	
	@Column
	private String city;
	
	@Column
	private String status;
	
	@Column
	private String contactperson;
	
	@Column
	private Long contactnumber;
	
	@Column
	private Long districtcode;
	
	@Column
	private Long statecode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}

	public Integer getBankcode() {
		return bankcode;
	}

	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	
	public Long getGlcode() {
		return glcode;
	}

	public void setGlcode(Long glcode) {
		this.glcode = glcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContactperson() {
		return contactperson;
	}

	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}

	public Long getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(Long contactnumber) {
		this.contactnumber = contactnumber;
	}

	public Long getDistrictcode() {
		return districtcode;
	}

	public void setDistrictcode(Long districtcode) {
		this.districtcode = districtcode;
	}

	public Long getStatecode() {
		return statecode;
	}

	public void setStatecode(Long statecode) {
		this.statecode = statecode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	public String getMicrcode() {
		return micrcode;
	}

	public void setMicrcode(String micrcode) {
		this.micrcode = micrcode;
	}


	
	

	
}
