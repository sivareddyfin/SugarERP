package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class ProductGroup 
{

	@Id
	@Column
    private Integer groupSeqNo;
	
	@Column(unique = true,nullable = false)
    private String productGroupCode;
    
	@Column(nullable = false)  
	private String groupName;
	
	@Column(nullable = true)  
	private String description;
    
    @Column
    private Byte status;

	public Integer getGroupSeqNo() {
		return groupSeqNo;
	}

	public String getProductGroupCode() {
		return productGroupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setGroupSeqNo(Integer groupSeqNo) {
		this.groupSeqNo = groupSeqNo;
	}

	public void setProductGroupCode(String productGroupCode) {
		this.productGroupCode = productGroupCode;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
    
    
    

	

    
    


}
