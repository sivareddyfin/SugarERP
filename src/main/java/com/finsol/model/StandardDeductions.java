package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */
@Entity
public class StandardDeductions
{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
    private Integer stdDedCode;
	
	@Column
    private String name;
	
	@Column
    private Double amount;
	
	@Column
    private Byte amountValidity;
	
	@Column
    private Integer deductionOrder;
		
	@Column
    private Byte status;
	
	@Column
    private String tablename;
	
	@Column
    private String columnname;
	

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public String getColumnname() {
		return columnname;
	}

	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStdDedCode() {
		return stdDedCode;
	}

	public void setStdDedCode(Integer stdDedCode) {
		this.stdDedCode = stdDedCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Byte getAmountValidity() {
		return amountValidity;
	}

	public void setAmountValidity(Byte amountValidity) {
		this.amountValidity = amountValidity;
	}

	public Integer getDeductionOrder() {
		return deductionOrder;
	}

	public void setDeductionOrder(Integer deductionOrder) {
		this.deductionOrder = deductionOrder;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	
}
