package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 *
 */
@Entity

public class CompanyBranches
{
	private static final long serialVersionUID = 1L;
	
	@Column
	private Integer id;
	
	@Id
	@Column
    private Integer branchid;
	
	@Column
    private String name;
	@Column
    private String location;
	@Column
    private String city;
	@Column
    private Integer statecode;
	@Column
    private Integer districtcode;
	
	@Column
    private String phoneno;
	@Column
    private String mobileno;
	@Column
    private String otherno;
	@Column
    private String fax;
	@Column
    private String email;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getBranchid() {
		return branchid;
	}
	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getStatecode() {
		return statecode;
	}
	public void setStatecode(Integer statecode) {
		this.statecode = statecode;
	}
	
	public Integer getDistrictcode() {
		return districtcode;
	}
	public void setDistrictcode(Integer districtcode) {
		this.districtcode = districtcode;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getOtherno() {
		return otherno;
	}
	public void setOtherno(String otherno) {
		this.otherno = otherno;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
