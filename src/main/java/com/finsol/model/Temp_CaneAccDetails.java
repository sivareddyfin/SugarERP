package com.finsol.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Temp_CaneAccDetails 
{
 private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String ryotcode;
	
	@Column
    private String ryotname;
	
	
	@Column
    private int bankcode;
	@Column
    private int caneslno;
	
	@Column
    private String accountnumber;
	
	
	@Column
    private double suppliedqty;
	
	@Column
    private double totalamt;
	
	
	@Column
    private double cdc;
	
	@Column
    private double ul;
	
	@Column
    private double tp;
	
	@Column
    private double harvest;
	
	
	@Column
    private double loan;
	
	@Column
    private double sb;
	
	@Column
    private double intrest;
	
	@Column
    private double advances;
	@Column
    private String betweendate;
	@Column
    private String branchname;
	@Column
    private Integer lbranchcode;
	@Column
    private String laccountnum;

	@Column
    private double otherded;
	
	
	public double getOtherded() {
		return otherded;
	}

	public void setOtherded(double otherded) {
		this.otherded = otherded;
	}

	public Integer getLbranchcode() {
		return lbranchcode;
	}

	public void setLbranchcode(Integer lbranchcode) {
		this.lbranchcode = lbranchcode;
	}

	public String getLaccountnum() {
		return laccountnum;
	}

	public void setLaccountnum(String laccountnum) {
		this.laccountnum = laccountnum;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public String getBetweendate() {
		return betweendate;
	}

	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}

	public double getIntrest() {
		return intrest;
	}

	public void setIntrest(double intrest) {
		this.intrest = intrest;
	}

	public double getAdvances() {
		return advances;
	}

	public void setAdvances(double advances) {
		this.advances = advances;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public int getBankcode() {
		return bankcode;
	}

	public void setBankcode(int bankcode) {
		this.bankcode = bankcode;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public double getSuppliedqty() {
		return suppliedqty;
	}

	public void setSuppliedqty(double suppliedqty) {
		this.suppliedqty = suppliedqty;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public double getCdc() {
		return cdc;
	}

	public void setCdc(double cdc) {
		this.cdc = cdc;
	}

	public double getUl() {
		return ul;
	}

	public void setUl(double ul) {
		this.ul = ul;
	}

	public double getTp() {
		return tp;
	}

	public void setTp(double tp) {
		this.tp = tp;
	}

	public double getHarvest() {
		return harvest;
	}

	public void setHarvest(double harvest) {
		this.harvest = harvest;
	}

	public double getLoan() {
		return loan;
	}

	public void setLoan(double loan) {
		this.loan = loan;
	}

	public double getSb() {
		return sb;
	}

	public void setSb(double sb) {
		this.sb = sb;
	}

	public int getCaneslno() {
		return caneslno;
	}

	public void setCaneslno(int caneslno) {
		this.caneslno = caneslno;
	}
	

}
