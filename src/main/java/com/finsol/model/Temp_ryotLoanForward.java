package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 */
@Entity
public class Temp_ryotLoanForward {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	 
	@Column
	private String branchname;

	@Column
	private Integer loannumber;

	@Column
	private Double disbursedamount;

	@Column
	private Double principle;

	@Column
	private String disburseddate;

	@Column
	private String ryotcode;

	@Column
	private Double plant;
	

	@Column
	private Double ratoon;

	@Column
	private String ryotname;

	@Column
	private String relativename;

	@Column
	private String village;
	
	public Integer getSlno() {
		return slno;
	}

	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public Integer getLoannumber() {
		return loannumber;
	}

	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}

	public Double getDisbursedamount() {
		return disbursedamount;
	}

	public void setDisbursedamount(Double disbursedamount) {
		this.disbursedamount = disbursedamount;
	}

	public Double getPrinciple() {
		return principle;
	}

	public void setPrinciple(Double principle) {
		this.principle = principle;
	}

	public String getDisburseddate() {
		return disburseddate;
	}

	public void setDisburseddate(String disburseddate) {
		this.disburseddate = disburseddate;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public Double getPlant() {
		return plant;
	}

	public void setPlant(Double plant) {
		this.plant = plant;
	}

	public Double getRatoon() {
		return ratoon;
	}

	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public String getRelativename() {
		return relativename;
	}

	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	
}
