package com.finsol.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 *
 */
@Entity
public class SssAgeementsAndRyots
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private double OFFER_SLNO;
	
	@Column
	@Type(type="date")
    private Date OFFER_DT;
	
	@Column
	private String RYOT_CODE;
	
	@Column
	private String TITLE_CODE;
	
	@Column
	private String NAME;
	
	@Column
	private String F_H_G_NAME;
	
	@Column
	private String LVCOD;
	
	@Column
	private double CI_CODE;
	
	@Column
	private String VARIETY_1;
	
	@Column
	private double PL_OR_RT_1;
	
	@Column
	@Type(type="date")
    private Date DT_PR_1;
	
	@Column
	private double EXTENT_1;
	
	@Column
	private double AGR_QTY_1;
	
	@Column
	private String SUR_1;
	
	
	@Column
	private String VARIETY_2;
	
	@Column
	private double PL_OR_RT_2;
	
	@Column
	@Type(type="date")
    private Date DT_PR_2;
	
	@Column
	private double EXTENT_2;
	
	@Column
	private double AGR_QTY_2;
	
	@Column
	private String SUR_2;
	
	@Column
	private String VARIETY_3;
	
	@Column
	private double PL_OR_RT_3;
	
	@Column
	@Type(type="date")
    private Date DT_PR_3;
	
	@Column
	private double EXTENT_3;
	
	@Column
	private double AGR_QTY_3;
	
	@Column
	private String SUR_3;
	
	
	@Column
	private String VARIETY_4;
	
	@Column
	private double PL_OR_RT_4;
	
	@Column
	@Type(type="date")
    private Date DT_PR_4;
	
	@Column
	private double EXTENT_4;
	
	@Column
	private double AGR_QTY_4;
	
	@Column
	private String SUR_4;
	
	@Column
	private String SEEDPAR;
	
	
	@Column
	private double MANRPAR;
	
	@Column
	private double BK_CODE;
	
	@Column
	private String SB_AC_NO;
	
	@Column
	private double M_NO;
	
	@Column
	private String SEEDPAR_1;
	
	@Column
	private String SEEDPAR_2;
	
	@Column
	private String SEEDPAR_3;
	
	@Column
	private String SEEDPAR_4;
	
	@Column
	private double PLOT_1;
	
	@Column
	private double PLOT_2;
	
	@Column
	private double PLOT_3;
	
	@Column
	private double PLOT_4;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getOFFER_SLNO() {
		return OFFER_SLNO;
	}

	public void setOFFER_SLNO(double offer_slno) {
		OFFER_SLNO = offer_slno;
	}

	public Date getOFFER_DT() {
		return OFFER_DT;
	}

	public void setOFFER_DT(Date offer_dt) {
		OFFER_DT = offer_dt;
	}

	public String getRYOT_CODE() {
		return RYOT_CODE;
	}

	public void setRYOT_CODE(String ryot_code) {
		RYOT_CODE = ryot_code;
	}

	public String getTITLE_CODE() {
		return TITLE_CODE;
	}

	public void setTITLE_CODE(String title_code) {
		TITLE_CODE = title_code;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String name) {
		NAME = name;
	}

	public String getF_H_G_NAME() {
		return F_H_G_NAME;
	}

	public void setF_H_G_NAME(String f_h_g_name) {
		F_H_G_NAME = f_h_g_name;
	}

	public String getLVCOD() {
		return LVCOD;
	}

	public void setLVCOD(String lvcod) {
		LVCOD = lvcod;
	}

	public double getCI_CODE() {
		return CI_CODE;
	}

	public void setCI_CODE(double ci_code) {
		CI_CODE = ci_code;
	}

	public String getVARIETY_1() {
		return VARIETY_1;
	}

	public void setVARIETY_1(String variety_1) {
		VARIETY_1 = variety_1;
	}

	public double getPL_OR_RT_1() {
		return PL_OR_RT_1;
	}

	public void setPL_OR_RT_1(double pl_or_rt_1) {
		PL_OR_RT_1 = pl_or_rt_1;
	}

	public Date getDT_PR_1() {
		return DT_PR_1;
	}

	public void setDT_PR_1(Date dt_pr_1) {
		DT_PR_1 = dt_pr_1;
	}

	public double getEXTENT_1() {
		return EXTENT_1;
	}

	public void setEXTENT_1(double extent_1) {
		EXTENT_1 = extent_1;
	}

	public double getAGR_QTY_1() {
		return AGR_QTY_1;
	}

	public void setAGR_QTY_1(double agr_qty_1) {
		AGR_QTY_1 = agr_qty_1;
	}



	public String getVARIETY_2() {
		return VARIETY_2;
	}

	public void setVARIETY_2(String variety_2) {
		VARIETY_2 = variety_2;
	}

	public double getPL_OR_RT_2() {
		return PL_OR_RT_2;
	}

	public void setPL_OR_RT_2(double pl_or_rt_2) {
		PL_OR_RT_2 = pl_or_rt_2;
	}

	public Date getDT_PR_2() {
		return DT_PR_2;
	}

	public void setDT_PR_2(Date dt_pr_2) {
		DT_PR_2 = dt_pr_2;
	}

	public double getEXTENT_2() {
		return EXTENT_2;
	}

	public void setEXTENT_2(double extent_2) {
		EXTENT_2 = extent_2;
	}

	public double getAGR_QTY_2() {
		return AGR_QTY_2;
	}

	public void setAGR_QTY_2(double agr_qty_2) {
		AGR_QTY_2 = agr_qty_2;
	}

	public String getVARIETY_3() {
		return VARIETY_3;
	}

	public void setVARIETY_3(String variety_3) {
		VARIETY_3 = variety_3;
	}

	public double getPL_OR_RT_3() {
		return PL_OR_RT_3;
	}

	public void setPL_OR_RT_3(double pl_or_rt_3) {
		PL_OR_RT_3 = pl_or_rt_3;
	}

	public Date getDT_PR_3() {
		return DT_PR_3;
	}

	public void setDT_PR_3(Date dt_pr_3) {
		DT_PR_3 = dt_pr_3;
	}

	public double getEXTENT_3() {
		return EXTENT_3;
	}

	public void setEXTENT_3(double extent_3) {
		EXTENT_3 = extent_3;
	}

	public double getAGR_QTY_3() {
		return AGR_QTY_3;
	}

	public void setAGR_QTY_3(double agr_qty_3) {
		AGR_QTY_3 = agr_qty_3;
	}

	public String getVARIETY_4() {
		return VARIETY_4;
	}

	public void setVARIETY_4(String variety_4) {
		VARIETY_4 = variety_4;
	}

	public double getPL_OR_RT_4() {
		return PL_OR_RT_4;
	}

	public void setPL_OR_RT_4(double pl_or_rt_4) {
		PL_OR_RT_4 = pl_or_rt_4;
	}

	public Date getDT_PR_4() {
		return DT_PR_4;
	}

	public void setDT_PR_4(Date dt_pr_4) {
		DT_PR_4 = dt_pr_4;
	}

	public double getEXTENT_4() {
		return EXTENT_4;
	}

	public void setEXTENT_4(double extent_4) {
		EXTENT_4 = extent_4;
	}

	public double getAGR_QTY_4() {
		return AGR_QTY_4;
	}

	public void setAGR_QTY_4(double agr_qty_4) {
		AGR_QTY_4 = agr_qty_4;
	}

	
	public String getSEEDPAR() {
		return SEEDPAR;
	}

	public void setSEEDPAR(String seedpar) {
		SEEDPAR = seedpar;
	}

	public double getMANRPAR() {
		return MANRPAR;
	}

	public void setMANRPAR(double manrpar) {
		MANRPAR = manrpar;
	}

	public double getBK_CODE() {
		return BK_CODE;
	}

	public void setBK_CODE(double bk_code) {
		BK_CODE = bk_code;
	}

	public String getSB_AC_NO() {
		return SB_AC_NO;
	}

	public void setSB_AC_NO(String sb_ac_no) {
		SB_AC_NO = sb_ac_no;
	}

	public double getM_NO() {
		return M_NO;
	}

	public void setM_NO(double m_no) {
		M_NO = m_no;
	}

	public String getSEEDPAR_1() {
		return SEEDPAR_1;
	}

	public void setSEEDPAR_1(String seedpar_1) {
		SEEDPAR_1 = seedpar_1;
	}

	public String getSEEDPAR_2() {
		return SEEDPAR_2;
	}

	public void setSEEDPAR_2(String seedpar_2) {
		SEEDPAR_2 = seedpar_2;
	}

	public String getSEEDPAR_3() {
		return SEEDPAR_3;
	}

	public void setSEEDPAR_3(String seedpar_3) {
		SEEDPAR_3 = seedpar_3;
	}

	public String getSEEDPAR_4() {
		return SEEDPAR_4;
	}

	public void setSEEDPAR_4(String seedpar_4) {
		SEEDPAR_4 = seedpar_4;
	}

	public double getPLOT_1() {
		return PLOT_1;
	}

	public void setPLOT_1(double plot_1) {
		PLOT_1 = plot_1;
	}

	public double getPLOT_2() {
		return PLOT_2;
	}

	public void setPLOT_2(double plot_2) {
		PLOT_2 = plot_2;
	}

	public double getPLOT_3() {
		return PLOT_3;
	}

	public void setPLOT_3(double plot_3) {
		PLOT_3 = plot_3;
	}

	public double getPLOT_4() {
		return PLOT_4;
	}

	public void setPLOT_4(double plot_4) {
		PLOT_4 = plot_4;
	}

	public String getSUR_1() {
		return SUR_1;
	}

	public void setSUR_1(String sur_1) {
		SUR_1 = sur_1;
	}

	public String getSUR_2() {
		return SUR_2;
	}

	public void setSUR_2(String sur_2) {
		SUR_2 = sur_2;
	}

	public String getSUR_3() {
		return SUR_3;
	}

	public void setSUR_3(String sur_3) {
		SUR_3 = sur_3;
	}

	public String getSUR_4() {
		return SUR_4;
	}

	public void setSUR_4(String sur_4) {
		SUR_4 = sur_4;
	}
	
	
	
}
