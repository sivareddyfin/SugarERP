package com.finsol.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva    
 */

@Entity
public class Temp_RankingStatementReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private Integer rank;
	@Column 
	private String dateofsample;
	@Column 
	private String aggrementno;
	@Column 
	private String plotno;
	@Column 
	private Integer samplecard;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private String village;
	@Column 
	private String plantorratoon;
	@Column 
	private String variety;
	@Column 
	private Double extentsize;
	@Column 
	private Double ccsrating;
	@Column 
	private String brix;
	@Column 
	private String pol;
	@Column 
	private String purity;
	@Column 
	private Integer programnumber;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getDateofsample() {
		return dateofsample;
	}
	public void setDateofsample(String dateofsample) {
		this.dateofsample = dateofsample;
	}
	public String getAggrementno() {
		return aggrementno;
	}
	public void setAggrementno(String aggrementno) {
		this.aggrementno = aggrementno;
	}
	public String getPlotno() {
		return plotno;
	}
	public void setPlotno(String plotno) {
		this.plotno = plotno;
	}
	public Integer getSamplecard() {
		return samplecard;
	}
	public void setSamplecard(Integer samplecard) {
		this.samplecard = samplecard;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Double getCcsrating() {
		return ccsrating;
	}
	public void setCcsrating(Double ccsrating) {
		this.ccsrating = ccsrating;
	}
	public String getBrix() {
		return brix;
	}
	public void setBrix(String brix) {
		this.brix = brix;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPurity() {
		return purity;
	}
	public void setPurity(String purity) {
		this.purity = purity;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Integer getProgramnumber() {
		return programnumber;
	}
	public void setProgramnumber(Integer programnumber) {
		this.programnumber = programnumber;
	}

	
	
}
