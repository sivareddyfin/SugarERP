package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class ICPDetailsByRyot {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private  String season;
	@Column
	private  String ryotcode; 
	@Column
	private Double suppliedcanewt;
	@Column
	private Double icprate;
	@Column
	private Double icpamount;
	
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getIcprate() {
		return icprate;
	}
	public void setIcprate(Double icprate) {
		this.icprate = icprate;
	}
	public Double getIcpamount() {
		return icpamount;
	}
	public void setIcpamount(Double icpamount) {
		this.icpamount = icpamount;
	}
	
	


}
