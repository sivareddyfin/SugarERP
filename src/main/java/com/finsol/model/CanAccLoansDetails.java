package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CanAccLoansDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer caneacctslno;
	@Column
	private String ryotcode;
	@Column
	private Integer branchcode;
	@Column
	private Integer loanno;
	@Column
	private String accountnum;	
	@Column
	private Integer advloan;
	@Column
	@Type(type="date")
	private Date advdate;
	@Column
	private Double advanceamount;
	@Column
	@Type(type="date")
	private Date repaymentdate;
	@Column
	private Double interestrate;
	@Column
	private Double subventedrate;
	@Column
	private Double netrate;
	@Column
	private Double interestamount;
	@Column
	private Double totalamount;
	@Column
	private Double paidamt;
	@Column
	private Double pendingamt;
	@Column
	private Double delayedintrest;
	@Column
	private Double adjustedprincipleamount;
	@Column
	private Double intryotpayable;
	@Column
	private Double additionalint;
	@Column
	private Double finalprinciple;
	@Column
	private Double finalinterest;
	@Column(nullable = true)
	@Type(type="date")
    private Date paymentdate;
	@Column
	private String paymentsource;
	@Column
	private byte isdirect;
	@Column
	private String ryotdetails;
	@Column
	private String mainryotcode;
	@Column
	private Double advisedamt;
	
	
	public Double getAdvisedamt() {
		return advisedamt;
	}
	public void setAdvisedamt(Double advisedamt) {
		this.advisedamt = advisedamt;
	}
	public Double getIntryotpayable() {
		return intryotpayable;
	}
	public void setIntryotpayable(Double intryotpayable) {
		this.intryotpayable = intryotpayable;
	}
	public Double getAdditionalint() {
		return additionalint;
	}
	public void setAdditionalint(Double additionalint) {
		this.additionalint = additionalint;
	}
	public Double getFinalprinciple() {
		return finalprinciple;
	}
	public void setFinalprinciple(Double finalprinciple) {
		this.finalprinciple = finalprinciple;
	}
	public Double getFinalinterest() {
		return finalinterest;
	}
	public void setFinalinterest(Double finalinterest) {
		this.finalinterest = finalinterest;
	}
	public Date getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(Date paymentdate) {
		this.paymentdate = paymentdate;
	}
	public String getPaymentsource() {
		return paymentsource;
	}
	public void setPaymentsource(String paymentsource) {
		this.paymentsource = paymentsource;
	}
	public byte getIsdirect() {
		return isdirect;
	}
	public void setIsdirect(byte isdirect) {
		this.isdirect = isdirect;
	}
	public String getRyotdetails() {
		return ryotdetails;
	}
	public void setRyotdetails(String ryotdetails) {
		this.ryotdetails = ryotdetails;
	}
	public String getMainryotcode() {
		return mainryotcode;
	}
	public void setMainryotcode(String mainryotcode) {
		this.mainryotcode = mainryotcode;
	}
	public Double getDelayedintrest() {
		return delayedintrest;
	}
	public void setDelayedintrest(Double delayedintrest) {
		this.delayedintrest = delayedintrest;
	}
	public Double getAdjustedprincipleamount() {
		return adjustedprincipleamount;
	}
	public void setAdjustedprincipleamount(Double adjustedprincipleamount) {
		this.adjustedprincipleamount = adjustedprincipleamount;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public Integer getCaneacctslno() {
		return caneacctslno;
	}
	public void setCaneacctslno(Integer caneacctslno) {
		this.caneacctslno = caneacctslno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	
	
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public Integer getLoanno() {
		return loanno;
	}
	public void setLoanno(Integer loanno) {
		this.loanno = loanno;
	}
	public String getAccountnum() {
		return accountnum;
	}
	public void setAccountnum(String accountnum) {
		this.accountnum = accountnum;
	}
	public Integer getAdvloan() {
		return advloan;
	}
	public void setAdvloan(Integer advloan) {
		this.advloan = advloan;
	}
	public Date getAdvdate() {
		return advdate;
	}
	public void setAdvdate(Date advdate) {
		this.advdate = advdate;
	}
	public Double getAdvanceamount() {
		return advanceamount;
	}
	public void setAdvanceamount(Double advanceamount) {
		this.advanceamount = advanceamount;
	}
	public Date getRepaymentdate() {
		return repaymentdate;
	}
	public void setRepaymentdate(Date repaymentdate) {
		this.repaymentdate = repaymentdate;
	}
	public Double getInterestrate() {
		return interestrate;
	}
	public void setInterestrate(Double interestrate) {
		this.interestrate = interestrate;
	}
	public Double getSubventedrate() {
		return subventedrate;
	}
	public void setSubventedrate(Double subventedrate) {
		this.subventedrate = subventedrate;
	}
	public Double getNetrate() {
		return netrate;
	}
	public void setNetrate(Double netrate) {
		this.netrate = netrate;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getPaidamt() {
		return paidamt;
	}
	public void setPaidamt(Double paidamt) {
		this.paidamt = paidamt;
	}
	public Double getPendingamt() {
		return pendingamt;
	}
	public void setPendingamt(Double pendingamt) {
		this.pendingamt = pendingamt;
	}
	
	

}
