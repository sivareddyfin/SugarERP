package com.finsol.model;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class GrnSummary
{
	@Id
	@Column
	private String grnno;
	@Column
    private String finyr;
	@Column
	@Type(type="date")
	private Date grndate;
	@Column
    private Timestamp grntime;
	@Column
    private Integer seqno;
	@Column
    private Integer deptid;
	@Column
    private String logninid;
	@Column
	private String stockissueno;
	@Column
	private int stockTransactionNo;
	
	
	public int getStockTransactionNo() {
		return stockTransactionNo;
	}
	public void setStockTransactionNo(int stockTransactionNo) {
		this.stockTransactionNo = stockTransactionNo;
	}
	public String getGrnno() {
		return grnno;
	}
	public void setGrnno(String grnno) {
		this.grnno = grnno;
	}
	public String getFinyr() {
		return finyr;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public Date getGrndate() {
		return grndate;
	}
	public void setGrndate(Date grndate) {
		this.grndate = grndate;
	}
	public Timestamp getGrntime() {
		return grntime;
	}
	public void setGrntime(Timestamp grntime) {
		this.grntime = grntime;
	}
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public Integer getDeptid() {
		return deptid;
	}
	public void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}
	public String getLogninid() {
		return logninid;
	}
	public void setLogninid(String logninid) {
		this.logninid = logninid;
	}
	public String getStockissueno() {
		return stockissueno;
	}
	public void setStockissueno(String stockissueno) {
		this.stockissueno = stockissueno;
	}
	
}
