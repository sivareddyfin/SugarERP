package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class Region 
{
	private static final long serialVersionUID = 1L;
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 
	 @Column(unique = true,nullable = false)
	 private Integer regioncode;
	 
	 @Column
	 private String region;
	 
	 @Column
	 private String description;
	 
	 @Column
	 private Byte status;

	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getRegioncode()
	{
		return regioncode;
	}
	public void setRegioncode(Integer regioncode)
	{
		this.regioncode = regioncode;
	}
	public String getRegion() 
	{
		return region;
	}
	public void setRegion(String region)
	{
		this.region = region;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Byte getStatus()
	{
		return status;
	}
	public void setStatus(Byte status) 
	{
		this.status = status;
	} 
}
