package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_ProcurementReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column 
	private String supplier;

	@Column 
	private String suppliername;
	@Column 
	private String village;
	@Column 
	private String variety;
	@Column 
	private Double nooftons;
	@Column 
	private String dateofplant;
	@Column 
	private String dateofharvest;
	@Column 
	private Double rate;
	@Column 
	private Double value;
	@Column 
	private Double acer;
	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getAcer() {
		return acer;
	}
	public void setAcer(Double acer) {
		this.acer = acer;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getNooftons() {
		return nooftons;
	}
	public void setNooftons(Double nooftons) {
		this.nooftons = nooftons;
	}
	public String getDateofplant() {
		return dateofplant;
	}
	public void setDateofplant(String dateofplant) {
		this.dateofplant = dateofplant;
	}
	public String getDateofharvest() {
		return dateofharvest;
	}
	public void setDateofharvest(String dateofharvest) {
		this.dateofharvest = dateofharvest;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	

}
