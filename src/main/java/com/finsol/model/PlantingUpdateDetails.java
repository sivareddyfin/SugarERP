package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class PlantingUpdateDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
    private String season;
	@Column
    private Integer fieldOfficer;
   	@Column
	@Type(type="date")
	private Date dateOfEntry;
	@Column
	private String variety;
	@Column
	private String ryotCode;
	@Column
	private Integer NoOfSeedling;
	@Column
	@Type(type="date")
	private Date dateofPlanting;
	
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getFieldOfficer() {
		return fieldOfficer;
	}
	public Date getDateOfEntry() {
		return dateOfEntry;
	}
	public String getVariety() {
		return variety;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public Integer getNoOfSeedling() {
		return NoOfSeedling;
	}
	public Date getDateofPlanting() {
		return dateofPlanting;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setFieldOfficer(Integer fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public void setDateOfEntry(Date dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setNoOfSeedling(Integer noOfSeedling) {
		NoOfSeedling = noOfSeedling;
	}
	public void setDateofPlanting(Date dateofPlanting) {
		this.dateofPlanting = dateofPlanting;
	}
	
	
    
}
