package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneReceiptWBBook {
	@Id
	@Column
	private Integer	weighbridgeid;
	@Column
	private Integer noofrects;
	@Column
	private Double grosswt;
	@Column
	private Double bindingmtlwt;
	@Column
	private Double netwt;
	@Column
	private Double burnedcanewt;
	@Column
	private Integer nooflorries;
	//@Column
	//private Integer nooftippers;
	//@Column
	//private Integer nooftentires;
	//@Column
	//private Integer nooftwelvetires;
	@Column
	private Integer noofpartloads;
	
	@Column
	private Integer nooftractors;
	@Column
	private Integer noofsmalllorries;
	@Column
	private Integer noofbiglorries;
	
	public Integer getNooftractors() {
		return nooftractors;
	}
	public void setNooftractors(Integer nooftractors) {
		this.nooftractors = nooftractors;
	}
	public Integer getNoofsmalllorries() {
		return noofsmalllorries;
	}
	public void setNoofsmalllorries(Integer noofsmalllorries) {
		this.noofsmalllorries = noofsmalllorries;
	}
	public Integer getNoofbiglorries() {
		return noofbiglorries;
	}
	public void setNoofbiglorries(Integer noofbiglorries) {
		this.noofbiglorries = noofbiglorries;
	}
	public Integer getWeighbridgeid() {
		return weighbridgeid;
	}
	public void setWeighbridgeid(Integer weighbridgeid) {
		this.weighbridgeid = weighbridgeid;
	}
	public Integer getNoofrects() {
		return noofrects;
	}
	public void setNoofrects(Integer noofrects) {
		this.noofrects = noofrects;
	}
	public Double getGrosswt() {
		return grosswt;
	}
	public void setGrosswt(Double grosswt) {
		this.grosswt = grosswt;
	}
	public Double getBindingmtlwt() {
		return bindingmtlwt;
	}
	public void setBindingmtlwt(Double bindingmtlwt) {
		this.bindingmtlwt = bindingmtlwt;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getBurnedcanewt() {
		return burnedcanewt;
	}
	public void setBurnedcanewt(Double burnedcanewt) {
		this.burnedcanewt = burnedcanewt;
	}
	public Integer getNooflorries() {
		return nooflorries;
	}
	public void setNooflorries(Integer nooflorries) {
		this.nooflorries = nooflorries;
	}
//	public Integer getNooftippers() {
//		return nooftippers;
//	}
//	public void setNooftippers(Integer nooftippers) {
//		this.nooftippers = nooftippers;
//	}
//	public Integer getNooftentires() {
//		return nooftentires;
//	}
//	public void setNooftentires(Integer nooftentires) {
//		this.nooftentires = nooftentires;
//	}
//	public Integer getNooftwelvetires() {
//		return nooftwelvetires;
//	}
//	public void setNooftwelvetires(Integer nooftwelvetires) {
//		this.nooftwelvetires = nooftwelvetires;
//	}
	public Integer getNoofpartloads() {
		return noofpartloads;
	}
	public void setNoofpartloads(Integer noofpartloads) {
		this.noofpartloads = noofpartloads;
	}
	
	
}
