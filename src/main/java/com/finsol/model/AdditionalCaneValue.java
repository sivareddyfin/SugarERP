package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class AdditionalCaneValue {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private  String season;
	
	@Column
	private  String ryotcode; 
	
	@Column
	@Type(type="date")
	private Date canesupplieddt;
	
	@Column
	private Double suppliedcanewt;
	
	@Column
	private Double additionalcaneprice;
	
	@Column
	private Double additionalcanevalue;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Date getCanesupplieddt() {
		return canesupplieddt;
	}
	public void setCanesupplieddt(Date canesupplieddt) {
		this.canesupplieddt = canesupplieddt;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getAdditionalcaneprice() {
		return additionalcaneprice;
	}
	public void setAdditionalcaneprice(Double additionalcaneprice) {
		this.additionalcaneprice = additionalcaneprice;
	}
	public Double getAdditionalcanevalue() {
		return additionalcanevalue;
	}
	public void setAdditionalcanevalue(Double additionalcanevalue) {
		this.additionalcanevalue = additionalcanevalue;
	}
	
	

}
