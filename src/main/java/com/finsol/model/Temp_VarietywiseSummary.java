package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author naidu
 *
 */
@Entity
public class Temp_VarietywiseSummary {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column 
	private String vcode;
	
	@Column
	private Integer varietycode;
	@Column 
	private String varietyname;
	@Column
	private Double plant;
	@Column
	private Double pqty;
	@Column
	private Double r1;
	@Column 
	private Double r2;
	@Column 
	private Double r3;
	@Column
	private Double r1qty;
	@Column
	private Double r2qty;
	@Column
	private Double r3qty;
	@Column
	private Double rtqty;
	@Column
	private Double rt;
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Integer getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}
	public String getVarietyname() {
		return varietyname;
	}
	public void setVarietyname(String varietyname) {
		this.varietyname = varietyname;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getPqty() {
		return pqty;
	}
	public void setPqty(Double pqty) {
		this.pqty = pqty;
	}
	public Double getR1() {
		return r1;
	}
	public void setR1(Double r1) {
		this.r1 = r1;
	}
	public Double getR2() {
		return r2;
	}
	public void setR2(Double r2) {
		this.r2 = r2;
	}
	public Double getR3() {
		return r3;
	}
	public void setR3(Double r3) {
		this.r3 = r3;
	}
	public Double getR1qty() {
		return r1qty;
	}
	public void setR1qty(Double r1qty) {
		this.r1qty = r1qty;
	}
	public Double getR2qty() {
		return r2qty;
	}
	public void setR2qty(Double r2qty) {
		this.r2qty = r2qty;
	}
	public Double getR3qty() {
		return r3qty;
	}
	public void setR3qty(Double r3qty) {
		this.r3qty = r3qty;
	}
	public Double getRtqty() {
		return rtqty;
	}
	public void setRtqty(Double rtqty) {
		this.rtqty = rtqty;
	}
	public Double getRt() {
		return rt;
	}
	public void setRt(Double rt) {
		this.rt = rt;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	

}
