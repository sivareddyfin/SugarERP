package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author DMurty
 */
@Entity
public class AccountSubGroup
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
    private Integer accountgroupcode;
	
	@Column(unique = true,nullable = false)
    private Integer accountsubgroupcode;

	@Column
    private String accountsubgroup;
    
    @Column
    private String description;
    
    @Column
    private Byte status;

    
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getAccountgroupcode()
	{
		return accountgroupcode;
	}

	public void setAccountgroupcode(Integer accountgroupcode)
	{
		this.accountgroupcode = accountgroupcode;
	}

	public Integer getAccountsubgroupcode()
	{
		return accountsubgroupcode;
	}

	public void setAccountsubgroupcode(Integer accountsubgroupcode)
	{
		this.accountsubgroupcode = accountsubgroupcode;
	}

	public String getAccountsubgroup() 
	{
		return accountsubgroup;
	}

	public void setAccountsubgroup(String accountsubgroup)
	{
		this.accountsubgroup = accountsubgroup;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status) 
	{
		this.status = status;
	}
}
