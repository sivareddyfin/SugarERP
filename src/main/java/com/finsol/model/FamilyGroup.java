package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class FamilyGroup 
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
    private Integer familygroupcode;
	
	@Column
    private String groupname;
	
	@Column
    private Integer zonecode;
	
	@Column
    private Integer circlecode;
	
	@Column
    private String description;
	
	@Column
    private Byte status;
	
	@Column
    private String familygroupmembers;
	
	
	public String getFamilygroupmembers() {
		return familygroupmembers;
	}

	public void setFamilygroupmembers(String familygroupmembers) {
		this.familygroupmembers = familygroupmembers;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFamilygroupcode() {
		return familygroupcode;
	}

	public void setFamilygroupcode(Integer familygroupcode) {
		this.familygroupcode = familygroupcode;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public Integer getZonecode() {
		return zonecode;
	}

	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}

	public Integer getCirclecode() {
		return circlecode;
	}

	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
}
