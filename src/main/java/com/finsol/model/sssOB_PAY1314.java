package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class sssOB_PAY1314
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String description;
	@Column
	private Double debit;
	@Column
	private Double credit;
	@Column
	private String obPdate;
	@Column
	private String fromyear;
	@Column
	private String toyear;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public String getObPdate() {
		return obPdate;
	}
	public void setObPdate(String obPdate) {
		this.obPdate = obPdate;
	}
	public String getFromyear() {
		return fromyear;
	}
	public void setFromyear(String fromyear) {
		this.fromyear = fromyear;
	}
	public String getToyear() {
		return toyear;
	}
	public void setToyear(String toyear) {
		this.toyear = toyear;
	}
	

}
