package com.finsol.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class DispatchAckSummary
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer dispatchSeq;
	@Column
	private String dispatchNo;
	@Column
	private String ryotCode;
	@Column
	private Integer fACode;
	@Column
	private Integer fOCode;
	@Column
	private String driverName;
	@Column
	private String drPhoneNo;
	@Column
	private String AcknowledgeBy;
	@Column
	private String indentNumbers;
	@Column
	private String ryotPhoneNumber;
	@Column
	private String dispatchedStatus;
	@Column(nullable = true)
	@Type(type="date")
    private Date dateOfAcknowledge;
	@Column
	private String loginUser;
	@Column
	private String ryotName;
	@Column
	private Integer seedlingQty;
	@Column
	private String vehicalNo;
	@Column
	private String landSurvey;
	@Column
	private String agreementSign;
	@Column
	private String soilWaterAnalyis;
	
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	
	public Integer getDispatchSeq() {
		return dispatchSeq;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public Integer getfACode() {
		return fACode;
	}
	public Integer getfOCode() {
		return fOCode;
	}
	public String getDriverName() {
		return driverName;
	}
	public String getDrPhoneNo() {
		return drPhoneNo;
	}
	public String getAcknowledgeBy() {
		return AcknowledgeBy;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public String getRyotPhoneNumber() {
		return ryotPhoneNumber;
	}
	public String getDispatchedStatus() {
		return dispatchedStatus;
	}
	public Date getDateOfAcknowledge() {
		return dateOfAcknowledge;
	}
	public String getLoginUser() {
		return loginUser;
	}
	public String getRyotName() {
		return ryotName;
	}
	public Integer getSeedlingQty() {
		return seedlingQty;
	}
	public String getVehicalNo() {
		return vehicalNo;
	}
	public String getLandSurvey() {
		return landSurvey;
	}
	public String getAgreementSign() {
		return agreementSign;
	}
	public String getSoilWaterAnalyis() {
		return soilWaterAnalyis;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public void setDispatchSeq(Integer dispatchSeq) {
		this.dispatchSeq = dispatchSeq;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setfACode(Integer fACode) {
		this.fACode = fACode;
	}
	public void setfOCode(Integer fOCode) {
		this.fOCode = fOCode;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public void setDrPhoneNo(String drPhoneNo) {
		this.drPhoneNo = drPhoneNo;
	}
	public void setAcknowledgeBy(String acknowledgeBy) {
		AcknowledgeBy = acknowledgeBy;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setRyotPhoneNumber(String ryotPhoneNumber) {
		this.ryotPhoneNumber = ryotPhoneNumber;
	}
	public void setDispatchedStatus(String dispatchedStatus) {
		this.dispatchedStatus = dispatchedStatus;
	}
	public void setDateOfAcknowledge(Date dateOfAcknowledge) {
		this.dateOfAcknowledge = dateOfAcknowledge;
	}
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public void setSeedlingQty(Integer seedlingQty) {
		this.seedlingQty = seedlingQty;
	}
	public void setVehicalNo(String vehicalNo) {
		this.vehicalNo = vehicalNo;
	}
	public void setLandSurvey(String landSurvey) {
		this.landSurvey = landSurvey;
	}
	public void setAgreementSign(String agreementSign) {
		this.agreementSign = agreementSign;
	}
	public void setSoilWaterAnalyis(String soilWaterAnalyis) {
		this.soilWaterAnalyis = soilWaterAnalyis;
	}
	
	

	
	
	

	
	

}
