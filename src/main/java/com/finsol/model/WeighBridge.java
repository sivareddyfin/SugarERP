package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */

@Entity
public class WeighBridge 
{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
    private Integer bridgeid;
	
	@Column
    private String bridgename;
	
	@Column
    private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBridgeid() {
		return bridgeid;
	}

	public void setBridgeid(Integer bridgeid) {
		this.bridgeid = bridgeid;
	}

	public String getBridgename() {
		return bridgename;
	}

	public void setBridgename(String bridgename) {
		this.bridgename = bridgename;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
}
