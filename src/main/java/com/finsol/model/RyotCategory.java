package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author DMurty
 *
 */
@Entity
public class RyotCategory
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	
	@Column
	private String ryotname;
	
	
	@Column
	private String harvestingstatus;
	
	@Column
	private String iscommon;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public String getHarvestingstatus() {
		return harvestingstatus;
	}

	public void setHarvestingstatus(String harvestingstatus) {
		this.harvestingstatus = harvestingstatus;
	}

	public String getIscommon() {
		return iscommon;
	}

	public void setIscommon(String iscommon) {
		this.iscommon = iscommon;
	}
}
