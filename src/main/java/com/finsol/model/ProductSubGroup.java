package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author umanath ch
 *
 */
@Entity
public class ProductSubGroup 
{
	@Id
	@Column
    private String productSubGroupCode;
	
	@Column(unique = true,nullable = false)
    private String subGroupName;
    
	@Column(nullable = false)  
	private String productGroupCode;
	
	@Column(nullable = false)  
	private Integer subGroupSeq; 

	
	@Column(nullable = true)  
	private String description;
    
    @Column
    private Byte status;

	public String getProductSubGroupCode() {
		return productSubGroupCode;
	}

	public String getSubGroupName() {
		return subGroupName;
	}

	public String getProductGroupCode() {
		return productGroupCode;
	}

	public Integer getSubGroupSeq() {
		return subGroupSeq;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setProductSubGroupCode(String productSubGroupCode) {
		this.productSubGroupCode = productSubGroupCode;
	}

	public void setSubGroupName(String subGroupName) {
		this.subGroupName = subGroupName;
	}

	public void setProductGroupCode(String productGroupCode) {
		this.productGroupCode = productGroupCode;
	}

	public void setSubGroupSeq(Integer subGroupSeq) {
		this.subGroupSeq = subGroupSeq;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	
    
    
	
}
