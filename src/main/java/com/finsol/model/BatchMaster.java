package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class BatchMaster 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date trayFillingDate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private String batchSeries;
	@Column
	private String variety;
	@Column
	private Integer batchMonth;
	@Column
	private Integer batchDate;
	@Column
	private String batch;
	@Column
	private Double totalSeedlings;
	@Column
	private Double delivered;
	@Column
	private Double takenOffOnes;
	@Column
	private Double balance;
	@Column
	private Double survivalPercent;
	@Column
	private Byte isBatchCompleted;
	
	public Integer getSlno()
	{
		return slno;
	}
	public String getSeason() {
		return season;
	}
	public Date getTrayFillingDate() {
		return trayFillingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public Integer getId() {
		return id;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	
	
	
	public String getBatch() {
		return batch;
	}
	public Double getTotalSeedlings() {
		return totalSeedlings;
	}
	public Double getDelivered() {
		return delivered;
	}
	public Double getTakenOffOnes() {
		return takenOffOnes;
	}
	public Double getBalance() {
		return balance;
	}
	public Double getSurvivalPercent() {
		return survivalPercent;
	}
	public Byte getIsBatchCompleted() {
		return isBatchCompleted;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setTrayFillingDate(Date trayFillingDate) {
		this.trayFillingDate = trayFillingDate;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public Integer getBatchMonth() {
		return batchMonth;
	}
	public Integer getBatchDate() {
		return batchDate;
	}
	public void setBatchMonth(Integer batchMonth) {
		this.batchMonth = batchMonth;
	}
	public void setBatchDate(Integer batchDate) {
		this.batchDate = batchDate;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public void setTotalSeedlings(Double totalSeedlings) {
		this.totalSeedlings = totalSeedlings;
	}
	public void setDelivered(Double delivered) {
		this.delivered = delivered;
	}
	public void setTakenOffOnes(Double takenOffOnes) {
		this.takenOffOnes = takenOffOnes;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public void setSurvivalPercent(Double survivalPercent) {
		this.survivalPercent = survivalPercent;
	}
	public void setIsBatchCompleted(Byte isBatchCompleted) {
		this.isBatchCompleted = isBatchCompleted;
	}
	
	
	

}
