package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */

@Entity
public class SeedlingTrayChargesDetails 
{
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private Integer id;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	@Column(nullable = false)
    private Integer returnid;

	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
    private String ryotcode;
	
	@Column(nullable = false)
    private Integer traytype;
	
	@Column(nullable = false)
    private Integer nooftrays;
	
	@Column(nullable = false)
    private Double cost;
	
	@Column(nullable = false)
    private Double totalcost;
	
	@Column(nullable = false)
    private Byte status;
	
	@Column(nullable = false)
    private Integer transactioncode;
	
	@Column
    private String villagecode;

	
	public Integer getSlno() 
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}
	public Integer getTransactioncode()
	{
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode)
	{
		this.transactioncode = transactioncode;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status) 
	{
		this.status = status;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getReturnid() 
	{
		return returnid;
	}

	public void setReturnid(Integer returnid) 
	{
		this.returnid = returnid;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public String getRyotcode()
	{
		return ryotcode;
	}

	public void setRyotcode(String ryotcode)
	{
		this.ryotcode = ryotcode;
	}


	public Integer getTraytype()
	{
		return traytype;
	}

	public void setTraytype(Integer traytype) 
	{
		this.traytype = traytype;
	}

	public Integer getNooftrays()
	{
		return nooftrays;
	}

	public void setNooftrays(Integer nooftrays) 
	{
		this.nooftrays = nooftrays;
	}

	public Double getCost() 
	{
		return cost;
	}

	public void setCost(Double cost)
	{
		this.cost = cost;
	}

	public Double getTotalcost()
	{
		return totalcost;
	}

	public void setTotalcost(Double totalcost)
	{
		this.totalcost = totalcost;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	
}
