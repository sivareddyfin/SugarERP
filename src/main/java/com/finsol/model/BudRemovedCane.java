package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class BudRemovedCane 
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date budremoveddate;
	
	@Column
    private String sentbyvehicle;
    
     @Column
    private String budremovetime;
	
    @Column
    private String weight;
    
    @Column
    private String drivermobilenumber;
    
    @Column
    private String destination;
    
    @Column
    private String drivername;
    
    @Column
    private String sentto;

    @Column
    private String vehiclenumber;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getBudremoveddate() {
		return budremoveddate;
	}

	public void setBudremoveddate(Date budremoveddate) {
		this.budremoveddate = budremoveddate;
	}

	public String getSentbyvehicle() {
		return sentbyvehicle;
	}

	public void setSentbyvehicle(String sentbyvehicle) {
		this.sentbyvehicle = sentbyvehicle;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getDrivermobilenumber() {
		return drivermobilenumber;
	}

	public String getBudremovetime() {
		return budremovetime;
	}

	public void setBudremovetime(String budremovetime) {
		this.budremovetime = budremovetime;
	}

	public void setDrivermobilenumber(String drivermobilenumber) {
		this.drivermobilenumber = drivermobilenumber;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDrivername() {
		return drivername;
	}

	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}

	public String getSentto() {
		return sentto;
	}

	public void setSentto(String sentto) {
		this.sentto = sentto;
	}

	public String getVehiclenumber() {
		return vehiclenumber;
	}

	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}
    
    
    
    
    
    
}
