package com.finsol.model;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva    
 */
@Entity
public class Temp_CaneGrowersListCircleProgramWise 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column 
	private String ryotcode;
	
	@Column 
	private String ryotname;
	@Column 
	private String circle;

	@Column 
	private String aggrementno;
	@Column 
	private String raggrementno;
	@Column 
	private String village;
	
	@Column 
	private String programnumber;
	@Column 
	private String rprogramnumber;
	@Column 
	private String variety;
	@Column 
	private Double extentsize;
	@Column 
	private String rvariety;
	@Column 
	private Double rextentsize;
	@Column 
	private Double agreedqty;
	@Column 
	private Double ragreedqty;
	@Column 
	private String cropdate;
	@Column 
	private String rcropdate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getAggrementno() {
		return aggrementno;
	}
	public void setAggrementno(String aggrementno) {
		this.aggrementno = aggrementno;
	}
	public String getRaggrementno() {
		return raggrementno;
	}
	public void setRaggrementno(String raggrementno) {
		this.raggrementno = raggrementno;
	}
	public String getProgramnumber() {
		return programnumber;
	}
	public void setProgramnumber(String programnumber) {
		this.programnumber = programnumber;
	}
	public String getRprogramnumber() {
		return rprogramnumber;
	}
	public void setRprogramnumber(String rprogramnumber) {
		this.rprogramnumber = rprogramnumber;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public String getRvariety() {
		return rvariety;
	}
	public void setRvariety(String rvariety) {
		this.rvariety = rvariety;
	}
	public Double getRextentsize() {
		return rextentsize;
	}
	public void setRextentsize(Double rextentsize) {
		this.rextentsize = rextentsize;
	}
	public Double getAgreedqty() {
		return agreedqty;
	}
	public void setAgreedqty(Double agreedqty) {
		this.agreedqty = agreedqty;
	}
	public Double getRagreedqty() {
		return ragreedqty;
	}
	public void setRagreedqty(Double ragreedqty) {
		this.ragreedqty = ragreedqty;
	}
	public String getCropdate() {
		return cropdate;
	}
	public void setCropdate(String cropdate) {
		this.cropdate = cropdate;
	}
	public String getRcropdate() {
		return rcropdate;
	}
	public void setRcropdate(String rcropdate) {
		this.rcropdate = rcropdate;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	
	
}
