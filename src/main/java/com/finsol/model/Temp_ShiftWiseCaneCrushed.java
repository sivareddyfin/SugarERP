package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_ShiftWiseCaneCrushed 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String shift;
	@Column
	private Double netwt1;
	@Column
	private Double netwt2;
	@Column
	private Double netwt3;
	@Column
	private Double openQty;
		
	public Double getOpenQty() {
		return openQty;
	}
	public void setOpenQty(Double openQty) {
		this.openQty = openQty;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
	public Double getNetwt1() {
		return netwt1;
	}
	public void setNetwt1(Double netwt1) {
		this.netwt1 = netwt1;
	}
	public Double getNetwt2() {
		return netwt2;
	}
	public void setNetwt2(Double netwt2) {
		this.netwt2 = netwt2;
	}
	public Double getNetwt3() {
		return netwt3;
	}
	public void setNetwt3(Double netwt3) {
		this.netwt3 = netwt3;
	}
	
	

}
