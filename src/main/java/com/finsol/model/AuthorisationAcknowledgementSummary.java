package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author s sahadeva
 */
@Entity
				
public class AuthorisationAcknowledgementSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	@Type(type="date")
	private Date authorisationDate;
	@Column
	private String ryotCode;
	@Column
	private String ryotName;
	
	@Column
	private String fieldOfficer;
	@Column
	private String indentNumbers;
	@Column
	private String authorisedBy;
	@Column
	private String phoneNo;
	@Column
	private String authorisationSeqNo;
	@Column
	private Double GrandTotal;
	@Column
	private Integer faCode;
	@Column
	private String agreementnumber;
	@Column
	private String storecode;
	@Column
	private Integer transactioncode;
	@Column
	private Double suppliertotal;
	@Column
	private Double ryottotal;
	@Column
	@Type(type="date")
	private Date billRefDate;
	@Column
	private String billRefNum;
	
	
	
	public Double getSuppliertotal() {
		return suppliertotal;
	}
	public Double getRyottotal() {
		return ryottotal;
	}
	public void setSuppliertotal(Double suppliertotal) {
		this.suppliertotal = suppliertotal;
	}
	public void setRyottotal(Double ryottotal) {
		this.ryottotal = ryottotal;
	}
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getAuthorisationDate() {
		return authorisationDate;
	}
	public void setAuthorisationDate(Date authorisationDate) {
		this.authorisationDate = authorisationDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getFieldOfficer() {
		return fieldOfficer;
	}
	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public String getAuthorisedBy() {
		return authorisedBy;
	}
	public void setAuthorisedBy(String authorisedBy) {
		this.authorisedBy = authorisedBy;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public Double getGrandTotal() {
		return GrandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		GrandTotal = grandTotal;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getStorecode() {
		return storecode;
	}
	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}
	public Date getBillRefDate() {
		return billRefDate;
	}
	public void setBillRefDate(Date billRefDate) {
		this.billRefDate = billRefDate;
	}
	public String getBillRefNum() {
		return billRefNum;
	}
	public void setBillRefNum(String billRefNum) {
		this.billRefNum = billRefNum;
	}
	
	
	

}
