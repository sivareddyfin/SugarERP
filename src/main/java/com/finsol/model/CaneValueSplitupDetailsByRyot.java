package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneValueSplitupDetailsByRyot {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private String ryotcode;
	@Column
	private String villagecode;
	@Column
	private Integer ryotcodeseqno;
	@Column
	private Double suppliedcaneWt;
	@Column
	private Double totalvalue;
	@Column
	private Double foradvloans;
	@Column
	private Double advancespaid;
	@Column
	private Double tieuploanspaid;
	@Column
	private Double loansparttosbac;
	@Column
	private Double fordeductions;
	@Column
	private Double cdcpaid;
	@Column
	private Double ucpaid;
	@Column
	private Double otherdedpaid;
	@Column
	private Double harvestchargespaid;
	@Column
	private Double transportchargespaid;
	@Column
	private Double dedparttosbac;
	@Column
	private Double totalsbpaid;
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getRyotcodeseqno() {
		return ryotcodeseqno;
	}
	public void setRyotcodeseqno(Integer ryotcodeseqno) {
		this.ryotcodeseqno = ryotcodeseqno;
	}
	public Double getSuppliedcaneWt() {
		return suppliedcaneWt;
	}
	public void setSuppliedcaneWt(Double suppliedcaneWt) {
		this.suppliedcaneWt = suppliedcaneWt;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getForadvloans() {
		return foradvloans;
	}
	public void setForadvloans(Double foradvloans) {
		this.foradvloans = foradvloans;
	}
	
	public Double getAdvancespaid() {
		return advancespaid;
	}
	public void setAdvancespaid(Double advancespaid) {
		this.advancespaid = advancespaid;
	}
	
	public Double getTieuploanspaid() {
		return tieuploanspaid;
	}
	public void setTieuploanspaid(Double tieuploanspaid) {
		this.tieuploanspaid = tieuploanspaid;
	}
	public Double getLoansparttosbac() {
		return loansparttosbac;
	}
	public void setLoansparttosbac(Double loansparttosbac) {
		this.loansparttosbac = loansparttosbac;
	}
	public Double getFordeductions() {
		return fordeductions;
	}
	public void setFordeductions(Double fordeductions) {
		this.fordeductions = fordeductions;
	}
	
	public Double getCdcpaid() {
		return cdcpaid;
	}
	public void setCdcpaid(Double cdcpaid) {
		this.cdcpaid = cdcpaid;
	}
	
	public Double getUcpaid() {
		return ucpaid;
	}
	public void setUcpaid(Double ucpaid) {
		this.ucpaid = ucpaid;
	}
	
	public Double getOtherdedpaid() {
		return otherdedpaid;
	}
	public void setOtherdedpaid(Double otherdedpaid) {
		this.otherdedpaid = otherdedpaid;
	}
	
	public Double getHarvestchargespaid() {
		return harvestchargespaid;
	}
	public void setHarvestchargespaid(Double harvestchargespaid) {
		this.harvestchargespaid = harvestchargespaid;
	}
	
	public Double getTransportchargespaid() {
		return transportchargespaid;
	}
	public void setTransportchargespaid(Double transportchargespaid) {
		this.transportchargespaid = transportchargespaid;
	}
	public Double getDedparttosbac() {
		return dedparttosbac;
	}
	public void setDedparttosbac(Double dedparttosbac) {
		this.dedparttosbac = dedparttosbac;
	}
	
	public Double getTotalsbpaid() {
		return totalsbpaid;
	}
	public void setTotalsbpaid(Double totalsbpaid) {
		this.totalsbpaid = totalsbpaid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
