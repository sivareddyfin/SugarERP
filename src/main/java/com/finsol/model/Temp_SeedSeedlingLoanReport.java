package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class Temp_SeedSeedlingLoanReport {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String relativename;
	@Column
	private String agreementnumber;
	@Column
	private String village;
	@Column
	private String seedsellercode;
	@Column
	private String seedryotname;
	@Column
	private String seedfathername;
	@Column
	private String seedvillage;
	@Column
	private String accountnumber;
	@Column
	private Double plant;
	@Column
	private Double loanamount;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSeedsellercode() {
		return seedsellercode;
	}
	public void setSeedsellercode(String seedsellercode) {
		this.seedsellercode = seedsellercode;
	}
	public String getSeedryotname() {
		return seedryotname;
	}
	public void setSeedryotname(String seedryotname) {
		this.seedryotname = seedryotname;
	}
	public String getSeedfathername() {
		return seedfathername;
	}
	public void setSeedfathername(String seedfathername) {
		this.seedfathername = seedfathername;
	}
	public String getSeedvillage() {
		return seedvillage;
	}
	public void setSeedvillage(String seedvillage) {
		this.seedvillage = seedvillage;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getLoanamount() {
		return loanamount;
	}
	public void setLoanamount(Double loanamount) {
		this.loanamount = loanamount;
	}
	
	
	
	
	
	
}
