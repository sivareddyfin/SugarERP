package com.finsol.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */
@Entity
public class Temp_CaneSampleCard 
{
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 @Column
	 private int programnumber;
	 @Column
	 private String ryotcode;
	 @Column
	 private String ryotname;
	 @Column
	 private String relativename;
	 @Column
	 private String village;
	 @Column
	 private String circle;
	 @Column
	 private String variety;
	 @Column
	 private Double extentsize;
	 @Column
	 private Date prdate;
	 @Column
	 private Date sampledate;
	 @Column
	 private Integer noofcanesinthesample;
	 @Column
	 private String fieldreport;
	 @Column
	 private Double caneweight;
	 @Column
	 private Double juiceweight;
	 @Column
	 private String pol;
	 @Column
	 private String brix;
	 @Column
	 private String purity;
	 @Column
	 private Double ccsrating;
	 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getProgramnumber() {
		return programnumber;
	}
	public void setProgramnumber(int programnumber) {
		this.programnumber = programnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Date getPrdate() {
		return prdate;
	}
	public void setPrdate(Date prdate) {
		this.prdate = prdate;
	}
	public Date getSampledate() {
		return sampledate;
	}
	public void setSampledate(Date sampledate) {
		this.sampledate = sampledate;
	}
	public Integer getNoofcanesinthesample() {
		return noofcanesinthesample;
	}
	public void setNoofcanesinthesample(Integer noofcanesinthesample) {
		this.noofcanesinthesample = noofcanesinthesample;
	}
	public String getFieldreport() {
		return fieldreport;
	}
	public void setFieldreport(String fieldreport) {
		this.fieldreport = fieldreport;
	}
	public Double getCaneweight() {
		return caneweight;
	}
	public void setCaneweight(Double caneweight) {
		this.caneweight = caneweight;
	}
	public Double getJuiceweight() {
		return juiceweight;
	}
	public void setJuiceweight(Double juiceweight) {
		this.juiceweight = juiceweight;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getBrix() {
		return brix;
	}
	public void setBrix(String brix) {
		this.brix = brix;
	}
	public String getPurity() {
		return purity;
	}
	public void setPurity(String purity) {
		this.purity = purity;
	}
	public Double getCcsrating() {
		return ccsrating;
	}
	public void setCcsrating(Double ccsrating) {
		this.ccsrating = ccsrating;
	}
	 
}
