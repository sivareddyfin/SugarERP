package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
/**
 * @author admin
 *
 */
@Entity
public class SeedConsumerDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String season;
	@Column(nullable = false)
    private Date sourcesupplieddate;

	@Column(nullable = true)
    private String consumercode;
	
	@Column(nullable = false)
    private String seedsrcode;
	@Column(nullable = false)
    private String batchno;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getSourcesupplieddate() {
		return sourcesupplieddate;
	}
	public void setSourcesupplieddate(Date sourcesupplieddate) {
		this.sourcesupplieddate = sourcesupplieddate;
	}
	public String getConsumercode() {
		return consumercode;
	}
	public void setConsumercode(String consumercode) {
		this.consumercode = consumercode;
	}
	public String getSeedsrcode() {
		return seedsrcode;
	}
	public void setSeedsrcode(String seedsrcode) {
		this.seedsrcode = seedsrcode;
	}
	public String getBatchno() {
		return batchno;
	}
	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}
	
	
}
