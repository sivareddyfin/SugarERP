package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class TransportAllowanceDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private  String Season;
	@Column
	@Type(type="date")
	private Date receiptdate;
	@Column
	private Time receipttime;
	@Column
	private  String ryotcode;
	@Column
	private  String agreementno;
	@Column
	private  String plotnumber;
	@Column
	private  Double suppliedcanewt;
	@Column
	private String landvillagecode;
	@Column
	private  Double  distance;
	@Column
	private  Double allowance;
	@Column
	private  Double amount;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return Season;
	}
	public void setSeason(String season) {
		Season = season;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public Time getReceipttime() {
		return receipttime;
	}
	public void setReceipttime(Time receipttime) {
		this.receipttime = receipttime;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getAgreementno() {
		return agreementno;
	}
	public void setAgreementno(String agreementno) {
		this.agreementno = agreementno;
	}
	public String getPlotnumber() {
		return plotnumber;
	}
	public void setPlotnumber(String plotnumber) {
		this.plotnumber = plotnumber;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	
	public String getLandvillagecode() {
		return landvillagecode;
	}
	public void setLandvillagecode(String landvillagecode) {
		this.landvillagecode = landvillagecode;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getAllowance() {
		return allowance;
	}
	public void setAllowance(Double allowance) {
		this.allowance = allowance;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	


}
