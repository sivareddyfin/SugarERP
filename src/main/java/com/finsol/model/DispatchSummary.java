package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class DispatchSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date dateOfDispatch;
	
	@Column
	private Integer dispatchSeq;
	@Column
	private String dispatchNo;
	@Column
	private String ryotCode;
	@Column
	private Integer fACode;
	@Column
	private Integer fOCode;
	@Column
	private String driverName;
	@Column
	private String drPhoneNo;
	@Column
	private String dispatchBy;
	@Column
	private String indentNumbers;
	@Column
	private String ryotPhoneNumber;
	@Column
	private Integer dispatchedStatus;
	@Column
	private Integer seedlingQty;
	@Column
	private String vehicalNo;
	@Column
	private String landSurvey;
	@Column
	private String agreementSign;
	@Column
	private String soilWaterAnalyis;
	@Column
	private String ryotName;
	@Column
	private Double issuedQty;
	@Column
	private Double pendingQty;
	@Column
	private String agreementnumber;
	@Column(nullable = true)
	@Type(type="time")
	private Time deliveryTime;
	@Column
	@Type(type="date")
	private Date deliveryDate;
	@Column
	private Integer trasportcontractor;
	
	public Time getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Time deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public Double getIssuedQty() {
		return issuedQty;
	}
	public Double getPendingQty() {
		return pendingQty;
	}
	public void setIssuedQty(Double issuedQty) {
		this.issuedQty = issuedQty;
	}
	public void setPendingQty(Double pendingQty) {
		this.pendingQty = pendingQty;
	}
	public String getRyotName() {
		return ryotName;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	public String getLandSurvey() {
		return landSurvey;
	}
	public String getAgreementSign() {
		return agreementSign;
	}
	public String getSoilWaterAnalyis() {
		return soilWaterAnalyis;
	}
	public void setLandSurvey(String landSurvey) {
		this.landSurvey = landSurvey;
	}
	public void setAgreementSign(String agreementSign) {
		this.agreementSign = agreementSign;
	}
	public void setSoilWaterAnalyis(String soilWaterAnalyis) {
		this.soilWaterAnalyis = soilWaterAnalyis;
	}
	public String getVehicalNo() {
		return vehicalNo;
	}
	public void setVehicalNo(String vehicalNo) {
		this.vehicalNo = vehicalNo;
	}
	public Integer getSeedlingQty() {
		return seedlingQty;
	}
	public void setSeedlingQty(Integer seedlingQty) {
		this.seedlingQty = seedlingQty;
	}
	
	
	public Integer getDispatchedStatus() {
		return dispatchedStatus;
	}
	public void setDispatchedStatus(Integer dispatchedStatus) {
		this.dispatchedStatus = dispatchedStatus;
	}
	public String getRyotPhoneNumber() {
		return ryotPhoneNumber;
	}
	public void setRyotPhoneNumber(String ryotPhoneNumber) {
		this.ryotPhoneNumber = ryotPhoneNumber;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	
	public Integer getDispatchSeq() {
		return dispatchSeq;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public Integer getfACode() {
		return fACode;
	}
	public Integer getfOCode() {
		return fOCode;
	}
	public String getDriverName() {
		return driverName;
	}
	public String getDrPhoneNo() {
		return drPhoneNo;
	}
	public String getDispatchBy() {
		return dispatchBy;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public Date getDateOfDispatch() {
		return dateOfDispatch;
	}
	public void setDateOfDispatch(Date dateOfDispatch) {
		this.dateOfDispatch = dateOfDispatch;
	}
	public void setDispatchSeq(Integer dispatchSeq) {
		this.dispatchSeq = dispatchSeq;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setfACode(Integer fACode) {
		this.fACode = fACode;
	}
	public void setfOCode(Integer fOCode) {
		this.fOCode = fOCode;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public void setDrPhoneNo(String drPhoneNo) {
		this.drPhoneNo = drPhoneNo;
	}
	public void setDispatchBy(String dispatchBy) {
		this.dispatchBy = dispatchBy;
	}
	public Integer getTrasportcontractor() {
		return trasportcontractor;
	}
	public void setTrasportcontractor(Integer trasportcontractor) {
		this.trasportcontractor = trasportcontractor;
	}
	
	
	
	
}
