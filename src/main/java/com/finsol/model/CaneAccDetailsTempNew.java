package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Naidu
 */
@Entity
public class CaneAccDetailsTempNew 
{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String ryotcode;
	
	@Column
    private String ryotname;
	
	@Column
    private double suppliedqty;
	
	@Column
    private double totalamt;
	
	
	@Column
    private double cdc;
	
	@Column
    private double ul;
	
	@Column
    private double tp;
	
	@Column
    private double harvest;
	
	@Column
    private double otherded;
	
	@Column
    private double loan;
	
	@Column
    private double sb;
	
	@Column
    private double intrest;
	
	@Column
    private double advances;
	
	@Column
    private double principleloan;
	
	@Column
    private double principleadvances;

	public double getOtherded() {
		return otherded;
	}

	public void setOtherded(double otherded) {
		this.otherded = otherded;
	}

	

	public double getIntrest() {
		return intrest;
	}

	public void setIntrest(double intrest) {
		this.intrest = intrest;
	}

	public double getAdvances() {
		return advances;
	}

	public void setAdvances(double advances) {
		this.advances = advances;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}


	public double getSuppliedqty() {
		return suppliedqty;
	}

	public void setSuppliedqty(double suppliedqty) {
		this.suppliedqty = suppliedqty;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public double getCdc() {
		return cdc;
	}

	public void setCdc(double cdc) {
		this.cdc = cdc;
	}

	public double getUl() {
		return ul;
	}

	public void setUl(double ul) {
		this.ul = ul;
	}

	public double getTp() {
		return tp;
	}

	public void setTp(double tp) {
		this.tp = tp;
	}

	public double getHarvest() {
		return harvest;
	}

	public void setHarvest(double harvest) {
		this.harvest = harvest;
	}

	public double getLoan() {
		return loan;
	}

	public void setLoan(double loan) {
		this.loan = loan;
	}

	public double getSb() {
		return sb;
	}

	public void setSb(double sb) {
		this.sb = sb;
	}

	
	public double getPrincipleloan() {
		return principleloan;
	}

	public void setPrincipleloan(double principleloan) {
		this.principleloan = principleloan;
	}

	public double getPrincipleadvances() {
		return principleadvances;
	}

	public void setPrincipleadvances(double principleadvances) {
		this.principleadvances = principleadvances;
	}
	
}
