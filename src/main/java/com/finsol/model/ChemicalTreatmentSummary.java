package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class ChemicalTreatmentSummary 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date treatmentdate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Byte labourSpplr;
	@Column(nullable = true)
	private Double noOfMen;
	@Column(nullable = true)
	private Double manCost;
	@Column(nullable = true)
	private Double menTotal;
	@Column(nullable = true)
	private Double noOfWomen;
	@Column(nullable = true)
	private Double womanCost;
	@Column(nullable = true)
	private Double womenTotal;
	@Column(nullable = true)
	private Double totalLabourCost;
	@Column(nullable = true)
	private Double costPerTon;
	@Column(nullable = true)
	private Double totalTons;
	@Column(nullable = true)
	private Double totalContractAmt;
	@Column(nullable = true)
	private Double totalNoOfBuds;
	@Column(nullable = true)
	private Double budGrandTotal;
	@Column
	private Integer lv;
	
	
	public Integer getLv() {
		return lv;
	}
	public void setLv(Integer lv) {
		this.lv = lv;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	
	public Date getTreatmentdate() {
		return treatmentdate;
	}
	public void setTreatmentdate(Date treatmentdate) {
		this.treatmentdate = treatmentdate;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public Double getWomanCost() {
		return womanCost;
	}
	public void setWomanCost(Double womanCost) {
		this.womanCost = womanCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public Double getTotalNoOfBuds() {
		return totalNoOfBuds;
	}
	public void setTotalNoOfBuds(Double totalNoOfBuds) {
		this.totalNoOfBuds = totalNoOfBuds;
	}
	public Double getBudGrandTotal() {
		return budGrandTotal;
	}
	public void setBudGrandTotal(Double budGrandTotal) {
		this.budGrandTotal = budGrandTotal;
	}
	
	
	
	
	
	
	
}
