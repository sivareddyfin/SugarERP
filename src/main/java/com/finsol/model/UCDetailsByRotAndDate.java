package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class UCDetailsByRotAndDate {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	@Type(type="date")
	private Date receiptdate;
	@Column
	private  String ryotcode; 
	@Column
	private Double suppliedcanewt;
	@Column
	private Double  udcrate;
	@Column
	private Double  udcamount;
	@Column
	private Byte castatus;
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getUdcrate() {
		return udcrate;
	}
	public void setUdcrate(Double udcrate) {
		this.udcrate = udcrate;
	}
	public Double getUdcamount() {
		return udcamount;
	}
	public void setUdcamount(Double udcamount) {
		this.udcamount = udcamount;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Byte getCastatus() {
		return castatus;
	}
	public void setCastatus(Byte castatus) {
		this.castatus = castatus;
	}
	
}
