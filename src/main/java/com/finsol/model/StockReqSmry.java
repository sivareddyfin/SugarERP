package com.finsol.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class StockReqSmry    
{
	@Id
	@Column(nullable = false)
	private String stockreqno;
	@Column
    private String finyr;
	@Column
	@Type(type="date")
	private Date reqdate;
	@Column
    private Timestamp reqtime;
	@Column
    private Integer seqno;
	@Column
    private Integer deptid;
	@Column
    private String reqlogninid;
	@Column
    private Integer status;
	@Column(nullable = true)
	@Type(type="date")
	private Date authoriseddt;
	@Column(nullable = true)
    private Timestamp authorisedtime;
	@Column(nullable = true)
    private String authoriseduser;
	public String getStockreqno() {
		return stockreqno;
	}
	public void setStockreqno(String stockreqno) {
		this.stockreqno = stockreqno;
	}
	public String getFinyr() {
		return finyr;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public Date getReqdate() {
		return reqdate;
	}
	public void setReqdate(Date reqdate) {
		this.reqdate = reqdate;
	}
	public Timestamp getReqtime() {
		return reqtime;
	}
	public void setReqtime(Timestamp reqtime) {
		this.reqtime = reqtime;
	}
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public Integer getDeptid() {
		return deptid;
	}
	public void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}
	public String getReqlogninid() {
		return reqlogninid;
	}
	public void setReqlogninid(String reqlogninid) {
		this.reqlogninid = reqlogninid;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getAuthoriseddt() {
		return authoriseddt;
	}
	public void setAuthoriseddt(Date authoriseddt) {
		this.authoriseddt = authoriseddt;
	}
	public Timestamp getAuthorisedtime() {
		return authorisedtime;
	}
	public void setAuthorisedtime(Timestamp authorisedtime) {
		this.authorisedtime = authorisedtime;
	}
	public String getAuthoriseduser() {
		return authoriseduser;
	}
	public void setAuthoriseduser(String authoriseduser) {
		this.authoriseduser = authoriseduser;
	}
	
	
	      
	
	

}
