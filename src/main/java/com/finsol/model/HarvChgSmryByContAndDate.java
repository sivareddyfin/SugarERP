package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class HarvChgSmryByContAndDate {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	@Type(type="date")
	private Date receiptdate;
	@Column
	private  Integer harvestcontcode;
	@Column
	private  Double harvestedcanewt;
	@Column
	private  Double totalextent;
	@Column
	private  Double totalamount;
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}
	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}
	public Double getHarvestedcanewt() {
		return harvestedcanewt;
	}
	public void setHarvestedcanewt(Double harvestedcanewt) {
		this.harvestedcanewt = harvestedcanewt;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	
	


}
