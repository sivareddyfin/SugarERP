package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class StockMethods
{
	@Id
	@Column
    private String stockMethodCode  ;
	
	@Column
    private String stockMethod  ;
		
	@Column(nullable = true)
    private String description;
	
    @Column
    private Byte status;

    
    
	public String getStockMethodCode() {
		return stockMethodCode;
	}

	public String getStockMethod() {
		return stockMethod;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStockMethodCode(String stockMethodCode) {
		this.stockMethodCode = stockMethodCode;
	}

	public void setStockMethod(String stockMethod) {
		this.stockMethod = stockMethod;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}


}
