package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class TransportingContractors {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private Integer transportercode;
	
	@Column
	private String transporter;
	
	@Column
	private String address;
	
	@Column
	private String contactnumber;
	
	@Column
	private String ifsccode;
	
	@Column
	private Integer bankcode;
	
	@Column
	private Integer branchcode;
	
	@Column
	private String accountnumber;
	
	@Column
	private String aadhaarnumber;
	
	@Column
	private String panno;	
	
	@Column
	private Byte status;
	@Column
	private String prefix;
	@Column
	private String accountcode;
	
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getAccountcode() {
		return accountcode;
	}
	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTransportercode() {
		return transportercode;
	}
	public void setTransportercode(Integer transportercode) {
		this.transportercode = transportercode;
	}
	public String getTransporter() {
		return transporter;
	}
	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getIfsccode() {
		return ifsccode;
	}
	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}
	public Integer getBankcode() {
		return bankcode;
	}
	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public String getAadhaarnumber() {
		return aadhaarnumber;
	}
	public void setAadhaarnumber(String aadhaarnumber) {
		this.aadhaarnumber = aadhaarnumber;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
}
