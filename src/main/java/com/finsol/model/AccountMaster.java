package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class AccountMaster {	
	
	@Id
	@Column
	private String accountcode;
	
	@Column
	private String accountname;
	
	@Column
	private Integer accounttype;
	
	@Column
	private Integer accountgroupcode;
	
	@Column
	private Integer accountsubgroupcode;

	public String getAccountcode() {
		return accountcode;
	}

	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}

	public String getAccountname() {
		return accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	public Integer getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(Integer accounttype) {
		this.accounttype = accounttype;
	}

	public Integer getAccountgroupcode() {
		return accountgroupcode;
	}

	public void setAccountgroupcode(Integer accountgroupcode) {
		this.accountgroupcode = accountgroupcode;
	}

	public Integer getAccountsubgroupcode() {
		return accountsubgroupcode;
	}

	public void setAccountsubgroupcode(Integer accountsubgroupcode) {
		this.accountsubgroupcode = accountsubgroupcode;
	}
	
	
	

}
