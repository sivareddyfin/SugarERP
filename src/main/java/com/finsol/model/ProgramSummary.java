package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class ProgramSummary
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column
	private String season;
	
	@Column
	private Integer programnumber;
	
	@Column(nullable = false)
	private Integer periodid;
	
	@Column(nullable = false)
	private Integer monthid;
	
	@Column(nullable = false)
	private Integer yearofplanting;
	
	@Column(nullable = false)
	private String typeofextent;
	
	@Column(nullable = false)
	private Integer varietyofcane;
	
	@Column(nullable = false)
	private Double totalextent;
	
	@Column
	private Double totalplant;
	
	@Column
	private Double totalratoon;


	public Integer getPeriodid() {
		return periodid;
	}

	public void setPeriodid(Integer periodid) {
		this.periodid = periodid;
	}

	public Integer getMonthid() {
		return monthid;
	}

	public void setMonthid(Integer monthid) {
		this.monthid = monthid;
	}

	public Integer getYearofplanting() {
		return yearofplanting;
	}

	public void setYearofplanting(Integer yearofplanting) {
		this.yearofplanting = yearofplanting;
	}

	public String getTypeofextent() {
		return typeofextent;
	}

	public void setTypeofextent(String typeofextent) {
		this.typeofextent = typeofextent;
	}

	public Integer getVarietyofcane() {
		return varietyofcane;
	}

	public void setVarietyofcane(Integer varietyofcane) {
		this.varietyofcane = varietyofcane;
	}

	public Double getTotalextent() {
		return totalextent;
	}

	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}

	public Double getTotalplant() {
		return totalplant;
	}

	public void setTotalplant(Double totalplant) {
		this.totalplant = totalplant;
	}

	public Double getTotalratoon() {
		return totalratoon;
	}

	public void setTotalratoon(Double totalratoon) {
		this.totalratoon = totalratoon;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Integer getProgramnumber() {
		return programnumber;
	}

	public void setProgramnumber(Integer programnumber) {
		this.programnumber = programnumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	
	

}
