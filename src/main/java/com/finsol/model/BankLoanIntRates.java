package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class BankLoanIntRates {
	
	private static final long serialVersionUID = 1L;
	

	@Column
	private Integer id;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column
	private String season;
	
	@Column
	private Integer branchcode;
	
	@Column
	private Double fromamount;
	
	@Column
	private Double toamount;
	
	@Column
	private Double interestrate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Integer getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}

	public Double getFromamount() {
		return fromamount;
	}

	public void setFromamount(Double fromamount) {
		this.fromamount = fromamount;
	}

	public Double getToamount() {
		return toamount;
	}

	public void setToamount(Double toamount) {
		this.toamount = toamount;
	}

	public Double getInterestrate() {
		return interestrate;
	}

	public void setInterestrate(Double interestrate) {
		this.interestrate = interestrate;
	}
	
	

}
