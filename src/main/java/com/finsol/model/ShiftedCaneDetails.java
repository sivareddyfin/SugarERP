package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class ShiftedCaneDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column
	private Integer id;
	@Column
	private String season;
	@Column(nullable = true)
	@Type(type="date")
    private Date shiftedCaneDate;
	@Column
	private Integer shiftedCaneShift;
	@Column
	private String batchSeries;
	@Column
	private Integer variety;
	@Column
	private String seedSupr;
	@Column
	private String landVillageCode;
	@Column
	private Double caneWt;
	
	public Integer getSlNo()
	{
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getShiftedCaneDate() {
		return shiftedCaneDate;
	}
	public void setShiftedCaneDate(Date shiftedCaneDate) {
		this.shiftedCaneDate = shiftedCaneDate;
	}
	public Integer getShiftedCaneShift() {
		return shiftedCaneShift;
	}
	public void setShiftedCaneShift(Integer shiftedCaneShift) {
		this.shiftedCaneShift = shiftedCaneShift;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public Integer getVariety() {
		return variety;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getCaneWt() {
		return caneWt;
	}
	public void setCaneWt(Double caneWt) {
		this.caneWt = caneWt;
	}
	
	
}
