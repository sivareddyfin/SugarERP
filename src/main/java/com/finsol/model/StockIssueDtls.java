package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class StockIssueDtls 
{	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
    private String stockissueno;
	@Column
    private String finyr;
	@Column
    private String productcode;
	@Column
    private String productgrpcode;
	@Column
    private String productsubgroupcode;
	@Column
    private String uom;
	@Column
    private Double quantity;
	@Column
    private Double issuedqty;
	@Column
    private Double pendingqty;
	@Column
    private String batchid;
	@Column
    private Double mrp;
	@Column
    private Integer status;
	@Column
    private Double batchQty;
	
	
	public Double getBatchQty() {
		return batchQty;
	}
	public void setBatchQty(Double batchQty) {
		this.batchQty = batchQty;
	}
	public Integer getId() {
		return id;
	}
	public String getStockissueno() {
		return stockissueno;
	}
	public String getFinyr() {
		return finyr;
	}
	public String getProductcode() {
		return productcode;
	}
	public String getProductgrpcode() {
		return productgrpcode;
	}
	public String getProductsubgroupcode() {
		return productsubgroupcode;
	}
	public String getUom() {
		return uom;
	}
	public Double getQuantity() {
		return quantity;
	}
	public Double getIssuedqty() {
		return issuedqty;
	}
	public Double getPendingqty() {
		return pendingqty;
	}
	public String getBatchid() {
		return batchid;
	}
	public Double getMrp() {
		return mrp;
	}
	public Integer getStatus() {
		return status;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setStockissueno(String stockissueno) {
		this.stockissueno = stockissueno;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public void setProductgrpcode(String productgrpcode) {
		this.productgrpcode = productgrpcode;
	}
	public void setProductsubgroupcode(String productsubgroupcode) {
		this.productsubgroupcode = productsubgroupcode;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public void setIssuedqty(Double issuedqty) {
		this.issuedqty = issuedqty;
	}
	public void setPendingqty(Double pendingqty) {
		this.pendingqty = pendingqty;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
}
