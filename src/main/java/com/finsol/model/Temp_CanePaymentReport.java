package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_CanePaymentReport
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column 
	private Integer circlecode;  
	@Column 
	private String ryotcode;  
	@Column 
	private String ryotname;
	@Column 
	private String village;
	@Column 
	private String relativename;
	@Column 
	private Double totalamount;
	@Column 
	private Double suppqty;
	@Column 
	private Double value;
	
	@Column 
	private Double otherded;
	@Column 
	private Double payamount; 
	@Column						
	private Double paid;
	@Column                       
	private Double cdcamt;
	@Column
	private Double ulamt;
	@Column
	private Double transport;
	@Column
	private Double harvesting;
	@Column 
	private String gtotalwords;
	@Column 
	private String betweendate;
	@Column
	private Double gnetwt;
	@Column
	private Double loantotal;
	@Column
	private Double biototal;
	@Column
	private Double seedtotal;
	@Column
	private Double harvtotal;
	@Column
	private Double advisedamt;
	@Column 
	private Integer bankcode;  
	@Column 
	private String sbaccno;  
	@Column
	private Double seedoutstanding;
	@Column
	private Double biooutstanding;
	@Column
	private Double harvestoutstanding;
	@Column
	private Double loanoutstanding;
	
	
	@Column
	private Double otheradvances;
	

	public Double getOtheradvances() {
		return otheradvances;
	}
	public void setOtheradvances(Double otheradvances) {
		this.otheradvances = otheradvances;
	}
	public Double getSeedtotal() {
		return seedtotal;
	}
	public void setSeedtotal(Double seedtotal) {
		this.seedtotal = seedtotal;
	}
	public Integer getBankcode() {
		return bankcode;
	}
	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}
	public String getSbaccno() {
		return sbaccno;
	}
	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}
	public Double getAdvisedamt() {
		return advisedamt;
	}
	public void setAdvisedamt(Double advisedamt) {
		this.advisedamt = advisedamt;
	}
	public Double getLoantotal() {
		return loantotal;
	}
	public void setLoantotal(Double loantotal) {
		this.loantotal = loantotal;
	}
	public Double getBiototal() {
		return biototal;
	}
	public void setBiototal(Double biototal) {
		this.biototal = biototal;
	}
	public Double getHarvtotal() {
		return harvtotal;
	}
	public void setHarvtotal(Double harvtotal) {
		this.harvtotal = harvtotal;
	}
	public Double getGnetwt() {
		return gnetwt;
	}
	public void setGnetwt(Double gnetwt) {
		this.gnetwt = gnetwt;
	}
	public String getGtotalwords() {
		return gtotalwords;
	}
	public void setGtotalwords(String gtotalwords) {
		this.gtotalwords = gtotalwords;
	}
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getSuppqty() {
		return suppqty;
	}
	public void setSuppqty(Double suppqty) {
		this.suppqty = suppqty;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getPayamount() {
		return payamount;
	}
	public void setPayamount(Double payamount) {
		this.payamount = payamount;
	}
	
	public Double getPaid() {
		return paid;
	}
	public void setPaid(Double paid) {
		this.paid = paid;
	}
	public Double getCdcamt() {
		return cdcamt;
	}
	public void setCdcamt(Double cdcamt) {
		this.cdcamt = cdcamt;
	}
	public Double getUlamt() {
		return ulamt;
	}
	public void setUlamt(Double ulamt) {
		this.ulamt = ulamt;
	}
	public Double getTransport() {
		return transport;
	}
	public void setTransport(Double transport) {
		this.transport = transport;
	}
	public Double getHarvesting() {
		return harvesting;
	}
	public void setHarvesting(Double harvesting) {
		this.harvesting = harvesting;
	}
	public Double getSeedoutstanding() {
		return seedoutstanding;
	}
	public void setSeedoutstanding(Double seedoutstanding) {
		this.seedoutstanding = seedoutstanding;
	}
	public Double getBiooutstanding() {
		return biooutstanding;
	}
	public void setBiooutstanding(Double biooutstanding) {
		this.biooutstanding = biooutstanding;
	}
	public Double getHarvestoutstanding() {
		return harvestoutstanding;
	}
	public void setHarvestoutstanding(Double harvestoutstanding) {
		this.harvestoutstanding = harvestoutstanding;
	}
	public Double getLoanoutstanding() {
		return loanoutstanding;
	}
	public void setLoanoutstanding(Double loanoutstanding) {
		this.loanoutstanding = loanoutstanding;
	}
	
	
}
