package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SeedlingTray {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;	
	
	@Column(unique = true,nullable = false)
	private Integer traycode;
	
	@Column
	private String tray;
	
	@Column
	private Integer lifeoftray;
	
	@Column
	private Integer noofseedspertray;
	
	@Column
	private String description;
	
	@Column
	private Byte traytype;
	
	@Column
	private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTraycode() {
		return traycode;
	}

	public void setTraycode(Integer traycode) {
		this.traycode = traycode;
	}

	public String getTray() {
		return tray;
	}

	public void setTray(String tray) {
		this.tray = tray;
	}

	public Integer getLifeoftray() {
		return lifeoftray;
	}

	public void setLifeoftray(Integer lifeoftray) {
		this.lifeoftray = lifeoftray;
	}

	public Integer getNoofseedspertray() {
		return noofseedspertray;
	}

	public void setNoofseedspertray(Integer noofseedspertray) {
		this.noofseedspertray = noofseedspertray;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getTraytype() {
		return traytype;
	}

	public void setTraytype(Byte traytype) {
		this.traytype = traytype;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
}
