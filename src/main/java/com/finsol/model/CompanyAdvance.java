package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author DMurty
 */
@Entity
public class CompanyAdvance 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String advance;
	
	@Column(nullable = true)
    private String description;
	
	@Column(nullable = false)
    private Integer advancecode;
	
	@Column(nullable = false)
    private Double maximumamount;
	
	@Column(nullable = false)
    private Byte advancemode;
	
	@Column(nullable = false)
    private Byte subsidypart;
	
	@Column(nullable = false)
    private Byte isamount;
	
	@Column
    private Double amount;
	
	@Column
    private Double advancepercent;	//previously it is percent.After migration of sql server changed to advancepercent
	
	@Column(nullable = false)
    private Integer advancefor;
	
	@Column(nullable = false)
    private Byte isseedlingadvance;
	
	@Column
    private Double seedlingcost;

	@Column(nullable = false)
    private Byte status;
	
	@Column(nullable = false)
    private Byte interestapplicable;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date calculationdate;
	
	@Column(nullable = false)
	private String glCode;
	@Column
	private Integer advorder;
	

	public Integer getAdvorder() {
		return advorder;
	}

	public void setAdvorder(Integer advorder) {
		this.advorder = advorder;
	}

	public String getGlCode()
	{
		return glCode;
	}

	public void setGlCode(String glCode)
	{
		this.glCode = glCode;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getAdvance()
	{
		return advance;
	}

	public void setAdvance(String advance)
	{
		this.advance = advance;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public Integer getAdvancecode()
	{
		return advancecode;
	}

	public void setAdvancecode(Integer advancecode)
	{
		this.advancecode = advancecode;
	}

	public Double getMaximumamount()
	{
		return maximumamount;
	}

	public void setMaximumamount(Double maximumamount)
	{
		this.maximumamount = maximumamount;
	}

	public Byte getAdvancemode() 
	{
		return advancemode;
	}

	public void setAdvancemode(Byte advancemode) 
	{
		this.advancemode = advancemode;
	}

	public Byte getSubsidypart() 
	{
		return subsidypart;
	}

	public void setSubsidypart(Byte subsidypart)
	{
		this.subsidypart = subsidypart;
	}

	public Byte getIsamount() 
	{
		return isamount;
	}

	public void setIsamount(Byte isamount)
	{
		this.isamount = isamount;
	}

	public Double getAmount() 
	{
		return amount;
	}

	public void setAmount(Double amount) 
	{
		this.amount = amount;
	}

	
	public Double getAdvancepercent() {
		return advancepercent;
	}

	public void setAdvancepercent(Double advancepercent) {
		this.advancepercent = advancepercent;
	}

	public Integer getAdvancefor()
	{
		return advancefor;
	}

	public void setAdvancefor(Integer advancefor)
	{
		this.advancefor = advancefor;
	}

	public Byte getIsseedlingadvance() {
		return isseedlingadvance;
	}

	public void setIsseedlingadvance(Byte isseedlingadvance) 
	{
		this.isseedlingadvance = isseedlingadvance;
	}

	public Double getSeedlingcost()
	{
		return seedlingcost;
	}

	public void setSeedlingcost(Double seedlingcost)
	{
		this.seedlingcost = seedlingcost;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status)
	{
		this.status = status;
	}

	public Byte getInterestapplicable() 
	{
		return interestapplicable;
	}

	public void setInterestapplicable(Byte interestapplicable)
	{
		this.interestapplicable = interestapplicable;
	}

	public Date getCalculationdate()
	{
		return calculationdate;
	}

	public void setCalculationdate(Date calculationdate) 
	{
		this.calculationdate = calculationdate;
	}
}
