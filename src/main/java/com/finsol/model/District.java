package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class District {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;	
	
	@Column(unique = true,nullable = false)
	private Integer districtcode;
	
	@Column(unique = true,nullable = false)
	private String district;
	
	@Column
	private Integer statecode;
	@Column
	private String description;
	
	@Column
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDistrictcode() {
		return districtcode;
	}

	public void setDistrictcode(Integer districtcode) {
		this.districtcode = districtcode;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Integer getStatecode() {
		return statecode;
	}

	public void setStatecode(Integer statecode) {
		this.statecode = statecode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
