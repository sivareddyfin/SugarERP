package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
/**
 * @author admin
 *
 */
@Entity
public class SeedSource
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
	@Type(type="date")
    private Date sourcesupplieddate;
	
	@Column(nullable = false)
    private Integer seedtype;
	
	@Column(nullable = false)
    private Integer circlecode;
	
	@Column(nullable = false)
    private Integer purpose;
	
	@Column(nullable = true)
    private String consumercode;
	
	@Column(nullable = false)
    private String seedsrcode;
	
	@Column(nullable = true)
    private String otherryot;
	
	@Column(nullable = false)
    private Integer varietycode;
	
	@Column(nullable = false)
    private String villagecode;
	
	@Column(nullable = false)
    private String batchno;
	
	@Column(nullable = false)
    private Double caneweight;
	
	@Column(nullable = false)
    private Double lorryweight;
	
	@Column(nullable = false)
    private Double netweight;
	
	@Column(nullable = false)
    private Double bm;
	
	@Column(nullable = false)
    private Double finalweight;
	
	@Column(nullable = false)
    private Double seedcostperton;
	
	@Column(nullable = false)
    private Double totalcost;
	
	@Column(nullable = false)
    private Double ageofcrop;
	
	@Column(nullable = false)
    private Double avgbudspercane;
	
	@Column(nullable = true)
    private String physicalcondition;
	
	@Column(nullable = true)
    private String seedcertifiedby;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date certdate;
	
	@Column(nullable = true)
    private String filelocation;
	
	@Column(nullable = true)
    private Double harvestingcharges;
	
	@Column(nullable = true)
    private Double transcharges;

	@Column(nullable = true)
    private String runningyear;
	
	@Column(nullable = true)
	private double noofacre;
	
	@Column(nullable = true)
	private double costperacre;
	
	@Column(nullable = true)
	private Byte acreage;
	
	@Column(nullable = true)
	private Byte updateType;
	@Column(nullable = true)
	private String otherRyotVillage;
	@Column(nullable = true)
	private String vehicleNo;
	@Column(nullable = true)
	//added on 24-2-2017
	private String aggrementNo;
	@Column
	private Integer transactioncode;
	@Column
	private Integer suptransactioncode;

	public String getAggrementNo() {
		return aggrementNo;
	}

	public void setAggrementNo(String aggrementNo) {
		this.aggrementNo = aggrementNo;
	}

	public String getOtherRyotVillage() {
		return otherRyotVillage;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setOtherRyotVillage(String otherRyotVillage) {
		this.otherRyotVillage = otherRyotVillage;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public double getNoofacre() {
		return noofacre;
	}

	public double getCostperacre() {
		return costperacre;
	}

	public Byte getAcreage() {
		return acreage;
	}

	public Byte getUpdateType() {
		return updateType;
	}

	public void setNoofacre(double noofacre) {
		this.noofacre = noofacre;
	}

	public void setCostperacre(double costperacre) {
		this.costperacre = costperacre;
	}

	public void setAcreage(Byte acreage) {
		this.acreage = acreage;
	}

	public void setUpdateType(Byte updateType) {
		this.updateType = updateType;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Date getSourcesupplieddate() {
		return sourcesupplieddate;
	}

	public void setSourcesupplieddate(Date sourcesupplieddate) {
		this.sourcesupplieddate = sourcesupplieddate;
	}

	public Integer getSeedtype() {
		return seedtype;
	}

	public void setSeedtype(Integer seedtype) {
		this.seedtype = seedtype;
	}

	public Integer getCirclecode() {
		return circlecode;
	}

	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}

	public Integer getPurpose() {
		return purpose;
	}

	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}

	public String getConsumercode() {
		return consumercode;
	}

	public void setConsumercode(String consumercode) {
		this.consumercode = consumercode;
	}

	public String getSeedsrcode() {
		return seedsrcode;
	}

	public void setSeedsrcode(String seedsrcode) {
		this.seedsrcode = seedsrcode;
	}

	public String getOtherryot() {
		return otherryot;
	}

	public void setOtherryot(String otherryot) {
		this.otherryot = otherryot;
	}



	public Integer getVarietycode() {
		return varietycode;
	}

	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}

	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public String getBatchno() {
		return batchno;
	}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}

	public Double getCaneweight() {
		return caneweight;
	}

	public void setCaneweight(Double caneweight) {
		this.caneweight = caneweight;
	}

	public Double getLorryweight() {
		return lorryweight;
	}

	public void setLorryweight(Double lorryweight) {
		this.lorryweight = lorryweight;
	}

	public Double getNetweight() {
		return netweight;
	}

	public void setNetweight(Double netweight) {
		this.netweight = netweight;
	}

	public Double getBm() {
		return bm;
	}

	public void setBm(Double bm) {
		this.bm = bm;
	}

	public Double getFinalweight() {
		return finalweight;
	}

	public void setFinalweight(Double finalweight) {
		this.finalweight = finalweight;
	}

	public Double getSeedcostperton() {
		return seedcostperton;
	}

	public void setSeedcostperton(Double seedcostperton) {
		this.seedcostperton = seedcostperton;
	}

	public Double getTotalcost() {
		return totalcost;
	}

	public void setTotalcost(Double totalcost) {
		this.totalcost = totalcost;
	}

	public Double getAgeofcrop() {
		return ageofcrop;
	}

	public void setAgeofcrop(Double ageofcrop) {
		this.ageofcrop = ageofcrop;
	}

	public Double getAvgbudspercane() {
		return avgbudspercane;
	}

	public void setAvgbudspercane(Double avgbudspercane) {
		this.avgbudspercane = avgbudspercane;
	}

	public String getPhysicalcondition() {
		return physicalcondition;
	}

	public void setPhysicalcondition(String physicalcondition) {
		this.physicalcondition = physicalcondition;
	}

	public String getSeedcertifiedby() {
		return seedcertifiedby;
	}

	public void setSeedcertifiedby(String seedcertifiedby) {
		this.seedcertifiedby = seedcertifiedby;
	}

	public Date getCertdate() {
		return certdate;
	}

	public void setCertdate(Date certdate) {
		this.certdate = certdate;
	}

	public String getFilelocation() {
		return filelocation;
	}

	public void setFilelocation(String filelocation) {
		this.filelocation = filelocation;
	}

	public Double getHarvestingcharges() {
		return harvestingcharges;
	}

	public void setHarvestingcharges(Double harvestingcharges) {
		this.harvestingcharges = harvestingcharges;
	}

	public Double getTranscharges() {
		return transcharges;
	}

	public void setTranscharges(Double transcharges) {
		this.transcharges = transcharges;
	}

	public String getRunningyear() {
		return runningyear;
	}

	public void setRunningyear(String runningyear) {
		this.runningyear = runningyear;
	}

	public Integer getTransactioncode() {
		return transactioncode;
	}

	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}

	public Integer getSuptransactioncode() {
		return suptransactioncode;
	}

	public void setSuptransactioncode(Integer suptransactioncode) {
		this.suptransactioncode = suptransactioncode;
	}
		
}
