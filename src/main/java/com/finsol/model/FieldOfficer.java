package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class FieldOfficer {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
    private Integer fieldOfficerid;
	
	@Column
    private String fieldOfficer;
	
	@Column
    private Integer caneManagerId;
	
	@Column
    private Integer employeeId;	
    
    @Column
    private String description;
    
    @Column
    private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFieldOfficerid() {
		return fieldOfficerid;
	}

	public void setFieldOfficerid(Integer fieldOfficerid) {
		this.fieldOfficerid = fieldOfficerid;
	}

	public String getFieldOfficer() {
		return fieldOfficer;
	}

	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}

	public Integer getCaneManagerId() {
		return caneManagerId;
	}

	public void setCaneManagerId(Integer caneManagerId) {
		this.caneManagerId = caneManagerId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
    
    

}
