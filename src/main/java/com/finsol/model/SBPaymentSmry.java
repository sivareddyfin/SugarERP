package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author DMurty
 *
 */
@Entity
public class SBPaymentSmry
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private Integer caneacslno;
	
	@Column
	private String ryotcode;
	
	@Column
	private String season;
	
	@Column
	private String villagecode;
	
	@Column
	private Integer bankcode;
	
	@Column
	private String sbaccno;
	
	@Column
	private Double paidamount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCaneacslno() {
		return caneacslno;
	}

	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public Integer getBankcode() {
		return bankcode;
	}

	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}

	public String getSbaccno() {
		return sbaccno;
	}

	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}

	public Double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
}
