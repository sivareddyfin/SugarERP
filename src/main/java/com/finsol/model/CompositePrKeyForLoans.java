package com.finsol.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CompositePrKeyForLoans implements Serializable
{
	@Column (nullable = false)
	private Integer loannumber;
	       
	        
	@Column (nullable = false)
	private Integer branchcode;

	@Column
	private String Season;
	
	public String getSeason() {
		return Season;
	}


	public void setSeason(String season) {
		Season = season;
	}


	public Integer getLoannumber()
	{
		return loannumber;
	}


	public void setLoannumber(Integer loannumber)
	{
		this.loannumber = loannumber;
	}


	public Integer getBranchcode() 
	{
		return branchcode;
	}

	public void setBranchcode(Integer branchcode) 
	{
		this.branchcode = branchcode;
	}
}
