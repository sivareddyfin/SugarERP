package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 */
@Entity
public class  LabourContractor 
{
	

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
    private Integer lcCode;
	
	@Column(nullable = false)
    private String lcName;
	
	@Column(nullable = true)
    private String description	;

	@Column(nullable = false)
    private Byte status	;
	
	@Column(nullable = false)
    private String glcode;
	
	
	
	public Integer getId() {
		return id;
	}

	public Integer getLcCode() {
		return lcCode;
	}

	public String getLcName() {
		return lcName;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLcCode(Integer lcCode) {
		this.lcCode = lcCode;
	}

	public void setLcName(String lcName) {
		this.lcName = lcName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getGlcode() {
		return glcode;
	}

	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	
	


}
