package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class HarvChgDtlsByRyotContAndDate {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private  String season;
	@Column
	private  String ryotcode; 
	@Column
	@Type(type="date")
	private Date receiptdate;
	@Column
	private  Integer harvestcontcode;
	@Column
	private  Double totalextent;
	@Column
	private  Double harvestedcanewt;
	@Column
	private  Double rate;
	@Column
	private  Double totalamount;
	@Column
	private Byte castatus;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}
	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Double getHarvestedcanewt() {
		return harvestedcanewt;
	}
	public void setHarvestedcanewt(Double harvestedcanewt) {
		this.harvestedcanewt = harvestedcanewt;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Byte getCastatus() {
		return castatus;
	}
	public void setCastatus(Byte castatus) {
		this.castatus = castatus;
	}
	
	


}
