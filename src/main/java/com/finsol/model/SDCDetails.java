package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class SDCDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String finyr;
	
	@Column(nullable = false)
    private String sdcno;
	
	@Column(nullable = false)
	private String productcode;
	
	@Column(nullable = false)
	private String groupcode;
	
	@Column(nullable = false)
	private String subgroupcode;
	
	@Column(nullable = false)
	private Integer uom;
	
	@Column(nullable = false)
    private String batchid;
	
	@Column(nullable = false)
    private Double qty;
	
	@Column(nullable = false)
    private Double totalcost;
	
	@Column(nullable = false)
    private Double discountamount;
	
	@Column(nullable = false)
    private Double netamount;
	
	public Double getDiscountamount() {
		return discountamount;
	}

	public Double getNetamount() {
		return netamount;
	}

	public void setDiscountamount(Double discountamount) {
		this.discountamount = discountamount;
	}

	public void setNetamount(Double netamount) {
		this.netamount = netamount;
	}

	public Integer getUom() {
		return uom;
	}

	public void setUom(Integer uom) {
		this.uom = uom;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFinyr() {
		return finyr;
	}

	public String getSdcno() {
		return sdcno;
	}

	public String getProductcode() {
		return productcode;
	}

	public String getGroupcode() {
		return groupcode;
	}

	public String getSubgroupcode() {
		return subgroupcode;
	}


	public String getBatchid() {
		return batchid;
	}

	
	public Double getQty() {
		return qty;
	}

	public Double getTotalcost() {
		return totalcost;
	}

	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}

	public void setSdcno(String sdcno) {
		this.sdcno = sdcno;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public void setSubgroupcode(String subgroupcode) {
		this.subgroupcode = subgroupcode;
	}


	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}

	public void setTotalcost(Double totalcost) {
		this.totalcost = totalcost;
	}

}
