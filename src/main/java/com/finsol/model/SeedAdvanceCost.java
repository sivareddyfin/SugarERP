package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class SeedAdvanceCost
{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer slno;
	
	@Column(nullable = false)
	private Integer id;
	
	@Column(nullable = false)
    private Double costofeachunit;
	
	@Column(nullable = false)
    private Double fromdistance;
	
	@Column(nullable = false)
    private Double todistance;
	
	@Column(nullable = false)
    private Integer advancecode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getCostofeachunit() {
		return costofeachunit;
	}

	public void setCostofeachunit(Double costofeachunit) {
		this.costofeachunit = costofeachunit;
	}

	public Double getFromdistance() {
		return fromdistance;
	}

	public void setFromdistance(Double fromdistance) {
		this.fromdistance = fromdistance;
	}

	public Double getTodistance() {
		return todistance;
	}

	public void setTodistance(Double todistance) {
		this.todistance = todistance;
	}

	public Integer getAdvancecode() {
		return advancecode;
	}

	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}
}
