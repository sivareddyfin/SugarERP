package com.finsol.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class StockIssueSmry 
{
	@Id
	@Column
    private String stockissueno;
	@Column
    private String stockreqno;
	@Column
    private String finyr;
	@Column
	@Type(type="date")
	private Date issuedate;
	@Column
    private Timestamp issuetime;
	@Column
    private Integer seqno;
	
	@Column
    private String loginid;
	@Column
    private Integer deptid;
	@Column
    private Integer status;
	@Column
    private String grnno;
	@Column
	private int stockTransactionNo;
	
	

	public int getStockTransactionNo() {
		return stockTransactionNo;
	}
	public void setStockTransactionNo(int stockTransactionNo) {
		this.stockTransactionNo = stockTransactionNo;
	}
	public String getStockreqno() {
		return stockreqno;
	}
	public void setStockreqno(String stockreqno) {
		this.stockreqno = stockreqno;
	}
	public String getFinyr() {
		return finyr;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public Date getIssuedate() {
		return issuedate;
	}
	public void setIssuedate(Date issuedate) {
		this.issuedate = issuedate;
	}
	public Timestamp getIssuetime() {
		return issuetime;
	}
	public void setIssuetime(Timestamp issuetime) {
		this.issuetime = issuetime;
	}
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public String getStockissueno() {
		return stockissueno;
	}
	public void setStockissueno(String stockissueno) {
		this.stockissueno = stockissueno;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public Integer getDeptid() {
		return deptid;
	}
	public void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getGrnno() {
		return grnno;
	}
	public void setGrnno(String grnno) {
		this.grnno = grnno;
	}
	
	
	

}
