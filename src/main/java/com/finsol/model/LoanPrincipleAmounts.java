package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 *
 */
@Entity
public class LoanPrincipleAmounts 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column(nullable = false)
	private String ryotcode;
	@Column(nullable = false)
	private String loanaccountnumber;
	@Column(nullable = false)
	private Double principle;  //DisbursedAmunt  from loan details
	
	@Column(nullable = false)
	@Type(type="date")
	private Date principledate; // DataOfDisbursement  from loan details
	
	@Column(nullable = false)
	@Type(type="date")
	private Date dateofupdate;
	
	@Column(nullable = false)
	private Integer branchcode;

	@Column(nullable = false)
	private Integer loannumber;
	
	

	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getSeason() 
	{
		return season;
	}
	public void setSeason(String season) 
	{
		this.season = season;
	}
	public String getRyotcode()
	{
		return ryotcode;
	}
	public void setRyotcode(String ryotcode)
	{
		this.ryotcode = ryotcode;
	}
	public String getLoanaccountnumber()
	{
		return loanaccountnumber;
	}
	public void setLoanaccountnumber(String loanaccountnumber)
	{
		this.loanaccountnumber = loanaccountnumber;
	}
	public Double getPrinciple()
	{
		return principle;
	}
	public void setPrinciple(Double principle)
	{
		this.principle = principle;
	}
	public Date getPrincipledate() 
	{
		return principledate;
	}
	public void setPrincipledate(Date principledate) 
	{
		this.principledate = principledate;
	}
	public Date getDateofupdate()
	{
		return dateofupdate;
	}
	public void setDateofupdate(Date dateofupdate)
	{
		this.dateofupdate = dateofupdate;
	}
	
	
}
