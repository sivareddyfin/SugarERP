package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */
@Entity
public class Temp_PrintPermit 
{
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 
	 @Column
	 private Integer permitnumber;
	 @Column
	 private Integer circlecode;
	 @Column
	 private Integer programno;
	 @Column
	 private Integer rank;
	 @Column
	 private String shift;
	 @Column
	 private String admin;
	 @Column
	 private String zone;
	 @Column
	 private String circle;
	 @Column
	 private String ryotcode;
	 @Column
	 private String variety;
	 @Column
	 private String landvilcode;
	 @Column
	 private String vehicleregn;
	 @Column
	 private String vehicleno;
	 @Column
	 private String plantorratoon;
	 
	 
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPermitnumber() {
		return permitnumber;
	}
	public void setPermitnumber(Integer permitnumber) {
		this.permitnumber = permitnumber;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public Integer getProgramno() {
		return programno;
	}
	public void setProgramno(Integer programno) {
		this.programno = programno;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
	public String getAdmin() {
		return admin;
	}
	public void setAdmin(String admin) {
		this.admin = admin;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getLandvilcode() {
		return landvilcode;
	}
	public void setLandvilcode(String landvilcode) {
		this.landvilcode = landvilcode;
	}
	public String getVehicleregn() {
		return vehicleregn;
	}
	public void setVehicleregn(String vehicleregn) {
		this.vehicleregn = vehicleregn;
	}
	public String getVehicleno() {
		return vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	 
	 
	 

}
