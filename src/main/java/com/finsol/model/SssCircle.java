package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 *
 */
@Entity
public class SssCircle 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
    private Float CI_CODE;
	
	@Column
    private String CI_NAME;
	
	@Column
    private String CI_FA;
	
	
	@Column
    private Float CI_ZONE;
	
	@Column
    private Float CC_CODE;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getCI_CODE() {
		return CI_CODE;
	}

	public void setCI_CODE(Float ci_code) {
		CI_CODE = ci_code;
	}

	public String getCI_NAME() {
		return CI_NAME;
	}

	public void setCI_NAME(String ci_name) {
		CI_NAME = ci_name;
	}

	public String getCI_FA() {
		return CI_FA;
	}

	public void setCI_FA(String ci_fa) {
		CI_FA = ci_fa;
	}

	public Float getCI_ZONE() {
		return CI_ZONE;
	}

	public void setCI_ZONE(Float ci_zone) {
		CI_ZONE = ci_zone;
	}

	public Float getCC_CODE() {
		return CC_CODE;
	}

	public void setCC_CODE(Float cc_code) {
		CC_CODE = cc_code;
	}
	
}
