package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneValueSplitupTemp {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer caneacslno;
	@Column
	private Byte  cancelproportion;
	@Column
	private Integer slno;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private Double canesupplied;
	@Column
	private Double rate;
	@Column
	private Double totalvalue;
	@Column
	private Double forloans;
	@Column
	private Double forsbacs;
	@Column
	private String login;
	@Column
	private Double cumcanesupplied;
	@Column
	private byte acctype;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public Byte getCancelproportion() {
		return cancelproportion;
	}
	public void setCancelproportion(Byte cancelproportion) {
		this.cancelproportion = cancelproportion;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getCanesupplied() {
		return canesupplied;
	}
	public void setCanesupplied(Double canesupplied) {
		this.canesupplied = canesupplied;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getForloans() {
		return forloans;
	}
	public void setForloans(Double forloans) {
		this.forloans = forloans;
	}
	public Double getForsbacs() {
		return forsbacs;
	}
	public void setForsbacs(Double forsbacs) {
		this.forsbacs = forsbacs;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Double getCumcanesupplied() {
		return cumcanesupplied;
	}
	public void setCumcanesupplied(Double cumcanesupplied) {
		this.cumcanesupplied = cumcanesupplied;
	}
	public byte getAcctype() {
		return acctype;
	}
	public void setAcctype(byte acctype) {
		this.acctype = acctype;
	}		

}
