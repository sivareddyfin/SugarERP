package com.finsol.model;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Murty Ayyagari
 */

@Entity
public class InvoiceType 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private byte invoicetypecode;
	
	@Column(nullable = false)
	private String invoicetype;
	
	@Column(nullable = false)
	private byte transactiontype;

	public Integer getId() {
		return id;
	}

	public byte getInvoicetypecode() {
		return invoicetypecode;
	}

	public String getInvoicetype() {
		return invoicetype;
	}

	public byte getTransactiontype() {
		return transactiontype;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setInvoicetypecode(byte invoicetypecode) {
		this.invoicetypecode = invoicetypecode;
	}

	public void setInvoicetype(String invoicetype) {
		this.invoicetype = invoicetype;
	}

	public void setTransactiontype(byte transactiontype) {
		this.transactiontype = transactiontype;
	}
	
}
