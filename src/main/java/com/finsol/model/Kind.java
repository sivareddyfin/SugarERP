package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 */
@Entity
public class Kind 
{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
    private Integer kindCode;
	
	@Column(nullable = false)
    private String kind;
	
	@Column(nullable = false)
    private Double limitPerAcre	;

	@Column(nullable = false)
    private Double cost	;


	@Column(nullable = false)
    private Byte roundTo;

	@Column(nullable = false)
    private Integer advanceCode;

	public Integer getId() {
		return id;
	}

	public Integer getKindCode() {
		return kindCode;
	}

	public String getKind() {
		return kind;
	}

	public Double getLimitPerAcre() {
		return limitPerAcre;
	}

	public Double getCost() {
		return cost;
	}

	

	public Integer getAdvanceCode() {
		return advanceCode;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setKindCode(Integer kindCode) {
		this.kindCode = kindCode;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setLimitPerAcre(Double limitPerAcre) {
		this.limitPerAcre = limitPerAcre;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Byte getRoundTo() {
		return roundTo;
	}

	public void setRoundTo(Byte roundTo) {
		this.roundTo = roundTo;
	}

	public void setAdvanceCode(Integer advanceCode) {
		this.advanceCode = advanceCode;
	}
	
	


}
