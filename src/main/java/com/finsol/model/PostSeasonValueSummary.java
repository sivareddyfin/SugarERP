package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class PostSeasonValueSummary {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column
	private  String season;
	@Column
	private  String ryotcode; 
	@Column
	private String villagecode;
	@Column
	private Integer ryotcodeseqno;
	@Column
	private Double suppliedcanewt;
	@Column
	private Double additionalcanevalue;
	@Column
	private Double transportallowance;
	@Column
	private Double totalvalue;
	@Column
	private Double paidamount;
	@Column
	private Double pendingamount;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getRyotcodeseqno() {
		return ryotcodeseqno;
	}
	public void setRyotcodeseqno(Integer ryotcodeseqno) {
		this.ryotcodeseqno = ryotcodeseqno;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getAdditionalcanevalue() {
		return additionalcanevalue;
	}
	public void setAdditionalcanevalue(Double additionalcanevalue) {
		this.additionalcanevalue = additionalcanevalue;
	}
	public Double getTransportallowance() {
		return transportallowance;
	}
	public void setTransportallowance(Double transportallowance) {
		this.transportallowance = transportallowance;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	
	


}
