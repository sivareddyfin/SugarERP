package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class VehicleTypes 
{
	@Id
	@Column(length = 11)
	private Integer id;
	@Column(length = 11)
	private Integer vehicletypecode;
	@Column(length = 50,nullable = false)
	private String vehicletype;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVehicletypecode() {
		return vehicletypecode;
	}

	public void setVehicletypecode(Integer vehicletypecode) {
		this.vehicletypecode = vehicletypecode;
	}

	public String getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}

	

}
