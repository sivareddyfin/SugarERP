package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author Naidu
 */
@Entity
public class SeedAccountingSummary {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer seedactslno;
	 
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private Date seedaccdate;
	 @Column(nullable = false)
	 private String loginid;
	 @Column(nullable = false)
	 private String villagecode;
	 
	public Integer getSeedactslno() {
		return seedactslno;
	}
	public void setSeedactslno(Integer seedactslno) {
		this.seedactslno = seedactslno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getSeedaccdate() {
		return seedaccdate;
	}
	public void setSeedaccdate(Date seedaccdate) {
		this.seedaccdate = seedaccdate;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	
	 
}
