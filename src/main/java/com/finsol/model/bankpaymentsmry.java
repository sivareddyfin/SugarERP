package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

@Entity
public class bankpaymentsmry {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private String season;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public boolean isSBorLoan() {
		return SBorLoan;
	}

	public void setSBorLoan(boolean sBorLoan) {
		SBorLoan = sBorLoan;
	}

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public Double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(Double totalamt) {
		this.totalamt = totalamt;
	}

	public Double getPaidamt() {
		return paidamt;
	}

	public void setPaidamt(Double paidamt) {
		this.paidamt = paidamt;
	}

	public Double getPendingamt() {
		return pendingamt;
	}

	public void setPendingamt(Double pendingamt) {
		this.pendingamt = pendingamt;
	}

	@Column
	private String ryotcode;
	
	@Column
	private String bankcode;
	
	@Column
	private boolean SBorLoan;	

	@Column
	private String AcctNo;
	
	@Column
	private Double totalamt;
	
	@Column
	private Double paidamt;	
	
	@Column
	private Double pendingamt;		
}
