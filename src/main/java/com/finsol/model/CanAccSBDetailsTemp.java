package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
@Entity
public class CanAccSBDetailsTemp
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private String season;
	
	@Column
	private String ryotcode;
	
	@Column
	private Double ded;
	
	@Column
	private Double loans;
	
	@Column
	private Integer bankcode;
	
	@Column
	private String sbaccno;
	
	@Column
	private Double suppqty;
	
	@Column
	private Double cdcamt;
	
	@Column
	private Double ulamt;
	
	@Column
	private Double otherded;
	
	@Column
	private Double transport;
	
	@Column
	private Double harvesting;
	
	@Column
	private Double loanamt;	
	
	@Column
	private Double advamt;		
	
	@Column
	private Double sbac;
	
	@Column
	private Double totalamt;
	
	@Column
	private Double paidamt;
	
	@Column
	private Double pendingamt;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date paymentdate;
	
	@Column
	private String paymentsource;
	
	@Column
	private byte isdirect;
	
	@Column
	private String ryotdetails;
	
	@Column
	private String mainryotcode;
	
	@Column
	private Double iciciloanamount;
	
	@Column
	private Integer caneacctslno;
	
	public Integer getCaneacctslno() {
		return caneacctslno;
	}

	public void setCaneacctslno(Integer caneacctslno) {
		this.caneacctslno = caneacctslno;
	}

	public Double getIciciloanamount() {
		return iciciloanamount;
	}

	public void setIciciloanamount(Double iciciloanamount) {
		this.iciciloanamount = iciciloanamount;
	}

	public Date getPaymentdate() {
		return paymentdate;
	}

	public void setPaymentdate(Date paymentdate) {
		this.paymentdate = paymentdate;
	}

	public String getPaymentsource() {
		return paymentsource;
	}

	public void setPaymentsource(String paymentsource) {
		this.paymentsource = paymentsource;
	}

	public byte getIsdirect() {
		return isdirect;
	}

	public void setIsdirect(byte isdirect) {
		this.isdirect = isdirect;
	}

	public String getRyotdetails() {
		return ryotdetails;
	}

	public void setRyotdetails(String ryotdetails) {
		this.ryotdetails = ryotdetails;
	}

	public String getMainryotcode() {
		return mainryotcode;
	}

	public void setMainryotcode(String mainryotcode) {
		this.mainryotcode = mainryotcode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public Double getDed() {
		return ded;
	}

	public void setDed(Double ded) {
		this.ded = ded;
	}

	public Double getLoans() {
		return loans;
	}

	public void setLoans(Double loans) {
		this.loans = loans;
	}

	public Integer getBankcode() {
		return bankcode;
	}

	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}

	public String getSbaccno() {
		return sbaccno;
	}

	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}

	public Double getSuppqty() {
		return suppqty;
	}

	public void setSuppqty(Double suppqty) {
		this.suppqty = suppqty;
	}

	public Double getCdcamt() {
		return cdcamt;
	}

	public void setCdcamt(Double cdcamt) {
		this.cdcamt = cdcamt;
	}

	public Double getUlamt() {
		return ulamt;
	}

	public void setUlamt(Double ulamt) {
		this.ulamt = ulamt;
	}

	public Double getOtherded() {
		return otherded;
	}

	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}

	public Double getTransport() {
		return transport;
	}

	public void setTransport(Double transport) {
		this.transport = transport;
	}

	public Double getHarvesting() {
		return harvesting;
	}

	public void setHarvesting(Double harvesting) {
		this.harvesting = harvesting;
	}

	public Double getLoanamt() {
		return loanamt;
	}

	public void setLoanamt(Double loanamt) {
		this.loanamt = loanamt;
	}

	public Double getAdvamt() {
		return advamt;
	}

	public void setAdvamt(Double advamt) {
		this.advamt = advamt;
	}

	public Double getSbac() {
		return sbac;
	}

	public void setSbac(Double sbac) {
		this.sbac = sbac;
	}

	public Double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(Double totalamt) {
		this.totalamt = totalamt;
	}

	public Double getPaidamt() {
		return paidamt;
	}

	public void setPaidamt(Double paidamt) {
		this.paidamt = paidamt;
	}

	public Double getPendingamt() {
		return pendingamt;
	}

	public void setPendingamt(Double pendingamt) {
		this.pendingamt = pendingamt;
	}	
}
