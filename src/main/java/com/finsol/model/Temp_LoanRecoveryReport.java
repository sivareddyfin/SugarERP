package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_LoanRecoveryReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column
	private Integer branchcode;
	@Column 
	private String branch;
	@Column 
	private String accountnum;
	@Column 
	private Double totalamount;
	@Column 
	private Double finalinterest;
	@Column 
	private Double loanamount;
	@Column 
	private String gtotalwords;
	@Column 
	private String betweendate;
	
	
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public String getGtotalwords() {
		return gtotalwords;
	}
	public void setGtotalwords(String gtotalwords) {
		this.gtotalwords = gtotalwords;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAccountnum() {
		return accountnum;
	}
	public void setAccountnum(String accountnum) {
		this.accountnum = accountnum;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getFinalinterest() {
		return finalinterest;
	}
	public void setFinalinterest(Double finalinterest) {
		this.finalinterest = finalinterest;
	}
	public Double getLoanamount() {
		return loanamount;
	}
	public void setLoanamount(Double loanamount) {
		this.loanamount = loanamount;
	}
	
	
	
	

}
