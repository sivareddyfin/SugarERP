package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class GreenHouseDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date detailsDate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private String batchSeries;
	@Column(nullable = true)
	private String variety;
	@Column(nullable = true)
	private String trayTypeIn;
	@Column(nullable = true)
	private double noOfTraysIn;
	@Column(nullable = true)
	private double noOfCavitiesPertrayin;
	@Column(nullable = true)
	private double totalSeedlingsIn;
	@Column(nullable = true)
	private String trayTypeOut;
	@Column(nullable = true)
	private double noOfTraysOut;
	@Column(nullable = true)
	private double noOfCavitiesPertrayOut;
	@Column(nullable = true)
	private double totalSeedlingsOut;
	
	
	
	
	public Integer getSlno()
	{
		return slno;
	}
	public String getSeason() {
		return season;
	}
	public Date getDetailsDate() {
		return detailsDate;
	}
	public Integer getShift() {
		return shift;
	}
	public Integer getId() {
		return id;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public String getTrayTypeIn() {
		return trayTypeIn;
	}
	public double getNoOfTraysIn() {
		return noOfTraysIn;
	}
	public double getNoOfCavitiesPertrayin() {
		return noOfCavitiesPertrayin;
	}
	public double getTotalSeedlingsIn() {
		return totalSeedlingsIn;
	}
	public String getTrayTypeOut() {
		return trayTypeOut;
	}
	public double getNoOfTraysOut() {
		return noOfTraysOut;
	}
	public double getNoOfCavitiesPertrayOut() {
		return noOfCavitiesPertrayOut;
	}
	public double getTotalSeedlingsOut() {
		return totalSeedlingsOut;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setDetailsDate(Date detailsDate) {
		this.detailsDate = detailsDate;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public void setTrayTypeIn(String trayTypeIn) {
		this.trayTypeIn = trayTypeIn;
	}
	public void setNoOfTraysIn(double noOfTraysIn) {
		this.noOfTraysIn = noOfTraysIn;
	}
	public void setNoOfCavitiesPertrayin(double noOfCavitiesPertrayin) {
		this.noOfCavitiesPertrayin = noOfCavitiesPertrayin;
	}
	public void setTotalSeedlingsIn(double totalSeedlingsIn) {
		this.totalSeedlingsIn = totalSeedlingsIn;
	}
	public void setTrayTypeOut(String trayTypeOut) {
		this.trayTypeOut = trayTypeOut;
	}
	public void setNoOfTraysOut(double noOfTraysOut) {
		this.noOfTraysOut = noOfTraysOut;
	}
	public void setNoOfCavitiesPertrayOut(double noOfCavitiesPertrayOut) {
		this.noOfCavitiesPertrayOut = noOfCavitiesPertrayOut;
	}
	public void setTotalSeedlingsOut(double totalSeedlingsOut) {
		this.totalSeedlingsOut = totalSeedlingsOut;
	}
	
	
	
	
	
}
