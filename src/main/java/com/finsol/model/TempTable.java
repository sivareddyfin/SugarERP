package com.finsol.model;


import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class TempTable
{

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private Integer ryotCode;
	
	@Column(nullable = true)
	private String transactiondate;
	
	@Column(nullable = true)
    private String  ryotName;
	
	@Column(nullable = false)
    private String villageName;
	
	@Column(nullable = false)
    private String circleName;
	
	@Column(nullable = false)
    private String zoneName;
	
	@Column(nullable = false)
    private String  dispatchNo;
	
	@Column(nullable = false)
    private String variety;
	
	@Column(nullable = false)
    private String noOfTrays;
	
	@Column(nullable = false)
    private Integer noOfSeedlings;
	
	@Column(nullable = false)
	private String trayType;

	
	
	public Integer getId()
	{
		return id;
	}

	public Integer getRyotCode() {
		return ryotCode;
	}

	

	public String getRyotName() {
		return ryotName;
	}

	public String getVillageName() {
		return villageName;
	}

	public String getCircleName() {
		return circleName;
	}

	public String getZoneName() {
		return zoneName;
	}

	public String getDispatchNo() {
		return dispatchNo;
	}

	public String getVariety() {
		return variety;
	}

	public String getNoOfTrays() {
		return noOfTrays;
	}

	public Integer getNoOfSeedlings() {
		return noOfSeedlings;
	}

	

	public void setId(Integer id) {
		this.id = id;
	}

	public void setRyotCode(Integer ryotCode) {
		this.ryotCode = ryotCode;
	}

	
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	

	public String getTransactiondate() {
		return transactiondate;
	}

	public void setTransactiondate(String transactiondate) {
		this.transactiondate = transactiondate;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public void setNoOfTrays(String noOfTrays) {
		this.noOfTrays = noOfTrays;
	}

	public void setNoOfSeedlings(Integer noOfSeedlings) {
		this.noOfSeedlings = noOfSeedlings;
	}

	public String getTrayType() {
		return trayType;
	}

	public void setTrayType(String trayType) {
		this.trayType = trayType;
	}

	
	
	
	
	

	
	
	
	
}
