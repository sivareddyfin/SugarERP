package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class RandDAgreementDetails 
{	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (nullable = false)
	private Integer slno;
	
	@Column (nullable = false)
	private String agreementnumber;
	@Column
	@Type(type="date")
	private Date agreementdate;
	@Column
	private String seasonyear;
	@Column
	private Integer plotseqno;
	@Column
	private String plotnumber;
	@Column
	private String ryotcode;
	@Column
	private String ryotsalutation;
	@Column
	private String ryotname;
	@Column
	private String relation;
	@Column
	private String relativename;
	@Column
	private Integer circlecode;
	@Column
	private String villagecode;
	@Column
	private Integer mandalcode;
	@Column
	private Integer regioncode;
	@Column
	private Integer zonecode;
	@Column
	private Integer varietycode;
	@Column
	private String plantorratoon;
	@Column	
	@Type(type="date")
	private Date cropdate;
	@Column
	private Double agreedqty;
	@Column
	private Double extentsize;
	@Column
	private String surveynumber;
	@Column
	private String landvillagecode;
	@Column
	private Integer landtype;	
	@Column
	private String extentimagepath;	
	@Column
	private Double estimatedqty;	
	@Column
	private Double progressiveqty;
	@Column
	private String programnumber;
	@Column
	private Double averageccsrating;	
	@Column
	private Double familygroupavgccsrating;	
	@Column
	private Integer rank;	
	@Column
	@Type(type="date")
	private Date canesupplystartdate;
	@Column
	@Type(type="date")
	private Date canesupplyenddate;
	@Column
	private String samplecards;
	@Column
	private String permitnumbers;
	@Column
	private String suretyryotcode;
	@Column
	private Integer canemanagerid;
	@Column
	private Integer fieldofficerid;
	@Column
	private Integer fieldassistantid;
	
	@Column
	private Integer ryotseqno;
	
	@Column
	private Byte commercialOrSeed;
	
	@Column
	private double plotdistance;
	
	@Column
	private String address;
	
	
	public double getPlotdistance() {
		return plotdistance;
	}
	public void setPlotdistance(double plotdistance) {
		this.plotdistance = plotdistance;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Byte getCommercialOrSeed() {
		return commercialOrSeed;
	}
	public void setCommercialOrSeed(Byte commercialOrSeed) {
		this.commercialOrSeed = commercialOrSeed;
	}
	public Integer getRyotseqno() {
		return ryotseqno;
	}
	public void setRyotseqno(Integer ryotseqno) {
		this.ryotseqno = ryotseqno;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	
	
	public String getSeasonyear() {
		return seasonyear;
	}
	public void setSeasonyear(String seasonyear) {
		this.seasonyear = seasonyear;
	}
	public Date getAgreementdate() {
		return agreementdate;
	}
	public void setAgreementdate(Date agreementdate) {
		this.agreementdate = agreementdate;
	}
	public Integer getPlotseqno() {
		return plotseqno;
	}
	public void setPlotseqno(Integer plotseqno) {
		this.plotseqno = plotseqno;
	}
	public String getPlotnumber() {
		return plotnumber;
	}
	public void setPlotnumber(String plotnumber) {
		this.plotnumber = plotnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotsalutation() {
		return ryotsalutation;
	}
	public void setRyotsalutation(String ryotsalutation) {
		this.ryotsalutation = ryotsalutation;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	
	
	
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getMandalcode() {
		return mandalcode;
	}
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	public Integer getRegioncode() {
		return regioncode;
	}
	public void setRegioncode(Integer regioncode) {
		this.regioncode = regioncode;
	}
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public Integer getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public Date getCropdate() {
		return cropdate;
	}
	public void setCropdate(Date cropdate) {
		this.cropdate = cropdate;
	}
	public Double getAgreedqty() {
		return agreedqty;
	}
	public void setAgreedqty(Double agreedqty) {
		this.agreedqty = agreedqty;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public String getSurveynumber() {
		return surveynumber;
	}
	public void setSurveynumber(String surveynumber) {
		this.surveynumber = surveynumber;
	}
	
	
	
	
	public String getLandvillagecode() {
		return landvillagecode;
	}
	public void setLandvillagecode(String landvillagecode) {
		this.landvillagecode = landvillagecode;
	}
	public Integer getLandtype() {
		return landtype;
	}
	public void setLandtype(Integer landtype) {
		this.landtype = landtype;
	}
	public String getExtentimagepath() {
		return extentimagepath;
	}
	public void setExtentimagepath(String extentimagepath) {
		this.extentimagepath = extentimagepath;
	}
	public Double getEstimatedqty() {
		return estimatedqty;
	}
	public void setEstimatedqty(Double estimatedqty) {
		this.estimatedqty = estimatedqty;
	}
	public Double getProgressiveqty() {
		return progressiveqty;
	}
	public void setProgressiveqty(Double progressiveqty) {
		this.progressiveqty = progressiveqty;
	}

	
	public String getProgramnumber() {
		return programnumber;
	}
	public void setProgramnumber(String programnumber) {
		this.programnumber = programnumber;
	}
	public Double getAverageccsrating() {
		return averageccsrating;
	}
	public void setAverageccsrating(Double averageccsrating) {
		this.averageccsrating = averageccsrating;
	}
	public Double getFamilygroupavgccsrating() {
		return familygroupavgccsrating;
	}
	public void setFamilygroupavgccsrating(Double familygroupavgccsrating) {
		this.familygroupavgccsrating = familygroupavgccsrating;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
	
	public Date getCanesupplystartdate() {
		return canesupplystartdate;
	}
	public void setCanesupplystartdate(Date canesupplystartdate) {
		this.canesupplystartdate = canesupplystartdate;
	}
	public Date getCanesupplyenddate() {
		return canesupplyenddate;
	}
	public void setCanesupplyenddate(Date canesupplyenddate) {
		this.canesupplyenddate = canesupplyenddate;
	}
	public String getSamplecards() {
		return samplecards;
	}
	public void setSamplecards(String samplecards) {
		this.samplecards = samplecards;
	}
	public String getPermitnumbers() {
		return permitnumbers;
	}
	public void setPermitnumbers(String permitnumbers) {
		this.permitnumbers = permitnumbers;
	}
	public String getSuretyryotcode() {
		return suretyryotcode;
	}
	public void setSuretyryotcode(String suretyryotcode) {
		this.suretyryotcode = suretyryotcode;
	}
	public Integer getCanemanagerid() {
		return canemanagerid;
	}
	public void setCanemanagerid(Integer canemanagerid) {
		this.canemanagerid = canemanagerid;
	}
	public Integer getFieldofficerid() {
		return fieldofficerid;
	}
	public void setFieldofficerid(Integer fieldofficerid) {
		this.fieldofficerid = fieldofficerid;
	}
	public Integer getFieldassistantid() {
		return fieldassistantid;
	}
	public void setFieldassistantid(Integer fieldassistantid) {
		this.fieldassistantid = fieldassistantid;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	
	
	

}
