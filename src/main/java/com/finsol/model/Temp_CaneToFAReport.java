package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class Temp_CaneToFAReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String glcode;
	@Column
	private Double debiteamount;
	@Column
	private Double creditamount;
	@Column
	private String weekendingdate;   
	@Column
	private String Particulars;
	@Column
	private String supplyqty;
	@Column
	private String slno;
	
	public String getSlno() {
		return slno;
	}
	public void setSlno(String slno) {
		this.slno = slno;
	}
	
	public String getSupplyqty() {
		return supplyqty;
	}
	public void setSupplyqty(String supplyqty) {
		this.supplyqty = supplyqty;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGlcode() {
		return glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	public Double getDebiteamount() {
		return debiteamount;
	}
	public void setDebiteamount(Double debiteamount) {
		this.debiteamount = debiteamount;
	}
	public Double getCreditamount() {
		return creditamount;
	}
	public void setCreditamount(Double creditamount) {
		this.creditamount = creditamount;
	}
	public String getWeekendingdate() {
		return weekendingdate;
	}
	public void setWeekendingdate(String weekendingdate) {
		this.weekendingdate = weekendingdate;
	}
	public String getParticulars() {
		return Particulars;
	}
	public void setParticulars(String particulars) {
		Particulars = particulars;
	}
	
}
