package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class WeighmentDetails {
	
	@Id
	@Column
	private Integer serialnumber;
	@Column
	@Type(type="date")
	private Date canereceiptdate;
	@Column
	private String season;
	@Column
	private Time canereceipttime;
	@Column
	private Integer shiftid;
	@Column
	private String shiftname;
	@Column
	private Integer weighbridgeslno;
	@Column
	private Integer weighmentno;
	@Column
	private Integer varietycode;
	@Column
	private Integer zonecode;
	@Column
	private Integer circlecode;
	@Column
	private String landvilcode;
	@Column
	private String villagecode;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String agreementnumber;
	@Column
	private String loginid;
	@Column
	private String fhgname;
	@Column
	private  Double grossweight;
	@Column
	private  Double netwt;
	@Column
	private  Double bindingmtlwght;
	@Column
	private Integer uccode;
	@Column
	private Byte plantorratoon;
	@Column
	private Byte partload;
	@Column
	private String plotnumber;
	@Column
	private Integer permitnumber;
	@Column
	private String weightmentype;
	@Column
	private Byte burntcane;
	@Column
	private Integer lorryortruck;
	@Column
	private Integer vehicletype;
	@Column
	private String vehicleno;
	@Column
	private Integer secondpermitno;
	@Column
	private Byte castatus;	
	@Column
	private Byte status;
	
	@Column
	@Type(type="date")
	private Date canereceiptenddate;
	@Column
	private Time canereceiptendtime;
	@Column
	private  Double lorrywt;
	@Column
	private  Double lorryandcanewt;
	@Column
	private Integer shiftid1;
	@Column
	private Integer regioncode;
	@Column
	private Integer mandalcode;
	@Column
	private Integer fieldofficerid;
	@Column
	private Integer fieldassistantid;
	@Column
	private String time;
	//Added by DMurty on 23-11-2016
	@Column
	private Integer actualserialnumber;
	@Column
	@Type(type="date")
	private Date shiftdate;
	
	@Column
	@Type(type="date")
	private Date systemstartdate;
	
	@Column
	@Type(type="date")
	private Date systemenddate;
	
	@Column
	private  Double transportallowancedistance;
	
	@Column
	private  Double transportallowance;

	
	
	public Double getTransportallowancedistance() {
		return transportallowancedistance;
	}
	public void setTransportallowancedistance(Double transportallowancedistance) {
		this.transportallowancedistance = transportallowancedistance;
	}
	public Double getTransportallowance() {
		return transportallowance;
	}
	public void setTransportallowance(Double transportallowance) {
		this.transportallowance = transportallowance;
	}
	public Date getSystemstartdate() {
		return systemstartdate;
	}
	public void setSystemstartdate(Date systemstartdate) {
		this.systemstartdate = systemstartdate;
	}
	public Date getSystemenddate() {
		return systemenddate;
	}
	public void setSystemenddate(Date systemenddate) {
		this.systemenddate = systemenddate;
	}
	@Column
	private Integer transactioncode;
	
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public Date getShiftdate() {
		return shiftdate;
	}
	public void setShiftdate(Date shiftdate) {
		this.shiftdate = shiftdate;
	}
	public Integer getActualserialnumber() {
		return actualserialnumber;
	}
	public void setActualserialnumber(Integer actualserialnumber) {
		this.actualserialnumber = actualserialnumber;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Integer getFieldofficerid() {
		return fieldofficerid;
	}
	public void setFieldofficerid(Integer fieldofficerid) {
		this.fieldofficerid = fieldofficerid;
	}
	public Integer getFieldassistantid() {
		return fieldassistantid;
	}
	public void setFieldassistantid(Integer fieldassistantid) {
		this.fieldassistantid = fieldassistantid;
	}
	public Integer getSerialnumber() {
		return serialnumber;
	}
	public void setSerialnumber(Integer serialnumber) {
		this.serialnumber = serialnumber;
	}
	public Integer getShiftid() {
		return shiftid;
	}
	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}
	public String getShiftname() {
		return shiftname;
	}
	public void setShiftname(String shiftname) {
		this.shiftname = shiftname;
	}
	public Integer getWeighbridgeslno() {
		return weighbridgeslno;
	}
	public void setWeighbridgeslno(Integer weighbridgeslno) {
		this.weighbridgeslno = weighbridgeslno;
	}
	public Integer getWeighmentno() {
		return weighmentno;
	}
	public void setWeighmentno(Integer weighmentno) {
		this.weighmentno = weighmentno;
	}
	
	
	
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getFhgname() {
		return fhgname;
	}
	public void setFhgname(String fhgname) {
		this.fhgname = fhgname;
	}
	public Double getGrossweight() {
		return grossweight;
	}
	public void setGrossweight(Double grossweight) {
		this.grossweight = grossweight;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getBindingmtlwght() {
		return bindingmtlwght;
	}
	public void setBindingmtlwght(Double bindingmtlwght) {
		this.bindingmtlwght = bindingmtlwght;
	}
	public Integer getUccode() {
		return uccode;
	}
	public void setUccode(Integer uccode) {
		this.uccode = uccode;
	}
	public Byte getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(Byte plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public Byte getPartload() {
		return partload;
	}
	public void setPartload(Byte partload) {
		this.partload = partload;
	}
	public String getPlotnumber() {
		return plotnumber;
	}
	public void setPlotnumber(String plotnumber) {
		this.plotnumber = plotnumber;
	}
	public Integer getPermitnumber() {
		return permitnumber;
	}
	public void setPermitnumber(Integer permitnumber) {
		this.permitnumber = permitnumber;
	}
	public String getWeightmentype() {
		return weightmentype;
	}
	public void setWeightmentype(String weightmentype) {
		this.weightmentype = weightmentype;
	}
	public Byte getBurntcane() {
		return burntcane;
	}
	public void setBurntcane(Byte burntcane) {
		this.burntcane = burntcane;
	}
	public Integer getLorryortruck() {
		return lorryortruck;
	}
	public void setLorryortruck(Integer lorryortruck) {
		this.lorryortruck = lorryortruck;
	}
	public Integer getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(Integer vehicletype) {
		this.vehicletype = vehicletype;
	}
	public String getVehicleno() {
		return vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public Integer getSecondpermitno() {
		return secondpermitno;
	}
	public void setSecondpermitno(Integer secondpermitno) {
		this.secondpermitno = secondpermitno;
	}
	public Byte getCastatus() {
		return castatus;
	}
	public void setCastatus(Byte castatus) {
		this.castatus = castatus;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public Date getCanereceiptdate() {
		return canereceiptdate;
	}
	public void setCanereceiptdate(Date canereceiptdate) {
		this.canereceiptdate = canereceiptdate;
	}
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Time getCanereceipttime() {
		return canereceipttime;
	}
	public void setCanereceipttime(Time canereceipttime) {
		this.canereceipttime = canereceipttime;
	}
	public Integer getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	
	public Date getCanereceiptenddate() {
		return canereceiptenddate;
	}
	public void setCanereceiptenddate(Date canereceiptenddate) {
		this.canereceiptenddate = canereceiptenddate;
	}
	public Time getCanereceiptendtime() {
		return canereceiptendtime;
	}
	public void setCanereceiptendtime(Time canereceiptendtime) {
		this.canereceiptendtime = canereceiptendtime;
	}
	public Double getLorrywt() {
		return lorrywt;
	}
	public void setLorrywt(Double lorrywt) {
		this.lorrywt = lorrywt;
	}
	public Integer getShiftid1() {
		return shiftid1;
	}
	public void setShiftid1(Integer shiftid1) {
		this.shiftid1 = shiftid1;
	}
	public Double getLorryandcanewt() {
		return lorryandcanewt;
	}
	public void setLorryandcanewt(Double lorryandcanewt) {
		this.lorryandcanewt = lorryandcanewt;
	}
	public String getLandvilcode() {
		return landvilcode;
	}
	public void setLandvilcode(String landvilcode) {
		this.landvilcode = landvilcode;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getRegioncode() {
		return regioncode;
	}
	public void setRegioncode(Integer regioncode) {
		this.regioncode = regioncode;
	}
	public Integer getMandalcode() {
		return mandalcode;
	}
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	
}
