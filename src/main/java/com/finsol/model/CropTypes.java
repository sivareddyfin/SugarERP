package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CropTypes 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(length = 11)
	private Integer croptypecode;
	
	@Column(length = 10,nullable = false)
	private String croptype;
	
	private Byte status;

	public Integer getCroptypecode() {
		return croptypecode;
	}

	public void setCroptypecode(Integer croptypecode) {
		this.croptypecode = croptypecode;
	}

	public String getCroptype() {
		return croptype;
	}

	public void setCroptype(String croptype) {
		this.croptype = croptype;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	

}
