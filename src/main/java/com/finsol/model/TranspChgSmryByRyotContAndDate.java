package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class TranspChgSmryByRyotContAndDate {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	@Type(type="date")
	private Date receiptdate;
	@Column
	private  String ryotcode; 
	@Column
	private  Integer transportcontcode;
	@Column
	private  Double transportedcanewt;
	@Column
	private  Double totalextent;
	
	@Column
	private  Double rate;
	@Column
	private  Double totalamount;
	@Column
	private  Double mincanewt;
	@Column
	private Byte castatus;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getTransportcontcode() {
		return transportcontcode;
	}
	public void setTransportcontcode(Integer transportcontcode) {
		this.transportcontcode = transportcontcode;
	}
	public Double getTransportedcanewt() {
		return transportedcanewt;
	}
	public void setTransportedcanewt(Double transportedcanewt) {
		this.transportedcanewt = transportedcanewt;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getMincanewt() {
		return mincanewt;
	}
	public void setMincanewt(Double mincanewt) {
		this.mincanewt = mincanewt;
	}
	public Byte getCastatus() {
		return castatus;
	}
	public void setCastatus(Byte castatus) {
		this.castatus = castatus;
	}

}
