package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;


/**
 * @author sahadeva
 */

@Entity
public class Temp_CheckListForSBAccountPortions 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private Double supplyqty;
	@Column 
	private Double amtpayble;
	@Column 
	private Double trptamount;
	@Column 
	private Double harvamount;
	@Column 
	private Double cdc;
	@Column 
	private Double uc;
	@Column 
	private Double otherded;
	
	@Column
	private Integer bankcode;
	@Column
	private String sbaccno;
	@Column
	private Double loanamt;	
	@Column
	private Double sbac;
	@Column
	private String betweendate;
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public Double getLoanamt() {
		return loanamt;
	}
	public void setLoanamt(Double loanamt) {
		this.loanamt = loanamt;
	}
	public Double getSbac() {
		return sbac;
	}
	public void setSbac(Double sbac) {
		this.sbac = sbac;
	}
	public Integer getBankcode() {
		return bankcode;
	}
	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}
	public String getSbaccno() {
		return sbaccno;
	}
	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getSupplyqty() {
		return supplyqty;
	}
	public void setSupplyqty(Double supplyqty) {
		this.supplyqty = supplyqty;
	}
	public Double getAmtpayble() {
		return amtpayble;
	}
	public void setAmtpayble(Double amtpayble) {
		this.amtpayble = amtpayble;
	}
	public Double getTrptamount() {
		return trptamount;
	}
	public void setTrptamount(Double trptamount) {
		this.trptamount = trptamount;
	}
	public Double getHarvamount() {
		return harvamount;
	}
	public void setHarvamount(Double harvamount) {
		this.harvamount = harvamount;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getUc() {
		return uc;
	}
	public void setUc(Double uc) {
		this.uc = uc;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	

	
}
