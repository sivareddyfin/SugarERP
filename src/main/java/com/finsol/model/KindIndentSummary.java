package com.finsol.model;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class KindIndentSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer seqNo;
	@Column
	private String indentNo;
	@Column
	private String ryotCode;
	@Column
	private String ryotName;
	@Column
	private String village;
	@Column
	private String fatherName;
	@Column
	private String aadhaarNumber;
	@Column
	private String phoneNumber;
	@Column
	private Integer fieldMan;
	@Column
	private String dispatchNo;
	@Column
	private Integer indentStatus;
	@Column
	private String loginUser;
	@Column
	private Integer fieldofficerEmpId;
	@Column(nullable = true)
	@Type(type="date")
	private Date SummaryDate;
	@Column(nullable = true)
    private Timestamp SummaryTime;
	@Column
	private Integer zone;
	@Column
	private Integer foCode;
	
	@Column
	private String storecode;
	
	@Column(nullable = true)
	@Type(type="date")
	private Date authorisationdate;
	
	@Column
	private Double noOfAcr;
	@Column
	private String agreementNo;

	
	
	public Double getNoOfAcr() {
		return noOfAcr;
	}

	public String getAgreementNo() {
		return agreementNo;
	}

	public void setNoOfAcr(Double noOfAcr) {
		this.noOfAcr = noOfAcr;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	public String getStorecode() {
		return storecode;
	}

	public Date getAuthorisationdate() {
		return authorisationdate;
	}

	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}

	public void setAuthorisationdate(Date authorisationdate) {
		this.authorisationdate = authorisationdate;
	}

	public Integer getFoCode() {
		return foCode;
	}

	public void setFoCode(Integer foCode) {
		this.foCode = foCode;
	}

	public Integer getZone()
	{
		return zone;
	}

	public void setZone(Integer zone)
	{
		this.zone = zone;
	}

	public Integer getFieldofficerEmpId() {
		return fieldofficerEmpId;
	}

	public void setFieldofficerEmpId(Integer fieldofficerEmpId) {
		this.fieldofficerEmpId = fieldofficerEmpId;
	}

	public Timestamp getSummaryTime() {
		return SummaryTime;
	}

	public void setSummaryTime(Timestamp summaryTime) {
		SummaryTime = summaryTime;
	}

	public Date getSummaryDate() {
		return SummaryDate;
	}
	
	public void setSummaryDate(Date summaryDate) {
		SummaryDate = summaryDate;
	}
	public String getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}

	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public String getIndentNo() {
		return indentNo;
	}
	
	public String getRyotName() {
		return ryotName;
	}
	
	public String getFatherName() {
		return fatherName;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	
	public String getRyotCode() {
		return ryotCode;
	}

	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}

	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	
	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getFieldMan() {
		return fieldMan;
	}

	public void setFieldMan(Integer fieldMan) {
		this.fieldMan = fieldMan;
	}
	
	
	
	
}
