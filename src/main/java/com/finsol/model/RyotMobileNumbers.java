package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author DMurty
 *
 */
@Entity
public class RyotMobileNumbers 
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	
	@Column
	private String ryotname;
	
	@Column
	private String mobileno;
	
	@Column
	private String agreementno;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getRyotcode() {
		return ryotcode;
	}


	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}


	public String getRyotname() {
		return ryotname;
	}


	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}


	public String getMobileno() {
		return mobileno;
	}


	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}


	public String getAgreementno() {
		return agreementno;
	}


	public void setAgreementno(String agreementno) {
		this.agreementno = agreementno;
	}

}
