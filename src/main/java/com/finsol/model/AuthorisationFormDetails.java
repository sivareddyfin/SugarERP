package com.finsol.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class AuthorisationFormDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private String authorisationSeqNo;
	@Column
	private String indentNumbers;
	@Column
	private Integer itemtype;
	@Column
	private Integer quantity;
	@Column
	private Double costofEachItem;
	@Column
	private String totalCost;
	@Column
	@Type(type="date")
	private Date dateofAutorisation;
	@Column
	private Integer indentStatus;
	@Column
	private Integer allowedqty;
	@Column
	private Integer indentqty;
	@Column
	private Integer pendingqty;
	@Column
	private Integer ackstatus;
	
	
	
	
	public Integer getAckstatus() {
		return ackstatus;
	}
	public void setAckstatus(Integer ackstatus) {
		this.ackstatus = ackstatus;
	}
	public Integer getAllowedqty() {
		return allowedqty;
	}
	public Integer getIndentqty() {
		return indentqty;
	}
	public Integer getPendingqty() {
		return pendingqty;
	}
	public void setAllowedqty(Integer allowedqty) {
		this.allowedqty = allowedqty;
	}
	public void setIndentqty(Integer indentqty) {
		this.indentqty = indentqty;
	}
	public void setPendingqty(Integer pendingqty) {
		this.pendingqty = pendingqty;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public Integer getItemtype() {
		return itemtype;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public Double getCostofEachItem() {
		return costofEachItem;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public Date getDateofAutorisation() {
		return dateofAutorisation;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setItemtype(Integer itemtype) {
		this.itemtype = itemtype;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public void setCostofEachItem(Double costofEachItem) {
		this.costofEachItem = costofEachItem;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public void setDateofAutorisation(Date dateofAutorisation) {
		this.dateofAutorisation = dateofAutorisation;
	}
	
	
	
	
}
