package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class AdvanceSummary {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String season;
	@Column
	private String villagecode;
	@Column
	private Integer ryotcodeSeq;
	@Column
	private Integer advancecode;
	@Column
	private Double advanceamount;
	@Column
	private Double interestamount;
	@Column
	private Double totalamount;
	@Column
	private Double recoveredamount;
	@Column
	private Double pendingamount;
	@Column
	private Double paidamount;
	@Column
	private Double pendingpayable;
	@Column
	private Byte advancestatus;
	@Column
	private String loginid;
	@Column
	private Integer transactioncode;
	
	
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}

	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getRyotcodeSeq() {
		return ryotcodeSeq;
	}
	public void setRyotcodeSeq(Integer ryotcodeSeq) {
		this.ryotcodeSeq = ryotcodeSeq;
	}
	public Integer getAdvancecode() {
		return advancecode;
	}
	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}
	public Double getAdvanceamount() {
		return advanceamount;
	}
	public void setAdvanceamount(Double advanceamount) {
		this.advanceamount = advanceamount;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getRecoveredamount() {
		return recoveredamount;
	}
	public void setRecoveredamount(Double recoveredamount) {
		this.recoveredamount = recoveredamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public Byte getAdvancestatus() {
		return advancestatus;
	}
	public void setAdvancestatus(Byte advancestatus) {
		this.advancestatus = advancestatus;
	}
	
	
	
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingpayable() {
		return pendingpayable;
	}
	public void setPendingpayable(Double pendingpayable) {
		this.pendingpayable = pendingpayable;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	
	


}
