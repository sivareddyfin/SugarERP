package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 *
 */
@Entity
public class SssLoans 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private Double LI_NO;
	
	@Column
	private Double LI_BK_COD;
	
	@Column
	private String LI_RYOT;
	
	@Column
	private Double LI_PEXTE;
	
	public Double getLI_NO() {
		return LI_NO;
	}

	public void setLI_NO(Double lI_NO) {
		LI_NO = lI_NO;
	}

	public Double getLI_BK_COD() {
		return LI_BK_COD;
	}

	public void setLI_BK_COD(Double lI_BK_COD) {
		LI_BK_COD = lI_BK_COD;
	}

	public String getLI_RYOT() {
		return LI_RYOT;
	}

	public void setLI_RYOT(String lI_RYOT) {
		LI_RYOT = lI_RYOT;
	}

	public Double getLI_PEXTE() {
		return LI_PEXTE;
	}

	public void setLI_PEXTE(Double lI_PEXTE) {
		LI_PEXTE = lI_PEXTE;
	}

	public Double getLI_REXTE() {
		return LI_REXTE;
	}

	public void setLI_REXTE(Double lI_REXTE) {
		LI_REXTE = lI_REXTE;
	}

	public String getLI_DATE() {
		return LI_DATE;
	}

	public void setLI_DATE(String lI_DATE) {
		LI_DATE = lI_DATE;
	}

	public String getLI_REF_NO() {
		return LI_REF_NO;
	}

	public void setLI_REF_NO(String lI_REF_NO) {
		LI_REF_NO = lI_REF_NO;
	}

	public Double getLI_PAMT() {
		return LI_PAMT;
	}

	public void setLI_PAMT(Double lI_PAMT) {
		LI_PAMT = lI_PAMT;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column
	private Double LI_REXTE;
	
	@Column
	private String LI_DATE;
	
	@Column
	private String LI_REF_NO;
	
	@Column
	private Double LI_PAMT;

	public Integer getId() {
		return id;
	}

}
