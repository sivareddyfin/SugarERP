package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_AreaWisePendingReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column 
	private String nametype;
	@Column 
	private Double estimatedqty;
	@Column 
	private Double progressiveqty;
	@Column 
	private Double estimatedqty1;
	@Column 
	private Double progressiveqty1;
	@Column 
	private Double estimatedqty2;
	@Column 
	private Double progressiveqty2;
	
	@Column 
	private Double totestimatedqty;
	@Column 
	private Double totprogressiveqty;
	
	
	public Double getTotestimatedqty() {
		return totestimatedqty;
	}
	public void setTotestimatedqty(Double totestimatedqty) {
		this.totestimatedqty = totestimatedqty;
	}
	public Double getTotprogressiveqty() {
		return totprogressiveqty;
	}
	public void setTotprogressiveqty(Double totprogressiveqty) {
		this.totprogressiveqty = totprogressiveqty;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getNametype() {
		return nametype;
	}
	public void setNametype(String nametype) {
		this.nametype = nametype;
	}
	public Double getEstimatedqty() {
		return estimatedqty;
	}
	public void setEstimatedqty(Double estimatedqty) {
		this.estimatedqty = estimatedqty;
	}
	public Double getProgressiveqty() {
		return progressiveqty;
	}
	public void setProgressiveqty(Double progressiveqty) {
		this.progressiveqty = progressiveqty;
	}
	public Double getEstimatedqty1() {
		return estimatedqty1;
	}
	public void setEstimatedqty1(Double estimatedqty1) {
		this.estimatedqty1 = estimatedqty1;
	}
	public Double getProgressiveqty1() {
		return progressiveqty1;
	}
	public void setProgressiveqty1(Double progressiveqty1) {
		this.progressiveqty1 = progressiveqty1;
	}
	public Double getEstimatedqty2() {
		return estimatedqty2;
	}
	public void setEstimatedqty2(Double estimatedqty2) {
		this.estimatedqty2 = estimatedqty2;
	}
	public Double getProgressiveqty2() {
		return progressiveqty2;
	}
	public void setProgressiveqty2(Double progressiveqty2) {
		this.progressiveqty2 = progressiveqty2;
	}
	
	

}
