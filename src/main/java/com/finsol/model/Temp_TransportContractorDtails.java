package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_TransportContractorDtails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column
	private String transporter;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String circle;
	@Column
	private Double netwt;
	@Column
	private Double totalamount;
	@Column
	private Double rate;
	@Column
	private Integer transportcontcode;
	@Column
	private String zone;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTransporter() {
		return transporter;
	}
	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Integer getTransportcontcode() {
		return transportcontcode;
	}
	public void setTransportcontcode(Integer transportcontcode) {
		this.transportcontcode = transportcontcode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}

	
}
