package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama krishna
 */
@Entity
public class SubsidyDetailsByRyot {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	private  String ryotcode;
	@Column
	private Double suppliedcanewt;
	@Column
	private Double subsidyrate;
	@Column
	private Double subsidyamount;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getSubsidyrate() {
		return subsidyrate;
	}
	public void setSubsidyrate(Double subsidyrate) {
		this.subsidyrate = subsidyrate;
	}
	public Double getSubsidyamount() {
		return subsidyamount;
	}
	public void setSubsidyamount(Double subsidyamount) {
		this.subsidyamount = subsidyamount;
	}
	
	


}
