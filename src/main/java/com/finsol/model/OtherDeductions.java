package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class OtherDeductions {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer caneacslno;
	@Column
	private String ryotcode;
	@Column
	private Double isbcdeduction;
	@Column
	private Double dedamount;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getIsbcdeduction() {
		return isbcdeduction;
	}
	public void setIsbcdeduction(Double isbcdeduction) {
		this.isbcdeduction = isbcdeduction;
	}
	public Double getDedamount() {
		return dedamount;
	}
	public void setDedamount(Double dedamount) {
		this.dedamount = dedamount;
	}

	

}
