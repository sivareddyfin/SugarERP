package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author DMurty
 */
@Entity
public class AdvanceOrder
{
	private static final long serialVersionUID = 1L;
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String season;
   
	@Id
	@Column(nullable = false)
    private Integer advanceCode;
	
	@Column(nullable = false)
    private Integer advanceOrder;
	
	@Column(nullable = false)
	@Type(type="date")
    private Date effectiveDate;
	

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public Integer getAdvanceCode() 
	{
		return advanceCode;
	}

	public void setAdvanceCode(Integer advanceCode)
	{
		this.advanceCode = advanceCode;
	}

	public Integer getAdvanceOrder()
	{
		return advanceOrder;
	}

	public void setAdvanceOrder(Integer advanceOrder) 
	{
		this.advanceOrder = advanceOrder;
	}

	public Date getEffectiveDate()
	{
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate)
	{
		this.effectiveDate = effectiveDate;
	}
}
