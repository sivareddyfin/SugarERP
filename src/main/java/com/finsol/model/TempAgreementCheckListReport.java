package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 * @author naidu
 */

@Entity
public class TempAgreementCheckListReport {
	
	
	//village,agreementnumber,ryotcode,ryotname,relativename,landvillagecode,plantorratoon,variety,
	//cropdate,extentsize,estimatedqty
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	@Column 
	private String village;
	@Column 
	private String agreementnumber;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private Integer landvillagecode;
	@Column 
	private String plantorratoon;
	@Column 
	private String variety;
	@Column 
	private String cropdate;
	@Column 
	private Double extentsize;
	@Column 
	private Double estimatedqty;
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Integer getLandvillagecode() {
		return landvillagecode;
	}
	public void setLandvillagecode(Integer landvillagecode) {
		this.landvillagecode = landvillagecode;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getCropdate() {
		return cropdate;
	}
	public void setCropdate(String cropdate) {
		this.cropdate = cropdate;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Double getEstimatedqty() {
		return estimatedqty;
	}
	public void setEstimatedqty(Double estimatedqty) {
		this.estimatedqty = estimatedqty;
	}
	

	

}
