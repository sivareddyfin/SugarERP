package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneReceiptDaySmryByRyot
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
		
	@Column
	@Type(type="date")
	private Date canereceiptdate;	
	@Column
	private Integer noofrects;
	
	@Column
	private  String ryotcode;
	
	@Column
	private Integer circlecode;
	@Column
	private Double netwt;
	@Column
	private Double totalextentsize;
	@Column
	private Integer harvestcontcode;
	@Column
	private Double harvestingcharges;
	@Column
	private Integer transpcontcode;
	@Column
	private Double transportcharges;
	@Column
	private byte castatus;
	
	//Added by DMurty on 04-08-2016 
	@Column
	private Double accruedbutnotduerate;
	@Column
	private Double accruedbutnotdueamount;
	
	@Column
	private  String season;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Double getAccruedbutnotduerate() {
		return accruedbutnotduerate;
	}
	public void setAccruedbutnotduerate(Double accruedbutnotduerate) {
		this.accruedbutnotduerate = accruedbutnotduerate;
	}
	public Double getAccruedbutnotdueamount() {
		return accruedbutnotdueamount;
	}
	public void setAccruedbutnotdueamount(Double accruedbutnotdueamount) {
		this.accruedbutnotdueamount = accruedbutnotdueamount;
	}
	public Date getCanereceiptdate() {
		return canereceiptdate;
	}
	public void setCanereceiptdate(Date canereceiptdate) {
		this.canereceiptdate = canereceiptdate;
	}
	public Integer getNoofrects() {
		return noofrects;
	}
	public void setNoofrects(Integer noofrects) {
		this.noofrects = noofrects;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getTotalextentsize() {
		return totalextentsize;
	}
	public void setTotalextentsize(Double totalextentsize) {
		this.totalextentsize = totalextentsize;
	}
	public Integer getHarvestcontcode() {
		return harvestcontcode;
	}
	public void setHarvestcontcode(Integer harvestcontcode) {
		this.harvestcontcode = harvestcontcode;
	}
	public Double getHarvestingcharges() {
		return harvestingcharges;
	}
	public void setHarvestingcharges(Double harvestingcharges) {
		this.harvestingcharges = harvestingcharges;
	}
	public Integer getTranspcontcode() {
		return transpcontcode;
	}
	public void setTranspcontcode(Integer transpcontcode) {
		this.transpcontcode = transpcontcode;
	}
	public Double getTransportcharges() {
		return transportcharges;
	}
	public void setTransportcharges(Double transportcharges) {
		this.transportcharges = transportcharges;
	}
	public byte getCastatus() {
		return castatus;
	}
	public void setCastatus(byte castatus) {
		this.castatus = castatus;
	}

	

}
