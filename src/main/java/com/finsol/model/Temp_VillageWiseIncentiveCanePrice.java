package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity

public class Temp_VillageWiseIncentiveCanePrice 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String village;
	@Column 
	private String villagecode;
	@Column 
	private Double supqty;
	@Column 
	private Double icpamount;
	@Column 
	private Double rcount;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Double getSupqty() {
		return supqty;
	}
	public void setSupqty(Double supqty) {
		this.supqty = supqty;
	}
	public Double getIcpamount() {
		return icpamount;
	}
	public void setIcpamount(Double icpamount) {
		this.icpamount = icpamount;
	}
	public Double getRcount() {
		return rcount;
	}
	public void setRcount(Double rcount) {
		this.rcount = rcount;
	}
	

}
