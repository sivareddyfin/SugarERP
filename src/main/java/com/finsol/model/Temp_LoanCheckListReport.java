package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity
public class Temp_LoanCheckListReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private Double disbursedamount;
	@Column 
	private String branch;
	@Column 
	private String disburseddate;
	@Column
	private String referencenumber;
	
	@Column 
	private Integer loannumber;
	
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getDisbursedamount() {
		return disbursedamount;
	}
	public void setDisbursedamount(Double disbursedamount) {
		this.disbursedamount = disbursedamount;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDisburseddate() {
		return disburseddate;
	}
	public void setDisburseddate(String disburseddate) {
		this.disburseddate = disburseddate;
	}
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}
	
	
	
	

}
