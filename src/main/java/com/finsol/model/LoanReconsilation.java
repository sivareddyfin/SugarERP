package com.finsol.model;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author DMurty    
 */

@Entity
public class LoanReconsilation
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	
	@Column
	private String ryotname;
	
	@Column
	private Double suppqty;
	
	@Column
	private Double totalamt;
	
	@Column
	private Double cdcamt;
	
	@Column
	private Double ulamt;
	
	@Column
	private Double harvesting;
	
	@Column
	private Double transport;
	
	@Column
	private Double otherded;
	
	@Column
	private Double advamt;	
	
	@Column
	private Double loantaken;	
	
	@Column
	private Double loanrecovered;	
	
	@Column
	private Double sbac;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public Double getSuppqty() {
		return suppqty;
	}

	public void setSuppqty(Double suppqty) {
		this.suppqty = suppqty;
	}

	public Double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(Double totalamt) {
		this.totalamt = totalamt;
	}

	public Double getCdcamt() {
		return cdcamt;
	}

	public void setCdcamt(Double cdcamt) {
		this.cdcamt = cdcamt;
	}

	public Double getUlamt() {
		return ulamt;
	}

	public void setUlamt(Double ulamt) {
		this.ulamt = ulamt;
	}

	public Double getHarvesting() {
		return harvesting;
	}

	public void setHarvesting(Double harvesting) {
		this.harvesting = harvesting;
	}

	public Double getTransport() {
		return transport;
	}

	public void setTransport(Double transport) {
		this.transport = transport;
	}

	public Double getOtherded() {
		return otherded;
	}

	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}

	public Double getAdvamt() {
		return advamt;
	}

	public void setAdvamt(Double advamt) {
		this.advamt = advamt;
	}

	

	public Double getLoantaken() {
		return loantaken;
	}

	public void setLoantaken(Double loantaken) {
		this.loantaken = loantaken;
	}

	public Double getLoanrecovered() {
		return loanrecovered;
	}

	public void setLoanrecovered(Double loanrecovered) {
		this.loanrecovered = loanrecovered;
	}

	public Double getSbac() {
		return sbac;
	}

	public void setSbac(Double sbac) {
		this.sbac = sbac;
	}
	
}
