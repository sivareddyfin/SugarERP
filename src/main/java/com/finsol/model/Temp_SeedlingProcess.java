package com.finsol.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_SeedlingProcess
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column 
	private String sdate;
	@Column 
	private Double budcutting;
	@Column 
	private Double treatmentandhardning;
	@Column 
	private Double trayfilling;
	@Column 
	private Double trayshifting;
	@Column 
	private Double irrigation;
	@Column 
	private Double trimming;
	@Column 
	private String machine;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public Double getBudcutting() {
		return budcutting;
	}
	public void setBudcutting(Double budcutting) {
		this.budcutting = budcutting;
	}
	public Double getTreatmentandhardning() {
		return treatmentandhardning;
	}
	public void setTreatmentandhardning(Double treatmentandhardning) {
		this.treatmentandhardning = treatmentandhardning;
	}
	public Double getTrayfilling() {
		return trayfilling;
	}
	public void setTrayfilling(Double trayfilling) {
		this.trayfilling = trayfilling;
	}
	public Double getTrayshifting() {
		return trayshifting;
	}
	public void setTrayshifting(Double trayshifting) {
		this.trayshifting = trayshifting;
	}
	public Double getIrrigation() {
		return irrigation;
	}
	public void setIrrigation(Double irrigation) {
		this.irrigation = irrigation;
	}
	public Double getTrimming() {
		return trimming;
	}
	public void setTrimming(Double trimming) {
		this.trimming = trimming;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}
	
	

}
