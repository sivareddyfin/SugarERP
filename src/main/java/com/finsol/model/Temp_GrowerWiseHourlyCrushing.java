package com.finsol.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_GrowerWiseHourlyCrushing
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer receiptno;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String variety;
	@Column
	private String village;
	@Column
	private Double grossweight;
	@Column
	private Integer bridgeno;
	@Column
	private String bridgetime;
	@Column
	private String type;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getReceiptno() {
		return receiptno;
	}
	public void setReceiptno(Integer receiptno) {
		this.receiptno = receiptno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public Double getGrossweight() {
		return grossweight;
	}
	public void setGrossweight(Double grossweight) {
		this.grossweight = grossweight;
	}
	public Integer getBridgeno() {
		return bridgeno;
	}
	public void setBridgeno(Integer bridgeno) {
		this.bridgeno = bridgeno;
	}
	public String getBridgetime() {
		return bridgetime;
	}
	public void setBridgetime(String bridgetime) {
		this.bridgetime = bridgetime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
