package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author DMurty
 *
 */
@Entity
public class CaneValueDetailsByRyotAndDate
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(nullable = false)
	private String season;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column
	@Type(type="date")
	private Date canesupplieddt;
	
	@Column
	private double suppliedcanewt;
	
	@Column
	private String calculationitem;
	
	@Column
	private double canevalue;
	
	@Column
	private Byte isaccruedanddue;
	
	@Column
	private double rate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public Date getCanesupplieddt() {
		return canesupplieddt;
	}

	public void setCanesupplieddt(Date canesupplieddt) {
		this.canesupplieddt = canesupplieddt;
	}

	public double getSuppliedcanewt() {
		return suppliedcanewt;
	}

	public void setSuppliedcanewt(double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}

	public String getCalculationitem() {
		return calculationitem;
	}

	public void setCalculationitem(String calculationitem) {
		this.calculationitem = calculationitem;
	}

	public double getCanevalue() {
		return canevalue;
	}

	public void setCanevalue(double canevalue) {
		this.canevalue = canevalue;
	}

	public Byte getIsaccruedanddue() {
		return isaccruedanddue;
	}

	public void setIsaccruedanddue(Byte isaccruedanddue) {
		this.isaccruedanddue = isaccruedanddue;
	}
}
