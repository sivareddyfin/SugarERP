package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class HCSuretyDetails {

	private static final long serialVersionUID = 1L;	

	private Integer id;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column
	private Integer harvestercode;
	
	@Column
	private String surety;
	@Column
	private String contactno;
	@Column
	private String aadhaarnumber;
	@Column
	private String panno;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getHarvestercode() {
		return harvestercode;
	}
	public void setHarvestercode(Integer harvestercode) {
		this.harvestercode = harvestercode;
	}
	public String getSurety() {
		return surety;
	}
	public void setSurety(String surety) {
		this.surety = surety;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	public String getAadhaarnumber() {
		return aadhaarnumber;
	}
	public void setAadhaarnumber(String aadhaarnumber) {
		this.aadhaarnumber = aadhaarnumber;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	
	
	
}
