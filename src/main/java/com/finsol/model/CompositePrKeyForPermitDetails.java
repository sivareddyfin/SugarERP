package com.finsol.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CompositePrKeyForPermitDetails implements Serializable
{	

	@Column
	private Integer permitnumber;
	       
	        
	@Column
	private Integer programno;


	public Integer getPermitnumber() {
		return permitnumber;
	}


	public void setPermitnumber(Integer permitnumber) {
		this.permitnumber = permitnumber;
	}


	public Integer getProgramno() {
		return programno;
	}


	public void setProgramno(Integer programno) {
		this.programno = programno;
	}
	

}
