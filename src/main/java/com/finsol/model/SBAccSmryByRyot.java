package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class SBAccSmryByRyot {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private String ryotcode;
	@Column
	private Integer sbcode;
	@Column
	private Double amountpayable;
	@Column
	private Double amountpaid;
	@Column
	private Double pendingamount;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getSbcode() {
		return sbcode;
	}
	public void setSbcode(Integer sbcode) {
		this.sbcode = sbcode;
	}
	public Double getAmountpayable() {
		return amountpayable;
	}
	public void setAmountpayable(Double amountpayable) {
		this.amountpayable = amountpayable;
	}
	public Double getAmountpaid() {
		return amountpaid;
	}
	public void setAmountpaid(Double amountpaid) {
		this.amountpaid = amountpaid;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	
	


}
