package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class TransportingRateDetails 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	
	@Column(nullable = false)
	private String season;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date effectivedate;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column(nullable = false)
	private Integer transportcontcode;
	
	@Column(nullable = false)
	private double slno;
	
	@Column(nullable = false)
	private double rate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Date getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(Date effectivedate) {
		this.effectivedate = effectivedate;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public Integer getTransportcontcode() {
		return transportcontcode;
	}

	public void setTransportcontcode(Integer transportcontcode) {
		this.transportcontcode = transportcontcode;
	}

	public double getSlno() {
		return slno;
	}

	public void setSlno(double slno) {
		this.slno = slno;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
}
