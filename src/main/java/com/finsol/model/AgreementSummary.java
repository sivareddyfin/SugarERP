package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class AgreementSummary 
{
	
	/*@EmbeddedId
	private CompositePrKeyForAgreementSummary compositePrKeyForAgreementSummary;
	
	
	public CompositePrKeyForAgreementSummary getCompositePrKeyForAgreementSummary()
	{
		return compositePrKeyForAgreementSummary;
	}
	public void setCompositePrKeyForAgreementSummary(CompositePrKeyForAgreementSummary compositePrKeyForAgreementSummary)
	{
		this.compositePrKeyForAgreementSummary = compositePrKeyForAgreementSummary;
	}*/
	@Id
	@Column (nullable = false)
	private String agreementnumber;
	@Column
	@Type(type="date")
	private Date agreementdate;
	@Column
	private String ryotcode;
	@Column
	private Integer mandalcode;
	@Column
	private Integer circlecode;
	@Column
	private Integer branchcode;
	@Column
	private String accountnumber;
	@Column
	private Byte status;
	@Column
	private Byte hasfamilygroup;
	@Column
	private Integer familygroupcode;
	@Column
	private String remarks;
	@Column
	private String loginId;
	@Column
	private Integer agreementseqno;          
	@Column
	private String seasonyear;
	
	@Column
	private String surityPerson;
	
	@Column
	private String surityPersonName;
	
	@Column
	private String surityPersonaadhaarNumber;
	

	

	public String getSurityPerson() {
		return surityPerson;
	}

	public void setSurityPerson(String surityPerson) {
		this.surityPerson = surityPerson;
	}

	public String getSurityPersonName() {
		return surityPersonName;
	}

	public void setSurityPersonName(String surityPersonName) {
		this.surityPersonName = surityPersonName;
	}

	

	public String getSurityPersonaadhaarNumber() {
		return surityPersonaadhaarNumber;
	}

	public void setSurityPersonaadhaarNumber(String surityPersonaadhaarNumber) {
		this.surityPersonaadhaarNumber = surityPersonaadhaarNumber;
	}

	public Integer getAgreementseqno() 
	{
		return agreementseqno;
	}

	public void setAgreementseqno(Integer agreementseqno)
	{
		this.agreementseqno = agreementseqno;
	}


	public String getSeasonyear() 
	{
		return seasonyear;
	}


	public void setSeasonyear(String seasonyear) 
	{
		this.seasonyear = seasonyear;
	}
	
	public String getAgreementnumber()
	{
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber)
	{
		this.agreementnumber = agreementnumber;
	}
	public Date getAgreementdate() 
	{
		return agreementdate;
	}
	public void setAgreementdate(Date agreementdate)
	{
		this.agreementdate = agreementdate;
	}
	public String getRyotcode()
	{
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getMandalcode() {
		return mandalcode;
	}
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	
	public Byte getHasfamilygroup() {
		return hasfamilygroup;
	}
	public void setHasfamilygroup(Byte hasfamilygroup) {
		this.hasfamilygroup = hasfamilygroup;
	}
	public Integer getFamilygroupcode() {
		return familygroupcode;
	}
	public void setFamilygroupcode(Integer familygroupcode) {
		this.familygroupcode = familygroupcode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
	

}
