package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

@Entity
public class AdvancePrincipleAmounts 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private String ryotcode;
	@Column
	private Integer advancecode;
	@Column
	private Double principle;
	@Column
	@Type(type="date")
	private Date principledate;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getAdvancecode() {
		return advancecode;
	}
	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}
	public Double getPrinciple() {
		return principle;
	}
	public void setPrinciple(Double principle) {
		this.principle = principle;
	}
	public Date getPrincipledate() {
		return principledate;
	}
	public void setPrincipledate(Date principledate) {
		this.principledate = principledate;
	}
	
	


}
