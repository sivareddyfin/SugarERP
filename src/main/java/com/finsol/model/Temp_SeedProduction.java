package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_SeedProduction
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String variety;
	@Column
	private String batchno;
	@Column
	private Double stubwt;
	@Column
	private String trayfilldate;
	@Column
	private Integer nooftrays;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getBatchno() {
		return batchno;
	}
	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}
	public Double getStubwt() {
		return stubwt;
	}
	public void setStubwt(Double stubwt) {
		this.stubwt = stubwt;
	}
	public String getTrayfilldate() {
		return trayfilldate;
	}
	public void setTrayfilldate(String trayfilldate) {
		this.trayfilldate = trayfilldate;
	}
	public Integer getNooftrays() {
		return nooftrays;
	}
	public void setNooftrays(Integer nooftrays) {
		this.nooftrays = nooftrays;
	}
	
	
	
	

}
