package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class PermitRules {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(nullable = false)
	private Integer nooftonsperacre;
	
	@Column(nullable = false)
	private Integer nooftonspereachpermit;
	
	@Column
	private Byte permittype;
	@Column
	private Integer noofpermitperacre;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNooftonsperacre() {
		return nooftonsperacre;
	}

	public void setNooftonsperacre(Integer nooftonsperacre) {
		this.nooftonsperacre = nooftonsperacre;
	}

	public Integer getNooftonspereachpermit() {
		return nooftonspereachpermit;
	}

	public void setNooftonspereachpermit(Integer nooftonspereachpermit) {
		this.nooftonspereachpermit = nooftonspereachpermit;
	}

	public Byte getPermittype()
	{
		return permittype;
	}

	public void setPermittype(Byte permittype)
	{
		this.permittype = permittype;
	}

	public Integer getNoofpermitperacre() 
	{
		return noofpermitperacre;
	}

	public void setNoofpermitperacre(Integer noofpermitperacre)
	{
		this.noofpermitperacre = noofpermitperacre;
	}

	
	
}
