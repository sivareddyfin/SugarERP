package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_VehicleTypeWiseCaneCrushingShiftsWise
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String vehicletype;
	@Column
	private Double netwt1;
	@Column
	private Double netwt2;
	@Column
	private Double netwt3;
	@Column
	private Integer nooftrips1;
	@Column
	private Integer nooftrips2;
	@Column
	private Integer nooftrips3;
	@Column
	private Double average;
	@Column
	private Integer zonecode;
	@Column
	private String zone;
	
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}
	public Double getNetwt1() {
		return netwt1;
	}
	public void setNetwt1(Double netwt1) {
		this.netwt1 = netwt1;
	}
	public Double getNetwt2() {
		return netwt2;
	}
	public void setNetwt2(Double netwt2) {
		this.netwt2 = netwt2;
	}
	public Double getNetwt3() {
		return netwt3;
	}
	public void setNetwt3(Double netwt3) {
		this.netwt3 = netwt3;
	}
	public Integer getNooftrips1() {
		return nooftrips1;
	}
	public void setNooftrips1(Integer nooftrips1) {
		this.nooftrips1 = nooftrips1;
	}
	public Integer getNooftrips2() {
		return nooftrips2;
	}
	public void setNooftrips2(Integer nooftrips2) {
		this.nooftrips2 = nooftrips2;
	}
	public Integer getNooftrips3() {
		return nooftrips3;
	}
	public void setNooftrips3(Integer nooftrips3) {
		this.nooftrips3 = nooftrips3;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	
	
}
