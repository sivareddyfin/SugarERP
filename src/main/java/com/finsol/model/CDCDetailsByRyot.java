package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CDCDetailsByRyot {
	
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	private  String ryotcode;
	@Column
	private Double suppliedcanewt;
	@Column
	private Double  cdcrate;
	@Column
	private Double  cdcamount;
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getCdcrate() {
		return cdcrate;
	}
	public void setCdcrate(Double cdcrate) {
		this.cdcrate = cdcrate;
	}
	public Double getCdcamount() {
		return cdcamount;
	}
	public void setCdcamount(Double cdcamount) {
		this.cdcamount = cdcamount;
	}
	
	


}
