package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class TranspChgSmryByRyotAndCont {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	private  String ryotcode; 
	@Column
	private  Integer transportcontcode;
	@Column
	private  String villagecode;
	@Column
	private  Double transportedcanewt;
	@Column
	private  Double totalextent;
	@Column
	private  Double totalamount;
	@Column
	private  Double totalpayableamount;
	@Column
	private  Double pendingpayable;
	@Column
	private  Double companypaidamount;
	@Column
	private  Double pendingamount;
	@Column
	private  Double mincanewt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Integer getTransportcontcode() {
		return transportcontcode;
	}
	public void setTransportcontcode(Integer transportcontcode) {
		this.transportcontcode = transportcontcode;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Double getTransportedcanewt() {
		return transportedcanewt;
	}
	public void setTransportedcanewt(Double transportedcanewt) {
		this.transportedcanewt = transportedcanewt;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getTotalpayableamount() {
		return totalpayableamount;
	}
	public void setTotalpayableamount(Double totalpayableamount) {
		this.totalpayableamount = totalpayableamount;
	}
	public Double getPendingpayable() {
		return pendingpayable;
	}
	public void setPendingpayable(Double pendingpayable) {
		this.pendingpayable = pendingpayable;
	}
	public Double getCompanypaidamount() {
		return companypaidamount;
	}
	public void setCompanypaidamount(Double companypaidamount) {
		this.companypaidamount = companypaidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public Double getMincanewt() {
		return mincanewt;
	}
	public void setMincanewt(Double mincanewt) {
		this.mincanewt = mincanewt;
	}
}
