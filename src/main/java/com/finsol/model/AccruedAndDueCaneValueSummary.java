package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author DMurty
 *
 */
@Entity
public class AccruedAndDueCaneValueSummary
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(nullable = false)
	private String season;
	
	@Column(nullable = false)
	private String villagecode;
	
	@Column(nullable = false)
	private int ryotcodeseqno;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column
	private double suppliedcanewt;
	
	@Column
	private double totalvalue;
	
	@Column
	private double paidamount;
	
	@Column
	private double dueamount;

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public int getRyotcodeseqno() {
		return ryotcodeseqno;
	}

	public void setRyotcodeseqno(int ryotcodeseqno) {
		this.ryotcodeseqno = ryotcodeseqno;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public double getSuppliedcanewt() {
		return suppliedcanewt;
	}

	public void setSuppliedcanewt(double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}

	public double getTotalvalue() {
		return totalvalue;
	}

	public void setTotalvalue(double totalvalue) {
		this.totalvalue = totalvalue;
	}

	public double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(double paidamount) {
		this.paidamount = paidamount;
	}

	public double getDueamount() {
		return dueamount;
	}

	public void setDueamount(double dueamount) {
		this.dueamount = dueamount;
	}
}
