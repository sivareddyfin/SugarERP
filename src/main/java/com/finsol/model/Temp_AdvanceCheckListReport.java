package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity
public class Temp_AdvanceCheckListReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private Double advanceamount;
	@Column 
	private String advancedate;
	
	@Column 
	private Integer circlecode;
	
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	@Column 
	private String advance;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Double getAdvanceamount() {
		return advanceamount;
	}
	public void setAdvanceamount(Double advanceamount) {
		this.advanceamount = advanceamount;
	}
	public String getAdvancedate() {
		return advancedate;
	}
	public void setAdvancedate(String advancedate) {
		this.advancedate = advancedate;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	

}
