package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class Season 
{
	private static final long serialVersionUID = 1L;
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 
	 @Column(unique = true,nullable = false)
	 private Integer seasonid;
	 
	 @Column
	 private String season;
	 
	 @Column
	 private Integer fromyear;
	 
	 @Column
	 private Integer toyear;
	 
	 @Column
	 private String description;
	 
	 @Column
	 private String status;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getSeasonid()
	{
		return seasonid;
	}

	public void setSeasonid(Integer seasonid)
	{
		this.seasonid = seasonid;
	}

	public String getSeason() 
	{
		return season;
	}

	public void setSeason(String season) 
	{
		this.season = season;
	}

	public Integer getFromyear() 
	{
		return fromyear;
	}

	public void setFromyear(Integer fromyear)
	{
		this.fromyear = fromyear;
	}

	public Integer getToyear()
	{
		return toyear;
	}

	public void setToyear(Integer toyear)
	{
		this.toyear = toyear;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}	
}
