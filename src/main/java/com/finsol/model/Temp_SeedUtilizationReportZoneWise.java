package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_SeedUtilizationReportZoneWise
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String supplier;
	@Column 
	private String suppliername;
	@Column 
	private String village;
	@Column 
	private String variety;
	@Column 
	private Double qty;
	@Column 
	private String consumer;
	@Column 
	private String consumername;
	@Column 
	private Double nooftons;
	@Column 
	private String dateofapproval;
	@Column 
	private Double transportvalue;
	@Column 
	private Double noofacre;
	@Column 
	private Double tfinalweight;
	@Column 
	private Double tnoofacre;
	@Column 
	private String consumervillage;
	@Column 
	private Double value;
	
	@Column 
	private Integer circlecode;
	@Column 
	private Integer zonecode;
	@Column 
	private String circle;
	@Column 
	private String zone;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getConsumer() {
		return consumer;
	}
	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}
	public String getConsumername() {
		return consumername;
	}
	public void setConsumername(String consumername) {
		this.consumername = consumername;
	}
	public Double getNooftons() {
		return nooftons;
	}
	public void setNooftons(Double nooftons) {
		this.nooftons = nooftons;
	}
	public String getDateofapproval() {
		return dateofapproval;
	}
	public void setDateofapproval(String dateofapproval) {
		this.dateofapproval = dateofapproval;
	}
	public Double getTransportvalue() {
		return transportvalue;
	}
	public void setTransportvalue(Double transportvalue) {
		this.transportvalue = transportvalue;
	}
	public Double getNoofacre() {
		return noofacre;
	}
	public void setNoofacre(Double noofacre) {
		this.noofacre = noofacre;
	}
	public Double getTfinalweight() {
		return tfinalweight;
	}
	public void setTfinalweight(Double tfinalweight) {
		this.tfinalweight = tfinalweight;
	}
	public Double getTnoofacre() {
		return tnoofacre;
	}
	public void setTnoofacre(Double tnoofacre) {
		this.tnoofacre = tnoofacre;
	}
	public String getConsumervillage() {
		return consumervillage;
	}
	public void setConsumervillage(String consumervillage) {
		this.consumervillage = consumervillage;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	

}
