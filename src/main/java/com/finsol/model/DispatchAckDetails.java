package com.finsol.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class DispatchAckDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column
	private String season;
	@Column
	private Integer variety;
	@Column
	private String indentNo;
	@Column
	private String dispatchNo;
	@Column
	private String batch;
	@Column
	private Double noOfSeedlings;
	@Column
	private Double seedlingCost;
	@Column
	private Double totalSeedlingCost;
	@Column
	private Double noOfTrays;
	@Column
	private Double trayCost;
	@Column
	private Double trayTotalCost;
	@Column
	private Integer trayTypeCode;
	@Column
	private String 	dispatchedStatus;
	@Column
	private String kind;
	
	
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public Integer getSlNo()
	{
		return slNo;
	}
	
	public String getSeason() {
		return season;
	}
	public Integer getVariety() {
		return variety;
	}
	public String getIndentNo() {
		return indentNo;
	}
	public String getDispatchNo() {
		return dispatchNo;
	}
	public String getBatch() {
		return batch;
	}
	public Double getNoOfSeedlings() {
		return noOfSeedlings;
	}
	public Double getSeedlingCost() {
		return seedlingCost;
	}
	public Double getTotalSeedlingCost() {
		return totalSeedlingCost;
	}
	public Double getNoOfTrays() {
		return noOfTrays;
	}
	public Double getTrayCost() {
		return trayCost;
	}
	public Double getTrayTotalCost() {
		return trayTotalCost;
	}
	public Integer getTrayTypeCode() {
		return trayTypeCode;
	}
	
	public String getDispatchedStatus() {
		return dispatchedStatus;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	
	public void setSeason(String season) {
		this.season = season;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public void setNoOfSeedlings(Double noOfSeedlings) {
		this.noOfSeedlings = noOfSeedlings;
	}
	public void setSeedlingCost(Double seedlingCost) {
		this.seedlingCost = seedlingCost;
	}
	public void setTotalSeedlingCost(Double totalSeedlingCost) {
		this.totalSeedlingCost = totalSeedlingCost;
	}
	public void setNoOfTrays(Double noOfTrays) {
		this.noOfTrays = noOfTrays;
	}
	public void setTrayCost(Double trayCost) {
		this.trayCost = trayCost;
	}
	public void setTrayTotalCost(Double trayTotalCost) {
		this.trayTotalCost = trayTotalCost;
	}
	public void setTrayTypeCode(Integer trayTypeCode) {
		this.trayTypeCode = trayTypeCode;
	}
	public void setDispatchedStatus(String dispatchedStatus) {
		this.dispatchedStatus = dispatchedStatus;
	}

	
	
	
}
