package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */

@Entity
public class AdvanceSubCategory 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private String subcategorycode;
		
	@Column
	private String name;
	
	@Column 
	private String description;
	
	@Column
	private Integer advanceCode;
	
	@Column
	private Integer advanceCategoryCode;
	
	@Column
	private Byte status;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	
	public String getSubcategorycode() 
	{
		return subcategorycode;
	}

	public void setSubcategorycode(String subcategorycode)
	{
		this.subcategorycode = subcategorycode;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getAdvanceCode()
	{
		return advanceCode;
	}

	public void setAdvanceCode(Integer advanceCode) 
	{
		this.advanceCode = advanceCode;
	}

	public Integer getAdvanceCategoryCode() 
	{
		return advanceCategoryCode;
	}

	public void setAdvanceCategoryCode(Integer advanceCategoryCode)
	{
		this.advanceCategoryCode = advanceCategoryCode;
	}

	public Byte getStatus()
	{
		return status;
	}

	public void setStatus(Byte status)
	{
		this.status = status;
	}

}
