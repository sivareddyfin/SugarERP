package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author naidu
 */

@Entity
public class WeighBridgeTestData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(nullable = false)
	private Integer shiftid;
	
	@Column(nullable = false)
	private Integer weighbridgeid;
	 
	@Column(nullable=false)
	 private String operatorid;
	 
	 @Column(nullable=false)
	 private Double totalweight;
	
	 @Column(nullable = false)
	 @Type(type="date")
	  private Date testweighmentdate;
	 
	 @Column(nullable=false)
	 @Type(type="time")
	 private Time testweighmenttime; 
	 



	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getShiftid() {
		return shiftid;
	}

	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}

	public Integer getWeighbridgeid() {
		return weighbridgeid;
	}

	public void setWeighbridgeid(Integer weighbridgeid) {
		this.weighbridgeid = weighbridgeid;
	}




	public String getOperatorid() {
		return operatorid;
	}

	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}

	public Double getTotalweight() {
		return totalweight;
	}

	public void setTotalweight(Double totalweight) {
		this.totalweight = totalweight;
	}

	public Date getTestweighmentdate() {
		return testweighmentdate;
	}

	public void setTestweighmentdate(Date testweighmentdate) {
		this.testweighmentdate = testweighmentdate;
	}

	public Time getTestweighmenttime() {
		return testweighmenttime;
	}

	public void setTestweighmenttime(Time testweighmenttime) {
		this.testweighmenttime = testweighmenttime;
	}


	  
	  
	 
	
}
