package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_PaymentCheckList
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private Integer zonecode;
	@Column 
	private String zone;
	@Column 
	private Double canesupplied;
	@Column 
	private Double totalvalue;
	@Column 
	private Double cdc;
	@Column 
	private Double ul;
	@Column 
	private Double tp;
	@Column 
	private Double harvesting;
	@Column 
	private Double otherded;
	
	@Column 
	private Double advances;
	@Column 
	private Double loan;
	@Column 
	private Double sb;
	
	@Column 
	private int bank;
	
	@Column 
	private String accountnumber;
	@Column 
	private Integer circlecode;
	@Column 
	private String village;
	
	
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public int getBank() {
		return bank;
	}
	public void setBank(int bank) {
		this.bank = bank;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Double getCanesupplied() {
		return canesupplied;
	}
	public void setCanesupplied(Double canesupplied) {
		this.canesupplied = canesupplied;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getUl() {
		return ul;
	}
	public void setUl(Double ul) {
		this.ul = ul;
	}
	public Double getTp() {
		return tp;
	}
	public void setTp(Double tp) {
		this.tp = tp;
	}
	public Double getHarvesting() {
		return harvesting;
	}
	public void setHarvesting(Double harvesting) {
		this.harvesting = harvesting;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getAdvances() {
		return advances;
	}
	public void setAdvances(Double advances) {
		this.advances = advances;
	}
	public Double getLoan() {
		return loan;
	}
	public void setLoan(Double loan) {
		this.loan = loan;
	}
	public Double getSb() {
		return sb;
	}
	public void setSb(Double sb) {
		this.sb = sb;
	}
	
	
}
