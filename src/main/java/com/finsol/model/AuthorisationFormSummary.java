package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class AuthorisationFormSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	@Type(type="date")
	private Date authorisationDate;
	@Column
	private String ryotCode;
	@Column
	private String ryotName;
	
	@Column
	private String fieldOfficer;
	@Column
	private String indentNumbers;
	@Column
	private String authorisedBy;
	@Column
	private String phoneNo;
	@Column
	private String authorisationSeqNo;
	@Column
	private Double GrandTotal;
	@Column
	private Integer faCode;
	@Column
	private String agreementnumber;
	@Column
	private String storecode;
	@Column
	private Integer ackstatus;
	@Column
	private Integer transactioncode;
	@Column
	private Integer printflag;
	
	
	public Integer getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}
	public Integer getAckstatus() {
		return ackstatus;
	}
	public void setAckstatus(Integer ackstatus) {
		this.ackstatus = ackstatus;
	}
	public String getStorecode() {
		return storecode;
	}
	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}
	public Integer getFaCode() {
		return faCode;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setFaCode(Integer faCode) {
		this.faCode = faCode;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Date getAuthorisationDate() {
		return authorisationDate;
	}
	public String getRyotCode() {
		return ryotCode;
	}
	public String getRyotName() {
		return ryotName;
	}
	
	public String getFieldOfficer() {
		return fieldOfficer;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public String getAuthorisedBy() {
		return authorisedBy;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public Double getGrandTotal() {
		return GrandTotal;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setAuthorisationDate(Date authorisationDate) {
		this.authorisationDate = authorisationDate;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}
	
	public void setFieldOfficer(String fieldOfficer) {
		this.fieldOfficer = fieldOfficer;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public void setAuthorisedBy(String authorisedBy) {
		this.authorisedBy = authorisedBy;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public void setGrandTotal(Double grandTotal) {
		GrandTotal = grandTotal;
	}
	public Integer getPrintflag() {
		return printflag;
	}
	public void setPrintflag(Integer printflag) {
		this.printflag = printflag;
	}
	
	
	
}
