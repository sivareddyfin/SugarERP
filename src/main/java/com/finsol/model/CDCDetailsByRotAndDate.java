package com.finsol.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class CDCDetailsByRotAndDate {
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private  String season;
	@Column
	private Date receiptdate;
	@Column
	private  String ryotcode; 
	@Column
	private Double suppliedcanewt;
	@Column
	private Double  cdcrate;
	@Column
	private Double  cdcamount;
	@Column
	private Byte castatus;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getReceiptdate() {
		return receiptdate;
	}
	public void setReceiptdate(Date receiptdate) {
		this.receiptdate = receiptdate;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getCdcrate() {
		return cdcrate;
	}
	public void setCdcrate(Double cdcrate) {
		this.cdcrate = cdcrate;
	}
	public Double getCdcamount() {
		return cdcamount;
	}
	public void setCdcamount(Double cdcamount) {
		this.cdcamount = cdcamount;
	}
	public Byte getCastatus() {
		return castatus;
	}
	public void setCastatus(Byte castatus) {
		this.castatus = castatus;
	}
	
	


}
