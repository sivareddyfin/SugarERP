package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class SeedAccountingDetails {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer seedactslno;
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private String seedsuppliercode;
	 @Column(nullable = false)
	 private Double extentsize;
	 @Column(nullable = false)
	 private Double totalamount;
	 @Column(nullable = false)
	 private Double payingamount;
	 @Column(nullable = false)
	 private Double loansandadvancespayable;
	 @Column(nullable = false)
	 private Double loansandadvancespaid;
	 @Column(nullable = false)
	 private Double paidtosbac;
	 @Column(nullable = false)
	 private Double seedrate;
	public Integer getSeedactslno() {
		return seedactslno;
	}
	public void setSeedactslno(Integer seedactslno) {
		this.seedactslno = seedactslno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getSeedsuppliercode() {
		return seedsuppliercode;
	}
	public void setSeedsuppliercode(String seedsuppliercode) {
		this.seedsuppliercode = seedsuppliercode;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getPayingamount() {
		return payingamount;
	}
	public void setPayingamount(Double payingamount) {
		this.payingamount = payingamount;
	}
	public Double getLoansandadvancespayable() {
		return loansandadvancespayable;
	}
	public void setLoansandadvancespayable(Double loansandadvancespayable) {
		this.loansandadvancespayable = loansandadvancespayable;
	}
	public Double getLoansandadvancespaid() {
		return loansandadvancespaid;
	}
	public void setLoansandadvancespaid(Double loansandadvancespaid) {
		this.loansandadvancespaid = loansandadvancespaid;
	}
	public Double getPaidtosbac() {
		return paidtosbac;
	}
	public void setPaidtosbac(Double paidtosbac) {
		this.paidtosbac = paidtosbac;
	}
	public Double getSeedrate() {
		return seedrate;
	}
	public void setSeedrate(Double seedrate) {
		this.seedrate = seedrate;
	}
	 
}
