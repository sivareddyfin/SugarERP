package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class AdvanceInterestRates 
{
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column
	private Integer id;
	
	@Column
	private Integer advancecode;
	
	@Column
	private Double fromamount;
	
	@Column
	private Double toamount;
	
	@Column
	private Double interestrate;

	
	public Integer getSlno() 
	{
		return slno;
	}

	public void setSlno(Integer slno) 
	{
		this.slno = slno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdvancecode()
	{
		return advancecode;
	}

	public void setAdvancecode(Integer advancecode)
	{
		this.advancecode = advancecode;
	}

	public Double getFromamount() 
	{
		return fromamount;
	}

	public void setFromamount(Double fromamount)
	{
		this.fromamount = fromamount;
	}

	public Double getToamount() 
	{
		return toamount;
	}

	public void setToamount(Double toamount) 
	{
		this.toamount = toamount;
	}

	public Double getInterestrate()
	{
		return interestrate;
	}

	public void setInterestrate(Double interestrate) 
	{
		this.interestrate = interestrate;
	}
}
