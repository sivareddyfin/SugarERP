package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneReceiptShiftSmry 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	@Type(type="date")
	private Date canereceiptdate;
	
	@Column
	private Integer shiftid;
	
	@Column
	private Integer noofrects;
	@Column
	private Double grosswt;
	@Column
	private Double bindingmtlwt;
	@Column
	private Double netwt;
	@Column
	private Double burnedcanewt;
	@Column
	private Integer nooflorries;
	@Column
	private Integer nooftippers;
	@Column
	private Integer nooftentires;
	@Column
	private Integer nooftwelvetires;
	@Column
	private Integer noofpartloads;
	@Column
	private  String season;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getCanereceiptdate() {
		return canereceiptdate;
	}
	public void setCanereceiptdate(Date canereceiptdate) {
		this.canereceiptdate = canereceiptdate;
	}
	public Integer getShiftid() {
		return shiftid;
	}
	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}
	public Integer getNoofrects() {
		return noofrects;
	}
	public void setNoofrects(Integer noofrects) {
		this.noofrects = noofrects;
	}
	public Double getGrosswt() {
		return grosswt;
	}
	public void setGrosswt(Double grosswt) {
		this.grosswt = grosswt;
	}
	public Double getBindingmtlwt() {
		return bindingmtlwt;
	}
	public void setBindingmtlwt(Double bindingmtlwt) {
		this.bindingmtlwt = bindingmtlwt;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getBurnedcanewt() {
		return burnedcanewt;
	}
	public void setBurnedcanewt(Double burnedcanewt) {
		this.burnedcanewt = burnedcanewt;
	}
	public Integer getNooflorries() {
		return nooflorries;
	}
	public void setNooflorries(Integer nooflorries) {
		this.nooflorries = nooflorries;
	}
	public Integer getNooftippers() {
		return nooftippers;
	}
	public void setNooftippers(Integer nooftippers) {
		this.nooftippers = nooftippers;
	}
	public Integer getNooftentires() {
		return nooftentires;
	}
	public void setNooftentires(Integer nooftentires) {
		this.nooftentires = nooftentires;
	}
	public Integer getNooftwelvetires() {
		return nooftwelvetires;
	}
	public void setNooftwelvetires(Integer nooftwelvetires) {
		this.nooftwelvetires = nooftwelvetires;
	}
	public Integer getNoofpartloads() {
		return noofpartloads;
	}
	public void setNoofpartloads(Integer noofpartloads) {
		this.noofpartloads = noofpartloads;
	}

	
	

}
