package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class Temp_TransportDeductionsForCane 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String transporter;
	@Column
	private Double snumber;
	@Column
	private Double rate;
	@Column
	private Double totalamount;
	@Column
	private Double transportedcanewt;
	@Column
	private Double amountpayed;
	@Column
	private Integer transportcontCode;
	
	
	public Integer getTransportcontCode() {
		return transportcontCode;
	}
	public void setTransportcontCode(Integer transportcontCode) {
		this.transportcontCode = transportcontCode;
	}
	public Double getAmountpayed() {
		return amountpayed;
	}
	public void setAmountpayed(Double amountpayed) {
		this.amountpayed = amountpayed;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getTransporter() {
		return transporter;
	}
	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}
	
	public Double getSnumber() {
		return snumber;
	}
	public void setSnumber(Double snumber) {
		this.snumber = snumber;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getTransportedcanewt() {
		return transportedcanewt;
	}
	public void setTransportedcanewt(Double transportedcanewt) {
		this.transportedcanewt = transportedcanewt;
	}
	
}
