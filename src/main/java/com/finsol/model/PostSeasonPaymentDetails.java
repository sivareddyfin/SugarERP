package com.finsol.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class PostSeasonPaymentDetails {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer pssino;
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private String canesuppliercode;
	 @Column(nullable = false)
	 private Double extentsize;
	 @Column(nullable = false)
	 private Double totalamount;
	 @Column(nullable = false)
	 private Double payingamount;
	 @Column(nullable = false)
	 private Double loansandadvancespayable;
	 @Column(nullable = false)
	 private Double loansandadvancespaid;
	 @Column(nullable = false)
	 private Double paidtosbac;
	
	public Integer getPssino() {
		return pssino;
	}
	public void setPssino(Integer pssino) {
		this.pssino = pssino;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getCanesuppliercode() {
		return canesuppliercode;
	}
	public void setCanesuppliercode(String canesuppliercode) {
		this.canesuppliercode = canesuppliercode;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getPayingamount() {
		return payingamount;
	}
	public void setPayingamount(Double payingamount) {
		this.payingamount = payingamount;
	}
	public Double getLoansandadvancespayable() {
		return loansandadvancespayable;
	}
	public void setLoansandadvancespayable(Double loansandadvancespayable) {
		this.loansandadvancespayable = loansandadvancespayable;
	}
	public Double getLoansandadvancespaid() {
		return loansandadvancespaid;
	}
	public void setLoansandadvancespaid(Double loansandadvancespaid) {
		this.loansandadvancespaid = loansandadvancespaid;
	}
	public Double getPaidtosbac() {
		return paidtosbac;
	}
	public void setPaidtosbac(Double paidtosbac) {
		this.paidtosbac = paidtosbac;
	}
	 
}
