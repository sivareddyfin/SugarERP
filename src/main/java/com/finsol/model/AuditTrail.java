package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class AuditTrail {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private Integer screenid;
	
	@Column
	private String fieldid;
	
	@Column
	private String userid;
	
	@Column
	@Type(type="date")
	private Date audittraildate;
	
	@Column
	private Time audittrailtime;
	
	@Column
	private String oldvalue;
	
	@Column
	private String newvalue;
	
	@Column
	private String action;
	
	@Column
	private String remarks;
	
	
	@Column
	private String screenname;
	
	


	public String getScreenname() {
		return screenname;
	}


	public void setScreenname(String screenname) {
		this.screenname = screenname;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getScreenid() {
		return screenid;
	}


	public void setScreenid(Integer screenid) {
		this.screenid = screenid;
	}


	public String getFieldid() {
		return fieldid;
	}


	public void setFieldid(String fieldid) {
		this.fieldid = fieldid;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public Date getAudittraildate() {
		return audittraildate;
	}


	public void setAudittraildate(Date audittraildate) {
		this.audittraildate = audittraildate;
	}


	public Time getAudittrailtime() {
		return audittrailtime;
	}


	public void setAudittrailtime(Time audittrailtime) {
		this.audittrailtime = audittrailtime;
	}


	public String getOldvalue() {
		return oldvalue;
	}


	public void setOldvalue(String oldvalue) {
		this.oldvalue = oldvalue;
	}


	public String getNewvalue() {
		return newvalue;
	}


	public void setNewvalue(String newvalue) {
		this.newvalue = newvalue;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	@Override
    public String toString()
    {
        return "AuditTrail [screenid=" + screenid + ", fieldid=" + fieldid + ", userid=" + userid + ", audittraildate=" + audittraildate + ", audittrailtime=" + audittrailtime + ", oldvalue=" + oldvalue + ",newvalue=" + newvalue + ", action=" + action + "]";
    }
	
}
