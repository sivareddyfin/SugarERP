package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity
public class StockReqDtls 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String stockreqno;
	@Column
    private String finyr;
	@Column
    private String productcode;
	@Column
    private String productgrpcode;
	@Column
    private String productsubgroupcode;
	@Column
    private String uom;
	@Column(nullable = false)
    private String purpose;
	@Column
    private Double quantity;
	@Column
    private Double issuedqty;
	@Column
    private Double pendingqty;
	@Column
    private Double deptstock;
	@Column
    private Double csstock;
	@Column
    private Integer status;
	@Column
    private String consumption;
	
	public String getConsumption() {
		return consumption;
	}
	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}
	public Integer getId() {
		return id;
	}
	public String getStockreqno() {
		return stockreqno;
	}
	public String getFinyr() {
		return finyr;
	}
	public String getProductcode() {
		return productcode;
	}
	public String getProductgrpcode() {
		return productgrpcode;
	}
	public String getProductsubgroupcode() {
		return productsubgroupcode;
	}
	public String getUom() {
		return uom;
	}
	public String getPurpose() {
		return purpose;
	}
	public Double getQuantity() {
		return quantity;
	}
	public Double getIssuedqty() {
		return issuedqty;
	}
	public Double getPendingqty() {
		return pendingqty;
	}
	public Double getDeptstock() {
		return deptstock;
	}
	public Double getCsstock() {
		return csstock;
	}
	public Integer getStatus() {
		return status;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setStockreqno(String stockreqno) {
		this.stockreqno = stockreqno;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public void setProductgrpcode(String productgrpcode) {
		this.productgrpcode = productgrpcode;
	}
	public void setProductsubgroupcode(String productsubgroupcode) {
		this.productsubgroupcode = productsubgroupcode;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public void setIssuedqty(Double issuedqty) {
		this.issuedqty = issuedqty;
	}
	public void setPendingqty(Double pendingqty) {
		this.pendingqty = pendingqty;
	}
	public void setDeptstock(Double deptstock) {
		this.deptstock = deptstock;
	}
	public void setCsstock(Double csstock) {
		this.csstock = csstock;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	

}
