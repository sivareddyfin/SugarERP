package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 *
 */
@Entity
public class Temp_UnloadContractorReport {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String vehiclenumber;
	@Column
	private Integer recieptnumber;
	@Column
	private String shiftname;
	@Column
	private String weighmentDate;
	@Column
	private Double unloadingqty;
	@Column
	private Double unloadinamount;
	@Column
	private String ulname;
	@Column
	private Integer ulcode;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVehiclenumber() {
		return vehiclenumber;
	}
	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}
	public Integer getRecieptnumber() {
		return recieptnumber;
	}
	public void setRecieptnumber(Integer recieptnumber) {
		this.recieptnumber = recieptnumber;
	}
	public String getShiftname() {
		return shiftname;
	}
	public void setShiftname(String shiftname) {
		this.shiftname = shiftname;
	}
	public String getWeighmentDate() {
		return weighmentDate;
	}
	public void setWeighmentDate(String weighmentDate) {
		this.weighmentDate = weighmentDate;
	}
	public Double getUnloadingqty() {
		return unloadingqty;
	}
	public void setUnloadingqty(Double unloadingqty) {
		this.unloadingqty = unloadingqty;
	}
	public Double getUnloadinamount() {
		return unloadinamount;
	}
	public void setUnloadinamount(Double unloadinamount) {
		this.unloadinamount = unloadinamount;
	}
	public String getUlname() {
		return ulname;
	}
	public void setUlname(String ulname) {
		this.ulname = ulname;
	}
	public Integer getUlcode() {
		return ulcode;
	}
	public void setUlcode(Integer ulcode) {
		this.ulcode = ulcode;
	}
	
}
