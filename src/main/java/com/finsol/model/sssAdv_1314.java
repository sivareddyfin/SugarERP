package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class sssAdv_1314 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String lm_ryot;
	@Column
	private Integer lm_bk_cod;
	@Column
	private String description;
	@Column
	private Double lm_amt_rec;
	@Column
	private String lm_date;
	@Column
	private String fromyr;
	@Column
	private String toyr;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLm_ryot() {
		return lm_ryot;
	}
	public void setLm_ryot(String lm_ryot) {
		this.lm_ryot = lm_ryot;
	}
	public Integer getLm_bk_cod() {
		return lm_bk_cod;
	}
	public void setLm_bk_cod(Integer lm_bk_cod) {
		this.lm_bk_cod = lm_bk_cod;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getLm_amt_rec() {
		return lm_amt_rec;
	}
	public void setLm_amt_rec(Double lm_amt_rec) {
		this.lm_amt_rec = lm_amt_rec;
	}
	public String getLm_date() {
		return lm_date;
	}
	public void setLm_date(String lm_date) {
		this.lm_date = lm_date;
	}
	public String getFromyr() {
		return fromyr;
	}
	public void setFromyr(String fromyr) {
		this.fromyr = fromyr;
	}
	public String getToyr() {
		return toyr;
	}
	public void setToyr(String toyr) {
		this.toyr = toyr;
	}
	

}
