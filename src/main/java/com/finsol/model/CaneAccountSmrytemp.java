package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneAccountSmrytemp {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer caneacslno;
	@Column
	private String season;
	@Column
	@Type(type="date")
	private Date datefrom;
	@Column
	@Type(type="date")
	private Date dateto;
	@Column
	private String login;
	@Type(type="date")
	private Date dateofcalc;
	
	//Added by DMurty on 01-03-2017
	//Here acctype is to identify which transaction it is.
	//1--- Cane Accounting
	//2--- Advances payment from Ryots in form of cash
	//3--- Loan Payment from Ryots in form of cash
	//4--- Transport Allowance from Company to Ryots
	@Column
	private byte acctype;

	public byte getAcctype() {
		return acctype;
	}
	public void setAcctype(byte acctype) {
		this.acctype = acctype;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCaneacslno() {
		return caneacslno;
	}
	public void setCaneacslno(Integer caneacslno) {
		this.caneacslno = caneacslno;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getDatefrom() {
		return datefrom;
	}
	public void setDatefrom(Date datefrom) {
		this.datefrom = datefrom;
	}
	public Date getDateto() {
		return dateto;
	}
	public void setDateto(Date dateto) {
		this.dateto = dateto;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Date getDateofcalc() {
		return dateofcalc;
	}
	public void setDateofcalc(Date dateofcalc) {
		this.dateofcalc = dateofcalc;
	}

}
