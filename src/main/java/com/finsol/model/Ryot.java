package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class Ryot {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private String ryotcode;
	
	@Column(nullable = false)
	private String salutation;
	
	@Column(nullable = false)
	private String ryotname;
	
	@Column(nullable = false)
	private Integer mandalcode;
	
	@Column(nullable = false)
	private String villagecode;
	
	@Column(nullable = false)
	private Integer ryotcodesequence;
	
	@Column(nullable = false)
	private Integer circleCode;
	
	@Column(nullable = false)
	private String aadhaarNumber;
	
	@Column(nullable = false)
	private String aadhaarimagepath;
	
	@Column(nullable = false)
	private String pannumber;
	
	@Column(nullable = false)
	private String ryotphotopath;
	
	@Column(nullable = false)
	private String relativename;
	
	@Column(nullable = false)
	private String relation;
	
	@Column(nullable = false)
	private String address;
	
	@Column(nullable = false)
	private String city;
	
	@Column(nullable = false)
	private String mobilenumber;
	
	@Column
	private String suretyryotcode;
	
	@Column(nullable = false)
	private Byte status;
	
	@Column(nullable = false)
	private Byte ryottype;
	
	
	@Column(nullable = false)
	private Byte ryotcategory;
	
	
	@Column(nullable = false)
	private Byte isweighmentcompleted;
	
	
	public Byte getRyotcategory() {
		return ryotcategory;
	}

	public void setRyotcategory(Byte ryotcategory) {
		this.ryotcategory = ryotcategory;
	}

	public Byte getIsweighmentcompleted() {
		return isweighmentcompleted;
	}

	public void setIsweighmentcompleted(Byte isweighmentcompleted) {
		this.isweighmentcompleted = isweighmentcompleted;
	}

	public Byte getRyottype() {
		return ryottype;
	}

	public void setRyottype(Byte ryottype) {
		this.ryottype = ryottype;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getRyotcode() {
		return ryotcode;
	}
	
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	
	public String getSalutation() {
		return salutation;
	}
	
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	
	public String getRyotname() {
		return ryotname;
	}
	
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	
	public Integer getMandalcode() {
		return mandalcode;
	}
	
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	
	
	
	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public Integer getRyotcodesequence() {
		return ryotcodesequence;
	}
	
	public void setRyotcodesequence(Integer ryotcodesequence) {
		this.ryotcodesequence = ryotcodesequence;
	}
	
	public Integer getCircleCode() {
		return circleCode;
	}
	
	public void setCircleCode(Integer circleCode) {
		this.circleCode = circleCode;
	}
	
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	
	public String getAadhaarimagepath() {
		return aadhaarimagepath;
	}
	
	public void setAadhaarimagepath(String aadhaarimagepath) {
		this.aadhaarimagepath = aadhaarimagepath;
	}
	
	public String getPannumber() {
		return pannumber;
	}
	
	public void setPannumber(String pannumber) {
		this.pannumber = pannumber;
	}
	
	public String getRyotphotopath() {
		return ryotphotopath;
	}
	
	public void setRyotphotopath(String ryotphotopath) {
		this.ryotphotopath = ryotphotopath;
	}
	
	public String getRelativename() {
		return relativename;
	}
	
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	
	public String getRelation() {
		return relation;
	}
	
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getMobilenumber() {
		return mobilenumber;
	}
	
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	
	
	
	public String getSuretyryotcode() {
		return suretyryotcode;
	}

	public void setSuretyryotcode(String suretyryotcode) {
		this.suretyryotcode = suretyryotcode;
	}

	public Byte getStatus() {
		return status;
	}
	
	public void setStatus(Byte status) {
		this.status = status;
	}



}
