package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class AccountGroupDetails 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private Integer transactioncode;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date transactiondate;
	
	@Column(nullable = true)
    private Timestamp transactiontime;
	
	@Column(nullable = false)
    private Integer accountgroupcode;
	
	@Column(nullable = false)
    private String journalmemo;
	
	@Column(nullable = false)
    private Double dr;
	
	@Column(nullable = false)
    private Double cr;
	
	@Column(nullable = false)
    private Double runningbalance;
	
	@Column(nullable = false)
    private String loginuser;
	
	@Column(nullable = false)
    private byte isapplyforledger;
	
	@Column(nullable = false)
	private Byte status;
	
	@Column(nullable = false)
	private String season;

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status) 
	{
		this.status = status;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getTransactioncode() 
	{
		return transactioncode;
	}

	public void setTransactioncode(Integer transactioncode)
	{
		this.transactioncode = transactioncode;
	}

	public Date getTransactiondate() 
	{
		return transactiondate;
	}

	public void setTransactiondate(Date transactiondate)
	{
		this.transactiondate = transactiondate;
	}

	public Timestamp getTransactiontime() 
	{
		return transactiontime;
	}

	public void setTransactiontime(Timestamp transactiontime) 
	{
		this.transactiontime = transactiontime;
	}

	public Integer getAccountgroupcode() 
	{
		return accountgroupcode;
	}

	public void setAccountgroupcode(Integer accountgroupcode)
	{
		this.accountgroupcode = accountgroupcode;
	}

	public String getJournalmemo()
	{
		return journalmemo;
	}

	public void setJournalmemo(String journalmemo)
	{
		this.journalmemo = journalmemo;
	}

	public Double getDr()
	{
		return dr;
	}

	public void setDr(Double dr)
	{
		this.dr = dr;
	}

	public Double getCr() 
	{
		return cr;
	}

	public void setCr(Double cr)
	{
		this.cr = cr;
	}

	public Double getRunningbalance() 
	{
		return runningbalance;
	}

	public void setRunningbalance(Double runningbalance) 
	{
		this.runningbalance = runningbalance;
	}

	

	public String getLoginuser() {
		return loginuser;
	}

	public void setLoginuser(String loginuser) {
		this.loginuser = loginuser;
	}

	public byte getIsapplyforledger() {
		return isapplyforledger;
	}

	public void setIsapplyforledger(byte isapplyforledger) {
		this.isapplyforledger = isapplyforledger;
	}

	
}
