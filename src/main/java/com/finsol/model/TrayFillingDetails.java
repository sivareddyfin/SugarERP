package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class TrayFillingDetails
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date trayFillingDate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private String batchSeries;
	@Column(nullable = true)
	private String variety;
	@Column(nullable = true)
	private String seedlingType;
	@Column(nullable = true)
	private String batchNo;
	@Column(nullable = true)
	@Type(type="time")
	private Time startTime;
	@Column(nullable = true)
	@Type(type="time")
	private Time endTime;
	@Column(nullable = true)
	@Type(type="time")
	private Time totalTime;
	@Column(nullable = true)
	private Integer trayType;
	@Column(nullable = true)
	private Integer noOfCavitiesPerTray;
	@Column(nullable = true)
	private Integer totalTrays;
	@Column(nullable = true)
	private Double TotalNoOfSeedlingss;
	
	public Integer getSlno()
	{
		return slno;
	}
	public String getSeason() {
		return season;
	}
	public Date getTrayFillingDate() {
		return trayFillingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public Integer getId() {
		return id;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public String getSeedlingType() {
		return seedlingType;
	}
	
	public Time getStartTime() {
		return startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	
	public Integer getTrayType() {
		return trayType;
	}
	public Integer getNoOfCavitiesPerTray() {
		return noOfCavitiesPerTray;
	}
	public Integer getTotalTrays() {
		return totalTrays;
	}
	public Double getTotalNoOfSeedlingss() {
		return TotalNoOfSeedlingss;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setTrayFillingDate(Date trayFillingDate) {
		this.trayFillingDate = trayFillingDate;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public void setSeedlingType(String seedlingType) {
		this.seedlingType = seedlingType;
	}
	
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	
	public Time getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(Time totalTime) {
		this.totalTime = totalTime;
	}
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	public void setNoOfCavitiesPerTray(Integer noOfCavitiesPerTray) {
		this.noOfCavitiesPerTray = noOfCavitiesPerTray;
	}
	public void setTotalTrays(Integer totalTrays) {
		this.totalTrays = totalTrays;
	}
	public void setTotalNoOfSeedlingss(Double totalNoOfSeedlingss) {
		TotalNoOfSeedlingss = totalNoOfSeedlingss;
	}
	
	
	
	
	
	
	
	
}
