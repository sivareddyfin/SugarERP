package com.finsol.model;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Murty Ayyagari
 */

@Entity
public class StockSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private String batchid;
	
	@Column(nullable = false)
	private String productcode;
	
	@Column(nullable = false)
	private int batchseqno;
	
	@Column(nullable = false)
	private double mrp;
	
	@Column(nullable = false)
	private String groupcode;
	
	@Column(nullable = false)
	private String subgroupcode;
	
	@Column(nullable = false)
	private double stock;
	
	@Column(nullable = false)
	private double costprice;
	
	@Column(nullable = false)
	private double stockvalueincp;
	
	@Column(nullable = false)
	private double stockvalueinmrp;
	
	@Column(nullable = false)
	private int expyear;
	
	@Column(nullable = false)
	private int expmonth;
	
	@Column(nullable = false)
	private int deptid;
	
	@Column(nullable = false)
	private String finyear;
	
	public String getFinyear() {
		return finyear;
	}

	public void setFinyear(String finyear) {
		this.finyear = finyear;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductcode() {
		return productcode;
	}

	public int getBatchseqno() {
		return batchseqno;
	}

	public String getBatchid() {
		return batchid;
	}

	public double getMrp() {
		return mrp;
	}

	public String getGroupcode() {
		return groupcode;
	}

	public String getSubgroupcode() {
		return subgroupcode;
	}

	public double getStock() {
		return stock;
	}
	
	public double getCostprice() {
		return costprice;
	}

	public double getStockvalueincp() {
		return stockvalueincp;
	}

	public double getStockvalueinmrp() {
		return stockvalueinmrp;
	}

	public int getExpyear() {
		return expyear;
	}

	public int getExpmonth() {
		return expmonth;
	}

	public int getDeptid() {
		return deptid;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public void setBatchseqno(int batchseqno) {
		this.batchseqno = batchseqno;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public void setSubgroupcode(String subgroupcode) {
		this.subgroupcode = subgroupcode;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public void setCostprice(double costprice) {
		this.costprice = costprice;
	}

	public void setStockvalueincp(double stockvalueincp) {
		this.stockvalueincp = stockvalueincp;
	}

	public void setStockvalueinmrp(double stockvalueinmrp) {
		this.stockvalueinmrp = stockvalueinmrp;
	}

	public void setExpyear(int expyear) {
		this.expyear = expyear;
	}

	public void setExpmonth(int expmonth) {
		this.expmonth = expmonth;
	}

	public void setDeptid(int deptid) {
		this.deptid = deptid;
	}
}
