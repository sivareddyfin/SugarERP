package com.finsol.model;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Murty Ayyagari
 */

@Entity
public class StockDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private int transactioncode;
	
	@Type(type="date")
	private Date transactiondate;
	
	@Column(nullable = false)
	private Time transactiontime;
	
	@Column(nullable = false)
	private String groupcode;
	
	@Column(nullable = false)
	private String subgroupcode;
	
	@Column(nullable = false)
	private String productcode;
	
	@Column(nullable = false)
	private String productname;
	
	@Column(nullable = false)
	private double currentstock;
	
	@Column(nullable = false)
	private int departmentid;
	
	@Column(nullable = false)
	private String invoicenumber;
	
	@Column(nullable = false)
	private double invoicestock;
	
	@Column(nullable = false)
	private byte invoicetype;
	
	@Column(nullable = false)
	private byte transactiontype;
	
	@Type(type="date")
	private Date dateofupdate;
	
	@Column(nullable = false)
	private String batchid;
	
	@Column(nullable = false)
	private String finyear;
	
	@Column(nullable = false)
	private byte status;
	
	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getFinyear() {
		return finyear;
	}

	public void setFinyear(String finyear) {
		this.finyear = finyear;
	}

	public Integer getId() {
		return id;
	}

	public int getTransactioncode() {
		return transactioncode;
	}

	public Date getTransactiondate() {
		return transactiondate;
	}

	public Time getTransactiontime() {
		return transactiontime;
	}

	public String getGroupcode() {
		return groupcode;
	}

	public String getSubgroupcode() {
		return subgroupcode;
	}

	public String getProductcode() {
		return productcode;
	}

	public String getProductname() {
		return productname;
	}

	public double getCurrentstock() {
		return currentstock;
	}

	public int getDepartmentid() {
		return departmentid;
	}

	public String getInvoicenumber() {
		return invoicenumber;
	}

	public double getInvoicestock() {
		return invoicestock;
	}

	public byte getInvoicetype() {
		return invoicetype;
	}

	public byte getTransactiontype() {
		return transactiontype;
	}

	public Date getDateofupdate() {
		return dateofupdate;
	}

	public String getBatchid() {
		return batchid;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTransactioncode(int transactioncode) {
		this.transactioncode = transactioncode;
	}

	public void setTransactiondate(Date transactiondate) {
		this.transactiondate = transactiondate;
	}

	public void setTransactiontime(Time transactiontime) {
		this.transactiontime = transactiontime;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public void setSubgroupcode(String subgroupcode) {
		this.subgroupcode = subgroupcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public void setCurrentstock(double currentstock) {
		this.currentstock = currentstock;
	}

	public void setDepartmentid(int departmentid) {
		this.departmentid = departmentid;
	}

	public void setInvoicenumber(String invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public void setInvoicestock(double invoicestock) {
		this.invoicestock = invoicestock;
	}

	public void setInvoicetype(byte invoicetype) {
		this.invoicetype = invoicetype;
	}

	public void setTransactiontype(byte transactiontype) {
		this.transactiontype = transactiontype;
	}

	public void setDateofupdate(Date dateofupdate) {
		this.dateofupdate = dateofupdate;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
}
