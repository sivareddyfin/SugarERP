package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class TrayReturnSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	private String ryotCode;
	@Column(nullable = true)
	private Integer zone;
	@Column(nullable = true)
	private Integer village;
	@Column(nullable = true)
	private Integer circle;
	@Column(nullable = true)
	private String vehicalNo;
	@Column(nullable = true)
	private String driverPhNo;
	@Column(nullable = true)
	private String testedBy;
	@Column(nullable = true)
	@Type(type="date")
	private Date SummaryDate;
	
	

	public Date getSummaryDate() {
		return SummaryDate;
	}
	public void setSummaryDate(Date summaryDate) {
		SummaryDate = summaryDate;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	
	public Integer getZone() {
		return zone;
	}
	public Integer getVillage() {
		return village;
	}
	public Integer getCircle() {
		return circle;
	}
	public String getVehicalNo() {
		return vehicalNo;
	}
	public String getDriverPhNo() {
		return driverPhNo;
	}
	public String getTestedBy() {
		return testedBy;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public String getRyotCode() {
		return ryotCode;
	}
	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}
	public void setZone(Integer zone) {
		this.zone = zone;
	}
	public void setVillage(Integer village) {
		this.village = village;
	}
	public void setCircle(Integer circle) {
		this.circle = circle;
	}
	public void setVehicalNo(String vehicalNo) {
		this.vehicalNo = vehicalNo;
	}
	public void setDriverPhNo(String driverPhNo) {
		this.driverPhNo = driverPhNo;
	}
	public void setTestedBy(String testedBy) {
		this.testedBy = testedBy;
	}
	
	
}
