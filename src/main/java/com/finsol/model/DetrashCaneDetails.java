package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class DetrashCaneDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column
	private Integer id;
	@Column
	private String season;
	@Column(nullable = true)
	@Type(type="date")
    private Date detrashingDetailsDate;
	@Column
	private Integer detrashingshift;
	@Column
	private String batchSeries;
	@Column
	private Integer variety;
	@Column
	private String seedSupr;
	@Column
	private String landVillageCode;
	@Column
	private Double wtWithTrash;
	@Column
	private Double wtWithoutTrash;
	
	
	public Integer getSlNo() 
	{
		return slNo;
	}
	public void setSlNo(Integer slNo) 
	{
		this.slNo = slNo;
	}
	public Integer getId() 
	{
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getDetrashingDetailsDate() {
		return detrashingDetailsDate;
	}
	public void setDetrashingDetailsDate(Date detrashingDetailsDate) {
		this.detrashingDetailsDate = detrashingDetailsDate;
	}
	
	public Integer getDetrashingshift() {
		return detrashingshift;
	}
	public void setDetrashingshift(Integer detrashingshift) {
		this.detrashingshift = detrashingshift;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public Integer getVariety() {
		return variety;
	}
	public void setVariety(Integer variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getWtWithTrash() {
		return wtWithTrash;
	}
	public void setWtWithTrash(Double wtWithTrash) {
		this.wtWithTrash = wtWithTrash;
	}
	public Double getWtWithoutTrash() {
		return wtWithoutTrash;
	}
	public void setWtWithoutTrash(Double wtWithoutTrash) {
		this.wtWithoutTrash = wtWithoutTrash;
	}
	
}
