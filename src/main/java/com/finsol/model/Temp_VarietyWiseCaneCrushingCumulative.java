package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_VarietyWiseCaneCrushingCumulative
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String variety;
	@Column
	private Double plant1;
	@Column
	private Double ratoon1;
	@Column
	private Double percentage;
	
	@Column
	private Double total1;
	
	public Integer getSlno()
	{
		return slno;
	}
	public void setSlno(Integer slno) 
	{
		this.slno = slno;
	}
	public String getVariety() 
	{
		return variety;
	}
	public void setVariety(String variety)
	{
		this.variety = variety;
	}
	public Double getPlant1()
	{
		return plant1;
	}
	public void setPlant1(Double plant1) 
	{
		this.plant1 = plant1;
	}
	public Double getRatoon1()
	{
		return ratoon1;
	}
	public void setRatoon1(Double ratoon1) 
	{
		this.ratoon1 = ratoon1;
	}
	public Double getPercentage()
	{
		return percentage;
	}
	public void setPercentage(Double percentage) 
	{
		this.percentage = percentage;
	}
	public Double getTotal1() {
		return total1;
	}
	public void setTotal1(Double total1) {
		this.total1 = total1;
	}
	
	

}
