package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Period 
{

	@Id
	@Column(nullable = false)
	private Integer periodid;
	
	@Column(nullable = false)
	private String period;
	
	@Column
	private Integer startdate;
	
	@Column
	private Integer enddate;

	public Integer getPeriodid() {
		return periodid;
	}

	public void setPeriodid(Integer periodid) {
		this.periodid = periodid;
	}

	public String getPeriod() 
	{
		return period;
	}

	public void setPeriod(String period)
	{
		this.period = period;
	}

	public Integer getStartdate() {
		return startdate;
	}

	public void setStartdate(Integer startdate) {
		this.startdate = startdate;
	}

	public Integer getEnddate() {
		return enddate;
	}

	public void setEnddate(Integer enddate) {
		this.enddate = enddate;
	}
	
	

}
