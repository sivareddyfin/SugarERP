package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class LandType {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	
	@Column(unique = true,nullable = false)
    private Integer landtypeid;
	
	@Column
    private String landtype;
    
    @Column
    private String description;
    
    @Column
    private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLandtypeid() {
		return landtypeid;
	}

	public void setLandtypeid(Integer landtypeid) {
		this.landtypeid = landtypeid;
	}

	public String getLandtype() {
		return landtype;
	}

	public void setLandtype(String landtype) {
		this.landtype = landtype;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	
    
    
}
