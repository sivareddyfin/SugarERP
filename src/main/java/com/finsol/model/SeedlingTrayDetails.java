package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SeedlingTrayDetails {
	@Column
	private Integer id;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;

	@Column
	private String ryotcode;
	@Column
	private String season;
	@Column
	private Integer suppliedseedlings;
	@Column
	private Integer traytype;
	@Column
	private Integer nooftrays;
	@Column
	private Integer varietycode;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getSuppliedseedlings() {
		return suppliedseedlings;
	}
	public void setSuppliedseedlings(Integer suppliedseedlings) {
		this.suppliedseedlings = suppliedseedlings;
	}
	public Integer getTraytype() {
		return traytype;
	}
	public void setTraytype(Integer traytype) {
		this.traytype = traytype;
	}
	public Integer getNooftrays() {
		return nooftrays;
	}
	public void setNooftrays(Integer nooftrays) {
		this.nooftrays = nooftrays;
	}
	
	public Integer getSlno() 
	{
		return slno;
	}
	public void setSlno(Integer slno)
	{
		this.slno = slno;
	}
	public Integer getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}
	




}
