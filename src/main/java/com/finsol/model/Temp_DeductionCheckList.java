package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_DeductionCheckList
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private Double supplyqty;
	@Column
	private Double amountpayble;
	@Column
	private Double trptamount;
	@Column
	private Double harvamount;
	@Column
	private Double odedamount;
	
	@Column
	private Double banktfer;
	@Column
	private Double bycash;
	
	public Double getBanktfer() {
		return banktfer;
	}
	public void setBanktfer(Double banktfer) {
		this.banktfer = banktfer;
	}
	public Double getBycash() {
		return bycash;
	}
	public void setBycash(Double bycash) {
		this.bycash = bycash;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getSupplyqty() {
		return supplyqty;
	}
	public void setSupplyqty(Double supplyqty) {
		this.supplyqty = supplyqty;
	}
	public Double getAmountpayble() {
		return amountpayble;
	}
	public void setAmountpayble(Double amountpayble) {
		this.amountpayble = amountpayble;
	}
	public Double getTrptamount() {
		return trptamount;
	}
	public void setTrptamount(Double trptamount) {
		this.trptamount = trptamount;
	}
	public Double getHarvamount() {
		return harvamount;
	}
	public void setHarvamount(Double harvamount) {
		this.harvamount = harvamount;
	}
	public Double getOdedamount() {
		return odedamount;
	}
	public void setOdedamount(Double odedamount) {
		this.odedamount = odedamount;
	}

}
