package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class AccountSummary
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	
	@Column(nullable = false)
    private String accountcode;
	
	@Column(nullable = false)
    private Double runningbalance;
	
	@Column(nullable = false)
    private String balancetype;
	
	@Column(nullable = false)
	private String season;

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getAccountcode()
	{
		return accountcode;
	}

	public void setAccountcode(String accountcode) 
	{
		this.accountcode = accountcode;
	}

	public Double getRunningbalance() 
	{
		return runningbalance;
	}

	public void setRunningbalance(Double runningbalance) 
	{
		this.runningbalance = runningbalance;
	}

	public String getBalancetype()
	{
		return balancetype;
	}

	public void setBalancetype(String balancetype)
	{
		this.balancetype = balancetype;
	}
}
