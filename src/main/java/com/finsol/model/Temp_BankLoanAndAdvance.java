package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_BankLoanAndAdvance
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String village;
	@Column
	private Integer loannumber;
	@Column
	private String loanrefnumber;
	@Column
	private Double quantity;
	@Column
	private Double balance;
	@Column
	private Double totalrecovery;
	@Column
	private Double totalamount;
	@Column
	private Double intrestamount;
	@Column
	private String bankname;
	
	
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	
	public String getLoanrefnumber() {
		return loanrefnumber;
	}
	public void setLoanrefnumber(String loanrefnumber) {
		this.loanrefnumber = loanrefnumber;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getTotalrecovery() {
		return totalrecovery;
	}
	public void setTotalrecovery(Double totalrecovery) {
		this.totalrecovery = totalrecovery;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getIntrestamount() {
		return intrestamount;
	}
	public void setIntrestamount(Double intrestamount) {
		this.intrestamount = intrestamount;
	}
	
	
	
}
