package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_BruntCane
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer receiptno;
	@Column
	private Integer bridgeno;
	@Column
	private String ryotcode;
	@Column
	private String shift;
	@Column
	private String plantorratoon;
	@Column
	private String variety;
	@Column
	private Double weight;
	@Column
	private String cdate;
	@Column
	private String wbdate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getReceiptno() {
		return receiptno;
	}
	public void setReceiptno(Integer receiptno) {
		this.receiptno = receiptno;
	}
	public Integer getBridgeno() {
		return bridgeno;
	}
	public void setBridgeno(Integer bridgeno) {
		this.bridgeno = bridgeno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getWbdate() {
		return wbdate;
	}
	public void setWbdate(String wbdate) {
		this.wbdate = wbdate;
	}

	
}
