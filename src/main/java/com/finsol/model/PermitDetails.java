package com.finsol.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class PermitDetails
{
	@EmbeddedId
	private CompositePrKeyForPermitDetails compositePrKeyForPermitDetails;
	
	public CompositePrKeyForPermitDetails getCompositePrKeyForPermitDetails() {
		return compositePrKeyForPermitDetails;
	}
	public void setCompositePrKeyForPermitDetails(
			CompositePrKeyForPermitDetails compositePrKeyForPermitDetails) {
		this.compositePrKeyForPermitDetails = compositePrKeyForPermitDetails;
	}
	@Column
	private  String season;	
	
	@Column
	private Integer rank;
	@Column
	private  String ryotcode;
	@Column
	private  String agreementno;
	@Column
	private  String plotno;
	@Column
	private Byte plantorratoon;
	@Column
	private Double extentsize;
	@Column
	private Integer varietycode;
	@Column
	private Integer zonecode;
	@Column
	private Integer circlecode;
	@Column
	private String landvilcode;
	@Column
	@Type(type="date")
	private Date harvestdt;
	@Column
	@Type(type="date")
	private Date departuredt;
	@Column
	@Type(type="date")
	private Date entrancedt;
	@Column
	@Type(type="date")
	private Date unloadingdt;
	@Column
	private String shiftandadmin;
	
	@Column
	private Byte status;
	@Column
	private Date permitdate;
	
	@Column(nullable = true)
	private String regeneratedby;
	
	@Column(nullable = true)
	private String reason;
	
	@Column(length = 10000)
	private byte[] qrcodedata;
	
	@Column
	@Type(type="date")
	private Date validitydate;
	@Column
	private String vehicleno;
	
	@Column
	private Integer oldpermitnumber;
	
	@Column
	private Byte vehicletype;
	
	@Column
	private Time checkintime;
	@Column
	@Type(type="date")
	private Date checkindate;
	@Column
	private Integer shiftid;
	
	@Column
	private Integer vehicleslno; 
	
	@Column
	private Byte checkinstatus;
	
	public Byte getCheckinstatus() {
		return checkinstatus;
	}
	public void setCheckinstatus(Byte checkinstatus) {
		this.checkinstatus = checkinstatus;
	}
	public Integer getOldpermitnumber() {
		return oldpermitnumber;
	}
	public void setOldpermitnumber(Integer oldpermitnumber) {
		this.oldpermitnumber = oldpermitnumber;
	}
	
	public Date getValiditydate() {
		return validitydate;
	}
	public void setValiditydate(Date validitydate) {
		this.validitydate = validitydate;
	}
	public String getRegeneratedby() {
		return regeneratedby;
	}
	public void setRegeneratedby(String regeneratedby) {
		this.regeneratedby = regeneratedby;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getPermitdate() {
		return permitdate;
	}
	public void setPermitdate(Date permitdate) {
		this.permitdate = permitdate;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getAgreementno() {
		return agreementno;
	}
	public void setAgreementno(String agreementno) {
		this.agreementno = agreementno;
	}
	public String getPlotno() {
		return plotno;
	}
	public void setPlotno(String plotno) {
		this.plotno = plotno;
	}
	public Byte getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(Byte plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	
	
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	
	public Integer getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(Integer varietycode) {
		this.varietycode = varietycode;
	}
	
	
	
	public String getLandvilcode() {
		return landvilcode;
	}
	public void setLandvilcode(String landvilcode) {
		this.landvilcode = landvilcode;
	}
	public Date getHarvestdt() {
		return harvestdt;
	}
	public void setHarvestdt(Date harvestdt) {
		this.harvestdt = harvestdt;
	}
	public Date getDeparturedt() {
		return departuredt;
	}
	public void setDeparturedt(Date departuredt) {
		this.departuredt = departuredt;
	}
	public Date getEntrancedt() {
		return entrancedt;
	}
	public void setEntrancedt(Date entrancedt) {
		this.entrancedt = entrancedt;
	}
	public Date getUnloadingdt() {
		return unloadingdt;
	}
	public void setUnloadingdt(Date unloadingdt) {
		this.unloadingdt = unloadingdt;
	}
	public String getShiftandadmin() {
		return shiftandadmin;
	}
	public void setShiftandadmin(String shiftandadmin) {
		this.shiftandadmin = shiftandadmin;
	}
	
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public byte[] getQrcodedata() {
		return qrcodedata;
	}
	public void setQrcodedata(byte[] qrcodedata) {
		this.qrcodedata = qrcodedata;
	}
	public String getVehicleno() {
		return vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public Byte getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(Byte vehicletype) {
		this.vehicletype = vehicletype;
	}
	public Time getCheckintime() {
		return checkintime;
	}
	public void setCheckintime(Time checkintime) {
		this.checkintime = checkintime;
	}
	public Date getCheckindate() {
		return checkindate;
	}
	public void setCheckindate(Date checkindate) {
		this.checkindate = checkindate;
	}
	public Integer getShiftid() {
		return shiftid;
	}
	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}
	public Integer getVehicleslno() {
		return vehicleslno;
	}
	public void setVehicleslno(Integer vehicleslno) {
		this.vehicleslno = vehicleslno;
	}
	
}
