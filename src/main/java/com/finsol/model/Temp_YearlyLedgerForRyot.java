package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author Naidu
 *
 */
@Entity
public class Temp_YearlyLedgerForRyot {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String relativename;
	@Column
	private String village;
	@Column
	private String sbaccno;
	@Column
	private String agrmntdate;
	@Column
	private String pamount;
	@Column
	private String iamount;
	@Column
	private String branchcode;
	@Column
	private String loancode;
	@Column
	private Double frpprice;
	@Column
	private Double agrqty;
	@Column
	private Double plant;
	@Column
	private Double ratoon;
	@Column
	private Integer sbaccbranchcode;
	@Column
	private String referencenumber;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSbaccno() {
		return sbaccno;
	}
	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}
	public String getAgrmntdate() {
		return agrmntdate;
	}
	public void setAgrmntdate(String agrmntdate) {
		this.agrmntdate = agrmntdate;
	}
	public String getPamount() {
		return pamount;
	}
	public void setPamount(String pamount) {
		this.pamount = pamount;
	}
	public String getIamount() {
		return iamount;
	}
	public void setIamount(String iamount) {
		this.iamount = iamount;
	}
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getLoancode() {
		return loancode;
	}
	public void setLoancode(String loancode) {
		this.loancode = loancode;
	}
	public Double getFrpprice() {
		return frpprice;
	}
	public void setFrpprice(Double frpprice) {
		this.frpprice = frpprice;
	}
	public Double getAgrqty() {
		return agrqty;
	}
	public void setAgrqty(Double agrqty) {
		this.agrqty = agrqty;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getRatoon() {
		return ratoon;
	}
	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}
	public Integer getSbaccbranchcode() {
		return sbaccbranchcode;
	}
	public void setSbaccbranchcode(Integer sbaccbranchcode) {
		this.sbaccbranchcode = sbaccbranchcode;
	}
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}
	
}
