package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 */
@Entity
public class OtherRyot 
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
    private String ryotCode;

	@Column(nullable = false)
    private String ryotName;
	
	@Column(nullable = false)
    private String address;

	@Column(nullable = false)
    private String city;
	
	@Column(nullable = false)
    private String phoneNo;

	@Column(nullable = false)
    private String accountNo;

	@Column(nullable = false)
    private Integer bankCode;

	@Column(nullable = false)
    private Integer branchCode;

	@Column(nullable = false)
    private String  village;
	
	@Column(nullable = false)
    private Byte status;


	public String getVillage() {
		return village;
	}


	public void setVillage(String village) {
		this.village = village;
	}


	public Integer getId() {
		return id;
	}


	public String getRyotCode() {
		return ryotCode;
	}


	public String getRyotName() {
		return ryotName;
	}


	public String getAddress() {
		return address;
	}


	public String getCity() {
		return city;
	}


	public String getPhoneNo() {
		return phoneNo;
	}


	public String getAccountNo() {
		return accountNo;
	}


	public Integer getBankCode() {
		return bankCode;
	}


	public Integer getBranchCode() {
		return branchCode;
	}


	public Byte getStatus() {
		return status;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}


	public void setRyotName(String ryotName) {
		this.ryotName = ryotName;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}


	public void setBankCode(Integer bankCode) {
		this.bankCode = bankCode;
	}


	public void setBranchCode(Integer branchCode) {
		this.branchCode = branchCode;
	}


	public void setStatus(Byte status) {
		this.status = status;
	}
	
	


}
