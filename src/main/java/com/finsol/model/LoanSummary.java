package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class LoanSummary
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String season;
	
	@Column
	private String ryotcode;
	@Column
	private Integer loannumber;
	@Column
	private Double Principle;
	@Column
	private Double disbursedamount;
	@Column
	private Double interestamount;
	@Column
	private Double totalamount;
	@Column
	private Integer loanstatus;
	@Column
	private Double paidamount;
	@Column
	private Double pendingamount;
	
	
	@Column (nullable = false)
	private Integer branchcode;

	@Column
	private Integer batchno;
	
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	
	
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public Double getPrinciple() {
		return Principle;
	}
	public void setPrinciple(Double principle) {
		Principle = principle;
	}
	public Double getDisbursedamount() {
		return disbursedamount;
	}
	public void setDisbursedamount(Double disbursedamount) {
		this.disbursedamount = disbursedamount;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	
	
	public Integer getLoanstatus() {
		return loanstatus;
	}
	public void setLoanstatus(Integer loanstatus) {
		this.loanstatus = loanstatus;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public Integer getBatchno() {
		return batchno;
	}
	public void setBatchno(Integer batchno) {
		this.batchno = batchno;
	}

	

}
