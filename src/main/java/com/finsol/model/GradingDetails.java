package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class GradingDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date gradingDate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private String batch;
	@Column(nullable = true)
	private String variety;
	@Column(nullable = true)
	private String supplier;
	@Column(nullable = true)
	private String village;
	@Column(nullable = true)
	private double totalNoOfBuds;
	@Column(nullable = true)
	private String transferToTrayFilling;
	@Column(nullable = true)
	private String sentBackToSprouting;
	@Column(nullable = true)
	private String disposedToRejection;
	@Column(nullable = true)
	private Double germinationPerc;
	@Column(nullable = true)
	private String remarks;
	
	public Integer getSlNo() 
	{
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getGradingDate() {
		return gradingDate;
	}
	public void setGradingDate(Date gradingDate) {
		this.gradingDate = gradingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public double getTotalNoOfBuds() {
		return totalNoOfBuds;
	}
	public void setTotalNoOfBuds(double totalNoOfBuds) {
		this.totalNoOfBuds = totalNoOfBuds;
	}
	public String getTransferToTrayFilling() {
		return transferToTrayFilling;
	}
	public void setTransferToTrayFilling(String transferToTrayFilling) {
		this.transferToTrayFilling = transferToTrayFilling;
	}
	public String getSentBackToSprouting() {
		return sentBackToSprouting;
	}
	public void setSentBackToSprouting(String sentBackToSprouting) {
		this.sentBackToSprouting = sentBackToSprouting;
	}
	public String getDisposedToRejection() {
		return disposedToRejection;
	}
	public void setDisposedToRejection(String disposedToRejection) {
		this.disposedToRejection = disposedToRejection;
	}
	public Double getGerminationPerc() {
		return germinationPerc;
	}
	public void setGerminationPerc(Double germinationPerc) {
		this.germinationPerc = germinationPerc;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
