package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 */
@Entity
public class CaneAcctRules 
{
	@Column
	private String season;
	@Column
	@Type(type="date")
	Date calcruledate; 
	@Id
	@Column 
	private Integer calcid;
	@Column
    private Double seasoncaneprice;
    @Column
    private Double postseasoncaneprice;
    @Column
    private Double netcaneprice;
    @Column
    private Byte ispercentoramt;
	@Column
	private Double amtforloans;
	@Column(nullable = false)
	private Double amtforded;
	@Column
	private Double percentforded;
	@Column
	private Double percentForloans;
	
	private Byte  istaapplicable;
	@Column
	private Double noofkms;
	@Column
	private Double allowanceperkmperton;
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getCalcruledate() {
		return calcruledate;
	}
	public void setCalcruledate(Date calcruledate) {
		this.calcruledate = calcruledate;
	}
	public Integer getCalcid() {
		return calcid;
	}
	public void setCalcid(Integer calcid) {
		this.calcid = calcid;
	}
	public Double getSeasoncaneprice() {
		return seasoncaneprice;
	}
	public void setSeasoncaneprice(Double seasoncaneprice) {
		this.seasoncaneprice = seasoncaneprice;
	}
	public Double getPostseasoncaneprice() {
		return postseasoncaneprice;
	}
	public void setPostseasoncaneprice(Double postseasoncaneprice) {
		this.postseasoncaneprice = postseasoncaneprice;
	}
	public Byte getIspercentoramt() {
		return ispercentoramt;
	}
	public void setIspercentoramt(Byte ispercentoramt) {
		this.ispercentoramt = ispercentoramt;
	}
	public Double getAmtforloans() {
		return amtforloans;
	}
	public void setAmtforloans(Double amtforloans) {
		this.amtforloans = amtforloans;
	}
	public Double getAmtforded() {
		return amtforded;
	}
	public void setAmtforded(Double amtforded) {
		this.amtforded = amtforded;
	}
	public Double getPercentforded() {
		return percentforded;
	}
	public void setPercentforded(Double percentforded) {
		this.percentforded = percentforded;
	}

	
	public Double getNetcaneprice() {
		return netcaneprice;
	}
	public void setNetcaneprice(Double netcaneprice) {
		this.netcaneprice = netcaneprice;
	}
	public Double getPercentForloans() {
		return percentForloans;
	}
	public void setPercentForloans(Double percentForloans) {
		this.percentForloans = percentForloans;
	}
	public Byte getIstaapplicable() {
		return istaapplicable;
	}
	public void setIstaapplicable(Byte istaapplicable) {
		this.istaapplicable = istaapplicable;
	}
	public Double getNoofkms() {
		return noofkms;
	}
	public void setNoofkms(Double noofkms) {
		this.noofkms = noofkms;
	}
	public Double getAllowanceperkmperton() {
		return allowanceperkmperton;
	}
	public void setAllowanceperkmperton(Double allowanceperkmperton) {
		this.allowanceperkmperton = allowanceperkmperton;
	}
	

}
