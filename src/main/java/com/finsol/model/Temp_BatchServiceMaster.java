	package com.finsol.model;

	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import org.hibernate.annotations.Type;

	/**
	 * @author Naidu
	 */
	@Entity
	public class Temp_BatchServiceMaster
	{
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id;
		
		@Column(nullable = false)
	    private String season;
		
		@Column(nullable = false)
	    private String batchseries;
		
		@Column(nullable = false)
	    private String SeedType;

		@Column(nullable = false)
	    private Integer varietycode;
		
		public Integer getVarietycode() {
			return varietycode;
		}

		public void setVarietycode(Integer varietycode) {
			this.varietycode = varietycode;
		}

		@Column(nullable = false)
	    private Integer seqno;
		
		@Column(nullable = false)
	    private String variety;

		@Column(nullable = false)
	    private String seedsuppliercode;

		@Column(nullable = false)
	    private String landvillagecode;

		@Column(nullable = false)
		private Byte isbatchprepared;
		
		@Column(nullable = false)
	    private String runningyear;

		
		public Integer getId() 
		{
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getSeason() {
			return season;
		}

		public void setSeason(String season) {
			this.season = season;
		}

		public String getBatchseries() {
			return batchseries;
		}

		public void setBatchseries(String batchseries) {
			this.batchseries = batchseries;
		}

		public String getSeedType() {
			return SeedType;
		}

		public void setSeedType(String seedType) {
			SeedType = seedType;
		}

		

		public Integer getSeqno() {
			return seqno;
		}

		public void setSeqno(Integer seqno) {
			this.seqno = seqno;
		}

		public String getVariety() {
			return variety;
		}

		public void setVariety(String variety) {
			this.variety = variety;
		}

		public String getSeedsuppliercode() {
			return seedsuppliercode;
		}

		public void setSeedsuppliercode(String seedsuppliercode) {
			this.seedsuppliercode = seedsuppliercode;
		}

		public String getLandvillagecode() {
			return landvillagecode;
		}

		public void setLandvillagecode(String landvillagecode) {
			this.landvillagecode = landvillagecode;
		}

		public Byte getIsbatchprepared() {
			return isbatchprepared;
		}

		public void setIsbatchprepared(Byte isbatchprepared) {
			this.isbatchprepared = isbatchprepared;
		}

		public String getRunningyear() {
			return runningyear;
		}

		public void setRunningyear(String runningyear) {
			this.runningyear = runningyear;
		}

	}

	

