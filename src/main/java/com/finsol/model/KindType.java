package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Umanath Ch
 *
 */
@Entity


public class KindType
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private String kindType;
	
	@Column
	private String description;
	
	@Column(nullable = false)
	private String uom;
	
	@Column
	private  Byte status;
	@Column
	private Byte roundingTo;
	@Column
	private Double limitperAcre;
	@Column
	private Double costofeachItem;
	@Column
	private Integer advance;
	@Column
	private Integer plantmaxcount;
	@Column
	private Integer ratoonmaxcount;
	
	
	public Integer getPlantmaxcount() {
		return plantmaxcount;
	}

	public Integer getRatoonmaxcount() {
		return ratoonmaxcount;
	}

	public void setPlantmaxcount(Integer plantmaxcount) {
		this.plantmaxcount = plantmaxcount;
	}

	public void setRatoonmaxcount(Integer ratoonmaxcount) {
		this.ratoonmaxcount = ratoonmaxcount;
	}

	public Byte getRoundingTo() {
		return roundingTo;
	}

	public Double getLimitperAcre() {
		return limitperAcre;
	}

	public Double getCostofeachItem() {
		return costofeachItem;
	}

	public Integer getAdvance() {
		return advance;
	}

	public void setRoundingTo(Byte roundingTo) {
		this.roundingTo = roundingTo;
	}

	public void setLimitperAcre(Double limitperAcre) {
		this.limitperAcre = limitperAcre;
	}

	public void setCostofeachItem(Double costofeachItem) {
		this.costofeachItem = costofeachItem;
	}

	public void setAdvance(Integer advance) {
		this.advance = advance;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKindType() {
		return kindType;
	}

	public void setKindType(String kindType) {
		this.kindType = kindType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
}
