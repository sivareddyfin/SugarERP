package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity

public class sssAccounts_CR_1314
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private Integer acctype;
	@Column
	private String cdate;
	@Column
	private String ryotcode;
	@Column
	private Double c_supqty;
	@Column
	private Double amount;
	@Column
	private String description;
	@Column
	private Double frprate;
	@Column
	private Double hsrate;
	@Column
	private String fromyear;
	@Column
	private String toyear;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAcctype() {
		return acctype;
	}
	public void setAcctype(Integer acctype) {
		this.acctype = acctype;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public Double getC_supqty() {
		return c_supqty;
	}
	public void setC_supqty(Double c_supqty) {
		this.c_supqty = c_supqty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getFrprate() {
		return frprate;
	}
	public void setFrprate(Double frprate) {
		this.frprate = frprate;
	}
	public Double getHsrate() {
		return hsrate;
	}
	public void setHsrate(Double hsrate) {
		this.hsrate = hsrate;
	}
	public String getFromyear() {
		return fromyear;
	}
	public void setFromyear(String fromyear) {
		this.fromyear = fromyear;
	}
	public String getToyear() {
		return toyear;
	}
	public void setToyear(String toyear) {
		this.toyear = toyear;
	}
	

}
