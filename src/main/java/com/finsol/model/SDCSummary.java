package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */
@Entity
public class SDCSummary
{
	
	
	@Column(nullable = false)
    private String finyr;
	
	@Column(nullable = false)
	@Type(type="date")
	private Date sdcdate;
	
	@Column(nullable = false)
	private Time sdctime;
	
	@Column(nullable = false)
    private Integer seqno;
	
	@Id
	@Column(nullable = false)
    private String sdcno;
	
	
	@Column(nullable = false)
    private String salesinvno;
	
	@Column(nullable = false)
    private byte status;
	
	@Column(nullable = false)
    private String loginid;
	
	@Column(nullable = false)
    private Integer deptid;
	
	@Column(nullable = true)
    private String address;
	
	@Column(nullable = true)
    private String contactnumber;
	
	@Column(nullable = true)
    private String customername;
	
	@Column(nullable = false)
    private Integer transactioncode;
	
	@Column(nullable = false)
    private double totalcost;
	
	@Column(nullable = false)
    private double discountamount;
	
	@Column(nullable = false)
    private double discountpercentage;
	
	@Column(nullable = false)
    private double netamount;
	
	@Column(nullable = false)
    private Integer disctype;
	
	
	public Integer getDisctype() {
		return disctype;
	}

	public void setDisctype(Integer disctype) {
		this.disctype = disctype;
	}

	public double getNetamount() {
		return netamount;
	}

	public void setNetamount(double netamount) {
		this.netamount = netamount;
	}

	
	public double getTotalcost() {
		return totalcost;
	}

	public double getDiscountamount() {
		return discountamount;
	}

	public double getDiscountpercentage() {
		return discountpercentage;
	}

	public void setTotalcost(double totalcost) {
		this.totalcost = totalcost;
	}

	public void setDiscountamount(double discountamount) {
		this.discountamount = discountamount;
	}

	public void setDiscountpercentage(double discountpercentage) {
		this.discountpercentage = discountpercentage;
	}

	public Integer getTransactioncode() {
		return transactioncode;
	}

	public void setTransactioncode(Integer transactioncode) {
		this.transactioncode = transactioncode;
	}

	public String getFinyr() {
		return finyr;
	}

	public Date getSdcdate() {
		return sdcdate;
	}

	public Time getSdctime() {
		return sdctime;
	}

	public Integer getSeqno() {
		return seqno;
	}

	public String getSdcno() {
		return sdcno;
	}

	public String getSalesinvno() {
		return salesinvno;
	}

	public byte getStatus() {
		return status;
	}

	public String getLoginid() {
		return loginid;
	}

	public Integer getDeptid() {
		return deptid;
	}

	public String getAddress() {
		return address;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public String getCustomername() {
		return customername;
	}

	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}

	public void setSdcdate(Date sdcdate) {
		this.sdcdate = sdcdate;
	}

	public void setSdctime(Time sdctime) {
		this.sdctime = sdctime;
	}

	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}

	public void setSdcno(String sdcno) {
		this.sdcno = sdcno;
	}

	public void setSalesinvno(String salesinvno) {
		this.salesinvno = salesinvno;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
}
