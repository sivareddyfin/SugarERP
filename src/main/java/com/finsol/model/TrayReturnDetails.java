package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */


@Entity
public class TrayReturnDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slNo;
	@Column(nullable = true)
	private Integer trayType;
	@Column(nullable = true)
	private Integer noOfTraysReturn;
	@Column(nullable = true)
	private Double traysInGoodCondition;
	@Column(nullable = true)
	private Double damagedTrays;
	@Column(nullable = true)
	@Type(type="date")
	private Date traysTakenDate;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private Integer ryotDamagedTray;
	@Column(nullable = true)
	private Integer damagedTrayTravelling;
	
	@Column(nullable = true)
	private String season;
	
	
	
	public Integer getRyotDamagedTray() {
		return ryotDamagedTray;
	}
	public Integer getDamagedTrayTravelling() {
		return damagedTrayTravelling;
	}
	public void setRyotDamagedTray(Integer ryotDamagedTray) {
		this.ryotDamagedTray = ryotDamagedTray;
	}
	public void setDamagedTrayTravelling(Integer damagedTrayTravelling) {
		this.damagedTrayTravelling = damagedTrayTravelling;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Integer getId()
	{
		return id;
	}
	public Integer getTrayType() {
		return trayType;
	}
	
	
	public Date getTraysTakenDate() {
		return traysTakenDate;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}
	
	
	public void setTraysTakenDate(Date traysTakenDate) {
		this.traysTakenDate = traysTakenDate;
	}
	public Double getTraysInGoodCondition() {
		return traysInGoodCondition;
	}
	
	public void setTraysInGoodCondition(Double traysInGoodCondition) {
		this.traysInGoodCondition = traysInGoodCondition;
	}
	public Integer getNoOfTraysReturn() {
		return noOfTraysReturn;
	}
	public Double getDamagedTrays() {
		return damagedTrays;
	}
	public void setNoOfTraysReturn(Integer noOfTraysReturn) {
		this.noOfTraysReturn = noOfTraysReturn;
	}
	public void setDamagedTrays(Double damagedTrays) {
		this.damagedTrays = damagedTrays;
	}
	
	
	
	
	
	
	
	
}
