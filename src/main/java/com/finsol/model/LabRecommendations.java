package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 */
@Entity
public class LabRecommendations {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	
	@Column
	private String plotslno;
	
	@Column
	private String surveynumber;
	
	@Column
	private String recommendation;
	
	@Column
	private String enteredBy;
	
	

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getPlotslno() {
		return plotslno;
	}

	public void setPlotslno(String plotslno) {
		this.plotslno = plotslno;
	}

	public String getSurveynumber() {
		return surveynumber;
	}

	public void setSurveynumber(String surveynumber) {
		this.surveynumber = surveynumber;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	
	
}
