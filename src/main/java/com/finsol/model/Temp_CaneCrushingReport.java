package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_CaneCrushingReport
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column
	private Double shift1;
	@Column
	private Double shift2;
	@Column
	private Double shift3;
	@Column 
	private String nametype;
	@Column 
	private Double netwt;
	@Column 
	private Double lorryandcanewt;
	
	
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	
	public Double getShift1() {
		return shift1;
	}
	public void setShift1(Double shift1) {
		this.shift1 = shift1;
	}
	public Double getShift2() {
		return shift2;
	}
	public void setShift2(Double shift2) {
		this.shift2 = shift2;
	}
	public Double getShift3() {
		return shift3;
	}
	public void setShift3(Double shift3) {
		this.shift3 = shift3;
	}
	public String getNametype() {
		return nametype;
	}
	public void setNametype(String nametype) {
		this.nametype = nametype;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getLorryandcanewt() {
		return lorryandcanewt;
	}
	public void setLorryandcanewt(Double lorryandcanewt) {
		this.lorryandcanewt = lorryandcanewt;
	}

	
	
}
