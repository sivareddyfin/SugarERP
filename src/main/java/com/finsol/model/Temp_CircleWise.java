package com.finsol.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 * @author sahadeva    
 */

@Entity

public class Temp_CircleWise 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column 
	private String variety;
	@Column 
	private String agreementnumber;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private String landvillagecode;
	@Column 
	private String plotnumber;
	@Column 
	private Date cropdate;
	@Column 
	private Double extentsize;
	@Column 
	private String aadhaarNumber;
	@Column 
	private String mobilenumber;
	@Column 
	private String accountnumber;

	@Column 
	private String branchname;
	
	@Column 
	private String city;
	@Column 
	private Integer bankcode;
	@Column
	private String surveynumber;
	@Column 
	private String circlecode;
	@Column 
	private String circle;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	
	public String getLandvillagecode() {
		return landvillagecode;
	}
	public void setLandvillagecode(String landvillagecode) {
		this.landvillagecode = landvillagecode;
	}
	public String getPlotnumber() {
		return plotnumber;
	}
	public void setPlotnumber(String plotnumber) {
		this.plotnumber = plotnumber;
	}
	public Date getCropdate() {
		return cropdate;
	}
	public void setCropdate(Date cropdate) {
		this.cropdate = cropdate;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getBankcode() {
		return bankcode;
	}
	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getSurveynumber() {
		return surveynumber;
	}
	public void setSurveynumber(String surveynumber) {
		this.surveynumber = surveynumber;
	}
	public String getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(String circlecode) {
		this.circlecode = circlecode;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	


}
