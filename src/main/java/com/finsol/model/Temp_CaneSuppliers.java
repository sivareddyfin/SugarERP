package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_CaneSuppliers 
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer zonecode;
	@Column
	private String zone;
	@Column
	private String circle;
	@Column
	private Integer circlecode;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private Double estqty;
	@Column
	private Double suplyqty;
	@Column
	private Double cumqty;
	@Column
	private Double suptotal;
	@Column
	private Double esttotal;
	@Column
	private Double cumtotal;
	@Column
	private Double supgrandtotal;
	@Column
	private Double estgrandtotal;
	@Column
	private Double cumgrandtotal;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getZonecode() {
		return zonecode;
	}
	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getEstqty() {
		return estqty;
	}
	public void setEstqty(Double estqty) {
		this.estqty = estqty;
	}
	public Double getSuplyqty() {
		return suplyqty;
	}
	public void setSuplyqty(Double suplyqty) {
		this.suplyqty = suplyqty;
	}
	public Double getCumqty() {
		return cumqty;
	}
	public void setCumqty(Double cumqty) {
		this.cumqty = cumqty;
	}
	public Double getSuptotal() {
		return suptotal;
	}
	public void setSuptotal(Double suptotal) {
		this.suptotal = suptotal;
	}
	public Double getEsttotal() {
		return esttotal;
	}
	public void setEsttotal(Double esttotal) {
		this.esttotal = esttotal;
	}
	public Double getCumtotal() {
		return cumtotal;
	}
	public void setCumtotal(Double cumtotal) {
		this.cumtotal = cumtotal;
	}
	public Double getSupgrandtotal() {
		return supgrandtotal;
	}
	public void setSupgrandtotal(Double supgrandtotal) {
		this.supgrandtotal = supgrandtotal;
	}
	public Double getEstgrandtotal() {
		return estgrandtotal;
	}
	public void setEstgrandtotal(Double estgrandtotal) {
		this.estgrandtotal = estgrandtotal;
	}
	public Double getCumgrandtotal() {
		return cumgrandtotal;
	}
	public void setCumgrandtotal(Double cumgrandtotal) {
		this.cumgrandtotal = cumgrandtotal;
	}
	
	


}
