package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 *
 */
@Entity
public class CanToFinancialAccounting 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer tcomp;
	@Column
	private Integer tdb;
	@Column
	private String tdoc;
	@Column
	private Integer tserial;
	@Column
	@Type(type="date")
	private Date tdt;
	@Column
	private String tdes;
	@Column
	private String tgen;
	@Column
	private Integer tsub_code;
	@Column
	private String tparty;
	@Column
	private Double tamount;
	@Column
	private Integer tdc;
	@Column
	private Double tqty;
	@Column
	private Double tunit_pric;
	@Column
	private String tadb;
	@Column
	private String tcon;
	@Column
	private String tprod;
	@Column
	private String tref_no;
	@Column
	private String remarksglcodewithnameofac;
	
	@Column
	private String season;
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTcomp() {
		return tcomp;
	}
	public void setTcomp(Integer tcomp) {
		this.tcomp = tcomp;
	}
	public Integer getTdb() {
		return tdb;
	}
	public void setTdb(Integer tdb) {
		this.tdb = tdb;
	}
	public String getTdoc() {
		return tdoc;
	}
	public void setTdoc(String tdoc) {
		this.tdoc = tdoc;
	}
	public Integer getTserial() {
		return tserial;
	}
	public void setTserial(Integer tserial) {
		this.tserial = tserial;
	}
	public Date getTdt() {
		return tdt;
	}
	public void setTdt(Date tdt) {
		this.tdt = tdt;
	}
	public String getTdes() {
		return tdes;
	}
	public void setTdes(String tdes) {
		this.tdes = tdes;
	}
	public String getTgen() {
		return tgen;
	}
	public void setTgen(String tgen) {
		this.tgen = tgen;
	}
	public Integer getTsub_code() {
		return tsub_code;
	}
	public void setTsub_code(Integer tsub_code) {
		this.tsub_code = tsub_code;
	}
	public String getTparty() {
		return tparty;
	}
	public void setTparty(String tparty) {
		this.tparty = tparty;
	}
	public Double getTamount() {
		return tamount;
	}
	public void setTamount(Double tamount) {
		this.tamount = tamount;
	}
	public Integer getTdc() {
		return tdc;
	}
	public void setTdc(Integer tdc) {
		this.tdc = tdc;
	}
	public Double getTqty() {
		return tqty;
	}
	public void setTqty(Double tqty) {
		this.tqty = tqty;
	}
	public Double getTunit_pric() {
		return tunit_pric;
	}
	public void setTunit_pric(Double tunit_pric) {
		this.tunit_pric = tunit_pric;
	}
	public String getTadb() {
		return tadb;
	}
	public void setTadb(String tadb) {
		this.tadb = tadb;
	}
	public String getTcon() {
		return tcon;
	}
	public void setTcon(String tcon) {
		this.tcon = tcon;
	}
	public String getTprod() {
		return tprod;
	}
	public void setTprod(String tprod) {
		this.tprod = tprod;
	}
	public String getTref_no() {
		return tref_no;
	}
	public void setTref_no(String tref_no) {
		this.tref_no = tref_no;
	}
	public String getRemarksglcodewithnameofac() {
		return remarksglcodewithnameofac;
	}
	public void setRemarksglcodewithnameofac(String remarksglcodewithnameofac) {
		this.remarksglcodewithnameofac = remarksglcodewithnameofac;
	}
	
	
}
