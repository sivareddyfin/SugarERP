package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 *
 */
@Entity
public class Temp_UpdatePaymentsReport {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String village;
	@Column
	private String caneaccperiod;
	@Column
	private String paymetsource;
	@Column
	private Double loanpaid;
	@Column
	private Double sbpaid;
	@Column
	private Double totalpaid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCaneaccperiod() {
		return caneaccperiod;
	}
	public void setCaneaccperiod(String caneaccperiod) {
		this.caneaccperiod = caneaccperiod;
	}
	public String getPaymetsource() {
		return paymetsource;
	}
	public void setPaymetsource(String paymetsource) {
		this.paymetsource = paymetsource;
	}
	public Double getLoanpaid() {
		return loanpaid;
	}
	public void setLoanpaid(Double loanpaid) {
		this.loanpaid = loanpaid;
	}
	public Double getSbpaid() {
		return sbpaid;
	}
	public void setSbpaid(Double sbpaid) {
		this.sbpaid = sbpaid;
	}
	public Double getTotalpaid() {
		return totalpaid;
	}
	public void setTotalpaid(Double totalpaid) {
		this.totalpaid = totalpaid;
	}
	
}
