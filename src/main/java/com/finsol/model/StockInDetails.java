package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class StockInDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
    private String finyr;
	@Column(nullable = false)
    private String stockinno;
	@Column(nullable = false)
    private String productcode;
	@Column(nullable = false)
    private String uom;
	@Column
    private String batchid;
	@Column
    private String expdt;
	@Column(nullable = false)
    private Double quantity;
	@Column(nullable = false)
    private Double mrp;
	@Column(nullable = false)
    private Double cp;
	@Column(nullable = false)
    private Integer fromdeptid;
	@Column(nullable = false)
    private String productsubgrpcode;
	@Column(nullable = false)
    private String productgrpcode;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFinyr() {
		return finyr;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public String getStockinno() {
		return stockinno;
	}
	public void setStockinno(String stockinno) {
		this.stockinno = stockinno;
	}
	
	public String getProductcode() {
		return productcode;
	}
	public String getProductsubgrpcode() {
		return productsubgrpcode;
	}
	public String getProductgrpcode() {
		return productgrpcode;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public void setProductsubgrpcode(String productsubgrpcode) {
		this.productsubgrpcode = productsubgrpcode;
	}
	public void setProductgrpcode(String productgrpcode) {
		this.productgrpcode = productgrpcode;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	
	public String getBatchid() {
		return batchid;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public String getExpdt() {
		return expdt;
	}
	public void setExpdt(String expdt) {
		this.expdt = expdt;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCp() {
		return cp;
	}
	public void setCp(Double cp) {
		this.cp = cp;
	}
	public Integer getFromdeptid() {
		return fromdeptid;
	}
	public void setFromdeptid(Integer fromdeptid) {
		this.fromdeptid = fromdeptid;
	}
	
	
	
	
	
	
	
	
}
