package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_rvr 
{
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column
		private Integer id;
		@Column
		private Double plant;
		@Column
		private Double ratoon;
		@Column
		private Double r1;
		@Column
		private Double r2;
		@Column
		private Double r3;
		@Column
		private String ryotcode;
		@Column
		private String ryotname;
		@Column
		private String relativename;
		@Column
		private Double extentsize;
		@Column
		private String aadhaarnumber;
		@Column
		private String accountnumber;
		@Column
		private String branchname;
		@Column
		private Integer bankcode;
		@Column
		private Integer mandalcode;
		@Column
		private String mandal;
		@Column
		private String village;
		@Column
		private Integer villagecode;
		@Column
		private Integer ryotbankbranchcode;
		
		
		
		
		public String getMandal() {
			return mandal;
		}
		public void setMandal(String mandal) {
			this.mandal = mandal;
		}
		public Integer getMandalcode() {
			return mandalcode;
		}
		public void setMandalcode(Integer mandalcode) {
			this.mandalcode = mandalcode;
		}
		public Integer getBankcode() {
			return bankcode;
		}
		public void setBankcode(Integer bankcode) {
			this.bankcode = bankcode;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		


		public Double getPlant() {
			return plant;
		}
		public void setPlant(Double plant) {
			this.plant = plant;
		}
		public Double getRatoon() {
			return ratoon;
		}
		public void setRatoon(Double ratoon) {
			this.ratoon = ratoon;
		}
		public Double getR1() {
			return r1;
		}
		public void setR1(Double r1) {
			this.r1 = r1;
		}
		public Double getR2() {
			return r2;
		}
		public void setR2(Double r2) {
			this.r2 = r2;
		}
		public Double getR3() {
			return r3;
		}
		public void setR3(Double r3) {
			this.r3 = r3;
		}
		public String getRyotcode() {
			return ryotcode;
		}
		public void setRyotcode(String ryotcode) {
			this.ryotcode = ryotcode;
		}
		public String getRyotname() {
			return ryotname;
		}
		public void setRyotname(String ryotname) {
			this.ryotname = ryotname;
		}
		public String getRelativename() {
			return relativename;
		}
		public void setRelativename(String relativename) {
			this.relativename = relativename;
		}
		public Double getExtentsize() {
			return extentsize;
		}
		public void setExtentsize(Double extentsize) {
			this.extentsize = extentsize;
		}
		public String getAadhaarnumber() {
			return aadhaarnumber;
		}
		public void setAadhaarnumber(String aadhaarnumber) {
			this.aadhaarnumber = aadhaarnumber;
		}
		public String getAccountnumber() {
			return accountnumber;
		}
		public void setAccountnumber(String accountnumber) {
			this.accountnumber = accountnumber;
		}
		public String getBranchname() {
			return branchname;
		}
		public void setBranchname(String branchname) {
			this.branchname = branchname;
		}
		
		public String getVillage() {
			return village;
		}
		public void setVillage(String village) {
			this.village = village;
		}
		public Integer getVillagecode() {
			return villagecode;
		}
		public void setVillagecode(Integer villagecode) {
			this.villagecode = villagecode;
		}
		public Integer getRyotbankbranchcode() {
			return ryotbankbranchcode;
		}
		public void setRyotbankbranchcode(Integer ryotbankbranchcode) {
			this.ryotbankbranchcode = ryotbankbranchcode;
		}
		
		

}
