package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

/**
 * @author DMurty
 */

@Entity
public class SeedlingTrayChargeSummary 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private Integer returnid;
	
	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
    private String ryotcode;
	
	@Column(nullable = true)
	@Type(type="date")
    private Date seedlingdate;
	
	@Column(nullable = false)
    private Double totalamount;
	
	@Column(nullable = false)
    private String loginid;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getReturnid() 
	{
		return returnid;
	}

	public void setReturnid(Integer returnid)
	{
		this.returnid = returnid;
	}

	public String getSeason() 
	{
		return season;
	}

	public void setSeason(String season) 
	{
		this.season = season;
	}

	public String getRyotcode() 
	{
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) 
	{
		this.ryotcode = ryotcode;
	}


	public Date getSeedlingdate()
	{
		return seedlingdate;
	}

	public void setSeedlingdate(Date seedlingdate)
	{
		this.seedlingdate = seedlingdate;
	}

	public Double getTotalamount()
	{
		return totalamount;
	}

	public void setTotalamount(Double totalamount)
	{
		this.totalamount = totalamount;
	}

	public String getLoginid()
	{
		return loginid;
	}

	public void setLoginid(String loginid)
	{
		this.loginid = loginid;
	}
}
