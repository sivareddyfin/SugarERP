package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Umanath Ch
 *
 */
@Entity
public class ProductMaster 
{
	
	
	@Column
    private String categoryCode  ;
	
	@Column
    private String subCategoryCode  ;
	
	@Id
	@Column(unique = true,nullable = false)
    private String productCode;
    
	@Column(nullable = false)  
	private String productName;
	
	@Column(nullable = true)  
	private String description;
    
    @Column
    private Byte status;
    
    @Column(nullable = false)  
	private Integer stockMethodCode;
    
    @Column(nullable = false)  
   	private Integer uom;
    
    @Column(nullable = false)  
   	private Integer productSeqNo;

	public String getCategoryCode()
	{
		return categoryCode;
	}

	public String getSubCategoryCode() {
		return subCategoryCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public String getProductName() {
		return productName;
	}

	public String getDescription() {
		return description;
	}

	public Byte getStatus() {
		return status;
	}



	public Integer getUom() {
		return uom;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public void setSubCategoryCode(String subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	
	public Integer getStockMethodCode() {
		return stockMethodCode;
	}

	public void setStockMethodCode(Integer stockMethodCode) {
		this.stockMethodCode = stockMethodCode;
	}

	public void setUom(Integer uom) {
		this.uom = uom;
	}

	public Integer getProductSeqNo() {
		return productSeqNo;
	}

	public void setProductSeqNo(Integer productSeqNo) {
		this.productSeqNo = productSeqNo;
	}
	
	
    
    


}
