package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 */
@Entity
public class CaneAcctAmounts 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String season;
	@Column 
	private Integer calcid;
	@Column
	private String pricename;
	@Column
	private Double amount;
	@Column
	private Double subsidy;
	@Column
	private Double netamt;
	@Column
	private Byte isthispurchasetax;
	@Column
	private Byte considerforseasonacts;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getCalcid() {
		return calcid;
	}
	public void setCalcid(Integer calcid) {
		this.calcid = calcid;
	}
	public String getPricename() {
		return pricename;
	}
	public void setPricename(String pricename) {
		this.pricename = pricename;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getSubsidy() {
		return subsidy;
	}
	public void setSubsidy(Double subsidy) {
		this.subsidy = subsidy;
	}
	public Double getNetamt() {
		return netamt;
	}
	public void setNetamt(Double netamt) {
		this.netamt = netamt;
	}
	public Byte getIsthispurchasetax() {
		return isthispurchasetax;
	}
	public void setIsthispurchasetax(Byte isthispurchasetax) {
		this.isthispurchasetax = isthispurchasetax;
	}
	public Byte getConsiderforseasonacts() {
		return considerforseasonacts;
	}
	public void setConsiderforseasonacts(Byte considerforseasonacts) {
		this.considerforseasonacts = considerforseasonacts;
	}


}
