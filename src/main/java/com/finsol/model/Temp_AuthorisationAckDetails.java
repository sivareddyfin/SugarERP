package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_AuthorisationAckDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column 
	private String ryotcode;

	@Column 
	private String ryotname;

	@Column 
	private Double totalamout;

	@Column 
	private String billdate;
	
	@Column 
	private String refnumber;
	
	@Column 
	private String authseqno;

	@Column 
	private String storecode;

	@Column 
	private String storename;
	

	public String getStorecode() {
		return storecode;
	}

	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public String getRyotname() {
		return ryotname;
	}

	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}

	public Double getTotalamout() {
		return totalamout;
	}

	public void setTotalamout(Double totalamout) {
		this.totalamout = totalamout;
	}

	public String getBilldate() {
		return billdate;
	}

	public void setBilldate(String billdate) {
		this.billdate = billdate;
	}

	public String getRefnumber() {
		return refnumber;
	}

	public void setRefnumber(String refnumber) {
		this.refnumber = refnumber;
	}

	public String getAuthseqno() {
		return authseqno;
	}

	public void setAuthseqno(String authseqno) {
		this.authseqno = authseqno;
	}


}
