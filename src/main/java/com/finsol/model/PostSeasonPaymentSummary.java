package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class PostSeasonPaymentSummary {

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer pssino;
	 @Column
	 private String villagecode;
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private Date postseasondate;
	 @Column(nullable = false)
	 private String loginid;
	public Integer getPssino() {
		return pssino;
	}
	public void setPssino(Integer pssino) {
		this.pssino = pssino;
	}
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getPostseasondate() {
		return postseasondate;
	}
	public void setPostseasondate(Date postseasondate) {
		this.postseasondate = postseasondate;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	 
}
