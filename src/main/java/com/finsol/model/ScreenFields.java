package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class ScreenFields 
{
	@Column
	private Integer id;
	
	@Column
	private Integer screenid;
	
	@Column
	private String fieldid;
	
	@Column
	private String fieldlabel;
	
	@Column
	private Byte isaudittrail;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getScreenid()
	{
		return screenid;
	}

	public void setScreenid(Integer screenid)
	{
		this.screenid = screenid;
	}

	public String getFieldid() 
	{
		return fieldid;
	}

	public void setFieldid(String fieldid)
	{
		this.fieldid = fieldid;
	}

	public String getFieldlabel()
	{
		return fieldlabel;
	}

	public void setFieldlabel(String fieldlabel)
	{
		this.fieldlabel = fieldlabel;
	}

	public Byte getIsaudittrail()
	{
		return isaudittrail;
	}

	public void setIsaudittrail(Byte isaudittrail)
	{
		this.isaudittrail = isaudittrail;
	}

	public Integer getSlno() {
		return slno;
	}

	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	
	
}
