package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_CanePaymentVoucher
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;  
	@Column 
	private String ryotname;
	@Column 
	private String village;
	@Column 
	private String relativename;
	@Column 
	private Double totalamount;
	@Column 
	private Double suppqty;
	@Column 
	private Double value;
	@Column 
	private Double loan;
	@Column 
	private Double otherded;
	@Column 
	private Double payamount;
	
	@Column 
	private String gtotalwords;
	@Column 
	private String betweendate;
	
	
	public String getGtotalwords() {
		return gtotalwords;
	}
	public void setGtotalwords(String gtotalwords) {
		this.gtotalwords = gtotalwords;
	}
	public String getBetweendate() {
		return betweendate;
	}
	public void setBetweendate(String betweendate) {
		this.betweendate = betweendate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getSuppqty() {
		return suppqty;
	}
	public void setSuppqty(Double suppqty) {
		this.suppqty = suppqty;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getLoan() {
		return loan;
	}
	public void setLoan(Double loan) {
		this.loan = loan;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getPayamount() {
		return payamount;
	}
	public void setPayamount(Double payamount) {
		this.payamount = payamount;
	}
	
	
}
