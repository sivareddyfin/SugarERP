package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class Temp_TransportingContractorServiceRates 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private Double rate;
	@Column 
	private String transporter;
	@Column 
	private Double slno;
	@Column 
	private Integer transportcontcode;
	
	
	public Integer getTransportcontcode() {
		return transportcontcode;
	}
	public void setTransportcontcode(Integer transportcontcode) {
		this.transportcontcode = transportcontcode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getTransporter() {
		return transporter;
	}
	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}
	public Double getSlno() {
		return slno;
	}
	public void setSlno(Double slno) {
		this.slno = slno;
	}

	
	
}
