package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author DMurty
 */
@Entity
public class SampleCards
{
	private static final long serialVersionUID = 1L;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private String season;
	
	@Column(nullable = false)
	private int samplecard;
	
	@Column(nullable = false)
	private int programnumber;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column(nullable = false)
	private String aggrementno;
	
	@Column(nullable = false)
	private String plotno;
	
	@Column(nullable = true)
	private Byte plantorratoon;
	
	@Column(nullable = false)
	private Double extentsize;
	
	@Column(nullable = true)
	private String pol;
	
	@Column(nullable = true)
	private String brix;
	
	@Column(nullable = true)
	private String purity;
	
	@Column(nullable = true)
	private Double ccsrating;
	
	@Column(nullable = true)
	private Double avgccsrating;
	
	@Column(nullable = true)
	private Double fgavgccsrating;
	
	@Column	
	private Date sampledate;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public int getSamplecard() {
		return samplecard;
	}

	public void setSamplecard(int samplecard) {
		this.samplecard = samplecard;
	}

	public int getProgramnumber() {
		return programnumber;
	}

	public void setProgramnumber(int programnumber) {
		this.programnumber = programnumber;
	}

	

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	
	public String getAggrementno() {
		return aggrementno;
	}

	public void setAggrementno(String aggrementno) {
		this.aggrementno = aggrementno;
	}

	public String getPlotno() {
		return plotno;
	}

	public void setPlotno(String plotno) {
		this.plotno = plotno;
	}

	
	public Byte getPlantorratoon() 
	{
		return plantorratoon;
	}

	public void setPlantorratoon(Byte plantorratoon) 
	{
		this.plantorratoon = plantorratoon;
	}

	public Double getExtentsize() {
		return extentsize;
	}

	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getBrix() {
		return brix;
	}

	public void setBrix(String brix) {
		this.brix = brix;
	}

	public String getPurity() {
		return purity;
	}

	public void setPurity(String purity) {
		this.purity = purity;
	}

	public Double getCcsrating() {
		return ccsrating;
	}

	public void setCcsrating(Double ccsrating) {
		this.ccsrating = ccsrating;
	}

	public Double getAvgccsrating() {
		return avgccsrating;
	}

	public void setAvgccsrating(Double avgccsrating) {
		this.avgccsrating = avgccsrating;
	}

	public Double getFgavgccsrating() {
		return fgavgccsrating;
	}

	public void setFgavgccsrating(Double fgavgccsrating) {
		this.fgavgccsrating = fgavgccsrating;
	}

	public Date getSampledate() {
		return sampledate;
	}

	public void setSampledate(Date sampledate) {
		this.sampledate = sampledate;
	}

	
	
}
