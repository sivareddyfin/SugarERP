package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
/**
 * @author Umanath Ch
 *
 */
@Entity
public class KindIndentDetails
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer seqNo;
	@Column
	private String indentNo;
	@Column
	private Integer kind;
	@Column(nullable = false)
	@Type(type="date")
	private Date batchDate;
	@Column
	private Double quantity;
	@Column
	private String uom;
	@Column
	private String dispatchNo;
	@Column
	private String landVillageCode;
	@Column
	private Integer indentStatus;
	@Column
	private Double dispatchedQuantity;
	@Column
	private Double pendingQuantity;
	@Column(nullable = true)
	private Integer variety;
	@Column
	private String ryotCode;
	
	public String getRyotCode() {
		return ryotCode;
	}

	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}

	public Double getQuantity() {
		return quantity;
	}

	public Double getDispatchedQuantity() {
		return dispatchedQuantity;
	}

	public Double getPendingQuantity() {
		return pendingQuantity;
	}

	public Integer getVariety() {
		return variety;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public void setDispatchedQuantity(Double dispatchedQuantity) {
		this.dispatchedQuantity = dispatchedQuantity;
	}

	public void setPendingQuantity(Double pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}

	public void setVariety(Integer variety) {
		this.variety = variety;
	}

	public String getDispatchNo() {
		return dispatchNo;
	}

	public void setDispatchNo(String dispatchNo) {
		this.dispatchNo = dispatchNo;
	}
	
	public Integer getIndentStatus() {
		return indentStatus;
	}

	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}

	public Integer getSlNo() 
	{
		return slNo;
	}
	public Integer getId() {
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public String getIndentNo() {
		return indentNo;
	}
	
	public Date getBatchDate() {
		return batchDate;
	}
	
	public String getUom() {
		return uom;
	}
	
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}
	
	public Integer getKind() {
		return kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}
	
	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getLandVillageCode() {
		return landVillageCode;
	}

	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	
	
	
	
}
