package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */

@Entity
public class AdvanceCategory 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	
	@Column(unique = true,nullable = false)
    private Integer advancecategorycode;
	
	@Column
    private String name;
	
	@Column
    private String description;
	
	@Column
    private Integer advancecode;
	
	@Column
    private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdvancecategorycode() {
		return advancecategorycode;
	}

	public void setAdvancecategorycode(Integer advancecategorycode) {
		this.advancecategorycode = advancecategorycode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAdvancecode() {
		return advancecode;
	}

	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}


}
