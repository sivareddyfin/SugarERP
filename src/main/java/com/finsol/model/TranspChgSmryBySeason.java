package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class TranspChgSmryBySeason {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private  String season;
	@Column
	private  Double transportedcanewt;
	@Column
	private  Double totalextent;
	@Column
	private  Double totalamount;
	@Column
	private  Double totalpayableamount;
	@Column
	private  Double pendingpayable;
	@Column
	private  Double paidAmount;	
	@Column
	private  Double pendingamount;
	
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getTransportedcanewt() {
		return transportedcanewt;
	}
	public void setTransportedcanewt(Double transportedcanewt) {
		this.transportedcanewt = transportedcanewt;
	}
	public Double getTotalextent() {
		return totalextent;
	}
	public void setTotalextent(Double totalextent) {
		this.totalextent = totalextent;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getTotalpayableamount() {
		return totalpayableamount;
	}
	public void setTotalpayableamount(Double totalpayableamount) {
		this.totalpayableamount = totalpayableamount;
	}
	public Double getPendingpayable() {
		return pendingpayable;
	}
	public void setPendingpayable(Double pendingpayable) {
		this.pendingpayable = pendingpayable;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	
	
	


}
