package com.finsol.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author DMurty
 *
 */
@Entity
public class CaneValueDetailsByRyot
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(nullable = false)
	private String season;
	
	@Column(nullable = false)
	private String villagecode;
	
	@Column(nullable = false)
	private int ryotcodeseqno;
	
	@Column(nullable = false)
	private String ryotcode;
	
	@Column
	private double suppliedcanewt;
	
	@Column
	private String calculationitem;
	
	@Column
	private double canevalue;
	
	@Column
	private Byte isaccruedamountanddue;

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public int getRyotcodeseqno() {
		return ryotcodeseqno;
	}

	public void setRyotcodeseqno(int ryotcodeseqno) {
		this.ryotcodeseqno = ryotcodeseqno;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public double getSuppliedcanewt() {
		return suppliedcanewt;
	}

	public void setSuppliedcanewt(double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}

	public String getCalculationitem() {
		return calculationitem;
	}

	public void setCalculationitem(String calculationitem) {
		this.calculationitem = calculationitem;
	}

	public double getCanevalue() {
		return canevalue;
	}

	public void setCanevalue(double canevalue) {
		this.canevalue = canevalue;
	}

	public Byte getIsaccruedamountanddue() {
		return isaccruedamountanddue;
	}

	public void setIsaccruedamountanddue(Byte isaccruedamountanddue) {
		this.isaccruedamountanddue = isaccruedamountanddue;
	}
}
