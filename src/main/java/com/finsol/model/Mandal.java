package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class Mandal 
{
	private static final long serialVersionUID = 1L;
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 
	 @Column(unique = true,nullable = false)
	 private Integer mandalcode;
	 
	 @Column
	 private String mandal;
	 
	 @Column
	 private String description;
	 
	 @Column
	 private Integer zonecode;
	 
	 @Column
	 private Byte status;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getMandalcode() 
	{
		return mandalcode;
	}

	public void setMandalcode(Integer mandalcode)
	{
		this.mandalcode = mandalcode;
	}



	public String getMandal() {
		return mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public Integer getZonecode() 
	{
		return zonecode;
	}

	public void setZonecode(Integer zonecode) 
	{
		this.zonecode = zonecode;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status) 
	{
		this.status = status;
	}
	 
}
