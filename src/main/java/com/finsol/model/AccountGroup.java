package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author DMurty
 *
 */
@Entity
public class AccountGroup 
{
	private static final long serialVersionUID = 1L;
	
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Id
	@Column(unique = true,nullable = false)
    private Integer accountgroupcode;
	
	@Column
    private String accountgroup;
    
    @Column
    private String description;
    
    @Column
    private Byte status;
    
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getAccountgroupcode()
	{
		return accountgroupcode;
	}

	public void setAccountgroupcode(Integer accountgroupcode)
	{
		this.accountgroupcode = accountgroupcode;
	}

	public String getAccountgroup()
	{
		return accountgroup;
	}

	public void setAccountgroup(String accountgroup)
	{
		this.accountgroup = accountgroup;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Byte getStatus() 
	{
		return status;
	}

	public void setStatus(Byte status)
	{
		this.status = status;
	}
}
