package com.finsol.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Murty Ayyagari
 */
@Entity
public class StoreBatchMaster 
{
	@Id
	@Column(nullable = false)
	private String batchid;
	
	@Column(nullable = false)
	private int seqno;
	
	@Column(nullable = false)
	private String productcode;
	
	@Column(nullable = false)
	private String batch;
	
	@Column(nullable = false)
	private double mrp;
	
	@Column(nullable = false)
	private double cp;
	
	@Column(nullable = false)
	private String expdt;
	
	@Column(nullable = false)
	private String mfgdt;
	
	@Type(type="date")
	private Date batchdate;

	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public String getProductcode() {
		return productcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public String getExpdt() {
		return expdt;
	}

	public void setExpdt(String expdt) {
		this.expdt = expdt;
	}

	public String getMfgdt() {
		return mfgdt;
	}

	public void setMfgdt(String mfgdt) {
		this.mfgdt = mfgdt;
	}

	public Date getBatchdate() {
		return batchdate;
	}

	public void setBatchdate(Date batchdate) {
		this.batchdate = batchdate;
	}

	public double getCp() {
		return cp;
	}

	public void setCp(double cp) {
		this.cp = cp;
	}
	
}
