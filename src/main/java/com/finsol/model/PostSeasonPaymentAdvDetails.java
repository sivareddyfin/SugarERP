package com.finsol.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class PostSeasonPaymentAdvDetails {
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer pssino;
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private String canesuppliercode;
	 @Column(nullable = false)
	 private String advance;
	 @Column(nullable = false)
	 private String advancecode;
	 @Column(nullable = false)
	 private Double principle;
	 @Column(nullable = false)
	 private Date issuingdate;
	 @Column(nullable = false)
	 private Date repaymentdate;
	 @Column(nullable = false)
	 private Double interestrate;
	 @Column(nullable = false)
	 private Double interestamount;
	 @Column(nullable = false)
	 private Double totalpayable;
	 @Column(nullable = false)
	 private Double paidamount;
	public Integer getPssino() {
		return pssino;
	}
	public void setPssino(Integer pssino) {
		this.pssino = pssino;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getCanesuppliercode() {
		return canesuppliercode;
	}
	public void setCanesuppliercode(String canesuppliercode) {
		this.canesuppliercode = canesuppliercode;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	public String getAdvancecode() {
		return advancecode;
	}
	public void setAdvancecode(String advancecode) {
		this.advancecode = advancecode;
	}
	public Double getPrinciple() {
		return principle;
	}
	public void setPrinciple(Double principle) {
		this.principle = principle;
	}
	public Date getIssuingdate() {
		return issuingdate;
	}
	public void setIssuingdate(Date issuingdate) {
		this.issuingdate = issuingdate;
	}
	public Date getRepaymentdate() {
		return repaymentdate;
	}
	public void setRepaymentdate(Date repaymentdate) {
		this.repaymentdate = repaymentdate;
	}
	public Double getInterestrate() {
		return interestrate;
	}
	public void setInterestrate(Double interestrate) {
		this.interestrate = interestrate;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalpayable() {
		return totalpayable;
	}
	public void setTotalpayable(Double totalpayable) {
		this.totalpayable = totalpayable;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	 
}
