package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 */
@Entity
public class ProgramVariety {
	
	@Id
	private Integer id;
	 @Column
	 private Integer programnumber;
	 @Column(nullable = false)
	 private String season;
	 @Column(nullable = false)
	 private String varietycode;
	 
	 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProgramnumber() {
		return programnumber;
	}
	public void setProgramnumber(Integer programnumber) {
		this.programnumber = programnumber;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getVarietycode() {
		return varietycode;
	}
	public void setVarietycode(String varietycode) {
		this.varietycode = varietycode;
	}
	 
}
