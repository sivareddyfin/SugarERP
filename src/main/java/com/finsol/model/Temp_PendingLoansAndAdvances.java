package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Naidu
 *
 */
@Entity
public class Temp_PendingLoansAndAdvances {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String village;
	@Column
	private String loanno;
	@Column
	private Integer bankcode;
	@Column
	private String refno;
	@Column
	private Double supquantity;
	@Column
	private Double advloanamt;
	@Column
	private Double totalrecovery;
	@Column
	private Double totalbal;
	@Column
	private Double intramt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public Integer getBankcode() {
		return bankcode;
	}
	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}
	public String getRefno() {
		return refno;
	}
	public void setRefno(String refno) {
		this.refno = refno;
	}
	public Double getSupquantity() {
		return supquantity;
	}
	public void setSupquantity(Double supquantity) {
		this.supquantity = supquantity;
	}
	public Double getAdvloanamt() {
		return advloanamt;
	}
	public void setAdvloanamt(Double advloanamt) {
		this.advloanamt = advloanamt;
	}
	public Double getTotalrecovery() {
		return totalrecovery;
	}
	public void setTotalrecovery(Double totalrecovery) {
		this.totalrecovery = totalrecovery;
	}
	public Double getTotalbal() {
		return totalbal;
	}
	public void setTotalbal(Double totalbal) {
		this.totalbal = totalbal;
	}
	public Double getIntramt() {
		return intramt;
	}
	public void setIntramt(Double intramt) {
		this.intramt = intramt;
	}
	public String getLoanno() {
		return loanno;
	}
	public void setLoanno(String loanno) {
		this.loanno = loanno;
	}
	
}
