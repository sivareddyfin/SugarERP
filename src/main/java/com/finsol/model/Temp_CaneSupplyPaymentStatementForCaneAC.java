package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */

@Entity
public class Temp_CaneSupplyPaymentStatementForCaneAC 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String branchname;
	@Column 
	private Integer ryotbankcode;
	@Column 
	private String aadhaarnumber;
	@Column
	private String accountnumber;
	@Column 
	private Double suppliedcanewt;
	@Column 
	private Double totalsbacpayable;
	@Column 
	private Double transportchargespayable;
	@Column 
	private Double harvestchargespayable;
	@Column 
	private Double cdc;
	@Column 
	private Double uc;
	@Column 
	private Double otherded;
	@Column
	private Double netprice;
	@Column
	private Double totalvalue;
	@Column
	private Double foradvandloans;
	@Column
	private Double lotaladvpayable;
	@Column
	private Double totalloanpayable;
	@Column
	private Double forsbacs;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Integer getRyotbankcode() {
		return ryotbankcode;
	}
	public void setRyotbankcode(Integer ryotbankcode) {
		this.ryotbankcode = ryotbankcode;
	}
	public String getAadhaarnumber() {
		return aadhaarnumber;
	}
	public void setAadhaarnumber(String aadhaarnumber) {
		this.aadhaarnumber = aadhaarnumber;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public Double getSuppliedcanewt() {
		return suppliedcanewt;
	}
	public void setSuppliedcanewt(Double suppliedcanewt) {
		this.suppliedcanewt = suppliedcanewt;
	}
	public Double getTotalsbacpayable() {
		return totalsbacpayable;
	}
	public void setTotalsbacpayable(Double totalsbacpayable) {
		this.totalsbacpayable = totalsbacpayable;
	}
	public Double getTransportchargespayable() {
		return transportchargespayable;
	}
	public void setTransportchargespayable(Double transportchargespayable) {
		this.transportchargespayable = transportchargespayable;
	}
	public Double getHarvestchargespayable() {
		return harvestchargespayable;
	}
	public void setHarvestchargespayable(Double harvestchargespayable) {
		this.harvestchargespayable = harvestchargespayable;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getUc() {
		return uc;
	}
	public void setUc(Double uc) {
		this.uc = uc;
	}
	public Double getOtherded() {
		return otherded;
	}
	public void setOtherded(Double otherded) {
		this.otherded = otherded;
	}
	public Double getNetprice() {
		return netprice;
	}
	public void setNetprice(Double netprice) {
		this.netprice = netprice;
	}
	public Double getTotalvalue() {
		return totalvalue;
	}
	public void setTotalvalue(Double totalvalue) {
		this.totalvalue = totalvalue;
	}
	public Double getForadvandloans() {
		return foradvandloans;
	}
	public void setForadvandloans(Double foradvandloans) {
		this.foradvandloans = foradvandloans;
	}
	public Double getLotaladvpayable() {
		return lotaladvpayable;
	}
	public void setLotaladvpayable(Double lotaladvpayable) {
		this.lotaladvpayable = lotaladvpayable;
	}
	public Double getTotalloanpayable() {
		return totalloanpayable;
	}
	public void setTotalloanpayable(Double totalloanpayable) {
		this.totalloanpayable = totalloanpayable;
	}
	public Double getForsbacs() {
		return forsbacs;
	}
	public void setForsbacs(Double forsbacs) {
		this.forsbacs = forsbacs;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	
}
