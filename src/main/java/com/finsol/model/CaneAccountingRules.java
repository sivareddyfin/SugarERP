package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;


/**
 * @author DMurty
 */
@Entity
public class CaneAccountingRules
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
    private String season;
	
	@Column(nullable = false)
	@Type(type="date")
    private Date date;
	
	@Column(nullable = false)
    private Double frp;
	
	@Column(nullable = false)
    private Double frpsubsidyamt;
	
	@Column(nullable = false)
    private Double additionalcaneprice;
	
	@Column(nullable = false)
    private Double icp;
	
	@Column(nullable = false)
    private Double netcaneprice;
	
	@Column(nullable = false)
    private Double seasoncaneprice;
	
	@Column(nullable = false)
    private Double postseasoncaneprice;
	
	@Column(nullable = false)
    private Byte ispercentoramt;
	
	@Column
    private Double amtforloans=0.0;
	
	@Column
    private Double amtforded=0.0;
	
	@Column
    private Double percentforded=0.0;
	
	@Column
    private Double percentforloans=0.0;
	
	@Column
    private Byte istaapplicable;
	
	@Column
    private Double noofkms;
	
	@Column
    private Double allowanceperkmperton;
	
	@Column(nullable = false)
    private Double loanRecovery;
	

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getSeason() 
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public Double getFrp() 
	{
		return frp;
	}

	public void setFrp(Double frp) 
	{
		this.frp = frp;
	}

	public Double getFrpsubsidyamt() 
	{
		return frpsubsidyamt;
	}

	public void setFrpsubsidyamt(Double frpsubsidyamt) 
	{
		this.frpsubsidyamt = frpsubsidyamt;
	}

	public Double getAdditionalcaneprice()
	{
		return additionalcaneprice;
	}

	public void setAdditionalcaneprice(Double additionalcaneprice)
	{
		this.additionalcaneprice = additionalcaneprice;
	}

	public Double getIcp()
	{
		return icp;
	}

	public void setIcp(Double icp) 
	{
		this.icp = icp;
	}

	public Double getNetcaneprice() 
	{
		return netcaneprice;
	}

	public void setNetcaneprice(Double netcaneprice) 
	{
		this.netcaneprice = netcaneprice;
	}

	public Double getSeasoncaneprice()
	{
		return seasoncaneprice;
	}

	public void setSeasoncaneprice(Double seasoncaneprice)
	{
		this.seasoncaneprice = seasoncaneprice;
	}

	public Double getPostseasoncaneprice()
	{
		return postseasoncaneprice;
	}

	public void setPostseasoncaneprice(Double postseasoncaneprice)
	{
		this.postseasoncaneprice = postseasoncaneprice;
	}

	public Byte getIspercentoramt()
	{
		return ispercentoramt;
	}

	public void setIspercentoramt(Byte ispercentoramt) 
	{
		this.ispercentoramt = ispercentoramt;
	}

	public Double getAmtforloans() 
	{
		return amtforloans;
	}

	public void setAmtforloans(Double amtforloans) 
	{
		this.amtforloans = amtforloans;
	}

	public Double getAmtforded()
	{
		return amtforded;
	}

	public void setAmtforded(Double amtforded)
	{
		this.amtforded = amtforded;
	}

	public Double getPercentforded() 
	{
		return percentforded;
	}

	public void setPercentforded(Double percentforded) 
	{
		this.percentforded = percentforded;
	}

	public Double getPercentforloans()
	{
		return percentforloans;
	}

	public void setPercentforloans(Double percentforloans)
	{
		this.percentforloans = percentforloans;
	}

	public Byte getIstaapplicable() 
	{
		return istaapplicable;
	}

	public void setIstaapplicable(Byte istaapplicable)
	{
		this.istaapplicable = istaapplicable;
	}

	public Double getNoofkms() 
	{
		return noofkms;
	}

	public void setNoofkms(Double noofkms) 
	{
		this.noofkms = noofkms;
	}

	public Double getAllowanceperkmperton()
	{
		return allowanceperkmperton;
	}

	public void setAllowanceperkmperton(Double allowanceperkmperton)
	{
		this.allowanceperkmperton = allowanceperkmperton;
	}
	
	public Double getLoanRecovery()
	{
		return loanRecovery;
	}
	public void setLoanRecovery(Double loanRecovery)
	{
		this.loanRecovery = loanRecovery;
	}
}
