package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;


/**
 * @author naidu
 */

@Entity
public class Temp_agrchecklist {
	
	
	//village,agreementnumber,ryotcode,ryotname,relativename,landvillagecode,plantorratoon,variety,
	//cropdate,extentsize,estimatedqty
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	@Column 
	private String village;
	@Column 
	private String agreementnumber;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private String landvillagecode;
	@Column 
	private String variety;
	@Column 
	private String cropdate;
	@Column 
	private Double plant1;
	@Column 
	private Double ratoon1;
	@Column 
	private Double pqty1;
	@Column 
	private Double rqty1;

	@Column 
	private Integer agreementsqeqno;
	
	
	public Integer getAgreementsqeqno() {
		return agreementsqeqno;
	}
	public void setAgreementsqeqno(Integer agreementsqeqno) {
		this.agreementsqeqno = agreementsqeqno;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getLandvillagecode() {
		return landvillagecode;
	}
	public void setLandvillagecode(String landvillagecode) {
		this.landvillagecode = landvillagecode;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getCropdate() {
		return cropdate;
	}
	public void setCropdate(String cropdate) {
		this.cropdate = cropdate;
	}
	public Double getPlant1() {
		return plant1;
	}
	public void setPlant1(Double plant1) {
		this.plant1 = plant1;
	}
	public Double getRatoon1() {
		return ratoon1;
	}
	public void setRatoon1(Double ratoon1) {
		this.ratoon1 = ratoon1;
	}
	public Double getPqty1() {
		return pqty1;
	}
	public void setPqty1(Double pqty1) {
		this.pqty1 = pqty1;
	}
	public Double getRqty1() {
		return rqty1;
	}
	public void setRqty1(Double rqty1) {
		this.rqty1 = rqty1;
	}
	
	
}
