package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_villagemandalsummary {
	
	private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column
		private Integer id;
			
		@Column
		private Double plant;
		@Column
		private Double ratoon;
		@Column
		private Double r1;
		@Column
		private Double r2;
		@Column
		private Double r3;
		@Column
		private Double extentsize;
		@Column
		private String village;
		@Column
		private String ryotcode;
		@Column
		private String villagecode;
		@Column
		private String mandal;
		@Column
		private Integer mandalcode;
		@Column
		private Double agreedqty;
		
		
		
		public String getRyotcode() {
			return ryotcode;
		}
		public void setRyotcode(String ryotcode) {
			this.ryotcode = ryotcode;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Double getPlant() {
			return plant;
		}
		public void setPlant(Double plant) {
			this.plant = plant;
		}
		public Double getRatoon() {
			return ratoon;
		}
		public void setRatoon(Double ratoon) {
			this.ratoon = ratoon;
		}
		public Double getR1() {
			return r1;
		}
		public void setR1(Double r1) {
			this.r1 = r1;
		}
		public Double getR2() {
			return r2;
		}
		public void setR2(Double r2) {
			this.r2 = r2;
		}
		public Double getR3() {
			return r3;
		}
		public void setR3(Double r3) {
			this.r3 = r3;
		}
		public Double getExtentsize() {
			return extentsize;
		}
		public void setExtentsize(Double extentsize) {
			this.extentsize = extentsize;
		}
		public String getVillage() {
			return village;
		}
		public void setVillage(String village) {
			this.village = village;
		}
		
		
		
		public String getVillagecode() {
			return villagecode;
		}
		public void setVillagecode(String villagecode) {
			this.villagecode = villagecode;
		}
		public String getMandal() {
			return mandal;
		}
		public void setMandal(String mandal) {
			this.mandal = mandal;
		}
		public Integer getMandalcode() {
			return mandalcode;
		}
		public void setMandalcode(Integer mandalcode) {
			this.mandalcode = mandalcode;
		}
		public Double getAgreedqty() {
			return agreedqty;
		}
		public void setAgreedqty(Double agreedqty) {
			this.agreedqty = agreedqty;
		}
		
		
		

		
		
}
