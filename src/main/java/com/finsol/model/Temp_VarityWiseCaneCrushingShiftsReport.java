package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_VarityWiseCaneCrushingShiftsReport 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	@Column
	private String variety;
	@Column
	private Double plant1;
	@Column
	private Double ratoon1;
	@Column
	private Double plant2;
	@Column
	private Double ratoon2;
	@Column
	private Double plant3;
	@Column
	private Double ratoon3;
	@Column
	private Double total1;
	@Column
	private Double total2;
	@Column
	private Double total3;
	@Column
	private Double percentage1;
	@Column
	private Double percentage2;
	@Column
	private Double percentage3;
	
	
	
	public Double getPercentage1() {
		return percentage1;
	}
	public void setPercentage1(Double percentage1) {
		this.percentage1 = percentage1;
	}
	public Double getPercentage2() {
		return percentage2;
	}
	public void setPercentage2(Double percentage2) {
		this.percentage2 = percentage2;
	}
	public Double getPercentage3() {
		return percentage3;
	}
	public void setPercentage3(Double percentage3) {
		this.percentage3 = percentage3;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getPlant1() {
		return plant1;
	}
	public void setPlant1(Double plant1) {
		this.plant1 = plant1;
	}
	public Double getRatoon1() {
		return ratoon1;
	}
	public void setRatoon1(Double ratoon1) {
		this.ratoon1 = ratoon1;
	}
	public Double getPlant2() {
		return plant2;
	}
	public void setPlant2(Double plant2) {
		this.plant2 = plant2;
	}
	public Double getRatoon2() {
		return ratoon2;
	}
	public void setRatoon2(Double ratoon2) {
		this.ratoon2 = ratoon2;
	}
	public Double getPlant3() {
		return plant3;
	}
	public void setPlant3(Double plant3) {
		this.plant3 = plant3;
	}
	public Double getRatoon3() {
		return ratoon3;
	}
	public void setRatoon3(Double ratoon3) {
		this.ratoon3 = ratoon3;
	}
	public Double getTotal1() {
		return total1;
	}
	public void setTotal1(Double total1) {
		this.total1 = total1;
	}
	public Double getTotal2() {
		return total2;
	}
	public void setTotal2(Double total2) {
		this.total2 = total2;
	}
	public Double getTotal3() {
		return total3;
	}
	public void setTotal3(Double total3) {
		this.total3 = total3;
	}
	
	
}
