package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_VegetableFrootsProduction 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String description;
	@Column 
	private String sourceofseed;
	@Column 
	private Double TotalNoOfSeedlingss;
	@Column 
	private Integer totalTrays;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSourceofseed() {
		return sourceofseed;
	}
	public void setSourceofseed(String sourceofseed) {
		this.sourceofseed = sourceofseed;
	}
	public Double getTotalNoOfSeedlingss() {
		return TotalNoOfSeedlingss;
	}
	public void setTotalNoOfSeedlingss(Double totalNoOfSeedlingss) {
		TotalNoOfSeedlingss = totalNoOfSeedlingss;
	}
	public Integer getTotalTrays() {
		return totalTrays;
	}
	public void setTotalTrays(Integer totalTrays) {
		this.totalTrays = totalTrays;
	}
	
	  

}
