package com.finsol.model;


import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 */
@Entity
public class Temp_CaneWeighmentReport
{
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 @Column
	 private Integer serialnumber;
	 @Column
	 private String ryotname;
	 @Column
	 private String ryotcode;
	 @Column
	 private String relativename;
	 @Column
	 private String village;
	 @Column
	 private String circle;
	 @Column
	 private String variety;
	 @Column
	 private String shiftname;
	 @Column
	 private Integer shiftid;
	 @Column
	 private Integer weighbridgeslno;
	 @Column
	 @Type(type="date")
	 private Date canereceiptdate;
	 @Column
	 private Timestamp canereceipttime;
	 @Column
	 private String plantorratoon;
	 @Column
	 private Integer lorryortruck;
	 @Column
	 private Integer vehicletype;
	 @Column
	 private String vehicleno;
	 @Column
	 private  Double grossweight;
	 @Column
	 private  Double netwt;
	 @Column
	 private  Double bindingmtlwght;  
	 @Column
	 private  Double lorrywt;
	 @Column
	 private  Double lorryandcanewt;
	 @Column
	 private String landvilcode;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSerialnumber() {
		return serialnumber;
	}
	public void setSerialnumber(Integer serialnumber) {
		this.serialnumber = serialnumber;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getShiftname() {
		return shiftname;
	}
	public void setShiftname(String shiftname) {
		this.shiftname = shiftname;
	}
	public Integer getShiftid() {
		return shiftid;
	}
	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}
	public Integer getWeighbridgeslno() {
		return weighbridgeslno;
	}
	public void setWeighbridgeslno(Integer weighbridgeslno) {
		this.weighbridgeslno = weighbridgeslno;
	}
	public Date getCanereceiptdate() {
		return canereceiptdate;
	}
	public void setCanereceiptdate(Date canereceiptdate) {
		this.canereceiptdate = canereceiptdate;
	}
	
	
	public Timestamp getCanereceipttime() {
		return canereceipttime;
	}
	public void setCanereceipttime(Timestamp canereceipttime) {
		this.canereceipttime = canereceipttime;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public Integer getLorryortruck() {
		return lorryortruck;
	}
	public void setLorryortruck(Integer lorryortruck) {
		this.lorryortruck = lorryortruck;
	}
	public Integer getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(Integer vehicletype) {
		this.vehicletype = vehicletype;
	}
	public String getVehicleno() {
		return vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public Double getGrossweight() {
		return grossweight;
	}
	public void setGrossweight(Double grossweight) {
		this.grossweight = grossweight;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Double getBindingmtlwght() {
		return bindingmtlwght;
	}
	public void setBindingmtlwght(Double bindingmtlwght) {
		this.bindingmtlwght = bindingmtlwght;
	}
	public Double getLorrywt() {
		return lorrywt;
	}
	public void setLorrywt(Double lorrywt) {
		this.lorrywt = lorrywt;
	}
	public Double getLorryandcanewt() {
		return lorryandcanewt;
	}
	public void setLorryandcanewt(Double lorryandcanewt) {
		this.lorryandcanewt = lorryandcanewt;
	}
	public String getLandvilcode() {
		return landvilcode;
	}
	public void setLandvilcode(String landvilcode) {
		this.landvilcode = landvilcode;
	}
	 

}
