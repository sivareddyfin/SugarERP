package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class TrayTracker 
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = true)
	private String season;
	
	@Column(nullable = true)
	@Type(type="date")
	private Date transactionDate;
	
	@Column(nullable = true)
	private String transactionRefNo;
	
	@Column(nullable = true)
	private Integer trayType;
	
	@Column(nullable = true)
	private Double tranInandOut;
	
	@Column(nullable = true)
	private String damaged;
	
	@Column(nullable = true)
	private String working;
	
	@Column(nullable = true)
	private String ryotCode;
	
	@Column(nullable = true)
	private String loginUser;

	public Integer getId() {
		return id;
	}

	public String getSeason() {
		return season;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public Integer getTrayType() {
		return trayType;
	}

	public Double getTranInandOut() {
		return tranInandOut;
	}

	public String getDamaged() {
		return damaged;
	}

	public String getWorking() {
		return working;
	}

	public String getRyotCode() {
		return ryotCode;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public void setTrayType(Integer trayType) {
		this.trayType = trayType;
	}

	public void setTranInandOut(Double tranInandOut) {
		this.tranInandOut = tranInandOut;
	}

	public void setDamaged(String damaged) {
		this.damaged = damaged;
	}

	public void setWorking(String working) {
		this.working = working;
	}

	public void setRyotCode(String ryotCode) {
		this.ryotCode = ryotCode;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	
	
	
	

	

	

	
	
	
	
	
}
