package com.finsol.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva    
 */

@Entity
public class Temp_CirclePlantRatoonWiseRyot 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column 
	private Integer circlecode;
	@Column 
	private String circle;
	@Column 
	private String village;
	@Column 
	private String villagecode;
	@Column 
	private String variety;
	@Column 
	private String agreementnumber;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private Double plant;
	@Column 
	private Double ratoon;
	@Column 
	private String dateofplanting;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCirclecode() {
		return circlecode;
	}
	public void setCirclecode(Integer circlecode) {
		this.circlecode = circlecode;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getRatoon() {
		return ratoon;
	}
	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}
	public String getDateofplanting() {
		return dateofplanting;
	}
	public void setDateofplanting(String dateofplanting) {
		this.dateofplanting = dateofplanting;
	}
	
	
	

}
