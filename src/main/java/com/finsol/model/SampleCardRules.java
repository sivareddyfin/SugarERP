package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SampleCardRules {
	
	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column
	private Integer id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	private Double sizefrom;
	
	@Column(nullable = false)
	private Double sizeto;
	
	@Column(nullable = false)
	private Integer noofsamplecards;

	public Double getSizefrom() {
		return sizefrom;
	}

	public void setSizefrom(Double sizefrom) {
		this.sizefrom = sizefrom;
	}

	public Double getSizeto() {
		return sizeto;
	}

	public void setSizeto(Double sizeto) {
		this.sizeto = sizeto;
	}

	public Integer getNoofsamplecards() {
		return noofsamplecards;
	}

	public void setNoofsamplecards(Integer noofsamplecards) {
		this.noofsamplecards = noofsamplecards;
	}

	
	
}
