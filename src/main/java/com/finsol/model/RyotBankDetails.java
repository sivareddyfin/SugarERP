package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class RyotBankDetails {
	
private static final long serialVersionUID = 1L;
	
	@Column	
	private Integer id;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	 @Column
	 private String season;
	 
	@Column
	private String ryotcode;
	
	@Column
	private Integer ryotbankbranchcode;
	
	@Column
	private String ifsccode;
	
	@Column
	private String accountnumber;
	
	@Column
	private Byte accounttype;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public Integer getSlno() {
		return slno;
	}

	public void setSlno(Integer slno) {
		this.slno = slno;
	}

	public String getRyotcode() {
		return ryotcode;
	}

	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}

	public Integer getRyotbankbranchcode() {
		return ryotbankbranchcode;
	}

	public void setRyotbankbranchcode(Integer ryotbankbranchcode) {
		this.ryotbankbranchcode = ryotbankbranchcode;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}


	public Byte getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(Byte accounttype) {
		this.accounttype = accounttype;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	

}
