package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class CaneManager {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(unique = true,nullable = false)
    private Integer canemanagerid;
	
	@Column
    private String canemanagername;
    
	@Column
    private Integer employeeid;
	
    @Column
    private String description;
    
    @Column
    private Byte status;
    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCanemanagerid() {
		return canemanagerid;
	}

	public void setCanemanagerid(Integer canemanagerid) {
		this.canemanagerid = canemanagerid;
	}

	public String getCanemanagername() {
		return canemanagername;
	}

	public void setCanemanagername(String canemanagername) {
		this.canemanagername = canemanagername;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Integer getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(Integer employeeid) {
		this.employeeid = employeeid;
	}

}
