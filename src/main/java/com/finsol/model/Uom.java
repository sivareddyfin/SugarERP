package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author umanath ch
 */

@Entity
public class Uom
{

	@Id
	@Column
	private Integer id;
	@Column(unique = true,nullable = false)
    private Integer uomCode;
	@Column(nullable = false)
	private String uom;
	@Column(nullable = true)
    private String description;
	@Column(nullable = false)
    private Byte status;
	
	
	public Integer getId() 
	{
		return id;
	}
	public Integer getUomCode() {
		return uomCode;
	}
	public String getUom() {
		return uom;
	}
	public String getDescription() {
		return description;
	}
	public Byte getStatus() {
		return status;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUomCode(Integer uomCode) {
		this.uomCode = uomCode;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
	
	
	

}
