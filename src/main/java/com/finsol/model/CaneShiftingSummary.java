package com.finsol.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class CaneShiftingSummary 
{


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column(nullable = true)
	@Type(type="date")
    private Date caneShiftingDate;
	@Column
	private Integer shift;
	@Column
	private Byte labourSpplr;
	@Column
	private Double noOfMen;
	@Column
	private Double manCost;
	@Column
	private Double noOfWomen;
	@Column
	private Double womanCost;
	@Column
	private Double womenTotal;
	@Column
	private Double totalLabourCost;
	@Column
	private Double costPerTon;
	@Column
	private Double totalTons;
	@Column
	private Double totalContractAmt;
	@Column
	private Double totalCaneWt;
	@Column
	private Double menTotal;
	
	//added by sahadeva 06-01-2017
	@Column
	private Integer lc;
	
	
	
	
	public Integer getLc() {
		return lc;
	}
	public void setLc(Integer lc) {
		this.lc = lc;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Date getCaneShiftingDate() {
		return caneShiftingDate;
	}
	public void setCaneShiftingDate(Date caneShiftingDate) {
		this.caneShiftingDate = caneShiftingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public Double getWomanCost() {
		return womanCost;
	}
	public void setWomanCost(Double womanCost) {
		this.womanCost = womanCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public Double getTotalCaneWt() {
		return totalCaneWt;
	}
	public void setTotalCaneWt(Double totalCaneWt) {
		this.totalCaneWt = totalCaneWt;
	}
	
	
}
