package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author sahadeva
 *
 */
@Entity
public class AuthorisationAcknowledgementDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private String authorisationSeqNo;
	@Column
	private String indentNumbers;
	@Column
	private Integer itemtype;
	@Column
	private Integer quantity;
	@Column
	private Double costofEachItem;
	@Column
	private String totalCost;
	@Column
	@Type(type="date")
	private Date dateofAutorisation;
	@Column
	private Integer indentStatus;
	@Column
	private Integer taken;
	@Column
	private Double ryotcost;
	@Column
	private Double consumaercost;
	
	
	public Integer getTaken() {
		return taken;
	}
	public Double getRyotcost() {
		return ryotcost;
	}
	public Double getConsumaercost() {
		return consumaercost;
	}
	public void setTaken(Integer taken) {
		this.taken = taken;
	}
	public void setRyotcost(Double ryotcost) {
		this.ryotcost = ryotcost;
	}
	public void setConsumaercost(Double consumaercost) {
		this.consumaercost = consumaercost;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getAuthorisationSeqNo() {
		return authorisationSeqNo;
	}
	public void setAuthorisationSeqNo(String authorisationSeqNo) {
		this.authorisationSeqNo = authorisationSeqNo;
	}
	public String getIndentNumbers() {
		return indentNumbers;
	}
	public void setIndentNumbers(String indentNumbers) {
		this.indentNumbers = indentNumbers;
	}
	public Integer getItemtype() {
		return itemtype;
	}
	public void setItemtype(Integer itemtype) {
		this.itemtype = itemtype;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getCostofEachItem() {
		return costofEachItem;
	}
	public void setCostofEachItem(Double costofEachItem) {
		this.costofEachItem = costofEachItem;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public Date getDateofAutorisation() {
		return dateofAutorisation;
	}
	public void setDateofAutorisation(Date dateofAutorisation) {
		this.dateofAutorisation = dateofAutorisation;
	}
	public Integer getIndentStatus() {
		return indentStatus;
	}
	public void setIndentStatus(Integer indentStatus) {
		this.indentStatus = indentStatus;
	}
	
	
	

}
