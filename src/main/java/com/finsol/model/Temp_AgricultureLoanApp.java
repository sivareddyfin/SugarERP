package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 */
//ryotcode,loannumber,village,ryotname,relativename,plant,ratoon,principle,aadhaarNumber
/**
 * @author sahu
 *
 */
@Entity
public class Temp_AgricultureLoanApp {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	@Column
	private Integer loannumber;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String village;
	@Column
	private String relativename;
	@Column
	private String aadhaarNumber;
	@Column
	private String branchname;
	@Column
	private Double plant;
	@Column
	private Double ratoon;
	@Column
	private Double principle;
	@Column
	private String loandate;
	@Column
	private Integer batchno;
	
	
	public String getLoandate() {
		return loandate;
	}
	public void setLoandate(String loandate) {
		this.loandate = loandate;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getRatoon() {
		return ratoon;
	}
	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}
	public Double getPrinciple() {
		return principle;
	}
	public void setPrinciple(Double principle) {
		this.principle = principle;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public Integer getBatchno() {
		return batchno;
	}
	public void setBatchno(Integer batchno) {
		this.batchno = batchno;
	}
	
	
}
