package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_DailyLogSheet
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private Integer receiptno;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String plantorratoon;
	@Column
	private String variety;
	@Column
	private String vehicletype;
	@Column
	private Double grossweight;
	@Column
	private Double bindingmtlwght;
	@Column
	private Double netwt;
	@Column
	private Integer shiftid;
	@Column
	private Integer bridgeno;
	@Column
	private String shiftdate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getReceiptno() {
		return receiptno;
	}
	public void setReceiptno(Integer receiptno) {
		this.receiptno = receiptno;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getPlantorratoon() {
		return plantorratoon;
	}
	public void setPlantorratoon(String plantorratoon) {
		this.plantorratoon = plantorratoon;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}
	public Double getGrossweight() {
		return grossweight;
	}
	public void setGrossweight(Double grossweight) {
		this.grossweight = grossweight;
	}
	public Double getBindingmtlwght() {
		return bindingmtlwght;
	}
	public void setBindingmtlwght(Double bindingmtlwght) {
		this.bindingmtlwght = bindingmtlwght;
	}
	public Double getNetwt() {
		return netwt;
	}
	public void setNetwt(Double netwt) {
		this.netwt = netwt;
	}
	public Integer getShiftid() {
		return shiftid;
	}
	public void setShiftid(Integer shiftid) {
		this.shiftid = shiftid;
	}
	public Integer getBridgeno() {
		return bridgeno;
	}
	public void setBridgeno(Integer bridgeno) {
		this.bridgeno = bridgeno;
	}
	public String getShiftdate() {
		return shiftdate;
	}
	public void setShiftdate(String shiftdate) {
		this.shiftdate = shiftdate;
	}
	
	
	

}
