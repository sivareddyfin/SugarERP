package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 */
@Entity
public class GrnDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String grnno;
	@Column
    private String finyr;
	@Column
    private String productcode;
	@Column
    private String productgrpcode;
	@Column
    private String productsubgroupcode;
	@Column
    private String uom;
	@Column(nullable = true)
    private String cause;
	@Column
    private String batchid;
	@Column
    private Double issuedqty;
	@Column
    private Double acceptedqty;
	@Column
    private Double rejectedqty;
	@Column
	private Double mrp;
	@Column
	@Type(type="date")
	private Date mfgDate;
	
	
	
	public Date getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(Date mfgDate) {
		this.mfgDate = mfgDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGrnno() {
		return grnno;
	}
	public void setGrnno(String grnno) {
		this.grnno = grnno;
	}
	public String getFinyr() {
		return finyr;
	}
	public void setFinyr(String finyr) {
		this.finyr = finyr;
	}
	public String getProductcode() {
		return productcode;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public String getProductgrpcode() {
		return productgrpcode;
	}
	public void setProductgrpcode(String productgrpcode) {
		this.productgrpcode = productgrpcode;
	}
	public String getProductsubgroupcode() {
		return productsubgroupcode;
	}
	public void setProductsubgroupcode(String productsubgroupcode) {
		this.productsubgroupcode = productsubgroupcode;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public String getBatchid() {
		return batchid;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public Double getIssuedqty() {
		return issuedqty;
	}
	public void setIssuedqty(Double issuedqty) {
		this.issuedqty = issuedqty;
	}
	public Double getAcceptedqty() {
		return acceptedqty;
	}
	public void setAcceptedqty(Double acceptedqty) {
		this.acceptedqty = acceptedqty;
	}
	public Double getRejectedqty() {
		return rejectedqty;
	}
	public void setRejectedqty(Double rejectedqty) {
		this.rejectedqty = rejectedqty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	
	
	
}
