package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_BankOrderReport
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private Double progressiveqty;
	@Column
	private String village;
	@Column
	private Integer loannumber;
	@Column
	private Double pendingamount;
	@Column
	private String referencenumber;
	@Column
	private Integer branchcode;
	@Column
	private String branchname;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public Double getProgressiveqty() {
		return progressiveqty;
	}
	public void setProgressiveqty(Double progressiveqty) {
		this.progressiveqty = progressiveqty;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	
	
	
	

}
