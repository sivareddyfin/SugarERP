package com.finsol.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class Shift
{
	private static final long serialVersionUID = 1L;	

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Id
	@Column
    private Integer shiftid;
	
	@Column
    private String shiftname;
	
	@Column
	private Time fromtime;
	
	@Column
	private Time totime;
	
	@Column
    private Time shifthrs;
	
	@Column
    private String totalhrs;
	


	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getShiftid()
	{
		return shiftid;
	}

	public void setShiftid(Integer shiftid)
	{
		this.shiftid = shiftid;
	}

	public String getShiftname() 
	{
		return shiftname;
	}

	public void setShiftname(String shiftname)
	{
		this.shiftname = shiftname;
	}

	public Time getFromtime() 
	{
		return fromtime;
	}

	public void setFromtime(Time fromtime)
	{
		this.fromtime = fromtime;
	}

	public Time getTotime()
	{
		return totime;
	}

	public void setTotime(Time totime)
	{
		this.totime = totime;
	}

	public Time getShifthrs() 
	{
		return shifthrs;
	}

	public void setShifthrs(Time shifthrs)
	{
		this.shifthrs = shifthrs;
	}

	public String getTotalhrs() 
	{
		return totalhrs;
	}

	public void setTotalhrs(String totalhrs) 
	{
		this.totalhrs = totalhrs;
	}

}
