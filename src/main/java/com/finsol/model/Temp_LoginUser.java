package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_LoginUser
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer slno;
	
	@Column
	private String employeename;
	@Column
	private String loginid;
	@Column
	private String password;
	@Column
	private String role;
	@Column
	private String changepassword;
	@Column
	private String resetpassword;
	@Column
	private Byte status;
	@Column
	private String mobileno;
	
	
	
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getEmployeename() {
		return employeename;
	}
	public void setEmployeename(String employeename) {
		this.employeename = employeename;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getChangepassword() {
		return changepassword;
	}
	public void setChangepassword(String changepassword) {
		this.changepassword = changepassword;
	}
	public String getResetpassword() {
		return resetpassword;
	}
	public void setResetpassword(String resetpassword) {
		this.resetpassword = resetpassword;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	
	
	
}
