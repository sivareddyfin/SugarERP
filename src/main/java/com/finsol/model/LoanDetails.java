package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class LoanDetails 
{
	@EmbeddedId
	private CompositePrKeyForLoans compositePrKeyForLoans;

	public CompositePrKeyForLoans getCompositePrKeyForLoans()
	{
		return compositePrKeyForLoans;
	}
	public void setCompositePrKeyForLoans(CompositePrKeyForLoans compositePrKeyForLoans)
	{
		this.compositePrKeyForLoans = compositePrKeyForLoans;
	}
	/*@Column
	private String Season;*/
	
	@Column
	private String addedloan;
	
	@Column
	private String ryotcode;
	@Column
	private String agreementnumber;
	@Column
	private String suveynumber;
	@Column
	private String plantextent;
	@Column
	private String ratoonextent;
	@Column
	private Double principle;
	@Column
	private Double interstrate;
	
	@Column
	@Type(type="date")
	private Date recommendeddate;
	
	@Column
	private String referencenumber;
	@Column
	private Double disbursedamount;
	@Column
	@Type(type="date")
	private Date disburseddate;
	@Column
	@Type(type="date")
	private Date recoverydate;
	@Column
	private Double interestamount;
	@Column
	private Double totalamount;
	@Column
	private Integer loanstatus;
	@Column
	private Double paidamount;
	@Column
	private Double pendingamount;
	 @Column
	 private Integer canemanagerid;
	 @Column
	 private Integer fieldofficerid;
	 @Column
	 private Integer fieldassistantid;
	
	@Column
	private String loginid;
	
	@Column
	private Integer transactioncode;
	
	@Column
	private Integer batchno;
	
	public Integer getTransactioncode()
	{
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) 
	{
		this.transactioncode = transactioncode;
	}
	/*public String getSeason() 
	{
		return Season;
	}
	public void setSeason(String season) 
	{
		Season = season;
	}*/
	
	public String getRyotcode()
	{
		return ryotcode;
	}
	public void setRyotcode(String ryotcode)
	{
		this.ryotcode = ryotcode;
	}
	public String getAgreementnumber() {
		return agreementnumber;
	}
	public void setAgreementnumber(String agreementnumber) {
		this.agreementnumber = agreementnumber;
	}
	public String getSuveynumber() {
		return suveynumber;
	}
	public void setSuveynumber(String suveynumber) {
		this.suveynumber = suveynumber;
	}
	public String getPlantextent() {
		return plantextent;
	}
	public void setPlantextent(String plantextent) {
		this.plantextent = plantextent;
	}
	public String getRatoonextent() {
		return ratoonextent;
	}
	public void setRatoonextent(String ratoonextent) {
		this.ratoonextent = ratoonextent;
	}
	public Double getPrinciple() {
		return principle;
	}
	public void setPrinciple(Double principle) {
		this.principle = principle;
	}
	public Double getInterstrate() {
		return interstrate;
	}
	public void setInterstrate(Double interstrate) {
		this.interstrate = interstrate;
	}
	
	
	public Date getRecommendeddate() {
		return recommendeddate;
	}
	public void setRecommendeddate(Date recommendeddate) {
		this.recommendeddate = recommendeddate;
	}
	
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}
	public Double getDisbursedamount() {
		return disbursedamount;
	}
	public void setDisbursedamount(Double disbursedamount) {
		this.disbursedamount = disbursedamount;
	}
	
	
	public Date getDisburseddate() {
		return disburseddate;
	}
	public void setDisburseddate(Date disburseddate) {
		this.disburseddate = disburseddate;
	}
	
	public Date getRecoverydate() {
		return recoverydate;
	}
	public void setRecoverydate(Date recoverydate) {
		this.recoverydate = recoverydate;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Integer getLoanstatus() {
		return loanstatus;
	}
	public void setLoanstatus(Integer loanstatus) {
		this.loanstatus = loanstatus;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getAddedloan() {
		return addedloan;
	}
	public void setAddedloan(String addedloan) {
		this.addedloan = addedloan;
	}
	public Integer getCanemanagerid() {
		return canemanagerid;
	}
	public void setCanemanagerid(Integer canemanagerid) {
		this.canemanagerid = canemanagerid;
	}
	public Integer getFieldofficerid() {
		return fieldofficerid;
	}
	public void setFieldofficerid(Integer fieldofficerid) {
		this.fieldofficerid = fieldofficerid;
	}
	public Integer getFieldassistantid() {
		return fieldassistantid;
	}
	public void setFieldassistantid(Integer fieldassistantid) {
		this.fieldassistantid = fieldassistantid;
	}
	public Integer getBatchno() {
		return batchno;
	}
	public void setBatchno(Integer batchno) {
		this.batchno = batchno;
	}
	
	

	
}
