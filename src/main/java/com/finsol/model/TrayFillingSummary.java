package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class TrayFillingSummary 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	@Type(type="date")
	private Date trayFillingDate;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	private Byte labourSpplr;
	@Column(nullable = true)
	private Byte machineOrMen;
	@Column(nullable = true)
	private Double noOfMen;
	@Column(nullable = true)
	private Double manCost;
	@Column(nullable = true)
	private Double menTotal;
	@Column(nullable = true)
	private Double noOfWomen;
	@Column(nullable = true)
	private Double womanCost;
	@Column(nullable = true)
	private Double womenTotal;
	@Column(nullable = true)
	private Double totalLabourCost;
	@Column(nullable = true)
	private Double costPerTon;
	@Column(nullable = true)
	private Double totalTons;
	@Column(nullable = true)
	private Double totalContractAmt;
	@Column(nullable = true)
	private Double noOfMachines;
	@Column(nullable = true)
	private Double totalNoOfSeedlings;
	
	@Column
	private Integer lc;
	@Column
	private Double totalCost;
		public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Integer getLc() {
		return lc;
	}
	public void setLc(Integer lc) {
		this.lc = lc;
	}
	public Integer getId() 
	{
		return id;
	}
	public String getSeason() {
		return season;
	}
	public Date getTrayFillingDate() {
		return trayFillingDate;
	}
	public Integer getShift() {
		return shift;
	}
	public Byte getLabourSpplr() {
		return labourSpplr;
	}
	public Byte getMachineOrMen() {
		return machineOrMen;
	}
	public Double getNoOfMen() {
		return noOfMen;
	}
	public Double getManCost() {
		return manCost;
	}
	public Double getMenTotal() {
		return menTotal;
	}
	public Double getNoOfWomen() {
		return noOfWomen;
	}
	public Double getWomanCost() {
		return womanCost;
	}
	public Double getWomenTotal() {
		return womenTotal;
	}
	public Double getTotalLabourCost() {
		return totalLabourCost;
	}
	public Double getCostPerTon() {
		return costPerTon;
	}
	public Double getTotalTons() {
		return totalTons;
	}
	public Double getTotalContractAmt() {
		return totalContractAmt;
	}
	public Double getNoOfMachines() {
		return noOfMachines;
	}
	public Double getTotalNoOfSeedlings() {
		return totalNoOfSeedlings;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public void setTrayFillingDate(Date trayFillingDate) {
		this.trayFillingDate = trayFillingDate;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public void setLabourSpplr(Byte labourSpplr) {
		this.labourSpplr = labourSpplr;
	}
	public void setMachineOrMen(Byte machineOrMen) {
		this.machineOrMen = machineOrMen;
	}
	public void setNoOfMen(Double noOfMen) {
		this.noOfMen = noOfMen;
	}
	public void setManCost(Double manCost) {
		this.manCost = manCost;
	}
	public void setMenTotal(Double menTotal) {
		this.menTotal = menTotal;
	}
	public void setNoOfWomen(Double noOfWomen) {
		this.noOfWomen = noOfWomen;
	}
	public void setWomanCost(Double womanCost) {
		this.womanCost = womanCost;
	}
	public void setWomenTotal(Double womenTotal) {
		this.womenTotal = womenTotal;
	}
	public void setTotalLabourCost(Double totalLabourCost) {
		this.totalLabourCost = totalLabourCost;
	}
	public void setCostPerTon(Double costPerTon) {
		this.costPerTon = costPerTon;
	}
	public void setTotalTons(Double totalTons) {
		this.totalTons = totalTons;
	}
	public void setTotalContractAmt(Double totalContractAmt) {
		this.totalContractAmt = totalContractAmt;
	}
	public void setNoOfMachines(Double noOfMachines) {
		this.noOfMachines = noOfMachines;
	}
	public void setTotalNoOfSeedlings(Double totalNoOfSeedlings) {
		this.totalNoOfSeedlings = totalNoOfSeedlings;
	}
}
