package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */

@Entity
public class CompanyDetails 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Integer companyid;	
	@Column
    private String name;
	@Column
    private String website;
	@Column
    private String address1;
	@Column
    private String address2;
	@Column
    private String city;
	@Column
	private Integer statecode;
	@Column
    private Integer districtcode;
	@Column
    private Integer countrycode;
	@Column
    private String pincode;
	@Column
    private String phoneno;
	@Column
    private String mobileno;
	@Column
    private String emailid;
	@Column
    private String faxno;
	@Column
    private String cin;
	@Column
    private String roc;
	@Column
    private String regnno;
	@Column
    private String gst;
	@Column
    private String tin;
	@Column
    private String pin;
	@Column
    private String pan;
	@Column
    private String proftaxregn;
	@Column
    private String stregnno;
	

	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getStatecode() {
		return statecode;
	}
	public void setStatecode(Integer statecode) {
		this.statecode = statecode;
	}



	public Integer getDistrictcode() {
		return districtcode;
	}
	public void setDistrictcode(Integer districtcode) {
		this.districtcode = districtcode;
	}
	public Integer getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(Integer countrycode) {
		this.countrycode = countrycode;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getFaxno() {
		return faxno;
	}
	public void setFaxno(String faxno) {
		this.faxno = faxno;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getRoc() {
		return roc;
	}
	public void setRoc(String roc) {
		this.roc = roc;
	}
	public String getRegnno() {
		return regnno;
	}
	public void setRegnno(String regnno) {
		this.regnno = regnno;
	}
 
	public String getGst() {
		return gst;
	}
	public void setGst(String gst) {
		this.gst = gst;
	}
	public String getTin() {
		return tin;
	}
	public void setTin(String tin) {
		this.tin = tin;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getProftaxregn() {
		return proftaxregn;
	}
	public void setProftaxregn(String proftaxregn) {
		this.proftaxregn = proftaxregn;
	}
	public String getStregnno() {
		return stregnno;
	}
	public void setStregnno(String stregnno) {
		this.stregnno = stregnno;
	}
	
	
	
}
