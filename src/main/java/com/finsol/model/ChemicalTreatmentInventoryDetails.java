package com.finsol.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class ChemicalTreatmentInventoryDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	
	@Column(nullable = true)
	private Integer id;
	
	@Column(nullable = true)
	@Type(type="date")
	private Date productDate;
	
	@Column(nullable = true)
	private Integer productShift;
	
	@Column(nullable = true)
	private String productSeason;
	
	@Column(nullable = true)
	private String productCode;
	
	@Column(nullable = true)
	private String productQuantity;
	
	@Column(nullable = true)
	private String productName;

	public Integer getSlNo() 
	{
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public Integer getProductShift() {
		return productShift;
	}

	public void setProductShift(Integer productShift) {
		this.productShift = productShift;
	}

	public String getProductSeason() {
		return productSeason;
	}

	public void setProductSeason(String productSeason) {
		this.productSeason = productSeason;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	
}
