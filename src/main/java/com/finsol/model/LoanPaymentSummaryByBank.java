package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class LoanPaymentSummaryByBank {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer loanbankcode;
	@Column
	private Double payableamount;
	@Column
	private Double paidamount;
	@Column
	private Double pendingamount;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getLoanbankcode() {
		return loanbankcode;
	}
	public void setLoanbankcode(Integer loanbankcode) {
		this.loanbankcode = loanbankcode;
	}
	public Double getPayableamount() {
		return payableamount;
	}
	public void setPayableamount(Double payableamount) {
		this.payableamount = payableamount;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	
	


}
