package com.finsol.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 *
 */
@Entity
public class BudCuttingDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column
	private Integer id;
	@Column
	private String season;
	@Column
	private Integer shift;
	@Column(nullable = true)
	@Type(type="date")
    private Date cuttingDetailsdate;
	@Column
	private String batchSeries;
	@Column
	private String variety;
	@Column
	private String seedSupr;
	@Column
	private String landVillageCode;
	@Column
	private String machine;
	@Column
	private Double budsPerKG;
	@Column
	private Double noOfTubs;
	@Column
	private Double tubWt;
	@Column
	private Double totalBuds;
	
	public Integer getSlNo() 
	{
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	
	public Date getCuttingDetailsdate() {
		return cuttingDetailsdate;
	}
	public void setCuttingDetailsdate(Date cuttingDetailsdate) {
		this.cuttingDetailsdate = cuttingDetailsdate;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}
	public Double getBudsPerKG() {
		return budsPerKG;
	}
	public void setBudsPerKG(Double budsPerKG) {
		this.budsPerKG = budsPerKG;
	}
	public Double getNoOfTubs() {
		return noOfTubs;
	}
	public void setNoOfTubs(Double noOfTubs) {
		this.noOfTubs = noOfTubs;
	}
	public Double getTubWt() {
		return tubWt;
	}
	public void setTubWt(Double tubWt) {
		this.tubWt = tubWt;
	}
	public Double getTotalBuds() {
		return totalBuds;
	}
	public void setTotalBuds(Double totalBuds) {
		this.totalBuds = totalBuds;
	}
	
	
	
	
}
