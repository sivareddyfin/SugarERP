package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author naidu
 */

@Entity
public class TempLoanMasterReport {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	
	@Column
	private String ryotname;
	@Column
	private String relativename;

	@Column
	private Integer loannumber;
	@Column
	private Integer branchcode;
	@Column
	private Double disbursedamount;
	@Column
	private String disburseddate;
	@Column
	private Double interestamount;
	@Column
	private Double totalamount;
	@Column
	private String village;
	@Column
	private String mandal;
	@Column
	private Integer mandalcode;
	@Column
	private String villagecode;
	@Column
	private String referencenumber;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public Integer getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(Integer branchcode) {
		this.branchcode = branchcode;
	}
	public Double getDisbursedamount() {
		return disbursedamount;
	}
	public void setDisbursedamount(Double disbursedamount) {
		this.disbursedamount = disbursedamount;
	}
	public String getDisburseddate() {
		return disburseddate;
	}
	public void setDisburseddate(String disburseddate) {
		this.disburseddate = disburseddate;
	}
	public Double getInterestamount() {
		return interestamount;
	}
	public void setInterestamount(Double interestamount) {
		this.interestamount = interestamount;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getMandal() {
		return mandal;
	}
	public void setMandal(String mandal) {
		this.mandal = mandal;
	}
	public Integer getMandalcode() {
		return mandalcode;
	}
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public String getReferencenumber() {
		return referencenumber;
	}
	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}

}
