package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author naidu
 *
 */
@Entity
public class SssBank {
	private static final long serialVersionUID = 1L;
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
	 private Integer id;
	 @Column
	 private Double BANK_CODE;
	 @Column
	 private String BANK_NAME;
	 @Column
	 private String GL_CODE;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getBANK_CODE() {
		return BANK_CODE;
	}
	public void setBANK_CODE(Double bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}
	public String getBANK_NAME() {
		return BANK_NAME;
	}
	public void setBANK_NAME(String bANK_NAME) {
		BANK_NAME = bANK_NAME;
	}
	public String getGL_CODE() {
		return GL_CODE;
	}
	public void setGL_CODE(String gL_CODE) {
		GL_CODE = gL_CODE;
	}
	

}
