package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity
public class SssVillage 
{
	private static final long serialVersionUID = 1L;

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column (nullable = false)
	 private Integer id;
	 
	 @Column
	 private String TK_CODE;
	 
	 @Column
	 private String TK_NAME;
	 
	 @Column
	 private String CI_FA;
	 
	 @Column
	 private double TK_KM;
	 
	 @Column
	 private double TK_CIRC;
	 
	 @Column
	 private double OTK_CODE;
	 
	 @Column
	 private double CC_CODE;
	 
	 @Column
	 private double M_NO;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTK_CODE() {
		return TK_CODE;
	}

	public void setTK_CODE(String tk_code) {
		TK_CODE = tk_code;
	}

	public String getTK_NAME() {
		return TK_NAME;
	}

	public void setTK_NAME(String tk_name) {
		TK_NAME = tk_name;
	}

	public String getCI_FA() {
		return CI_FA;
	}

	public void setCI_FA(String ci_fa) {
		CI_FA = ci_fa;
	}

	public double getTK_KM() {
		return TK_KM;
	}

	public void setTK_KM(double tk_km) {
		TK_KM = tk_km;
	}

	public double getTK_CIRC() {
		return TK_CIRC;
	}

	public void setTK_CIRC(double tk_circ) {
		TK_CIRC = tk_circ;
	}

	public double getOTK_CODE() {
		return OTK_CODE;
	}

	public void setOTK_CODE(double otk_code) {
		OTK_CODE = otk_code;
	}

	public double getCC_CODE() {
		return CC_CODE;
	}

	public void setCC_CODE(double cc_code) {
		CC_CODE = cc_code;
	}

	public double getM_NO() {
		return M_NO;
	}

	public void setM_NO(double m_no) {
		M_NO = m_no;
	}

}
