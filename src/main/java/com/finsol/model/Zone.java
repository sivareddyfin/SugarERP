package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Rama Krishna
 *
 */
@Entity
public class Zone {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private Integer zonecode;
	
	@Column
	private String zone;
	
	@Column
	private Integer canemanagerid;
	
	@Column
	private Integer fieldofficerid;
	
	@Column
	private Integer regioncode;	
	
	@Column
	private String description;
	
	@Column
	private Byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getZonecode() {
		return zonecode;
	}

	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public Integer getCanemanagerid() {
		return canemanagerid;
	}

	public void setCanemanagerid(Integer canemanagerid) {
		this.canemanagerid = canemanagerid;
	}

	public Integer getFieldofficerid() {
		return fieldofficerid;
	}

	public void setFieldofficerid(Integer fieldofficerid) {
		this.fieldofficerid = fieldofficerid;
	}

	public Integer getRegioncode() {
		return regioncode;
	}

	public void setRegioncode(Integer regioncode) {
		this.regioncode = regioncode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	
	

}
