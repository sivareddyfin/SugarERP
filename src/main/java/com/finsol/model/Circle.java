package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author DMurty
 */
@Entity

public class Circle 
{
	private static final long serialVersionUID = 1L;

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column (nullable = false)
	 private Integer id;
	 
	 @Column
	 private Integer circlecode;
	 
	 @Column
	 private String villagecode;
	 
	 @Column (unique = true,nullable = false)
	 private String circle;
	 
	 @Column
	 private String description;
	 
	 @Column
	 private Integer fieldassistantid;

	 @Column
	 private Integer fieldofficerid;
	 
	 @Column
	 private Integer canemanagerid;
	 
	 @Column
	 private Byte status;
	 @Column
	 private Integer zonecode;

	 
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getCirclecode() 
	{
		return circlecode;
	}

	public void setCirclecode(Integer circlecode) 
	{
		this.circlecode = circlecode;
	}



	public String getVillagecode() {
		return villagecode;
	}

	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}

	public String getCircle()
	{
		return circle;
	}

	public void setCircle(String circle)
	{
		this.circle = circle;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getFieldassistantid()
	{
		return fieldassistantid;
	}

	public void setFieldassistantid(Integer fieldassistantid) 
	{
		this.fieldassistantid = fieldassistantid;
	}

	public Integer getFieldofficerid() 
	{
		return fieldofficerid;
	}

	public void setFieldofficerid(Integer fieldofficerid)
	{
		this.fieldofficerid = fieldofficerid;
	}

	public Integer getCanemanagerid()
	{
		return canemanagerid;
	}

	public void setCanemanagerid(Integer canemanagerid) 
	{
		this.canemanagerid = canemanagerid;
	}

	public Byte getStatus()
	{
		return status;
	}

	public void setStatus(Byte status)
	{
		this.status = status;
	}

	public Integer getZonecode() {
		return zonecode;
	}

	public void setZonecode(Integer zonecode) {
		this.zonecode = zonecode;
	}
	 
	 
	 

	 
	 
	 
	 
}
