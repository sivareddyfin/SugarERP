package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Tepm_CumulativeCaneCrushing
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slno;
	
	@Column 
	private String nametype;
	@Column 
	private Double estimatedqty;
	@Column 
	private Double progressiveqty;
	
	
	public Integer getSlno() {
		return slno;
	}
	public void setSlno(Integer slno) {
		this.slno = slno;
	}
	public String getNametype() {
		return nametype;
	}
	public void setNametype(String nametype) {
		this.nametype = nametype;
	}
	public Double getEstimatedqty() {
		return estimatedqty;
	}
	public void setEstimatedqty(Double estimatedqty) {
		this.estimatedqty = estimatedqty;
	}
	public Double getProgressiveqty() {
		return progressiveqty;
	}
	public void setProgressiveqty(Double progressiveqty) {
		this.progressiveqty = progressiveqty;
	}
	
	

}
