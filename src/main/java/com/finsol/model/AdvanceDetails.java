package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class AdvanceDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String ryotcode;
	@Column
	private String season;
	@Column
	private Integer advancecode;
	@Column
	private Byte isSeedlingAdvance;
	@Column
	private Integer advancecateogrycode;
	@Column
	private Integer advancesubcategorycode;
	@Column
	private String agreements;
	@Column
	private Double extentsize;
	@Column
	private Byte isitseedling;
	@Column
	private String seedsuppliercode;
	@Column
	private Integer seedingsquantity;
	@Column
	private Double totalamount;
	@Column
	private Double advanceamount;
	@Column
	private Double ryotpayable;
	@Column
	private Integer transactioncode;
	@Column
	private String loginId;
	@Column
	@Type(type="date")
	private Date advancedate;
	@Column
	private Double plantExtent;
	@Column
	private Double ratoonExtent;
	
	public Date getAdvancedate() {
		return advancedate;
	}
	public void setAdvancedate(Date advancedate) {
		this.advancedate = advancedate;
	}
	public String getLoginId()
	{
		return loginId;
	}
	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}
	public Integer getTransactioncode()
	{
		return transactioncode;
	}
	public void setTransactioncode(Integer transactioncode) 
	{
		this.transactioncode = transactioncode;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getAdvancecode() {
		return advancecode;
	}
	public void setAdvancecode(Integer advancecode) {
		this.advancecode = advancecode;
	}
	
	public Byte getIsSeedlingAdvance() {
		return isSeedlingAdvance;
	}
	public void setIsSeedlingAdvance(Byte isSeedlingAdvance) {
		this.isSeedlingAdvance = isSeedlingAdvance;
	}
	public Integer getAdvancecateogrycode() {
		return advancecateogrycode;
	}
	public void setAdvancecateogrycode(Integer advancecateogrycode) {
		this.advancecateogrycode = advancecateogrycode;
	}
	public Integer getAdvancesubcategorycode() {
		return advancesubcategorycode;
	}
	public void setAdvancesubcategorycode(Integer advancesubcategorycode) {
		this.advancesubcategorycode = advancesubcategorycode;
	}
	public String getAgreements() {
		return agreements;
	}
	public void setAgreements(String agreements) {
		this.agreements = agreements;
	}
	public Double getExtentsize() {
		return extentsize;
	}
	public void setExtentsize(Double extentsize) {
		this.extentsize = extentsize;
	}
	public Byte getIsitseedling() {
		return isitseedling;
	}
	public void setIsitseedling(Byte isitseedling) {
		this.isitseedling = isitseedling;
	}
	
	public String getSeedsuppliercode() {
		return seedsuppliercode;
	}
	public void setSeedsuppliercode(String seedsuppliercode) {
		this.seedsuppliercode = seedsuppliercode;
	}
	public Integer getSeedingsquantity() {
		return seedingsquantity;
	}
	public void setSeedingsquantity(Integer seedingsquantity) {
		this.seedingsquantity = seedingsquantity;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getAdvanceamount() {
		return advanceamount;
	}
	public void setAdvanceamount(Double advanceamount) {
		this.advanceamount = advanceamount;
	}
	public Double getRyotpayable() {
		return ryotpayable;
	}
	public void setRyotpayable(Double ryotpayable) {
		this.ryotpayable = ryotpayable;
	}
	public Double getPlantExtent() {
		return plantExtent;
	}
	public void setPlantExtent(Double plantExtent) {
		this.plantExtent = plantExtent;
	}
	public Double getRatoonExtent() {
		return ratoonExtent;
	}
	public void setRatoonExtent(Double ratoonExtent) {
		this.ratoonExtent = ratoonExtent;
	}


}
