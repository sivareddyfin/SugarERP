package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity
public class Temp_SeedDistribution
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String village;
	@Column 
	private String variety;
	@Column 
	private String indentno;

	@Column 
	private String indentdate;
	@Column 
	private String dispatchno;
	@Column 
	private String dispatchdate;
	@Column 
	private Double issuedQty;
	
	public Double getIssuedQty() {
		return issuedQty;
	}
	public void setIssuedQty(Double issuedQty) {
		this.issuedQty = issuedQty;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getIndentno() {
		return indentno;
	}
	public void setIndentno(String indentno) {
		this.indentno = indentno;
	}
	public String getIndentdate() {
		return indentdate;
	}
	public void setIndentdate(String indentdate) {
		this.indentdate = indentdate;
	}
	public String getDispatchno() {
		return dispatchno;
	}
	public void setDispatchno(String dispatchno) {
		this.dispatchno = dispatchno;
	}
	public String getDispatchdate() {
		return dispatchdate;
	}
	public void setDispatchdate(String dispatchdate) {
		this.dispatchdate = dispatchdate;
	}

	
}
