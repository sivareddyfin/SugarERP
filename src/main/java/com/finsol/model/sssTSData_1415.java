package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */
@Entity
public class sssTSData_1415
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String c_ryotcode;
	@Column
	private Double c_supqty;
	@Column
	private Double c_rec;
	@Column
	private Integer transactiontype;
	@Column
	private String description;
	@Column
	private String cdate;
	@Column
	private String fromyr;
	@Column
	private String toyr;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getC_ryotcode() {
		return c_ryotcode;
	}
	public void setC_ryotcode(String c_ryotcode) {
		this.c_ryotcode = c_ryotcode;
	}
	public Double getC_supqty() {
		return c_supqty;
	}
	public void setC_supqty(Double c_supqty) {
		this.c_supqty = c_supqty;
	}
	public Double getC_rec() {
		return c_rec;
	}
	public void setC_rec(Double c_rec) {
		this.c_rec = c_rec;
	}
	public Integer getTransactiontype() {
		return transactiontype;
	}
	public void setTransactiontype(Integer transactiontype) {
		this.transactiontype = transactiontype;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getFromyr() {
		return fromyr;
	}
	public void setFromyr(String fromyr) {
		this.fromyr = fromyr;
	}
	public String getToyr() {
		return toyr;
	}
	public void setToyr(String toyr) {
		this.toyr = toyr;
	}
	

}
