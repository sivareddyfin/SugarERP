package com.finsol.model;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Umanath Ch
 */
@Entity
public class ChemicalTreatmentDetails 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer slNo;
	@Column(nullable = true)
	private Integer id;
	@Column(nullable = true)
	private String season;
	@Column(nullable = true)
	private Integer shift;
	@Column(nullable = true)
	@Type(type="date")
	private Date treatmentDate;
	@Column(nullable = true)
	private String batchSeries;
	@Column(nullable = true)
	private String variety;
	@Column(nullable = true)
	private String seedSupr;
	@Column(nullable = true)
	private String landVillageCode;
	@Column(nullable = true)
	private Double noOfTubs;
	@Column(nullable = true)
	private Double tubWt;
	
	public Integer getSlNo()
	{
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public Integer getShift() {
		return shift;
	}
	public void setShift(Integer shift) {
		this.shift = shift;
	}
	public Date getTreatmentDate() {
		return treatmentDate;
	}
	public void setTreatmentDate(Date treatmentDate) {
		this.treatmentDate = treatmentDate;
	}
	public String getBatchSeries() {
		return batchSeries;
	}
	public void setBatchSeries(String batchSeries) {
		this.batchSeries = batchSeries;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getSeedSupr() {
		return seedSupr;
	}
	public void setSeedSupr(String seedSupr) {
		this.seedSupr = seedSupr;
	}
	public String getLandVillageCode() {
		return landVillageCode;
	}
	public void setLandVillageCode(String landVillageCode) {
		this.landVillageCode = landVillageCode;
	}
	public Double getNoOfTubs() {
		return noOfTubs;
	}
	public void setNoOfTubs(Double noOfTubs) {
		this.noOfTubs = noOfTubs;
	}
	public Double getTubWt() {
		return tubWt;
	}
	public void setTubWt(Double tubWt) {
		this.tubWt = tubWt;
	}
	
	
	
	
}
