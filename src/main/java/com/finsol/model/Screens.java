package com.finsol.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 */
@Entity

public class Screens 
{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Integer screencode;
	
	@Column
    private String screenname;
	
	@Column
    private String description;
	
	@Column
    private Boolean canadd;
	
	@Column
    private Boolean canmodify;
	
	@Column
    private Boolean canread;
	
	@Column
    private Boolean candelete;
	
	@Column
    private Byte isaudittrailrequired;
	
	@Column
    private Byte status;

	public Integer getScreencode() {
		return screencode;
	}

	public void setScreencode(Integer screencode) {
		this.screencode = screencode;
	}

	public String getScreenname() {
		return screenname;
	}

	public void setScreenname(String screenname) {
		this.screenname = screenname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getCanadd() {
		return canadd;
	}

	public void setCanadd(Boolean canadd) {
		this.canadd = canadd;
	}

	public Boolean getCanmodify() {
		return canmodify;
	}

	public void setCanmodify(Boolean canmodify) {
		this.canmodify = canmodify;
	}

	public Boolean getCanread() {
		return canread;
	}

	public void setCanread(Boolean canread) {
		this.canread = canread;
	}

	public Boolean getCandelete() {
		return candelete;
	}

	public void setCandelete(Boolean candelete) {
		this.candelete = candelete;
	}

	public Byte getIsaudittrailrequired() {
		return isaudittrailrequired;
	}

	public void setIsaudittrailrequired(Byte isaudittrailrequired) {
		this.isaudittrailrequired = isaudittrailrequired;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}
