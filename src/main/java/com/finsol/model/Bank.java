package com.finsol.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;



/**
 * @author Rama Krishna
 *
 */
@Entity
public class Bank {

	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column
	private Integer id;
	
	@Column(unique = true,nullable = false)
	private Integer bankcode;
	
	@Column
	private String bankname;
	
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBankcode() {
		return bankcode;
	}

	public void setBankcode(Integer bankcode) {
		this.bankcode = bankcode;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	
	
	
}
