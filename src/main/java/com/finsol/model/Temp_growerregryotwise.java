package com.finsol.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;
/**
 * @author sahadeva    
 */

@Entity
public class Temp_growerregryotwise
{
	//ryotcode,ryotname,relativename,plant,ratoon,surveynumber,village,nooftonsperacre,villagecode,mandal,mandalcode


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String relativename;
	@Column 
	private String mandal;
	@Column 
	private Double plant;
	@Column 
	private Double ratoon;
	@Column 
	private String surveynumber;
	@Column 
	private String village;
	@Column 
	private String villagecode;
	@Column 
	private Integer mandalcode;
	@Column 
	private Double agreedqty;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getRelativename() {
		return relativename;
	}
	public void setRelativename(String relativename) {
		this.relativename = relativename;
	}
	public String getMandal() {
		return mandal;
	}
	public void setMandal(String mandal) {
		this.mandal = mandal;
	}
	public Double getPlant() {
		return plant;
	}
	public void setPlant(Double plant) {
		this.plant = plant;
	}
	public Double getRatoon() {
		return ratoon;
	}
	public void setRatoon(Double ratoon) {
		this.ratoon = ratoon;
	}
	public String getSurveynumber() {
		return surveynumber;
	}
	public void setSurveynumber(String surveynumber) {
		this.surveynumber = surveynumber;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	
	
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public Integer getMandalcode() {
		return mandalcode;
	}
	public void setMandalcode(Integer mandalcode) {
		this.mandalcode = mandalcode;
	}
	public Double getAgreedqty() {
		return agreedqty;
	}
	public void setAgreedqty(Double agreedqty) {
		this.agreedqty = agreedqty;
	}
	
	
	
	
	
}
