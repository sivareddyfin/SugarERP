package com.finsol.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author sahadeva
 *
 */
@Entity

public class Temp_SeedDevlopment
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column 
	private String supplier;
	@Column 
	private String suppliername;
	@Column 
	private String variety;
	@Column 
	private Double tons;
	@Column 
	private String batch;
	@Column 
	private Double noofseedlings;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public Double getTons() {
		return tons;
	}
	public void setTons(Double tons) {
		this.tons = tons;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getNoofseedlings() {
		return noofseedlings;
	}
	public void setNoofseedlings(Double noofseedlings) {
		this.noofseedlings = noofseedlings;
	}
	
	

}
