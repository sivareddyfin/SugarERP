package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author sahadeva
 */
@Entity
public class sssSBandLoans_1415
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ryotcode;
	@Column
	private String ryotname;
	@Column
	private String description;
	@Column
	private Double debit;
	@Column
	private Integer type;
	@Column
	private String fromyr;
	@Column
	private String toyr;
	@Column
	private String cdate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getFromyr() {
		return fromyr;
	}
	public void setFromyr(String fromyr) {
		this.fromyr = fromyr;
	}
	public String getToyr() {
		return toyr;
	}
	public void setToyr(String toyr) {
		this.toyr = toyr;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}	
	
	
}
