package com.finsol.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @author Rama Krishna
 *
 */
@Entity
public class SeedSuppliersSummary
{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer id;
	private String season;
	@Column
	private String seedsuppliercode;
	@Column
	private Double totalextentsize;
	@Column
	private Double advancepayable;
	@Column
	private Double paidamount;
	@Column
	private Double pendingamount;
	@Column
	private Byte advancestatus;
	@Column
	private String loginid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	public String getSeedsuppliercode() {
		return seedsuppliercode;
	}
	public void setSeedsuppliercode(String seedsuppliercode) {
		this.seedsuppliercode = seedsuppliercode;
	}
	public Double getTotalextentsize() {
		return totalextentsize;
	}
	public void setTotalextentsize(Double totalextentsize) {
		this.totalextentsize = totalextentsize;
	}
	public Double getAdvancepayable() {
		return advancepayable;
	}
	public void setAdvancepayable(Double advancepayable) {
		this.advancepayable = advancepayable;
	}
	public Double getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(Double paidamount) {
		this.paidamount = paidamount;
	}
	public Double getPendingamount() {
		return pendingamount;
	}
	public void setPendingamount(Double pendingamount) {
		this.pendingamount = pendingamount;
	}
	public Byte getAdvancestatus() {
		return advancestatus;
	}
	public void setAdvancestatus(Byte advancestatus) {
		this.advancestatus = advancestatus;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
}
