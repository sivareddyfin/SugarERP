package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * @author rk
 */
@Entity
public class Agreementmaster1516_temp {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
    private String M_RYOT;
	@Column
    private String M_TITLE;
	@Column
    private String M_NAME;
	@Column
    private String M_FNAME;
	@Column
    private Double M_EXT_P;
	@Column
    private Double M_EXT_R;
	@Column
    private Double M_AGRQTY;
	@Column
    private Double RUN1_QTY;
	@Column
    private Double M_PRGQTY;
	@Column
    private Double RUN2_QTY;
	@Column
    private Double M_PRGVAL;
	@Column
    private Double M_BKCODE;
	@Column
    private String M_ACNO;
	@Column
    private String LVCOD;
	@Column
    private Double M_ZONE;
	@Column
    private Double M_CICODE;
	@Column
    private Double M_MANDAL;
	@Column
    private Double M_REGN;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getM_RYOT() {
		return M_RYOT;
	}
	public void setM_RYOT(String m_RYOT) {
		M_RYOT = m_RYOT;
	}
	public String getM_TITLE() {
		return M_TITLE;
	}
	public void setM_TITLE(String m_TITLE) {
		M_TITLE = m_TITLE;
	}
	public String getM_NAME() {
		return M_NAME;
	}
	public void setM_NAME(String m_NAME) {
		M_NAME = m_NAME;
	}
	public String getM_FNAME() {
		return M_FNAME;
	}
	public void setM_FNAME(String m_FNAME) {
		M_FNAME = m_FNAME;
	}
	public Double getM_EXT_P() {
		return M_EXT_P;
	}
	public void setM_EXT_P(Double m_EXT_P) {
		M_EXT_P = m_EXT_P;
	}
	public Double getM_EXT_R() {
		return M_EXT_R;
	}
	public void setM_EXT_R(Double m_EXT_R) {
		M_EXT_R = m_EXT_R;
	}
	public Double getM_AGRQTY() {
		return M_AGRQTY;
	}
	public void setM_AGRQTY(Double m_AGRQTY) {
		M_AGRQTY = m_AGRQTY;
	}
	public Double getRUN1_QTY() {
		return RUN1_QTY;
	}
	public void setRUN1_QTY(Double rUN1_QTY) {
		RUN1_QTY = rUN1_QTY;
	}
	public Double getM_PRGQTY() {
		return M_PRGQTY;
	}
	public void setM_PRGQTY(Double m_PRGQTY) {
		M_PRGQTY = m_PRGQTY;
	}
	public Double getRUN2_QTY() {
		return RUN2_QTY;
	}
	public void setRUN2_QTY(Double rUN2_QTY) {
		RUN2_QTY = rUN2_QTY;
	}
	public Double getM_PRGVAL() {
		return M_PRGVAL;
	}
	public void setM_PRGVAL(Double m_PRGVAL) {
		M_PRGVAL = m_PRGVAL;
	}
	public Double getM_BKCODE() {
		return M_BKCODE;
	}
	public void setM_BKCODE(Double m_BKCODE) {
		M_BKCODE = m_BKCODE;
	}
	public String getM_ACNO() {
		return M_ACNO;
	}
	public void setM_ACNO(String m_ACNO) {
		M_ACNO = m_ACNO;
	}
	public String getLVCOD() {
		return LVCOD;
	}
	public void setLVCOD(String lVCOD) {
		LVCOD = lVCOD;
	}
	public Double getM_ZONE() {
		return M_ZONE;
	}
	public void setM_ZONE(Double m_ZONE) {
		M_ZONE = m_ZONE;
	}
	public Double getM_CICODE() {
		return M_CICODE;
	}
	public void setM_CICODE(Double m_CICODE) {
		M_CICODE = m_CICODE;
	}
	public Double getM_MANDAL() {
		return M_MANDAL;
	}
	public void setM_MANDAL(Double m_MANDAL) {
		M_MANDAL = m_MANDAL;
	}
	public Double getM_REGN() {
		return M_REGN;
	}
	public void setM_REGN(Double m_REGN) {
		M_REGN = m_REGN;
	}

	
	

}
