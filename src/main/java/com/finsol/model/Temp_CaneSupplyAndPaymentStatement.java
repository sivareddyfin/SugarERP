package com.finsol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Temp_CaneSupplyAndPaymentStatement 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column 
	private String ryotcode;
	@Column 
	private String ryotname;
	@Column 
	private String village;
	@Column 
	private String zone;
	@Column 
	private String circle;
	@Column 
	private Double canesupply;
	@Column 
	private Double canevalue;
	@Column 
	private Double ul;
	@Column 
	private Double cdc;
	@Column 
	private Double transport;
	@Column 
	private Double harvesting;
	@Column 
	private Double odd;
	@Column 
	private Double lprinciple;
	@Column 
	private Double linterest;
	@Column 
	private Double advance;
	@Column 
	private Double ltransfer;
	@Column 
	private Double lpaid;
	@Column 
	private Double sbtransfer;
	@Column 
	private Double sbpaid;
	@Column 
	private Double balancedue;
	@Column 
	private String payamantresourcebank;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRyotcode() {
		return ryotcode;
	}
	public void setRyotcode(String ryotcode) {
		this.ryotcode = ryotcode;
	}
	public String getRyotname() {
		return ryotname;
	}
	public void setRyotname(String ryotname) {
		this.ryotname = ryotname;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public Double getCanesupply() {
		return canesupply;
	}
	public void setCanesupply(Double canesupply) {
		this.canesupply = canesupply;
	}
	public Double getCanevalue() {
		return canevalue;
	}
	public void setCanevalue(Double canevalue) {
		this.canevalue = canevalue;
	}
	public Double getUl() {
		return ul;
	}
	public void setUl(Double ul) {
		this.ul = ul;
	}
	public Double getCdc() {
		return cdc;
	}
	public void setCdc(Double cdc) {
		this.cdc = cdc;
	}
	public Double getTransport() {
		return transport;
	}
	public void setTransport(Double transport) {
		this.transport = transport;
	}
	public Double getHarvesting() {
		return harvesting;
	}
	public void setHarvesting(Double harvesting) {
		this.harvesting = harvesting;
	}
	public Double getOdd() {
		return odd;
	}
	public void setOdd(Double odd) {
		this.odd = odd;
	}
	public Double getLprinciple() {
		return lprinciple;
	}
	public void setLprinciple(Double lprinciple) {
		this.lprinciple = lprinciple;
	}
	public Double getLinterest() {
		return linterest;
	}
	public void setLinterest(Double linterest) {
		this.linterest = linterest;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getLtransfer() {
		return ltransfer;
	}
	public void setLtransfer(Double ltransfer) {
		this.ltransfer = ltransfer;
	}
	public Double getLpaid() {
		return lpaid;
	}
	public void setLpaid(Double lpaid) {
		this.lpaid = lpaid;
	}
	public Double getSbtransfer() {
		return sbtransfer;
	}
	public void setSbtransfer(Double sbtransfer) {
		this.sbtransfer = sbtransfer;
	}
	public Double getSbpaid() {
		return sbpaid;
	}
	public void setSbpaid(Double sbpaid) {
		this.sbpaid = sbpaid;
	}
	public Double getBalancedue() {
		return balancedue;
	}
	public void setBalancedue(Double balancedue) {
		this.balancedue = balancedue;
	}
	public String getPayamantresourcebank() {
		return payamantresourcebank;
	}
	public void setPayamantresourcebank(String payamantresourcebank) {
		this.payamantresourcebank = payamantresourcebank;
	}
	
	
		


}
