package com.finsol.utils;

import java.util.Properties;

import com.finsol.model.Users;


/**
 * @author kishore.k
 *
 */
public class UserData
{
    private static ThreadLocal<UserData> userLocal = new ThreadLocal<UserData>();

    private Users user;
    private Properties userProperties;
    
    public static UserData getCurrent()
    {
        UserData data = userLocal.get();
        if (data == null)
        {
            data = new UserData();
            userLocal.set(data);
        }
        return data;
    }

    public static void setCurrent(UserData userData)
    {
        userLocal.set(userData);
    }

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Properties getUserProperties() {
		return userProperties;
	}

	public void setUserProperties(Properties userProperties) {
		this.userProperties = userProperties;
	}
   
    
}

