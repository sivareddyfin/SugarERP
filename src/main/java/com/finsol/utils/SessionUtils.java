/**
 * 
 */
package com.finsol.utils;


import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.util.WebUtils;

import com.finsol.model.Users;





/**
 * @author Rama Krishna
 * 
 */
public class SessionUtils
{
    private static Log s_log = LogFactory.getLog(SessionUtils.class);

    public static void setSessionData(Users user, UserData userData, HttpServletRequest request)
    {
        Properties userProperties = new Properties();
        userProperties.setProperty("IPADDRESS", request.getRemoteAddr());
        
        userData.setUser(user);
        //userData.setLanguage(language);
        //userData.setUserProperties(userProperties);
        
        UserData.setCurrent(userData);
        WebUtils.setSessionAttribute(request, "USERSESSION", userData);
 //       s_log.info("Added user [" + user.Name() + "] to session and URI is : " + request.getRequestURI());
 //       s_log.info("URI is : " + request.getRequestURI());
    }
   
    

}
