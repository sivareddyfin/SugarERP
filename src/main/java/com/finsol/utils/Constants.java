package com.finsol.utils;

import java.io.File;
import java.util.HashMap;


/**
 * This class is used to declare constants
 * 
 * @author Rama Krishna
 * 
 */
public class Constants {
	
    private Constants()
    {

    }
    public static final String USER="USER";	   

    public static final HashMap<String,String> DropdownName =new HashMap<String,String>();
        
    static
	{
    	DropdownName.put("Zone", "zone");
    	DropdownName.put("FieldAssistant", "fieldassistant");
    	DropdownName.put("FieldOfficer", "fieldOfficer");
    	DropdownName.put("Village", "village");
    	DropdownName.put("Mandal", "mandal");
    	DropdownName.put("DepartmentMaster", "department");  
    	DropdownName.put("Region", "region");
    	DropdownName.put("Circle", "circle");
    	DropdownName.put("CaneManager", "canemanagername");
    	DropdownName.put("Bank", "bankname");
    	DropdownName.put("State", "state");
    	DropdownName.put("District", "district");
    	DropdownName.put("Employees", "employeename");
    	DropdownName.put("Variety", "variety");
    	DropdownName.put("AdvanceCategory", "name");
    	DropdownName.put("AdvanceSubCategory", "name");
    	DropdownName.put("Branch", "branchname");
    	DropdownName.put("Ryot", "ryotname");
    	DropdownName.put("AccountGroup", "accountgroup");
    	DropdownName.put("FromYear", "fromyear");
    	DropdownName.put("StandardDeductions", "name");
    	DropdownName.put("HarvestingContractors", "harvester");
    	DropdownName.put("TransportingContractors", "transporter");
    	DropdownName.put("Season", "season");
    	DropdownName.put("CompanyAdvance", "advance");
    	DropdownName.put("ExtentDetails", "ryotname");
    	DropdownName.put("Designation", "designation");
    	DropdownName.put("Country", "country");
    	DropdownName.put("LandType", "landtype");
    	DropdownName.put("CropTypes", "croptype");
    	DropdownName.put("FamilyGroup", "groupname");
    	DropdownName.put("AgreementSummary", "agreementnumber");
    	DropdownName.put("LoanDetails", "loannumber");
    	DropdownName.put("Roles", "role");
    	DropdownName.put("Screens", "screenname");
    	DropdownName.put("SeedlingTray", "tray");
    	DropdownName.put("Period", "period");
    	DropdownName.put("Month", "month");
    	DropdownName.put("WeighBridge", "bridgename");
    	DropdownName.put("ProgramSummary", "programnumber");
    	DropdownName.put("UnloadingContractor", "name");
    	DropdownName.put("CaneAcctRules", "calcruledate");
    	DropdownName.put("HarvestingContractors", "harvester");
    	DropdownName.put("TransportingContractors", "transporter");
	}

    
	public static String getFileExtension(String fileName) 
	{
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
	}
	
	public static class GenericDateFormat
    {
        public static final String DATE_FORMAT = "dd-MM-yyyy";
    }

}
