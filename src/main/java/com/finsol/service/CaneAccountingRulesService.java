package com.finsol.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CaneAccountingRulesDao;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctRules;



/**
 * @author DMurty
 */
@Service("CaneAccountingRulesService")
public class CaneAccountingRulesService
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private CaneAccountingRulesDao caneAccountingRulesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCaneAccountingRules(CaneAccountingRules caneAccountingRules)
	{
		caneAccountingRulesDao.addCaneAccountingRules(caneAccountingRules);
	}
	
	/*public CaneAccountingRules getCaneAccRulesDtls(String season,Date effDate)
	{
		return caneAccountingRulesDao.getCaneAccRulesDtls(season,effDate);
	}*/

	public List<CaneAccountingRules> getCaneAccRulesForCalc(String season,Date date1,Date date2)
	{
		return caneAccountingRulesDao.getCaneAccRulesForCalc(season,date1,date2);
	}

	
	public CaneAcctRules getCaneAccRulesDtls(String season,Date effDate)
	{
		return caneAccountingRulesDao.getCaneAccRulesDtls(season,effDate);
	}
	public List getCaneAccAmounts(Integer calcid)
	{
		return caneAccountingRulesDao.getCaneAccAmounts(calcid);
	}
	
	
	
	
	
}
