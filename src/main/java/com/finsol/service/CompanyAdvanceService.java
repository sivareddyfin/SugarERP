package com.finsol.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CompanyAdvanceDao;
import com.finsol.model.AccountMaster;
import com.finsol.model.AccountType;
import com.finsol.model.AdvanceInterestRates;
import com.finsol.model.AdvanceOrder;
import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.SeedAdvanceCost;




/**
 * @author DMurty
 *
 */
@Service("CompanyAdvanceService")
public class CompanyAdvanceService
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private CompanyAdvanceDao companyAdvanceDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCompanyAdvance(CompanyAdvance companyAdvance) 
	{
		companyAdvanceDao.addCompanyAdvance(companyAdvance);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addIntrestRate(AdvanceInterestRates advanceInterestRates)
	{
		companyAdvanceDao.addAdvIntrestRate(advanceInterestRates);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountMaster(AccountMaster accountMaster)
	{
		companyAdvanceDao.addAccountMaster(accountMaster);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvanceOrder(AdvanceOrder advanceOrder)
	{
		companyAdvanceDao.addAdvanceOrder(advanceOrder);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountType(AccountType accountType)
	{
		companyAdvanceDao.addAccountType(accountType);
	}
	
	public void deleteIntrestRate(CompanyAdvance companyAdvance) 
	{
		companyAdvanceDao.deleteIntrestRate(companyAdvance);
	}
	public void deleteSeedlingCost(CompanyAdvance companyAdvance) 
	{
		companyAdvanceDao.deleteSeedlingCost(companyAdvance);
	}
	
	public CompanyAdvance getCompanyAdvance(int id)
	{
		return companyAdvanceDao.getCompanyAdvance(id);
	}
	
	public List<AdvanceInterestRates> getIntrestRatesByAdvanceCode(Integer advancecode)
    {
        return companyAdvanceDao.getIntrestRatesByAdvanceCode(advancecode);
    }
	//Added by DMurty on 05-07-2016
	public List<SeedAdvanceCost> getSeedlingCost(Integer advancecode)
    {
        return companyAdvanceDao.getSeedlingCost(advancecode);
    }

	public List getCompanyAdvancesForOrder()
	{
		return companyAdvanceDao.getCompanyAdvancesForOrder();
	}
	
	 public void updateOrderForCompanyAdvance(int advanceCode,int orderId,int id)
     {
		 companyAdvanceDao.updateOrderForCompanyAdvance(advanceCode,orderId,id);
     }
	 
	 public Integer GetAccGrpCode(String AccType)
	    {
			Integer AccGrpCode=(Integer) companyAdvanceDao.GetAccGrpCode("select accountgroupcode from AccountGroup where accountgroup='"+AccType+"'");
	        return AccGrpCode;
	    }
	
	
	public Integer GetAccSubGrpCode(int AccGrpCode)
    {
		Integer AccSubGrpCode=(Integer) companyAdvanceDao.GetAccGrpCode("select accountsubgroupcode from AccountSubGroup where accountgroupcode="+AccGrpCode);
        return AccSubGrpCode;
    }
	
	public Integer GetAccSubGrpCodeNew(String AccSubGrpName)
    {
		Integer AccSubGrpCode=(Integer) companyAdvanceDao.GetAccGrpCode("select accountsubgroupcode from AccountSubGroup where accountsubgroup='"+AccSubGrpName+"'");
        return AccSubGrpCode;
    }
	
	
	
		
}
