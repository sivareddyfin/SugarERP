package com.finsol.service;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.RolesDao;
import com.finsol.model.Employees;
import com.finsol.model.RoleScreens;
import com.finsol.model.Roles;

/**
 * @author SahaDeva
 */
@Service("RolesService")

public class RolesService 
{
	private static final Logger logger = Logger.getLogger(RolesService.class);
	@Autowired
	private RolesDao rolesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRoles(Roles roles)
	{
		rolesDao.addRoles(roles);
	}
	public List<Roles> listRole() 
	{
		return rolesDao.listRole();
	}
	
	public Roles getRoles(int id)
	{
		return rolesDao.getRoles(id);
	}
	
	public List<RoleScreens> getRoleScreensByRoleId(Integer roleId)
	{
		return rolesDao.getRoleScreensByRoleId(roleId);
	}
}
