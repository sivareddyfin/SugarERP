package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CaneManagerDao;
import com.finsol.model.CaneManager;
import com.finsol.model.DepartmentMaster;


/**
 * @author Rama Krishna
 *
 */
@Service("CaneManagerService")
public class CaneManagerService {
	
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private CaneManagerDao caneManagerDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCaneManager(CaneManager caneManager) {
		caneManagerDao.addCaneManager(caneManager);
	}

	public Integer getMaxID()
    {
		Integer maxVal=(Integer) caneManagerDao.getMaxValue("select max(id) from CaneManager");
		logger.info("====CaneManagerService getMaxID() maxVal========"+maxVal);
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	
	public List<CaneManager> listCaneManagers() {
		return caneManagerDao.listCaneManagers();
	}
	
	public List getCaneManageNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, storename from DepartmentMaster";
		return caneManagerDao.getCaneManageNames(query);
	}
	
	
	
	
	
}
