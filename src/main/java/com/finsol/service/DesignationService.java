package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.DesignationDao;
import com.finsol.model.CaneManager;
import com.finsol.model.Designation;

/**
 * @author DMurty
 */
@Service("DesignationService")
public class DesignationService
{
	private static final Logger logger = Logger.getLogger(DesignationService.class);
	
	
	@Autowired
	private DesignationDao designationDao;
		
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addDesignation(Designation designation)
	{
		designationDao.addDesignations(designation);
	}
	
	public List<Designation> listDesignations()
	{
		return designationDao.listDesignations();
	}
	
	
	

	
	
}
