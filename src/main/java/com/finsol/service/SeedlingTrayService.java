package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.SeedlingTrayDao;
import com.finsol.model.SeedlingTray;
import com.finsol.model.SeedlingTrayChargeByDistance;


@Service("SeedlingTrayService")
public class SeedlingTrayService {

	
private static final Logger logger = Logger.getLogger(SeedlingTrayService.class);
	
	
	@Autowired
	private SeedlingTrayDao seedlingTrayDao;
		
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeedlingTray(SeedlingTray seedlingTray)
	{
		seedlingTrayDao.addSeedlingTray(seedlingTray);
	}
	
	public List<SeedlingTray> listSeedlingTray()
	{
		return seedlingTrayDao.listSeedlingTray();
	}
	
	public SeedlingTray getSeedlingTray(int id) {
		return seedlingTrayDao.getSeedlingTray(id);
	}
	
	public List<SeedlingTrayChargeByDistance> getSeedlingTrayChargeByDistance(Integer trayCode)
    {
        return seedlingTrayDao.getSeedlingTrayChargeByDistance(trayCode);
    }
	
	public void deleteAllSeedlingTrayChargesByDistance(Integer trayCode) {
		seedlingTrayDao.truncateSeedlingTrayChargesByDistance(trayCode);
	}
}
