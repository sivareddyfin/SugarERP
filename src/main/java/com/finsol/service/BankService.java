package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.BankDao;
import com.finsol.dao.CaneManagerDao;
import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.CaneManager;
import com.finsol.model.Employee;


/**
 * @author Rama Krishna
 *
 */
@Service("BankService")
public class BankService {
	
	
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private BankDao bankDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addBank(Bank bank) {
		bankDao.addBank(bank);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addBranch(Branch branch) {
		bankDao.addBranch(branch);
	}
	
/*	public void deleteBranch(Branch branch) {
		bankDao.deleteBranch(branch);
	}*/
	
	
	public List<Bank> listBanks() {
		return bankDao.listBanks();
	}
	public List<Branch> listBranches() {
		return bankDao.listBranches();
	}
	
	
	public List getBank(int id)
	{
		return bankDao.getBank(id);
	}
	
	public List<Branch> getBranchByBankCode(Integer bankcode)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return bankDao.getBranchByBankCode(bankcode);
    }

	public void deleteBranch(Bank bank) {
		bankDao.deleteBranch(bank);
	}
	public Integer getBranchGlcode(Integer branchcode)
	{
		return bankDao.getBranchGlcode(branchcode);
	}
	
}
