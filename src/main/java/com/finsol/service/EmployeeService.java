package com.finsol.service;

import java.util.List;

import com.finsol.model.Employee;

/**
 * @author Rama Krishna
 *
 */
public interface EmployeeService {
	
	public void addEmployee(Employee employee);

	public List<Employee> listEmployeess();
	
	public Employee getEmployee(int empid);
	
	public void deleteEmployee(Employee employee);
}
