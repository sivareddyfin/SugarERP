package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.VarietyDao;
import com.finsol.model.Variety;
/**
 * @author Rama Krishna
 *
 */
@Service("VarietyService")
public class VarietyService {
	private static final Logger logger = Logger.getLogger(VarietyService.class);

	@Autowired
	private VarietyDao varietyDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addVariety(Variety variety) {
		varietyDao.addVariety(variety);
	}
	
	public Integer getMaxID()
    {
		Integer maxVal=(Integer) varietyDao.getMaxValue("select max(id) from Variety");
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	
	
	public List<Variety> listVarieties() {
		return varietyDao.listVarieties();
	}
	
}
