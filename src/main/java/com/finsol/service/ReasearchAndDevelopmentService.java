package com.finsol.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.HibernateDao;
import com.finsol.dao.ReasearchAndDevelopmentDao;
import com.finsol.model.AgreementDetails;
import com.finsol.model.BudRemovedCane;
import com.finsol.model.CaneShiftingSummary;
import com.finsol.model.DetrashSummary;
import com.finsol.model.DispatchAckSummary;
import com.finsol.model.DispatchDetails;
import com.finsol.model.KindType;
import com.finsol.model.Machine;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedlingEstimate;
import com.finsol.model.SeedlingTray;
import com.finsol.model.SeedlingTrayChargeByDistance;


/**
 * @author Umanath Ch
 *
 */
@Service("ReasearchAndDevelopmentService")


public class ReasearchAndDevelopmentService
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private ReasearchAndDevelopmentDao reasearchAndDevelopmentDao;
	@Autowired
	private HibernateDao hibernateDao;
	public List<Machine> listMachines()
	{
		return reasearchAndDevelopmentDao.listMachines();
	}
	
	public List<KindType> listKindTypes()
	{
		return reasearchAndDevelopmentDao.listKindTypes();
	}
	public List<BudRemovedCane> listBudRemovedCanes(String buddate)
	{
		return reasearchAndDevelopmentDao.listBudRemovedCanes(buddate);
	}
	
	public List getEstimateDates()
    {        
        return reasearchAndDevelopmentDao.getEstimateDates();
    }
	public List getOtherRyots()
    {        
        return reasearchAndDevelopmentDao.getOtherRyots();
    }
	
	public List getseedsourceChangeSummary(String batchSeries)
    {        
        return reasearchAndDevelopmentDao.getseedsourceChangeSummary(batchSeries);
    }
	public List getseedSupplyRyots()
    {        
        return reasearchAndDevelopmentDao.getseedSupplyRyots();
    }
	public List getseedsourceChangeSummaryreport(String fromdate,String todate,String seedsupplier)
    {        
        return reasearchAndDevelopmentDao.getseedsourceChangeSummaryreport(fromdate,todate, seedsupplier);
    }
	public List getcircledetailsbyfieldman(Integer fieldman)
    {        
        return reasearchAndDevelopmentDao.getcircledetailsbyfieldman(fieldman);
    }
	
	public List getIndentSummaryreport(Integer Zone,Integer variety,Integer kind)
    {        
		logger.info("========getIndentSummaryreport()==========");
		String strQuery=null;
		List entityList = new ArrayList();
		
		try 
		{
			//if(Circle==0)
			//{
					if(Zone==0)
					{
							if(variety==0)
							{
								strQuery=" select A.indentNo,B.SummaryDate,A.batchDate,B.zone,A.variety,A.pendingQuantity,B.fieldMan from KindIndentDetails A,KindIndentSummary B where A.kind="+kind+" and A.indentNo=B.indentNo and B.indentStatus!=3";
							}
							else
							{
								strQuery=" select A.indentNo,B.SummaryDate,A.batchDate,B.zone,A.variety,A.pendingQuantity,B.fieldMan from KindIndentDetails A,KindIndentSummary B where A.kind="+kind+"  and A.variety="+variety+" and A.indentNo=B.indentNo and B.indentStatus!=3";
							}
					}
					else
					{
						if(variety==0)
						{
							strQuery=" select A.indentNo,B.SummaryDate,A.batchDate,B.zone,A.variety,A.pendingQuantity,B.fieldMan from KindIndentDetails A,KindIndentSummary B where A.kind="+kind+" and A.indentNo=B.indentNo and B.zone="+Zone+" and B.indentStatus!=3";
						}
						else
						{
							strQuery=" select A.indentNo,B.SummaryDate,A.batchDate,B.zone,A.variety,A.pendingQuantity,B.fieldMan from KindIndentDetails A,KindIndentSummary B where A.kind="+kind+"  and A.variety="+variety+" and A.indentNo=B.indentNo  and B.zone="+Zone+" and B.indentStatus!=3";
						}
					}
			
			//}
			return hibernateDao.findBySqlCriteria(strQuery);
		}
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
		    return null;

		}
    }

	public List getcircledetailsfordispatch(Integer circle)
    {        
        return reasearchAndDevelopmentDao.getcircledetailsfordispatch(circle);
    }
	public List getryotdetailsforpendingseedlings(String ryotcode,String season)
	{
		String sql="select max(dispatchSeq) as maxno from DispatchSummary where ryotCode='" +ryotcode+"' and season='"+season+"'";
		logger.info("getryotdetailsforpendingseedlings() ========sql=========="+sql);
		return reasearchAndDevelopmentDao.getMaxryotdetailsforpendingseedlings(sql);
	}
	public List getdispatchdetailsforpendingseedlings(Integer seqno,String season)
    {        
        return reasearchAndDevelopmentDao.getdispatchdetailsforpendingseedlings(seqno,season);
    }
	public List getaggrementDetails(String ryotcode,String season)
    {        
        return reasearchAndDevelopmentDao.getaggrementDetails(ryotcode,season);
    }
	public List getauthaggrementDetails(String season,String ryotcode)
    {        
        return reasearchAndDevelopmentDao.getauthaggrementDetails(season,ryotcode);
    }
    public List getContratorDetails()
    {        
        return reasearchAndDevelopmentDao.getContratorDetails();
    }
    
    public List getTransportContratorDetails()
    {        
        return reasearchAndDevelopmentDao.getTransportContratorDetails();
    }
    

	public List getseedsourceLoadSummary(String batchSeries)
    {        
        return reasearchAndDevelopmentDao.getseedsourceLoadSummary(batchSeries);
    }
	public List getOtherRyotsnames(String ryotcode)
    {        
        return reasearchAndDevelopmentDao.getOtherRyotsnames(ryotcode);
    }
	
	public List getTrayType()
    {        
        return reasearchAndDevelopmentDao.getTrayType();
    }
	
	public List<SeedlingEstimate> listSeedlingEstimate()
	{
		return reasearchAndDevelopmentDao.listSeedlingEstimate();
	}
	
	public List<SeedSource> listSourceSeeds(String batchno, String year,String dateid)
	{
		return reasearchAndDevelopmentDao.listSourceSeeds(batchno,year,dateid);
	}
	public List getYearSourceofSeeds()
    {        
        return reasearchAndDevelopmentDao.getYearSourceofSeeds();
    }
	public List getbatchSourceofSeeds(String year,String seedsupplier,String otherryot)
    {        
        return reasearchAndDevelopmentDao.getbatchSourceofSeeds(year,seedsupplier,otherryot);
    }
	public List listAllBatchNumbers(String batchno)
	{
		return reasearchAndDevelopmentDao.listAllBatchNumbers(batchno);
	}
	public List getRyotName(String ryotCode)
    {
		ryotCode=ryotCode.replaceAll("\"", "");
        String sql = "select * from Ryot where ryotcode = '"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllBatchSeries(String batchno)
	{
		return reasearchAndDevelopmentDao.listAllBatchSeries(batchno);
	}
	public List listAllBatchSeriesForDispatch(String batchseries,int variety)
	{
		return reasearchAndDevelopmentDao.listAllBatchSeriesForDispatch(batchseries,variety);
	}
	
	public List listAllKindTypeDetails()
	{
		return reasearchAndDevelopmentDao.listAllKindTypeDetails();
	}
	public List listAllBatchNumbersDetails(String batchno)
	{
		return reasearchAndDevelopmentDao.listAllBatchNumbersDetails(batchno);
	}
	public List listAllBatchSeriesDetails(String batchno)
	{
		return reasearchAndDevelopmentDao.listAllBatchSeriesDetails(batchno);
	}
	public List<DetrashSummary> listAllDetrashingSummaryDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllDetrashingSummaryDetails(Season,addedDate,Shift);
	}
	public List listAllDetrashingDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllDetrashingDetails(Season,addedDate,Shift);
	}
	
	public List<CaneShiftingSummary> listAllCaneShiftingSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllCaneShiftingSummary(Season,addedDate,Shift);
	}
	public List listAllShiftedCaneDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllShiftedCaneDetails(Season,addedDate,Shift);
	}
	public void deleteDetrashDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteDetrashDetails(Season,addedDate,Shift);
	}
	public void deleteShiftedCaneDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteShiftedCaneDetails(Season,addedDate,Shift);
	}
	public void deleteBudCuttingDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteBudCuttingDetails(Season,addedDate,Shift);
	}
	public List listAllBudCuttingSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllBudCuttingSummary(Season,addedDate,Shift);
	}
	public List listAllBudCuttingDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllBudCuttingDetails(Season,addedDate,Shift);
	}
	public void deleteChemicalinventoryDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteChemicalinventoryDetails(Season,addedDate,Shift);
	}
	public void deleteChemicalDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteChemicalDetails(Season,addedDate,Shift);
	}
	public List listAllChemicalTreatmentSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllChemicalTreatmentSummary(Season,addedDate,Shift);
	}
	public List listAllChemicalTreatmentDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllChemicalTreatmentDetails(Season,addedDate,Shift);
	}
	public List listAllChemicalTreatmentInventoryDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllChemicalTreatmentInventoryDetails(Season,addedDate,Shift);
	}
	public List listAllGradingSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllGradingSummary(Season,addedDate,Shift);
	}
	public List listAllGradingDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllGradingDetails(Season,addedDate,Shift);
	}
	public void deleteTrayfillingDetalis(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteTrayfillingDetalis(Season,addedDate,Shift);
	}
	public void deleteGradingDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteGradingDetails(Season,addedDate,Shift);
	}
	public List listAllTrayFillingSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllTrayFillingSummary(Season,addedDate,Shift);
	}
	public List listAllTrayFillingDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllTrayFillingDetails(Season,addedDate,Shift);
	}
	public List getbatchMasterDetails(String Season,String addedDate,String Batchseries,Integer batchdate,Integer batchmonth )
	{
		return reasearchAndDevelopmentDao.getbatchMasterDetails(Season,addedDate,Batchseries,batchdate,batchmonth);
	}
	
	public void updateBatchMasterDetails(Double totalSeedling,Double survivalper,Double balance,Double takenoff,String Season,String addedDate,String Batchseries,Integer batchdate,Integer batchmonth )
	{
		reasearchAndDevelopmentDao.updateBatchMasterDetails(totalSeedling,survivalper,balance,takenoff,Season,addedDate,Batchseries,batchdate,batchmonth);
	}
	public void updateDispatchBatchMasterDetails(Double totalSeedling,Double survivalper,Double balance,Double takenoff,String Batchseries)
	{
		reasearchAndDevelopmentDao.updateDispatchBatchMasterDetails(totalSeedling,survivalper,balance,takenoff,Batchseries);
	}
	public List getbatchMasterDetailsforupdate(String batchSeries)
	{
		return reasearchAndDevelopmentDao.getbatchMasterDetailsforupdate(batchSeries);
	}
	public Double getbatchsurvivalper(Double delivered,Double totalSeedlings)
    {
		Double survivalper= (delivered*100)/totalSeedlings;
		return survivalper;
    }
	public Double getbatchbalance(Double totalSeedlings,Double delivered)
    {
		Double balance= totalSeedlings-delivered;
		return balance;
    }
	public Double getbatchtakenoff(Double totalSeedlings,Double delivered,Double balance)
    {
		Double takenoff= totalSeedlings-delivered-balance;
		return balance;
    }
	public void deleteGreenHouseDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteGreenHouseDetails(Season,addedDate,Shift);
	}
	public void deleteHardeningDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteHardeningDetails(Season,addedDate,Shift);
	}
	public void deleteIndentDetails(String Season,String indentno)
	{
		reasearchAndDevelopmentDao.deleteIndentDetails(Season,indentno);
	}
	public void deleteTrimmingDetails(String Season,String addedDate,Integer Shift)
	{
		reasearchAndDevelopmentDao.deleteTrimmingDetails(Season,addedDate,Shift);
	}
	public List listAllGreenHouseSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllGreenHouseSummary(Season,addedDate,Shift);
	}
	public List listAllGreenHouseDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllGreenHouseDetails(Season,addedDate,Shift);
	}
	public List listAllHardenningSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllHardenningSummary(Season,addedDate,Shift);
	}
	public List listAllHardenningDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllHardenningDetails(Season,addedDate,Shift);
	}
	public List listAllTrimmingSummary(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllTrimmingSummary(Season,addedDate,Shift);
	}
	public List listAllTrimmingDetails(String Season,String addedDate,Integer Shift)
	{
		return reasearchAndDevelopmentDao.listAllTrimmingDetails(Season,addedDate,Shift);
	}
	public List listAllKindDetailsByType(Integer kindType)
	{
		return reasearchAndDevelopmentDao.listAllKindDetailsByType(kindType);

	}
	public List listGetEmployeeType(String empid)
	{
		return reasearchAndDevelopmentDao.listGetEmployeeType(empid);

	}
	public List listGetOnloadIndentsstatus(String Status)
	{
		return reasearchAndDevelopmentDao.listGetOnloadIndentsstatus(Status);
	}
	public List listGetIndents(String status,String Season,String Dfromdate,String Dtodate,Integer variety,Integer fieldman)
	{
		return reasearchAndDevelopmentDao.listGetIndents(status,Season,Dfromdate,Dtodate,variety,fieldman);
	}
	public List GetFeildOfficer(Integer fieldofficer)
	{
		return reasearchAndDevelopmentDao.GetFeildOfficer(fieldofficer);
	}
	public List GetFeildOfficerZone(Integer fieldofficerid)
	{
		return reasearchAndDevelopmentDao.GetFeildOfficerZone(fieldofficerid);
	}
	
	public List GetFeildOfficerempid(Integer fieldempid)
	{
		return reasearchAndDevelopmentDao.GetFeildOfficerempid(fieldempid);
	}
	public void deleteDispatchDetails(String Season,String dispatchno)
	{
		reasearchAndDevelopmentDao.deleteDispatchDetails(Season,dispatchno);
	}
	public List getAuthorisedIndentsforDispatch(String RyotCode,Integer FieldOfficer)
    {        
        return reasearchAndDevelopmentDao.getAuthorisedIndentsforDispatch(RyotCode,FieldOfficer);
    }
	public List getAuthorisedIndentsforDispatch1(String RyotCode,String Season )
    {        
        return reasearchAndDevelopmentDao.getAuthorisedIndentsforDispatch1(RyotCode,Season);
    }
	public List listGetAllIndentDetails(String IndentNo)
	{
		return reasearchAndDevelopmentDao.listGetAllIndentDetails(IndentNo);
	}
	public List listGetAllIndentDetailsbykind(String IndentNo,Integer kind)
	{
		return reasearchAndDevelopmentDao.listGetAllIndentDetailsbykind(IndentNo,kind);
	}
	
	public List listGetAllvarietyIndentDetails(String IndentNo,Integer variety)
	{
		return reasearchAndDevelopmentDao.listGetAllvarietyIndentDetails(IndentNo,variety);
	}
	public List listGetAllIndentSummary(String IndentNo)
	{
		return reasearchAndDevelopmentDao.listGetAllIndentSummary(IndentNo);
	}
	public List listGetDetailsbyKind(Integer kind)
	{
		return reasearchAndDevelopmentDao.listGetDetailsbyKind(kind);
	}
	
	public List getAuthorisedIndentsforDispatchpopup(String IndentNo)
	{
		return reasearchAndDevelopmentDao.getAuthorisedIndentsforDispatchpopup(IndentNo);
	}
	public List getDispatchNoDetailsForPrint(String season,String fromdate,String todate)
	{
		return reasearchAndDevelopmentDao.getDispatchNoDetailsForPrint( season, fromdate, todate);
	}
	public List getAuthorisationDetailsForPrint(String season,String fromdate,String todate)
	{
		return reasearchAndDevelopmentDao.getAuthorisationDetailsForPrint( season, fromdate, todate);
	}
	public List getDispatchDetails(String jsonData)
	{
		return reasearchAndDevelopmentDao.getDispatchDetails(jsonData);
	}
	
	public void deleteAckDetails(String Season,String dispatchNo)
	{
		reasearchAndDevelopmentDao.deleteAckDetails(Season,dispatchNo);
	}
	public List<DispatchDetails> getDataForAcknowledgePrint(String dispatchno)
    {        
        return reasearchAndDevelopmentDao.getDataForAcknowledgePrint(dispatchno);
    }
	public List getAckPrintData(String dispatchno)
	{
		return reasearchAndDevelopmentDao.getAckPrintData(dispatchno);
	}
	public List getryotDetails(String ryotCode)
    {
		return reasearchAndDevelopmentDao.getryotDetails(ryotCode);
    }
	public List getAuthorisePrintsummary(String authseqno,String Season)
    {
		return reasearchAndDevelopmentDao.getAuthorisePrintsummary(authseqno,Season);
    }
    public List getAuthorisePrintdetals(String authseqno,String Season)
    {
		return reasearchAndDevelopmentDao.getAuthorisePrintdetals(authseqno,Season);
    }
    public List listUpdateStatusForAllIndentDetails(String indentno)
    {
		return reasearchAndDevelopmentDao.listUpdateStatusForAllIndentDetails(indentno);
    }
    public List listUpdateStatusForAllIndentDetails1(String indentno,Double issuedQty)
    {
		return reasearchAndDevelopmentDao.listUpdateStatusForAllIndentDetails1(indentno, issuedQty);
    }
    
    public List listAllDispatchAuthorisationSummary(String Season,String Dispatch)
	{
		return reasearchAndDevelopmentDao.listAllDispatchAuthorisationSummary(Season,Dispatch);
	}
    public List listAllDispatchAuthorisationDetails(String Season,String Dispatch)
  	{
  		return reasearchAndDevelopmentDao.listAllDispatchAuthorisationDetails(Season,Dispatch);
  	}
    @SuppressWarnings("unchecked")
	public List<SeedlingTrayChargeByDistance> getTraytypeCostUnitPerDistance(Double Distance,Integer traytype)
    {
		 String sql = "select * from SeedlingTrayChargeByDistance where traycode="+traytype+" and distancefrom<="+Distance+" and "+Distance+"<=distanceto ";
		 logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SeedlingTrayChargeByDistance.class);
    }
    @SuppressWarnings("unchecked")
	public List<SeedlingTray> getTraytype(Integer traytype)
    {
		 String sql = "select * from SeedlingTray where traycode="+traytype+"  ";
		 logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SeedlingTray.class);
    }
    public JSONArray GetZonesforIndent(Integer focode)
  	{
		logger.info("========GetZonesforIndent()==========");
		String strQuery=null;
		int zonecode = 0;
		String zone="NA";
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			
			
				List DateList2 = GetFeildOfficerZone(focode);
				if(focode != null && DateList2.size()>0)
				{
					for (int j = 0; j < DateList2.size(); j++)
					{
						jsonObj = new JSONObject();
						Map dMap2 = new HashMap();
						dMap2 = (Map) DateList2.get(j);
						zonecode = (Integer) dMap2.get("zonecode");
						zone = (String) dMap2.get("zone");

						jsonObj.put("id", zonecode);
						jsonObj.put("zone", zone);

						jArray.add(jsonObj);
					}
				}
			
			return jArray;
		} 
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
	

}
    public Double getSumOfDispatchedqty(String season,String ryotCode,Integer kind)
    {	
		logger.info("========getSumOfDispatchedqty()==========");
		String strQuery=null;
		Double sum=0.0;
		
		strQuery=" select sum(dispatchedQuantity) as sum from KindIndentDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and kind="+kind;
		logger.info("getSumOfDispatchedqty()========strQuery=========="+strQuery);		
		List dropdownList=reasearchAndDevelopmentDao.getSumofDispatch(strQuery);
		
	
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList.get(0);
				sum = (Double) tempMap.get("sum");
			}
			
			if(sum==null)
			{
				sum=0.0;
				return sum;
			}
			else 
			return sum;
			
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
		    return sum;
		}
	}
    
    public List getIndentDtList(String strIndentNo)
	{
		return reasearchAndDevelopmentDao.getIndentDtList(strIndentNo);
	}
    
    public List getryotDetailsForDispatch(String strAgrNo,int agrtype)
    {
		return reasearchAndDevelopmentDao.getryotDetailsForDispatch(strAgrNo,agrtype);
    }
    
    public String getAgrnoForDispatchno(String dispatchno)
    {
		return reasearchAndDevelopmentDao.getAgrnoForDispatchno(dispatchno);
    }
    public double getMaxSeedlingsExtentForAgr(String agrno,String season)
    {
		return reasearchAndDevelopmentDao.getMaxSeedlingsExtentForAgr(agrno,season);
    }
    public double getMaxSeedlingsExtentForAgrauth(String agrno,String season)
    {
		return reasearchAndDevelopmentDao.getMaxSeedlingsExtentForAgrauth(agrno,season);
    }
    public double getTotalSeedlingsDispatchForAgr(String agrno,String season)
    {
		return reasearchAndDevelopmentDao.getTotalSeedlingsDispatchForAgr(agrno,season);
    }
    public List getBatchData()
	{
	return reasearchAndDevelopmentDao.getBatchData();
	 }
    
    public Integer getTransactioncodedetails(String ryotcode ,String glcode ,String authorisationcode)
  	 {        
  		 return reasearchAndDevelopmentDao.getTransactioncodedetails(ryotcode,glcode,authorisationcode);
  	 }
    public double getPrincipalamtdetails(String ryotcode ,Integer accountcode ,String season)
 	 {        
 		 return reasearchAndDevelopmentDao.getPrincipalamtdetails(ryotcode,accountcode,season);
 	 }
    public boolean getadvsummryamtdetails(String ryotcode ,Integer accountcode ,String season,double ryotAmt)
	 {        
		 return reasearchAndDevelopmentDao.getadvsummryamtdetails(ryotcode,accountcode,season,ryotAmt);
	 }
    
    public List getStoreAuthorization()
   	 {        
   		 return reasearchAndDevelopmentDao.getStoreAuthorization();
   	 }
    public String getAuthozationStoreCity(String storeCode)
    {
    	return reasearchAndDevelopmentDao.getAuthozationStoreCity(storeCode);
    }
 public List listAllAuthorisationFormSummary(String Season,String authorisationSeqNo)
	  {
			return reasearchAndDevelopmentDao.listAllAuthorisationFormSummary(Season,authorisationSeqNo);
	  }
	  public List listAllAuthorisationFormDetails(String Season,String authorisationSeqNo)
	  {
			return reasearchAndDevelopmentDao.listAllAuthorisationFormDetails(Season,authorisationSeqNo);
	  }
	 
	  public List getaggrementDetailsForConsumer(String ryotcode,String season)
	    {        
	        return reasearchAndDevelopmentDao.getaggrementDetailsForConsumer(ryotcode,season);
	    }
	  public List<AgreementDetails> getExtentForAuthorizationAgr(String agrno,String season)
	    {
			return reasearchAndDevelopmentDao.getExtentForAuthorizationAgr(agrno,season);
	    }
	  public List getAuthorizationStatusDetails(java.util.Date fromdate,java.util.Date todate)
	    {
			return reasearchAndDevelopmentDao.getAuthorizationStatusDetails(fromdate,todate);
	    }
		
		 public boolean closingIndent(String season,String ryotcode,String indent)
		{
			return reasearchAndDevelopmentDao.closingIndent(season,ryotcode,indent);
		}
	  public boolean updatePrintFlag(String authSeqNo,String season)
		{
			return reasearchAndDevelopmentDao.updatePrintFlag(authSeqNo,season);
		}
	  //added by naidu on 07-03-2017
	  public List getRejectedIndents(String season)
		{
		  return reasearchAndDevelopmentDao.getRejectedIndents(season);
		}
	  public List getRejectedIndentsDetails(String indentno)
		{
		  return reasearchAndDevelopmentDao.getRejectedIndentsDetails(indentno);
		}  
	  public String getRyotAadharNo(String ryotcode)
	    {
		  return reasearchAndDevelopmentDao.getRyotAadharNo(ryotcode);
	    }
	  public List getSeedSourceDetailsForMigration()
		{	 return reasearchAndDevelopmentDao.getSeedSourceDetailsForMigration();
		} 
	  public List getSeedSourceSummary(String seedsupplier)
		{	 return reasearchAndDevelopmentDao.getSeedSourceSummary(seedsupplier);
		} 
	  public List getVillageCircleFromSupplierCode(String supplierCode,String season)
	    {        
	        return reasearchAndDevelopmentDao.getVillageCircleFromSupplierCode(supplierCode,season);
	    }

		public List<DispatchAckSummary> listOfDispatchAckForDispatchNo(String Season,String DispatchNo)
		{
		  return reasearchAndDevelopmentDao.listOfDispatchAckForDispatchNo(Season,DispatchNo);
		}
	public List getVillageCircleFromRyot(String supplierCode)
		    {        
		        return reasearchAndDevelopmentDao.getVillageCircleFromRyot(supplierCode);
		    }
	  
}
