package com.finsol.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.ShiftDao;
import com.finsol.model.CaneManager;
import com.finsol.model.Shift;


/**
 * @author DMurty
 */
@Service("ShiftService")
public class ShiftService 
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);

	@Autowired
	private ShiftDao shiftDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addShift(Shift shift)
	{
		shiftDao.addShift(shift);
	}
	
	public List<Shift> listShifts() 
	{
		return shiftDao.listShifts();
	}
	
	public void deleteShift() {
		shiftDao.deleteShift();
	}

	
	
}
