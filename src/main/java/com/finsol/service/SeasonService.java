package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.SeasonDao;
import com.finsol.model.Season;

/**
 * @author DMurty
 */
@Service("SeasonService")

public class SeasonService 
{
	private static final Logger logger = Logger.getLogger(SeasonService.class);
	
	@Autowired
	private SeasonDao seasonDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeason(Season season)
	{
		seasonDao.addSeason(season);
	}
	
	public List<Season> listSeasons() 
	{
		return seasonDao.listSeasons();
	}
	
	
	

}
