package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.FamilyGroupDao;
import com.finsol.model.FamilyGroup;

/**
 * @author DMurty
 */
@Service("FamilyGroupService")
public class FamilyGroupService 
{
	private static final Logger logger = Logger.getLogger(FamilyGroupService.class);
	
	@Autowired
	private FamilyGroupDao familyGroupDao;
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	
	public void addFamilyGroup(FamilyGroup familyGroup) 
	{
		familyGroupDao.addFamilyGroup(familyGroup);
	}
	
	public List<FamilyGroup> listFamilyGroups() 
	{
		return familyGroupDao.listFamilyGroups();
	}
	
	public List getCircleNames()
    {		
		String query="Select id, circle from Circle";
		return familyGroupDao.getCircleNames(query);
    }
	

	
}
