package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.HarvestingContractorDao;
import com.finsol.dao.TransportingContractorDao;
import com.finsol.model.HCSuretyDetails;
import com.finsol.model.HarvestingContractors;
import com.finsol.model.TCSuretyDetails;
import com.finsol.model.TransportingContractors;

/**
 * @author Rama Krishna
 *
 */
@Service("TransportingContractorService")
public class TransportingContractorService {
	
	private static final Logger logger = Logger.getLogger(TransportingContractorService.class);

	@Autowired
	private TransportingContractorDao transportingContractorDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTransportingContractor(TransportingContractors transportingContractor) {
		transportingContractorDao.addTransportingContractor(transportingContractor);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTCSuretyDetails(TCSuretyDetails tcSuretyDetails) {
		transportingContractorDao.addTCSuretyDetails(tcSuretyDetails);
	}
	

	
	public List<TransportingContractors> listTransportingContractors() {
		return transportingContractorDao.lisTransportingContractors();
	}
	public List<TCSuretyDetails> listTCSuretyDetails() {
		return transportingContractorDao.listTCSuretyDetails();
	}
	
	public void deleteHCSuretyDetails(TransportingContractors transportingContractor) {
		transportingContractorDao.deleteTCSuretyDetails(transportingContractor);
	}
	
	public TransportingContractors getTransportingContractors(int id) {
		return transportingContractorDao.getTransportingContractors(id);
	}
	
	public List<TCSuretyDetails> getTCDetailsByCode(Integer tcode)
    {
        return transportingContractorDao.getTCDetailsByCode(tcode);
    }

}
