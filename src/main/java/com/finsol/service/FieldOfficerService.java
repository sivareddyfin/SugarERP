package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CaneManagerDao;
import com.finsol.dao.FieldOfficerDao;
import com.finsol.model.CaneManager;
import com.finsol.model.FieldOfficer;


/**
 * @author Rama Krishna
 *
 */
@Service("FieldOfficerService")
public class FieldOfficerService {
	
	
private static final Logger logger = Logger.getLogger(FieldOfficerService.class);
	
	@Autowired
	private FieldOfficerDao fieldOfficerDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addFieldOfficer(FieldOfficer fieldOfficer) {
		fieldOfficerDao.addFieldOfficer(fieldOfficer);
	}

	public Integer getMaxID()
    {
		Integer maxVal=(Integer) fieldOfficerDao.getMaxValue("select max(id) from FieldOfficer");
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	
	public List<FieldOfficer> listFieldOfficers() {
		return fieldOfficerDao.listFieldOfficers();
	}
	
	public List getFieldOfficerNames()
    {		
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, fieldOfficer from FieldOfficer";
		return fieldOfficerDao.getFieldOfficerNames(query);
    }

}
