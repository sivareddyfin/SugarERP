package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.controller.LoginController;
import com.finsol.dao.UserDaoImpl;
import com.finsol.model.Users;

/**
 * @author Rama Krishna
 *
 */
@Service("userService")
public class UserSeviceImpl {

	private static final Logger logger = Logger.getLogger(UserSeviceImpl.class);
	@Autowired
	private UserDaoImpl userDao;
	
	public Users getUser(String name) {
		logger.info("----------------UserSeviceImpl-----------");
		return userDao.getUser(name);
	}




}
