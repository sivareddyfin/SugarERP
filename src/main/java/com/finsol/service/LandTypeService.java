package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.LandTypeDao;
import com.finsol.model.CaneManager;
import com.finsol.model.LandType;



/**
 * @author Rama Krishna
 *
 */
@Service("LandTypeService")
public class LandTypeService {
	private static final Logger logger = Logger.getLogger(LandTypeService.class);

	@Autowired
	private LandTypeDao landTypeDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLandType(LandType landType) {
		landTypeDao.addLandType(landType);
	}
	
	public Integer getMaxID()
    {
		Integer maxVal=(Integer) landTypeDao.getMaxValue("select max(id) from RyotType");
		logger.info("====CaneManagerService getMaxID() maxVal========"+maxVal);
		
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	
	
	public List<LandType> listLandTypes() {
		return landTypeDao.listLandTypes();
	}
}
