package com.finsol.service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.HibernateDao;
import com.finsol.model.AuditTrail;
import com.finsol.model.ScreenFields;
import com.finsol.model.Screens;


/**
 * @author Rama Krishna
 *
 */
@Service("AuditTrailService")
public class AuditTrailService {
	
	private static final Logger logger = Logger.getLogger(AuditTrailService.class);
	
	@Autowired
	public CommonService commonService;
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public boolean isAuditTrailRequired(String screenName,String fieldid)
	{		
		//code here
		boolean isAuditTrailRequired=false;
		Screens screen=null;
		ScreenFields screenFields=null;
		String sql = "select * from Screens where screenname ='" + screenName+"'";
        
        List<Screens> ScreenList=hibernateDao.findByNativeSql(sql, Screens.class);
        if(!ScreenList.isEmpty() && ScreenList!=null)
        {
        	screen=(Screens)ScreenList.get(0);
        	Byte screenValue=screen.getIsaudittrailrequired();
        	if(screenValue==0)
        	{
        		/*sql = "select * from ScreenFields where fieldlabel ='" + screenName+"'";
        		List<ScreenFields> screenFieldsList=hibernateDao.findByNativeSql(sql, ScreenFields.class);
        		if(!screenFieldsList.isEmpty() && screenFieldsList!=null)
                {
        			for(int i=0;i<screenFieldsList.size();i++)
        			{
        				screenFields=(ScreenFields)screenFieldsList.get(i);
        				
        				Byte screenFieldsValue=screenFields.getIsaudittrail();
        				if(screenFieldsValue==0)
        				{
        					
        				}
        			}
        			
                }*/
        		
        		isAuditTrailRequired=true;
        	}
        }
        
		return isAuditTrailRequired;		
	}
	
	public void updateAuditTrail(Integer id,Object entityClassNew,String screenName) throws Exception
	{
		
		logger.info("---ID---"+id+"-------Entity Class Name----"+entityClassNew.getClass().getName());
		
		Object entityClassOld=commonService.getEntityById(id,entityClassNew.getClass());	
		
		if(entityClassOld!=null)
			prepareAndSaveModelForAuditTrail(entityClassOld,entityClassNew,screenName);	
		
	}
	
	public void updateAuditTrailAgSummary(String id,Object entityClassNew,String screenName) throws Exception
	{
		
		logger.info("---ID---"+id+"-------Entity Class Name----"+entityClassNew.getClass().getName());
		
		Object entityClassOld=commonService.getEntityByIdVarchar(id,entityClassNew.getClass());	
		
		if(entityClassOld!=null)
			prepareAndSaveModelForAuditTrail(entityClassOld,entityClassNew,screenName);	
		
	}
	
	
	private void prepareAndSaveModelForAuditTrail(Object entityClassOld,Object entityClassNew,String screenName)  throws Exception
	{
		try 
		{
			Class OldClass=entityClassOld.getClass();
			Class NewClass=entityClassNew.getClass();
			Calendar cal = Calendar.getInstance();
			AuditTrail auditTrail = null;
			
			//Field[] Oldfields=OldClass.getDeclaredFields();
			//Field[] fields = OldClass.class.getDeclaredFields();
			//Field[] Newfields=NewClass.getDeclaredFields();
	
			//get all methods in the class
			Method[] method=OldClass.getDeclaredMethods();
			Method[] method1=NewClass.getDeclaredMethods();		
			
			for(int i=0;i<method.length-1;i++)
			{
				 if(isGetter(method[i]))
				 {
					 	//getter method names
					 	String getterNameOld=method[i].getName();
						String getterNameNew=method1[i].getName();
						
						logger.info("getter Name Old---"+getterNameOld+"-----getter name new---"+getterNameNew);
						
						Object valueOld = method[i].invoke(entityClassOld);					
						Object valueNew = method[i].invoke(entityClassNew);
						String fieldName=getterNameNew.substring(3, getterNameNew.length());
						
						
						//logger.info("----Field Name---"+getterNameNew.substring(3, getterNameNew.length()));					
						//logger.info("value------"+valueOld+"---"+valueOld.hashCode());					
						//logger.info("value1------"+valueNew+"---"+valueNew.hashCode());
						if(valueNew!=null && valueNew!=null)
						{
							if(valueOld.hashCode()!=valueNew.hashCode())
							{
								//Prepare Modal Object
								auditTrail = new AuditTrail();
								auditTrail.setScreenid(1);
								auditTrail.setFieldid(fieldName);
								auditTrail.setOldvalue(valueOld.toString());
								auditTrail.setNewvalue(valueNew.toString());
								auditTrail.setAction("update");
								auditTrail.setRemarks("ok");
								auditTrail.setUserid("makella");
								auditTrail.setScreenname(screenName);
								auditTrail.setAudittraildate(new Date());
								auditTrail.setAudittrailtime(new java.sql.Time(cal.getTimeInMillis()));
								
								/*if("CANE MANAGER MASTER".equalsIgnoreCase(screenName))
								{
									String oldval = valueOld.toString();
									String newVal = valueNew.toString();
									
									String table = "Employees";
									String whereClmn="employeeid";
									
									List ValList = new ArrayList();
									ValList = getNamesById(oldval,table,whereClmn);
									if(ValList !=null && ValList.size()>0)
									{
										Map tempMap = new HashMap();
										tempMap = (Map) ValList.get(0);
										String Name = (String) tempMap.get("employeename");
										auditTrail.setOldvalue(Name);
									}
									
									ValList = getNamesById(newVal,table,whereClmn);
									if(ValList !=null && ValList.size()>0)
									{
										Map tempMap = new HashMap();
										tempMap = (Map) ValList.get(0);
										String Name = (String) tempMap.get("employeename");
										auditTrail.setNewvalue(Name);
									}
								}*/
								//FieldOfficerMaster
								/*if("Field Officer Master".equalsIgnoreCase(screenName))
								{
									String oldval = valueOld.toString();
									String newVal = valueNew.toString();
									String whereClmn="id";
									String columnname="id";
									String table = "";
									if("CaneManagerId".equalsIgnoreCase(fieldName) || ("EmployeeId".equalsIgnoreCase(fieldName)))
									{
										if("CaneManagerId".equalsIgnoreCase(fieldName))
										{
											table = "CaneManager";
											whereClmn="id";
										}
										else if("EmployeeId".equalsIgnoreCase(fieldName))
										{
											table = "Employees";
											whereClmn="employeeid";
										}
										
										List ValList = new ArrayList();
										ValList = getNamesById(oldval,table,whereClmn,columnname);
										if(ValList !=null && ValList.size()>0)
										{
											Map tempMap = new HashMap();
											tempMap = (Map) ValList.get(0);
											String Name = (String) tempMap.get("fieldOfficer");
											auditTrail.setOldvalue(Name);
										}
										
										ValList = getNamesById(newVal,table,whereClmn,columnname);
										if(ValList !=null && ValList.size()>0)
										{
											Map tempMap = new HashMap();
											tempMap = (Map) ValList.get(0);
											String Name = (String) tempMap.get("fieldOfficer");
											auditTrail.setNewvalue(Name);
										}
									}
								}*/
								
								
								//Save to Database
								Session session=(Session)hibernateDao.getSessionFactory().openSession();
								session.save(auditTrail);
								session.flush();
							}
						}					 
				 }
			
			}
		}
		catch(Exception e)
		{
			 logger.info(e.getCause(), e);
		}
	}
	
	public List getNamesById(String id,String table,String whereClmn,String columnname)
    {    
    	String sql = "select * from "+table+" where "+whereClmn+"="+id+"";
		List ValueList=hibernateDao.findBySqlCriteria(sql);
        return ValueList;
    }
	
	public static boolean isGetter(Method method)
	{
		  if(!method.getName().startsWith("get"))      return false;
		  if(method.getParameterTypes().length != 0)   return false;  
		  if(void.class.equals(method.getReturnType())) return false;
		  return true;
	}
	
    public List getAuditTrailForSeason(Date date1,Date date2)
    {
    	String sql = "select * from AuditTrail where audittraildate between '" +date1+"' and '" +date2+"'";
		
        
		List auditTrailList=hibernateDao.findBySqlCriteria(sql);
        if(auditTrailList.size()>0 && auditTrailList!=null)
        	return auditTrailList;
        else
        	return null;    
    }

}
