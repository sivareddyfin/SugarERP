package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.StandardDeductionsDao;
import com.finsol.model.StandardDeductions;




/**
 * @author SahaDeva
 */
@Service("StandardDeductionsService")

public class StandardDeductionsService 
{
private static final Logger logger = Logger.getLogger(StandardDeductionsService.class);
	
   @Autowired
   private StandardDeductionsDao standardDeductionsDao;
   
   @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
   public void addStandardDeductions(StandardDeductions standardDeductions)
     {
	   standardDeductionsDao.addStandardDeductions(standardDeductions);
     }
   public List<StandardDeductions> listStandardDeduction() {
		return standardDeductionsDao.listStandardDeduction();
	}
   
   
   
   public void updateStandardDeductionDetails(String name,int id,int orderId)
   {
	   
	   standardDeductionsDao.updateStandardDeductionDetails(name, id,orderId);
   }
   
   

}
