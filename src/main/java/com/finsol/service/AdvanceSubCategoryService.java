package com.finsol.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.AdvanceSubCategoryDao;
import com.finsol.model.AdvanceSubCategory;
import com.finsol.model.CaneManager;

/**
 * @author DMurty
 */
@Service("AdvanceSubCategoryService")

public class AdvanceSubCategoryService
{
	private static final Logger logger = Logger.getLogger(AdvanceSubCategoryService.class);
	
	@Autowired
	private AdvanceSubCategoryDao advanceSubCategoryDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvanceSubCategory(AdvanceSubCategory advanceSubCategory) 
	{
		advanceSubCategoryDao.addAdvanceSubCategory(advanceSubCategory);
	}
	
	public List<AdvanceSubCategory> listAllAdvanceSubCategories() {
		return advanceSubCategoryDao.listAdvanceSubCategories();
	}
	
	
	
}
