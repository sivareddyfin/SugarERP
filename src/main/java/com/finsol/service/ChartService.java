package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.ChartDao;

/**
 * @author Rama Krishna
 * 
 */
@Service("ChartService")
public class ChartService {
	
	private static final Logger logger = Logger.getLogger( ChartService.class);

	@Autowired
	private ChartDao chartDao;
	
	
	public List getExtentSizeByFieldOfficer() 
	{
		return chartDao.getExtentSizeByFieldOfficer();
	}
	
	public List getExtentSizeByFieldAssistant(Integer foid) 
	{
		return chartDao.getExtentSizeByFieldAssistant(foid);
	}
	
	public List getBankDetailsChart() 
	{
		return chartDao.getBankDetailsChart();
	}
	
	public List getBranchDetailsChart(Integer bankid) 
	{
		return chartDao.getBranchDetailsChart(bankid);
	}
	
	public List crushedCaneByPlantOrRatoon() 
	{
		return chartDao.crushedCaneByPlantOrRatoon();
	}
	
	public List crushedCaneByVariety() 
	{
		return chartDao.crushedCaneByVariety();
	}
	
	public List crushedWeightByZone() 
	{
		return chartDao.crushedWeightByZone();
	}
	
	public List circleWiseCrushedWeightByZones(int zoneCode) 
	{
		return chartDao.circleWiseCrushedWeightByZones(zoneCode);
	}

}
