package com.finsol.service;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.HibernateDao;
import com.finsol.dao.StockDao;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.StockDetails;
import com.finsol.model.StockSummary;
import com.finsol.model.StoreBatchMaster;
import com.finsol.utils.DateUtils;

/**
 * @author Murty Ayyagari
 *
 */
@Service("StockService")
public class StockService
{
	private static final Logger logger = Logger.getLogger(StockService.class);
	@Autowired
	private HibernateDao hibernateDao;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private StockDao stockDao;
	
	//InvoiceType
	public List UpdateStockTables(Map StockMap) throws Exception
	{
		List entityList = new ArrayList();
		try
		{
			String finYr = (String) StockMap.get("finYr");
			
			String productCode = (String) StockMap.get("productCode");
			String uom = (String) StockMap.get("uom");
			String batch = (String) StockMap.get("batch");
			if ("".equals(batch) || "null".equals(batch) || batch == null)
			{
				batch = "*";
			}
			String expDt = (String) StockMap.get("expDt");	
			double quantity = (Double) StockMap.get("quantity");	
			double mrp = (Double) StockMap.get("mrp");
			double cp = (Double) StockMap.get("cp");
			String productName = (String) StockMap.get("productName");	
			String productGrpCode = (String) StockMap.get("productGrpCode");
			String productSubGrpCode = (String) StockMap.get("productSubGrpCode");
			String batchId = (String) StockMap.get("batchId");
			int batchSeqNo = (Integer) StockMap.get("batchSeqNo");	

			//
			String batchFlag = (String) StockMap.get("batchFlag");
			String mfgdt = (String) StockMap.get("mfgdt");
			if ("".equals(mfgdt) || "null".equals(mfgdt) || mfgdt == null)
			{
				mfgdt = "*";
			}
			int departmentId = (Integer) StockMap.get("departmentId");	
			byte transactionType = (Byte) StockMap.get("transactionType");
			byte invType = (Byte) StockMap.get("invType");
			String invoiceNo = (String) StockMap.get("invoiceNo");
			int transactionId = (Integer) StockMap.get("transactionId");	
			
			//We need to update BatchMaster table for StockIn Screens only. i.e we need to write if condition here
			//insert in StoreBatchMaster
			
			StoreBatchMaster storeBatchMaster = null;
			if(transactionType == 0)
			{
				if("New".equalsIgnoreCase(batchFlag))
				{
					//int batchSeqNo = storeService.getMaxIDfortable("StoreBatchMaster","seqno",productCode,"productcode");
					//String strBatchSeqNo = Integer.toString(batchSeqNo);
					//String actualBatchId = productCode+strBatchSeqNo;
					//batchId = actualBatchId;
					
					storeBatchMaster = new StoreBatchMaster();
					
					storeBatchMaster.setBatch(batch);
					storeBatchMaster.setExpdt(expDt);
					storeBatchMaster.setMrp(mrp);
					storeBatchMaster.setProductcode(productCode);
					
					storeBatchMaster.setMfgdt(mfgdt);
					Date CurDate = new Date();
					storeBatchMaster.setBatchdate(CurDate);
					storeBatchMaster.setBatchid(batchId);
					storeBatchMaster.setSeqno(batchSeqNo);
					storeBatchMaster.setCp(cp);

				}
				else
				{
					//Here we need to get some parameters from table and add to it.
					List batchList = getBatchDetails(batchId);
					storeBatchMaster = (StoreBatchMaster) batchList.get(0);
					
					storeBatchMaster.setBatch(batch);
					storeBatchMaster.setExpdt(expDt);
					storeBatchMaster.setMrp(mrp);
					storeBatchMaster.setProductcode(productCode);
					storeBatchMaster.setCp(cp);
					

				}
				entityList.add(storeBatchMaster);
			}
			
			
			//Getting CurrentStock
			//Here we need to get Sum of cr and dr and  dr-cr is current stock and we need to pass batchId,finyear and DepartmentId
			double currentStock = getCurrentStock(batchId,finYr,departmentId);
			//Here we need to return list as null. because in this case stock becomes -ve.
			if(currentStock == 0 && transactionType == 1)
			{
				return null;
			}
			
			//insert in StockDetails
			StockDetails stockDetails = new StockDetails();
			
			stockDetails.setBatchid(batchId);
			stockDetails.setDateofupdate(new Date());
			stockDetails.setDepartmentid(departmentId);
			stockDetails.setFinyear(finYr);
			stockDetails.setGroupcode(productGrpCode);
			stockDetails.setInvoicenumber(invoiceNo);
			stockDetails.setInvoicestock(quantity);
			stockDetails.setInvoicetype(invType);
			stockDetails.setProductcode(productCode);
			stockDetails.setProductname(productName);
			stockDetails.setSubgroupcode(productSubGrpCode);
			stockDetails.setTransactioncode(transactionId);
			stockDetails.setTransactiondate(new Date());	// Based on entry date only.
			stockDetails.setCurrentstock(currentStock);
			stockDetails.setStatus((byte) 0);
			
			Time transactiontime = DateUtils.getCurrentSystemTime();
			stockDetails.setTransactiontime(transactiontime);
			
			stockDetails.setTransactiontype(transactionType);
			
			entityList.add(stockDetails);
			
			StockSummary sm = null;
			List StockSummaryList = getStockSummaryDetails(batchId,departmentId);
			if(StockSummaryList != null && StockSummaryList.size()>0)
			{
				sm = (StockSummary) StockSummaryList.get(0);
				
				if(transactionType == 0)	//Stock Supply
				{
					// Here transactionType is always 0 for StockSupply. So we need to add stock to it.
					currentStock = currentStock+quantity;
				}
				else if(transactionType == 1)		
				{
					currentStock = currentStock-quantity;
				}
				
				double stockvalueincp = currentStock*cp;
				double stockvalueinmrp = currentStock*mrp;
				
				sm.setStock(currentStock);
				sm.setStockvalueincp(stockvalueincp);
				sm.setStockvalueinmrp(stockvalueinmrp);
			}
			else
			{
				sm = new StockSummary();
					
				int expmonth = 0;
				int expyear = 0;
				
				if(expDt != null)
				{
					if(expDt.contains("/"))
					{
						String expiry[] = expDt.split("/");
						String strExpmonth = expiry[0];
						expmonth = Integer.parseInt(strExpmonth);
						
						String strExpyear = expiry[1];
						expyear = Integer.parseInt(strExpyear);
					}
					
				}
				
				sm.setBatchid(batchId);
				
				int seqNo = 0;
				List batchList = getBatchDetails(batchId);
				if(batchList != null && batchList.size()>0)
				{
					StoreBatchMaster storeBatchMaster1 = (StoreBatchMaster) batchList.get(0);
					seqNo = storeBatchMaster1.getSeqno();
				}
				else
				{
					seqNo = storeBatchMaster.getSeqno();
				}
				sm.setBatchseqno(seqNo);
				
				sm.setCostprice(cp);
				sm.setDeptid(departmentId);
				sm.setExpmonth(expmonth);
				sm.setExpyear(expyear);
				sm.setGroupcode(productGrpCode);
				sm.setMrp(mrp);
				sm.setProductcode(productCode);
				sm.setSubgroupcode(productSubGrpCode);
				
				currentStock = 0.0;
				if(transactionType == 0)	//Dr
				{
					currentStock = currentStock+quantity;
				}
				else if(transactionType == 1)	//Cr
				{
					currentStock = currentStock-quantity;
				}
				
				double stockvalueincp = currentStock*cp;
				double stockvalueinmrp = currentStock*mrp;
				
				sm.setStock(currentStock);
				sm.setStockvalueincp(stockvalueincp);
				sm.setStockvalueinmrp(stockvalueinmrp);
				sm.setFinyear(finYr);
			}
			entityList.add(sm);
			return entityList;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}
	
	public List getBatchDetails(String batchId)
	{       
        return stockDao.getBatchDetails(batchId);
    }
	
	public double getCurrentStock(String batchId,String finyear,int departmentId)
	{       
        return stockDao.getCurrentStock(batchId,finyear,departmentId);
    }
	
	public List getStockSummaryDetails(String batchId,int departmentId)
	{       
        return stockDao.getStockSummaryDetails(batchId,departmentId);
    }
	
	public List getProductDls(String productcode)
	{       
        return stockDao.getProductDls(productcode);
    }

	public List RevertStockTables(Map StockMap) throws Exception
	{
		List entityList = new ArrayList();
		try
		{
			String finYr = (String) StockMap.get("finYr");
			if("".equals(finYr) || "null".equals(finYr) || finYr == null)
			{
				finYr = "17-18";
			}
			String productCode = (String) StockMap.get("productCode");
			String uom = (String) StockMap.get("uom");
			String batch = (String) StockMap.get("batch");
			if ("".equals(batch) || "null".equals(batch) || batch == null)
			{
				batch = "*";
			}
			String expDt = (String) StockMap.get("expDt");	
			double quantity = (Double) StockMap.get("quantity");	
			double mrp = (Double) StockMap.get("mrp");
			double cp = (Double) StockMap.get("cp");
			String productName = (String) StockMap.get("productName");	
			String productGrpCode = (String) StockMap.get("productGrpCode");
			String productSubGrpCode = (String) StockMap.get("productSubGrpCode");
			String batchId = (String) StockMap.get("batchId");
			String batchFlag = (String) StockMap.get("batchFlag");
			String mfgdt = (String) StockMap.get("mfgdt");
			if ("".equals(mfgdt) || "null".equals(mfgdt) || mfgdt == null)
			{
				mfgdt = "*";
			}
			int departmentId = (Integer) StockMap.get("departmentId");	
			byte transactionType = (Byte) StockMap.get("transactionType");
			byte invType = (Byte) StockMap.get("invType");
			String invoiceNo = (String) StockMap.get("invoiceNo");
			
			int transactionId = (Integer) StockMap.get("transactionId");	
			int batchSeqNo = (Integer) StockMap.get("batchSeqNo");	
			
			//Based on if condition we need to update
			if(transactionType == 0)
			{
				StoreBatchMaster storeBatchMaster = new StoreBatchMaster();
				
				storeBatchMaster.setBatch(batch);
				storeBatchMaster.setExpdt(expDt);
				storeBatchMaster.setMrp(mrp);
				storeBatchMaster.setProductcode(productCode);
			
				List batchList = getBatchDetails(batchId);
				StoreBatchMaster storeBatchMaster1 = (StoreBatchMaster) batchList.get(0);
				
				storeBatchMaster.setMfgdt(storeBatchMaster1.getMfgdt());
				storeBatchMaster.setBatchdate(storeBatchMaster1.getBatchdate());
				storeBatchMaster.setBatchid(batchId);
				storeBatchMaster.setSeqno(storeBatchMaster1.getSeqno());
				storeBatchMaster.setCp(cp);
				entityList.add(storeBatchMaster);
			}
			
			double currentStock = 0.0;
			double prevStock = getStockRunBal(batchId,transactionId,finYr,departmentId);
			if(transactionType == 0)	//Dr
			{
				currentStock = prevStock+quantity;
			}
			else	//Cr
			{
				currentStock = prevStock-quantity;
			}
			
			StockDetails sd = new StockDetails();
			
			sd.setBatchid(batchId);
			sd.setDateofupdate(new Date());
			sd.setDepartmentid(departmentId);
			sd.setFinyear(finYr);
			sd.setGroupcode(productGrpCode);
			sd.setInvoicenumber(invoiceNo);
			sd.setInvoicestock(quantity);
			sd.setInvoicetype(invType);
			sd.setProductcode(productCode);
			sd.setProductname(productName);
			sd.setSubgroupcode(productSubGrpCode);
			sd.setTransactioncode(transactionId);
			sd.setTransactiondate(new Date());	// Based on entry date only.
			sd.setCurrentstock(prevStock);
			sd.setStatus((byte) 0);
			Time transactiontime = DateUtils.getCurrentSystemTime();
			sd.setTransactiontime(transactiontime);
			
			sd.setTransactiontype(transactionType);
			
			entityList.add(sd);
			
			
			StockSummary sm = null;
			List StockSummaryList = getStockSummaryDetails(batchId,departmentId);
			if(StockSummaryList != null && StockSummaryList.size()>0)
			{
				sm = (StockSummary) StockSummaryList.get(0);
				
				double stockvalueincp = currentStock*cp;
				double stockvalueinmrp = currentStock*mrp;
				
				sm.setStock(currentStock);
				sm.setStockvalueincp(stockvalueincp);
				sm.setStockvalueinmrp(stockvalueinmrp);
			}
			else
			{
				sm = new StockSummary();
					
				int expmonth = 0;
				int expyear = 0;
				
				if(expDt != null)
				{
					if(expDt.contains("/"))
					{
						String expiry[] = expDt.split("/");
						String strExpmonth = expiry[0];
						expmonth = Integer.parseInt(strExpmonth);
						
						String strExpyear = expiry[1];
						expyear = Integer.parseInt(strExpyear);
					}
				}
				
				sm.setBatchid(batchId);
				sm.setBatchseqno(batchSeqNo);
				sm.setCostprice(cp);
				sm.setDeptid(departmentId);
				sm.setExpmonth(expmonth);
				sm.setExpyear(expyear);
				sm.setGroupcode(productGrpCode);
				sm.setMrp(mrp);
				sm.setProductcode(productCode);
				sm.setSubgroupcode(productSubGrpCode);
				
				double stockvalueincp = currentStock*cp;
				double stockvalueinmrp = currentStock*mrp;
				
				sm.setStock(currentStock);
				sm.setStockvalueincp(stockvalueincp);
				sm.setStockvalueinmrp(stockvalueinmrp);
				
				//Added by DMurty on 21-03-2017
				sm.setFinyear(finYr);

			}
			entityList.add(sm);

			return entityList;
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	public List getStockDetailsDataForRevert(String batchId,int transactionId,String finYr,int deptId)
	{       
        return stockDao.getStockDetailsDataForRevert(batchId,transactionId,finYr,deptId);
    }
	
	public double getStockRunBal(String batchId,int transactionId,String finyear,int deptId)
	{       
        return stockDao.getStockRunBal(batchId,transactionId,finyear,deptId);
    }
	
	
	public List getTransactionTypeByScreen(String screen)
	{       
        return stockDao.getTransactionTypeByScreen(screen);
    }
	
	public List getProductBatches(String productCode,int deptId)
	{       
        return stockDao.getProductBatches(productCode,deptId);
    }
	//Added by DMurty on 15-03-2017
	public List revertModiftStockOpngBal(Map StockMap) throws Exception
	{
		List entityList = new ArrayList();
		try
		{
			String finYr = (String) StockMap.get("finYr");
			if("".equals(finYr) || "null".equals(finYr) || finYr == null)
			{
				finYr = "17-18";
			}
			String productCode = (String) StockMap.get("productCode");
			String uom = (String) StockMap.get("uom");
			String batch = (String) StockMap.get("batch");
			if ("".equals(batch) || "null".equals(batch) || batch == null)
			{
				batch = "*";
			}
			String expDt = (String) StockMap.get("expDt");	
			double quantity = (Double) StockMap.get("quantity");	
			double mrp = (Double) StockMap.get("mrp");
			double cp = (Double) StockMap.get("cp");
			String productName = (String) StockMap.get("productName");	
			String productGrpCode = (String) StockMap.get("productGrpCode");
			String productSubGrpCode = (String) StockMap.get("productSubGrpCode");
			String batchId = (String) StockMap.get("batchId");
			int batchSeqNo = (Integer) StockMap.get("batchSeqNo");	

			String batchFlag = (String) StockMap.get("batchFlag");
			String mfgdt = (String) StockMap.get("mfgdt");
			if ("".equals(mfgdt) || "null".equals(mfgdt) || mfgdt == null)
			{
				mfgdt = "*";
			}
			int departmentId = (Integer) StockMap.get("departmentId");	
			byte transactionType = (Byte) StockMap.get("transactionType");
			byte invType = (Byte) StockMap.get("invType");
			String invoiceNo = (String) StockMap.get("invoiceNo");
			
			int transactionId = 0;
			if((Integer) StockMap.get("transactionId") != null)
			{
				transactionId = (Integer) StockMap.get("transactionId");
			}
			
			
			StoreBatchMaster storeBatchMaster = null;
			if(transactionType == 0)
			{
				if("New".equalsIgnoreCase(batchFlag))
				{
					storeBatchMaster = new StoreBatchMaster();
					
					storeBatchMaster.setBatch(batch);
					storeBatchMaster.setExpdt(expDt);
					storeBatchMaster.setMrp(mrp);
					storeBatchMaster.setProductcode(productCode);
					
					storeBatchMaster.setMfgdt(mfgdt);
					Date CurDate = new Date();
					storeBatchMaster.setBatchdate(CurDate);
					storeBatchMaster.setBatchid(batchId);
					storeBatchMaster.setSeqno(batchSeqNo);
					storeBatchMaster.setCp(cp);
				}
				else
				{
					//Here we need to get some parameters from table and add to it.
					List batchList = getBatchDetails(batchId);
					storeBatchMaster = (StoreBatchMaster) batchList.get(0);
					
					storeBatchMaster.setBatch(batch);
					storeBatchMaster.setExpdt(expDt);
					storeBatchMaster.setMrp(mrp);
					storeBatchMaster.setProductcode(productCode);
				}
				entityList.add(storeBatchMaster);
			}
			double opngStock = 0.0;
			int oldTranCode = 0;
			
			//if the selected batch is from Opening bal,we should get by these 4 parameters and update stock.
			List stockDetailsList = getStockDetailsForModifyStockOpngRevert(batchId,finYr,departmentId,invType);
			if(stockDetailsList != null && stockDetailsList.size()>0)
			{
				StockDetails stockDetails = (StockDetails) stockDetailsList.get(0);
				opngStock = stockDetails.getInvoicestock();
				oldTranCode = stockDetails.getTransactioncode();
				transactionId = oldTranCode;//(Integer) StockMap.get("transactionId");
				
				//Here 0 is active and 1 is inactive
				stockDetails.setStatus((byte) 1);
				entityList.add(stockDetails);
				
				double currentStock = 0.0;
				double prevStock = getStockRunBal(batchId,oldTranCode,finYr,departmentId);
				if(transactionType == 0)	//Dr
				{
					currentStock = prevStock+quantity;
				}
				else	//Cr
				{
					currentStock = prevStock-quantity;
				}
				
				StockDetails sd = new StockDetails();
				
				sd.setBatchid(batchId);
				sd.setDateofupdate(new Date());
				sd.setDepartmentid(departmentId);
				sd.setFinyear(finYr);
				sd.setGroupcode(productGrpCode);
				sd.setInvoicenumber(invoiceNo);
				sd.setInvoicestock(quantity);
				sd.setInvoicetype(invType);
				sd.setProductcode(productCode);
				sd.setProductname(productName);
				sd.setSubgroupcode(productSubGrpCode);
				sd.setTransactioncode(transactionId);
				sd.setTransactiondate(new Date());	// Based on entry date only.
				sd.setCurrentstock(prevStock);
				sd.setStatus((byte) 0);
				Time transactiontime = DateUtils.getCurrentSystemTime();
				sd.setTransactiontime(transactiontime);
				sd.setTransactiontype(transactionType);
				
				entityList.add(sd);
				
				StockSummary sm = null;
				List StockSummaryList = getStockSummaryDetails(batchId,departmentId);
				if(StockSummaryList != null && StockSummaryList.size()>0)
				{
					sm = (StockSummary) StockSummaryList.get(0);
					
					double stockvalueincp = currentStock*cp;
					double stockvalueinmrp = currentStock*mrp;
					
					sm.setStock(currentStock);
					sm.setStockvalueincp(stockvalueincp);
					sm.setStockvalueinmrp(stockvalueinmrp);
				}
				else
				{
					sm = new StockSummary();
						
					int expmonth = 0;
					int expyear = 0;
					
					if(expDt != null)
					{
						if(expDt.contains("/"))
						{
							String expiry[] = expDt.split("/");
							String strExpmonth = expiry[0];
							expmonth = Integer.parseInt(strExpmonth);
							
							String strExpyear = expiry[1];
							expyear = Integer.parseInt(strExpyear);
						}
					}
					
					sm.setBatchid(batchId);
					sm.setBatchseqno(batchSeqNo);
					sm.setCostprice(cp);
					sm.setDeptid(departmentId);
					sm.setExpmonth(expmonth);
					sm.setExpyear(expyear);
					sm.setGroupcode(productGrpCode);
					sm.setMrp(mrp);
					sm.setProductcode(productCode);
					sm.setSubgroupcode(productSubGrpCode);
					
					double stockvalueincp = currentStock*cp;
					double stockvalueinmrp = currentStock*mrp;
					
					sm.setStock(currentStock);
					sm.setStockvalueincp(stockvalueincp);
					sm.setStockvalueinmrp(stockvalueinmrp);
					
					//Added by DMurty on 21-03-2017
					sm.setFinyear(finYr);
				}
				entityList.add(sm);
			}
			else	
			{
				//here selected batch is not from opening stock. So we need to post entry in stock details with new transaction id and we should get previous stock and add opening stock to previous stock.
				//tempMap.put("New_TransactionId", transactionId);
				int transId = (Integer) StockMap.get("New_TransactionId");
				transactionId = transId;
				
				double currentStock = 0.0;
				double prevStock = getStockRunBalForStockOpngBal(batchId,finYr,departmentId);
				if(transactionType == 0)	//Dr
				{
					currentStock = prevStock+quantity;
				}
				else	//Cr
				{
					currentStock = prevStock-quantity;
				}
				
				StockDetails sd = new StockDetails();
				
				sd.setBatchid(batchId);
				sd.setDateofupdate(new Date());
				sd.setDepartmentid(departmentId);
				sd.setFinyear(finYr);
				sd.setGroupcode(productGrpCode);
				sd.setInvoicenumber(invoiceNo);
				sd.setInvoicestock(quantity);
				sd.setInvoicetype(invType);
				sd.setProductcode(productCode);
				sd.setProductname(productName);
				sd.setSubgroupcode(productSubGrpCode);
				sd.setTransactioncode(transactionId);
				sd.setTransactiondate(new Date());	// Based on entry date only.
				sd.setCurrentstock(prevStock);
				sd.setStatus((byte) 0);
				Time transactiontime = DateUtils.getCurrentSystemTime();
				sd.setTransactiontime(transactiontime);
				
				sd.setTransactiontype(transactionType);
				
				entityList.add(sd);
				
				StockSummary sm = null;
				List StockSummaryList = getStockSummaryDetails(batchId,departmentId);
				if(StockSummaryList != null && StockSummaryList.size()>0)
				{
					sm = (StockSummary) StockSummaryList.get(0);
					
					double stockvalueincp = currentStock*cp;
					double stockvalueinmrp = currentStock*mrp;
					
					sm.setStock(currentStock);
					sm.setStockvalueincp(stockvalueincp);
					sm.setStockvalueinmrp(stockvalueinmrp);
				}
				else
				{
					sm = new StockSummary();
						
					int expmonth = 0;
					int expyear = 0;
					
					if(expDt != null)
					{
						if(expDt.contains("/"))
						{
							String expiry[] = expDt.split("/");
							String strExpmonth = expiry[0];
							expmonth = Integer.parseInt(strExpmonth);
							
							String strExpyear = expiry[1];
							expyear = Integer.parseInt(strExpyear);
						}
					}
					
					sm.setBatchid(batchId);
					sm.setBatchseqno(batchSeqNo);
					sm.setCostprice(cp);
					sm.setDeptid(departmentId);
					sm.setExpmonth(expmonth);
					sm.setExpyear(expyear);
					sm.setGroupcode(productGrpCode);
					sm.setMrp(mrp);
					sm.setProductcode(productCode);
					sm.setSubgroupcode(productSubGrpCode);
					
					double stockvalueincp = currentStock*cp;
					double stockvalueinmrp = currentStock*mrp;
					
					sm.setStock(currentStock);
					sm.setStockvalueincp(stockvalueincp);
					sm.setStockvalueinmrp(stockvalueinmrp);
					
					//Added by DMurty on 21-03-2017
					sm.setFinyear(finYr);
				}
				entityList.add(sm);
			}
			return entityList;
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	
	//Added by DMurty on 15-03-2017 batchId,finYr,deptId,invType
	public List getStockDetailsForModifyStockOpngRevert(String batchId,String finYr,int deptId,int invType)
	{       
        return stockDao.getStockDetailsForModifyStockOpngRevert(batchId,finYr,deptId,invType);
    }
	
	
	public double getStockRunBalForStockOpngBal(String batchId,String finyear,int deptId)
	{       
        return stockDao.getStockRunBalForStockOpngBal(batchId,finyear,deptId);
    }
	//Added by DMurty on 17-03-2017
	public double getCurrentStockForDepartmentByProductCode(String productCode,int deptId,String finYear)
	{       
        return stockDao.getCurrentStockForDepartmentByProductCode(productCode,deptId,finYear);
    }
	
	public int getCentranStoreId()
	{       
        return stockDao.getCentranStoreId();
    }
	
	//Added by DMurty on 22-03-2017
	public List getUomDetails(int uomCode)
	{       
        return stockDao.getUomDetails(uomCode);
    }
	
}
