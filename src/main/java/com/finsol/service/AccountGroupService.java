package com.finsol.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.AccountGroupDao;
import com.finsol.dao.CaneManagerDao;
import com.finsol.model.AccountGroup;
import com.finsol.model.CaneManager;


/**
 * @author DMurty
 */
@Service("AccountGroupService")
public class AccountGroupService 
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private AccountGroupDao accountGroupDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountGroup(AccountGroup accountGroup)
	{
		accountGroupDao.addAccountGroup(accountGroup);
	}
	
	
	//
	public List<AccountGroup> listAllAccountGroups()
	{
		return accountGroupDao.listAccountGroups();
	}
	
	

	
	

}
