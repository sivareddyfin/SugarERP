package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.HibernateDao;
import com.finsol.model.Branch;


/**
 * @author Rama Krishna
 *
 */
@Service("JasperReportService")
public class JasperReportService {
	
	private static final Logger logger = Logger.getLogger(JasperReportService.class);
	@Autowired
	private HibernateDao hibernateDao;
	
	
	public List getBranchDetailsByBankCode(Integer bankcode)
    {
        String sql = "select branchname,city,ifsccode from Branch where bankcode =" + bankcode;
        
        return hibernateDao.getColumns(sql);
    }

}
