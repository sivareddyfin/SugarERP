package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.RegionDao;
import com.finsol.model.CaneManager;
import com.finsol.model.Region;


/**
 * @author DMurty
 */
@Service("RegionService")

public class RegionService 
{
	private static final Logger logger = Logger.getLogger(RegionService.class);

	
	@Autowired
	private RegionDao regionDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRegion(Region region)
	{
		regionDao.addRegion(region);
	}
	
	public List<Region> listRegions() {
		return regionDao.listAllRegions();
	}
	
	
	
}
