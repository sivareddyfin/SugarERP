package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CircleDao;
import com.finsol.model.CaneManager;
import com.finsol.model.Circle;


/**
 * @author DMurty
 */

@Service("CircleService")
public class CircleService
{
	private static final Logger logger = Logger.getLogger(CircleService.class);
	
	@Autowired
	private CircleDao circleDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCircle(Circle circle)
	{
		circleDao.addCircle(circle);
	}
	
	public List<Circle> listCircles() 
	{
		return circleDao.listCircles();
	}
	
	public List getFieldAssistantNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, fieldassistant from FieldAssistant";
		return circleDao.getFieldAssistantNames(query);
	}
	
	public List getVillageNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, name from Village";
		return circleDao.getVillageNames(query);
	}
	
	public List listCircleNew() 
	{
		String query = "select c.id,c.circlecode,c.villagecode,c.circle,c.description,c.fieldassistantid,c.fieldofficerid,c.canemanagerid,c.status,v.village,f.fieldassistant,c.zonecode from  Village v right join Circle c on v.villagecode=c.villagecode left join FieldAssistant f on c.fieldassistantid=f.fieldassistantid;";
		return circleDao.listCircleNew(query);
	}

	
}
