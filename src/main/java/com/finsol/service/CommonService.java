package com.finsol.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CommonDao;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccountSummaryTemp;
import com.finsol.model.FieldOfficer;
import com.finsol.model.Ryot;
import com.finsol.model.WeighmentDetails;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author Rama Krishna
 *
 */
@Service("CommonService")
public class CommonService {
	
	private static final Logger logger = Logger.getLogger(CommonService.class);

	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private HibernateDao hibernateDao;
	
	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired
	private ReasearchAndDevelopmentService reasearchAndDevelopmentService;
	
	///
	
	public Integer getMaxID(String tableName)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max(id) from "+tableName);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public Integer getMaxSeedID(String tableName,String columnname)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max("+columnname+") from "+tableName);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public Integer getMaxIDforColumn(String tableName,String columnName)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max("+columnName+") from "+tableName);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }

	

	public Integer getMaxIDforContract(String tableName,String columnName,String prefix)
    {  
		String sql=null;
			sql="select max("+columnName+") from "+tableName+" where prefix='"+prefix+"'";
		Integer maxVal=(Integer) commonDao.getMaxValue(sql);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public Integer getMaxLoanNo(String tableName,String colName,int bankCode)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max("+colName+") from "+tableName+" where branchcode="+bankCode);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	
	
	public Integer getCaneManagerID(int officerId) {		
		
		Integer caneId=(Integer) commonDao.getCaneManagerID("select caneManagerId  from FieldOfficer where fieldOfficerId="+officerId);
		return caneId;
	}
	
	public String getEmployeeNameID(Integer employeeId) {		
		
		String employeeName=(String) commonDao.getCaneManagerID("select employeename  from Employees where employeeid="+employeeId);
		return employeeName;
	}

	
	public JSONArray getDropdownNames(String tableName,String columnName)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		
		if(tableName.equalsIgnoreCase("Bank"))
		{
			strQuery="Select id,"+ columnName+" from "+ tableName;
		}
		else if(tableName.equalsIgnoreCase("StandardDeductions"))
		{
			strQuery="Select stdDedCode,"+ columnName+" from "+ tableName+" where status = 0 order by deductionOrder";
		}
		else if(tableName.equalsIgnoreCase("Branch"))
		{
			strQuery="Select branchcode,"+ columnName+" from "+ tableName+" where status=0";
		}
		else if(tableName.equalsIgnoreCase("ExtentDetails"))
		{
			strQuery="Select ryotcode,"+ columnName+" from Ryot where status=0";
		}
		else if(tableName.equalsIgnoreCase("LoanDetails"))
		{
			strQuery="Select id,"+ columnName+" from LoanDetails";
		}
		else if(tableName.equalsIgnoreCase("Ryot"))
		{
			strQuery="Select ryotcode,"+ columnName+" from Ryot where status=0";
		}
		else if(tableName.equalsIgnoreCase("Period"))
		{
			strQuery="Select periodid,"+ columnName+" from Period";
		}
		else if(tableName.equalsIgnoreCase("Month"))
		{
			strQuery="Select monthid,"+ columnName+" from Month where status=0";
		}
		else if(tableName.equalsIgnoreCase("ProgramSummary"))
		{
			strQuery="Select id,"+ columnName+" from ProgramSummary";
		}
		else if(tableName.equalsIgnoreCase("Village"))
		{
			strQuery="Select villagecode,"+ columnName+" from Village";
		}
		//Added by DMurty on 10-08-2016
		else if(tableName.equalsIgnoreCase("StandardDeductions"))
		{
			strQuery="Select id,"+ columnName+" from StandardDeductions where status=0";
		}
		//Added by DMurty on 30-08-2016
		else if(tableName.equalsIgnoreCase("Circle"))
		{
			strQuery="Select circlecode,"+ columnName+" from Circle where status=0 order by circle asc";
		}
		//Added by DMurty on 27-10-2016
		else if(tableName.equalsIgnoreCase("AccountGroup"))
		{
			strQuery="Select accountgroupcode,"+ columnName+" from AccountGroup where status=0 order by accountgroup asc";
		}
		//Added by DMurty on 19-11-2016
		else if(tableName.equalsIgnoreCase("WeighBridge"))
		{
			strQuery="Select id,"+ columnName+" from "+ tableName+" where status=0 order by bridgeid";
		}	//Added by Umanath Ch on 25-10-2016
		else if(tableName.equalsIgnoreCase("Shift"))
		{
			strQuery="Select shiftid,shiftname from Shift order by shiftname asc";
		}
		else if(tableName.equalsIgnoreCase("Machine"))
		{
			strQuery="Select machinecode,machinenumber from Machine where status=0 order by machinecode asc";
		}
		else if(tableName.equalsIgnoreCase("FieldOfficer"))
		{
			strQuery="Select fieldOfficerid,fieldOfficer from FieldOfficer where status=0 order by fieldOfficerid asc";
		}
			else
			strQuery="Select id,"+ columnName+" from "+ tableName+" where status=0";
		
		List dropdownList=commonDao.getDropdownNames(strQuery);
		
		try 
		{
			
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					
					Object id =null;
					if(myResult[0] instanceof String)
						id=myResult[0];
					else
					 id =myResult[0];
					
					Object cName = null;
					
					if(myResult[1] instanceof String)
						cName=myResult[1];
					else
						cName = myResult[1];
					
					jsonObj.put("id", id);
					if(tableName.equalsIgnoreCase("Shift"))
					{
						jsonObj.put("shiftName", cName);
					}
					else if(tableName.equalsIgnoreCase("Machine"))
					{
						jsonObj.put("machineNumber", cName);
					}
					
					else
					{
					jsonObj.put(columnName, cName);
					}
					//jsonObj.put(columnName, cName);
					jArray.add(jsonObj);
				}
			}
			
			return jArray;
			
		} catch (Exception e) {
		            logger.info(e.getCause(), e);
		            return null;
		}
		
	}
	
	public JSONArray getDropdownNamesForVillage(String tableName,String columnName)
    {	
		logger.info("========getDropdownNamesForVillage()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select distinct villagecode,"+ columnName+" from Village where status=0 order by village";
		List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				for (int i = 0; i < dropdownList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					String villageCode = (String) tempMap.get("villagecode");
					String village = (String) tempMap.get("village");
					
					jsonObj.put("id", villageCode);
					jsonObj.put(columnName, village);
					jArray.add(jsonObj);
				}
			}
			return jArray;
			
		} catch (Exception e) {
		            logger.info(e.getCause(), e);
		            return null;
		}
		
	}
	public boolean ValidateRyotCode(String ryotCode)
    {	
		logger.info("========ValidateRyotCode()=========="+ryotCode);
		boolean flag = false;
		String strQuery=null;
		strQuery="Select * from Ryot where ryotcode='"+ryotCode+"'";
		logger.info("========strQuery=========="+strQuery);

		List dropdownList=commonDao.ValidateRyotCode(strQuery);
		logger.info("========dropdownList=========="+dropdownList);
		if(dropdownList != null && dropdownList.size()>0)
		{
			 flag = true;
		}
		logger.info("========flag=========="+flag);

		return flag;
    }
	//Added by DMurty on 15-06-2016 
	public JSONArray GetRyotNameForCode(String ryotCode)
    {	
		logger.info("========GetRyotNameForCode()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select ryotname from Ryot where ryotcode='"+ryotCode+"'";
		List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList.get(0);
				
				jsonObj = new JSONObject();
				String ryotName = (String) tempMap.get("ryotname");
				if(ryotName == "" || ryotName == "null" || ryotName == null)
				{
					ryotName = "NA";
				}
				jsonObj.put("ryotName", ryotName);
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
	}
	
	//Added by DMurty on 15-06-2016 
	public JSONArray GetMaxRyotCodeByVillage(String villageCode)
    {	
		logger.info("========GetMaxRyotCodeByVillage()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select max(ryotcodesequence) as maxno from Ryot where villagecode='"+villageCode+"'";
		List dropdownList=commonDao.getMaxRyotCode(strQuery);
		try 
		{
			int ryotcount =0;
			if(dropdownList != null && dropdownList.size()>0)
			{
				if (!dropdownList.contains(null)) // oldDate.equals(returndate.trim()))
				{
					ryotcount = (Integer) dropdownList.get(0);
				}
				else
				{
					ryotcount = 0;
				}
				//Map tempMap = new HashMap();
				//tempMap = (Map) dropdownList.get(0);
				
				jsonObj = new JSONObject();
				
				//ryotcount = (Integer) tempMap.get("maxno");
				ryotcount++;
				
				String strryotcount = Integer.toString(ryotcount);
				if(strryotcount.length() == 3)
				{
					strryotcount = "0"+strryotcount;
				}
				else if(strryotcount.length() == 2)
				{
					strryotcount = "00"+strryotcount;
				}
				else if(strryotcount.length() == 1)
				{
					strryotcount = "000"+strryotcount;
				}
				strryotcount = villageCode+strryotcount;
				
				jsonObj.put("ryotCode", strryotcount);
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
	}
	public Object getEntityByIdVarchar(String id,Class className) 
	{
		logger.info("----------class Name---------"+className);
		return commonDao.getEntityByIdVarchar(id,className);
	}
	public String GetstrMaxRyotCodeByVillage(String villageCode)
    {	
		logger.info("========GetstrMaxRyotCodeByVillage()==========");
		String strQuery=null;
		strQuery="Select max(ryotcodesequence) as maxno from Ryot where villagecode='"+villageCode+"'";
		//List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		List dropdownList=commonDao.getMaxRyotCode(strQuery);

		try 
		{
			String strryotcount = null;
			if(dropdownList != null && dropdownList.size()>0)
			{
				//Map tempMap = new HashMap();
				//tempMap = (Map) dropdownList.get(0);
				int ryotcount = 0 ;
				if (!dropdownList.contains(null)) // oldDate.equals(returndate.trim()))
				{
					ryotcount = (Integer) dropdownList.get(0);
				}
				ryotcount++;

				strryotcount = Integer.toString(ryotcount);
				if(strryotcount.length() == 3)
				{
					strryotcount = "0"+strryotcount;
				}
				else if(strryotcount.length() == 2)
				{
					strryotcount = "00"+strryotcount;
				}
				else if(strryotcount.length() == 1)
				{
					strryotcount = "000"+strryotcount;
				}
				strryotcount = villageCode+strryotcount;
				logger.info("========strryotcount=========="+strryotcount);

			}
			return strryotcount;
		} 
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
	}
	
	public JSONArray loadMaxIdForVillage(int mandalCode)
    {	
		logger.info("========loadMaxIdForVillage()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select count(*) as maxcount from Village where mandalcode="+mandalCode;
		List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				for (int i = 0; i < dropdownList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					
					
					int villageCode = (Integer) tempMap.get("maxcount");
					villageCode++;

					String strvillageCode = Integer.toString(villageCode);
					String strmandalCode = Integer.toString(mandalCode);
					
					if(strmandalCode.length() == 1)
					{
						strmandalCode = "0"+strmandalCode;
					}
					
					if(strvillageCode.length() == 1)
					{
						strvillageCode = "0"+strvillageCode;
					}
					
					String finalVillageCode = strmandalCode+strvillageCode;
					
					jsonObj.put("id", finalVillageCode);
					
					jArray.add(jsonObj);
				}
			}
			return jArray;
			
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		    return null;
		}
	}
	
	public int getMaxPermitNum(String season)
    {	
		logger.info("========getMaxPermitNum()==========");
		
		String strQuery=null;
		Integer permitNum=0;
		strQuery="Select max(permitnumber) as maxcount from PermitDetails where season='"+season+"'";
		logger.info("========strQuery=========="+strQuery);		
		List dropdownList=commonDao.getDropdownNamesForPermitNum(strQuery);
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList.get(0);
				permitNum = (Integer) tempMap.get("maxcount");
				if(permitNum==null)
				{
					permitNum = 0;
				}
				else
				{
					permitNum=permitNum+1;
				}
					
			}
			return permitNum;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
		    return permitNum;
		}
		
	}
	
	public String loadMaxIdForVillageForMaster(int mandalCode)
    {	
		logger.info("========loadMaxIdForVillageForMaster()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select count(*) as maxcount from Village where mandalcode="+mandalCode;
		List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		try 
		{
			String finalVillageCode = "";
			if(dropdownList != null && dropdownList.size()>0)
			{
				for (int i = 0; i < dropdownList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					
					
					int villageCode = (Integer) tempMap.get("maxcount");
					villageCode++;

					String strvillageCode = Integer.toString(villageCode);
					String strmandalCode = Integer.toString(mandalCode);
					
					if(strmandalCode.length() == 1)
					{
						strmandalCode = "0"+strmandalCode;
					}
					
					if(strvillageCode.length() == 1)
					{
						strvillageCode = "0"+strvillageCode;
					}
					
					finalVillageCode = strmandalCode+strvillageCode;
					
				}
			}
			return finalVillageCode;
			
		} catch (Exception e) {
		            logger.info(e.getCause(), e);
		            return null;
		}
		
	}
	
	public JSONArray loadDropDownNamesForSurietyRyot(String tableName,String columnName)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		jsonObj = new JSONObject();
		strQuery="Select ryotcode,"+ columnName+" from Ryot where status=0";
		List dropdownList=commonDao.getDropdownNames(strQuery);
		
		jsonObj.put("id", "0");
		jsonObj.put(columnName, "Other");
		jArray.add(jsonObj);

		try 
		{
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					
					Object[] myResult = (Object[]) it.next();
					
					Object id =null;
					if(myResult[0] instanceof String)
						id=myResult[0];
					else
					 id =myResult[0];
					
					Object cName = null;
					
					if(myResult[1] instanceof String)
						cName=myResult[1];
					else
						cName = myResult[1];
					
					jsonObj.put("id", id);
					jsonObj.put(columnName, cName);
					jArray.add(jsonObj);
				}
			}
			return jArray;
		}
		catch (Exception e)
		{
		     logger.info(e.getCause(), e);
		     return null;
		}
		
	}
	
	public JSONArray loadAddedDropDownsForMasters(String tableName,String columnName,String columnName1)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		strQuery="Select "+columnName+","+ columnName1+" from "+ tableName;
		List dropdownList=commonDao.getDropdownNames(strQuery);
		try 
		{
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					
					Object id =null;
					
					if(myResult[0] instanceof String)
						id=myResult[0];
					else
					 id =myResult[0];
					
					Object cName = null;
					
					if(myResult[1] instanceof String)
						cName=myResult[1];
					else
						cName = myResult[1];
					
					jsonObj.put("id", id);
					jsonObj.put(columnName1, cName);
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e) 
		{
		    logger.info(e.getCause(), e);
		    return null;
		}
	}
	
	public JSONArray onChangeDropDownValues(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		//Modified by DMurty on 01-07-2016 for transaction screens are getting in inactive values
		strQuery="Select id,"+ columnName+" from "+ tableName+" where status=0 and "+ whereColumnname +"='"+value+"'";
		//strQuery="Select id,"+ columnName+" from "+ tableName+" where  "+ whereColumnname +"='"+value+"'";
		List dropdownList=commonDao.getDropdownNames(strQuery);
		try 
		{
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					Object id =null;
					if(myResult[0] instanceof String)
						id=(String)myResult[0];
					else
					 id = (Integer) myResult[0];
					
					String cName = (String) myResult[1];
					jsonObj.put("id", id);
					jsonObj.put(columnName, cName);
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
	}
	
	public JSONArray onChangeDropDownValuesForVillage(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		
		strQuery="Select villagecode,"+ columnName+" from "+ tableName+" where  "+ whereColumnname +"='"+value+"'";
		List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
		
		try 
		{

			if(dropdownList != null && dropdownList.size()>0)
			{
				for (int i = 0; i < dropdownList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					String villageCode = (String) tempMap.get("villagecode");
					String village = (String) tempMap.get("village");
					
					jsonObj.put("id", villageCode);
					jsonObj.put(columnName, village);
					jArray.add(jsonObj);
				}
			}
			
			return jArray;
			
		} catch (Exception e) {
		            logger.info(e.getCause(), e);
		            return null;
		}
		
	}
	
	public JSONObject onChangeFieldValues(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		
		strQuery="Select "+ columnName+" from "+ tableName+" where "+ whereColumnname +"='"+value+"'";
		List dropdownList=commonDao.getDropdownNames(strQuery);
		try 
		{
			jsonObj = new JSONObject();
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				Object myResult =(Object)dropdownList.get(0);
				
				if(myResult instanceof String)			
					jsonObj.put("ifsccode", myResult.toString());
				else
					jsonObj.put("value", myResult);				
				//String ifsccode = (String) myResult;
				//jsonObj.put("ifsccode", myResult.toString());
				
			}
			return jsonObj;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}
	public JSONArray getLoansForSeason(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;

		strQuery="Select "+ columnName+" from "+ tableName+" where "+ whereColumnname +"='"+value+"'";
		List dropdownList=hibernateDao.findBySqlCriteria(strQuery);
		try 
		{
			jsonObj = new JSONObject();
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				for (int i=0;i<dropdownList.size();i++)
				{
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					int loannumber = (Integer) TempMap.get("loannumber");
					jsonObj.put("addedLoan", loannumber);		
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e) 
		{
		      logger.info(e.getCause(), e);
		     return null;
		}
	}
	

	public JSONArray getRyotsFromAgreementSummaryNew(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;

		strQuery="Select a."+ columnName+",b.ryotname from "+ tableName+" a,Ryot b where a."+ whereColumnname +"='"+value+"' and a.ryotcode=b.ryotcode order by b.ryotname asc";
		List dropdownList=hibernateDao.findBySqlCriteria(strQuery);
		try 
		{
			jsonObj = new JSONObject();
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				for (int i=0;i<dropdownList.size();i++)
				{
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					
					String ryotCode = (String) TempMap.get("ryotcode");
					String ryotName = (String) TempMap.get("ryotname");

					/*List<String> RyotNamesList = new ArrayList<String>();
					RyotNamesList = ahFuctionalService.getRyotName(ryotCode);
					if (RyotNamesList != null && RyotNamesList.size() > 0)
					{
						String myResult1 = RyotNamesList.get(0);
						ryotName = myResult1;
					}*/
					jsonObj.put("ryotcode", ryotCode);		
					jsonObj.put("ryotName", ryotName);	
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e) 
		{
		      logger.info(e.getCause(), e);
		     return null;
		}
	}
	
	
	public JSONArray getRyotsFromAgreementSummary(String tableName,String columnName,String whereColumnname,String value)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;

		strQuery="Select "+ columnName+" from "+ tableName+" where "+ whereColumnname +"='"+value+"'";
		List dropdownList=hibernateDao.findBySqlCriteria(strQuery);
		try 
		{
			jsonObj = new JSONObject();
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				for (int i=0;i<dropdownList.size();i++)
				{
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					
					String ryotCode = (String) TempMap.get("ryotcode");
					
					String ryotName = "NA";
					List<String> RyotNamesList = new ArrayList<String>();
					RyotNamesList = ahFuctionalService.getRyotName(ryotCode);
					if (RyotNamesList != null && RyotNamesList.size() > 0)
					{
						String myResult1 = RyotNamesList.get(0);
						ryotName = myResult1;
					}
					jsonObj.put("ryotcode", ryotCode);		
					jsonObj.put("ryotName", ryotName);	
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e) 
		{
		      logger.info(e.getCause(), e);
		     return null;
		}
	}
	
	public String getEmployeeNameFromEmployee(int employeeId) {		
		
		String employeeName=null;
		//String employeeName=(String) commonDao.getCaneManagerID("select employeename  from Employee where employeeid="+employeeId);
		return employeeName;
	}
	

	public Object getEntityById(Integer id,Class className) 
	{
		logger.info("----------class Name---------"+className);
		return commonDao.getEntityById(id,className);
	}
	
	public Object getEntityByIdForVillage(String id,Class className) 
	{
		logger.info("----------class Name---------"+className);
		return commonDao.getEntityByIdForVillage(id,className);
	}
	
	public Integer getCircleCodeFromAgreementDetails(String agNumber)
	{	
		Integer circleCode=(Integer) commonDao.getCircleCodeFromAgreementDetails("select circlecode  from AgreementDetails where agreementnumber='"+agNumber+"'");
		return circleCode;
	}
	
	//Added by DMurty on 11-04-2016
	//This code is to Get,Update data from AccDtls,AccSmry,AccGrpDtls,AccGrpSmry,AccSubGrpDtls,AccSubGrpSmry
	
	public List AccSmryBalanceDetails(String AccCode,double TotalAmount,String TransactionType,String season)
	{
		return commonDao.AccSmryBalanceDetails(AccCode,TotalAmount,TransactionType,season);
	}
	
	public List getAccCodes(String AccCode)
	{
		return commonDao.getAccCodes(AccCode);
	}
	
	public List AccGrpBalanceDetails(int AccGrpCode,double TotalAmt,String TransactionType,String season)
	{
		return commonDao.AccGrpBalanceDetails(AccGrpCode,TotalAmt,TransactionType,season);
	}
	
	public List SubGrpBalanceDetails(int AccGrpCode,int AccSubGrpCode,double TotalAmt,String TransactionType,String season)
	{
		return commonDao.SubGrpBalanceDetails(AccGrpCode,AccSubGrpCode,TotalAmt,TransactionType,season);
	}
	
	public List BalDetails(String AccCode,String season)
	{
		return commonDao.BalDetails(AccCode,season);
	}
	
	public void updateRunningBalinSmry(double finalAmt,String strCrDr,String AccCode,String season)
    {
		commonDao.updateRunningBalinSmry(finalAmt,strCrDr,AccCode,season);
    }
	
	public void updateStatusForAccDtls(int TransactionCode,String season)
    {
		commonDao.updateStatusForAccDtls(TransactionCode,season);
    }
	
	public List SubGrpBalDetailsForRevert(int Transcode)
	{
		return commonDao.SubGrpBalDetailsForRevert(Transcode);
	}
	public List SubGrpBalDetails(int AccGrpCode,int AccSubGrpCode,String season)
	{
		return commonDao.SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
	}
	
	public void updateRunningBalinAccSubGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,int AccSubGrpCode,String season)
    {
		commonDao.updateRunningBalinAccSubGrpSmry(finalAmt,strCrDr,AccGrpCode,AccSubGrpCode,season);
    }
	
	
	public void updateStatusForAccSubGrpDtls(int TransactionCode,String season)
    {
		commonDao.updateStatusForAccSubGrpDtls(TransactionCode,season);
    }
	
	
	public List AccGrpBalDetailsForRevert(int Transcode)
	{
		return commonDao.AccGrpBalDetailsForRevert(Transcode);
	}
	
	public List AccGrpBalDetails(int AccGrpCode,String season)
	{
		return commonDao.AccGrpBalDetails(AccGrpCode,season);
	}
	
	public void updateRunningBalinAccGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,String season)
    {
		commonDao.updateRunningBalinAccGrpSmry(finalAmt,strCrDr,AccGrpCode,season);
    }
	
	public void updateStatusForAccGrpDtls(int TransactionCode,String season)
    {
		commonDao.updateStatusForAccGrpDtls(TransactionCode,season);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSmry(AccountSummary accountSummary) 
	{
		commonDao.addAccountSmry(accountSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountGrpDtls(AccountGroupDetails accountGroupDetails) 
	{
		commonDao.addAccountGrpDtls(accountGroupDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountGrpSmry(AccountGroupSummary accountGroupSummary) 
	{
		commonDao.addAccountGrpSmry(accountGroupSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSubGrpDtls(AccountSubGroupDetails accountSubGroupDetails) 
	{
		commonDao.addAccountSubGrpDtls(accountSubGroupDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSubGrpSmry(AccountSubGroupSummary accountSubGroupSummary) 
	{
		commonDao.addAccountSubGrpSmry(accountSubGroupSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountDtls(AccountDetails accountDetails) 
	{
		commonDao.addAccountDtls(accountDetails);
	}
	
	public void RevertAccountTables(int TransactionCode)
	{
		double finalAmt = 0;
		String strCrDr = "Dr";
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		
		List AccDtlsList =  commonDao.AccDetailsForRevert(TransactionCode);
		logger.info("========AccDtlsList=========="+AccDtlsList);
		if (AccDtlsList != null && AccDtlsList.size() > 0)
		{
			for (Iterator it = AccDtlsList.iterator(); it.hasNext();)
			{
				jsonObj = new JSONObject();
				Object[] myResult = (Object[]) it.next();
				
				String AccCode = (String)  myResult[0];
				Double CrAmt = (Double) myResult[1];
				Double DrAmt = (Double) myResult[2];
				String glCode = (String)  myResult[3];
				Double UpdatingAmt = (Double) myResult[4];
				Byte status = (Byte)  myResult[5];
				String user = (String)  myResult[6];
				String season = (String)  myResult[7];

				String TransType = "Dr";
				if(CrAmt>0)
				{
					TransType="Cr";
				}
				else
				{
					TransType="Dr";
				}
				//Now Get Smry Rec
				List BalList = BalDetails(AccCode,season);
				logger.info("========BalList=========="+BalList);
				if (BalList != null && BalList.size() > 0)
				{
					Object[] myResult1 = (Object[]) BalList.get(0);
					Double RunnBal = (Double) myResult1[1];
					String BalType = (String) myResult1[0];
					logger.info("========RunnBal=========="+RunnBal);
					logger.info("========BalType=========="+BalType);
					if (BalType.equals(TransType))
					{
						if (RunnBal > UpdatingAmt)
						{
							finalAmt = RunnBal-UpdatingAmt;
							strCrDr = BalType;
						}
						else if (RunnBal < UpdatingAmt)
						{
							finalAmt = UpdatingAmt - RunnBal;
							if ("Cr".equals(BalType))
							{
								strCrDr = "Dr";
							}
							else
							{
								strCrDr = "Cr";
							}	
						}
						else
						{
							finalAmt = 0.0;
							strCrDr = "Dr";
						}
					}
					else
					{
						finalAmt = RunnBal + UpdatingAmt;
						strCrDr = BalType;
					}
				}
				
				//Here we need to update finalAmt and strCrDr in Acc Smry
				updateRunningBalinSmry(finalAmt,strCrDr,AccCode,season);
	    		//Update that transaction status to invalid i.e 0 in account details table.
	    		updateStatusForAccDtls(TransactionCode,season);
	    		
			}
		}
		
		//AccSubGrp
		 List AccSubGrpDtlsList = SubGrpBalDetailsForRevert(TransactionCode);
		 if (AccSubGrpDtlsList != null && AccSubGrpDtlsList.size() > 0)
		 {
			for (Iterator it = AccSubGrpDtlsList.iterator(); it.hasNext();)
			{
				jsonObj = new JSONObject();
				Object[] myResult = (Object[]) it.next();
											
				Integer AccGrpCode = (Integer)  myResult[0];
				Double CrAmt = (Double) myResult[1];
				Double DrAmt = (Double) myResult[2];
				Double UpdatingAmt = (Double) myResult[3];
				Byte status = (Byte)  myResult[4];
				String user = (String)  myResult[5];
				Integer AccSubGrpCode = (Integer)  myResult[6];
				String season = (String)  myResult[7];
				
				String TransType = "Dr";
				if(CrAmt>0)
				{
					TransType="Cr";
				}
				else
				{
					TransType="Dr";
				}
				//Now Get Smry Rec
				List SubGrpBalList = SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
				if (SubGrpBalList != null && SubGrpBalList.size() > 0)
				{
					Object[] myResult1 = (Object[]) SubGrpBalList.get(0);
					Double RunnBal = (Double) myResult1[1];
					String BalType = (String) myResult1[0];
					logger.info("========RunnBal=========="+RunnBal);
					logger.info("========BalType=========="+BalType);
					
					if (BalType.equals(TransType))
					{
						if (RunnBal > UpdatingAmt)
						{
							finalAmt = RunnBal-UpdatingAmt;
							strCrDr = BalType;
						}
						else if (RunnBal < UpdatingAmt)
						{
							finalAmt = UpdatingAmt - RunnBal;
							if ("Cr".equals(BalType))
							{
								strCrDr = "Dr";
							}
							else
							{
								strCrDr = "Cr";
							}	
						}
						else
						{
							finalAmt = 0.0;
							strCrDr = "Dr";
						}
					}
					else
					{
						finalAmt = RunnBal + UpdatingAmt;
						strCrDr = BalType;
					}
				}
				//Here we need to update finalAmt and strCrDr in Acc Smry
				updateRunningBalinAccSubGrpSmry(finalAmt,strCrDr,AccGrpCode,AccSubGrpCode,season);
	    		//Update that transaction status to invalid i.e 0 in account details table.
	    		updateStatusForAccSubGrpDtls(TransactionCode,season);
			}
		}
		
		//AccGrpSmry
		 List AccGrpDtlsList = AccGrpBalDetailsForRevert(TransactionCode);
		 if (AccGrpDtlsList != null && AccGrpDtlsList.size() > 0)
		 {
			for (Iterator it = AccGrpDtlsList.iterator(); it.hasNext();)
			{
				jsonObj = new JSONObject();
				Object[] myResult = (Object[]) it.next();
											
				Integer AccGrpCode = (Integer)  myResult[0];
				Double CrAmt = (Double) myResult[1];
				Double DrAmt = (Double) myResult[2];
				Double UpdatingAmt = (Double) myResult[3];
				Byte status = (Byte)  myResult[4];
				String user = (String)  myResult[5];
				String season = (String)  myResult[6];
				
				String TransType = "Dr";
				if(CrAmt>0)
				{
					TransType="Cr";
				}
				else
				{
					TransType="Dr";
				}
				
				List AccGrpBalList = AccGrpBalDetails(AccGrpCode,season);
				if (AccGrpBalList != null && AccGrpBalList.size() > 0)
				{
					Object[] myResult1 = (Object[]) AccGrpBalList.get(0);
					Double RunnBal = (Double) myResult1[1];
					String BalType = (String) myResult1[0];
					logger.info("========RunnBal=========="+RunnBal);
					logger.info("========BalType=========="+BalType);
					
					if (BalType.equals(TransType))
					{
						if (RunnBal > UpdatingAmt)
						{
							finalAmt = RunnBal-UpdatingAmt;
							strCrDr = BalType;
						}
						else if (RunnBal < UpdatingAmt)
						{
							finalAmt = UpdatingAmt - RunnBal;
							if ("Cr".equals(BalType))
							{
								strCrDr = "Dr";
							}
							else
							{
								strCrDr = "Cr";
							}	
						}
						else
						{
							finalAmt = 0.0;
							strCrDr = "Dr";
						}
					}
					else
					{
						finalAmt = RunnBal + UpdatingAmt;
						strCrDr = BalType;
					}
				}
				//Here we need to update finalAmt and strCrDr in Acc Smry
				updateRunningBalinAccGrpSmry(finalAmt,strCrDr,AccGrpCode,season);
	    		
	    		//Update that transaction status to invalid i.e 0 in account details table.
	    		updateStatusForAccGrpDtls(TransactionCode,season);	
			}
		 }
	}
	
	public Integer GetMaxTransCode(String season)
    {
		//Modified by DMurty on 18-10-2016
		Integer maxVal=(Integer) commonDao.GetMaxTransCode("select max(transactioncode) from AccountDetails where season='"+season+"'");
		if(maxVal==null)
		{
			maxVal=1;
		}
		else
		{
			maxVal = maxVal+1;
		}
		return maxVal;
    }
	
	public Integer getMaxColumnValue(String tableName,String columnName1,String columnName2,String value) throws Exception
    {
		try
		{
			Integer maxVal=(Integer) commonDao.getMaxColumnValue("select max("+columnName1+") from "+tableName+" where "+columnName2+"='"+value+"'");
			if(maxVal==null)
				maxVal=0;
			maxVal = maxVal + 1;
	        return maxVal;
		}
		catch(Exception e)
		{
			 logger.info(e.getCause(), e);
	         return null;
		}
    }

	//saveMultipleEntities
	public boolean saveMultipleEntities(List entitiesList)
	{
		return commonDao.saveMultipleEntities(entitiesList);
	}
	
	public boolean saveMultipleEntitiesForFunctionalScreens(List entitiesList,List UpdateList)
	{
		return commonDao.saveMultipleEntitiesForFunctionalScreens(entitiesList,UpdateList);
	}
	
	public boolean saveMultipleEntitiesForScreensUpdateFirst(List entitiesList,List UpdateList)
	{
		return commonDao.saveMultipleEntitiesForScreensUpdateFirst(entitiesList,UpdateList);
	}
	
	public boolean saveMultipleEntitiesForAgreement(List entitiesList,List UpdateList,String modifyFlag)
	{
		return commonDao.saveMultipleEntitiesForAgreement(entitiesList,UpdateList,modifyFlag);
	}
	
	
	public JSONArray getLoanNumberFromLoanDetails(String tableName,String columnName,String whereColumnname,String value,String whereColumnname1,String value1)
    {	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;

		strQuery="Select "+ columnName+" from "+ tableName+" where "+ whereColumnname +"='"+value+"' and "+ whereColumnname1 +"='"+value1+"'";
		List dropdownList=hibernateDao.findBySqlCriteria(strQuery);
		try 
		{
			jsonObj = new JSONObject();
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				for (int i=0;i<dropdownList.size();i++)
				{
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					
					Integer loannumber = (Integer) TempMap.get("loannumber");
					
					
					jsonObj.put("loanname", loannumber);		
					jsonObj.put("loannumber", loannumber);	
					jArray.add(jsonObj);
				}
			}
			return jArray;
		} 
		catch (Exception e) 
		{
		      logger.info(e.getCause(), e);
		     return null;
		}
	}
	
	public List getLoanRyotCode(String season,Integer branchcode)
	{
		return commonDao.getLoanRyotCode(season,branchcode);
	}
	
	public List getAccountNumber(String ryotcode,String season)
	{
		return commonDao.getAccountNumber(ryotcode,season);
	}
	
	public String getVillage(String colname)
    {
		String village=(String) commonDao.getMaxValue("select village from Village where villagecode='"+colname+"'");
		if(village==null)
			village="";
		
        return village;
    }
	public List getMandal(Integer mandalcode)
	{
		return commonDao.getMandal(mandalcode);
	}

	public boolean checkUser(String username)
    {	
		logger.info("========ValidateUser()=========="+username);
		boolean flag = false;
		String strQuery=null;
		strQuery="Select * from Employees where loginid='"+username+"'";
		logger.info("========strQuery=========="+strQuery);

		List dropdownList=commonDao.ValidateRyotCode(strQuery);
		logger.info("========dropdownList=========="+dropdownList);
		if(dropdownList != null && dropdownList.size()>0)
		{
			 flag = true;
		}
		logger.info("========flag=========="+flag);

		return flag;
    }	
	
	public List getSearchData(String ryotcode,String agreementno,String village,int permitNo)
	{
		return commonDao.getSearchData(ryotcode,agreementno,village,permitNo);
	}
	
	public List getRyotData(String season)
	{
		return commonDao.getRyotData(season);
	}
	
	public List getRyotForCaneLedgerReport(String season)
	{
		return commonDao.getRyotForCaneLedgerReport(season);
	}
	
	public List getRyotAgreementData(String season,String ryotcode)
	{
		return commonDao.getRyotAgreementData(season,ryotcode);
	}
	public List getPrintCaneWeighment(Integer serailNumber,String season)
	{
		return commonDao.getPrintCaneWeighment(serailNumber,season);
	}
	//Added by sahadeva on 08-08-2016
	public List getEmployeeData(String loginid)
	{
		return commonDao.getEmployeeData(loginid);
	}
	
	public List getPermitRulesNew()
    {        
        return commonDao.getPermitRulesNew();
    }
	public List getVariety(String season,String ryotCode)
    {        
        return commonDao.getVariety(season,ryotCode);
    }
	public List<WeighmentDetails> getDataForCaneLedger(String season,String ryotCode)
    {        
        return commonDao.getDataForCaneLedger(season,ryotCode);
    }
	
	public List getloanDetailsData(String season,String ryotCode)
    {        
        return commonDao.getloanDetailsData(season,ryotCode);
    }
	
	public List getAccountDetailsData(String season,String accCode)
    {        
        return commonDao.getAccountDetailsData(season,accCode);
    }
	public List getAccountGroupData(String season,Integer accGroupCode)
    {        
        return commonDao.getAccountGroupData(season,accGroupCode);
    }
	public List getAccSubGroupData(String season,Integer accSubGroupCode)
    {        
        return commonDao.getAccSubGroupData(season,accSubGroupCode);
    }
	
	public List getloanSummaryData(String season,String ryotCode)
    {        
        return commonDao.getloanSummaryData(season,ryotCode);
    }
	
	public List getAgreementdetailsData(String season,String ryotCode)
    {        
        return commonDao.getAgreementdetailsData(season,ryotCode);
    }
	
	//Added by Sahadeva on 12-08-2016
	public Double getSumOfAdvanceAmount(String season,String ryotCode,Integer advanceCode)
    {	
		logger.info("========getSumOfAdvanceAmount()==========");
		String strQuery=null;
		Double sumAmount=0.0;
		Double mAmount=0.0;
		
		strQuery=" select sum(advanceamount) as sum from AdvanceDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and advancecode="+advanceCode;
		logger.info("getSumOfAdvanceAmount()========strQuery=========="+strQuery);		
		List dropdownList=commonDao.getDropdownNamesForPermitNum(strQuery);
		
		String strQuery1=" select maximumamount from CompanyAdvance where advancecode="+advanceCode;
		logger.info("getSumOfAdvanceAmount()========strQuery1=========="+strQuery1);		
		List dropdownList1=commonDao.getDropdownNamesForPermitNum(strQuery1);
		try 
		{
			if(dropdownList != null && dropdownList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList.get(0);
				sumAmount = (Double) tempMap.get("sum");
			}
			if(dropdownList1 != null && dropdownList1.size()>0)
			{
				Map tempMap1 = new HashMap();
				tempMap1 = (HashMap) dropdownList1.get(0);
				mAmount=(Double) tempMap1.get("maximumamount");
			}
			if(sumAmount==null)
			{
				return mAmount;
			}
			else 
			return mAmount-sumAmount;
			
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
		    return sumAmount;
		}
	}
	//Added by DMurty on 26-10-2016
	public boolean saveMultipleEntitiesForFunctionalScreensForAccounting(List entitiesList,List UpdateList,List AccountsList)
	{
		return commonDao.saveMultipleEntitiesForFunctionalScreensForAccounting(entitiesList,UpdateList,AccountsList);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSmryTemp(AccountSummaryTemp accountSummaryTemp) 
	{
		commonDao.addAccountSmryTemp(accountSummaryTemp);
	}
	
	public Integer GetMaxRank(String season,Integer program)
    {
		Integer maxVal=(Integer) commonDao.GetMaxRank("select max(rank) from PermitDetails where season='"+season+"' and programno ="+program+"");
		if(maxVal==null)
		{
			maxVal=1;
		}
		else
		{
			maxVal = maxVal+1;
		}
		return maxVal;
    }
	
	public List listGetReceiptDetails(String frmdate,String todate,Integer Shift,int lorryStatus)
	{
			return commonDao.listGetReceiptDetails(frmdate,todate,Shift,lorryStatus);
	}
	
	public int getMaxSlNoForWeighment(String shiftdate,String season)
    {
		int maxVal = commonDao.getMaxSlNoForWeighment(shiftdate, season);
		logger.info("========getMaxSlNoForWeighment()========== maxVal--"+maxVal);
        return maxVal;
    }
	

	public List getRyotFromCircle(Integer circlecode)
	    {   
		logger.info("========getRyotFromCircle()========== circlecode--"+circlecode);
	        return commonDao.getRyotFromCircle(circlecode);
	    }
	
	public List listGetCirclesFromZones(Integer zonecode)
    {      
		logger.info("========listGetCirclesFromZones()========== zonecode--"+zonecode);
        return commonDao.listGetCirclesFromZones(zonecode);
    }
	
	//Added by Umanath Ch on 14-10-2016 
		public JSONArray GetMaxSourceOfSeed(String seedType,String season)
	    {	
			logger.info("========GetMaxSourceOfSeed()==========");
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			String strQuery=null;
			String type="O";
			if("0".equals(seedType))
			{
				type="S";
			}
			else if("1".equals(seedType))
			{
				type="V";
			}
			else if("2".equals(seedType))
			{
				type="F";
			}
			/*Date currentDate = new Date();
			String todayDate= new SimpleDateFormat("yyyy-MM-dd").format(currentDate);*/
			String finyear[] = season.split("-");
			String firstyr = finyear[0];
			firstyr = firstyr.substring(1);
			firstyr = firstyr.substring(1);
			String secondyr = finyear[1];
			secondyr = secondyr.substring(1);
			secondyr = secondyr.substring(1);
			
			strQuery="Select max(seqno) as maxno from batchservicesmaster where seedtype='"+type+"' and season='"+season+"'";
			List dropdownList=commonDao.getMaxRyotCode(strQuery);
			try 
			{
				int seedseqno =0;
				if(dropdownList != null && dropdownList.size()>0)
				{
					if (!dropdownList.contains(null)) // oldDate.equals(returndate.trim()))
					{
						seedseqno = (Integer) dropdownList.get(0);
					}
					else
					{
						seedseqno = 0;
					}
					jsonObj = new JSONObject();
					seedseqno++;
					String strseedseqcount = Integer.toString(seedseqno);
					if(strseedseqcount.length() == 2)
					{
						strseedseqcount = "0"+strseedseqcount;
					}
					else if(strseedseqcount.length() == 1)
					{
						strseedseqcount = "00"+strseedseqcount;
					}
					else 
					{
						strseedseqcount = strseedseqcount;
					}
					
					
					strseedseqcount = type+firstyr+secondyr+strseedseqcount;
					jsonObj.put("batchNo", strseedseqcount);
					jArray.add(jsonObj);
				}
				return jArray;
			} 
			catch (Exception e)
			{
	            logger.info(e.getCause(), e);
	            return null;
			}
		}
		public String GetMaxAuthoriseFertiliser(String season)
	    {	
			logger.info("========GetMaxAuthoriseFertiliser()==========");
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			String strQuery=null;
			String type="AF";
			String idtype="";
			//strQuery="Select max(id) as maxno from AuthorisationFormSummary where season='"+season+"'";
			strQuery="Select * from AuthorisationFormSummary where season='"+season+"'";
			List dropdownList=commonDao.getMaxRyotCode(strQuery);
			try 
			{
				
				int id =0;
				if(dropdownList != null && dropdownList.size()>0)
				{
					id=dropdownList.size()+1;
				}
				else
				{
					id=1;
				}
				idtype = type+"-"+Integer.toString(id);
				
				return idtype;
			} 
			catch (Exception e)
			{
	            logger.info(e.getCause(), e);
	            return null;
			}
		}
		
		//Added by Umanath Ch on 8-11-2016 
				public JSONArray GetMaxKindIndent(String season,Integer zone)
			    {	
					logger.info("========GetMaxKindIndent()==========");
					JSONArray jArray = new JSONArray();
					JSONObject jsonObj =null;
					String strQuery=null;
					
					/*Date currentDate = new Date();
					String todayDate= new SimpleDateFormat("yyyy-MM-dd").format(currentDate);*/
					String PresentYear[] = season.split("-");
						
					strQuery="Select max(seqno) as maxno from KindIndentSummary where season='"+season+"' and zone='"+zone+"' ";
					List dropdownList=commonDao.getMaxRyotCode(strQuery);
					try 
					{
						int kindSeqnoseqno =0;
						if(dropdownList != null && dropdownList.size()>0)
						{
							if (!dropdownList.contains(null)) // oldDate.equals(returndate.trim()))
							{
								kindSeqnoseqno = (Integer) dropdownList.get(0);
							}
							else
							{
								kindSeqnoseqno = 0;
							}
							jsonObj = new JSONObject();
							kindSeqnoseqno++;
							
							String strkindSeqnoseqno = Integer.toString(kindSeqnoseqno);
							
							String zoneno="";
							int length = String.valueOf(zone).length();
							if(length==1)
							{
								zoneno="0"+zone;
							}
							else
							{
								zoneno=Integer.toString(zone);
							}
							if(strkindSeqnoseqno.length() == 4)
							{
								strkindSeqnoseqno = zoneno+strkindSeqnoseqno;
							}
							else if(strkindSeqnoseqno.length() == 3)
							{
								strkindSeqnoseqno = zoneno+strkindSeqnoseqno;
							}
							else if(strkindSeqnoseqno.length() == 2)
							{
								strkindSeqnoseqno = zoneno+"0"+strkindSeqnoseqno;
							}
							else if(strkindSeqnoseqno.length() == 1)
							{
								strkindSeqnoseqno = zoneno+"00"+strkindSeqnoseqno;
							}
							else 
							{
								strkindSeqnoseqno = zoneno+strkindSeqnoseqno;
							}
							
							String year = PresentYear[0];
							String year1 = PresentYear[1];
							year = year.substring(1);
							year = year.substring(1);
							year1 = year1.substring(1);
							year1 = year1.substring(1);
							strkindSeqnoseqno = "KI"+strkindSeqnoseqno+"/"+year+"-"+year1;
							jsonObj.put("indentNo", strkindSeqnoseqno);
							jArray.add(jsonObj);
						}
						return jArray;
					} 
					catch (Exception e)
					{
			            logger.info(e.getCause(), e);
			            return null;
					}
				}
				public JSONArray GetMaxDispatchNo(String season)
			    {	
					logger.info("========GetMaxDispatchNo()==========");
					JSONArray jArray = new JSONArray();
					JSONObject jsonObj =null;
					String strQuery=null;
					
					/*Date currentDate = new Date();
					String todayDate= new SimpleDateFormat("yyyy-MM-dd").format(currentDate);*/
					String PresentYear[] = season.split("-");
					
					strQuery="Select max(dispatchSeq) as maxno from DispatchSummary where season='"+season+"'";
					List dropdownList=commonDao.getMaxRyotCode(strQuery);
					try 
					{
						int Dispatchseqno =0;
						if(dropdownList != null && dropdownList.size()>0)
						{
							if (!dropdownList.contains(null)) // oldDate.equals(returndate.trim()))
							{
								Dispatchseqno = (Integer) dropdownList.get(0);
							}
							else
							{
								Dispatchseqno = 0;
							}
							jsonObj = new JSONObject();
							Dispatchseqno++;
							String strDispseqno = Integer.toString(Dispatchseqno);
							
							if(strDispseqno.length() == 4)
							{
								strDispseqno = "0"+strDispseqno;
							}
							else if(strDispseqno.length() == 3)
							{
								strDispseqno = "00"+strDispseqno;
							}
							else if(strDispseqno.length() == 2)
							{
								strDispseqno = "000"+strDispseqno;
							}
							else if(strDispseqno.length() == 1)
							{
								strDispseqno = "0000"+strDispseqno;
							}
							else 
							{
								strDispseqno = strDispseqno;
							}
							
							String year = PresentYear[0];
							String year1 = PresentYear[1];
							year = year.substring(1);
							year = year.substring(1);
							year1 = year1.substring(1);
							year1 = year1.substring(1);
							strDispseqno = "SD"+strDispseqno+"/"+year+"-"+year1;
							jsonObj.put("DispatchNo", strDispseqno);
							jArray.add(jsonObj);
						}
						return jArray;
					} 
					catch (Exception e)
					{
			            logger.info(e.getCause(), e);
			            return null;
					}
				}
				
				public boolean SendSMSForRyot(String ryotPhoneNumber,String indentNumbers,String Dateofindent,String variety,Double issuedQty,String deliveryDate,String vehicalno) throws Exception
				{
					boolean bRet = false;
					String mobilenumber = ryotPhoneNumber;

					int strcode = 0;
					int reportcode = 0;
					List resultList = new ArrayList();
					List subdiagnosis = new ArrayList();
					try
					{
							List patName = new ArrayList();
							List pidList = new ArrayList();
							String patientname = "";
							
							String strUserName = "";
							String User = "";//"sms4finsol";
							String passwd = "";//"41490561";
							String sid = "";//FINSOL
							String mtype = "LNG";
							String DR = "Y";
							int pid=0;
						
							if (!("NA".equals(mobilenumber)) &&!("".equals(mobilenumber))&& !(mobilenumber == null) && !("null".equals(mobilenumber)))
							{
								String postData = "";
								String retval = "";
								String message = "";
								 
								message = "మీ ధరఖాస్తు నెం: " +indentNumbers+" తేదీ "+Dateofindent+" సంభందించి  "+variety+" వెరైటీ "+issuedQty+" చెరుకు నారు మొక్కలను "+deliveryDate+" తేదీన "+vehicalno+" నెంబరు గల లారీ ద్వారా పంపించడం జరిగింది."; 	
								//"We have today dispatched 10000 seedlings of 86V76 variety as per your indent No:KI000001 dated 03-1-2017 " ;
								 User = "sarvar";//"sarvar";
								 passwd = "sarvar";//"sarvar";
								 sid = "SSSLTD";//SSSLTD
								 mtype = "LNG";
								 DR = "Y";
							
							
								if(!("".equals(message)))
									{
									postData += "User=" + URLEncoder.encode(User, "UTF-8") + "&passwd=" + passwd + "&mobilenumber="
									        + mobilenumber + "&message=" + URLEncoder.encode(message, "UTF-8") + "&sid=" + sid + "&mtype="
									        + mtype + "&DR=" + DR;
									URL url = new URL("http://www.sms4finsol.org/WebserviceSMS.aspX");
									//URL url = new URL("http://smscountry.com/SMSCwebservice.asp");
									HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
				
									// If You Are Behind The Proxy Server Set IP And PORT else Comment Below 4 Lines
									//Properties sysProps = System.getProperties();
									//sysProps.put("proxySet", "true");
									//sysProps.put("proxyHost", "Proxy Ip");
									//sysProps.put("proxyPort", "PORT");
				
									urlconnection.setRequestMethod("POST");
									urlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
									urlconnection.setDoOutput(true);
									OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
									out.write(postData);
									out.close();
									BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));
									String decodedString;
									while ((decodedString = in.readLine()) != null)
									{
										retval += decodedString;
									}
									in.close();
				
									System.out.println(retval);
									
									if ("Messages Sent Successfully".equals(retval))
									{
										bRet = true;
									}
								}
							}
					}
					catch (Exception e)
					{
			            logger.info(e.getCause(), e);
						throw e;
					}
					return bRet;
				}

				//Added by DMurty on 02-12-2016
	public List getEmployeeDetails(String loginUser)
    {
        return commonDao.getEmployeeDetails(loginUser);
    }
	public List getPermits(String season,String ryotCode)
	{
			return commonDao.getPermits(season,ryotCode);
	}
	public List getDateByCaneslno(String season, Integer caneSlNo)
    {      
        return commonDao.getDateByCaneslno(season,caneSlNo);
    }
	public List getLoanSummaryData(String season,String ryotcode)
    {        
        return commonDao.getLoanSummaryData(season,ryotcode);
    }
	public List getCaneAccLoanData(String season,String ryotcode)
    {        
        return commonDao.getCaneAccLoanData(season,ryotcode);
    }
	public List getCaneAccLoanData1(String season,String ryotcode,Integer caneNo)
    {        
        return commonDao.getCaneAccLoanData1(season,ryotcode,caneNo);
    }
	public List getCaneAccLoanData2(String season,String ryotcode,Integer caneNo)
    {        
        return commonDao.getCaneAccLoanData2(season,ryotcode,caneNo);
    }
	//added by naidu on 13-02-2017
	public Integer getBatchForLoanRecommendedBranch(String season,int branchcode,String loanrecdate)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select batchno from LoanDetails where season='"+season+"' and branchcode="+branchcode+" and recommendeddate='"+DateUtils.getSqlDateFromString(loanrecdate,Constants.GenericDateFormat.DATE_FORMAT)+"'");
		if(maxVal==null)
			maxVal=0;	
        return maxVal;
    }
	//added by naidu on 13-02-2017
	public Integer getNewBatchForLoanrecommeddedBranch(String season,int branchcode)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max(batchno) from LoanDetails where season='"+season+"' and branchcode="+branchcode+"");
		if(maxVal==null)
			maxVal=1;
		else
			maxVal=maxVal+1;
        return maxVal;
    }
	
	//added by naidu on 13-02-2017
		public JSONArray getBatchnoForBranch(String season,int branchcode)
	    {	
			logger.info("========getBatchnoForBranch()==========");
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			String strQuery=null;
			strQuery="Select distinct batchno from LoanDetails where season='"+season+"' and branchcode="+branchcode+"";
			List dropdownList=commonDao.getDropdownNamesForVillage(strQuery);
			try 
			{
				if(dropdownList != null && dropdownList.size()>0 && !dropdownList.isEmpty()) 
				{
					for (int i=0;i<dropdownList.size();i++)
					{
						Map tempMap = new HashMap();
						tempMap = (Map) dropdownList.get(i);
						
						jsonObj = new JSONObject();
						Integer batchno = (Integer) tempMap.get("batchno");
						if(batchno == null)
						{
							batchno =0;
						}
						jsonObj.put("batchno", batchno);
						jArray.add(jsonObj);
					}
				}
				return jArray;
			} 
			catch (Exception e)
			{
	            logger.info(e.getCause(), e);
	            return null;
			}
		}

		public Integer getAdvcodeForkind(Integer kindid)
	    {
			Integer advcode=(Integer) commonDao.getMaxValue("select advance from KindType where id="+kindid+"");
	        return advcode;
	    }
		public List getAdvPrncAmt(String season )
	    {        
	        return commonDao.getAdvPrncAmt(season);
	    }
		public List GetAdvanceAmt(String season,String ryotCode,Integer advCode ) 
	    {        
	        return commonDao.GetAdvanceAmt(season,ryotCode,advCode);
	    }
		public List getLoanPrncData(String season )
	    {        
	        return commonDao.getLoanPrncData(season);
	    }
	
		public List getDateByCaneslnoForUpdation(String season, Integer caneSlNo)
		{
			return commonDao.getDateByCaneslnoForUpdation(season,caneSlNo);
		}
		public List getLoanSummaryDataForYearlyLedger(String season,String ryotcode)
	    {
			return commonDao.getLoanSummaryDataForYearlyLedger(season,ryotcode);
	    }
	        public List getZonesListByZone(int zonecode)
	    	{
	        	return commonDao.getZonesListByZone(zonecode);
	    	}
	        public String getVarietyName(int varietycode) {		
	    		
	    		String variety=(String) commonDao.getCaneManagerID("select variety  from Variety where varietycode="+varietycode);
	    		return variety;
	    	}
	        public String saveMultipleEntitiesForSeedSource(List entitiesList,List UpdateList,String modifyFlag)
	    	{
	        	return saveMultipleEntitiesForSeedSource(entitiesList,UpdateList,modifyFlag);
	    	}
	 
}
