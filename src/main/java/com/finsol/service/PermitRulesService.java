package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.PermitRulesDao;
import com.finsol.model.LandType;
import com.finsol.model.PermitRules;
/**
 * @author Rama Krishna
 *
 */
@Service("PermitRulesService")
public class PermitRulesService {
	
	private static final Logger logger = Logger.getLogger(PermitRulesService.class);
	
	@Autowired
	private PermitRulesDao permitRulesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addPermitRules(PermitRules permitRules) {
		permitRulesDao.addPermitRules(permitRules);
	}

	public List<PermitRules> listPermitRules() {
		return permitRulesDao.listPermitRules();
	}
}
