package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.ScreensDao;
import com.finsol.model.Screens;

/**
 * @author SahaDeva
 */
@Service("ScreensService")


public class ScreensService 
{
private static final Logger logger = Logger.getLogger(ScreensService.class);
	
	@Autowired
	private ScreensDao screensDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addScreens(Screens screens)
	{
		screensDao.addScreens(screens);
	}
	public List<Screens> listScreen() {
		return screensDao.listScreen();
	}
	
	public List<Screens> getScreensByCode(Integer screencode)
    {
        return screensDao.getScreensByCode(screencode);
    }
	

}
