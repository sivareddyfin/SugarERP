package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.VarietyDao;
import com.finsol.dao.ZoneDao;
import com.finsol.model.Variety;
import com.finsol.model.Zone;

/**
 * @author Rama Krishna
 *
 */
@Service("ZoneService")
public class ZoneService {
	private static final Logger logger = Logger.getLogger(VarietyService.class);

	@Autowired
	private ZoneDao zoneDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addZone(Zone zone) {
		zoneDao.addZone(zone);
	}
	
	public List<Zone> listZones() {
		return zoneDao.listZones();
	}
	
	public List getRegionNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, region from Region";
		return zoneDao.getRegionNames(query);
	}
}
