package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import com.finsol.dao.UnloadingContractorDao;
import com.finsol.model.UnloadingContractor;



/**
 * @author SahaDeva
 */
@Service("UnloadingContractorService")


public class UnloadingContractorService 
{
private static final Logger logger = Logger.getLogger(UnloadingContractorService.class);
	
	@Autowired
	private UnloadingContractorDao unloadingContractorDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addUnloadingContractor(UnloadingContractor unloadingContractor)
	{
		unloadingContractorDao.addUnloadingContractor(unloadingContractor);
	}
	public List<UnloadingContractor> listUnloadingContractors() {
		return unloadingContractorDao.listUnloadingContractors();
	}
	
}
