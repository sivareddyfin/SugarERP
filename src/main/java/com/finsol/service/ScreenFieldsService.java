package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.ScreenFieldsDao;
import com.finsol.model.ScreenFields;
import com.finsol.model.Screens;
import com.finsol.model.SetupDetails;

/**
 * @author Sahadeva
 */
@Service("ScreenFieldsService")

public class ScreenFieldsService 
{
	private static final Logger logger = Logger.getLogger(ScreenFieldsDao.class);

	@Autowired
	private ScreenFieldsDao screenFieldsDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addScreenFields(ScreenFields screenFields)
	{
		screenFieldsDao.addScreenFields(screenFields);
	}
	public List<ScreenFields> listScreenField() 
	{
		return screenFieldsDao.listScreenField();
	}
	
	
	public List<ScreenFields> getScreenFieldsByCode(Integer screencode)
    {
        return screenFieldsDao.getScreenFieldsByCode(screencode);
    }
	
	public void deleteScreenFields(Integer screenid) 
	{
		screenFieldsDao.deleteScreenFields(screenid);
	}

}
