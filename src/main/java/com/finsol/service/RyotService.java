package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.BankDao;
import com.finsol.dao.RyotDao;
import com.finsol.model.AccountMaster;
import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;


/**
 * @author Rama Krishna
 *
 */
@Service("RyotService")
public class RyotService {
	
	
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private RyotDao ryotDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRyot(Ryot ryot) {
		ryotDao.addRyot(ryot);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRyotBankDetails(RyotBankDetails ryotBankDetails) {
		ryotDao.addRyotBankDetails(ryotBankDetails);
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountMaster(AccountMaster accountMaster) {
		ryotDao.addAccountMaster(accountMaster);
	}
/*	public void deleteBranch(Branch branch) {
		bankDao.deleteBranch(branch);
	}*/
	
	
	public List<Ryot> listRyots() {
		return ryotDao.listRyots();
	}
	public List<RyotBankDetails> listRyotBankDetails() {
		return ryotDao.listRyotBankDetails();
	}
	
	public Ryot getRyot(String id) {
		return ryotDao.getRyot(id).get(0);
	}
	
	public List<RyotBankDetails> getBranchByRyotCode(String ryotcode)
    {
        return ryotDao.getBranchByRyotCode(ryotcode);
    }

	public void deleteRyotBankDetails(Ryot ryot) 
	{
		ryotDao.deleteRyotBankDetails(ryot);
	}
	
	public List<Ryot> getRyotByRyotCode(String ryotcode)
    {
        return ryotDao.getRyotByRyotCode(ryotcode);
    }
	
	public List<Ryot> getRyotByRyotCodeNew(String ryotcode)
    {
        return ryotDao.getRyotByRyotCodeNew(ryotcode);
    }

}
