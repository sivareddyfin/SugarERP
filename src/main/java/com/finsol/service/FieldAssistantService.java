package com.finsol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.FieldAssistantDao;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;

/**
 * @author Rama Krishna
 *
 */
@Service("FieldAssistantService")
public class FieldAssistantService {
	
	
	@Autowired
	private FieldAssistantDao fieldAssistantDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addFieldAssistant(FieldAssistant fieldAssistant) {
		fieldAssistantDao.addFieldAssistant(fieldAssistant);
	}
	
	public Integer getMaxID()
    {
		Integer maxVal=(Integer) fieldAssistantDao.getMaxValue("select max(id) from FieldAssistant");
		if(maxVal==null)
			maxVal=0;
        return maxVal;
    }
	
	
	public List<FieldAssistant> listFieldAssistants() {
		return fieldAssistantDao.listFieldAssistants();
	}

}
