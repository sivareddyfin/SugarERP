package com.finsol.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.MandalDao;
import com.finsol.model.CaneManager;
import com.finsol.model.Mandal;

/**
 * @author DMurty
 */

@Service("MandalService")
public class MandalService 
{
	private static final Logger logger = Logger.getLogger(MandalService.class);

	@Autowired
	private MandalDao mandalDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addMandal(Mandal mandal) 
	{
		mandalDao.addMandal(mandal);
	}
	
	public List<Mandal> listMandals() {
		return mandalDao.listAllMandals();
	}
	
	public List getZoneNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, zone from Zone";
		return mandalDao.getZoneNames(query);
	}

	
	
	
	
	
	
}
