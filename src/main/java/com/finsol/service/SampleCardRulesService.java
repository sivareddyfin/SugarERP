package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.SampleCardRulesDao;
import com.finsol.model.Bank;
import com.finsol.model.CaneManager;
import com.finsol.model.SampleCardRules;

/**
 * @author Rama Krishna
 *
 */
@Service("SampleCardRulesService")
public class SampleCardRulesService {
	
	private static final Logger logger = Logger.getLogger(SampleCardRulesService.class);
	
	@Autowired
	private SampleCardRulesDao sampleCardRulesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSampleCardRules(SampleCardRules sampleCardRules) {
		sampleCardRulesDao.addSampleCardRules(sampleCardRules);
	}
	
	public List<SampleCardRules> listSampleCardRules() {
		return sampleCardRulesDao.listSampleCardRules();
	}
	
	
	public void deleteAllSampleCardRules() {
		sampleCardRulesDao.truncateSampleCardRules();
	}

}
