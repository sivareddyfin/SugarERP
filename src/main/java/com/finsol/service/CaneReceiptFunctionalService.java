package com.finsol.service;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finsol.dao.CaneReceiptFunctionalDao;
import com.finsol.dao.CommonDao;
import com.finsol.dao.HibernateDao;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author Rama Krishna
 *
 */
@Service("CaneReceiptFunctionalService")
public class CaneReceiptFunctionalService {
	
	private static final Logger logger = Logger.getLogger(CaneReceiptFunctionalService.class);
	
	@Autowired	
	private CaneReceiptFunctionalDao caneReceiptFunctionalDao;
	
	@Autowired	
	private CommonDao commonDao;
	
	@Autowired
	private HibernateDao hibernateDao;

	
	public boolean saveAllCaneWeighment(List caneWeighmentTables)
	{
		return caneReceiptFunctionalDao.saveAllCaneWeighment(caneWeighmentTables);
	}
	
	
	public List getCaneReceiptShiftSmry(String canedate,Integer shift)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptShiftSmry(canedate,shift);
    }
	public List getShifts()
    {		
        return caneReceiptFunctionalDao.getShifts();
    }
	
	
	public List getCaneReceiptDaySmryByDate(String canedate)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptDaySmryByDate(canedate);
    }
	
	public List getCaneReceiptDaySmryByRyot(String ryotcode,String finalDate)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptDaySmryByRyot(ryotcode,finalDate);
    }
	public List getCaneReceiptSeasonSmry(String season)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptSeasonSmry(season);
    }
	public List getCaneReceiptWBBook(Integer weighbridgeid)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptWBBook(weighbridgeid);
    }
	
	public List getAdditionalCaneValue(String season,String ryotcode)
    {		
        return caneReceiptFunctionalDao.getAdditionalCaneValue(season,ryotcode);
    }
	
	public List<CaneAccountingRules> getTransportAllowanceDetailsFromCAR(String season)
    {		
        return caneReceiptFunctionalDao.getTransportAllowanceDetailsFromCAR(season);
    }
	
	public List<CaneAcctRules> getTransprtAlwnceFromCaneAcctRules(String season,String finalDate)
    {		
        return caneReceiptFunctionalDao.getTransprtAlwnceFromCaneAcctRules(season,finalDate);
    }

	//Added by DMurty on 12-07-2016
	public List<CaneAcctAmounts> getCaneAcctAmounts(String season,int calcid,String pricename)
    {		
        return caneReceiptFunctionalDao.getCaneAcctAmounts(season,calcid,pricename);
    }

	public List getTransportAllowanceDetailsFromCARList(String season)
    {		
        return caneReceiptFunctionalDao.getTransportAllowanceDetailsFromCARList(season);
    }
	
	public List getTransportAllowanceDetails(String season,String ryotCode,String plotNo,String finalDate)
    {		
        return caneReceiptFunctionalDao.getTransportAllowanceDetails(season,ryotCode,plotNo,finalDate);
    }
	
	public List getPostSeasonValueSummary(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getPostSeasonValueSummary(season,ryotCode);
    }
	
	public List getHarvestingRateDetails(String season,String ryotCode,String finalDate)
    {		
        return caneReceiptFunctionalDao.getHarvestingRateDetails(season,ryotCode,finalDate);
    }
	
	public List getHarvChgDtlsByRyotContAndDate(String season,String ryotCode,double harvestingRate,String finalDate)
    {		
        return caneReceiptFunctionalDao.getHarvChgDtlsByRyotContAndDate(season,ryotCode,harvestingRate,finalDate);
    }
	
	public List CaneValueDetailsByRyotAndDateList(String season,String ryotCode,String finalDate)
    {		
        return caneReceiptFunctionalDao.CaneValueDetailsByRyotAndDateList(season,ryotCode,finalDate);
    }
	
	public List getCaneValDtlsByRyotDateList(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getCaneValDtlsByRyotDateList(season,ryotCode);
    }
	
	public List getHarvChgSmryByRyotAndCont(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getHarvChgSmryByRyotAndCont(season,ryotCode);
    }
	
	public List getHarvChgSmryByContAndDate(String season)
    {		
        return caneReceiptFunctionalDao.getHarvChgSmryByContAndDate(season);
    }
	
	public List getHarvChgSmryBySeasonByCont(String season)
    {		
        return caneReceiptFunctionalDao.getHarvChgSmryBySeasonByCont(season);
    }
	
	public List getHarvChgSmryBySeason(String season)
    {		
        return caneReceiptFunctionalDao.getHarvChgSmryBySeason(season);
    }
	
	public List getTransportingRateDetails(String season,String ryotCode,String finalDate)
    {		
        return caneReceiptFunctionalDao.getTransportingRateDetails(season,ryotCode,finalDate);
    }
	
	public List getTranspChgSmryByRyotContAndDate(String season,String ryotCode,double transportRate,String finalDate)
    {		
        return caneReceiptFunctionalDao.getTranspChgSmryByRyotContAndDate(season,ryotCode,transportRate,finalDate);
    }
	
	public List getTranspChgSmryByRyotAndCont(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getTranspChgSmryByRyotAndCont(season,ryotCode);
    }
	
	public List getTranspChgSmryByContAndDate(String season)
    {		
        return caneReceiptFunctionalDao.getTranspChgSmryByContAndDate(season);
    }
	
	public List getTranspChgSmryBySeasonByCont(String season)
    {		
        return caneReceiptFunctionalDao.getTranspChgSmryBySeasonByCont(season);
    }
	
	public List getTranspChgSmryBySeason(String season)
    {		
        return caneReceiptFunctionalDao.getTranspChgSmryBySeason(season);
    }
	public List getWeighmentDetailsByAgNumberSeasonPlot(String agreementNumber,String season,String plotNumber)
    {		
        return caneReceiptFunctionalDao.getWeighmentDetailsByAgNumberSeasonPlot(agreementNumber,season,plotNumber);
    }
	
	public Object getProgressiveQuantityFromAgreement(String agreementNumber,String season,String plotNumber)
    {		
        return caneReceiptFunctionalDao.getProgressiveQuantityFromAgreement(agreementNumber,season,plotNumber);
    }

	public boolean updateProgressiveQuantity(String agreementNumber,String season,String plotNumber,Double netWeight)
    {		
        return caneReceiptFunctionalDao.updateProgressiveQuantity(agreementNumber,season,plotNumber,netWeight);
    }
	
	public boolean updateCaneReceptSartEndDateInAgreement(String agreementNumber,String season,String plotNumber,Date caneReceiptStartDate,Date caneReceiptEndDate)
    {		
        return caneReceiptFunctionalDao.updateCaneReceptSartEndDateInAgreement(agreementNumber,season,plotNumber,caneReceiptStartDate,caneReceiptEndDate);
    }

	public boolean insertVcheckIn(String checkindate,Integer serialNumber,String permitNumber,String season,String vehicleNumber,Integer shiftId,Integer vehicletype)
	{
		return caneReceiptFunctionalDao.insertVcheckIn(checkindate,serialNumber,permitNumber,season,vehicleNumber,shiftId,vehicletype);
	}
	public boolean updatePermitDetailsStatus(Integer permitNumber,Integer programNumber,String season)
    {		
        return caneReceiptFunctionalDao.updatePermitDetailsStatus(permitNumber,programNumber,season);
    }
	
	public int getProgramNo(int permitNo)
    {		
		int permitNum = caneReceiptFunctionalDao.getProgramNo(permitNo);
        return permitNum;
    }
	
	public byte getStatusForWeighment(int serialnumber,String validationFor)
    {		
		byte status = caneReceiptFunctionalDao.getStatusForWeighment(serialnumber,validationFor);
        return status;
    }
	//Added by DMurty on 03-08-2016
	public List CaneValueDetailsByRyotList(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.CaneValueDetailsByRyotList(season,ryotCode);
    }
	
	public List getAccruedAndDueCaneValueSummaryList(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getAccruedAndDueCaneValueSummaryList(season,ryotCode);
    }
	
	public List getRyotDatainWeighmentDetails(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getRyotDatainWeighmentDetails(season,ryotCode);
    }
	public boolean updateCaneReceptEndDateInAgreement(String agreementNumber,String season,String plotNumber,Date caneReceiptEndDate)
    {		
        return caneReceiptFunctionalDao.updateCaneReceptEndDateInAgreement(agreementNumber,season,plotNumber,caneReceiptEndDate);
    }
	
	//Added by DMurty on 31-8-2016
	public List getDeductionRates(int dedCode)
    {		
        return caneReceiptFunctionalDao.getDeductionRates(dedCode);
    }
	
	public List getCDCDetailsByRyotList(String season,String ryotCode,String table)
    {		
        return caneReceiptFunctionalDao.getCDCDetailsByRyotList(season,ryotCode,table);
    }
	public List getCDCDetailsByRotAndDateList(String season,String ryotCode,String table,String finalDate)
    {		
        return caneReceiptFunctionalDao.getCDCDetailsByRotAndDateList(season,ryotCode,table,finalDate);
    }
	//getUCDetailsByRotAndDateList
	public List getUCDetailsByRotAndDateList(String season,String ryotCode,String table,String finalDate)
    {		
        return caneReceiptFunctionalDao.getUCDetailsByRotAndDateList(season,ryotCode,table,finalDate);
    }
	//getUCDetailsByRyotList
	public List getUCDetailsByRyotList(String season,String ryotCode,String table)
    {		
        return caneReceiptFunctionalDao.getUCDetailsByRyotList(season,ryotCode,table);
    }
	
	//Added by DMurty on 06-10-2016
	public List getICPDetailsByRyot(String season,String ryotCode)
    {		
        return caneReceiptFunctionalDao.getICPDetailsByRyot(season,ryotCode);
    }
	
	//Added by DMurty on 06-10-2016
	public List getICPDetailsByRyotAndDate(String season,String ryotCode,String finalDate)
    {		
        return caneReceiptFunctionalDao.getICPDetailsByRyotAndDate(season,ryotCode,finalDate);
    }
	
	//Added by DMurty on 08-11-2016
	public List getWeighmentDetailsByPermit(int permitNo)
    {		
		List weighmentListList = caneReceiptFunctionalDao.getWeighmentDetailsByPermit(permitNo);
        return weighmentListList;
    }
	
	public int getActualSlNoBySerialNumber(int slNo,String season)
    {
		int actualSlNo = caneReceiptFunctionalDao.getActualSlNoBySerialNumber(slNo,season);
		return actualSlNo;
    }
	
	//Added by DMurty on 26-11-2016
	public List getSubsidyDetailsByRotAndDateList(String ryotCode,String season,String finalDate)
    {		
        return caneReceiptFunctionalDao.getSubsidyDetailsByRotAndDateList(ryotCode,season,finalDate);
    }
	public List getSubsidyDetailsByRyotList(String ryotCode,String season)
    {		
        return caneReceiptFunctionalDao.getSubsidyDetailsByRyotList(ryotCode,season);
    }
	//Added by DMurty on 28-11-2016
	public List getCaneReceiptShiftSmryList(int shiftId,String shiftDate)
    {		
        return caneReceiptFunctionalDao.getCaneReceiptShiftSmryList(shiftId,shiftDate);
    }
	public Integer GetSecondRec(String canedate,int shift)
    {		
        return caneReceiptFunctionalDao.GetSecondRec(canedate,shift);
    }
	
	public Integer getMaxIDForVcheckIn(String tableName,String checkindate,int shiftid)
    {
		Integer maxVal=(Integer) commonDao.getMaxValue("select max(vehicleslno) from "+tableName+" where checkindate='"+ DateUtils.getSqlDateFromString(checkindate,Constants.GenericDateFormat.DATE_FORMAT)+"'  and vehicleslno is not null and shiftid="+shiftid+"");
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public List getPermitCheckInTotalPerShift(String checkInDate,Integer shift)
    {
		return caneReceiptFunctionalDao.getPermitCheckInTotalPerShift(checkInDate,shift);
    }
	public List getWaitingVehiclesForWeighment(String checkInDate,Integer shift)
    {
		return caneReceiptFunctionalDao.getWaitingVehiclesForWeighment(checkInDate,shift);
    }
	public List getVehiclesForWeighment(String frmdate,String todate)
    {
		return caneReceiptFunctionalDao.getVehiclesForWeighment(frmdate,todate);
    }
	public byte getStatusForVehicleCheckIn(int permitnumber)
    {		
		byte status = caneReceiptFunctionalDao.getStatusForVehicleCheckIn(permitnumber);
        return status;
    }
	public List getPermitListAfterCheckIn(int permitNo)
    {        
        return caneReceiptFunctionalDao.getPermitListAfterCheckIn(permitNo);
    }
	public List getWeighmentDetailsForAccMigration(String season)
	{        
        return caneReceiptFunctionalDao.getWeighmentDetailsForAccMigration(season);
    }
	//Added by DMurty on 08-01-2017
	public int getPermitNoForPartLoad(int permitNo)
    {		
		int secPermitNo = caneReceiptFunctionalDao.getPermitNoForPartLoad(permitNo);
        return secPermitNo;
    }
	
	public List getHarvestingRateDetailsForUpdateHC(String season,String ryotCode,String finalDate)
    {		
        return caneReceiptFunctionalDao.getHarvestingRateDetailsForUpdateHC(season,ryotCode,finalDate);
    }
}
