package com.finsol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.dao.DepartmentMasterDaoImpl;
import com.finsol.model.DepartmentMaster;

/**
 * @author Rama Krishna
 *
 */
@Service("DepartmentMasterService")
public class DepartmentMasterServiceImpl {

	
	
	@Autowired
	private DepartmentMasterDaoImpl departmentMasterDaoImpl;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addDepartment(DepartmentMaster departmentMaster) {
		departmentMasterDaoImpl.addDepartment(departmentMaster);
	}
	
	public List<DepartmentMaster> listDepartments() {
		return departmentMasterDaoImpl.listDepartments();
	}
}
