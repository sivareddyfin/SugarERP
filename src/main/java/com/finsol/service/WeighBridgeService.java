package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.WeighBridgeDao;
import com.finsol.model.WeighBridge;
import com.finsol.model.Zone;


/**
 * @author SahaDeva
 */
@Service("WeighBridgeService")

public class WeighBridgeService
{

private static final Logger logger = Logger.getLogger(WeighBridgeService.class);
	
	@Autowired
	private WeighBridgeDao weighBridgeDao;
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addWeighBridge(WeighBridge weighBridge)
	{
		weighBridgeDao.addWeighBridge(weighBridge);
	}
	
	public List<WeighBridge> listWeighBridge() {
		return weighBridgeDao.listWeighBridge();
	}
	
	
	
}
