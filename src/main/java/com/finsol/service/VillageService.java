package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.VillageDao;
import com.finsol.dao.ZoneDao;
import com.finsol.model.Village;
import com.finsol.model.Zone;

/**
 * @author Rama Krishna
 *
 */
@Service("VillageService")
public class VillageService {
	
	private static final Logger logger = Logger.getLogger(VillageService.class);

	@Autowired
	private VillageDao villageDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addVillage(Village village) {
		villageDao.addVillage(village);
	}
	
	public List<Village> listVillages() {
		return villageDao.listVillages();
	}
	
	public List getMandalNames()
    {	
		//return caneManagerDao.getColumns("select id,storename  from DepartmentMaster",DepartmentMaster.class);
		String query="Select id, name from Mandal";
		return villageDao.getMandalNames(query);
	}
	
	public List listVillagesNew() 
	{
		String query = "select a.villagecode,a.village,a.status,a.mandalcode,a.distance,a.description,a.id,a.pincode,b.mandal from Village a left join Mandal b on a.mandalcode=b.mandalcode";
		return villageDao.listVillagesNew(query);
	}

}
