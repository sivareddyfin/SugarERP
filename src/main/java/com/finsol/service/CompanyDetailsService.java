package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CompanyDetailsDao;
import com.finsol.model.CompanyBranches;
import com.finsol.model.CompanyDetails;
 
/**
 * @author sahadeva
 *
 */
@Service("CompanyDetailsService")

public class CompanyDetailsService 
{

	private static final Logger logger = Logger.getLogger(CompanyDetailsService.class);
	
	   @Autowired
	   private CompanyDetailsDao companyDetailsDao;
	   
	   @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	   public void addCompanyDetails(CompanyDetails companyDetails)
	     {
		   companyDetailsDao.addCompanyDetails(companyDetails);
	     }
	   public List<CompanyDetails> listCompanyDetail() {
			return companyDetailsDao.listCompanyDetail();
		}
	   
	   @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
		public void addCompanyBranches(CompanyBranches companyBranches)
		{
		   companyDetailsDao.addCompanyBranches(companyBranches);
		}
		public List<CompanyBranches> listCompanyBranches() {
			return companyDetailsDao.listCompanyBranches();
		}
		
		public void deleteAllCompanyBranches() 
		{
			companyDetailsDao.deleteAllCompanyBranches();
		}
		

}
