package com.finsol.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CaneWeightDao;
import com.finsol.model.CaneWeight;



/**
 * @author SahaDeva
 */
@Service("CaneWeightService")

public class CaneWeightService 
{
	private static final Logger logger = Logger.getLogger(CaneWeightService.class);
	
	 @Autowired
	 private CaneWeightDao  caneWeightDao ;
	   
	 @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	 public void addCaneWeight(CaneWeight caneWeight)
	 {
		 caneWeightDao.addCaneWeight(caneWeight);
	 }

	 		
	public List getCaneWeight(String season)
	{
		return caneWeightDao.getCaneWeight(season);
	}
	public List getReviseSchedulePermits(Integer circleCode,String season,Integer fromRank,Integer toRank,Integer permitDateFlag,Integer programNumber)
	{
		return caneWeightDao.getReviseSchedulePermits(circleCode,season,fromRank,toRank,permitDateFlag,programNumber);
	}
	//Added by DMurty on 02-11-2016
	public List getPermitPrintByHarvestingDates(Date fromDate,Date toDate)
	{
		return caneWeightDao.getPermitPrintByHarvestingDates(fromDate,toDate);
	}
}
