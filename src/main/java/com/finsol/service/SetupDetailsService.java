package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.SetupDetailsDao;
import com.finsol.model.SetupDetails;

/**
 * @author Sahadeva
 */
@Service("SetupDetailsService")

public class SetupDetailsService 
{	
	private static final Logger logger = Logger.getLogger(SetupDetailsService.class);

	@Autowired
	private SetupDetailsDao setupDetailsDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSetupDetails(SetupDetails setupDetails)
	{
		setupDetailsDao.addSetupDetails(setupDetails);
	}
	
	public List<SetupDetails> listSetupDetail() 
	{
		return setupDetailsDao.listSetupDetail();
	}
	public void deleteSetupDetails() 
	{
		setupDetailsDao.deleteSetupDetails();
	}
}
