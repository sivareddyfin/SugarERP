package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.HarvestingContractorDao;
import com.finsol.model.HCSuretyDetails;
import com.finsol.model.HarvestingContractors;

/**
 * @author Rama Krishna
 *
 */
@Service("HarvestingContractorService")
public class HarvestingContractorService {

	private static final Logger logger = Logger.getLogger(HarvestingContractorService.class);
	
	@Autowired
	private HarvestingContractorDao harvestingContractorDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvestingContractor(HarvestingContractors harvestingContractor) {
		harvestingContractorDao.addHarvestingContractor(harvestingContractor);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHCSuretyDetails(HCSuretyDetails hcSuretyDetails) {
		harvestingContractorDao.addHCSuretyDetails(hcSuretyDetails);
	}
	

	
	public List<HarvestingContractors> listHarvestingContractors() {
		return harvestingContractorDao.lisHarvestingContractors();
	}
	public List<HCSuretyDetails> listHCSuretyDetails() {
		return harvestingContractorDao.listHCSuretyDetails();
	}
	
	public void deleteHCSuretyDetails(HarvestingContractors harvestingContractor) {
		harvestingContractorDao.deleteHCSuretyDetails(harvestingContractor);
	}
	
	public HarvestingContractors getHarvestingContractors(int id) {
		return harvestingContractorDao.getHarvestingContractors(id);
	}
	
	public List<HCSuretyDetails> getHCDetailsByCode(Integer hcode)
    {
        return harvestingContractorDao.getHCDetailsByCode(hcode);
    }
	
	/*public Ryot getRyot(int id) {
		return harvestingContractorDao.getRyot(id);
	}
	
	public List<RyotBankDetails> getHCDetailsByCode(String ryotcode)
    {
       // String sql = "from Branch where bankcode =" + bankcode;
        return ryotDao.getBranchByRyotCode(ryotcode);
    }

	public void deleteRyotBankDetails(Ryot ryot) {
		ryotDao.deleteRyotBankDetails(ryot);
	}*/

}
