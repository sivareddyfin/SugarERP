package com.finsol.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.CaneAccountingFunctionalDao;
import com.finsol.dao.HibernateDao;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneValueSplitupSummary;
import com.finsol.model.HarvChgDtlsByRyotContAndDate;
import com.finsol.model.HarvChgSmryByContAndDate;
import com.finsol.model.HarvChgSmryByRyotAndCont;
import com.finsol.model.HarvChgSmryBySeason;
import com.finsol.model.HarvChgSmryBySeasonByCont;
import com.finsol.model.HarvestingRateDetails;
import com.finsol.model.HarvestingRatesSmry;
import com.finsol.model.TranspChgSmryByContAndDate;
import com.finsol.model.TranspChgSmryByRyotAndCont;
import com.finsol.model.TranspChgSmryByRyotContAndDate;
import com.finsol.model.TranspChgSmryBySeason;
import com.finsol.model.TranspChgSmryBySeasonByCont;
import com.finsol.model.TransportingRateDetails;
import com.finsol.model.TransportingRatesSmry;


/**
 * @author Rama Krishna
 *
 */
@Service("CaneAccountingFunctionalService")
public class CaneAccountingFunctionalService {
	
	private static final Logger logger = Logger.getLogger(CaneAccountingFunctionalService.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	@Autowired	
	private CaneAccountingFunctionalDao caneAccountingFunctionalDao;
	
	
		
	public List getRyotsByCircleandSeason(String season,int circleCode)
    {        
        return caneAccountingFunctionalDao.getRyotsByCircleandSeason(season,circleCode);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvestingChargesSmry(HarvestingRatesSmry harvestingRatesSmry)
	{
		caneAccountingFunctionalDao.addHarvestingChargesSmry(harvestingRatesSmry);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvestingChargesDtls(HarvestingRateDetails harvestingRateDetails)
	{
		caneAccountingFunctionalDao.addHarvestingChargesDtls(harvestingRateDetails);
	}
	
	public void deleteHarvestingCharges(String rtoyCode,double slno) 
	{
		caneAccountingFunctionalDao.deleteHarvestingCharges(rtoyCode,slno);
	}
	
	public HarvestingRateDetails getUpdatedHarvestingCharges(String season,int circleCode,Date effDate,String ryotcode)
	{
		return caneAccountingFunctionalDao.getUpdatedHarvestingCharges(season,circleCode,effDate,ryotcode);
	}

	public List GetDetailsForHarvChgDtlsByRyotContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetDetailsForHarvChgDtlsByRyotContAndDate(ryotCode,contractorCode,season,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvChgDtlsByRyotContAndDate(HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate)
	{
		caneAccountingFunctionalDao.addHarvChgDtlsByRyotContAndDate(harvChgDtlsByRyotContAndDate);
	}
	
	
	public void updateAmountInHarvChgDtlsByRyotContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInHarvChgDtlsByRyotContAndDate(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	public List getCaneWeightByRyotCode(String ryotCode,String season)
    {        
        return caneAccountingFunctionalDao.getCaneWeightByRyotCode(ryotCode,season);
    }
	
	public List HarvChgSmryByContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.HarvChgSmryByContAndDate(ryotCode,contractorCode,season,efDate);
    }
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvChgSmryByContAndDate(HarvChgSmryByContAndDate harvChgSmryByContAndDate)
	{
		caneAccountingFunctionalDao.addHarvChgSmryByContAndDate(harvChgSmryByContAndDate);
	}
	
	public void updateAmountInHarvChgSmryByContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInHarvChgSmryByContAndDate(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	public List HarvChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season)
    {        
        return caneAccountingFunctionalDao.HarvChgSmryByRyotAndCont(ryotCode,contractorCode,season);
    }
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvChgSmryByRyotAndCont(HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont)
	{
		caneAccountingFunctionalDao.addHarvChgSmryByRyotAndCont(harvChgSmryByRyotAndCont);
	}
	public void updateAmountInHarvChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,double UpdatingAmt)
    {
		caneAccountingFunctionalDao.updateAmountInHarvChgSmryByRyotAndCont(ryotCode,contractorCode,season,UpdatingAmt);
    }
	
	public List HarvChgSmryBySeason(String ryotCode,int contractorCode,String season)
    {        
        return caneAccountingFunctionalDao.HarvChgSmryBySeason(ryotCode,contractorCode,season);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvChgSmryBySeason(HarvChgSmryBySeason harvChgSmryBySeason)
	{
		caneAccountingFunctionalDao.addHarvChgSmryBySeason(harvChgSmryBySeason);
	}

	public void updateAmountInHarvChgSmryBySeason(String ryotCode,int contractorCode,String season,double UpdatingAmt)
    {
		caneAccountingFunctionalDao.updateAmountInHarvChgSmryBySeason(ryotCode,contractorCode,season,UpdatingAmt);
    }
	
	
	public List HarvChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season)
    {        
        return caneAccountingFunctionalDao.HarvChgSmryBySeasonByCont(ryotCode,contractorCode,season);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addHarvChgSmryBySeasonByCont(HarvChgSmryBySeasonByCont harvChgSmryBySeasonByCont)
	{
		caneAccountingFunctionalDao.addHarvChgSmryBySeasonByCont(harvChgSmryBySeasonByCont);
	}
	
	public void updateAmountInHarvChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,double UpdatingAmt)
    {
		caneAccountingFunctionalDao.updateAmountInHarvChgSmryBySeasonByCont(ryotCode,contractorCode,season,UpdatingAmt);
    }
	

	public List getExtentByRyots(String ryotCode,String season)
    {        
        return caneAccountingFunctionalDao.getExtentByRyots(ryotCode,season);
    }
	
	
	//Transport Charges Update
	
	public void deleteTransportCharges(String rtoyCode,double slno) 
	{
		caneAccountingFunctionalDao.deleteTransportCharges(rtoyCode,slno);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTransportingRatesSmry(TransportingRatesSmry transportingRatesSmry)
	{
		caneAccountingFunctionalDao.addTransportingRatesSmry(transportingRatesSmry);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTransportingRatesDtls(TransportingRateDetails transportingRateDetails)
	{
		caneAccountingFunctionalDao.addTransportingRatesDtls(transportingRateDetails);
	}
	
	public List getCaneWeightByContAndDate(int contCode,String ryotCode)
    {        
        return caneAccountingFunctionalDao.getCaneWeightByContAndDate(contCode,ryotCode);
    }
	
	public List GetTranspChgSmryByContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetTranspChgSmryByContAndDate(ryotCode,contractorCode,season,efDate);
    }
	
	public void updateAmountInTranspChgSmryByContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInTranspChgSmryByContAndDate(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTranspChgSmryByContAndDate(TranspChgSmryByContAndDate transpChgSmryByContAndDate)
	{
		caneAccountingFunctionalDao.addTranspChgSmryByContAndDate(transpChgSmryByContAndDate);
	}
	
	public List GetTranspChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetTranspChgSmryByRyotAndCont(ryotCode,contractorCode,season,efDate);
    }
	
	public void updateAmountInTranspChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInTranspChgSmryByRyotAndCont(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTranspChgSmryByRyotAndCont(TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont)
	{
		caneAccountingFunctionalDao.addTranspChgSmryByRyotAndCont(transpChgSmryByRyotAndCont);
	}
	
	public List GetTranspChgSmryByRyotContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetTranspChgSmryByRyotContAndDate(ryotCode,contractorCode,season,efDate);
    }
	
	public void updateAmountInTranspChgSmryByRyotContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInTranspChgSmryByRyotContAndDate(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTranspChgSmryByRyotContAndDate(TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate)
	{
		caneAccountingFunctionalDao.addTranspChgSmryByRyotContAndDate(transpChgSmryByRyotContAndDate);
	}
	
	public List GetTranspChgSmryBySeason(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetTranspChgSmryBySeason(ryotCode,contractorCode,season,efDate);
    }
	
	public void updateAmountInTranspChgSmryBySeason(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInTranspChgSmryBySeason(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTranspChgSmryBySeason(TranspChgSmryBySeason transpChgSmryBySeason)
	{
		caneAccountingFunctionalDao.addTranspChgSmryBySeason(transpChgSmryBySeason);
	}
	
	public List GetTranspChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetTranspChgSmryBySeasonByCont(ryotCode,contractorCode,season,efDate);
    }
	
	public void updateAmountInTranspChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
    {
		caneAccountingFunctionalDao.updateAmountInTranspChgSmryBySeasonByCont(ryotCode,contractorCode,season,UpdatingAmt,efDate);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTranspChgSmryBySeasonByCont(TranspChgSmryBySeasonByCont transpChgSmryBySeasonByCont)
	{
		caneAccountingFunctionalDao.addTranspChgSmryBySeasonByCont(transpChgSmryBySeasonByCont);
	}
	//PCAC
	public boolean saveMultipleEntities(List entitiesList)
	{
		return caneAccountingFunctionalDao.saveMultipleEntities(entitiesList);
	}
	public List getWeighmentDetails(Date fromdate,Date todate,String season,String villageCode,int category)
    {		
        return caneAccountingFunctionalDao.getWeighmentDetails(fromdate,todate,season,villageCode,category);
    }
	public List<CaneAccountingRules> getCaneAccRulesForCalc(String season,Date date1,Date date2)
	{
		return caneAccountingFunctionalDao.getCaneAccRulesForCalc(season,date1,date2);
	}
	
	public List getCaneValueSplitupSummary(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupSummary(season,ryotCode);
    }
	
	public List getCaneValueSplitupDetailsByDate(String season,String ryotCode,String efDate)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupDetailsByDate(season,ryotCode,efDate);
    }
	
	public List getCaneValueSplitupDetailsByRyot(String ryotCode)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupDetailsByRyot(ryotCode);
    }
	
	public List getCaneAccountSmry(String season,Integer slno)
    {		
        return caneAccountingFunctionalDao.getCaneAccountSmry(season,slno);
    }
	
	public List getCaneAccountSmrytemp(String season,Integer slno)
    {		
        return caneAccountingFunctionalDao.getCaneAccountSmrytemp(season,slno);
    }
	
	public List<CaneValueSplitupSummary> listCaneValueSplitupDetails() {
		return caneAccountingFunctionalDao.listCaneValueSplitupDetails();
	}
	
	public List getCaneValueSplitupTemp(String season,Integer caneacno)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupTemp(season,caneacno);
    }
	public List getAllDeductions(String season,Integer caneacno,Map splitupMap)
    {
		//Ryot Code Added by DMurty on 18-08-2016
        return caneAccountingFunctionalDao.getAllDeductions(season,caneacno,splitupMap);
    }

	public List getHarvestingRatesDetails(String season,String ryotcode)
    {		
        return caneAccountingFunctionalDao.getHarvestingRatesDetails(season,ryotcode);
    }
	public List getNewAmount(String season,String ryotcode,Date fromdate,Date todate)
    {		
        return caneAccountingFunctionalDao.getNewAmount(season,ryotcode,fromdate,todate);
    }
	
	public List getPendingAmount(String season,String ryotcode)
    {		
        return caneAccountingFunctionalDao.getPendingAmount(season,ryotcode);
    }
	//modified by naidu on 07-03-2017
	public List getAllAdvances(Date fromdate,Date todate,String ryotCode,String season)
    {		
        return caneAccountingFunctionalDao.getAllAdvances(fromdate,todate,ryotCode,season);
    }
	
	public List getLoanDetails(String season,String ryotcode)
    {		
        return caneAccountingFunctionalDao.getLoanDetails(season,ryotcode);
    }
	public List getAdvanceCalcDetails(String season,String ryotcode,Integer advancecode)
    {		
		logger.info("getAdvanceCalcDetails()------ ");
       // return caneAccountingFunctionalDao.getAdvanceCalcDetails(season,ryotcode,advancecode);
		String sql="SELECT principle,principledate FROM AdvancePrincipleAmounts  where ryotcode='" + ryotcode+"' and season='" + season+"' and advancecode="+advancecode+" and principle>0";
		
		return caneAccountingFunctionalDao.getAdvanceCalcDetails(sql);
    }
	
	public List getLoanCalcDetails(String season,String ryotcode,Integer branchcode, int ryotLoanNo)
    {		
		logger.info("getAdvanceCalcDetails()------ ");
 		String sql="SELECT principle,principledate FROM LoanPrincipleAmounts  where ryotcode='" + ryotcode+"' and season='" + season+"' and branchcode="+branchcode+" and loannumber = " +ryotLoanNo+" and principle>0";
		
		return caneAccountingFunctionalDao.getAdvanceCalcDetails(sql);
    }	
	
	public Object getInterestAdvance(Double Amount,Integer advanceCode)
	{
		String sql="SELECT interestrate FROM AdvanceInterestRates WHERE  "+Amount.intValue()+" BETWEEN fromamount AND toamount and advancecode="+advanceCode;
		
		return hibernateDao.getID(sql);
	}
	
	public Object getCaneReceiptDate(String season,String ryotcode)
	{
		String sql="SELECT canereceiptdate FROM WeighmentDetails where ryotcode='" + ryotcode+"' and season='" + season+"'";
		
		return hibernateDao.getID(sql);
	}
	
	
	public List getAllAdvancePayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {		
        return caneAccountingFunctionalDao.getAllAdvancePayableDetailsForUpdateRyotAccounts(villageCode,caneaccdate);
    }
	
	public List getAllLoanPayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {		
        return caneAccountingFunctionalDao.getAllLoanPayableDetailsForUpdateRyotAccounts(villageCode,caneaccdate);
    }
	
	public List getAllHarvesterPayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {		
        return caneAccountingFunctionalDao.getAllHarvesterPayableDetailsForUpdateRyotAccounts(villageCode,caneaccdate);
    }
	
	public List getAllTransporterDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {		
        return caneAccountingFunctionalDao.getAllTransporterDetailsForUpdateRyotAccounts(villageCode,caneaccdate);
    }
	
	public List getAllSBAccountDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {		
        return caneAccountingFunctionalDao.getAllSBAccountDetailsForUpdateRyotAccounts(villageCode,caneaccdate);
    }
	
	public List getSBAccSmryByBank(String season,Integer bankcode)
    {		
        return caneAccountingFunctionalDao.getSBAccSmryByBank(season,bankcode);
    }
	
	
	public List getAdvancePaymentSummary(String season,String ryotCode,Integer advCode)
    {		
        return caneAccountingFunctionalDao.getAdvancePaymentSummary(season,ryotCode,advCode);
    }
	
	public List getAdvanceSummary(String season,String ryotCode,Integer advCode)
    {		
        return caneAccountingFunctionalDao.getAdvanceSummary(season,ryotCode,advCode);
    }
	
	public List getLoanPaymentSummary(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getLoanPaymentSummary(season,ryotCode);
    }
	
	public List getLoanSummary(String season,String ryotCode,Integer loannumber)
    {		
        return caneAccountingFunctionalDao.getLoanSummary(season,ryotCode,loannumber);
    }
	
	public List getLoanPaymentSummaryByBank(String season)
    {		
        return caneAccountingFunctionalDao.getLoanPaymentSummaryByBank(season);
    }
	
	public List getSBAccSmryByRyot(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getSBAccSmryByRyot(season,ryotCode);
    }
	
	public List getSBAccByRyot(String ryotCode)
    {		
        return caneAccountingFunctionalDao.getSBAccByRyot(ryotCode);
    }
	public List getOtherDedSmryByRyot(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getOtherDedSmryByRyot(season,ryotCode);
    }
	
	
	public List getHarvChgSmryByRyotAndCont(String season,String ryotCode,Integer harvestcontcode)
    {		
        return caneAccountingFunctionalDao.getHarvChgSmryByRyotAndCont(season,ryotCode,harvestcontcode);
		
    }
	
	public List getHarvChgSmryBySeasonByCont(String season,Integer harvestcontcode)
    {		
        return caneAccountingFunctionalDao.getHarvChgSmryBySeasonByCont(season,harvestcontcode);
    }
	
	public List getHarvChgSmryBySeason(String season)
    {		
        return caneAccountingFunctionalDao.getHarvChgSmryBySeason(season);
    }
	
	public List getTranspChgSmryByRyotAndCont(String season,String ryotCode,Integer transportcontcode)
    {		
        return caneAccountingFunctionalDao.getTranspChgSmryByRyotAndCont(season,ryotCode,transportcontcode);
    }
	
	public List getTranspChgSmryBySeasonByCont(String season,Integer transportcontcode)
    {		
        return caneAccountingFunctionalDao.getTranspChgSmryBySeasonByCont(season,transportcontcode);
    }
	
	public List getTranspChgSmryBySeason(String season)
    {		
        return caneAccountingFunctionalDao.getTranspChgSmryBySeason(season);
    }
	
	public List getRyotLedgerForSeason(String season,String ryotcode,Date date1,Date date2)
	{
		return caneAccountingFunctionalDao.getRyotLedgerForSeason(season,ryotcode,date1,date2);
	}
	
	public List getICPDetails()
	{
		return caneAccountingFunctionalDao.getICPDetails();
	}
	public List getAccMasterData(String ryotCode)
	{
		return caneAccountingFunctionalDao.getAccMasterData(ryotCode);
	}
	
	
	public List getTrialBalForSeason(String season)
	{
		return caneAccountingFunctionalDao.getTrialBalForSeason(season);
	}
	
	public void deleteCaneAcctAmounts(int calcid,String season) 
	{
		caneAccountingFunctionalDao.deleteCaneAcctAmounts(calcid,season);
	}
	public List getSeedSupplierRyots(String season,String villageCode)
	{
		return caneAccountingFunctionalDao.getSeedSupplierRyots(season,villageCode);
	}
	public String getRyotName(String ryotcode)
	{
		return caneAccountingFunctionalDao.getRyotName(ryotcode);
	}
	public List getSeedAccounting(Integer seedactno)
	{
		return caneAccountingFunctionalDao.getSeedAccounting(seedactno);
	}
	public List getRyotPrincipleAmountsList(String season,String ryotcode,String tablename)
	{
		return caneAccountingFunctionalDao.getRyotPrincipleAmountsList(season,ryotcode,tablename);
	}
	public Object getAdvName(Integer advcode)
	{
		String sql="SELECT advance FROM CompanyAdvance WHERE advancecode="+advcode;
		
		return hibernateDao.getID(sql);
	}
	public Object getInterestForLoans(Double Amount,Integer branchcode,String season)
	{
		String sql="SELECT interestrate FROM BankLoanIntRates WHERE  "+Amount.intValue()+" BETWEEN fromamount AND toamount and branchcode="+branchcode+" and season='"+season+"'";
		
		return hibernateDao.getID(sql);
	}
	
	public List<CaneAcctRules> getCaneAccRulesForCalcNew(String season,Date date1,Date date2)
	{
		return caneAccountingFunctionalDao.getCaneAccRulesForCalcNew(season,date1,date2);
	}
		public List getDueCaneSupplierRyots(String season,String villageCode)
	{
		return caneAccountingFunctionalDao.getDueCaneSupplierRyots(season,villageCode);
	}

	public List getSeedSuppliersSummaryDetails(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getSeedSuppliersSummaryDetails(season,ryotCode);
    }
	public List getadvancePrincipleAmountsDetails(String season,String ryotCode,Integer advcode)
    {		
        return caneAccountingFunctionalDao.getadvancePrincipleAmountsDetails(season,ryotCode,advcode);
    }
	public List getLoanPrincipleAmountsDetails(String season,String ryotCode,String accnum)
    {		
        return caneAccountingFunctionalDao.getLoanPrincipleAmountsDetails(season,ryotCode,accnum);
    }
	public List getLoanDetailsForSeedAccounting(String season,String ryotCode,String accnum)
    {		
        return caneAccountingFunctionalDao.getLoanDetailsForSeedAccounting(season,ryotCode,accnum);
    }
	public List getLoanSummaryForSeeAcc(String season,String ryotCode,Integer branchCode,Integer loannumber)
    {		
        return caneAccountingFunctionalDao.getLoanSummaryForSeeAcc(season,ryotCode,branchCode,loannumber);
    }
	public List getAccAndDueCaneValueSummaryDetailsForUpdate(String season,String ryotcode,String villageCode)
	{
	return 	caneAccountingFunctionalDao.getAccAndDueCaneValueSummaryDetailsForUpdate(season,ryotcode,villageCode);
	}
	public Double getExtentSizeForRyot(String canesuppliercode)
	{
		String sql="SELECT sum(extentsize) FROM AgreementDetails WHERE  ryotcode='"+canesuppliercode+"'";
		Object extent=hibernateDao.getID(sql);
		if(extent==null)
		return 0.0;
		else
		return (Double)extent;
	}
	
	//Added by DMurty on 17-10-2016
	public List getCaneWeightFromTranspChgSmryByRyotAndCont(String ryotCode,String season)
    {        
        return caneAccountingFunctionalDao.getCaneWeightFromTranspChgSmryByRyotAndCont(ryotCode,season);
    }
	//Added by DMurty on 24-10-2016
	public List getOldPendingAmtList(String ryotCode,String season,int dedCode)
    {		
        return caneAccountingFunctionalDao.getOldPendingAmtList(ryotCode,season,dedCode);
    }
	
	public List getDedDtlsFromTemp(int caneAccSlNo,String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getDedDtlsFromTemp(caneAccSlNo,season,ryotCode);
    }
	
	public double getdedSbAmt(int caneAccSlNo,String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getdedSbAmt(caneAccSlNo,season,ryotCode);
    }
	
	public List getAccountCode(int contractorCode,String table,String column)
    {		
        return caneAccountingFunctionalDao.getAccountCode(contractorCode,table,column);
    }
	
	public List getLoanDetails(int loanNo,String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getLoanDetails(loanNo,season,ryotCode);
    }
	
	//Added by DMurty on 01-11-2016
	public List getCaneAccountSmryDetails(int caneacslno,String season)
    {		
        return caneAccountingFunctionalDao.getCaneAccountSmryDetails(caneacslno,season);
    }
	
	//Added by DMurty on 14-11-2016
	public List<CaneAcctRules> getCaneAccRulesForCalcForCaneAccounting(String season,Date date1,Date date2)
	{
		return caneAccountingFunctionalDao.getCaneAccRulesForCalcForCaneAccounting(season,date1,date2);
	}
	
	//Added by DMurty on 14-11-2016
	public List getTpDedDtls(String ryotCode,String season,int harvestContCode)
    {		
		//TranspChgSmryByRyotAndCont
        return caneAccountingFunctionalDao.getTpDedDtls(ryotCode,season,harvestContCode);
    }
	
	public List getHarvestinChargesDtls(String ryotCode,String season,int harvestContCode)
    {		
		//TranspChgSmryByRyotAndCont
        return caneAccountingFunctionalDao.getHarvestinChargesDtls(ryotCode,season,harvestContCode);
    }
	
	//Added by DMurty on 15-11-2016
	public List CaneAccountSmryTemp(int caneAccSlNo,String season)
    {		
        return caneAccountingFunctionalDao.CaneAccountSmryTemp(caneAccSlNo,season);
    }
	//
	public List CaneValueSplitupTemp(int caneAccSlNo,String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.CaneValueSplitupTemp(caneAccSlNo,season,ryotCode);
    }
	//Added by DMurty on 16-11-2016
	public void TruncateTempTable(String model,String season,int caneacslno,String user) 
	{
		caneAccountingFunctionalDao.TruncateTempTable(model,season,caneacslno,user);
	}
	//Added by DMurty on 24-11-2016
	public Object getInterestForLoan(Double Amount,Integer branchcode,String season)
	{
		logger.info("getInterestForLoan()------ ");
		String sql="SELECT interestrate FROM BankLoanIntRates WHERE  "+Amount.intValue()+" BETWEEN fromamount AND toamount and branchcode="+branchcode+" and season='"+season+"'";
		return hibernateDao.getID(sql);
	}
	
	public List getRyotSbAccNo(String ryotCode,String season,String agreementNo)
    {		
        return caneAccountingFunctionalDao.getRyotSbAccNo(ryotCode,season,agreementNo);
    }
	
	public List getCaneValueSplitupByRyot(String season,Integer caneacno,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupByRyot(season,caneacno,ryotCode);
    }
	
	public List getMaxWeighmentDate(String ryotCode,String season,Date fromdate,Date todate)
    {		
        return caneAccountingFunctionalDao.getMaxWeighmentDate(ryotCode,season,fromdate,todate);
    }
	
	public List getCaneAccountingPeriods(String season)
    {        
        return caneAccountingFunctionalDao.getCaneAccountingPeriods(season);
    }
	
	public List getCaneAccountingPaymentDetails(String season,int paymentToRyot,int branch,int caneAccSlNo,int bankPayment)
    {        
        return caneAccountingFunctionalDao.getCaneAccountingPaymentDetails(season,paymentToRyot,branch,caneAccSlNo,bankPayment);
    }
	
	public List getAccountNumbers(String season,String ryotCode,String loanOrSb)
    {        
        return caneAccountingFunctionalDao.getAccountNumbers(season,ryotCode,loanOrSb);
    }
	//Added by DMurty on 16-12-2016
	public double getPendingAmountForRyotByCaneAccSlNo(String ryotCode,String season,String loanOrSb,int caneAccSlNo)
    {        
        return caneAccountingFunctionalDao.getPendingAmountForRyotByCaneAccSlNo(ryotCode,season,loanOrSb,caneAccSlNo);
    }
	
	public List getAccountedRyots(String season,String loanOrSb,int caneAccSlNo)
    {        
        return caneAccountingFunctionalDao.getAccountedRyots(season,loanOrSb,caneAccSlNo);
    }
	
	public List getCaneAccountingDataByRyot(String ryotCode,String season,String loanOrSb,int caneAccSlNo,int loanNo)
    {        
        return caneAccountingFunctionalDao.getCaneAccountingDataByRyot(ryotCode,season,loanOrSb,caneAccSlNo,loanNo);
    }
	
	public List getLoanSummaryByBankAndLoanNo(String season,String ryotCode,Integer loannumber,int bankBranch)
    {		
        return caneAccountingFunctionalDao.getLoanSummaryByBankAndLoanNo(season,ryotCode,loannumber,bankBranch);
    }
	
	public List getRyotLoanBanks(String ryotCode,String season)
    {        
        return caneAccountingFunctionalDao.getRyotLoanBanks(ryotCode,season);
    }
	public List getRyotLoanAccountDetailsByBranch(String ryotCode,String season,int branchcode)
    {        
        return caneAccountingFunctionalDao.getRyotLoanAccountDetailsByBranch(ryotCode,season,branchcode);
    }
	
	public double getPrincipleForAdvance(int advanceCode,String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getPrincipleForAdvance(advanceCode,season,ryotCode);
    }
	public List getRyotLoanPrincipleByBranch(String ryotCode,String season,int branchcode,String accNo)
    {        
        return caneAccountingFunctionalDao.getRyotLoanPrincipleByBranch(ryotCode,season,branchcode,accNo);
    }
	
	public List getCaneValueSplitup()
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitup();
    }
	//getPreviousIntrestAmt
	public double getPreviousIntrestAmt(String ryotCode,String season,int branchcode,int loanno)
    {		
        return caneAccountingFunctionalDao.getPreviousIntrestAmt(ryotCode,season,branchcode,loanno);
    }
	public List getCaneValueSplitupNew(int caneAccSlno)
    {		
        return caneAccountingFunctionalDao.getCaneValueSplitupNew(caneAccSlno);
    }
	public List getCaneAccountingPeriodsTemp(String season)
    {        
        return caneAccountingFunctionalDao.getCaneAccountingPeriodsTemp(season);
    }
	public String getAgreementNoByDates(Date datefrom,Date date2,String season,String ryotcode)
    {		
        return caneAccountingFunctionalDao.getAgreementNoByDates(datefrom,date2,season,ryotcode);
    }
	public List getDedutionsData(String season,int caneslno)
    {        
        return caneAccountingFunctionalDao.getDedutionsData(season,caneslno);
    }
	public List getCaneToFAData(String season,int caneslno)
    {        
        return caneAccountingFunctionalDao.getCaneToFAData(season,caneslno);
    }
	
	public List GetHarvChgDtlsByRyotContAndDateForRevert(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetHarvChgDtlsByRyotContAndDateForRevert(ryotCode,contractorCode,season,efDate);
    }
	public List GetHarvChgDtlsByRyotContForRevert(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetHarvChgDtlsByRyotContForRevert(ryotCode,contractorCode,season,efDate);
    }
	public List getLoanCalcDetailsForPaymentChecklist(String season,String ryotcode,Integer branchcode, int ryotLoanNo,Date todate)
    {		
		logger.info("getAdvanceCalcDetails()------ ");
 		String sql="SELECT principle,principledate FROM LoanPrincipleAmounts  where ryotcode='" + ryotcode+"' and season='" + season+"' and branchcode="+branchcode+" and loannumber = " +ryotLoanNo+" and principle>0 and principledate <='"+todate+"'";
		return caneAccountingFunctionalDao.getLoanCalcDetailsForPaymentChecklist(sql);
    }

	public List getAdvanceCalcDetailsForPaymentChecklist(String season,String ryotcode,Integer advancecode,Date todate)
    {		
		logger.info("getAdvanceCalcDetails()------ ");
		String sql="SELECT principle,principledate FROM AdvancePrincipleAmounts  where ryotcode='" + ryotcode+"' and season='" + season+"' and advancecode="+advancecode+" and principle>0 and principledate <='"+todate+"'";
		return caneAccountingFunctionalDao.getAdvanceCalcDetails(sql);
    }	
	public List GetDetailsForHarvChgDtlsByRyotContAndDateNew(String ryotCode,int contractorCode,String season,Date efDate)
    {        
        return caneAccountingFunctionalDao.GetDetailsForHarvChgDtlsByRyotContAndDateNew(ryotCode,contractorCode,season,efDate);
    }
	
	//added by naidu on 04-02-2017
	public List getCaneAccDetailsTemp()
    {
		return caneAccountingFunctionalDao.getCaneAccDetailsTemp();
    }
	//Added by DMurty on 28-02-20217
	public List getAdvOrLoanWithOrWithoutSupply(int advOrLoan,int supplyType,String season,String villageCode,int category)
    {		
        return caneAccountingFunctionalDao.getAdvOrLoanWithOrWithoutSupply(advOrLoan,supplyType,season,villageCode,category);
    }
	
	
	//Added by DMurty on 02-03-2017
	public double getPendingPrinciple(String ryotcode,String season,int advOrLoan)
    {
		return caneAccountingFunctionalDao.getPendingPrinciple(ryotcode,season,advOrLoan);
    }
	
	public List getSeedSupplierDetails(String season)
    {
		return caneAccountingFunctionalDao.getSeedSupplierDetails(season);
    }
	public List getSeedSupplieramts(String season,String ryotCode)
    {		
        return caneAccountingFunctionalDao.getSeedSupplieramts(season,ryotCode);
    }
	
}
