package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.EmployeesDao;
import com.finsol.model.Employees;



/**
 * @author SahaDeva
 */
@Service("EmployeesService")


public class EmployeesService
{
	private static final Logger logger = Logger.getLogger(EmployeesService.class);
	
	@Autowired
	   private EmployeesDao employeesDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	   public void addEmployees(Employees employees)
	   {
			employeesDao.addEmployees(employees);
	   }
	public List<Employees> listEmployee() {
		return employeesDao.listEmployee();
	}
	
	
	public Employees getEmployees(int id)
	{
		return employeesDao.getEmployees(id);
	}
	
	public Employees getEmployeeByLoginId(String loginId) throws Exception
	{
		try
		{
			Employees employees = null;
			employees = employeesDao.getEmployeeByLoginId(loginId);
			logger.info("====employees---------"+employees);
			return employees;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
		
	}

}
