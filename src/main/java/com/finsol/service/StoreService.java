package com.finsol.service;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finsol.bean.DcBean;
import com.finsol.bean.DcSummaryBean;
import com.finsol.bean.GrnDetailsBean;
import com.finsol.bean.GrnSummaryBean;
import com.finsol.bean.GroupMasterBean;
import com.finsol.bean.ProductMasterBean;
import com.finsol.bean.ProductSubGroupBean;
import com.finsol.bean.StockInDetailsBean;
import com.finsol.bean.StockInSummaryBean;
import com.finsol.bean.StockIssueDtlsBean;
import com.finsol.bean.StockIssueSmryBean;
import com.finsol.bean.StockReqDtlsBean;
import com.finsol.bean.StockReqSmryBean;
import com.finsol.bean.UomBean;
import com.finsol.dao.HibernateDao;
import com.finsol.dao.StoreDao;
import com.finsol.model.DepartmentMaster;
import com.finsol.model.Employees;
import com.finsol.model.GrnDetails;
import com.finsol.model.GrnSummary;
import com.finsol.model.ProductGroup;
import com.finsol.model.ProductMaster;
import com.finsol.model.ProductSubGroup;
import com.finsol.model.SDCDetails;
import com.finsol.model.SDCSummary;
import com.finsol.model.StockInDetails;
import com.finsol.model.StockInSummary;
import com.finsol.model.StockIssueDtls;
import com.finsol.model.StockIssueSmry;
import com.finsol.model.StockReqDtls;
import com.finsol.model.StockReqSmry;
import com.finsol.model.StoreBatchMaster;
import com.finsol.model.Uom;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author Umanath Ch
 *
 */
@Service("StoreService")


public class StoreService 
{
	private static final Logger logger = Logger.getLogger(StoreService.class);
	
	@Autowired
	private StoreDao storeDao;
	
	@Autowired
	private HibernateDao hibernateDao;
	
	@Autowired
	private HttpServletRequest request;
	

	
	@Autowired
	private StockService stockService;
	

	public List<Uom> getAllUOM()
	{
		return storeDao.getAllUOM();
	}
	public List<ProductGroup> getAllProductGroups()
	{
		return storeDao.getAllProductGroups();
	}
	public List<ProductSubGroup> getAllProductSubGroups()
	{
		return storeDao.getAllProductSubGroups();
	}
	public List getAllProducts(String jsonData)
	{
		return storeDao.getAllProducts(jsonData);
	}
	public List<ProductMaster> getProductsNameByCode(String jsonData)
	{
		return storeDao.getProductsNameByCode(jsonData);
	}
	
	public Integer getMaxIDforColumn(String tableName,String columnName)
    {
		Integer maxVal=(Integer) hibernateDao.getMaxValue("select max("+columnName+") from "+tableName);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public Integer getMaxIDfortable(String tableName,String columnName,String coldata ,String colname)
    {
		Integer maxVal=(Integer) hibernateDao.getMaxValue("select max("+columnName+") from "+tableName+" where "+colname+"='"+coldata+"' ");
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	
	public Integer getMaxIDfortablewithDept(String tableName,String columnName,String colname,String coldata,String colname1,String coldata1)
    {
		Integer maxVal=(Integer) hibernateDao.getMaxValue("select max("+columnName+") from "+tableName+" where "+colname+"="+coldata+" AND "+colname1+"='"+coldata1+"' ");
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	
	
	public Uom prepareModelforUom(UomBean uomBean)
	{
		Integer maxId = getMaxIDforColumn("Uom","id");
		Uom uom = new Uom();

		String modifyFlag = uomBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			uom.setId(uomBean.getId());
		}
		else
		{
			uom.setId(maxId);
		}
		uom.setUom(uomBean.getUom());
		uom.setUomCode(uomBean.getUomCode());
		uom.setStatus(uomBean.getStatus());
		uom.setDescription(uomBean.getDescription());

		return uom;
	}
	
	
	public ProductGroup prepareModelforProductGroup(GroupMasterBean Bean)
	{
		Integer maxId = getMaxIDforColumn("ProductGroup","groupSeqNo");
		ProductGroup model = new ProductGroup();

		String modifyFlag = Bean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			model.setProductGroupCode(Bean.getProductGroupCode());
			model.setGroupSeqNo(Bean.getGroupCode());

		}
		else
		{
			String value = Integer.toString(Bean.getGroupCode());
			if(value.length() == 1)
			{
				value = "0"+value;
			}
			model.setProductGroupCode(value);
			model.setGroupSeqNo(Bean.getGroupCode());

		}
		model.setGroupName(Bean.getGroup());
		model.setDescription(Bean.getDescription());
		model.setStatus(Bean.getStatus());
		return model;
	}
	public ProductSubGroup prepareModelforSubProductGroup(ProductSubGroupBean Bean)
	{
		//Integer maxId = getMaxIDforColumn("productSubGroup","SubgroupSeq");
		ProductSubGroup model = new ProductSubGroup();

		String modifyFlag = Bean.getModifyFlag();
		model.setStatus(Bean.getStatus());
		model.setDescription(Bean.getDescription());
		String subgroupno=Bean.getSubGroupCode();
		subgroupno = subgroupno.substring(1);
		subgroupno = subgroupno.substring(1);
		Integer subgrpno=Integer.parseInt(subgroupno);
		model.setSubGroupSeq(subgrpno);
		model.setProductSubGroupCode(Bean.getSubGroupCode());
		model.setSubGroupName(Bean.getSubGroup());
		model.setProductGroupCode(Bean.getGroupCode());

		return model;
	}
	public List<GroupMasterBean> prepareListofBeanForProductGroup(List<ProductGroup> ProductGroups)
	{
		List<GroupMasterBean> beans = null;
		if (ProductGroups != null && !ProductGroups.isEmpty())
		{
			beans = new ArrayList<GroupMasterBean>();
			GroupMasterBean bean = null;
			for (ProductGroup productGroup : ProductGroups)
			{
				bean = new GroupMasterBean();
				bean.setProductGroupCode(productGroup.getProductGroupCode());
				bean.setGroupCode(productGroup.getGroupSeqNo());
				bean.setDescription(productGroup.getDescription());
				bean.setModifyFlag("Yes");
				bean.setStatus(productGroup.getStatus());
				bean.setGroup(productGroup.getGroupName());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	public List<UomBean> prepareListofBeanForUom(List<Uom> Uoms)
	{
		List<UomBean> beans = null;
		if (Uoms != null && !Uoms.isEmpty())
		{
			beans = new ArrayList<UomBean>();
			UomBean bean = null;
			for (Uom uom : Uoms)
			{
				bean = new UomBean();

				bean.setId(uom.getId());
				bean.setDescription(uom.getDescription());
				bean.setModifyFlag("Yes");
				bean.setStatus(uom.getStatus());
				bean.setUom(uom.getUom());
				bean.setUomCode(uom.getUomCode());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	public boolean saveMultipleEntities(List entitiesList)
	{
		return storeDao.saveMultipleEntities(entitiesList);
	}
	
	public boolean saveMultipleEntitiesForFunctionalScreens(List entitiesList,List UpdateList)
	{
		return storeDao.saveMultipleEntitiesForFunctionalScreens(entitiesList,UpdateList);
	}
	
	public boolean saveMultipleEntitiesForScreensUpdateFirst(List entitiesList,List UpdateList)
	{
		return storeDao.saveMultipleEntitiesForScreensUpdateFirst(entitiesList,UpdateList);
	}
	 public String getGroupName(String groupid)
	{
		return storeDao.getGroupName(groupid);
	}
	 public List getSubGroupsListByGrp(String groupid)
	{
		return storeDao.getSubGroupsListByGrp(groupid);
	}
	 public List getGroupsList()
	{
		return storeDao.getGroupsList();
	}
	 public List getUomsList()
	{
		return storeDao.getUomsList();
	}
	 
	 public List getStockMethodList()
		{
			return storeDao.getStockMethodList();
		}
	 public List getStockMethodListById(Integer id)
		{
			return storeDao.getStockMethodListById(id);
		}
	 public List getUomNameById(Integer uomcode)
		{
			return storeDao.getUomNameById(uomcode);
		}
	
	 public List<ProductSubGroupBean> prepareListofBeanForProductSubGroup(List<ProductSubGroup> productSubGroups)
		{
			List<ProductSubGroupBean> beans = null;
			if (productSubGroups != null && !productSubGroups.isEmpty())
			{
				beans = new ArrayList<ProductSubGroupBean>();
				ProductSubGroupBean bean = null;
				for (ProductSubGroup model : productSubGroups)
				{
					bean = new ProductSubGroupBean();
					bean.setGroupCode(model.getProductGroupCode());
					bean.setDescription(model.getDescription());	
					bean.setModifyFlag("Yes");
					bean.setStatus(model.getStatus());
					bean.setSubGroupCode(model.getProductSubGroupCode());
					bean.setSubGroup(model.getSubGroupName());
					String groupname = getGroupName(model.getProductGroupCode());
					bean.setGroup(groupname);
					beans.add(bean);
				}
			}
			return beans;
		}
	
	 public ProductMaster prepareModelforProductMaster(ProductMasterBean Bean)
		{
		 	ProductMaster model = new ProductMaster();
		 	
		 	String modifyFlag = Bean.getModifyFlag();
			model.setStatus(Bean.getStatus());
			model.setDescription(Bean.getDescription());
			String productcode=Bean.getProductCode();
			productcode = productcode.substring(1);
			productcode = productcode.substring(1);
			productcode = productcode.substring(1);
			productcode = productcode.substring(1);
			Integer productseqno=Integer.parseInt(productcode);
			model.setProductSeqNo(productseqno);
			model.setProductCode(Bean.getProductCode());
			model.setProductName(Bean.getProductName());
			model.setUom(Bean.getUom());
			model.setStockMethodCode(Bean.getStockMethod());

			model.setSubCategoryCode(Bean.getProductSubGroup());
			model.setCategoryCode(Bean.getProductGroup());

			/*String psCode=Bean.getProductCode();
			psCode = psCode.substring(0, psCode.length() - 4);
			model.setSubCategoryCode(psCode);
			String pCode = psCode.substring(1);
			pCode = pCode.substring(1);*/


			return model;
		}
	 
	 public List<ProductMasterBean> prepareListofBeanForProductMaster(List<ProductMaster> productMasters)
		{
		 		List<ProductMasterBean> beans = null;
			if (productMasters != null && !productMasters.isEmpty())
			{
				beans = new ArrayList<ProductMasterBean>();
				ProductMasterBean bean = null;
				for (ProductMaster model : productMasters)
				{
					bean = new ProductMasterBean();
					bean.setModifyFlag("Yes");
					bean.setStatus(model.getStatus());
					bean.setProductCode(model.getProductCode());
					bean.setProductName(model.getProductName());
					bean.setUom(model.getUom());
					bean.setDescription(model.getDescription());
					bean.setStockMethod(model.getStockMethodCode());
					bean.setProductGroup(model.getCategoryCode());
					bean.setProductSubGroup(model.getSubCategoryCode());
					beans.add(bean);
				}
			}
			return beans;
		}
	 

	 public List getStockSupplyDetails(String fromdate, String todate,Integer department)
		 {        
			 return storeDao.getStockSupplyDetails(fromdate,todate, department);
		 }
	 public List getProductCodes(String productseq)
	 {        
		 return storeDao.getProductCodes(productseq);
	 }
	 public List getProductDetailsBycode(String productseq)
	 {        
		 return storeDao.getProductDetailsBycode(productseq);
	 }
	 
		 @SuppressWarnings("unchecked")
			public List<DepartmentMaster> getDeptName(Integer dept)
		    {
		        String sql = "select * from DepartmentMaster where deptCode ="+dept+"";
				//logger.info("---------sql---------------"+sql);
		        return hibernateDao.findByNativeSql(sql, DepartmentMaster.class);
		    }
		 public List getQuantityForStockInNo(String stockinno)
		 {        
			 return storeDao.getQuantityForStockInNo(stockinno);
		 }
		 
	
		 public StockInSummary prepareModelforStockInSummary(StockInSummaryBean stockInSummaryBean)
		 {
			 StockInSummary stockInSummary = new StockInSummary();
			 
			 String sDate=stockInSummaryBean.getStockDate();
			 Date stockDate = DateUtils.getSqlDateFromString(sDate, Constants.GenericDateFormat.DATE_FORMAT);
			 stockInSummary.setStockdate(stockDate);
			 storeDao.deleteDetails(stockInSummaryBean.getStockInNo(),"StockInDetails");
			 String finyeardata=stockInSummaryBean.getStockInNo();
			 String finyear[] = finyeardata.split("/");
			 String Fyear = finyear[1];
			 String sequenceno = finyear[0];

			 sequenceno = sequenceno.substring(1);
			 sequenceno =sequenceno.substring(1);
			 stockInSummary.setFinyr(Fyear);

			 Date date = new Date();
			 Date datendtime = new java.util.Date();
			 Integer seqno=Integer.parseInt(sequenceno);
			 stockInSummary.setSummaryTime(new Timestamp(datendtime.getTime()));
			 stockInSummary.setSeqno(seqno);
			 stockInSummary.setStockinno(stockInSummaryBean.getStockInNo());
			 String eDt=stockInSummaryBean.getEntryDate();
			 Date entrydt = DateUtils.getSqlDateFromString(eDt, Constants.GenericDateFormat.DATE_FORMAT);
			 stockInSummary.setEntrydt(entrydt);
			 //stockInSummary.setStocktransactionno("1");//have to append once stock tables are ready
			 String userId = getLoggedInUserName();
			 stockInSummary.setLoginid(userId);
			 stockInSummary.setTodeptid(stockInSummaryBean.getProductionDepartment());
			 stockInSummary.setFromdeptid(stockInSummaryBean.getDepartment());
			
			return stockInSummary;
			}	
		 
		 
		 public StockInDetails prepareModelforStockInDetails(StockInSummaryBean stockInSummaryBean,StockInDetailsBean stockInDetailsBean)
			{
			 StockInDetails stockInDetails = new StockInDetails();
			 String finyeardata=stockInSummaryBean.getStockInNo();
			 String finyear[] = finyeardata.split("/");
			 String Fyear = finyear[1];
		 	 stockInDetails.setFinyr(Fyear);
			 stockInDetails.setStockinno(stockInSummaryBean.getStockInNo());
			 stockInDetails.setProductcode(stockInDetailsBean.getProductCode());
			 stockInDetails.setUom(stockInDetailsBean.getUom());
			 stockInDetails.setBatchid(stockInDetailsBean.getBatchId());
			 stockInDetails.setExpdt(stockInDetailsBean.getExpDt());
			 stockInDetails.setQuantity(stockInDetailsBean.getQuantity());
			 stockInDetails.setMrp(stockInDetailsBean.getMrp());
			 
			 if(stockInDetailsBean.getCp()==null)
			 {
				 stockInDetails.setCp(stockInDetailsBean.getMrp());
			 }
			 else
			 {
				 stockInDetails.setCp(stockInDetailsBean.getCp());
			 }
			 stockInDetails.setFromdeptid(stockInSummaryBean.getDepartment());
			 String productcode=stockInDetailsBean.getProductCode();
			 List ProductList = stockService.getProductDls(productcode);
			 String gropucode="";
			 String subgropucode="";
				if(ProductList != null && ProductList.size()>0)
				{
					Map productMap = (Map) ProductList.get(0);
					gropucode = (String) productMap.get("categoryCode");
					subgropucode = (String) productMap.get("subCategoryCode");
				}
			 stockInDetails.setProductsubgrpcode(subgropucode);
			 stockInDetails.setProductgrpcode(gropucode);
			 return stockInDetails;
			}
		 
		 public List<StockInSummary> listAllStockInSummary(String stockInNo)
		 {        
			 return storeDao.listAllStockInSummary(stockInNo);
		 }	 
		 public List<StockInDetails> listAllStockInDetails(String stockInNo)
		 {        
			 return storeDao.listAllStockInDetails(stockInNo);
		 }	
		 

			public StockInSummaryBean prepareStockInSummaryBean(List<StockInSummary> stockInSummary)
			{
				StockInSummaryBean bean = null;
				if (stockInSummary != null)
				{
					bean = new StockInSummaryBean();
					bean.setFinYr(stockInSummary.get(0).getFinyr());
					Date sdate=stockInSummary.get(0).getStockdate();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String stockDate = df.format(sdate);
					bean.setStockDate(stockDate);
					Timestamp ts=stockInSummary.get(0).getSummaryTime();
					bean.setTime(ts.toString());
					bean.setSeqNo(stockInSummary.get(0).getSeqno());
					bean.setStockInNo(stockInSummary.get(0).getStockinno());
					Date edate=stockInSummary.get(0).getEntrydt();
					String entryDate = df.format(edate);
					bean.setEntryDate(entryDate);
					bean.setStockTransactionNo(stockInSummary.get(0).getStocktransactionno());
					bean.setLoginID(stockInSummary.get(0).getLoginid());
					bean.setProductionDepartment(stockInSummary.get(0).getTodeptid());
					bean.setDepartment(stockInSummary.get(0).getFromdeptid());
					bean.setModifyFlag("Yes");

					/*
						private String modifyFlag;
						private Integer department;
						private Integer productionDepartment;
					 */
				}
				return bean;
			}
			public List<StockInDetailsBean> prepareStockInDetails(List<StockInDetails> stockInDetails)
			{
				List<StockInDetailsBean> beans = null;
				if (stockInDetails != null && !stockInDetails.isEmpty())
				{
					beans = new ArrayList<StockInDetailsBean>();
					StockInDetailsBean bean = null;
					for (StockInDetails stockInDetail : stockInDetails)
					{
						bean = new StockInDetailsBean();
						bean.setId(stockInDetail.getId());
						bean.setFinYr(stockInDetail.getFinyr());
						bean.setProductCode(stockInDetail.getProductcode());
						bean.setUom(stockInDetail.getUom());
						
						bean.setExpDt(stockInDetail.getExpdt());
						bean.setQuantity(stockInDetail.getQuantity());
						bean.setMrp(stockInDetail.getMrp());
						bean.setCp(stockInDetail.getCp());
					
						bean.setBatchId(stockInDetail.getBatchid());
						
						String batchId = stockInDetail.getBatchid();
						
						bean.setBatchFlag("Old");
						List batchList = stockService.getBatchDetails(batchId);
						if(batchList != null && batchList.size()>0)
						{
							StoreBatchMaster storeBatchMaster = (StoreBatchMaster) batchList.get(0);
							bean.setBatch(storeBatchMaster.getBatch());
							bean.setBatchSeqNo(storeBatchMaster.getSeqno());
						}
						beans.add(bean);
					}
				}
				return beans;
			}
			
			

			public boolean saveandupdateMultipleEntities(List entitiesList,List UpdateList,String modifyFlag)
			{
				return storeDao.saveandupdateMultipleEntities(entitiesList,UpdateList,modifyFlag);
			}
			
			 public List listGetEmployeeType(String loginid)
			 {        
				 return storeDao.listGetEmployeeType(loginid);
			 }
			
			 public List getStockIndentDetails(String fromDate,String toDate, Integer department,Integer userstatus)
			 {        
				 return storeDao.getStockIndentDetails(fromDate,toDate,department,userstatus);
			 }
			 public List getQuantityForStockIndent(String stockReqNo)
			 {        
				 return storeDao.getQuantityForStockIndent(stockReqNo);
			 }
			
			 
			public StockReqSmry prepareModelforStockReqSmry(StockReqSmryBean stockReqSmryBean)
			{
				StockReqSmry stockReqSmry = new StockReqSmry();
				
				 String finyeardata=stockReqSmryBean.getStockReqNo();
				 
				 String finyear[] = finyeardata.split("/");
				 String Fyear = finyear[1];
				 String sequenceno = finyear[0];
				 sequenceno = sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);

				 Integer seqno=Integer.parseInt(sequenceno);

				 stockReqSmry.setFinyr(Fyear);
				 
				 storeDao.deleteDetailstable(stockReqSmryBean.getStockReqNo(),"StockReqDtls","stockreqno");

				 String rDate=stockReqSmryBean.getReqDate();
				 Date reqdate = DateUtils.getSqlDateFromString(rDate, Constants.GenericDateFormat.DATE_FORMAT);
				 stockReqSmry.setReqdate(reqdate);
				 Date date = new Date();
				 Date datendtime = new java.util.Date();
				 stockReqSmry.setReqtime(new Timestamp(datendtime.getTime()));
				 stockReqSmry.setSeqno(seqno);
				 stockReqSmry.setStockreqno(stockReqSmryBean.getStockReqNo());
				 stockReqSmry.setDeptid(stockReqSmryBean.getDeptID());
				 String userId = getLoggedInUserName();
				 stockReqSmry.setReqlogninid(userId);
				Integer status=0;
						List DateList = listGetEmployeeType(userId);
						logger.info("listGetEmployeeType() ========DateList==========" + DateList);
						if (DateList != null && DateList.size() > 0)
						{
							Map dateMap = new HashMap();
							dateMap = (Map) DateList.get(0);
							status = (Integer) dateMap.get("empStatus");
						}
				 stockReqSmry.setStatus(status);
				 stockReqSmry.setAuthoriseddt(reqdate);
				 stockReqSmry.setAuthorisedtime(new Timestamp(datendtime.getTime()));
				 stockReqSmry.setAuthoriseduser(stockReqSmryBean.getAuthorisedUser());
				 
				return stockReqSmry;
				}	
			 
			public StockReqDtls prepareModelforStockReqDtls(StockReqSmryBean stockReqSmryBean,StockReqDtlsBean stockReqDtlsBean)
			{
				StockReqDtls StockReqDtl = new StockReqDtls();
				
				 String finyeardata=stockReqSmryBean.getStockReqNo();
				 String finyear[] = finyeardata.split("/");
				 String Fyear = finyear[1];
				 StockReqDtl.setFinyr(Fyear);
				 StockReqDtl.setStockreqno(stockReqSmryBean.getStockReqNo());
				 StockReqDtl.setProductcode(stockReqDtlsBean.getProductCode());
				 String productcode=stockReqDtlsBean.getProductCode();
				 String gropucode="";
				 String subgropucode="";
				 List ProductList = stockService.getProductDls(productcode);
					if(ProductList != null && ProductList.size()>0)
					{
						Map productMap = (Map) ProductList.get(0);
						gropucode = (String) productMap.get("categoryCode");
						subgropucode = (String) productMap.get("subCategoryCode");
					}
				 StockReqDtl.setProductgrpcode(subgropucode);
				 StockReqDtl.setProductsubgroupcode(gropucode);

				StockReqDtl.setUom(stockReqDtlsBean.getUom());
				StockReqDtl.setPurpose(stockReqDtlsBean.getPurpose());
				StockReqDtl.setQuantity(stockReqDtlsBean.getQuantity());
				StockReqDtl.setIssuedqty(0.0);
				StockReqDtl.setPendingqty(stockReqDtlsBean.getQuantity());
				StockReqDtl.setDeptstock((stockReqDtlsBean.getDeptStock()));
				StockReqDtl.setCsstock((stockReqDtlsBean.getCsStock()));
				String userId = getLoggedInUserName();
					Integer status=0;
							List DateList = listGetEmployeeType(userId);
							logger.info("listGetEmployeeType() ========DateList==========" + DateList);
							if (DateList != null && DateList.size() > 0)
							{
								Map dateMap = new HashMap();
								dateMap = (Map) DateList.get(0);
								status = (Integer) dateMap.get("empStatus");
							}
				
				StockReqDtl.setStatus(status);
				StockReqDtl.setConsumption(stockReqDtlsBean.getConsumption());

				return StockReqDtl;
				}
			

			 public List<StockReqSmry> listAllStockReqSummary(String stockInNo)
			 {        
				 return storeDao.listAllStockReqSummary(stockInNo);
			 }	 
			 public List<StockReqDtls> listAllStockReqDetails(String stockInNo)
			 {        
				 return storeDao.listAllStockReqDetails(stockInNo);
			 }	
			 

			 public List<StockIssueSmry> listAllStockIssueSummary(String stockIssueNo)
			 {        
				 return storeDao.listAllStockIssueSummary(stockIssueNo);
			 }	 
			 public List<StockIssueDtls> listAllStockIssueDetails(String stockIssueNo)
			 {        
				 return storeDao.listAllStockIssueDetails(stockIssueNo);
			 }
			 
			 
			 public StockIssueSmryBean prepareStockIssueSummaryBean(List<StockIssueSmry> stockIssueSmry)
				{
				 StockIssueSmryBean bean = null;
					if (stockIssueSmry != null && !stockIssueSmry.isEmpty())
					{
						bean = new StockIssueSmryBean();
						Date sdate=stockIssueSmry.get(0).getIssuedate();
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String issuedate = df.format(sdate);
						bean.setIssueDate(issuedate);
						bean.setStockIssueNo(stockIssueSmry.get(0).getStockissueno());
						bean.setStockReqNo(stockIssueSmry.get(0).getStockreqno());
						bean.setStatus(stockIssueSmry.get(0).getStatus());
						bean.setLoginId(stockIssueSmry.get(0).getLoginid());
						bean.setStockTransactionNo(stockIssueSmry.get(0).getStockTransactionNo());
						bean.setModifFlag("Yes");
						bean.setDeptId(stockIssueSmry.get(0).getDeptid());

					}
					return bean;
				}
				public List<StockIssueDtlsBean> prepareStockIssueDetails(List<StockIssueDtls> stockIssueDtls,String StrReqNo)
				{
					List<StockIssueDtlsBean> beans = null;
					if (stockIssueDtls != null && !stockIssueDtls.isEmpty())
					{
						Integer i=0;
						beans = new ArrayList<StockIssueDtlsBean>();
						StockIssueDtlsBean bean = null;
						for (StockIssueDtls stockIssueDtl : stockIssueDtls)
						{
							i++;
							
							bean = new StockIssueDtlsBean();
							bean.setId(i);
							bean.setProductCode(stockIssueDtl.getProductcode());
							

							bean.setUom(stockIssueDtl.getUom());
							 List DetailsList = storeDao.listGetIndentDetails(StrReqNo,stockIssueDtl.getProductcode());
							 Double reqpenqty=0.0;
								if(DetailsList != null && DetailsList.size()>0)
								{
									
									Map dateMap = new HashMap();
									 dateMap = (Map) DetailsList.get(0);
									 reqpenqty = (Double) dateMap.get("pendingqty");
									
								}
								Double totissuedqty=0.0;
								 List PendingDetailsList = storeDao.getTotalQuantityForStockIssueByProductcode(stockIssueDtl.getStockissueno(),stockIssueDtl.getProductcode());
								 if(PendingDetailsList != null && PendingDetailsList.size()>0)
									{
									 for (int j = 0; j < PendingDetailsList.size(); j++)
										{
										
										Map dateMap = new HashMap();
										 dateMap = (Map) PendingDetailsList.get(j);
										double issuedqty = (Double) dateMap.get("issuedqty");
										totissuedqty=totissuedqty+issuedqty;
										}
										
									}
								
							Double pendingqty=totissuedqty+reqpenqty;
							List batchList = stockService.getBatchDetails(stockIssueDtl.getBatchid());
							if(batchList != null && batchList.size()>0)
							{
								StoreBatchMaster storeBatchMaster = (StoreBatchMaster) batchList.get(0);
								bean.setBatch(storeBatchMaster.getBatch());
								bean.setBatchSeqNo(storeBatchMaster.getSeqno());
							}
							bean.setPendingQty(pendingqty);
							bean.setIssuedQty(stockIssueDtl.getIssuedqty());
							bean.setBatchId(stockIssueDtl.getBatchid());
							bean.setBatchQty(stockIssueDtl.getBatchQty());
							bean.setStatus(stockIssueDtl.getStatus());
							bean.setMrp(stockIssueDtl.getMrp());
							beans.add(bean);
						}
					}
					return beans;
				}
			 
		public StockIssueSmry prepareModelforStockIssueSmry(StockIssueSmryBean stockIssueSmryBean)
		{	
			StockIssueSmry stockIssueSmry = new StockIssueSmry();
			String finyeardata=stockIssueSmryBean.getStockIssueNo();
			 String finyear[] = finyeardata.split("/");
			 String Fyear = finyear[1];
			 stockIssueSmry.setFinyr(Fyear);
			 String userId = getLoggedInUserName();
			 Integer status=2;
			storeDao.deleteDetailstable(finyeardata,"StockIssueDtls","stockissueno");

			 String sequenceno = finyear[0];
			 sequenceno = sequenceno.substring(1);
			 sequenceno =sequenceno.substring(1);
			 Integer seqno=Integer.parseInt(sequenceno);
			 stockIssueSmry.setSeqno(seqno);
			String iDate=stockIssueSmryBean.getIssueDate();
			Date issuedate = DateUtils.getSqlDateFromString(iDate, Constants.GenericDateFormat.DATE_FORMAT);
			stockIssueSmry.setIssuedate(issuedate);
			Date datendtime = new java.util.Date();
			stockIssueSmry.setIssuetime(new Timestamp(datendtime.getTime()));
			stockIssueSmry.setStockissueno(stockIssueSmryBean.getStockIssueNo());
			stockIssueSmry.setStockreqno(stockIssueSmryBean.getStockReqNo());
			stockIssueSmry.setDeptid(stockIssueSmryBean.getDeptId());
			stockIssueSmry.setLoginid(userId);
			stockIssueSmry.setStatus(1);//issued
			stockIssueSmry.setGrnno("NA");
			/*
			 private Integer id;
			private String modifyFlag;
			 */
			return stockIssueSmry;
		}	
		
		public StockIssueDtls prepareModelforStockIssueDtls(StockIssueSmryBean stockIssueSmryBean,StockIssueDtlsBean stockIssueDtlsBean)
		{
			StockIssueDtls StockIssueDtl = new StockIssueDtls();
			 
			StockIssueDtl.setFinyr(stockIssueDtlsBean.getFinYr());
			String finyeardata=stockIssueSmryBean.getStockIssueNo();

			 String finyear[] = finyeardata.split("/");
			 String Fyear = finyear[1];
			 StockIssueDtl.setFinyr(Fyear);
			StockIssueDtl.setStockissueno(stockIssueSmryBean.getStockIssueNo());
			StockIssueDtl.setProductcode(stockIssueDtlsBean.getProductCode());
			String productcode=stockIssueDtlsBean.getProductCode();
			 String gropucode="";
			 String subgropucode="";
			 List ProductList = stockService.getProductDls(productcode);
				if(ProductList != null && ProductList.size()>0)
				{
					Map productMap = (Map) ProductList.get(0);
					gropucode = (String) productMap.get("categoryCode");
					subgropucode = (String) productMap.get("subCategoryCode");
				}
			StockIssueDtl.setProductgrpcode(gropucode);
			StockIssueDtl.setProductsubgroupcode(subgropucode);
			StockIssueDtl.setUom(stockIssueDtlsBean.getUom());
			StockIssueDtl.setQuantity(stockIssueDtlsBean.getIssuedQty());
			StockIssueDtl.setIssuedqty(stockIssueDtlsBean.getIssuedQty());
			Double pendingqty=stockIssueDtlsBean.getPendingQty()-stockIssueDtlsBean.getIssuedQty();
			StockIssueDtl.setPendingqty(pendingqty);
			StockIssueDtl.setBatchid(stockIssueDtlsBean.getBatchId());
			StockIssueDtl.setMrp(stockIssueDtlsBean.getMrp());
			StockIssueDtl.setBatchQty(stockIssueDtlsBean.getBatchQty());

			StockIssueDtl.setStatus(1);//issued
				
			return StockIssueDtl;
			}
		 public List getStockIssueDtls(String fromdate, String todate,Integer department)
		 {        
				 return storeDao.getStockIssueDtls(fromdate,todate, department);
		 }
		 public List getQuantityForStockIssue(String StockIssueNo)
		 {        
			 return storeDao.getQuantityForStockIssue(StockIssueNo);
		 }
		 public String getLoggedInUserName()
			{
				HttpSession session = request.getSession(false);
				Employees employee = (Employees) session.getAttribute(Constants.USER);
				String userName = employee.getLoginid();
				return userName;
			}
			public GrnSummary prepareModelforGrnSummary(GrnSummaryBean grnSummaryBean)
			{
				GrnSummary grnSummary = new GrnSummary();
				String finyeardata=grnSummaryBean.getGrnNo();
				String finyear[] = finyeardata.split("/");
				String Fyear = finyear[1];
				grnSummary.setFinyr(Fyear);
				 
				storeDao.deleteDetailstable(grnSummaryBean.getGrnNo(),"GrnDetails","grnno");

				 String grnDate=grnSummaryBean.getGrnDate();
				 Date grndate = DateUtils.getSqlDateFromString(grnDate, Constants.GenericDateFormat.DATE_FORMAT);
				 grnSummary.setGrndate(grndate);
				 Date datendtime = new java.util.Date();
				 grnSummary.setGrntime(new Timestamp(datendtime.getTime()));
				 
				 String sequenceno = finyear[0];
				 sequenceno = sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);
				 sequenceno = sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);
				 sequenceno =sequenceno.substring(1);
				 Integer seqno=Integer.parseInt(sequenceno);
				 grnSummary.setSeqno(seqno);
				 grnSummary.setGrnno(grnSummaryBean.getGrnNo());
				 grnSummary.setStockissueno(grnSummaryBean.getStockIssueNo());
				 grnSummary.setLogninid(grnSummaryBean.getLoginId());
				 grnSummary.setDeptid(grnSummaryBean.getDeptId());
				/*private Integer id;
					private String modifyFlag;
					*/
				return grnSummary;
				}	
			public GrnDetails prepareModelforGrnDetails(GrnSummaryBean grnSummaryBean,GrnDetailsBean grnDetailsBean)
			{
				GrnDetails grnDetails = new GrnDetails();
				
				grnDetails.setGrnno(grnSummaryBean.getGrnNo());
				String finyeardata=grnSummaryBean.getGrnNo();
				String finyear[] = finyeardata.split("/");
				String Fyear = finyear[1];
				grnDetails.setFinyr(Fyear);
				grnDetails.setProductcode(grnDetailsBean.getProductCode());
				String productcode=grnDetailsBean.getProductCode();
				 String gropucode="";
				 String subgropucode="";
				 List ProductList = stockService.getProductDls(productcode);
					if(ProductList != null && ProductList.size()>0)
					{
						Map productMap = (Map) ProductList.get(0);
						gropucode = (String) productMap.get("categoryCode");
						subgropucode = (String) productMap.get("subCategoryCode");
					}
					grnDetails.setProductgrpcode(gropucode);
				grnDetails.setProductsubgroupcode(subgropucode);
						
				grnDetails.setUom(grnDetailsBean.getUom());
				grnDetails.setCause(grnDetailsBean.getCause());
				grnDetails.setIssuedqty(grnDetailsBean.getIssuedQty());
				grnDetails.setAcceptedqty(grnDetailsBean.getAcceptedQty());
				grnDetails.setRejectedqty(grnDetailsBean.getRejectedQty());
				grnDetails.setMrp(grnDetailsBean.getMrp());
				 String MfrDate=grnDetailsBean.getMfgDate();
				 Date grndate = DateUtils.getSqlDateFromString(MfrDate, Constants.GenericDateFormat.DATE_FORMAT);
				 grnDetails.setMfgDate(grndate);
				grnDetails.setBatchid(grnDetailsBean.getBatchId());
				/*
				private Integer id;
				 */
				return grnDetails;
				}
			 public List getGrnsDtls(String fromdate, String todate,Integer department)
			 {        
					 return storeDao.getGrnsDtls(fromdate,todate, department);
			 }	
			 public List getQuantityForGrn(String grnNo)
			 {        
				 return storeDao.getQuantityForGrn(grnNo);
			 }
			 public List<GrnSummary> listAllGrnSummary(String grnNo)
			 {        
				 return storeDao.listAllGrnSummary(grnNo);
			 }	
			 public GrnSummaryBean prepareGrnSummaryBean(List<GrnSummary> grnSummary)
			 {
				 GrnSummaryBean bean = null;
				 if (grnSummary != null)
				 {
					 bean = new GrnSummaryBean();
					 bean.setFinYr(grnSummary.get(0).getFinyr());
					 Date sdate=grnSummary.get(0).getGrndate();
					 DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					 String grnDate = df.format(sdate);
					 bean.setGrnDate(grnDate);
					 Timestamp ts=grnSummary.get(0).getGrntime();
					 bean.setGrnTime(ts.toString());
					 bean.setGrnNo(grnSummary.get(0).getGrnno());
					 bean.setSeqNo(grnSummary.get(0).getSeqno());
					 bean.setDeptId(grnSummary.get(0).getDeptid());
					 bean.setLoginId(grnSummary.get(0).getLogninid());
					 bean.setStockIssueNo(grnSummary.get(0).getStockissueno());
					 bean.setModifyFlag("Yes");
				 }
				 return bean;
			 }
			 
			 public List<GrnDetails> listAllGrnDetails(String grnNo)
			 {        
				 return storeDao.listAllGrnDetails(grnNo);
			 }	
			 public List<GrnDetailsBean> prepareGrnDetails(List<GrnDetails> grnDetails)
				{
					List<GrnDetailsBean> beans = null;
					if (grnDetails != null && !grnDetails.isEmpty())
					{
						beans = new ArrayList<GrnDetailsBean>();
						GrnDetailsBean bean = null;
						for (GrnDetails grnDetail : grnDetails)
						{
							bean = new GrnDetailsBean();
							bean.setId(grnDetail.getId());
							bean.setFinYr(grnDetail.getFinyr());
							bean.setGrnNo(grnDetail.getGrnno());
							bean.setProductCode(grnDetail.getProductcode());
							bean.setProductGrpCode(grnDetail.getProductgrpcode());
							bean.setProductSubgroupCode(grnDetail.getProductsubgroupcode());
							bean.setUom(grnDetail.getUom());
							bean.setCause(grnDetail.getCause());
							bean.setBatchId(grnDetail.getBatchid());
							bean.setIssuedQty(grnDetail.getIssuedqty());
							bean.setAcceptedQty(grnDetail.getAcceptedqty());
							bean.setRejectedQty(grnDetail.getRejectedqty());
							bean.setMrp(grnDetail.getMrp());
							beans.add(bean);
						}
					}
					return beans;
				}
				
					public boolean saveMultipleEntities(List entitiesList,List UpdateList,List AccountList)
					{
						return storeDao.saveMultipleEntities(entitiesList,UpdateList,AccountList);
					}
					
					 public StockReqSmryBean prepareStockIndentSummaryBean(List<StockReqSmry> stockReqSmry)
						{
						 StockReqSmryBean bean = null;
							if (stockReqSmry != null && !stockReqSmry.isEmpty())
							{
								bean = new StockReqSmryBean();
								Date sdate=stockReqSmry.get(0).getReqdate();
								DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
								String ReqDate = df.format(sdate);
								bean.setReqDate(ReqDate);
								bean.setStockReqNo(stockReqSmry.get(0).getStockreqno());
								bean.setStatus(stockReqSmry.get(0).getStatus());
								bean.setAuthorisedUser(stockReqSmry.get(0).getAuthoriseduser());
								bean.setModifyFlag("Yes");
								bean.setDeptID(stockReqSmry.get(0).getDeptid());

							}
							return bean;
						}
						public List<StockReqDtlsBean> prepareStockIndentDetails(List<StockReqDtls> stockReqDtls)
						{
							List<StockReqDtlsBean> beans = null;
							if (stockReqDtls != null && !stockReqDtls.isEmpty())
							{
								Integer i=0;
								beans = new ArrayList<StockReqDtlsBean>();
								StockReqDtlsBean bean = null;
								for (StockReqDtls stockReqDtl : stockReqDtls)
								{
									i++;
									
									bean = new StockReqDtlsBean();
									bean.setId(i);
									bean.setProductCode(stockReqDtl.getProductcode());
									bean.setUom(stockReqDtl.getUom());
									bean.setPurpose(stockReqDtl.getPurpose());
									bean.setQuantity(stockReqDtl.getQuantity());
									bean.setDeptStock(stockReqDtl.getDeptstock());
									bean.setCsStock(stockReqDtl.getCsstock());
									bean.setStatus(stockReqDtl.getStatus());
									bean.setPendingQty(stockReqDtl.getPendingqty());
									bean.setStockReqNo(stockReqDtl.getStockreqno());
									bean.setConsumption(stockReqDtl.getConsumption());

									beans.add(bean);
									
									
								}
							}
							return beans;
						}
					
	public SDCSummary prepareModeForSDCSummary(DcSummaryBean dcSummaryBean,String finYear)
	{
		SDCSummary sDCSummary = new SDCSummary();
		
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		sDCSummary.setDeptid(dcSummaryBean.getDepartment());
		sDCSummary.setFinyr(finYear);
		sDCSummary.setLoginid(userName);
		sDCSummary.setSalesinvno(dcSummaryBean.getSaleInvoiceNo());
		
		String dcDate = dcSummaryBean.getDcdate();
		Date challanDate = DateUtils.getSqlDateFromString(dcDate, Constants.GenericDateFormat.DATE_FORMAT);
		sDCSummary.setSdcdate(challanDate);
		sDCSummary.setSdcno(dcSummaryBean.getDcnumber());
		sDCSummary.setSeqno(dcSummaryBean.getDcSeqNo());
		sDCSummary.setStatus((byte) 0);
		
		Time dcTime = DateUtils.getCurrentSystemTime();
		sDCSummary.setSdctime(dcTime);
		
		sDCSummary.setAddress(dcSummaryBean.getAddress());
		sDCSummary.setCustomername(dcSummaryBean.getCustomername());
		sDCSummary.setContactnumber(dcSummaryBean.getContactnumber());
		
		//Added by DMurty on 23-03-2017
		sDCSummary.setDiscountamount(dcSummaryBean.getDiscountAmount());
		sDCSummary.setDiscountpercentage(dcSummaryBean.getDiscountPercent());
		sDCSummary.setTotalcost(dcSummaryBean.getTotalCost());
		sDCSummary.setNetamount(dcSummaryBean.getNetAmount());
		sDCSummary.setDisctype(dcSummaryBean.getDiscType());
		return sDCSummary;
	}	
					
	public SDCDetails prepareModeForSDCDetails(DcSummaryBean dcSummaryBean,String finYear,DcBean dcBean)
	{
		SDCDetails sDCDetails = new SDCDetails();
		
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		
		sDCDetails.setBatchid(dcBean.getBatchId());
		sDCDetails.setFinyr(finYear);
		
		String gropucode = null;
		String subgropucode=null;
		List ProductList = stockService.getProductDls(dcBean.getProductCode());
		if(ProductList != null && ProductList.size()>0)
		{
			Map productMap = (Map) ProductList.get(0);
			gropucode = (String) productMap.get("categoryCode");
			subgropucode = (String) productMap.get("subCategoryCode");
		}
		
		sDCDetails.setGroupcode(gropucode);
		sDCDetails.setProductcode(dcBean.getProductCode());
		sDCDetails.setQty(dcBean.getQuantity());
		sDCDetails.setSdcno(dcSummaryBean.getDcnumber());
		sDCDetails.setSubgroupcode(subgropucode);
		sDCDetails.setTotalcost(dcBean.getTotalCost());
		sDCDetails.setUom(dcBean.getUom());
		//Added by DMurty on 23-03-2017
		sDCDetails.setDiscountamount(dcBean.getDiscountAmount());
		sDCDetails.setNetamount(dcBean.getNetAmount());
		
		return sDCDetails;
	}	
					
	public DcSummaryBean prepareDcSummaryBean(List<SDCSummary> sDCSummary)
	{
		DcSummaryBean bean = null;
		if (sDCSummary != null)
		{
			bean = new DcSummaryBean();
			
			bean.setAddress(sDCSummary.get(0).getAddress());
			bean.setContactnumber(sDCSummary.get(0).getContactnumber());
			bean.setCustomername(sDCSummary.get(0).getCustomername());
			
			Date dcDate = sDCSummary.get(0).getSdcdate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String dtrDcDate = df.format(dcDate);
			
			bean.setDateOfEntry(dtrDcDate);
			bean.setDcdate(dtrDcDate);
			bean.setDcnumber(sDCSummary.get(0).getSdcno());
			bean.setDcSeqNo(sDCSummary.get(0).getSeqno());
			bean.setDepartment(sDCSummary.get(0).getDeptid());
			bean.setModifyFlag("Yes");
			bean.setTransactioncode(sDCSummary.get(0).getTransactioncode());
			
			//Added by DMurty on 23-03-2017
			bean.setSaleInvoiceNo(sDCSummary.get(0).getSalesinvno());
			bean.setTotalCost(sDCSummary.get(0).getTotalcost());
			bean.setDiscountPercent(sDCSummary.get(0).getDiscountpercentage());
			bean.setDiscountAmount(sDCSummary.get(0).getDiscountamount());
			bean.setNetAmount(sDCSummary.get(0).getNetamount());
			bean.setDiscType(sDCSummary.get(0).getDisctype());
		}
		return bean;
	}			
	public List<DcBean> prepareDcDetails(List<SDCDetails> sDCDetailss)
	{
		List<DcBean> beans = null;
		if (sDCDetailss != null && !sDCDetailss.isEmpty())
		{
			Integer i=0;
			beans = new ArrayList<DcBean>();
			DcBean bean = null;
			for (SDCDetails sDCDetails : sDCDetailss)
			{
				bean = new DcBean();
				i++;
				String batchId = sDCDetails.getBatchid();
				List batchList = stockService.getBatchDetails(batchId);
				if(batchList != null && batchList.size()>0)
				{
					
					StoreBatchMaster storeBatchMaster = (StoreBatchMaster) batchList.get(0);
					
					bean.setProductCode(storeBatchMaster.getProductcode());
					
					String productNmae = null;
					List ProductList = stockService.getProductDls(storeBatchMaster.getProductcode());
					if(ProductList != null && ProductList.size()>0)
					{
						Map productMap = (Map) ProductList.get(0);
						productNmae = (String) productMap.get("productName");
						bean.setProductName(productNmae);
					}
					bean.setUom(sDCDetails.getUom());
					bean.setBatch(storeBatchMaster.getBatch());
					bean.setExpDt(storeBatchMaster.getExpdt());
					bean.setMrp(storeBatchMaster.getMrp());
					
					Date batchDate = storeBatchMaster.getBatchdate();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strbatchDate = df.format(batchDate);
					bean.setStockInDate(strbatchDate);
					
					bean.setQuantity(sDCDetails.getQty());
					bean.setTotalCost(sDCDetails.getTotalcost());
					bean.setId(i);
					
					bean.setBatchSeqNo(storeBatchMaster.getSeqno());
					bean.setBatchFlag("Old");
					bean.setCp(storeBatchMaster.getCp());
					
					bean.setDiscountAmount(sDCDetails.getDiscountamount());
					bean.setNetAmount(sDCDetails.getNetamount());
					bean.setBatchId(batchId);
					//Added by DMurty on 22-03-2017
					int uomCode = sDCDetails.getUom();
					String uomName=null;
					List UomList = stockService.getUomDetails(uomCode);
					if(UomList != null && UomList.size()>0)
					{
						Map uomMap = (Map) UomList.get(0);
						uomName = (String) uomMap.get("uom");
					}
					bean.setUomName(uomName);
					
					beans.add(bean);
				}
			}
		}
		return beans;
	}
	
	//Added by DMurty on 21-03-2017
	 public List<SDCSummary> listAllSDCSummary(String dcNo)
	 {        
		 return storeDao.listAllSDCSummary(dcNo);
	 }
	 public List<SDCDetails> listAllSDcDetails(String dcNo)
	 {        
		 return storeDao.listAllSDcDetails(dcNo);
	 }			
	 public List getSDCDetails(String fromDate,String toDate, Integer department)
	 {        
		 return storeDao.getSDCDetails(fromDate,toDate,department);
	 }			 
	 
	 public List getQuantityForDcDetails(String sdcno)
	 {        
		 return storeDao.getQuantityForDcDetails(sdcno);
	 }
	 public List getIndentDetailsForPrint(String requestno)
	 {        
		 return storeDao.getIndentDetailsForPrint(requestno);
	 }
	 			
			 
}
