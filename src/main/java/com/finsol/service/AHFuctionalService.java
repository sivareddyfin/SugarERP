package com.finsol.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.AHFunctionalDao;
import com.finsol.dao.ExtentDetailsDao;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccountSummaryTemp;
import com.finsol.model.AdvanceDetails;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementDetails;
import com.finsol.model.AgreementSummary;
import com.finsol.model.BatchServicesMaster;
import com.finsol.model.Branch;
import com.finsol.model.Circle;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.CropTypes;
import com.finsol.model.DedDetails_temp;
import com.finsol.model.ExtentDetails;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;
import com.finsol.model.GenerateRankingTemp;
import com.finsol.model.LabRecommendations;
import com.finsol.model.LabRecommendations_temp;
import com.finsol.model.LabourContractor;
import com.finsol.model.LoanDetails;
import com.finsol.model.LoanPrincipleAmounts;
import com.finsol.model.LoanSummary;
import com.finsol.model.Mandal;
import com.finsol.model.PermitDetails;
import com.finsol.model.PermitRules;
import com.finsol.model.ProgramSummary;
import com.finsol.model.ProgramVariety;
import com.finsol.model.RandDAgreementDetails;
import com.finsol.model.RandDAgreementSummary;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;
import com.finsol.model.SampleCards;
import com.finsol.model.SampleCardsTemp;
import com.finsol.model.SeedAdvanceCost;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.SeedlingTrayChargeSummary;
import com.finsol.model.SeedlingTrayChargesDetails;
import com.finsol.model.SeedlingTrayDetails;
import com.finsol.model.Temp_BatchServiceMaster;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.model.WeighBridgeTestData;
import com.finsol.model.WeighmentDetails;
import com.finsol.model.Zone;

/**
 * @author Rama Krishna
 *
 */
@Service("AHFuctionalService")
public class AHFuctionalService {
	
	private static final Logger logger = Logger.getLogger(AHFuctionalService.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	@Autowired	
	private ExtentDetailsDao extentDetailsDao;
	
	@Autowired	
	private AHFunctionalDao ahFunctionalDao;
	
	

	public JSONArray getDetails(String ryotCode) throws Exception
	{
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		List detailLst=null;
		try{
		
			String sqlQuery="select a.ryotname,a.ryotcode,b.village,a.villagecode from ryot a,village b where a.villagecode=b.villagecode and a.ryotcode='"+ryotCode+"'";
			
			detailLst=hibernateDao.findBySql(sqlQuery);
			
				for (Iterator it = detailLst.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					
					String ryotName = (String) myResult[0];
					String ryotcd = (String) myResult[1];
					String villageName = (String) myResult[2];
					String villageCode=(String) myResult[3];
					
					jsonObj.put("ryotName", ryotName);
					jsonObj.put("ryotCode", ryotcd);
					jsonObj.put("villageName", villageName);
					jsonObj.put("villageCode", villageCode);
					jsonObj.put("id", "1");
					
					jArray.add(jsonObj);
				}
				logger.info("==========jarray========="+jArray.toString());
			return jArray;
		
		
		}
		 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return null;
	        }
		
	}
	
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotCode)
    {
		
        return ahFunctionalDao.getExtentDetailsByRyotCode(ryotCode);
    }
	
	public List<LabRecommendations> getLabRecommendationsByRyotCode(String ryotCode)
    {	
		
        return ahFunctionalDao.getLabRecommendationsByRyotCode(ryotCode);
    }
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addExtentDetails(ExtentDetails extentDetails)
	{
		ahFunctionalDao.addExtentDetails(extentDetails);
	}
	
	public void deleteExtentDetails(String ryotCode) 
	{
		ahFunctionalDao.deleteExtentDetails(ryotCode);
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLabTestResults_Temp(LabRecommendations_temp labRecommendations_temp )
	{
		ahFunctionalDao.addLabRecommendations_Temp(labRecommendations_temp);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLabTestResults(LabRecommendations labRecommendations )
	{
		ahFunctionalDao.addLabRecommendations(labRecommendations);
	}
	
	public void updateExtedDetails(Byte cultivation,String ryotcode,Integer plotno)
	{
		ahFunctionalDao.updateExtedDetails(cultivation,ryotcode,plotno);
	}
	
	public void deleteLabRecomendations(String ryotCode)
	{
		ahFunctionalDao.deleteLabRecomendations(ryotCode);
	}
	
	public void deleteLoanPrincipleAmounts(String ryotCode,String season,Integer branchId,Integer loanNumber)
	{
		ahFunctionalDao.deleteLoanPrincipleAmounts(ryotCode,season,branchId,loanNumber);
	}
	
	
	
	
	public List<LabRecommendations_temp> getLabRecomendationsTempByRyotCode(String ryotCode,String plotno,String surveyno)
    {
		
        return ahFunctionalDao.getLabRecomendationsTempByRyotCode(ryotCode,plotno,surveyno);
    }
	
	public List<LabRecommendations_temp> getLabRecomendationsFromRecomendations(String ryotCode,String plotno,String surveyno)
    {
		
        return ahFunctionalDao.getLabRecomendationsFromRecomendations(ryotCode,plotno,surveyno);
    }
	
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotCode,Integer plotno,String surveyno,String Screen)
    {
        return ahFunctionalDao.getExtentDetailsByRyotCode(ryotCode,plotno,surveyno,Screen);
    }
	
	public void deleteLabRecomendations_temp(String ryotCode,String plotno,String surveyno) {
		ahFunctionalDao.deleteLabRecomendations_temp(ryotCode,plotno,surveyno);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeedlingTrayChargeSummary(SeedlingTrayChargeSummary seedlingTrayChargeSummary) 
	{
		ahFunctionalDao.addSeedlingTrayChargeSummary(seedlingTrayChargeSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeedlingTrayChargeDtls(SeedlingTrayChargesDetails seedlingTrayChargesDetails) 
	{
		ahFunctionalDao.addSeedlingTrayChargeDtls(seedlingTrayChargesDetails);
	}
	
	public void deleteSeedlingTrayCharges(SeedlingTrayChargeSummary seedlingTrayChargeSummary) 
	{
		ahFunctionalDao.deleteSeedlingTrayCharges(seedlingTrayChargeSummary);
	}
	
	public Integer GetMaxTransCode()
    {
		Integer maxVal=(Integer) ahFunctionalDao.GetMaxTransCode("select max(transactioncode) from AccountDetails");
		if(maxVal==null)
		{
			maxVal=1;
		}
		else
		{
			maxVal = maxVal+1;
		}
		return maxVal;
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountDtls(AccountDetails accountDetails) 
	{
		ahFunctionalDao.addAccountDtls(accountDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSmry(AccountSummary accountSummary) 
	{
		ahFunctionalDao.addAccountSmry(accountSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountGrpDtls(AccountGroupDetails accountGroupDetails) 
	{
		ahFunctionalDao.addAccountGrpDtls(accountGroupDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountGrpSmry(AccountGroupSummary accountGroupSummary) 
	{
		ahFunctionalDao.addAccountGrpSmry(accountGroupSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSubGrpDtls(AccountSubGroupDetails accountSubGroupDetails) 
	{
		ahFunctionalDao.addAccountSubGrpDtls(accountSubGroupDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSubGrpSmry(AccountSubGroupSummary accountSubGroupSummary) 
	{
		ahFunctionalDao.addAccountSubGrpSmry(accountSubGroupSummary);
	}
	
	public Integer GetAccGrpCode(String AccCode)
    {
		Integer AccGrpCode=(Integer) ahFunctionalDao.GetAccGrpCode("select accountgroupcode from AccountMaster where accountcode='"+AccCode+"'");
        return AccGrpCode;
    }
	
	public Integer GetAccSubGrpCode(String AccCode)
    {
		Integer AccSubGrpCode=(Integer) ahFunctionalDao.GetAccSubGrpCode("select accountsubgroupcode from AccountMaster where accountcode='"+AccCode+"'");
        return AccSubGrpCode;
    }
	
	public Double GetUDCAmount(String season,String ryotCode)
    {
		Double udcAmount=(Double) ahFunctionalDao.GetAccSubGrpCode("select udcamount from UCDetailsByRyot where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return udcAmount;
    }
	public Double GetCDCAmount(String season,String ryotCode)
    {
		Double cdccAmount=(Double) ahFunctionalDao.GetAccSubGrpCode("select cdcamount from CDCDetailsByRyot where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return cdccAmount;
    }
	public Double GetTransportAmount(String season,String ryotCode)
    {
		Double transport=(Double) ahFunctionalDao.GetAccSubGrpCode("select totalamount from TranspChgSmryByRyotContAndDate where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return transport;
    }
	public Double GetAdvanceAmount(String season,String ryotCode)
    {
		Double advanceAmount=(Double) ahFunctionalDao.GetAccSubGrpCode("select pendingamount from AdvanceSummary where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return advanceAmount;
    }
	public Double GetICPAmount(String season,String ryotCode)
    {
		Double icpAmount=(Double) ahFunctionalDao.GetAccSubGrpCode("select icpamount from ICPDetailsByRyot where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return icpAmount;
    }
	public Double GetHarvestAmountPayble(String season,String ryotCode)
    {
		Double harvAmountPay=(Double) ahFunctionalDao.GetAccSubGrpCode("select PendingAmount from HarvChgSmryByRyotAndCont where season='"+season+"' and ryotcode='"+ryotCode+"'");
        return harvAmountPay;
    }
	
	public Double GetCanePrice(String season)
    {
		Double canePrice=(Double) ahFunctionalDao.GetAccSubGrpCode("select distinct netamt from CaneAcctAmounts where season='"+season+"' and pricename='FRP'");
        return canePrice;
    }
	
	
	public SeedlingTrayChargeSummary getSeedlingTrayCharges(String Season,String RyotCode)
	{
		return ahFunctionalDao.getSeedlingTrayCharges(Season,RyotCode);
	}
	
	public List<SeedlingTrayChargesDetails> getSeedlingTrayChargesDetails(Integer returnId)
    {
        return ahFunctionalDao.getSeedlingTrayChargesDetails(returnId);
    }

	public List BalDetails(String AccCode,String season)
	{
		return ahFunctionalDao.BalDetails(AccCode,season);
	}
	
	
	public Integer GetTransactionCodefromDtlsCode(int RetId)
    {
		Integer TransId=(Integer) ahFunctionalDao.GetTransactionCodefromDtlsCode("select transactioncode from SeedlingTrayChargesDetails where returnid="+RetId+"");
		return TransId;
    }
	public List AccDetailsForRevert(int Transcode)
	{
		return ahFunctionalDao.AccDetailsForRevert(Transcode);
	}
	
	public void updateRunningBalinSmry(double finalAmt,String strCrDr,String AccCode)
    {
		ahFunctionalDao.updateRunningBalinSmry(finalAmt,strCrDr,AccCode);
    }
	
	public void updateStatusForAccDtls(int TransactionCode)
    {
		ahFunctionalDao.updateStatusForAccDtls(TransactionCode);
    }
	
	public List AccSubGrpDetailsForRevert(int Transcode)
	{
		return ahFunctionalDao.AccSubGrpDetailsForRevert(Transcode);
	}
	

	public List SubGrpBalDetailsForRevert(int Transcode)
	{
		return ahFunctionalDao.SubGrpBalDetailsForRevert(Transcode);
	}
	
	public List SubGrpBalDetails(int AccGrpCode,int AccSubGrpCode,String season)
	{
		return ahFunctionalDao.SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
	}
	
	public void updateRunningBalinAccSubGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,int AccSubGrpCode)
    {
		ahFunctionalDao.updateRunningBalinAccSubGrpSmry(finalAmt,strCrDr,AccGrpCode,AccSubGrpCode);
    }
	
	
	public void updateStatusForAccSubGrpDtls(int TransactionCode)
    {
		ahFunctionalDao.updateStatusForAccSubGrpDtls(TransactionCode);
    }
	
	
	public List AccGrpBalDetailsForRevert(int Transcode)
	{
		return ahFunctionalDao.AccGrpBalDetailsForRevert(Transcode);
	}
	
	public List AccGrpBalDetails(int AccGrpCode,String season)
	{
		return ahFunctionalDao.AccGrpBalDetails(AccGrpCode,season);
	}
	
	public void updateRunningBalinAccGrpSmry(double finalAmt,String strCrDr,int AccGrpCode)
    {
		ahFunctionalDao.updateRunningBalinAccGrpSmry(finalAmt,strCrDr,AccGrpCode);
    }
	
	public void updateStatusForAccGrpDtls(int TransactionCode)
    {
		ahFunctionalDao.updateStatusForAccGrpDtls(TransactionCode);
    }
	
	public List getAccCodes(String AccCode)
	{
		return ahFunctionalDao.getAccCodes(AccCode);
	}
	
	
	public List getRecordsforAccGroupCode(int AccGroupCode)
	{
		return ahFunctionalDao.getRecordsforAccGroupCode(AccGroupCode);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAgreementSummary(AgreementSummary agreementSummary,String modifyFlag) 
	{
		ahFunctionalDao.addAgreementSummary(agreementSummary,modifyFlag);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAgreementDetails(AgreementDetails agreementDetails) 
	{
		ahFunctionalDao.addAgreementDetails(agreementDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLoanSummary(LoanSummary loanSummary) 
	{
		ahFunctionalDao.addLoanSummary(loanSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLoanDetails(LoanDetails loanDetails) 
	{
		ahFunctionalDao.addLoanDetails(loanDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addLoanDetailsNew(LoanDetails loanDetails) 
	{
		ahFunctionalDao.addLoanDetailsNew(loanDetails);
	}
	
	public Object getInterestLoan(Double Amount,Integer branchCode)
	{
		String sql="SELECT interestrate FROM bankloanintrates WHERE  "+Amount.intValue()+" BETWEEN fromamount AND toamount and branchcode="+branchCode;
		
		return hibernateDao.getID(sql);
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvanceDetails(AdvanceDetails advanceDetails) 
	{
		ahFunctionalDao.addAdvanceDetails(advanceDetails);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvanceSummary(AdvanceSummary advanceSummary) 
	{
		ahFunctionalDao.addAdvanceSummary(advanceSummary);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeedlingTrayDetails(SeedlingTrayDetails seedlingTrayDetails) 
	{
		ahFunctionalDao.addSeedlingTrayDetails(seedlingTrayDetails);
	}
	
	
	
	/*	public List<BankLoanRepayDt> listTieUpLoanInterests() {
		return tieUpLoanInterestDao.listTieUpLoanInterests();
	}
		/public List<BankLoanIntRates> listTieUpLoanInterestDetails() {
		return tieUpLoanInterestDao.listTieUpLoanInterestDetails();
	}
	
	public void deleteAgreementDetails(String agNumber) {
		tieUpLoanInterestDao.deleteBankLoanIntRates(bankLoanRepayDt);
	}
	*
	*/
	
	
	@SuppressWarnings("unchecked")
	public List<LoanDetails> getLoanDetailsByRyotCode(String season,Integer loanno,int branchCode)
    {
		season=season.replaceAll("\"", "");

        String sql = "select * from LoanDetails where Season ='" + season+"' and branchcode="+branchCode+" and loannumber=" + loanno;        
        return hibernateDao.findByNativeSql(sql, LoanDetails.class);
    }
	
	public List getLoanDetailsByAgreement(String agreementnumber)
    {
		agreementnumber=agreementnumber.replaceAll("\"", "");
        String sql = "select plantorratoon,surveynumber,extentsize from agreementdetails where agreementnumber in(" + agreementnumber+")";
		//String sql = "select plantorratoon,surveynumber,extentsize from agreementdetails where FIND_IN_SET(agreementnumber,"+agreementnumber+")";
		
        return hibernateDao.findBySql(sql);
    }
	
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementDetailsBySeason(String season,String agnumber)
    {
        String sql = "select * from AgreementDetails where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"'";        
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<RandDAgreementDetails> getranddAgreementDetailsBySeason(String season,String agnumber)
    {
        String sql = "select * from RandDAgreementDetails where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"'";        
        return hibernateDao.findByNativeSql(sql, RandDAgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<RandDAgreementSummary> getranddAgreementSummaryBySeason(String season,String agnumber)
    {
        String sql = "select * from RandDAgreementSummary where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"'";        
        return hibernateDao.findByNativeSql(sql, RandDAgreementSummary.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<PermitDetails> getValidityDate(Integer permitNo)
    {
        String sql = "select * from PermitDetails where permitnumber=" + permitNo+"";        
        return hibernateDao.findByNativeSql(sql, PermitDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<Circle> getCircleByCirclecode(Integer circlecode)
    {
        String sql = "select * from circle where circlecode=" + circlecode+"";        
        return hibernateDao.findByNativeSql(sql, Circle.class);
    }
	@SuppressWarnings("unchecked")
	public List<Zone> getZoneByZonecode(Integer zonecode)
    {
        String sql = "select * from Zone where zonecode=" + zonecode+"";        
        return hibernateDao.findByNativeSql(sql, Zone.class);
    }
	@SuppressWarnings("unchecked")
	public List<Village> getVillageBylandVilCode(String landvillagecode)
    {
        String sql = "select * from Village where villagecode='"+landvillagecode+"'";        
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	@SuppressWarnings("unchecked")
	public List<CropTypes> getPtOrRt(Integer nptorrt)
    {
        String sql = "select * from CropTypes where croptypecode="+nptorrt+"";        
        return hibernateDao.findByNativeSql(sql, CropTypes.class);
    }
	
	
	
	@SuppressWarnings("unchecked")
	public List<AgreementSummary> getAgreementSummaryBySeason(String season,String agnumber)
    {
        String sql = "select * from AgreementSummary where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"'";        
        return hibernateDao.findByNativeSql(sql, AgreementSummary.class);
    }
	public List<Circle> getCircleRecordDetails(Integer circleCode)
    {
        String sql = "select * from Circle where circlecode ="+circleCode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Circle.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> listAggrements(String season,Integer circle)
    {
        String sql = "select * from AgreementDetails where seasonyear ='"+season+"' and circlecode="+circle;   
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }

	public void updateEstQty(String ryotCode,String agreementNo,String surveyNo,double EstQty,String plotNo)
    {
		ahFunctionalDao.updateEstQty(ryotCode,agreementNo,surveyNo,EstQty,plotNo);
    }
	
	
	@SuppressWarnings("unchecked")
	public List<SeedlingTrayDetails> getSeedlingTrayDetailsByRyotCodeSeason(String season,String  ryotcode)
    {
        String sql = "select * from SeedlingTrayDetails where season ='" + season+"' and ryotcode='" + ryotcode+"'";        
        return hibernateDao.findByNativeSql(sql, SeedlingTrayDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<AdvanceDetails> getAdvanceDetailsBySeason(String season,String ryotcode)
    {
        String sql = "select * from AdvanceDetails where season ='" + season+"' and ryotcode='" + ryotcode+"'";        
        return hibernateDao.findByNativeSql(sql, AdvanceDetails.class);
    }

	public List<LoanDetails> getAllRyotLoansByRyotCode(String ryotcode,String season)
    {
        String sql = "select * from LoanDetails where ryotcode ='" + ryotcode+"' and Season ='" + season+"'";        
        return hibernateDao.findByNativeSql(sql, LoanDetails.class);
   }
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementByAgreementNumber(String agreementNumber)
    {
        String sql = "select * from AgreementDetails where agreementnumber ='" + agreementNumber+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<RandDAgreementDetails> getranddAgreementByAgreementNumber(String agreementNumber)
    {
        String sql = "select * from RandDAgreementDetails where agreementnumber ='" + agreementNumber+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, RandDAgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementDetailsByNumberSeasonAndPlot(String agreementNumber,String pltno,String season)
    {
        String sql = "select * from AgreementDetails where agreementnumber ='" + agreementNumber+"' and seasonyear='"+season+"' and plotnumber='"+pltno+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<PermitDetails> getPermitDetailsByNumberSeasonAndPlot(Integer oldPermitNo)
    {
        String sql = "select * from PermitDetails where permitnumber =" + oldPermitNo;
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, PermitDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<PermitDetails> getPermitData(String season,Integer nprogram )
    {
        String sql = "select * from PermitDetails where season = '"+season+"' and programno="+nprogram+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, PermitDetails.class);
    }
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<Ryot> getRyotNameByRyotCode(String rCode)
    {
        String sql = "select ryotname from Ryot where ryotcode ='" +rCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<Ryot> getRyotNameByRyotCodeNew(String rCode)
    {
        String sql = "select * from Ryot where ryotcode ='" +rCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }
	
	
	@SuppressWarnings("unchecked")
	public List<Ryot> getRelativeNameByRyotCode(String rcode)
    {
        String sql = "select * from Ryot where ryotcode ='" +rcode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }

	@SuppressWarnings("unchecked")
	public List<Village> getVillageNameByvillageCode(String villagecode)
    {
        String sql = "select * from Village where villagecode ='" +villagecode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	@SuppressWarnings("unchecked")
	public List<Variety> getVarietyNameByvarietyCode(Integer varietycode)
    {
        String sql = "select * from Variety where varietycode =" +varietycode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Variety.class);
    }
	@SuppressWarnings("unchecked")
	public List<Circle> getCircleNameBycircleCode(Integer circlecode)
    {
        String sql = "select * from Circle where circlecode =" +circlecode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Circle.class);
    }
	@SuppressWarnings("unchecked")
	public List<CropTypes> getCropType(Integer ptorrt)
    {
        String sql = "select * from CropTypes where croptypecode =" +ptorrt+"";
       // select croptype from CropTypes where croptypecode="+ptorrt+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, CropTypes.class);
    }
	
	
	public void deleteAgreementDetails(String agNumber) {
		ahFunctionalDao.deleteAgreementDetails(agNumber);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addProgramSummary(ProgramSummary programSummary,List ProgramList) 
	{
		ahFunctionalDao.addProgramSummary(programSummary,ProgramList);
	}
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementDetailsForGenerateProgram(String season,String varietycode,String dateFrom,String dateTo,String prgNumber,String modifyFlag,Integer ptorrt)
    {
		String sql =null;
		//Modified by DMurty on 31-08-2016
		if(ptorrt==0)
			sql = "select * from AgreementDetails A,AgreementSummary B where A.seasonyear ='" + season+"' and A.varietycode in("+varietycode+")  and A.cropdate between '" + dateFrom+"' and '" + dateTo+"' and A.plantorratoon='1' and B.status=0  and a.agreementnumber=b.agreementnumber and A.programnumber is null";
		else if(ptorrt==1)
			sql = "select * from AgreementDetails A,AgreementSummary B where A.seasonyear ='" + season+"' and A.varietycode in("+varietycode+")  and A.cropdate between '" + dateFrom+"' and '" + dateTo+"' and A.plantorratoon != '1' and B.status=0 and a.agreementnumber=b.agreementnumber and A.programnumber is null";
		else
			sql = "select * from AgreementDetails A,AgreementSummary B where A.seasonyear ='" + season+"' and A.varietycode in("+varietycode+")  and A.cropdate between '" + dateFrom+"' and '" + dateTo+"' and  B.status=0 and a.agreementnumber=b.agreementnumber and A.programnumber is null ";
	
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAllAgreementDetailsForGenerateProgramsByProgramNo(String season,Integer programNumber)
    {
		String sql ="select * from AgreementDetails where seasonyear='"+season+"' and programnumber="+programNumber+"";
		
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<ProgramVariety> getVarietyFromProgramVariety(Integer programNumber,String season)
    {
		String sql ="select * from ProgramVariety where season='"+season+"' and programnumber="+programNumber+"";
		
        return hibernateDao.findByNativeSql(sql, ProgramVariety.class);
    }
	
	
	
	@SuppressWarnings("unchecked")
	public List getDetailsFromProgramSummary(String season,int prgNumber)
    {
		//String sql = "select * from ProgramSummary where season ='" + season+"' and programnumber='" + prgNumber+"'";
		String sql = "select ps.season,ps.monthid,ps.periodid,ps.programnumber,ps.totalextent,ps.totalplant,ps.yearofplanting,ps.totalratoon,ps.typeofextent,pv.varietycode,pv.id from ProgramSummary as ps inner join ProgramVariety as pv on pv.id=ps.id where ps.season ='"+season+"' and ps.programnumber="+prgNumber;
		List SmryList = hibernateDao.findBySqlCriteria(sql);
		//List SmryList = hibernateDao.findByNativeSql(sql, ProgramSummary.class);
		
        if(SmryList.size()>0 && SmryList!=null)
        	return SmryList;
        else
        	return null;
    }
	
	@SuppressWarnings("unchecked")
	public List<ProgramSummary> getGenerateProgramDetails(String season,Integer prgno)
    {
        String sql = "select * from ProgramSummary where season ='" + season+"' and programnumber="+prgno+"";   
       // String sql = "select * from AgreementDetails where seasonyear ='" + season+"' and varietycode="+varietycode+" and cropdate between startDate and endDate";  
        return hibernateDao.findByNativeSql(sql, ProgramSummary.class);
    }
	public void updateProgramNumberInAgreementDetails(String ryotCode,String agreementNo,Integer plotNo,String program)
    {
		ahFunctionalDao.updateProgramNumberInAgreementDetails(ryotCode,agreementNo,plotNo,program);
    }
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementDetailsForProgramDetails(String season,String prgNumber)
    {
        String sql = "select * from AgreementDetails where seasonyear ='" + season+"' and  programnumber='" + prgNumber+"'";   
       
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvancePrincipleAmounts(AdvancePrincipleAmounts advancePrincipleAmounts) 
	{
		ahFunctionalDao.addAdvancePrincipleAmounts(advancePrincipleAmounts);
	}
	

	@SuppressWarnings("unchecked")
	public List<AdvanceDetails> getAdvanceDetailsBySeason(String season,String ryotcode,int advancecode)
    {
        String sql = "select * from AdvanceDetails where season ='" + season+"' and ryotcode='" + ryotcode+"' and advancecode="+advancecode;        
        return hibernateDao.findByNativeSql(sql, AdvanceDetails.class);
    }
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSeedSuppliersSummary(SeedSuppliersSummary seedSuppliersSummary) 
	{
		ahFunctionalDao.addSeedSuppliersSummary(seedSuppliersSummary);
	}
	
	public List<CompanyAdvance> getAdvanceDateilsByAdvanceCode(int advanceCode)
    {        
        return ahFunctionalDao.getAdvanceDateilsByAdvanceCode(advanceCode);
    }
	public List getRyotAdvances(String seasonCode)
    {        
        return ahFunctionalDao.getRyotAdvances(seasonCode);
    }
	
	public List getRyotAdvanceNames(int advanceCode)
    {        
        return ahFunctionalDao.getRyotAdvanceNames(advanceCode);
    }

	public List getRyotNamesByAdvances(String seasonCode,int advancecode)
    {        
        return ahFunctionalDao.getRyotNamesByAdvances(seasonCode,advancecode);
    }
	public List getRyotName(String rtotCode)
    {        
        return ahFunctionalDao.getRyotName(rtotCode);
    }
	
	public List getRyotNameNew(String rtotCode)
    {        
        return ahFunctionalDao.getRyotNameNew(rtotCode);
    }
	
	public void deleteSeedlingTrayDetails(String season, String ryotCode) 
	{
		ahFunctionalDao.deleteSeedlingTrayDetails(season,ryotCode);
	}
	
	public void deleteSeedSupplierDetails(String season, String ryotCode) 
	{
		ahFunctionalDao.deleteSeedSupplierDetails(season,ryotCode);
	}
	
	@SuppressWarnings("unchecked")
	public List<SampleCards> listSampleCards(String season,int program,int samplecard)
    {
        String sql = "select TOP 1 * from SampleCards where season ='"+season+"' and programnumber="+program+" and samplecard="+samplecard;   
		logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SampleCards.class);
    }
	
	public List getAggrementDetailsByCode(String agreementNo,String season)
    {        
        return ahFunctionalDao.getAggrementDetailsByCode(agreementNo,season);
    }
	
	public List getVillageDetailsByCode(String villageCode)
    {        
        return ahFunctionalDao.getVillageDetailsByCode(villageCode);
    }
	
	public List getVarietye(Integer varietyCode)
    {        
        return ahFunctionalDao.getVarietye(varietyCode);
    }
	
	
	public List getCirclePerGivenCircle(Integer circleCode)
    {        
        return ahFunctionalDao.getCirclePerGivenCircle(circleCode);
    }
	
	public List getVillageDetailsByCodeNew(String villageCode)
    {        
        return ahFunctionalDao.getVillageDetailsByCodeNew(villageCode);
    }
	
	public List getCropTypeByCode(int cropCode)
    {        
        return ahFunctionalDao.getCropTypeByCode(cropCode);
    }
	
	public List getVarietyDetailsByCode(int varietyCode)
    {        
        return ahFunctionalDao.getVarietyDetailsByCode(varietyCode);
    }
	
	//getVarietyDetailsByCodeNew
	public List getVarietyDetailsByCodeNew(int varietyCode)
    {        
        return ahFunctionalDao.getVarietyDetailsByCodeNew(varietyCode);
    }
	
	
	public List getLVName(String LVCode)
    {        
        return ahFunctionalDao.getLVName(LVCode);
    }	
	
	public List getProgramNumbersBySeason(String season)
    {        
        return ahFunctionalDao.getProgramNumbersBySeason(season);
    }
	


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSampleDetails(SampleCards sampleCards) 
	{
		ahFunctionalDao.addSampleDetails(sampleCards);
	}
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> listAggrementsforSampleCards(String season,int programno)
    {
        String sql = "select * from AgreementDetails where seasonyear ='"+season+"' and programnumber="+programno+" order by zonecode,circlecode,ryotcode,cropdate";   
		logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	public List getSampleCardNo(double extentSize)
    {        
        return ahFunctionalDao.getSampleCardNo(extentSize);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addWeighBridgeTestData(WeighBridgeTestData weighBridgeTestData) 
	{
		ahFunctionalDao.addWeighBridgeTestData(weighBridgeTestData);
	}
	
	@SuppressWarnings("unchecked")
	public List<LoanDetails> getLoanDetailsForUpdateLoan(String season,Integer branchcode)
    {
        String sql = "select * from LoanDetails where season ='" + season+"' and  branchcode=" + branchcode+"";   
       
        return hibernateDao.findByNativeSql(sql, LoanDetails.class);
    }
	
	public void updateLoanDetails(String ryotCode,String season,Integer branchcode,Double disbursedamount,String disburseddate,String refNumber)
    {
		ahFunctionalDao.updateLoanDetails(ryotCode,season,branchcode,disbursedamount,disburseddate,refNumber);
    }
	
	public void updateLoanSummary(String ryotCode,String season,Double disbursedamount)
    {
		ahFunctionalDao.updateLoanSummary(ryotCode,season,disbursedamount);
    }
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	
	public void addloanPrincipleAmounts(LoanPrincipleAmounts loanPrincipleAmounts) 
	{
		ahFunctionalDao.addloanPrincipleAmounts(loanPrincipleAmounts);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addsampleCards(SampleCards sampleCards)
	{
		ahFunctionalDao.addsampleCards(sampleCards);
	}
	
	 public void updateSampleCardInAgreementDetails(String ryotCode,String AgreementNo,String sampleCardNos,String plotno)
     {
		 ahFunctionalDao.updateSampleCardInAgreementDetails(ryotCode,AgreementNo,sampleCardNos,plotno);
     }
	 
	public void deleteSampleCards(String season,String nprogram) 
	{
		ahFunctionalDao.deleteSampleCards(season,nprogram);
	}
	@SuppressWarnings("unchecked")
	public List<SampleCards> listSampleCardsBySeasonandPrgrm(String season,int program,String Page)
    {
		String sql = "";
		if("Update".equalsIgnoreCase(Page))
		{
			 sql = "select TOP 1 * from SampleCards where season ='"+season+"' and programnumber="+program;  
		}
		else
		{
			 sql = "select * from SampleCards where season ='"+season+"' and programnumber="+program+" and ccsrating is not null";  
		}
        
		logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SampleCards.class);
    }
	//Added by DMurty on 10-08-2016
	@SuppressWarnings("unchecked")
	public List<SampleCards> listTempSampleCardsBySeasonandPrgrm(String season,int program,String Page)
    {
		String sql = "select * from SampleCardsTemp where season ='"+season+"' and programnumber="+program;  
		logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SampleCards.class);
    }
	
	
	public List<AgreementDetails> getAgreementDetailsByCode(String agreementNo,String season)
    {        
        return ahFunctionalDao.getAgreementDetailsByCode(agreementNo,season);
    }
	
	public void updateValuesforSampleCard(Map TempMap)
    {
		ahFunctionalDao.updateValuesforSampleCard(TempMap);
    }
	
	public List getCountofSampleCards(String season,int programNo )
    {
        return ahFunctionalDao.getCountofSampleCards(season,programNo);
    }
	
	public List getPlotNosforSampleCards()
    {        
        return ahFunctionalDao.getPlotNosforSampleCards();
    }
	public List getCcsratingsByPlotno(String plotNo,int programNo,String agreementNo)
    {        
        return ahFunctionalDao.getCcsratingsByPlotno(plotNo,programNo,agreementNo);
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSampleCardTemp(SampleCardsTemp sampleCardsTemp) 
	{
		ahFunctionalDao.addSampleCardTemp(sampleCardsTemp);
	}
	
	public List<AgreementSummary> getAgreementDetails(String agreementNo)
    {        
        return ahFunctionalDao.getAgreementDetails(agreementNo);
    }
	
	public List getFamilyAvgCcsratingsByPlotno(String plotNo,int programNo,int familyGrpCode)
    {        
        return ahFunctionalDao.getFamilyAvgCcsratingsByPlotno(plotNo,programNo,familyGrpCode);
    }
	
	public void TruncateTemp(String model) 
	{
		ahFunctionalDao.TruncateTemp(model);
	}
	
	public void updateValuesforAvgCcsRating(Map TempMap)
    {
		ahFunctionalDao.updateValuesforAvgCcsRating(TempMap);
    }
	
	public List getAdvanceAmtforValidation(int advancecode)
    {        
        return ahFunctionalDao.getAdvanceAmtforValidation(advancecode);
    }
	
	
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> listAggrementsforRanking(String season,int programno,String pageName,String fromCcsRating,String toCcsRating)
    {
        String sql = null;   
        if("Permit".equalsIgnoreCase(pageName))
        {
           // sql = "select * from AgreementDetails where seasonyear ='"+season+"' and programnumber="+programno+" and averageccsrating between "+fromCcsRating+" and "+toCcsRating+" order by rank asc";   
            sql = "select * from AgreementDetails where seasonyear ='"+season+"' and programnumber="+programno+" and averageccsrating between "+fromCcsRating+" and "+toCcsRating+" order by rank asc";
        }
        else
        {
           // sql = "select * from AgreementDetails where seasonyear ='"+season+"' and programnumber="+programno+" order by familygroupavgccsrating desc,extentsize desc,zonecode asc,circlecode asc,ryotseqno asc";
            
            sql = "select * from AgreementDetails where seasonyear ='"+season+"' and programnumber="+programno+" order by familygroupavgccsrating desc,cropdate";//extentsize desc,zonecode asc,circlecode asc,ryotseqno asc";   

        }
        
        logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	public List getZone(int zoneCode)
    {        
        return ahFunctionalDao.getZone(zoneCode);
    }
	public List getZoneDetails(int zoneCode)
    {        
        return ahFunctionalDao.getZoneDetails(zoneCode);
    }
	public List getVillageDetails(String villageCode)
    {        
        return ahFunctionalDao.getVillageDetails(villageCode);
    }
	public List getCircle(int circleCode)
    {        
        return ahFunctionalDao.getCircle(circleCode);
    }
	//getCircleById
	public List getCircleById(int circleCode)
    {        
        return ahFunctionalDao.getCircleById(circleCode);
    }
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRankingTemp(GenerateRankingTemp generateRankingTemp) 
	{
		ahFunctionalDao.addRankingTemp(generateRankingTemp);
	}
	
	@SuppressWarnings("unchecked")
	public List<GenerateRankingTemp> getRankingTempData()
    {
        String sql = "select * from GenerateRankingTemp order by id";   
        return hibernateDao.findByNativeSql(sql, GenerateRankingTemp.class);
    }
	
	public void UpdateRankinginAgreementDetails(Map TempMap,String pageName)
    {
		ahFunctionalDao.UpdateRankinginAgreementDetails(TempMap,pageName);
    }
	
	
	public List<PermitRules> getPermitRules()
    {        
        return ahFunctionalDao.getPermitRules();
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addPermits(PermitDetails permitDetails)
	{
		ahFunctionalDao.addPermits(permitDetails);
	}
	
	public void deletePermits(String season,String nprogram) 
	{
		ahFunctionalDao.deletePermits(season,nprogram);
	}
	
	public List<AgreementDetails> getPreviousAgreementsForRatoon(String season,String ryotCode)
    {        
        return ahFunctionalDao.getPreviousAgreementsForRatoon(season,ryotCode);
    }
	
	public List<ProgramSummary> getValidatreProgramNumForSeason(String season,int programNo)
    {        
        return ahFunctionalDao.getValidatreProgramNumForSeason(season,programNo);
    }
	public List getIntrestRatesByAmount(double amount)
    {        
        return ahFunctionalDao.getIntrestRatesByAmount(amount);
    }
	
	public void deleteIntrestAmounts(String season, String ryotCode,int advCode) 
	{
		logger.info("========deleteIntrestAmounts()==========");
		ahFunctionalDao.deleteIntrestAmounts(season,ryotCode,advCode);
	}
	public List getValidateRyotForAgreement(String season,String ryotCode)
    {        
        return ahFunctionalDao.getValidateRyotForAgreement(season,ryotCode);
    }
	
	@SuppressWarnings("unchecked")
	public List<AgreementSummary> getAgreementSmryBySeason(String season,String ryotCode,String agreementNo)
    {
        String sql = "select * from AgreementSummary where seasonyear='"+season+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"'";        
        return hibernateDao.findByNativeSql(sql, AgreementSummary.class);
    }
	@SuppressWarnings("unchecked")
	public List<AgreementSummary> getAgreementSmryByAgreementNo(String season,String agreementNo)
    {
        String sql = "select * from AgreementSummary where agreementnumber='"+agreementNo+"'";        
        return hibernateDao.findByNativeSql(sql, AgreementSummary.class);
    }
	
	
	public void deleteRyotBankDetails(String ryotCode,String accNo,int branchCode)
	{
		ahFunctionalDao.deleteRyotBankDetails(ryotCode,accNo,branchCode);
	}
	
	public List getIfscCodeByBrancgCode(int branchCode)
    {        
        return ahFunctionalDao.getIfscCodeByBrancgCode(branchCode);
    }
	
	public List getMaxIdForBankDetails(String ryotCode)
    {        
        return ahFunctionalDao.getMaxIdForBankDetails(ryotCode);
    }
	
	public boolean saveAllagreementTables(List agreementTablesList)
	{
		boolean isInsertSuccess = false;
		isInsertSuccess = ahFunctionalDao.saveAllagreementTables(agreementTablesList);
		return isInsertSuccess;
	}
	
	public List getAgreementNumbersForSeason(String season,String tablename)
    {        
        return ahFunctionalDao.getAgreementNumbersForSeason(season,tablename);
    }
	
	public List getVarityForSeason(String season)
    {        
        return ahFunctionalDao.getVarityForSeason(season);
    }
	
	public Integer getProgramNoBySeason(String season) throws Exception
    {
		logger.info("========getProgramNoBySeason()==========" + season);
		try
		{
	        String sql = "select max(programnumber) from ProgramSummary where season='"+season+"'";
			logger.info("========sql Query==========" + sql);

			Integer maxVal=(Integer) ahFunctionalDao.getMaxProgramNumForSeason(sql);
			if(maxVal==null)
			{
				maxVal=1;
			}
			else
			{
				maxVal = maxVal + 1;
			}
			return maxVal;
		}
		catch(Exception e)
		{
			 logger.info(e.getCause(), e);
	         return null;
		}
    }
	
	public List<AgreementSummary> getAgreementSummaryByCode(String agreementNo,String season)
    {        
        return ahFunctionalDao.getAgreementSummaryByCode(agreementNo,season);
    }
	
	public Object getTrayChargeByDistance(double distance,Integer trayCode)
    {        
        return ahFunctionalDao.getTrayChargeByDistance(distance,trayCode);
    }
	
	
	public List getAgreementDataToMigrate(String table)
    {        
        return ahFunctionalDao.getAgreementDataToMigrate(table);
    }
	
	public List getAccountCodesToMigrate(String season)
    {        
        return ahFunctionalDao.getAccountCodesToMigrate(season);
    }
	public List getAccountGroupCodesToMigrate(String season)
    {        
        return ahFunctionalDao.getAccountGroupCodesToMigrate(season);
    }
	
	public List getAccSubGroupCodesToMigrate(String season)
    {        
        return ahFunctionalDao.getAccSubGroupCodesToMigrate(season);
    }
	
	public List getZonecodeFromAgreementDetails()
    {        
        return ahFunctionalDao.getZonecodeFromAgreementDetails();
    }
	
	public List updateRyotNames(String table)
    {        
        return ahFunctionalDao.updateRyotNames(table);
    }
	
	
	public List<Village> getVillageDtls(int nlandVillageCode)
    {        
        return ahFunctionalDao.getVillageDtls(nlandVillageCode);
    }
	
	public List getVarietyDetailsByName(String varietyName)
    {        
        return ahFunctionalDao.getVarietyDetailsByName(varietyName);
    }
	
	public List getZoneCodeByMandal(int mandalCode)
    {        
        return ahFunctionalDao.getZoneCodeByMandal(mandalCode);
    }
	
	public List<FieldAssistant> getFieldAssistantDetails(String fAName)
    {        
        return ahFunctionalDao.getFieldAssistantDetails(fAName);
    }
	
	public List<FieldOfficer> getFieldOfficerDetails(int fOId)
    {        
        return ahFunctionalDao.getFieldOfficerDetails(fOId);
    }
	
	public Integer getMaxIDforPlotSeqNo(String tableName,String strAgreementNo)
    {
		Integer maxVal=(Integer) ahFunctionalDao.getMaxValue("select max(plotseqno) from "+tableName+" where agreementnumber='"+strAgreementNo+"'");
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	
	public List getBankVerification(String bankname) 
	{	
		return ahFunctionalDao.getBankVerification(bankname);
	}
	public List getBankCode(String bankname) 
	{
		return ahFunctionalDao.getBankCode(bankname);
	}
	
	public List getVarietyDetailsByNameNew(String varietyName)
    {        
        return ahFunctionalDao.getVarietyDetailsByNameNew(varietyName);
    }
		
	public List getZoneCodeByMandalNew(int mandalCode)
    {        
        return ahFunctionalDao.getZoneCodeByMandalNew(mandalCode);
    }
	
	public List getCircleDetailsNew(int circleCode)
    {        
        return ahFunctionalDao.getCircleDetailsNew(circleCode);
    }
	public List getCircleDetailsByVillageCode(String villageCode)
    {        
        return ahFunctionalDao.getCircleDetailsByVillageCode(villageCode);
    }
	public List getMandalDetailsNew(int nmandalCode)
    {        
        return ahFunctionalDao.getMandalDetailsNew(nmandalCode);
    }	
	
	public Integer maxIdForBranch(int bankCode)
    {
		Integer maxVal=(Integer) ahFunctionalDao.getMaxValue("select max(id) from Branch where bankcode="+bankCode);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	
	public List getIfscCodeByBrancgCodeNew(int branchCode)
    {        
        return ahFunctionalDao.getIfscCodeByBrancgCodeNew(branchCode);
    }
	public List getMaxIdForBankDetailsNew(String ryotCode)
    {        
        return ahFunctionalDao.getMaxIdForBankDetailsNew(ryotCode);
    }
	
	public Integer getMaxLoanNo(String tableName,String colName,int bankCode)
    {
		Integer maxVal=(Integer) ahFunctionalDao.getMaxValue("select max("+colName+") from "+tableName+" where branchcode="+bankCode);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }
	public List getMaxLoanNoList(int bankCode,String season)
    {        
        return ahFunctionalDao.getMaxLoanNoList(bankCode,season);
    }
	public List getAgreementsByRyot(String ryotCode,String season)
    {        
        return ahFunctionalDao.getAgreementsByRyot(ryotCode,season);
    }
	public List getWeighmentRyots(String season)
    {
		 return ahFunctionalDao.getWeighmentRyots(season);
    }
    
	public Integer getCircleCodeFromAgreementDetailsNew(String agNumber)
	{	
		Integer circleCode=(Integer) ahFunctionalDao.getCircleCodeFromAgreementDetailsNew("select circlecode  from AgreementDetails where agreementnumber='"+agNumber+"'");
		return circleCode;
	}
	
	public List getCircleCodeByAgreementNo(String AgNoforCricle)
    {        
        return ahFunctionalDao.getCircleCodeByAgreementNo(AgNoforCricle);
    }
	
	public Integer maxIdForAdvanceIntrest(int advCode)
    {
		Integer maxVal=(Integer) ahFunctionalDao.getMaxValue("select max(id) from AdvanceInterestRates where advancecode="+advCode);
		if(maxVal==null)
			maxVal=0;

		maxVal = maxVal + 1;
		
        return maxVal;
    }

	
	public Object getSeedlingAdvance(Integer advCode)
    {        
        return ahFunctionalDao.getSeedlingAdvance(advCode);
    }
	
	public List<Circle> getCircleDtls(int circleCode)
    {        
        return ahFunctionalDao.getCircleDtls(circleCode);
    }
	
	public List<Mandal> getMandalDtls(int mandalCode)
    {        
        return ahFunctionalDao.getMandalDtls(mandalCode);
    }
	public List<Branch> getBranchDtls(int branchCode)
    {        
        return ahFunctionalDao.getBranchDtls(branchCode);
    }
	
	
	
	public List getLoanNumbersForBranch(String seasonCode,int branchCode)
    {        
        return ahFunctionalDao.getLoanNumbersForBranch(seasonCode,branchCode);
    }
	
	public List getAdvanceSummaryData(String seasonCode,String ryotCode,int advanceCode)
    {        
        return ahFunctionalDao.getAdvanceSummaryData(seasonCode,ryotCode,advanceCode);
    }
	
	public List getFamilyMembers(int familyGroupId)
    {        
        return ahFunctionalDao.getFamilyMembers(familyGroupId);
    }
	
	public void UpdateFamilyMembers(String familyMembers,int familyGroupCode)
	{
		ahFunctionalDao.UpdateFamilyMembers(familyMembers,familyGroupCode);
	}
	
	public List getDatesByCalcruledate(String season)
    {        
        return ahFunctionalDao.getDatesByCalcruledate(season);
    }
	public List getPermitDate(Integer permitnum)
    {        
        return ahFunctionalDao.getPermitDate(permitnum);
    }
	public List getProgramNoByProgramSummary(String season)
    {        
        return ahFunctionalDao.getProgramNoByProgramSummary(season);
    }
	
	public List getAccountNames(String accName)
    {        
        return ahFunctionalDao.getAccountNames(accName);
    }
	@SuppressWarnings("unchecked")
	public List<Variety> getVarietyByVarietyCode(Integer varietyCode)
    {
        String sql = "select * from Variety where varietycode =" +varietyCode;
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Variety.class);
    }
	@SuppressWarnings("unchecked")
	public List<Village> getVillageDistance(String villageCode)
    {
        String sql = "select * from Village where villagecode ='"+villageCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	@SuppressWarnings("unchecked")
	public List<Village> getVillageForLedger(String villageCode)
    {
        String sql = "select * from Village where villagecode ='"+villageCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	@SuppressWarnings("unchecked")
	public List<RyotBankDetails> getAccountNumberForLedger(String ryotCode)
    {
        String sql = "select * from RyotBankDetails where ryotcode ='"+ryotCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, RyotBankDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<Branch> getBranchForLedger(Integer branchCode)
    {
        String sql = "select * from Branch where branchcode ="+branchCode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Branch.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<Zone> getRegionCode(Integer zoneCode)
    {
        String sql = " select * from Zone where zonecode="+zoneCode+"";
        
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Zone.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<Circle> getVillageCode(Integer circleCode)
    {
        String sql = " select * from Circle where circlecode="+circleCode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Circle.class);
    }
	@SuppressWarnings("unchecked")
	public List<AccountDetails> getMaxTransactionId(String season)
    {
        String sql = " select max(transactioncode) transactioncode from AccountDetails where season='"+season+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AccountDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<AccountGroupDetails> getMaxTransactionCode(String season)
    {
        String sql = " select max(transactioncode) transactioncode from AccountGroupDetails where season='"+season+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AccountGroupDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<AccountSubGroupDetails> getMaxTCodeForAccSub(String season)
    {
        String sql = " select max(transactioncode) transactioncode from AccountSubGroupDetails where season='"+season+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AccountSubGroupDetails.class);
    }
	
	
	/*@SuppressWarnings("unchecked")
	public List<AccountDetails> getAccSum(String season)
    {
        String sql = " select sum(cr) cr,sum(dr) dr from AccountDetails where season='"+season+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, AccountDetails.class);
    }*/
	@SuppressWarnings("unchecked")
	public List<Village> getMandalCode(String vCode)
    {
        String sql = " select * from Village where villagecode="+vCode+"";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	
	
	
	@SuppressWarnings("unchecked")
	public List<SeedAdvanceCost> getSeedCostUnitPerDistance(int advancecode,Double vDistance)
    {
		 String sql = "select * from SeedAdvanceCost where advancecode="+advancecode+" and fromdistance<="+vDistance+" and "+vDistance+"<=todistance ";
		 logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, SeedAdvanceCost.class);
    }
	
	public List getSampleCardDetails(int sampleCardNo,String season,String table)
    {        
        return ahFunctionalDao.getSampleCardDetails(sampleCardNo,season,table);
    }
	
	//written by naidu 16-08-2016
	public List getAllCaneReceiptData(String season)
	{
		return ahFunctionalDao.getAllCaneReceiptData(season);
	}
	
	public List getTransportSubsidyData(String season)
	{
		return ahFunctionalDao.getTransportSubsidyData(season);
	}	
	public List getAdvances(String season)
	{
		return ahFunctionalDao.getAdvances(season);
	}
	public List getSBandLoanPayments(String season)
	{
		return ahFunctionalDao.getSBandLoanPayments(season);
	}
	public List getACPDetails()
	{
		return ahFunctionalDao.getACPDetails();
	}
	public List TransferBankCharges(String season)
	{
		return ahFunctionalDao.getBankCharges(season);
	}
	public List getCPDetails()
	{
		return ahFunctionalDao.getCPDetails();
	}
	public List getOBData(String season)
	{
		return ahFunctionalDao.getOBData(season);
	}	
	public List getFinalPayDetails(String season)
	{
		return ahFunctionalDao.getFinalPayDetails(season);
	}
	
	public String getAdvName(Integer advcode)
	{
		return ahFunctionalDao.getAdvName(advcode);
	}
	public String getRyotRecordName(String ryotcode)
	{
		String ryotname=null;
		List<String> ryot=ahFunctionalDao.getRyotName(ryotcode);
		if(ryot.size()>0 && ryot != null)
		{
			ryotname=(String)ryot.get(0);
		}
		return ryotname;
	}
	public List getRyotNameFromTemp(String ryotcode)
	{
		return ahFunctionalDao.getRyotNameFromTemp(ryotcode);
	}
	public boolean validateAccountMaster(String ryotcode)
	{
		return ahFunctionalDao.validateAccountMaster(ryotcode);
	}
	
	public List generatePermitPrint(Integer pNum)
	{
		return ahFunctionalDao.generatePermitPrint(pNum);
	}
	//Added by DMurty on 02-09-2016
	public List getVillageNames()
    {        
        return ahFunctionalDao.getVillageNames();
    }
//---------------------------------Advance migration code----------------------------
	public List getAllSeedAndSeedAdvanceMgData()
	{
		return ahFunctionalDao.getAllSeedAndSeedAdvanceMgData();
	}
public Integer getRyotAdvCode(String advname)
	{	
		Integer advCode=(Integer) ahFunctionalDao.getCircleCodeFromAgreementDetailsNew("select advancecode  from CompanyAdvance where advance like '"+advname+"%'");
		return advCode;
	}

public String GetVillageCodeForRyot(String ryotcode)
    {
		String RyotCode=(String) ahFunctionalDao.GetAccSubGrpCode("select villagecode from Ryot where ryotcode='"+ryotcode+"'");
        return RyotCode;
    }
public Integer GetAdvCategoryCodeForAdvcode(Integer advcode)
    {
		Integer advCatCode=(Integer) ahFunctionalDao.getCircleCodeFromAgreementDetailsNew("select advancecategorycode  from AdvanceCategory where advancecode="+advcode+"");
		return advCatCode;
    }
public Integer GetAdvSubCategoryCodeForAdvcode(Integer advcode,Integer advcatCode)
    {
		Integer advSubCatCode=(Integer) ahFunctionalDao.getCircleCodeFromAgreementDetailsNew("select subcategorycode   from AdvanceCategory where advanceCode="+advcode+" and advancecategorycode="+advcatCode+"");
		return advSubCatCode;
    }	
	public List getAgreementsAndExtentSizes(String ryotcode)
	{
		return ahFunctionalDao.getAgreementsAndExtentSizes(ryotcode);
	}
	public List getSeedSupplierDetails(String season,String ryotcode)
	{
		return ahFunctionalDao.getSeedSupplierDetails(season,ryotcode);
	}
	public Double GetSupplierExtentSize(String ryotcode)
    {
		Double extsize=(Double) ahFunctionalDao.getCircleCodeFromAgreementDetailsNew("select max(extentsize)  from AgreementDetails where ryotcode='"+ryotcode+"'");
		return extsize;
    }
	public void addProgramVariety(ProgramVariety programVariety)
    {        
      ahFunctionalDao.addProgramVariety(programVariety);
    }
	
	public List getAllTransportMgData()
	{
		return ahFunctionalDao.getAllTransportMgData();
	}
	//Added by DMurty on 05-10-2016
	public List getProgramDetailsByNo(int programNumber,int maxId)
    {        
        return ahFunctionalDao.getProgramDetailsByNo(programNumber,maxId);
    }
		//added by naidu
	@SuppressWarnings("unchecked")
	public List<RyotBankDetails> getRyotBankAccListForValidation(String accNum)
    {
        String sql = "select * from RyotBankDetails where accountnumber='"+accNum+"'";        
        return hibernateDao.findByNativeSql(sql, RyotBankDetails.class);
    }
	//Added by DMurty on 21-10-2016
	public List getRyotAdvancePrincipleData(String ryotCode,int advCode,String season)
    {        
        return ahFunctionalDao.getRyotAdvancePrincipleData(ryotCode,advCode,season);
    }
	
	public List getAllAdvanceData()
	{
		return ahFunctionalDao.getAllAdvanceData();
	}
	//Added by DMurty on 31-10-2016
	public List<Branch> getBranchDetailsByBranchCode(int branchCode)
    {        
        return ahFunctionalDao.getBranchDetailsByBranchCode(branchCode);
    }
	
	public boolean validateHarvestDateForPermit(int permitNo)
    {        
        return ahFunctionalDao.validateHarvestDateForPermit(permitNo);
    }
	
	public Integer validateHarvestDateForPermitForWeighment(int permitNo)
    {        
        return ahFunctionalDao.validateHarvestDateForPermitForWeighment(permitNo);
    }
	//Added by DMurty on 08-11-2016
	public List getDisbursedDate(int branchcode,int lnumber)
    {        
        return ahFunctionalDao.getDisbursedDate(branchcode,lnumber);
    }
	//Added by DMurty on 09-11-2016
	public List<AccountSummaryTemp> getRecordinAccSmryTemp(String accountCode,String season)
    {        
        return ahFunctionalDao.getRecordinAccSmryTemp(accountCode,season);
    }
	
	public List<AccountSummary> getRecordinAccSmry(String accountCode,String season)
    {        
        return ahFunctionalDao.getRecordinAccSmry(accountCode,season);
    }
	//Added by DMurty on 10-11-2016
	public List getAccSubGrpDtls(int accGrpCode,int accSubGrpCode,String sesn)
    {        
        return ahFunctionalDao.getAccSubGrpDtls(accGrpCode,accSubGrpCode,sesn);
    }

	@SuppressWarnings("unchecked")
	public List<WeighmentDetails> getLorryandcanewBySeasonandpnum(String season,Integer permitnumber)
    {
        String sql = "select * from WeighmentDetails where season ='" + season+"' and permitnumber=" + permitnumber+"";        
        return hibernateDao.findByNativeSql(sql, WeighmentDetails.class);
    }
	
	public List getAccDetailsFromSummary(String accountCode,String sesn)
    {        
        return ahFunctionalDao.getAccDetailsFromSummary(accountCode,sesn);
    }
	
	public List getAccGrpDetailsFromSummary(int accountGroupCode,String sesn)
    {        
        return ahFunctionalDao.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
    }
	
	
	public void deleteTempRecords(String model,String loginUser,int caneacslno,String season) 
	{
		ahFunctionalDao.deleteTempRecords(model,loginUser,caneacslno,season);
	}
	
	//Added by DMurty on 16-11-2016
	public List getDedPartForSbAccList(int caneaccslno,String sesn)
    {        
        return ahFunctionalDao.getDedPartForSbAccList(caneaccslno,sesn);
    }
	@SuppressWarnings("unchecked")
	public List<AgreementDetails> getAgreementDetailsBySeasonByPlot(String season,String agnumber,String plotNo)
    {
        String sql = "select * from AgreementDetails where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"' and plotnumber='"+plotNo+"'";        
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	@SuppressWarnings("unchecked")
	public List<RandDAgreementDetails> getranddAgreementDetailsBySeasonByPlot(String season,String agnumber,String plotNo)
    {
        String sql = "select * from RandDAgreementDetails where seasonyear ='" + season+"' and agreementnumber='" + agnumber+"' and plotnumber='"+plotNo+"'";        
        return hibernateDao.findByNativeSql(sql, RandDAgreementDetails.class);
    }
	//Added by DMurty on 17-11-2016
	public List getDedPartForSbAccListForRyot(int caneaccslno,String sesn,String ryotCode)
    {        
        return ahFunctionalDao.getDedPartForSbAccListForRyot(caneaccslno,sesn,ryotCode);
    }
	
	public List getCaneValueSplitupDetailsByDate(String sesn,String strCaneAccDate)
    {        
        return ahFunctionalDao.getCaneValueSplitupDetailsByDate(sesn,strCaneAccDate);
    }

	public List getCaneValueSplitupDetailsByRyot(String sesn,String strCaneAccDate,String ryotCode)
    {        
        return ahFunctionalDao.getCaneValueSplitupDetailsByRyot(sesn,strCaneAccDate,ryotCode);
    }
	@SuppressWarnings("unchecked")
	public List getAgreementDetailsByaggrementno(String season,String agreementNo,Integer plotNum,String ryotCode)
    {
	     return ahFunctionalDao.getAgreementDetailsByaggrementno( season, agreementNo, plotNum, ryotCode);
    }
	public List getVehicleNumbers(String season)
    {        
        return ahFunctionalDao.getVehicleNumbers(season);
    }

	public List getAdvancePrinciples(String ryotcode,int advanceCode,String season)
	{       
        return ahFunctionalDao.getAdvancePrinciples(ryotcode,advanceCode,season);
    }
	
	public List getLoanPrinciples(String ryotcode,int loannumber,String season)
	{       
        return ahFunctionalDao.getLoanPrinciples(ryotcode,loannumber,season);
    }
	public List getWeighmentList(int permitNo)
    {        
        return ahFunctionalDao.getWeighmentList(permitNo);
    }
	
	@SuppressWarnings("unchecked")
	public List getPermits(String season,String ryotCode)
    {
		String sql="select * from PermitDetails where ryotcode='"+ryotCode+"' and season='"+season+"'";
		logger.info("getPermits()---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql,PermitDetails.class);
    }
	public List CheckWeighmentList(int permitNo)
    {        
        return ahFunctionalDao.CheckWeighmentList(permitNo);
    }
	
	public List getAdvanceSummaryList(String ryotcode,int advanceCode,String season)
	{       
        return ahFunctionalDao.getAdvanceSummaryList(ryotcode,advanceCode,season);
    }
	
	public List getEffectedRyots(String ryotcode)
	{       
        return ahFunctionalDao.getEffectedRyots(ryotcode);
    }
	
	public List getLoanPrinciples(String ryotcode,String season)
	{       
        return ahFunctionalDao.getLoanPrinciples(ryotcode,season);
    }
	public List getAdvancePrinciplesNew(String ryotcode,String season)
	{       
        return ahFunctionalDao.getAdvancePrinciplesNew(ryotcode,season);
    }
	public List getSbDetails(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getSbDetails(ryotCode,CanAccSlNo,season);
    }
	
	public List getLoanDetails(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getLoanDetails(ryotCode,CanAccSlNo,season);
    }
	
	public List getAdvanceDetails(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getAdvanceDetails(ryotCode,CanAccSlNo,season);
    }
	
	public List getLoanAccountNumber(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getLoanAccountNumber(ryotCode,CanAccSlNo,season);
    }
	//getLoanAcNo
	public List getLoanAcNo(String ryotcode,String season,int loanNo,int bankCode)
	{       
        return ahFunctionalDao.getLoanAcNo(ryotcode,season,loanNo,bankCode);
    }
	
	public List getLoanDetailsByCanAccSlno(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getLoanDetailsByCanAccSlno(ryotCode,CanAccSlNo,season);
    }
	public List getAdvanceSummary(String ryotcode,int advanceCode,String season)
	{       
        return ahFunctionalDao.getAdvanceSummary(ryotcode,advanceCode,season);
    }
	public List getSbDetailsTemp(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getSbDetailsTemp(ryotCode,CanAccSlNo,season);
    }
	public List getLoanDetailsTemp(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getLoanDetailsTemp(ryotCode,CanAccSlNo,season);
    }
	public List getLoanDetailsByCanAccSlnoTemp(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getLoanDetailsByCanAccSlnoTemp(ryotCode,CanAccSlNo,season);
    }
	public List getAdvanceDetailsTemp(String ryotCode,int CanAccSlNo,String season)
    {        
        return ahFunctionalDao.getAdvanceDetailsTemp(ryotCode,CanAccSlNo,season);
    }
	public List getWeighmentListForSecondPermit(int permitNo,String season)
    {        
        return ahFunctionalDao.getWeighmentListForSecondPermit(permitNo,season);
    }
	public Double getCaneValueSplitUp(String season,Integer caneslno)
    {
		Double transport=(Double) ahFunctionalDao.GetAccSubGrpCode("select sum(totalvalue) as totalvalue from CaneValueSplitup  where season='"+season+"'  and caneacslno='"+caneslno+"' ");
        return transport;
    }
	public Double getTotalSupply(String season,Integer caneslno)
    {
		Double canesupplied=(Double) ahFunctionalDao.GetAccSubGrpCode("select sum(canesupplied) as canesupplied from CaneValueSplitup  where season='"+season+"'  and caneacslno='"+caneslno+"' ");
        return canesupplied;
    }
	public Date getDate(String season,Integer caneslno)
    {
		Date frmDate=(Date) ahFunctionalDao.GetAccSubGrpCode("select datefrom  from CaneAccountSmry  where season='"+season+"'  and caneacslno='"+caneslno+"' ");
        return frmDate;
    }
	
	public Date getPayDate(String season,Integer caneslno)
    {
		Date frmDate=(Date) ahFunctionalDao.GetAccSubGrpCode("select dateofcalc  from CaneAccountSmry  where season='"+season+"'  and caneacslno='"+caneslno+"' ");
        return frmDate;
    }
	
	public String getGlcode(Integer bkcode)
    {
		String glCode=(String) ahFunctionalDao.GetAccSubGrpCode("select glCode  from CompanyAdvance  where advancecode="+bkcode+" ");
        return glCode;
    }
	public Long getgrcodefromBranch(Integer bkcode)
    {
		Long glcode=(Long) ahFunctionalDao.GetAccSubGrpCode("select glcode  from Branch  where branchcode="+bkcode+" ");
        return glcode;
    }
	public Double bankwiseData(String season,Integer caneslno,Integer bkcode)
    {
		Double transport=(Double) ahFunctionalDao.GetAccSubGrpCode("select sum(paidamt) as totalamt from CanAccLoansDetails  where advloan=1 and season='"+season+"'  and caneacctslno='"+caneslno+"' and branchcode="+bkcode+" ");
        return transport;
    }
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addDeductionsTemp(DedDetails_temp dedDetails_temp) 
	{
		ahFunctionalDao.addDeductionsTemp(dedDetails_temp);
	}
	
	public void updateAmountForLoans(String strQry)
	{
		ahFunctionalDao.updateAmountForLoans(strQry);
	}
	public Double GetprincipleAmt(String season,String ryotCode,Integer loancode)
    {
		Double principleAmt=(Double) ahFunctionalDao.GetAccSubGrpCode("select Principle from LoanSummary where season='"+season+"' and ryotcode='"+ryotCode+"' and loannumber="+loancode+"");
        return principleAmt;
    }
	
	//added by sahadeva 17-01-2017
	public Double GetRyotSupply(String season,String ryotcode)
    {
		//Double supplyQty=(Double) ahFunctionalDao.GetAccSubGrpCode("select sum(netwt) from WeighmentDetails where season='"+season+"' and ryotcode='"+ryotcode+"''");
		String qry = "select sum(netwt) as netwt from WeighmentDetails where season='"+season+"' and ryotcode='"+ryotcode+"'";	
		Double supplyQty=ahFunctionalDao.GetRyotSupply(qry);
		return supplyQty;
    }
	public Double getTotalLoanAmt(String season,String ryotcode)
    {
		String qry = "select sum(disbursedamount) as loan from LoanDetails where season='"+season+"' and ryotcode='"+ryotcode+"' and disburseddate is not null";	
		Double advAmount=ahFunctionalDao.getTotalLoanAmt(qry);
        return advAmount;
    }
	public List getCompletedCaneAccounts(String table)
    {        
        return ahFunctionalDao.getCompletedCaneAccounts(table);
    }
	public List getRyotList(int nCaneAcSlNo, String strTable)
    {        
        return ahFunctionalDao.getRyotList(nCaneAcSlNo, strTable);
    }	
	public Double bankwiseData1(String season,Integer caneslno,Integer bkcode)
    {
		Double transport=(Double) ahFunctionalDao.GetAccSubGrpCode("select sum(paidamt) as totalamt from CanAccLoansDetails  where advloan=0 and season='"+season+"'  and caneacctslno='"+caneslno+"' and branchcode="+bkcode+" ");
        return transport;
    }
	
	public List getHarvestingOrTpDetails(int code)
    {        
        return ahFunctionalDao.getHarvestingOrTpDetails(code);
    }
	public int ContCode(String ryotcode,String season,int code)
    {        
        return ahFunctionalDao.ContCode(ryotcode,season,code);
    }
	//added by naidu on 04-02-2017
	public Double getPrincipleAdvAmt(String season,String ryotcode)
    {
		String qry = "select sum(advanceamount) as padvance from AdvanceDetails where season='"+season+"' and ryotcode='"+ryotcode+"'";	
		Double advAmount=ahFunctionalDao.getPrincipleAdvAmt(qry);
        return advAmount;
    }

	//Added by DMurty on 01-02-2017
	public List getTempAgreementsForRyot(String ryotcode,String season)
    {        
        return ahFunctionalDao.getTempAgreementsForRyot(ryotcode,season);
    }
	
	//Added by DMurty on 13-02-2017
	public List getSeedSupplierAccounts(String season)
    {        
        return ahFunctionalDao.getSeedSupplierAccounts(season);
    }
	
	public List getRunningBalForAccount(String accountCode,String season)
    {        
        return ahFunctionalDao.getRunningBalForAccount(accountCode,season);
    }
	
	public List getRunningBalForAccountGrp(int accountgroupcode,String season)
    {        
        return ahFunctionalDao.getRunningBalForAccountGrp(accountgroupcode,season);
    }
	public List getRunningBalForAccountSubGrp(int accountgroupcode,String season)
    {        
        return ahFunctionalDao.getRunningBalForAccountSubGrp(accountgroupcode,season);
    }
	public List getWeighmentDetailsData()
    {        
        return ahFunctionalDao.getWeighmentDetailsData();
    }
	public List GetAccInfo(String ryotCode,String season,Integer tCode,String glcode)
    {        
        return ahFunctionalDao.GetAccInfo(ryotCode, season,tCode,glcode);
    }

	public List getWeighmentDatesData()
    {        
        return ahFunctionalDao.getWeighmentDatesData();
    }
	
	public List updateLoanPrinciplesForAccountedRyots()
    {        
        return ahFunctionalDao.updateLoanPrinciplesForAccountedRyots();
    }
	
	public List getOldLoans()
    {        
        return ahFunctionalDao.getOldLoans();
    }
	
	public List checkLoanDetails(int loanNumber,int branchCode,String season)
    {        
        return ahFunctionalDao.checkLoanDetails(loanNumber,branchCode,season);
    }
	public List getOldLoanDetails(int loanNumber,int branchCode,String season)
    {        
        return ahFunctionalDao.getOldLoanDetails(loanNumber,branchCode,season);
    }
	
	public List loanReconsilation(String suppliedOrNot)
    {        
        return ahFunctionalDao.loanReconsilation(suppliedOrNot);
    }
	public List getRyotSupplyAndPayDetails(String ryotcode,String season)
    {        
        return ahFunctionalDao.getRyotSupplyAndPayDetails(ryotcode,season);
    }
	//Added by DMurty on 24-02-2017
	public Double getTpAllowance(String season,String ryotcode)
    {
		String qry = "select sum(transportallowance) as loan from WeighmentDetails where season='"+season+"' and ryotcode='"+ryotcode+"'";	
		Double advAmount=ahFunctionalDao.getTotalLoanAmt(qry);
        return advAmount;
    }
	@SuppressWarnings("unchecked")
	public List<Ryot> getRyotNameAndVilCode(String ryotCode)
    {
        String sql = "select * from ryot where ryotcode ='"+ryotCode+"'";
		//logger.info("---------sql---------------"+sql);
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }
	public Double GetTotalAdvanceAmount(String season,String ryotCode,Integer advCode)
    {
		Double advanceAmount=(Double) ahFunctionalDao.GetAccSubGrpCode("select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+" ");
        return advanceAmount;
    }
	public void updateRyotNameInAgreement(String ryotcode,String ryotname,String relativename)
	{
		 ahFunctionalDao.updateRyotNameInAgreement(ryotcode, ryotname,relativename);
	}
	public List<LabourContractor> getLabourContractorAccCode(int lccode)
    {       
        return ahFunctionalDao.getLabourContractorAccCode(lccode);
    }
	
	public int getZoneCodeForVillage(String villagecode)
    {
		int zonecode=(Integer) ahFunctionalDao.GetAccSubGrpCode("select m.zonecode from Village v,Mandal m where v.mandalcode=m.mandalcode and v.villagecode='"+villagecode+"'");
        return zonecode;
    }
	public String GetZoneNameByZoneCode(int zonecode)
    {
		String ZoneName=(String) ahFunctionalDao.GetAccSubGrpCode("select zone from Zone where zonecode="+zonecode+"");
        return ZoneName;
    }
	public String GetUnloadingContractorForWeighment(Integer ulcode)
    {
		String ulname=(String) ahFunctionalDao.GetAccSubGrpCode("select name from UnloadingContractor where ucCode="+ulcode+"");
        return ulname;
    }
	public String GetAdvanceNameByAdvCode(Integer advcode)
    {
		String advname=(String) ahFunctionalDao.GetAccSubGrpCode("select advance from CompanyAdvance where advancecode="+advcode+"");
        return advname;
    }
	public String GetBranchNameByBranchCode(Integer branchCode)
    {
		String branch=(String) ahFunctionalDao.GetAccSubGrpCode("select branchname from Branch where branchcode="+branchCode+"");
        return branch;
    }
	public List getPincodeDataFromTemp()
    {
    return ahFunctionalDao.getPincodeDataFromTemp();
    }
	public List<AccountDetails> getAccountDetailsForSeed(String season,int transactioncode)
    {
        return ahFunctionalDao.getAccountDetailsForSeed( season,transactioncode);
    }
	public List<AccountGroupDetails> getAccountGroupDetailsForSeed(String season,int transactioncode)
    {
        return ahFunctionalDao.getAccountGroupDetailsForSeed(season,transactioncode);
    }
	public List<AccountSubGroupDetails> getAccountSubGroupDetailsForSeed(String season,int transactioncode)
    {
        return ahFunctionalDao.getAccountSubGroupDetailsForSeed(season,transactioncode);
    }
	public List getRunningBalForSeedAccount(String accountCode,String season,int transactionCode,String acctype,int accgrpcode)
    {        
		
		double cr = 0.0;
		double dr = 0.0;
		double runBal = 0.0;
		String balType = "";
		List ls=new ArrayList();
		  List AccList =null;
		if(acctype.equalsIgnoreCase("accSmry"))
		{
        AccList = ahFunctionalDao.getRunningBalForSeedAccount(accountCode,season,transactionCode);
		}
		else if(acctype.equalsIgnoreCase("accGrpSmry"))
		{
			 AccList = ahFunctionalDao.getRunningBalForSeedAccountGrp(Integer.parseInt(accountCode),season,transactionCode);
		}
		else if(acctype.equalsIgnoreCase("accSubGrpSmry"))
		{
			 AccList = ahFunctionalDao.getRunningBalForSeedAccountSubGrp(Integer.parseInt(accountCode),season,transactionCode,accgrpcode);
		}

		if(AccList != null && AccList.size()>0 )
		{
			Map AccSmryMap = new HashMap();
			AccSmryMap=(Map)AccList.get(0);
			if( (Double)((Map<Object,Object>)AccList.get(0)).get("cr")!=null )
			cr=(Double)AccSmryMap.get("cr");
			if( (Double)((Map<Object,Object>)AccList.get(0)).get("dr")!=null )
			dr=(Double)AccSmryMap.get("dr");
			
			if(cr>dr)
			{
				runBal = cr-dr;
				balType = "Cr";
			}
			else
			{
				runBal = dr-cr;
				balType = "Dr";
			}
			Map AccSmryMap1 = new HashMap();
			AccSmryMap1.put("runBal", runBal);
			AccSmryMap1.put("balType", balType);
			ls.add(AccSmryMap1);
			
		}
		return ls;
    }
	public List getRunningBalForSeedAccountGrp(int accountgroupcode,String season,int transactionCode)
    {        
        return ahFunctionalDao.getRunningBalForSeedAccountGrp(accountgroupcode,season,transactionCode);
    }
	public List getRunningBalForSeedAccountSubGrp(int accountgroupcode,String season,int transactionCode,int accountsubgroupcode)
    {        
        return ahFunctionalDao.getRunningBalForSeedAccountSubGrp(accountgroupcode,season,transactionCode,accountsubgroupcode);
    }
	@SuppressWarnings("unchecked")
	public List<AdvanceDetails> getAdvanceDetailsBySeasonForSeed(String season,String ryotcode,int advancecode,int transactioncode)
    {
        String sql = "select * from AdvanceDetails where season ='" + season+"' and ryotcode='" + ryotcode+"' and advancecode="+advancecode+" and transactioncode="+transactioncode+"";        
        return hibernateDao.findByNativeSql(sql, AdvanceDetails.class);
    }
	public List<AccountDetails> getAccountDetailsForSeedSuuplier(String season,String glcode)
    {
		return ahFunctionalDao.getAccountDetailsForSeedSuuplier(season,glcode);
    }
	public List<AccountGroupDetails> getAccountGpDetailsForSeedSuuplier(String season,int transactioncode)
    {
		return ahFunctionalDao.getAccountGpDetailsForSeedSuuplier(season,transactioncode);
    }
	public List getDistinctAccountsForSeedSuuplier(String season,String glcode)
    {
		return ahFunctionalDao.getDistinctAccountsForSeedSuuplier(season,glcode);
    }
	public List<Temp_BatchServiceMaster> getBatchServicemasterDataForMig(String season)
    {
		return ahFunctionalDao.getBatchServicemasterDataForMig(season);
    }
	public List getSuppliersFromBatchSeries(String season)
    {
		return ahFunctionalDao.getSuppliersFromBatchSeries(season);
    }
	public List<SeedSource> listSourceSeedsForBatch(String supcode,String season)
	{
		return ahFunctionalDao.listSourceSeedsForBatch(supcode,season);
	}
	public List getBatchForSupplier(String suppliercode)
    {
		return ahFunctionalDao.getBatchForSupplier(suppliercode);
    }
}
