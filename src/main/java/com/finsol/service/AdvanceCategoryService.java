package com.finsol.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.AdvanceCategoryDao;
import com.finsol.model.AdvanceCategory;
import com.finsol.model.CaneManager;


/**
 * @author DMurty
 */

@Service("AdvanceCategoryService")

public class AdvanceCategoryService
{
	private static final Logger logger = Logger.getLogger(AdvanceCategoryService.class);
	
	@Autowired
	private AdvanceCategoryDao advanceCategoryDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAdvanceCategory(AdvanceCategory advanceCategory)
	{
		advanceCategoryDao.addAdvanceCategory(advanceCategory);
	}
	
	public List<AdvanceCategory> listAdvanceCategories() {
		return advanceCategoryDao.listAdvanceCategories();
	}
	

	

}
