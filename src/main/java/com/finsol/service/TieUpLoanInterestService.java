package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.TieUpLoanInterestDao;
import com.finsol.dao.TransportingContractorDao;
import com.finsol.model.BankLoanIntRates;
import com.finsol.model.BankLoanRepayDt;
import com.finsol.model.TCSuretyDetails;
import com.finsol.model.TransportingContractors;

/**
 * @author Rama Krishna
 *
 */
@Service("TieUpLoanInterestService")
public class TieUpLoanInterestService {
	
	

	
	private static final Logger logger = Logger.getLogger(TieUpLoanInterestService.class);

	@Autowired
	private TieUpLoanInterestDao tieUpLoanInterestDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTieUpLoanInterest(BankLoanRepayDt bankLoanRepayDt) {
		tieUpLoanInterestDao.addTieUpLoanInterest(bankLoanRepayDt);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addTieUpLoanInterestDetails(BankLoanIntRates bankLoanIntRates) {
		tieUpLoanInterestDao.addTCSuretyDetails(bankLoanIntRates);
	}
	

	
	public List<BankLoanRepayDt> listTieUpLoanInterests() {
		return tieUpLoanInterestDao.listTieUpLoanInterests();
	}
	public List<BankLoanIntRates> listTieUpLoanInterestDetails() {
		return tieUpLoanInterestDao.listTieUpLoanInterestDetails();
	}
	
	public void deleteBankLoanIntRates(BankLoanRepayDt bankLoanRepayDt) {
		tieUpLoanInterestDao.deleteBankLoanIntRates(bankLoanRepayDt);
	}
	
	public BankLoanRepayDt getBankLoanRepayDtls(String season,int branchCode) {
		return tieUpLoanInterestDao.getBankLoanRepayDtls(season,branchCode);
	}
	
	public List<BankLoanIntRates> getBankLoanIntRatesByCode(Integer tcode)
    {
        return tieUpLoanInterestDao.getBankLoanIntRatesByCode(tcode);
    }


	

}
