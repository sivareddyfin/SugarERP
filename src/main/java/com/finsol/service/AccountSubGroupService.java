package com.finsol.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.dao.AccountSubGroupDao;
import com.finsol.model.AccountSubGroup;

/**
 * @author DMurty
 */
@Service("AccountSubGroupService")
public class AccountSubGroupService 
{
	private static final Logger logger = Logger.getLogger(CaneManagerService.class);
	
	@Autowired
	private AccountSubGroupDao accountSubGroupDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAccountSubGroup(AccountSubGroup accountSubGroup)
	{
		accountSubGroupDao.addAccountSubGroup(accountSubGroup);
	}
	
	public List<AccountSubGroup> listAllAccountSubGroups()
	{
		return accountSubGroupDao.listAccountSubGroups();
	}
	
	
	
}
