package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountMaster;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;


@Repository("RyotDao")
@Transactional
public class RyotDao {
	
	private static final Logger logger = Logger.getLogger(RyotDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addRyot(Ryot ryot) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(ryot);
		session.flush();
		session.close();
	}
	
	public void addRyotBankDetails(RyotBankDetails ryotBankDetails) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(ryotBankDetails);
		session.flush();
		session.close();
	}
	
	
	public void addAccountMaster(AccountMaster accountMaster) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountMaster);
		session.flush();
		session.close();
	}
	
/*	public void deleteBranch(Branch branch) {
		hibernateDao.getSessionFactory().getCurrentSession().createQuery("DELETE FROM Branch WHERE bankcode = "+branch.getBankcode()).executeUpdate();
	}*/
	
	@SuppressWarnings("unchecked")
	public List<Ryot> listRyots() 
	{
		return (List<Ryot>) hibernateDao.getSessionFactory().openSession().createCriteria(Ryot.class).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RyotBankDetails> listRyotBankDetails() 
	{
		return (List<RyotBankDetails>) hibernateDao.getSessionFactory().openSession().createCriteria(RyotBankDetails.class).list();
	}
	
	public List<Ryot> getRyot(String id) {
		
		//id = "\"" + id + "\"";

		String sql = "select * from Ryot where ryotcode ='"+id+"'";
        
        return hibernateDao.findByNativeSql(sql, Ryot.class);
		
		
	}
	
	public List<RyotBankDetails> getBranchByRyotCode(String ryotcode)
    {
        String sql = "select * from RyotBankDetails where ryotcode =" + ryotcode;
        
        return hibernateDao.findByNativeSql(sql, RyotBankDetails.class);
    }
	

	public void deleteRyotBankDetails(Ryot ryot) 
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM RyotBankDetails WHERE ryotcode = '"+ryot.getRyotcode()+"'").executeUpdate();
	}
	
	public List<Ryot> getRyotByRyotCode(String ryotcode)
    {
        String sql = "select * from Ryot where ryotcode ='"+ryotcode+"'";
        
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }
	public List<Ryot> getRyotByRyotCodeNew(String ryotcode)
    {
        String sql = "select * from Ryot where ryotcode =" + ryotcode;
        
        return hibernateDao.findByNativeSql(sql, Ryot.class);
    }

}
