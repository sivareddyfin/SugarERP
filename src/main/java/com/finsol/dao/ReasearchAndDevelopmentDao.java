package com.finsol.dao;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AgreementDetails;
import com.finsol.model.AuthorisationFormDetails;
import com.finsol.model.AuthorisationFormSummary;
import com.finsol.model.BatchMaster;
import com.finsol.model.BudCuttingDetails;
import com.finsol.model.BudCuttingSummary;
import com.finsol.model.BudRemovedCane;
import com.finsol.model.CaneShiftingSummary;
import com.finsol.model.ChemicalTreatmentDetails;
import com.finsol.model.ChemicalTreatmentInventoryDetails;
import com.finsol.model.ChemicalTreatmentSummary;
import com.finsol.model.DetrashCaneDetails;
import com.finsol.model.DetrashSummary;
import com.finsol.model.DispatchAckSummary;
import com.finsol.model.DispatchDetails;
import com.finsol.model.DispatchSummary;
import com.finsol.model.GradingDetails;
import com.finsol.model.GradingSummary;
import com.finsol.model.GreenHouseDetails;
import com.finsol.model.GreenHouseSummary;
import com.finsol.model.HardeningDetails;
import com.finsol.model.HardeningSummary;
import com.finsol.model.KindIndentDetails;
import com.finsol.model.KindIndentSummary;
import com.finsol.model.KindType;
import com.finsol.model.Machine;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedlingEstimate;
import com.finsol.model.ShiftedCaneDetails;
import com.finsol.model.TrayFillingDetails;
import com.finsol.model.TrayFillingSummary;
import com.finsol.model.TrimmingDetails;
import com.finsol.model.TrimmingSummary;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("ReasearchAndDevelopmentDao")
@Transactional
public class ReasearchAndDevelopmentDao 
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	@SuppressWarnings("unchecked")
	public List<Machine> listMachines()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Machine> machines=session.createCriteria(Machine.class).list();
		session.close();
		return machines;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<KindType> listKindTypes()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<KindType> kindTypes=session.createCriteria(KindType.class).list();
		session.close();
		return kindTypes;
	}
	
	@SuppressWarnings("unchecked")
	public List<BudRemovedCane> listBudRemovedCanes(String buddate)
	{
	        String sql = "select * from BudRemovedCane where budremoveddate ='" +DateUtils.getSqlDateFromString(buddate,Constants.GenericDateFormat.DATE_FORMAT)+"'";
	        return hibernateDao.findByNativeSql(sql, BudRemovedCane.class);
	}
	public List getEstimateDates()
	{
		String sql="select distinct budremoveddate from BudRemovedCane order by budremoveddate";
		logger.info("getEstimateDates() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getOtherRyots()
	{
		String sql="select * from OtherRyot order by id";
		logger.info("getOtherRyots() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getseedsourceChangeSummary(String batchSeries)
	{
		String sql="select * from SeedSource where batchno='" +batchSeries+"' ";
		logger.info("getseedsourceChangeSummary() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getseedSupplyRyots()
	{
		String sql="select distinct seedsrcode from SeedSource ";
		logger.info("getseedSupplyRyots() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getseedsourceChangeSummaryreport(String fromdate,String todate,String seedsupplier)
	{
		if("All".equals(seedsupplier))
		{
			String sql="select * from SeedSource where sourcesupplieddate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getseedsourceChangeSummaryreport() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			String sql="select * from SeedSource where seedsrcode='" +seedsupplier+"' and sourcesupplieddate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getseedsourceChangeSummaryreport() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
	}
	public List getcircledetailsfordispatch(Integer circle)
	{
		String sql="select * from Circle where circlecode='" +circle+"' ";
		logger.info("getcircledetailsfordispatch() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getcircledetailsbyfieldman(Integer fieldman)
	{
		String sql="select * from Circle where fieldassistantid='" +fieldman+"' ";
		logger.info("getcircledetailsbyfieldman() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getdispatchdetailsforpendingseedlings(Integer seqno,String season)
	{
		String sql="select * from DispatchSummary where dispatchSeq=" +seqno+" and season='"+season+"'";
		logger.info("getdispatchdetailsforpendingseedlings() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	//findBySql
		public List getMaxryotdetailsforpendingseedlings(String qry)
	    {
			return hibernateDao.findBySql(qry);
	    }
		public List getSumofDispatch(String qry)
	    {
			return hibernateDao.findBySqlCriteria(qry);
	    }
	
	public List getaggrementDetails(String ryotcode,String season)
	{
		String sql="select * from RandDAgreementSummary where ryotcode='" +ryotcode+"' and seasonyear='"+season+"'";
		logger.info("getaggrementDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getauthaggrementDetails(String season,String ryotcode)
	{
		String sql="select * from AgreementSummary where ryotcode='" +ryotcode+"' and seasonyear='"+season+"'";
		logger.info("getauthaggrementDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getContratorDetails()
	{
		String sql="select * from LabourContractor order by lcName";
		logger.info("getContratorDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getTransportContratorDetails()
	{
		String sql="select * from TransportContractor order by tcName";
		logger.info("getTransportContratorDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}	
		
	public List getseedsourceLoadSummary(String batchSeries)
	{
		String sql="select * from BatchServicesMaster where batchseries='" +batchSeries+"' ";
		logger.info("getseedsourceLoadSummary() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getOtherRyotsnames(String ryotcode)
	{
		String sql="select * from OtherRyot where ryotCode='" +ryotcode+"' ";
		logger.info("getOtherRyotsnames() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getTrayType()
	{
		String sql="select * from SeedlingTray order by traycode";
		logger.info("getTrayType() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	@SuppressWarnings("unchecked")
	public List<SeedlingEstimate> listSeedlingEstimate()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<SeedlingEstimate> seedlingEstimate=session.createCriteria(SeedlingEstimate.class).list();
		session.close();
		return seedlingEstimate;
	}
	
	@SuppressWarnings("unchecked")
	public List<SeedSource> listSourceSeeds(String batchno,String year,String dateid)
	{	
		String Presentdetails[] = dateid.split("-");
			String id=Presentdetails[3];
		String sql = "select * from SeedSource where batchno ='" +batchno+"' and id="+id+" ";// and runningyear='"+year+"'
		return hibernateDao.findByNativeSql(sql, SeedSource.class);
	}
	
	public List getbatchSourceofSeeds(String Year,String seedsupplier,String otherryot)
	{
		String sql="";
		if(seedsupplier!=null)
		{
		 sql="select * from BatchServicesMaster where seedsuppliercode='"+seedsupplier+"' ";//runningyear='"+Year+"' and 
		}
		else
		{
		sql="select * from BatchServicesMaster where  seedsuppliercode='"+otherryot+"' ";//runningyear='"+Year+"' and
		}
		logger.info("getbatchSourceofSeeds() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getYearSourceofSeeds()
	{
		String sql="select distinct runningyear from BatchServicesMaster";
		logger.info("getYearSourceofSeeds() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllBatchNumbers(String batchseries)
	{
      //  String sql = "select distinct accountname,accountcode from AccountMaster where UPPER(accountname) like '"+accName.toUpperCase()+"%' order by accountname";

		String sql="select distinct batchseries from BatchServicesMaster where UPPER(batchseries) like '"+batchseries.toUpperCase()+"%'  ";//AND isbatchprepared=0
		logger.info("listAllBatchNumbers() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllBatchSeries(String batchseries)
	{

		String sql="select distinct batch from BatchMaster where UPPER(batch) like '"+batchseries.toUpperCase()+"%' AND totalSeedlings>0 ";//AND isBatchCompleted=0 
		logger.info("listAllBatchSeries() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List listAllBatchSeriesForDispatch(String batchseries,int variety)
	{

		String sql="select distinct batch from BatchMaster where UPPER(batch) like '"+batchseries.toUpperCase()+"%' AND totalSeedlings>0  and variety='"+variety+"'";//AND isBatchCompleted=0 
		logger.info("listAllBatchSeries() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	
	public List listAllKindTypeDetails()
	{

		String sql="select distinct kindType,id from KindType where status=0" ;
		logger.info("listAllKindTypeDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllKindDetailsByType(Integer id)
	{
		String sql="select * from KindType where id = "+id+"  ";
		logger.info("listAllKindDetailsByType() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listGetEmployeeType(String loginid)
	{
		String sql="select empStatus from Employees where  UPPER(loginid) = '"+loginid.toUpperCase()+"'  ";
		logger.info("listGetEmployeeType() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List GetFeildOfficer(Integer fieldassitant)
	{
		String sql="select * from FieldAssistant where   fieldassistantid= "+fieldassitant+"  ";
		logger.info("GetFeildOfficer() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List GetFeildOfficerZone(Integer foid)
	{
		String sql="select * from Zone where  fieldofficerid= "+foid+"  ";
		logger.info("GetFeildOfficerZone() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List GetFeildOfficerempid(Integer empid)
	{
		String sql="select * from FieldOfficer where fieldOfficerid= '"+empid+"'  ";
		logger.info("GetFeildOfficerempid() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	
	public List listGetIndents(String empStatus,String Season,String Dfromdate,String Dtodate,Integer variety,Integer fieldman)
	{
		
		String sql="";
		
		if(fieldman==0)
		{
		if("3".equals(empStatus))//All
		{
			if("".equals(variety)||"null".equals(variety)||(null==variety))
			{
			 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and  A.indentNo = B.indentNo" ;
			}
			else
			{
			sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo" ;
			}
		}
		else if("0".equals(empStatus))//Request
		{
			if("".equals(variety)||"null".equals(variety)||(null==variety))
			{
			 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus!=3 ";
			}
			else
			{
			sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus!=3 ";
			}
		}
		else if("1".equals(empStatus))//Authorise
		{
			if("".equals(variety)||"null".equals(variety)||(null==variety))
			{
			 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and A.indentStatus!=3 ";
			}
			else
			{
			sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and A.indentStatus!=3 ";
			}
		}
		else if("2".equals(empStatus))//Pending
		{
			if("".equals(variety)||"null".equals(variety)||(null==variety))
			{
			 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and A.indentStatus=3 ";
			}
			else
			{
			sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo  and A.indentStatus=3 ";
			}
		}
		else
		{
			if("".equals(variety)||"null".equals(variety)||(null==variety))
			{
			 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and a.indentStatus=0 and A.indentStatus!=3 ";
			}
			else
			{
			sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and A.indentNo = B.indentNo and a.indentStatus=0 and A.indentStatus!=3 ";
			}
		}
		}
		else
		{

			if("3".equals(empStatus))//All
			{
				if("".equals(variety)||"null".equals(variety)||(null==variety))
				{
				 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+"  and A.indentNo = B.indentNo" ;
				}
				else
				{
				sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+" and A.indentNo = B.indentNo" ;
				}
			}
			else if("0".equals(empStatus))//Request
			{
				if("".equals(variety)||"null".equals(variety)||(null==variety))
				{
				 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+"  and A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus!=3 ";
				}
				else
				{
				sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+" and A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus!=3 ";
				}
			}
			else if("1".equals(empStatus))//Authorise
			{
				if("".equals(variety)||"null".equals(variety)||(null==variety))
				{
				 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+"  and A.indentNo = B.indentNo and A.indentStatus!=3 ";
				}
				else
				{
				sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+" and A.indentNo = B.indentNo and A.indentStatus!=3 ";
				}
			}
			else if("2".equals(empStatus))//Pending
			{
				if("".equals(variety)||"null".equals(variety)||(null==variety))
				{
				 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+"  and A.indentNo = B.indentNo and A.indentStatus=3 ";
				}
				else
				{
				sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+" and A.indentNo = B.indentNo  and A.indentStatus=3 ";
				}
			}
			else
			{
				if("".equals(variety)||"null".equals(variety)||(null==variety))
				{
				 sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+"  and A.indentNo = B.indentNo and a.indentStatus=0 and A.indentStatus!=3 ";
				}
				else
				{
				sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where  A.batchDate between '" + Dfromdate+"' and '" + Dtodate+"' and A.season='" + Season+"'  and B.fieldman=" + fieldman+" and A.indentNo = B.indentNo and a.indentStatus=0 and A.indentStatus!=3 ";
				}
			}
			
			
		}
		
		logger.info("listGetIndents() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllBatchNumbersDetails(String batchseries)
	{
		String sql="select varietycode,seedsuppliercode,landvillagecode from BatchServicesMaster where batchseries = '"+batchseries+"'  ";
		logger.info("listAllBatchNumbersDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List listAllBatchSeriesDetails(String batchno)
	{
		String sql="select * from BatchMaster where batch = '"+batchno+"'  ";
		logger.info("listAllBatchSeriesDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}	
	@SuppressWarnings("unchecked")
	public List<DetrashSummary> listAllDetrashingSummaryDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from DetrashSummary where season = '"+Season+"' AND detrashingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllDetrashingSummaryDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, DetrashSummary.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<DetrashCaneDetails> listAllDetrashingDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from DetrashCaneDetails where season = '"+Season+"' AND detrashingdetailsdate = '"+AddedDate+"' AND detrashingshift = "+Shift+" ";
		logger.info("listAllDetrashingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, DetrashCaneDetails.class);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CaneShiftingSummary> listAllCaneShiftingSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from CaneShiftingSummary where season = '"+Season+"' AND caneShiftingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllCaneShiftingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, CaneShiftingSummary.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<ShiftedCaneDetails> listAllShiftedCaneDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from ShiftedCaneDetails where season = '"+Season+"' AND shiftedCaneDate = '"+AddedDate+"' AND shiftedCaneShift = "+Shift+" ";
		logger.info("listAllShiftedCaneDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, ShiftedCaneDetails.class);
	}
	
	public void deleteDetrashDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DetrashCaneDetails WHERE season = '"+Season+"' AND detrashingdetailsdate = '"+AddedDate+"' AND detrashingshift = "+Shift).executeUpdate();
	}
	public void deleteShiftedCaneDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ShiftedCaneDetails WHERE season = '"+Season+"' AND shiftedCaneDate = '"+AddedDate+"' AND shiftedCaneShift = "+Shift).executeUpdate();
	}
	public void deleteBudCuttingDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM BudCuttingDetails WHERE season = '"+Season+"' AND cuttingDetailsdate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	public List<BudCuttingSummary> listAllBudCuttingSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from BudCuttingSummary where season = '"+Season+"' AND budCuttingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllBudCuttingSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, BudCuttingSummary.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<BudCuttingDetails> listAllBudCuttingDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from BudCuttingDetails where season = '"+Season+"' AND cuttingDetailsdate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllBudCuttingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, BudCuttingDetails.class);
	}
	public void deleteChemicalinventoryDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ChemicalTreatmentInventoryDetails WHERE productSeason = '"+Season+"' AND productDate = '"+AddedDate+"' AND productShift = "+Shift).executeUpdate();
	}
	public void deleteChemicalDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ChemicalTreatmentDetails WHERE season = '"+Season+"' AND TreatmentDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	public List<ChemicalTreatmentSummary> listAllChemicalTreatmentSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from ChemicalTreatmentSummary where season = '"+Season+"' AND Treatmentdate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllChemicalTreatmentSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, ChemicalTreatmentSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<ChemicalTreatmentInventoryDetails> listAllChemicalTreatmentInventoryDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from ChemicalTreatmentInventoryDetails where productSeason = '"+Season+"' AND productDate = '"+AddedDate+"' AND productShift = "+Shift+" ";
		logger.info("listAllChemicalTreatmentInventoryDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, ChemicalTreatmentInventoryDetails.class);
	}
	@SuppressWarnings("unchecked")
	public List<ChemicalTreatmentDetails> listAllChemicalTreatmentDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from ChemicalTreatmentDetails where season = '"+Season+"' AND TreatmentDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllChemicalTreatmentDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, ChemicalTreatmentDetails.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<GradingSummary> listAllGradingSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from GradingSummary where season = '"+Season+"' AND gradingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllGradingSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, GradingSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<GradingDetails> listAllGradingDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from GradingDetails where season = '"+Season+"' AND gradingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllGradingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, GradingDetails.class);
	}
	public void deleteGradingDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM GradingDetails WHERE season = '"+Season+"' AND gradingDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	
	public void deleteTrayfillingDetalis(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM TrayFillingDetails WHERE season = '"+Season+"' AND trayFillingDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	public List<TrayFillingSummary> listAllTrayFillingSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from TrayFillingSummary where season = '"+Season+"' AND trayFillingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllTrayFillingSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, TrayFillingSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<TrayFillingDetails> listAllTrayFillingDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from TrayFillingDetails where season = '"+Season+"' AND trayFillingDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllTrayFillingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, TrayFillingDetails.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<BatchMaster> getbatchMasterDetails(String Season,String addedDate,String Batchseries,Integer batchdate,Integer batchmonth )
	{
		String sql="select * from BatchMaster where season = '"+Season+"' AND trayFillingDate = '"+addedDate+"' AND batchSeries = '"+Batchseries+"' AND  batchMonth = "+batchmonth+" AND batchDate = "+batchdate+" ";
	
		logger.info("getbatchMasterDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, BatchMaster.class);
	}
	@SuppressWarnings("unchecked")
	public List<BatchMaster> getbatchMasterDetailsforupdate(String batchseries )
	{
		String sql="select * from BatchMaster where batch='"+batchseries+"'  ";
	
		logger.info("getbatchMasterDetailsforupdate() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, BatchMaster.class);
	}
	
	public void updateBatchMasterDetails(Double totalSeedlings,Double survivalper,Double balance,Double takenoff,String Season,String addedDate,String Batchseries,Integer batchdate,Integer batchmonth )
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE BatchMaster set totalSeedlings="+totalSeedlings+",survivalPercent="+survivalper+",balance="+balance+",takenOffOnes="+takenoff+" WHERE season = '"+Season+"' AND trayFillingDate = '"+addedDate+"' AND batchSeries = '"+Batchseries+"' AND  batchMonth = "+batchmonth+"  AND batchDate = "+batchdate+" ").executeUpdate();
	}
	public void updateDispatchBatchMasterDetails(Double totalSeedlings,Double survivalper,Double balance,Double takenoff,String Batchseries)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE BatchMaster set survivalPercent="+survivalper+",balance="+balance+",takenOffOnes="+takenoff+" WHERE batchSeries = '"+Batchseries+"' ").executeUpdate();
	}
	
	public void deleteGreenHouseDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM GreenHouseDetails WHERE season = '"+Season+"' AND detailsDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	public void deleteHardeningDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM HardeningDetails WHERE season = '"+Season+"' AND detailsDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	public void deleteTrimmingDetails(String Season,String AddedDate,Integer Shift)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM TrimmingDetails WHERE season = '"+Season+"' AND detailsDate = '"+AddedDate+"' AND shift = "+Shift).executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<GreenHouseSummary> listAllGreenHouseSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from GreenHouseSummary where season = '"+Season+"' AND summaryDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllGreenHouseSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, GreenHouseSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<GreenHouseDetails> listAllGreenHouseDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from GreenHouseDetails where season = '"+Season+"' AND detailsDate= '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllGreenHouseDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, GreenHouseDetails.class);
	}
	@SuppressWarnings("unchecked")
	public List<TrimmingSummary> listAllTrimmingSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from TrimmingSummary where season = '"+Season+"' AND summaryDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllTrimmingSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, TrimmingSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<TrimmingDetails> listAllTrimmingDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from TrimmingDetails where season = '"+Season+"' AND detailsDate= '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllTrimmingDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, TrimmingDetails.class);
	}
	@SuppressWarnings("unchecked")
	public List<HardeningSummary> listAllHardenningSummary(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from HardeningSummary where season = '"+Season+"' AND summaryDate = '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllHardenningSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, HardeningSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<HardeningDetails> listAllHardenningDetails(String Season,String AddedDate,Integer Shift)
	{
		String sql="select * from HardeningDetails where season = '"+Season+"' AND detailsDate= '"+AddedDate+"' AND shift = "+Shift+" ";
		logger.info("listAllHardenningDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, HardeningDetails.class);
	}
	@SuppressWarnings("unchecked")
	public void deleteIndentDetails(String Season,String indentNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM KindIndentDetails WHERE season = '"+Season+"'  AND indentNo = '"+indentNo+"' ").executeUpdate();
	}
	@SuppressWarnings("unchecked")
	public void deleteAckDetails(String Season,String dispatchno)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DispatchAckDetails WHERE season = '"+Season+"'  AND dispatchNo = '"+dispatchno+"' ").executeUpdate();
	}
	@SuppressWarnings("unchecked")
	public void deleteDispatchDetails(String Season,String dispatchno)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM DispatchDetails WHERE season = '"+Season+"'  AND dispatchNo = '"+dispatchno+"' ").executeUpdate();
	}
	
	public List getAuthorisedIndentsforDispatch(String RyotCode,Integer FieldOfficer)
	{
		String sql="select indentNo from KindIndentSummary where ryotCode='"+RyotCode+"' and (indentStatus=1 OR indentStatus=2)";
		logger.info("getAuthorisedIndentsforDispatch() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getAuthorisedIndentsforDispatch1(String RyotCode,String Season)
	{
		String sql="select indentNo from KindIndentSummary where ryotCode='"+RyotCode+"' and season='"+Season+"' and (indentStatus=1 OR indentStatus=2) ";
		logger.info("getAuthorisedIndentsforDispatch1() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getAuthorisedIndentsforDispatchpopup(String IndentNo )
	{
		String sql="select * from KindIndentDetails where indentNo='"+ IndentNo+"'  and pendingQuantity>0    ";
		logger.info("getAuthorisedIndentsforDispatch() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getDispatchNoDetailsForPrint(String season,String fromddate,String todate )
	{
		String sql="select * from DispatchSummary where season='"+ season+"'  and dateOfDispatch between '" + fromddate+"' and '" + todate+"'    ";
		logger.info("getDispatchNoDetailsForPrint() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getAuthorisationDetailsForPrint(String season,String fromddate,String todate )
	{
		String sql="select * from AuthorisationFormSummary where season='"+ season+"' and  authorisationDate between '" + fromddate+"' and '" + todate+"'    ";
		logger.info("getAuthorisationDetailsForPrint() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getDispatchDetails(String dispatchno )
	{
		String sql="select * from DispatchSummary where dispatchNo='"+ dispatchno+"' ";
		logger.info("getDispatchDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List listGetAllIndentDetails(String indentno)
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"'  ";
		logger.info("listGetAllIndentDetails() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentDetails.class);
	}
	public List listGetAllIndentDetailsbykind(String indentno,Integer kind)
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"' and kind="+kind+"  ";
		logger.info("listGetAllIndentDetails() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentDetails.class);
	}
	
	public List listGetAllvarietyIndentDetails(String indentno,Integer variety)
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"' and variety="+variety+" ";
		logger.info("listGetAllIndentDetails() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentDetails.class);
	}
	public List listGetAllIndentSummary(String indentno)
	{
		String sql="select * from KindIndentSummary where indentNo= '"+indentno+"' ";
		logger.info("listGetAllIndentSummary() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentSummary.class);
	}
	public List listGetDetailsbyKind(Integer kind)
	{
		String sql="select * from KindType where id= '"+kind+"' ";
		logger.info("listGetDetailsbyKind() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindType.class);
	}
	
	public List listGetOnloadIndentsstatus(String empStatus)
	{
		String sql="";
		if("0".equals(empStatus))//Requested
		{
			//sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus!=3 and A.quantity!='' ";
			sql="select distinct A.indentNo,B.SummaryDate,B.fieldMan,B.foCode,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where A.indentNo = B.indentNo and a.indentStatus='" + empStatus+"' and A.indentStatus not in(3,4) and A.quantity!='' ";
		}
		else
		{
			//sql="select A.indentNo,B.SummaryDate,A.variety,B.fieldMan,B.foCode,A.kind,A.quantity,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where A.indentNo = B.indentNo and A.indentStatus!=3 and A.quantity!='' ";
			sql="select distinct A.indentNo,B.SummaryDate,B.fieldMan,B.foCode,B.ryotCode from KindIndentDetails A ,KindIndentSummary B where A.indentNo = B.indentNo and A.indentStatus not in(3,4) and A.quantity!='' ";
		}
		
		
		
		logger.info("listGetOnloadIndentsstatus() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	
	public List<DispatchDetails> getDataForAcknowledgePrint(String dispatchNo)
    {
        Session  session = (Session)hibernateDao.getSessionFactory().openSession();
		Query q=session.createQuery("From DispatchDetails where dispatchNo='"+dispatchNo+"' ");
		return 	q.list();
    }
	public List getAckPrintData(String DispatchNo)
    {
        String sql = "select * from DispatchSummary where dispatchNo='"+DispatchNo+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getryotDetails(String ryotCode)
    {
        String sql = "select * from AgreementDetails where ryotcode='"+ryotCode+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAuthorisePrintsummary(String Authoriseno,String Season)
    {
        String sql = "select * from AuthorisationFormSummary where authorisationSeqNo='"+Authoriseno+"' and Season='"+Season+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAuthorisePrintdetals(String Authoriseno,String Season)
    {
        String sql = "select * from AuthorisationFormDetails where authorisationSeqNo='"+Authoriseno+"' and Season='"+Season+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List listUpdateStatusForAllIndentDetails1(String indentno,Double issuedQty )
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"' and dispatchedQuantity= '"+issuedQty+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);

	}
	public List listUpdateStatusForAllIndentDetails(String indentno )
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"'  ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);

	}
	@SuppressWarnings("unchecked")
	public List<DispatchSummary> listAllDispatchAuthorisationSummary(String Season,String DispatchNo)
	{
		String sql="select * from DispatchSummary where season = '"+Season+"' AND dispatchNo = '"+DispatchNo+"' ";
		logger.info("listAllDispatchAuthorisationSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, DispatchSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<DispatchDetails> listAllDispatchAuthorisationDetails(String Season,String DispatchNo)
	{
		String sql="select * from DispatchDetails where season = '"+Season+"' AND dispatchNo= '"+DispatchNo+"' ";
		logger.info("listAllDispatchAuthorisationDetails() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, DispatchDetails.class);
	}
	public List getTrayTypeCost(String traytype)
	{
		String sql="select * from KindIndentDetails where indentNo= '"+traytype+"'  ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);

	}
	
	public List getIndentDtList(String strIndentNo)
	{
		String sql="select * from KindIndentSummary where indentNo= '"+strIndentNo+"'  ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getryotDetailsForDispatch(String strAgrNo,int agrtype)
    {
		String sql = null;
		if(agrtype==1)
        sql = "select * from RandDAgreementDetails where agreementnumber='"+strAgrNo+"' ";
		else
		sql = "select * from AgreementDetails where agreementnumber='"+strAgrNo+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public String getAgrnoForDispatchno(String dispatchNo)
    {
        String sql = "select agreementnumber from DispatchSummary where dispatchNo='"+dispatchNo+"' ";
		logger.info("========sql=========="+sql);
		
        String agr=(String)hibernateDao.getID(sql);
        return agr;
    }
	public double getMaxSeedlingsExtentForAgr(String agrno,String season)
    {
		String sql = "select sum(extentsize) from RandDAgreementDetails where agreementnumber='"+agrno+"' and seasonyear='"+season+"'";
		logger.info("========sql=========="+sql);
		
        double extent=(Double)hibernateDao.getID(sql);
        return extent;
    }
	
	public double getMaxSeedlingsExtentForAgrauth(String agrno,String season)
    {
		String sql = "select sum(extentsize) from AgreementDetails where agreementnumber='"+agrno+"' and seasonyear='"+season+"'";
		logger.info("========sql=========="+sql);
		
        double extent=(Double)hibernateDao.getID(sql);
        return extent;
    }
	public double getTotalSeedlingsDispatchForAgr(String agrno,String season)
    {
		String sql = "select sum(issuedQty) from DispatchSummary where agreementnumber='"+agrno+"' and season='"+season+"'";
		logger.info("========sql=========="+sql);
		
        double totalSeedlings=(Double)hibernateDao.getID(sql);
        return totalSeedlings;
    }
	
	public List getBatchData()
	{
		String sql="select * from BatchMaster where balance>0 ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);

	}
	
	public List getStoreAuthorization()
	{
			String sql="select * from StoreAuthorization ";
			logger.info("getStoreAuthorization() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
	}
	 public Integer getTransactioncodedetails(String ryotcode ,String glcode ,String authorisationcode)
  	 {     
		 	String sql="select transactioncode from AccountDetails where accountcode='"+ryotcode+"' and glcode='"+glcode+"' and journalmemo Like '%"+authorisationcode+"%' ";
			logger.info("getTransactioncodedetails() ========sql=========="+sql);
			 Integer transactioncode=(Integer)hibernateDao.getID(sql);
		     return transactioncode;
  	 }
	 public double getPrincipalamtdetails(String ryotcode ,Integer advancecode ,String season)
  	 {     
		 	String sql="select principle from AdvancePrincipleAmounts where advancecode="+advancecode+" and ryotcode='"+ryotcode+"' and season='"+season+"' ";
			logger.info("getPrincipalamtdetails() ========sql=========="+sql);
			 double Principalamtdetails=(Double)hibernateDao.getID(sql);
		     return Principalamtdetails;
  	 }
	 public boolean getadvsummryamtdetails(String ryotcode ,int advancecode ,String season,double ryotAmt)
  	 {     
		 	String sql="select advanceamount from AdvanceSummary where advancecode="+advancecode+" and ryotcode='"+ryotcode+"' and season='"+season+"' ";
			logger.info("getadvsummryamtdetails() ========sql=========="+sql);
			 double advamt=(Double)hibernateDao.getID(sql);
			double finalamt=advamt-ryotAmt;
			hibernateDao.getSessionFactory().openSession().createQuery("Update AdvanceSummary set advanceamount='" + finalamt + "',pendingamount='" + finalamt
				        + "', pendingpayable='" + finalamt + "' , totalamount ='" + finalamt + "'  where ryotcode='" + ryotcode + "' and season='" + season + "' and advancecode=11 ").executeUpdate();
			return true;
			
	 }
	 
	public String getAuthozationStoreCity(String storeCode)
    {
        String sql = "select city from StoreAuthorization where storecode='"+storeCode+"' ";
		logger.info("========sql=========="+sql);
		
        String city=(String)hibernateDao.getID(sql);
        return city;
    }
	
	/*public List GetZonesforIndent(Integer feildassitant)
	{
				logger.info("========GetZonesforIndent()==========");
				String strQuery=null;
				int zonecode = 0;
				String zone="NA";
				try 
				{
					
					List DateList1 = GetFeildOfficer(feildassitant);
					if (DateList1 != null && DateList1.size() > 0)
					{
						Map dMap = new HashMap();
						dMap = (Map) DateList1.get(0);
						Integer foId = (Integer) dMap.get("fieldofficerid");
						List DateList2 = reasearchAndDevelopmentService.GetFeildOfficerZone(foId);
						if(fieldMan != null && DateList2.size()>0)
						{
							Map dMap2 = new HashMap();
							dMap2 = (Map) DateList2.get(0);
							zonecode = (Integer) dMap2.get("zonecode");
							int length = String.valueOf(zonecode).length();
							
						}
					}
					
					
					
					return jArray;
				} 
				catch (Exception e)
				{
		            logger.info(e.getCause(), e);
		            return null;
				}
			

	}*/
	
	@SuppressWarnings("unchecked")
	public List<AuthorisationFormSummary> listAllAuthorisationFormSummary(String Season,String authorisationSeqNo)
	{
		String sql="select * from AuthorisationFormSummary where season = '"+Season+"' AND authorisationSeqNo = '"+authorisationSeqNo+"' AND ackstatus!=1 ";
		logger.info("listAllAuthorisationFormSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, AuthorisationFormSummary.class);
	}
	@SuppressWarnings("unchecked")
	public List<AuthorisationFormDetails> listAllAuthorisationFormDetails(String Season,String authorisationSeqNo)
	{
		String sql="select * from AuthorisationFormDetails where season = '"+Season+"' AND authorisationSeqNo = '"+authorisationSeqNo+"'  AND ackstatus!=1  ";
		logger.info("listAllAuthorisationFormSummary() ========sql=========="+sql);
	        return hibernateDao.findByNativeSql(sql, AuthorisationFormDetails.class);
	}
	public List getaggrementDetailsForConsumer(String ryotcode,String season)
	{
		String sql="select * from AgreementSummary where ryotcode='" +ryotcode+"' and seasonyear='"+season+"'";
		logger.info("getaggrementDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List<AgreementDetails> getExtentForAuthorizationAgr(String agrno,String season)
    {
		//String sql = "select sum(extentsize) from AgreementDetails where agreementnumber='"+agrno+"' and seasonyear='"+season+"' and plantorratoon='1'";
		String sql = "select * from AgreementDetails where agreementnumber='"+agrno+"' and seasonyear='"+season+"'";
		logger.info("========sql=========="+sql);
		return hibernateDao.findByNativeSql(sql, AgreementDetails.class);

     }
	
	public List getAuthorizationStatusDetails(java.util.Date fromdate,java.util.Date todate)
    {
		String sql="select * from AuthorisationFormSummary where authorisationDate between '" + fromdate+"' and '" + todate+"'  ";
		logger.info("getAuthorizationStatusDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
     }
	 
	 	//added by naidu 03-02-2017
	public boolean closingIndent(String season,String ryotcode,String indent)
	{
		int x=hibernateDao.getSessionFactory().openSession().createQuery("UPDATE KindIndentDetails SET indentStatus=4 WHERE season = '"+season+"' AND ryotCode = '"+ryotcode+"' AND indentNo = '"+indent+"'").executeUpdate();
		int y=hibernateDao.getSessionFactory().openSession().createQuery("UPDATE KindIndentSummary SET indentStatus=4 WHERE season = '"+season+"' AND ryotCode = '"+ryotcode+"' AND indentNo = '"+indent+"'").executeUpdate();
	if(x>0 && y>0)
		return true;
	else
		return false;
	}
	//added by naidu 04-02-2017
	public boolean updatePrintFlag(String authSeqNo,String season)
	{
		int x=hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AuthorisationFormSummary SET printflag=1 WHERE season = '"+season+"' AND authorisationSeqNo = '"+authSeqNo+"'").executeUpdate();
	if(x>0)
		return true;
	else
		return false;
	}
	 //added by naidu on 07-03-2017
	public List getRejectedIndents(String season)
	{
		String sql="select * from KindIndentSummary where season= '"+season+"' and  indentStatus=4 and storecode is not null";
		logger.info("listGetAllIndentSummary() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentSummary.class);
	}
	public List getRejectedIndentsDetails(String indentno)
	{
		String sql="select * from KindIndentDetails where indentNo= '"+indentno+"'";
		logger.info("listGetAllIndentSummary() ========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, KindIndentDetails.class);
	}
	public String getRyotAadharNo(String ryotcode)
    {
        String sql = "select aadhaarNumber from Ryot where ryotcode='"+ryotcode+"' ";
		logger.info("========sql=========="+sql);
		
        String aadhaarNumber=(String)hibernateDao.getID(sql);
        return aadhaarNumber;
    }
		public List getSeedSourceDetailsForMigration()
	{	
		
		String sql = "select distinct seedsrcode from SeedSource";
		return hibernateDao.findBySqlCriteria(sql); 
	}
	public List getSeedSourceSummary(String suppliercode)
	{	
		String sql = "select sum(totalcost) as totalcost,sum(noofacre) as noofacre  from SeedSource where seedsrcode='"+suppliercode+"'" ;
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getVillageCircleFromSupplierCode(String supplierCode,String season)
    {
		String sql="select a.villagecode,a.circlecode, v.village,c.circle from AgreementDetails a,village v,circle c where a.seasonyear='"+season+"' and  a.ryotcode='"+supplierCode+"' and  a.villagecode=v.villagecode and a.circlecode=c.circlecode   ";
		logger.info("getVillageCircleFromSupplierCode() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
     }
	

	@SuppressWarnings("unchecked")
	public List<DispatchAckSummary> listOfDispatchAckForDispatchNo(String Season,String DispatchNo)
	{
	 String sql="select * from DispatchAckSummary where season = '"+Season+"' AND dispatchNo = '"+DispatchNo+"'  ";
	 logger.info("listOfDispatchAckForDispatchNo() ========sql=========="+sql);
         return hibernateDao.findByNativeSql(sql, DispatchAckSummary.class);
	}
	
	public List getVillageCircleFromRyot(String supplierCode)
    {
		String sql="select a.villagecode,a.circlecode, v.village,c.circle from ryot a,village v,circle c where    a.ryotcode='"+supplierCode+"' and  a.villagecode=v.villagecode and a.circlecode=c.circlecode   ";
		logger.info("getVillageCircleFromRyot() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
     }
}
