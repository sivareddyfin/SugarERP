package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Region;

@Repository("RegionDao")
@Transactional
public class RegionDao 
{
	private static final Logger logger = Logger.getLogger(RegionDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addRegion(Region region)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(region);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Region> listAllRegions()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Region> regions=session.createCriteria(Region.class).list();
		session.close();
		return regions;
	}
	
}
