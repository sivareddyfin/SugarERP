package com.finsol.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.AdvanceCategory;


@Repository("AdvanceCategoryDao")
@Transactional

public class AdvanceCategoryDao
{
	private static final Logger logger = Logger.getLogger(AdvanceCategoryDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addAdvanceCategory(AdvanceCategory advanceCategory)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advanceCategory);
		session.flush();
		session.close();
	}
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<AdvanceCategory> listAdvanceCategories() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<AdvanceCategory> advanceCategorys=session.createCriteria(AdvanceCategory.class).list();
		session.close();
		return advanceCategorys;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
