package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.LandType;

@Repository("LandTypeDao")
@Transactional
public class LandTypeDao {

	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addLandType(LandType landType) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(landType);
		session.flush();
		session.close();
	}
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<LandType> listLandTypes() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<LandType> landTypes=session.createCriteria(LandType.class).list();
		session.close();
		return landTypes; 
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
}
