package com.finsol.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccountSummaryTemp;
import com.finsol.model.AdvanceDetails;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementDetails;
import com.finsol.model.AgreementSummary;
import com.finsol.model.BatchServicesMaster;
import com.finsol.model.Branch;
import com.finsol.model.Circle;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.CompositePrKeyForPermitDetails;
import com.finsol.model.CropTypes;
import com.finsol.model.DedDetails_temp;
import com.finsol.model.ExtentDetails;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;
import com.finsol.model.GenerateRankingTemp;
import com.finsol.model.LabRecommendations;
import com.finsol.model.LabRecommendations_temp;
import com.finsol.model.LabourContractor;
import com.finsol.model.LoanDetails;
import com.finsol.model.LoanPrincipleAmounts;
import com.finsol.model.LoanSummary;
import com.finsol.model.Mandal;
import com.finsol.model.PermitDetails;
import com.finsol.model.PermitRules;
import com.finsol.model.ProgramSummary;
import com.finsol.model.ProgramVariety;
import com.finsol.model.Ryot;
import com.finsol.model.SampleCards;
import com.finsol.model.SampleCardsTemp;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.SeedlingTrayChargeSummary;
import com.finsol.model.SeedlingTrayChargesDetails;
import com.finsol.model.SeedlingTrayDetails;
import com.finsol.model.Temp_BatchServiceMaster;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.model.WeighBridgeTestData;
import com.finsol.model.Zone;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneReceiptFunctionalService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("AHFunctionaDao")
@Transactional
public class AHFunctionalDao {
	private static final Logger logger = Logger.getLogger(AHFunctionalDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	@Autowired	
	private CaneReceiptFunctionalService caneReceiptFunctionalService;
	@Autowired	
	private AHFuctionalService ahFuctionalService;
	  
	
	public void addExtentDetails(ExtentDetails extentDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(extentDetails);
		session.flush();
		session.close();
	}
	
	public void deleteExtentDetails(String ryotcode) {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ExtentDetails where ryotcode ='" + ryotcode+"'").executeUpdate();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotcode)
    {
        String sql = "select * from ExtentDetails where ryotcode ='" + ryotcode+"'";
        
        return hibernateDao.findByNativeSql(sql, ExtentDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotcode,Integer plotno,String surveyno,String Screen)
    {
        String sql = "";
        if("Generate Agreement".equalsIgnoreCase(Screen))
        {
            sql = "select * from ExtentDetails where ryotcode ='" + ryotcode+"' and plotslnumber=" + plotno+" and surveyno='" + surveyno+"' and IsSuitableForCultivation=0";
        }
        else
        {
            sql = "select * from ExtentDetails where ryotcode ='" + ryotcode+"' and plotslnumber=" + plotno+" and surveyno='" + surveyno+"'";
        }
        return hibernateDao.findByNativeSql(sql, ExtentDetails.class);
    }
	
	
	public void addLabRecommendations_Temp(LabRecommendations_temp labRecommendations_temp)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(labRecommendations_temp);
		session.flush();
		session.close();
	}
	
	public void addLabRecommendations(LabRecommendations labRecommendations)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(labRecommendations);
		session.flush();
		session.close();
		
	}
	
	public List<LabRecommendations> getLabRecommendationsByRyotCode(String ryotcode)
    {
        String sql = "select * from LabRecommendations where ryotcode ='" + ryotcode+"'";
        
        return hibernateDao.findByNativeSql(sql, LabRecommendations.class);
    }
	
	public void updateExtedDetails(Byte cultivation,String ryotcode,Integer plotno)
	{
		Date currentDate = new Date();
		String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
		//Modified by DMurty on 06-05-2016.curdate() was removed due to sql server
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE ExtentDetails set IsSuitableForCultivation="+cultivation+",TestDate='"+modifiedDate+"',soilteststatus=1 WHERE ryotcode='"+ryotcode+"' and plotslnumber="+plotno+"").executeUpdate();
	}
	
	public void deleteLabRecomendations(String ryotCode) {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM LabRecommendations where ryotcode='"+ryotCode+"'").executeUpdate();
	}
	
	public void deleteLoanPrincipleAmounts(String ryotCode,String season,Integer branchId,Integer loanNumber)
	{
		
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM LoanPrincipleAmounts where ryotcode='"+ryotCode+"' and season='"+season+"' and branchcode="+branchId+" and loannumber="+loanNumber+"").executeUpdate();
	}
	
	
	
	
	
	public List<LabRecommendations_temp> getLabRecomendationsTempByRyotCode(String ryotcode,String plotno,String surveyno)
    {
        String sql = "select * from LabRecommendations_temp where ryotcode ='" + ryotcode+"' and plotslno='" + plotno+"' and surveynumber='" + surveyno+"'";
        
        return hibernateDao.findByNativeSql(sql, LabRecommendations_temp.class);
    }
	
	public List<LabRecommendations_temp> getLabRecomendationsFromRecomendations(String ryotcode,String plotno,String surveyno)
    {
        String sql = "select * from LabRecommendations where ryotcode ='" + ryotcode+"' and plotslno='" + plotno+"' and surveynumber='" + surveyno+"'";
        
        return hibernateDao.findByNativeSql(sql, LabRecommendations_temp.class);
    }
	
	public void deleteLabRecomendations_temp(String ryotCode,String plotno,String surveyno) {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM LabRecommendations_temp where plotslno='"+plotno+"' and ryotcode='"+ryotCode+"' and surveynumber='"+surveyno+"'").executeUpdate();
	}
	
	public void addSeedlingTrayChargeSummary(SeedlingTrayChargeSummary seedlingTrayChargeSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(seedlingTrayChargeSummary);
		session.flush();
		session.close();
	}
	
	public void addSeedlingTrayChargeDtls(SeedlingTrayChargesDetails seedlingTrayChargesDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(seedlingTrayChargesDetails);
		session.flush();
		session.close();
	}
	
	public void deleteSeedlingTrayCharges(SeedlingTrayChargeSummary seedlingTrayChargeSummary)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SeedlingTrayChargesDetails WHERE returnid = "+seedlingTrayChargeSummary.getReturnid()).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public Object GetMaxTransCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	
	public void addAccountDtls(AccountDetails accountDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountDetails);
		session.flush();
		session.close();
	}
	
	public void addAccountSmry(AccountSummary accountSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSummary);
		session.flush();
		session.close();
	}
	
	public void addAccountGrpDtls(AccountGroupDetails accountGroupDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountGroupDetails);
		session.flush();
		session.close();
	}
	
	public void addAccountGrpSmry(AccountGroupSummary accountGroupSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountGroupSummary);
		session.flush();
		session.close();
	}
	
	public void addAccountSubGrpDtls(AccountSubGroupDetails accountSubGroupDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSubGroupDetails);
		session.flush();
		session.close();
	}
	
	public void addAccountSubGrpSmry(AccountSubGroupSummary accountSubGroupSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSubGroupSummary);
		session.flush();
		session.close();
	}
	
	public Object GetAccGrpCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	
	public Object GetAccSubGrpCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	
	public SeedlingTrayChargeSummary getSeedlingTrayCharges(String Season,String RyotCode) 
	{
		String sqlString="Select * from SeedlingTrayChargeSummary where season='"+Season+"' and ryotcode='"+RyotCode+"'";
		List<SeedlingTrayChargeSummary> seedlingTrayChargeSummary=hibernateDao.findByNativeSql(sqlString, SeedlingTrayChargeSummary.class);
		return seedlingTrayChargeSummary.get(0);
	}
	
	public List<SeedlingTrayChargesDetails> getSeedlingTrayChargesDetails(Integer returnId)
    {
        String sql = "select * from SeedlingTrayChargesDetails where returnid =" + returnId;
        return hibernateDao.findByNativeSql(sql, SeedlingTrayChargesDetails.class);
    }
	
	public List BalDetails(String AccCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountSummary where accountcode='"+AccCode+"' and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}

	public Object GetTransactionCodefromDtlsCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	
	//
	public List AccDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountcode,cr,dr,glcode,runningbalance,status,user from AccountDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	public void updateRunningBalinSmry(double finalAmt,String strCrDr,String AccCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ="+AccCode).executeUpdate();
	}
	
	public void updateStatusForAccDtls(int TransactionCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountDetails set status=0 WHERE transactioncode ="+TransactionCode).executeUpdate();
	}
	
	public List AccSubGrpDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountcode,cr,dr,glcode,runningbalance,status,user from AccountSubGroupDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	
	public List SubGrpBalDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountgroupcode,cr,dr,runningbalance,status,user,accountsubgroupcode from AccountSubGroupDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	public List SubGrpBalDetails(int AccGrpCode,int AccSubGrpCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountSubGroupSummary where accountgroupcode="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}
	
	
	//
	public void updateRunningBalinAccSubGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,int AccSubGrpCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode ).executeUpdate();
	}
	
	public void updateStatusForAccSubGrpDtls(int TransactionCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSubGroupDetails set status=0 WHERE transactioncode ="+TransactionCode).executeUpdate();
	}
	
	
	public List AccGrpBalDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountgroupcode,cr,dr,runningbalance,status,user from AccountGroupDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	public List AccGrpBalDetails(int AccGrpCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountGroupSummary where accountgroupcode="+AccGrpCode+" and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}
	
	public void updateRunningBalinAccGrpSmry(double finalAmt,String strCrDr,int AccGrpCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode).executeUpdate();
	}
	
	public void updateStatusForAccGrpDtls(int TransactionCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountGroupDetails set status=0 WHERE transactioncode ="+TransactionCode).executeUpdate();
	}
	
	public List getAccCodes(String AccCode)
	{
		String strQuery="Select accountgroupcode,accountsubgroupcode from AccountMaster where accountcode='"+AccCode+"'";
		return hibernateDao.getColumns(strQuery);
	}
	
	public List getRecordsforAccGroupCode(int AccGroupCode)
	{
		String strQuery="Select balancetype,runningbalance from AccountGroupSummary where accountgroupcode="+AccGroupCode+"";
		return hibernateDao.getColumns(strQuery);
	}
	
	public void addAgreementSummary(AgreementSummary agreementSummary,String modifyFlag)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			session.saveOrUpdate(agreementSummary);
		}
		else
		{
			session.save(agreementSummary);
		}
		session.flush();
		session.close();
	}
	
	public void addAgreementDetails(AgreementDetails agreementDetails) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(agreementDetails);
		session.flush();
		session.close();
	}
	
	
	public void addLoanSummary(LoanSummary loanSummary) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(loanSummary);
		session.flush();
		session.close();
		logger.info("========addLoanSummary==========");
	}
	
	public void addLoanDetails(LoanDetails loanDetails) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(loanDetails);
		session.flush();
		session.close();
	}
	
	public void addLoanDetailsNew(LoanDetails loanDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(loanDetails);
		session.flush();
		session.close();
	}
	
	public void addAdvanceDetails(AdvanceDetails advanceDetails) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advanceDetails);
		session.flush();
		session.close();
	}
	
	public void addAdvanceSummary(AdvanceSummary advanceSummary) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advanceSummary);
		session.flush();
		session.close();
	}
	
	public void addSeedlingTrayDetails(SeedlingTrayDetails seedlingTrayDetails) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(seedlingTrayDetails);
		session.flush();
		session.close();
	}
	
	public void updateEstQty(String ryotCode,String agreementNo,String surveyNo,double EstQty,String plotNo)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set estimatedqty= "+EstQty+" WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+agreementNo+"' and surveynumber='"+surveyNo+"' and plotnumber='"+plotNo+"'").executeUpdate();
	}
	
	
	public void deleteAgreementDetails(String agNumber) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM AgreementDetails WHERE agreementnumber = '"+agNumber+"'").executeUpdate();
	}
	
	public void updateProgramNumberInAgreementDetails(String ryotCode,String agreementNo,Integer plotNo,String program)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set programnumber= '"+program+"' WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+agreementNo+"' and plotnumber='"+plotNo+"'").executeUpdate();
	}
	//Modified by DMurty on 05-10-2016
	public void addProgramSummary(ProgramSummary programSummary,List ProgramList) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		if(ProgramList != null && ProgramList.size()>0)
		{
			session.saveOrUpdate(programSummary);
		}
		else
		{
			session.save(programSummary);
		}
		session.flush();
		session.close();
	}
	
	
	public void addSeedSuppliersSummary(SeedSuppliersSummary seedSuppliersSummary) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(seedSuppliersSummary);
		session.flush();
		session.close();
	}
	
	public List<CompanyAdvance> getAdvanceDateilsByAdvanceCode(int advanceCode)
    {
        String sql = "select * from CompanyAdvance where advancecode =" + advanceCode;
        
        return hibernateDao.findByNativeSql(sql, CompanyAdvance.class);
    }
	
	public List getRyotAdvances(String seasonCode)
    {
        String sql = "select distinct advancecode from AdvanceSummary where season = '" + seasonCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }

	public List getRyotAdvanceNames(int advanceCode)
    {
        String sql = "select advance from CompanyAdvance where advancecode = "+advanceCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getRyotNamesByAdvances(String seasonCode,int advancecode)
    {
        String sql = "select distinct ryotcode from AdvanceSummary where season = '" + seasonCode+"' and advancecode="+advancecode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public List getRyotName(String ryotCode)
    {
		ryotCode=ryotCode.replaceAll("\"", "");
        String sql = "select ryotname from Ryot where ryotcode = '"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
	}
	
	public List getLVName(String LVCode)
    {
		LVCode=LVCode.replaceAll("\"", "");
        String sql = "select village from Village where villagecode = '"+LVCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getRyotNameNew(String ryotCode)
    {
		ryotCode=ryotCode.replaceAll("\"", "");
        String sql = "select ryotname from Ryot where ryotcode = '"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public void deleteSeedlingTrayDetails(String season,String ryotCode)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SeedlingTrayDetails WHERE season = '"+season+"' and ryotcode='"+ryotCode+"'").executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public void deleteSeedSupplierDetails(String season,String ryotCode)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SeedSuppliersSummary WHERE season = '"+season+"' and seedsuppliercode='"+ryotCode+"'").executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public List getAggrementDetailsByCode(String agreementNo,String season)
    {
        String sql = "select * from AgreementDetails where agreementnumber = '"+agreementNo+"' and season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	

	public List getVillageDetailsByCode(String villageCode)
    {
        String sql = "select village from Village where villagecode = '"+villageCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public List getVarietye(Integer varietyCode)
    {
        String sql = "select variety from Variety where varietyCode = "+varietyCode+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	
	public List getCirclePerGivenCircle(Integer circleCode)
    {
        String sql = "select circle from Circle where circlecode = "+circleCode+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	
	public List getVillageDetailsByCodeNew(String villageCode)
    {
        String sql = "select * from Village where villagecode = '"+villageCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getCropTypeByCode(int cropCode)
    {
        String sql = "select croptype from CropTypes where croptypecode = "+cropCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getVarietyDetailsByCode(int varietyCode)
    {
        String sql = "select variety from Variety where varietycode = "+varietyCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getVarietyDetailsByCodeNew(int varietyCode)
    {
        String sql = "select variety from Variety where varietycode = "+varietyCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getProgramNumbersBySeason(String season)
    {
        String sql = "select distinct programnumber from ProgramSummary where season = '" + season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	
	public void addAdvancePrincipleAmounts(AdvancePrincipleAmounts advancePrincipleAmounts) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advancePrincipleAmounts);
		session.flush();
	}
	

	public void addSampleDetails(SampleCards sampleCards)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(sampleCards);
		session.flush();
		session.close();
	}
	
	public List getSampleCardNo(double extentSize)
    {
        String sql = "select noofsamplecards from SampleCardRules where "+ extentSize +" BETWEEN sizefrom AND sizeto";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	
	public void addWeighBridgeTestData(WeighBridgeTestData weighBridgeTestData)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(weighBridgeTestData);
		session.flush();
		session.close();
	}
	
	public void addloanPrincipleAmounts(LoanPrincipleAmounts loanPrincipleAmounts) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(loanPrincipleAmounts);
		session.flush();
		session.close();
	}
	
	public void updateLoanDetails(String ryotCode,String season,Integer branchcode,Double disbursedamount,String disburseddate,String refNumber)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		tx.begin();
		session.createQuery("UPDATE LoanDetails set disbursedamount= "+disbursedamount+", disburseddate= '"+disburseddate+"',referencenumber='"+refNumber+"'  WHERE ryotcode ='"+ryotCode+"' and Season='"+season+"' and branchcode="+branchcode+"").executeUpdate();
		tx.commit();
		//session.flush();
		session.close();
	}
	public void updateLoanSummary(String ryotCode,String season,Double disbursedamount)
	{
		Session session=null;
		try {
		session=(Session)hibernateDao.getSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		tx.begin();
		session.createQuery("UPDATE LoanSummary set disbursedamount= "+disbursedamount+" WHERE ryotcode ='"+ryotCode+"'  and season='"+season+"'").executeUpdate();
		tx.commit();
		//session.flush();
		session.close();
		} catch (Exception jde) {
	        logger.fatal("Error occured in database communication", jde);
	        throw new RuntimeException(jde);
	    } finally {
	        if (session.isOpen()) {
	            session.close();
	        }
	    }
	}
	
	public void addsampleCards(SampleCards sampleCards) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(sampleCards);
		session.flush();
	}
	
	public void updateSampleCardInAgreementDetails(String ryotCode,String AgreementNo,String sampleCardNos,String plotno)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set samplecards='"+sampleCardNos+"' WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+AgreementNo+"' and plotnumber='"+plotno+"'" ).executeUpdate();
	}
	
	public void deleteSampleCards(String season,String nprogram)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SampleCards WHERE season = '"+season+"' and programnumber="+nprogram).executeUpdate();
		hibernateDao.flush();
		logger.info("========After Delete==========");
	}
	
	public List<AgreementDetails> getAgreementDetailsByCode(String agreementNo,String season)
    {
        String sql = "select * from AgreementDetails where agreementnumber ='"+ agreementNo+"' and seasonyear='"+season+"'";
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }

	public void updateValuesforSampleCard(Map TempMap)
	{
		
		String brix = (String) TempMap.get("BRIX");
		String pol = (String) TempMap.get("POL");
		String purity = (String) TempMap.get("PURITY");
		double ccsRating = (Double) TempMap.get("CCS_RATING");
		String season = (String) TempMap.get("SEASON");
		int SampleCard = (Integer)  TempMap.get("SAMPLE_CARD");
		int program = (Integer)  TempMap.get("PROGRAM");

		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE SampleCards set brix= '"+brix+"',pol='"+pol+"',purity='"+purity+"',ccsrating="+ccsRating+" WHERE season ='"+season+"' and samplecard="+SampleCard+" and programnumber="+program+"").executeUpdate();
	}
	
	public List getCountofSampleCards(String season,int programNo)
    {
        String sql = "select count(*) from SampleCards where season='"+season+"' and programnumber="+programNo+"";
        return hibernateDao.findBySql(sql);
    }
	
	public List getPlotNosforSampleCards()
    {
        String sql = "select distinct plotno from SampleCards";
        return hibernateDao.findBySql(sql);
    }
	
	public List getCcsratingsByPlotno(String plotNo,int programNo,String agreementNo)
    {
        String sql = "SELECT AVG(ccsrating) FROM SampleCards where plotno='"+plotNo+"' and programnumber="+programNo+" and aggrementno='"+agreementNo+"'";
        return hibernateDao.findBySql(sql);
    }
	
	
	public void addSampleCardTemp(SampleCardsTemp sampleCardsTemp)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(sampleCardsTemp);
		session.flush();
		session.close();
	}
	
	public List<AgreementSummary> getAgreementDetails(String agreementNo)
    {
        String sql = "select * from AgreementSummary where agreementnumber ='"+agreementNo+"'";
        return hibernateDao.findByNativeSql(sql, AgreementSummary.class);
    }
	
	public List getFamilyAvgCcsratingsByPlotno(String plotNo,int programNo,int familyGrpCode)
    {
        String sql = "SELECT AVG(avgccsrating) FROM SampleCardsTemp where plotno='"+plotNo+"' and programnumber="+programNo+" and familygroupcode="+familyGrpCode;
        return hibernateDao.findBySql(sql);
    }
	
	public void TruncateTemp(String model)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createSQLQuery("truncate table "+model).executeUpdate();
		logger.info("========After Delete==========");
	}

	public void updateValuesforAvgCcsRating(Map TempMap)
	{
		String ryotCode = (String) TempMap.get("RYOT_CODE");
		String agreementNo = (String) TempMap.get("AGREEMENT_NO");
		String plotNo = (String) TempMap.get("PLOT_NO");
		double fAvgCcsRating = (Double) TempMap.get("F_CCS_RATING");
		double avgCcsRating = (Double) TempMap.get("AVG_CCS_RATING");
		double ccsRating = (Double) TempMap.get("CCS_RATING");
		int program = (Integer)  TempMap.get("PROGRAM");
		String season = (String) TempMap.get("SEASON");

		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE SampleCards set avgccsrating = "+avgCcsRating+",fgavgccsrating="+fAvgCcsRating+" WHERE season ='"+season+"' and plotno='"+plotNo+"' and aggrementno='"+agreementNo+"'").executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set averageccsrating = "+avgCcsRating+",familygroupavgccsrating="+fAvgCcsRating+" WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"'").executeUpdate();
	}
	public List getAdvanceAmtforValidation(int advancecode)
    {
        String sql = "SELECT maximumamount FROM CompanyAdvance where advancecode="+advancecode;
        return hibernateDao.findBySql(sql);
    }
	
	public List getZone(int zonecCode)
    {
        String sql = "select zone from Zone where zonecode = "+zonecCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public List getZoneDetails(int zonecCode)
    {
        String sql = "select * from Zone where zonecode = "+zonecCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getVillageDetails(String villageCode)
    {
        String sql = "select * from Village where villagecode = '"+villageCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getCircle(int circleCode)
    {
        String sql = "select circle from Circle where circlecode = "+circleCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getCircleById(int circleCode)
    {
        String sql = "select circle from Circle where id = "+circleCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void addRankingTemp(GenerateRankingTemp generateRankingTemp)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(generateRankingTemp);
		session.flush();
		session.close();
	}
	
	public void UpdateRankinginAgreementDetails(Map TempMap,String pageName)
	{
		String ryotCode = (String) TempMap.get("RYOT_CODE");
		String agreementNo = (String) TempMap.get("AGREEMENT_NO");
		String plotNo = (String) TempMap.get("PLOT_NO");
		double ccsRating = (Double) TempMap.get("CCS_RATING");
		double extentSize = (Double)  TempMap.get("EXTENT_SIZE");
		String season = (String) TempMap.get("SEASON");
		int rank = (Integer) TempMap.get("RANK");
		int program = (Integer) TempMap.get("PROGRAM");
		String permitNo = (String) TempMap.get("PERMIT_NO");

		if("Permit".equalsIgnoreCase(pageName))
        {
			//For Permit
			hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set permitnumbers = '"+permitNo+"' WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"' and programnumber="+program+" and rank="+rank).executeUpdate();
        }
		else
		{
			//For Ranking
			hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set rank = "+rank+" WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"' and familygroupavgccsrating="+ccsRating+" and programnumber="+program).executeUpdate();
		}
	}
	
	public List<PermitRules> getPermitRules()
    {
        String sql = "select * from PermitRules";
		logger.info("========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, PermitRules.class);
    }
	
	public void addPermits(PermitDetails permitDetails) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(permitDetails);
		session.flush();
	}
	
	public void deletePermits(String season,String nprogram)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM PermitDetails WHERE season = '"+season+"' and programno="+nprogram).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	//Agreement Details
	public List<AgreementDetails> getPreviousAgreementsForRatoon(String season,String ryotCode)
    {
        String sql = "select * from AgreementDetails where ryotcode ='"+ryotCode+"' and seasonyear='"+season+"' and plantorratoon=1";
        
        return hibernateDao.findByNativeSql(sql, AgreementDetails.class);
    }
	
	public List<ProgramSummary> getValidatreProgramNumForSeason(String season,int programNo)
    {
        String sql = "select * from ProgramSummary where programnumber="+programNo+" and season='"+season+"'";
        
        return hibernateDao.findByNativeSql(sql, ProgramSummary.class);
    }
	
	public List getIntrestRatesByAmount(double amount)
    {
        String sql = "select interestrate from AdvanceInterestRates where "+ amount +" BETWEEN fromamount AND toamount";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void deleteIntrestAmounts(String season,String ryotCode,int advCode)
	{
		logger.info("========deleteIntrestAmounts()==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM AdvancePrincipleAmounts WHERE season = '"+season+"' and ryotcode='"+ryotCode+"' and advancecode="+advCode).executeUpdate();
	}
	
	public List getValidateRyotForAgreement(String season,String ryotCode)
    {
        String sql = "select IsSuitableForCultivation from ExtentDetails where ryotcode='"+ ryotCode +"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void deleteRyotBankDetails(String ryotCode,String accNo,int branchCode)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM RyotBankDetails WHERE ryotcode='"+ryotCode+"' and accountnumber='"+accNo+"' and ryotbankbranchcode="+branchCode).executeUpdate();
	}
	
	public List getIfscCodeByBrancgCode(int branchcode)
    {
        String sql = "select ifsccode from Branch where branchcode ="+ branchcode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getMaxIdForBankDetails(String ryotCode)
    {
        String sql = "select max(id) from RyotBankDetails where ryotcode ='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getAgreementNumbersForSeason(String season,String tablename)
    {
		logger.info("========getAgreementNumbersForSeason()==========");
		season=season.replaceAll("\"", "");
        String sql = "select agreementnumber,agreementseqno from "+tablename+" where seasonyear ='"+season+"' order by agreementseqno";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql); 
    }
	
	public List getVarityForSeason(String season)
    {
		season=season.replaceAll("\"", "");
        //String sql = "select varietycode from AgreementDetails where seasonyear ='"+season+"'";
		String sql = "select distinct a.varietycode,b.variety from AgreementDetails a,Variety b where seasonyear ='"+season+"' and  a.varietycode=b.varietycode";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	/*public List<ProgramSummary> getMaxProgramNumForSeason(String season)
    {
        String sql = "select max(programnumber) from ProgramSummary where season='"+season+"'";
        
        return hibernateDao.findByNativeSql(sql, ProgramSummary.class);
    }*/
	
	public Object getMaxProgramNumForSeason(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	public List<AgreementSummary> getAgreementSummaryByCode(String agreementNo,String season)
    {
        String sql = "select * from AgreementSummary where agreementnumber ='"+ agreementNo+"' and seasonyear='"+season+"'";
        return hibernateDao.findByNativeSql(sql, AgreementSummary.class);
    }
	
	
	public List getAgreementDataToMigrate(String table)
    {
		logger.info("========getAgreementDataToMigrate()==========");
        String sql = "select * from "+table;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	
	public List getAccountCodesToMigrate(String season)
    {
		logger.info("========getAccountCodesToMigrate()==========");
        String sql = "select distinct accountcode from AccountDetails where season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAccountGroupCodesToMigrate(String season)
    {
		logger.info("========getAccountGroupCodesToMigrate()==========");
        String sql = "select distinct accountgroupcode from AccountGroupDetails where season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAccSubGroupCodesToMigrate(String season)
    {
		logger.info("========getAccSubGroupCodesToMigrate()==========");
        String sql = "select distinct accountsubgroupcode from AccountSubGroupDetails where season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	
	public List getZonecodeFromAgreementDetails()
    {
		logger.info("========getAgreementDataToMigrate()==========");
        String sql = "select * from  AgreementDetails";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List updateRyotNames(String table)
    {
		logger.info("========updateRyotNames()==========");
        String sql = "select accountcode from "+table+" where accountname='Sundry Creditors'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List<Village> getVillageDtls(int villagecode)
    {
        String sql = "select * from Village where villagecode =" + villagecode;
        return hibernateDao.findByNativeSql(sql, Village.class);
    }
	
	public List getVarietyDetailsByName(String varietyName)
    {
        String sql = "select varietycode from Variety where variety = '"+varietyName+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getZoneCodeByMandal(int mandalcode)
    {
        String sql = "select zonecode from Mandal where mandalcode = "+mandalcode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List<FieldAssistant> getFieldAssistantDetails(String fAName)
    {
        String sql = "select * from FieldAssistant where fieldassistant ='"+ fAName+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, FieldAssistant.class);
    }
	
	public List<FieldOfficer> getFieldOfficerDetails(int fOId)
    {
        String sql = "select * from FieldOfficer where fieldOfficerid =" + fOId;
		logger.info("========sql=========="+sql);

        return hibernateDao.findByNativeSql(sql, FieldOfficer.class);
    }
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	public List getBankVerification(String bankname) 
	{
		logger.info("========getAgreementDataToMigrate()==========");
        String sql = "select bankname from Bank where bankname ='"+bankname+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getBankCode(String bankname) 
	{
		logger.info("========getAgreementDataToMigrate()==========");
        String sql = "select bankcode from Bank where bankname ='"+bankname+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getVarietyDetailsByNameNew(String varietyName)
    {
        String sql = "select varietycode from Variety where variety = '"+varietyName+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getCircleDetailsNew(int circleCode)
    {
        String sql = "select * from Circle where circlecode = '"+circleCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getCircleDetailsByVillageCode(String villageCode)
    {
        String sql = "select * from Circle where villagecode = '"+villageCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getMandalDetailsNew(int nmandalCode)
    {
        String sql = "select * from Mandal where mandalcode = '"+nmandalCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public Object getTrayChargeByDistance(double distance,Integer trayCode)
    {
        String sql = "select traycharge from SeedlingTrayChargeByDistance where  traycode="+ trayCode +" and "+ distance +" BETWEEN distancefrom AND distanceto";
		logger.info("========sql=========="+sql);
        return hibernateDao.getID(sql);
    }
	public List getZoneCodeByMandalNew(int mandalcode)
    {
        String sql = "select zonecode from Mandal where mandalcode = "+mandalcode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getIfscCodeByBrancgCodeNew(int branchcode)
    {
        String sql = "select ifsccode from Branch where branchcode ="+ branchcode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public List getMaxIdForBankDetailsNew(String ryotCode)
    {
        String sql = "select max(id) from RyotBankDetails where ryotcode ='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List getAgreementsByRyot(String ryotCode,String season)
    {
        String sql = "select * from AgreementSummary where ryotcode = '"+ryotCode+"' and seasonyear='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getWeighmentRyots(String season)
    {
        String sql = "select distinct ryotcode from WeighmentDetails where season='"+season+"' order by ryotcode";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getMaxLoanNoList(int bankCode,String season)
    {        
       String sql="select * from LoanDetails where branchcode="+bankCode+" and Season='"+season+"'";
       return hibernateDao.findBySqlCriteria(sql);

    }
	public Object getCircleCodeFromAgreementDetailsNew(String sql)
	{
		return hibernateDao.getID(sql);
	}
	public List getCircleCodeByAgreementNo(String AgNoforCricle)
    {
        String sql = "select circlecode  from AgreementDetails where agreementnumber='"+AgNoforCricle+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public List<Circle> getCircleDtls(int circleCode)
    {
        String sql = "select * from Circle where circlecode =" + circleCode;
        return hibernateDao.findByNativeSql(sql, Circle.class);
    }
	
	public List<Mandal> getMandalDtls(int mandalCode)
    {
        String sql = "select * from Mandal where mandalcode =" + mandalCode;
        return hibernateDao.findByNativeSql(sql, Mandal.class);
    }
	
	public List<Branch> getBranchDtls(int branchCode)
    {
        String sql = "select * from Branch where branchcode =" + branchCode;
        return hibernateDao.findByNativeSql(sql, Branch.class);
    }
	
	public List<Branch> getVillageDetailsByCode(int branchCode)
    {
        String sql = "select * from Branch where branchcode =" + branchCode;
        return hibernateDao.findByNativeSql(sql, Branch.class);
    }

	public boolean saveAllagreementTables(List caneWeighmentTables) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean isInsertSuccess = false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<caneWeighmentTables.size();i++)
	        {
	        	session.saveOrUpdate(caneWeighmentTables.get(i));
	        }
	        session.flush();
	        transaction.commit();
	        isInsertSuccess = true;
		    return isInsertSuccess;
	    } 
	    catch (JDBCException jde) 
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        isInsertSuccess = false;
		    return isInsertSuccess;
	    }
	    finally 
	    {
	        if (session.isOpen())
	        {
	            session.close();
	        }
	    }
	}

	public Object getSeedlingAdvance(Integer advanceCode)
    {
        String sql = "select isseedlingadvance from CompanyAdvance where  advancecode="+ advanceCode +"";
		logger.info("========sql=========="+sql);
        return hibernateDao.getID(sql);
    }
	
	
	public List getLoanNumbersForBranch(String seasonCode,int branchCode)
    {
        String sql = "select loannumber from LoanSummary where season = '" + seasonCode+"' and branchcode="+branchCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAdvanceSummaryData(String seasonCode,String ryotCode,int advanceCode)
    {
        String sql = "select * from AdvanceSummary where season = '" + seasonCode+"' and ryotcode="+ryotCode+" and advancecode="+advanceCode;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getFamilyMembers(int familyGroupId)
    {
        String sql = "select familygroupmembers from FamilyGroup where familygroupcode=" + familyGroupId;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public void UpdateFamilyMembers(String familyMembers,int familyGroupCode)
	{
		logger.info("========UpdateFamilyMembers()==========");
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE FamilyGroup set familygroupmembers='"+familyMembers+"' WHERE familygroupcode="+familyGroupCode).executeUpdate();
	}
	
	public List getDatesByCalcruledate(String season)
    {
		season=season.replaceAll("\"", "");
        String sql = "select calcruledate from CaneAcctRules where season = '"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getPermitDate(Integer permitnum)
	{
		String sql = "select permitdate from PermitDetails where permitnumber = "+permitnum+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);	
		
	}
	public List getProgramNoByProgramSummary(String season)
    {
        String sql = "select distinct programnumber from ProgramSummary where season = '"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAccountNames(String accName)
    {
		//accName = "\"" + accName + '%' + "\"";

        String sql = "select distinct accountname,accountcode from AccountMaster where UPPER(accountname) like '"+accName.toUpperCase()+"%' order by accountname";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getSampleCardDetails(int sampleCardNo,String season,String table)
    {
        String sql = "select * from  "+table+" where samplecard ="+sampleCardNo+" and season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAllCaneReceiptData(String season)
	{
		List<String> accounts = null;
		String query = null;
		try
		{
			if (season == "2013-2014")
				query = "select * from sssAccounts_CR_1314 order by id";
			else if (season == "2014-2015")
				query = "select * from sssAccounts_CR_1415 order by id";
			else
				query="select * from sssAccounts_CR_1516 order by id";

			logger.info("========query=========="+query);
			accounts =  hibernateDao.findBySqlCriteria(query);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;		

		
	}
	
	public List getTransportSubsidyData(String season)
	{
		List<String> accounts = null;
		String sql = null;
		try
		{
			if (season == "2013-2014")
				sql="select * from sssTSData_1314 order by id";
			else if (season == "2014-2015")
				sql="select * from sssTSData_1415 order by id";
			else
				sql="select * from sssTSData_1516 order by id";
			
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}
	
	public List getAdvances(String season)
	{
		List<String> accounts = null;
		String sql = null;
		try
		{
			if(season == "2013-2014")
				sql="select * from sssAdv_1314 order by id";
			else if(season == "2014-2015")
				sql="select * from sssAdv_1415 order by id";
			else if(season == "2015-2016")
				sql="select * from sssAdv_1516 order by id";
			
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}

	public List getSBandLoanPayments(String season)
	{
		List<String> accounts = null;
		String sql = null;
		try
		{
			if (season == "2013-2014")
				sql = "select * from sssSBandLoans_1314 order by id";
			else if (season == "2014-2015")
				sql = "select * from sssSBandLoans_1415 order by id";
			else if (season == "2015-2016")
				sql = "select * from sssSBandLoans_1516 order by id";			
			
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}
	
	public List getACPDetails()
	{
		List<String> accounts = null;
		try
		{
			String sql="select * from sssACPPt_1516 order by id";
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}	
	public List getCPDetails()
	{
		List<String> accounts = null;
		try
		{
			String sql="select * from sssCP_1516 order by id";
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}	
	
	public List getBankCharges(String season)
	{
		List<String> accounts = null;
		try
		{
			String sql="select * from sssBC_1314 order by id";
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}		

	public List getOBData(String season)
	{
		List<String> accounts = null;
		try
		{
			String sql="select * from sssOB_1314 order by id";
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}
	
	public List getFinalPayDetails(String season)
	{
		List<String> accounts = null;
		try
		{
			String sql="select * from sssOB_PAY1314 order by id";
			logger.info("========sql=========="+sql);
			accounts =  hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return accounts;
		
	}	
	
		
	public String getAdvName(Integer advcode)
	{
		String advance=null;
		Session s=hibernateDao.getSessionFactory().openSession();
		List<String> adv=null;
		try
		{
			SQLQuery query =s.createSQLQuery("select advance from CompanyAdvance where advancecode="+advcode);
			adv=query.list();
			if(adv.size()>0 && adv != null)
			{
				advance=(String)adv.get(0);
			}
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return advance;
	}
	
	public List getRyotNameFromTemp(String ryotcode)
    {
		List rtname=null;
        String sql = "select distinct ryotname from migtransporting_temp where ryotcode = '"+ryotcode+"'";
		logger.info("========sql=========="+sql);
        try
        {
        	rtname=hibernateDao.findBySqlCriteria(sql);;
        }
        catch(Exception e)
        {
        	logger.info("========The Exception is=========="+e);
        }
        return rtname;
    }
	
	public boolean validateAccountMaster(String ryotcode)
	{
		
		boolean valid=false;
		List<String> rtname=null;
        String sql = "select accountname from AccountMaster where accountcode = '"+ryotcode+"'";
		logger.info("========sql=========="+sql);
        try
        {
        	rtname=hibernateDao.findBySql(sql);
        	if(rtname.size()>0 && rtname != null)
        	valid=true;
        }
        catch(Exception e)
        {
        	logger.info("========The Exception is=========="+e);
        }
    	return valid;
	}
	
	public List generatePermitPrint(Integer pNum)
	{	
		List dataList= new ArrayList();
		Map rlMap=new HashMap();
		try
        {
			Session session=hibernateDao.getSessionFactory().openSession();
			Date d1 = new Date();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
			String formattedDate = df.format(d1);
			String[] curTime=formattedDate.split(" ");
			
	  		Query qry=session.createQuery("from PermitDetails p where p.compositePrKeyForPermitDetails.permitnumber="+pNum+"");
	  		dataList=qry.list();
	  		PermitDetails permitDetails=(PermitDetails)dataList.get(0);
			Integer zoneCode=permitDetails.getZonecode();
			Integer circleCode=permitDetails.getCirclecode();
			Integer varietyCode=permitDetails.getVarietycode();
			String ryotcode=permitDetails.getRyotcode();
			String landvilCode=permitDetails.getLandvilcode();
			Date hdate=permitDetails.getHarvestdt();
			//Added by sahadeva 14/11/2016
			Date vdate=permitDetails.getValiditydate();
			DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
			String strvDate = df1.format(vdate);
			
			String harvestDate=DateUtils.formatDate(hdate,Constants.GenericDateFormat.DATE_FORMAT);
			
			//Zone zone =(Zone)hibernateDao.getSessionFactory().openSession().get(Zone.class, zoneCode);
			List<Zone> zone=ahFuctionalService.getZoneByZonecode(zoneCode);
			//Circle circle=(Circle)hibernateDao.getSessionFactory().openSession().get(Circle.class, circleCode);
			List<Circle> circle= ahFuctionalService.getCircleNameBycircleCode(circleCode);
			 String circleName = null;
			 if(circle != null && circle.size()>0)
			 {
				 circleName = circle.get(0).getCircle();
			 }
			
			//Variety variety=(Variety)hibernateDao.getSessionFactory().openSession().get(Variety.class, varietyCode);
			List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(varietyCode);
			
			String sql3="select plantorratoon from AgreementDetails where ryotcode="+ryotcode+"";
	   		List ls3=hibernateDao.findBySqlCriteria(sql3); 
	   		
	   		Map rlMap1=(Map)ls3.get(0);	   		
	   		String plantorratoon=(String)rlMap1.get("plantorratoon");
			
	   		CropTypes cropTypes=(CropTypes)hibernateDao.getSessionFactory().openSession().get(CropTypes.class, Integer.parseInt(plantorratoon));
	   		Ryot ryot=(Ryot)hibernateDao.getRowByColumn(Ryot.class,"ryotcode",ryotcode);
	   		
	   		Village village=(Village)hibernateDao.getRowByColumn(Village.class,"villagecode",landvilCode);
	   		
	   		//Modified by Sahu on 24-10-2016 as per the discussion with makella. 
	   		rlMap.put("harvestdt",harvestDate);
	   		rlMap.put("zone", zone.get(0).getZone());
	   		rlMap.put("circle", circleName);
	   		rlMap.put("variety", variety.get(0).getVariety());
	   		rlMap.put("plantorratoon", cropTypes.getCroptype());
	   		rlMap.put("currenttime", curTime[1]+" "+curTime[2]);
	   		rlMap.put("currentdate", curTime[0]);
	   		rlMap.put("ryotname", ryot.getRyotname());
	   		rlMap.put("fathername", ryot.getRelativename());
	   		rlMap.put("villagename", village.getVillage());
	   		rlMap.put("permitnumber", permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
	   		rlMap.put("ryotcode", permitDetails.getRyotcode());
	   		rlMap.put("programno", permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
	   		rlMap.put("qrcodedata", permitDetails.getQrcodedata());
	   		rlMap.put("circlecode", permitDetails.getCirclecode());
	   		rlMap.put("landvilcode", permitDetails.getLandvilcode());
	   		
	   		//Added by DMurty on 03-11-2016
	   		rlMap.put("village",village);
	   		rlMap.put("rank",permitDetails.getRank());
			Date havestDate = permitDetails.getHarvestdt();
			DateFormat dff = new SimpleDateFormat("dd-MM-yyyy");
			String strhavestDate = dff.format(havestDate);
			rlMap.put("harvestDate",strhavestDate);
			//Added by sahadeva 14/11/2016
			rlMap.put("validityDate",strvDate);
			
	   		dataList.clear();
	   		dataList.add(rlMap);
        }
		catch(Exception e)
        {
        	logger.info("========The Exception is=========="+e);
        }
		return dataList;

	}
	
	public List getVillageNames()
	{
		String sql="select distinct village from Village order by village";
		logger.info("getVillageNames() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	//--------------------------------------------Advance migration code-------------------------
	public List getAllSeedAndSeedAdvanceMgData()
	{
		List AdvList = null;
		try
		{
			String sql="select * from TempMigAdvance20162017  order by id";
			logger.info("========sql=========="+sql);			
			AdvList = hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
        	logger.info("========The Exception is=========="+e);
		}

		return AdvList;
	}

public List getAgreementsAndExtentSizes(String ryotcode)
	{
		List agreementsList=null;
        String sql = "select agreementnumber,extentsize from AgreementDetails where ryotcode = '"+ryotcode+"'";
		logger.info("========sql=========="+sql);
        try
        {
        	agreementsList=hibernateDao.findBySqlCriteria(sql);;
        }
        catch(Exception e)
        {
        	logger.info("========The Exception is=========="+e);
        }
        return agreementsList;
	}
	public List getSeedSupplierDetails(String season,String ryotcode)
	{
		String sql = "select * from SeedSuppliersSummary where seedsuppliercode='"+ryotcode+"' and season='"+season+"'";
        return hibernateDao.findByNativeSql(sql, SeedSuppliersSummary.class);
	}
	public List getAllTransportMgData()
	{
		String sql="select * from tempallcane order by id";
		//String sql="select * from temptransportsubsidy order by id";
		//String sql="select * from temppaymentbank order by id";
		//String sql="select * from tempcaneamount order by id";
		//String sql="select * from temploanacc order by id";
		//String sql="select * from tempadd order by id";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
		
	}
	public void addProgramVariety(ProgramVariety programVariety)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(programVariety);
		session.flush();
		session.close();
	}
	//Added by DMurty on 05-10-2016
	public List getProgramDetailsByNo(int programNumber,int maxId)
    {
        String sql = "select * from ProgramSummary where programnumber=" + programNumber + " and id="+maxId;
		logger.info("getProgramDetailsByNo() ========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	//Added by Dmurty on 21-10-2016
	public List getRyotAdvancePrincipleData(String ryotCode,int advCode,String season)
    {
        String sql = "select * from AdvancePrincipleAmounts where ryotcode = '"+ryotCode+"' and advancecode="+advCode+" and season='"+season+"'";
		logger.info("getRyotAdvancePrincipleData()========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	//--------------------------------------------Get Current Advances -------------------------
	public List getAllAdvanceData()
	{
		List AdvList = null;
		try
		{
			String sql="select * from AdvanceDetails order by id";
			logger.info("========sql=========="+sql);			
			AdvList = hibernateDao.findBySqlCriteria(sql);
		}
		catch(Exception e)
		{
        	logger.info("========The Exception is=========="+e);
		}

		return AdvList;
	}	
	//Added by DMurty on 31-10-2016
	public List<Branch> getBranchDetailsByBranchCode(int branchCode)
    {
		logger.info("========getBranchDetailsByBranchCode()========== branchCode "+branchCode);			
        String sql = "select * from Branch where branchcode =" + branchCode;
        return hibernateDao.findByNativeSql(sql, Branch.class);
    }
	//Added by DMurty on 02-11-2016
	public boolean validateHarvestDateForPermit(int permitNo)
    {
		logger.info("========validateHarvestDateForPermit()========== permitNo "+permitNo);			
		boolean hasHarvestingDate = false;
		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		compositePrKeyForPermitDetails.setPermitnumber(permitNo);
		int programNo =  caneReceiptFunctionalService.getProgramNo(permitNo);
		compositePrKeyForPermitDetails.setProgramno(programNo);
		
		PermitDetails pd=new PermitDetails();
		PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
		if (permitDetails != null)
		{
			Date harvestDate = permitDetails.getHarvestdt();
			if(harvestDate != null)
			{
				hasHarvestingDate = true;
			}
		}
		logger.info("========validateHarvestDateForPermit()========== hasHarvestingDate "+hasHarvestingDate);			
        return hasHarvestingDate;
    }
	//Added by DMurty on 07-11-2016
	public Integer validateHarvestDateForPermitForWeighment(int permitNo)
    {
		logger.info("========validateHarvestDateForPermitForWeighment()========== permitNo "+permitNo);			
		//boolean hasHarvestingDate = true;
		int nRet = 1;
		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		compositePrKeyForPermitDetails.setPermitnumber(permitNo);
		int programNo =  caneReceiptFunctionalService.getProgramNo(permitNo);
		compositePrKeyForPermitDetails.setProgramno(programNo);
		
		PermitDetails pd=new PermitDetails();
		PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
		if (permitDetails != null)
		{
			Date currDate = new Date();
			String strcurrDate= new SimpleDateFormat("dd-MM-yyyy").format(currDate);
			Date curDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			Date harvestDate = permitDetails.getHarvestdt();
			//Added by sahadeva 14/11/2016
			List<PermitDetails> permitDetails1 = ahFuctionalService.getValidityDate(permitNo);
			Date vdate=permitDetails1.get(0).getValiditydate();
			if(vdate != null)
			{
				long diff = vdate.getTime()-curDate.getTime();
				long diffDays = diff / (24 * 60 * 60 * 1000);
				float daysdiff = (float) diffDays;
				float nodays = daysdiff;// / 2;
				nodays = Math.round(nodays);
				int nnodays = (int) nodays;
				//nnodays = Math.abs(nnodays);
				if(nnodays < 0 )
				{
					nRet = 3;
					//hasHarvestingDate = false;
				}
				else if ( nnodays > 1)
				{
					nRet = 3;					
				}
			}
			else
			{
				nRet = 2;
				//hasHarvestingDate = false;
			}
		}
		logger.info("========validateHarvestDateForPermitForWeighment()========== nRet "+nRet);			
        return nRet;
    }
	//Added by DMurty on 08-11-2016
	public List getDisbursedDate(int branchcode,int lnumber)
    {
		logger.info("========getDisbursedDate()==========");
        String sql = "select * from LoanDetails where branchcode="+branchcode+" and loannumber="+lnumber;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 09-11-2016
	public List<AccountSummaryTemp> getRecordinAccSmryTemp(String accountCode,String season)
    {
        String sql = "select * from AccountSummaryTemp where accountcode ='" + accountCode+"' and season = '"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, AccountSummaryTemp.class);
    }
	public List<AccountSummary> getRecordinAccSmry(String accountCode,String season)
    {
        String sql = "select * from AccountSummary where accountcode ='"+accountCode+"' and season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findByNativeSql(sql, AccountSummary.class);
    }
	public List getAccSubGrpDtls(int accGrpCode,int accSubGrpCode,String sesn)
    {
		logger.info("========getAccSubGrpDtls()==========");
        String sql = "select * from AccountSubGroupSummary where accountgroupcode="+accGrpCode+" and accountsubgroupcode="+accSubGrpCode+" and season='"+sesn+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 14-11-2016 
	public List getAccDetailsFromSummary(String accountCode,String sesn)
    {
		logger.info("========getAccDetailsFromSummary()==========");
        String sql = "select * from AccountSummary where accountcode='"+accountCode+"' and season='"+sesn+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAccGrpDetailsFromSummary(int accountGroupCode,String sesn)
    {
		logger.info("========getAccGrpDetailsFromSummary()==========");
        String sql = "select * from AccountGroupSummary where accountgroupcode="+accountGroupCode+" and season='"+sesn+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public void deleteTempRecords(String model,String loginUser,int caneacslno,String season)
	{
		logger.info("========deleteTempRecords Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createSQLQuery("Delete from "+model+" where caneacslno="+caneacslno+" and login='"+loginUser+"' and season='"+season+"'").executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public List getDedPartForSbAccList(int caneaccslno,String sesn)
    {
		logger.info("========getDedPartForSbAccList()==========");
        String sql = "select sum(forsbacs) as ded,sum(forloans) as advloans,sum(totalvalue) as totalval from CaneValueSplitupTemp where caneacslno="+caneaccslno+" and season='"+sesn+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getDedPartForSbAccListForRyot(int caneaccslno,String sesn,String ryotCode)
    {
		logger.info("========getDedPartForSbAccListForRyot()==========");
        String sql = "select forsbacs,forloans,totalvalue from CaneValueSplitupTemp where caneacslno="+caneaccslno+" and season='"+sesn+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 19-11-2016 
	public List getCaneValueSplitupDetailsByDate(String sesn,String strCaneAccDate)
    {
		Date CaneAccDate=DateUtils.getSqlDateFromString(strCaneAccDate,Constants.GenericDateFormat.DATE_FORMAT);
		logger.info("========getCaneValueSplitupDetailsByDate()==========");
		String sql = "select * from CaneValueSplitupDetailsByDate where season='"+sesn+"' and caneaccountingdate between '"+CaneAccDate+"' and '"+CaneAccDate+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getCaneValueSplitupDetailsByRyot(String sesn,String strCaneAccDate,String ryotCode)
    {
		Date CaneAccDate=DateUtils.getSqlDateFromString(strCaneAccDate,Constants.GenericDateFormat.DATE_FORMAT);
		logger.info("========getCaneValueSplitupDetailsByRyot()==========");
		String sql = "select * from CaneValueSplitupDetailsByRyot where ryotcode='"+ryotCode+"' and season='"+sesn+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAgreementDetailsByaggrementno(String season,String agreementNo,Integer plotNum,String ryotCode)
    {
		logger.info("========getAgreementDetailsByaggrementno()==========");
		String sql = "Select * from  AgreementDetails  WHERE seasonyear ='"+season+"' and plotnumber='"+plotNum+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }

	public List getVehicleNumbers(String season)
    {
		String sql = "select distinct vehicleno from WeighmentDetails where season='"+season+"' order by vehicleno";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAdvancePrinciples(String ryotcode,int advanceCode,String season)
	{
		String sql = "select * from AdvancePrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"' and  advancecode="+advanceCode;
        return hibernateDao.findByNativeSql(sql, AdvancePrincipleAmounts.class);
	}
	
	public List getLoanPrinciples(String ryotcode,int loannumber,String season)
	{
		String sql = "select * from LoanPrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"' and loannumber="+loannumber;
        return hibernateDao.findByNativeSql(sql, LoanPrincipleAmounts.class);
	}
	//Added by DMurty on 01-12-2016
	public List getWeighmentList(int permitNo)
    {
		int slNo = 0;
		logger.info("========getWeighmentList()========== permitNo"+permitNo);
		String sql = "select * from WeighmentDetails where permitnumber="+permitNo+" and status=0";
		logger.info("========sql=========="+sql);
		List WeighmentList = hibernateDao.findBySqlCriteria(sql);
        return WeighmentList;
    }
	
	public List CheckWeighmentList(int permitNo)
    {
		int slNo = 0;
		logger.info("========CheckWeighmentList()========== permitNo"+permitNo);
		String sql = "select * from WeighmentDetails where permitnumber="+permitNo;
		logger.info("========sql=========="+sql);
		List WeighmentList = hibernateDao.findBySqlCriteria(sql);
        return WeighmentList;
    }
	public List getAdvanceSummaryList(String ryotcode,int advanceCode,String season)
	{
		String sql = "select * from AdvanceSummary where ryotcode='"+ryotcode+"' and season='"+season+"' and advancecode="+advanceCode;
        return hibernateDao.findByNativeSql(sql, AdvanceSummary.class);
	}
	
	public List getEffectedRyots(String ryotCode)
    {
		int slNo = 0;
		logger.info("========getEffectedRyots()========== permitNo"+ryotCode);
		String sql = "select * from EffectedRyots where ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
		List WeighmentList = hibernateDao.findBySqlCriteria(sql);
        return WeighmentList;
    }
	public List getLoanPrinciples(String ryotcode,String season)
	{
		String sql = "select sum(principle) as loanamount from LoanPrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"'";
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getAdvancePrinciplesNew(String ryotcode,String season)
	{
		String sql = "select sum(principle) as advanceamount from AdvancePrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"'";
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	
	public List getSbDetails(String ryotCode,int CanAccSlNo,String season)
	{
		
		String sql = "select cdcamt,ulamt,harvesting,transport,pendingamt+paidamt as pendingamt,sbaccno,otherded,bankcode from CanAccSBDetails where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacctslno="+CanAccSlNo;
		//String sql = "select * from CanAccSBDetails where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanDetails(String ryotCode,int CanAccSlNo,String season)
	{
		String sql =null;
		if(CanAccSlNo<12)
		sql = "select sum(pendingamt)+sum(paidamt) as amt,sum(interestamount) as intrest from CanAccLoansDetails where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and caneacctslno="+CanAccSlNo;
		else
	    sql = "select sum(pendingamt) as amt,sum(interestamount) as intrest from CanAccLoansDetails where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and caneacctslno="+CanAccSlNo;
		
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getAdvanceDetails(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select sum(paidamt) as advance from CanAccLoansDetails where ryotcode='"+ryotCode+"' and advloan=0 and season='"+season+"' and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanAccountNumber(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select * from CanAccLoansDetails where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and pendingamt>0 and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getLoanAcNo(String ryotcode,String season,int loanNo,int bankCode)
	{
		String sql = "select * from LoanPrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"' and branchcode="+bankCode+" and loannumber="+loanNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getLoanDetailsByCanAccSlno(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select * from CanAccLoansDetails where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and  caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getAdvanceSummary(String ryotcode,int advanceCode,String season)
	{
		String sql = "select * from AdvanceSummary where ryotcode='"+ryotcode+"' and season='"+season+"' and  advancecode="+advanceCode;
        return hibernateDao.findByNativeSql(sql, AdvanceSummary.class);
	}
	public List getSbDetailsTemp(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select * from CanAccSBDetailsTemp where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanDetailsTemp(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select sum(pendingamt) as amt,sum(interestamount) as intrest from CanAccLoansDetailsTemp where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanDetailsByCanAccSlnoTemp(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select * from CanAccLoansDetailsTemp where ryotcode='"+ryotCode+"' and advloan=1 and season='"+season+"' and  caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getAdvanceDetailsTemp(String ryotCode,int CanAccSlNo,String season)
	{
		String sql = "select sum(paidamt) as advance from CanAccLoansDetailsTemp where ryotcode='"+ryotCode+"' and advloan=0 and season='"+season+"' and caneacctslno="+CanAccSlNo;
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getWeighmentListForSecondPermit(int permitNo,String season)
    {
		int slNo = 0;
		logger.info("========getWeighmentList()========== permitNo"+permitNo);
		String sql = "select * from WeighmentDetails where permitnumber="+permitNo+" and status=1 and season='"+season+"'";
		logger.info("========sql=========="+sql);
		List WeighmentList = hibernateDao.findBySqlCriteria(sql);
        return WeighmentList;
    }
	//Added by DMurty on 04-01-2017
	public void addDeductionsTemp(DedDetails_temp dedDetails_temp) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(dedDetails_temp);
		session.flush();
		session.close();
	}
	public void updateAmountForLoans(String strQry)
	{
		logger.info("========updateAmountForLoans()=========="+strQry);
		hibernateDao.getSessionFactory().openSession().createQuery(strQry).executeUpdate();
	}
	
	//added by sahadeva 17-01-2017
	public double getTotalLoanAmt(String qurey)
    {
		double loan = 0.0;
        String sql = qurey;
		logger.info("========sql=========="+sql);
		List loanList = hibernateDao.findBySqlCriteria(sql);
		if(loanList != null && loanList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) loanList.get(0);
			if((Double) tempMap.get("loan") != null)
			{
				loan = (Double) tempMap.get("loan");
			}
		}
        return loan;
    }
	public double GetRyotSupply(String qurey)
    {
		double netwt = 0.0;
        String sql = qurey;
		logger.info("========sql=========="+sql);
		List netwtList = hibernateDao.findBySqlCriteria(sql);
		if(netwtList != null && netwtList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) netwtList.get(0);
			if((Double) tempMap.get("netwt") != null)
			{
				netwt = (Double) tempMap.get("netwt");
			}
		}
        return netwt;
    }
	
	public List getCompletedCaneAccounts(String table)
    {
		logger.info("========getCompletedCaneAccounts()==========");
        String sql = "select * from "+table;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getRyotList(int nCaneActSlNo, String strTable)
    {
		logger.info("========getRyotList()==========");
        String sql = "select * from "+strTable+" where caneacslno = "+nCaneActSlNo+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getHarvestingOrTpDetails(int code)
    {
		logger.info("========getHarvestingDetails()==========");
        String sql = "select distinct ryotcode,season from DedDetails where DedCode="+code;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public int ContCode(String ryotCode,String season,int code)
    {
		int ContCode=0;
		if(code == 4)
		{
			//Harvest
			String qry = "Select harvestcontcode from HarvestingRateDetails where ryotcode='"+ryotCode+"' and season='"+season+"'";
	        List harvestRateList=hibernateDao.findBySqlCriteria(qry);
			logger.info("ContCode()========harvestRateList=========="+harvestRateList);
	        if(harvestRateList !=null && harvestRateList.size()>0)
	        {
	        	Map HarvestMap = new HashMap();
	    		HarvestMap = (Map) harvestRateList.get(0);
	    		ContCode = (Integer) HarvestMap.get("harvestcontcode");
	        }
		}
		else
		{
			//Tp
			String qry = "Select transportcontcode from TransportingRateDetails where ryotcode='"+ryotCode+"' and season='"+season+"'";
	        List tpRateList=hibernateDao.findBySqlCriteria(qry);
			logger.info("ContCode()========tpRateList=========="+tpRateList);
	        if(tpRateList !=null && tpRateList.size()>0)
	        {
	        	Map tpMap = new HashMap();
	        	tpMap = (Map) tpRateList.get(0);
	    		ContCode = (Integer) tpMap.get("transportcontcode");
	        }
		}
        return ContCode;
    }	
	//Added by DMurty on 01-02-2017
	public List getTempAgreementsForRyot(String ryotcode,String season)
    {
        String sql = "select * from RandDAgreementSummary where ryotcode='"+ryotcode+"' and seasonyear='"+season+"'";
		logger.info("getTempAgreementsForRyot()========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
		    }
	//added by naidu on 04-02-2017
	public double getPrincipleAdvAmt(String qurey)
    {
		double advance = 0.0;
		logger.info("========sql=========="+qurey);
		List loanList = hibernateDao.findBySqlCriteria(qurey);
		if(loanList != null && loanList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) loanList.get(0);
			if((Double) tempMap.get("padvance") != null)
			{
				advance = (Double) tempMap.get("padvance");
			}
		}
        return advance;
    }
	
//Added by DMurty on 13-02-2017	
	public List getSeedSupplierAccounts(String season)
    {
		logger.info("========getSeedSupplierAccounts()==========");
        String sql = "select * from AdvanceDetails where ryotcode=seedsuppliercode and season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getRunningBalForAccount(String accountcode,String season)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountDetails where accountcode='"+accountcode+"' and season='"+season+"'";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	
	public List getRunningBalForAccountGrp(int accountgroupcode,String season)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountGroupDetails where accountgroupcode='"+accountgroupcode+"' and season='"+season+"'";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	
	public List getRunningBalForAccountSubGrp(int accountgroupcode,String season)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountSubGroupDetails where accountsubgroupcode='"+accountgroupcode+"' and season='"+season+"'";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	
	public List getWeighmentDetailsData()
    {
		logger.info("========getWeighmentDetailsData()==========");
        String sql = "select  ryotcode,season,transactioncode,netwt from WeighmentDetails where transactioncode<=5281 ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List GetAccInfo(String ryotCode,String season,Integer tCode,String glcode)
    {
		logger.info("========getWeighmentDetailsData()==========");
        String sql = "select  * from AccountDetails where accountcode='"+ryotCode+"' and season='"+season+"' and transactioncode="+tCode+" and glcode='"+glcode+"' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }

	public List getWeighmentDatesData()
    {
		logger.info("========getWeighmentDatesData()==========");
        String sql = "select  canereceiptenddate,season,transactioncode from WeighmentDetails where season='2016-2017' ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }

	public List updateLoanPrinciplesForAccountedRyots()
    {
		logger.info("========updateLoanPrinciplesForAccountedRyots()==========");
        String sql = "select * from CanAccLoansDetails where advisedamt=0 and advloan=1 and caneacctslno=13 and paidamt>0";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 22-02-2017
	public List getOldLoans()
    {
		logger.info("========getOldLoans()==========");
        String sql = "select  * from LoanSummary where season='2016-2017' order by branchcode,loannumber";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List checkLoanDetails(int loanNumber,int branchCode,String season)
    {
		logger.info("========checkLoanDetails()==========");
        String sql = "select  * from LoanDetails where Season='"+season+"' and branchcode="+branchCode+" and loannumber="+loanNumber;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getOldLoanDetails(int loanNumber,int branchCode,String season)
    {
		logger.info("========checkLoanDetails()==========");
        String sql = "select  * from LoanDetailsTemp where Season='"+season+"' and branchcode="+branchCode+" and loannumber="+loanNumber;
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List loanReconsilation(String suppliedOrNot)
    {
		String sql = null;
		logger.info("========loanReconsilation()==========suppliedOrNot--"+suppliedOrNot);
		if("Supplied".equalsIgnoreCase(suppliedOrNot))
		{
	        sql = "select distinct ryotcode,season from LoanPrincipleAmounts where principle>2 and season='2016-2017' and ryotcode  in (select ryotcode from WeighmentDetails) order by ryotcode ";
		}
		else
		{
	        sql = "select distinct ryotcode,season from LoanPrincipleAmounts where principle>2 and season='2016-2017' and ryotcode not in (select ryotcode from WeighmentDetails) order by ryotcode ";
		}
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getRyotSupplyAndPayDetails(String ryotcode,String season)
    {
		logger.info("========getRyotSupplyAndPayDetails()==========");
        String sql = "select sum(suppqty) as qty,sum(totalamt) as val,sum(cdcamt) as cdc,sum(ulamt) as ul,sum(harvesting) as har,sum(transport) as tp,sum(otherded) as od,sum(advamt) as advance,sum(loanamt) as loan,sum(sbac) as sb from CanAccSBDetails  where ryotcode='"+ryotcode+"' and season='"+season+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//added by naidu on 13-03-2017
	public void updateRyotNameInAgreement(String ryotcode,String ryotname,String relativename)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails SET ryotname='"+ryotname+"',relativename='"+relativename+"' WHERE ryotcode = '"+ryotcode+"'").executeUpdate();
	
	}
	public List<LabourContractor> getLabourContractorAccCode(int lccode)
    {
        String sql = "select * from LabourContractor where lcCode =" + lccode;
        
        return hibernateDao.findByNativeSql(sql, LabourContractor.class);
    }
	public List getPincodeDataFromTemp()
    {
		logger.info("========updateLoanPrinciplesForAccountedRyots()==========");
        String sql = "select * from Temp_VillagePincode where pincode is not null";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List<AccountDetails> getAccountDetailsForSeed(String season,int transactioncode)
    {
		 String sql = "select  * from AccountDetails where season='"+season+"' and transactioncode="+transactioncode+" and status!=0";
         return hibernateDao.findByNativeSql(sql, AccountDetails.class);
    }
	public List<AccountGroupDetails> getAccountGroupDetailsForSeed(String season,int transactioncode)
    {
		 String sql = "select  * from AccountGroupDetails where season='"+season+"' and transactioncode="+transactioncode+" and status!=0";
         return hibernateDao.findByNativeSql(sql, AccountGroupDetails.class);
    }
	public List<AccountSubGroupDetails> getAccountSubGroupDetailsForSeed(String season,int transactioncode)
    {
		 String sql = "select  * from AccountSubGroupDetails where season='"+season+"' and transactioncode="+transactioncode+" and status!=0";
         return hibernateDao.findByNativeSql(sql, AccountSubGroupDetails.class);
    }
	public List getRunningBalForSeedAccount(String accountcode,String season,int transactioncode)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountDetails where accountcode='"+accountcode+"' and season='"+season+"' and transactioncode!="+transactioncode+"";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	
	public List getRunningBalForSeedAccountGrp(int accountgroupcode,String season,int transactioncode)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountGroupDetails where accountgroupcode='"+accountgroupcode+"' and season='"+season+"' and transactioncode!="+transactioncode+"";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	
	public List getRunningBalForSeedAccountSubGrp(int accountgroupcode,String season,int transactioncode,int accountsubgroupcode)
	{
		String sql = "select sum(cr) as cr,sum(dr) as dr from AccountSubGroupDetails where accountgroupcode="+accountgroupcode+" and accountsubgroupcode='"+accountsubgroupcode+"' and season='"+season+"' and transactioncode!="+transactioncode+"";
		List AccList = hibernateDao.findBySqlCriteria(sql);
		return AccList;
	}
	public List<AccountDetails> getAccountDetailsForSeedSuuplier(String season,String glcode)
    {
		 String sql = "select  * from AccountDetails where season='"+season+"' and glcode='"+glcode+"' and status!=0 order by transactioncode";
         return hibernateDao.findByNativeSql(sql, AccountDetails.class);
    }
	public List getDistinctAccountsForSeedSuuplier(String season,String glcode)
    {
		 String sql = "select  distinct accountcode from AccountDetails where season='"+season+"' and glcode='"+glcode+"' and status!=0";
         return hibernateDao.findBySqlCriteria(sql);
    }
	public List<AccountGroupDetails> getAccountGpDetailsForSeedSuuplier(String season,int transactioncode)
    {
		 String sql = "select  * from AccountGroupDetails where season='"+season+"' transactioncode="+transactioncode+" and status!=0 ";
         return hibernateDao.findByNativeSql(sql, AccountGroupDetails.class);
    }
	public List<Temp_BatchServiceMaster> getBatchServicemasterDataForMig(String season)
    {
		 String sql = "select  * from Temp_BatchServiceMaster where season='"+season+"' order by id";
         return hibernateDao.findByNativeSql(sql, Temp_BatchServiceMaster.class);
    }
	public List getSuppliersFromBatchSeries(String season)
    {
		 String sql = "select  distinct seedsuppliercode from BatchServicesMaster where season='"+season+"' and id>33";
		 return hibernateDao.findBySqlCriteria(sql);
    }
	public List<SeedSource> listSourceSeedsForBatch(String supcode,String season)
	{	
		String sql = "select * from SeedSource where seedsrcode ='" +supcode+"' and id>116";// and runningyear='"+year+"'
		return hibernateDao.findByNativeSql(sql, SeedSource.class);
	}
	public List getBatchForSupplier(String suppliercode)
    {
		 String sql = "select  * from BatchServicesMaster where seedsuppliercode='"+suppliercode+"' and id>33 order by id";
		 return hibernateDao.findBySqlCriteria(sql);
    }
	
	
	
	
	
	
	
}
