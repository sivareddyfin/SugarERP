package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Screens;

@Repository("ScreensDao")
@Transactional


public class ScreensDao 
{
	private static final Logger logger = Logger.getLogger(ScreensDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addScreens(Screens screens) 
    {
    	Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(screens);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Screens> listScreen() 
	{	
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Screens> screenss=session.createCriteria(Screens.class).list();
		session.close();
		return screenss;
	
	}
	
	 public List<Screens> getScreensByCode(Integer screencode)
    {
        String sql = "select * from Screens where screencode >" + screencode;
        
        return hibernateDao.findByNativeSql(sql, Screens.class);
    }

}
