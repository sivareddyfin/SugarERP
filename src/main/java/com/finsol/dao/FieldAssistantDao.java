package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;

@Repository("FieldAssistantDao")
@Transactional
public class FieldAssistantDao {
	
	private static final Logger logger = Logger.getLogger(FieldAssistantDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addFieldAssistant(FieldAssistant fieldAssistant) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(fieldAssistant);
		session.flush();
		session.close();
	}
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<FieldAssistant> listFieldAssistants() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<FieldAssistant> fieldAssistants=session.createCriteria(FieldAssistant.class).list();
		session.close();
		return fieldAssistants;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}

}
