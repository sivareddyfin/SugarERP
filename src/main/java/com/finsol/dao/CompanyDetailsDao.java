package com.finsol.dao;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CompanyBranches;
import com.finsol.model.CompanyDetails;


@Repository("CompanyDetailsDao")
@Transactional
public class CompanyDetailsDao 
{
	private static final Logger logger = Logger.getLogger(CompanyDetailsDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addCompanyDetails(CompanyDetails companyDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(companyDetails);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<CompanyDetails> listCompanyDetail() 
	{
		return (List<CompanyDetails>) hibernateDao.getSessionFactory().openSession().createCriteria(CompanyDetails.class).list();	
	}
	
	public void addCompanyBranches(CompanyBranches companyBranches) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(companyBranches);
		session.flush();
		session.close();
	}
	@SuppressWarnings("unchecked")
	public List<CompanyBranches> listCompanyBranches() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<CompanyBranches> companyBranchess=session.createCriteria(CompanyBranches.class).list();
		session.close();
		return companyBranchess;
	}
	 
	 public void deleteAllCompanyBranches() 
	 {
			hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM CompanyBranches").executeUpdate();
	 }

	
	
}
