package com.finsol.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.controller.AgricultureHarvestingController;
import com.finsol.model.AccruedAndDueCaneValueSummary;
import com.finsol.model.AdvancePaymentSummary;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementSummary;
import com.finsol.model.CaneAccountSmry;
import com.finsol.model.CaneAccountSmrytemp;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneValueSplitup;
import com.finsol.model.CaneValueSplitupDetailsByDate;
import com.finsol.model.CaneValueSplitupDetailsByRyot;
import com.finsol.model.CaneValueSplitupSummary;
import com.finsol.model.CaneValueSplitupTemp;
import com.finsol.model.DedDetails_temp;
import com.finsol.model.HarvChgDtlsByRyotContAndDate;
import com.finsol.model.HarvChgSmryByContAndDate;
import com.finsol.model.HarvChgSmryByRyotAndCont;
import com.finsol.model.HarvChgSmryBySeason;
import com.finsol.model.HarvChgSmryBySeasonByCont;
import com.finsol.model.HarvestingRateDetails;
import com.finsol.model.HarvestingRatesSmry;
import com.finsol.model.LoanPaymentSummary;
import com.finsol.model.LoanPaymentSummaryByBank;
import com.finsol.model.LoanPrincipleAmounts;
import com.finsol.model.LoanSummary;
import com.finsol.model.OtherDedSmryByRyot;
import com.finsol.model.RyotBankDetails;
import com.finsol.model.SBAccSmryByBank;
import com.finsol.model.SBAccSmryByRyot;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.TranspChgSmryByContAndDate;
import com.finsol.model.TranspChgSmryByRyotAndCont;
import com.finsol.model.TranspChgSmryByRyotContAndDate;
import com.finsol.model.TranspChgSmryBySeason;
import com.finsol.model.TranspChgSmryBySeasonByCont;
import com.finsol.model.TransportingRateDetails;
import com.finsol.model.TransportingRatesSmry;
import com.finsol.model.WeighmentDetails;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("CaneAccountingFunctionalDao")
@Transactional
public class CaneAccountingFunctionalDao {
	
private static final Logger logger = Logger.getLogger(CaneAccountingFunctionalDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	@Autowired	
	private AgricultureHarvestingController agricultureHarvestingController;
	
	
	
	public List getRyotsByCircleandSeason(String season,int circleCode)
    {
		//Modified by DMurty on 10-08-2016
        String sql = "SELECT DISTINCT a.ryotcode,a.ryotseqno,b.ryotname from AgreementDetails a,Ryot b where a.seasonyear='"+season+"' and a.circlecode="+circleCode+" and a.ryotcode=b.ryotcode ORDER BY ryotseqno";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void addHarvestingChargesSmry(HarvestingRatesSmry harvestingRatesSmry)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvestingRatesSmry);
		session.flush();
		session.close();
	}
	public void addHarvestingChargesDtls(HarvestingRateDetails harvestingRateDetails) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(harvestingRateDetails);
		session.flush();
	}
	public void deleteHarvestingCharges(String rtoyCode,double sno)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM HarvestingRateDetails WHERE ryotCode ='"+rtoyCode+"' and sno="+sno).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public HarvestingRateDetails getUpdatedHarvestingCharges(String season,int circleCode,Date effDate,String ryotcode) 
	{
		String sqlString="select *  from HarvestingRateDetails where season='"+season+"' and circlecode="+circleCode+" and date='"+effDate+"' and ryotcode='"+ryotcode+"'";
		List<HarvestingRateDetails> harvestingRateDetails=hibernateDao.findByNativeSql(sqlString, HarvestingRateDetails.class);
		return harvestingRateDetails.get(0);
	}
	
	public List GetDetailsForHarvChgDtlsByRyotContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {
		//Modified by DMurty on 17-10-2016
        String sql = "select * from HarvChgDtlsByRyotContAndDate where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        //return hibernateDao.findBySqlCriteria(sql); 
        List <HarvChgDtlsByRyotContAndDate> HarvChgDtlsByRyotContAndDate=hibernateDao.findByNativeSql(sql, HarvChgDtlsByRyotContAndDate.class);
        if(HarvChgDtlsByRyotContAndDate.size()>0 && HarvChgDtlsByRyotContAndDate!=null)
        	return HarvChgDtlsByRyotContAndDate;
        else
        	return null;
    }
	
	public void addHarvChgDtlsByRyotContAndDate(HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvChgDtlsByRyotContAndDate);
		session.flush();
		session.close();
	}
	
	public void updateAmountInHarvChgDtlsByRyotContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate )
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE HarvChgDtlsByRyotContAndDate set totalamount="+UpdatingAmt+",harvestcontcode="+contractorCode+" where ryotcode ='"+ryotCode+"' and season='"+season+"'").executeUpdate();
	}
	
	public List getCaneWeightByRyotCode(String ryotCode,String season)
    {
        String sql = "select netwt from WeighmentDetails where ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List HarvChgSmryByContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select totalamount from HarvChgSmryByContAndDate where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void addHarvChgSmryByContAndDate(HarvChgSmryByContAndDate harvChgSmryByContAndDate)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvChgSmryByContAndDate);
		session.flush();
		session.close();
	}
	
	public void updateAmountInHarvChgSmryByContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE HarvChgSmryByContAndDate set totalamount="+UpdatingAmt+",harvestcontcode="+contractorCode+" WHERE season='"+season+"'").executeUpdate();
	}
	
	public List HarvChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season)
    {
		//Modified by DMurty on 17-10-2016
        String sql = "select * from HarvChgSmryByRyotAndCont where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        List <HarvChgSmryByRyotAndCont> HarvChgSmryByRyotAndCont=hibernateDao.findByNativeSql(sql, HarvChgSmryByRyotAndCont.class);
        if(HarvChgSmryByRyotAndCont.size()>0 && HarvChgSmryByRyotAndCont!=null)
        	return HarvChgSmryByRyotAndCont;
        else
        	return null;
    }
	
	public void addHarvChgSmryByRyotAndCont(HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvChgSmryByRyotAndCont);
		session.flush();
		session.close();
	}
	public void updateAmountInHarvChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,double UpdatingAmt)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE HarvChgSmryByRyotAndCont set totalamount="+UpdatingAmt+",PendingAmount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+",harvestcontcode="+contractorCode+" WHERE season='"+season+"' and ryotcode='"+ryotCode+"'").executeUpdate();
	}
	
	
	public List HarvChgSmryBySeason(String ryotCode,int contractorCode,String season)
    {
        String sql = "select totalamount from HarvChgSmryBySeason where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void addHarvChgSmryBySeason(HarvChgSmryBySeason harvChgSmryBySeason)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvChgSmryBySeason);
		session.flush();
		session.close();
	}
	
	public void updateAmountInHarvChgSmryBySeason(String ryotCode,int contractorCode,String season,double UpdatingAmt)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE HarvChgSmryBySeason set totalamount="+UpdatingAmt+",pendingAmount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+" WHERE season='"+season+"'").executeUpdate();
	}
	
	public List HarvChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season)
    {
        String sql = "select totalamount from HarvChgSmryBySeasonByCont where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void addHarvChgSmryBySeasonByCont(HarvChgSmryBySeasonByCont harvChgSmryBySeasonByCont)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvChgSmryBySeasonByCont);
		session.flush();
		session.close();
	}
	public void updateAmountInHarvChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,double UpdatingAmt)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE HarvChgSmryBySeasonByCont set totalamount="+UpdatingAmt+",PendingAmount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+",harvestcontcode="+contractorCode+" WHERE season='"+season+"'").executeUpdate();
	}
	
	public List getExtentByRyots(String ryotCode,String season)
    {
        String sql = "select agreementnumber,plotnumber from AgreementDetails where ryotcode='"+ryotCode+"' and seasonyear='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	
	//Transport Charges Update
	public void deleteTransportCharges(String rtoyCode,double sno)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM TransportingRateDetails WHERE ryotcode ='"+rtoyCode+"' and slno="+sno).executeUpdate();
		logger.info("========After Delete==========");
	}
	public void addTransportingRatesSmry(TransportingRatesSmry transportingRatesSmry)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transportingRatesSmry);
		session.flush();
		session.close();
	}
	
	public void addTransportingRatesDtls(TransportingRateDetails transportingRateDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transportingRateDetails);
		session.flush();
		session.close();
	}
	
	public List getCaneWeightByContAndDate(int contCode,String ryotCode)
    {
        String sql = "select netwt from WeighmentDetails where ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public List GetTranspChgSmryByContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select totalamount from TranspChgSmryByContAndDate where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void updateAmountInTranspChgSmryByContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE TranspChgSmryByContAndDate set totalamount="+UpdatingAmt+",transportcontcode="+contractorCode+" WHERE season='"+season+"' and receiptdate='"+efDate+"'").executeUpdate();
	}
	
	public void addTranspChgSmryByContAndDate(TranspChgSmryByContAndDate transpChgSmryByContAndDate)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transpChgSmryByContAndDate);
		session.flush();
		session.close();
	}
	
	public List GetTranspChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,Date efDate)
    {
		//Modified by DMurty on 17-10-2016
        String sql = "select id,totalamount,transportedcanewt from TranspChgSmryByRyotAndCont where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public void updateAmountInTranspChgSmryByRyotAndCont(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE TranspChgSmryByRyotAndCont set totalamount="+UpdatingAmt+",pendingamount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+",transportcontcode="+contractorCode+" WHERE season='"+season+"' and ryotcode='"+ryotCode+"'").executeUpdate();
	}
	
	public void addTranspChgSmryByRyotAndCont(TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transpChgSmryByRyotAndCont);
		session.flush();
		session.close();
	}
	
	public List GetTranspChgSmryByRyotContAndDate(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select id,totalamount,transportedcanewt,mincanewt from TranspChgSmryByRyotContAndDate where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public void updateAmountInTranspChgSmryByRyotContAndDate(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE TranspChgSmryByRyotContAndDate set totalamount="+UpdatingAmt+",transportcontcode="+contractorCode+" WHERE season='"+season+"' and ryotcode='"+ryotCode+"'").executeUpdate();
	}
	
	public void addTranspChgSmryByRyotContAndDate(TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transpChgSmryByRyotContAndDate);
		session.flush();
		session.close();
	}
	
	public List GetTranspChgSmryBySeason(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select totalamount from TranspChgSmryBySeason where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	public void updateAmountInTranspChgSmryBySeason(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE TranspChgSmryBySeason set totalamount="+UpdatingAmt+",pendingamount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+" WHERE season='"+season+"'").executeUpdate();
	}
	
	public void addTranspChgSmryBySeason(TranspChgSmryBySeason transpChgSmryBySeason)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transpChgSmryBySeason);
		session.flush();
		session.close();
	}
	
	
	public List GetTranspChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select totalamount from TranspChgSmryBySeasonByCont where season='"+season+"'";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	
	public void updateAmountInTranspChgSmryBySeasonByCont(String ryotCode,int contractorCode,String season,double UpdatingAmt,Date efDate)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE TranspChgSmryBySeasonByCont set totalamount="+UpdatingAmt+",pendingamount="+UpdatingAmt+",pendingpayable="+UpdatingAmt+",totalpayableamount="+UpdatingAmt+",transportcontcode="+contractorCode+" WHERE season='"+season+"'").executeUpdate();
	}
	
	public void addTranspChgSmryBySeasonByCont(TranspChgSmryBySeasonByCont transpChgSmryBySeasonByCont)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transpChgSmryBySeasonByCont);
		session.flush();
		session.close();
	}
	
	//PCAC
	public boolean saveMultipleEntities(List entitiesList) {
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	        
	    } catch (JDBCException jde) {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        throw new RuntimeException(jde);
	    } finally {
	        if (session.isOpen()) {
	            session.close();
	            
	        }
	    }
	}
	
	
	public void addCaneValueSplitup(CaneValueSplitup caneValueSplitup)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneValueSplitup);
		session.flush();
		session.close();
	}
	
	public void addCaneValueSplitupTemp(CaneValueSplitupTemp caneValueSplitupTemp)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneValueSplitupTemp);
		session.flush();
		session.close();
	}
	
	public void addCaneValueSplitupSummary(CaneValueSplitupSummary caneValueSplitupSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneValueSplitupSummary);
		session.flush();
		session.close();
	}
	
	public List getWeighmentDetails(Date fromdate,Date todate,String season,String villageCode,int category)
    {
		String sql = null;
		if("0".equals(villageCode))
		{
			if(category ==2)
			{
		        sql = "select a.* from WeighmentDetails a,Ryot b where a.season ='" + season+"' and a.canereceiptenddate between '" + fromdate+"' and '" + todate+"' and a.castatus=0 and a.status=1 and a.ryotcode=b.ryotcode and b.isweighmentcompleted=1 order by a.ryotcode";
			}
			else
			{
		        sql = "select a.* from WeighmentDetails a,Ryot b where a.season ='" + season+"' and a.canereceiptenddate between '" + fromdate+"' and '" + todate+"' and a.castatus=0 and a.status=1 and a.ryotcode=b.ryotcode and b.isweighmentcompleted=1 and b.ryotcategory="+category+" order by a.ryotcode";
			}
		}
		else
		{
			if(category ==2)
			{
		        sql = "select a.* from WeighmentDetails a,Ryot b where a.season ='" + season+"' and a.canereceiptenddate between '" + fromdate+"' and '" + todate+"' and a.castatus=0 and a.status=1 and a.landvilcode='"+villageCode+"'  and a.ryotcode=b.ryotcode and b.isweighmentcompleted=1 order by a.ryotcode";
			}
			else
			{
		        sql = "select a.* from WeighmentDetails a,Ryot b where a.season ='" + season+"' and a.canereceiptenddate between '" + fromdate+"' and '" + todate+"' and a.castatus=0 and a.status=1 and a.landvilcode='"+villageCode+"'  and a.ryotcode=b.ryotcode and b.isweighmentcompleted=1 and b.ryotcategory="+category+" order by a.ryotcode";
			}
		}
		logger.info("getWeighmentDetails()========sql=========="+sql);

        List weighmentDetailsList=hibernateDao.findByNativeSql(sql, WeighmentDetails.class);
		logger.info("getWeighmentDetails()========weighmentDetailsList=========="+weighmentDetailsList);
        if(weighmentDetailsList.size()>0 && weighmentDetailsList!=null)
        	return weighmentDetailsList;
        else
        	return null; 
    }
	
	public List<CaneAccountingRules> getCaneAccRulesForCalc(String season,Date date1,Date date2) 
	{
		String sqlString="select *  from CaneAccountingRules where season='"+season+"' and date=(select max(date) from CaneAccountingRules)";
        //String sql = "select * from CaneAcctRules where season ='"+season+"' and calcruledate = (SELECT max(calcruledate) FROM CaneAcctRules where season ='"+season+"' and calcruledate <= '"+finalDate+"')";
		List<CaneAccountingRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAccountingRules.class);
		return caneAccountingRules;
	}
	
	public List getCaneValueSplitupSummary(String season,String ryotCode)
    {
        String sql = "select * from CaneValueSplitupSummary where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        List caneValueSplitupSummaryList=hibernateDao.findByNativeSql(sql, CaneValueSplitupSummary.class);
        if(caneValueSplitupSummaryList.size()>0 && caneValueSplitupSummaryList!=null)
        	return caneValueSplitupSummaryList;
        else
        	return null; 
    }
	
	public List getCaneValueSplitupDetailsByDate(String season,String ryotCode,String efDate)
    {
		//Modified by DMurty on 16-11-2016 changed from ryotcode to date.
        String sql = "select * from CaneValueSplitupDetailsByDate where season ='" + season+"' and caneaccountingdate between '"+efDate+"' and '"+efDate+"'";
        List caneValueSplitupDetailsByDateList=hibernateDao.findByNativeSql(sql, CaneValueSplitupDetailsByDate.class);
        if(caneValueSplitupDetailsByDateList.size()>0 && caneValueSplitupDetailsByDateList!=null)
        	return caneValueSplitupDetailsByDateList;
        else
        	return null;         
    }
	
	public List getCaneValueSplitupDetailsByRyot(String ryotCode)
    {
        String sql = "select * from CaneValueSplitupDetailsByRyot where ryotcode='" + ryotCode+"'";
        
        List caneValueSplitupDetailsByRyotList=hibernateDao.findByNativeSql(sql, CaneValueSplitupDetailsByRyot.class);
        if(caneValueSplitupDetailsByRyotList.size()>0 && caneValueSplitupDetailsByRyotList!=null)
        	return caneValueSplitupDetailsByRyotList;
        else
        	return null;         
    }
	
	public List getCaneAccountSmry(String season,Integer slno)
    {
        String sql = "select * from CaneAccountSmry where season ='" + season+"' and caneacslno="+slno;
        
        List caneAccountSmryList=hibernateDao.findByNativeSql(sql, CaneAccountSmry.class);
        if(caneAccountSmryList.size()>0 && caneAccountSmryList!=null)
        	return caneAccountSmryList;
        else
        	return null;         
    }
	
	public List getCaneAccountSmrytemp(String season,Integer slno)
    {
        String sql = "select * from CaneAccountSmrytemp where season ='" + season+"' and caneacslno="+slno;
        
        List caneAccountSmrytemptList=hibernateDao.findByNativeSql(sql, CaneAccountSmrytemp.class);
        if(caneAccountSmrytemptList.size()>0 && caneAccountSmrytemptList!=null)
        	return caneAccountSmrytemptList;
        else
        	return null;         
    }
	
	@SuppressWarnings("unchecked")
	public List<CaneValueSplitupSummary> listCaneValueSplitupDetails() 
	{
		return (List<CaneValueSplitupSummary>) hibernateDao.getSessionFactory().openSession().createCriteria(CaneValueSplitupSummary.class).list();
	}
	
	public List getCaneValueSplitupTemp(String season,Integer caneacno)
    {
		String sql = "SELECT RyotName,ryotcode,forsbacs,caneacslno,ForLoans,canesupplied FROM CaneValueSplitupTemp where season ='" + season+"' and caneacslno=" + caneacno+" order by ryotcode";
		List caneValueSplitupTempList=hibernateDao.findBySqlCriteria(sql);
        if(caneValueSplitupTempList.size()>0 && caneValueSplitupTempList!=null)
        	return caneValueSplitupTempList;
        else
        	return null;         
    }

	public List getAllDeductions(String season,Integer caneacno,Map tempMap)
    {
		List FinalList = new ArrayList();
		
		Date datefrom = null;
		Date dateto = null;
		List DateList = getCaneAccountSmryDetails(caneacno,season);
		logger.info("getAllDeductions()========DateList=========="+DateList);
		if(DateList != null && DateList.size()>0)
		{
			Map DateMap = new HashMap();
			DateMap = (Map) DateList.get(0);
			datefrom = (Date) DateMap.get("datefrom");
			dateto = (Date) DateMap.get("dateto");
			
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String datefrom1 = df.format(datefrom);
			String dateto1 = df.format(dateto);

			datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
			dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
		}
		logger.info("getAllDeductions()========datefrom=========="+datefrom);
		logger.info("getAllDeductions()========dateto=========="+dateto);

        String dedQry = "select * from standarddeductions where status=0 order by deductionOrder";
        List dedList=hibernateDao.findBySqlCriteria(dedQry);
		logger.info("getAllDeductions()========dedList=========="+dedList);
  		
		String ryotCode = (String) tempMap.get("ryotcode");
		double caneSupplied = (Double) tempMap.get("canesupplied");
		int caneAccSlNo = (Integer) tempMap.get("caneacslno");
		logger.info("getAllDeductions()========tempMap=========="+tempMap);

		if(dedList !=null && dedList.size()>0)
        {
        	for(int j = 0;j<dedList.size();j++)
        	{
        		logger.info("getAllDeductions()========j=========="+j);
        		Map DedMap = new HashMap();
        		DedMap = (Map) dedList.get(j);
        		
        		int id = (Integer) DedMap.get("id");
        		double amount = (Double) DedMap.get("amount");
        		byte amountValidity = (Byte) DedMap.get("amountValidity");
        		int deductionOrder = (Integer) DedMap.get("deductionOrder");
        		String name = (String) DedMap.get("name");
        		byte status = (Byte) DedMap.get("status");
        		int stdDedCode = (Integer) DedMap.get("stdDedCode");
        		String columnname = (String) DedMap.get("columnname");
        		String tablename = (String) DedMap.get("tablename");
        		
        		double harvestingAmt = 0.0;
        		double harvestingRate = 0.0;
        		int hcContCode = 0;
        		int tcContCode = 0;

        		String harvestQry = "Select sum(totalamount) as tamt from HarvChgDtlsByRyotContAndDate where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"' and castatus=0";
                List harvestList=hibernateDao.findBySqlCriteria(harvestQry);
                if(harvestList !=null && harvestList.size()>0)
                {
            		Map HarvestMap = new HashMap();
            		HarvestMap = (Map) harvestList.get(0);
            		if((Double) HarvestMap.get("tamt") != null)
            		{
                		harvestingAmt = (Double) HarvestMap.get("tamt");
                		harvestingAmt = agricultureHarvestingController.round(harvestingAmt);
            		}
                }
        		logger.info("getAllDeductions()========harvestingAmt=========="+harvestingAmt);

                String harvestRateQry = "Select rate,harvestcontcode from HarvChgDtlsByRyotContAndDate where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"' and castatus=0";
                List harvestRateList=hibernateDao.findBySqlCriteria(harvestRateQry);
        		logger.info("getAllDeductions()========harvestRateList=========="+harvestRateList);
                if(harvestRateList !=null && harvestRateList.size()>0)
                {
                	Map HarvestMap = new HashMap();
            		HarvestMap = (Map) harvestRateList.get(0);
            		harvestingRate = (Double) HarvestMap.get("rate");
            		hcContCode = (Integer) HarvestMap.get("harvestcontcode");
                }
                
                double tpAmt = 0.0;
        		double tpRate = 0.0;
        		int tpContCode = 0;
        		String tpQry = "Select sum(totalamount) as tamt from TranspChgSmryByRyotContAndDate where receiptdate between '"+datefrom+"' and '"+dateto+"' and  ryotcode='"+ryotCode+"' and season='"+season+"' and castatus=0";
                List tpList=hibernateDao.findBySqlCriteria(tpQry);
                if(tpList !=null && tpList.size()>0)
                {
            		Map TpMap = new HashMap();
            		TpMap = (Map) tpList.get(0);
            		if((Double) TpMap.get("tamt") != null)
            		{
                		tpAmt = (Double) TpMap.get("tamt");
                		tpAmt = agricultureHarvestingController.round(tpAmt);
            		}
                }
        		logger.info("getAllDeductions()========tpAmt=========="+tpAmt);

                String tpRateQry = "Select rate,transportcontcode from TranspChgSmryByRyotContAndDate where receiptdate between '"+datefrom+"' and '"+dateto+"' and  ryotcode='"+ryotCode+"' and season='"+season+"' and castatus=0";
                List tpRateList=hibernateDao.findBySqlCriteria(tpRateQry);
        		logger.info("getAllDeductions()========tpRateList=========="+tpRateList);
                if(tpRateList !=null && tpRateList.size()>0)
                {
            		Map TpMap = new HashMap();
            		TpMap = (Map) tpRateList.get(0);
            		tpRate = (Double) TpMap.get("rate");
            		tcContCode = (Integer) TpMap.get("transportcontcode");
                }
                double netPayable = 0.0;
                
                if("cdcdetailsbyrotanddate".equalsIgnoreCase(tablename))
                {
                	//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingCdcAmt = 0.0;
                    String cdcDed = "select * from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=1 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=1 )";
                    List PendingCdcList=hibernateDao.findBySqlCriteria(cdcDed);
                    if(PendingCdcList != null && PendingCdcList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingCdcList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingCdcAmt = (Double) pendingMap.get("pendingamt");
                        	pendingCdcAmt = agricultureHarvestingController.round(pendingCdcAmt);
                    	}
                    }
                    netPayable = amount*caneSupplied;
                    netPayable = agricultureHarvestingController.round(netPayable);
                    netPayable = netPayable+pendingCdcAmt;

            		logger.info("getAllDeductions()========CDC NET=========="+netPayable);
                }
                else if("ucdetailsbyrotanddate".equalsIgnoreCase(tablename))
                {
                	//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingUlAmt = 0.0;
                    String ulDed = "select * from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=2 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=2 )";
                    List PendingUlList=hibernateDao.findBySqlCriteria(ulDed);
                    if(PendingUlList != null && PendingUlList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingUlList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingUlAmt = (Double) pendingMap.get("pendingamt");
                        	pendingUlAmt = agricultureHarvestingController.round(pendingUlAmt);
                    	}
                    }
                    netPayable = amount*caneSupplied;
                    netPayable = agricultureHarvestingController.round(netPayable);
                    netPayable = netPayable+pendingUlAmt;
            		logger.info("getAllDeductions()========UL NET=========="+netPayable);

                }
                else if("transpchgsmrybyryotcontanddate".equalsIgnoreCase(tablename))
                {
                	//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
                	double pendingTpAmt = 0.0;
                    String tpDed = "select * from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=3 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=3 )";
                    List PendingTpList=hibernateDao.findBySqlCriteria(tpDed);
                    if(PendingTpList != null && PendingTpList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingTpList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingTpAmt = (Double) pendingMap.get("pendingamt");
                        	pendingTpAmt = agricultureHarvestingController.round(pendingTpAmt);
                    	}
                    }
                    tpAmt = tpAmt+pendingTpAmt;
                    netPayable = tpAmt;
            		logger.info("getAllDeductions()========TP NET=========="+netPayable);
                }
                else if("harvchgdtlsbyryotcontanddate".equalsIgnoreCase(tablename))
                {
                	//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingHarAmt = 0.0;
                    String harDed = "select * from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=4 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotCode+"' and DedCode=4 )";
                    List PendingHarList=hibernateDao.findBySqlCriteria(harDed);
                    if(PendingHarList != null && PendingHarList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingHarList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingHarAmt = (Double) pendingMap.get("pendingamt");
                        	pendingHarAmt = agricultureHarvestingController.round(pendingHarAmt);
                    	}
                    }
                    harvestingAmt = harvestingAmt+pendingHarAmt;
                    netPayable = harvestingAmt;
            		logger.info("getAllDeductions()========HARVESTING NET=========="+netPayable);
                }
                else
                {
        			double pendingOtherAmt = 0.0;
                	 String harDed = "select * from  DedDetails where ryotcode='"+ryotCode+"' and DedCode="+stdDedCode+" and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotCode+"' and DedCode="+stdDedCode+" )";
                     List PendingHarList=hibernateDao.findBySqlCriteria(harDed);
                     if(PendingHarList != null && PendingHarList.size()>0)
                     {
                     	Map pendingMap = new HashMap();
                     	pendingMap = (Map) PendingHarList.get(0);
                     	if((Double) pendingMap.get("pendingamt") != null)
                     	{
                         	pendingOtherAmt = (Double) pendingMap.get("pendingamt");
                         	pendingOtherAmt = agricultureHarvestingController.round(pendingOtherAmt);
                     	}
                     }
                     netPayable = amount*caneSupplied;
                     netPayable = agricultureHarvestingController.round(netPayable);
                     netPayable = netPayable+pendingOtherAmt;
             		logger.info("getAllDeductions()========OTHER DED. NET=========="+netPayable);
                }
                double val = 0.0;
                
                Map finalMap = new HashMap();
                if("transpchgsmrybyryotcontanddate".equalsIgnoreCase(tablename))
                {
                    finalMap.put("harvestcontcode", tcContCode);
                }
                else if("harvchgdtlsbyryotcontanddate".equalsIgnoreCase(tablename))
                {
                    finalMap.put("harvestcontcode", hcContCode);
                }
                else
                {
                    finalMap.put("harvestcontcode", 0);
                }

                finalMap.put("id", id);
                finalMap.put("amount", amount);
                finalMap.put("amountValidity", amountValidity);
                finalMap.put("deductionOrder", deductionOrder);
                finalMap.put("name", name);
                finalMap.put("status", status);
                finalMap.put("stdDedCode", stdDedCode);
                finalMap.put("columnname", columnname);
                finalMap.put("tablename", tablename);
                finalMap.put("canesupplied", caneSupplied);
                finalMap.put("caneacslno", caneAccSlNo);
                finalMap.put("rate", harvestingRate);
                finalMap.put("netPayable", netPayable);
                finalMap.put("amount", netPayable);
                finalMap.put("newPayable", val);

                FinalList.add(finalMap);
        	}
        }
 		logger.info("getAllDeductions()====== Method Passed Successfully");
        
        if(FinalList.size()>0 && FinalList!=null)
        	return FinalList;
        else
        	return null; 
    }
	
	public List getHarvestingRatesDetails(String season,String ryotcode)
    {
        String sql = "select * from HarvestingRateDetails where season ='" + season+"' and ryotcode='" + ryotcode+"'";
        List harvestingRateDetailsList=hibernateDao.findByNativeSql(sql, HarvestingRateDetails.class);
        if(harvestingRateDetailsList.size()>0 && harvestingRateDetailsList!=null)
        	return harvestingRateDetailsList;
        else
        	return null;         
    }
	
	public List getNewAmount(String season,String ryotcode,Date fromdate,Date todate)
    {
		List newAmtList=new ArrayList();
		List FinalList=new ArrayList();
		
        String sql = "Select * from StandardDeductions where status=0 order by deductionOrder";
        newAmtList=hibernateDao.findBySqlCriteria(sql);
        if(newAmtList != null && newAmtList.size()>0)
        {
        	for(int i=0;i<newAmtList.size();i++)
        	{
    			logger.info("getNewAmount()========i=========="+i);

        		Map tempMap = new HashMap();
        		tempMap = (Map) newAmtList.get(i);
        		
        		String dedName = (String) tempMap.get("name");
        		String tableName = (String) tempMap.get("tablename");
        		String colName = (String) tempMap.get("columnname");
        		int dedCode = (Integer) tempMap.get("stdDedCode");

        		String Name = "";
        		//receiptdate
        		String dateColumn = "receiptdate";
        		if("transpchgsmrybyryotcontanddate".equalsIgnoreCase(tableName))
        		{
        			dateColumn = "receiptdate";
        			Name = "TP";
        		}
        		else if("harvchgdtlsbyryotcontanddate".equalsIgnoreCase(tableName))
        		{
        			dateColumn = "receiptdate";
        			Name = "HAR";

        		}
        		else if("cdcdetailsbyrotanddate".equalsIgnoreCase(tableName))
        		{
        			dateColumn = "receiptdate";
        			Name = "CDC";
        		}
        		else if("ucdetailsbyrotanddate".equalsIgnoreCase(tableName))
        		{
        			dateColumn = "receiptdate";
        			Name = "UC";
        		}
        		
        		
        		double Amt = 0.0;
        		if("transpchgsmrybyryotcontanddate".equalsIgnoreCase(tableName) || "harvchgdtlsbyryotcontanddate".equalsIgnoreCase(tableName) || "cdcdetailsbyrotanddate".equalsIgnoreCase(tableName) || "ucdetailsbyrotanddate".equalsIgnoreCase(tableName))
        		{
				//added by Amurty 21-01-2017
        			String sql1 = "select "+colName+" from "+tableName+" where ryotcode='"+ryotcode+"' and season='"+season+"' and "+dateColumn+" between '"+fromdate+"' and '"+todate+"' and castatus != 1";
        			logger.info("getNewAmount()========sql1=========="+sql1);
        			List AmtList=hibernateDao.findBySqlCriteria(sql1);
        			logger.info("getNewAmount()========AmtList=========="+AmtList);
                    if(AmtList != null && AmtList.size()>0)
                    {
                    	Map AmtMap = new HashMap();
                    	AmtMap = (Map) AmtList.get(0);
                    	if((Double) AmtMap.get(colName) != null)
                    	{
                        	Amt = (Double) AmtMap.get(colName);
                        	Amt = agricultureHarvestingController.round(Amt);
                    	}
                    }
        		}
                double extent = 0.0;
                String sql2 = "select totalextent from  transpchgsmrybyryotcontanddate where ryotcode='"+ryotcode+"'";
                List ExtentList=hibernateDao.findBySqlCriteria(sql2);
                if(ExtentList != null && ExtentList.size()>0)
                {
                	Map extentMap = new HashMap();
                	extentMap = (Map) ExtentList.get(0);
                	if((Double) extentMap.get("totalextent") != null)
                	{
                    	extent = (Double) extentMap.get("totalextent");
                	}
        			logger.info("getNewAmount()========extent=========="+extent);
                }
                Map FinalMap = new HashMap();
                
    			logger.info("getNewAmount()========tableName=========="+tableName);

                if("transpchgsmrybyryotcontanddate".equalsIgnoreCase(tableName))
        		{	
                	//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
                	double pendingTpAmt = 0.0;
                    String tpDed = "select * from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=3 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=3 )";
                    List PendingTpList=hibernateDao.findBySqlCriteria(tpDed);
        			logger.info("getNewAmount()========tpDed=========="+tpDed);
        			logger.info("getNewAmount()========PendingTpList=========="+PendingTpList);
                    if(PendingTpList != null && PendingTpList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingTpList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingTpAmt = (Double) pendingMap.get("pendingamt");
                        	pendingTpAmt = agricultureHarvestingController.round(pendingTpAmt);
                    	}
                    }
                    Amt = Amt+pendingTpAmt;
                    
                	FinalMap.put("tcamount",Amt);
                	FinalMap.put("cdcamount","0.00");
                	FinalMap.put("icpamount","0.00");
                	FinalMap.put("hcamount","0.00");
                	FinalMap.put("ucamount","0.00");
                	FinalMap.put("wextent",extent);
        		}
        		else if("harvchgdtlsbyryotcontanddate".equalsIgnoreCase(tableName))
        		{
        			//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingHarAmt = 0.0;
                    String harDed = "select * from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=4 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=4 )";
                    List PendingHarList=hibernateDao.findBySqlCriteria(harDed);
                    logger.info("getNewAmount()========harDed=========="+harDed);
        			logger.info("getNewAmount()========PendingHarList=========="+PendingHarList);
                    if(PendingHarList != null && PendingHarList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingHarList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingHarAmt = (Double) pendingMap.get("pendingamt");
                        	pendingHarAmt = agricultureHarvestingController.round(pendingHarAmt);
                    	}
                    }
                    Amt = Amt+pendingHarAmt;
        			FinalMap.put("tcamount","0.00");
                	FinalMap.put("cdcamount","0.00");
                	FinalMap.put("icpamount","0.00");
                	FinalMap.put("hcamount",Amt);
                	FinalMap.put("ucamount","0.00");
                	FinalMap.put("wextent",extent);

        		}
        		else if("cdcdetailsbyrotanddate".equalsIgnoreCase(tableName))
        		{
        			//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingCdcAmt = 0.0;
                    String cdcDed = "select * from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=1 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=1 )";
                    List PendingCdcList=hibernateDao.findBySqlCriteria(cdcDed);
                    logger.info("getNewAmount()========cdcDed=========="+cdcDed);
        			logger.info("getNewAmount()========PendingCdcList=========="+PendingCdcList);
                    if(PendingCdcList != null && PendingCdcList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingCdcList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingCdcAmt = (Double) pendingMap.get("pendingamt");
                        	pendingCdcAmt = agricultureHarvestingController.round(pendingCdcAmt);
                    	}
                    }
                    Amt = Amt+pendingCdcAmt;
        			FinalMap.put("tcamount","0.00");
                	FinalMap.put("cdcamount",Amt);
                	FinalMap.put("icpamount","0.00");
                	FinalMap.put("hcamount","0.00");
                	FinalMap.put("ucamount","0.00");
                	FinalMap.put("wextent",extent);
        		}
        		else if("ucdetailsbyrotanddate".equalsIgnoreCase(tableName))
        		{
        			//Added by DMurty on 01-11-2016 because we need to consider pending Amt for previous caneslno.
        			double pendingUlAmt = 0.0;
                    String ulDed = "select * from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=2 and caneacslno=(Select max(caneacslno) from  DedDetails where ryotcode='"+ryotcode+"' and DedCode=2 )";
                    List PendingUlList=hibernateDao.findBySqlCriteria(ulDed);
                    logger.info("getNewAmount()========ulDed=========="+ulDed);
        			logger.info("getNewAmount()========PendingUlList=========="+PendingUlList);
                    if(PendingUlList != null && PendingUlList.size()>0)
                    {
                    	Map pendingMap = new HashMap();
                    	pendingMap = (Map) PendingUlList.get(0);
                    	if((Double) pendingMap.get("pendingamt") != null)
                    	{
                        	pendingUlAmt = (Double) pendingMap.get("pendingamt");
                        	pendingUlAmt = agricultureHarvestingController.round(pendingUlAmt);
                    	}
                    }
                    Amt = Amt+pendingUlAmt;
                    
        			FinalMap.put("tcamount","0.00");
                	FinalMap.put("cdcamount","0.00");
                	FinalMap.put("icpamount","0.00");
                	FinalMap.put("hcamount","0.00");
                	FinalMap.put("ucamount",Amt);
                	FinalMap.put("wextent",extent);
        		}
        		else
        		{
        			 double otherded = 0.0;
                     sql2 = "select * from  StandardDeductions where stdDedCode="+dedCode;
                     List DedDtls=hibernateDao.findBySqlCriteria(sql2);
                     logger.info("getNewAmount()========sql2=========="+sql2);
         			logger.info("getNewAmount()========DedDtls=========="+DedDtls);
                     if(DedDtls != null && DedDtls.size()>0)
                     {
                     	Map dedMap = new HashMap();
                     	dedMap = (Map) DedDtls.get(0);
                     	if((Double) dedMap.get("amount") != null)
                     	{
                         	otherded = (Double) dedMap.get("amount");
                         	otherded = agricultureHarvestingController.round(otherded);
                     	}
                     }
                     Amt = otherded*extent;
                     Amt = agricultureHarvestingController.round(Amt);
        			FinalMap.put("tcamount","0.00");
                	FinalMap.put("cdcamount","0.00");
                	FinalMap.put("icpamount","0.00");
                	FinalMap.put("hcamount","0.00");
                	FinalMap.put("ucamount",Amt);
                	FinalMap.put("wextent",extent);
        		}
                FinalList.add(FinalMap);
    			logger.info("getNewAmount()========FinalList=========="+FinalList);
        	}
        }
        return FinalList;         
    }
	
	public List getPendingAmount(String season,String ryotcode)
    {
        String sql = "select distinct a.pendingpayable ,b.pendingpayable from harvchgsmrybyryotandcont a,transpchgsmrybyryotandcont b where a.ryotcode='" + ryotcode+"' and a.season='" + season+"' and b.ryotcode='" + ryotcode+"' and b.season='" + season+"'";
        List pendAmtList=hibernateDao.findBySqlCriteria(sql);
        if(pendAmtList.size()>0 && pendAmtList!=null)
        	return pendAmtList;
        else
        	return null;         
    }
	
	public List getAllAdvances(Date fromdate,Date todate,String ryotCode,String season)
    {
		//Modified by DMurty on 17-12-2016
		//String sql = "select a.advancecode,a.advance,a.glcode,a.advorder from companyadvance a,AdvancePrincipleAmounts c where a.advancecode=c.advancecode and c.principle>0 and c.ryotcode='"+ryotCode+"' and a.status=0 order by a.advorder";
		//Modified by DMurty on 07-03-2017
		String sql = "select a.advancecode,a.advance,a.glcode,a.advorder from companyadvance a,AdvancePrincipleAmounts c where a.advancecode=c.advancecode and c.principle>0 and c.ryotcode='"+ryotCode+"' and season='"+season+"' and a.status=0 order by a.advorder";
		List advancesList=hibernateDao.findBySqlCriteria(sql);
        if(advancesList.size()>0 && advancesList!=null)
        	return advancesList;
        else
        	return null;
    }
	
	public List getLoanDetails(String season,String ryotcode)
    {
		String sql = "select loannumber,addedloan,disbursedamount,disburseddate,interstrate,branchcode,referencenumber from loandetails  where ryotcode='" + ryotcode+"' and season='" + season+"' and disburseddate IS NOT NULL";
        List loanList=hibernateDao.findBySqlCriteria(sql);
        if(loanList.size()>0 && loanList!=null)
        	return loanList;
        else
        	return null; 
    }
	
	public List getAdvanceCalcDetails(String sql)
    {
        List pendAmtList=hibernateDao.findBySqlCriteria(sql);
        if(pendAmtList.size()>0 && pendAmtList!=null)
        	return pendAmtList;
        else
        	return null;         
    }
	
	public List getAllAdvancePayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {
		String sql = "select distinct a.advancecode,a.payableamount,a.ryotcode,b.dateofupdate from advancepaymentsummary a,advancepaymentdetails b where a.advancecode=b.advancecode and a.ryotcode=b.ryotcode";
        List advanceList=hibernateDao.findBySqlCriteria(sql);
        if(advanceList.size()>0 && advanceList!=null)
        	return advanceList;
        else
        	return null;         
    }
	
	public List getAllLoanPayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {
		String sql = "select distinct a.payableamount,a.ryotcode,b.dateofupdate from loanpaymentsummary a,loanpaymentdetails b where a.season=b.season and a.ryotcode=b.ryotcode";
        List loansList=hibernateDao.findBySqlCriteria(sql);
        if(loansList.size()>0 && loansList!=null)
        	return loansList;
        else
        	return null;         
    }
	
	public List getAllHarvesterPayableDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {
		String sql = "select distinct harvestcontcode,pendingpayable,ryotcode from harvchgsmrybyryotandcont";
        List loansList=hibernateDao.findBySqlCriteria(sql);
        if(loansList.size()>0 && loansList!=null)
        	return loansList;
        else
        	return null;         
    }
	
	public List getAllTransporterDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {
		String sql = "select distinct transportcontcode,pendingpayable,ryotcode from transpchgsmrybyryotandcont";
        List loansList=hibernateDao.findBySqlCriteria(sql);
        if(loansList.size()>0 && loansList!=null)
        	return loansList;
        else
        	return null;         
    }
	
	public List getAllSBAccountDetailsForUpdateRyotAccounts(Integer villageCode,Date caneaccdate)
    {
		String sql = "select distinct amountpayable,sbcode,ryotcode from SBAccSmryByRyot;";
        List sbaccList=hibernateDao.findBySqlCriteria(sql);
        if(sbaccList.size()>0 && sbaccList!=null)
        	return sbaccList;
        else
        	return null;         
    }
	
	public List getAdvancePaymentSummary(String season,String ryotCode,Integer advCode)
    {
        String sql = "select * from AdvancePaymentSummary where ryotcode='" + ryotCode+"' and season='" + season+"' and advancecode="+advCode+"";
        
        List advancePaymentSummaryList=hibernateDao.findByNativeSql(sql, AdvancePaymentSummary.class);
        if(advancePaymentSummaryList.size()>0 && advancePaymentSummaryList!=null)
        	return advancePaymentSummaryList;
        else
        	return null;         
    }
	
	public List getAdvanceSummary(String season,String ryotCode,Integer advCode)
    {
        String sql = "select * from AdvanceSummary where ryotcode='" + ryotCode+"' and season='" + season+"' and advancecode="+advCode+"";
        
        List advanceSummaryList=hibernateDao.findByNativeSql(sql, AdvanceSummary.class);
        if(advanceSummaryList.size()>0 && advanceSummaryList!=null)
        	return advanceSummaryList;
        else
        	return null;         
    }
	
	public List getLoanPaymentSummary(String season,String ryotCode)
    {
        String sql = "select * from LoanPaymentSummary where ryotcode='" + ryotCode+"' and season='" + season+"'";
        
        List loanPaymentSummaryList=hibernateDao.findByNativeSql(sql, LoanPaymentSummary.class);
        if(loanPaymentSummaryList.size()>0 && loanPaymentSummaryList!=null)
        	return loanPaymentSummaryList;
        else
        	return null;         
    }
	
	public List getLoanSummary(String season,String ryotCode,Integer loannumber)
    {
        String sql = "select * from LoanSummary where ryotcode='" + ryotCode+"' and season='" + season+"' and loannumber="+loannumber+"";
        
        List loanSummaryList=hibernateDao.findByNativeSql(sql, LoanSummary.class);
        if(loanSummaryList.size()>0 && loanSummaryList!=null)
        	return loanSummaryList;
        else
        	return null;         
    }
	public List getLoanPaymentSummaryByBank(String season)
    {
        String sql = "select * from LoanPaymentSummaryByBank where  season='" + season+"'";
        
        List loanPaymentSummaryByBankList=hibernateDao.findByNativeSql(sql, LoanPaymentSummaryByBank.class);
        if(loanPaymentSummaryByBankList.size()>0 && loanPaymentSummaryByBankList!=null)
        	return loanPaymentSummaryByBankList;
        else
        	return null;         
    }
	
	public List getSBAccSmryByBank(String season,Integer bankcode)
    {
        String sql = "select * from SBAccSmryByBank where  season='" + season+"' and bankcode="+bankcode+"";
        
        List sBAccSmryByBankList=hibernateDao.findByNativeSql(sql, SBAccSmryByBank.class);
        if(sBAccSmryByBankList.size()>0 && sBAccSmryByBankList!=null)
        	return sBAccSmryByBankList;
        else
        	return null;         
    }
	
	public List getSBAccSmryByRyot(String season,String ryotCode)
    {
        String sql = "select * from SBAccSmryByRyot where  season='" + season+"' and ryotcode='" + ryotCode+"'";
        
        List sBAccSmryByRyotList=hibernateDao.findByNativeSql(sql, SBAccSmryByRyot.class);
        if(sBAccSmryByRyotList.size()>0 && sBAccSmryByRyotList!=null)
        	return sBAccSmryByRyotList;
        else
        	return null;         
    }
	public List getSBAccByRyot(String ryotCode)
    {
        String sql = "select * from RyotBankDetails where  ryotcode='" + ryotCode+"'";
        List sBAccByRyotList=hibernateDao.findByNativeSql(sql, RyotBankDetails.class);
        if(sBAccByRyotList.size()>0 && sBAccByRyotList!=null)
        	return sBAccByRyotList;
        else
        	return null;         
    }
	
	
	public List getOtherDedSmryByRyot(String season,String ryotCode)
    {
        String sql = "select * from OtherDedSmryByRyot where  season='" + season+"' and ryotcode='" + ryotCode+"'";
        
        List otherDedSmryByRyotList=hibernateDao.findByNativeSql(sql, OtherDedSmryByRyot.class);
        if(otherDedSmryByRyotList.size()>0 && otherDedSmryByRyotList!=null)
        	return otherDedSmryByRyotList;
        else
        	return null;         
    }
	
	
	public List getHarvChgSmryByRyotAndCont(String season,String ryotCode,Integer harvestcontcode)
    {
        String sql = "select * from HarvChgSmryByRyotAndCont where season ='" + season+"' and ryotcode='" + ryotCode+"' and harvestcontcode="+harvestcontcode+"";
        
        List harvChgSmryByRyotAndContList=hibernateDao.findByNativeSql(sql, HarvChgSmryByRyotAndCont.class);
        
        if(harvChgSmryByRyotAndContList.size()>0 && harvChgSmryByRyotAndContList!=null)
        	return harvChgSmryByRyotAndContList;
        else
        	return null;         
    }
	
		public List getHarvChgSmryBySeasonByCont(String season,Integer harvestcontcode)
    {
        String sql = "select * from HarvChgSmryBySeasonByCont where season ='" + season+"' and harvestcontcode="+harvestcontcode+"";
        
        List harvChgSmryBySeasonByContList=hibernateDao.findByNativeSql(sql, HarvChgSmryBySeasonByCont.class);
        
        if(harvChgSmryBySeasonByContList.size()>0 && harvChgSmryBySeasonByContList!=null)
        	return harvChgSmryBySeasonByContList;
        else
        	return null;         
    }
	public List getHarvChgSmryBySeason(String season)
    {
        String sql = "select * from HarvChgSmryBySeason where season ='" + season+"'";
        
        List harvChgSmryBySeasonList=hibernateDao.findByNativeSql(sql, HarvChgSmryBySeason.class);
        
        if(harvChgSmryBySeasonList.size()>0 && harvChgSmryBySeasonList!=null)
        	return harvChgSmryBySeasonList;
        else
        	return null;         
    }
	
	public List getTranspChgSmryBySeasonByCont(String season,Integer transportcontcode)
    {
        String sql = "select * from TranspChgSmryBySeasonByCont  where season ='" + season+"' and transportcontcode="+transportcontcode+"";
        
        List transpChgSmryBySeasonByContList=hibernateDao.findByNativeSql(sql, TranspChgSmryBySeasonByCont.class);
        
        if(transpChgSmryBySeasonByContList.size()>0 && transpChgSmryBySeasonByContList!=null)
        	return transpChgSmryBySeasonByContList;
        else
        	return null;         
    }
	public List getTranspChgSmryBySeason(String season)
    {
        String sql = "select * from TranspChgSmryBySeason where  season ='" + season+"'";
        
        List transpChgSmryBySeasonList=hibernateDao.findByNativeSql(sql, TranspChgSmryBySeason.class);
        
        if(transpChgSmryBySeasonList.size()>0 && transpChgSmryBySeasonList!=null)
        	return transpChgSmryBySeasonList;
        else
        	return null;         
    }
	
	public List getTranspChgSmryByRyotAndCont(String season,String ryotCode,Integer transportcontcode)
    {
        String sql = "select * from TranspChgSmryByRyotAndCont where season ='" + season+"' and ryotcode='" + ryotCode+"' and transportcontcode="+transportcontcode+"";
        
        List transpChgSmryByRyotAndContList=hibernateDao.findByNativeSql(sql, TranspChgSmryByRyotAndCont.class);
        
        if(transpChgSmryByRyotAndContList.size()>0 && transpChgSmryByRyotAndContList!=null)
        	return transpChgSmryByRyotAndContList;
        else
        	return null;         
    }
		
	public List getRyotLedgerForSeason(String season,String ryotcode,Date date1,Date date2)
    {
		//String sql = "select transactiondate,journalmemo,dr,cr,runningbalance from AccountDetails where accountcode='" + ryotcode+"' and transactiondate between '" + date1+"' and '" + date2+"'";
		String sql = null;//"select transactiondate,journalmemo,dr,cr,runningbalance from AccountDetails_temp where accountcode='" + ryotcode+"' and transactiondate between '" + date1+"' and '" + date2+"'";
		//if("2015-2016".equals(season))
		//{
		//	sql = "select transactiondate,journalmemo,dr,cr,runningbalance from AccountDetails_temp where accountcode='" + ryotcode+"' and transactiondate between '" + date1+"' and '" + date2+"'";
		//}
		//else
		//{
			sql = "select transactiondate,journalmemo,dr,cr,runningbalance from AccountDetails where accountcode='" + ryotcode+"' and season = '"+season+"' and status!=0 order by transactiondate";//transactiondate between '" + date1+"' and '" + date2+"'";
		//}
        List ryotLedgerList=hibernateDao.findBySqlCriteria(sql);
        if(ryotLedgerList.size()>0 && ryotLedgerList!=null)
        	return ryotLedgerList;
        else
        	return null;         
    }
	
	public List getICPDetails()
    {
		String	sql = "select * from ICPDetailsByRyot";
        List ICPDetailsByRyotList=hibernateDao.findBySqlCriteria(sql);
        if(ICPDetailsByRyotList.size()>0 && ICPDetailsByRyotList!=null)
        	return ICPDetailsByRyotList;
        else
        	return null;         
    }
	public List getAccMasterData(String ryotCode)
    {
		String	sql = "select * from AccountMaster where accountcode='"+ryotCode+"'";
        List AccMasterList=hibernateDao.findBySqlCriteria(sql);
        if(AccMasterList.size()>0 && AccMasterList!=null)
        	return AccMasterList;
        else
        	return null;         
    }
	
	
	public List getTrialBalForSeason(String season)
    {
		//String sql = "select * from AccountSummary_temp order by ryotname";
		//Modified by DMurty on 27-08-2016
		String sql = "select a.*,b.ryotname from AccountSummary a,ryot b where a.accountcode=b.ryotcode order by b.ryotname";
	
        List ryotLedgerList=hibernateDao.findBySqlCriteria(sql);
        if(ryotLedgerList.size()>0 && ryotLedgerList!=null)
        	return ryotLedgerList;
        else
        	return null;   
    }
	
	public void deleteCaneAcctAmounts(int calcid,String season)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM CaneAcctAmounts WHERE calcid = "+calcid+" and season='"+season+"'").executeUpdate();
		logger.info("========After Delete==========");
	}
	
	//Added by DMurty on 17-08-2016
	public List<CaneAcctRules> getCaneAccRulesForCalcNew(String season,Date date1,Date date2) 
	{
		//String sqlString="select *  from CaneAcctRules where season='"+season+"' and date=(select max(date) from CaneAccountingRules)";
        String sqlString = "select * from CaneAcctRules where season ='"+season+"' and calcruledate = (SELECT max(calcruledate) FROM CaneAcctRules where season ='"+season+"')";
		List<CaneAcctRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAcctRules.class);
		return caneAccountingRules;
	}
	
		public List getSeedSupplierRyots(String season,String villageCode)
    {
		//String sql = "select * from SeedSuppliersSummary where pendingamount>0 and season='"+season+"' order by seedsuppliercode";
		List ryotrList=null;
		String sql = "select distinct ryotcode from AgreementDetails where villagecode='"+villageCode+"' and seasonyear='"+season+"' and commercialOrSeed=1 order by ryotcode";
        try
        {
		ryotrList=hibernateDao.findBySqlCriteria(sql);
        if(ryotrList.size()>0 && ryotrList!=null)
        	return ryotrList;
        }
        catch(Exception e)
        {
        	logger.info("========while getting ryots from agreementdetails who ever taking advances for seeds supply=========="+e);
        }
        return ryotrList;
    }
	
	
	public String getRyotName(String ryotcode)
    {
	
		String ryotname=null;
		Session ss=hibernateDao.getSessionFactory().openSession();  
		List<String> ryot=null;
		
		try
		{
		SQLQuery query =ss.createSQLQuery("select  ryotname from Ryot where ryotcode='"+ryotcode+"'");
		ryot=query.list();
		if(ryot.size()>0 && ryot != null)
		{
			ryotname=(String)ryot.get(0);
		}
		return ryotname;
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
			return ryotname;	
		}
	
    }
	public List getSeedAccounting(Integer seedactno)
    {
		String sql = "select * from SeedAccountingSummary where seedactno="+seedactno;
        List supplierList=hibernateDao.findBySqlCriteria(sql);
        if(supplierList.size()>0 && supplierList!=null)
        	return supplierList;
        else
        	return null;   
        
    }
	public List getRyotPrincipleAmountsList(String season,String ryotcode,String tablename)
	 {
		List supplierList=null;
		String sql = "select * from "+tablename+" where ryotcode='"+ryotcode+"' and season='"+season+"' and principle>0";
       try{
    	   supplierList=hibernateDao.findBySqlCriteria(sql);
		  if(supplierList.size()>0 && supplierList!=null)
		       	return supplierList;
		       else
		       	return null; 
       }
       catch(Exception e)
       {
    	   System.out.println("The exception is"+e);
    	   return null;
       }
    }
	
	//post season cane accounting calculation
	public List getDueCaneSupplierRyots(String season,String villageCode)
    {
		try
		{
		String sql = "select * from AccruedAndDueCaneValueSummary where dueamount>0 and season='"+season+"' and villagecode='"+villageCode+"' order by ryotcode";
		List supplierList=hibernateDao.findBySqlCriteria(sql);
        if(supplierList.size()>0 && supplierList!=null)
        	return supplierList;
        else
        	return null;   
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
			return null;
		}
    }
	public List getSeedSuppliersSummaryDetails(String season,String ryotCode)
    {
		List advanceSummaryList=null;
		String sql = "select * from SeedSuppliersSummary where seedsuppliercode='"+ryotCode+"' and season='"+season+"'";
        try
        {
        advanceSummaryList=hibernateDao.findByNativeSql(sql, SeedSuppliersSummary.class);
        if(advanceSummaryList.size()>0 && advanceSummaryList!=null)
        	return advanceSummaryList;
        }
        catch(Exception e)
        {
        	logger.info("========Before Delete=========="+e);
        	return null;
        }
        return advanceSummaryList;
    }
	public List getadvancePrincipleAmountsDetails(String season,String ryotCode,Integer advcode)
	{
	    
	        String sql = "select * from AdvancePrincipleAmounts where ryotcode='" + ryotCode+"' and season='" + season+"' and advancecode="+advcode;
	        
	        List advanceSummaryList=hibernateDao.findByNativeSql(sql, AdvancePrincipleAmounts.class);
	        if(advanceSummaryList.size()>0 && advanceSummaryList!=null)
	        	return advanceSummaryList;
	        else
	        	return null;         
	    
	}
	public List getLoanPrincipleAmountsDetails(String season,String ryotCode,String accnum)
	{
	    
        String sql = "select * from LoanPrincipleAmounts where ryotcode='" + ryotCode+"' and season='" + season+"' and loanaccountnumber='"+accnum+"'";
        
        List advanceSummaryList=hibernateDao.findByNativeSql(sql, LoanPrincipleAmounts.class);
        if(advanceSummaryList.size()>0 && advanceSummaryList!=null)
        	return advanceSummaryList;
        else
        	return null;         
    
}
	public List getLoanDetailsForSeedAccounting(String season,String ryotCode,String accnum)
	{
		List supplierList=null;
        try
		{
    	String sql = "select * from LoanDetails where ryotcode='" + ryotCode+"' and season='" + season+"' and referencenumber="+accnum;
    	supplierList=hibernateDao.findBySqlCriteria(sql);
        if(supplierList.size()>0 && supplierList!=null)
        	return supplierList;
        else
        	return null;   
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
			return null;
		}
	}
	
	public List getLoanSummaryForSeeAcc(String season,String ryotCode,Integer branchCode,Integer loannumber)
    {
        String sql = "select * from LoanSummary where ryotcode='" + ryotCode+"' and season='" + season+"' and loannumber="+loannumber+" and branchcode="+branchCode;
        
        List loanSummaryList=hibernateDao.findByNativeSql(sql, LoanSummary.class);
        if(loanSummaryList.size()>0 && loanSummaryList!=null)
        	return loanSummaryList;
        else
        	return null;         
    }
	public List getAccAndDueCaneValueSummaryDetailsForUpdate(String season,String ryotcode,String villageCode)
	{
		List accAndDueCaneDetailsList=null;
        try
		{
        String sql = "select * from AccruedAndDueCaneValueSummary where season='"+season+"' and villagecode='"+villageCode+"' and ryotcode='"+ryotcode+"'";
    	accAndDueCaneDetailsList=hibernateDao.findByNativeSql(sql, AccruedAndDueCaneValueSummary.class);
        if(accAndDueCaneDetailsList.size()>0 && accAndDueCaneDetailsList!=null)
        	return accAndDueCaneDetailsList;
        else
        	return null;   
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
			return null;
		}
		
	}
	
	//Added by DMurty on 17-10-2016
	public List getCaneWeightFromTranspChgSmryByRyotAndCont(String ryotCode,String season)
    {
        String sql = "select distinct transportedcanewt from TranspChgSmryByRyotAndCont where ryotcode='"+ryotCode+"' and season='"+season+"' group by ryotcode,transportedcanewt ";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySql(sql);
    }
	//Added by DMurty on 24-10-2016
	public List getOldPendingAmtList(String ryotCode,String season,int dedCode)
    {
		String qry = "Select * from DedDetails where ryotcode='"+ryotCode+"' and season='"+season+"' and DedCode="+dedCode+" and caneacslno=(select max(caneacslno) from DedDetails where ryotcode='"+ryotCode+"' and season='"+season+"' and DedCode="+dedCode+")";
        List OldPendingAmtList=hibernateDao.findBySqlCriteria(qry);
        if(OldPendingAmtList.size()>0 && OldPendingAmtList!=null)
        	return OldPendingAmtList;
        else
        	return null;
    }
	//Added by DMurty on 25-10-2016
	public List getDedDtlsFromTemp(int caneAccSlNo,String season,String ryotCode)
    {
        String sql = "select * from DedDetails_temp where season='" + season+"' and ryotcode='" + ryotCode+"' and caneacslno="+caneAccSlNo+"";
        List DedDetails_tempList=hibernateDao.findBySqlCriteria(sql);
        if(DedDetails_tempList.size()>0 && DedDetails_tempList!=null)
        	return DedDetails_tempList;
        else
        	return null;         
    }
	//Added by DMurty on 26-10-2016
	public double getdedSbAmt(int caneAccSlNo,String season,String ryotCode)
    {
		double dedSbAmt = 0.0;
        String sql = "select * from CaneValueSplitupTemp where season='" + season+"' and ryotcode='" + ryotCode+"' and caneacslno="+caneAccSlNo+"";
        List dedSbAmtList=hibernateDao.findBySqlCriteria(sql);
        if(dedSbAmtList.size()>0 && dedSbAmtList!=null)
        {
        	Map tempMap = new HashMap();
        	tempMap = (Map) dedSbAmtList.get(0);
        	dedSbAmt = (Double) tempMap.get("forsbacs");
        }
        return dedSbAmt;
    }
	
	public List getAccountCode(int contractorCode,String table,String column)
    {
        String sql = "select * from "+table+" where "+column+"="+contractorCode+"";
        List detailsList=hibernateDao.findBySqlCriteria(sql);
        if(detailsList.size()>0 && detailsList!=null)
        	return detailsList;
        else
        	return null;    
    }
	
	public List getLoanDetails(int loanNo,String season,String ryotCode)
    {
        String sql = "select * from LoanDetails where loannumber="+loanNo+" and season='"+season+"' and ryotcode='"+ryotCode+"'";
        List loanList=hibernateDao.findBySqlCriteria(sql);
        if(loanList.size()>0 && loanList!=null)
        	return loanList;
        else
        	return null;    
    }
	public List getCaneAccountSmryDetails(int caneacslno,String season)
    {
        String sql = "select * from CaneAccountSmryTemp where caneacslno="+caneacslno+" and season='"+season+"'";
        List loanList=hibernateDao.findBySqlCriteria(sql);
        if(loanList.size()>0 && loanList!=null)
        	return loanList;
        else
        	return null;    
    }
	
	//Added by DMurty on 14-11-2016
	public List<CaneAcctRules> getCaneAccRulesForCalcForCaneAccounting(String season,Date date1,Date date2) 
	{
        //String sqlString = "select * from CaneAcctRules where season ='"+season+"' and calcruledate <= '"+date1+"' and calcruledate = (SELECT max(calcruledate) FROM CaneAcctRules where season ='"+season+"' )";
        String sqlString = "select * from CaneAcctRules where season ='"+season+"' and calcruledate = (SELECT max(calcruledate) FROM CaneAcctRules where season ='"+season+"' and calcruledate <= '"+date1+"')";
		List<CaneAcctRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAcctRules.class);
		return caneAccountingRules;
	}
	
	//Added by DMurty on 14-11-2016
	public List getTpDedDtls(String ryotCode,String season,int harvestContCode)
    {
        String sql = "select * from TranspChgSmryByRyotAndCont where season='" + season+"' and ryotcode='" + ryotCode+"' and transportcontcode="+harvestContCode;
        List TpList=hibernateDao.findBySqlCriteria(sql);
        if(TpList.size()>0 && TpList!=null)
        	return TpList;
        else
        	return null;         
    }
	
	public List getHarvestinChargesDtls(String ryotCode,String season,int harvestContCode)
    {
        String sql = "select * from HarvChgSmryByRyotAndCont where season='" + season+"' and ryotcode='" + ryotCode+"' and harvestcontcode="+harvestContCode;
        List HarvestList=hibernateDao.findBySqlCriteria(sql);
        if(HarvestList.size()>0 && HarvestList!=null)
        	return HarvestList;
        else
        	return null;         
    }
	//Added by DMurty on 15-11-2016
	public List CaneAccountSmryTemp(int caneAccSlNo,String season)
    {
        String sql = "select * from CaneAccountSmrytemp where season='" + season+"' and caneacslno="+caneAccSlNo+"";
        List CaneAccountSmryTempList=hibernateDao.findBySqlCriteria(sql);
        if(CaneAccountSmryTempList.size()>0 && CaneAccountSmryTempList!=null)
        	return CaneAccountSmryTempList;
        else
        	return null;         
    }
	
	public List CaneValueSplitupTemp(int caneAccSlNo,String season,String ryotCode)
    {
        String sql = "select * from CaneValueSplitupTemp where season='" + season+"' and caneacslno="+caneAccSlNo+" and ryotcode='"+ryotCode+"'";
        List CaneValueSplitupTempList=hibernateDao.findBySqlCriteria(sql);
        if(CaneValueSplitupTempList.size()>0 && CaneValueSplitupTempList!=null)
        	return CaneValueSplitupTempList;
        else
        	return null;         
    }
	//Added by DMurty on 16-11-2016
	public void TruncateTempTable(String model,String season,int caneacslno,String user) 
	{
		String qry = "Delete from "+model+" where season='"+season+"' and caneacslno="+caneacslno+" and login='"+user+"'";
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createSQLQuery(qry).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public List getRyotSbAccNo(String ryotCode,String season,String agreementNo)
    {
		//Modified by DMurty on 23-12-2016
       // String sql = "select * from RyotBankDetails where  ryotcode='" + ryotCode+"' and accountnumber not in (select referencenumber from LoanDetails where ryotcode='"+ryotCode+"')";
		 String sql = "select * from AgreementSummary where ryotcode='" + ryotCode+"' and seasonyear='"+season+"' and agreementnumber='"+agreementNo+"'";
        List sBAccByRyotList=hibernateDao.findByNativeSql(sql, AgreementSummary.class);
        if(sBAccByRyotList.size()>0 && sBAccByRyotList!=null)
        	return sBAccByRyotList;
        else
        	return null;         
    }
	
	public List getCaneValueSplitupByRyot(String season,Integer caneacno,String ryotCode)
    {
		String sql = "SELECT * FROM CaneValueSplitupTemp where season ='" + season+"' and caneacslno=" + caneacno+" and ryotcode='"+ryotCode+"'";
		List caneValueSplitupTempList=hibernateDao.findBySqlCriteria(sql);
        if(caneValueSplitupTempList.size()>0 && caneValueSplitupTempList!=null)
        	return caneValueSplitupTempList;
        else
        	return null;         
    }
	//Added by DMurty on 10-12-2016
	public List getMaxWeighmentDate(String ryotCode,String season,Date fromdate,Date todate)
    {
        String sql = "select * from WeighmentDetails where canereceiptenddate between '"+fromdate+"' and '"+todate+"' and ryotcode='"+ryotCode+"' and season='"+season+"' and canereceiptenddate=(select max(canereceiptenddate) from WeighmentDetails where ryotcode='"+ryotCode+"' and season='"+season+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"')";
        List WeighmentList=hibernateDao.findBySqlCriteria(sql);
        if(WeighmentList.size()>0 && WeighmentList!=null)
        	return WeighmentList;
        else
        	return null;    
    }
	
	public List getCaneAccountingPeriods(String season)
    {
        logger.info("========getCaneAccountingPeriods()==========");
        String sql = "select * from CaneAccountSmry where season='"+season+"' order by caneacslno";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getCaneAccountingPaymentDetails(String season,int paymentToRyot,int branch,int caneAccSlNo,int bankPayment)
    {
        logger.info("========getCaneAccountingPeriods()==========");
        String sql = null;
        logger.info("========bankPayment=========="+bankPayment);
        //Modified by DMurty on 10-02-2017
        if(bankPayment == 0)
        {
        	if(branch == 0)
        	{
        		if(paymentToRyot == 0)
                {
                    sql = "select * from CanAccSBDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and paidamt=0 and pendingamt>1";
                }
                else
                {
                    sql = "select * from CanAccSBDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and pendingamt>1 ";
                }
        	}
        	else
        	{
        		if(paymentToRyot == 0)
                {
                    sql = "select * from CanAccSBDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and bankcode="+branch+" and paidamt=0 and pendingamt>1";
                }
                else
                {
                    sql = "select * from CanAccSBDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and bankcode="+branch+" and pendingamt>1 ";
                }
        	}
        }
        else
        {
        	if(branch == 0)
        	{
        		if(paymentToRyot == 0)
                {
                    sql = "select * from CanAccLoansDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and paidamt=0 and advloan=1 and pendingamt>1";
                }
            	else
            	{
                    sql = "select * from CanAccLoansDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and advloan=1 and pendingamt>1";
            	}
        	}
        	else
        	{
        		if(paymentToRyot == 0)
                {
                    sql = "select * from CanAccLoansDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and branchcode="+branch+" and paidamt=0 and advloan=1 and pendingamt>1";
                }
            	else
            	{
                    sql = "select * from CanAccLoansDetails where season='"+season+"' and caneacctslno="+caneAccSlNo+" and branchcode="+branch+" and advloan=1 and pendingamt>1";
            	}
        	}
        }
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getAccountNumbers(String season,String ryotCode,String loanOrSb)
    {
        logger.info("========getAccountNumbers()==========");
        String sql = null;
        if("Loan".equalsIgnoreCase(loanOrSb))
        {
            sql = "select distinct loanaccountnumber from LoanPrincipleAmounts where season='"+season+"' and ryotcode='"+ryotCode+"'";
        }
        else
        {
            sql = "select distinct accountnumber from AgreementSummary where seasonyear='"+season+"' and ryotcode='"+ryotCode+"'";
        }
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 16-12-2016
	public double getPendingAmountForRyotByCaneAccSlNo(String ryotCode,String season,String loanOrSb,int caneAccSlNo)
    {
		double pendingAmt = 0.0;
        logger.info("========getPendingAmountForRyotByCaneAccSlNo()==========");
        String sql = null;
        if("Loan".equalsIgnoreCase(loanOrSb))
        {
            sql = "select pendingamt from CanAccLoansDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and advloan=1 and caneacctslno="+caneAccSlNo;
            logger.info("getPendingAmountForRyotByCaneAccSlNo()========sql=========="+sql);
            List AmtList =  hibernateDao.findBySqlCriteria(sql);
            logger.info("getPendingAmountForRyotByCaneAccSlNo()========AmtList=========="+AmtList);
            if(AmtList.size()>0 && AmtList!=null)
            {
            	Map tempMap = new HashMap();
            	tempMap = (Map) AmtList.get(0);
            	if((Double) tempMap.get("pendingamt") != null)
            	{
                	pendingAmt = (Double) tempMap.get("pendingamt");
            	}
            }
            return pendingAmt;
        }
        else
        {
            sql = "select loanamt from CanAccSBDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and caneacctslno="+caneAccSlNo;
            logger.info("getPendingAmountForRyotByCaneAccSlNo()========sql=========="+sql);
            List AmtList =  hibernateDao.findBySqlCriteria(sql);
            logger.info("getPendingAmountForRyotByCaneAccSlNo()========AmtList=========="+AmtList);
            if(AmtList.size()>0 && AmtList!=null)
            {
            	Map tempMap = new HashMap();
            	tempMap = (Map) AmtList.get(0);
            	if((Double) tempMap.get("loanamt") != null)
            	{
                	pendingAmt = (Double) tempMap.get("loanamt");
            	}
            }
            return pendingAmt;
        }
       
    }
	public List getAccountedRyots(String season,String loanOrSb,int caneAccSlNo)
    {
        logger.info("========getAccountedRyots()==========");
        String sql = null;
        if("Loan".equalsIgnoreCase(loanOrSb))
        {
            sql = "select ryotcode from CanAccLoansDetails where season='"+season+"' and advloan=1 and caneacctslno="+caneAccSlNo;
        }
        else
        {
            sql = "select ryotcode from CanAccSBDetails where season='"+season+"' and caneacctslno="+caneAccSlNo;
        }
        logger.info("getAccountedRyots()========sql=========="+sql);
        List RyotList =  hibernateDao.findBySqlCriteria(sql);
        if(RyotList.size()>0 && RyotList!=null)
        	return RyotList;
        else
        	return null; 
    }
	
	
	public List getCaneAccountingDataByRyot(String ryotCode,String season,String loanOrSb,int caneAccSlNo,int loanNo)
    {
        logger.info("========getCaneAccountingDataByRyot()==========");
        String sql = null;
        if("Loan".equalsIgnoreCase(loanOrSb))
        {
            sql = "select * from CanAccLoansDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and advloan=1 and loanno="+loanNo+" and caneacctslno="+caneAccSlNo;
        }
        else
        {
            sql = "select * from CanAccSBDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and caneacctslno="+caneAccSlNo;
        }
        logger.info("getCaneAccountingDataByRyot()========sql=========="+sql);
        List RyotList =  hibernateDao.findBySqlCriteria(sql);
        if(RyotList.size()>0 && RyotList!=null)
        	return RyotList;
        else
        	return null; 
    }
	//Added by DMurty on 17-12-2016
	public List getLoanSummaryByBankAndLoanNo(String season,String ryotCode,Integer loannumber,int bankBranch)
    {
        String sql = "select * from LoanSummary where ryotcode='" + ryotCode+"' and season='" + season+"' and loannumber="+loannumber+" and branchcode="+bankBranch;
        
        List loanSummaryList=hibernateDao.findByNativeSql(sql, LoanSummary.class);
        if(loanSummaryList.size()>0 && loanSummaryList!=null)
        	return loanSummaryList;
        else
        	return null;         
    }
	
	public List getRyotLoanBanks(String ryotCode,String season)
    {
        logger.info("========getRyotLoanBanks()==========");
        String sql = null;
        sql = "select branchcode from LoanPrincipleAmounts where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("getRyotLoanBanks()========sql=========="+sql);
        List BranchList =  hibernateDao.findBySqlCriteria(sql);
        if(BranchList.size()>0 && BranchList!=null)
        	return BranchList;
        else
        	return null; 
    }
	
	public List getRyotLoanAccountDetailsByBranch(String ryotCode,String season,int branchcode)
    {
        logger.info("========getRyotLoanAccountDetailsByBranch()==========");
        String sql = null;
        sql = "select * from LoanPrincipleAmounts where season='"+season+"' and ryotcode='"+ryotCode+"' and branchcode="+branchcode;
        logger.info("getRyotLoanAccountDetailsByBranch()========sql=========="+sql);
        List BranchList =  hibernateDao.findBySqlCriteria(sql);
        if(BranchList.size()>0 && BranchList!=null)
        	return BranchList;
        else
        	return null; 
    }
	
	public double getPrincipleForAdvance(int advanceCode,String season,String ryotCode)
    {
		double principle = 0.0;
        String sql = "select principle from AdvancePrincipleAmounts where season='" + season+"' and ryotcode='"+ryotCode+"' and advancecode="+advanceCode;
        List AdvList=hibernateDao.findBySqlCriteria(sql);
		logger.info("======== principle for advance==========sql"+sql);
        if(AdvList.size()>0 && AdvList!=null)
        {
        	Map tempMap = new HashMap();
        	tempMap = (Map) AdvList.get(0);
        	if((Double) tempMap.get("principle") != null)
        	{
            	principle = (Double) tempMap.get("principle");
        	}
        }
		logger.info("======== principle for advance=========="+principle);
        return principle;
    }
	
	public List getRyotLoanPrincipleByBranch(String ryotCode,String season,int branchcode,String accNo)
    {
        logger.info("========getRyotLoanPrincipleByBranch()==========");
        String sql = null;
        sql = "select * from CanAccLoansDetails where season='"+season+"' and ryotcode='"+ryotCode+"' and accountnum='"+accNo+"' and branchcode="+branchcode;
        logger.info("getRyotLoanAccountDetailsByBranch()========sql=========="+sql);
        List BranchList =  hibernateDao.findBySqlCriteria(sql);
        if(BranchList.size()>0 && BranchList!=null)
        	return BranchList;
        else
        	return null; 
    }
	public List getCaneValueSplitup()
    {
		String sql = "SELECT * FROM CaneValueSplitup  order by caneacslno";
		List caneValueSplitupList=hibernateDao.findBySqlCriteria(sql);
        if(caneValueSplitupList.size()>0 && caneValueSplitupList!=null)
        	return caneValueSplitupList;
        else
        	return null;         
    }
	
	public double getPreviousIntrestAmt(String ryotCode,String season,int branchcode,int loanno)
    {
		double intrest = 0.0;
		String sql = "SELECT interestamount FROM LoanSummary where ryotcode='"+ryotCode+"' and season='"+season+"' and branchcode="+branchcode+" and loannumber="+loanno;
        logger.info("getPreviousIntrestAmt()========sql=========="+sql);
		List caneValueSplitupList=hibernateDao.findBySqlCriteria(sql);
        logger.info("getPreviousIntrestAmt()========caneValueSplitupList=========="+caneValueSplitupList);
        if(caneValueSplitupList.size()>0 && caneValueSplitupList!=null)
    	{
        	Map intMap = new HashMap();
        	intMap = (Map) caneValueSplitupList.get(0);
        	if((Double) intMap.get("interestamount") != null)
        	{
            	intrest = (Double) intMap.get("interestamount");
        	}
    	}
        logger.info("getPreviousIntrestAmt()========intrest=========="+intrest);
        return intrest;
    }
	public List getCaneValueSplitupNew(int caneAccSlno)
    {
		String sql = "SELECT * FROM CaneValueSplitupTemp where caneacslno="+caneAccSlno+" order by ryotcode";
        logger.info("getCaneValueSplitupNew()========sql=========="+sql);
		List caneValueSplitupList=hibernateDao.findBySqlCriteria(sql);
        logger.info("getCaneValueSplitupNew()========caneValueSplitupList=========="+caneValueSplitupList);
        if(caneValueSplitupList.size()>0 && caneValueSplitupList!=null)
        	return caneValueSplitupList;
        else
        	return null;         
    }
	public List getCaneAccountingPeriodsTemp(String season)
    {
        logger.info("========getCaneAccountingPeriods()==========");
        String sql = "select * from CaneAccountSmrytemp where season='"+season+"' order by caneacslno";
        logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	//Added by DMurty on 29-12-2016
	public String getAgreementNoByDates(Date datefrom,Date date2,String season,String ryotcode)
    {
		String agreementNo = null;
        String sql = "select distinct agreementnumber from WeighmentDetails where canereceiptenddate between '"+datefrom+"' and '"+date2+"' and season='"+season+"' and ryotcode='"+ryotcode+"'";
        List weighmentList=hibernateDao.findBySqlCriteria(sql);
        if(weighmentList.size()>0 && weighmentList!=null)
        {
        	Map tempMap = new HashMap();
        	tempMap = (Map) weighmentList.get(0);
        	agreementNo = (String) tempMap.get("agreementnumber");
        }
        return agreementNo;
    }
	public List getDedutionsData(String season,int caneslno)
    {
        logger.info("========getDedutionsData()==========");
        String sql = null;
        sql = "select sum(cdcamt) as  cdc ,sum(transport) as trans,sum(ulamt) as ulamt,sum(harvesting) as harv,sum(otherded) as od ,sum(loanamt) as lamt,sum(sbac) as sbac,sum(advamt) as advamt from CanAccSBDetails where season='"+season+"'  and caneacctslno='"+caneslno+"' ";
        logger.info("getDedutionsData()========sql=========="+sql);
        List CanAccSBDetails =  hibernateDao.findBySqlCriteria(sql);
        if(CanAccSBDetails.size()>0 && CanAccSBDetails!=null)
        	return CanAccSBDetails;
        else
        	return null; 
    }
 	public List getCaneToFAData(String season,int caneslno)
    {
        logger.info("========getCaneToFAData()==========");
        String sql = null;
        sql = "select distinct branchcode from CanAccLoansDetails  where season='"+season+"'  and caneacctslno='"+caneslno+"' order by branchcode ";
        logger.info("getCaneToFAData()========sql=========="+sql);
        List CanAccSBDetails =  hibernateDao.findBySqlCriteria(sql);
        if(CanAccSBDetails.size()>0 && CanAccSBDetails!=null)
        	return CanAccSBDetails;
        else
        	return null; 
    }
 	
 	public List GetHarvChgDtlsByRyotContAndDateForRevert(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select * from HarvChgDtlsByRyotContAndDate where season='"+season+"' and ryotcode='"+ryotCode+"' and receiptdate>='"+efDate+"'";
        logger.info("========sql=========="+sql);
        List <HarvChgDtlsByRyotContAndDate> HarvChgDtlsByRyotContAndDate=hibernateDao.findByNativeSql(sql, HarvChgDtlsByRyotContAndDate.class);
        if(HarvChgDtlsByRyotContAndDate.size()>0 && HarvChgDtlsByRyotContAndDate!=null)
        	return HarvChgDtlsByRyotContAndDate;
        else
        	return null;   
        
    }
 	public List GetHarvChgDtlsByRyotContForRevert(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select * from HarvChgSmryByRyotAndCont where season='"+season+"' and ryotcode='"+ryotCode+"'";
        logger.info("========sql=========="+sql);
        List <HarvChgSmryByRyotAndCont> HarvChgSmryByRyotAndCont=hibernateDao.findByNativeSql(sql, HarvChgSmryByRyotAndCont.class);
        if(HarvChgSmryByRyotAndCont.size()>0 && HarvChgSmryByRyotAndCont!=null)
        	return HarvChgSmryByRyotAndCont;
        else
        	return null;
    }
 	public List getLoanCalcDetailsForPaymentChecklist(String sql)
    {
        List pendAmtList=hibernateDao.findBySqlCriteria(sql);
        if(pendAmtList.size()>0 && pendAmtList!=null)
        	return pendAmtList;
        else
        	return null;         
    }
 	public List GetDetailsForHarvChgDtlsByRyotContAndDateNew(String ryotCode,int contractorCode,String season,Date efDate)
    {
        String sql = "select * from HarvChgDtlsByRyotContAndDate where season='"+season+"' and ryotcode='"+ryotCode+"' and receiptdate>='"+efDate+"'";
        logger.info("========sql=========="+sql);
        List <HarvChgDtlsByRyotContAndDate> HarvChgDtlsByRyotContAndDate=hibernateDao.findByNativeSql(sql, HarvChgDtlsByRyotContAndDate.class);
        if(HarvChgDtlsByRyotContAndDate.size()>0 && HarvChgDtlsByRyotContAndDate!=null)
        	return HarvChgDtlsByRyotContAndDate;
        else
        	return null;
    }
	//added by naidu on 04-02-2017
 	public List getCaneAccDetailsTemp()
    {
		String sql = "SELECT ryotcode,ryotname,sum(suppliedqty) as supqty,sum(totalamt) as totalamount,sum(cdc) as cdc,sum(ul) as ul,sum(tp) as transport,sum(harvest) as harvest,sum(otherded) as otherded,sum(loan) as loan,sum(intrest) as intrest,sum(advances) as advances,sum(sb) as sb FROM CaneAccDetailsTemp  group by ryotcode,ryotname order by ryotcode";
		List caneAccDetailsTempList=hibernateDao.findBySqlCriteria(sql);
        if(caneAccDetailsTempList.size()>0 && caneAccDetailsTempList!=null)
        	return caneAccDetailsTempList;
        else
        	return null;         
    }
 	
 	public List getAdvOrLoanWithOrWithoutSupply(int advOrLoan,int supplyType,String season,String villageCode,int category)
    {
		String sql = null;
		if(supplyType == 0)
		{
			if(advOrLoan == 0)
			{
				sql = "select a.ryotcode,r.ryotname,a.advancecode as branchcode,c.advance as branchname,a.principle from AdvancePrincipleAmounts a,ryot r,CompanyAdvance c where a.season='"+season+"' and a.principle>2 and a.advancecode=c.advancecode and a.ryotcode=r.ryotcode and a.ryotcode in (select ryotcode from WeighmentDetails where season='"+season+"') order by a.ryotcode;";
			}
			else
			{
				sql = "select l.ryotcode,r.ryotname,l.branchcode,b.branchname,l.loanaccountnumber,l.principle from LoanPrincipleAmounts l, Branch b,ryot r where l.season='"+season+"' and l.principle>2 and l.branchcode=b.branchcode and l.ryotcode=r.ryotcode and l.ryotcode in (select ryotcode from WeighmentDetails where season='"+season+"') order by l.ryotcode;";
			}
		}
		else
		{
			if(advOrLoan == 0)
			{
				sql = "select a.ryotcode,r.ryotname,a.advancecode as branchcode,c.advance as branchname,a.principle from AdvancePrincipleAmounts a,ryot r,CompanyAdvance c where a.season='"+season+"' and a.principle>2 and a.advancecode=c.advancecode and a.ryotcode=r.ryotcode and a.ryotcode not in (select ryotcode from WeighmentDetails where season='"+season+"') order by a.ryotcode;";
			}
			else
			{
				sql = "select l.ryotcode,r.ryotname,l.branchcode,b.branchname,l.loanaccountnumber,l.principle from LoanPrincipleAmounts l, Branch b,ryot r where l.season='"+season+"' and l.principle>2 and l.branchcode=b.branchcode and l.ryotcode=r.ryotcode and l.ryotcode not in (select ryotcode from WeighmentDetails where season='"+season+"') order by l.ryotcode;";
			}
		}
		logger.info("getAdvOrLoanWithOrWithoutSupply()========sql=========="+sql);
        List weighmentDetailsList = hibernateDao.findBySqlCriteria(sql);
        
        logger.info("getAdvOrLoanWithOrWithoutSupply()========weighmentDetailsList=========="+weighmentDetailsList);
        if(weighmentDetailsList.size()>0 && weighmentDetailsList!=null)
        	return weighmentDetailsList;
        else
        	return null; 
    }
 	
 	//Added by DMurty on 02-03-2017
 	public double getPendingPrinciple(String ryotcode,String season,int advOrLoan)
    {
 		double principle = 0.0;
 		String sql = null;
 		if(advOrLoan == 0)
 		{
 			sql = "select sum(principle) as amt from AdvancePrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"'";
 		}
 		else if(advOrLoan == 1)
 		{
 			sql = "select sum(principle) as amt from LoanPrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"'";
 		}
 		else //for post season. Present its not there
 		{
 			
 		}
		List principleList=hibernateDao.findBySqlCriteria(sql);
        if(principleList.size()>0 && principleList!=null)
        {
        	Map tempMap = (Map) principleList.get(0);
        	if((Double)tempMap.get("amt") != null)
        	{
        		principle = (Double)tempMap.get("amt");
        	}
        }
        return principle;
    }
 	public List getSeedSupplierDetails(String season)
    {
		//String	sql = "select l.ryotcode,r.ryotname,l.branchcode,b.branchname,l.loanaccountnumber,l.principle from LoanPrincipleAmounts l, Branch b,ryot r where l.season='"+season+"' and l.principle>2 and l.branchcode=b.branchcode and l.ryotcode=r.ryotcode and l.ryotcode in (select ryotcode from WeighmentDetails where season='"+season+"') order by l.ryotcode;";
 	//	select sum(pendingamount),ss.seedsuppliercode,r.ryotname from SeedSuppliersSummary as ss,ryot as r where ss.seedsuppliercode=r.ryotcode group by ss.seedsuppliercode,r.ryotname having sum(pendingamount)>0
 		String	sql = "select ss.seedsuppliercode,r.ryotname,sum(pendingamount) as principle from SeedSuppliersSummary ss,ryot r where ss.season='"+season+"' and ss.seedsuppliercode=r.ryotcode group by ss.seedsuppliercode,r.ryotname  having sum(pendingamount)>0 order by ss.seedsuppliercode";
		
		logger.info("seedSuppList()========sql=========="+sql);
        List seedSuppList = hibernateDao.findBySqlCriteria(sql);
        
        logger.info("seedSuppList()========seedSuppList=========="+seedSuppList);
        if(seedSuppList.size()>0 && seedSuppList!=null)
        	return seedSuppList;
        else
        	return null; 
    }
 	public List getSeedSupplieramts(String season,String ryotCode)
    {
        String sql = "select * from SeedSuppliersSummary where seedsuppliercode='" + ryotCode+"' and season='" + season+"'";
        
        List seedSuppList=hibernateDao.findByNativeSql(sql, SeedSuppliersSummary.class);
        if(seedSuppList.size()>0 && seedSuppList!=null)
        	return seedSuppList;
        else
        	return null;         
    }
 	
 	
}
