package com.finsol.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finsol.model.Employee;

/**
 * @author Rama Krishna
 *
 */
@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private HibernateDao hibernateDao;
	
	public void addEmployee(Employee employee) {
		hibernateDao.getSessionFactory().getCurrentSession().saveOrUpdate(employee);
	}

	@SuppressWarnings("unchecked")
	public List<Employee> listEmployeess() {
		//return (List<Employee>) hibernateDao.getSessionFactory().getCurrentSession().createCriteria(Employee.class).list();
		return (List<Employee>) hibernateDao.getCriteria();
	}

	public Employee getEmployee(int empid) {
		return (Employee) hibernateDao.getSessionFactory().getCurrentSession().get(Employee.class, empid);
	}

	public void deleteEmployee(Employee employee) {
		hibernateDao.getSessionFactory().getCurrentSession().createQuery("DELETE FROM Employee WHERE empid = "+employee.getEmpId()).executeUpdate();
	}
	

}

