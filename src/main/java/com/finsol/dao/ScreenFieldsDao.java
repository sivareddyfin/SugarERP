package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.ScreenFields;
import com.finsol.model.Screens;
import com.finsol.model.SetupDetails;



@Repository("ScreenFieldsDao")
@Transactional

public class ScreenFieldsDao
{
	private static final Logger logger = Logger.getLogger(ScreenFieldsDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addScreenFields(ScreenFields screenFields)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(screenFields);
		session.flush();
		session.close();
	}
	@SuppressWarnings("unchecked")
	public List<ScreenFields> listScreenField()
	{
		return (List<ScreenFields>) hibernateDao.getSessionFactory().openSession().createCriteria(ScreenFields.class).list();
	}
	
	public List<ScreenFields> getScreenFieldsByCode(Integer screenid)
    {
        String sql = "select * from ScreenFields where screenid =" + screenid;
        
        return hibernateDao.findByNativeSql(sql, ScreenFields.class);
    }
	
	 public void deleteScreenFields(Integer screenid) 
	 {
			hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ScreenFields where screenid="+screenid).executeUpdate();
	 }

}
