package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.BankLoanIntRates;
import com.finsol.model.BankLoanRepayDt;
import com.finsol.model.TCSuretyDetails;
import com.finsol.model.TransportingContractors;

@Repository("TieUpLoanInterestDao")
@Transactional
public class TieUpLoanInterestDao {
	
	private static final Logger logger = Logger.getLogger(TieUpLoanInterestDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addTieUpLoanInterest(BankLoanRepayDt bankLoanRepayDt) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(bankLoanRepayDt);
		session.flush();
		session.close();
	}
	
	public void addTCSuretyDetails(BankLoanIntRates bankLoanIntRates) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(bankLoanIntRates);
		session.flush();
		session.close();
	}
	
	

	@SuppressWarnings("unchecked")
	public List<BankLoanRepayDt> listTieUpLoanInterests()
	{
		return (List<BankLoanRepayDt>) hibernateDao.getSessionFactory().openSession().createCriteria(BankLoanRepayDt.class).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<BankLoanIntRates> listTieUpLoanInterestDetails() 
	{
		return (List<BankLoanIntRates>) hibernateDao.getSessionFactory().openSession().createCriteria(BankLoanIntRates.class).list();
	}
	
	public void deleteBankLoanIntRates(BankLoanRepayDt bankLoanRepayDt) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM BankLoanIntRates WHERE branchcode = "+bankLoanRepayDt.getBranchcode()).executeUpdate();
	}
	
	
	public BankLoanRepayDt getBankLoanRepayDtls(String season,int id) {
		
		String sqlString="select *  from BankLoanRepayDt where season='"+season+"' and branchcode="+id;
		//List  bankLoanRepayDt=hibernateDao.findBySql(sqlString);
		List<BankLoanRepayDt> bankLoanRepayDt=hibernateDao.findByNativeSql(sqlString, BankLoanRepayDt.class);
		
		return bankLoanRepayDt.get(0);
	}
	
	public List<BankLoanIntRates> getBankLoanIntRatesByCode(Integer tcode)
    {
        String sql = "select * from BankLoanIntRates where branchcode =" + tcode;
        
        return hibernateDao.findByNativeSql(sql, BankLoanIntRates.class);
    }


}
