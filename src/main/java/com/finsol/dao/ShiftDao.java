package com.finsol.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Shift;


@Repository("ShiftDao")
@Transactional
public class ShiftDao
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addShift(Shift shift)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(shift);
		session.flush();
		session.close();
	}
	//listShifts
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Shift> listShifts()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Shift> shifts=session.createCriteria(Shift.class).list();
		session.close();
		return shifts;
	}
	
	public void deleteShift() {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift").executeUpdate();
	}
}
