package com.finsol.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Employees;
import com.finsol.model.RoleScreens;
import com.finsol.model.Roles;

@Repository("RolesDao")
@Transactional

public class RolesDao
{
	private static final Logger logger = Logger.getLogger(RolesDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addRoles(Roles roles)
	{
    	Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(roles);
		session.flush();
	}
	@SuppressWarnings("unchecked")
	public List<Roles> listRole()
	{
		return (List<Roles>) hibernateDao.getSessionFactory().openSession().createCriteria(Roles.class).list();

	}
	
	public Roles getRoles(int id)
	{
		return (Roles) hibernateDao.getSessionFactory().openSession().get(Roles.class, id);
	}
	
	public List<RoleScreens> getRoleScreensByRoleId(Integer roleId)
	{		
		return  hibernateDao.findByNativeSql("select * from RoleScreens where roleid="+roleId+"",RoleScreens.class);
	}

}
