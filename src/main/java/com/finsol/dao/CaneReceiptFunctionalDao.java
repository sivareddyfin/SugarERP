package com.finsol.dao;

import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.controller.CaneReceiptFunctionalController;
import com.finsol.dao.CommonDao;
import com.finsol.model.AdditionalCaneValue;
import com.finsol.model.CDCDetailsByRotAndDate;
import com.finsol.model.CDCDetailsByRyot;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneReceiptDaySmry;
import com.finsol.model.CaneReceiptDaySmryByRyot;
import com.finsol.model.CaneReceiptSeasonSmry;
import com.finsol.model.CaneReceiptShiftSmry;
import com.finsol.model.CaneReceiptWBBook;
import com.finsol.model.CaneValueDetailsByRyotAndDate;
import com.finsol.model.HarvChgDtlsByRyotContAndDate;
import com.finsol.model.HarvChgSmryByContAndDate;
import com.finsol.model.HarvChgSmryByRyotAndCont;
import com.finsol.model.HarvChgSmryBySeason;
import com.finsol.model.HarvChgSmryBySeasonByCont;
import com.finsol.model.HarvestingRateDetails;
import com.finsol.model.ICPDetailsByRyot;
import com.finsol.model.ICPDetailsByRyotAndDate;
import com.finsol.model.PermitDetails;
import com.finsol.model.PostSeasonValueSummary;
import com.finsol.model.Shift;
import com.finsol.model.SubsidyDetailsByRotAndDate;
import com.finsol.model.SubsidyDetailsByRyot;
import com.finsol.model.TranspChgSmryByContAndDate;
import com.finsol.model.TranspChgSmryByRyotAndCont;
import com.finsol.model.TranspChgSmryByRyotContAndDate;
import com.finsol.model.TranspChgSmryBySeason;
import com.finsol.model.TranspChgSmryBySeasonByCont;
import com.finsol.model.TransportAllowanceDetails;
import com.finsol.model.TransportingRateDetails;
import com.finsol.model.UCDetailsByRotAndDate;
import com.finsol.model.UCDetailsByRyot;
import com.finsol.model.WeighmentDetails;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

@Repository("CaneReceiptFunctionalDao")
@Transactional
public class CaneReceiptFunctionalDao {
	
	private static final Logger logger = Logger.getLogger(CaneReceiptFunctionalDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public boolean saveAllCaneWeighment(List caneWeighmentTables) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean isInsertTrue=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<caneWeighmentTables.size();i++)
	        {
	        	session.saveOrUpdate(caneWeighmentTables.get(i));
	        }
	        session.flush();
	        transaction.commit();
	        isInsertTrue=true;
	        return isInsertTrue;	        
	    } 
	    catch (JDBCException jde) 
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        //throw new RuntimeException(jde);
	        return isInsertTrue;
	    }
	    finally 
	    {
	        if (session.isOpen())
	        {
	            session.close();
	        }
	    }
	}
	
	public List getCaneReceiptDaySmryByDate(String caneReceiptDate)
    {
        String sql = "select * from CaneReceiptDaySmry where canereceiptdate ='" + DateUtils.getSqlDateFromString(caneReceiptDate,Constants.GenericDateFormat.DATE_FORMAT)+"'";
        List caneReceiptDaySmryList=hibernateDao.findByNativeSql(sql, CaneReceiptDaySmry.class);
        if(caneReceiptDaySmryList.size()>0 &&caneReceiptDaySmryList!=null)
        	return caneReceiptDaySmryList;
        else
        	return null;
    }
	
	public List getCaneReceiptShiftSmry(String caneReceiptDate,Integer shift)
    {
		
       // String sql = "select * from CaneReceiptShiftSmry where canereceiptdate ='" + DateUtils.getSqlDateFromString(caneReceiptDate,Constants.GenericDateFormat.DATE_FORMAT)+"' order by shiftid";
		String sql ="select count(serialnumber) as rs2,sum(netwt) as netwt from WeighmentDetails where status=1 and canereceiptenddate ='" + DateUtils.getSqlDateFromString(caneReceiptDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and shiftid1="+shift;
		//String sql ="select shiftid1,count(sum(lorryandcanewt) as lorrycanewt,sum(lorrywt) as lorrywt,bmpercentage from WeighmentDetails inner join CaneWeight on WeighmentDetails.season=CaneWeight.season  where status=1 and canereceiptenddate ='" + DateUtils.getSqlDateFromString(caneReceiptDate,Constants.GenericDateFormat.DATE_FORMAT)+"' group by shiftid1,bmpercentage";
		List caneReceiptShiftSmryList=hibernateDao.findBySqlCriteria(sql);
        if(caneReceiptShiftSmryList.size()>0 && caneReceiptShiftSmryList!=null)
        	return caneReceiptShiftSmryList;
        else
        	return null;
    }
	public List getShifts()
    {
		String sql = "select * from Shift";
        List caneReceiptDaySmryByRyotList=hibernateDao.findByNativeSql(sql, Shift.class);
        if(caneReceiptDaySmryByRyotList.size()>0 &&caneReceiptDaySmryByRyotList!=null)
        	return caneReceiptDaySmryByRyotList;
        else
        	return null; 
   
    }
	
	public List getCaneReceiptDaySmryByRyot(String ryotcode,String finalDate)
    {
		//Modified by DMurty on 06-10-2016
        //String sql = "select * from CaneReceiptDaySmryByRyot where ryotcode ='" + ryotcode+"' and canereceiptdate = (SELECT max(canereceiptdate) FROM CaneReceiptDaySmryByRyot where ryotcode ='"+ryotcode+"' and canereceiptdate <= '"+finalDate+"')";
		String sql = "select * from CaneReceiptDaySmryByRyot where ryotcode ='" + ryotcode+"' and canereceiptdate between '"+finalDate+"' and '"+finalDate+"'";
        List caneReceiptDaySmryByRyotList=hibernateDao.findByNativeSql(sql, CaneReceiptDaySmryByRyot.class);
        if(caneReceiptDaySmryByRyotList.size()>0 &&caneReceiptDaySmryByRyotList!=null)
        	return caneReceiptDaySmryByRyotList;
        else
        	return null; 
    }
	
	public List getCaneReceiptSeasonSmry(String season)
    {
        String sql = "select * from CaneReceiptSeasonSmry where season ='" + season+"'";
        
        List caneReceiptSeasonSmryList= hibernateDao.findByNativeSql(sql, CaneReceiptSeasonSmry.class);
        
        if(caneReceiptSeasonSmryList.size()>0 &&caneReceiptSeasonSmryList!=null)
        	return caneReceiptSeasonSmryList;
        else
        	return null; 
    }
	
	public List getCaneReceiptWBBook(Integer wbId)
    {
        String sql = "select * from CaneReceiptWBBook where weighbridgeid ="+wbId;
        
        List caneReceiptWBBookList= hibernateDao.findByNativeSql(sql, CaneReceiptWBBook.class);
        if(caneReceiptWBBookList.size()>0 && caneReceiptWBBookList!=null)
        	return caneReceiptWBBookList;
        else
        	return null; 
        
    }
	public List getAdditionalCaneValue(String season,String ryotCode)
    {
        String sql = "select * from AdditionalCaneValue where season ='" + season+"' and ryotcode='"+ryotCode+"'";
        
        List additionalCaneValueList= hibernateDao.findByNativeSql(sql, AdditionalCaneValue.class);
        if(additionalCaneValueList.size()>0 && additionalCaneValueList!=null)
        	return additionalCaneValueList;
        else
        	return null;        
    }
	
	public List<CaneAccountingRules> getTransportAllowanceDetailsFromCAR(String season)
    {
        String sql = "select * from CaneAccountingRules where season ='" + season+"'";
        
        return hibernateDao.findByNativeSql(sql, CaneAccountingRules.class);
    }
	
	public List<CaneAcctRules> getTransprtAlwnceFromCaneAcctRules(String season,String finalDate)
    {
        String sql = "select * from CaneAcctRules where season ='"+season+"' and calcruledate = (SELECT max(calcruledate) FROM CaneAcctRules where season ='"+season+"' and calcruledate <= '"+finalDate+"')";
        return hibernateDao.findByNativeSql(sql, CaneAcctRules.class);
    }
	//Added by DMurty on 12-07-2016
	public List<CaneAcctAmounts> getCaneAcctAmounts(String season,int calcid,String pricename)
    {
        String sql = "select * from CaneAcctAmounts where season ='" + season+"' and pricename='"+pricename+"' and calcid = "+calcid;
        return hibernateDao.findByNativeSql(sql, CaneAcctAmounts.class);
    }
	
	public List getTransportAllowanceDetailsFromCARList(String season)
    {
        String sql = "select * from CaneAccountingRules where season ='" + season+"'";
        List TList = hibernateDao.findBySqlCriteria(sql);
        return TList;
    }
	
	public List getTransportAllowanceDetails(String season,String ryotCode,String plotNo,String finalDate)
    {
        String sql = "select * from TransportAllowanceDetails where receiptdate between '"+finalDate+"' and '"+finalDate+"' and season ='" + season+"' and ryotcode='" + ryotCode+"' and plotnumber='"+plotNo+"'";
		logger.info("========getTransportAllowanceDetails()=========="+sql);
        List transportAllowanceDetailsList=hibernateDao.findByNativeSql(sql, TransportAllowanceDetails.class);
        if(transportAllowanceDetailsList.size()>0 && transportAllowanceDetailsList!=null)
        	return transportAllowanceDetailsList;
        else
        	return null; 
    }
	
	public List getPostSeasonValueSummary(String season,String ryotCode)
    {
		
        String sql = "select * from PostSeasonValueSummary where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        
        List postSeasonValueSummaryList=hibernateDao.findByNativeSql(sql, PostSeasonValueSummary.class);
        
        if(postSeasonValueSummaryList.size()>0 && postSeasonValueSummaryList!=null)
        	return postSeasonValueSummaryList;
        else
        	return null;         
    }
	
	public List getHarvestingRateDetails(String season,String ryotCode,String finalDate)
    {
		//Modified by DMurty on 11-08-2016
        String sql = "select * from HarvestingRateDetails where season ='" + season+"' and ryotcode='" + ryotCode+"' and effectivedate<='"+finalDate+"' and effectivedate=(select max(effectivedate) from HarvestingRateDetails where season ='"+season+"' and ryotcode='"+ryotCode+"' and effectivedate<='"+finalDate+"')";
        List harvestingRateDetailsList=hibernateDao.findByNativeSql(sql, HarvestingRateDetails.class);
        
        if(harvestingRateDetailsList.size()>0 && harvestingRateDetailsList!=null)
        	return harvestingRateDetailsList;
        else
        	return null;         
    }
	
	public List getHarvChgDtlsByRyotContAndDate(String season,String ryotCode,double harvestingRate,String finalDate)
    {
        String sql = "select * from HarvChgDtlsByRyotContAndDate where season ='" + season+"' and ryotcode='" + ryotCode+"' and rate="+harvestingRate+" and receiptdate='"+finalDate+"'";
        List harvChgDtlsByRyotContAndDateList=hibernateDao.findByNativeSql(sql, HarvChgDtlsByRyotContAndDate.class);
        
        if(harvChgDtlsByRyotContAndDateList.size()>0 && harvChgDtlsByRyotContAndDateList!=null)
        	return harvChgDtlsByRyotContAndDateList;
        else
        	return null;         
    }
	
	public List CaneValueDetailsByRyotAndDateList(String season,String ryotCode,String finalDate)
    {
        String sql = "select * from CaneValueDetailsByRyotAndDate where season ='" + season+"' and ryotcode='" + ryotCode+"' and canesupplieddt between '"+finalDate+"' and '"+finalDate+"'";
        List caneValueDetailsByRyotAndDateList=hibernateDao.findByNativeSql(sql, CaneValueDetailsByRyotAndDate.class);
        if(caneValueDetailsByRyotAndDateList.size()>0 && caneValueDetailsByRyotAndDateList!=null)
        	return caneValueDetailsByRyotAndDateList;
        else
        	return null;         
    }
	
	public List getCaneValDtlsByRyotDateList(String season,String ryotCode)
    {
		 String sql = "select * from CaneValueDetailsByRyotAndDate where season ='" + season+"' and ryotcode='" + ryotCode+"'";
		 List CaneValDtlsByRyotDate = hibernateDao.findBySqlCriteria(sql); 
         return CaneValDtlsByRyotDate;
    }
	
	public List getHarvChgSmryByRyotAndCont(String season,String ryotCode)
    {
        String sql = "select * from HarvChgSmryByRyotAndCont where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        
        List harvChgSmryByRyotAndContList=hibernateDao.findByNativeSql(sql, HarvChgSmryByRyotAndCont.class);
        
        if(harvChgSmryByRyotAndContList.size()>0 && harvChgSmryByRyotAndContList!=null)
        	return harvChgSmryByRyotAndContList;
        else
        	return null;         
    }
	
	public List getHarvChgSmryByContAndDate(String season)
    {
        String sql = "select * from HarvChgSmryByContAndDate where season ='" + season+"'";
        
        List harvChgSmryByContAndDateList=hibernateDao.findByNativeSql(sql, HarvChgSmryByContAndDate.class);
        
        if(harvChgSmryByContAndDateList.size()>0 && harvChgSmryByContAndDateList!=null)
        	return harvChgSmryByContAndDateList;
        else
        	return null;         
    }
	
	
	public List getHarvChgSmryBySeasonByCont(String season)
    {
        String sql = "select * from HarvChgSmryBySeasonByCont where season ='" + season+"'";
        
        List harvChgSmryBySeasonByContList=hibernateDao.findByNativeSql(sql, HarvChgSmryBySeasonByCont.class);
        
        if(harvChgSmryBySeasonByContList.size()>0 && harvChgSmryBySeasonByContList!=null)
        	return harvChgSmryBySeasonByContList;
        else
        	return null;         
    }
	public List getHarvChgSmryBySeason(String season)
    {
        String sql = "select * from HarvChgSmryBySeason where season ='" + season+"'";
        
        List harvChgSmryBySeasonList=hibernateDao.findByNativeSql(sql, HarvChgSmryBySeason.class);
        
        if(harvChgSmryBySeasonList.size()>0 && harvChgSmryBySeasonList!=null)
        	return harvChgSmryBySeasonList;
        else
        	return null;         
    }
	
	
	//--------------------------------------------------//
	public List getTransportingRateDetails(String season,String ryotCode,String finalDate)
    {
		//Modified by DMurty on 11-08-2016
        String sql = "select * from TransportingRateDetails where season ='" + season+"' and ryotcode='" + ryotCode+"' and effectivedate <= '"+finalDate+"' and effectivedate=(select max(effectivedate) from TransportingRateDetails where season ='"+season+"' and ryotcode='"+ryotCode+"' and effectivedate<='"+finalDate+"')";
        List transportingRateDetailsList=hibernateDao.findByNativeSql(sql, TransportingRateDetails.class);
        if(transportingRateDetailsList.size()>0 && transportingRateDetailsList!=null)
        	return transportingRateDetailsList;
        else
        	return null;         
    }
	
	public List getTranspChgSmryByRyotContAndDate(String season,String ryotCode,double transportRate,String finalDate)
    {
        String sql = "select * from TranspChgSmryByRyotContAndDate where season ='" + season+"' and ryotcode='" + ryotCode+"' and rate="+transportRate+" and receiptdate='"+finalDate+"'";
        List tanspChgSmryByRyotContAndDateList=hibernateDao.findByNativeSql(sql, TranspChgSmryByRyotContAndDate.class);
        if(tanspChgSmryByRyotContAndDateList.size()>0 && tanspChgSmryByRyotContAndDateList!=null)
        	return tanspChgSmryByRyotContAndDateList;
        else
        	return null;         
    }
	
	public List getTranspChgSmryByRyotAndCont(String season,String ryotCode)
    {
        String sql = "select * from TranspChgSmryByRyotAndCont where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        
        List transpChgSmryByRyotAndContList=hibernateDao.findByNativeSql(sql, TranspChgSmryByRyotAndCont.class);
        
        if(transpChgSmryByRyotAndContList.size()>0 && transpChgSmryByRyotAndContList!=null)
        	return transpChgSmryByRyotAndContList;
        else
        	return null;         
    }
	
	public List getTranspChgSmryByContAndDate(String season)
    {
        String sql = "select * from TranspChgSmryByContAndDate  where season ='" + season+"'";
        
        List transpChgSmryByContAndDateList=hibernateDao.findByNativeSql(sql, TranspChgSmryByContAndDate.class);
        
        if(transpChgSmryByContAndDateList.size()>0 && transpChgSmryByContAndDateList!=null)
        	return transpChgSmryByContAndDateList;
        else
        	return null;         
    }
	
	
	public List getTranspChgSmryBySeasonByCont(String season)
    {
        String sql = "select * from TranspChgSmryBySeasonByCont  where season ='" + season+"'";
        
        List transpChgSmryBySeasonByContList=hibernateDao.findByNativeSql(sql, TranspChgSmryBySeasonByCont.class);
        
        if(transpChgSmryBySeasonByContList.size()>0 && transpChgSmryBySeasonByContList!=null)
        	return transpChgSmryBySeasonByContList;
        else
        	return null;         
    }
	public List getTranspChgSmryBySeason(String season)
    {
        String sql = "select * from TranspChgSmryBySeason where  season ='" + season+"'";
        
        List transpChgSmryBySeasonList=hibernateDao.findByNativeSql(sql, TranspChgSmryBySeason.class);
        
        if(transpChgSmryBySeasonList.size()>0 && transpChgSmryBySeasonList!=null)
        	return transpChgSmryBySeasonList;
        else
        	return null;         
    }
	
	public List getWeighmentDetailsByAgNumberSeasonPlot(String agreementNumber,String season,String plotNumber)
    {
        String sql = "select *  from WeighmentDetails WHERE agreementnumber='"+agreementNumber+"' and season ='"+season+"' and plotnumber ='"+plotNumber+"'";
        
        List weighmentDetailsList=hibernateDao.findByNativeSql(sql, WeighmentDetails.class);
        
        if(weighmentDetailsList.size()>0 && weighmentDetailsList!=null)
        	return weighmentDetailsList;
        else
        	return null;         
    }
	
	
	//-------------------------

	public Object getProgressiveQuantityFromAgreement(String agreementNumber,String season,String plotNumber)
	{
		String sqlQurery="select progressiveqty  from AgreementDetails WHERE agreementnumber='"+agreementNumber+"' and seasonyear ='"+season+"' and plotnumber ='"+plotNumber+"'";
		Object obj=hibernateDao.getID(sqlQurery);
		if(obj==null)
			return null;
		else			
			return obj;
	}
	public boolean updateProgressiveQuantity(String agreementNumber,String season,String plotNumber,Double netWeight)
	{
		boolean updateFlag=false;
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set progressiveqty="+netWeight+" WHERE agreementnumber='"+agreementNumber+"' and seasonyear ='"+season+"' and plotnumber ='"+plotNumber+"'").executeUpdate();
		updateFlag=true;
		return updateFlag;
	}
	public boolean updateCaneReceptSartEndDateInAgreement(String agreementNumber,String season,String plotNumber,Date caneReceiptStartDate,Date caneReceiptEndDate)
	{
		boolean updateFlag=false;
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set canesupplystartdate='"+caneReceiptStartDate+"',canesupplyenddate='"+caneReceiptEndDate+"' WHERE agreementnumber='"+agreementNumber+"' and seasonyear ='"+season+"' and plotnumber ='"+plotNumber+"'").executeUpdate();
		updateFlag=true;
		return updateFlag;
	}
	
	public boolean 	updatePermitDetailsStatus(Integer permitNumber,Integer programNumber,String season)
	{
		boolean updateFlag=false;
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE PermitDetails set status=1 WHERE permitnumber='"+permitNumber+"' and programno ='"+programNumber+"' and season ='"+season+"'").executeUpdate();
		updateFlag=true;
		return updateFlag;
	}
	
	public int getProgramNo(int permitno)
    {
		int programNo = 0;
		try
		{
	        Session session=hibernateDao.getSessionFactory().openSession();
	        SQLQuery query= session.createSQLQuery("select programno from PermitDetails  where permitnumber="+permitno);
	        List values=query.list();
	        
	        Integer  myResult  = (Integer)values.get(0);
	      //  programNo = (Integer) myResult[0];
	        
	       /* if(ProgramList.size()>0 && ProgramList!=null)
	        {
	        	Map tempMap = new HashMap();
	        	tempMap = (Map) ProgramList.get(0);
	        	programNo = (Integer) tempMap.get("programno");
	        }*/
	        return myResult.intValue(); 
		}
		catch(Exception e)
		{
	        return programNo; 
		}
    }
	
	public byte getStatusForWeighment(int serialnumber,String validationFor)
    {
		String columnName = "";
		byte status = 0;
		if("SerialNo".equalsIgnoreCase(validationFor))
		{
			columnName = "serialnumber";
		}
		else if("PermitNo".equalsIgnoreCase(validationFor))
		{
			columnName = "permitnumber";
		}
        String sql = "select status from WeighmentDetails where "+columnName+"=" + serialnumber;
        List ProgramList = hibernateDao.findBySqlCriteria(sql); 
        if(ProgramList.size()>0 && ProgramList!=null)
        {
        	Map tempMap = new HashMap();
        	tempMap = (Map) ProgramList.get(0);
        	status = (Byte) tempMap.get("status");
        }
        return status;    
    }
	
	//Added by DMurty on 03-08-2016
	public List CaneValueDetailsByRyotList(String season,String ryotCode)
    {
		 String sql = "select * from CaneValueDetailsByRyot where season ='" + season+"' and ryotcode='" + ryotCode+"'";
		 List CaneValDtlsByRyot = hibernateDao.findBySqlCriteria(sql); 
         return CaneValDtlsByRyot;
    }
	//Added by DMurty on 03-08-2016
	public List getAccruedAndDueCaneValueSummaryList(String season,String ryotCode)
    {
		 String sql = "select * from AccruedAndDueCaneValueSummary where season ='" + season+"' and ryotcode='" + ryotCode+"'";
		 List CaneValDtlsByRyot = hibernateDao.findBySqlCriteria(sql); 
         return CaneValDtlsByRyot;
    }
	//Added by DMurty on 19-08-2016
	public List getRyotDatainWeighmentDetails(String season,String ryotCode)
    {
		 String sql = "select * from WeighmentDetails where season ='" + season+"' and ryotcode='" + ryotCode+"'";
		 List CaneValDtlsByRyot = hibernateDao.findBySqlCriteria(sql); 
         return CaneValDtlsByRyot;
    }
	public boolean updateCaneReceptEndDateInAgreement(String agreementNumber,String season,String plotNumber,Date caneReceiptEndDate)
	{
		boolean updateFlag=false;
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AgreementDetails set canesupplyenddate='"+caneReceiptEndDate+"' WHERE agreementnumber='"+agreementNumber+"' and seasonyear ='"+season+"' and plotnumber ='"+plotNumber+"'").executeUpdate();
		updateFlag=true;
		return updateFlag;
	}
	
	public List getDeductionRates(int dedCode)
    {
		 String sql = "select * from StandardDeductions where stdDedCode="+dedCode;
		 List DeductionList = hibernateDao.findBySqlCriteria(sql); 
         return DeductionList;
    }
	public List getCDCDetailsByRyotList(String season,String ryotCode,String table)
    {
		String sql = "select * from CDCDetailsByRyot where season='"+season+"' and ryotcode='"+ryotCode+"'";
		List DeductionList=hibernateDao.findByNativeSql(sql, CDCDetailsByRyot.class);
        if(DeductionList.size()>0 && DeductionList!=null)
        	return DeductionList;
        else
        	return null; 
    }
	
	public List getCDCDetailsByRotAndDateList(String season,String ryotCode,String table,String finalDate)
    {
		//Modified by DMurty on 25-11-2016 (Date was added)
		String sql = "select * from CDCDetailsByRotAndDate where receiptdate between '"+finalDate+"' and '"+finalDate+"' and season='"+season+"' and ryotcode='"+ryotCode+"'";
		List DeductionList=hibernateDao.findByNativeSql(sql, CDCDetailsByRotAndDate.class);
        if(DeductionList.size()>0 && DeductionList!=null)
        	return DeductionList;
        else
        	return null; 
    }
	public List getUCDetailsByRotAndDateList(String season,String ryotCode,String table,String finalDate)
    {
		String sql = "select * from UCDetailsByRotAndDate where receiptdate between '"+finalDate+"' and '"+finalDate+"' and season='"+season+"' and ryotcode='"+ryotCode+"'";
		List DeductionList=hibernateDao.findByNativeSql(sql,UCDetailsByRotAndDate.class);
        if(DeductionList.size()>0 && DeductionList!=null)
        	return DeductionList;
        else
        	return null; 
    }
	public List getUCDetailsByRyotList(String season,String ryotCode,String table)
    {
		String sql = "select * from UCDetailsByRyot where season='"+season+"' and ryotcode='"+ryotCode+"'";
		List DeductionList=hibernateDao.findByNativeSql(sql,UCDetailsByRyot.class);
        if(DeductionList.size()>0 && DeductionList!=null)
        	return DeductionList;
        else
        	return null; 
    }
	//Added by DMurty on 06-10-2016
	public List getICPDetailsByRyot(String season,String ryotCode)
    {
        String sql = "select * from ICPDetailsByRyot where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        List ICPDetailsByRyotList=hibernateDao.findByNativeSql(sql,ICPDetailsByRyot.class);
        if(ICPDetailsByRyotList.size()>0 && ICPDetailsByRyotList!=null)
        	return ICPDetailsByRyotList;
        else
        	return null; 
    }
	
	public List getICPDetailsByRyotAndDate(String season,String ryotCode,String finalDate)
    {
		//Modified by DMurty on 11-08-2016
        //String sql = "select * from ICPDetailsByRyotAndDate where season ='" + season+"' and ryotcode='" + ryotCode+"' and receiptdate <= '"+finalDate+"' and receiptdate=(select max(receiptdate) from ICPDetailsByRyotAndDate where season ='"+season+"' and ryotcode='"+ryotCode+"' and receiptdate<='"+finalDate+"')";
		String sql = "select * from ICPDetailsByRyotAndDate where receiptdate between '"+finalDate+"' and '"+finalDate+"' and season ='" + season+"' and ryotcode='" + ryotCode+"'";
        List ICPDetailsByRyotAndDateList=hibernateDao.findByNativeSql(sql, ICPDetailsByRyotAndDate.class);
        if(ICPDetailsByRyotAndDateList.size()>0 && ICPDetailsByRyotAndDateList!=null)
        	return ICPDetailsByRyotAndDateList;
        else
        	return null;         
    }
	
	public List getWeighmentDetailsByPermit(int permitno)
    {
		try
		{
			String sql = "select * from WeighmentDetails where permitnumber="+permitno+" and status=0";
	        List weighmentListList=hibernateDao.findBySqlCriteria(sql);
	        if(weighmentListList.size()>0 && weighmentListList!=null)
	        	return weighmentListList;
	        else
	        	return null;  		
	    }
		catch(Exception e)
		{
	        return null; 
		}
    }
	//Added by DMurty on 23-11-2016
	public int getActualSlNoBySerialNumber(int slNo,String season)
    {
		int actualSlNo = 0;
		try
		{
			String sql = "select * from WeighmentDetails where serialnumber="+slNo+" and season='"+season+"'";
	        List weighmentList=hibernateDao.findBySqlCriteria(sql);
	        if(weighmentList.size()>0 && weighmentList!=null)
	        {
	        	Map tempMap = new HashMap();
	        	tempMap = (Map) weighmentList.get(0);
	        	actualSlNo = (Integer) tempMap.get("actualserialnumber");
	        }
	        return actualSlNo;
	    }
		catch(Exception e)
		{
	        return actualSlNo; 
		}
    }
	//Added by DMurty on 26-11-2016
	public List getSubsidyDetailsByRotAndDateList(String ryotCode,String season,String finalDate)
    {
        String sql = "select * from SubsidyDetailsByRotAndDate where receiptdate between '"+finalDate+"' and '"+finalDate+"' and  season ='" + season+"' and ryotcode='" + ryotCode+"'";
        List SubsidyDetailsByRotAndDateList=hibernateDao.findByNativeSql(sql,SubsidyDetailsByRotAndDate.class);
        if(SubsidyDetailsByRotAndDateList.size()>0 && SubsidyDetailsByRotAndDateList!=null)
        	return SubsidyDetailsByRotAndDateList;
        else
        	return null; 
    }
	
	public List getSubsidyDetailsByRyotList(String ryotCode,String season)
    {
        String sql = "select * from SubsidyDetailsByRyot where season ='" + season+"' and ryotcode='" + ryotCode+"'";
        List SubsidyDetailsByRyotList=hibernateDao.findByNativeSql(sql,SubsidyDetailsByRyot.class);
        if(SubsidyDetailsByRyotList.size()>0 && SubsidyDetailsByRyotList!=null)
        	return SubsidyDetailsByRyotList;
        else
        	return null; 
    }
	
	//Added by DMurty on 28-11-2016
	public List getCaneReceiptShiftSmryList(int shiftId,String caneReceiptDate)
    {
        String sql = "select * from CaneReceiptShiftSmry where canereceiptdate ='" + caneReceiptDate+"' and shiftid="+shiftId;
        List CaneReceiptShiftSmryList=hibernateDao.findByNativeSql(sql, CaneReceiptShiftSmry.class);
        if(CaneReceiptShiftSmryList.size()>0 &&CaneReceiptShiftSmryList!=null)
        	return CaneReceiptShiftSmryList;
        else
        	return null;
    }
	public Integer GetSecondRec(String canedate,int shift)
    {
		String sql="select * from WeighmentDetails where  canereceiptdate ='" + DateUtils.getSqlDateFromString(canedate,Constants.GenericDateFormat.DATE_FORMAT)+"' and shiftid="+shift+"";
        List CaneReceiptShiftSmryList=hibernateDao.findByNativeSql(sql, WeighmentDetails.class);
        if(CaneReceiptShiftSmryList.size()>0 &&CaneReceiptShiftSmryList!=null)
        	return CaneReceiptShiftSmryList.size();
        else
        	return 0;
    }
	public boolean insertVcheckIn(String checkindate,Integer serialNumber,String permitNumber,String season,String vehicleNumber,Integer shiftId,Integer vehicletype)
	{
		boolean updateFlag=false;
		try
		{
			hibernateDao.getSessionFactory().openSession().createQuery("UPDATE PermitDetails set checkinstatus=1,shiftid="+shiftId+",vehicleno='"+vehicleNumber+"',vehicletype="+vehicletype+",checkintime=GETDATE(),checkindate='"+ DateUtils.getSqlDateFromString(checkindate,Constants.GenericDateFormat.DATE_FORMAT)+"',vehicleslno="+serialNumber+" WHERE permitnumber="+permitNumber+" and season ='"+season+"'").executeUpdate();
			updateFlag=true;
			return updateFlag;
		}
		catch(Exception e)
		{
			logger.info("---updation fails due to----"+e);
			return updateFlag;
		}
		
	}
	public List getPermitCheckInTotalPerShift(String checkInDate,Integer shift)
    {
		
       	String sql ="select shiftid,count(vehicleslno) as vtotal from PermitDetails  where checkindate ='" + DateUtils.getSqlDateFromString(checkInDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and shiftid="+shift+" and checkinstatus>0 group by shiftid";
		List caneReceiptShiftSmryList=hibernateDao.findBySqlCriteria(sql);
        if(caneReceiptShiftSmryList.size()>0 && caneReceiptShiftSmryList!=null)
        	return caneReceiptShiftSmryList;
        else
        	return null;
    }
	public List getWaitingVehiclesForWeighment(String checkInDate,Integer shift)
    {
		
       	String sql ="select shiftid,count(vehicleslno) as wtotal from PermitDetails  where checkindate ='" + DateUtils.getSqlDateFromString(checkInDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and shiftid="+shift+" and checkinstatus=1 and permitnumber not in(select permitnumber from WeighmentDetails where shiftdate='" + DateUtils.getSqlDateFromString(checkInDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and shiftid="+shift+") group by shiftid";
		List caneReceiptShiftSmryList=hibernateDao.findBySqlCriteria(sql);
        if(caneReceiptShiftSmryList.size()>0 && caneReceiptShiftSmryList!=null)
        	return caneReceiptShiftSmryList;
        else
        	return null;
    }
	public List getVehiclesForWeighment(String frmdate,String todate)
    {
		
		String sql ="select vehicletype,shiftid,checkindate,checkintime,permitnumber,vehicleno,vehicleslno,p.ryotcode,r.ryotname from PermitDetails as p,Ryot as r  where checkinstatus>0 and checkindate between '" + DateUtils.getSqlDateFromString(frmdate,Constants.GenericDateFormat.DATE_FORMAT)+"' and '"+DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT)+"' and permitnumber not in(select permitnumber from WeighmentDetails where shiftdate between '" + DateUtils.getSqlDateFromString(frmdate,Constants.GenericDateFormat.DATE_FORMAT)+"' and '"+ DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT)+"')  and p.ryotcode=r.ryotcode order by checkInDate,shiftid,vehicleslno";
		List caneReceiptShiftSmryList=hibernateDao.findBySqlCriteria(sql);
        if(caneReceiptShiftSmryList.size()>0 && caneReceiptShiftSmryList!=null)
        	return caneReceiptShiftSmryList;
        else
        	return null;
    }
	
	public byte getStatusForVehicleCheckIn(int permitnumber)
    {
		byte status = 0;
        String sql = "select * from PermitDetails where permitnumber="+permitnumber+" and checkinstatus=1";
        List ProgramList=hibernateDao.findByNativeSql(sql, PermitDetails.class);
        if(ProgramList.size()>0 && ProgramList!=null)
        {
        	status =1;
        }
        return status;    
    }
	public List getPermitListAfterCheckIn(int permitNo)
    {

		logger.info("========getWeighmentList()========== permitNo"+permitNo);
		String sql = "select * from PermitDetails where permitnumber="+permitNo+" and checkinstatus=1";
		logger.info("========sql=========="+sql);
		return hibernateDao.findByNativeSql(sql, PermitDetails.class);
		
    }
	public List getWeighmentDetailsForAccMigration(String season)
	{
		//Added by DMurty on 25-02-2017
		String sql = "select * from WeighmentDetails where season='"+season+"' and actualserialnumber>0";
        return hibernateDao.findByNativeSql(sql, WeighmentDetails.class);
	}
	public int getPermitNoForPartLoad(int permitno)
    {
		int secPermitNo = 0;
		try
		{
			String sql = "select * from WeighmentDetails where secondpermitno="+permitno;
			List WeighmentList=hibernateDao.findBySqlCriteria(sql);
			if(WeighmentList.size()>0 && WeighmentList!=null)
	        {
				Map tempMap = new HashMap();
	        	tempMap = (Map) WeighmentList.get(0);
	        	secPermitNo = (Integer) tempMap.get("permitnumber");
	        }
			return secPermitNo;
		}
		catch(Exception e)
		{
	        return secPermitNo; 
		}
    }
	public List getHarvestingRateDetailsForUpdateHC(String season,String ryotCode,String finalDate)
    {
        String sql = "select * from HarvestingRateDetails where season ='" + season+"' and ryotcode='" + ryotCode+"' and effectivedate<='"+finalDate+"' and id=(select max(id) from HarvestingRateDetails where season ='"+season+"' and ryotcode='"+ryotCode+"')";// and effectivedate<='"+finalDate+"')";
        List harvestingRateDetailsList=hibernateDao.findByNativeSql(sql, HarvestingRateDetails.class);
        if(harvestingRateDetailsList.size()>0 && harvestingRateDetailsList!=null)
        	return harvestingRateDetailsList;
        else
        	return null;         
    }
}
