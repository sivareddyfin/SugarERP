package com.finsol.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccountSummaryTemp;
import com.finsol.model.Employees;
import com.finsol.model.WeighmentDetails;

@Repository("CommonDao")
@Transactional
public class CommonDao {
private static final Logger logger = Logger.getLogger(CommonDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
	
	public Object getCaneManagerID(String sql) {
		return hibernateDao.getID(sql);
	}
	
	public List getDropdownNames(String qry)
    {
		return hibernateDao.getColumns(qry);
    }
	
	public List getDropdownNamesForVillage(String qry)
    {
		return hibernateDao.findBySqlCriteria(qry);
    }
	
	public List getDropdownNamesForPermitNum(String qry)
    {
		return hibernateDao.findBySqlCriteria(qry);
    }
	
	
	//findBySql
	public List getMaxRyotCode(String qry)
    {
		return hibernateDao.findBySql(qry);
    }
	
	
	
	public List ValidateRyotCode(String qry)
    {
		return hibernateDao.findBySqlCriteria(qry);
    }
	
	public Object getEntityById(Integer id,Class className) 
	{		
		return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
	}
	
	public Object getEntityByIdForVillage(String id,Class className) 
	{		
		return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
	}
	
	public Object getCircleCodeFromAgreementDetails(String sql)
	{
		return hibernateDao.getID(sql);
	}
	
	public List AccSmryBalanceDetails(String AccCode,double TotalAmount,String TransactionType,String season)
	{
		List FinalList = new ArrayList();
		double RunnBal=0.0;
		String TransType = "Dr";
		
		String strQuery="Select balancetype,runningbalance from AccountSummary where accountcode='"+AccCode+"' and season='"+season+"'";
		List BalList = hibernateDao.getColumns(strQuery);
		if (BalList != null && BalList.size() > 0)
		{
			Object[] myResult = (Object[]) BalList.get(0);
			RunnBal = (Double) myResult[1];
			String BalType = (String) myResult[0];
			if(BalType.equalsIgnoreCase(TransactionType))
			{
				double Amt = TotalAmount;
				RunnBal = RunnBal+Amt;
				TransType = TransactionType;	
			}
			else
			{
				double Amt = TotalAmount;
				if(Amt>RunnBal)
				{
					RunnBal = Amt-RunnBal;
					TransType = TransactionType;
				}
				else if(RunnBal>Amt)
				{
					RunnBal = RunnBal-Amt;
					TransType = BalType;
				}
				else
				{
					RunnBal = 0.0;
					TransType = BalType;
				}
			}
			
		}
		else
		{
			RunnBal = TotalAmount;
			TransType = TransactionType;
		}
		
		Map TempMap = new HashMap();
		
		TempMap.put("RUNNING_BAL", RunnBal);
		TempMap.put("TRANS_TYPE", TransType);
		FinalList.add(TempMap);
		
		return FinalList;
	}	
	public List getAccCodes(String AccCode)
	{
		String strQuery="Select accountgroupcode,accountsubgroupcode from AccountMaster where accountcode='"+AccCode+"'";
		return hibernateDao.getColumns(strQuery);
	}
	public Object getEntityByIdVarchar(String id,Class className) 
	{		
		return (Object) hibernateDao.getSessionFactory().openSession().get(className, id);
	}
	public List AccGrpBalanceDetails(int AccGrpCode,double TotalAmt,String TransactionType,String season)
	{
		List FinalList = new ArrayList();
		double RunnBal=0.0;
		String TransType = "Dr";
		
		String strQuery="Select balancetype,runningbalance from AccountGroupSummary where accountgroupcode="+AccGrpCode+" and season='"+season+"'";
		List BalList = hibernateDao.getColumns(strQuery);
		if (BalList != null && BalList.size() > 0)
		{
			Object[] myResult = (Object[]) BalList.get(0);
			RunnBal = (Double) myResult[1];
			String BalType = (String) myResult[0];
			if(BalType.equalsIgnoreCase(TransactionType))
			{
				double Amt = TotalAmt;
				RunnBal = RunnBal+Amt;
				TransType = TransactionType;	
			}
			else
			{
				double Amt = TotalAmt;
				if(Amt>RunnBal)
				{
					RunnBal = Amt-RunnBal;
					TransType = TransactionType;
				}
				else if(RunnBal>Amt)
				{
					RunnBal = RunnBal-Amt;
					TransType = BalType;
				}
				else
				{
					RunnBal = 0.0;
					TransType = BalType;
				}
			}

		}
		else
		{
			RunnBal = TotalAmt;
			TransType = TransactionType;
		}
		Map TempMap = new HashMap();
		
		TempMap.put("RUNNING_BAL", RunnBal);
		TempMap.put("TRANS_TYPE", TransType);
		FinalList.add(TempMap);
		
		return FinalList;
	}
	
	
	public List SubGrpBalanceDetails(int AccGrpCode,int AccSubGrpCode,double TotalAmt,String TransactionType,String season)
	{
		List FinalList = new ArrayList();
		double RunnBal=0.0;
		String TransType = "Dr";
	
		String strQuery="Select balancetype,runningbalance from AccountSubGroupSummary where accountgroupcode="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
		List BalList = hibernateDao.getColumns(strQuery);
		if (BalList != null && BalList.size() > 0)
		{
			Object[] myResult = (Object[]) BalList.get(0);
			RunnBal = (Double) myResult[1];
			String BalType = (String) myResult[0];
			logger.info("========RunnBal=========="+RunnBal);
			logger.info("========BalType=========="+BalType);
			
			if(BalType.equalsIgnoreCase(TransactionType))
			{
				double Amt = TotalAmt;
				RunnBal = RunnBal+Amt;
				TransType = TransactionType;	
			}
			else
			{
				double Amt = TotalAmt;
				if(Amt>RunnBal)
				{
					RunnBal = Amt-RunnBal;
					TransType = TransactionType;
				}
				else if(RunnBal>Amt)
				{
					RunnBal = RunnBal-Amt;
					TransType = BalType;
				}
				else
				{
					RunnBal = 0.0;
					TransType = BalType;
				}
			}

		}
		else
		{
			RunnBal = TotalAmt;
			TransType = TransactionType;
		}

		Map TempMap = new HashMap();
		
		TempMap.put("RUNNING_BAL", RunnBal);
		TempMap.put("TRANS_TYPE", TransType);
		FinalList.add(TempMap);
		
		return FinalList;
	}
	
	public List AccDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountcode,cr,dr,glcode,runningbalance,status,user,season from AccountDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	public List BalDetails(String AccCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountSummary where accountcode='"+AccCode+"' and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}

	public void updateRunningBalinSmry(double finalAmt,String strCrDr,String AccCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'").executeUpdate();
	}
	
	public void updateStatusForAccDtls(int TransactionCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountDetails set status=0 WHERE transactioncode ="+TransactionCode+" and season='"+season+"'").executeUpdate();
	}
	
	public List SubGrpBalDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountgroupcode,cr,dr,runningbalance,status,user,accountsubgroupcode,season from AccountSubGroupDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	public List SubGrpBalDetails(int AccGrpCode,int AccSubGrpCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountSubGroupSummary where accountgroupcode="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}

	public void updateRunningBalinAccSubGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,int AccSubGrpCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'" ).executeUpdate();
	}
	
	public void updateStatusForAccSubGrpDtls(int TransactionCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountSubGroupDetails set status=0 WHERE transactioncode ="+TransactionCode+" and season='"+season+"'").executeUpdate();
	}
	
	
	public List AccGrpBalDetailsForRevert(int Transcode)
	{
		String strQuery="Select accountgroupcode,cr,dr,runningbalance,status,user,season from AccountGroupDetails where transactioncode='"+Transcode+"' and status=1";
		return hibernateDao.getColumns(strQuery);
	}
	
	public List AccGrpBalDetails(int AccGrpCode,String season)
	{
		String strQuery="Select balancetype,runningbalance from AccountGroupSummary where accountgroupcode="+AccGrpCode+" and season='"+season+"'";
		return hibernateDao.getColumns(strQuery);
	}
	
	public void updateRunningBalinAccGrpSmry(double finalAmt,String strCrDr,int AccGrpCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and season='"+season+"'").executeUpdate();
	}
	
	public void updateStatusForAccGrpDtls(int TransactionCode,String season)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AccountGroupDetails set status=0 WHERE transactioncode ="+TransactionCode+" and season='"+season+"'").executeUpdate();
	}
	
	public Object GetMaxTransCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	public void addAccountSmry(AccountSummary accountSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSummary);
		session.flush();
		session.close();
	}
	
	public void addAccountGrpDtls(AccountGroupDetails accountGroupDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountGroupDetails);
		session.flush();
		session.close();
	}
	
	public void addAccountGrpSmry(AccountGroupSummary accountGroupSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountGroupSummary);
		session.flush();
		session.close();
	}
	
	public void addAccountSubGrpDtls(AccountSubGroupDetails accountSubGroupDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSubGroupDetails);
		session.flush();
		session.close();
	}
	
	public void addAccountSubGrpSmry(AccountSubGroupSummary accountSubGroupSummary)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSubGroupSummary);
		session.flush();
		session.close();
	}
	
	public void addAccountDtls(AccountDetails accountDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountDetails);
		session.flush();
		session.close();
	}
	
	public Object getMaxColumnValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	public boolean saveMultipleEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public boolean saveMultipleEntitiesForFunctionalScreens(List entitiesList,List UpdateList) 
	{
		logger.info("========saveMultipleEntitiesForFunctionalScreens()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
			
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);

	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	 for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i).toString()=========="+entitiesList.get(i).toString());
	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i=========="+i);
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();
		        }		
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public boolean saveMultipleEntitiesForAgreement(List entitiesList,List UpdateList,String modifyFlag)
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	for(int i=0;i<entitiesList.size();i++)
	 	        {
	        		if("Yes".equalsIgnoreCase(modifyFlag))
        			{
        				session.saveOrUpdate(entitiesList.get(i));
        			}
        			else
        			{
        				session.save(entitiesList.get(i));
        			}	 	        
	        	}
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();	
		        }		
	        }
	                
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        
	    } 
	    catch (Exception e)
	    {
	        logger.fatal("Error is -  e.getMessage()");
	      //  transaction.rollback();
	       // throw e;
	    }
	    finally
	    {

	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    	return updateFlag;
	    }
	}
	
	public boolean saveMultipleEntitiesForScreensUpdateFirst(List entitiesList,List UpdateList) 
	{
		logger.info("========saveMultipleEntitiesForFunctionalScreens()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
			
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);

			if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i==========");
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();
		        }		
	        }
	       
			if(entitiesList != null && entitiesList.size()>0)
	        {
	        	for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i==========");
	     			logger.info("========entitiesList.get(i)=========="+entitiesList.get(i));

	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
			
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	
	public List getLoanRyotCode(String season,Integer branchcode)
    {
		String sql = "select loannumber,ryotcode from LoanDetails where season='"+season+"'  and  branchcode="+branchcode+"";
        List LoanRyotCodeList=hibernateDao.findBySqlCriteria(sql);
        if(LoanRyotCodeList.size()>0 && LoanRyotCodeList!=null)
        	return LoanRyotCodeList;
        else
        	return null;   
        
    }
	
	
	public List getAccountNumber(String ryotcode,String season)
    {
		String sql = "select accountnumber from AgreementSummary where ryotcode='"+ryotcode+"' and seasonyear='"+season+"'";
        List AccountNumberList=hibernateDao.findBySqlCriteria(sql);
        if(AccountNumberList.size()>0 && AccountNumberList!=null)
        	return AccountNumberList;
        else
        	return null;   
        
    }
	
	
	public List getMandal(Integer mandalcode)
    {
		String sql = "select mandal from Mandal  where mandalcode="+mandalcode+"";
        List MandalList=hibernateDao.findBySqlCriteria(sql);
        if(MandalList.size()>0 && MandalList!=null)
        	return MandalList;
        else
        	return null;   
        
    }
	
	public List getSearchData(String ryotcode,String agreementno,String village,int permitNo)
    {
		List SearchListData = new ArrayList();
		if(!"".equals(ryotcode) && !("NA".equalsIgnoreCase(ryotcode)) && ryotcode != null && !ryotcode.equalsIgnoreCase("null"))
		{
			String sql = "select distinct a.harvestdt,a.ryotcode,a.programno,a.agreementno,a.plotno,a.landvilcode,a.permitnumber,a.season,b.mandalcode,c.ryotname from PermitDetails a,AgreementDetails b,ryot c where a.ryotcode='"+ryotcode+"' and a.status='0' and a.agreementno=b.agreementnumber  and a.ryotcode=b.ryotcode and b.ryotcode=c.ryotcode";
	        SearchListData=hibernateDao.findBySqlCriteria(sql);
		}
		else if(!("".equalsIgnoreCase(agreementno)) && !("NA".equalsIgnoreCase(agreementno)) && agreementno != null && !agreementno.equalsIgnoreCase("null"))
		{
			String sql = "select distinct a.harvestdt,a.ryotcode,a.programno,a.agreementno,a.plotno,a.landvilcode,a.permitnumber,a.season,b.mandalcode,c.ryotname from PermitDetails a,AgreementDetails b,ryot c where a.agreementno='"+agreementno+"' and a.status='0' and a.agreementno=b.agreementnumber  and a.ryotcode=b.ryotcode and b.ryotcode=c.ryotcode";
	        SearchListData=hibernateDao.findBySqlCriteria(sql);
		}
		else if(!("".equalsIgnoreCase(village))  && !("NA".equalsIgnoreCase(village)) && village != null && !village.equalsIgnoreCase("null"))
		{
			String sql = "select distinct a.harvestdt,a.ryotcode,a.programno,a.agreementno,a.plotno,a.landvilcode,a.permitnumber,a.season,b.mandalcode,c.ryotname from PermitDetails a,AgreementDetails b,ryot c where a.landvilcode='"+village+"' and a.status='0' and a.agreementno=b.agreementnumber  and a.ryotcode=b.ryotcode and b.ryotcode=c.ryotcode";
	        SearchListData=hibernateDao.findBySqlCriteria(sql);
		}
		else if(permitNo>0)
		{
			String sql = "select distinct a.harvestdt,a.ryotcode,a.programno,a.agreementno,a.plotno,a.landvilcode,a.permitnumber,a.season,b.mandalcode,c.ryotname from PermitDetails a,AgreementDetails b,ryot c where a.permitnumber="+permitNo+" and a.status='0' and a.agreementno=b.agreementnumber  and a.ryotcode=b.ryotcode and b.ryotcode=c.ryotcode";
	        SearchListData=hibernateDao.findBySqlCriteria(sql);
		}
		
		if(SearchListData.size()>0 && SearchListData!=null)
        	return SearchListData;
        else
        	return null;   
    }
	
	public List getRyotData(String season)
    {
		String sql = "select a.ryotcode,b.ryotname from AdvanceDetails a, Ryot b where a.ryotcode=b.ryotcode and a.isSeedlingAdvance=0 and a.season='"+season+"'";
		List RyotListData=hibernateDao.findBySqlCriteria(sql);
		if(RyotListData.size()>0 && RyotListData!=null)
        	return RyotListData;
        else
        	return null;   
        
    }
	
	public List getRyotForCaneLedgerReport(String season)
    {
		String sql = "select ryotcode,ryotname from AgreementDetails where seasonyear='"+season+"'";
		List RyotListData=hibernateDao.findBySqlCriteria(sql);
		if(RyotListData.size()>0 && RyotListData!=null)
        	return RyotListData;
        else
        	return null;   
        
    }
	
	
	public List getRyotAgreementData(String season,String ryotcode)
    {
		//Status added by DMurty on 10-08-2016
		String sql = "select agreementnumber from AgreementSummary where seasonyear='"+season+"' and ryotcode='"+ryotcode+"' and status = 0";
		List RyotAgreementtListData=hibernateDao.findBySqlCriteria(sql);
		if(RyotAgreementtListData.size()>0 && RyotAgreementtListData!=null)
        	return RyotAgreementtListData;
        else
        	return null;   
        
    }
	
	public List getPrintCaneWeighment(Integer serailNumber,String season)
    {
		String sql = "select distinct * from WeighmentDetails where serialnumber="+serailNumber+" and season='"+season+"'";
		List CaneWeighmentList=hibernateDao.findBySqlCriteria(sql);
		if(CaneWeighmentList.size()>0 && CaneWeighmentList!=null)
        	return CaneWeighmentList;
        else
        	return null;   
    }
	//Added by sahadeva on 08-08-2016
	public List getEmployeeData(String loginid)
    {
		String sql = "select * from Employees where loginid ='"+loginid+"'";
		List EmployeeData=hibernateDao.findBySqlCriteria(sql);
    	return EmployeeData;
    }
	public List getPermitRulesNew()
    {
        String sql = "select * from PermitRules";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getVariety(String season,String ryotCode)
    {
        String sql = "select * from AgreementDetails where seasonyear='"+season+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List<WeighmentDetails> getDataForCaneLedger(String season,String ryotCode)
    {
        String sql = "select * from WeighmentDetails where season='"+season+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
		
		Session  session = (Session)hibernateDao.getSessionFactory().openSession();
		
		Query q=session.createQuery("From WeighmentDetails where season='"+season+"' and ryotcode='"+ryotCode+"'");
	return 	q.list();
        /*return hibernateDao.findBySqlCriteria(sql);*/
    }
	public List getloanDetailsData(String season,String ryotCode)
    {
        String sql = "select * from LoanDetails where season='"+season+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAccountDetailsData(String season,String accCode)
    {
        String sql = "select sum(cr) cr,sum(dr) dr from AccountDetails where season='"+season+"' and accountcode='"+accCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAccountGroupData(String season,Integer accGroupCode)
    {
        String sql = "select sum(cr) cr,sum(dr) dr from AccountGroupDetails where season='"+season+"' and accountgroupcode="+accGroupCode+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAccSubGroupData(String season,Integer accSubGroupCode)
    {
        String sql = "select sum(cr) cr,sum(dr) dr from AccountSubGroupDetails where season='"+season+"' and accountsubgroupcode="+accSubGroupCode+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getloanSummaryData(String season,String ryotCode)
    {
        String sql = "select * from LoanSummary where season='"+season+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAgreementdetailsData(String season,String ryotCode)
    {
        String sql = "select progressiveqty,agreedqty,isnull(extentsize,0) as plant,plantorratoon,ryotname,villagecode,relativename from AgreementDetails where seasonyear='"+season+"' and ryotcode='"+ryotCode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	//Added by DMurty on 26-10-2016
	public boolean saveMultipleEntitiesForFunctionalScreensForAccounting(List entitiesList,List UpdateList,List AccountsList) 
	{
		logger.info("========saveMultipleEntitiesForFunctionalScreensForTest()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
			
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);
			logger.info("========AccountsList==========" +AccountsList);

	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	 for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i)=========="+entitiesList.get(i));
	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
	        
	        if(AccountsList != null && AccountsList.size()>0)
	        {
	        	 for(int i=0;i<AccountsList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i)=========="+AccountsList.get(i));
	 	        	session.save(AccountsList.get(i));
	 	        }
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i=========="+i);
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createSQLQuery(strQry).executeUpdate();
		        }		
	        }
	        
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	//Added by DMurty on 09-11-2016
	public void addAccountSmryTemp(AccountSummaryTemp accountSummaryTemp)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSummaryTemp);
		session.flush();
		session.close();
	}
	
	public Object GetMaxRank(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }

	public List listGetReceiptDetails(String frmdate,String Todate,Integer Shift,int lorryStatus)
	{
		//Modified by DMurty on 29-11-2016
		String sql= null;
		if(lorryStatus == 0)
		{
			sql="select netwt,serialnumber,permitnumber,ryotcode,ryotname from WeighmentDetails where canereceiptdate between '" + frmdate+"' and '" + Todate+"' and shiftid="+Shift+"  ";
		}
		else
		{
			sql="select netwt,serialnumber,permitnumber,ryotcode,ryotname from WeighmentDetails where canereceiptdate between '" + frmdate+"' and '" + Todate+"' AND shiftid1="+Shift+"  ";
		}
		logger.info("listGetReceiptDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List listGetCirclesFromZones(Integer Zonecode)
	{
		String sql="select distinct A.circlecode,C.circle from AgreementDetails A,Circle C where A.zonecode="+Zonecode+" and A.circlecode=C.circlecode ";
		logger.info("listGetMandalsByZones() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
		

	public List getRyotFromCircle(Integer circlecode)
    {
        String sql = "select distinct ryotcode,ryotname from  ryot where circleCode="+circlecode+" and status=0";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	
	
	public int getMaxSlNoForWeighment(String shiftdate,String season)
    {
		int maxVal=0;
		String sql = "select max(actualserialnumber)+1 as maxno from WeighmentDetails where shiftdate='"+shiftdate+"' and season='"+season+"'";
		logger.info("========getMaxSlNoForWeighment()========== sql--"+sql);			
		List maxList = hibernateDao.findBySqlCriteria(sql);
		if(maxList != null && maxList.size()>0)
		{
			Map tempMap = (Map) maxList.get(0);
			if (tempMap.get("maxno") == null)
			{
				maxVal = 1;
			}
			else
			{
				maxVal = (Integer) tempMap.get("maxno");
			}
		}
		logger.info("========getMaxSlNoForWeighment()========== maxVal--"+maxVal);			
        return maxVal;
    }
	//Added by DMurty on 02-12-2016
	public List getEmployeeDetails(String loginUser)
    {
        String sql = "select * from Employees where loginid='" + loginUser+"'";
        List loginUserList= hibernateDao.findByNativeSql(sql, Employees.class);
        if(loginUserList.size()>0 &&loginUserList!=null)
        	return loginUserList;
        else
        	return null; 
    }
	
	public List getPermits(String season,String ryotCode)
	{
		String sql="select * from PermitDetails where ryotcode='"+ryotCode+"' and season='"+season+"'";
		logger.info("getPermits() ========sql=========="+sql);
		List permitList = hibernateDao.findBySqlCriteria(sql);
		 if(permitList.size()>0 &&permitList!=null)
	        	return permitList;
	        else
	        	return null; 
	}
	public List getDateByCaneslno(String season, Integer caneSlNo)
	{
		String sql="select * from CaneAccountSmry ";
		logger.info("listGetMandalsByZones() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanSummaryData(String season,String ryotcode)
    {
        String sql = "select loannumber,branchcode,disbursedamount,interestamount,disburseddate from LoanDetails where season='"+season+"' and ryotcode='"+ryotcode+"' and disburseddate is not null  ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getCaneAccLoanData(String season,String ryotcode)
    {
        String sql = "select distinct advancecode from AdvanceSummary where season='"+season+"' and ryotcode='"+ryotcode+"'";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	
	
	public List getCaneAccLoanData1(String season,String ryotcode,Integer caneNo)
    {
        String sql = "select advisedamt,finalinterest from CanAccLoansDetails where season='"+season+"' and ryotcode='"+ryotcode+"' and advloan =1 and caneacctslno<"+caneNo+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getCaneAccLoanData2(String season,String ryotcode,Integer caneNo)
    {
        String sql = "select advanceamount, advisedamt,finalinterest,adjustedprincipleamount,accountnum from CanAccLoansDetails where season='"+season+"' and ryotcode='"+ryotcode+"' and advloan =1 and caneacctslno="+caneNo+"";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getAdvPrncAmt(String season)
    {
        String sql = "select * from AdvancePrincipleAmounts where season='"+season+"' and  principle>0 order by advancecode,ryotcode ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List GetAdvanceAmt(String season,String ryotCode,Integer advCode)
    {
        String sql = "select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+" ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getLoanPrncData(String season)
    {
        String sql = "select * from LoanPrincipleAmounts where season='"+season+"' and  principle>10 order by branchcode,loannumber ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getDateByCaneslnoForUpdation(String season, Integer caneSlNo)
	{
		String sql="select * from CaneAccountSmry where season='"+season+"' and caneacslno="+caneSlNo+"";
		logger.info("listGetMandalsByZones() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getLoanSummaryDataForYearlyLedger(String season,String ryotcode)
    {
        String sql = "select ld.loannumber,ld.branchcode,ld.disbursedamount,ls.interestamount,ld.disburseddate,ld.referencenumber from LoanDetails as ld,LoanSummary as ls where ld.loannumber=ls.loannumber and  ld.season=ls.season and ld.ryotcode=ls.ryotcode and ld.season='"+season+"' and ld.ryotcode='"+ryotcode+"' and ld.disburseddate is not null  ";
		logger.info("========sql=========="+sql);
        return hibernateDao.findBySqlCriteria(sql);
    }
	public List getZonesListByZone(int zonecode)
	{
		String sql="select zone,zonecode from Zone where zonecode="+zonecode+"";
		logger.info("listGetMandalsByZones() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public String saveMultipleEntitiesForSeedSource(List entitiesList,List UpdateList,String modifyFlag)
	{
	    Session session = null;
	    Transaction transaction = null;
	    String updateFlag="false";
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	for(int i=0;i<entitiesList.size();i++)
	 	        {
	        		if("Yes".equalsIgnoreCase(modifyFlag))
        			{
        				session.saveOrUpdate(entitiesList.get(i));
        			}
        			else
        			{
        				session.save(entitiesList.get(i));
        			}	 	        
	        	}
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();	
		        }		
	        }
	                
	        session.flush();
	        transaction.commit();
	        updateFlag ="true";
	        
	    } 
	    catch (Exception e)
	    {
	    	if (e instanceof SQLIntegrityConstraintViolationException) {
				logger.info(e.getCause(), e);
				return "duplicateBatch";
		    } else {
	        logger.fatal("Error is -  e.getMessage()");
	        return updateFlag;
		    }
	      //  transaction.rollback();
	       // throw e;
	    }
	    finally
	    {

	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    	return updateFlag;
	    }
	}
}
