package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountGroup;
import com.finsol.model.CaneManager;



@Repository("AccountGroupDao")
@Transactional
public class AccountGroupDao 
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addAccountGroup(AccountGroup accountGroup) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountGroup);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<AccountGroup> listAccountGroups() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<AccountGroup> accountGroups=session.createCriteria(AccountGroup.class).list();
		session.close();
		return accountGroups;
	}
	
	
	
	
	

}
