package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.HCSuretyDetails;
import com.finsol.model.HarvestingContractors;
import com.finsol.model.TCSuretyDetails;
import com.finsol.model.TransportingContractors;

@Repository("TransportingContractorDao")
@Transactional
public class TransportingContractorDao {

	
	private static final Logger logger = Logger.getLogger(TransportingContractorDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addTransportingContractor(TransportingContractors transportingContractor) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(transportingContractor);
		session.flush();
		session.close();
	}
	
	public void addTCSuretyDetails(TCSuretyDetails tcSuretyDetails) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(tcSuretyDetails);
		session.flush();
		session.close();
	}
	
	

	@SuppressWarnings("unchecked")
	public List<TransportingContractors> lisTransportingContractors() 
	{
		return (List<TransportingContractors>) hibernateDao.getSessionFactory().openSession().createCriteria(TransportingContractors.class).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<TCSuretyDetails> listTCSuretyDetails() 
	{
		return (List<TCSuretyDetails>) hibernateDao.getSessionFactory().openSession().createCriteria(TCSuretyDetails.class).list();
	}
	
	public void deleteTCSuretyDetails(TransportingContractors transportingContractor) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM TCSuretyDetails WHERE transportercode = "+transportingContractor.getTransportercode()).executeUpdate();
	}
	
	
	public TransportingContractors getTransportingContractors(int id) {
		
		return (TransportingContractors) hibernateDao.getSessionFactory().openSession().get(TransportingContractors.class, id);
	}
	
	public List<TCSuretyDetails> getTCDetailsByCode(Integer tcode)
    {
        String sql = "select * from TCSuretyDetails where transportercode =" + tcode;
        
        return hibernateDao.findByNativeSql(sql, TCSuretyDetails.class);
    }

}
