package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.WeighBridge;


@Repository("WeighBridgeDao")
@Transactional


public class WeighBridgeDao 
{
	private static final Logger logger = Logger.getLogger(WeighBridgeDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	public void addWeighBridge(WeighBridge weighBridge) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(weighBridge);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<WeighBridge> listWeighBridge() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<WeighBridge> weighBridges=session.createCriteria(WeighBridge.class).list();
		session.close();
		return weighBridges;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}

}
