package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.Circle;


@Repository("CircleDao")
@Transactional
public class CircleDao
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addCircle(Circle circle) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(circle);
		session.flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<Circle> listCircles() 
	{
		return (List<Circle>) hibernateDao.getSessionFactory().openSession().createCriteria(Circle.class).list();
	}
	
	
	public List getFieldAssistantNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
	public List getVillageNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
	
	public List listCircleNew(String qry)
    {
		return hibernateDao.findBySqlCriteria(qry);
    }
	
	
	
}
