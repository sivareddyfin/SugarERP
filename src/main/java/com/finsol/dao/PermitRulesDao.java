package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.LandType;
import com.finsol.model.PermitRules;

@Repository("PermitRulesDao")
@Transactional
public class PermitRulesDao {
	
	private static final Logger logger = Logger.getLogger(PermitRulesDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addPermitRules(PermitRules permitRules) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(permitRules);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<PermitRules> listPermitRules() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<PermitRules> permitRuless=session.createCriteria(PermitRules.class).list();
		session.close();
		return permitRuless;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
}
