package com.finsol.dao;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finsol.controller.LoginController;
import com.finsol.model.Employee;
import com.finsol.model.Users;
/**
 * @author Rama Krishna
 *
 */
@Repository("userDao")
public class UserDaoImpl {
	
	private static final Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	@Autowired
	private HibernateDao hibernateDao;
	
	public Users getUser(String name) {
		
		logger.info("----------------In  UserDaoImpl-----------");
		return (Users) hibernateDao.getSessionFactory().openSession().get(Users.class, 1);
		
/*		logger.info("----------------In  UserDaoImpl-----------"+name);
		Transaction tx=sessionFactory.getCurrentSession().beginTransaction();
		tx.begin();
		User user=(User) sessionFactory.getCurrentSession().get(User.class,Integer.parseInt("1"));
		tx.commit();
		return user;*/
	}

}
