package com.finsol.dao;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.GrnDetails;
import com.finsol.model.GrnSummary;
import com.finsol.model.ProductGroup;
import com.finsol.model.ProductMaster;
import com.finsol.model.ProductSubGroup;
import com.finsol.model.SDCDetails;
import com.finsol.model.SDCSummary;
import com.finsol.model.StockInDetails;
import com.finsol.model.StockInSummary;
import com.finsol.model.StockIssueDtls;
import com.finsol.model.StockIssueSmry;
import com.finsol.model.StockReqDtls;
import com.finsol.model.StockReqSmry;
import com.finsol.model.Uom;

@Repository("StoreDao")
@Transactional
public class StoreDao 
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	@SuppressWarnings("unchecked")
	public List<Uom> getAllUOM()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Uom> uom=session.createCriteria(Uom.class).list();
		session.close();
		return uom;
	}
	@SuppressWarnings("unchecked")
	public List<ProductGroup> getAllProductGroups()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<ProductGroup> productGroups=session.createCriteria(ProductGroup.class).list();
		session.close();
		return productGroups;
	}
	@SuppressWarnings("unchecked")
	public List<ProductSubGroup> getAllProductSubGroups()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<ProductSubGroup> productSubGroups=session.createCriteria(ProductSubGroup.class).list();
		session.close();
		return productSubGroups;
	}
	
	@SuppressWarnings("unchecked")
	public List getAllProducts(String jsonData)
	{	
		String sql="";
		if("0".equals(jsonData))
		{
		 sql = "select * from productMaster order by categoryCode  ";
		}
		else
		{
			 sql = "select * from productMaster where subCategoryCode ='" +jsonData+"' order by categoryCode  ";
		}
		return hibernateDao.findBySqlCriteria(sql);

		//return hibernateDao.findByNativeSql(sql, ProductMaster.class);
	}
	@SuppressWarnings("unchecked")
	public List<ProductMaster> getProductsNameByCode(String jsonData)
	{	
		String sql="";
		
		 sql = "select * from productMaster where productCode ='" +jsonData+"'  ";
		
		return hibernateDao.findByNativeSql(sql, ProductMaster.class);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public boolean saveMultipleEntities(List entitiesList) 
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        for(int i=0;i<entitiesList.size();i++)
	        {
	        	session.saveOrUpdate(entitiesList.get(i));	        	
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public boolean saveMultipleEntitiesForFunctionalScreens(List entitiesList,List UpdateList) 
	{
		logger.info("========saveMultipleEntitiesForFunctionalScreens()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
			
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);

	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	 for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i).toString()=========="+entitiesList.get(i).toString());
	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i=========="+i);
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();
		        }		
	        }
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public boolean saveMultipleEntitiesForScreensUpdateFirst(List entitiesList,List UpdateList) 
	{
		logger.info("========saveMultipleEntitiesForFunctionalScreens()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
			
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);

			if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i==========");
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();
		        }		
	        }
	       
			if(entitiesList != null && entitiesList.size()>0)
	        {
	        	for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i==========");
	     			logger.info("========entitiesList.get(i)=========="+entitiesList.get(i));

	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
			
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public String getGroupName(String groupid)
    {
		String sql = "select groupName  from ProductGroup where productGroupCode='"+groupid+"' ";
		logger.info("========sql=========="+sql);
		
        String groupname=(String)hibernateDao.getID(sql);
        return groupname;
    }
	public List getSubGroupsListByGrp(String groupid)
    {
		String sql = "select *  from ProductSubGroup where productGroupCode='"+groupid+"' ";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);

    }
	
	public List getGroupsList()
    {
		String sql = "select distinct productGroupCode, groupName  from ProductGroup ";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);

    }
	public List getUomsList()
    {
		String sql = "select * from Uom ";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getStockMethodList()
    {
		String sql = "select * from StockMethods ";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
    }
	public List getStockMethodListById(Integer id)
    {
		String sql = "select * from StockMethods where stockMethodCode='"+id+"'";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getUomNameById(Integer uomcode)
    {
		String sql = "select * from Uom where uomCode="+uomcode+"  ";
		logger.info("========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
    }
	
	public List getStockSupplyDetails(String fromdate, String todate,Integer department)
	{
		if("All".equals(department))
		{
			String sql="select * from StockInSummary where stockdate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getStockSupplyDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			String sql="select * from StockInSummary where fromdeptid='" +department+"' and stockdate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getStockSupplyDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
	}
	
	public List getProductCodes(String producttype)
	{
			String sql="select productCode from ProductMaster where productCode like '"+producttype +"%'   ";
			logger.info("getProductCodes() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
	}
	public List getProductDetailsBycode(String producttype)
	{
			String sql="select * from ProductMaster where productCode ='"+ producttype +"'   ";
			logger.info("getProductDetailsBycode() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
	}
	public List getQuantityForStockInNo(String stockinno)
	{
			String sql="select * from StockInDetails where stockinno='"+stockinno+"'  ";
			logger.info("getStockSupplyDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		
	}
	public void deleteDetails(String Supplyid,String tablename)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM "+tablename+" WHERE stockinno = '"+Supplyid+"' " ).executeUpdate();
	}
	public void deleteDetailstable(String data,String tablename,String column)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM "+tablename+" WHERE "+column+" = '"+data+"' " ).executeUpdate();
	}
	
	public List<StockInSummary> listAllStockInSummary(String stockInNo)
	{
			String sql="select * from StockInSummary where stockinno = '"+stockInNo+"' ";
			logger.info("listAllStockInSummary() ========sql=========="+sql);
				return hibernateDao.findByNativeSql(sql, StockInSummary.class);
	}
	public List<StockInDetails> listAllStockInDetails(String stockInNo)
	{
			String sql="select * from StockInDetails where stockinno = '"+stockInNo+"' ";
			logger.info("listAllStockInDetails() ========sql=========="+sql);
			return hibernateDao.findByNativeSql(sql, StockInDetails.class);
	}
	
	public List<StockReqSmry> listAllStockReqSummary(String stockInNo)
	{
			String sql="select * from StockReqSmry where stockreqno = '"+stockInNo+"' ";
			logger.info("listAllStockInSummary() ========sql=========="+sql);
				return hibernateDao.findByNativeSql(sql, StockReqSmry.class);
	}
	public List<StockReqDtls> listAllStockReqDetails(String stockInNo)
	{
			String sql="select * from StockReqDtls where stockreqno = '"+stockInNo+"' ";
			logger.info("listAllStockInDetails() ========sql=========="+sql);
			return hibernateDao.findByNativeSql(sql, StockReqDtls.class);
	}
	
	public List<StockIssueSmry> listAllStockIssueSummary(String stockIssueNo)
	{
			String sql="select * from StockIssueSmry where stockissueno = '"+stockIssueNo+"' ";
			logger.info("listAllStockIssueSummary() ========sql=========="+sql);
				return hibernateDao.findByNativeSql(sql, StockIssueSmry.class);
	}
	public List<StockIssueDtls> listAllStockIssueDetails(String stockIssueNo)
	{
			String sql="select * from StockIssueDtls where stockissueno = '"+stockIssueNo+"' ";
			logger.info("listAllStockIssueDetails() ========sql=========="+sql);
			return hibernateDao.findByNativeSql(sql, StockIssueDtls.class);
	}
	
	
	public boolean saveandupdateMultipleEntities(List entitiesList,List UpdateList,String modifyFlag)
	{
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        
	        if(entitiesList != null && entitiesList.size()>0)
	        {
	        	for(int i=0;i<entitiesList.size();i++)
	 	        {
	        		if("Yes".equalsIgnoreCase(modifyFlag))
        			{
        				session.saveOrUpdate(entitiesList.get(i));
        			}
        			else
        			{
        				session.save(entitiesList.get(i));
        			}	 	        
	        	}
	        }
	       
	        if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	String strQry = Qry.toString();
		        	session.createQuery(strQry).executeUpdate();	
		        }		
	        }
	                
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        
	    } 
	    catch (Exception e)
	    {
	        logger.fatal("Error is -  e.getMessage()");
	      //  transaction.rollback();
	       // throw e;
	    }
	    finally
	    {

	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    	return updateFlag;
	    }
	}
	
	public boolean saveMultipleEntities(List entitiesList,List UpdateList,List AccountsList) 
	{
		logger.info("========saveMultipleEntities()==========");
	    Session session = null;
	    Transaction transaction = null;
	    boolean updateFlag=false;
	    try 
	    {
	        session = (Session)hibernateDao.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        logger.info("========entitiesList=========="+entitiesList );
			logger.info("========UpdateList==========" +UpdateList);
			logger.info("========AccountsList==========" +AccountsList);
			
			if(UpdateList != null && UpdateList.size()>0)
	        {
	        	for(int i=0;i<UpdateList.size();i++)
		        {
		        	Object Qry = UpdateList.get(i);
		        	logger.info("========i=========="+i);
	     			logger.info("========Qry=========="+Qry);
		        	String strQry = Qry.toString();
		        	session.createSQLQuery(strQry).executeUpdate();
		        }		
	        }
			
		   if(entitiesList != null && entitiesList.size()>0)
	        {
	        	 for(int i=0;i<entitiesList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i)=========="+entitiesList.get(i));
	 	        	session.saveOrUpdate(entitiesList.get(i));
	 	        }
	        }
	        
	        if(AccountsList != null && AccountsList.size()>0)
	        {
	        	 for(int i=0;i<AccountsList.size();i++)
	 	        {
	     			logger.info("========i=========="+i);
	     			logger.info("========entitiesList.get(i)=========="+AccountsList.get(i));
	 	        	session.save(AccountsList.get(i));
	 	        }
	        }
	        
	        session.flush();
	        transaction.commit();
	        updateFlag =true;
	        return updateFlag;
	    } 
	    catch (JDBCException jde)
	    {
	        logger.fatal("Error occured in database communication", jde);
	        transaction.rollback();
	        return updateFlag;
	    }
	    finally
	    {
	        if (session.isOpen()) 
	        {
	            session.close();
	        }
	    }
	}
	
	public List getStockIndentDetails(String fromdate, String todate,Integer department,Integer userstatus)
	{
		String dept="NA";
		String sql="";
		if(department==0)
		{
			dept="All";
		}
		
		if("All".equals(dept))
		{
			
			if(userstatus==0)
			{
			 sql="select * from StockReqSmry where reqdate between '" + fromdate+"' and '" + todate+"' and status ="+userstatus+" ";
			}
			else
			{
			 sql="select * from StockReqSmry where reqdate between '" + fromdate+"' and '" + todate+"' and status !=3 ";
			}
		}
		else
		{
			if(userstatus==0)
			{
			 sql="select * from StockReqSmry where deptid='" +department+"' and reqdate between '" + fromdate+"' and '" + todate+"'  and status ="+userstatus+"  ";
			}
			else
			{
			 sql="select * from StockReqSmry where deptid='" +department+"' and reqdate between '" + fromdate+"' and '" + todate+"' and status !=3  ";	
			}
		}
		logger.info("getStockIndentDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	public List getStockIssueDtls(String fromdate, String todate,Integer department)
	{
		if("All".equals(department))
		{
			String sql="select * from StockIssueSmry where issuedate between '" + fromdate+"' and '" + todate+"' and status!=4 ";
			logger.info("getStockIndentDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			String sql="select * from StockIssueSmry where deptid='" +department+"' and issuedate between '" + fromdate+"' and '" + todate+"' and status!=4  ";
			logger.info("getStockIndentDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
	}
	
	public List getQuantityForStockIndent(String stockReqNo)
	{
			String sql="select * from StockReqDtls where stockreqno='"+stockReqNo+"'  ";
			logger.info("getQuantityForStockIndent() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		
	}
	
	
	
	public List getQuantityForStockIssue(String StockIssueNo)
	{
			String sql="select * from StockIssueDtls where stockissueno='"+StockIssueNo+"'  ";
			logger.info("getQuantityForStockIndent() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		
	}
	
	public List getTotalQuantityForStockIssueByProductcode(String StockIssueNo,String ProductCode)
	{
			String sql="select * from StockIssueDtls where stockissueno='"+StockIssueNo+"'  and productcode='"+ProductCode+"' ";
			logger.info("getQuantityForStockIndent() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		
	}
	
	public List listGetEmployeeType(String loginid)
	{
		String sql="select empStatus from Employees where  UPPER(loginid) = '"+loginid.toUpperCase()+"'  ";
		logger.info("listGetEmployeeType() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List listGetIndentDetails(String stockReqNo,String Productcode)
	{
		String sql="select * from StockReqDtls where stockreqno = '"+stockReqNo+"' and productcode='"+Productcode+"' ";
		logger.info("listGetIndentDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getGrnsDtls(String fromdate, String todate,Integer department)
	{
		if("All".equals(department))
		{
			String sql="select * from GrnSummary where grndate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getGrnsDtls() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			String sql="select * from GrnSummary where deptid='" +department+"' and grndate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getGrnsDtls() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
	}
	public List getQuantityForGrn(String grnNo)
	{
			String sql="select * from GrnDetails where grnno='"+grnNo+"'  ";
			logger.info("getQuantityForGrn() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List<GrnSummary> listAllGrnSummary(String grnNo)
	{
			String sql="select * from GrnSummary where grnno = '"+grnNo+"' ";
			logger.info("listAllGrnSummary() ========sql=========="+sql);
				return hibernateDao.findByNativeSql(sql, GrnSummary.class);
	}
	public List<GrnDetails>  listAllGrnDetails(String grnNo)
	{
			String sql="select * from GrnDetails where grnno = '"+grnNo+"' ";
			logger.info("listAllGrnDetails() ========sql=========="+sql);
			return hibernateDao.findByNativeSql(sql, GrnDetails.class);
	}
	
	//Added by DMurty on 21-03-2017
	public List<SDCSummary> listAllSDCSummary(String dcNo)
	{
		String sql="select * from SDCSummary where sdcno = '"+dcNo+"' ";
		logger.info("listAllSDCSummary() ========sql=========="+sql);
		return hibernateDao.findByNativeSql(sql, SDCSummary.class);
	}
	
	public List<SDCDetails> listAllSDcDetails(String dcNo)
	{
			String sql="select * from SDCDetails where sdcno = '"+dcNo+"'";
			logger.info("listAllSDcDetails() ========sql=========="+sql);
			return hibernateDao.findByNativeSql(sql, SDCDetails.class);
	}
	
	public List getSDCDetails(String fromdate, String todate,Integer department)
	{
		if("All".equals(department))
		{
			String sql="select * from SDCSummary where sdcdate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getSDCDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
		else
		{
			String sql="select * from SDCSummary where deptid='" +department+"' and sdcdate between '" + fromdate+"' and '" + todate+"'  ";
			logger.info("getSDCDetails() ========sql=========="+sql);
			return hibernateDao.findBySqlCriteria(sql);
		}
	}
	
	
	public List getIndentDetailsForPrint(String requestNo)
	{
		String sql="select * from StockReqDtls where stockreqno='"+requestNo+"'";
		logger.info("getIndentDetailsForPrint() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	
	
	public List getQuantityForDcDetails(String sdcno)
	{
		String sql="select * from SDCDetails where sdcno='"+sdcno+"'";
		logger.info("getQuantityForDcDetails() ========sql=========="+sql);
		return hibernateDao.findBySqlCriteria(sql);
	}
	

}
