package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Bank;
import com.finsol.model.CaneManager;
import com.finsol.model.SampleCardRules;

@Repository("SampleCardRulesDao")
@Transactional
public class SampleCardRulesDao {
	
	private static final Logger logger = Logger.getLogger(SampleCardRulesDao.class);
	@Autowired	
	private HibernateDao hibernateDao;	
	
	public void addSampleCardRules(SampleCardRules sampleCardRules) {
		
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(sampleCardRules);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<SampleCardRules> listSampleCardRules() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<SampleCardRules> sampleCardRuless=session.createCriteria(SampleCardRules.class).list();
		session.close();
		return  sampleCardRuless;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
	public void truncateSampleCardRules() {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SampleCardRules").executeUpdate();
	}

}
