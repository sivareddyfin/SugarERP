package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Zone;

@Repository("ZoneDao")
@Transactional
public class ZoneDao {
private static final Logger logger = Logger.getLogger(ZoneDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addZone(Zone zone) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(zone);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Zone> listZones() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Zone> zones=session.createCriteria(Zone.class).list();
		session.close();
		return zones;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
	public List getRegionNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
}
