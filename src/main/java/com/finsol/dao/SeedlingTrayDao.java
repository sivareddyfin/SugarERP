package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.SeedlingTray;
import com.finsol.model.SeedlingTrayChargeByDistance;

@Repository("SeedlingTrayDao")
@Transactional
public class SeedlingTrayDao {

	
	private static final Logger logger = Logger.getLogger(SeedlingTrayDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addSeedlingTray(SeedlingTray seedlingTray) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(seedlingTray);
		session.flush();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<SeedlingTray> listSeedlingTray()
	{
		return (List<SeedlingTray>) hibernateDao.getSessionFactory().openSession().createCriteria(SeedlingTray.class).list();
	}
	public SeedlingTray getSeedlingTray(int id) {
		
		return (SeedlingTray) hibernateDao.getSessionFactory().openSession().get(SeedlingTray.class, id);
	}
	
	public List<SeedlingTrayChargeByDistance> getSeedlingTrayChargeByDistance(Integer traycode)
    {
        String sql = "select * from SeedlingTrayChargeByDistance where traycode =" + traycode;
        List<SeedlingTrayChargeByDistance> lst=hibernateDao.findByNativeSql(sql, SeedlingTrayChargeByDistance.class);
        return lst;
    }
	public void truncateSeedlingTrayChargesByDistance(Integer trayCode) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SeedlingTrayChargeByDistance where traycode="+trayCode+"").executeUpdate();
	}
	
}
