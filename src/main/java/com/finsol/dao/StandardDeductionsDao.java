package com.finsol.dao;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Ryot;
import com.finsol.model.StandardDeductions;


@Repository("StandardDeductionsDao")
@Transactional

public class StandardDeductionsDao
{
	private static final Logger logger = Logger.getLogger(StandardDeductionsDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	public void addStandardDeductions(StandardDeductions standardDeductions) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(standardDeductions);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	 @SuppressWarnings("unchecked")
		public List<StandardDeductions> listStandardDeduction() 
		{	
		 	Session session=(Session)hibernateDao.getSessionFactory().openSession();
		 	List<StandardDeductions> standardDeductionss=session.createCriteria(StandardDeductions.class).list();
		 	session.close();
			return standardDeductionss;
		}
	 
	 public void updateStandardDeductionDetails(String name,int id,int orderId) {
			hibernateDao.getSessionFactory().openSession().createQuery("UPDATE StandardDeductions set name='"+name+"',deductionOrder="+orderId+" WHERE id ="+id).executeUpdate();
		}
}
