package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Village;


@Repository("VillageDao")
@Transactional
public class VillageDao {
	private static final Logger logger = Logger.getLogger(VillageDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addVillage(Village village) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(village);
		session.flush();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Village> listVillages()
	{
		return (List<Village>) hibernateDao.getSessionFactory().openSession().createCriteria(Village.class).list();
	}
	
	public List getMandalNames(String qry)
    {
		return hibernateDao.getColumns(qry);
    }
	
	
	public List listVillagesNew(String qry)
    {
		return hibernateDao.findBySqlCriteria(qry);
    }
}
