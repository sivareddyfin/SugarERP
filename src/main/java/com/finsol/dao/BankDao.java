package com.finsol.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.Circle;
import com.finsol.model.Employee;


@Repository("BankDao")
@Transactional
public class BankDao {
	private static final Logger logger = Logger.getLogger(BankDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addBank(Bank bank) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(bank);
		session.flush();
		session.close();
	}
	
	public void addBranch(Branch branch) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(branch);
		session.flush();
		session.close();
	}
	
/*	public void deleteBranch(Branch branch) {
		hibernateDao.getSessionFactory().getCurrentSession().createQuery("DELETE FROM Branch WHERE bankcode = "+branch.getBankcode()).executeUpdate();
	}*/
	
	@SuppressWarnings("unchecked")
	public List<Bank> listBanks() 
	{
		return (List<Bank>) hibernateDao.getSessionFactory().openSession().createCriteria(Bank.class).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Branch> listBranches() 
	{
		return (List<Branch>) hibernateDao.getSessionFactory().openSession().createCriteria(Branch.class).list();
	}
	
	public List getBank(int id) 
	{
		String sql="select * from Bank where bankcode="+id;
		return hibernateDao.findBySqlCriteria(sql);
		//return (Bank) hibernateDao.getSessionFactory().openSession().get(Bank.class, id);
	}
	
	public List<Branch> getBranchByBankCode(Integer bankcode)
    {
        String sql = "select * from Branch where bankcode =" + bankcode;
        
        return hibernateDao.findByNativeSql(sql, Branch.class);
    }
	

	public void deleteBranch(Bank bank) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Branch WHERE bankcode = "+bank.getBankcode()).executeUpdate();
	}
	public Integer getBranchGlcode(Integer branchcode)
	{
		Integer glcode=0;
		Session s=hibernateDao.getSessionFactory().openSession();
		List branchList=null;
		try
		{
			SQLQuery query =s.createSQLQuery("select glcode from Branch where branchcode="+branchcode);
			branchList=query.list();
			if(branchList.size()>0 && branchList != null)
			{
				glcode=((BigDecimal)branchList.get(0)).intValue();
			}
		}
		catch(Exception e)
		{
			System.out.println("The exception is"+e);
		}
		return glcode;
	}
	
}
