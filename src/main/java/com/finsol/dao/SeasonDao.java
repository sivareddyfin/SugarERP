package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Season;


@Repository("SeasonDao")
@Transactional
public class SeasonDao 
{
	private static final Logger logger = Logger.getLogger(SeasonDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addSeason(Season season) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(season);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Season> listSeasons() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Season> seasons=session.createCriteria(Season.class).list();
		session.close();
		return seasons;
	}
	
	
}
