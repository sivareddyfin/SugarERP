package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AdvanceSubCategory;

@Repository("AdvanceSubCategoryDao")
@Transactional

public class AdvanceSubCategoryDao 
{
	private static final Logger logger = Logger.getLogger(AdvanceSubCategoryDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addAdvanceSubCategory(AdvanceSubCategory advanceSubCategory)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advanceSubCategory);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<AdvanceSubCategory> listAdvanceSubCategories()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<AdvanceSubCategory> advanceSubCategorys=session.createCriteria(AdvanceSubCategory.class).list();
		session.close();
		return advanceSubCategorys;
	}
	
	
	
	
}
