package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.FieldOfficer;



@Repository("FieldOfficerDao")
@Transactional
public class FieldOfficerDao {
	private static final Logger logger = Logger.getLogger(FieldOfficerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addFieldOfficer(FieldOfficer fieldOfficer) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(fieldOfficer);
		session.flush();
		session.close();
	}
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<FieldOfficer> listFieldOfficers() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<FieldOfficer> fieldOfficers=session.createCriteria(FieldOfficer.class).list();
		session.close();
		return fieldOfficers;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
	
	
	public List getFieldOfficerNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
	
	

}
