package com.finsol.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.ProductMaster;
import com.finsol.model.StockDetails;
import com.finsol.model.StockSummary;
import com.finsol.model.StoreBatchMaster;

/**
 * @author Murty Ayyagari
 *
 */

@Repository("StockDao")
@Transactional
public class StockDao
{
	private static final Logger logger = Logger.getLogger(StockDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public List getBatchDetails(String batchId)
	{
		String sql = "select * from StoreBatchMaster where batchid='"+batchId+"'";
        return hibernateDao.findByNativeSql(sql, StoreBatchMaster.class);
	}
	
	public double getCurrentStock(String batchId,String finyear,int departmentId)
	{
		double currentStock=0.0;
		double dr=0.0;
		double cr=0.0;
		//Getting Dr Stock
		String drsql = "select sum(invoicestock) as dr from StockDetails where batchid='"+batchId+"' and status=0 and finyear='"+finyear+"' and transactiontype=0 and departmentid="+departmentId;
        List DrList = hibernateDao.findBySqlCriteria(drsql);
		if(DrList != null && DrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) DrList.get(0);
			if((Double) tempMap.get("dr") != null)
			{
				dr = (Double) tempMap.get("dr");
			}
		}
		
		//Getting Cr Stock
    	String crsql = "select sum(invoicestock) as cr from StockDetails where batchid='"+batchId+"' and status=0  and finyear='"+finyear+"' and transactiontype=1 and departmentid="+departmentId;
        List CrList = hibernateDao.findBySqlCriteria(crsql);
        if(CrList != null && CrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) CrList.get(0);
			if((Double) tempMap.get("cr") != null)
			{
				cr = (Double) tempMap.get("cr");
			}
		}
        currentStock = dr-cr;
        if(currentStock < 0)
        {
        	currentStock = 0.0;
        }
        
		return currentStock;
	}
	
	public List getStockSummaryDetails(String batchId,int departmentId)
	{
		String sql = "select * from StockSummary where batchid='"+batchId+"' and deptid="+departmentId;
        return hibernateDao.findByNativeSql(sql, StockSummary.class);
	}
	
	//Added by DMurty on 07-03-2017
	public List getProductDls(String productcode)
	{
		String sql = "select * from ProductMaster where productCode='"+productcode+"'";
        return hibernateDao.findBySqlCriteria(sql);
	}
	
	public List getStockDetailsDataForRevert(String batchId,int transactionId,String finYr,int deptId)
	{
		//Modified by DMurty on 15-03-2017
		//String sql = "select * from StockDetails where batchid='"+batchId+"' and transactioncode="+transactionId+" and finyear='"+finYr+"' and departmentid="+deptId;
		String sql = "select * from StockDetails where transactioncode="+transactionId+" and finyear='"+finYr+"' and status = 0 and departmentid="+deptId;
        return hibernateDao.findByNativeSql(sql, StockDetails.class);
	}
	
	public double getStockRunBal(String batchId,int transactionCode,String finyear,int deptId)
	{
		double currentStock=0.0;
		double dr=0.0;
		double cr=0.0;
		//Getting Dr Stock
		String drsql = "select sum(invoicestock) as dr from StockDetails where batchid='"+batchId+"' and finyear='"+finyear+"' and status=0 and transactiontype=0 and departmentid="+deptId+" and transactioncode != "+transactionCode;
        List DrList = hibernateDao.findBySqlCriteria(drsql);
		if(DrList != null && DrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) DrList.get(0);
			if((Double) tempMap.get("dr") != null)
			{
				dr = (Double) tempMap.get("dr");
			}
		}
		
		//Getting Cr Stock
    	String crsql = "select sum(invoicestock) as cr from StockDetails where batchid='"+batchId+"' and finyear='"+finyear+"' and  status=0 and transactiontype=1 and departmentid="+deptId+" and transactioncode != "+transactionCode;
        List CrList = hibernateDao.findBySqlCriteria(crsql);
        if(CrList != null && CrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) CrList.get(0);
			if((Double) tempMap.get("cr") != null)
			{
				cr = (Double) tempMap.get("cr");
			}
		}
        currentStock = dr-cr;
        if(currentStock < 0)
        {
        	currentStock = 0.0;
        }
		return currentStock;
	}
	
	public List getTransactionTypeByScreen(String screen)
	{
		String sql = "select * from InvoiceType where invoicetype='"+screen+"'";
        List invoiceTypeList = hibernateDao.findBySqlCriteria(sql);
		return invoiceTypeList;
	}
	
	public List getProductBatches(String productCode,int deptId)
	{
		//Modified by DMurty on 20-03-2017
		String sql = "select a.batchid,a.stock,a.mrp,a.costprice,a.deptid,b.batch,b.expdt,b.mfgdt,b.seqno,b.batchdate from StockSummary a,StoreBatchMaster b where a.productcode='"+productCode+"' and a.deptid="+deptId+" and a.stock>0 and a.productcode=b.productcode and a.batchid=b.batchid order by b.seqno";
        List BatchList = hibernateDao.findBySqlCriteria(sql);
		return BatchList;
	}
	//Added by DMurty on 15-03-2017
	public List getStockDetailsForModifyStockOpngRevert(String batchId,String finYr,int deptId,int invType)
	{
		//Modified by DMurty on 15-03-2017
		String sql = "select * from StockDetails where batchid='"+batchId+"' and invoicetype="+invType+" and finyear='"+finYr+"' and departmentid="+deptId;
        return hibernateDao.findByNativeSql(sql, StockDetails.class);
	}
	
	//Added by DMurty on 16-03-2017
	public double getStockRunBalForStockOpngBal(String batchId,String finyear,int deptId)
	{
		double currentStock=0.0;
		double dr=0.0;
		double cr=0.0;
		//Getting Dr Stock
		String drsql = "select sum(invoicestock) as dr from StockDetails where batchid='"+batchId+"' and finyear='"+finyear+"' and status=0 and transactiontype=0 and departmentid="+deptId;
        List DrList = hibernateDao.findBySqlCriteria(drsql);
		if(DrList != null && DrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) DrList.get(0);
			if((Double) tempMap.get("dr") != null)
			{
				dr = (Double) tempMap.get("dr");
			}
		}
		
		//Getting Cr Stock
    	String crsql = "select sum(invoicestock) as cr from StockDetails where batchid='"+batchId+"' and finyear='"+finyear+"' and  status=0 and transactiontype=1 and departmentid="+deptId;
        List CrList = hibernateDao.findBySqlCriteria(crsql);
        if(CrList != null && CrList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) CrList.get(0);
			if((Double) tempMap.get("cr") != null)
			{
				cr = (Double) tempMap.get("cr");
			}
		}
        currentStock = dr-cr;
        if(currentStock < 0)
        {
        	currentStock = 0.0;
        }
		return currentStock;
	}
	
	//Added by DMurty on 17-03-2017
	public double getCurrentStockForDepartmentByProductCode(String productCode,int deptId,String finYear)
	{
		double currentStock=0.0;
		//Getting Dr Stock
		String drsql = "select sum(stock) as stock from StockSummary where productcode='"+productCode+"' and deptid="+deptId;
        List StockList = hibernateDao.findBySqlCriteria(drsql);
		if(StockList != null && StockList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) StockList.get(0);
			if((Double) tempMap.get("stock") != null)
			{
				currentStock = (Double) tempMap.get("stock");
			}
		}
        
		return currentStock;
	}
	//Added by DMurty on 17-03-2017
	public int getCentranStoreId()
	{
		int centralStoreId=0;
		String sql = "select deptCode from DepartmentMaster where ismainstore=1";
        List DeptList = hibernateDao.findBySqlCriteria(sql);
		if(DeptList != null && DeptList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) DeptList.get(0);
			centralStoreId = (Integer) tempMap.get("deptCode");
		}
		return centralStoreId;
	}
	
	//Added by DMurty on 22-03-2017
	public List getUomDetails(int uomCode)
	{
		String sql = "select * from Uom where uomCode="+uomCode;
        return hibernateDao.findBySqlCriteria(sql);
	}
	
}
