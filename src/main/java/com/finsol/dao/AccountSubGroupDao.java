package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountSubGroup;

@Repository("AccountSubGroupDao")
@Transactional
public class AccountSubGroupDao
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addAccountSubGroup(AccountSubGroup accountSubGroup) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountSubGroup);
		session.flush();
		session.close();
	}
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<AccountSubGroup> listAccountSubGroups() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<AccountSubGroup> accountSubGroups=session.createCriteria(AccountSubGroup.class).list();
		session.close();
		return accountSubGroups;
	}
	
	
	
	
	

}
