package com.finsol.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Circle;
import com.finsol.model.CropTypes;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;
import com.finsol.model.Variety;
import com.finsol.model.Zone;
import com.finsol.service.AHFuctionalService;

/**
 * @author Rama Krishna
 * 
 */
@Repository("ChartDao")
@Transactional
public class ChartDao {
	
private static final Logger logger = Logger.getLogger(ChartDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	@Autowired	
	private AHFuctionalService ahFuctionalService;
	
	public List getExtentSizeByFieldOfficer() 
	{
		String sql = "select fieldofficerid as foid,sum(extentsize) as value from AgreementDetails group by fieldofficerid order by fieldofficerid";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Integer foId=(Integer)rlMap.get("foid");
			
			//DecimalFormat df = new DecimalFormat("#.##");
			//String runningbalancest=df.format(rbal);
			FieldOfficer fieldOfficer=(FieldOfficer) hibernateDao.getSessionFactory().openSession().get(FieldOfficer.class, foId);
			if(fieldOfficer==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", fieldOfficer.getFieldOfficer());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	public List getExtentSizeByFieldAssistant(Integer foid) 
	{
		String sql = "select fieldassistantid as faid,sum(extentsize) as value from AgreementDetails where fieldofficerid="+foid+" group by fieldassistantid  order by fieldassistantid";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Integer foId=(Integer)rlMap.get("faid");

			FieldAssistant fieldAssistant=(FieldAssistant) hibernateDao.getSessionFactory().openSession().get(FieldAssistant.class, foId);
			if(fieldAssistant==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", fieldAssistant.getFieldassistant());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	public List getBankDetailsChart() 
	{
		String sql = "select bankname as label,bankcode as value from Bank";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	public List getBranchDetailsChart(Integer bankid) 
	{
		String sql = "select branchname as label,branchcode as value from Branch where bankcode="+bankid+"";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	public List crushedCaneByPlantOrRatoon() 
	{
		String sql = "select sum(netwt) as value,plantorratoon as prcode from WeighmentDetails  where status=1 group by plantorratoon";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Byte prcode=(Byte)rlMap.get("prcode");			

			CropTypes cropType=(CropTypes) hibernateDao.getSessionFactory().openSession().get(CropTypes.class, prcode.intValue());
			if(cropType==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", cropType.getCroptype());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	public List crushedCaneByVariety() 
	{
		String sql = "select sum(netwt) as value,varietycode as vcode from WeighmentDetails  where status=1 group by varietycode";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Integer foId=(Integer)rlMap.get("vcode");
			Variety variety=(Variety) hibernateDao.getSessionFactory().openSession().get(Variety.class, foId);
			if(variety==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", variety.getVariety());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	
	//Added by DMurty on 05-12-2016
	public List crushedWeightByZone() 
	{
		String sql = "select zonecode as faid,sum(netwt) as value from WeighmentDetails group by zonecode  order by zonecode";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Integer foId=(Integer)rlMap.get("faid");

			Zone zone=(Zone) hibernateDao.getSessionFactory().openSession().get(Zone.class, foId);
			if(zone==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", zone.getZone());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
	
	
	public List circleWiseCrushedWeightByZones(int zoneCode) 
	{
		String sql = "select circlecode as faid,sum(netwt) as value from WeighmentDetails where zonecode="+zoneCode+"  group by circlecode  order by circlecode";
	    List reqDataList=hibernateDao.findBySqlCriteria(sql);
	    
		for(int i=0;i<reqDataList.size();i++)
		{
			Map rlMap=(Map)reqDataList.get(i);
			Integer foId=(Integer)rlMap.get("faid");

			//Circle circle=(Circle) hibernateDao.getSessionFactory().openSession().get(Circle.class, foId);
			List<Circle> circle=ahFuctionalService.getCircleRecordDetails(foId);

			if(circle==null)
			{
				rlMap.put("label", "NA");
			}
			else
				rlMap.put("label", circle.get(0).getCircle());
		}	
		
	    if(reqDataList.size()>0 && reqDataList!=null)
	    	return reqDataList;
	    else
	    	return null;  
	}
}
