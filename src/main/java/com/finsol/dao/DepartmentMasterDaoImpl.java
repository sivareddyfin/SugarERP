package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.controller.SugarCaneSecurityController;
import com.finsol.model.DepartmentMaster;

/**
 * @author Rama Krishna
 *
 */

@Repository("DepartmentMasterDaoImpl")
@Transactional
public class DepartmentMasterDaoImpl {

	private static final Logger logger = Logger.getLogger(DepartmentMasterDaoImpl.class);
	@Autowired
	private HibernateDao hibernateDao;
	
	public void addDepartment(DepartmentMaster departmentMaster) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(departmentMaster);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<DepartmentMaster> listDepartments()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<DepartmentMaster> departmentMasters=session.createCriteria(DepartmentMaster.class).list();
		session.close();
		return departmentMasters;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
}
