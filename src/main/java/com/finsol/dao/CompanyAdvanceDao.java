package com.finsol.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.AccountMaster;
import com.finsol.model.AccountType;
import com.finsol.model.AdvanceInterestRates;
import com.finsol.model.AdvanceOrder;
import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.SeedAdvanceCost;

@Repository("CompanyAdvanceDao")
@Transactional
public class CompanyAdvanceDao
{
	
	private static final Logger logger = Logger.getLogger(CompanyAdvanceDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addCompanyAdvance(CompanyAdvance companyAdvance)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(companyAdvance);
		session.flush();
	}
	
	public void addAdvIntrestRate(AdvanceInterestRates advanceInterestRates) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(advanceInterestRates);
		session.flush();
	}
	
	public void addAccountMaster(AccountMaster accountMaster)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountMaster);
		session.flush();
	}
	
	public void addAdvanceOrder(AdvanceOrder advanceOrder)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(advanceOrder);
		session.flush();
	}
	
	public void addAccountType(AccountType accountType)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(accountType);
		session.flush();
	}

	public void deleteIntrestRate(CompanyAdvance companyAdvance)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM AdvanceInterestRates WHERE advancecode = "+companyAdvance.getAdvancecode()).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public void deleteSeedlingCost(CompanyAdvance companyAdvance)
	{
		logger.info("========Before Delete==========");
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SeedAdvanceCost WHERE advancecode = "+companyAdvance.getAdvancecode()).executeUpdate();
		logger.info("========After Delete==========");
	}
	
	public CompanyAdvance getCompanyAdvance(int id)
	{
		return (CompanyAdvance) hibernateDao.getSessionFactory().openSession().get(CompanyAdvance.class, id);
	}
	
	public List<AdvanceInterestRates> getIntrestRatesByAdvanceCode(Integer advancecode)
    {
        String sql = "select * from AdvanceInterestRates where advancecode =" + advancecode;
        
        return hibernateDao.findByNativeSql(sql, AdvanceInterestRates.class);
    }
	
	public List<SeedAdvanceCost> getSeedlingCost(Integer advancecode)
    {
        String sql = "select * from SeedAdvanceCost where advancecode =" + advancecode;
        
        return hibernateDao.findByNativeSql(sql, SeedAdvanceCost.class);
    }
	
	public List getCompanyAdvancesForOrder()
	{
		/*Date currentDate = new Date();
		String orderDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
		String fDate[] = date.split("-");
		String dd = fDate[0];
		String mm = fDate[1];
		String yy = fDate[2];
		String orderDate = yy+"-"+mm+"-"+dd;
		
		
		String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
*/	
		// modified by naidu on 11-07-2016
		String strQuery="Select advancecode,advance,id from CompanyAdvance order by advorder";
		return hibernateDao.getColumns(strQuery);
	}
	
	public void updateOrderForCompanyAdvance(int advanceCode,int orderId,int id)
	{
		hibernateDao.getSessionFactory().openSession().createQuery("UPDATE AdvanceOrder set advanceOrder="+orderId+",id="+id+" WHERE advanceCode ="+advanceCode).executeUpdate();
	}
	
	public Object GetAccGrpCode(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }	
}
