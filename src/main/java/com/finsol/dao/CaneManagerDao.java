package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.DepartmentMaster;


@Repository("CaneManagerDao")
@Transactional
public class CaneManagerDao {

	
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addCaneManager(CaneManager caneManager) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneManager);
		session.flush();
	}
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	
	@SuppressWarnings("unchecked")
	public List<CaneManager> listCaneManagers() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<CaneManager> caneManagers=session.createCriteria(CaneManager.class).list();
		session.close();
		return caneManagers;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
	
	public List getCaneManageNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
	
}
