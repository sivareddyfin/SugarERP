package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.FamilyGroup;

@Repository("FamilyGroupDao")
@Transactional
public class FamilyGroupDao
{
	private static final Logger logger = Logger.getLogger(FamilyGroupDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addFamilyGroup(FamilyGroup familyGroup) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(familyGroup);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<FamilyGroup> listFamilyGroups()
	{	
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<FamilyGroup> familyGroups=session.createCriteria(FamilyGroup.class).list();
		session.close();
		return familyGroups;
	}
	
	public List getCircleNames(String qry)
    {
		return hibernateDao.getColumns(qry);
    }

	
}
