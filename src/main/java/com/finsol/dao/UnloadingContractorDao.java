package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.finsol.model.UnloadingContractor;

@Repository("UnloadingContractorDao")
@Transactional

public class UnloadingContractorDao 
{
	private static final Logger logger = Logger.getLogger(UnloadingContractorDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
    public void addUnloadingContractor(UnloadingContractor unloadingContractor) 
    {
    	Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(unloadingContractor);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
    @SuppressWarnings("unchecked")
	public List<UnloadingContractor> listUnloadingContractors() 
	{
    	Session session=(Session)hibernateDao.getSessionFactory().openSession();
    	List<UnloadingContractor> unloadingContractors=session.createCriteria(UnloadingContractor.class).list();
    	session.close();
		return unloadingContractors;
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
}
