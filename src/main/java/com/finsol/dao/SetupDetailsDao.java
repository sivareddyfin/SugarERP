package com.finsol.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.SetupDetails;



@Repository("SetupDetailsDao")
@Transactional
public class SetupDetailsDao 
{	
	private static final Logger logger = Logger.getLogger(SetupDetailsDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addSetupDetails(SetupDetails setupDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(setupDetails);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<SetupDetails> listSetupDetail()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<SetupDetails> setupDetailss=session.createCriteria(SetupDetails.class).list();
		session.close();
		return setupDetailss;
	}
	public void deleteSetupDetails() 
	{
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM SetupDetails").executeUpdate();
	}
}
