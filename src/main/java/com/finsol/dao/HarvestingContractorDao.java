package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.HCSuretyDetails;
import com.finsol.model.HarvestingContractors;


@Repository("HarvestingContractorDao")
@Transactional
public class HarvestingContractorDao {
	
	private static final Logger logger = Logger.getLogger(HarvestingContractorDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addHarvestingContractor(HarvestingContractors harvestingContractor) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(harvestingContractor);
		session.flush();
		session.close();
	}
	
	public void addHCSuretyDetails(HCSuretyDetails hcSuretyDetails) {
		//logger.info("====saveDepartment() from SecurityController========"+departmentMaster.getId());
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.save(hcSuretyDetails);
		session.flush();
		session.close();
	}
	
	

	@SuppressWarnings("unchecked")
	public List<HarvestingContractors> lisHarvestingContractors() 
	{
		return (List<HarvestingContractors>) hibernateDao.getSessionFactory().openSession().createCriteria(HarvestingContractors.class).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<HCSuretyDetails> listHCSuretyDetails() 
	{
		return (List<HCSuretyDetails>) hibernateDao.getSessionFactory().openSession().createCriteria(HCSuretyDetails.class).list();
	}
	
	public void deleteHCSuretyDetails(HarvestingContractors harvestingContractor) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM HCSuretyDetails WHERE harvestercode = "+harvestingContractor.getHarvestercode()).executeUpdate();
	}
	
	
	public HarvestingContractors getHarvestingContractors(int id) {
		
		return (HarvestingContractors) hibernateDao.getSessionFactory().openSession().get(HarvestingContractors.class, id);
	}
	
	public List<HCSuretyDetails> getHCDetailsByCode(Integer hcode)
    {
        String sql = "select * from HCSuretyDetails where harvestercode =" + hcode;
        
        return hibernateDao.findByNativeSql(sql, HCSuretyDetails.class);
    }
	
	/*public Ryot getRyot(int id) {
		
		return (Ryot) hibernateDao.getSessionFactory().openSession().get(Ryot.class, id);
	}
	
	public List<RyotBankDetails> getHCDetailsByCode(String ryotcode)
    {
        String sql = "select * from RyotBankDetails where ryotcode =" + ryotcode;
        
        return hibernateDao.findByNativeSql(sql, RyotBankDetails.class);
    }
	

	public void deleteRyotBankDetails(Ryot ryot) {
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM RyotBankDetails WHERE ryotcode = "+ryot.getRyotcode()).executeUpdate();
	}*/

}
