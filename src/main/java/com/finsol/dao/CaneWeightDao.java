package com.finsol.dao;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneWeight;
import com.finsol.model.PermitDetails;

@Repository("CaneWeightDao")
@Transactional
public class CaneWeightDao 
{
	private static final Logger logger = Logger.getLogger(CaneWeightDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addCaneWeight(CaneWeight caneWeight) {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneWeight);
		session.flush();
	}
	public List getCaneWeight(String season)
	{		
		logger.info("========getCaneWeight==========season" + season);
		List<CaneWeight> caneWeight= null;
		String sqlString="select *  from CaneWeight where season='"+season+"'";
		logger.info("========sqlString==========" + sqlString);
		caneWeight=hibernateDao.findByNativeSql(sqlString, CaneWeight.class);	
		return caneWeight;
	}
	
	public List getReviseSchedulePermits(Integer circleCode,String season,Integer fromRank,Integer toRank,Integer permitDateFlag,Integer programNumber) 
    {
		String sql =null;
		if(circleCode == 0)
		{
			//added by naidu 09-12-2016
			if(permitDateFlag==0)
	        sql = "select * from PermitDetails where season='"+season+"' and programno="+programNumber+" and rank between "+fromRank+" and "+toRank+" and harvestdt is null and permitnumber not in(select permitnumber from Weighmentdetails)";
			else
		        sql = "select * from PermitDetails where season='"+season+"'  and programno="+programNumber+" and rank between "+fromRank+" and "+toRank+" and permitnumber not in(select permitnumber from Weighmentdetails)";
		}
		else
		{
			if(permitDateFlag==0)
		        sql = "select * from PermitDetails where circlecode ="+circleCode+" and season='"+season+"' and programno="+programNumber+" and rank between "+fromRank+" and "+toRank+" and harvestdt is null and permitnumber not in(select permitnumber from Weighmentdetails where circlecode ="+circleCode+")";
				else
			        sql = "select * from PermitDetails where circlecode ="+circleCode+" and season='"+season+"'  and programno="+programNumber+" and rank between "+fromRank+" and "+toRank+" and permitnumber not in(select permitnumber from Weighmentdetails where circlecode ="+circleCode+")";
			
		}
		logger.info("========getReviseSchedulePermits()==========sql" + sql);
        return hibernateDao.findByNativeSql(sql, PermitDetails.class);
    }
	//Added by DMurty on 02-11-2016
	public List getPermitPrintByHarvestingDates(Date fromDate,Date toDate) 
    {
	    String sql = "select * from PermitDetails where harvestdt between '"+fromDate+"' and '"+toDate+"'";
		logger.info("========getPermitPrintByHarvestingDates()==========sql" + sql);
	    return hibernateDao.findByNativeSql(sql, PermitDetails.class);
    }
}
