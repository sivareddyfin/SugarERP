package com.finsol.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;

@Repository("CaneAccountingRulesDao")
@Transactional
public class CaneAccountingRulesDao
{
	private static final Logger logger = Logger.getLogger(CaneManagerDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	public void addCaneAccountingRules(CaneAccountingRules caneAccountingRules) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(caneAccountingRules);
		session.flush();
		session.close();
	}
	
/*	public CaneAccountingRules getCaneAccRulesDtls(String season,Date effDate) 
	{
		String sqlString="select *  from CaneAccountingRules where season='"+season+"' and date='"+effDate+"'";
		List<CaneAccountingRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAccountingRules.class);
		return caneAccountingRules.get(0);
	}*/
	
	public List<CaneAccountingRules> getCaneAccRulesForCalc(String season,Date date1,Date date2) 
	{
		String sqlString="select *  from CaneAccountingRules where season='"+season+"' and date between '"+date1+"' and '"+date2+"' ";
		List<CaneAccountingRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAccountingRules.class);
		return caneAccountingRules;
	}
	
	public CaneAcctRules getCaneAccRulesDtls(String season,Date effDate) 
	{
		String sqlString="select *  from CaneAcctRules where season='"+season+"' and calcruledate='"+effDate+"'";
		List<CaneAcctRules> caneAccountingRules=hibernateDao.findByNativeSql(sqlString, CaneAcctRules.class);
		return caneAccountingRules.get(0);
	}
	
	public List getCaneAccAmounts(Integer calcid) 
	{
		String sqlString="select *  from CaneAcctAmounts where calcid="+calcid+"";
		List caneAcctAmountsList=hibernateDao.findByNativeSql(sqlString, CaneAcctAmounts.class);
		return caneAcctAmountsList;
	}
}
