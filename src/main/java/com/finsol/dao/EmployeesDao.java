package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Employees;




@Repository("EmployeesDao")
@Transactional
public class EmployeesDao 
{
	private static final Logger logger = Logger.getLogger(EmployeesDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addEmployees(Employees employees) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(employees);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Employees> listEmployee() 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Employees> employeess=session.createCriteria(Employees.class).list();
		session.close();
		return employeess;
		
	}
	
	public Employees getEmployees(int id)
	{
		return (Employees) hibernateDao.getSessionFactory().openSession().get(Employees.class, id);
	}
	 
	public Employees getEmployeeByLoginId(String loginId)  throws Exception
	{	
		try
		{
			Employees employees = null;
			logger.info("=========getEmployeeByLoginId()==========");
			employees = (Employees) hibernateDao.getID("from Employees where loginid='"+loginId+"'");
			logger.info("=========getEmployeeByLoginId()=========="+employees);
			return employees;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}

}
