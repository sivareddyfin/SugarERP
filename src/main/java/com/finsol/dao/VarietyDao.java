package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Variety;

@Repository("VarietyDao")
@Transactional
public class VarietyDao {

	private static final Logger logger = Logger.getLogger(VarietyDao.class);
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addVariety(Variety variety) {
		
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(variety);
		session.flush();
		session.close();
	}
	
	public Object getMaxValue(String sql)
    {
        return hibernateDao.getMaxValue(sql);
    }
	
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Variety> listVarieties() {
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Variety> varietys=session.createCriteria(Variety.class).list();
		session.close();
		return varietys; 
		//return (List<DepartmentMaster>) hibernateDao.getCriteria();
	}
}
