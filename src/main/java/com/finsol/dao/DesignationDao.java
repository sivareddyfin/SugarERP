package com.finsol.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.Designation;


@Repository("DesignationDao")
@Transactional
public class DesignationDao
{
	private static final Logger logger = Logger.getLogger(DesignationDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addDesignations(Designation designation) 
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(designation);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Designation> listDesignations()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Designation> designations=session.createCriteria(Designation.class).list();
		session.close();
		return designations;
	}
	
	
	
	
	
}
