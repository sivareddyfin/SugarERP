package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.CaneManager;
import com.finsol.model.Mandal;

@Repository("MandalDao")
@Transactional

public class MandalDao
{
	private static final Logger logger = Logger.getLogger(MandalDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addMandal(Mandal mandal)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(mandal);
		session.flush();
		session.close();
	}
	//Added by sahadeva 18-08-2016
	@SuppressWarnings("unchecked")
	public List<Mandal> listAllMandals()
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		List<Mandal> mandals=session.createCriteria(Mandal.class).list();
		session.close();
		return mandals;
	}
	
	public List getZoneNames(String qry)
    {
        //return hibernateDao.getColumns(sql,DepartmentMaster);
		return hibernateDao.getColumns(qry);
    }
}
