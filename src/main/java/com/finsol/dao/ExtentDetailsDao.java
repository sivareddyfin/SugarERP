package com.finsol.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.finsol.model.Branch;
import com.finsol.model.ExtentDetails;
import com.finsol.model.LabRecommendations;

/**
 * @author Rama Krishna
 */
@Repository("ExtentDetailsDao")
@Transactional
public class ExtentDetailsDao {

	private static final Logger logger = Logger.getLogger(ExtentDetailsDao.class);
	@Autowired	
	private HibernateDao hibernateDao;
	
	public void addExtentDetails(ExtentDetails extentDetails)
	{
		Session session=(Session)hibernateDao.getSessionFactory().openSession();
		session.saveOrUpdate(extentDetails);
		session.flush();
		session.close();
	}
	
	public void deleteExtentDetails(String ryotcode) {
		//hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM Shift WHERE shiftid = "+shift.getShiftid()).executeUpdate();
		hibernateDao.getSessionFactory().openSession().createQuery("DELETE FROM ExtentDetails where ryotcode ='" + ryotcode+"'").executeUpdate();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotcode)
    {
        String sql = "select * from ExtentDetails where ryotcode ='" + ryotcode+"'";
        
        return hibernateDao.findByNativeSql(sql, ExtentDetails.class);
    }
	
	@SuppressWarnings("unchecked")
	public List<ExtentDetails> getExtentDetailsByRyotCode(String ryotcode,Integer plotno,String surveyno)
    {
        String sql = "select * from ExtentDetails where ryotcode ='" + ryotcode+"' and plotslnumber=" + plotno+" and surveyno='" + surveyno+"'";
        
        return hibernateDao.findByNativeSql(sql, ExtentDetails.class);
    }
	

}
