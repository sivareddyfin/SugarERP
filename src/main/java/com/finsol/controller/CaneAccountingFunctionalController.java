package com.finsol.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.AdvancesLoansForPCABean;
import com.finsol.bean.CaneAccountingRulesWeighmentDetailsForPCACBean;
import com.finsol.bean.CaneValueSplitupBean;
import com.finsol.bean.DeductionsbaccforPCABean;
import com.finsol.bean.HarvestingRateDetailsBean;
import com.finsol.bean.HarvestingRatesSmryBean;
import com.finsol.bean.SampleCardsBean;
import com.finsol.bean.SeedAccountingAdvDetailsBean;
import com.finsol.bean.SeedAccountingDetailsBean;
import com.finsol.bean.SeedAccountingSummaryBean;
import com.finsol.bean.TransportingRateDetailsBean;
import com.finsol.bean.TransportingRatesSmryBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccruedAndDueCaneValueSummary;
import com.finsol.model.AdvancePaymentDetails;
import com.finsol.model.AdvancePaymentSummary;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementSummary;
import com.finsol.model.Branch;
import com.finsol.model.CanAccLoansDetails;
import com.finsol.model.CanAccLoansDetailsTemp;
import com.finsol.model.CanAccSBDetails;
import com.finsol.model.CanAccSBDetailsTemp;
import com.finsol.model.CaneAccDetailsTemp;
import com.finsol.model.CaneAccDetailsTempNew;
import com.finsol.model.CaneAccountSmry;
import com.finsol.model.CaneAccountSmrytemp;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneValueSplitup;
import com.finsol.model.CaneValueSplitupDetailsByDate;
import com.finsol.model.CaneValueSplitupDetailsByRyot;
import com.finsol.model.CaneValueSplitupSummary;
import com.finsol.model.CaneValueSplitupTemp;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.DedDetails;
import com.finsol.model.DedDetails_temp;
import com.finsol.model.Employees;
import com.finsol.model.HarvChgDtlsByRyotContAndDate;
import com.finsol.model.HarvChgSmryByContAndDate;
import com.finsol.model.HarvChgSmryByRyotAndCont;
import com.finsol.model.HarvChgSmryBySeason;
import com.finsol.model.HarvChgSmryBySeasonByCont;
import com.finsol.model.HarvestingRateDetails;
import com.finsol.model.HarvestingRatesSmry;
import com.finsol.model.LoanPaymentDetails;
import com.finsol.model.LoanPaymentSummary;
import com.finsol.model.LoanPaymentSummaryByBank;
import com.finsol.model.LoanPrincipleAmounts;
import com.finsol.model.LoanSummary;
import com.finsol.model.OtherDedSmryByRyot;
import com.finsol.model.OtherDeductions;
import com.finsol.model.OtherDeductionsBySeason;
import com.finsol.model.PostSeasonPaymentAdvDetails;
import com.finsol.model.PostSeasonPaymentDetails;
import com.finsol.model.PostSeasonPaymentSummary;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;
import com.finsol.model.SBAccSmryByBank;
import com.finsol.model.SBAccSmryByRyot;
import com.finsol.model.SBAccTransDetails;
import com.finsol.model.SeedAccountingAdvDetails;
import com.finsol.model.SeedAccountingDetails;
import com.finsol.model.SeedAccountingSummary;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.TranspChgSmryByContAndDate;
import com.finsol.model.TranspChgSmryByRyotAndCont;
import com.finsol.model.TranspChgSmryByRyotContAndDate;
import com.finsol.model.TranspChgSmryBySeason;
import com.finsol.model.TranspChgSmryBySeasonByCont;
import com.finsol.model.TransportingRateDetails;
import com.finsol.model.TransportingRatesSmry;
import com.finsol.model.Village;
import com.finsol.model.WeighmentDetails;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CaneReceiptFunctionalService;
import com.finsol.service.CommonService;
import com.finsol.service.RyotService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 * @author Rama Krishna
 */
@Controller
public class CaneAccountingFunctionalController {
	
	private static final Logger logger = Logger.getLogger(CaneAccountingFunctionalController.class);
	
	@Autowired	
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	
	@Autowired	
	private AHFuctionalService aHFuctionalService;
	
	@Autowired	
	private CommonService commonService;
	
	@Autowired
	private RyotService ryotService;
	@Autowired
	private AHFunctionalController ahfController;
	
	@Autowired
	private HibernateDao hibernateDao;
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired	
	private LoginController loginController;
	
	DecimalFormat HMSDecFormatter = new DecimalFormat("0.00");
	
	@Autowired	
	private AgricultureHarvestingController agricultureHarvestingController;
	
	
	@Autowired	
	private AHFunctionalController aHFunctionalController;

	@Autowired	
	private CaneReceiptFunctionalService caneReceiptFunctionalService;
	
	//
	//------------------------------------------------------------------------common method for seed accounting maxid-------------------------
	
	@RequestMapping(value = "/getMaxSeedId", method = RequestMethod.POST)
	public @ResponseBody
	String getMaxId(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tableName=(String)jsonData.get("tablename");
		String columnName=(String)jsonData.get("columnname");
		Integer maxId = commonService.getMaxSeedID(tableName,columnName);		
		JSONObject json = new JSONObject();
		json.put("id", maxId);
		return json.toString();
	}
	//--------------------------------------- Update Harvesting Rates ------------------------------------------//
	@RequestMapping(value = "/getRyotsBySeasonandCircle", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotsBySeasonandCircle(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		Integer circleCode=(Integer)jsonData.get("circleCode");
		String date=(String)jsonData.get("date");
		String finalDate = null;
		Date efDate= null;
		if(date != null)
		{
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			finalDate = yy+"-"+mm+"-"+dd;
			efDate=DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
		}
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getRyotsByCircleandSeason(season,circleCode);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					//Modified by DMurty on 07-05-2016
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					
					String ryotcode=null;
					ryotcode = (String) myResult[0];
										
					String ryotName = null;
					ryotName = (String) myResult[2];
					
					jsonObj.put("ryotCode", ryotcode);
					jsonObj.put("ryotName", ryotName);
					
					double harvestRate = 0.0;
					double slNo = 0;
					int harvestContractorCode = 0;
					List harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetailsForUpdateHC(season,ryotcode,finalDate);
					if(harvestingRateDetailsList!=null)
					{
						HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
						harvestRate = harvestingRateDetails.getRate();
						harvestContractorCode = harvestingRateDetails.getHarvestcontcode();
					}
					jsonObj.put("rate", harvestRate);
					jsonObj.put("harvestContractorCode", harvestContractorCode);

					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
            logger.info(e.getCause(), e);
            return null;
		}
		return jArray;
	}
	
	@RequestMapping(value = "/getRyotsBySeasonandCircleForTransportCharges", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotsBySeasonandCircleForTransportCharges(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		Integer circleCode=(Integer)jsonData.get("circleCode");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getRyotsByCircleandSeason(season,circleCode);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					Object[] myResult = (Object[]) it.next();
					
					String ryotcode=null;
					ryotcode = (String) myResult[0];
										
					String ryotName = null;
					List<String> RyotNamesList = new ArrayList<String>();
					RyotNamesList=aHFuctionalService.getRyotName(ryotcode);
					if (RyotNamesList != null && RyotNamesList.size() > 0)
					{
						String myResult1 = RyotNamesList.get(0);
						ryotName = myResult1;
					}
				
					jsonObj.put("ryotCode", ryotcode);
					jsonObj.put("ryotName", ryotName);
					
					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
		            logger.info(e.getCause(), e);
		            return null;
		}
		return jArray;
	}
	
	@RequestMapping(value = "/saveUpdateHarvestingCharges", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveUpdateHarvestingCharges(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String strQry = null;
		Object Qryobj = null;
		List rateByRyotContAndDateList = new ArrayList();
		Map rateByRyotContAndDateMap = new HashMap();
		List rateByRyotContList = new ArrayList();
		Map rateByRyotContMap = new HashMap();
		try
		{
			JSONArray gridData=(JSONArray) jsonData.get("gridData");
			JSONObject formData=(JSONObject) jsonData.get("formData");
		
			HarvestingRatesSmryBean harvestingRatesSmryBean = new ObjectMapper().readValue(formData.toString(), HarvestingRatesSmryBean.class);
			HarvestingRatesSmry harvestingRatesSmry = prepareModelForUpdateHarvestingRatesSmry(harvestingRatesSmryBean);
			entityList.add(harvestingRatesSmry);
			
			for(int i=0;i<gridData.size();i++)
			{
				HarvestingRateDetailsBean harvestingRateDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(), HarvestingRateDetailsBean.class);
				String rtoyCode=harvestingRateDetailsBean.getRyotCode();
				if(harvestingRateDetailsBean.getSlNo() != null)
				{
					double slno = harvestingRateDetailsBean.getSlNo();
					caneAccountingFunctionalService.deleteHarvestingCharges(rtoyCode,slno);
				}
			}
			
			for(int i=0;i<gridData.size();i++)
			{
				HarvestingRateDetailsBean harvestingRateDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(), HarvestingRateDetailsBean.class);
				
				if(harvestingRateDetailsBean.getHarvestContractorCode() != null && harvestingRateDetailsBean.getHarvestContractorCode() != 0 && harvestingRateDetailsBean.getSlNo() != null)
				{
					HarvestingRateDetails harvestingRateDetails = prepareModelForUpdateHarvestingRatesDtls(harvestingRateDetailsBean,harvestingRatesSmryBean);
					entityList.add(harvestingRateDetails);
			
					String ryotCode = harvestingRateDetailsBean.getRyotCode();
					int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
					String season = harvestingRatesSmryBean.getSeason();
					String date = harvestingRatesSmryBean.getEffectiveDate();
					Date efDate=DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
					
					//Added by DMurty on 17-01-2017
					String dt[] = date.split("-");
					String dd = dt[0];
					String mm = dt[1];
					String yy = dt[2];
					String finalDate = yy+"-"+mm+"-"+dd;
					
					double oldHarvestRate = 0.0;
					double slNo = 0;
					int harvestContractorCode = 0;
					double newHarvestRate = harvestingRateDetailsBean.getRate();
					
					List oldHarvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(season,ryotCode,finalDate);
					if(oldHarvestingRateDetailsList!=null)
					{
						HarvestingRateDetails oldHarvestingRateDetails =(HarvestingRateDetails)oldHarvestingRateDetailsList.get(0);
						oldHarvestRate = oldHarvestingRateDetails.getRate();
					}
					HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDateOld = null;
					HarvChgSmryByRyotAndCont harvChgSmryByRyotAndContOld = null;
					if(oldHarvestRate != newHarvestRate)
					{ 
						double totalWt = 0.0;
						//Getting old rate data from HarvChgDtlsByRyotContAndDate
						List HarvChgDtlsByRyotContAndDateList =  caneAccountingFunctionalService.GetHarvChgDtlsByRyotContAndDateForRevert(ryotCode,contractorCode,season,efDate);
						if (HarvChgDtlsByRyotContAndDateList != null && !HarvChgDtlsByRyotContAndDateList.isEmpty())
						{
							for(int m = 0;m<HarvChgDtlsByRyotContAndDateList.size();m++)
							{
								harvChgDtlsByRyotContAndDateOld = (HarvChgDtlsByRyotContAndDate) HarvChgDtlsByRyotContAndDateList.get(m);
								
								double Amt = harvChgDtlsByRyotContAndDateOld.getTotalamount();
								double weight = harvChgDtlsByRyotContAndDateOld.getHarvestedcanewt();
								totalWt += weight;
								int id = harvChgDtlsByRyotContAndDateOld.getId();
								String strId = Integer.toString(id);
								
								double finalVal = 0.0;
								
								harvChgDtlsByRyotContAndDateOld.setTotalamount(finalVal);
								Map rateIdMap = new HashMap();
								rateIdMap.put("ID", strId);
								boolean isratethere = rateByRyotContAndDateList.contains(rateIdMap);
								if (isratethere == false)
								{
									rateByRyotContAndDateList.add(rateIdMap);
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDateOld);
								}
								else
								{
									harvChgDtlsByRyotContAndDateOld = (HarvChgDtlsByRyotContAndDate) rateByRyotContAndDateMap.get(strId);
									double Val = 0.0;
									finalVal = finalVal-Val;
									harvChgDtlsByRyotContAndDateOld.setTotalamount(finalVal);
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDateOld);
								}
							}
						}
						
						if(totalWt >0)
						{
							//Getting old rate data from HarvChgSmryByRyotAndCont HarvChgSmryByRyotAndCont 
							List HarvChgSmryByRyotAndContList =  caneAccountingFunctionalService.GetHarvChgDtlsByRyotContForRevert(ryotCode,contractorCode,season,efDate);
							if (HarvChgSmryByRyotAndContList != null && !HarvChgSmryByRyotAndContList.isEmpty())
							{
								for(int m = 0;m<HarvChgSmryByRyotAndContList.size();m++)
								{
									harvChgSmryByRyotAndContOld = (HarvChgSmryByRyotAndCont) HarvChgSmryByRyotAndContList.get(m);
									double Amt = harvChgSmryByRyotAndContOld.getTotalamount();
									double weight = totalWt;
									int id = harvChgSmryByRyotAndContOld.getId();
									String strId = Integer.toString(id);
									
									double PendingAmount = harvChgSmryByRyotAndContOld.getPendingAmount();
									
									double oldAmt = weight*oldHarvestRate;
									double finalVal = Amt-oldAmt;
									if(finalVal<0)
										finalVal=0.0;
									
									PendingAmount = PendingAmount-oldAmt;
									if(PendingAmount<0)
										PendingAmount=0.0;
									
									harvChgSmryByRyotAndContOld.setTotalamount(finalVal);
									harvChgSmryByRyotAndContOld.setPendingAmount(PendingAmount);
									
									Map rateIdMap = new HashMap();
									rateIdMap.put("ID", strId);
									boolean isratethere = rateByRyotContList.contains(rateIdMap);
									if (isratethere == false)
									{
										rateByRyotContList.add(rateIdMap);
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndContOld);
									}
									else
									{
										harvChgSmryByRyotAndContOld = (HarvChgSmryByRyotAndCont) rateByRyotContMap.get(strId);
										harvChgSmryByRyotAndContOld.setTotalamount(finalVal);
										harvChgSmryByRyotAndContOld.setPendingAmount(PendingAmount);
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndContOld);
									}
								}
							}
						}
						
						//Here we need to update new value for HarvChgDtlsByRyotContAndDate 
						HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate = null;
						double UpdatingAmt = 0.0;
						double rate = 0.0;
						double totWt = 0.0;   
						List HarvstRyotContractorList =  caneAccountingFunctionalService.GetDetailsForHarvChgDtlsByRyotContAndDateNew(ryotCode,contractorCode,season,efDate);
						if (HarvstRyotContractorList != null && !HarvstRyotContractorList.isEmpty())
						{
							for(int m = 0;m<HarvstRyotContractorList.size();m++)
							{
								harvChgDtlsByRyotContAndDate = (HarvChgDtlsByRyotContAndDate) HarvstRyotContractorList.get(m);
								double Amt = harvChgDtlsByRyotContAndDate.getTotalamount();
								double weight = harvChgDtlsByRyotContAndDate.getHarvestedcanewt();
								totWt += weight;
								int id = harvChgDtlsByRyotContAndDate.getId();
								String strId = Integer.toString(id);
								Map rateIdMap = new HashMap();
								rateIdMap.put("ID", strId);
								boolean isratethere = rateByRyotContAndDateList.contains(rateIdMap);
								if (isratethere == false)
								{
									UpdatingAmt = weight*harvestingRateDetailsBean.getRate();
									harvChgDtlsByRyotContAndDate.setTotalamount(UpdatingAmt);
									harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetailsBean.getRate());
									harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDate);
								}
								else
								{
									harvChgDtlsByRyotContAndDate = (HarvChgDtlsByRyotContAndDate) rateByRyotContAndDateMap.get(strId);
									double val = weight*harvestingRateDetailsBean.getRate();
									UpdatingAmt = val;
									harvChgDtlsByRyotContAndDate.setTotalamount(UpdatingAmt);
									harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetailsBean.getRate());
									harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDate);
								}
							}
						}
						//Here we need to check weight condition. Because if there is no record in HarvChgDtlsByRyotContAndDate no need to update HarvChgSmryByRyotAndCont table.
						if(totWt>0)
						{
							//Here we need to update new value for HarvChgDtlsByRyotContAndDate
							HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont = null;
							List HarvestChgSmryByRyotAndContList =  caneAccountingFunctionalService.HarvChgSmryByRyotAndCont(ryotCode,contractorCode,season);
							if (HarvestChgSmryByRyotAndContList != null && !HarvestChgSmryByRyotAndContList.isEmpty())
							{
								for(int m = 0;m<HarvestChgSmryByRyotAndContList.size();m++)
								{
									harvChgSmryByRyotAndCont = (HarvChgSmryByRyotAndCont) HarvestChgSmryByRyotAndContList.get(m);
									double Amt = harvChgSmryByRyotAndCont.getTotalamount();
									double weight = totWt;//harvChgSmryByRyotAndCont.getHarvestedcanewt();
									int id = harvChgSmryByRyotAndCont.getId();
									String strId = Integer.toString(id);

									UpdatingAmt = weight*harvestingRateDetailsBean.getRate();
									
									harvChgSmryByRyotAndCont.setTotalamount(UpdatingAmt);
									harvChgSmryByRyotAndCont.setPendingAmount(UpdatingAmt);
									harvChgSmryByRyotAndCont.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
								
									Map rateIdMap = new HashMap();
									rateIdMap.put("ID", strId);
									boolean isratethere = rateByRyotContList.contains(rateIdMap);
									if (isratethere == false)
									{
										rateByRyotContList.add(rateIdMap);
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndCont);
									}
									else
									{
										harvChgSmryByRyotAndCont = (HarvChgSmryByRyotAndCont) rateByRyotContMap.get(strId);
										
										double finalTotal = harvChgSmryByRyotAndCont.getTotalamount();
										finalTotal = finalTotal+UpdatingAmt;
										
										double finalPending = harvChgSmryByRyotAndCont.getPendingAmount();
										finalPending = finalPending+UpdatingAmt;

										harvChgSmryByRyotAndCont.setTotalamount(finalTotal);
										harvChgSmryByRyotAndCont.setPendingAmount(finalPending);
										harvChgSmryByRyotAndCont.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndCont);
									}
								}
							}
						}
					}
					else if(oldHarvestRate == newHarvestRate)
					{
						continue;
					}
					else if(newHarvestRate == 0)
					{
						continue;
					}
					else
					{
						//Modified by DMurty on 10-08-2016
						HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate = null;
						double UpdatingAmt =0.0;
						double rate = 0.0;
						double totalwt = 0.0;
						List HarvstRyotContractorList =  caneAccountingFunctionalService.GetDetailsForHarvChgDtlsByRyotContAndDateNew(ryotCode,contractorCode,season,efDate);
						if (HarvstRyotContractorList != null && !HarvstRyotContractorList.isEmpty())
						{
							for(int m = 0;m<HarvstRyotContractorList.size();m++)
							{
								harvChgDtlsByRyotContAndDate = (HarvChgDtlsByRyotContAndDate) HarvstRyotContractorList.get(m);
								double Amt = harvChgDtlsByRyotContAndDate.getTotalamount();
								double weight = harvChgDtlsByRyotContAndDate.getHarvestedcanewt();
								totalwt += weight;
								int id = harvChgDtlsByRyotContAndDate.getId();
								String strId = Integer.toString(id);
								Map rateIdMap = new HashMap();
								rateIdMap.put("ID", strId);
								boolean isratethere = rateByRyotContAndDateList.contains(rateIdMap);
								if (isratethere == false)
								{
									UpdatingAmt = weight*harvestingRateDetailsBean.getRate();
									harvChgDtlsByRyotContAndDate.setTotalamount(UpdatingAmt);
									harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetailsBean.getRate());
									harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDate);
								}
								else
								{
									harvChgDtlsByRyotContAndDate = (HarvChgDtlsByRyotContAndDate) rateByRyotContAndDateMap.get(strId);
									double val = weight*harvestingRateDetailsBean.getRate();
									UpdatingAmt = harvChgDtlsByRyotContAndDate.getTotalamount()+val;
									harvChgDtlsByRyotContAndDate.setTotalamount(UpdatingAmt);
									harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetailsBean.getRate());
									harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
									rateByRyotContAndDateMap.put(strId, harvChgDtlsByRyotContAndDate);
								}
							}
						}
						
						if(totalwt>0)
						{
							//HarvChgSmryByRyotAndCont
							HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont = null;//prepareModelForHarvChgSmryByRyotAndCont(harvestingRateDetailsBean,harvestingRatesSmryBean);
							List HarvChgSmryByRyotAndContList =  caneAccountingFunctionalService.HarvChgSmryByRyotAndCont(ryotCode,contractorCode,season);
							if (HarvChgSmryByRyotAndContList != null && !HarvChgSmryByRyotAndContList.isEmpty())
							{
								for(int m = 0;m<HarvChgSmryByRyotAndContList.size();m++)
								{
									harvChgSmryByRyotAndCont = (HarvChgSmryByRyotAndCont) HarvChgSmryByRyotAndContList.get(m);
									double Amt = harvChgSmryByRyotAndCont.getTotalamount();
									double weight = totalwt;//harvChgSmryByRyotAndCont.getHarvestedcanewt();
									int id = harvChgSmryByRyotAndCont.getId();
									String strId = Integer.toString(id);

									UpdatingAmt = weight*harvestingRateDetailsBean.getRate();
									
									harvChgSmryByRyotAndCont.setTotalamount(UpdatingAmt);
									harvChgSmryByRyotAndCont.setPendingAmount(UpdatingAmt);
									harvChgSmryByRyotAndCont.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
									
									Map rateIdMap = new HashMap();
									rateIdMap.put("ID", strId);
									boolean isratethere = rateByRyotContList.contains(rateIdMap);
									if (isratethere == false)
									{
										rateByRyotContList.add(rateIdMap);
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndCont);
									}
									else
									{
										harvChgSmryByRyotAndCont = (HarvChgSmryByRyotAndCont) rateByRyotContMap.get(strId);
										
										double finalTotal = harvChgSmryByRyotAndCont.getTotalamount();
										finalTotal = finalTotal+UpdatingAmt;
										
										double finalPending = harvChgSmryByRyotAndCont.getPendingAmount();
										finalPending = finalPending+UpdatingAmt;

										harvChgSmryByRyotAndContOld.setTotalamount(finalTotal);
										harvChgSmryByRyotAndContOld.setPendingAmount(finalPending);
										rateByRyotContMap.put(strId, harvChgSmryByRyotAndCont);
									}
								}
							}
						}
					}
				}
			}
			Iterator entries = rateByRyotContAndDateMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
			    Object value = entry.getValue();
			    HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate = (HarvChgDtlsByRyotContAndDate) value;
			    entityList.add(harvChgDtlsByRyotContAndDate);
			}
			Iterator entries1 = rateByRyotContMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
			    Object value = entry.getValue();
			    HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont = (HarvChgSmryByRyotAndCont) value;
			    entityList.add(harvChgSmryByRyotAndCont);
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}
	
	private HarvChgSmryBySeasonByCont prepareModelForHarvChgSmryBySeasonByCont(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{
		
		Date date = new Date();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		HarvChgSmryBySeasonByCont harvChgSmryBySeasonByCont = new HarvChgSmryBySeasonByCont();
		
		harvChgSmryBySeasonByCont.setSeason(harvestingRatesSmryBean.getSeason());
		harvChgSmryBySeasonByCont.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
		
		String ryotCode = harvestingRateDetailsBean.getRyotCode();
		int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
		String season = harvestingRatesSmryBean.getSeason();
		
		double netWeight = 0.0;
		
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		harvChgSmryBySeasonByCont.setHarvestedcanewt(netWeight);
		
		
		//HarvChgSmryBySeasonByCont
		double rate = harvestingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
		
		harvChgSmryBySeasonByCont.setPaidAmount(0.0);
		harvChgSmryBySeasonByCont.setPendingAmount(finalAmt);
		harvChgSmryBySeasonByCont.setPendingpayable(finalAmt);
		harvChgSmryBySeasonByCont.setTotalamount(finalAmt);
		harvChgSmryBySeasonByCont.setTotalpayableamount(finalAmt);
		harvChgSmryBySeasonByCont.setTotalextent(0.0);
		
		return harvChgSmryBySeasonByCont;
	}
	
	
	private HarvChgSmryBySeason prepareModelForHarvChgSmryBySeason(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{
		Date date = new Date();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		HarvChgSmryBySeason harvChgSmryBySeason = new HarvChgSmryBySeason();
		
		harvChgSmryBySeason.setSeason(harvestingRatesSmryBean.getSeason());
		
		
		String ryotCode = harvestingRateDetailsBean.getRyotCode();
		int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
		String season = harvestingRatesSmryBean.getSeason();
		double netWeight = 0.0;

		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		
		harvChgSmryBySeason.setHarvestedcanewt(netWeight);
	
		double rate = harvestingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;

		harvChgSmryBySeason.setTotalpayableamount(finalAmt);
		harvChgSmryBySeason.setPendingAmount(finalAmt);
		harvChgSmryBySeason.setPendingpayable(finalAmt);
		harvChgSmryBySeason.setTotalamount(finalAmt);
		harvChgSmryBySeason.setTotalextent(0.0);
		harvChgSmryBySeason.setPaidAmount(0.0);
		
		return harvChgSmryBySeason;
	}
	
	private HarvChgSmryByRyotAndCont prepareModelForHarvChgSmryByRyotAndCont(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{
		Date date = new Date();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont = new HarvChgSmryByRyotAndCont();
		
		
		harvChgSmryByRyotAndCont.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
		//harvestedcanewt
		harvChgSmryByRyotAndCont.setRyotcode(harvestingRateDetailsBean.getRyotCode());
		harvChgSmryByRyotAndCont.setSeason(harvestingRatesSmryBean.getSeason());
		
		String ryotCode = harvestingRateDetailsBean.getRyotCode();
		int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
		String season = harvestingRatesSmryBean.getSeason();
		
		double netWeight = 0.0;
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		harvChgSmryByRyotAndCont.setHarvestedcanewt(netWeight);
	
		double rate = harvestingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
	
		harvChgSmryByRyotAndCont.setTotalpayableamount(finalAmt);
		harvChgSmryByRyotAndCont.setPendingAmount(finalAmt);
		harvChgSmryByRyotAndCont.setCompanypaidamount(0.0);
		harvChgSmryByRyotAndCont.setPendingpayable(finalAmt);
		harvChgSmryByRyotAndCont.setTotalamount(finalAmt);
		harvChgSmryByRyotAndCont.setTotalextent(0.0);
		
		return harvChgSmryByRyotAndCont;
	}

	private HarvChgSmryByContAndDate prepareModelForHarvChgSmryByContAndDate(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{	
		Date date = new Date();
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		
		HarvChgSmryByContAndDate harvChgSmryByContAndDate = new HarvChgSmryByContAndDate();
		
		harvChgSmryByContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
		
		String ryotCode = harvestingRateDetailsBean.getRyotCode();
		int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
		String season = harvestingRatesSmryBean.getSeason();
		
		double netWeight = 0.0;
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		harvChgSmryByContAndDate.setHarvestedcanewt(netWeight);
		String effDate = harvestingRatesSmryBean.getEffectiveDate();
		harvChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		harvChgSmryByContAndDate.setSeason(harvestingRatesSmryBean.getSeason());
		
		double rate = harvestingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
		harvChgSmryByContAndDate.setTotalamount(finalAmt);
		harvChgSmryByContAndDate.setTotalextent(0.0);
		
		return harvChgSmryByContAndDate;		
	}
	
	
	
	private HarvChgDtlsByRyotContAndDate prepareModelForHarvChgDtlsByRyotContAndDate(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{	
		Date date = new Date();
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		
		HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate = new HarvChgDtlsByRyotContAndDate();
		
		String ryotCode = harvestingRateDetailsBean.getRyotCode();
		int contractorCode = harvestingRateDetailsBean.getHarvestContractorCode();
		String season = harvestingRatesSmryBean.getSeason();
		
		harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
		
		double netWeight = 0.0;
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}

		harvChgDtlsByRyotContAndDate.setHarvestedcanewt(netWeight);
		
		double rate = harvestingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;

		harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetailsBean.getRate());
		String effDate = harvestingRatesSmryBean.getEffectiveDate();
		harvChgDtlsByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		harvChgDtlsByRyotContAndDate.setRyotcode(harvestingRateDetailsBean.getRyotCode());
		harvChgDtlsByRyotContAndDate.setSeason(harvestingRatesSmryBean.getSeason());
		harvChgDtlsByRyotContAndDate.setTotalamount(finalAmt);
	
		double totalExtent = 0.0;
		//List ExtentList =  caneAccountingFunctionalService.getExtentByRyots(ryotCode,season);
		harvChgDtlsByRyotContAndDate.setTotalextent(0.0);
		
		return harvChgDtlsByRyotContAndDate;		
	}
	
	private HarvestingRateDetails prepareModelForUpdateHarvestingRatesDtls(HarvestingRateDetailsBean harvestingRateDetailsBean,HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{	
		Date date = new Date();
		HarvestingRateDetails harvestingRateDetails = new HarvestingRateDetails();
		
		if(harvestingRateDetailsBean.getHarvestContractorCode() != null)
		{
			harvestingRateDetails.setSeason(harvestingRatesSmryBean.getSeason());
			harvestingRateDetails.setRyotCode(harvestingRateDetailsBean.getRyotCode());
			harvestingRateDetails.setId(harvestingRateDetailsBean.getId());
			String effDate = harvestingRatesSmryBean.getEffectiveDate();
			harvestingRateDetails.setEffectivedate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
			harvestingRateDetails.setHarvestcontcode(harvestingRateDetailsBean.getHarvestContractorCode());
			harvestingRateDetails.setRate(harvestingRateDetailsBean.getRate());
			harvestingRateDetails.setCirclecode(harvestingRatesSmryBean.getCircleCode());
			//Added by DMurty on 03-12-2016
			harvestingRateDetails.setSno(harvestingRateDetailsBean.getSlNo());
		}
		return harvestingRateDetails;	
	}
	
	private HarvestingRatesSmry prepareModelForUpdateHarvestingRatesSmry(HarvestingRatesSmryBean harvestingRatesSmryBean) 
	{	
		Date date = new Date();
		HarvestingRatesSmry harvestingRatesSmry = new HarvestingRatesSmry();

		harvestingRatesSmry.setId(harvestingRatesSmryBean.getId());
		harvestingRatesSmry.setSeason(harvestingRatesSmryBean.getSeason());
		harvestingRatesSmry.setCirclecode(harvestingRatesSmryBean.getCircleCode());
	
		String effDate = harvestingRatesSmryBean.getEffectiveDate();
		harvestingRatesSmry.setEffectivedate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		harvestingRatesSmry.setHarvestingmode(harvestingRatesSmryBean.getHarvestingMode());

		return harvestingRatesSmry;		
	}
	//Modify for Update harvesting charges
	@RequestMapping(value = "/getUpdatedHarvestingCharges", method = RequestMethod.POST)
	public @ResponseBody
	List getUpdatedHarvestingCharges(@RequestBody JSONObject jsonData) throws Exception
	{
		SampleCardsBean sampleCardsBean =null;
		org.json.JSONObject response = new org.json.JSONObject();
		JSONObject jsonObj =null;
		JSONArray jArray = new JSONArray();

		String season=(String)jsonData.get("season");
		Integer circleCode=(Integer)jsonData.get("circleCode");
		String date=(String)jsonData.get("date");
		Date efDate=DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
		
		List dropdownList= caneAccountingFunctionalService.getRyotsByCircleandSeason(season,circleCode);
		if (dropdownList != null && dropdownList.size() > 0)
		{
			for (Iterator it = dropdownList.iterator(); it.hasNext();)
			{
				//Modified by DMurty on 07-05-2016
				jsonObj = new JSONObject();
				Object[] myResult = (Object[]) it.next();
				
				String ryotcode=null;
				ryotcode = (String) myResult[0];
									
				String ryotName = null;
				ryotName = (String) myResult[2];
				
				jsonObj.put("ryotCode", ryotcode);
				jsonObj.put("ryotName", ryotName);
				
				double harvestRate = 0.0;
				double slNo = 0;
				HarvestingRateDetails harvestingRateDetails = caneAccountingFunctionalService.getUpdatedHarvestingCharges(season,circleCode,efDate,ryotcode);
				if(harvestingRateDetails != null)
				{
					harvestRate = harvestingRateDetails.getRate();
					slNo = harvestingRateDetails.getSno();
				}
				jsonObj.put("rate", harvestRate);
				jsonObj.put("slNo", slNo);

				jArray.add(jsonObj);
			}
		}
		return jArray;
	}
	
	//----------------------------------------------- End of  Update Harvesting Charges ---------------------------------------------------------//
	
	//----------------------------------------------- Update Transport Rates --------------------------------------------------------------------//
	@RequestMapping(value = "/saveUpdateTransportCharges", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveUpdateTransportCharges(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{	
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String strQry = null;
		Object Qryobj = null;
		try
		{
			JSONArray gridData=(JSONArray) jsonData.get("gridData");
			JSONObject formData=(JSONObject) jsonData.get("formData");
			
			TransportingRatesSmryBean transportingRatesSmryBean = new ObjectMapper().readValue(formData.toString(), TransportingRatesSmryBean.class);
			TransportingRatesSmry transportingRatesSmry = prepareModelForTransportingRatesSmry(transportingRatesSmryBean);
			entityList.add(transportingRatesSmry);
			
			for(int i=0;i<gridData.size();i++)
			{
				TransportingRateDetailsBean transportingRateDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(), TransportingRateDetailsBean.class);
				String rtoyCode=transportingRateDetailsBean.getRyotCode();
				if(transportingRateDetailsBean.getSlNo() != null)
				{
					double slno = transportingRateDetailsBean.getSlNo();
					caneAccountingFunctionalService.deleteTransportCharges(rtoyCode,slno);
				}
			}
			for(int i=0;i<gridData.size();i++)
			{
				TransportingRateDetailsBean transportingRateDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(), TransportingRateDetailsBean.class);
				//Modified by DMurty on 10-08-2016
				if(transportingRateDetailsBean.getTransportContractorCode() != null)
				{
					TransportingRateDetails transportingRateDetails = prepareModelForTransportingRateDetails(transportingRateDetailsBean,transportingRatesSmryBean);
					entityList.add(transportingRateDetails);
					
					TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont = prepareModelForTranspChgSmryByRyotAndCont(transportingRateDetailsBean,transportingRatesSmryBean);
					
					String ryotCode = transportingRateDetails.getRyotcode();
					int contractorCode = transportingRateDetails.getTransportcontcode();
					String season = transportingRatesSmryBean.getSeason();
					
					String date= transportingRatesSmryBean.getEffectiveDate();
					Date efDate=DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
					double UpdatingAmt = 0;//transpChgSmryByRyotAndCont.getTotalamount();
					
					List TranspChgSmryByRyotAndContList = new ArrayList<Double>();
					TranspChgSmryByRyotAndContList =  caneAccountingFunctionalService.GetTranspChgSmryByRyotAndCont(ryotCode,contractorCode,season,efDate);
					if (TranspChgSmryByRyotAndContList != null && TranspChgSmryByRyotAndContList.size() > 0)
					{
						//Modified by DMurty on 17-10-2016
						for(int m = 0;m<TranspChgSmryByRyotAndContList.size();m++)
						{
							Map tempMap = new HashMap();
							tempMap = (Map) TranspChgSmryByRyotAndContList.get(m);
							double Amt = (Double) tempMap.get("totalamount");
							double weight = (Double) tempMap.get("transportedcanewt");
							int id = (Integer) tempMap.get("id");
							
							UpdatingAmt = weight*transportingRateDetailsBean.getRate();
							if(Amt == 0)
							{
								strQry = "UPDATE TranspChgSmryByRyotAndCont set totalamount="+UpdatingAmt+",pendingamount="+UpdatingAmt+",pendingpayable=0.0,totalpayableamount=0.0,transportcontcode="+contractorCode+" WHERE id="+id;//season='"+season+"' and ryotcode='"+ryotCode+"' and totalamount=0.0";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
						}
						//Amt = TranspChgSmryByRyotAndContList.get(0);
					}
					 
					TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate = prepareModelForTranspChgSmryByRyotContAndDate(transportingRateDetailsBean,transportingRatesSmryBean);
					
					ryotCode = transportingRateDetails.getRyotcode();
					contractorCode = transportingRateDetails.getTransportcontcode();
					season = transportingRatesSmryBean.getSeason();
					
					date= transportingRatesSmryBean.getEffectiveDate();
					efDate=DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
					UpdatingAmt = transpChgSmryByRyotAndCont.getTotalamount();
					double rate = transportingRateDetailsBean.getRate();
					List TranspChgSmryByRyotContAndDateList =  caneAccountingFunctionalService.GetTranspChgSmryByRyotContAndDate(ryotCode,contractorCode,season,efDate);
					if (TranspChgSmryByRyotContAndDateList != null && TranspChgSmryByRyotContAndDateList.size() > 0)
					{
						//Modified by DMurty on 17-10-2016
						for(int m = 0;m<TranspChgSmryByRyotContAndDateList.size();m++)
						{
							Map tempMap = new HashMap();
							tempMap = (Map) TranspChgSmryByRyotContAndDateList.get(m);
							double Amt = (Double) tempMap.get("totalamount");
							//Modified by DMurty on 24-02-2017. As per the concept of minimum tonnage, We need to consider MinCaneWeight.
							double weight = (Double) tempMap.get("transportedcanewt");
							double minweight = (Double) tempMap.get("mincanewt");
							int id = (Integer) tempMap.get("id");
							UpdatingAmt = minweight*transportingRateDetailsBean.getRate();
							if(Amt == 0)
							{
								strQry = "UPDATE TranspChgSmryByRyotContAndDate set totalamount="+UpdatingAmt+",transportcontcode="+contractorCode+",rate="+rate+" WHERE id="+id;//season='"+season+"' and ryotcode='"+ryotCode+"' and totalamount=0.0";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
						}
					}
				}
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}
	private TransportingRatesSmry prepareModelForTransportingRatesSmry(TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TransportingRatesSmry transportingRatesSmry = new TransportingRatesSmry();
		
		transportingRatesSmry.setSeason(transportingRatesSmryBean.getSeason());
		transportingRatesSmry.setCirclecode(transportingRatesSmryBean.getCircleCode());
		String effDate = transportingRatesSmryBean.getEffectiveDate();
		transportingRatesSmry.setEffectivedate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
	
		return transportingRatesSmry;
	}
	
	private TransportingRateDetails prepareModelForTransportingRateDetails(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TransportingRateDetails transportingRateDetails = new TransportingRateDetails();
		
		transportingRateDetails.setSeason(transportingRatesSmryBean.getSeason());
		String effDate = transportingRatesSmryBean.getEffectiveDate();
		transportingRateDetails.setEffectivedate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		transportingRateDetails.setRyotcode(transportingRateDetailsBean.getRyotCode());
		transportingRateDetails.setTransportcontcode(transportingRateDetailsBean.getTransportContractorCode());
		transportingRateDetails.setSlno(transportingRateDetailsBean.getSlNo());
		transportingRateDetails.setRate(transportingRateDetailsBean.getRate());
		
		return transportingRateDetails;
	}
	
	private TranspChgSmryByContAndDate prepareModelForTranspChgSmryByContAndDate(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TranspChgSmryByContAndDate transpChgSmryByContAndDate = new TranspChgSmryByContAndDate();
		
		String effDate = transportingRatesSmryBean.getEffectiveDate();
		transpChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		transpChgSmryByContAndDate.setSeason(transportingRatesSmryBean.getSeason());
		
		double netWeight = 0.0;
		int contCode = transportingRateDetailsBean.getTransportContractorCode();
		String ryotCode = transportingRateDetailsBean.getRyotCode();
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByContAndDate(contCode,ryotCode);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		
		transpChgSmryByContAndDate.setTransportedcanewt(netWeight);
		transpChgSmryByContAndDate.setTransportcontcode(transportingRateDetailsBean.getTransportContractorCode());
		
		double rate = transportingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;

		transpChgSmryByContAndDate.setTotalamount(finalAmt);
		transpChgSmryByContAndDate.setTotalextent(0.0);
		

		return transpChgSmryByContAndDate;
	}
	
	private TranspChgSmryByRyotAndCont prepareModelForTranspChgSmryByRyotAndCont(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont = new TranspChgSmryByRyotAndCont();
		
		transpChgSmryByRyotAndCont.setSeason(transportingRatesSmryBean.getSeason());
		transpChgSmryByRyotAndCont.setRyotcode(transportingRateDetailsBean.getRyotCode());
		transpChgSmryByRyotAndCont.setTransportcontcode(transportingRateDetailsBean.getTransportContractorCode());
	
		double netWeight = 0.0;
		int contCode = transportingRateDetailsBean.getTransportContractorCode();
		String ryotCode = transportingRateDetailsBean.getRyotCode();
		List<Double> WeightList = new ArrayList<Double>();
		//Modified by DMurty on 17-10-2016
		WeightList =  caneAccountingFunctionalService.getCaneWeightFromTranspChgSmryByRyotAndCont(ryotCode,transportingRatesSmryBean.getSeason());
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		
		transpChgSmryByRyotAndCont.setTransportedcanewt(netWeight);
		
		double rate = transportingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;

		transpChgSmryByRyotAndCont.setCompanypaidamount(0.0);
		transpChgSmryByRyotAndCont.setPendingamount(finalAmt);
		transpChgSmryByRyotAndCont.setPendingpayable(finalAmt);
		transpChgSmryByRyotAndCont.setTotalamount(finalAmt);
		transpChgSmryByRyotAndCont.setTotalpayableamount(finalAmt);
		transpChgSmryByRyotAndCont.setTotalextent(0.0);

		return transpChgSmryByRyotAndCont;
	}
	
	//TranspChgSmryByRyotContAndDate
	private TranspChgSmryByRyotContAndDate prepareModelForTranspChgSmryByRyotContAndDate(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate = new TranspChgSmryByRyotContAndDate();
		
		transpChgSmryByRyotContAndDate.setRate(transportingRateDetailsBean.getRate());
		
		String effDate = transportingRatesSmryBean.getEffectiveDate();
		transpChgSmryByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(effDate,Constants.GenericDateFormat.DATE_FORMAT));
		transpChgSmryByRyotContAndDate.setRyotcode(transportingRateDetailsBean.getRyotCode());
		transpChgSmryByRyotContAndDate.setSeason(transportingRatesSmryBean.getSeason());
		transpChgSmryByRyotContAndDate.setTransportcontcode(transportingRateDetailsBean.getTransportContractorCode());
		
		String ryotCode = transportingRateDetailsBean.getRyotCode();
		String season = transportingRatesSmryBean.getSeason();
		double netWeight = 0.0;
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		transpChgSmryByRyotContAndDate.setTransportedcanewt(netWeight);
		
		double rate = transportingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
		transpChgSmryByRyotContAndDate.setTotalamount(finalAmt);
		transpChgSmryByRyotContAndDate.setTotalextent(0.0);

		return transpChgSmryByRyotContAndDate;
	}
	
	private TranspChgSmryBySeason prepareModelForTranspChgSmryBySeason(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TranspChgSmryBySeason transpChgSmryBySeason = new TranspChgSmryBySeason();
		
		String ryotCode = transportingRateDetailsBean.getRyotCode();
		String season = transportingRatesSmryBean.getSeason();
		
		double netWeight = 0.0;
		transpChgSmryBySeason.setSeason(transportingRatesSmryBean.getSeason());
		
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		transpChgSmryBySeason.setTransportedcanewt(netWeight);
		
		double rate = transportingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
		transpChgSmryBySeason.setTotalamount(finalAmt);
		transpChgSmryBySeason.setPaidAmount(0.0);
		transpChgSmryBySeason.setPendingamount(finalAmt);
		transpChgSmryBySeason.setPendingpayable(finalAmt);
		transpChgSmryBySeason.setTotalpayableamount(finalAmt);
		transpChgSmryBySeason.setTotalextent(0.0);
			
		return transpChgSmryBySeason;
	}
	
	private TranspChgSmryBySeasonByCont prepareModelForTranspChgSmryBySeasonByCont(TransportingRateDetailsBean transportingRateDetailsBean,TransportingRatesSmryBean transportingRatesSmryBean) 
	{
		TranspChgSmryBySeasonByCont transpChgSmryBySeasonByCont = new TranspChgSmryBySeasonByCont();
		
		transpChgSmryBySeasonByCont.setSeason(transportingRatesSmryBean.getSeason());
		transpChgSmryBySeasonByCont.setTransportcontcode(transportingRateDetailsBean.getTransportContractorCode());
		
		String ryotCode = transportingRateDetailsBean.getRyotCode();
		String season = transportingRatesSmryBean.getSeason();
		
		double netWeight = 0.0;
		List<Double> WeightList = new ArrayList<Double>();
		WeightList =  caneAccountingFunctionalService.getCaneWeightByRyotCode(ryotCode,season);
		if (WeightList != null && WeightList.size() > 0)
		{
			netWeight = WeightList.get(0);
		}
		
		transpChgSmryBySeasonByCont.setTransportedcanewt(netWeight);
		
		double rate = transportingRateDetailsBean.getRate();
		double totalAmt = rate*netWeight;
		double finalAmt = totalAmt;
		
		transpChgSmryBySeasonByCont.setPaidAmount(0.0);
		transpChgSmryBySeasonByCont.setPendingamount(finalAmt);
		transpChgSmryBySeasonByCont.setPendingpayable(finalAmt);
		transpChgSmryBySeasonByCont.setTotalamount(finalAmt);
		transpChgSmryBySeasonByCont.setTotalpayableamount(finalAmt);
		transpChgSmryBySeasonByCont.setTotalextent(0.0);
		return transpChgSmryBySeasonByCont;
	}
//------------------------------------------Perform Cane Accounting Calculation-----------------------------------------------------//
	
	//Second  Tab Loading
	@RequestMapping(value = "/getCaneAccRulesWeighmentDtlssForCACalculation", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<CaneAccountingRulesWeighmentDetailsForPCACBean> getCaneAccRulesWeighmentDtlssForCACalculation(@RequestBody JSONObject jsonData) throws Exception
	{
		List<CaneAccountingRulesWeighmentDetailsForPCACBean> caneAccountingRulesForPCACBean =null;
		org.json.JSONObject jsonResponse = new org.json.JSONObject();
		
		String season=(String)jsonData.get("season");
		String datefrom=(String)jsonData.get("fromdate");
		String dateto=(String)jsonData.get("todate");
		String villageCode=(String)jsonData.get("villageCode");
		int category=(Integer)jsonData.get("ryotCategory");
		
		int advOrLoan=(Integer)jsonData.get("advOrLoan");
		int supplyType=(Integer)jsonData.get("supplyType");

		if(supplyType == 0)
		{
			Date date1=DateUtils.getSqlDateFromString(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			Date date2=DateUtils.getSqlDateFromString(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			
			List<CaneAcctRules> caneAccountingRules = caneAccountingFunctionalService.getCaneAccRulesForCalcForCaneAccounting(season,date1,date2);
			List weigmentDetailsList=caneAccountingFunctionalService.getWeighmentDetails(date1,date2,season,villageCode,category);
			logger.info("getCaneAccRulesWeighmentDtlssForCACalculation()========weigmentDetailsList=========="+weigmentDetailsList);

			if(weigmentDetailsList !=null)
				caneAccountingRulesForPCACBean =prepareBeanForCaneAccountingRulesWeighmentDetailsForPCACBean(caneAccountingRules.get(0),weigmentDetailsList);
			
			org.json.JSONObject caneAccountingRulesBeanJson=new org.json.JSONObject(caneAccountingRulesForPCACBean);
			jsonResponse.put("caneAccountingRules",caneAccountingRulesBeanJson);
		}
		else if(supplyType == 1)
		{
			List<CaneAccountingRulesWeighmentDetailsForPCACBean> beans = null;
			HashMap hmap=new HashMap();
			List weigmentDetailsList=null;//caneAccountingFunctionalService.getWeighmentDetails(date1,date2,season,villageCode,category);
			weigmentDetailsList=caneAccountingFunctionalService.getAdvOrLoanWithOrWithoutSupply(advOrLoan,supplyType,season,villageCode,category);
			if(weigmentDetailsList != null && weigmentDetailsList.size()>0)
			{
				beans = new ArrayList<CaneAccountingRulesWeighmentDetailsForPCACBean>();
				CaneAccountingRulesWeighmentDetailsForPCACBean bean = null;
				Double netWeight=0.00;
				for (int i=0;i<weigmentDetailsList.size();i++)
				{	
					Map tempMap = (Map) weigmentDetailsList.get(i);
					
					String ryotCode = (String) tempMap.get("ryotcode");
					String ryotName = (String) tempMap.get("ryotname");
					int branchcode = (Integer) tempMap.get("branchcode");
					String advanceName = (String) tempMap.get("branchname");
					
					double principle = caneAccountingFunctionalService.getPendingPrinciple(ryotCode,season,advOrLoan);
					
					bean = new CaneAccountingRulesWeighmentDetailsForPCACBean();
					bean.setId(i);
					bean.setSeason(season);
					bean.setSeasonCanePrice(0.0);
					bean.setIsPercentOrAmt((byte) 0);
					bean.setPercentForDed(0.0);
					bean.setRyotCode(ryotCode);
					bean.setRyotName(ryotName);
					bean.setVillageCode(villageCode);
					bean.setNetWeight(0.0);
					bean.setAmtForDed(principle);
					
					hmap.put(ryotCode, bean);
				} 
				Collection collection=hmap.values();
				Iterator it=collection.iterator();
				
				while(it.hasNext())
					beans.add((CaneAccountingRulesWeighmentDetailsForPCACBean)it.next());
				
				caneAccountingRulesForPCACBean = beans;

				org.json.JSONObject caneAccountingRulesBeanJson=new org.json.JSONObject(caneAccountingRulesForPCACBean);
				jsonResponse.put("caneAccountingRules",caneAccountingRulesBeanJson);
			}
		}
		//added by naidu on 21-03-2017
		else if(supplyType == 2)
		{
			String ss[]=season.split("-");
			String supseason=ss[1]+"-"+(Integer.parseInt(ss[1])+1);
			List<CaneAccountingRulesWeighmentDetailsForPCACBean> beans = null;
			HashMap hmap=new HashMap();
			List seedSuppList=null;//caneAccountingFunctionalService.getWeighmentDetails(date1,date2,season,villageCode,category);
			seedSuppList=caneAccountingFunctionalService.getSeedSupplierDetails(supseason);
			if(seedSuppList != null && seedSuppList.size()>0)
			{
				beans = new ArrayList<CaneAccountingRulesWeighmentDetailsForPCACBean>();
				CaneAccountingRulesWeighmentDetailsForPCACBean bean = null;
				Double netWeight=0.00;
				for (int i=0;i<seedSuppList.size();i++)
				{	
					Map tempMap = (Map) seedSuppList.get(i);
					
					String ryotCode = (String) tempMap.get("seedsuppliercode");
					String ryotName = (String) tempMap.get("ryotname");
					
					double principle =(Double) tempMap.get("principle");
					
					bean = new CaneAccountingRulesWeighmentDetailsForPCACBean();
					bean.setId(i);
					bean.setSeason(season);
					bean.setSeasonCanePrice(0.0);
					bean.setIsPercentOrAmt((byte) 0);
					bean.setPercentForDed(0.0);
					bean.setRyotCode(ryotCode);
					bean.setRyotName(ryotName);
					bean.setNetWeight(0.0);
					bean.setAmtForDed(principle);
					
					hmap.put(ryotCode, bean);
				} 
				Collection collection=hmap.values();
				Iterator it=collection.iterator();
				
				while(it.hasNext())
					beans.add((CaneAccountingRulesWeighmentDetailsForPCACBean)it.next());
				
				caneAccountingRulesForPCACBean = beans;

				org.json.JSONObject caneAccountingRulesBeanJson=new org.json.JSONObject(caneAccountingRulesForPCACBean);
				jsonResponse.put("caneAccountingRules",caneAccountingRulesBeanJson);
			}
		}
		return caneAccountingRulesForPCACBean;
	}
	
	private List<CaneAccountingRulesWeighmentDetailsForPCACBean> prepareBeanForCaneAccountingRulesWeighmentDetailsForPCACBean(CaneAcctRules caneAccountingRules,List weigmentDetailsLst) 
	{
		List<CaneAccountingRulesWeighmentDetailsForPCACBean> beans = null;
		WeighmentDetails weigmentDetails=null;
		HashMap hmap=new HashMap();
		if (weigmentDetailsLst != null && !weigmentDetailsLst.isEmpty())
		{
			beans = new ArrayList<CaneAccountingRulesWeighmentDetailsForPCACBean>();				
			CaneAccountingRulesWeighmentDetailsForPCACBean bean = null;
			Double netWeight=0.00;
			String ryotCode="";
			for (int i=0;i<weigmentDetailsLst.size();i++)
			{	
				logger.info("prepareBeanForCaneAccountingRulesWeighmentDetailsForPCACBean()------ "+i);
				bean = new CaneAccountingRulesWeighmentDetailsForPCACBean();
				weigmentDetails=(WeighmentDetails)weigmentDetailsLst.get(i);
				if(ryotCode.equalsIgnoreCase(weigmentDetails.getRyotcode()))
				{
					netWeight=netWeight+weigmentDetails.getNetwt();	
					netWeight =Double.parseDouble(new DecimalFormat("##.###").format(netWeight));

					bean.setId(caneAccountingRules.getCalcid());
					
					bean.setSeason(caneAccountingRules.getSeason());
					bean.setSeasonCanePrice(caneAccountingRules.getSeasoncaneprice());			
					bean.setIsPercentOrAmt(caneAccountingRules.getIspercentoramt());
					
					if(caneAccountingRules.getIspercentoramt()==0)
					{				
						bean.setPercentForDed(caneAccountingRules.getPercentforded());			
					}
					else
					{
						bean.setAmtForDed(caneAccountingRules.getAmtforded());
					}					
					bean.setRyotCode(weigmentDetails.getRyotcode());
					bean.setRyotName(weigmentDetails.getRyotname());
					bean.setVillageCode(weigmentDetails.getVillagecode());
					bean.setNetWeight(netWeight);
					hmap.put(weigmentDetails.getRyotcode(), bean);					
				}
				else
				{
					bean.setId(caneAccountingRules.getCalcid());
					bean.setSeason(caneAccountingRules.getSeason());
					bean.setSeasonCanePrice(caneAccountingRules.getSeasoncaneprice());			
					bean.setIsPercentOrAmt(caneAccountingRules.getIspercentoramt());
					if(caneAccountingRules.getIspercentoramt()==0)
					{				
						bean.setPercentForDed(caneAccountingRules.getPercentforded());			
					}
					else
					{
						bean.setAmtForDed(caneAccountingRules.getAmtforded());
					}					
					bean.setRyotCode(weigmentDetails.getRyotcode());
					bean.setRyotName(weigmentDetails.getRyotname());
					bean.setVillageCode(weigmentDetails.getVillagecode());
					
					double wt = weigmentDetails.getNetwt();
					wt =Double.parseDouble(new DecimalFormat("##.###").format(wt));
					bean.setNetWeight(wt);
					
					hmap.put(weigmentDetails.getRyotcode(), bean);
					
					netWeight=wt;//weigmentDetails.getNetwt();
					
				}				
				ryotCode=weigmentDetails.getRyotcode();	
			}
		}
		Collection collection=hmap.values();
		Iterator it=collection.iterator();
		
		while(it.hasNext())
			beans.add((CaneAccountingRulesWeighmentDetailsForPCACBean)it.next());
		return beans;
	}
	
	//Second Tab Form Submit
	@RequestMapping(value = "/saveCaneValueSplitupDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean saveCaneValueSplitupDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List caneValueSplitupTablesList=new ArrayList();
		boolean updateFlag= false;
		try
		{	
			JSONObject formData = (JSONObject) jsonData.get("formData");
			int advOrLoan = formData.getInt("advOrLoan");
			int supplyType = formData.getInt("supplyType");
			
 			JSONArray gridData=(JSONArray) jsonData.get("gridData");
 			double suppliedcanewt = 0.0;
 			double netprice = 0.0;
 			double forsbacs = 0.0;
 			double foradvandloans = 0.0;
 			double dedparttosbacpayable = 0.0;

			HttpSession session=request.getSession(false);
			Employees employ = (Employees) session.getAttribute(Constants.USER);
			String user = employ.getLoginid();
			CaneValueSplitupBean caneValueSplitupBean1 = new ObjectMapper().readValue(gridData.get(0).toString(), CaneValueSplitupBean.class);
			
			//Delete Rec in Temp table
			ahFuctionalService.deleteTempRecords("CaneValueSplitupTemp",user,caneValueSplitupBean1.getCaneAccSlno(),caneValueSplitupBean1.getSeason());
			ahFuctionalService.deleteTempRecords("CaneAccountSmrytemp",user,caneValueSplitupBean1.getCaneAccSlno(),caneValueSplitupBean1.getSeason());
			
			for(int i=0;i<gridData.size();i++)
			{
				logger.info("saveCaneValueSplitupDetails()========i=========="+i);

				CaneValueSplitupBean caneValueSplitupBean = new ObjectMapper().readValue(gridData.get(i).toString(), CaneValueSplitupBean.class);
				suppliedcanewt += caneValueSplitupBean.getNetWeight();
				
				//Modified by DMurty on 04-01-2017
				double totalVal = caneValueSplitupBean.getTotalValue();
				totalVal = agricultureHarvestingController.round(totalVal);
				netprice += totalVal;
				
				double forSbAc = caneValueSplitupBean.getForSbac();
				forSbAc = agricultureHarvestingController.round(forSbAc);
				forsbacs += forSbAc;

				foradvandloans += caneValueSplitupBean.getForLoans();
				
				double forDed = caneValueSplitupBean.getForSbac();
				forDed = agricultureHarvestingController.round(forDed);
				dedparttosbacpayable += forDed;

				
				//In CaneAccountSmrytemp table, We need to post only one record
				 if(i == 0)
				 {
					CaneAccountSmrytemp caneAccountSmrytemp=prepareModelForCaneAccountSmrytemp(caneValueSplitupBean);
					//Added by Dmurty on 01-03-2017
					caneAccountSmrytemp.setAcctype((byte) supplyType);
					
					caneValueSplitupTablesList.add(caneAccountSmrytemp);
				 }
				 if(supplyType == 0)
				 {
					 CaneValueSplitupTemp caneValueSplitupTemp = prepareModelForCaneValueSplitupTemp(caneValueSplitupBean);
					 caneValueSplitupTemp.setAcctype((byte) supplyType);
						caneValueSplitupTablesList.add(caneValueSplitupTemp);
				 }
				 else  if(supplyType == 1)
				 {
					 boolean isSelected = caneValueSplitupBean.isSelectData();
					 if(isSelected == true)
					 {
						 CaneValueSplitupTemp caneValueSplitupTemp = prepareModelForCaneValueSplitupTemp(caneValueSplitupBean);
						 caneValueSplitupTablesList.add(caneValueSplitupTemp);
					 }
				 }
				 else if(supplyType == 2)
				 {
					 boolean isSelected = caneValueSplitupBean.isSelectData();
					 if(isSelected == true)
					 {
						 CaneValueSplitupTemp caneValueSplitupTemp = prepareModelForCaneValueSplitupTemp(caneValueSplitupBean);
						 caneValueSplitupTemp.setAcctype((byte) supplyType);
						 caneValueSplitupTablesList.add(caneValueSplitupTemp);
					 }
				 }
			}
			updateFlag=caneAccountingFunctionalService.saveMultipleEntities(caneValueSplitupTablesList);
			logger.info("saveCaneValueSplitupDetails()========updateFlag=========="+updateFlag);

			return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	}		
	
	private CaneValueSplitup prepareModelForCaneValueSplitup(CaneValueSplitupBean caneValueSplitupBean) 
	{
		CaneValueSplitup caneValueSplitup=new CaneValueSplitup();		
		caneValueSplitup.setRyotcode(caneValueSplitupBean.getRyotCode());
		caneValueSplitup.setRyotname(caneValueSplitupBean.getRyotName());
		caneValueSplitup.setSlno(caneValueSplitupBean.getSlno());
		caneValueSplitup.setSeason(caneValueSplitupBean.getSeason());
		caneValueSplitup.setForloans(caneValueSplitupBean.getForLoans());
		caneValueSplitup.setForsbacs(caneValueSplitupBean.getForSbac());
		caneValueSplitup.setCanesupplied(caneValueSplitupBean.getNetWeight());
		caneValueSplitup.setRate(caneValueSplitupBean.getSeasonCanePrice());
		caneValueSplitup.setTotalvalue(caneValueSplitupBean.getTotalValue());	
		caneValueSplitup.setCaneacslno(caneValueSplitupBean.getCaneAccSlno());
		
		return caneValueSplitup;
	}
	
	private CaneValueSplitupTemp prepareModelForCaneValueSplitupTemp(CaneValueSplitupBean caneValueSplitupBean) 
	{

		HttpSession session=request.getSession(false);
		Employees employ = (Employees) session.getAttribute(Constants.USER);
		String user = employ.getLoginid();
		
		//DecimalFormat df = new DecimalFormat("#.##");
		//String netWt=df.format(caneValueSplitupBean.getNetWeight());
		
		CaneValueSplitupTemp caneValueSplitupTemp=new CaneValueSplitupTemp();		
		caneValueSplitupTemp.setRyotcode(caneValueSplitupBean.getRyotCode());
		caneValueSplitupTemp.setRyotname(caneValueSplitupBean.getRyotName());
		//caneValueSplitupTemp.setSlno(caneValueSplitupBean.getSlno());
		caneValueSplitupTemp.setSeason(caneValueSplitupBean.getSeason());
		
		//Modified by DMurty on 04-01-2017
		double forLoans = caneValueSplitupBean.getForLoans();
		forLoans = agricultureHarvestingController.round(forLoans);
		caneValueSplitupTemp.setForloans(forLoans);
		
		double forSbAc = caneValueSplitupBean.getForSbac();
		forSbAc = agricultureHarvestingController.round(forSbAc);
		caneValueSplitupTemp.setForsbacs(forSbAc);
		
		double totalVal = caneValueSplitupBean.getTotalValue();
		totalVal = agricultureHarvestingController.round(totalVal);
		caneValueSplitupTemp.setTotalvalue(totalVal);
		
		//Modified by DMurty on 15-12-2016
		caneValueSplitupTemp.setCanesupplied(caneValueSplitupBean.getNetWeight());
		caneValueSplitupTemp.setRate(caneValueSplitupBean.getSeasonCanePrice());
		caneValueSplitupTemp.setCaneacslno(caneValueSplitupBean.getCaneAccSlno());
		//Added by DMurty on 15-11-2016
		caneValueSplitupTemp.setLogin(user);
		caneValueSplitupTemp.setSlno(0);
		caneValueSplitupTemp.setCancelproportion((byte) 1);
		return caneValueSplitupTemp;
	}


	private CaneValueSplitupSummary prepareModelForCaneValueSplitupSummary(CaneValueSplitupBean caneValueSplitupBean) 
	{
		CaneValueSplitupSummary caneValueSplitupSummary= null;
		List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitupSummary(caneValueSplitupBean.getSeason(),caneValueSplitupBean.getRyotCode());

		if(caneValueSplitupSummaryList !=null)
		{
			caneValueSplitupSummary=(CaneValueSplitupSummary)caneValueSplitupSummaryList.get(0);
			caneValueSplitupSummary.setTotalvalue(caneValueSplitupSummary.getTotalvalue()+caneValueSplitupBean.getTotalValue());
			caneValueSplitupSummary.setForloans(caneValueSplitupSummary.getForloans()+caneValueSplitupBean.getForLoans());
			caneValueSplitupSummary.setForsbacs(caneValueSplitupSummary.getForsbacs()+caneValueSplitupBean.getForSbac());
			caneValueSplitupSummary.setId(caneValueSplitupSummary.getId());
		}
		else
		{
			caneValueSplitupSummary=new CaneValueSplitupSummary();			
			caneValueSplitupSummary.setSeason(caneValueSplitupBean.getSeason());
			caneValueSplitupSummary.setRyotcode(caneValueSplitupBean.getRyotCode());
			caneValueSplitupSummary.setRyotname(caneValueSplitupBean.getRyotName());
			caneValueSplitupSummary.setCanesupplied(caneValueSplitupBean.getNetWeight());
			caneValueSplitupSummary.setTotalvalue(caneValueSplitupBean.getTotalValue());
			caneValueSplitupSummary.setForloans(caneValueSplitupBean.getForLoans());
			caneValueSplitupSummary.setForsbacs(caneValueSplitupBean.getForSbac());
		}
		
		return caneValueSplitupSummary;
	}
	
	
	private CaneValueSplitupDetailsByDate prepareModelForCaneValueSplitupDetailsByDate(CaneValueSplitupBean caneValueSplitupBean,Map tempMap,String user) 
	{
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String effDate = df.format(date);
		
		CaneValueSplitupDetailsByDate caneValueSplitupDetailsByDate= null;
		List caneValueSplitupDetailsByDateList=caneAccountingFunctionalService.getCaneValueSplitupDetailsByDate(caneValueSplitupBean.getSeason(),caneValueSplitupBean.getRyotCode(),effDate);
		
		if(caneValueSplitupDetailsByDateList !=null)
		{
			double suppliedcanewt = (Double) tempMap.get("suppliedcanewt");
			double netprice = (Double) tempMap.get("netprice");
			double foradvandloans = (Double) tempMap.get("foradvandloans");
			double forsbacs = (Double) tempMap.get("forsbacs");
			double dedparttosbacpayable = (Double) tempMap.get("dedparttosbacpayable");

			caneValueSplitupDetailsByDate=(CaneValueSplitupDetailsByDate)caneValueSplitupDetailsByDateList.get(0);			
			caneValueSplitupDetailsByDate.setNetprice(netprice+caneValueSplitupBean.getTotalValue());
			caneValueSplitupDetailsByDate.setForadvandloans(foradvandloans+caneValueSplitupBean.getForLoans());
			caneValueSplitupDetailsByDate.setForsbacs(forsbacs+caneValueSplitupBean.getForSbac());
			caneValueSplitupDetailsByDate.setSuppliedcanewt(suppliedcanewt+caneValueSplitupBean.getNetWeight());
			caneValueSplitupDetailsByDate.setId(caneValueSplitupDetailsByDate.getId());
		}
		else
		{
			double suppliedcanewt = (Double) tempMap.get("suppliedcanewt");
			double netprice = (Double) tempMap.get("netprice");
			double foradvandloans = (Double) tempMap.get("foradvandloans");
			double forsbacs = (Double) tempMap.get("forsbacs");
			double dedparttosbacpayable = (Double) tempMap.get("dedparttosbacpayable");
			Date caneAccDate = (Date) tempMap.get("caneAccDate");
			
			caneValueSplitupDetailsByDate=new CaneValueSplitupDetailsByDate();
			caneValueSplitupDetailsByDate.setSeason(caneValueSplitupBean.getSeason());
			caneValueSplitupDetailsByDate.setSuppliedcanewt(suppliedcanewt);
			caneValueSplitupDetailsByDate.setForsbacs(forsbacs);
			caneValueSplitupDetailsByDate.setForadvandloans(foradvandloans);
			caneValueSplitupDetailsByDate.setDedparttosbacpayable(forsbacs);
			caneValueSplitupDetailsByDate.setCaneaccountingdate(caneAccDate);
			caneValueSplitupDetailsByDate.setNetprice(netprice);
			caneValueSplitupDetailsByDate.setCaneacctslno(caneValueSplitupBean.getCaneAccSlno());
			caneValueSplitupDetailsByDate.setLogin(user);
			//Added by DMurty on 23-11-2016
			caneValueSplitupDetailsByDate.setCdc(0.0);
			caneValueSplitupDetailsByDate.setHarvestchargespayable(0.0);
			caneValueSplitupDetailsByDate.setLotaladvpayable(0.0);
			caneValueSplitupDetailsByDate.setOtherded(0.0);
			caneValueSplitupDetailsByDate.setTotalloanpayable(0.0);
			caneValueSplitupDetailsByDate.setTotalsbacpayable(0.0);
			caneValueSplitupDetailsByDate.setTotalvalue(0.0);
			caneValueSplitupDetailsByDate.setTransportchargespayable(0.0);
			caneValueSplitupDetailsByDate.setUc(0.0);
		}
		return caneValueSplitupDetailsByDate;
	}
	
	private CaneValueSplitupDetailsByRyot prepareModelForCaneValueSplitupDetailsByRyot(CaneValueSplitupBean caneValueSplitupBean) 
	{		
		CaneValueSplitupDetailsByRyot caneValueSplitupDetailsByRyot= null;
		List caneValueSplitupDetailsByRyotList=caneAccountingFunctionalService.getCaneValueSplitupDetailsByRyot(caneValueSplitupBean.getRyotCode());
		if(caneValueSplitupDetailsByRyotList !=null)
		{
			caneValueSplitupDetailsByRyot=(CaneValueSplitupDetailsByRyot)caneValueSplitupDetailsByRyotList.get(0);
			
			caneValueSplitupDetailsByRyot.setForadvloans(caneValueSplitupDetailsByRyot.getForadvloans()+caneValueSplitupBean.getForLoans());
			caneValueSplitupDetailsByRyot.setFordeductions(caneValueSplitupDetailsByRyot.getFordeductions()+caneValueSplitupBean.getForSbac());
		}
		else
		{
			caneValueSplitupDetailsByRyot=new CaneValueSplitupDetailsByRyot();
			List<Ryot> ryot = ryotService.getRyotByRyotCode(caneValueSplitupBean.getRyotCode());
			caneValueSplitupDetailsByRyot.setRyotcode(caneValueSplitupBean.getRyotCode());
			caneValueSplitupDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
			caneValueSplitupDetailsByRyot.setForadvloans(caneValueSplitupBean.getForLoans());
			caneValueSplitupDetailsByRyot.setFordeductions(caneValueSplitupBean.getForSbac());
			caneValueSplitupDetailsByRyot.setVillagecode(caneValueSplitupBean.getVillageCode());
			caneValueSplitupDetailsByRyot.setAdvancespaid(0.00);
			caneValueSplitupDetailsByRyot.setHarvestchargespaid(0.00);
			caneValueSplitupDetailsByRyot.setLoansparttosbac(caneValueSplitupBean.getForSbac());
			caneValueSplitupDetailsByRyot.setTransportchargespaid(0.00);
			caneValueSplitupDetailsByRyot.setSeason(caneValueSplitupBean.getSeason());
			caneValueSplitupDetailsByRyot.setSuppliedcaneWt(caneValueSplitupBean.getNetWeight());
			//Added by DMurty on 23-11-2016
			caneValueSplitupDetailsByRyot.setCdcpaid(0.0);
			caneValueSplitupDetailsByRyot.setDedparttosbac(0.0);
			caneValueSplitupDetailsByRyot.setOtherdedpaid(0.0);
			caneValueSplitupDetailsByRyot.setTieuploanspaid(0.0);
			caneValueSplitupDetailsByRyot.setTotalsbpaid(0.0);
			caneValueSplitupDetailsByRyot.setTotalvalue(0.0);
			caneValueSplitupDetailsByRyot.setUcpaid(0.0);
		}
		return caneValueSplitupDetailsByRyot;
	}
	
	
	private CaneAccountSmry prepareModelForCaneAccountSmry(CaneValueSplitupBean caneValueSplitupBean) 
	{
		
		CaneAccountSmry caneAccountSmry= null;
		List caneAccountSmryList=caneAccountingFunctionalService.getCaneAccountSmry(caneValueSplitupBean.getSeason(),caneValueSplitupBean.getSlno());

		if(caneAccountSmryList !=null)
		{
			caneAccountSmry=(CaneAccountSmry)caneAccountSmryList.get(0);			
		}
		else
		{
			//Added by DMurty on 24-11-2016
			Date currDate = new Date();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strcurrDate = df.format(currDate);
			currDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			caneAccountSmry=new CaneAccountSmry();
			caneAccountSmry.setCaneacslno(caneValueSplitupBean.getCaneAccSlno());
			caneAccountSmry.setDatefrom(DateUtils.getSqlDateFromString(caneValueSplitupBean.getFromDate(),Constants.GenericDateFormat.DATE_FORMAT));
			caneAccountSmry.setDateto(DateUtils.getSqlDateFromString(caneValueSplitupBean.getTodate(),Constants.GenericDateFormat.DATE_FORMAT));
			caneAccountSmry.setDateofcalc(currDate);
			caneAccountSmry.setSeason(caneValueSplitupBean.getSeason());
			//added by sahadeva 28/10/2016
			HttpSession session=request.getSession(false);
			Employees employ = (Employees) session.getAttribute(Constants.USER);
			String user = employ.getLoginid();
			caneAccountSmry.setLogin(user);	
			caneAccountSmry.setTransactioncode(0);
		}
		
		return caneAccountSmry;
	}
	private CaneAccountSmrytemp prepareModelForCaneAccountSmrytemp(CaneValueSplitupBean caneValueSplitupBean) 
	{
		CaneAccountSmrytemp caneAccountSmrytemp =  null;
		List caneAccountSmrytempList=caneAccountingFunctionalService.getCaneAccountSmrytemp(caneValueSplitupBean.getSeason(),caneValueSplitupBean.getSlno());

		if(caneAccountSmrytempList !=null)
		{
			caneAccountSmrytemp=(CaneAccountSmrytemp)caneAccountSmrytempList.get(0);			
		}
		else
		{
			//Added by DMurty on 24-11-2016
			/*Date currDate = new Date();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strcurrDate = df.format(currDate);
			currDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);*/
			
			String caneAccDate = caneValueSplitupBean.getAllRepayDate();
			Date currDate = DateUtils.getSqlDateFromString(caneAccDate,Constants.GenericDateFormat.DATE_FORMAT);

			caneAccountSmrytemp=new CaneAccountSmrytemp();			
			caneAccountSmrytemp.setCaneacslno(caneValueSplitupBean.getCaneAccSlno());
			caneAccountSmrytemp.setDatefrom(DateUtils.getSqlDateFromString(caneValueSplitupBean.getFromDate(),Constants.GenericDateFormat.DATE_FORMAT));
			caneAccountSmrytemp.setDateto(DateUtils.getSqlDateFromString(caneValueSplitupBean.getTodate(),Constants.GenericDateFormat.DATE_FORMAT));
			//Added by DMurty on 24-11-2016
			caneAccountSmrytemp.setDateofcalc(currDate);
			caneAccountSmrytemp.setSeason(caneValueSplitupBean.getSeason());
			//added by sahadeva 28/10/2016
			HttpSession session=request.getSession(false);
			Employees employee = (Employees) session.getAttribute(Constants.USER);
			String userName = employee.getLoginid();
			
			caneAccountSmrytemp.setLogin(userName);			
		}
		return caneAccountSmrytemp;
	}

	//Third Tab Loading
	@RequestMapping(value = "/getAllCaneValuesSplitupDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllCaneValuesSplitupDetails(@RequestBody JSONObject jsonData)throws Exception 
	{
		logger.info("getAllCaneValuesSplitupDetails()========jsonData=========="+jsonData);

		String season=(String)jsonData.getString("season");
		Integer caneaccslno=(Integer)jsonData.getInt("caneAccSlno");
		
		String fromdate=(String)jsonData.getString("fromDate");
		String toDate=(String)jsonData.getString("toDate");
		
		org.json.JSONArray jsonArray = new org.json.JSONArray();
		net.sf.json.JSONObject jsonObject = null;
		Map hm=null;
		String ryotCode = null;
		List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitupTemp(season,caneaccslno);
		logger.info("getAllCaneValuesSplitupDetails()========caneValueSplitupSummaryList=========="+caneValueSplitupSummaryList);
		for(int i=0;i<caneValueSplitupSummaryList.size();i++)
		{
			logger.info("getAllCaneValuesSplitupDetails()========i=========="+i); 

			List standardDeductionsList=caneAccountingFunctionalService.getAllDeductions(season,caneaccslno,(Map)caneValueSplitupSummaryList.get(i));

			jsonObject = new net.sf.json.JSONObject();
			hm=new HashMap();
			hm=(Map)caneValueSplitupSummaryList.get(i);

			jsonObject.putAll(hm);
			jsonObject.put("caneacslno",standardDeductionsList);
			jsonArray.put(jsonObject);	
		}
 		logger.info("getAllDeductions()==========="+jsonArray.toString());

		return jsonArray.toString();
	}
	
	@RequestMapping(value = "/getDeductionValues", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getDeductionValues(@RequestBody JSONObject jsonData)throws Exception 
	{
		logger.info("getDeductionValues()------jsonData"+jsonData);
		String season=(String)jsonData.getString("season");
		Integer caneaccslno=(Integer)jsonData.getInt("caneAccSlno");		
		String fromdate=(String)jsonData.getString("fromDate");
		String toDate=(String)jsonData.getString("toDate");
		
		Date date1=DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT);
		Date date2=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		logger.info("getDeductionValues()------date1"+date1);
		logger.info("getDeductionValues()------date2"+date2);

		org.json.JSONArray jsonArray = new org.json.JSONArray();
		net.sf.json.JSONObject jsonObject = null;
		List newList=new ArrayList();
		List newList1=new ArrayList();
		HashMap hashM=null;
		Map hm=null;
		List FinalList = new ArrayList();
		List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitupTemp(season,caneaccslno);
		logger.info("getDeductionValues()------caneValueSplitupSummaryList"+caneValueSplitupSummaryList);
		Map valueMap=null;
		for(int j=0;j<caneValueSplitupSummaryList.size();j++)
		{	
			Map caneValueMap=(Map)caneValueSplitupSummaryList.get(j);
			logger.info("getDeductionValues()------caneValueMap"+caneValueMap);

			List pendingDeductionsList=caneAccountingFunctionalService.getPendingAmount(season,caneValueMap.get("ryotcode").toString());
			logger.info("getDeductionValues()------pendingDeductionsList"+pendingDeductionsList);

			List newDeductionsList=caneAccountingFunctionalService.getNewAmount(season,caneValueMap.get("ryotcode").toString(),date1,date2);
			logger.info("getDeductionValues()------newDeductionsList"+newDeductionsList);
			if(newDeductionsList!=null && newDeductionsList.size()>0)
			{
				newList.add(newDeductionsList);
			}
			else
			{
				hashM=new HashMap();
				hashM.put("cdcamount","0.00");
				hashM.put("icpamount","0.00");
				hashM.put("hcamount","0.00");
				hashM.put("tcamount","0.00");
				hashM.put("ucamount","0.00");
				hashM.put("wextent","0.00");	
				newList.add(hashM);
			}
			
			if(pendingDeductionsList!=null)
			{
				valueMap=(Map)pendingDeductionsList.get(0);
				hashM=new HashMap();
				hashM.put("cdcpayable","0.00");
				hashM.put("icppayable","0.00");
				hashM.put("hcpayable",valueMap.get("pendingpayable"));
				hashM.put("tcpayable",valueMap.get("pendingpayable"));
				hashM.put("ucpayable","0.00");
				//hashM.put("wextent",valueMap.get("totalextent"));
				newList1.add(hashM);
			}
			else
			{
				hashM=new HashMap();
				hashM.put("cdcpayable","0.00");
				hashM.put("icppayable","0.00");
				hashM.put("hcpayable","0.00");
				hashM.put("tcpayable","0.00");
				hashM.put("ucpayable","0.00");
				//hashM.put("wextent","0.00");	
				newList1.add(hashM);
			}
		}
		Object ob=null;
		for(int i=0;i<caneValueSplitupSummaryList.size();i++)
		{
			jsonObject = new net.sf.json.JSONObject();
			hm=new HashMap();
			hm=(Map)caneValueSplitupSummaryList.get(i);	
			jsonObject.putAll(hm);			
			jsonObject.put("caneacslno",newList.get(i));
			jsonObject.put("forsbacs",newList1.get(i));
			logger.info("getDeductionValues()------hm"+hm);
			logger.info("getDeductionValues()------newList.get(i)"+newList.get(i));
			logger.info("getDeductionValues()------newList1.get(i)"+newList1.get(i));
			jsonArray.put(jsonObject);
		}
		logger.info("getDeductionValues()------jsonArray"+jsonArray);
		logger.info("----deduction values--"+jsonArray.toString());
		return jsonArray.toString();
	}
	
	@RequestMapping(value = "/saveDeductionsAndSBDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean saveDeductionsAndSBDetails(@RequestBody JSONArray jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List deductionsAndSBTablesList=new ArrayList();
		boolean updateFlag= false;
		Object Qryobj = null;
		List UpdateList = new ArrayList();
		logger.info("saveDeductionsAndSBDetails()------"+jsonData);
		try
		{	
			//Added by DMurty on 16-11-2016
			JSONObject jsonObject1=(JSONObject)jsonData.get(0);
			Integer caneAccSlno1=(Integer) jsonObject1.get("caneAccSlno");				
			String season1=(String) jsonObject1.get("season");	
			
 			HttpSession session=request.getSession(false);
 			Employees employee = (Employees) session.getAttribute(Constants.USER);
 			String userName = employee.getLoginid();
 			caneAccountingFunctionalService.TruncateTempTable("DedDetails_temp",season1,caneAccSlno1,userName);
 			logger.info("saveDeductionsAndSBDetails()------userName=="+userName);

			for(int i=0;i<jsonData.size();i++)
			{
				logger.info("saveDeductionsAndSBDetails()------i"+i);

				JSONObject jsonObject=(JSONObject)jsonData.get(i);
				//String ryotName=(String) jsonObject.get("RyotName");	
				Integer caneAccSlno=(Integer) jsonObject.get("caneAccSlno");				
				String ryotCode=(String) jsonObject.get("ryotcode");
				//int forsbacs=(Integer) jsonObject.get("forsbacs");
				String ryotsbac=(String) jsonObject.get("ryotsbac");	
				String season=(String) jsonObject.get("season");	
				
	 			JSONArray gridData=(JSONArray) jsonObject.get("caneacslno");	
	 			double ryotsbacc = Double.parseDouble(ryotsbac);
	 			
	 			/*//Added by DMurty on 19-12-2016
	 			List EffectedRyotList = new ArrayList();
	 			EffectedRyotList = ahFuctionalService.getEffectedRyots(ryotCode);
	 			if(EffectedRyotList != null && EffectedRyotList.size()>0)
				{
	 				double finalryotsbacc = 0.0;
					List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(caneAccSlno1,season,ryotCode);
					logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
				    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
				    {
				    	Map tempMap = new HashMap();
				    	tempMap = (Map) dedPartForRyotList.get(0);
				    	double loanPartAmount = (Double) tempMap.get("forloans");
				    	loanPartAmount = loanPartAmount+ryotsbacc;
				    	
				    	String strQry = "Update CaneValueSplitupTemp set forsbacs="+finalryotsbacc+",forloans="+loanPartAmount+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
			 			Qryobj = strQry;
						UpdateList.add(Qryobj);
				    }
				}
	 			else
	 			{
	 				double advancesAndLoans = 0.0;
	 				int advanceCode = 9;
		 			//Getting Principles >0. So if any advance is there amount will not go for sb.
		 			List advPrincipleList = new ArrayList();
					advPrincipleList = ahFuctionalService.getAdvancePrinciplesNew(ryotCode,season);
					if(advPrincipleList != null && advPrincipleList.size()>0)
					{
						double advanceamt = 0.0;
						Map tempMap = new HashMap();
						tempMap = (Map) advPrincipleList.get(0);
						if((Double) tempMap.get("advanceamount") != null)
						{
							advanceamt = (Double) tempMap.get("advanceamount");
						}
						advancesAndLoans += advanceamt;
					}
					
					List loanPrincipleList = ahFuctionalService.getLoanPrinciples(ryotCode,season);
					if(loanPrincipleList != null && loanPrincipleList.size()>0)
					{
						double loanAmount = 0.0;
						Map tempMap = new HashMap();
						tempMap = (Map) loanPrincipleList.get(0);
						if((Double) tempMap.get("loanamount") != null)
						{
							loanAmount = (Double) tempMap.get("loanamount");
						}
						advancesAndLoans += loanAmount;
					}
					
					if(advancesAndLoans>0)
					{
						double loanPartAmount = 0.0;
						double finalryotsbacc = 0.0;
						List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(caneAccSlno1,season,ryotCode);
						logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
					    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
					    {
					    	Map tempMap = new HashMap();
					    	tempMap = (Map) dedPartForRyotList.get(0);
					    	loanPartAmount = (Double) tempMap.get("forloans");
					    	loanPartAmount = loanPartAmount+ryotsbacc;
					    	
					    	String strQry = "Update CaneValueSplitupTemp set forsbacs="+finalryotsbacc+",forloans="+loanPartAmount+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
				 			Qryobj = strQry;
							UpdateList.add(Qryobj);
					    }
					}
					else
					{
						String strQry = "Update CaneValueSplitupTemp set forsbacs="+ryotsbacc+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
			 			Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
	 			}*/
	 			
	 			String strQry = "Update CaneValueSplitupTemp set forsbacs="+ryotsbacc+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
				ahFuctionalService.updateAmountForLoans(strQry);

	 			for(int j=0;j<gridData.size();j++)
				{
	 				DeductionsbaccforPCABean deductionsbaccforPCABean = new ObjectMapper().readValue(gridData.get(j).toString(), DeductionsbaccforPCABean.class);
					DedDetails_temp dedDetails_temp=prepareModelForDedDetails_temp(deductionsbaccforPCABean,season,ryotCode);
					logger.info("saveDeductionsAndSBDetails()------"+dedDetails_temp);
					ahFuctionalService.addDeductionsTemp(dedDetails_temp);
				}
			}
			updateFlag=true;//commonService.saveMultipleEntitiesForFunctionalScreens(deductionsAndSBTablesList,UpdateList);
			logger.info("saveDeductionsAndSBDetails()------After Save updateFlag"+updateFlag);
			return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	}
	
	private boolean updateAccounts(String ryotCode,Double totalPrice,String journalMemo,String season,String transType,String glCode,int transactionCode)
	{
		//int transactionCode = commonService.GetMaxTransCode(season);
		String AccountCode = ryotCode;
		int AccGrpCode = aHFuctionalService.GetAccGrpCode(ryotCode);
		
		Map DtlsMap = new HashMap();
		
		DtlsMap.put("TRANSACTION_CODE", transactionCode);
		DtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		DtlsMap.put("LOGIN_USER", userName);
		DtlsMap.put("ACCOUNT_CODE", AccountCode);		
		DtlsMap.put("JOURNAL_MEMO", journalMemo);
		DtlsMap.put("TRANSACTION_TYPE", transType);
		//Added by DMurty on 18-10-2016
		DtlsMap.put("SEASON",season);
		DtlsMap.put("GL_CODE",glCode);

		logger.info("updateAccounts()------DtlsMap"+DtlsMap);

		AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);//1
		commonService.addAccountDtls(accountDetails);
		
		String TransactionType = transType;//"Dr"; 
		AccountSummary accountSummary = prepareModelforAccSmry(totalPrice,AccountCode,TransactionType,season); //2
		
		double finalAmt = accountSummary.getRunningbalance();
		String strCrDr = accountSummary.getBalancetype();
		String AccCode = accountSummary.getAccountcode();
		List BalList = aHFuctionalService.BalDetails(AccCode,season);
		logger.info("updateAccounts()------AccountSummary BalList"+BalList);
		if (BalList != null && BalList.size() > 0)
		{
			commonService.updateRunningBalinSmry(finalAmt,strCrDr,AccCode,season);
		}
		else
		{
			commonService.addAccountSmry(accountSummary);
		}
		
		//Insert Rec in AccGrpDetails
		Map AccGrpDtlsMap = new HashMap();
		
		AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		HttpSession session1=request.getSession(false);
		Employees employe = (Employees) session1.getAttribute(Constants.USER);
		String userNames = employe.getLoginid();
		
		AccGrpDtlsMap.put("LOGIN_USER", userNames);
		AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccGrpDtlsMap.put("JOURNAL_MEMO", journalMemo);
		AccGrpDtlsMap.put("TRANSACTION_TYPE", transType);
		AccGrpDtlsMap.put("SEASON",season);
		logger.info("updateAccounts()------AccGrpDtlsMap"+AccGrpDtlsMap);

		AccountGroupDetails AccountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);//3
		commonService.addAccountGrpDtls(AccountGroupDetails);
		
		TransactionType = transType;//"Dr";
		AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//4
		
		List BalList1 = commonService.AccGrpBalDetails(AccGrpCode,season);
		logger.info("updateAccounts()------ AccountGroupSummary BalList1"+BalList1);
		if (BalList1 != null && BalList1.size() > 0)
		{
			double finalAmt1 = accountGroupSummary.getRunningbalance();
			String strCrDr1 = accountGroupSummary.getBalancetype();
			int AccGroupCode = accountGroupSummary.getAccountgroupcode();
			commonService.updateRunningBalinAccGrpSmry(finalAmt1,strCrDr1,AccGroupCode,season);
		}
		else
		{
			commonService.addAccountGrpSmry(accountGroupSummary);
		}
		
		//Insert Rec in AccSubGrpDtls
		Map AccSubGrpDtlsMap = new HashMap();		
		AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		HttpSession session2=request.getSession(false);
		Employees employ = (Employees) session2.getAttribute(Constants.USER);
		String userNam = employ.getLoginid();
		AccSubGrpDtlsMap.put("LOGIN_USER", userNam);
		
		AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccSubGrpDtlsMap.put("JOURNAL_MEMO",journalMemo);
		AccSubGrpDtlsMap.put("TRANSACTION_TYPE", transType);
		AccSubGrpDtlsMap.put("SEASON",season);
		logger.info("updateAccounts()------AccSubGrpDtlsMap"+AccSubGrpDtlsMap);

		AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);//5
		commonService.addAccountSubGrpDtls(accountSubGroupDetails);
		
		TransactionType = transType;//"Dr";
		AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//6
		
		double finalAmt2 = accountSubGroupSummary.getRunningbalance();
		String strCrDr2 = accountSubGroupSummary.getBalancetype();
		int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
		int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
		
		List BalList2 = commonService.SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
		logger.info("updateAccounts()------ AccountSubGroupSummary BalList2"+BalList2);
		if (BalList2 != null && BalList2.size() > 0)
		{
			commonService.updateRunningBalinAccSubGrpSmry(finalAmt2,strCrDr2,AccGrpCode,AccSubGrpCode,season);
		}
		else
		{
			commonService.addAccountSubGrpSmry(accountSubGroupSummary);
		}
		return true;
	}
	
	//-------------------Accounts
	private AccountDetails prepareModelForAccountDetails(Map DtlsMap) 
	{
		
		int TransactionCode = (Integer) DtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) DtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) DtlsMap.get("LOGIN_USER");
		String AccountCode = (String) DtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) DtlsMap.get("JOURNAL_MEMO");
		String TransType = (String) DtlsMap.get("TRANSACTION_TYPE");
		String GlCode = (String) DtlsMap.get("GL_CODE");
		if ("".equals(GlCode) || "null".equals(GlCode) || (GlCode == null))
		{
			GlCode = "NA";
		}
		String season = (String) DtlsMap.get("SEASON");
		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();

		//Added by DMurty on 25-12-2016
		String transactionDate = (String) DtlsMap.get("TRANSACTION_DATE");
		if(transactionDate == "" || transactionDate == null || transactionDate == "null")
		{
			date = new Date();
		}
		else
		{
			date=DateUtils.getSqlDateFromString(transactionDate,Constants.GenericDateFormat.DATE_FORMAT);
		}
		
	    AccountDetails accountDetails = new AccountDetails();				
		accountDetails.setTransactioncode(TransactionCode);
		accountDetails.setTransactiondate(date);
		accountDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountDetails.setAccountcode(AccountCode);
		accountDetails.setJournalmemo(journalMemo);
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountDetails.setDr(Amt);
			accountDetails.setCr(0.0);
		}
		else
		{
			accountDetails.setDr(0.0);
			accountDetails.setCr(Amt);
		}
		accountDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		accountDetails.setLoginuser(userName);
		
		accountDetails.setIsapplyforledger((byte) 0);
		accountDetails.setGlcode(GlCode);
		accountDetails.setStatus((byte) 1);
		accountDetails.setSeason(season);
		
		return accountDetails;
	}
	
	
	private AccountSummary prepareModelforAccSmry(Double Amt,String AccountCode,String TransactionType,String season) 
	{			
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		String AccCode = AccountCode;
		double TotalAmount = Amt;
		
		//Getting Data from Account Summary
		List BalList = commonService.AccSmryBalanceDetails(AccCode,TotalAmount,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		
		AccountSummary accountSummary = new AccountSummary();
		accountSummary.setAccountcode(AccountCode);
		accountSummary.setRunningbalance(RunnBal);
		accountSummary.setBalancetype(TransType);
		accountSummary.setSeason(season);
		return accountSummary;
	}
	
	private AccountGroupDetails prepareModelForAccGrpDetails(Map AccGrpDtlsMap) 
	{
		
		int TransactionCode = (Integer) AccGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccGrpDtlsMap.get("TRANSACTION_TYPE");
		//Added by DMurty on 18-10-2016
		String season = (String) AccGrpDtlsMap.get("SEASON");
		
		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();
		//Added by DMurty on 25-12-2016
		String transactionDate = (String) AccGrpDtlsMap.get("TRANSACTION_DATE");
		if(transactionDate == "" || transactionDate == null || transactionDate == "null")
		{
			date = new Date();
		}
		else
		{
			date=DateUtils.getSqlDateFromString(transactionDate,Constants.GenericDateFormat.DATE_FORMAT);
		}
		
		AccountGroupDetails accountGroupDetails = new AccountGroupDetails();
		
		accountGroupDetails.setTransactioncode(TransactionCode);
		accountGroupDetails.setTransactiondate(date);
		accountGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountGroupDetails.setAccountgroupcode(AccGrpCode);
		accountGroupDetails.setJournalmemo(journalMemo);
		
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountGroupDetails.setDr(Amt);
			accountGroupDetails.setCr(0.0);
		}
		else
		{
			accountGroupDetails.setDr(0.0);
			accountGroupDetails.setCr(Amt);
		}
		
		accountGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		accountGroupDetails.setLoginuser(userName);
		
		accountGroupDetails.setIsapplyforledger((byte) 0);
		accountGroupDetails.setStatus((byte) 1);
		accountGroupDetails.setSeason(season);
		return accountGroupDetails;
	}
	
	private AccountGroupSummary prepareModelForAccGrpSmry(String AccountCode,Double Amt,int AccGrpCode,String TransactionType,String season) 
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		
		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
		
		String AccCode = AccountCode;
		double TotalAmt = Amt;

		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0)
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}
		
		List BalList = commonService.AccGrpBalanceDetails(AccGrpCode,TotalAmt,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		accountGroupSummary.setAccountgroupcode(AccGrpCode);
		accountGroupSummary.setRunningbalance(RunnBal);
		accountGroupSummary.setBalancetype(TransType);
		accountGroupSummary.setSeason(season);
		
		return accountGroupSummary;
	}
	
	private AccountSubGroupDetails prepareModelForAccSubGrpDtls(Map AccSubGrpDtlsMap) 
	{
		
		int TransactionCode = (Integer) AccSubGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccSubGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccSubGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccSubGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccSubGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccSubGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccSubGrpDtlsMap.get("TRANSACTION_TYPE");
		String season = (String) AccSubGrpDtlsMap.get("SEASON");

		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();
		//Added by DMurty on 25-12-2016
		String transactionDate = (String) AccSubGrpDtlsMap.get("TRANSACTION_DATE");
		if(transactionDate == "" || transactionDate == null || transactionDate == "null")
		{
			date = new Date();
		}
		else
		{
			date=DateUtils.getSqlDateFromString(transactionDate,Constants.GenericDateFormat.DATE_FORMAT);
		}
		int AccSubGrpCode = aHFuctionalService.GetAccSubGrpCode(AccountCode);
		String IsSeedSupplier = (String) AccSubGrpDtlsMap.get("IsSeedSupplier");
		if("Yes".equalsIgnoreCase(IsSeedSupplier))
		{
			AccSubGrpCode = 2;
		}
		AccountSubGroupDetails accountSubGroupDetails = new AccountSubGroupDetails();
		
		accountSubGroupDetails.setTransactioncode(TransactionCode);
		accountSubGroupDetails.setTransactiondate(date);
		accountSubGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountSubGroupDetails.setAccountgroupcode(AccGrpCode);
		accountSubGroupDetails.setAccountsubgroupcode(AccSubGrpCode);
		accountSubGroupDetails.setJournalmemo(journalMemo);
		
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountSubGroupDetails.setDr(Amt);
			accountSubGroupDetails.setCr(0.0);
		}
		else
		{
			accountSubGroupDetails.setDr(0.0);
			accountSubGroupDetails.setCr(Amt);
		}
		
		accountSubGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		accountSubGroupDetails.setLoginuser(userName);
		
		accountSubGroupDetails.setIsapplyforledger((byte) 0);
		accountSubGroupDetails.setStatus((byte) 1);
		accountSubGroupDetails.setSeason(season);
		
		return accountSubGroupDetails;
	}
	
	private AccountSubGroupSummary prepareModelforAccSubGrpSmry(String AccountCode,Double Amt,int AccGrpCode,String TransactionType,String season) 
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		
		String AccCode = AccountCode;
		double TotalAmt = Amt;

		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0)
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}
		
		List BalList = commonService.SubGrpBalanceDetails(AccGrpCode,AccSubGroupCode,TotalAmt,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
			
		accountSubGroupSummary.setAccountgroupcode(AccGrpCode);
		accountSubGroupSummary.setAccountsubgroupcode(AccSubGroupCode);
		accountSubGroupSummary.setRunningbalance(RunnBal);
		accountSubGroupSummary.setBalancetype(TransType);
		accountSubGroupSummary.setSeason(season);
		
		return accountSubGroupSummary;
	}
	
	private DedDetails prepareModelForDedDetails(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode) 
	{		
		DedDetails dedDetails=new DedDetails();
		
		double netPayableAmt = deductionsbaccforPCABean.getNetPayable();
		double payingAmt = deductionsbaccforPCABean.getAmount();
		double pendingAmount = deductionsbaccforPCABean.getPendingAmount();
		int dedCode = deductionsbaccforPCABean.getStdDedCode();
		double oldPending = 0.0;
		List oldPendingList=caneAccountingFunctionalService.getOldPendingAmtList(ryotCode,season,dedCode);
		logger.info("prepareModelForDedDetails()------ oldPendingList"+oldPendingList);
		if(oldPendingList != null && oldPendingList.size()>0)
		{
			Map AmtMap = new HashMap();
			AmtMap = (Map) oldPendingList.get(0);
			oldPending = (Double) AmtMap.get("pendingamt");
		}
		double finalNetPayable = oldPending+netPayableAmt;

		dedDetails.setCaneacslno(deductionsbaccforPCABean.getCaneacslno());
		dedDetails.setDedCode(deductionsbaccforPCABean.getStdDedCode());
		dedDetails.setExtentsize(deductionsbaccforPCABean.getTotalextent());
		dedDetails.setHarvestcontcode(deductionsbaccforPCABean.getHarvestcontcode());
		dedDetails.setSuppliedcaneWt(deductionsbaccforPCABean.getCanesupplied());
		dedDetails.setRyotcode(ryotCode);
		dedDetails.setSeason(season);
		dedDetails.setOldpendingamount(oldPending);
		dedDetails.setNewpayable(netPayableAmt);
		dedDetails.setNetpayable(finalNetPayable);
		dedDetails.setDeductionamt(payingAmt);
		dedDetails.setPendingamt(pendingAmount);
		
		return dedDetails;
	}
	
	private DedDetails_temp prepareModelForDedDetails_temp(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode) 
	{		
		DedDetails_temp dedDetails_temp=new DedDetails_temp();		
		
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		double netPayableAmt = deductionsbaccforPCABean.getNetPayable();
		netPayableAmt = agricultureHarvestingController.round(netPayableAmt);

		double payingAmt = deductionsbaccforPCABean.getAmount();
		payingAmt = agricultureHarvestingController.round(payingAmt);
		
		double pendingAmount = deductionsbaccforPCABean.getPendingAmount();
		pendingAmount = agricultureHarvestingController.round(pendingAmount);

		int dedCode = deductionsbaccforPCABean.getStdDedCode();

		double oldPending = 0.0;
		List oldPendingList=caneAccountingFunctionalService.getOldPendingAmtList(ryotCode,season,dedCode);
		logger.info("prepareModelForDedDetails_temp()------ oldPendingList"+oldPendingList);
		if(oldPendingList != null && oldPendingList.size()>0)
		{
			Map AmtMap = new HashMap();
			AmtMap = (Map) oldPendingList.get(0);
			oldPending = (Double) AmtMap.get("pendingamt");
			oldPending = agricultureHarvestingController.round(oldPending);
		}
		double finalNetPayable = oldPending+netPayableAmt;

		dedDetails_temp.setCaneacslno(deductionsbaccforPCABean.getCaneacslno());
		dedDetails_temp.setDedCode(deductionsbaccforPCABean.getStdDedCode());
		dedDetails_temp.setExtentsize(deductionsbaccforPCABean.getTotalextent());
		dedDetails_temp.setHarvestcontcode(deductionsbaccforPCABean.getHarvestcontcode());
		dedDetails_temp.setSuppliedcaneWt(deductionsbaccforPCABean.getCanesupplied());
		dedDetails_temp.setRyotcode(ryotCode);
		dedDetails_temp.setSeason(season);
		
		dedDetails_temp.setOldpendingamount(oldPending);
		dedDetails_temp.setNewpayable(netPayableAmt);
		dedDetails_temp.setNetpayable(finalNetPayable);
		dedDetails_temp.setDeductionamt(payingAmt);
		dedDetails_temp.setPendingamt(pendingAmount);
		dedDetails_temp.setLogin(userName);
		
		return dedDetails_temp;
	}
	
	private OtherDeductions prepareModelForOtherDeductions(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode)  
	{		
		OtherDeductions otherDeductions=new OtherDeductions();		
		otherDeductions.setCaneacslno(deductionsbaccforPCABean.getCaneacslno());
		otherDeductions.setDedamount(deductionsbaccforPCABean.getAmount());
		otherDeductions.setRyotcode(ryotCode);
		otherDeductions.setSeason(season);

		return otherDeductions;
	}
	
	private OtherDedSmryByRyot prepareModelForOtherDedSmryByRyot(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode) 
	{
		OtherDedSmryByRyot otherDedSmryByRyot=null;
		List  otherDedSmryByRyotList=caneAccountingFunctionalService.getOtherDedSmryByRyot(season,ryotCode);
		if(otherDedSmryByRyotList!=null)
		{
			otherDedSmryByRyot=(OtherDedSmryByRyot)otherDedSmryByRyotList.get(0);
			otherDedSmryByRyot.setId(otherDedSmryByRyot.getId());
			otherDedSmryByRyot.setSuppliedcanewt(deductionsbaccforPCABean.getCanesupplied()+otherDedSmryByRyot.getSuppliedcanewt());
			otherDedSmryByRyot.setTotaldedamount(deductionsbaccforPCABean.getAmount()+otherDedSmryByRyot.getTotaldedamount());
		}
		else
		{
			otherDedSmryByRyot=new OtherDedSmryByRyot();
			otherDedSmryByRyot.setRyotcode(ryotCode);
			otherDedSmryByRyot.setSeason(season);
			otherDedSmryByRyot.setSuppliedcanewt(deductionsbaccforPCABean.getCanesupplied());
			otherDedSmryByRyot.setTotaldedamount(deductionsbaccforPCABean.getAmount());
		}
		return otherDedSmryByRyot;
	}

	private OtherDeductionsBySeason prepareModelForOtherDedBySeason(DeductionsbaccforPCABean deductionsbaccforPCABean,String season) 
	{
		OtherDeductionsBySeason otherDeductionsBySeason=new OtherDeductionsBySeason();
		otherDeductionsBySeason.setSeason(season);
		otherDeductionsBySeason.setDedamount(deductionsbaccforPCABean.getAmount());
		return otherDeductionsBySeason;
	}
	private SBAccSmryByRyot prepareModelForSBAccSmryByRyot(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode) 
	{
		SBAccSmryByRyot sBAccSmryByRyot=null;
		List  sBAccSmryByBankList=caneAccountingFunctionalService.getSBAccSmryByRyot(season,ryotCode);
		Integer ryotSBAccNum=0;
		RyotBankDetails ryotBankDetails = null;

		if(sBAccSmryByBankList!=null)
		{
			sBAccSmryByRyot=(SBAccSmryByRyot)sBAccSmryByBankList.get(0);
			sBAccSmryByRyot.setId(sBAccSmryByRyot.getId());
			sBAccSmryByRyot.setAmountpayable(deductionsbaccforPCABean.getNetPayable()+sBAccSmryByRyot.getAmountpayable());
			sBAccSmryByRyot.setPendingamount(deductionsbaccforPCABean.getPendingAmount()+sBAccSmryByRyot.getPendingamount());			
		}
		else
		{
			sBAccSmryByRyot=new SBAccSmryByRyot();
			List ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
			if(ryotBankList!=null)
			{
				ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
				ryotSBAccNum=ryotBankDetails.getRyotbankbranchcode();
			}
			sBAccSmryByRyot.setAmountpaid(0.00);
			sBAccSmryByRyot.setAmountpayable(deductionsbaccforPCABean.getNetPayable());
			sBAccSmryByRyot.setPendingamount(deductionsbaccforPCABean.getPendingAmount());
			sBAccSmryByRyot.setRyotcode(ryotCode);
			sBAccSmryByRyot.setSbcode(ryotSBAccNum);
			sBAccSmryByRyot.setSeason(season);
		}
		return sBAccSmryByRyot;
	}
	
	private SBAccSmryByBank prepareModelForSBAccSmryByBank(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode) 
	{
		SBAccSmryByBank sBAccSmryByBank=null;
		Integer bankCode=0;
		RyotBankDetails ryotBankDetails = null;
		List  ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
		if(ryotBankList!=null)
		{
			ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
			bankCode=ryotBankDetails.getRyotbankbranchcode();
		}
		
		List  sBAccSmryByBankList=caneAccountingFunctionalService.getSBAccSmryByBank(season,bankCode);
		if(sBAccSmryByBankList!=null)
		{
			sBAccSmryByBank=(SBAccSmryByBank)sBAccSmryByBankList.get(0);
			sBAccSmryByBank.setId(sBAccSmryByBank.getId());
			sBAccSmryByBank.setAmountpayable(deductionsbaccforPCABean.getNetPayable()+sBAccSmryByBank.getAmountpayable());
			sBAccSmryByBank.setPendingamount(deductionsbaccforPCABean.getPendingAmount()+sBAccSmryByBank.getAmountpaid());
		}
		else
		{
			sBAccSmryByBank=new SBAccSmryByBank();			
			sBAccSmryByBank.setAmountpaid(0.00);
			sBAccSmryByBank.setAmountpayable(deductionsbaccforPCABean.getNetPayable());
			sBAccSmryByBank.setBankcode(bankCode);
			sBAccSmryByBank.setPendingamount(deductionsbaccforPCABean.getPendingAmount());
			sBAccSmryByBank.setSeason(season);
		}
		return sBAccSmryByBank;
	}
	
	private SBAccTransDetails prepareModelForSBAccTransDetails(DeductionsbaccforPCABean deductionsbaccforPCABean,String season,String ryotCode,String ryotsbac) 
	{
		Integer bankCode=0;
		RyotBankDetails ryotBankDetails = null;
		List  ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
		if(ryotBankList!=null)
		{
			ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
			bankCode=ryotBankDetails.getRyotbankbranchcode();
		}
		SBAccTransDetails sBAccTransDetails=new SBAccTransDetails();
		//Modified by DMurty on 19-10-2016
		double dryotsbac = Double.parseDouble(ryotsbac);
		sBAccTransDetails.setAmount(dryotsbac);
		sBAccTransDetails.setBankcode(bankCode);
		sBAccTransDetails.setRyotcode(ryotCode);
		sBAccTransDetails.setSeason(season);
		sBAccTransDetails.setTransdate(new Date());
		sBAccTransDetails.setTranstype("Dr");
		return sBAccTransDetails;
	}
	
	//Fourth Tab Loading
	@RequestMapping(value = "/getAllAdvancesDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllAdvancesDetails(@RequestBody JSONObject jsonData)throws Exception 
	{
		try
		{
			logger.info("getAllAdvancesDetails()------jsonData"+jsonData);

			String season=(String)jsonData.getString("season");
			Integer caneaccslno=(Integer)jsonData.getInt("caneAccSlno");				
			String fromdate=(String)jsonData.getString("fromDate");
			String toDate=(String)jsonData.getString("toDate");	
			
			//Added by DMurty on 01-03-2017
			Integer advOrLoan=(Integer)jsonData.getInt("advOrLoan");				
			Integer supplyType=(Integer)jsonData.getInt("supplyType");				

			//Added by DMurty on 05-01-2017
			String repaymentDate=null;//(String)jsonData.getString("AllRepayDate");
			List CaneAccountSmryTempList = new ArrayList();
			CaneAccountSmryTempList=caneAccountingFunctionalService.CaneAccountSmryTemp(caneaccslno,season);
			if (CaneAccountSmryTempList != null && !CaneAccountSmryTempList.isEmpty())
			{
				Map CaneAccSmryMap = new HashMap();
				CaneAccSmryMap = (Map) CaneAccountSmryTempList.get(0);
				Date dateOfCalculation = (Date) CaneAccSmryMap.get("dateofcalc");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				repaymentDate = df.format(dateOfCalculation);
			}
			else
			{
				Date currDt = new Date();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				repaymentDate = df.format(currDt);
			}

			Date fdate=DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT);
			Date tDate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			HarvestingRateDetails harvestingRatesDetails =null;
			org.json.JSONObject jsonResponse = new org.json.JSONObject();
			org.json.JSONArray jsonArray = new org.json.JSONArray();
			org.json.JSONArray jsonArray1 = new org.json.JSONArray();
			net.sf.json.JSONObject jsonObject = null;
			List newList=new ArrayList();
			HashMap hashM=new HashMap();
			Map hm=null;
			
			List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitupTemp(season,caneaccslno); 
			logger.info("getAllAdvancesDetails()------caneValueSplitupSummaryList"+caneValueSplitupSummaryList);

			List advancesList=new ArrayList();
			Map valueMap=null;
			Object ob=null;
			
			for(int i=0;i<caneValueSplitupSummaryList.size();i++)
			{	
				logger.info("getAllAdvancesDetails()------i"+i);
				advancesList.clear();
				Map caneValueMap=(Map)caneValueSplitupSummaryList.get(i);			
				logger.info("getAllAdvancesDetails()------ ryotCode"+caneValueMap.get("ryotcode").toString());
				
				//Modified by DMurty on 01-03-2017
				if(supplyType == 0)
				{
					//modified by naidu on 07-03-2017
					advancesList=caneAccountingFunctionalService.getAllAdvances(fdate,tDate,caneValueMap.get("ryotcode").toString(),season);
				}
				else if(supplyType == 1)
				{
					if(advOrLoan == 0)
					{
						//modified by naidu on 07-03-2017
						advancesList=caneAccountingFunctionalService.getAllAdvances(fdate,tDate,caneValueMap.get("ryotcode").toString(),season);
						logger.info("getAllAdvancesDetails()------advancesList"+advancesList);
					}
				}
				else if(supplyType == 2)
				{
					advancesList=caneAccountingFunctionalService.getAllAdvances(fdate,tDate,caneValueMap.get("ryotcode").toString(),season);
				}

				List loanList=caneAccountingFunctionalService.getLoanDetails(season,caneValueMap.get("ryotcode").toString());
				logger.info("getAllAdvancesDetails()------loanList"+loanList);

				Date caneReceiptDate=null;
				String strRepayDt = repaymentDate;//"19-12-2016";
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				caneReceiptDate = df.parse(strRepayDt);
				String strcaneReceiptDate = df.format(caneReceiptDate);

				String strcaneReceiptDate1[] =strRepayDt.split("-");
				String year = strcaneReceiptDate1[2];
				int yyy = Integer.parseInt(year);
				int noOfDaysPerYear = 365;
				
				double months=0.00;
				if(advancesList != null && advancesList.size()>0)
				{
					for(int j=0;j<advancesList.size();j++)
					{				
						Map advListMap=(Map)advancesList.get(j);
						Double principle=0.00;
						
						Date principleDate=null;
						double nDays = 0.0;
						List principleList=caneAccountingFunctionalService.getAdvanceCalcDetails(season,caneValueMap.get("ryotcode").toString(),(Integer)advListMap.get("advancecode"));
						logger.info("getAllAdvancesDetails()------ principleList"+principleList);
						if(principleList!=null)
						{
							Map principleMap=(Map)principleList.get(0);
							
							if(principleMap.get("principle")==null)
								principle=0.00;
							else
								principle=(Double)principleMap.get("principle");
							
							principle = agricultureHarvestingController.round(principle);
							
							principleDate=(Date)principleMap.get("principledate");					
							long earliertime=principleDate.getTime();
							
							long latertime=caneReceiptDate.getTime();
							
							long days=DateUtils.dateDifference(latertime, earliertime);
							days = Math.abs(days);
							Long ldays = new Long(days);
							double dDays = ldays.doubleValue();
							nDays = dDays+1;
							months=dDays/30;
							months = Math.round(months * 100.0) / 100.0;
						}
						
						Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,(Integer)advListMap.get("advancecode"));
						logger.info("getAllAdvancesDetails()------Advance interest"+interest);

						if(interest==null)
							interest=0.00;
						
						double intrate = interest/100;
						
						double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
						double intrestAmt = intrestPerDay*nDays;
						intrestAmt = agricultureHarvestingController.round(intrestAmt);

						Double total=principle+intrestAmt;
						
						if(principleDate==null)
						{
							advListMap.put("advancedate","");
						}
						else
						{
							//df = new SimpleDateFormat("dd-MM-yyyy");
							String strprincipleDate = df.format(principleDate);
							advListMap.put("advancedate",strprincipleDate);
						}
						Date date = new Date();
						//String stradvDate = df.format(date);
						String stradvDate = repaymentDate;//"19-12-2016";
						advListMap.put("advancerepaydate",stradvDate);
						advListMap.put("advanceamount",principle);	
						advListMap.put("advanceinterest",interest);
						advListMap.put("totalamount",total);
						advListMap.put("advanceflag","0");
						advListMap.put("loanNo", 0);

						//Added by DMurty on 08-12-2016
						String strAdvCode = Integer.toString((Integer)advListMap.get("advancecode"));
						advListMap.put("referencenumber", (Integer)advListMap.get("advancecode"));
						advListMap.put("branchcode", 0);
						advListMap.put("intamount",intrestAmt);
						logger.info("getAllAdvancesDetails()------advListMap"+advListMap);

					}
				}
				else
				{
					advancesList=new ArrayList();
				}
				
				jsonObject = new net.sf.json.JSONObject();
				hm=new HashMap();
				hm=(Map)caneValueSplitupSummaryList.get(i);	
				double forsbac=(Double)hm.get("forsbacs");
				double forLoan=(Double)hm.get("ForLoans");
				forLoan = forLoan+forsbac;
				forLoan = agricultureHarvestingController.round(forLoan);

				hm.put("ForLoans", forLoan);
				jsonObject.putAll(hm);						

				if(loanList!=null)
				{
					for(int j=0;j<loanList.size();j++)
					{
						Map loanMap=(Map)loanList.get(j);
						//makella - 17-12-2016 - this is required when we calculate delayed interest,
						//so commenting now
						//caneReceiptDate=(Date)caneAccountingFunctionalService.getCaneReceiptDate(season,caneValueMap.get("ryotcode").toString());
						
						Double loanAmount = 0.00;						
						Date loanDate=null;
						int branchcode=(Integer)loanMap.get("branchcode");
						String loanAccNo = (String) loanMap.get("referencenumber");
						//Modified by DMurty on 07-01-2017
						int ryotLoanNo = (Integer) loanMap.get("loannumber");
						List principleList=caneAccountingFunctionalService.getLoanCalcDetails(season,caneValueMap.get("ryotcode").toString(),branchcode,ryotLoanNo);
						logger.info("getAllAdvancesDetails()------ principleList"+principleList);
						logger.info("LoanList------ ryotCode"+caneValueMap.get("ryotcode").toString());

						if(principleList!=null)
						{
							Map principleMap=(Map)principleList.get(0);
							
							if(principleMap.get("principle")==null)
								loanAmount=0.00;
							else
								loanAmount=(Double)principleMap.get("principle");
							
							loanAmount = agricultureHarvestingController.round(loanAmount);
							
							if(loanAmount == 0)
							{
								continue;
							}
							
							Date lDate=(Date)principleMap.get("principledate");
							DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
							String strlDate = df1.format(lDate);
							
							if(lDate != null)
							{
								loanDate=DateUtils.getSqlDateFromString(strlDate,Constants.GenericDateFormat.DATE_FORMAT);
							}
							else
							{
								loanDate = new Date();
							}		
							
							int loanNo = (Integer) loanMap.get("loannumber");
							double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(caneValueMap.get("ryotcode").toString(),season,branchcode,loanNo);
							
							//caneReceiptDate=(Date)caneAccountingFunctionalService.getCaneReceiptDate(season,caneValueMap.get("ryotcode").toString());
							long earliertime=loanDate.getTime();
							//makella - 17-12-2016 - this is commented out to calculate the interest till
							//March 31st

							long latertime=caneReceiptDate.getTime();
							
							long days=DateUtils.dateDifference(latertime, earliertime);
							days = Math.abs(days);
							Long ldays = new Long(days);
							double dDays = ldays.doubleValue();
							if(previousIntrest == 0)
							{
								dDays = dDays+1;
							}
							months=dDays/30;
							months = Math.round(months * 100.0) / 100.0;							
														
							// interestRate=(Double)loanMap.get("interstrate");
							Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(loanAmount,branchcode,season);
							logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

							if(interestRate==null)
								interestRate=0.00;
							
							if(loanAmount==null)
								loanAmount=0.00;
							
							//months=days/30;
							//Double advanceInterestTill=(principle*months*interest)*30/365*100;
							double intrate = interestRate/100;
							//Double loanInterestTill=(loanAmount*months*intrate)*30/365*100;
									
							double intrestPerDay = 	loanAmount*intrate/noOfDaysPerYear;
							double intrestAmt = intrestPerDay*dDays;
							intrestAmt = agricultureHarvestingController.round(intrestAmt);
		
							Double totalAmoutOnInterest=loanAmount+intrestAmt;
							
							Map newLoanMap=new HashMap();
							
							String loan = "Loan"+"("+loanAccNo+")";
							newLoanMap.put("advance",loan);
							
							//Modified by DMurty on 15-12-2016
							//newLoanMap.put("advancecode", loanMap.get("loannumber"));
							newLoanMap.put("advancecode", branchcode);
							newLoanMap.put("loanNo", loanMap.get("loannumber"));
									
							newLoanMap.put("advanceamount", loanAmount);
							if(loanDate != null)
							{
								String strdisburseddate = df.format(loanDate);
								newLoanMap.put("advancedate", strdisburseddate);
							}
							else
							{
								Date date = new Date();
								String strDisbursedDate = df.format(date);
								newLoanMap.put("advancedate",strDisbursedDate);
							}
							//Added by DMurty on 08-12-2016
							newLoanMap.put("referencenumber", loanAccNo);
							newLoanMap.put("branchcode", branchcode);
		
							newLoanMap.put("advanceinterest", interestRate);
							newLoanMap.put("totalamount",totalAmoutOnInterest);
							newLoanMap.put("advanceflag","1");
							//Modified by DMurty on 20-12-2016
							newLoanMap.put("intamount",intrestAmt);							
							
							Date date = new Date();
							
							//makella - 17-12-2016 - since the interest to be paid for till march 2017, 
							//we are keeping the following, otherwise we need to take system date only 
							//String strLoanDate = df.format(date);
							String strLoanDate = repaymentDate;//"19-12-2016";
							
							newLoanMap.put("advancerepaydate",strLoanDate);
							logger.info("getAllAdvancesDetails()------newLoanMap"+newLoanMap);
							
							//Added by DMurty on 01-03-2017
							if(supplyType == 0)
							{
								advancesList.add(newLoanMap);	
							}
							else if(supplyType == 1)
							{
								if(advOrLoan == 1)
								{
									advancesList.add(newLoanMap);	
								}
							}
							else if(supplyType == 2)
							{
								advancesList.add(newLoanMap);	
							}
						}
					}
				}
				jsonObject.put("caneacslno",advancesList);
				jsonArray.put(jsonObject);	
			}
			logger.info("---------Advances Details----------"+jsonArray.toString());
			return jsonArray.toString();
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return null;
        }
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountSummaryModel(Map accountMap)
	{
		AccountSummary accountSummary = new AccountSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		String accCode = (String) accountMap.get("ACCOUNT_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSummary.setRunningbalance(runbal);
		accountSummary.setBalancetype(strtype);
		accountSummary.setAccountcode(accCode);
		accountSummary.setSeason(ses);
		tempMap.put(key,accountSummary);
		
		return tempMap;
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountGroupSummaryModel(Map accountMap)
	{
		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountGroupSummary.setAccountgroupcode(accountGroupCode);
		accountGroupSummary.setRunningbalance(runbal);
		accountGroupSummary.setBalancetype(strtype);
		accountGroupSummary.setSeason(ses);
		tempMap.put(key,accountGroupSummary);
		
		return tempMap;
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountSubGroupSummaryModel(Map accountMap)
	{
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		int accountsubgroupcode = (Integer) accountMap.get("ACCOUNT_SUB_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSubGroupSummary.setAccountsubgroupcode(accountsubgroupcode);
		accountSubGroupSummary.setAccountgroupcode(accountGroupCode);
		accountSubGroupSummary.setRunningbalance(runbal);
		accountSubGroupSummary.setBalancetype(strtype);
		accountSubGroupSummary.setSeason(ses);
		tempMap.put(key,accountSubGroupSummary);
		
		return tempMap;
	}
		
	//Fourth Tab Form Submit		
	@RequestMapping(value = "/saveAdvancesAndLoansDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean saveAdvancesAndLoansDetails(@RequestBody JSONArray jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		logger.info("saveAdvancesAndLoansDetails()------jsonData"+jsonData);
		List advanceLoansTablesList=new ArrayList();
		Object Qryobj = null;
		List UpdateList = new ArrayList();
		List AccountsList = new ArrayList();

		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		List ryotList = new ArrayList();
		String strQry = null;
		
		double finalCdcAmount = 0.0;
		double finalUlAmount = 0.0;
		double finalTcAmount = 0.0;
		double finalHarAmount = 0.0;
		double otherDed = 0.0;
		double finalDedPartToSbAcPayable = 0.0;
		double forAdvAndLoans = 0.0;
		double finalforSbAcs = 0.0;
		double harvestChargePayable = 0.0;
		double totalAdvPayable = 0.0;
		double totalLoanPayable = 0.0;
		double finalTotalVal = 0.0;
		
		double ryotCdcAmount = 0.0;
		double ryotUlAmount = 0.0;
		double ryotTcAmount = 0.0;
		double ryotHarAmount = 0.0;
		double ryotOtherDed = 0.0;
		
		double ryotDedPartToSbAcPayable = 0.0;
		double ryotforAdvAndLoans = 0.0;
		double ryotforSbAcs = 0.0;
		double ryotharvestChargePayable = 0.0;
		double ryottotalAdvPayable = 0.0;
		double ryottotalLoanPayable = 0.0;
		double ryotfinalTotalVal = 0.0;
		double ryotforDeductions = 0.0;
		double ryotLoansPartToSbAccPaid = 0.0;
		int transactionCode = 0;
		boolean updateFlag= false;
		int ncaneAccSlno = 0;
		String strSeason = null;
		try
		{
			for(int i=0;i<jsonData.size();i++)
			{
				double ryotsbacc =0.0;
				ryotCdcAmount = 0.0;
				ryotUlAmount = 0.0;
				ryotTcAmount = 0.0;
				ryotHarAmount = 0.0;
				ryotOtherDed = 0.0;
				ryotDedPartToSbAcPayable = 0.0;
				ryotforAdvAndLoans = 0.0;
				ryotforSbAcs = 0.0;
				ryotharvestChargePayable = 0.0;
				ryottotalAdvPayable = 0.0;
				ryottotalLoanPayable = 0.0;
				ryotfinalTotalVal = 0.0;
				ryotforDeductions = 0.0;
				ryotLoansPartToSbAccPaid = 0.0;
				
				JSONObject jsonObject=(JSONObject)jsonData.get(i);
				
				String ryotName=(String) jsonObject.get("RyotName");	
				Integer caneAccSlno=(Integer) jsonObject.get("caneAccSlno");
				String ryotCode=(String) jsonObject.get("ryotcode");
				
				String ryotsbac=(String) jsonObject.get("ryotLoansbac");	

				String season=(String) jsonObject.get("season");

				Integer advOrLoan=(Integer) jsonObject.get("advOrLoan");
				Integer supplyType=(Integer) jsonObject.get("supplyType");
				
				Date CaneaccDate = new Date();
				List CanAccountSmryTempList = new ArrayList();
				CanAccountSmryTempList=caneAccountingFunctionalService.CaneAccountSmryTemp(caneAccSlno,season);
				if (CanAccountSmryTempList != null && !CanAccountSmryTempList.isEmpty())
				{
					Map CaneAccSmryMap = new HashMap();
					CaneAccSmryMap = (Map) CanAccountSmryTempList.get(0);
					CaneaccDate = (Date) CaneAccSmryMap.get("dateofcalc");
				}
				else
				{
					CaneaccDate = new Date();
				}
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strCaneaccDate = df.format(CaneaccDate);
				
				

				//This is for with supply. i.e Regular Cane Accounting
				if(supplyType == 0 || supplyType == 2)
				{
					double totalSuppliedWeight = 0.0;
					List CaneValueSplitupList = caneAccountingFunctionalService.getCaneValueSplitupByRyot(season,caneAccSlno,ryotCode);
					if(CaneValueSplitupList != null && CaneValueSplitupList.size()>0)
					{
						Map tempMap = new HashMap();
						tempMap = (Map) CaneValueSplitupList.get(0);
						totalSuppliedWeight = (Double) tempMap.get("canesupplied");
					}
					
					strSeason = season;
				
					//For entire Cane Accounting, this is unique for all ryots.
					if(i==0)
		 			{
						transactionCode = commonService.GetMaxTransCode(season);
						logger.info("saveAdvancesAndLoansDetails()------transactionCode"+transactionCode);
		 			}
					
					logger.info("======== For Ryot No=========="+i);
					logger.info("======== For Ryot Code=========="+ryotCode);

		 			JSONArray gridData=(JSONArray) jsonObject.get("caneacslno");
					logger.info("======== saveAdvancesAndLoansDetails()==========gridData "+gridData);

		 			if(gridData != null && gridData.isEmpty() == false)
		 			{
		 				ryotsbacc = Double.parseDouble(ryotsbac);
		 				//double ryotsbacc = (double) ryotsbac;
			 			ryotLoansPartToSbAccPaid = ryotsbacc;
			 			//Here getting Sum of DedPartToSbAcPayable from CaneValueSplitupTemp. For first rec it is enough to get. No need to call this in for loop
			 			if(i==0)
			 			{
			 				ncaneAccSlno = caneAccSlno;
						    List dedPartList = ahFuctionalService.getDedPartForSbAccList(ncaneAccSlno,season);
						    if(dedPartList != null && dedPartList.size()>0)
						    {
						    	Map tempMap = new HashMap();
						    	tempMap = (Map) dedPartList.get(0);
						    	finalDedPartToSbAcPayable = (Double) tempMap.get("ded");
						    	forAdvAndLoans = (Double) tempMap.get("advloans");
						    	finalTotalVal = (Double) tempMap.get("totalval");
						    }
			 			}
			 			
			 			//Getting amounts for individual ryots
					    List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(ncaneAccSlno,season,ryotCode);
						logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
					    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
					    {
					    	Map tempMap = new HashMap();
					    	tempMap = (Map) dedPartForRyotList.get(0);
					    	ryotforAdvAndLoans = (Double) tempMap.get("forloans");
					    	ryotfinalTotalVal = (Double) tempMap.get("totalvalue");
					    	ryotforDeductions = ryotfinalTotalVal-ryotforAdvAndLoans;//(Double) tempMap.get("forsbacs");
					    }
			 			
			 			//get dedsbacc Amt from canevaluesplituptemp
						double dedSbAmt = caneAccountingFunctionalService.getdedSbAmt(caneAccSlno,season,ryotCode);
						ryotDedPartToSbAcPayable = dedSbAmt;
						
						double finalSbAcAmt = ryotsbacc;
						ryotLoansPartToSbAccPaid = ryotsbacc;
			 			finalforSbAcs += finalSbAcAmt;
			 			
			 			ryotforSbAcs += finalSbAcAmt;
			 			
			 			strQry = "Update CaneValueSplitupTemp set forsbacs="+ryotforDeductions+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
			 			Qryobj = strQry;
						UpdateList.add(Qryobj);
						logger.info("saveAdvancesAndLoansDetails()------strQry"+strQry);
						
						//Deductions moved to here because in CanAccSBDetails we are posting deduction amounts. To update variables moved to top of the advances and loans.
						if(supplyType == 0)
						{
							List FinalList = new ArrayList();
							List DedDetails_temps=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
							logger.info("saveAdvancesAndLoansDetails()------DedDetails_temps"+DedDetails_temps);
							if (DedDetails_temps != null && !DedDetails_temps.isEmpty())
							{
								for(int k=0;k<DedDetails_temps.size();k++)
								{
									Map DedMap = new HashMap();
									DedMap = (Map) DedDetails_temps.get(k);
									
									int id = (Integer) DedMap.get("id");
									int dedCode = (Integer) DedMap.get("DedCode");
									String deduction = (String) DedMap.get("Deduction");
									int caneAcSlno = (Integer) DedMap.get("caneacslno");
									double deductionAmt = (Double) DedMap.get("deductionamt");
									double extentSize = (Double) DedMap.get("extentsize");
									int harvestContCode = (Integer) DedMap.get("harvestcontcode");
									double netPayable = (Double) DedMap.get("netpayable");
									double newPayable = (Double) DedMap.get("newpayable");
									double oldPendingAmount = (Double) DedMap.get("oldpendingamount");
									double pendingAmount = (Double) DedMap.get("pendingamt");
									String ryotcode = (String) DedMap.get("ryotcode");
									String dedSeason = (String) DedMap.get("season");
									double suppliedCaneWt = (Double) DedMap.get("suppliedcaneWt");
									String login = (String) DedMap.get("login");
									if(dedCode == 1)
									{
										finalCdcAmount += deductionAmt;
										ryotCdcAmount += deductionAmt;
									}
									else if(dedCode == 2)
									{
										finalUlAmount += deductionAmt;
										ryotUlAmount += deductionAmt;
									}
									else if(dedCode == 3)
									{
										finalTcAmount += deductionAmt;
										ryotTcAmount += deductionAmt;
									}
									else if(dedCode == 4)
									{
										finalHarAmount += deductionAmt;
										ryotHarAmount += deductionAmt;
										ryotharvestChargePayable += deductionAmt;
										harvestChargePayable += deductionAmt;
									}
									else if(dedCode == 5)
									{
										otherDed += deductionAmt;
										ryotOtherDed += deductionAmt;
									}
									
									//Here i need to update amount fields in harvesting and transport charges tables.
									//3 tp charges (deductionAmt)
									//4 harvestingcharges (deductionAmt)
									if(dedCode == 3)
									{
										List TpList = caneAccountingFunctionalService.getTpDedDtls(ryotCode,season,harvestContCode);
										if (TpList != null && !TpList.isEmpty())
										{
											Map TempMap = new HashMap();
											TempMap = (Map) TpList.get(0);
											double pendingAmt = (Double) TempMap.get("pendingamount");
											double totalAmt = (Double) TempMap.get("totalamount");
											double totalPayable = (Double) TempMap.get("totalpayableamount");
	
											double finalTotalPayable = totalPayable+deductionAmt;
											double finalpendingAmt = totalAmt-finalTotalPayable;
											
											strQry = "Update TranspChgSmryByRyotAndCont set totalpayableamount="+finalTotalPayable+",pendingamount="+finalpendingAmt+" where ryotcode ='"+ryotCode+"' and season='"+season+"' and transportcontcode="+harvestContCode+"";
											Qryobj = strQry;
											UpdateList.add(Qryobj);
										}
									}
									else if(dedCode == 4)
									{
										List HarvestingList = caneAccountingFunctionalService.getHarvestinChargesDtls(ryotCode,season,harvestContCode);
										if (HarvestingList != null && !HarvestingList.isEmpty())
										{
											Map TempMap = new HashMap();
											TempMap = (Map) HarvestingList.get(0);
											
											double pendingAmt = (Double) TempMap.get("PendingAmount");
											double totalAmt = (Double) TempMap.get("totalamount");
											double totalPayable = (Double) TempMap.get("totalpayableamount");
	
											double finalTotalPayable = totalPayable+deductionAmt;
											double finalpendingAmt = totalAmt-finalTotalPayable;
											
											strQry = "Update HarvChgSmryByRyotAndCont set totalpayableamount="+finalTotalPayable+",PendingAmount="+finalpendingAmt+" where ryotcode ='"+ryotCode+"' and season='"+season+"' and harvestcontcode="+harvestContCode+"";
											Qryobj = strQry;
											UpdateList.add(Qryobj);
										}
									}
									
									DedDetails dedDetails = new DedDetails();
	
									dedDetails.setCaneacslno(caneAcSlno);
									dedDetails.setDedCode(dedCode);
									dedDetails.setDeduction(deduction);
									dedDetails.setDeductionamt(deductionAmt);
									dedDetails.setExtentsize(extentSize);
									dedDetails.setHarvestcontcode(harvestContCode);
									//dedDetails.setId(id);
									dedDetails.setNetpayable(netPayable);
									dedDetails.setNewpayable(newPayable);
									dedDetails.setOldpendingamount(oldPendingAmount);
									dedDetails.setPendingamt(pendingAmount);
									dedDetails.setRyotcode(ryotcode);
									dedDetails.setSeason(dedSeason);
									dedDetails.setSuppliedcaneWt(suppliedCaneWt);
									dedDetails.setLogin(login);
									advanceLoansTablesList.add(dedDetails);
	
									double totalPrice = dedDetails.getDeductionamt();
									String glCode="";
									if(dedCode==1 || dedCode==2 || dedCode==3  || dedCode==4)
									{
										String journalMemo=null;
										if(dedCode==1)
										{
											journalMemo= "Towards CDC Fund";
											glCode = "1186";
										}
										else if(dedCode==2)
										{
											journalMemo="Towards Unloading Charges";
											glCode = "UL";
										}
										else if(dedCode==3)
										{
											journalMemo="Towards Transport Charges";
											int transportCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(transportCont,"TransportingContractors","transportercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												glCode = (String) tempMap.get("accountcode");
											}								
										}
										else if(dedCode==4)
										{
											journalMemo="Towards Harvesting Charges";
											int harvestCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(harvestCont,"HarvestingContractors","harvestercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												glCode = (String) tempMap.get("accountcode");
											}
										}
										String transactionType = "Dr";
	
										String TransactionType = transactionType;
										String strglCode = glCode;
										String accCode = ryotCode;
										double Amt = totalPrice;
										if(accCode != null)
										{
											int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
											String strAccGrpCode = Integer.toString(AccGrpCode);
	
											int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
											String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
											Map tempMap1 = new HashMap();
											tempMap1.put("ACCOUNT_CODE", accCode);
											tempMap1.put("AMOUNT", Amt);
											tempMap1.put("JRNL_MEM", journalMemo);
											tempMap1.put("SEASON", season);
											tempMap1.put("TRANS_TYPE", TransactionType);
											tempMap1.put("GL_CODE", strglCode);
											tempMap1.put("TRANSACTION_CODE", transactionCode); 
											
											//Modified by Dmurty on 03-03-2017
											tempMap1.put("TRANSACTION_DATE", strCaneaccDate); 
											// here o
											Map AccMap = updateAccountsNew(tempMap1);
											logger.info("saveAdvancesAndLoansDetails()------AccMap"+AccMap);
	
											advanceLoansTablesList.add(AccMap.get("accDtls"));
											advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
											advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
											
											Map AccSmry = new HashMap();
											AccSmry.put("ACC_CODE", accCode);
											boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
											if (ryotAccCodetrue == false)
											{
												AccountCodeList.add(AccSmry);
												AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
											}
											else
											{
												AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
												String straccCode = accountSummary.getAccountcode();
												if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
												{
													double runbal = accountSummary.getRunningbalance();
													String baltype = accountSummary.getBalancetype();
													String ses = accountSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_CODE", accCode);
													tmpMap.put("KEY", accCode);
													Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
													AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
												}
											}
											
											Map AccGrpSmry = new HashMap();
											AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
											if (loanAccGrpCodetrue == false)
											{
												AccountGrpCodeList.add(AccGrpSmry);
												AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
											}
											else
											{
												AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
												int accountGrpCode = accountGroupSummary.getAccountgroupcode();
												if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
												{
													double runbal = accountGroupSummary.getRunningbalance();
													String baltype = accountGroupSummary.getBalancetype();
													String ses = accountGroupSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("KEY", strAccGrpCode);
													
													Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
													AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
												}
											}
											
											Map AccSubGrpSmry = new HashMap();
											//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
											boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
											if(ryotLoanAccSubGrpCodetrue == false)
											{
												AccountSubGrpCodeList.add(AccSubGrpSmry);
												AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
											}
											else
											{
												AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
												int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
												int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
												String ses = accountSubGroupSummary.getSeason();
												if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
												{
													double runbal = accountSubGroupSummary.getRunningbalance();
													String baltype = accountSubGroupSummary.getBalancetype();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
													tmpMap.put("KEY",strAccSubGrpCode);
													Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
													AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
												}
											}
										}
										logger.info("Posted Deductions for Ryot in Accounts------");
	
										//Here we need to post dr entry for individual deductions.
										glCode=ryotCode;
										strglCode = glCode;
										String accountCode = null;
										if(dedCode==1)
										{
											journalMemo= "Towards CDC Fund";
											accountCode = "1186";
										}
										else if(dedCode==2)
										{
											journalMemo="Towards Unloading Charges";
											accountCode = "UL";
										}
										else if(dedCode==3)
										{
											journalMemo="Towards Transport Charges";
											int transportCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(transportCont,"TransportingContractors","transportercode");
											logger.info("getAccountCode()------detailsList"+detailsList);
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												accountCode = (String) tempMap.get("accountcode");
											}	
										}
										else if(dedCode==4)
										{
											journalMemo="Towards Harvesting Charges";
											int harvestCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(harvestCont,"HarvestingContractors","harvestercode");
											logger.info("getAccountCode()------detailsList"+detailsList);
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												accountCode = (String) tempMap.get("accountcode");
											}
										}
										transactionType = "Dr";
										if(accountCode != null)
										{
											accCode = accountCode;
											TransactionType = transactionType;
											int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
											String strAccGrpCode = Integer.toString(AccGrpCode);
	
											int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
											String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
											Map tempMap = new HashMap();
											tempMap.put("ACCOUNT_CODE", accCode);
											tempMap.put("AMOUNT", Amt);
											tempMap.put("JRNL_MEM", journalMemo);
											tempMap.put("SEASON", season);
											tempMap.put("TRANS_TYPE", TransactionType);
											tempMap.put("GL_CODE", strglCode);
											tempMap.put("TRANSACTION_CODE", transactionCode);
											
											//Modified by DMurty on 03-03-2017
											tempMap.put("TRANSACTION_DATE", strCaneaccDate);
											Map AccMap = updateAccountsNew(tempMap);
											
											advanceLoansTablesList.add(AccMap.get("accDtls"));
											advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
											advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
											
											Map AccSmry = new HashMap();
											AccSmry.put("ACC_CODE", accCode);
											boolean istrueforded = AccountCodeList.contains(AccSmry);
											if (istrueforded == false)
											{
												AccountCodeList.add(AccSmry);
												AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
											}
											else
											{
												AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
												String straccCode = accountSummary.getAccountcode();
												if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
												{
													double runbal = accountSummary.getRunningbalance();
													String baltype = accountSummary.getBalancetype();
													String ses = accountSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_CODE", accCode);
													tmpMap.put("KEY",accCode);
													Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
													AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
												}
											}
											//dedAccGrpCodeList
											Map AccGrpSmry = new HashMap();
											AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											boolean dedAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
											if (dedAccGrpCodetrue == false)
											{
												AccountGrpCodeList.add(AccGrpSmry);
												AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
											}
											else
											{
												AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
												int accountGrpCode = accountGroupSummary.getAccountgroupcode();
												if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
												{
													double runbal = accountGroupSummary.getRunningbalance();
													String baltype = accountGroupSummary.getBalancetype();
													String ses = accountGroupSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("KEY", strAccGrpCode);
													
													Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
													AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
												}
											}
											
											Map AccSubGrpSmry = new HashMap();
											//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
											boolean dedAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
											if(dedAccSubGrpCodetrue == false)
											{
												AccountSubGrpCodeList.add(AccSubGrpSmry);
												AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
											}
											else
											{
												AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
												int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
												int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
												String ses = accountSubGroupSummary.getSeason();
												if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
												{
													double runbal = accountSubGroupSummary.getRunningbalance();
													String baltype = accountSubGroupSummary.getBalancetype();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
													tmpMap.put("KEY",strAccSubGrpCode);
													Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
													AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
												}
											}
										}
									}
								}
								logger.info("Posted Deductions for individual deduction in Accounts------");
	
								strQry = "Delete from DedDetails_temp where season='"+season+"' and caneacslno = "+caneAccSlno+" and ryotcode='"+ryotCode+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
			 			}
		 			
			 			for(int j=0;j<gridData.size();j++)
						{
							AdvancesLoansForPCABean advancesLoansForPCABean = new ObjectMapper().readValue(gridData.get(j).toString(), AdvancesLoansForPCABean.class);
							if(advancesLoansForPCABean.getAdvanceflag().equals("0"))
							{
								AdvanceSummary advanceSummary = prepareModelForUpdateAdvanceSummary(advancesLoansForPCABean,season,ryotCode);
								if(advanceSummary!=null)
								{
									double advanceAmt = advanceSummary.getAdvanceamount();
									double pendingAmt = advanceSummary.getPendingamount();
									
									//Modified by DMurty on 02-03-2017
									String qry = "Update AdvancePrincipleAmounts set principle="+pendingAmt+",principledate='"+CaneaccDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and advancecode="+advancesLoansForPCABean.getAdvancecode();
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
									
									double paidAmt = advanceSummary.getRecoveredamount();
									double intAmt = 0.0;
									if(advanceSummary.getInterestamount() != null)
									{
										intAmt = advanceSummary.getInterestamount();
									}
									double totalAmt = paidAmt+intAmt;
									advanceLoansTablesList.add(advanceSummary);	
								}
								CanAccLoansDetails canAccLoansDetails=new CanAccLoansDetails();
								
								canAccLoansDetails.setAccountnum(advancesLoansForPCABean.getReferencenumber());
								double inrst = 0.0;
								if(advancesLoansForPCABean.getAdvanceinterest() != null)
								{
									inrst = advancesLoansForPCABean.getAdvanceinterest();
								}
								
								double adjPrinciple = advancesLoansForPCABean.getTotalamount()-advancesLoansForPCABean.getLoanPayable();
								canAccLoansDetails.setAdjustedprincipleamount(adjPrinciple);
								
								canAccLoansDetails.setAdvanceamount(advancesLoansForPCABean.getAdvanceamount());
								
								//Modified by DMurty on 22-12-2016
								Date currDate = DateUtils.getSqlDateFromString(advancesLoansForPCABean.getAdvancedate(),Constants.GenericDateFormat.DATE_FORMAT);
								
								canAccLoansDetails.setAdvdate(currDate);
								canAccLoansDetails.setAdvloan(0);
								canAccLoansDetails.setBranchcode(Integer.parseInt(advancesLoansForPCABean.getReferencenumber()));
								canAccLoansDetails.setCaneacctslno(caneAccSlno);
								canAccLoansDetails.setDelayedintrest(0.0);
								canAccLoansDetails.setInterestamount(advancesLoansForPCABean.getAdvanceinterest());
								double intRate = 0.0;
								if(advancesLoansForPCABean.getNetRate() != null)
								{
									intRate = advancesLoansForPCABean.getNetRate();
								}
								intRate =Double.parseDouble(new DecimalFormat("##.##").format(intRate));
								
								canAccLoansDetails.setInterestrate(intRate);
								canAccLoansDetails.setLoanno(advancesLoansForPCABean.getLoanNo());
								canAccLoansDetails.setNetrate(advancesLoansForPCABean.getAdvanceinterest());
								canAccLoansDetails.setPaidamt(advancesLoansForPCABean.getLoanPayable());
								double pendingAmt = advancesLoansForPCABean.getAdvanceamount()-advancesLoansForPCABean.getLoanPayable();
								pendingAmt =Double.parseDouble(new DecimalFormat("##.##").format(pendingAmt));
								canAccLoansDetails.setPendingamt(pendingAmt);
							
								String repayDate = advancesLoansForPCABean.getAdvancerepaydate();
								Date strRepayDate = DateUtils.getSqlDateFromString(repayDate,Constants.GenericDateFormat.DATE_FORMAT);
								canAccLoansDetails.setRepaymentdate(strRepayDate);
								
								canAccLoansDetails.setRyotcode(ryotCode);
								canAccLoansDetails.setSeason(season);
								if(advancesLoansForPCABean.getSubventedInterestRate() != null)
								{
									double subvIntrestRate = Double.parseDouble(advancesLoansForPCABean.getSubventedInterestRate());
									canAccLoansDetails.setSubventedrate(subvIntrestRate);
								}
								else
								{
									canAccLoansDetails.setSubventedrate(0.0);
								}
								canAccLoansDetails.setTotalamount(advancesLoansForPCABean.getTotalamount()+inrst);
								
								canAccLoansDetails.setPaymentsource(null);
								canAccLoansDetails.setIsdirect((byte) 0);
								canAccLoansDetails.setRyotdetails("NA");
								canAccLoansDetails.setMainryotcode("NA");
								
								//Modified by DMurty on 16-12-2016
								canAccLoansDetails.setAdvisedamt(advancesLoansForPCABean.getLoanPayable());
								
								canAccLoansDetails.setAdditionalint(0.0);
								
								double finalPrinciple = adjPrinciple+canAccLoansDetails.getAdditionalint();
								canAccLoansDetails.setFinalprinciple(finalPrinciple);
								
								canAccLoansDetails.setIntryotpayable(0.0);
								canAccLoansDetails.setFinalinterest(0.0);
								canAccLoansDetails.setCaneacctslno(caneAccSlno);
								
								advanceLoansTablesList.add(canAccLoansDetails);	
								

								totalAdvPayable += advancesLoansForPCABean.getLoanPayable();
								ryottotalAdvPayable += advancesLoansForPCABean.getLoanPayable();
								
								Map rMap = new HashMap();
								rMap.put("RYOT_CODE", ryotCode);
								boolean istrue = ryotList.contains(rMap);
								if(istrue == false)
								{
									ryotList.add(rMap);
								}
								
								//Added by DMurty on 31-10-2016
								int advCode = advancesLoansForPCABean.getAdvancecode();
								List<CompanyAdvance> companyAdvance = aHFuctionalService.getAdvanceDateilsByAdvanceCode(advCode);
								String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
								
								String TransactionType = "Cr";
								String strglCode = ryotCode;
								String accCode = GlCodeforAdvance;
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);
								
								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								double amt = advancesLoansForPCABean.getLoanPayable();
								double Amt = amt;
								String strtype = "";
								if(accCode != null)
								{
									Map tempMap = new HashMap();
									tempMap.put("ACCOUNT_CODE", accCode);
									tempMap.put("AMOUNT", amt);
									tempMap.put("JRNL_MEM", "Towards Advances Recovery");
									tempMap.put("SEASON", season);
									tempMap.put("TRANS_TYPE", TransactionType);
									tempMap.put("GL_CODE", strglCode);
									tempMap.put("TRANSACTION_CODE", transactionCode);

									//Modified by DMurty on 03-03-2017
									tempMap.put("TRANSACTION_DATE", strCaneaccDate);

									Map AccMap = updateAccountsNew(tempMap);
									logger.info("saveAdvancesAndLoansDetails()------AccMap"+AccMap);

									advanceLoansTablesList.add(AccMap.get("accDtls"));
									advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
									advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
									
									Map AccSmry = new HashMap();
									AccSmry.put("ACC_CODE", accCode);
									boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
									logger.info("saveAdvancesAndLoansDetails()------AccountCodeList"+AccountCodeList);
									if (advAccCodetrue == false)
									{
										AccountCodeList.add(AccSmry);
										AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
									}
									else
									{
										AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
										String straccCode = accountSummary.getAccountcode();
										if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
										{
											double runbal = accountSummary.getRunningbalance();
											String baltype = accountSummary.getBalancetype();
											String ses = accountSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_CODE", accCode);
											tmpMap.put("KEY", accCode);
											Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
											AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
										}
									}
									
									//advClrAccGrpCodeList
									Map AccGrpSmry = new HashMap();
									AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
									boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
									if (advAccGrpCodetrue == false)
									{
										AccountGrpCodeList.add(AccGrpSmry);
										AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
									}
									else
									{
										AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
										int accountGrpCode = accountGroupSummary.getAccountgroupcode();
										if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
										{
											double runbal = accountGroupSummary.getRunningbalance();
											String baltype = accountGroupSummary.getBalancetype();
											String ses = accountGroupSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("KEY", strAccGrpCode);
											
											Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
											AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode)); 
										}
									}
									
									//advClrAccSubCodeGrpList
									Map AccSubGrpSmry = new HashMap();
									//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
									AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
									
									boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
									if(advAccSubGrpCodetrue == false)
									{
										AccountSubGrpCodeList.add(AccSubGrpSmry);
										AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
									}
									else
									{
										AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
										int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
										int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
										String ses = accountSubGroupSummary.getSeason();
										if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
										{
											double runbal = accountSubGroupSummary.getRunningbalance();
											String baltype = accountSubGroupSummary.getBalancetype();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
											tmpMap.put("KEY", strAccSubGrpCode); 
											Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
											AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
										}
									}
								}
							}
							else 
							{ 
								//Modified by DMurty on 14-03-2017
								CanAccLoansDetails canAccLoansDetails=prepareModelForCanAccLoansDetails(advancesLoansForPCABean,season,ryotCode,caneAccSlno,supplyType,CaneaccDate);
								totalLoanPayable += advancesLoansForPCABean.getLoanPayable();
								ryottotalLoanPayable += advancesLoansForPCABean.getLoanPayable();
								
								advanceLoansTablesList.add(canAccLoansDetails);
								
								//Update Loan Tables here
								int loanno = advancesLoansForPCABean.getLoanNo();
								int bankBranch = advancesLoansForPCABean.getBranchcode();
								double intAmt = advancesLoansForPCABean.getInterestAmount();
								double paidAmt = advancesLoansForPCABean.getLoanPayable();
								double loanAmt = advancesLoansForPCABean.getAdvanceamount();
								double total = loanAmt+intAmt;
								double loanPayable =  paidAmt-intAmt;
								double finalPrinciple = loanAmt-loanPayable;
								List loanSummaryList=caneAccountingFunctionalService.getLoanSummaryByBankAndLoanNo(season,ryotCode,loanno,bankBranch);
								if(loanSummaryList!=null)
								{		
									LoanSummary loanSummary=(LoanSummary)loanSummaryList.get(0);
									double intrestAmount =0.0;
									if(loanSummary.getInterestamount()!=null)
									{
										intrestAmount=loanSummary.getInterestamount();
									}
									intrestAmount = intrestAmount+intAmt;
									loanSummary.setInterestamount(intrestAmount);
									
									double disbursed = loanSummary.getDisbursedamount();
									double totalAmt = disbursed+intrestAmount;
									loanSummary.setTotalamount(totalAmt);
									
									double fpaid = loanSummary.getPaidamount()+paidAmt;
									loanSummary.setPaidamount(fpaid);
									
									loanSummary.setPendingamount(finalPrinciple);
									advanceLoansTablesList.add(loanSummary);
									
									
									int branchCode = loanSummary.getBranchcode();

									
									String qry = "Update LoanPrincipleAmounts set principle="+finalPrinciple+",principledate='"+CaneaccDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
									
									//Updating Record in Loan Details table.
									qry = "Update LoanDetails set interestamount="+intrestAmount+",pendingamount="+finalPrinciple+",recoverydate='"+CaneaccDate+"',paidamount="+fpaid+" where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
								}
								
								
							}
						}
						
						if(i == 0)
						{
							List CaneAccountSmryTempList = new ArrayList();
							CaneAccountSmryTempList=caneAccountingFunctionalService.CaneAccountSmryTemp(caneAccSlno,season);
							if (CaneAccountSmryTempList != null && !CaneAccountSmryTempList.isEmpty())
							{
								Map CaneAccSmryMap = new HashMap();
								CaneAccSmryMap = (Map) CaneAccountSmryTempList.get(0);
								
								int caneacslno = (Integer) CaneAccSmryMap.get("caneacslno");
								Date dateFrom = (Date) CaneAccSmryMap.get("datefrom");
								Date dateOfCalculation = (Date) CaneAccSmryMap.get("dateofcalc");
								Date dateTo = (Date) CaneAccSmryMap.get("dateto");
								String user = (String) CaneAccSmryMap.get("login");
								String ses = (String) CaneAccSmryMap.get("season");
								byte acctype = (Byte) CaneAccSmryMap.get("acctype");
								CaneAccountSmry caneAccountSmry = new CaneAccountSmry();

								caneAccountSmry.setCaneacslno(caneacslno);
								caneAccountSmry.setDatefrom(dateFrom);
								caneAccountSmry.setDateofcalc(dateOfCalculation);
								caneAccountSmry.setDateto(dateTo);
								caneAccountSmry.setLogin(user);
								caneAccountSmry.setSeason(ses);
								caneAccountSmry.setTransactioncode(transactionCode);
								caneAccountSmry.setAcctype(acctype);
								advanceLoansTablesList.add(caneAccountSmry);
							
								strQry = "Delete from CaneAccountSmrytemp where season='"+season+"' and caneacslno = "+caneAccSlno;
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
						}
						
						List CaneValueSplitupTempList = new ArrayList();
						CaneValueSplitupTempList=caneAccountingFunctionalService.CaneValueSplitupTemp(caneAccSlno,season,ryotCode);
						if (CaneValueSplitupTempList != null && CaneValueSplitupTempList.size()>0)
						{
							Map CaneValueSplitupTempMap = new HashMap();
							CaneValueSplitupTempMap = (Map) CaneValueSplitupTempList.get(0);
							
							double ForLoans = (Double) CaneValueSplitupTempMap.get("forloans");
							String ryot = (String) CaneValueSplitupTempMap.get("ryotname");
							byte cancelproportion = (Byte) CaneValueSplitupTempMap.get("cancelproportion");
							int caneacslno = (Integer) CaneValueSplitupTempMap.get("caneacslno");
							double canesupplied = (Double) CaneValueSplitupTempMap.get("canesupplied");
							double forsbaccs = (Double) CaneValueSplitupTempMap.get("forsbacs");
							double rate = (Double) CaneValueSplitupTempMap.get("rate");
							String ryotcde = (String) CaneValueSplitupTempMap.get("ryotcode");
							String ses = (String) CaneValueSplitupTempMap.get("season");
							int slno = (Integer) CaneValueSplitupTempMap.get("slno");
							double totalvalue = (Double) CaneValueSplitupTempMap.get("totalvalue");
							String login = (String) CaneValueSplitupTempMap.get("login");
							byte acctype = (Byte) CaneValueSplitupTempMap.get("acctype");
							CaneValueSplitup caneValueSplitup = new CaneValueSplitup();
							
							caneValueSplitup.setCancelproportion(cancelproportion);
							caneValueSplitup.setCaneacslno(caneacslno);
							caneValueSplitup.setCanesupplied(canesupplied);
							caneValueSplitup.setForloans(ForLoans);
							caneValueSplitup.setForsbacs(ryotforDeductions);
							caneValueSplitup.setRate(rate);
							caneValueSplitup.setRyotcode(ryotcde);
							caneValueSplitup.setRyotname(ryot);
							caneValueSplitup.setSeason(ses);
							caneValueSplitup.setSlno(slno);
							caneValueSplitup.setTotalvalue(totalvalue);
							caneValueSplitup.setLogin(login);
							caneValueSplitup.setAcctype(acctype);
							advanceLoansTablesList.add(caneValueSplitup);
						
							strQry = "Delete from CaneValueSplitupTemp where season='"+season+"' and caneacslno = "+caneAccSlno+" and ryotcode='"+ryotCode+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						}
						if(supplyType == 0)
				 		{
							//CaneAccountSmry
							List DateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
							if(DateList != null && DateList.size()>0)
							{
								Map tempMap = new HashMap();
								tempMap = (Map) DateList.get(0);
								Date datefrom = (Date) tempMap.get("datefrom");
								Date dateto = (Date) tempMap.get("dateto");
								
								strQry = "Update WeighmentDetails set castatus=1 where canereceiptenddate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								//Added by DMurty on 07-12-2016
								strQry = "Update CaneReceiptDaySmryByRyot set castatus=1 where canereceiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								//Added by DMurty on 23-01-2017
								//Here we can update transport and harvesting charges.
								strQry = "Update HarvChgDtlsByRyotContAndDate set castatus=1 where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								strQry = "Update TranspChgSmryByRyotContAndDate set castatus=1 where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
				 		}
						//Added by DMurty on 29-12-2016
						Date datefrom = null;
						Date dateto = null;
						List AccDateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
						logger.info("saveAdvancesAndLoansDetails()========AccDateList=========="+AccDateList);
						if(AccDateList != null && AccDateList.size()>0)
						{
							Map DateMap = new HashMap();
							DateMap = (Map) AccDateList.get(0);
							datefrom = (Date) DateMap.get("datefrom");
							dateto = (Date) DateMap.get("dateto");
							
							df = new SimpleDateFormat("dd-MM-yyyy");
							String datefrom1 = df.format(datefrom);
							String dateto1 = df.format(dateto);

							datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
							dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
						}
						logger.info("saveAdvancesAndLoansDetails()========datefrom=========="+datefrom);
						logger.info("saveAdvancesAndLoansDetails()========dateto=========="+dateto);
						
						
						CanAccSBDetails canAccSBDetails = new CanAccSBDetails();
						int sbBankCode = 0;
						String sbAccNo = null;
						if(supplyType==0)
						{
							String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
							logger.info("saveAdvancesAndLoansDetails()========agreementNo=========="+agreementNo);

							List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
							if(ryotBankList!=null)
							{
								
								AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
								sbBankCode=agreement.getBranchcode();
								sbAccNo = agreement.getAccountnumber();
								logger.info("========getRyotSbAccNo() sbBankCode=========="+sbBankCode);
								logger.info("========getRyotSbAccNo() sbAccNo=========="+sbAccNo);
							}	
						}
						else if(supplyType==2)
						{
							List AgreementList = ahFuctionalService.getAgreementsByRyot(ryotCode,season);
							if(AgreementList != null && AgreementList.size()>0)
							{
								sbAccNo=(String)((Map<Object,Object>)AgreementList.get(0)).get("accountnumber"); 
								 if((Integer)((Map<Object,Object>)AgreementList.get(0)).get("branchcode")!=null)
								 {
								 sbBankCode=(Integer)((Map<Object,Object>)AgreementList.get(0)).get("branchcode");
								 }
							}
						}
						
						
						canAccSBDetails.setAdvamt(ryottotalAdvPayable);
						canAccSBDetails.setBankcode(sbBankCode);
						canAccSBDetails.setCdcamt(ryotCdcAmount);
						canAccSBDetails.setDed(ryotforDeductions);
						canAccSBDetails.setHarvesting(ryotharvestChargePayable);
						canAccSBDetails.setLoanamt(ryottotalLoanPayable);				
						canAccSBDetails.setLoans(ryotforAdvAndLoans);
						canAccSBDetails.setOtherded(ryotOtherDed);
						canAccSBDetails.setPaidamt(0.0);//From Cane Accounting it will be 0
						canAccSBDetails.setPendingamt(ryotforSbAcs);
						canAccSBDetails.setRyotcode(ryotCode);
						canAccSBDetails.setSbac(ryotforSbAcs);
						canAccSBDetails.setSbaccno(sbAccNo);
						canAccSBDetails.setSeason(season);
						canAccSBDetails.setSuppqty(totalSuppliedWeight);
						canAccSBDetails.setTotalamt(ryotfinalTotalVal);//No Need
						canAccSBDetails.setTransport(ryotTcAmount);
						canAccSBDetails.setUlamt(ryotUlAmount);		
						canAccSBDetails.setCaneacctslno(caneAccSlno);
						//canAccSBDetails.setPaymentdate(new Date());
						canAccSBDetails.setPaymentsource(null);
						canAccSBDetails.setIsdirect((byte) 0);
						canAccSBDetails.setRyotdetails("NA");
						canAccSBDetails.setMainryotcode("NA");
						canAccSBDetails.setIciciloanamount(0.0);
						
						advanceLoansTablesList.add(canAccSBDetails);
		 			}
		 			else
		 			{
		 				ryotsbacc = Double.parseDouble(ryotsbac);
		 				//double ryotsbacc = (double) ryotsbac;

			 			ryotLoansPartToSbAccPaid = ryotsbacc;
			 			//Here getting Sum of DedPartToSbAcPayable from CaneValueSplitupTemp. For first rec it is enough to get. No need to call this in for loop
			 			if(i==0)
			 			{
			 				ncaneAccSlno = caneAccSlno;
						    List dedPartList = ahFuctionalService.getDedPartForSbAccList(ncaneAccSlno,season);
						    if(dedPartList != null && dedPartList.size()>0)
						    {
						    	Map tempMap = new HashMap();
						    	tempMap = (Map) dedPartList.get(0);
						    	finalDedPartToSbAcPayable = (Double) tempMap.get("ded");
						    	forAdvAndLoans = (Double) tempMap.get("advloans");
						    	finalTotalVal = (Double) tempMap.get("totalval");
						    }
			 			}
			 			
			 			//Getting amounts for individual ryots
					    List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(ncaneAccSlno,season,ryotCode);
						logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
					    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
					    {
					    	Map tempMap = new HashMap();
					    	tempMap = (Map) dedPartForRyotList.get(0);
					    	ryotforAdvAndLoans = (Double) tempMap.get("forloans");
					    	ryotfinalTotalVal = (Double) tempMap.get("totalvalue");
					    	ryotforDeductions = ryotfinalTotalVal-ryotforAdvAndLoans;//(Double) tempMap.get("forsbacs");
					    }
			 			
					    //get dedsbacc Amt from canevaluesplituptemp
						double dedSbAmt = caneAccountingFunctionalService.getdedSbAmt(caneAccSlno,season,ryotCode);
						ryotDedPartToSbAcPayable = dedSbAmt;
						
						double finalSbAcAmt = ryotsbacc;
						ryotLoansPartToSbAccPaid = ryotsbacc;
			 			finalforSbAcs += finalSbAcAmt;
			 			ryotforSbAcs += finalSbAcAmt;
			 			
			 			strQry = "Update CaneValueSplitupTemp set forsbacs="+ryotforDeductions+" where ryotcode='"+ryotCode+"' and season='"+season+"' and caneacslno="+caneAccSlno+"";
			 			Qryobj = strQry;
						UpdateList.add(Qryobj);
						logger.info("saveAdvancesAndLoansDetails()------strQry"+strQry);
			 			
						if(i == 0)
						{
							List CaneAccountSmryTempList = new ArrayList();
							CaneAccountSmryTempList=caneAccountingFunctionalService.CaneAccountSmryTemp(caneAccSlno,season);
							if (CaneAccountSmryTempList != null && !CaneAccountSmryTempList.isEmpty())
							{
								Map CaneAccSmryMap = new HashMap();
								CaneAccSmryMap = (Map) CaneAccountSmryTempList.get(0);
								
								int caneacslno = (Integer) CaneAccSmryMap.get("caneacslno");
								Date dateFrom = (Date) CaneAccSmryMap.get("datefrom");
								Date dateOfCalculation = (Date) CaneAccSmryMap.get("dateofcalc");
								Date dateTo = (Date) CaneAccSmryMap.get("dateto");
								String user = (String) CaneAccSmryMap.get("login");
								String ses = (String) CaneAccSmryMap.get("season");
								byte acctype = (Byte) CaneAccSmryMap.get("acctype");
								CaneAccountSmry caneAccountSmry = new CaneAccountSmry();

								caneAccountSmry.setCaneacslno(caneacslno);
								caneAccountSmry.setDatefrom(dateFrom);
								caneAccountSmry.setDateofcalc(dateOfCalculation);
								caneAccountSmry.setDateto(dateTo);
								caneAccountSmry.setLogin(user);
								caneAccountSmry.setSeason(ses);
								caneAccountSmry.setTransactioncode(transactionCode);
								caneAccountSmry.setAcctype(acctype);
								advanceLoansTablesList.add(caneAccountSmry);
							
								strQry = "Delete from CaneAccountSmrytemp where season='"+season+"' and caneacslno = "+caneAccSlno;
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
						}
						
						
						List CaneValueSplitupTempList = new ArrayList();
						CaneValueSplitupTempList=caneAccountingFunctionalService.CaneValueSplitupTemp(caneAccSlno,season,ryotCode);
						if (CaneValueSplitupTempList != null && CaneValueSplitupTempList.size()>0)
						{

							Map CaneValueSplitupTempMap = new HashMap();
							CaneValueSplitupTempMap = (Map) CaneValueSplitupTempList.get(0);
							
							double ForLoans = (Double) CaneValueSplitupTempMap.get("forloans");
							String ryot = (String) CaneValueSplitupTempMap.get("ryotname");
							byte cancelproportion = (Byte) CaneValueSplitupTempMap.get("cancelproportion");
							int caneacslno = (Integer) CaneValueSplitupTempMap.get("caneacslno");
							double canesupplied = (Double) CaneValueSplitupTempMap.get("canesupplied");
							double forsbaccs = (Double) CaneValueSplitupTempMap.get("forsbacs");
							double rate = (Double) CaneValueSplitupTempMap.get("rate");
							String ryotcde = (String) CaneValueSplitupTempMap.get("ryotcode");
							String ses = (String) CaneValueSplitupTempMap.get("season");
							int slno = (Integer) CaneValueSplitupTempMap.get("slno");
							double totalvalue = (Double) CaneValueSplitupTempMap.get("totalvalue");
							String login = (String) CaneValueSplitupTempMap.get("login");
							byte acctype = (Byte) CaneValueSplitupTempMap.get("acctype");
							CaneValueSplitup caneValueSplitup = new CaneValueSplitup();
							
							caneValueSplitup.setCancelproportion(cancelproportion);
							caneValueSplitup.setCaneacslno(caneacslno);
							caneValueSplitup.setCanesupplied(canesupplied);
							caneValueSplitup.setForloans(ForLoans);
							caneValueSplitup.setForsbacs(ryotforDeductions);
							caneValueSplitup.setRate(rate);
							caneValueSplitup.setRyotcode(ryotcde);
							caneValueSplitup.setRyotname(ryot);
							caneValueSplitup.setSeason(ses);
							caneValueSplitup.setSlno(slno);
							caneValueSplitup.setTotalvalue(totalvalue);
							caneValueSplitup.setLogin(login);
							caneValueSplitup.setAcctype(acctype);
							
							advanceLoansTablesList.add(caneValueSplitup);
						
							strQry = "Delete from CaneValueSplitupTemp where season='"+season+"' and caneacslno = "+caneAccSlno+" and ryotcode='"+ryotCode+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						}
						if(supplyType == 0)
				 		{
							List FinalList = new ArrayList();
							List DedDetails_temps=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
							logger.info("saveAdvancesAndLoansDetails()------DedDetails_temps"+DedDetails_temps);
							if (DedDetails_temps != null && !DedDetails_temps.isEmpty())
							{
								for(int k=0;k<DedDetails_temps.size();k++)
								{
									Map DedMap = new HashMap();
									DedMap = (Map) DedDetails_temps.get(k);
									
									int id = (Integer) DedMap.get("id");
									int dedCode = (Integer) DedMap.get("DedCode");
									String deduction = (String) DedMap.get("Deduction");
									int caneAcSlno = (Integer) DedMap.get("caneacslno");
									double deductionAmt = (Double) DedMap.get("deductionamt");
									double extentSize = (Double) DedMap.get("extentsize");
									int harvestContCode = (Integer) DedMap.get("harvestcontcode");
									double netPayable = (Double) DedMap.get("netpayable");
									double newPayable = (Double) DedMap.get("newpayable");
									double oldPendingAmount = (Double) DedMap.get("oldpendingamount");
									double pendingAmount = (Double) DedMap.get("pendingamt");
									String ryotcode = (String) DedMap.get("ryotcode");
									String dedSeason = (String) DedMap.get("season");
									double suppliedCaneWt = (Double) DedMap.get("suppliedcaneWt");
									String login = (String) DedMap.get("login");
									if(dedCode == 1)
									{
										finalCdcAmount += deductionAmt;
										ryotCdcAmount += deductionAmt;
									}
									else if(dedCode == 2)
									{
										finalUlAmount += deductionAmt;
										ryotUlAmount += deductionAmt;
									}
									else if(dedCode == 3)
									{
										finalTcAmount += deductionAmt;
										ryotTcAmount += deductionAmt;
									}
									else if(dedCode == 4)
									{
										finalHarAmount += deductionAmt;
										ryotHarAmount += deductionAmt;
										ryotharvestChargePayable += deductionAmt;
										harvestChargePayable += deductionAmt;
									}
									else if(dedCode == 5)
									{
										otherDed += deductionAmt;
										ryotOtherDed += deductionAmt;
									}
									
									//Here i need to update amount fields in harvesting and transport charges tables.
									//3 tp charges (deductionAmt)
									//4 harvestingcharges (deductionAmt)
									if(dedCode == 3)
									{
										List TpList = caneAccountingFunctionalService.getTpDedDtls(ryotCode,season,harvestContCode);
										if (TpList != null && !TpList.isEmpty())
										{
											Map TempMap = new HashMap();
											TempMap = (Map) TpList.get(0);
											double pendingAmt = (Double) TempMap.get("pendingamount");
											double totalAmt = (Double) TempMap.get("totalamount");
											double totalPayable = (Double) TempMap.get("totalpayableamount");
	
											double finalTotalPayable = totalPayable+deductionAmt;
											double finalpendingAmt = totalAmt-finalTotalPayable;
											
											strQry = "Update TranspChgSmryByRyotAndCont set totalpayableamount="+finalTotalPayable+",pendingamount="+finalpendingAmt+" where ryotcode ='"+ryotCode+"' and season='"+season+"' and transportcontcode="+harvestContCode+"";
											Qryobj = strQry;
											UpdateList.add(Qryobj);
										}
									}
									else if(dedCode == 4)
									{
										List HarvestingList = caneAccountingFunctionalService.getHarvestinChargesDtls(ryotCode,season,harvestContCode);
										if (HarvestingList != null && !HarvestingList.isEmpty())
										{
											Map TempMap = new HashMap();
											TempMap = (Map) HarvestingList.get(0);
											
											double pendingAmt = (Double) TempMap.get("PendingAmount");
											double totalAmt = (Double) TempMap.get("totalamount");
											double totalPayable = (Double) TempMap.get("totalpayableamount");
	
											double finalTotalPayable = totalPayable+deductionAmt;
											double finalpendingAmt = totalAmt-finalTotalPayable;
											
											strQry = "Update HarvChgSmryByRyotAndCont set totalpayableamount="+finalTotalPayable+",PendingAmount="+finalpendingAmt+" where ryotcode ='"+ryotCode+"' and season='"+season+"' and harvestcontcode="+harvestContCode+"";
											Qryobj = strQry;
											UpdateList.add(Qryobj);
										}
									}
									
									DedDetails dedDetails = new DedDetails();
	
									dedDetails.setCaneacslno(caneAcSlno);
									dedDetails.setDedCode(dedCode);
									dedDetails.setDeduction(deduction);
									dedDetails.setDeductionamt(deductionAmt);
									dedDetails.setExtentsize(extentSize);
									dedDetails.setHarvestcontcode(harvestContCode);
									//dedDetails.setId(id);
									dedDetails.setNetpayable(netPayable);
									dedDetails.setNewpayable(newPayable);
									dedDetails.setOldpendingamount(oldPendingAmount);
									dedDetails.setPendingamt(pendingAmount);
									dedDetails.setRyotcode(ryotcode);
									dedDetails.setSeason(dedSeason);
									dedDetails.setSuppliedcaneWt(suppliedCaneWt);
									dedDetails.setLogin(login);
									advanceLoansTablesList.add(dedDetails);
	
									double totalPrice = dedDetails.getDeductionamt();
									String glCode="";
									if(dedCode==1 || dedCode==2 || dedCode==3  || dedCode==4)
									{
										String journalMemo=null;
										if(dedCode==1)
										{
											journalMemo= "Towards CDC Fund";
											glCode = "1186";
										}
										else if(dedCode==2)
										{
											journalMemo="Towards Unloading Charges";
											glCode = "UL";
										}
										else if(dedCode==3)
										{
											journalMemo="Towards Transport Charges";
											int transportCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(transportCont,"TransportingContractors","transportercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												glCode = (String) tempMap.get("accountcode");
											}								
										}
										else if(dedCode==4)
										{
											journalMemo="Towards Harvesting Charges";
											int harvestCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(harvestCont,"HarvestingContractors","harvestercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												glCode = (String) tempMap.get("accountcode");
											}
										}
										String transactionType = "Dr";
	
										String TransactionType = transactionType;
										String strglCode = glCode;
										String accCode = ryotCode;
										double Amt = totalPrice;
										if(accCode != null)
										{
											int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
											String strAccGrpCode = Integer.toString(AccGrpCode);
	
											int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
											String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
											Map tempMap1 = new HashMap();
											tempMap1.put("ACCOUNT_CODE", accCode);
											tempMap1.put("AMOUNT", Amt);
											tempMap1.put("JRNL_MEM", journalMemo);
											tempMap1.put("SEASON", season);
											tempMap1.put("TRANS_TYPE", TransactionType);
											tempMap1.put("GL_CODE", strglCode);
											tempMap1.put("TRANSACTION_CODE", transactionCode);
										
											//Modified by DMurty on 03-03-2017
											tempMap1.put("TRANSACTION_DATE",strCaneaccDate);
											Map AccMap = updateAccountsNew(tempMap1);
											logger.info("saveAdvancesAndLoansDetails()------AccMap"+AccMap);
	
											advanceLoansTablesList.add(AccMap.get("accDtls"));
											advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
											advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
											
											Map AccSmry = new HashMap();
											AccSmry.put("ACC_CODE", accCode);
											boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
											if (ryotAccCodetrue == false)
											{
												AccountCodeList.add(AccSmry);
												AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
											}
											else
											{
												AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
												String straccCode = accountSummary.getAccountcode();
												if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
												{
													double runbal = accountSummary.getRunningbalance();
													String baltype = accountSummary.getBalancetype();
													String ses = accountSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_CODE", accCode);
													tmpMap.put("KEY", accCode);
													Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
													AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
												}
											}
											
											Map AccGrpSmry = new HashMap();
											AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
											if (loanAccGrpCodetrue == false)
											{
												AccountGrpCodeList.add(AccGrpSmry);
												AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
											}
											else
											{
												AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
												int accountGrpCode = accountGroupSummary.getAccountgroupcode();
												if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
												{
													double runbal = accountGroupSummary.getRunningbalance();
													String baltype = accountGroupSummary.getBalancetype();
													String ses = accountGroupSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("KEY", strAccGrpCode);
													
													Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
													AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
												}
											}
											
											Map AccSubGrpSmry = new HashMap();
											//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
											boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
											if(ryotLoanAccSubGrpCodetrue == false)
											{
												AccountSubGrpCodeList.add(AccSubGrpSmry);
												AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
											}
											else
											{
												AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
												int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
												int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
												String ses = accountSubGroupSummary.getSeason();
												if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
												{
													double runbal = accountSubGroupSummary.getRunningbalance();
													String baltype = accountSubGroupSummary.getBalancetype();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
													tmpMap.put("KEY",strAccSubGrpCode);
													Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
													AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
												}
											}
										}
										//boolean accountFlag=updateAccounts(ryotCode,totalPrice,journalMemo,season,transactionType,glCode,transactionCode);
										
										//Here we need to post dr entry for individual deductions.
										glCode=ryotCode;
										strglCode = glCode;
										String accountCode = null;
										if(dedCode==1)
										{
											journalMemo= "Towards CDC Fund";
											accountCode = "1186";
										}
										else if(dedCode==2)
										{
											journalMemo="Towards Unloading Charges";
											accountCode = "UL";
										}
										else if(dedCode==3)
										{
											journalMemo="Towards Transport Charges";
											int transportCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(transportCont,"TransportingContractors","transportercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												accountCode = (String) tempMap.get("accountcode");
											}	
										}
										else if(dedCode==4)
										{
											journalMemo="Towards Harvesting Charges";
											int harvestCont = dedDetails.getHarvestcontcode();
											List detailsList = caneAccountingFunctionalService.getAccountCode(harvestCont,"HarvestingContractors","harvestercode");
											if(detailsList != null && detailsList.size()>0)
											{
												Map tempMap = new HashMap();
												tempMap = (Map) detailsList.get(0);
												accountCode = (String) tempMap.get("accountcode");
											}
										}
										transactionType = "Dr";
										if(accountCode != null)
										{
											accCode = accountCode;
											TransactionType = transactionType;
											int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
											String strAccGrpCode = Integer.toString(AccGrpCode);
	
											int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
											String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
											Map tempMap = new HashMap();
											tempMap.put("ACCOUNT_CODE", accCode);
											tempMap.put("AMOUNT", Amt);
											tempMap.put("JRNL_MEM", journalMemo);
											tempMap.put("SEASON", season);
											tempMap.put("TRANS_TYPE", TransactionType);
											tempMap.put("GL_CODE", strglCode);
											tempMap.put("TRANSACTION_CODE", transactionCode);
											//Added by DMurty on 25-12-2016
											tempMap.put("TRANSACTION_DATE", strCaneaccDate);
											
											Map AccMap = updateAccountsNew(tempMap);
											
											advanceLoansTablesList.add(AccMap.get("accDtls"));
											advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
											advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
											
											Map AccSmry = new HashMap();
											AccSmry.put("ACC_CODE", accCode);
											boolean istrueforded = AccountCodeList.contains(AccSmry);
											if (istrueforded == false)
											{
												AccountCodeList.add(AccSmry);
												AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
											}
											else
											{
												AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
												String straccCode = accountSummary.getAccountcode();
												if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
												{
													double runbal = accountSummary.getRunningbalance();
													String baltype = accountSummary.getBalancetype();
													String ses = accountSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_CODE", accCode);
													tmpMap.put("KEY",accCode);
													Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
													AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
												}
											}
											//dedAccGrpCodeList
											Map AccGrpSmry = new HashMap();
											AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											boolean dedAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
											if (dedAccGrpCodetrue == false)
											{
												AccountGrpCodeList.add(AccGrpSmry);
												AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
											}
											else
											{
												AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
												int accountGrpCode = accountGroupSummary.getAccountgroupcode();
												if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
												{
													double runbal = accountGroupSummary.getRunningbalance();
													String baltype = accountGroupSummary.getBalancetype();
													String ses = accountGroupSummary.getSeason();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("KEY", strAccGrpCode);
													
													Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
													AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
												}
											}
											
											Map AccSubGrpSmry = new HashMap();
											//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
											AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
											boolean dedAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
											if(dedAccSubGrpCodetrue == false)
											{
												AccountSubGrpCodeList.add(AccSubGrpSmry);
												AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
											}
											else
											{
												AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
												int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
												int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
												String ses = accountSubGroupSummary.getSeason();
												if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
												{
													double runbal = accountSubGroupSummary.getRunningbalance();
													String baltype = accountSubGroupSummary.getBalancetype();
													
													Map tmpMap = new HashMap();
													tmpMap.put("RUN_BAL", runbal);
													tmpMap.put("AMT", Amt);
													tmpMap.put("BAL_TYPE", baltype);
													tmpMap.put("SEASON", ses);
													tmpMap.put("TRANSACTION_TYPE", TransactionType);
													tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
													tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
													tmpMap.put("KEY",strAccSubGrpCode);
													Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
													AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
												}
											}
										}
									}
								}
								strQry = "Delete from DedDetails_temp where season='"+season+"' and caneacslno = "+caneAccSlno+" and ryotcode='"+ryotCode+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
				 			
							//CaneAccountSmry
							List DateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
							if(DateList != null && DateList.size()>0)
							{
								Map tempMap = new HashMap();
								tempMap = (Map) DateList.get(0);
								Date datefrom = (Date) tempMap.get("datefrom");
								Date dateto = (Date) tempMap.get("dateto");
								
								strQry = "Update WeighmentDetails set castatus=1 where canereceiptenddate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								//Added by DMurty on 07-12-2016
								strQry = "Update CaneReceiptDaySmryByRyot set castatus=1 where canereceiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								//Added by DMurty on 23-01-2017
								//Here we can update transport and harvesting charges.
								strQry = "Update HarvChgDtlsByRyotContAndDate set castatus=1 where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
								
								strQry = "Update TranspChgSmryByRyotContAndDate set castatus=1 where receiptdate between '"+datefrom+"' and '"+dateto+"' and ryotcode='"+ryotCode+"' and season='"+season+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
				 		}
						//Added by DMurty on 29-12-2016
						Date datefrom = null;
						Date dateto = null;
						List AccDateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
						logger.info("saveAdvancesAndLoansDetails()========AccDateList=========="+AccDateList);
						if(AccDateList != null && AccDateList.size()>0)
						{
							Map DateMap = new HashMap();
							DateMap = (Map) AccDateList.get(0);
							datefrom = (Date) DateMap.get("datefrom");
							dateto = (Date) DateMap.get("dateto");
							
							df = new SimpleDateFormat("dd-MM-yyyy");
							String datefrom1 = df.format(datefrom);
							String dateto1 = df.format(dateto);

							datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
							dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
						}
						logger.info("saveAdvancesAndLoansDetails()========datefrom=========="+datefrom);
						logger.info("saveAdvancesAndLoansDetails()========dateto=========="+dateto);
						
						
						String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
						logger.info("saveAdvancesAndLoansDetails()========agreementNo=========="+agreementNo);
						
						CanAccSBDetails canAccSBDetails = new CanAccSBDetails();
						int sbBankCode = 0;
						String sbAccNo = null;
						List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
						if(ryotBankList!=null)
						{
							AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
							sbBankCode=agreement.getBranchcode();
							sbAccNo = agreement.getAccountnumber();
							logger.info("========getRyotSbAccNo() sbBankCode=========="+sbBankCode);
							logger.info("========getRyotSbAccNo() sbAccNo=========="+sbAccNo);
						}
						canAccSBDetails.setAdvamt(ryottotalAdvPayable);
						canAccSBDetails.setBankcode(sbBankCode);
						canAccSBDetails.setCdcamt(ryotCdcAmount);
						canAccSBDetails.setDed(ryotforDeductions);
						canAccSBDetails.setHarvesting(ryotharvestChargePayable);
						canAccSBDetails.setLoanamt(ryottotalLoanPayable);				
						canAccSBDetails.setLoans(ryotforAdvAndLoans);
						canAccSBDetails.setOtherded(ryotOtherDed);
						canAccSBDetails.setPaidamt(0.0);//From Cane Accounting it will be 0
						canAccSBDetails.setPendingamt(ryotforSbAcs);
						canAccSBDetails.setRyotcode(ryotCode);
						canAccSBDetails.setSbac(ryotforSbAcs);
						canAccSBDetails.setSbaccno(sbAccNo);
						canAccSBDetails.setSeason(season);
						canAccSBDetails.setSuppqty(totalSuppliedWeight);
						canAccSBDetails.setTotalamt(ryotfinalTotalVal);//No Need
						canAccSBDetails.setTransport(ryotTcAmount);
						canAccSBDetails.setUlamt(ryotUlAmount);		
						canAccSBDetails.setCaneacctslno(caneAccSlno);
						canAccSBDetails.setPaymentsource(null);
						canAccSBDetails.setIsdirect((byte) 0);
						canAccSBDetails.setRyotdetails("NA");
						canAccSBDetails.setMainryotcode("NA");
						canAccSBDetails.setIciciloanamount(0.0);
						
						advanceLoansTablesList.add(canAccSBDetails);
		 			}
				}
				else
				{
					//This is for with out supply. i.e For Advance or Loan Payment
					
					double totalSuppliedWeight = 0.0;
					strSeason = season;
					
					//For entire Cane Accounting, this is unique for all ryots.
					if(i==0)
		 			{
						transactionCode = commonService.GetMaxTransCode(season);
						logger.info("saveAdvancesAndLoansDetails()------transactionCode"+transactionCode);
		 			}
					
					logger.info("======== For Ryot No=========="+i);
					logger.info("======== For Ryot Code=========="+ryotCode);
					
					JSONArray gridData=(JSONArray) jsonObject.get("caneacslno");
					logger.info("======== saveAdvancesAndLoansDetails()==========gridData "+gridData);

		 			if(gridData != null && gridData.isEmpty() == false)
		 			{
					    for(int j=0;j<gridData.size();j++)
						{
					    	AdvancesLoansForPCABean advancesLoansForPCABean = new ObjectMapper().readValue(gridData.get(j).toString(), AdvancesLoansForPCABean.class);
							if(advancesLoansForPCABean.getAdvanceflag().equals("0"))
							{
								Date repaymentDate=null;
								/*if(advancesLoansForPCABean.getLoanPayable()== 0)
								{
									continue;
								}*/
								AdvanceSummary advanceSummary = prepareModelForUpdateAdvanceSummary(advancesLoansForPCABean,season,ryotCode);
								if(advanceSummary!=null)
								{
									double advanceAmt = advanceSummary.getAdvanceamount();
									double pendingAmt = advanceSummary.getPendingamount();
								
									String qry = "Update AdvancePrincipleAmounts set principle="+pendingAmt+",principledate='"+CaneaccDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and advancecode="+advancesLoansForPCABean.getAdvancecode();
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
									
									double paidAmt = advanceSummary.getRecoveredamount();
									double intAmt = 0.0;
									if(advanceSummary.getInterestamount() != null)
									{
										intAmt = advanceSummary.getInterestamount();
									}
									double totalAmt = paidAmt+intAmt;
									advanceLoansTablesList.add(advanceSummary);	
								}
								
								CanAccLoansDetails canAccLoansDetails=new CanAccLoansDetails();
								
								canAccLoansDetails.setAccountnum(advancesLoansForPCABean.getReferencenumber());
								double inrst = 0.0;
								if(advancesLoansForPCABean.getAdvanceinterest() != null)
								{
									inrst = advancesLoansForPCABean.getAdvanceinterest();
								}
								
								double adjPrinciple = advancesLoansForPCABean.getTotalamount()-advancesLoansForPCABean.getLoanPayable();
								canAccLoansDetails.setAdjustedprincipleamount(adjPrinciple);
								
								canAccLoansDetails.setAdvanceamount(advancesLoansForPCABean.getAdvanceamount());
								//Modified by DMurty on 22-12-2016
								Date currDate = DateUtils.getSqlDateFromString(advancesLoansForPCABean.getAdvancedate(),Constants.GenericDateFormat.DATE_FORMAT);
								canAccLoansDetails.setAdvdate(currDate);
								canAccLoansDetails.setAdvloan(0);
								canAccLoansDetails.setBranchcode(Integer.parseInt(advancesLoansForPCABean.getReferencenumber()));
								canAccLoansDetails.setCaneacctslno(caneAccSlno);
								canAccLoansDetails.setDelayedintrest(0.0);
								canAccLoansDetails.setInterestamount(advancesLoansForPCABean.getAdvanceinterest());
								double intRate = 0.0;
								if(advancesLoansForPCABean.getNetRate() != null)
								{
									intRate = advancesLoansForPCABean.getNetRate();
								}
								intRate =Double.parseDouble(new DecimalFormat("##.##").format(intRate));
								
								canAccLoansDetails.setInterestrate(intRate);
								canAccLoansDetails.setLoanno(advancesLoansForPCABean.getLoanNo());
								canAccLoansDetails.setNetrate(advancesLoansForPCABean.getAdvanceinterest());
								canAccLoansDetails.setPaidamt(advancesLoansForPCABean.getLoanPayable());
								double pendingAmt = advancesLoansForPCABean.getAdvanceamount()-advancesLoansForPCABean.getLoanPayable();
								pendingAmt =Double.parseDouble(new DecimalFormat("##.##").format(pendingAmt));
								canAccLoansDetails.setPendingamt(pendingAmt);
							
								String repayDate = advancesLoansForPCABean.getAdvancerepaydate();
								Date strRepayDate = DateUtils.getSqlDateFromString(repayDate,Constants.GenericDateFormat.DATE_FORMAT);
								canAccLoansDetails.setRepaymentdate(strRepayDate);
								
								canAccLoansDetails.setRyotcode(ryotCode);
								canAccLoansDetails.setSeason(season);
								if(advancesLoansForPCABean.getSubventedInterestRate() != null)
								{
									double subvIntrestRate = Double.parseDouble(advancesLoansForPCABean.getSubventedInterestRate());
									canAccLoansDetails.setSubventedrate(subvIntrestRate);
								}
								else
								{
									canAccLoansDetails.setSubventedrate(0.0);
								}
								canAccLoansDetails.setTotalamount(advancesLoansForPCABean.getTotalamount()+inrst);
								
								canAccLoansDetails.setPaymentsource(null);
								canAccLoansDetails.setIsdirect((byte) 0);
								canAccLoansDetails.setRyotdetails("NA");
								canAccLoansDetails.setMainryotcode("NA");
								
								//Modified by DMurty on 16-12-2016
								canAccLoansDetails.setAdvisedamt(advancesLoansForPCABean.getLoanPayable());
								
								canAccLoansDetails.setAdditionalint(0.0);
								
								double finalPrinciple = adjPrinciple+canAccLoansDetails.getAdditionalint();
								canAccLoansDetails.setFinalprinciple(finalPrinciple);
								
								canAccLoansDetails.setIntryotpayable(0.0);
								canAccLoansDetails.setFinalinterest(0.0);
								
								advanceLoansTablesList.add(canAccLoansDetails);	
								
								Map rMap = new HashMap();
								rMap.put("RYOT_CODE", ryotCode);
								boolean istrue = ryotList.contains(rMap);
								if(istrue == false)
								{
									ryotList.add(rMap);
								}
								
								//Added by DMurty on 31-10-2016
								int advCode = advancesLoansForPCABean.getAdvancecode();
								List<CompanyAdvance> companyAdvance = aHFuctionalService.getAdvanceDateilsByAdvanceCode(advCode);
								String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
								
								String TransactionType = "Cr";
								String strglCode = ryotCode;
								String accCode = GlCodeforAdvance;
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);
								
								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								double amt = advancesLoansForPCABean.getLoanPayable();
								double Amt = amt;
								String strtype = "";
								if(accCode != null)
								{
									Map tempMap = new HashMap();
									tempMap.put("ACCOUNT_CODE", accCode);
									tempMap.put("AMOUNT", amt);
									tempMap.put("JRNL_MEM", "Towards Advances Recovery");
									tempMap.put("SEASON", season);
									tempMap.put("TRANS_TYPE", TransactionType);
									tempMap.put("GL_CODE", strglCode);
									tempMap.put("TRANSACTION_CODE", transactionCode);
									
									//Modified by DMurty on 03-03-2017
									tempMap.put("TRANSACTION_DATE", strCaneaccDate);

									Map AccMap = updateAccountsNew(tempMap);
									logger.info("saveAdvancesAndLoansDetails()------AccMap"+AccMap);

									advanceLoansTablesList.add(AccMap.get("accDtls"));
									advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
									advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
									
									Map AccSmry = new HashMap();
									AccSmry.put("ACC_CODE", accCode);
									boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
									logger.info("saveAdvancesAndLoansDetails()------AccountCodeList"+AccountCodeList);
									if (advAccCodetrue == false)
									{
										AccountCodeList.add(AccSmry);
										AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
									}
									else
									{
										AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
										String straccCode = accountSummary.getAccountcode();
										if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
										{
											double runbal = accountSummary.getRunningbalance();
											String baltype = accountSummary.getBalancetype();
											String ses = accountSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_CODE", accCode);
											tmpMap.put("KEY", accCode);
											Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
											AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
										}
									}
									
									//advClrAccGrpCodeList
									Map AccGrpSmry = new HashMap();
									AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
									boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
									if (advAccGrpCodetrue == false)
									{
										AccountGrpCodeList.add(AccGrpSmry);
										AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
									}
									else
									{
										AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
										int accountGrpCode = accountGroupSummary.getAccountgroupcode();
										if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
										{
											double runbal = accountGroupSummary.getRunningbalance();
											String baltype = accountGroupSummary.getBalancetype();
											String ses = accountGroupSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("KEY", strAccGrpCode);
											
											Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
											AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode)); 
										}
									}
									
									Map AccSubGrpSmry = new HashMap();
									AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
									
									boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
									if(advAccSubGrpCodetrue == false)
									{
										AccountSubGrpCodeList.add(AccSubGrpSmry);
										AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
									}
									else
									{
										AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
										int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
										int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
										String ses = accountSubGroupSummary.getSeason();
										if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
										{
											double runbal = accountSubGroupSummary.getRunningbalance();
											String baltype = accountSubGroupSummary.getBalancetype();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
											tmpMap.put("KEY", strAccSubGrpCode); 
											Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
											AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
										}
									}
								}
								
								
								//Here we need to post Cr Entry for ryot to show payment in ledger
								TransactionType = "Cr";
								strglCode = GlCodeforAdvance;
								accCode = ryotCode;
								AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								strAccGrpCode = Integer.toString(AccGrpCode);
								
								AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								amt = advancesLoansForPCABean.getLoanPayable();
								Amt = amt;
								strtype = "";
								if(accCode != null)
								{
									Map tempMap = new HashMap();
									tempMap.put("ACCOUNT_CODE", accCode);
									tempMap.put("AMOUNT", amt);
									tempMap.put("JRNL_MEM", "Payment towards Advance");
									tempMap.put("SEASON", season);
									tempMap.put("TRANS_TYPE", TransactionType);
									tempMap.put("GL_CODE", strglCode);
									tempMap.put("TRANSACTION_CODE", transactionCode);
									
									//Modified by DMurty on 03-03-2017
									tempMap.put("TRANSACTION_DATE", strCaneaccDate);
									
									Map AccMap = updateAccountsNew(tempMap);
									logger.info("saveAdvancesAndLoansDetails()------AccMap"+AccMap);

									advanceLoansTablesList.add(AccMap.get("accDtls"));
									advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
									advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
									
									Map AccSmry = new HashMap();
									AccSmry.put("ACC_CODE", accCode);
									boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
									logger.info("saveAdvancesAndLoansDetails()------AccountCodeList"+AccountCodeList);
									if (advAccCodetrue == false)
									{
										AccountCodeList.add(AccSmry);
										AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
									}
									else
									{
										AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
										String straccCode = accountSummary.getAccountcode();
										if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
										{
											double runbal = accountSummary.getRunningbalance();
											String baltype = accountSummary.getBalancetype();
											String ses = accountSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_CODE", accCode);
											tmpMap.put("KEY", accCode);
											Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
											AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
										}
									}
									
									//advClrAccGrpCodeList
									Map AccGrpSmry = new HashMap();
									AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
									boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
									if (advAccGrpCodetrue == false)
									{
										AccountGrpCodeList.add(AccGrpSmry);
										AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
									}
									else
									{
										AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
										int accountGrpCode = accountGroupSummary.getAccountgroupcode();
										if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
										{
											double runbal = accountGroupSummary.getRunningbalance();
											String baltype = accountGroupSummary.getBalancetype();
											String ses = accountGroupSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("KEY", strAccGrpCode);
											
											Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
											AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode)); 
										}
									}
									
									Map AccSubGrpSmry = new HashMap();
									AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
									
									boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
									if(advAccSubGrpCodetrue == false)
									{
										AccountSubGrpCodeList.add(AccSubGrpSmry);
										AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
									}
									else
									{
										AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
										int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
										int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
										String ses = accountSubGroupSummary.getSeason();
										if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
										{
											double runbal = accountSubGroupSummary.getRunningbalance();
											String baltype = accountSubGroupSummary.getBalancetype();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
											tmpMap.put("KEY", strAccSubGrpCode); 
											Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
											AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
										}
									}
								}
							}
							else
							{ 
								//Modified by DMurty on 14-03-2017
								CanAccLoansDetails canAccLoansDetails=prepareModelForCanAccLoansDetails(advancesLoansForPCABean,season,ryotCode,caneAccSlno,supplyType,CaneaccDate);
								ryottotalLoanPayable += advancesLoansForPCABean.getLoanPayable();
								
								int loanno = advancesLoansForPCABean.getLoanNo();
								int bankBranch = advancesLoansForPCABean.getBranchcode();
								
								double intAmt = advancesLoansForPCABean.getInterestAmount();
								double paidAmt = advancesLoansForPCABean.getLoanPayable();
								
								double loanAmt = advancesLoansForPCABean.getAdvanceamount();
								double total = loanAmt+intAmt;
								double loanPayable =  paidAmt-intAmt;
								double finalPrinciple = loanAmt-loanPayable;
								
								List loanSummaryList=caneAccountingFunctionalService.getLoanSummaryByBankAndLoanNo(season,ryotCode,loanno,bankBranch);
								if(loanSummaryList!=null)
								{		
									LoanSummary loanSummary=(LoanSummary)loanSummaryList.get(0);
									double intrestAmount =0.0;
									if(loanSummary.getInterestamount()!=null)
									{
										intrestAmount=loanSummary.getInterestamount();
									}
									intrestAmount = intrestAmount+intAmt;
									loanSummary.setInterestamount(intrestAmount);
									
									double disbursed = loanSummary.getDisbursedamount();
									double totalAmt = disbursed+intrestAmount;
									loanSummary.setTotalamount(totalAmt);
									
									double fpaid = loanSummary.getPaidamount()+paidAmt;
									loanSummary.setPaidamount(fpaid);
									
									loanSummary.setPendingamount(finalPrinciple);
									advanceLoansTablesList.add(loanSummary);
									
									
									int branchCode = loanSummary.getBranchcode();

									
									String qry = "Update LoanPrincipleAmounts set principle="+finalPrinciple+",principledate='"+CaneaccDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
									
									//Updating Record in Loan Details table.
									qry = "Update LoanDetails set interestamount="+intrestAmount+",pendingamount="+finalPrinciple+",recoverydate='"+CaneaccDate+"',paidamount="+fpaid+" where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
									logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
									Qryobj = qry;
									UpdateList.add(Qryobj);
									
									//here we need to post Accounting for Loans
									
									String transType = "Cr";
									String TransactionType = transType;
									String strglCode = ryotCode;
									String accCode = "RTL";
									double Amt = advancesLoansForPCABean.getLoanPayable();
									if(accCode != null)
									{
										int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
										String strAccGrpCode = Integer.toString(AccGrpCode);

										int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
										String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

										Map tempMap = new HashMap();
										tempMap.put("ACCOUNT_CODE", accCode);
										tempMap.put("AMOUNT", Amt);
										tempMap.put("JRNL_MEM", "Towards Tieup Loan Recovery");
										tempMap.put("SEASON", season);
										tempMap.put("TRANS_TYPE", TransactionType);
										tempMap.put("GL_CODE", strglCode);
										tempMap.put("TRANSACTION_CODE", transactionCode);

										//Modified by DMurty on 03-03-2017
										tempMap.put("TRANSACTION_DATE", strCaneaccDate);
										
										Map AccMap = updateAccountsNew(tempMap);
										
										advanceLoansTablesList.add(AccMap.get("accDtls"));
										advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
										advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
										
										Map AccSmry = new HashMap();
										AccSmry.put("ACC_CODE", accCode);
										
										boolean rtlAccCodetrue = AccountCodeList.contains(AccSmry);
										if (rtlAccCodetrue == false)
										{
											AccountCodeList.add(AccSmry);
											AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
										}
										else
										{
											AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
											String straccCode = accountSummary.getAccountcode();
											if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
											{
												double runbal = accountSummary.getRunningbalance();
												String baltype = accountSummary.getBalancetype();
												String ses = accountSummary.getSeason();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_CODE", accCode);
												tmpMap.put("KEY", accCode);
												Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
												AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
											}
										}
										Map AccGrpSmry = new HashMap();
										AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
										boolean rtlAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
										if (rtlAccGrpCodetrue == false)
										{
											AccountGrpCodeList.add(AccGrpSmry);
											AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
										}
										else
										{
											AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
											int accountGrpCode = accountGroupSummary.getAccountgroupcode();
											if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
											{
												double runbal = accountGroupSummary.getRunningbalance();
												String baltype = accountGroupSummary.getBalancetype();
												String ses = accountGroupSummary.getSeason();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
												tmpMap.put("KEY", strAccGrpCode);
												Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
												AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
											}
										}
										
										Map AccSubGrpSmry = new HashMap();
										AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
										
										boolean rtlAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
										if(rtlAccSubGrpCodetrue == false)
										{
											AccountSubGrpCodeList.add(AccSubGrpSmry);
											AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
										}
										else
										{
											AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
											int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
											int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
											String ses = accountSubGroupSummary.getSeason();
											if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
											{
												double runbal = accountSubGroupSummary.getRunningbalance();
												String baltype = accountSubGroupSummary.getBalancetype();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
												tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
												tmpMap.put("KEY", strAccSubGrpCode);
												Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
												AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
											}
										}
									}
									
									String strbrancgGlCode = null;
									int loanNo = advancesLoansForPCABean.getAdvancecode();
									List loanList = caneAccountingFunctionalService.getLoanDetails(loanNo,season,ryotCode);
									if(loanList != null && loanList.size()>0)
									{
										Map loanMap = new HashMap();
										loanMap = (Map) loanList.get(0);
										int bankCode = (Integer) loanMap.get("branchcode");
										List<Branch> branch = aHFuctionalService.getBranchDetailsByBranchCode(bankCode);
										long brancgGlCode = branch.get(0).getGlcode();
										strbrancgGlCode = Long.toString(brancgGlCode);
									}
									
									transType = "Dr";
									TransactionType = transType;
									strglCode = ryotCode;
									accCode = strbrancgGlCode;
									if(accCode != null)
									{
										int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
										String strAccGrpCode = Integer.toString(AccGrpCode);

										int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
										String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

										Map tempMap = new HashMap();
										tempMap.put("ACCOUNT_CODE", accCode);
										tempMap.put("AMOUNT", Amt);
										tempMap.put("JRNL_MEM", "Towards Tieup Loan Recovery");
										tempMap.put("SEASON", season);
										tempMap.put("TRANS_TYPE", TransactionType);
										tempMap.put("GL_CODE", strglCode);
										tempMap.put("TRANSACTION_CODE", transactionCode);
										
										//Added by DMurty on 03-03-2017
										tempMap.put("TRANSACTION_DATE", strCaneaccDate);
										
										Map AccMap = updateAccountsNew(tempMap);
										
										advanceLoansTablesList.add(AccMap.get("accDtls"));
										advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
										advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
										
										Map AccSmry = new HashMap();
										AccSmry.put("ACC_CODE", accCode);
										boolean brnchLoanAccCodetrue = AccountCodeList.contains(AccSmry);
										if (brnchLoanAccCodetrue == false)
										{
											AccountCodeList.add(AccSmry);
											AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
										}
										else
										{
											AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
											String straccCode = accountSummary.getAccountcode();
											if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
											{
												double runbal = accountSummary.getRunningbalance();
												String baltype = accountSummary.getBalancetype();
												String ses = accountSummary.getSeason();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_CODE", accCode);
												tmpMap.put("KEY", accCode);
												
												Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
												AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
											}
										}
										
										Map AccGrpSmry = new HashMap();
										AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
										boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
										if (advAccGrpCodetrue == false)
										{
											AccountGrpCodeList.add(AccGrpSmry);
											AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
										}
										else
										{
											AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
											int accountGrpCode = accountGroupSummary.getAccountgroupcode();
											if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
											{
												double runbal = accountGroupSummary.getRunningbalance();
												String baltype = accountGroupSummary.getBalancetype();
												String ses = accountGroupSummary.getSeason();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
												tmpMap.put("KEY", strAccGrpCode);
												
												Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
												AccountGroupMap.put(strAccGrpCode,finalAccGrpSmryMap.get(strAccGrpCode));
											}
										}
										
										Map AccSubGrpSmry = new HashMap();
										AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
										boolean brnchLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
										if(brnchLoanAccSubGrpCodetrue == false)
										{
											AccountSubGrpCodeList.add(AccSubGrpSmry);
											AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
										}
										else
										{
											AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
											int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
											int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
											String ses = accountSubGroupSummary.getSeason();
											if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
											{
												double runbal = accountSubGroupSummary.getRunningbalance();
												String baltype = accountSubGroupSummary.getBalancetype();
												
												Map tmpMap = new HashMap();
												tmpMap.put("RUN_BAL", runbal);
												tmpMap.put("AMT", Amt);
												tmpMap.put("BAL_TYPE", baltype);
												tmpMap.put("SEASON", ses);
												tmpMap.put("TRANSACTION_TYPE", TransactionType);
												tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
												tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
												tmpMap.put("KEY", strAccSubGrpCode);
												Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
												AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
											}
										}
									}
								}
								advanceLoansTablesList.add(canAccLoansDetails);
							}
						}
					    
					    if(i == 0)
						{
							List CaneAccountSmryTempList = new ArrayList();
							CaneAccountSmryTempList=caneAccountingFunctionalService.CaneAccountSmryTemp(caneAccSlno,season);
							if (CaneAccountSmryTempList != null && !CaneAccountSmryTempList.isEmpty())
							{
								Map CaneAccSmryMap = new HashMap();
								CaneAccSmryMap = (Map) CaneAccountSmryTempList.get(0);
								
								int caneacslno = (Integer) CaneAccSmryMap.get("caneacslno");
								Date dateFrom = (Date) CaneAccSmryMap.get("datefrom");
								Date dateOfCalculation = (Date) CaneAccSmryMap.get("dateofcalc");
								Date dateTo = (Date) CaneAccSmryMap.get("dateto");
								String user = (String) CaneAccSmryMap.get("login");
								String ses = (String) CaneAccSmryMap.get("season");
								
								byte acctype = (Byte) CaneAccSmryMap.get("acctype");
								
								CaneAccountSmry caneAccountSmry = new CaneAccountSmry();

								caneAccountSmry.setCaneacslno(caneacslno);
								caneAccountSmry.setDatefrom(dateFrom);
								caneAccountSmry.setDateofcalc(dateOfCalculation);
								caneAccountSmry.setDateto(dateTo);
								caneAccountSmry.setLogin(user);
								caneAccountSmry.setSeason(ses);
								caneAccountSmry.setTransactioncode(transactionCode);
								caneAccountSmry.setAcctype(acctype);
								
								advanceLoansTablesList.add(caneAccountSmry);
							
								strQry = "Delete from CaneAccountSmrytemp where season='"+season+"' and caneacslno = "+caneAccSlno;
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
						}
					    
					    List CaneValueSplitupTempList = new ArrayList();
						CaneValueSplitupTempList=caneAccountingFunctionalService.CaneValueSplitupTemp(caneAccSlno,season,ryotCode);
						if (CaneValueSplitupTempList != null && CaneValueSplitupTempList.size()>0)
						{
							Map CaneValueSplitupTempMap = new HashMap();
							CaneValueSplitupTempMap = (Map) CaneValueSplitupTempList.get(0);
							
							double ForLoans = (Double) CaneValueSplitupTempMap.get("forloans");
							String ryot = (String) CaneValueSplitupTempMap.get("ryotname");
							byte cancelproportion = (Byte) CaneValueSplitupTempMap.get("cancelproportion");
							int caneacslno = (Integer) CaneValueSplitupTempMap.get("caneacslno");
							double canesupplied = (Double) CaneValueSplitupTempMap.get("canesupplied");
							double forsbaccs = (Double) CaneValueSplitupTempMap.get("forsbacs");
							double rate = (Double) CaneValueSplitupTempMap.get("rate");
							String ryotcde = (String) CaneValueSplitupTempMap.get("ryotcode");
							String ses = (String) CaneValueSplitupTempMap.get("season");
							int slno = (Integer) CaneValueSplitupTempMap.get("slno");
							double totalvalue = (Double) CaneValueSplitupTempMap.get("totalvalue");
							String login = (String) CaneValueSplitupTempMap.get("login");
							
							CaneValueSplitup caneValueSplitup = new CaneValueSplitup();
							
							caneValueSplitup.setCancelproportion(cancelproportion);
							caneValueSplitup.setCaneacslno(caneacslno);
							caneValueSplitup.setCanesupplied(canesupplied);
							caneValueSplitup.setForloans(ForLoans);
							caneValueSplitup.setForsbacs(ryotforDeductions);
							caneValueSplitup.setRate(rate);
							caneValueSplitup.setRyotcode(ryotcde);
							caneValueSplitup.setRyotname(ryot);
							caneValueSplitup.setSeason(ses);
							caneValueSplitup.setSlno(slno);
							caneValueSplitup.setTotalvalue(totalvalue);
							caneValueSplitup.setLogin(login);
							
							advanceLoansTablesList.add(caneValueSplitup);
						
							strQry = "Delete from CaneValueSplitupTemp where season='"+season+"' and caneacslno = "+caneAccSlno+" and ryotcode='"+ryotCode+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						}
		 			}
				}
				if(supplyType==2)
				{
					//seedsupplier updation
					String ss[]=season.split("-");
					String supseason=ss[1]+"-"+(Integer.parseInt(ss[1])+1);
					List seedSupplieramtsList=caneAccountingFunctionalService.getSeedSupplieramts(supseason,ryotCode);
					if(seedSupplieramtsList!=null)
					{		
						SeedSuppliersSummary seedSuppliersSummary=(SeedSuppliersSummary)seedSupplieramtsList.get(0);
						
							double fpaid = seedSuppliersSummary.getPaidamount()+ryotsbacc;
							double fpendingamt = seedSuppliersSummary.getAdvancepayable()-fpaid;
							seedSuppliersSummary.setPaidamount(fpaid);
							if(fpendingamt>0)
							seedSuppliersSummary.setPendingamount(fpendingamt);
							else
								seedSuppliersSummary.setPendingamount(0.0);	
							advanceLoansTablesList.add(seedSuppliersSummary);						
							
					}
				}
			}
			//here needs to update or insert in tables.
			logger.info("saveAdvancesAndLoansDetails()------advanceLoansTablesList"+advanceLoansTablesList);
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries.next();
			    Object value = entry.getValue();
			    AccountSummary accountSummary = (AccountSummary) value;
			    
			    String accountCode = accountSummary.getAccountcode();
			    String sesn = accountSummary.getSeason();
			    double runnbal = accountSummary.getRunningbalance();
			    String crOrDr = accountSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSummary);
			    }
			}
			
			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries1.next();
			    Object value = entry.getValue();
			    
			    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
			    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
			    String sesn = accountGroupSummary.getSeason();
			    double runnbal = accountGroupSummary.getRunningbalance();
			    String crOrDr = accountGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
				logger.info("updateAccountsNew()------AccList"+AccList);

			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountGroupSummary);
			    }
			}
			
			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries2.next();
			    Object value = entry.getValue();
			    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
			   
			    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
			    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
			    String sesn = accountSubGroupSummary.getSeason();
			    double runnbal = accountSubGroupSummary.getRunningbalance();
			    String crOrDr = accountSubGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
				logger.info("updateAccountsNew()------tempMap"+AccList);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSubGroupSummary);
			    }
			}
	    	/*CanToFinancialAccounting canToFinancialAccounting = new CanToFinancialAccounting();
	    	canToFinancialAccounting.setTcomp(3);
	    	canToFinancialAccounting.setTdb(51);
	    	canToFinancialAccounting.setTdoc("C042");
	    	canToFinancialAccounting.setTserial(1);
	    	Date date = new Date();
	    	canToFinancialAccounting.setTdt(date);*/
	    	//canToFinancialAccounting
			
			updateFlag=commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(advanceLoansTablesList,UpdateList,AccountsList);
			
			return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	}
	//Added by DMurty on 12-11-2016
	public Map updateAccountsNew(Map tempMap)
	{
		logger.info("updateAccountsNew()------tempMap"+tempMap);
		
		Map finalMap = new HashMap();
		Map AccSmryMap = new HashMap();
		
		String AccountCode = (String) tempMap.get("ACCOUNT_CODE");
		double totalPrice = (Double) tempMap.get("AMOUNT");
		String journalMemo = (String) tempMap.get("JRNL_MEM");
		String season = (String) tempMap.get("SEASON");
		String transType = (String) tempMap.get("TRANS_TYPE");
		String glCode = (String) tempMap.get("GL_CODE");
		int transactionCode = (Integer) tempMap.get("TRANSACTION_CODE");
		int AccGrpCode = aHFuctionalService.GetAccGrpCode(AccountCode);
		String seedsupply = (String) tempMap.get("IsSeedSupplier");

		////Added by DMurty on 25-12-2016
		//tempMap1.put("TRANSACTION_DATE", caneAccountingDate); 
		String transactionDate = (String) tempMap.get("TRANSACTION_DATE");

		Map DtlsMap = new HashMap();
		
		DtlsMap.put("TRANSACTION_CODE", transactionCode);
		DtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		DtlsMap.put("LOGIN_USER", userName);
		DtlsMap.put("ACCOUNT_CODE", AccountCode);		
		DtlsMap.put("JOURNAL_MEMO", journalMemo);
		DtlsMap.put("TRANSACTION_TYPE", transType);
		DtlsMap.put("SEASON",season);
		DtlsMap.put("GL_CODE",glCode);
		DtlsMap.put("TRANSACTION_DATE",transactionDate);

		logger.info("updateAccounts()------DtlsMap"+DtlsMap);

		AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
		AccSmryMap.put("accDtls", accountDetails);

		String TransactionType = transType;//"Dr"; 
		AccountSummary accountSummary = prepareModelforAccSmry(totalPrice,AccountCode,TransactionType,season); //2
		AccSmryMap.put("accSmry", accountSummary);
		
		Map AccGrpDtlsMap = new HashMap();
		
		AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		
		AccGrpDtlsMap.put("LOGIN_USER", userName);
		AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccGrpDtlsMap.put("JOURNAL_MEMO", journalMemo);
		AccGrpDtlsMap.put("TRANSACTION_TYPE", transType);
		AccGrpDtlsMap.put("SEASON",season);
		AccGrpDtlsMap.put("TRANSACTION_DATE",transactionDate);

		logger.info("updateAccounts()------AccGrpDtlsMap"+AccGrpDtlsMap);

		AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);//3
		AccSmryMap.put("accGrpDtls", accountGroupDetails);
		
		TransactionType = transType;
		AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//4
		AccSmryMap.put("accGrpSmry", accountGroupSummary);
		

		Map AccSubGrpDtlsMap = new HashMap();		
		AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		AccSubGrpDtlsMap.put("LOGIN_USER", userName);
		
		AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccSubGrpDtlsMap.put("JOURNAL_MEMO",journalMemo);
		AccSubGrpDtlsMap.put("TRANSACTION_TYPE", transType);
		AccSubGrpDtlsMap.put("SEASON",season);
		AccSubGrpDtlsMap.put("TRANSACTION_DATE",transactionDate);
		AccSubGrpDtlsMap.put("IsSeedSupplier",seedsupply);
		
		logger.info("updateAccounts()------AccSubGrpDtlsMap"+AccSubGrpDtlsMap);

		AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);//5
		AccSmryMap.put("accSubGrpDtls", accountSubGroupDetails);
		
		TransactionType = transType;//"Dr";
		AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//6
		AccSmryMap.put("accSubGrpSmry", accountSubGroupSummary);
		
		logger.info("updateAccountsNew()------AccSmryMap"+AccSmryMap);
		
		return AccSmryMap;
	}
	
	private SBAccTransDetails prepareModelForSBAccTransDetailsNew(String season,String ryotCode,String ryotsbac,int caneAccSlno,double finalSbAcAmt) 
	{
		Integer bankCode=0;
		RyotBankDetails ryotBankDetails = null;
		List  ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
		if(ryotBankList!=null)
		{
			ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
			bankCode=ryotBankDetails.getRyotbankbranchcode();
		}
		SBAccTransDetails sBAccTransDetails=new SBAccTransDetails();
		sBAccTransDetails.setBankcode(bankCode);
		sBAccTransDetails.setRyotcode(ryotCode);
		sBAccTransDetails.setSeason(season);
		sBAccTransDetails.setTransdate(new Date());
		sBAccTransDetails.setTranstype("Dr");
		sBAccTransDetails.setAmount(finalSbAcAmt);
		
		return sBAccTransDetails;
	}
	
	//Added by DMurty on 25-10-2016
	private SBAccSmryByBank prepareModelForSBAccSmryByBankNew(String season,String ryotCode,int caneAccSlno,double finalSbAcAmt) 
	{
		SBAccSmryByBank sBAccSmryByBank=null;
		Integer bankCode=0;
		RyotBankDetails ryotBankDetails = null;
		List  ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
		if(ryotBankList!=null)
		{
			ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
			bankCode=ryotBankDetails.getRyotbankbranchcode();
		}
		
		/*double netPayable = 0.0;
		double pendingAmount = 0.0;
		double deductionAmt = 0.0;
		List DedDtlsTempList=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
		if (DedDtlsTempList != null && !DedDtlsTempList.isEmpty())
		{
			for(int k=0;k<DedDtlsTempList.size();k++)
			{
				Map DedMap = new HashMap();
				DedMap = (Map) DedDtlsTempList.get(k);
				
				double nnetPayable = (Double) DedMap.get("netpayable");
				double npendingAmount = (Double) DedMap.get("pendingamt");
				double ndeductionAmt = (Double) DedMap.get("deductionamt");

				netPayable += nnetPayable;
				pendingAmount += npendingAmount;
				deductionAmt += ndeductionAmt;
			}
		}*/
		
		List sBAccSmryByBankList=caneAccountingFunctionalService.getSBAccSmryByBank(season,bankCode);
		if(sBAccSmryByBankList!=null)
		{
			sBAccSmryByBank=(SBAccSmryByBank)sBAccSmryByBankList.get(0);
			sBAccSmryByBank.setId(sBAccSmryByBank.getId());
			sBAccSmryByBank.setAmountpayable(finalSbAcAmt+sBAccSmryByBank.getAmountpayable());
			sBAccSmryByBank.setPendingamount(finalSbAcAmt+sBAccSmryByBank.getAmountpaid());
		}
		else
		{
			sBAccSmryByBank=new SBAccSmryByBank();			
			sBAccSmryByBank.setBankcode(bankCode);
			sBAccSmryByBank.setSeason(season);
			
			sBAccSmryByBank.setAmountpaid(finalSbAcAmt);
			sBAccSmryByBank.setPendingamount(0.0);
			sBAccSmryByBank.setAmountpayable(finalSbAcAmt);
		}
		return sBAccSmryByBank;
	}
	
	//Added by DMurty on 25-10-2016
	private SBAccSmryByRyot prepareModelForSBAccSmryByRyotNew(String season,String ryotCode,int caneAccSlno,double finalSbAcAmt) 
	{
		SBAccSmryByRyot sBAccSmryByRyot=null;
		List sBAccSmryByBankList=caneAccountingFunctionalService.getSBAccSmryByRyot(season,ryotCode);
		Integer ryotSBAccNum=0;
		RyotBankDetails ryotBankDetails = null;
		/*double netPayable = 0.0;
		double pendingAmount = 0.0;
		double deductionAmt = 0.0;
		//DedDetails_temp dedDetails_temp = null;
		List DedDtlsTempList=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
		if (DedDtlsTempList != null && !DedDtlsTempList.isEmpty())
		{
			for(int k=0;k<DedDtlsTempList.size();k++)
			{
				Map DedMap = new HashMap();
				DedMap = (Map) DedDtlsTempList.get(k);
				
				double nnetPayable = (Double) DedMap.get("netpayable");
				double npendingAmount = (Double) DedMap.get("pendingamt");
				double ndeductionAmt = (Double) DedMap.get("deductionamt");

				netPayable += nnetPayable;
				pendingAmount += npendingAmount;
				deductionAmt += ndeductionAmt;
			}
		}*/
		if(sBAccSmryByBankList!=null)
		{
			sBAccSmryByRyot=(SBAccSmryByRyot)sBAccSmryByBankList.get(0);
			sBAccSmryByRyot.setId(sBAccSmryByRyot.getId());
			sBAccSmryByRyot.setAmountpayable(finalSbAcAmt+sBAccSmryByRyot.getAmountpayable());
			sBAccSmryByRyot.setPendingamount(finalSbAcAmt+sBAccSmryByRyot.getPendingamount());
		}
		else
		{
			sBAccSmryByRyot=new SBAccSmryByRyot();
			List ryotBankList=caneAccountingFunctionalService.getSBAccByRyot(ryotCode);
			if(ryotBankList!=null)
			{
				ryotBankDetails = (RyotBankDetails)ryotBankList.get(0);
				ryotSBAccNum=ryotBankDetails.getRyotbankbranchcode();
			}
			sBAccSmryByRyot.setRyotcode(ryotCode);
			sBAccSmryByRyot.setSbcode(ryotSBAccNum);
			sBAccSmryByRyot.setSeason(season);
			
			sBAccSmryByRyot.setAmountpaid(finalSbAcAmt);
			sBAccSmryByRyot.setPendingamount(0.0);
			sBAccSmryByRyot.setAmountpayable(finalSbAcAmt);
		}
		return sBAccSmryByRyot;
	}
	
	private List prepareModelForDedDetailsNew(int caneAccSlno,String season,String ryotCode) 
	{		
		List FinalList = new ArrayList();
		DedDetails_temp dedDetails_temp = null;
		
		List<DedDetails_temp> DedDetails_temps=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
		if (DedDetails_temps != null && !DedDetails_temps.isEmpty())
		{
			DedDetails dedDetails = new DedDetails();
			for (DedDetails_temp DedDetails_temp : DedDetails_temps) 
			{
				dedDetails.setCaneacslno(dedDetails_temp.getCaneacslno());
				dedDetails.setDedCode(dedDetails_temp.getDedCode());
				dedDetails.setDeduction(dedDetails_temp.getDeduction());
				dedDetails.setDeductionamt(dedDetails_temp.getDeductionamt());
				dedDetails.setExtentsize(dedDetails_temp.getExtentsize());
				dedDetails.setHarvestcontcode(dedDetails_temp.getHarvestcontcode());
				dedDetails.setId(dedDetails_temp.getId());
				dedDetails.setNetpayable(dedDetails_temp.getNetpayable());
				dedDetails.setNewpayable(dedDetails_temp.getNewpayable());
				dedDetails.setOldpendingamount(dedDetails_temp.getOldpendingamount());
				dedDetails.setPendingamt(dedDetails_temp.getPendingamt());
				dedDetails.setRyotcode(dedDetails_temp.getRyotcode());
				dedDetails.setSeason(dedDetails_temp.getSeason());
				dedDetails.setSuppliedcaneWt(dedDetails_temp.getSuppliedcaneWt());
				
				FinalList.add(dedDetails);
			}
		}
		return FinalList;
	}
	
	
	private AdvanceSummary prepareModelForUpdateAdvanceSummary(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode)
	{
		AdvanceSummary advanceSummary=null;
		List  advanceSummaryList=caneAccountingFunctionalService.getAdvanceSummary(season,ryotCode,advancesLoansForPCABean.getAdvancecode());
		if(advanceSummaryList!=null)
		{
			advanceSummary=(AdvanceSummary)advanceSummaryList.get(0);
			//Added by DMurty on 19-11-2016
			double advanceAmt = advanceSummary.getAdvanceamount();
			double totalRecAmt = advanceSummary.getRecoveredamount()+advancesLoansForPCABean.getLoanPayable();
			double pendingAmt = advanceAmt-totalRecAmt;
			/*if(pendingAmt < 0)
				pendingAmt = 0;*/
			double intrestAmt = 0;//advancesLoansForPCABean.getInterestAmount();
			if(advancesLoansForPCABean.getInterestAmount() != null)
			{
				intrestAmt = advancesLoansForPCABean.getInterestAmount();
			}
			double totalAmt = advancesLoansForPCABean.getAdvanceamount()+intrestAmt;
			
			advanceSummary.setRecoveredamount(advanceSummary.getRecoveredamount()+advancesLoansForPCABean.getLoanPayable());
			advanceSummary.setPaidamount(0.0);
			advanceSummary.setPendingpayable(0.00);
			advanceSummary.setPendingamount(pendingAmt);
			advanceSummary.setTotalamount(totalAmt);
			advanceSummary.setId(advanceSummary.getId());
		}
		
		return advanceSummary;
	}
	
	
	private AdvancePaymentDetails  prepareModelForAdvancePaymentDetails (AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode) 
	{
		AdvancePaymentDetails  advancePaymentDetails =new AdvancePaymentDetails ();
		
		List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotCode);
		Ryot ryot=ryots.get(0);
		
		advancePaymentDetails.setAdvancecode(advancesLoansForPCABean.getAdvancecode());
		advancePaymentDetails.setDateofupdate(new Date());
		advancePaymentDetails.setPaidamount(0.00);
		advancePaymentDetails.setPayableamount(advancesLoansForPCABean.getLoanPayable());
		advancePaymentDetails.setRyotcode(ryotCode);
		advancePaymentDetails.setSeason(season);
		
		advancePaymentDetails.setVillagecode(ryot.getVillagecode());
		
		
		return advancePaymentDetails;
	}
	
	private AdvancePaymentSummary  prepareModelForAdvancePaymentSummary(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode) 
	{
		AdvancePaymentSummary  advancePaymentSummary =null;
		
		List  advancePaymentSummaryList=caneAccountingFunctionalService.getAdvancePaymentSummary(season,ryotCode,advancesLoansForPCABean.getAdvancecode());
		
		if(advancePaymentSummaryList!=null)
		{
			advancePaymentSummary=(AdvancePaymentSummary)advancePaymentSummaryList.get(0);	
			advancePaymentSummary.setId(advancePaymentSummary.getId());			
			advancePaymentSummary.setPayableamount(advancePaymentSummary.getPayableamount()+advancesLoansForPCABean.getLoanPayable());
			advancePaymentSummary.setPendingamount(advancePaymentSummary.getPendingamount()+advancesLoansForPCABean.getLoanPendingAmount());
			
		}
		else
		{
			advancePaymentSummary =new AdvancePaymentSummary ();
			List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotCode);
			Ryot ryot=ryots.get(0);
			
			advancePaymentSummary.setAdvancecode(advancesLoansForPCABean.getAdvancecode());
			advancePaymentSummary.setPaidamount(0.00);
			advancePaymentSummary.setPayableamount(advancesLoansForPCABean.getLoanPayable());
			advancePaymentSummary.setPendingamount(advancesLoansForPCABean.getLoanPendingAmount());
			advancePaymentSummary.setRyotcode(ryotCode);
			advancePaymentSummary.setSeason(season);
			
			advancePaymentSummary.setVillagecode(ryot.getVillagecode());
		}
		
		return advancePaymentSummary;
	}
	
	private CanAccLoansDetails prepareModelForCanAccLoansDetails(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode,int caneAccSlno, int supplyType,Date paydate) 
	{
		CanAccLoansDetails canAccLoansDetails=new CanAccLoansDetails();	
		
		canAccLoansDetails.setAccountnum(advancesLoansForPCABean.getReferencenumber());
		double inrst = 0.0;
		if(advancesLoansForPCABean.getInterestAmount() != null)
		{
			inrst = advancesLoansForPCABean.getInterestAmount();
		}
		canAccLoansDetails.setAdvanceamount(advancesLoansForPCABean.getAdvanceamount());
		
		Date currDate = new Date();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String strcurrDate = df.format(currDate);
		currDate = DateUtils.getSqlDateFromString(advancesLoansForPCABean.getAdvancedate(),Constants.GenericDateFormat.DATE_FORMAT);
		canAccLoansDetails.setAdvdate(currDate);
		canAccLoansDetails.setAdvloan(1);
		canAccLoansDetails.setBranchcode(advancesLoansForPCABean.getBranchcode());
		canAccLoansDetails.setCaneacctslno(caneAccSlno);
		
		if(advancesLoansForPCABean.getInterestAmount() != null)
		{
			canAccLoansDetails.setInterestamount(advancesLoansForPCABean.getInterestAmount());
			canAccLoansDetails.setIntryotpayable(advancesLoansForPCABean.getInterestAmount());
			canAccLoansDetails.setFinalinterest(advancesLoansForPCABean.getInterestAmount());
		}
		else
		{
			canAccLoansDetails.setInterestamount(0.0);
			canAccLoansDetails.setIntryotpayable(0.0);
			canAccLoansDetails.setFinalinterest(0.0);
		}
		
		double intRate = 0.0;
		if(advancesLoansForPCABean.getNetRate() != null)
		{
			intRate = advancesLoansForPCABean.getNetRate();
		}
		intRate =Double.parseDouble(new DecimalFormat("##.##").format(intRate));
		canAccLoansDetails.setInterestrate(intRate);
		canAccLoansDetails.setLoanno(advancesLoansForPCABean.getLoanNo());
		canAccLoansDetails.setNetrate(advancesLoansForPCABean.getAdvanceinterest());
		
		double pendingAmt = advancesLoansForPCABean.getLoanPayable();
		pendingAmt =Double.parseDouble(new DecimalFormat("##.##").format(pendingAmt));

		
		
		String strrepaymentDate = advancesLoansForPCABean.getAdvancerepaydate();
		Date repaymentDate = DateUtils.getSqlDateFromString(strrepaymentDate,Constants.GenericDateFormat.DATE_FORMAT);
		canAccLoansDetails.setRepaymentdate(repaymentDate);
		
		canAccLoansDetails.setRyotcode(ryotCode);
		canAccLoansDetails.setSeason(season);
		if(advancesLoansForPCABean.getSubventedInterestRate() != null)
		{
			double subvIntrestRate = Double.parseDouble(advancesLoansForPCABean.getSubventedInterestRate());
			canAccLoansDetails.setSubventedrate(subvIntrestRate);
		}
		else
		{
			canAccLoansDetails.setSubventedrate(0.0);
		}
		//canAccLoansDetails.setTotalamount(advancesLoansForPCABean.getTotalamount());
		
		double delayedIntrest = 0.0;//getDelayedIntrest(caneAccSlno,ryotCode,season,advancesLoansForPCABean);
		canAccLoansDetails.setDelayedintrest(delayedIntrest);

		canAccLoansDetails.setPaymentsource(null);
		canAccLoansDetails.setIsdirect((byte) 0);
		canAccLoansDetails.setRyotdetails("NA");
		canAccLoansDetails.setMainryotcode("NA");
		
		//Modified by DMurty on 16-12-2016
		
		
		
		double totalAmt = advancesLoansForPCABean.getAdvanceamount()+inrst;
		canAccLoansDetails.setTotalamount(totalAmt);
		
		//Modified by DMurty on 16-12-2016
		double adjPrinciple = totalAmt-advancesLoansForPCABean.getLoanPayable();
		canAccLoansDetails.setAdjustedprincipleamount(adjPrinciple);
		double addInt = 0.0;
		if(canAccLoansDetails.getAdditionalint() != null)
		{
			addInt = canAccLoansDetails.getAdditionalint();
		}
		double finalPrinciple = adjPrinciple+addInt;
		canAccLoansDetails.setFinalprinciple(finalPrinciple);
		//Modified by DMurty on 17-12-2016
		canAccLoansDetails.setPaidamt(advancesLoansForPCABean.getLoanPayable());
		canAccLoansDetails.setAdditionalint(0.0);
		
		//Modified by DMurty on 14-03-2017
		if(supplyType == 0 || supplyType == 2)
		{
			canAccLoansDetails.setAdvisedamt(0.0);
			canAccLoansDetails.setPendingamt(pendingAmt);
		}
		else
		{
			canAccLoansDetails.setPaymentsource("DIRECT RYOT");
			canAccLoansDetails.setPaymentdate(paydate);
			canAccLoansDetails.setAdvisedamt(advancesLoansForPCABean.getLoanPayable());			
			canAccLoansDetails.setPendingamt(0.0);
		}		

		return canAccLoansDetails;
	}
	
	public double getDelayedIntrest(int caneAccSlno,String ryotCode,String season,AdvancesLoansForPCABean advancesLoansForPCABean)
	{
		double delayedIntrest = 0.0;
		//delayedint = delayeddate*intAmt/totaldays
		//delayeddate = repaymentdate-ryotpayabledate
		//ryotpayabledate = canesupplieddate + 14days
		//noofdays = repaydate-principledate
		//ryotpayable date
		Date datefrom = null;
		Date dateto = null;
		List DateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
		logger.info("getDelayedIntrest()------DateList"+DateList);
		if(DateList != null && DateList.size()>0)
		{
			Map DateMap = new HashMap();
			DateMap = (Map) DateList.get(0);
			datefrom = (Date) DateMap.get("datefrom");
			dateto = (Date) DateMap.get("dateto");
			
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String datefrom1 = df.format(datefrom);
			String dateto1 = df.format(dateto);

			datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
			dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
			
			//get max weighmentdate
			Date weighmentDate = null;
			List WeighmentList = caneAccountingFunctionalService.getMaxWeighmentDate(ryotCode,season,datefrom,dateto);
			logger.info("getDelayedIntrest()------WeighmentList"+WeighmentList);
			if(WeighmentList != null && WeighmentList.size()>0)
			{
				Map rcptMap = new HashMap();
				rcptMap = (Map) WeighmentList.get(0);
				weighmentDate = (Date) rcptMap.get("canereceiptenddate");
			}
			Date ryotPayDate = null;
			if(weighmentDate != null)
			{
				ryotPayDate = aHFunctionalController.addDays(weighmentDate, 14);
			}

			//int noofdayes = repaydate-principledate;
			Date principleDate = null;
			String strprincipleDate = advancesLoansForPCABean.getAdvancedate();
			principleDate = DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			Date currDate = new Date();
			String strcurrDate= new SimpleDateFormat("dd-MM-yyyy").format(currDate);
			Date curDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			long noofdays = curDate.getTime()-principleDate.getTime();
			long diffDays = noofdays / (24 * 60 * 60 * 1000);
			float daysdiff = (float) diffDays;
			daysdiff = Math.round(daysdiff);
			int totalNoOfDayes = (int) daysdiff;
			
			double intrestAmt = advancesLoansForPCABean.getInterestAmount();
			//nnodays
			long additionalDays = curDate.getTime()-ryotPayDate.getTime();
			
			if(additionalDays < 0)
			{
				return delayedIntrest;
			}
			else
			{
				additionalDays = Math.abs(additionalDays);
				additionalDays = additionalDays / (24 * 60 * 60 * 1000);
				float fadditionalDays = (float) additionalDays;
				fadditionalDays = Math.round(fadditionalDays);
				int nadditionalDays =  (int) fadditionalDays;
				logger.info("getDelayedIntrest()------nadditionalDays"+nadditionalDays);

				if(nadditionalDays >0)
				{
					delayedIntrest = (nadditionalDays*intrestAmt)/totalNoOfDayes;
					delayedIntrest = Math.round(delayedIntrest);
				}
			}
			
		}
		logger.info("getDelayedIntrest()------delayedIntrest"+delayedIntrest);
		return delayedIntrest;
	}
	
	private CanAccLoansDetailsTemp prepareModelCanAccLoansDetailsTemp(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode,int caneAccSlno) 
	{
		
		CanAccLoansDetailsTemp canAccLoansDetailsTemp=new CanAccLoansDetailsTemp();
		
		canAccLoansDetailsTemp.setAccountnum(advancesLoansForPCABean.getReferencenumber());
		double inrst = 0.0;
		if(advancesLoansForPCABean.getInterestAmount() != null)
		{
			inrst = advancesLoansForPCABean.getInterestAmount();
		}
		canAccLoansDetailsTemp.setAdvanceamount(advancesLoansForPCABean.getAdvanceamount());
		
		Date currDate = new Date();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String strcurrDate = df.format(currDate);
		currDate = DateUtils.getSqlDateFromString(advancesLoansForPCABean.getAdvancedate(),Constants.GenericDateFormat.DATE_FORMAT);
		canAccLoansDetailsTemp.setAdvdate(currDate);
		canAccLoansDetailsTemp.setAdvloan(1);
		canAccLoansDetailsTemp.setBranchcode(advancesLoansForPCABean.getBranchcode());
		canAccLoansDetailsTemp.setCaneacctslno(caneAccSlno);
		
		if(advancesLoansForPCABean.getInterestAmount() != null)
		{
			canAccLoansDetailsTemp.setInterestamount(advancesLoansForPCABean.getInterestAmount());
			canAccLoansDetailsTemp.setIntryotpayable(advancesLoansForPCABean.getInterestAmount());
			canAccLoansDetailsTemp.setFinalinterest(advancesLoansForPCABean.getInterestAmount());
		}
		else
		{
			canAccLoansDetailsTemp.setInterestamount(0.0);
			canAccLoansDetailsTemp.setIntryotpayable(0.0);
			canAccLoansDetailsTemp.setFinalinterest(0.0);
		}
		
		double intRate = 0.0;
		if(advancesLoansForPCABean.getNetRate() != null)
		{
			intRate = advancesLoansForPCABean.getNetRate();
		}
		intRate =Double.parseDouble(new DecimalFormat("##.##").format(intRate));
		canAccLoansDetailsTemp.setInterestrate(intRate);
		canAccLoansDetailsTemp.setLoanno(advancesLoansForPCABean.getLoanNo());
		canAccLoansDetailsTemp.setNetrate(advancesLoansForPCABean.getAdvanceinterest());
		
		double pendingAmt = advancesLoansForPCABean.getLoanPayable();
		pendingAmt =Double.parseDouble(new DecimalFormat("##.##").format(pendingAmt));

		canAccLoansDetailsTemp.setPendingamt(pendingAmt);
		
		String strrepaymentDate = advancesLoansForPCABean.getAdvancerepaydate();
		Date repaymentDate = DateUtils.getSqlDateFromString(strrepaymentDate,Constants.GenericDateFormat.DATE_FORMAT);
		canAccLoansDetailsTemp.setRepaymentdate(repaymentDate);
		
		canAccLoansDetailsTemp.setRyotcode(ryotCode);
		canAccLoansDetailsTemp.setSeason(season);
		if(advancesLoansForPCABean.getSubventedInterestRate() != null)
		{
			double subvIntrestRate = Double.parseDouble(advancesLoansForPCABean.getSubventedInterestRate());
			canAccLoansDetailsTemp.setSubventedrate(subvIntrestRate);
		}
		else
		{
			canAccLoansDetailsTemp.setSubventedrate(0.0);
		}
		//canAccLoansDetails.setTotalamount(advancesLoansForPCABean.getTotalamount());
		
		double delayedIntrest = 0.0;//getDelayedIntrest(caneAccSlno,ryotCode,season,advancesLoansForPCABean);
		canAccLoansDetailsTemp.setDelayedintrest(delayedIntrest);

		canAccLoansDetailsTemp.setPaymentsource(null);
		canAccLoansDetailsTemp.setIsdirect((byte) 0);
		canAccLoansDetailsTemp.setRyotdetails("NA");
		canAccLoansDetailsTemp.setMainryotcode("NA");
		
		//Modified by DMurty on 16-12-2016
		
		canAccLoansDetailsTemp.setAdditionalint(0.0);
		
		double totalAmt = advancesLoansForPCABean.getAdvanceamount()+inrst;
		canAccLoansDetailsTemp.setTotalamount(totalAmt);
		
		//Modified by DMurty on 16-12-2016
		double adjPrinciple = totalAmt-advancesLoansForPCABean.getLoanPayable();
		canAccLoansDetailsTemp.setAdjustedprincipleamount(adjPrinciple);
				
		double finalPrinciple = adjPrinciple+canAccLoansDetailsTemp.getAdditionalint();
		canAccLoansDetailsTemp.setFinalprinciple(finalPrinciple);
		//Modified by DMurty on 17-12-2016
		canAccLoansDetailsTemp.setPaidamt(advancesLoansForPCABean.getLoanPayable());
		canAccLoansDetailsTemp.setAdvisedamt(0.0);

	
		return canAccLoansDetailsTemp;
	}
	
	private LoanPaymentDetails  prepareModelForLoanPaymentDetails(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode) 
	{
		LoanPaymentDetails  loanPaymentDetails =new LoanPaymentDetails();
		
		List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotCode);
		Ryot ryot=ryots.get(0);
		
		loanPaymentDetails.setDateofupdate(new Date());
		loanPaymentDetails.setPaidamount(0.00);
		loanPaymentDetails.setPayableamount(advancesLoansForPCABean.getLoanPayable());
		loanPaymentDetails.setRyotcode(ryotCode);
		loanPaymentDetails.setSeason(season);
		loanPaymentDetails.setVillagecode(ryot.getVillagecode());
		
		return loanPaymentDetails;
	}
	
	private LoanPaymentSummary  prepareLoanPaymentSummary(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode) 
	{
		LoanPaymentSummary  loanPaymentSummary =null;
		
		List  loanPaymentSummaryList=caneAccountingFunctionalService.getLoanPaymentSummary(season,ryotCode);
		if(loanPaymentSummaryList!=null)
		{		
			loanPaymentSummary=(LoanPaymentSummary)loanPaymentSummaryList.get(0);	
			loanPaymentSummary.setId(loanPaymentSummary.getId());
			loanPaymentSummary.setPayableamount(loanPaymentSummary.getPayableamount()+advancesLoansForPCABean.getLoanPayable());
			loanPaymentSummary.setPendingamount(loanPaymentSummary.getPendingamount()+advancesLoansForPCABean.getLoanPendingAmount());
			
		}
		else
		{
			loanPaymentSummary =new LoanPaymentSummary();
			List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotCode);
			Ryot ryot=ryots.get(0);
			
			//loanPaymentSummary.setLoanbankcode(loanbankcode);
			loanPaymentSummary.setPaidamount(0.00);
			loanPaymentSummary.setPayableamount(advancesLoansForPCABean.getLoanPayable());
			loanPaymentSummary.setRyotcode(ryotCode);
			loanPaymentSummary.setPendingamount(advancesLoansForPCABean.getLoanPendingAmount());
			loanPaymentSummary.setSeason(season);		
			loanPaymentSummary.setVillagecode(ryot.getVillagecode());			
		}
		return loanPaymentSummary;
	}
	
	
	private LoanSummary prepareModelForUpdateLoanSummary(AdvancesLoansForPCABean advancesLoansForPCABean,String season,String ryotCode)
	{
		LoanSummary loanSummary=null;
		List  loanSummaryList=caneAccountingFunctionalService.getLoanSummary(season,ryotCode,advancesLoansForPCABean.getAdvancecode());
		if(loanSummaryList!=null)
		{		
			loanSummary=(LoanSummary)loanSummaryList.get(0);
			double intrestAmt = 0.0;
			if(advancesLoansForPCABean.getInterestAmount() != null)
			{
				intrestAmt = advancesLoansForPCABean.getInterestAmount();
			}
			double pndngAmt = 0.0;
			if(loanSummary.getPendingamount() != null)
			{
				pndngAmt = loanSummary.getPendingamount();
			}
			double totalAmt = pndngAmt+intrestAmt;
			
			loanSummary.setPaidamount(advancesLoansForPCABean.getLoanPayable());
			loanSummary.setPendingamount(advancesLoansForPCABean.getLoanPendingAmount());
			loanSummary.setRyotcode(loanSummary.getRyotcode());
			loanSummary.setTotalamount(totalAmt);
		}
		return loanSummary;
		
	}
	
	private LoanPaymentSummaryByBank  prepareLoanPaymentSummaryByBank(AdvancesLoansForPCABean advancesLoansForPCABean,String season) 
	{
		LoanPaymentSummaryByBank  loanPaymentSummaryByBank =new LoanPaymentSummaryByBank();
		
		List  loanPaymentSummaryByBankList=caneAccountingFunctionalService.getLoanPaymentSummaryByBank(season);
		if(loanPaymentSummaryByBankList!=null)
		{
			loanPaymentSummaryByBank=(LoanPaymentSummaryByBank)loanPaymentSummaryByBankList.get(0);	
			loanPaymentSummaryByBank.setId(loanPaymentSummaryByBank.getId());
			loanPaymentSummaryByBank.setPayableamount(loanPaymentSummaryByBank.getPayableamount()+advancesLoansForPCABean.getLoanPayable());
			loanPaymentSummaryByBank.setPendingamount(loanPaymentSummaryByBank.getPendingamount()+advancesLoansForPCABean.getLoanPendingAmount());
		}
		else
		{
			//loanPaymentSummaryByBank.setLoanbankcode(loanbankcode);
			loanPaymentSummaryByBank.setPaidamount(0.00);
			loanPaymentSummaryByBank.setPayableamount(advancesLoansForPCABean.getLoanPayable());
			loanPaymentSummaryByBank.setPendingamount(advancesLoansForPCABean.getLoanPendingAmount());
			loanPaymentSummaryByBank.setSeason(season);
		}
		return loanPaymentSummaryByBank;
	}
	
	
	//---------------------------------------------------------Update Ryot Accounts ------------------------------------------------//
	
	@RequestMapping(value = "/getAllAdvancePayableDetailsForUpdateRyotAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getAllAdvancePayableDetailsForUpdateRyotAccounts(@RequestBody JSONObject jsonData)throws Exception 
	{
		Integer villagecode=(Integer)jsonData.getInt("village");
		String caneaccdate=(String)jsonData.getString("caneaccdate");
		Date date1=DateUtils.getSqlDateFromString(caneaccdate,Constants.GenericDateFormat.DATE_FORMAT);	
				
		/*org.json.JSONObject jsonResponse = new org.json.JSONObject();
		org.json.JSONArray jsonArray = new org.json.JSONArray();
		net.sf.json.JSONObject jsonObject = null;
		List newList=new ArrayList();
		HashMap hashM=new HashMap();
		Map hm=null;*/
		
		
		List companyAdvancesList=caneAccountingFunctionalService.getAllAdvancePayableDetailsForUpdateRyotAccounts(villagecode, date1);
		
		/*Map valueMap=null;						
		Object ob=null;		
		for(int i=0;i<caneValueSplitupSummaryList.size();i++)
		{
			jsonObject = new net.sf.json.JSONObject();
			hm=new HashMap();
			hm=(Map)caneValueSplitupSummaryList.get(i);				 
			jsonObject.putAll(hm);
			jsonObject.put("caneacslno",standardDeductionsList);
			jsonArray.put(jsonObject);	
		}*/
		
		return companyAdvancesList;
	}	
	
	
	@RequestMapping(value = "/getAllLoanPayableDetailsForUpdateRyotAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getAllLoanPayableDetailsForUpdateRyotAccounts(@RequestBody JSONObject jsonData)throws Exception 
	{
		Integer villagecode=(Integer)jsonData.getInt("village");
		String caneaccdate=(String)jsonData.getString("caneaccdate");
		Date date1=DateUtils.getSqlDateFromString(caneaccdate,Constants.GenericDateFormat.DATE_FORMAT);
		
		List loansList=caneAccountingFunctionalService.getAllLoanPayableDetailsForUpdateRyotAccounts(villagecode, date1);
		
		return loansList;
	}	
	
	@RequestMapping(value = "/getAllHarvesterDetailsForUpdateRyotAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getAllHarvesterDetailsForUpdateRyotAccounts(@RequestBody JSONObject jsonData)throws Exception 
	{
		Integer villagecode=(Integer)jsonData.getInt("village");
		String caneaccdate=(String)jsonData.getString("caneaccdate");
		Date date1=DateUtils.getSqlDateFromString(caneaccdate,Constants.GenericDateFormat.DATE_FORMAT);
		
		List loansList=caneAccountingFunctionalService.getAllHarvesterPayableDetailsForUpdateRyotAccounts(villagecode, date1);
		
		return loansList;
	}
	
	@RequestMapping(value = "/getAllTransporterDetailsForUpdateRyotAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getAllTransporterDetailsForUpdateRyotAccounts(@RequestBody JSONObject jsonData)throws Exception 
	{
		Integer villagecode=(Integer)jsonData.getInt("village");
		String caneaccdate=(String)jsonData.getString("caneaccdate");
		Date date1=DateUtils.getSqlDateFromString(caneaccdate,Constants.GenericDateFormat.DATE_FORMAT);
		
		List loansList=caneAccountingFunctionalService.getAllTransporterDetailsForUpdateRyotAccounts(villagecode, date1);
		
		return loansList;
	}
	
	@RequestMapping(value = "/getAllSBAccountDetailsForUpdateRyotAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getAllSBAccountDetailsForUpdateRyotAccounts(@RequestBody JSONObject jsonData)throws Exception 
	{
		Integer villagecode=(Integer)jsonData.getInt("village");
		String caneaccdate=(String)jsonData.getString("caneaccdate");
		Date date1=DateUtils.getSqlDateFromString(caneaccdate,Constants.GenericDateFormat.DATE_FORMAT);
		
		List loansList=caneAccountingFunctionalService.getAllSBAccountDetailsForUpdateRyotAccounts(villagecode, date1);
		
		return loansList;
	}
	
	
	@RequestMapping(value = "/updateAdvancePaymentDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean updateAdvancePaymentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List entitiesList=new ArrayList();
		boolean updateFlag= false;
		DecimalFormat df = new DecimalFormat("#.##");
		try
		{
				JSONObject jsonObject=(JSONObject)jsonData.get("formData");
				
				String villageCode=(String) jsonObject.get("village");				
				String caneaccdate=(String) jsonObject.get("caneaccdate");
				String season=(String) jsonObject.get("season");	
				
	 			JSONArray gridData=(JSONArray) jsonData.get("gridData");	
	 			for(int j=0;j<gridData.size();j++)
				{	
	 				JSONObject jObject=(JSONObject)gridData.get(j);
	 				String ryotCode=(String) jObject.get("ryotcode");
	 				Integer advandecode=(Integer) jObject.get("advancecode");
	 				Long dateofupdate=(Long) jObject.get("dateofupdate");
	 				Integer payableamount=(Integer) jObject.get("payableamount");
	 				String pendingAmount=(String) jObject.get("pendingamount");
	 				Integer paidAmount=(Integer) jObject.get("paidamount");	 				
	 				 Date d = new Date(dateofupdate);  
	 				 
					List  advancePaymentSummaryList=caneAccountingFunctionalService.getAdvancePaymentSummary(season,ryotCode,advandecode);
					if(advancePaymentSummaryList!=null)
					{					
						AdvancePaymentSummary advancePaymentSummary=(AdvancePaymentSummary)advancePaymentSummaryList.get(0);	
						advancePaymentSummary.setId(advancePaymentSummary.getId());
						
						//advancePaymentSummary.setPayableamount(advancePaymentSummary.getPayableamount()- payableamount);
						//advancePaymentSummary.setPendingamount(advancePaymentSummary.getPendingamount()+ advancesLoansForPCABean.getLoanPendingAmount());
						//String paidamount=df.format(paidAmount);
						
						advancePaymentSummary.setPaidamount((double)paidAmount);
						
						AdvancePaymentDetails  advancePaymentDetails =new AdvancePaymentDetails ();						
						String payable=df.format(payableamount);
						
						advancePaymentDetails.setAdvancecode(advandecode);
						advancePaymentDetails.setDateofupdate(new Date());
						advancePaymentDetails.setPaidamount((double)paidAmount);
						advancePaymentDetails.setPayableamount(Double.parseDouble(payable));		
						advancePaymentDetails.setRyotcode(ryotCode);
						advancePaymentDetails.setSeason(season);
						advancePaymentDetails.setVillagecode(villageCode);
						
						entitiesList.add(advancePaymentSummary);
						entitiesList.add(advancePaymentDetails);
						
						//updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
						//Added by DMurty on 26-10-2016
						String transactionType = "Dr";
						//Added by DMurty on 28-10-2016
						String glCode="";
						int advCode = advandecode;
						List<CompanyAdvance> companyAdvance = aHFuctionalService.getAdvanceDateilsByAdvanceCode(advCode);
						glCode = companyAdvance.get(0).getGlCode();
						int transactionCode = commonService.GetMaxTransCode(season);
						boolean accountFlag=updateAccounts(ryotCode,(double)paidAmount,"Towards Company Advances Clearance",season,transactionType,glCode,transactionCode);
					}
					
					List  advanceSummaryList=caneAccountingFunctionalService.getAdvanceSummary(season,ryotCode,advandecode);
					if(advanceSummaryList!=null)
					{
						AdvanceSummary advanceSummary=(AdvanceSummary)advanceSummaryList.get(0);
						
						//advanceSummary.setRecoveredamount(advanceSummary.getRecoveredamount()+Double.parseDouble(paidAmount));
						advanceSummary.setPaidamount(advanceSummary.getPaidamount()+(double)paidAmount);
						advanceSummary.setId(advanceSummary.getId());
						entitiesList.add(advanceSummary);
					}
				}
				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
			 return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
		//return null;
	}

	@RequestMapping(value = "/updateLoanPaymentDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean updateLoanPaymentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List entitiesList=null;
		boolean updateFlag= false;
		DecimalFormat df = new DecimalFormat("#.##");
		try
		{
			JSONObject jsonObject=(JSONObject)jsonData.get("formData");			
			String villageCode=(String) jsonObject.get("village");				
			String caneaccdate=(String) jsonObject.get("caneaccdate");
			String season=(String) jsonObject.get("season");	
			
 			JSONArray gridData=(JSONArray) jsonData.get("gridData");	
 			
 			for(int j=0;j<gridData.size();j++)
			{	 				
 				JSONObject jObject=(JSONObject)gridData.get(j);
 				String ryotCode=(String) jObject.get("ryotcode");
 				Integer loancode=(Integer) jObject.get("loannumber");
 				Long dateofupdate=(Long) jObject.get("dateofupdate");
 				Integer payableamount=(Integer) jObject.get("payableamount");
 				String pendingAmount=(String) jObject.get("pendingamount");
 				Integer paidAmount=(Integer) jObject.get("paidamount");
			
				List  loanPaymentSummaryList=caneAccountingFunctionalService.getLoanPaymentSummary(season,ryotCode);
				if(loanPaymentSummaryList!=null)
				{
					entitiesList=new ArrayList();
					LoanPaymentSummary loanPaymentSummary=(LoanPaymentSummary)loanPaymentSummaryList.get(0);	
					loanPaymentSummary.setId(loanPaymentSummary.getId());
					//loanPaymentSummary.setPayableamount(loanPaymentSummary.getPayableamount()+advancesLoansForPCABean.getLoanPayable());
					//loanPaymentSummary.setPendingamount(loanPaymentSummary.getPendingamount()+advancesLoansForPCABean.getLoanPendingAmount());
					loanPaymentSummary.setPaidamount((double)paidAmount);
					
					LoanPaymentDetails  loanPaymentDetails =new LoanPaymentDetails();		
					String payable=df.format(payableamount);
					loanPaymentDetails.setDateofupdate(new Date());
					loanPaymentDetails.setPaidamount((double)paidAmount);
					loanPaymentDetails.setPayableamount(Double.parseDouble(payable));
					loanPaymentDetails.setRyotcode(ryotCode);
					loanPaymentDetails.setSeason(season);
					loanPaymentDetails.setVillagecode(villageCode);
				
					entitiesList.add(loanPaymentSummary);
					entitiesList.add(loanPaymentDetails);
					//Added by DMurty on 26-10-2016
					String transactionType = "Dr";
					String glCode="";
					int transactionCode = commonService.GetMaxTransCode(season);

					boolean accountFlag=updateAccounts(ryotCode,(double)paidAmount,"Towards Loan Payment",season,transactionType,glCode,transactionCode);

				}
				List  loanSummaryList=caneAccountingFunctionalService.getLoanSummary(season,ryotCode,loancode);
				if(loanSummaryList!=null)
				{		
					LoanSummary loanSummary=(LoanSummary)loanSummaryList.get(0);
					loanSummary.setPaidamount(loanSummary.getPaidamount()+(double)paidAmount);
					loanSummary.setRyotcode(loanSummary.getRyotcode());	
					entitiesList.add(loanSummary);
				}			
				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
			}		
			return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
		//return null;
	}		

	@RequestMapping(value = "/updateHarvestingContractorPaymentDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean updateHarvestingContractorPaymentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List entitiesList=null;
		boolean updateFlag= false;
		DecimalFormat df = new DecimalFormat("#.##");
		
		try
		{
				JSONObject jsonObject=(JSONObject)jsonData.get("formData");
				
				Integer villageCode=(Integer) jsonObject.get("village");				
				String caneaccdate=(String) jsonObject.get("caneaccdate");
				String season=(String) jsonObject.get("season");	
				
	 			JSONArray gridData=(JSONArray) jsonData.get("gridData");
	 			
	 			for(int j=0;j<gridData.size();j++)
				{
	 				entitiesList=new ArrayList();
	 				JSONObject jObject=(JSONObject)gridData.get(j);
	 				String ryotCode=(String) jObject.get("ryotcode");
	 				Integer harvestcontcode=(Integer) jObject.get("harvestcontcode");
	 				Long dateofupdate=(Long) jObject.get("dateofupdate");
	 				Integer payableamount=(Integer) jObject.get("payableamount");
	 				String pendingAmount=(String) jObject.get("pendingamount");
	 				Integer paidAmount=(Integer) jObject.get("paidamount");
	 				
	 				List  harvChgSmryByRyotAndContList=caneAccountingFunctionalService.getHarvChgSmryByRyotAndCont(season,ryotCode,harvestcontcode);
	 				
	 				if(harvChgSmryByRyotAndContList!=null)
	 				{
	 					HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont=(HarvChgSmryByRyotAndCont)harvChgSmryByRyotAndContList.get(0);
	 					harvChgSmryByRyotAndCont.setId(harvChgSmryByRyotAndCont.getId());
	 					harvChgSmryByRyotAndCont.setCompanypaidamount((double)paidAmount);
	 					entitiesList.add(harvChgSmryByRyotAndCont);
	 					
	 				}
	 				List  harvChgSmryBySeasonByContList=caneAccountingFunctionalService.getHarvChgSmryBySeasonByCont(season,harvestcontcode);
	 				if(harvChgSmryBySeasonByContList!=null)
	 				{
	 					HarvChgSmryBySeasonByCont harvChgSmryBySeasonByCont=(HarvChgSmryBySeasonByCont)harvChgSmryBySeasonByContList.get(0);
	 					harvChgSmryBySeasonByCont.setId(harvChgSmryBySeasonByCont.getId());
	 					harvChgSmryBySeasonByCont.setPaidAmount((double)paidAmount);
	 					entitiesList.add(harvChgSmryBySeasonByCont);
	 				}
	 				List  harvChgSmryBySeasonList=caneAccountingFunctionalService.getHarvChgSmryBySeason(season);
	 				if(harvChgSmryBySeasonList!=null)
	 				{
	 					 HarvChgSmryBySeason harvChgSmryBySeason=(HarvChgSmryBySeason)harvChgSmryBySeasonList.get(0);
	 					harvChgSmryBySeason.setId(harvChgSmryBySeason.getId());
	 					harvChgSmryBySeason.setPaidAmount((double)paidAmount);
	 					entitiesList.add(harvChgSmryBySeason);	 					 
	 				}
	 				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
				}
	 			
			
			 return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
		//return null;
	}		
	
	@RequestMapping(value = "/updateTransportContractorPaymentDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean updateTransportContractorPaymentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List entitiesList=null;
		boolean updateFlag= false;
		DecimalFormat df = new DecimalFormat("#.##");
		
		try
		{
			JSONObject jsonObject=(JSONObject)jsonData.get("formData");
			
			Integer villageCode=(Integer) jsonObject.get("village");				
			String caneaccdate=(String) jsonObject.get("caneaccdate");
			String season=(String) jsonObject.get("season");	
			
 			JSONArray gridData=(JSONArray) jsonData.get("gridData");
 			
 			for(int j=0;j<gridData.size();j++)
			{
 				entitiesList=new ArrayList();
 				JSONObject jObject=(JSONObject)gridData.get(j);
 				String ryotCode=(String) jObject.get("ryotcode");
 				Integer transportcontcode=(Integer) jObject.get("transportcontcode");
 				Long dateofupdate=(Long) jObject.get("dateofupdate");
 				Integer payableamount=(Integer) jObject.get("payableamount");
 				String pendingAmount=(String) jObject.get("pendingamount");
 				Integer paidAmount=(Integer) jObject.get("paidamount");
 				
 				List  transpChgSmryByRyotAndContList=caneAccountingFunctionalService.getTranspChgSmryByRyotAndCont(season,ryotCode,transportcontcode);
 				if(transpChgSmryByRyotAndContList!=null)
 				{
 					TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont=(TranspChgSmryByRyotAndCont)transpChgSmryByRyotAndContList.get(0);
 					
 					transpChgSmryByRyotAndCont.setId(transpChgSmryByRyotAndCont.getId());
 					transpChgSmryByRyotAndCont.setCompanypaidamount((double)paidAmount);
 					entitiesList.add(transpChgSmryByRyotAndCont);
 					
 				}
 				List  transpChgSmryBySeasonByContList=caneAccountingFunctionalService.getTranspChgSmryBySeasonByCont(season,transportcontcode);
 				if(transpChgSmryBySeasonByContList!=null)
 				{
 					TranspChgSmryBySeasonByCont transpChgSmryBySeasonByCont=(TranspChgSmryBySeasonByCont)transpChgSmryBySeasonByContList.get(0);
 					transpChgSmryBySeasonByCont.setId(transpChgSmryBySeasonByCont.getId());
 					transpChgSmryBySeasonByCont.setPaidAmount(transpChgSmryBySeasonByCont.getPaidAmount());
 					entitiesList.add(transpChgSmryBySeasonByCont);
 				}
 				List  transpChgSmryBySeasonList=caneAccountingFunctionalService.getTranspChgSmryBySeason(season);
 				if(transpChgSmryBySeasonList!=null)
 				{
 					TranspChgSmryBySeason transpChgSmryBySeason=(TranspChgSmryBySeason)transpChgSmryBySeasonList.get(0);
 					transpChgSmryBySeason.setId(transpChgSmryBySeason.getId());
 					transpChgSmryBySeason.setPaidAmount(transpChgSmryBySeason.getPaidAmount());
 					entitiesList.add(transpChgSmryBySeason); 					
 				}
 				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
			}
			return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
		//return null;
	}		
	
	@RequestMapping(value = "/updateSBPaymentDetails", method = RequestMethod.POST)
	public @ResponseBody
	Boolean updateSBPaymentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List entitiesList=null;
		boolean updateFlag= false;
		DecimalFormat df = new DecimalFormat("#.##");
		
		try
		{
			JSONObject jsonObject=(JSONObject)jsonData.get("formData");
			Integer villageCode=(Integer) jsonObject.get("village");				
			String caneaccdate=(String) jsonObject.get("caneaccdate");
			String season=(String) jsonObject.get("season");	
 			JSONArray gridData=(JSONArray) jsonData.get("gridData");
 			
 			for(int j=0;j<gridData.size();j++)
			{
 				entitiesList=new ArrayList();
				JSONObject jObject=(JSONObject)gridData.get(j);
				String ryotCode=(String) jObject.get("ryotcode");
				String sbcode=(String) jObject.get("sbcode");
				Long dateofupdate=(Long) jObject.get("dateofupdate");
				Integer payableamount=(Integer) jObject.get("payableamount");
				String pendingAmount=(String) jObject.get("pendingamount");
				Integer paidAmount=(Integer) jObject.get("paidamount");
				List  sBAccSmryByRyotList=caneAccountingFunctionalService.getSBAccSmryByRyot(season,ryotCode);
				if(sBAccSmryByRyotList!=null)
				{
					SBAccSmryByRyot sBAccSmryByRyot=(SBAccSmryByRyot)sBAccSmryByRyotList.get(0);
					sBAccSmryByRyot.setId(sBAccSmryByRyot.getId());
					sBAccSmryByRyot.setPendingamount((double)paidAmount);			
					entitiesList.add(sBAccSmryByRyot);
				}
				
				List  sBAccSmryByBankList=caneAccountingFunctionalService.getSBAccSmryByBank(season,0);
				if(sBAccSmryByBankList!=null)
				{
					SBAccSmryByBank sBAccSmryByBank=(SBAccSmryByBank)sBAccSmryByBankList.get(0);
					sBAccSmryByBank.setId(sBAccSmryByBank.getId());
					sBAccSmryByBank.setPendingamount((double)paidAmount);
					entitiesList.add(sBAccSmryByBank);
				}		
				//Added by DMurty on 26-10-2016
				String transactionType = "Dr";
				String glCode="";
				int transactionCode = commonService.GetMaxTransCode(season);
				boolean accountFlag=updateAccounts(ryotCode,(double)paidAmount,"Towards SB A/C payment",season,transactionType,glCode,transactionCode);
				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(entitiesList);
			}
	 		return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	}		
	
	// --------------------------------------------- Ryot Ledger -----------------------------------------------------------------------//
	
		@RequestMapping(value = "/getRyotLedger", method = RequestMethod.POST)
		public @ResponseBody	List getRyotLedger(@RequestBody JSONObject jsonData)throws Exception 
		{

			String season = (String) jsonData.get("season");
			String ryotcode = (String) jsonData.get("ryotcode");
			String datefrom=(String)jsonData.get("fromdate");
			String dateto=(String)jsonData.get("todate");
			Date date1=DateUtils.getSqlDateFromString(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			Date date2=DateUtils.getSqlDateFromString(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			
			List ryotLedgerList = caneAccountingFunctionalService.getRyotLedgerForSeason(season,ryotcode,date1,date2);
			double rlbal=0.0;
			for(int i=0;i<ryotLedgerList.size();i++)
			{
				Map rlMap=(Map)ryotLedgerList.get(i);
				double cr=(Double)rlMap.get("cr");
				double dr=(Double)rlMap.get("dr");
			//	double runningbalance=(Double)rlMap.get("runningbalance");
				/*if(i==0)
					runningbalance=0.0;
*/				if(cr>0)
				{
					rlbal=rlbal+cr;
					rlMap.put("runningbalance", Math.round(rlbal * 100.0) / 100.0);
				}
				else
				{
					rlbal=rlbal-dr;
					rlMap.put("runningbalance", Math.round(rlbal * 100.0) / 100.0);
				}
				
				
				Timestamp timeStamp=(Timestamp)rlMap.get("transactiondate");
				String dateString=DateUtils.getStringFromTimestamp(timeStamp,"dd-MM-yyyy");
		       // SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
		       // String dateText = df2.format(date);
		       // System.out.println(dateText);
		        rlMap.put("transactiondate", dateString);
		        //ryotLedgerList.add(rlMap);
			}
			logger.info("The Ryot Ledger"+ryotLedgerList.toString());
			return ryotLedgerList;
		}
		
		@RequestMapping(value = "/getTrialBalance", method = RequestMethod.POST)
		public @ResponseBody	List getTrialBalance(@RequestBody JSONObject jsonData)throws Exception 
		{
			String season = (String) jsonData.get("season");
			List ryotLedgerList = caneAccountingFunctionalService.getTrialBalForSeason(season);
			//"select a.*,b.ryotname from AccountSummary a,ryot b where a.accountcode=b.ryotcode order by b.ryotname";

			for(int i=0;i<ryotLedgerList.size();i++)
			{
				Map rlMap=(Map)ryotLedgerList.get(i);
				Double rbal=(Double)rlMap.get("runningbalance");
				String accCode = (String) rlMap.get("accountcode");
				String ryot = (String) rlMap.get("ryotname");
				String finalAcc = ryot+"("+accCode+")";
				DecimalFormat df = new DecimalFormat("#.##");
				String runningbalancest=df.format(rbal);
		        rlMap.put("runningbalance", runningbalancest);
		        rlMap.put("ryotname", finalAcc);
			}
				
			return ryotLedgerList;
			}
				/*---------------------------------------------------Start Update Seed Accounting----------------------------------------------------------------------*/
		@RequestMapping(value = "/getAllSeedSupplierRyotsAccounting", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String getAllSeedSupplierRyotsAccounting(@RequestBody JSONObject jsonData) throws Exception
		{
			String season=(String)jsonData.get("season");
			season=preparePreviousSeason(season);
			Integer seedActSlno=(Integer)jsonData.get("seedActSlno");
			String villageCode=(String)jsonData.get("villageCode");
			List seedRyots=caneAccountingFunctionalService.getSeedSupplierRyots(season,villageCode);
			String seedaccountingdetailslist=prepareListForSeedAccountingDetails(seedRyots,season);
			return seedaccountingdetailslist;
		}
		private String  prepareListForSeedAccountingDetails(List seedaccountingdetailsList,String prvseason) 
		{
			Map mm1=null;
			String advname=null;
			Double pamount=0.0;
			String seedsuppliercode=null;
			String ryotname=null;
			Double advprinciple=0.0;
			Double loanprinciple=0.0;
			Date advdate=null;
			Integer ss1=0;
			Integer ss2=0;
			Integer advcode=0;
			long months=0;
			Double advinterest=0.0;
			Double advanceInterestTill=0.0;
			Date newdate=new Date();
			Map mp2=null;
			Double advtotal=0.0;
			String loanaccNum=null;
			Integer branchcode=0;
			Date loanprincipledate=null;
			String lpdate=null;
			long earliertime=0;
			long latertime=0;
			long days=0;
			Double loaninterest=0.0;
			Double extentSize=0.0;
			Double totalPayable=0.0;
			Double Totalpaid=0.0;
			Integer id=0;
			List ryotList=new ArrayList();	
			net.sf.json.JSONObject jsonObject=null;
			org.json.JSONArray jsonArray = new org.json.JSONArray();
			try
			{
			if (seedaccountingdetailsList != null && !seedaccountingdetailsList.isEmpty()) 
			{
				Integer g=1;
				
			for(int i=0;i<seedaccountingdetailsList.size();i++)
			{	
				totalPayable=0.0;
				Totalpaid=0.0;
				jsonObject = new net.sf.json.JSONObject();
				Map ryListMap=new HashMap();
				List summaryList=new ArrayList();
				List advList=new ArrayList();
				mm1=(HashMap)seedaccountingdetailsList.get(i);
				//pamount=(Double)mm1.get("pendingamount");
				seedsuppliercode=(String)mm1.get("ryotcode");
				//extentSize=(Double)mm1.get("totalextentsize");
				//id=(Integer)mm1.get("id");
				ryotname=caneAccountingFunctionalService.getRyotName(seedsuppliercode);
				
				List loanprincipleList=caneAccountingFunctionalService.getRyotPrincipleAmountsList(prvseason,seedsuppliercode,"LoanPrincipleAmounts");
				List advprincipleList=caneAccountingFunctionalService.getRyotPrincipleAmountsList(prvseason,seedsuppliercode,"AdvancePrincipleAmounts");
				Map mp1=null;
				if(advprincipleList != null)
				{
				for(int j=0; j<advprincipleList.size(); j++)	
				{
				//Integer advslno=(Integer)((Map<Object,Object>)advprincipleList.get(j)).get("id");
				advcode=(Integer)((Map<Object,Object>)advprincipleList.get(j)).get("advancecode");
				advname=(String)caneAccountingFunctionalService.getAdvName(advcode);
				advprinciple=(Double)((Map<Object,Object>)advprincipleList.get(j)).get("principle");
				advdate=(Date)((Map<Object,Object>)advprincipleList.get(j)).get("principledate");
				Timestamp timeStamp=(Timestamp)((Map<Object,Object>)advprincipleList.get(j)).get("principledate");
				earliertime=advdate.getTime();
				latertime=newdate.getTime();
				days=DateUtils.dateDifference(latertime, earliertime);
				months=days/30;
				advinterest=(Double)caneAccountingFunctionalService.getInterestAdvance(advprinciple,advcode);
				if(advinterest==null)
				advinterest=0.00;
				advanceInterestTill=(advprinciple*months*advinterest)/100;
				advtotal=advprinciple+advanceInterestTill;
				mp1=new HashMap();
				mp1.put("advance", advname);
				mp1.put("advanceCode", advcode.toString());
				mp1.put("principle", advprinciple);
				mp1.put("issuingDate", DateUtils.getStringFromTimestamp(timeStamp,"dd-MM-yyyy"));
				mp1.put("repaymentDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
				mp1.put("interestRate", advinterest);
				mp1.put("interestAmount", advanceInterestTill);
				mp1.put("totalPayable", advtotal);
				mp1.put("paidAmount", advtotal);
				mp1.put("advanceFlag","0");
			
				totalPayable=totalPayable+advtotal;
				Totalpaid=Totalpaid+advtotal;
			
				advList.add(mp1);
				}
				}
				if(loanprincipleList != null)
				{
				Map mp3=null;
				for(int k=0; k<loanprincipleList.size(); k++)	
				{
				//Integer loanid=(Integer)((Map<Object,Object>)loanprincipleList.get(k)).get("id");
					
				loanaccNum=(String)((Map<Object,Object>)loanprincipleList.get(k)).get("loanaccountnumber");
				loanprinciple=(Double)((Map<Object,Object>)loanprincipleList.get(k)).get("principle");
				branchcode=(Integer)((Map<Object,Object>)loanprincipleList.get(k)).get("branchcode");
				loanprincipledate=(Date)((Map<Object,Object>)loanprincipleList.get(k)).get("principledate");
				Timestamp timeStamp1=(Timestamp)((Map<Object,Object>)loanprincipleList.get(k)).get("principledate");
				//loanprincipledate=DateUtils.getSqlDateFromString(lpdate,Constants.GenericDateFormat.DATE_FORMAT);
				earliertime=loanprincipledate.getTime();
				latertime=newdate.getTime();
				days=DateUtils.dateDifference(latertime, earliertime);
				months=days/30;
				loaninterest=(Double)caneAccountingFunctionalService.getInterestForLoans(loanprinciple,branchcode,prvseason);
				if(loaninterest==null)
					loaninterest=0.00;
				advanceInterestTill=(loanprinciple*months*loaninterest)/100;
				advtotal=loanprinciple+advanceInterestTill;
				mp3=new HashMap();
				mp3.put("advance", "TieUpLoans");
				mp3.put("advanceCode", loanaccNum);
				mp3.put("principle", loanprinciple);
				mp3.put("issuingDate", DateUtils.getStringFromTimestamp(timeStamp1,"dd-MM-yyyy"));
				mp3.put("repaymentDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
				mp3.put("interestRate", loaninterest);
				mp3.put("interestAmount", advanceInterestTill);
				mp3.put("totalPayable", advtotal);
				mp3.put("paidAmount", advtotal);
				mp3.put("advanceFlag","1");
				
				
				totalPayable=totalPayable+advtotal;
				Totalpaid=Totalpaid+advtotal;
				
				advList.add(mp3);
				}
				}
				mp2=new HashMap();
				mp2.put("seedActSlno", g);
				mp2.put("seedSupplierCode", seedsuppliercode);
				mp2.put("seedSupplierName", ryotname);
				//mp2.put("totalAmount", pamount);
				mp2.put("totalAmount",0);
				//mp2.put("payingAmount", pamount);
				mp2.put("payingAmount", 0);
				//mp2.put("extentSize", extentSize);
				mp2.put("extentSize", 0);
				mp2.put("seedRate", 0);
				mp2.put("loansAndAdvancesPayable", totalPayable);
				mp2.put("loansAndAdvancesPaid", Totalpaid);
				//mp2.put("paidToSBAC", pamount-Totalpaid);
				mp2.put("paidToSBAC", 0);
				g++;
				//summaryList.add(mp2);
				jsonObject.putAll(mp2);
				jsonObject.put("advanceAndLoans", advList);
				jsonArray.put(jsonObject);	
				
				
				
				//ryListMap.put("seedSummary", summaryList);
				//ryListMap.put("advanceAndLoans", advList);
				//ryotList.add(ryListMap);
				//ryotList.add(summaryList);
				//ryotList.add(advList);
				
			}
			logger.info("---------Advances Details----------"+jsonArray.toString());
		}
			
			return jsonArray.toString();
			}
			catch(Exception e)
			{
				 logger.info(e.getCause(), e);
				return null;
			}
		}
		private String preparePreviousSeason(String season)
		{
			Integer ss1=0;
			Integer ss2=0;
			String ss[]=season.split("-");
			ss1=Integer.parseInt(ss[0])-1;
			ss2=Integer.parseInt(ss[1])-1;
			return ss1+"-"+ss2;
			
		}
		
		//----------------------------------------saveAllSeedSupplierRyotsAccounting---------------------------------------------------------
		
		@RequestMapping(value = "/saveAllSeedSupplierRyotsAccounting", method = RequestMethod.POST)
		public @ResponseBody
		Boolean saveAllSeedSupplierRyotsAccounting(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
		{		
			List advanceLoansTablesList=new ArrayList();
			boolean updateFlag= false;	
			boolean saveFlag=false;
			JSONObject seedAccountingSummary=(JSONObject) jsonData.get("formData");
			JSONArray seedAdvLoanDetails=(JSONArray) jsonData.get("SaveSeedCalculationData");			
			
			SeedAccountingSummaryBean sseedAccountingSummaryBean = new ObjectMapper().readValue(seedAccountingSummary.toString(), SeedAccountingSummaryBean.class);
			SeedAccountingSummary seedaccsummary=prepareModelForSeedAccountingSummaryBean(sseedAccountingSummaryBean);
			advanceLoansTablesList.add(seedaccsummary);
			try
			{
				for(int i=0;i<seedAdvLoanDetails.size();i++)
				{
					boolean acflag=false;
					List ryotFormList=new ArrayList();
					JSONObject jsonObject=(JSONObject)seedAdvLoanDetails.get(i);
					SeedAccountingDetailsBean seedAccDetailsBean=new ObjectMapper().readValue(jsonObject.toString(), SeedAccountingDetailsBean.class);
					
					SeedAccountingDetails seedaccdetails=prepareModelForSeedAccountDetails(seedAccDetailsBean,sseedAccountingSummaryBean.getSeedActSlno(),sseedAccountingSummaryBean.getSeason());
					//SeedSuppliersSummary seedSuppliersSummary=prepareModelForSeedSupplierSummary(seedAccDetailsBean,sseedAccountingSummaryBean.getSeason());
					advanceLoansTablesList.add(seedaccdetails);
					//advanceLoansTablesList.add(seedSuppliersSummary);
					//Added by DMurty on 26-10-2016
					String transactionType = "Dr";
					String glCode = "";
					int transactionCode = commonService.GetMaxTransCode(seedAccDetailsBean.getSeason());

					acflag=updateAccounts(seedAccDetailsBean.getSeedSupplierCode(),seedAccDetailsBean.getPaidToSBAC(),"To SBAccount",sseedAccountingSummaryBean.getSeason(),transactionType,glCode,transactionCode);
					logger.info("----Inserting Account Data towards SBAC--"+acflag);
					String ryotName=(String) jsonObject.get("seedSupplierName");	
					//Integer caneAccSlno=(Integer) jsonObject.get("seedActSlno");				
					String ryotCode=(String) jsonObject.get("seedSupplierCode");
					Integer ttamount=(Integer) jsonObject.get("totalAmount");
					Double totalAmount=ttamount.doubleValue();
					Integer pamount=(Integer) jsonObject.get("payingAmount");	
					Double payingAmount=pamount.doubleValue();
					String season=sseedAccountingSummaryBean.getSeason();		
					Integer laPayable=(Integer) jsonObject.get("loansAndAdvancesPayable");
					Double loansAndAdvancesPayable=laPayable.doubleValue();
					Integer laPaid=(Integer) jsonObject.get("loansAndAdvancesPaid");
					Double loansAndAdvancesPaid=laPaid.doubleValue();
					Integer paidsb=(Integer) jsonObject.get("paidToSBAC");
					Double paidToSBAC=paidsb.doubleValue();
					
					
					//obj.doubleValue() 
					//season=preparePreviousSeason(season);
		 			JSONArray gridData=(JSONArray) jsonObject.get("advanceAndLoans");	

		 			for(int j=0;j<gridData.size();j++)
					{
		 			
		 				SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean = new ObjectMapper().readValue(gridData.get(j).toString(), SeedAccountingAdvDetailsBean.class);
		 				SeedAccountingAdvDetails postSeasonPaymentAdvDetails=prepareModelForSeedAccountingAdvDetailsBean(seedAccountingAdvDetailsBean,sseedAccountingSummaryBean.getSeason(),ryotCode);
		 				
		 				advanceLoansTablesList.add(postSeasonPaymentAdvDetails);
		 				if(seedAccountingAdvDetailsBean.getAdvanceFlag().equals("0"))
						{
		 					AdvancePrincipleAmounts advPrincipleAmounts=prepareModelForAdvancePrincipleAmounts(seedAccountingAdvDetailsBean,season,ryotCode);
							AdvanceSummary advanceSummary = prepareModelForUpdateAdvSummary(seedAccountingAdvDetailsBean,season,ryotCode);
							if(advPrincipleAmounts!=null)
							advanceLoansTablesList.add(advPrincipleAmounts);
							if(advanceSummary!=null)
								advanceLoansTablesList.add(advanceSummary);	
							
				
						}
						else
						{
													
							LoanPrincipleAmounts loanPrincipleAmounts=prepareModelForLoanPrincipleAmounts(seedAccountingAdvDetailsBean,season,ryotCode);				
							LoanSummary loanSummary=prepareModelForUpdateLnSummary(seedAccountingAdvDetailsBean,season,ryotCode);	
							if(loanPrincipleAmounts!=null)
							advanceLoansTablesList.add(loanPrincipleAmounts);
							if(loanSummary!=null)
							advanceLoansTablesList.add(loanSummary);
							//Added by DMurty on 26-10-2016
							transactionType = "Dr";
							glCode = "";
							acflag=updateAccounts(ryotCode,seedAccountingAdvDetailsBean.getPaidAmount(),"To TieUP Loans",sseedAccountingSummaryBean.getSeason(),transactionType,glCode,transactionCode);
							logger.info("----Inserting Account Data towards TieUP Loans--"+acflag);
						}
					}
				}
				updateFlag=caneAccountingFunctionalService.saveMultipleEntities(advanceLoansTablesList);
				 return updateFlag;
			}
			catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return updateFlag;
	        }
		}		
	/*private SeedSuppliersSummary prepareModelForSeedSupplierSummary(SeedAccountingDetailsBean seedAccountingDetailsBean,String season)
	{
		SeedSuppliersSummary seedSuppliersSummary=null;
		List  seedSuppliersSummaryList=caneAccountingFunctionalService.getSeedSuppliersSummaryDetails(season,seedAccountingDetailsBean.getSeedSupplierCode());
		if(seedSuppliersSummaryList!=null)
		{
			seedSuppliersSummary=(SeedSuppliersSummary)seedSuppliersSummaryList.get(0);
			Double paidamount=seedSuppliersSummary.getPaidamount();
			Double pendingAmount=seedSuppliersSummary.getPendingamount();
			if(paidamount==null)
				paidamount=0.0;
			if(pendingAmount==null)
				pendingAmount=0.0;
		Double	tpaidamount=seedAccountingDetailsBean.getPayingAmount()+paidamount;
		Double tpendingamount=pendingAmount-seedAccountingDetailsBean.getPaidToSBAC();
		seedSuppliersSummary.setPendingamount(tpendingamount);
		seedSuppliersSummary.setPaidamount(tpaidamount);
			
		}
		return seedSuppliersSummary;
	}*/
	
	private AdvancePrincipleAmounts prepareModelForAdvancePrincipleAmounts(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotcode)
	{
		AdvancePrincipleAmounts advancePrincipleAmounts=null;
		List  advancePrincipleAmountsList=caneAccountingFunctionalService.getadvancePrincipleAmountsDetails(season,ryotcode,Integer.parseInt(seedAccountingAdvDetailsBean.getAdvanceCode()));
	if(advancePrincipleAmountsList != null)
	{
		advancePrincipleAmounts=(AdvancePrincipleAmounts)advancePrincipleAmountsList.get(0);
		Double netpayable=seedAccountingAdvDetailsBean.getTotalPayable();
		if(netpayable==null)
			netpayable=0.0;
		Double paidamount=seedAccountingAdvDetailsBean.getPaidAmount();
		if(paidamount==null)
			paidamount=0.0;
		Double principle=netpayable-paidamount;
		advancePrincipleAmounts.setPrinciple(principle);
		advancePrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getRepaymentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		
	}
	return advancePrincipleAmounts;
	}
	private LoanPrincipleAmounts prepareModelForLoanPrincipleAmounts(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotcode)
	{
	LoanPrincipleAmounts loanPrincipleAmounts=null;
	List  loanPrincipleAmountsList=caneAccountingFunctionalService.getLoanPrincipleAmountsDetails(season,ryotcode,seedAccountingAdvDetailsBean.getAdvanceCode());
	if(loanPrincipleAmountsList != null)
	{
		loanPrincipleAmounts=(LoanPrincipleAmounts)loanPrincipleAmountsList.get(0);
		Double netpayable=seedAccountingAdvDetailsBean.getTotalPayable();
		if(netpayable==null)
			netpayable=0.0;
		Double paidamount=seedAccountingAdvDetailsBean.getPaidAmount();
		if(paidamount==null)
			paidamount=0.0;
		Double principle=netpayable-paidamount;
		loanPrincipleAmounts.setPrinciple(principle);
		loanPrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getRepaymentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		loanPrincipleAmounts.setDateofupdate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getRepaymentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		
	}
	return loanPrincipleAmounts;
	}
	private SeedAccountingDetails prepareModelForSeedAccountDetails(SeedAccountingDetailsBean seedAccBean,Integer slno,String season)
	{
		SeedAccountingDetails seedAccountDetails=new SeedAccountingDetails();
		seedAccountDetails.setLoansandadvancespaid(seedAccBean.getLoansAndAdvancesPaid());
		seedAccountDetails.setLoansandadvancespayable(seedAccBean.getLoansAndAdvancesPayable());
		seedAccountDetails.setPaidtosbac(seedAccBean.getPaidToSBAC());
		seedAccountDetails.setPayingamount(seedAccBean.getPayingAmount());
		seedAccountDetails.setSeason(season);
		seedAccountDetails.setSeedsuppliercode(seedAccBean.getSeedSupplierCode());
		seedAccountDetails.setTotalamount(seedAccBean.getTotalAmount());
		seedAccountDetails.setExtentsize(seedAccBean.getExtentSize());
		seedAccountDetails.setSeedrate(seedAccBean.getSeedRate());
		return seedAccountDetails;
		
	}
	private SeedAccountingAdvDetails prepareModelForSeedAccountingAdvDetailsBean(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotcode)
	{
		SeedAccountingAdvDetails seeAccAdv=new SeedAccountingAdvDetails();
		seeAccAdv.setAdvance(seedAccountingAdvDetailsBean.getAdvance());
		seeAccAdv.setAdvancecode(seedAccountingAdvDetailsBean.getAdvanceCode());
		seeAccAdv.setInterestamount(seedAccountingAdvDetailsBean.getInterestAmount());
		seeAccAdv.setInterestrate(seedAccountingAdvDetailsBean.getInterestRate());
		seeAccAdv.setIssuingdate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getIssuingDate(),Constants.GenericDateFormat.DATE_FORMAT));
		seeAccAdv.setPaidamount(seedAccountingAdvDetailsBean.getPaidAmount());
		seeAccAdv.setPrinciple(seedAccountingAdvDetailsBean.getPrinciple());
		seeAccAdv.setRepaymentdate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getRepaymentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		seeAccAdv.setSeason(season);
	//	seeAccAdv.setSeedactslno(seedAccountingAdvDetailsBean.getSeedActSlno());
		seeAccAdv.setSeedsuppliercode(ryotcode);
		seeAccAdv.setTotalpayable(seedAccountingAdvDetailsBean.getTotalPayable());
		return seeAccAdv;
	}
	private SeedAccountingSummary prepareModelForSeedAccountingSummaryBean(SeedAccountingSummaryBean seedAccountingSummaryBean) {
		
		SeedAccountingSummary seedAccSummary = new SeedAccountingSummary();
		seedAccSummary.setSeason(seedAccountingSummaryBean.getSeason());
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		
		seedAccSummary.setLoginid(userName);
	
		seedAccSummary.setVillagecode(seedAccountingSummaryBean.getVillageCode());
	//	seedAccSummary.setSeedactslno(seedAccountingSummaryBean.getSeedActSlno());
		seedAccSummary.setSeedaccdate(DateUtils.getSqlDateFromString(seedAccountingSummaryBean.getSeedAccDate(),Constants.GenericDateFormat.DATE_FORMAT));
		return seedAccSummary;
	}
	
	
	private AdvanceSummary prepareModelForUpdateAdvSummary(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotCode)
	{
		AdvanceSummary advanceSummary=null;
		List  advanceSummaryList=caneAccountingFunctionalService.getAdvanceSummary(season,ryotCode,Integer.parseInt(seedAccountingAdvDetailsBean.getAdvanceCode()));
		if(advanceSummaryList!=null)
		{
			advanceSummary=(AdvanceSummary)advanceSummaryList.get(0);
			Double advrecamt=advanceSummary.getRecoveredamount();
			if(advrecamt==null)
				advrecamt=0.0;
			Double recoveredAmt=advrecamt+seedAccountingAdvDetailsBean.getPaidAmount();
			advanceSummary.setRecoveredamount(recoveredAmt);
			Double intramt=advanceSummary.getInterestamount();
			if(intramt==null)
				intramt=0.0;
			Double totalIntrestAmt=intramt+seedAccountingAdvDetailsBean.getInterestAmount();
			advanceSummary.setInterestamount(totalIntrestAmt);
			Double totalAmt=advanceSummary.getAdvanceamount()+totalIntrestAmt;
			advanceSummary.setTotalamount(totalAmt);
			advanceSummary.setPendingamount(totalAmt-recoveredAmt);
			advanceSummary.setId(advanceSummary.getId());
		}
		
		return advanceSummary;
		
	}
	private LoanSummary prepareModelForUpdateLnSummary(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotCode)
	{
		LoanSummary loanSummary=null;
		List loanDetailsList=caneAccountingFunctionalService.getLoanDetailsForSeedAccounting(season,ryotCode,seedAccountingAdvDetailsBean.getAdvanceCode());
		if(loanDetailsList!=null)
		{
		Integer branchCode=(Integer)((Map<Object,Object>)loanDetailsList.get(0)).get("branchcode");
		Integer loanNum=(Integer)((Map<Object,Object>)loanDetailsList.get(0)).get("loannumber");
		
		List  loanSummaryList=caneAccountingFunctionalService.getLoanSummaryForSeeAcc(season,ryotCode,branchCode,loanNum);
		if(loanSummaryList!=null)
		{		
			loanSummary=(LoanSummary)loanSummaryList.get(0);
			Double recamt=loanSummary.getPaidamount();
			if(recamt==null)
				recamt=0.0;
			Double recoveredAmt=recamt+seedAccountingAdvDetailsBean.getPaidAmount();
			loanSummary.setPaidamount(recoveredAmt);
			Double intramt=loanSummary.getInterestamount();
			if(intramt==null)
				intramt=0.0;
			Double totalInterest=intramt+seedAccountingAdvDetailsBean.getInterestAmount();
			Double disamt=loanSummary.getDisbursedamount();
			if(disamt==null)
				disamt=0.0;
			loanSummary.setTotalamount(disamt+totalInterest);
			loanSummary.setPendingamount(totalInterest-recoveredAmt);
			loanSummary.setRyotcode(loanSummary.getRyotcode());
			
		}
		}
		return loanSummary;
		
	}
	/*private AccountDetails prepareModelForAccountDetailsOfSeed(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,boolean accflag,Double amount)
	{
		int transactionCode = commonService.GetMaxTransCode();
		String cdate=seedAccountingAdvDetailsBean.getRepaymentDate();
		String accountcode=seedAccountingAdvDetailsBean.getSeedSupplierCode();
		double finalAmtToPost=0.0;
		String JrnlMemo =null;
		if(accflag)
		{
		finalAmtToPost =seedAccountingAdvDetailsBean.getPaidAmount();
		JrnlMemo ="To Tie up loans";
		}
		else
		{
			finalAmtToPost =amount;
			JrnlMemo ="To SBAC";
		}
		Map DtlsMap = new HashMap();
		
		DtlsMap.put("TRANSACTION_CODE", transactionCode);
		DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
		DtlsMap.put("LOGIN_USER", "makella");
		DtlsMap.put("ACCOUNT_CODE", accountcode);
		DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
		DtlsMap.put("TRANSACTION_TYPE", "Dr");
		AccountDetails accountDetails =  ahfController.prepareModelForAccountDetails(DtlsMap);
	}
	*/
	
	//--------------------------------end saveUpdateSeedAccounting---------------------------------------------
	
	/*-----------------------------------------------------End Update Seed Accounting--------------------------------------------------------------------*/
	
	/*---------------------------------------------------- Start Accrued and Due Post Season Calculation-------------------------------------------------*/
	@RequestMapping(value = "/getAllAccruedAndDueRyotsAccounting", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody String getAllAccruedAndDueRyotsAccounting(@RequestBody JSONObject jsonData) throws Exception
	{
		String season=(String)jsonData.get("season");
		Integer seedActSlno=(Integer)jsonData.get("seedActSlno");
		String villageCode=(String)jsonData.get("villageCode");
		List dueRyots=caneAccountingFunctionalService.getDueCaneSupplierRyots(season,villageCode);
		String caneDueAccountingDetailsList=prepareListForDueRyotAccountingDetails(dueRyots,season);
		return caneDueAccountingDetailsList;
	}
	private String  prepareListForDueRyotAccountingDetails(List caneDueAccountingDetailsList,String season) 
	{
		Map mp2=null;
		Map mm1=null;
		String advname=null;
		Double pamount=0.0;
		Double qtysupplied=0.0;
		String canesuppliercode=null;
		String ryotname=null;
		Double advprinciple=0.0;
		Double loanprinciple=0.0;
		Date advdate=null;
		Integer ss1=0;
		Integer ss2=0;
		String prvseason=null;
		Integer advcode=0;
		long months=0;
		Double advinterest=0.0;
		Double advanceInterestTill=0.0;
		Date newdate=new Date();
		Double advtotal=0.0;
		Double totalPayable=0.0;
		Double Totalpaid=0.0;
		String loanaccNum=null;
		Integer branchcode=0;
		Date loanprincipledate=null;
		String lpdate=null;
		long earliertime=0;
		long latertime=0;
		long days=0;
		Double loaninterest=0.0;
		Double extentSize=0.0;
		List ryotList=new ArrayList();	
		net.sf.json.JSONObject jsonObject=null;
		org.json.JSONArray jsonArray = new org.json.JSONArray();
		try
		{
		if (caneDueAccountingDetailsList != null && !caneDueAccountingDetailsList.isEmpty()) 
		{
			Integer g=1;
		for(int i=0;i<caneDueAccountingDetailsList.size();i++)
		{
			totalPayable=0.0;
			Totalpaid=0.0;
			jsonObject = new net.sf.json.JSONObject();
			Map ryListMap=new HashMap();
			List summaryList=new ArrayList();
			List advList=new ArrayList();
			mm1=(HashMap)caneDueAccountingDetailsList.get(i);
			pamount=(Double)mm1.get("dueamount");
			canesuppliercode=(String)mm1.get("ryotcode");
			extentSize=caneAccountingFunctionalService.getExtentSizeForRyot(canesuppliercode);
			ryotname=caneAccountingFunctionalService.getRyotName(canesuppliercode);
			List loanprincipleList=caneAccountingFunctionalService.getRyotPrincipleAmountsList(season,canesuppliercode,"LoanPrincipleAmounts");
			List advprincipleList=caneAccountingFunctionalService.getRyotPrincipleAmountsList(season,canesuppliercode,"AdvancePrincipleAmounts");
			Map mp1=null;
			if(advprincipleList != null)
			{
			for(int j=0; j<advprincipleList.size(); j++)	
			{
			advcode=(Integer)((Map<Object,Object>)advprincipleList.get(j)).get("advancecode");
			advname=(String)caneAccountingFunctionalService.getAdvName(advcode);
			advprinciple=(Double)((Map<Object,Object>)advprincipleList.get(j)).get("principle");
			advdate=(Date)((Map<Object,Object>)advprincipleList.get(j)).get("principledate");
			Timestamp timeStamp=(Timestamp)((Map<Object,Object>)advprincipleList.get(j)).get("principledate");
			earliertime=advdate.getTime();
			latertime=newdate.getTime();
			days=DateUtils.dateDifference(latertime, earliertime);
			months=days/30;
			advinterest=(Double)caneAccountingFunctionalService.getInterestAdvance(advprinciple,advcode);
			if(advinterest==null)
			advinterest=0.00;
			advanceInterestTill=(advprinciple*months*advinterest)/100;
			advtotal=advprinciple+advanceInterestTill;
			mp1=new HashMap();
			mp1.put("advance", advname);
			mp1.put("advanceCode", advcode);
			mp1.put("principle", advprinciple);
			mp1.put("issuingDate", DateUtils.getStringFromTimestamp(timeStamp,"dd-MM-yyyy"));
			mp1.put("repaymentDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
			mp1.put("interestRate", advinterest);
			mp1.put("interestAmount", advanceInterestTill);
			mp1.put("totalPayable", advtotal);
			mp1.put("paidAmount", advtotal);
			mp1.put("advanceFlag","0");
			advList.add(mp1);
			}
			}
			if(loanprincipleList != null)
			{
			Map mp3=null;
			for(int k=0; k<loanprincipleList.size(); k++)	
			{
			loanaccNum=(String)((Map<Object,Object>)loanprincipleList.get(k)).get("loanaccountnumber");
			loanprinciple=(Double)((Map<Object,Object>)loanprincipleList.get(k)).get("principle");
			branchcode=(Integer)((Map<Object,Object>)loanprincipleList.get(k)).get("branchcode");
			loanprincipledate=(Date)((Map<Object,Object>)loanprincipleList.get(k)).get("principledate");
			Timestamp timeStamp1=(Timestamp)((Map<Object,Object>)loanprincipleList.get(k)).get("principledate");
			//loanprincipledate=DateUtils.getSqlDateFromString(lpdate,Constants.GenericDateFormat.DATE_FORMAT);
			earliertime=loanprincipledate.getTime();
			latertime=newdate.getTime();
			days=DateUtils.dateDifference(latertime, earliertime);
			months=days/30;
			loaninterest=(Double)caneAccountingFunctionalService.getInterestForLoans(loanprinciple,branchcode,season);
			if(loaninterest==null)
			loaninterest=0.00;
			advanceInterestTill=(loanprinciple*months*loaninterest)/100;
			advtotal=loanprinciple+advanceInterestTill;
			mp3=new HashMap();
			mp3.put("advance", "TieUpLoans");
			mp3.put("advanceCode", loanaccNum);
			mp3.put("principle", loanprinciple);
			mp3.put("issuingDate", DateUtils.getStringFromTimestamp(timeStamp1,"dd-MM-yyyy"));
			mp3.put("repaymentDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
			mp3.put("interestRate", loaninterest);
			mp3.put("interestAmount", advanceInterestTill);
			mp3.put("totalPayable", advtotal);
			mp3.put("paidAmount", advtotal);
			mp3.put("advanceFlag","1");
			totalPayable=totalPayable+advtotal;
			Totalpaid=Totalpaid+advtotal;
			advList.add(mp3);
			}
			}
			mp2=new HashMap();
			mp2.put("seedActSlno", g);
			mp2.put("seedSupplierCode", canesuppliercode);
			mp2.put("seedSupplierName", ryotname);
			mp2.put("totalAmount", pamount);
			mp2.put("payingAmount", pamount);
			mp2.put("extentSize", extentSize);
			mp2.put("loansAndAdvancesPayable", totalPayable);
			mp2.put("loansAndAdvancesPaid", Totalpaid);
			mp2.put("paidToSBAC", pamount-Totalpaid);
			g++;
			jsonObject.putAll(mp2);
			jsonObject.put("advanceAndLoans", advList);
			jsonArray.put(jsonObject);	

		}
		logger.info("---------Advances Details----------"+jsonArray.toString());
	}
		
		return jsonArray.toString();
		}
		catch(Exception e)
		{
			 logger.info(e.getCause(), e);
			return null;
		}
	}
	
	
	

	@RequestMapping(value = "/saveAllAccruedAndDueRyotsAccounting", method = RequestMethod.POST)
	public @ResponseBody
	Boolean saveAllAccruedAndDueRyotsAccounting(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{		
		List advanceLoansTablesList=new ArrayList();
		boolean updateFlag= false;	
		boolean saveFlag=false;
		JSONObject seedAccountingSummary=(JSONObject) jsonData.get("formData");
		JSONArray seedAdvLoanDetails=(JSONArray) jsonData.get("SaveSeedCalculationData");			
		
		SeedAccountingSummaryBean sseedAccountingSummaryBean = new ObjectMapper().readValue(seedAccountingSummary.toString(), SeedAccountingSummaryBean.class);
		PostSeasonPaymentSummary postSeasonPaymentSummary=prepareModelForPostSeasonPaymentSummary(sseedAccountingSummaryBean);
		advanceLoansTablesList.add(postSeasonPaymentSummary);
		try
		{
			for(int i=0;i<seedAdvLoanDetails.size();i++)
			{
				boolean acflag=false;
				List ryotFormList=new ArrayList();
				JSONObject jsonObject=(JSONObject)seedAdvLoanDetails.get(i);
				SeedAccountingDetailsBean seedAccDetailsBean=new ObjectMapper().readValue(jsonObject.toString(), SeedAccountingDetailsBean.class);
				
				PostSeasonPaymentDetails postSeasonPaymentDetails=prepareModelForPostSeasonPaymentDetails(seedAccDetailsBean,sseedAccountingSummaryBean.getSeason());
				AccruedAndDueCaneValueSummary accruedAndDueCaneValueSummary=prepareModelForAccruedAndDueCaneValueSummary(seedAccDetailsBean,sseedAccountingSummaryBean.getSeason(),sseedAccountingSummaryBean.getVillageCode());
				advanceLoansTablesList.add(postSeasonPaymentDetails);
				advanceLoansTablesList.add(accruedAndDueCaneValueSummary);
				//Added by DMurty on 26-10-2016
				String transactionType = "Dr";
				int transactionCode = commonService.GetMaxTransCode(seedAccDetailsBean.getSeason());
				String glCode = "";
				acflag=updateAccounts(seedAccDetailsBean.getSeedSupplierCode(),seedAccDetailsBean.getPaidToSBAC(),"To SBAccount",sseedAccountingSummaryBean.getSeason(),transactionType,glCode,transactionCode);
				logger.info("----Inserting Account Data towards SBAC--"+acflag);
				String ryotName=(String) jsonObject.get("seedSupplierName");	
				//Integer caneAccSlno=(Integer) jsonObject.get("seedActSlno");				
				String ryotCode=(String) jsonObject.get("seedSupplierCode");
				Integer ttamount=(Integer) jsonObject.get("totalAmount");
				Double totalAmount=ttamount.doubleValue();
				Integer pamount=(Integer) jsonObject.get("payingAmount");	
				Double payingAmount=pamount.doubleValue();
				String season=sseedAccountingSummaryBean.getSeason();		
				Integer laPayable=(Integer) jsonObject.get("loansAndAdvancesPayable");
				Double loansAndAdvancesPayable=laPayable.doubleValue();
				Integer laPaid=(Integer) jsonObject.get("loansAndAdvancesPaid");
				Double loansAndAdvancesPaid=laPaid.doubleValue();
				Integer paidsb=(Integer) jsonObject.get("paidToSBAC");
				Double paidToSBAC=paidsb.doubleValue();
				
				season=preparePreviousSeason(season);
	 			JSONArray gridData=(JSONArray) jsonObject.get("advanceAndLoans");	

	 			for(int j=0;j<gridData.size();j++)
				{
	 				SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean = new ObjectMapper().readValue(gridData.get(j).toString(), SeedAccountingAdvDetailsBean.class);
	 				PostSeasonPaymentAdvDetails postSeasonPaymentAdvDetails=prepareModelForPostSeasonPaymentAdvDetails(seedAccountingAdvDetailsBean,sseedAccountingSummaryBean.getSeason(),ryotCode);
	 				advanceLoansTablesList.add(postSeasonPaymentAdvDetails);
	 				if(seedAccountingAdvDetailsBean.getAdvanceFlag().equals("0"))
					{
	 					AdvancePrincipleAmounts advPrincipleAmounts=prepareModelForAdvancePrincipleAmounts(seedAccountingAdvDetailsBean,season,ryotCode);
						AdvanceSummary advanceSummary = prepareModelForUpdateAdvSummary(seedAccountingAdvDetailsBean,season,ryotCode);
						if(advPrincipleAmounts!=null)
						advanceLoansTablesList.add(advPrincipleAmounts);
						if(advanceSummary!=null)
							advanceLoansTablesList.add(advanceSummary);	
					}
					else
					{
						LoanPrincipleAmounts loanPrincipleAmounts=prepareModelForLoanPrincipleAmounts(seedAccountingAdvDetailsBean,season,ryotCode);				
						LoanSummary loanSummary=prepareModelForUpdateLnSummary(seedAccountingAdvDetailsBean,season,ryotCode);	
						if(loanPrincipleAmounts!=null)
						advanceLoansTablesList.add(loanPrincipleAmounts);
						if(loanSummary!=null)
						advanceLoansTablesList.add(loanSummary);
						//Added by DMurty on 26-10-2016
						transactionType = "Dr";
						glCode = "";
						acflag=updateAccounts(ryotCode,seedAccountingAdvDetailsBean.getPaidAmount(),"To TieUP Loans",season,transactionType,glCode,transactionCode);
						logger.info("----Inserting Account Data towards TieUP Loans--"+acflag);
					}
				}
			}
			updateFlag=caneAccountingFunctionalService.saveMultipleEntities(advanceLoansTablesList);
			 return updateFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	}	
	
	private PostSeasonPaymentSummary prepareModelForPostSeasonPaymentSummary(SeedAccountingSummaryBean seedAccountingSummaryBean) 
	{
		PostSeasonPaymentSummary pSeasonPmtSummary = new PostSeasonPaymentSummary();
		pSeasonPmtSummary.setSeason(seedAccountingSummaryBean.getSeason());
		//added by sahadeva 28/10/2016
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		pSeasonPmtSummary.setLoginid(userName);
		
		pSeasonPmtSummary.setVillagecode(seedAccountingSummaryBean.getVillageCode());
		pSeasonPmtSummary.setPostseasondate(DateUtils.getSqlDateFromString(seedAccountingSummaryBean.getSeedAccDate(),Constants.GenericDateFormat.DATE_FORMAT));
		return pSeasonPmtSummary;
	}	

	private PostSeasonPaymentDetails prepareModelForPostSeasonPaymentDetails(SeedAccountingDetailsBean seedAccBean,String season)
	{
		PostSeasonPaymentDetails ptSeasonPmtDetails=new PostSeasonPaymentDetails();
		ptSeasonPmtDetails.setLoansandadvancespaid(seedAccBean.getLoansAndAdvancesPaid());
		ptSeasonPmtDetails.setLoansandadvancespayable(seedAccBean.getLoansAndAdvancesPayable());
		ptSeasonPmtDetails.setPaidtosbac(seedAccBean.getPaidToSBAC());
		ptSeasonPmtDetails.setPayingamount(seedAccBean.getPayingAmount());
		ptSeasonPmtDetails.setSeason(season);
		ptSeasonPmtDetails.setCanesuppliercode(seedAccBean.getSeedSupplierCode());
		ptSeasonPmtDetails.setTotalamount(seedAccBean.getTotalAmount());
		ptSeasonPmtDetails.setExtentsize(seedAccBean.getExtentSize());
		return ptSeasonPmtDetails;
		
	}

	private PostSeasonPaymentAdvDetails prepareModelForPostSeasonPaymentAdvDetails(SeedAccountingAdvDetailsBean seedAccountingAdvDetailsBean,String season,String ryotcode)
	{
		PostSeasonPaymentAdvDetails pSeasonPmtAdvDetails=new PostSeasonPaymentAdvDetails();
		pSeasonPmtAdvDetails.setAdvance(seedAccountingAdvDetailsBean.getAdvance());
		pSeasonPmtAdvDetails.setAdvancecode(seedAccountingAdvDetailsBean.getAdvanceCode());
		pSeasonPmtAdvDetails.setInterestamount(seedAccountingAdvDetailsBean.getInterestAmount());
		pSeasonPmtAdvDetails.setInterestrate(seedAccountingAdvDetailsBean.getInterestRate());
		pSeasonPmtAdvDetails.setIssuingdate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getIssuingDate(),Constants.GenericDateFormat.DATE_FORMAT));
		pSeasonPmtAdvDetails.setPaidamount(seedAccountingAdvDetailsBean.getPaidAmount());
		pSeasonPmtAdvDetails.setPrinciple(seedAccountingAdvDetailsBean.getPrinciple());
		pSeasonPmtAdvDetails.setRepaymentdate(DateUtils.getSqlDateFromString(seedAccountingAdvDetailsBean.getRepaymentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		pSeasonPmtAdvDetails.setSeason(season);
	//	seeAccAdv.setSeedactslno(seedAccountingAdvDetailsBean.getSeedActSlno());
		pSeasonPmtAdvDetails.setCanesuppliercode(ryotcode);
		pSeasonPmtAdvDetails.setTotalpayable(seedAccountingAdvDetailsBean.getTotalPayable());
		return pSeasonPmtAdvDetails;
	}
	private AccruedAndDueCaneValueSummary prepareModelForAccruedAndDueCaneValueSummary(SeedAccountingDetailsBean seedAccDetailsBean,String season,String villageCode)
	{
		AccruedAndDueCaneValueSummary accAndDueCaneValueSummary=null;
		List  accAndDueCaneValueSummaryList=caneAccountingFunctionalService.getAccAndDueCaneValueSummaryDetailsForUpdate(season,seedAccDetailsBean.getSeedSupplierCode(),villageCode);
		if(accAndDueCaneValueSummaryList != null)
		{
			accAndDueCaneValueSummary=(AccruedAndDueCaneValueSummary)accAndDueCaneValueSummaryList.get(0);
			Double paidamount=accAndDueCaneValueSummary.getPaidamount();
			Double pendingAmount=accAndDueCaneValueSummary.getDueamount();
			if(paidamount==null)
				paidamount=0.0;
			if(pendingAmount==null)
				pendingAmount=0.0;
			Double	tpaidamount=seedAccDetailsBean.getPayingAmount()+paidamount;
			Double tpendingamount=pendingAmount-seedAccDetailsBean.getPaidToSBAC();
			accAndDueCaneValueSummary.setPaidamount(tpaidamount);
			accAndDueCaneValueSummary.setDueamount(tpendingamount);
		}
		return accAndDueCaneValueSummary;
	}	
	
	
	/*-----------------------------------------------------------End Accrued and Due Post Season Calculation----------------------------------------------------*/
	
	@RequestMapping(value = "/getPurchaseTaxTransfer", method = RequestMethod.POST)
	public @ResponseBody boolean getPurchaseTaxTransfer(@RequestBody JSONObject jsonData)throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList=new ArrayList();
		List AccountsList=new ArrayList();
		
		String pTaxx = (String) jsonData.get("pTax");
		int pTax=Integer.valueOf(pTaxx);
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		String strQry = null;
		Object Qryobj = null;
		
		Double icpAmount=0.0;
		String rtoyCode=null;
		String season=null;
		try
		{
			if(pTax == 0)
			{
				List ICPDetailsByRyotList = caneAccountingFunctionalService.getICPDetails();
				for(int i=0;i<ICPDetailsByRyotList.size();i++)
				{
					Map rlMap=(Map)ICPDetailsByRyotList.get(i);
					
					rtoyCode=(String)rlMap.get("ryotcode");
					season=(String)rlMap.get("season");
					icpAmount=(Double)rlMap.get("icpamount");
					int accountgpCode=0;
					int accountSubgpCode=0;
					List AccMasterList = caneAccountingFunctionalService.getAccMasterData(rtoyCode);
					if(AccMasterList !=null && AccMasterList.size()>0) 
					{
						Map AccMasterMap=(Map)AccMasterList.get(0);
						accountgpCode=(Integer)AccMasterMap.get("accountgroupcode");
						accountSubgpCode=(Integer)AccMasterMap.get("accountsubgroupcode");
					}
					int AccGrpCode = accountgpCode;
					String strAccGrpCode = Integer.toString(AccGrpCode);
					String strAccSubGrpCode = Integer.toString(accountSubgpCode);
	
					int TransactionCode = commonService.GetMaxTransCode(season);
					String AccountCode = rtoyCode;
					if (AccountCode != null)
					{
						Double Amt = icpAmount;
						String loginuser = loginController.getLoggedInUserName();
						
						String glCode="CANEPURTAX";
						Map DtlsMap = new HashMap();
		
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						DtlsMap.put("TRANSACTION_TYPE", "Cr");
						DtlsMap.put("GL_CODE", glCode);
						DtlsMap.put("SEASON", season);
						Date cdate=new Date();
						DtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
		
						String TransactionType = "Cr";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,AccountCode, TransactionType,season);
								
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();
		
						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						AccGrpDtlsMap.put("SEASON", season);
						AccGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);
		
						TransactionType = "Cr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						// Insert Rec in AccSubGrpDtls
		
						Map AccSubGrpDtlsMap = new HashMap();
		
						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						AccSubGrpDtlsMap.put("SEASON", season);
						AccSubGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);
		
						TransactionType = "Cr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", accountSubgpCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && accountSubgpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", accountSubgpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
					
					AccountCode = "CANEPURTAX";
					AccMasterList = caneAccountingFunctionalService.getAccMasterData(AccountCode);
					if(AccMasterList !=null && AccMasterList.size()>0) 
					{
						Map AccMasterMap=(Map)AccMasterList.get(0);
						accountgpCode=(Integer)AccMasterMap.get("accountgroupcode");
						accountSubgpCode=(Integer)AccMasterMap.get("accountsubgroupcode");
					}
					AccGrpCode = accountgpCode;
					strAccGrpCode = Integer.toString(AccGrpCode);
					strAccSubGrpCode = Integer.toString(accountSubgpCode);
					if(AccountCode != null)
					{
						Double Amt = icpAmount;
						String loginuser = loginController.getLoggedInUserName();
						
						String glCode="CANEPURTAX";
						Map DtlsMap = new HashMap();
		
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						DtlsMap.put("GL_CODE", rtoyCode);
						DtlsMap.put("SEASON", season);
						Date cdate=new Date();
						DtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
		
						String TransactionType = "Dr";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,AccountCode, TransactionType,season);
								
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();
		
						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccGrpDtlsMap.put("SEASON", season);
						AccGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);
		
						TransactionType = "Dr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						// Insert Rec in AccSubGrpDtls
		
						Map AccSubGrpDtlsMap = new HashMap();
		
						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccSubGrpDtlsMap.put("SEASON", season);
						AccSubGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);
		
						TransactionType = "Dr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", accountSubgpCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && accountSubgpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", accountSubgpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
			}
			else
			{
	
				List ICPDetailsByRyotList = caneAccountingFunctionalService.getICPDetails();
				for(int i=0;i<ICPDetailsByRyotList.size();i++)
				{
					Map rlMap=(Map)ICPDetailsByRyotList.get(i);
					
					rtoyCode=(String)rlMap.get("ryotcode");
					season=(String)rlMap.get("season");
					icpAmount=(Double)rlMap.get("icpamount");
					int TransactionCode = commonService.GetMaxTransCode(season);
					
					String AccountCode = "SRCFORSBPAY";
					int accountgpCode=0;
					int accountSubgpCode=0;
					List AccMasterList = caneAccountingFunctionalService.getAccMasterData(rtoyCode);
					if(AccMasterList !=null && AccMasterList.size()>0) 
					{
						Map AccMasterMap=(Map)AccMasterList.get(0);
						accountgpCode=(Integer)AccMasterMap.get("accountgroupcode");
						accountSubgpCode=(Integer)AccMasterMap.get("accountsubgroupcode");
					}
					int AccGrpCode = accountgpCode;
					String strAccGrpCode = Integer.toString(AccGrpCode);
					String strAccSubGrpCode = Integer.toString(accountSubgpCode);
					
					if (AccountCode != null)
					{
						Double Amt = icpAmount;
						String loginuser = loginController.getLoggedInUserName();
						
						String glCode="CANEPURTAX";
						Map DtlsMap = new HashMap();
		
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						DtlsMap.put("TRANSACTION_TYPE", "Cr");
						DtlsMap.put("GL_CODE", glCode);
						DtlsMap.put("SEASON", season);
						Date cdate=new Date();
						DtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
		
						String TransactionType = "Cr";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,AccountCode, TransactionType,season);
								
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();
		
						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						AccGrpDtlsMap.put("SEASON", season);
						AccGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);
		
						TransactionType = "Cr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						// Insert Rec in AccSubGrpDtls
		
						Map AccSubGrpDtlsMap = new HashMap();
		
						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						AccSubGrpDtlsMap.put("SEASON", season);
						AccSubGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);
		
						TransactionType = "Cr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", accountSubgpCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && accountSubgpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", accountSubgpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
					
					
					
					AccountCode = "CANEPURTAX";
					AccMasterList = caneAccountingFunctionalService.getAccMasterData(rtoyCode);
					if(AccMasterList !=null && AccMasterList.size()>0) 
					{
						Map AccMasterMap=(Map)AccMasterList.get(0);
						accountgpCode=(Integer)AccMasterMap.get("accountgroupcode");
						accountSubgpCode=(Integer)AccMasterMap.get("accountsubgroupcode");
					}
					AccGrpCode = accountgpCode;
					strAccGrpCode = Integer.toString(AccGrpCode);
					strAccSubGrpCode = Integer.toString(accountSubgpCode);
					if(AccountCode != null)
					{
						Double Amt = icpAmount;
						String loginuser = loginController.getLoggedInUserName();
						
						String glCode="SRCFORSBPAY";
						Map DtlsMap = new HashMap();
		
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						DtlsMap.put("GL_CODE", rtoyCode);
						DtlsMap.put("SEASON", season);
						Date cdate=new Date();
						DtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
		
						String TransactionType = "Dr";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,AccountCode, TransactionType,season);
								
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();
		
						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccGrpDtlsMap.put("SEASON", season);
						AccGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);
		
						TransactionType = "Dr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						// Insert Rec in AccSubGrpDtls
		
						Map AccSubGrpDtlsMap = new HashMap();
		
						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Cane Purchase Tax");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccSubGrpDtlsMap.put("SEASON", season);
						AccSubGrpDtlsMap.put("TRANSACTION_DATE", cdate);
		
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);
		
						TransactionType = "Dr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", accountSubgpCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && accountSubgpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", accountSubgpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
			}
			
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries.next();
			    Object value = entry.getValue();
			    AccountSummary accountSummary = (AccountSummary) value;
			    
			    String accountCode = accountSummary.getAccountcode();
			    String sesn = accountSummary.getSeason();
			    double runnbal = accountSummary.getRunningbalance();
			    String crOrDr = accountSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSummary);
			    }
			}
			
			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries1.next();
			    Object value = entry.getValue();
			    
			    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
			    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
			    String sesn = accountGroupSummary.getSeason();
			    double runnbal = accountGroupSummary.getRunningbalance();
			    String crOrDr = accountGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountGroupSummary);
			    }
			}
			
			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries2.next();
			    Object value = entry.getValue();
			    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
			   
			    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
			    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
			    String sesn = accountSubGroupSummary.getSeason();
			    double runnbal = accountSubGroupSummary.getRunningbalance();
			    String crOrDr = accountSubGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSubGroupSummary);
			    }
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}


@RequestMapping(value = "/getCaneAccountingPeriods", method = RequestMethod.POST, headers = { "Content-type=application/json" })
public @ResponseBody
JSONArray getCaneAccountingPeriods(@RequestBody String season) throws Exception
{
	/*org.json.JSONObject response = new org.json.JSONObject();
	String season=(String)jsonData.get("season");*/
	JSONArray jArray = new JSONArray();
	JSONObject jsonObj =null;
	String strQuery=null;
	try 
	{
		List dropdownList= caneAccountingFunctionalService.getCaneAccountingPeriods(season);
		if (dropdownList != null && dropdownList.size() > 0)
		{
			for (int i = 0;i<dropdownList.size();i++)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList.get(i);
				int caneAccSlNo = (Integer) tempMap.get("caneacslno");
				Date datefrom = (Date) tempMap.get("datefrom");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strdatefrom = df.format(datefrom);
				Date dateto = (Date) tempMap.get("dateto");
				String strdateto = df.format(dateto);
				String finalDate = strdatefrom+" To "+strdateto;
				
				jsonObj = new JSONObject();
				jsonObj.put("foCode", finalDate);
				jsonObj.put("id", caneAccSlNo);


				jArray.add(jsonObj);
			}
		}
	}
	catch (Exception e)
	{
        logger.info(e.getCause(), e);
        return null;
	}
    logger.info("getCaneAccountingPeriods()========jArray=========="+jArray);
	return jArray;
}

	//Added by DMurty on 12-12-2016
	@RequestMapping(value = "/getCaneAccountingPeriodsForPayment", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getCaneAccountingPeriods(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getCaneAccountingPeriods(season);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					int caneAccSlNo = (Integer) tempMap.get("caneacslno");
					
					//Date dateofcalc = (Date) tempMap.get("dateofcalc");
					//String strdateofcalc = df.format(dateofcalc);
					
					Date datefrom = (Date) tempMap.get("datefrom");
					String strdatefrom = df.format(datefrom);
					Date dateto = (Date) tempMap.get("dateto");
					String strdateto = df.format(dateto);
					String finalDate = strdatefrom+" To "+strdateto;
					
					jsonObj = new JSONObject();
					jsonObj.put("foCode", finalDate);
					jsonObj.put("id", caneAccSlNo);
					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
	    logger.info("getCaneAccountingPeriods()========jArray=========="+jArray);
		return jArray;
	}

	@RequestMapping(value = "/getDataForUpdatePayments", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getDataForUpdatePayments(@RequestBody JSONObject jsonData) throws Exception
	{
	    logger.info("getDataForUpdatePayments()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		int paymentToRyot=(Integer)jsonData.get("paymentToRyot");
		int branch=(Integer)jsonData.get("branch");
		int caneAccSlNo=(Integer)jsonData.get("foCode");
		int bankPayment=(Integer)jsonData.get("bankPayment");
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getCaneAccountingPaymentDetails(season,paymentToRyot,branch,caneAccSlNo,bankPayment);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					jsonObj = new JSONObject();
					if(bankPayment == 0)//CaneAccSBDetails
			        {
						String ryotCode = (String) tempMap.get("ryotcode");
						int branchcode = (Integer) tempMap.get("bankcode");
						String accountNum = (String) tempMap.get("sbaccno");
						double paidAmt = (Double) tempMap.get("paidamt");
						double loanamt = (Double) tempMap.get("loanamt");
						double pendingAmt = (Double) tempMap.get("pendingamt");

						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("branchcode", branchcode);
						jsonObj.put("accountNum", accountNum);
						jsonObj.put("paidAmt", paidAmt);
						jsonObj.put("loanamt", pendingAmt);
						jsonObj.put("pendingAmt", pendingAmt);
						jsonObj.put("mainRyot", ryotCode);
						jsonObj.put("paidtoRyot", pendingAmt);
						jsonObj.put("bifAmt", pendingAmt);

						jArray.add(jsonObj);
	
			        }
					else//CaneAccLoanDetails
					{
						String ryotCode = (String) tempMap.get("ryotcode");
						double paidAmt = (Double) tempMap.get("paidamt");
						int branchcode = (Integer) tempMap.get("branchcode");
						String accountNum = (String) tempMap.get("accountnum");
						double intrestRate = (Double) tempMap.get("interestrate");
						double principle = (Double) tempMap.get("advanceamount");
						double additionalInt = (Double) tempMap.get("delayedintrest");
						double totalAmount = principle+additionalInt;
						double pendingAmt = (Double) tempMap.get("pendingamt");
						int loanNo =  (Integer) tempMap.get("loanno");
						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("branchcode", branchcode);
						jsonObj.put("accountNum", accountNum);
						jsonObj.put("paidAmt", paidAmt);
						jsonObj.put("intrestRate", intrestRate);
						jsonObj.put("principle", principle);
						jsonObj.put("additionalInt", additionalInt);
						jsonObj.put("totalAmount", totalAmount);
						jsonObj.put("pendingAmt", pendingAmt);
						jsonObj.put("amount", pendingAmt);
						jsonObj.put("bifAmt", pendingAmt);
						jsonObj.put("tobePaidAmt", pendingAmt);
						jsonObj.put("mainRyot", ryotCode);
						jsonObj.put("loanno", loanNo);

						//
						jArray.add(jsonObj);
					}
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
	    logger.info("getCaneAccountingPeriods()========jArray=========="+jArray);
		return jArray;
	}

	@RequestMapping(value = "/getAccountNumbers", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getAccountNumbers(@RequestBody JSONObject jsonData) throws Exception
	{
	    logger.info("getAccountNumbers()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		String ryotCode=(String)jsonData.get("ryotCode");
		String loanOrSb=(String)jsonData.get("loanOrSb");	// Loan Or SB
		int caneaccSlNo=(Integer)jsonData.get("caneAccSlNo");	// 1 

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getAccountNumbers(season,ryotCode,loanOrSb);
			double pendingAmt = caneAccountingFunctionalService.getPendingAmountForRyotByCaneAccSlNo(ryotCode,season,loanOrSb,caneaccSlNo);
			List<Ryot> ryot = ryotService.getRyotByRyotCode(ryotCode);
			
		    logger.info("getAccountNumbers()========dropdownList=========="+dropdownList);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					if("Loan".equalsIgnoreCase(loanOrSb))
			        {
						String accountNum = (String) tempMap.get("loanaccountnumber");
						jsonObj.put("accountNum", accountNum);
						jsonObj.put("pendingAmt", pendingAmt);
						jsonObj.put("ryotName", ryot.get(0).getRyotname());
						jArray.add(jsonObj);
			        }
					else
					{
						String accountNum = (String) tempMap.get("accountnumber");
						jsonObj.put("accountNum", accountNum);
						jsonObj.put("pendingAmt", pendingAmt);
						jsonObj.put("ryotName", ryot.get(0).getRyotname());
						jArray.add(jsonObj);
					}
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
	    logger.info("getAccountNumbers()========jArray=========="+jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAccountedRyots", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getAccountedRyots(@RequestBody JSONObject jsonData) throws Exception
	{
		logger.info("getAccountedRyots()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		String loanOrSb=(String)jsonData.get("loanOrSb");	// Loan Or SB
		int caneaccSlNo=(Integer)jsonData.get("caneAccSlNo");	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getAccountedRyots(season,loanOrSb,caneaccSlNo);
		    logger.info("getAccountedRyots()========dropdownList=========="+dropdownList);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					String ryotcode = (String) tempMap.get("ryotcode");
					jsonObj.put("ryotcode", ryotcode);
					
					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
		logger.info("getAccountedRyots()========jArray=========="+jArray);
		return jArray;
	}
	//Added by DMurty on 18-12-2016
	@RequestMapping(value = "/getRyotLoanBanks", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotLoanBanks(@RequestBody JSONObject jsonData) throws Exception
	{
		logger.info("getRyotLoanBanks()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		String ryotCode=(String)jsonData.get("ryotCode");	
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getRyotLoanBanks(ryotCode,season);
		    logger.info("getRyotLoanBanks()========dropdownList=========="+dropdownList);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					int branchCode = (Integer) tempMap.get("branchcode");
					List<Branch> branch = ahFuctionalService.getBranchDtls(branchCode);
					
					jsonObj.put("branchCode", branchCode);
					jsonObj.put("branchName", branch.get(0).getBranchname());

					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
		logger.info("getRyotLoanBanks()========jArray=========="+jArray);
		return jArray;
	}
	//Added by DMurty on 18-12-2016
	@RequestMapping(value = "/getRyotLoanAccountDetailsByBranch", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotLoanAccountDetailsByBranch(@RequestBody JSONObject jsonData) throws Exception
	{
		logger.info("getRyotLoanAccountDetailsByBranch()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		String ryotCode=(String)jsonData.get("ryotCode");	
		int branchcode=(Integer)jsonData.get("branchCode");	
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getRyotLoanAccountDetailsByBranch(ryotCode,season,branchcode);
		    logger.info("getRyotLoanAccountDetailsByBranch()========dropdownList=========="+dropdownList);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					String loanaccountnumber = (String) tempMap.get("loanaccountnumber");
					jsonObj.put("LoanAccNo", loanaccountnumber);
					jsonObj.put("LoanAccNo", loanaccountnumber);
					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
		logger.info("getRyotLoanAccountDetailsByBranch()========jArray=========="+jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getRyotLoanPrincipleByBranch", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotLoanPrincipleByBranch(@RequestBody JSONObject jsonData) throws Exception
	{
		logger.info("getRyotLoanPrincipleByBranch()========JSONObject=========="+jsonData);
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		String ryotCode=(String)jsonData.get("ryotCode");	
		int branchcode=(Integer)jsonData.get("branchCode");	
		String loanAccNo=(String)jsonData.get("LoanAccNo");	

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getRyotLoanPrincipleByBranch(ryotCode,season,branchcode,loanAccNo);
		    logger.info("getRyotLoanPrincipleByBranch()========dropdownList=========="+dropdownList);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					
					jsonObj = new JSONObject();
					double principle = (Double) tempMap.get("principle");
					jsonObj.put("principle", principle);
					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
		logger.info("getRyotLoanPrincipleByBranch()========jArray=========="+jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/updatePayments", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean updatePayments(@RequestBody JSONObject jsonData) throws Exception
	{
	    logger.info("updatePayments()========jsonData=========="+jsonData);

		boolean isSuccess = false;
		String strMainRyot = null;
		//JSONObject formData = (JSONObject) jsonData.get("formData");
		String season=(String)jsonData.get("season");
		int accountedRyots=(Integer)jsonData.get("paymentToRyot");
		int branch=(Integer)jsonData.get("branch");
		int caneAccSlNo=(Integer)jsonData.get("foCode");
		int loanOrSb=(Integer)jsonData.get("bankPayment");
		//Added by DMurty on 25-12-2016
		String strpaymentDate = (String)jsonData.get("systemDate");
		int accType=(Integer)jsonData.get("acctype");
		Date paymentDate = DateUtils.getSqlDateFromString(strpaymentDate,Constants.GenericDateFormat.DATE_FORMAT);

		//		
		JSONArray paymentData=(JSONArray) jsonData.get("gridData");
		
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		int transactionCode = commonService.GetMaxTransCode(season);
		
		List advanceLoansTablesList=new ArrayList();
		Object Qryobj = null;
		List UpdateList = new ArrayList();
		List AccountsList = new ArrayList();
		String ryotCode="";
		//Date currDate = new Date();
		///DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		//String strcurrDate = df.format(currDate);
		//currDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		if(loanOrSb == 0)//CaneAccSBDetails
        {
			for(int i=0;i<paymentData.size();i++)
			{
				
				JSONObject jsonObject=(JSONObject)paymentData.get(i);
				Boolean chkFlag=(Boolean)jsonObject.get("chkFlag");
				if(chkFlag)
				{
					ryotCode=(String) jsonObject.get("ryotCode");
					int bankBranch=(Integer)jsonObject.get("branchcode");
					String accountNum=(String) jsonObject.get("accountNum");
					String strpayingAmt = (String) jsonObject.get("bifAmt");
					double payingAmt = Double.parseDouble(strpayingAmt);
					String isDirect = (String) jsonObject.get("isDirect");
					int nisDirect = 0;
					if("Yes".equalsIgnoreCase(isDirect))
					{
						nisDirect = 1;
					}
					String mainRyotCode=(String) jsonObject.get("mainRyot");
					
					String paidFrom=(String) jsonObject.get("paidFrom");
					String strtotalAmt = (String) jsonObject.get("paidtoRyot");
					double totalAmt = Double.parseDouble(strtotalAmt);
					String strloanOrSb = null;
					if(loanOrSb == 0)
					{
						strloanOrSb = "SB";
					}
					List caneAccList = new ArrayList();
					caneAccList= caneAccountingFunctionalService.getCaneAccountingDataByRyot(ryotCode,season,strloanOrSb,caneAccSlNo,0);
					if(caneAccList != null && caneAccList.size()>0)
					{
						Map CaneAccSbMap = new HashMap();
						CaneAccSbMap = (Map) caneAccList.get(0);
						double forSbAc = (Double) CaneAccSbMap.get("sbac");
						double sbPending = (Double) CaneAccSbMap.get("pendingamt");
						
						double sbPaid = 0.0;
						if((Double) CaneAccSbMap.get("paidamt") != null)
						{
							sbPaid = (Double) CaneAccSbMap.get("paidamt");
						}
						double fialSbPaid = sbPaid+payingAmt;
						
						
						

						//Here if isDirect = yes, we need to post for ICICI and ryot.For ICICI it is total amount for all ryots.
						//For Ryot it is his paid amount.
						//For ICICI glcode is Main ryotcode and for Main Ryot glcode is ICICI.
						//Here isDirect will come Yes for main ryot and for others it will come no.
						//So else will come for other ryots. we need to post SRCFORSBPAY.
						if("Yes".equalsIgnoreCase(isDirect))
						{
							//Main Ryot Posting
							String transType = "Dr";
							String TransactionType = transType;
							String strglCode = null;
							String journalMemo = null;
							if("ICICI".equalsIgnoreCase(paidFrom))
							{
								strglCode = "ICICI";
								journalMemo = "Payment through ICICI";
							}
							else if("SBH".equalsIgnoreCase(paidFrom))
							{
								strglCode = "SBH";
								journalMemo = "Payment through SBH";
							}
							else if("Company Funds".equalsIgnoreCase(paidFrom))
							{
								strglCode = "SRCFORSBPAY";
								journalMemo = "Payment through Company Funds";

							}
							
							String accCode = ryotCode;
							double Amt = payingAmt;
							
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", journalMemo);
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								
								boolean srcSbAccCodetrue = AccountCodeList.contains(AccSmry);
								if (srcSbAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean srcSbAccGrpCodetrue = AccountGrpCodeList.contains(AccSmry);
								if (srcSbAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean srcSbAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(srcSbAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
							//Here we need to update in CanAccSBDetails  
							//Hence isDirect is Yes, ryot will be main ryot.
							double sbPendingAmt = forSbAc-fialSbPaid;
							String strQry = "Update CanAccSBDetails set paidamt="+fialSbPaid+",pendingamt="+sbPendingAmt+",paymentdate='"+paymentDate+"',paymentsource='"+paidFrom+"',iciciloanamount="+Amt+",mainryotcode='"+ryotCode+"',isdirect="+nisDirect+" where season='"+season+"' and ryotcode='"+ryotCode+"' and caneacctslno="+caneAccSlNo;
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);
							
							//Posting for ICICI 
							transType = "Cr";
							TransactionType = transType;
							strglCode = ryotCode;
							accCode = null;
							if("ICICI".equalsIgnoreCase(paidFrom))
							{
								accCode = "ICICI";
								journalMemo = "Payment through ICICI";
							}
							else if("SBH".equalsIgnoreCase(paidFrom))
							{
								accCode = "SBH";
								journalMemo = "Payment through SBH";
							}
							else if("Company Funds".equalsIgnoreCase(paidFrom))
							{
								accCode = "SRCFORSBPAY";
								journalMemo = "Payment through Company Funds";
							}
							Amt = payingAmt;
							
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "For Source For SB AC Payment");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								
								boolean srcSbAccCodetrue = AccountCodeList.contains(AccSmry);
								if (srcSbAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean srcSbAccGrpCodetrue = AccountGrpCodeList.contains(AccSmry);
								if (srcSbAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean srcSbAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(srcSbAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
						}
						else
						{
							
							String transType = "Dr";
							String TransactionType = transType;
							String strglCode = null;
							strglCode = mainRyotCode;
							/*if("ICICI".equalsIgnoreCase(paidFrom))
							{
								strglCode = "ICICI";
							}
							else if("SBH".equalsIgnoreCase(paidFrom))
							{
								strglCode = "SBH";
							}
							else if("Company Funds".equalsIgnoreCase(paidFrom))
							{
								strglCode = "SRCFORSBPAY";
							}*/
							String accCode = ryotCode;
							double Amt = payingAmt;
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "For Cane Accounting");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);

								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								
								boolean srcSbAccCodetrue = AccountCodeList.contains(AccSmry);
								if (srcSbAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean srcSbAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (srcSbAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
							
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								
								boolean srcSbAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(srcSbAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
							//Here we need to update in CanAccSBDetails
							double sbPendingAmt = forSbAc-Amt;
							String strQry = "Update CanAccSBDetails set paidamt="+Amt+",pendingamt="+sbPendingAmt+",paymentdate='"+paymentDate+"',paymentsource='"+paidFrom+"',iciciloanamount="+Amt+",isdirect="+nisDirect+" where season='"+season+"' and ryotcode='"+ryotCode+"' and caneacctslno="+caneAccSlNo;
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);
					    	
							transType = "Cr";
							TransactionType = transType;
							strglCode = ryotCode;
							Amt = payingAmt;
							if("ICICI".equalsIgnoreCase(paidFrom))
							{
								accCode = "ICICI";
							}
							else if("SBH".equalsIgnoreCase(paidFrom))
							{
								accCode = "SBH";
							}
							else if("Company Funds".equalsIgnoreCase(paidFrom))
							{
								accCode = "SRCFORSBPAY";
							}
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "For Source For SB AC Payment");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								
								boolean srcSbAccCodetrue = AccountCodeList.contains(AccSmry);
								if (srcSbAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean srcSbAccGrpCodetrue = AccountGrpCodeList.contains(AccSmry);
								if (srcSbAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean srcSbAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(srcSbAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
						}
					}
					//accounting coding for seedsupplier account
					if(accType==2)
					{
						double Amt = payingAmt;
						String accCode = "PaySour";
						String strglCode = ryotCode;
						String TransactionType = "Cr";
						String journalMemo = "Payment To Seed Supplier";
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);

							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							tempMap1.put("TRANSACTION_DATE", strpaymentDate); 
							Map AccMap = updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);

							advanceLoansTablesList.add(AccMap.get("accDtls"));
							advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
							advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));

							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap =returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}

							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);

									Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}

							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
								        .get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}	
					}// ending seedsupplier accounting posting
				}
			}
        }
		else
		{
			for(int i=0;i<paymentData.size();i++)
			{
				JSONObject jsonObject=(JSONObject)paymentData.get(i);
				Boolean chkFlag=(Boolean)jsonObject.get("chkFlag");
				if(chkFlag)
				{
					ryotCode=(String) jsonObject.get("ryotCode");
					int bankBranch=(Integer)jsonObject.get("branchcode");
					String accountNum=(String) jsonObject.get("accountNum");
					int intrestRate = (Integer) jsonObject.get("intrestRate");
					String strprinciple = (String) jsonObject.get("principle");
					double principle = Double.parseDouble(strprinciple);
					
					String stradditionalInt = (String) jsonObject.get("additionalInt");
					double additionalInt = Double.parseDouble(stradditionalInt);
					
					String strtotalAmount = (String) jsonObject.get("totalAmount");
					double totalAmount = Double.parseDouble(strtotalAmount);

					String strpaidAmt = (String) jsonObject.get("bifAmt");
					double paidAmt = Double.parseDouble(strpaidAmt);
					String isDirect = (String) jsonObject.get("isDirect");
					
					
					String striciciAmount = (String) jsonObject.get("amount");
					double iciciAmount = Double.parseDouble(striciciAmount);
					
					
					int nloan=(Integer)jsonObject.get("loanno");

					
					int nisDirect = 0;
					if("Yes".equalsIgnoreCase(isDirect))
					{
						nisDirect = 1;
					}
					String paidFrom=(String) jsonObject.get("paidFrom");
					String mainRyotCode=(String) jsonObject.get("mainRyot");

					String strloanOrSb = null;
					if(loanOrSb == 0)
					{
						strloanOrSb = "SB";
					}
					else
					{
						strloanOrSb = "Loan";
					}
					
					List caneAccList = new ArrayList();
					caneAccList= caneAccountingFunctionalService.getCaneAccountingDataByRyot(ryotCode,season,strloanOrSb,caneAccSlNo,nloan);
					if(caneAccList != null && caneAccList.size()>0)
					{
						Map CaneAccLoanMap = new HashMap();
						CaneAccLoanMap = (Map) caneAccList.get(0);
						
						int loanno=(Integer)CaneAccLoanMap.get("loanno");
						double intrestAmt = (Double)CaneAccLoanMap.get("interestamount");
						double loanPending = (Double) CaneAccLoanMap.get("pendingamt");	//From Cane Accounting//5000
						double loanAmt =  (Double) CaneAccLoanMap.get("advanceamount"); //Loan Amount
						
						//Added by DMurty on 17-02-2017
						double prevAdvisedAmt=0.0;
						if((Double)CaneAccLoanMap.get("advisedamt") != null)
						{
							prevAdvisedAmt=(Double)CaneAccLoanMap.get("advisedamt");
						}
						double finalAdvisedAmt = prevAdvisedAmt+paidAmt;
						
						double total = loanAmt+intrestAmt;	//Total Amount
						double finalIntrest = intrestAmt+additionalInt;			//7500+add
						
						double floanPending = loanPending-paidAmt;				//Loan From Cane Accounting-Paid from Update Payments
						
						//this needs to be deducted from principle
						double loanPayable = finalAdvisedAmt-finalIntrest;			//5000-7500=-2500 = principle left
						double finalPrinciple = loanAmt-loanPayable;			//100000-(-2500)=102500 // this is same as adjustedPrincple + addn.int
						//finalPrinciple = finalPrinciple+additionalInt;				//102500+add. Int if it is there
						
						//double paidLoanAmt = loanAmt-finalPrinciple;       //10000-103000
						double intryotpayable = finalIntrest;// once delayed intrest is calculated. This will be final int-delayed int. Present Delayed int is 0.
						
						String qry = "Update CanAccLoansDetails set pendingamt="+floanPending+",advisedamt="+finalAdvisedAmt+",paymentsource='"+paidFrom+"',finalprinciple="+finalPrinciple+",finalinterest="+finalIntrest+",additionalint="+additionalInt+",intryotpayable="+intryotpayable+",isdirect="+nisDirect+",mainryotcode='"+ryotCode+"',paymentdate='"+paymentDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and advloan=1 and accountnum='"+accountNum+"' and caneacctslno="+caneAccSlNo+" and loanno="+loanno;
						logger.info("updatePayments()------qry"+qry);
						Qryobj = qry;
				    	UpdateList.add(Qryobj);
						
						List loanSummaryList=caneAccountingFunctionalService.getLoanSummaryByBankAndLoanNo(season,ryotCode,loanno,bankBranch);
						if(loanSummaryList!=null)
						{		
							LoanSummary loanSummary=(LoanSummary)loanSummaryList.get(0);
							//Commented by DMurty on 24-02-2016
							/*if(caneAccSlNo != 12)
							{
								double intrestAmount = loanSummary.getInterestamount();
								intrestAmount = intrestAmount+finalIntrest;
								loanSummary.setInterestamount(intrestAmount);
								double disbursed = loanSummary.getDisbursedamount();
								double totalAmt = disbursed+finalIntrest;
								loanSummary.setTotalamount(totalAmt);
								
								double fpaid = loanSummary.getPaidamount()+paidAmt;
								loanSummary.setPaidamount(fpaid);
								
								loanSummary.setPendingamount(finalPrinciple);
								loanSummary.setRyotcode(loanSummary.getRyotcode());
								advanceLoansTablesList.add(loanSummary);						
								
								int branchCode = loanSummary.getBranchcode();
								//double pendingAmt = pending;
								//present as we are considering the payment is happening on the same day of updatepayments we are taking system date
								//but if the updatepayments happen in next day, then the ui given date need to be considered
								Date principleDate=paymentDate;//DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
								
								qry = "Update LoanPrincipleAmounts set principle="+finalPrinciple+",principledate='"+principleDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
								logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
								Qryobj = qry;
								UpdateList.add(Qryobj);
								
								//Updating Record in Loan Details table.
								qry = "Update LoanDetails set interestamount="+intrestAmount+",pendingamount="+finalPrinciple+",recoverydate='"+principleDate+"',paidamount="+fpaid+" where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
								logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
								Qryobj = qry;
								UpdateList.add(Qryobj);
							}*/
							
							int branchCode = loanSummary.getBranchcode();
							
							
							String transType = "Dr";
							String TransactionType = transType;
							String strglCode = null;
							
							
							List<Branch> bankBranch1 = aHFuctionalService.getBranchDetailsByBranchCode(branchCode);
							long brancgGlCode = bankBranch1.get(0).getGlcode();
							String strbrancgGlCode = Long.toString(brancgGlCode);
							//If is Direct payment is Yes, That ryot will get from bank.So for him glcode is bank gl code.
							//If no, Main Ryot code will be gl code
							if("Yes".equalsIgnoreCase(isDirect))
							{
								strglCode = strbrancgGlCode;
							}
							else
							{
								strglCode = mainRyotCode;
							}
							String accCode = ryotCode;
							double Amt = paidAmt;
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "Towards Tieup Loan Recovery");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								boolean istrueforryotloan = AccountCodeList.contains(AccSmry);
								if (istrueforryotloan == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (loanAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(ryotLoanAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY",strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
							
							transType = "Cr";
							TransactionType = transType;
							strglCode = ryotCode;
							accCode = "RTL";
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "Towards Tieup Loan Recovery");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								
								boolean rtlAccCodetrue = AccountCodeList.contains(AccSmry);
								if (rtlAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean rtlAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (rtlAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								
								boolean rtlAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(rtlAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
							
							transType = "Dr";
							TransactionType = transType;
							strglCode = ryotCode;
							accCode = strbrancgGlCode;
							if(accCode != null)
							{
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);

								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

								Map tempMap = new HashMap();
								tempMap.put("ACCOUNT_CODE", accCode);
								tempMap.put("AMOUNT", Amt);
								tempMap.put("JRNL_MEM", "Towards Tieup Loan Recovery");
								tempMap.put("SEASON", season);
								tempMap.put("TRANS_TYPE", TransactionType);
								tempMap.put("GL_CODE", strglCode);
								tempMap.put("TRANSACTION_CODE", transactionCode);
								//Added by DMurty on 25-12-2016
								tempMap.put("TRANSACTION_DATE", strpaymentDate); 
								Map AccMap = updateAccountsNew(tempMap);
								
								advanceLoansTablesList.add(AccMap.get("accDtls"));
								advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
								advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								boolean brnchLoanAccCodetrue = AccountCodeList.contains(AccSmry);
								if (brnchLoanAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
									}
								}
								
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (advAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode,finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean brnchLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(brnchLoanAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
						}
					}
				
				
					//accounting coding for seedsupplier account
					if(accType==2)
					{
						double Amt = paidAmt;
						String accCode = "PaySour";
						String strglCode = ryotCode;
						String TransactionType = "Cr";
						String journalMemo = "Payment To Seed Supplier";
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);

							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							tempMap1.put("TRANSACTION_DATE", strpaymentDate); 
							Map AccMap = updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);

							advanceLoansTablesList.add(AccMap.get("accDtls"));
							advanceLoansTablesList.add(AccMap.get("accGrpDtls"));
							advanceLoansTablesList.add(AccMap.get("accSubGrpDtls"));

							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap =returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}

							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);

									Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}

							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
								        .get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();

									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}	
					}// ending seedsupplier accounting posting
				}
			}
		}
		//here needs to update or insert in tables.
		logger.info("updatePayments()------advanceLoansTablesList"+advanceLoansTablesList);
		Iterator entries = AccountSummaryMap.entrySet().iterator();
		while (entries.hasNext())
		{
		    Map.Entry entry = (Map.Entry) entries.next();
		    Object value = entry.getValue();
		    AccountSummary accountSummary = (AccountSummary) value;
		    
		    String accountCode = accountSummary.getAccountcode();
		    String sesn = accountSummary.getSeason();
		    double runnbal = accountSummary.getRunningbalance();
		    String crOrDr = accountSummary.getBalancetype();
		    
		    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
		    if(AccList != null && AccList.size()>0)
		    {
		    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
		    	Qryobj = qry;
		    	UpdateList.add(Qryobj);
		    }
		    else
		    {
		    	AccountsList.add(accountSummary);
		    }
		}
		
		Iterator entries1 = AccountGroupMap.entrySet().iterator();
		while (entries1.hasNext())
		{
		    Map.Entry entry = (Map.Entry) entries1.next();
		    Object value = entry.getValue();
		    
		    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
		    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
		    String sesn = accountGroupSummary.getSeason();
		    double runnbal = accountGroupSummary.getRunningbalance();
		    String crOrDr = accountGroupSummary.getBalancetype();
		    
		    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
			logger.info("updateAccountsNew()------AccList"+AccList);

		    if(AccList != null && AccList.size()>0)
		    {
		    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
		    	Qryobj = qry;
		    	UpdateList.add(Qryobj);
		    }
		    else
		    {
		    	AccountsList.add(accountGroupSummary);
		    }
		}
		
		Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
		while (entries2.hasNext())
		{
		    Map.Entry entry = (Map.Entry) entries2.next();
		    Object value = entry.getValue();
		    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
		   
		    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
		    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
		    String sesn = accountSubGroupSummary.getSeason();
		    double runnbal = accountSubGroupSummary.getRunningbalance();
		    String crOrDr = accountSubGroupSummary.getBalancetype();
		    
		    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
			logger.info("updateAccountsNew()------tempMap"+AccList);
		    if(AccList != null && AccList.size()>0)
		    {
		    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
		    	Qryobj = qry;
		    	UpdateList.add(Qryobj);
		    }
		    else
		    {
		    	AccountsList.add(accountSubGroupSummary);
		    }
		}
		isSuccess=commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(advanceLoansTablesList,UpdateList,AccountsList);
		return isSuccess;
	}
	
	@RequestMapping(value = "/saveAdvancesAndLoansDetailsForVerify", method = RequestMethod.POST)
	public @ResponseBody
	Boolean saveAdvancesAndLoansDetailsForVerify(@RequestBody JSONArray jsonData) throws JsonParseException, JsonMappingException, Exception 
	{	
		logger.info("saveAdvancesAndLoansDetails()------jsonData"+jsonData);
		List advanceLoansTablesList=new ArrayList();
		Object Qryobj = null;
		List UpdateList = new ArrayList();
		List AccountsList = new ArrayList();

		double finalCdcAmount = 0.0;
		double finalUlAmount = 0.0;
		double finalTcAmount = 0.0;
		double finalHarAmount = 0.0;
		double otherDed = 0.0;
		double finalDedPartToSbAcPayable = 0.0;
		double forAdvAndLoans = 0.0;
		double finalforSbAcs = 0.0;
		double harvestChargePayable = 0.0;
		double totalAdvPayable = 0.0;
		double totalLoanPayable = 0.0;
		double finalTotalVal = 0.0;
		
		double ryotCdcAmount = 0.0;
		double ryotUlAmount = 0.0;
		double ryotTcAmount = 0.0;
		double ryotHarAmount = 0.0;
		double ryotOtherDed = 0.0;
		
		double ryotDedPartToSbAcPayable = 0.0;
		double ryotforAdvAndLoans = 0.0;
		double ryotforSbAcs = 0.0;
		double ryotharvestChargePayable = 0.0;
		double ryottotalAdvPayable = 0.0;
		double ryottotalLoanPayable = 0.0;
		double ryotfinalTotalVal = 0.0;
		double ryotforDeductions = 0.0;
		double ryotLoansPartToSbAccPaid = 0.0;
		int transactionCode = 0;
		boolean updateFlag= false;
		int ncaneAccSlno = 0;
		String strSeason = null;
		
		
		ahFuctionalService.TruncateTemp("CanAccLoansDetailsTemp");
		ahFuctionalService.TruncateTemp("CanAccSBDetailsTemp");
		try
		{
			for(int i=0;i<jsonData.size();i++)
			{
				ryotCdcAmount = 0.0;
				ryotUlAmount = 0.0;
				ryotTcAmount = 0.0;
				ryotHarAmount = 0.0;
				ryotOtherDed = 0.0;
				ryotDedPartToSbAcPayable = 0.0;
				ryotforAdvAndLoans = 0.0;
				ryotforSbAcs = 0.0;
				ryotharvestChargePayable = 0.0;
				ryottotalAdvPayable = 0.0;
				ryottotalLoanPayable = 0.0;
				ryotfinalTotalVal = 0.0;
				ryotforDeductions = 0.0;
				ryotLoansPartToSbAccPaid = 0.0;
				JSONObject jsonObject=(JSONObject)jsonData.get(i);
				
				String ryotName=(String) jsonObject.get("RyotName");	
				Integer caneAccSlno=(Integer) jsonObject.get("caneAccSlno");
				String ryotCode=(String) jsonObject.get("ryotcode");
				String ryotsbac=(String) jsonObject.get("ryotLoansbac");	
				String season=(String) jsonObject.get("season");
				
				double totalSuppliedWeight = 0.0;
				List CaneValueSplitupList = caneAccountingFunctionalService.getCaneValueSplitupByRyot(season,caneAccSlno,ryotCode);
				if(CaneValueSplitupList != null && CaneValueSplitupList.size()>0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) CaneValueSplitupList.get(0);
					totalSuppliedWeight = (Double) tempMap.get("canesupplied");
				}
				
				strSeason = season;
			
				logger.info("======== For Ryot No=========="+i);
				logger.info("======== For Ryot Code=========="+ryotCode);

	 			JSONArray gridData=(JSONArray) jsonObject.get("caneacslno");
				logger.info("======== saveAdvancesAndLoansDetails()==========gridData "+gridData);

	 			if(gridData != null && gridData.isEmpty() == false)
	 			{
	 				double ryotsbacc = Double.parseDouble(ryotsbac);
		 			ryotLoansPartToSbAccPaid = ryotsbacc;
		 			if(i==0)
		 			{
		 				ncaneAccSlno = caneAccSlno;
					    List dedPartList = ahFuctionalService.getDedPartForSbAccList(ncaneAccSlno,season);
					    if(dedPartList != null && dedPartList.size()>0)
					    {
					    	Map tempMap = new HashMap();
					    	tempMap = (Map) dedPartList.get(0);
					    	finalDedPartToSbAcPayable = (Double) tempMap.get("ded");
					    	forAdvAndLoans = (Double) tempMap.get("advloans");
					    	finalTotalVal = (Double) tempMap.get("totalval");
					    }
		 			}
		 			
		 			//Getting amounts for individual ryots
				    List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(ncaneAccSlno,season,ryotCode);
					logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
				    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
				    {
				    	Map tempMap = new HashMap();
				    	tempMap = (Map) dedPartForRyotList.get(0);
				    	ryotforAdvAndLoans = (Double) tempMap.get("forloans");
				    	ryotfinalTotalVal = (Double) tempMap.get("totalvalue");
				    	ryotforDeductions = ryotfinalTotalVal-ryotforAdvAndLoans;//(Double) tempMap.get("forsbacs");
				    }
		 			
		 			//get dedsbacc Amt from canevaluesplituptemp
					double dedSbAmt = caneAccountingFunctionalService.getdedSbAmt(caneAccSlno,season,ryotCode);
					ryotDedPartToSbAcPayable = dedSbAmt;
					
					double finalSbAcAmt = ryotsbacc;
					ryotLoansPartToSbAccPaid = ryotsbacc;
		 			finalforSbAcs += finalSbAcAmt;
		 			
		 			ryotforSbAcs += finalSbAcAmt;
		 			String strQry = null;
					List FinalList = new ArrayList();
					List DedDetails_temps=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
					logger.info("saveAdvancesAndLoansDetails()------DedDetails_temps"+DedDetails_temps);
					if (DedDetails_temps != null && !DedDetails_temps.isEmpty())
					{
						for(int k=0;k<DedDetails_temps.size();k++)
						{
							Map DedMap = new HashMap();
							DedMap = (Map) DedDetails_temps.get(k);
							
							int id = (Integer) DedMap.get("id");
							int dedCode = (Integer) DedMap.get("DedCode");
							String deduction = (String) DedMap.get("Deduction");
							int caneAcSlno = (Integer) DedMap.get("caneacslno");
							double deductionAmt = (Double) DedMap.get("deductionamt");
							double extentSize = (Double) DedMap.get("extentsize");
							int harvestContCode = (Integer) DedMap.get("harvestcontcode");
							double netPayable = (Double) DedMap.get("netpayable");
							double newPayable = (Double) DedMap.get("newpayable");
							double oldPendingAmount = (Double) DedMap.get("oldpendingamount");
							double pendingAmount = (Double) DedMap.get("pendingamt");
							String ryotcode = (String) DedMap.get("ryotcode");
							String dedSeason = (String) DedMap.get("season");
							double suppliedCaneWt = (Double) DedMap.get("suppliedcaneWt");
							String login = (String) DedMap.get("login");
							if(dedCode == 1)
							{
								finalCdcAmount += deductionAmt;
								ryotCdcAmount += deductionAmt;
							}
							else if(dedCode == 2)
							{
								finalUlAmount += deductionAmt;
								ryotUlAmount += deductionAmt;
							}
							else if(dedCode == 3)
							{
								finalTcAmount += deductionAmt;
								ryotTcAmount += deductionAmt;
							}
							else if(dedCode == 4)
							{
								finalHarAmount += deductionAmt;
								ryotHarAmount += deductionAmt;
								ryotharvestChargePayable += deductionAmt;
								harvestChargePayable += deductionAmt;
							}
							else if(dedCode == 5)
							{
								otherDed += deductionAmt;
								ryotOtherDed += deductionAmt;
							}
						}
						logger.info("Posted Deductions for individual deduction in Accounts------");
					}
					
		 			for(int j=0;j<gridData.size();j++)
					{
						AdvancesLoansForPCABean advancesLoansForPCABean = new ObjectMapper().readValue(gridData.get(j).toString(), AdvancesLoansForPCABean.class);
						if(advancesLoansForPCABean.getAdvanceflag().equals("0"))
						{
							AdvanceSummary advanceSummary = prepareModelForUpdateAdvanceSummary(advancesLoansForPCABean,season,ryotCode);
							
							CanAccLoansDetailsTemp canAccLoansDetailsTemp=new CanAccLoansDetailsTemp();
							
							canAccLoansDetailsTemp.setAccountnum(advancesLoansForPCABean.getReferencenumber());
							double inrst = 0.0;
							if(advancesLoansForPCABean.getAdvanceinterest() != null)
							{
								inrst = advancesLoansForPCABean.getAdvanceinterest();
							}
							
							double adjPrinciple = advancesLoansForPCABean.getTotalamount()-advancesLoansForPCABean.getLoanPayable();
							canAccLoansDetailsTemp.setAdjustedprincipleamount(adjPrinciple);
							
							canAccLoansDetailsTemp.setAdvanceamount(advancesLoansForPCABean.getAdvanceamount());
							Date currDate = new Date();
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							String strcurrDate = df.format(currDate);
							//Modified by DMurty on 22-12-2016
							currDate = DateUtils.getSqlDateFromString(advancesLoansForPCABean.getAdvancedate(),Constants.GenericDateFormat.DATE_FORMAT);
							canAccLoansDetailsTemp.setAdvdate(currDate);
							canAccLoansDetailsTemp.setAdvloan(0);
							canAccLoansDetailsTemp.setBranchcode(Integer.parseInt(advancesLoansForPCABean.getReferencenumber()));
							canAccLoansDetailsTemp.setCaneacctslno(caneAccSlno);
							canAccLoansDetailsTemp.setDelayedintrest(0.0);
							canAccLoansDetailsTemp.setInterestamount(advancesLoansForPCABean.getAdvanceinterest());
							double intRate = 0.0;
							if(advancesLoansForPCABean.getNetRate() != null)
							{
								intRate = advancesLoansForPCABean.getNetRate();
							}
							intRate =Double.parseDouble(new DecimalFormat("##.##").format(intRate));
							
							canAccLoansDetailsTemp.setInterestrate(intRate);
							canAccLoansDetailsTemp.setLoanno(advancesLoansForPCABean.getLoanNo());
							canAccLoansDetailsTemp.setNetrate(advancesLoansForPCABean.getAdvanceinterest());
							canAccLoansDetailsTemp.setPaidamt(advancesLoansForPCABean.getLoanPayable());
							double pendingAmt = advancesLoansForPCABean.getAdvanceamount()-advancesLoansForPCABean.getLoanPayable();
							pendingAmt =Double.parseDouble(new DecimalFormat("##.##").format(pendingAmt));
							canAccLoansDetailsTemp.setPendingamt(pendingAmt);
						
							String repayDate = advancesLoansForPCABean.getAdvancerepaydate();
							Date strRepayDate = DateUtils.getSqlDateFromString(repayDate,Constants.GenericDateFormat.DATE_FORMAT);
							canAccLoansDetailsTemp.setRepaymentdate(strRepayDate);
							
							canAccLoansDetailsTemp.setRyotcode(ryotCode);
							canAccLoansDetailsTemp.setSeason(season);
							if(advancesLoansForPCABean.getSubventedInterestRate() != null)
							{
								double subvIntrestRate = Double.parseDouble(advancesLoansForPCABean.getSubventedInterestRate());
								canAccLoansDetailsTemp.setSubventedrate(subvIntrestRate);
							}
							else
							{
								canAccLoansDetailsTemp.setSubventedrate(0.0);
							}
							canAccLoansDetailsTemp.setTotalamount(advancesLoansForPCABean.getTotalamount()+inrst);
							
							canAccLoansDetailsTemp.setPaymentsource(null);
							canAccLoansDetailsTemp.setIsdirect((byte) 0);
							canAccLoansDetailsTemp.setRyotdetails("NA");
							canAccLoansDetailsTemp.setMainryotcode("NA");
							
							//Modified by DMurty on 16-12-2016
							canAccLoansDetailsTemp.setAdvisedamt(advancesLoansForPCABean.getLoanPayable());
							
							canAccLoansDetailsTemp.setAdditionalint(0.0);
							
							double finalPrinciple = adjPrinciple+canAccLoansDetailsTemp.getAdditionalint();
							canAccLoansDetailsTemp.setFinalprinciple(finalPrinciple);
							
							canAccLoansDetailsTemp.setIntryotpayable(0.0);
							canAccLoansDetailsTemp.setFinalinterest(0.0);
							
							advanceLoansTablesList.add(canAccLoansDetailsTemp);
							
							totalAdvPayable += advancesLoansForPCABean.getLoanPayable();
							ryottotalAdvPayable += advancesLoansForPCABean.getLoanPayable();
						}
						else
						{ 
							CanAccLoansDetailsTemp canAccLoansDetailsTemp=prepareModelCanAccLoansDetailsTemp(advancesLoansForPCABean,season,ryotCode,caneAccSlno);
							totalLoanPayable += advancesLoansForPCABean.getLoanPayable();
							ryottotalLoanPayable += advancesLoansForPCABean.getLoanPayable();
							advanceLoansTablesList.add(canAccLoansDetailsTemp);
						}
					}
		 			
		 			//Added by DMurty on 29-12-2016
					Date datefrom = null;
					Date dateto = null;
					List AccDateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
					logger.info("saveAdvancesAndLoansDetails()========AccDateList=========="+AccDateList);
					if(AccDateList != null && AccDateList.size()>0)
					{
						Map DateMap = new HashMap();
						DateMap = (Map) AccDateList.get(0);
						datefrom = (Date) DateMap.get("datefrom");
						dateto = (Date) DateMap.get("dateto");
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String datefrom1 = df.format(datefrom);
						String dateto1 = df.format(dateto);

						datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
						dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
					}
					logger.info("saveAdvancesAndLoansDetails()========datefrom=========="+datefrom);
					logger.info("saveAdvancesAndLoansDetails()========dateto=========="+dateto);
					
					
					String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
					logger.info("saveAdvancesAndLoansDetails()========agreementNo=========="+agreementNo);
					
		 			CanAccSBDetailsTemp canAccSBDetailsTemp = new CanAccSBDetailsTemp();
					int sbBankCode = 0;
					String sbAccNo = null;
					List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
					if(ryotBankList!=null)
					{
						AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
						sbBankCode=agreement.getBranchcode();
						sbAccNo = agreement.getAccountnumber();
						logger.info("========getRyotSbAccNo() sbBankCode=========="+sbBankCode);
						logger.info("========getRyotSbAccNo() sbAccNo=========="+sbAccNo);
					}
					canAccSBDetailsTemp.setAdvamt(ryottotalAdvPayable);
					canAccSBDetailsTemp.setBankcode(sbBankCode);
					canAccSBDetailsTemp.setCdcamt(ryotCdcAmount);
					canAccSBDetailsTemp.setDed(ryotforDeductions);
					canAccSBDetailsTemp.setHarvesting(ryotharvestChargePayable);
					canAccSBDetailsTemp.setLoanamt(ryottotalLoanPayable);				
					canAccSBDetailsTemp.setLoans(ryotforAdvAndLoans);
					canAccSBDetailsTemp.setOtherded(ryotOtherDed);
					canAccSBDetailsTemp.setPaidamt(0.0);//From Cane Accounting it will be 0
					canAccSBDetailsTemp.setPendingamt(ryotforSbAcs);
					canAccSBDetailsTemp.setRyotcode(ryotCode);
					canAccSBDetailsTemp.setSbac(ryotforSbAcs);
					canAccSBDetailsTemp.setSbaccno(sbAccNo);
					canAccSBDetailsTemp.setSeason(season);
					canAccSBDetailsTemp.setSuppqty(totalSuppliedWeight);
					canAccSBDetailsTemp.setTotalamt(ryotfinalTotalVal);//No Need
					canAccSBDetailsTemp.setTransport(ryotTcAmount);
					canAccSBDetailsTemp.setUlamt(ryotUlAmount);		
					canAccSBDetailsTemp.setCaneacctslno(caneAccSlno);

					//canAccSBDetails.setPaymentdate(new Date());
					canAccSBDetailsTemp.setPaymentsource(null);
					canAccSBDetailsTemp.setIsdirect((byte) 0);
					canAccSBDetailsTemp.setRyotdetails("NA");
					canAccSBDetailsTemp.setMainryotcode("NA");
					canAccSBDetailsTemp.setIciciloanamount(0.0);
					
					advanceLoansTablesList.add(canAccSBDetailsTemp);
	 			}
	 			else
	 			{
	 				double ryotsbacc = Double.parseDouble(ryotsbac);
	 				//double ryotsbacc = (double) ryotsbac;

		 			ryotLoansPartToSbAccPaid = ryotsbacc;
		 			//Here getting Sum of DedPartToSbAcPayable from CaneValueSplitupTemp. For first rec it is enough to get. No need to call this in for loop
		 			if(i==0)
		 			{
		 				ncaneAccSlno = caneAccSlno;
					    List dedPartList = ahFuctionalService.getDedPartForSbAccList(ncaneAccSlno,season);
					    if(dedPartList != null && dedPartList.size()>0)
					    {
					    	Map tempMap = new HashMap();
					    	tempMap = (Map) dedPartList.get(0);
					    	finalDedPartToSbAcPayable = (Double) tempMap.get("ded");
					    	forAdvAndLoans = (Double) tempMap.get("advloans");
					    	finalTotalVal = (Double) tempMap.get("totalval");
					    }
		 			}
		 			
		 			//Getting amounts for individual ryots
				    List dedPartForRyotList = ahFuctionalService.getDedPartForSbAccListForRyot(ncaneAccSlno,season,ryotCode);
					logger.info("======== getDedPartForSbAccListForRyot() =========="+dedPartForRyotList);
				    if(dedPartForRyotList != null && dedPartForRyotList.size()>0)
				    {
				    	Map tempMap = new HashMap();
				    	tempMap = (Map) dedPartForRyotList.get(0);
				    	ryotforAdvAndLoans = (Double) tempMap.get("forloans");
				    	ryotfinalTotalVal = (Double) tempMap.get("totalvalue");
				    	ryotforDeductions = ryotfinalTotalVal-ryotforAdvAndLoans;//(Double) tempMap.get("forsbacs");
				    }
		 			
				    //get dedsbacc Amt from canevaluesplituptemp
					double dedSbAmt = caneAccountingFunctionalService.getdedSbAmt(caneAccSlno,season,ryotCode);
					ryotDedPartToSbAcPayable = dedSbAmt;
					
					double finalSbAcAmt = ryotsbacc;
					ryotLoansPartToSbAccPaid = ryotsbacc;
		 			finalforSbAcs += finalSbAcAmt;
		 			ryotforSbAcs += finalSbAcAmt;
		 			
					List FinalList = new ArrayList();
					List DedDetails_temps=caneAccountingFunctionalService.getDedDtlsFromTemp(caneAccSlno,season,ryotCode);
					logger.info("saveAdvancesAndLoansDetails()------DedDetails_temps"+DedDetails_temps);
					if (DedDetails_temps != null && !DedDetails_temps.isEmpty())
					{
						for(int k=0;k<DedDetails_temps.size();k++)
						{
							Map DedMap = new HashMap();
							DedMap = (Map) DedDetails_temps.get(k);
							
							int id = (Integer) DedMap.get("id");
							int dedCode = (Integer) DedMap.get("DedCode");
							String deduction = (String) DedMap.get("Deduction");
							int caneAcSlno = (Integer) DedMap.get("caneacslno");
							double deductionAmt = (Double) DedMap.get("deductionamt");
							double extentSize = (Double) DedMap.get("extentsize");
							int harvestContCode = (Integer) DedMap.get("harvestcontcode");
							double netPayable = (Double) DedMap.get("netpayable");
							double newPayable = (Double) DedMap.get("newpayable");
							double oldPendingAmount = (Double) DedMap.get("oldpendingamount");
							double pendingAmount = (Double) DedMap.get("pendingamt");
							String ryotcode = (String) DedMap.get("ryotcode");
							String dedSeason = (String) DedMap.get("season");
							double suppliedCaneWt = (Double) DedMap.get("suppliedcaneWt");
							String login = (String) DedMap.get("login");
							if(dedCode == 1)
							{
								finalCdcAmount += deductionAmt;
								ryotCdcAmount += deductionAmt;
							}
							else if(dedCode == 2)
							{
								finalUlAmount += deductionAmt;
								ryotUlAmount += deductionAmt;
							}
							else if(dedCode == 3)
							{
								finalTcAmount += deductionAmt;
								ryotTcAmount += deductionAmt;
							}
							else if(dedCode == 4)
							{
								finalHarAmount += deductionAmt;
								ryotHarAmount += deductionAmt;
								ryotharvestChargePayable += deductionAmt;
								harvestChargePayable += deductionAmt;
							}
							else if(dedCode == 5)
							{
								otherDed += deductionAmt;
								ryotOtherDed += deductionAmt;
							}
						}
					}
					
					//Added by DMurty on 29-12-2016
					Date datefrom = null;
					Date dateto = null;
					List AccDateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(caneAccSlno,season);
					logger.info("saveAdvancesAndLoansDetails()========AccDateList=========="+AccDateList);
					if(AccDateList != null && AccDateList.size()>0)
					{
						Map DateMap = new HashMap();
						DateMap = (Map) AccDateList.get(0);
						datefrom = (Date) DateMap.get("datefrom");
						dateto = (Date) DateMap.get("dateto");
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String datefrom1 = df.format(datefrom);
						String dateto1 = df.format(dateto);

						datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
						dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
					}
					logger.info("saveAdvancesAndLoansDetails()========datefrom=========="+datefrom);
					logger.info("saveAdvancesAndLoansDetails()========dateto=========="+dateto);
					
					
					String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
					logger.info("saveAdvancesAndLoansDetails()========agreementNo=========="+agreementNo);
						
		 			CanAccSBDetailsTemp canAccSBDetailsTemp = new CanAccSBDetailsTemp();
					int sbBankCode = 0;
					String sbAccNo = null;
					List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
					if(ryotBankList!=null)
					{
						AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
						sbBankCode=agreement.getBranchcode();
						sbAccNo = agreement.getAccountnumber();
						logger.info("========getRyotSbAccNo() sbBankCode=========="+sbBankCode);
						logger.info("========getRyotSbAccNo() sbAccNo=========="+sbAccNo);
					}
					canAccSBDetailsTemp.setCaneacctslno(caneAccSlno);
					canAccSBDetailsTemp.setAdvamt(ryottotalAdvPayable);
					canAccSBDetailsTemp.setBankcode(sbBankCode);
					canAccSBDetailsTemp.setCdcamt(ryotCdcAmount);
					canAccSBDetailsTemp.setDed(ryotforDeductions);
					canAccSBDetailsTemp.setHarvesting(ryotharvestChargePayable);
					canAccSBDetailsTemp.setLoanamt(ryottotalLoanPayable);				
					canAccSBDetailsTemp.setLoans(ryotforAdvAndLoans);
					canAccSBDetailsTemp.setOtherded(ryotOtherDed);
					canAccSBDetailsTemp.setPaidamt(0.0);//From Cane Accounting it will be 0
					canAccSBDetailsTemp.setPendingamt(ryotforSbAcs);
					canAccSBDetailsTemp.setRyotcode(ryotCode);
					canAccSBDetailsTemp.setSbac(ryotforSbAcs);
					canAccSBDetailsTemp.setSbaccno(sbAccNo);
					canAccSBDetailsTemp.setSeason(season);
					canAccSBDetailsTemp.setSuppqty(totalSuppliedWeight);
					canAccSBDetailsTemp.setTotalamt(ryotfinalTotalVal);//No Need
					canAccSBDetailsTemp.setTransport(ryotTcAmount);
					canAccSBDetailsTemp.setUlamt(ryotUlAmount);		
					canAccSBDetailsTemp.setPaymentsource(null);
					canAccSBDetailsTemp.setIsdirect((byte) 0);
					canAccSBDetailsTemp.setRyotdetails("NA");
					canAccSBDetailsTemp.setMainryotcode("NA");
					canAccSBDetailsTemp.setIciciloanamount(0.0);
					advanceLoansTablesList.add(canAccSBDetailsTemp);
	 			}
			}

			logger.info("saveAdvancesAndLoansDetails()------advanceLoansTablesList"+advanceLoansTablesList);
			updateFlag=commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(advanceLoansTablesList,UpdateList,AccountsList);
			boolean reportFlag = false;
			if(updateFlag == true)
			{
				advanceLoansTablesList.clear();
				UpdateList.clear();
				AccountsList.clear();
				
				List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitupNew(ncaneAccSlno);
				if(caneValueSplitupSummaryList != null && caneValueSplitupSummaryList.size()>0)
				{
					ahFuctionalService.TruncateTemp("CaneAccDetailsTemp");
					if (caneValueSplitupSummaryList != null && !caneValueSplitupSummaryList.isEmpty())
					{
						for(int k=0;k<caneValueSplitupSummaryList.size();k++)
						{
							logger.info("========getCaneUpdationCheckList() k=========="+k);

							Map valMap = new HashMap();
							valMap = (Map) caneValueSplitupSummaryList.get(k);
							logger.info("========getCaneUpdationCheckList() valMap=========="+valMap);

							String ryotCode  = (String)valMap.get("ryotcode");
							String ryotname  = (String)valMap.get("ryotname");
							double wt = (Double)valMap.get("canesupplied");
							double toalVal = (Double)valMap.get("totalvalue");
							int  CanAccSlNo = (Integer)valMap.get("caneacslno");
							String season  = (String)valMap.get("season");
							
							//Added by DMurty on 29-12-2016
							Date datefrom = null;
							Date dateto = null;
							List AccDateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(CanAccSlNo,season);
							logger.info("saveAdvancesAndLoansDetails()========AccDateList=========="+AccDateList);
							if(AccDateList != null && AccDateList.size()>0)
							{
								Map DateMap = new HashMap();
								DateMap = (Map) AccDateList.get(0);
								datefrom = (Date) DateMap.get("datefrom");
								dateto = (Date) DateMap.get("dateto");
								
								DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
								String datefrom1 = df.format(datefrom);
								String dateto1 = df.format(dateto);

								datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
								dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
							}
							logger.info("saveAdvancesAndLoansDetails()========datefrom=========="+datefrom);
							logger.info("saveAdvancesAndLoansDetails()========dateto=========="+dateto);
							
							
							String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
							logger.info("saveAdvancesAndLoansDetails()========agreementNo=========="+agreementNo);

							double cdc = 0.0;
							double ul = 0.0;
							double harvest = 0.0;
							double tp = 0.0;
							double sbAmt = 0.0;
							String accountnumber = null;
							int bankcode = 0;
							String bankName = null;
							double otherded = 0.0;
						    List SbAccList = ahFuctionalService.getSbDetailsTemp(ryotCode,CanAccSlNo,season);
							logger.info("========getCaneUpdationCheckList() SbAccList=========="+SbAccList);
						    if (SbAccList != null && !SbAccList.isEmpty())
							{
						    	Map sbMap = new HashMap();
						    	sbMap = (Map) SbAccList.get(0);
								cdc = (Double) sbMap.get("cdcamt");
								ul = (Double) sbMap.get("ulamt");
								harvest = (Double)sbMap.get("harvesting");
								tp = (Double) sbMap.get("transport");
								sbAmt = (Double) sbMap.get("pendingamt");
								accountnumber  = (String)sbMap.get("sbaccno");
								otherded = (Double) sbMap.get("otherded");
								
								if(accountnumber == "" || accountnumber == null || accountnumber == "null")
								{
									List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
									if(ryotBankList!=null)
									{
										AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
										accountnumber = agreement.getAccountnumber();
										logger.info("========getRyotSbAccNo() accountnumber=========="+accountnumber);
									}
								}
								bankcode  = (Integer)sbMap.get("bankcode");
							}
							
						    double loanAmt = 0.0;
						    double intrest = 0.0;
						    String loanAccountNumber = null;
						    List LoanList = ahFuctionalService.getLoanDetailsTemp(ryotCode,CanAccSlNo,season);
							logger.info("========getCaneUpdationCheckList() LoanList=========="+LoanList);
						    if (LoanList != null && !LoanList.isEmpty())
							{
						    	Map loanMap = new HashMap();
						    	loanMap = (Map) LoanList.get(0);
						    	if((Double)loanMap.get("amt")!=null)
						    	{
									loanAmt = (Double)loanMap.get("amt");
						    	}
						    	if((Double)loanMap.get("amt") != null)
						    	{
							    	intrest = (Double)loanMap.get("intrest");
						    	}
							}
						    
						    List LoanAccList = ahFuctionalService.getLoanDetailsByCanAccSlnoTemp(ryotCode,CanAccSlNo,season);
				    		if (LoanAccList != null && !LoanAccList.isEmpty())
							{
						    	Map loanAccMap = new HashMap();
						    	loanAccMap = (Map) LoanAccList.get(0);
						    	loanAccountNumber = (String)loanAccMap.get("accountnum");
						    	int branchCode = (Integer) loanAccMap.get("branchcode");
						    	List<Branch> branch = ahFuctionalService.getBranchDtls(branchCode);
								bankName = branch.get(0).getBranchname();
							}
						    
						    double advance = 0.0;
						    List AdvanceList = ahFuctionalService.getAdvanceDetailsTemp(ryotCode,CanAccSlNo,season);
							logger.info("========getCaneUpdationCheckList() AdvanceList=========="+AdvanceList);
						    if (AdvanceList != null && !AdvanceList.isEmpty())
							{
						    	Map advMap = new HashMap();
						    	advMap = (Map) AdvanceList.get(0);
						    	if((Double)advMap.get("advance")!=null)
						    	{
						    		advance = (Double)advMap.get("advance");
						    	}
							}
							
						    CaneAccDetailsTemp caneAccDetailsTemp = new CaneAccDetailsTemp();
							
						    caneAccDetailsTemp.setAccountnumber(accountnumber);
						    caneAccDetailsTemp.setBankcode(bankcode);
						    caneAccDetailsTemp.setCdc(cdc);
						    caneAccDetailsTemp.setUl(ul);
						    caneAccDetailsTemp.setHarvest(harvest);
						    caneAccDetailsTemp.setTp(tp);
						    caneAccDetailsTemp.setTotalamt(toalVal);
						    caneAccDetailsTemp.setLoan(loanAmt);
						    caneAccDetailsTemp.setRyotcode(ryotCode);
						    caneAccDetailsTemp.setRyotname(ryotname);
						    caneAccDetailsTemp.setSb(sbAmt);
						    caneAccDetailsTemp.setSuppliedqty(wt);
						    caneAccDetailsTemp.setCaneslno(CanAccSlNo);
						    caneAccDetailsTemp.setIntrest(intrest);
						    caneAccDetailsTemp.setAdvances(advance);
						    caneAccDetailsTemp.setLoanaccountnumber(loanAccountNumber);
						    caneAccDetailsTemp.setBankname(bankName);
						    caneAccDetailsTemp.setOtherded(otherded);
						    advanceLoansTablesList.add(caneAccDetailsTemp);
						    
						}
						reportFlag=commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(advanceLoansTablesList,UpdateList,AccountsList);
						logger.info("========getCaneUpdationCheckList() reportFlag=========="+reportFlag);
					}
				}
			}
			return reportFlag;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return updateFlag;
        }
	
	}
	@RequestMapping(value = "/getCaneUpdationCheckList", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getCaneUpdationCheckList(@RequestBody String jsonData) throws Exception 
	{	
		boolean isSuccess = false;
		List entityList = new  ArrayList();
		List UpdateList = new  ArrayList();
		List AccountsList = new  ArrayList();
		List caneValueSplitupSummaryList=caneAccountingFunctionalService.getCaneValueSplitup();
		if(caneValueSplitupSummaryList != null && caneValueSplitupSummaryList.size()>0)
		{
			//Truncating temp table
			ahFuctionalService.TruncateTemp("CaneAccDetailsTemp");
			
			if (caneValueSplitupSummaryList != null && !caneValueSplitupSummaryList.isEmpty())
			{
				for(int k=0;k<caneValueSplitupSummaryList.size();k++)
				{
					logger.info("========getCaneUpdationCheckList() k=========="+k);

					Map valMap = new HashMap();
					valMap = (Map) caneValueSplitupSummaryList.get(k);
					logger.info("========getCaneUpdationCheckList() valMap=========="+valMap);

					String ryotCode  = (String)valMap.get("ryotcode");
					String ryotname  = (String)valMap.get("ryotname");
					double wt = (Double)valMap.get("canesupplied");
					double toalVal = (Double)valMap.get("totalvalue");
					int  CanAccSlNo = (Integer)valMap.get("caneacslno");
					String season  = (String)valMap.get("season");
					
					//Added by DMurty on 29-12-2016
					Date datefrom = null;
					Date dateto = null;
					List DateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(CanAccSlNo,season);
					logger.info("getCaneUpdationCheckList()========DateList=========="+DateList);
					if(DateList != null && DateList.size()>0)
					{
						Map DateMap = new HashMap();
						DateMap = (Map) DateList.get(0);
						datefrom = (Date) DateMap.get("datefrom");
						dateto = (Date) DateMap.get("dateto");
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String datefrom1 = df.format(datefrom);
						String dateto1 = df.format(dateto);

						datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
						dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
					}
					logger.info("getCaneUpdationCheckList()========datefrom=========="+datefrom);
					logger.info("getCaneUpdationCheckList()========dateto=========="+dateto);
					
					
					String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
					logger.info("getCaneUpdationCheckList()========agreementNo=========="+agreementNo);

					
					
					double cdc = 0.0;
					double ul = 0.0;
					double harvest = 0.0;
					double tp = 0.0;
					double sbAmt = 0.0;
					String accountnumber = null;
					int bankcode = 0;
					String bankName = null;
					double otherded = 0.0;
				    List SbAccList = ahFuctionalService.getSbDetails(ryotCode,CanAccSlNo,season);
					logger.info("========getCaneUpdationCheckList() SbAccList=========="+SbAccList);
				    if (SbAccList != null && !SbAccList.isEmpty())
					{
				    	Map sbMap = new HashMap();
				    	sbMap = (Map) SbAccList.get(0);
						cdc = (Double) sbMap.get("cdcamt");
						ul = (Double) sbMap.get("ulamt");
						harvest = (Double)sbMap.get("harvesting");
						tp = (Double) sbMap.get("transport");
						sbAmt = (Double) sbMap.get("pendingamt");
						accountnumber  = (String)sbMap.get("sbaccno");
						otherded = (Double) sbMap.get("otherded");
						
						if(accountnumber == "" || accountnumber == null || accountnumber == "null")
						{
							accountnumber="8897857533";
/*
 
					Date datefrom = null;
					Date dateto = null;
					List DateList = caneAccountingFunctionalService.getCaneAccountSmryDetails(CanAccSlNo,season);
					logger.info("getCaneUpdationCheckList()========DateList=========="+DateList);
					if(DateList != null && DateList.size()>0)
					{
						Map DateMap = new HashMap();
						DateMap = (Map) DateList.get(0);
						datefrom = (Date) DateMap.get("datefrom");
						dateto = (Date) DateMap.get("dateto");
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String datefrom1 = df.format(datefrom);
						String dateto1 = df.format(dateto);

						datefrom=DateUtils.getSqlDateFromString(datefrom1,Constants.GenericDateFormat.DATE_FORMAT);
						dateto=DateUtils.getSqlDateFromString(dateto1,Constants.GenericDateFormat.DATE_FORMAT);
					}
					logger.info("getCaneUpdationCheckList()========datefrom=========="+datefrom);
					logger.info("getCaneUpdationCheckList()========dateto=========="+dateto);
							String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(datefrom,dateto,season,ryotCode);
							logger.info("getCaneUpdationCheckList()========agreementNo=========="+agreementNo);
							List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotCode,season,agreementNo);
							if(ryotBankList!=null)
							{
								AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
								accountnumber = agreement.getAccountnumber();
								logger.info("========getRyotSbAccNo() accountnumber=========="+accountnumber);
							}*/
						}
						bankcode  = (Integer)sbMap.get("bankcode");
					}
					
				    double loanAmt = 0.0;
				    double intrest = 0.0;
				    String loanAccountNumber = null;
				    List LoanList = ahFuctionalService.getLoanDetails(ryotCode,CanAccSlNo,season);
					logger.info("========getCaneUpdationCheckList() LoanList=========="+LoanList);
				    if (LoanList != null && !LoanList.isEmpty())
					{
				    	Map loanMap = new HashMap();
				    	loanMap = (Map) LoanList.get(0);
				    	if((Double)loanMap.get("amt")!=null)
				    	{
							loanAmt = (Double)loanMap.get("amt");
				    	}
				    	if((Double)loanMap.get("amt") != null)
				    	{
					    	intrest = (Double)loanMap.get("intrest");
				    	}
					}
				    
				    List LoanAccList = ahFuctionalService.getLoanDetailsByCanAccSlno(ryotCode,CanAccSlNo,season);
		    		if (LoanAccList != null && !LoanAccList.isEmpty())
					{
				    	Map loanAccMap = new HashMap();
				    	loanAccMap = (Map) LoanAccList.get(0);
				    	loanAccountNumber = (String)loanAccMap.get("accountnum");
				    	int branchCode = (Integer) loanAccMap.get("branchcode");
				    	List<Branch> branch = ahFuctionalService.getBranchDtls(branchCode);
						bankName = branch.get(0).getBranchname();
					}
				    
				    
				    double advance = 0.0;
				    List AdvanceList = ahFuctionalService.getAdvanceDetails(ryotCode,CanAccSlNo,season);
					logger.info("========getCaneUpdationCheckList() AdvanceList=========="+AdvanceList);
				    if (AdvanceList != null && !AdvanceList.isEmpty())
					{
				    	Map advMap = new HashMap();
				    	advMap = (Map) AdvanceList.get(0);
				    	if((Double)advMap.get("advance")!=null)
				    	{
				    		advance = (Double)advMap.get("advance");
				    	}
					}
					
				    CaneAccDetailsTemp caneAccDetailsTemp = new CaneAccDetailsTemp();
					
				    caneAccDetailsTemp.setAccountnumber(accountnumber);
				    caneAccDetailsTemp.setBankcode(bankcode);
				    caneAccDetailsTemp.setCdc(cdc);
				    caneAccDetailsTemp.setUl(ul);
				    caneAccDetailsTemp.setHarvest(harvest);
				    caneAccDetailsTemp.setTp(tp);
				    caneAccDetailsTemp.setTotalamt(toalVal);
				    caneAccDetailsTemp.setLoan(loanAmt);
				    caneAccDetailsTemp.setRyotcode(ryotCode);
				    caneAccDetailsTemp.setRyotname(ryotname);
				    caneAccDetailsTemp.setSb(sbAmt);
				    caneAccDetailsTemp.setSuppliedqty(wt);
				    caneAccDetailsTemp.setCaneslno(CanAccSlNo);
				    caneAccDetailsTemp.setIntrest(intrest);
				    caneAccDetailsTemp.setAdvances(advance);
				    caneAccDetailsTemp.setLoanaccountnumber(loanAccountNumber);
				    caneAccDetailsTemp.setBankname(bankName);
				    caneAccDetailsTemp.setOtherded(otherded);
				    entityList.add(caneAccDetailsTemp);
				    
				}
				isSuccess=commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
				logger.info("========getCaneUpdationCheckList() isSuccess=========="+isSuccess);

			}
		}
		return isSuccess;
	}
	
	
	
	@RequestMapping(value = "/getCaneToFA", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray  getCaneToFA(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONObject mainobject=new JSONObject();
		JSONArray mainarray=new JSONArray();
		
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		int caneslno=(Integer)jsonData.get("ftDate");	
		String paymentdate=null;
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Double totalValues=0.0;
		int i=1;
		for(i=1;i<=2;i++)
		{
			JSONObject subobject=new JSONObject();
			
			totalValues= ahFuctionalService.getCaneValueSplitUp(season,caneslno);
			Double totalSupply= ahFuctionalService.getTotalSupply(season,caneslno);
			Date date= ahFuctionalService.getDate(season,caneslno);
			Date paydate= ahFuctionalService.getPayDate(season,caneslno);
			paymentdate=DateUtils.formatDate(paydate,Constants.GenericDateFormat.DATE_FORMAT);
			
			String fdate=DateUtils.formatDate(date,Constants.GenericDateFormat.DATE_FORMAT);
			String frmdate[]=fdate.split("-");
			String p=frmdate[0];
			//int tdoc=Integer.parseInt(p);
			String tdc="C"+p;
			Double s=(double)Math.round(totalSupply * 1000d) / 1000d;
			Double totalValue=(double)Math.round(totalValues * 100d) / 100d;
			
			String description=s.toString()+"M.Ts Cane:WkDt"+fdate;
			
			subobject.put("TDC",i);
			subobject.put("TSERIAL",i);
			subobject.put("TDES",description);
			subobject.put("TDT",paymentdate);
			subobject.put("TAMOUNT",totalValue);
			subobject.put("TCOMP","3");
			subobject.put("TDB","51");
			subobject.put("TSUB_CODE","0");
			subobject.put("TPARTY"," ");
			subobject.put("TQTY","0.000");
			subobject.put("TADB","J");
			subobject.put("TDOC","C041");
			subobject.put("TUNIT_PRIC","0.0");
			if(i==1){
			subobject.put("TGEN", "4041");
			subobject.put("TDC","1");
			}
			if(i==2){
				subobject.put("TGEN", "1132");
				subobject.put("TDC","2");
			}
		 
			mainarray.add(subobject);
		}
		List dropdownList1= caneAccountingFunctionalService.getDedutionsData(season,caneslno);
		Double roundvalue=0.0;
		Double cdc=0.0;
		Double trans=0.0;
		Double ulamt=0.0;
		Double harv=0.0;
		Double od=0.0;
		Double lamt=0.0;
		Double sbac=0.0;
		Double grandtotal=0.0;
		Double advamt=0.0;
		Double loans=0.0;
		if (dropdownList1 != null && dropdownList1.size() > 0)
		{
			for (int j = 0;j<dropdownList1.size();j++)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList1.get(j);
				
				cdc = (Double) tempMap.get("cdc");
				cdc=(double)Math.round(cdc * 100d) / 100d;
				trans = (Double) tempMap.get("trans");
				trans=(double)Math.round(trans * 100d) / 100d;
				ulamt = (Double) tempMap.get("ulamt");
				ulamt=(double)Math.round(ulamt * 100d) / 100d;
				harv = (Double) tempMap.get("harv");
				harv=(double)Math.round(harv * 100d) / 100d;
				od = (Double) tempMap.get("od");
				od=(double)Math.round(od * 100d) / 100d;
				lamt = (Double) tempMap.get("lamt");
				advamt = (Double) tempMap.get("advamt");
				sbac = (Double) tempMap.get("sbac");
				sbac=(double)Math.round(sbac * 100d) / 100d;
				loans=advamt+lamt;
				loans=(double)Math.round(loans * 100d) / 100d;
				grandtotal=cdc+trans+ulamt+harv+od+lamt+advamt+sbac;
				grandtotal=(double)Math.round(grandtotal * 100d) / 100d;
				//roundvalue=grandtotal-totalValues;
				roundvalue=totalValues-grandtotal;
				roundvalue=(double)Math.round(roundvalue * 100d) / 100d;
			}
		}
		Date date= ahFuctionalService.getDate(season,caneslno);
	//	String pattern = "dd/MM/yyyy";
	//	SimpleDateFormat format = new SimpleDateFormat(pattern);
		String fdate=DateUtils.formatDate(date,Constants.GenericDateFormat.DATE_FORMAT);
		
		HashMap<Integer,String> zelcode=new HashMap<Integer,String>();
		zelcode.put(1,"1132");
		zelcode.put(2,"");
		zelcode.put(3,"1186");
		zelcode.put(4,"1132");
		zelcode.put(5,"1132");
		zelcode.put(6,"1132");
		zelcode.put(7,"1132");
		zelcode.put(8,"2300");
		zelcode.put(9,"1132");
		
		HashMap<Integer,String> descrition=new HashMap<Integer,String>();
		descrition.put(1,"CDC,BC,LoanRecov.week end"+fdate);
		descrition.put(2,"Round off value week end"+fdate);
		descrition.put(3,"CDC.week end"+fdate);
		descrition.put(4,"TRNS.week end"+fdate);
		descrition.put(5,"UNLOAD.week end"+fdate);
		descrition.put(6,"HARV.week end"+fdate);
		descrition.put(7,"ODED.week end"+fdate);
		descrition.put(8,"LOAN.week end"+fdate);
		descrition.put(9,"SBAC.week end"+fdate);
		HashMap<Integer,Double> totalvalue=new  HashMap<Integer,Double>();
		totalvalue.put(1,grandtotal);
		totalvalue.put(2,roundvalue);
		totalvalue.put(3,cdc);
		totalvalue.put(4,trans);
		totalvalue.put(5,ulamt);
		totalvalue.put(6,harv);
		totalvalue.put(7,od);
		totalvalue.put(8,advamt);
		totalvalue.put(9,sbac);
		for(int k=1;k<=9;k++)
		{
			JSONObject subobject=new JSONObject();
			//String description="CDC,BC,LoanRecov.week end"+fdate;
			if(k==1)
			{
			subobject.put("TSERIAL",1);
			}
			if(k!=1)
			{
				subobject.put("TSERIAL",k);
			}
			subobject.put("TDES",(String)descrition.get(k));
			subobject.put("TDT",paymentdate);
			subobject.put("TAMOUNT",(Double)totalvalue.get(k));
			if(k==2)
			{
				subobject.put("TAMOUNT",(Double)totalvalue.get(k));
			}
			subobject.put("TCOMP","3");
			subobject.put("TDB","51");
			subobject.put("TSUB_CODE","0");
			subobject.put("TPARTY"," ");
			if(k==1)
			{
				subobject.put("TDC","1");
			}
			else
			{
			subobject.put("TDC","2");
			}
			subobject.put("TSUB_CODE","0");
		    subobject.put("TGEN", (String)zelcode.get(k));
		    subobject.put("TDOC","C042");
		    subobject.put("TQTY","0.000");
		    subobject.put("TADB","J");
		    subobject.put("TUNIT_PRIC","0.0");
		    subobject.put("RDIFF",roundvalue);
            mainarray.add(subobject);
		}
		List dropdownList= caneAccountingFunctionalService.getCaneToFAData(season,caneslno);
	    logger.info("getCaneToFAData()========dropdownList=========="+dropdownList);
		if (dropdownList != null && dropdownList.size() > 0)
		{
			int p=10;
			for (int j= 0;j<dropdownList.size();j++)
			{
				/*Map tempMap = new HashMap();*/
				Map	tempMap10 = (Map) dropdownList.get(j);
				
				JSONObject subobject=new JSONObject();
				Integer bkcode = (Integer) tempMap10.get("branchcode");
				if(bkcode<= 9)
  				{
					String glcode = ahFuctionalService.getGlcode(bkcode);
					subobject.put("TGEN", glcode);
  				}
				else
				{
					Long glcode=ahFuctionalService.getgrcodefromBranch(bkcode);
					subobject.put("TGEN", glcode);
				}
				Double banktotal = ahFuctionalService.bankwiseData(season,caneslno,bkcode);
				Double bktotal=0.0;
				if(banktotal !=null)  
				{
					 bktotal=(double)Math.round(banktotal * 100d) / 100d;
				}
				String description="Loan Recov. of week end"+fdate;
				//BigDecimal glcode=(BigDecimal)tempMap10.get("glcode");
				subobject.put("TSERIAL",p);
				subobject.put("TDT", paymentdate);
				subobject.put("TDES",description);
				subobject.put("TPARTY"," ");
				subobject.put("TAMOUNT",bktotal);
				subobject.put("TCOMP","3");
				subobject.put("TDB","51");
				subobject.put("TDOC","C042");
				subobject.put("TSUB_CODE","0");
				subobject.put("TDC","2");
				subobject.put("TQTY","0.000");
				subobject.put("TADB","J");
				subobject.put("TUNIT_PRIC","0.0");
		       mainarray.add(subobject);
		       p++;
		 	}
		}
		return mainarray;
	}

	

	@RequestMapping(value = "/getCaneAccountingPeriodsTemp", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getCaneAccountingPeriodsTemp(@RequestBody String season) throws Exception
	{
		/*org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");*/
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		String strQuery=null;
		try 
		{
			List dropdownList= caneAccountingFunctionalService.getCaneAccountingPeriodsTemp(season);
			if (dropdownList != null && dropdownList.size() > 0)
			{
				for (int i = 0;i<dropdownList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) dropdownList.get(i);
					int caneAccSlNo = (Integer) tempMap.get("caneacslno");
					Date datefrom = (Date) tempMap.get("datefrom");
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strdatefrom = df.format(datefrom);
					Date dateto = (Date) tempMap.get("dateto");
					String strdateto = df.format(dateto);
					String finalDate = strdatefrom+" To "+strdateto;
					
					jsonObj = new JSONObject();
					jsonObj.put("foCode", finalDate);
					jsonObj.put("id", caneAccSlNo);


					jArray.add(jsonObj);
				}
			}
		}
		catch (Exception e)
		{
	        logger.info(e.getCause(), e);
	        return null;
		}
	    logger.info("getCaneAccountingPeriods()========jArray=========="+jArray);
		return jArray;
	}
	//Added by sahadeva 17-01-2017
	@RequestMapping(value = "/getRyotLedgerData", method = RequestMethod.POST)
	public @ResponseBody	List getRyotLedgerData(@RequestBody JSONObject jsonData)throws Exception 
	{
		String season = (String) jsonData.get("season");
		String ryotcode = (String) jsonData.get("ryotcode");
		String datefrom=(String)jsonData.get("fromdate");
		String dateto=(String)jsonData.get("todate");
		Date date1=DateUtils.getSqlDateFromString(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
		Date date2=DateUtils.getSqlDateFromString(dateto,Constants.GenericDateFormat.DATE_FORMAT);
		String ryotname =null;
		//String ryotCode=null;
		List RyotList = new ArrayList();
		HashMap hm= null;
		
		List<Ryot> ryot= ryotService.getRyotByRyotCode(ryotcode);
		if(ryot != null && ryot.size()>0)
		{
			hm= new HashMap();
			ryotname = ryot.get(0).getRyotname();
			String relativename=ryot.get(0).getRelativename();
			Double supplyQty = ahFuctionalService.GetRyotSupply(season,ryotcode);
			Double loanAmount = ahFuctionalService.getTotalLoanAmt(season,ryotcode);
			String village=null;
			String mandal=null;
			String pincode=null;
			String sql3="select v.village,m.mandal,v.pincode  from AgreementDetails a,village v,mandal m where a.ryotcode='"+ryotcode+"'  and a.villagecode=v.villagecode and a.mandalcode=m.mandalcode ";
			List ls3=hibernateDao.findBySqlCriteria(sql3);
			if(ls3.size()>0 && ls3!=null)
			{
				  village=(String)((Map<Object,Object>)ls3.get(0)).get("village");
				  mandal=(String)((Map<Object,Object>)ls3.get(0)).get("mandal");
				  pincode=(String)((Map<Object,Object>)ls3.get(0)).get("pincode");
			}
			
			//Added by DMurty on 24-02-2017
			Double tpAllowance = ahFuctionalService.getTpAllowance(season,ryotcode);
			hm.put("village",village);
			hm.put("pincode",pincode);
			hm.put("mandal",mandal);
			hm.put("ryotname",ryotname);
			hm.put("ryotCode",ryotcode);
			hm.put("supplyQty", new DecimalFormat("#.###").format(supplyQty));
			hm.put("advAmount", new DecimalFormat("#.##").format(loanAmount));
			//Added by DMurty on 24-02-2017
			hm.put("tpAllowance", new DecimalFormat("#.##").format(tpAllowance));
			//Added by sahadeva  on 21-03-2017
			hm.put("relativeName",relativename);
			RyotList.add(hm);
		}
		return RyotList;
	}
	
	//added by naidu on 04-02-2017
	@RequestMapping(value = "/getCaneAccCumulativeCheckList", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getCaneAccCumulativeCheckList(@RequestBody String jsonData) throws Exception 
	{	
		boolean isSuccess = false;
		List entityList = new  ArrayList();
		List UpdateList = new  ArrayList();
		List caneAccDetailsTempList=caneAccountingFunctionalService.getCaneAccDetailsTemp();
		if(caneAccDetailsTempList != null && caneAccDetailsTempList.size()>0)
		{
			//Truncating temp table
			ahFuctionalService.TruncateTemp("CaneAccDetailsTempNew");
			
				for(int k=0;k<caneAccDetailsTempList.size();k++)
				{
					logger.info("========getCaneAccCumulativeCheckList() k=========="+k);

					Map valMap = new HashMap();
					valMap = (Map) caneAccDetailsTempList.get(k);
					logger.info("========getCaneUpdationCheckList() valMap=========="+valMap);
					String ryotCode  = (String)valMap.get("ryotcode");
					String ryotname  = (String)valMap.get("ryotname");
					double supqty = (Double)valMap.get("supqty");
					double totalamount = (Double)valMap.get("totalamount");
					double cdc = (Double)valMap.get("cdc");
					double ul = (Double)valMap.get("ul");
					double transport = (Double)valMap.get("transport");
					double harvest = (Double)valMap.get("harvest");
					double otherded = (Double)valMap.get("otherded");
					double loan = (Double)valMap.get("loan");
					double intrest = (Double)valMap.get("intrest");
					double  advances = (Double)valMap.get("advances");
					double  sb  = (Double)valMap.get("sb");
					
				    double ploanAmt = ahFuctionalService.getTotalLoanAmt("2016-2017",ryotCode);
				    double padvance = ahFuctionalService.getPrincipleAdvAmt("2016-2017",ryotCode);
					
				    CaneAccDetailsTempNew caneAccDetailsTempNew = new CaneAccDetailsTempNew();
				    caneAccDetailsTempNew.setAdvances(advances);
				    caneAccDetailsTempNew.setCdc(cdc);
				    caneAccDetailsTempNew.setHarvest(harvest);
				    caneAccDetailsTempNew.setIntrest(intrest);
				    caneAccDetailsTempNew.setLoan(loan);
				    caneAccDetailsTempNew.setOtherded(otherded);
				    caneAccDetailsTempNew.setPrincipleadvances(padvance);
				    caneAccDetailsTempNew.setPrincipleloan(ploanAmt);
				    caneAccDetailsTempNew.setRyotcode(ryotCode);
				    caneAccDetailsTempNew.setRyotname(ryotname);
				    caneAccDetailsTempNew.setSb(sb);
				    caneAccDetailsTempNew.setSuppliedqty(supqty);
				    caneAccDetailsTempNew.setTotalamt(totalamount);
				    caneAccDetailsTempNew.setTp(transport);
				    caneAccDetailsTempNew.setUl(ul);
				   
				    entityList.add(caneAccDetailsTempNew);
				    
				}
				isSuccess=commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
				logger.info("========getCaneAccCumulativeCheckList() isSuccess=========="+isSuccess);

			}
		return isSuccess;
	}
	
	
}
