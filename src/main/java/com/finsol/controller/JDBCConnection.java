package com.finsol.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;

public class JDBCConnection {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    Connection conn = null;
	    try 
	    {	    	
	        try
	        {	 
	             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	        } 
	        catch (ClassNotFoundException e) 
	        {
	        	System.out.println("Please include Classpath Where your SQL Driver is located");
	            e.printStackTrace();
	        }  
	        conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=sugarerpdb;integratedSecurity=false;encrypt=false;trustServerCertificate=false;","admin","finsol@1");
		    if (conn != null)
		    {
		        System.out.println("Database Connected");
		    }
		    else
		    {
		        System.out.println(" connection Failed ");
		    }
	    }
	    catch (Exception sqlExp) 
	    {
	    	System.out.println( "Exception::" + sqlExp.toString());
	    } 
	    finally 
	    {
	    	try
	        {
	    		if (conn != null)
	            {
	                conn.close();
	                conn = null;
	            }
	        }
	        catch (SQLException expSQL)
	        {
	            System.out.println("SQLExp::CLOSING::" + expSQL.toString());
	        }
	     }

	}


}
