/*package com.finsol.controller;
 
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.ParallelPort;
import javax.comm.PortInUseException;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleHtmlReportConfiguration;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.model.jasper.form.JasperInputForm;
import com.finsol.service.JasperReportService;

@Controller
public class JasperReportController {
	
	private static OutputStream outputStream;
	private static InputStream inputStream;
	private static ParallelPort parallelPort;
	private static CommPortIdentifier port;
	static byte dat = 0x02;
	public static final String PARALLEL_PORT = "LPT1";
	private static final Logger logger = Logger.getLogger(JasperReportController.class);
	@Autowired
	private JasperReportService jasperReportService;
 
    @ModelAttribute("jasperRptFormats")
    public ArrayList getJasperRptFormats()
    {
        ArrayList < String> jasperRptFormats = new ArrayList<String>();
        jasperRptFormats.add("Html");
        jasperRptFormats.add("PDF");
        jasperRptFormats.add("LPT");
        jasperRptFormats.add("Dynamic");
 
        return jasperRptFormats;
    }   
    
    
	@RequestMapping(value = "/geBranchReport", method = RequestMethod.POST)
	public @ResponseBody
	String geBranchReport(@RequestBody JSONObject jsonData) throws Exception 
	{
		Integer bankId=(Integer)jsonData.get("AddedBankName");

		List branchlist=jasperReportService.getBranchDetailsByBankCode(bankId);
		logger.info("branch list-----"+branchlist.toString());
		

		return "ok";
	}
 
@RequestMapping(value = "/loadJasper", method = RequestMethod.GET)
public String loadSurveyPg(@ModelAttribute("jasperInputForm") JasperInputForm jasperInputForm,Model model) 
{
	System.out.println("in jasper----");

    model.addAttribute("JasperInputForm", jasperInputForm);

        return "loadJasper";
}
 
@RequestMapping(value = "/generateReport", method = RequestMethod.POST)
public String generateReport(@Valid @ModelAttribute("jasperInputForm") JasperInputForm jasperInputForm,BindingResult result,Model model, HttpServletRequest request,HttpServletResponse response) throws ParseException {
 
    if (result.hasErrors()) {
        System.out.println("validation error occured in jasper input form");
        return "loadJasper";
 
    }
 
    String reportFileName = "JREmp1";
 
    
    Connection conn = null;
    try {
        try {
 
             Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }  
 
         conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/davdb","admin","finsol");
 
     if (conn != null)
     {
         System.out.println("Database Connected");
     }
     else
     {
         System.out.println(" connection Failed ");
     }
 
     
     
          String rptFormat = jasperInputForm.getRptFmt();
          String frmDate=jasperInputForm.getFromDate();
          String tDate=jasperInputForm.getToDate();
          String QString=jasperInputForm.getQueryString();
          
          //String noy = jasperInputForm.getNoofYears();
          //String dt = jasperInputForm.getJoinDate();
         String QueryString=QString+" from emp_master where Doj between '"+frmDate+"' and  '"+tDate+"'";
          System.out.println("rpt format--- " + rptFormat);
          System.out.println("frmDate--- " + frmDate);
          System.out.println("tDate--- " + tDate);
          System.out.println("QString-- " + QString);
          System.out.println("QueryString-- " + QueryString);
          //System.out.println("no of years " + noy);
          //System.out.println("input dt----" + dt);
         // String[] SplitString=QString.split(",");
          
          
          
         // String[] st=dt.split("/");
		// String MysqlDateFormat=st[2]+"-"+st[0]+"-"+st[1];
          
		// System.out.println("New  MysqlDateFormat Date----" + MysqlDateFormat);  

          //System.out.println("SplitString[0]-- " + SplitString[0]);
          //System.out.println("SplitString[1]-- " + SplitString[1]);
          //System.out.println("SplitString[2]-- " + SplitString[2]);
          //System.out.println("SplitString[3]-- " + SplitString[3]);
           //Parameters as Map to be passed to Jasper
           HashMap<String,Object> hmParams=new HashMap<String,Object>();
           hmParams.put("Title", "Employees List");
           hmParams.put("dt", frmDate);
           hmParams.put("qrs", QueryString);
           
           //hmParams.put("col1", SplitString[0]);
          // hmParams.put("col2", SplitString[1]);
          // hmParams.put("col3", SplitString[2]);
          // hmParams.put("col4", SplitString[3]);
          // hmParams.put("noy", noy);
           
           //String ecodedValue1 = URLEncoder.encode(QueryString, "UTF-8");
           
           
          
           
           
           System.out.println("qrs----------" + hmParams.toString());
          // System.out.println("QueryString-------" + QueryString);
            JasperReport jasperReport = getCompiledFile(reportFileName, request);
 
        if (rptFormat.equalsIgnoreCase("html") ) {
 
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hmParams, conn);
            generateReportHtml(jasperPrint, request, response); // For HTML report
 
        }
 
        else if  (rptFormat.equalsIgnoreCase("pdf") )  {
        	
        	 
        	System.out.println("In pDF-------" + hmParams.toString());
            generateReportPDF(response, hmParams, jasperReport, conn); // For PDF report
            System.out.println("In pDF-44------" + hmParams.toString());
 
            }
        else if  (rptFormat.equalsIgnoreCase("LPT") )  {
        	generateReportLPT(response, hmParams, jasperReport, conn);
        	
        }
 
        else if  (rptFormat.equalsIgnoreCase("Dynamic") )  {
        	
        	//getDynamicJasper(response,conn);
        	
        }
       } catch (Exception sqlExp) {
 
           System.out.println( "Exception::" + sqlExp.toString());
 
       } finally {
 
            try {
 
            if (conn != null) {
                conn.close();
                conn = null;
            }
 
            } catch (SQLException expSQL) {
 
                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
 
            }
 
           }
 
return null;
 
}

public String getDynamicJasper(HttpServletResponse response,Connection conn){
	 String result = "";
	 
	try{

	 String query = "select e.empId,e.empName from Employee e";
	 String title = "List of Employees";
	 String fileName = "EmployeeList";
	 
	DynamicJasperReport  djr = new DynamicJasperReport();
	result = djr.getDynamicJasperReport(query, title, fileName, response,conn);
	 System.out.println("result : "+result);
	 
	}catch(Exception e){
	 System.out.println("Error in getDynamicJasper : "+e.getMessage());
	 result = "error";
	 e.printStackTrace();
	 }
	 return result;
	 }
 
private JasperReport getCompiledFile(String fileName, HttpServletRequest request) throws JRException {
    System.out.println("path " + request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
    File reportFile = new File( request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
    // If compiled file is not found, then compile XML template
    if (!reportFile.exists()) {
               JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jrxml"),request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
               
        }
        //JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromLocation(reportFile.getPath());
    JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
       return jasperReport;
    } 
 
    private void generateReportHtml( JasperPrint jasperPrint, HttpServletRequest req, HttpServletResponse resp) throws IOException, JRException {
         HtmlExporter exporter=new HtmlExporter();
         List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
         jasperPrintList.add(jasperPrint);
         exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
         exporter.setExporterOutput( new SimpleHtmlExporterOutput(resp.getWriter()));
         SimpleHtmlReportConfiguration configuration =new SimpleHtmlReportConfiguration();
         exporter.setConfiguration(configuration);
          exporter.exportReport();
 
    }
 
    
    private void generateReportPDF (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException {
        byte[] bytes = null;
        bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
        resp.reset();
        resp.resetBuffer();
        resp.setContentType("application/pdf");
        resp.setContentLength(bytes.length);
        ServletOutputStream ouputStream = resp.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        ouputStream.flush();
        ouputStream.close();
    } 
    
    
    
    
    private void generateReportLPT (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException
    {
    	 System.out.println("Port : " + PARALLEL_PORT + " is detected");
    	 
    	 try {
    	 
    		 Enumeration port_list = CommPortIdentifier.getPortIdentifiers();

    		 System.out.println ("port_list port:" + port_list.nextElement());
    		 
    		 while (port_list.hasMoreElements())
    		 {
    		 CommPortIdentifier port_id = (CommPortIdentifier)port_list.nextElement();

    		 if (port_id.getPortType() == CommPortIdentifier.PORT_SERIAL)
    		 {
    		 System.out.println ("Serial port:" + port_id.getName());
    		 }
    		 else if (port_id.getPortType() == CommPortIdentifier.PORT_PARALLEL)
    		 {
    		 System.out.println ("Parallel port: " + port_id.getName());
    		 }
    		 else
    		 System.out.println ("Other port:" + port_id.getName());
    		 }
 	 
    			//byte[] bytes = null;
  		 
    		 port = CommPortIdentifier.getPortIdentifier(PARALLEL_PORT);
        	 //port identified
             System.out.println("Port identified : " + port);
             // open the parallel port --
             //port(App name, timeout);
             parallelPort = (ParallelPort) port.open("LX300", 1000);
             //parallelPort = (ParallelPort) port.open("LX300", 90);
             //port opened
             System.out.println("Port opened : " + parallelPort);
             outputStream = parallelPort.getOutputStream();
             
    	 Map parameters1 = new HashMap();
JRTextExporter exporter = new JRTextExporter();
File sourceFile = new File("C:\\Tomcat7\\webapps\\JasperReport\\jasper\\JREmp1.jasper");
//File sourceFile = new File("C:\\JREmp1.jasper");
 System.out.println("get path------------------"+sourceFile.getAbsolutePath());
 JasperReport report = (JasperReport) JRLoader.loadObject(sourceFile);
 JasperPrint jasperPrint = JasperFillManager.fillReport(report,parameters, conn);
 
	File destFile = new File(sourceFile.getParent(), jasperPrint.getName() + ".txt");
	FileWriter fr=new FileWriter(destFile);
System.out.println("get path------------------"+destFile.getAbsolutePath());
exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
exporter.setParameter(JRTextExporterParameter.BETWEEN_PAGES_TEXT ,"\f");       
exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT        , new Float(798));
exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH         , new Float(581));
exporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH    , new Float(  7));
exporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT   , new Float( 14));
//exporter.setParameter(JRExporterParameter.OUTPUT_WRITER,resp.getWriter());
//exporter.setParameter(JRExporterParameter.OUTPUT_WRITER,fr);
exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);

exporter.exportReport();

outputStream.flush();
outputStream.close(); 
    	 
 
    	
    	//byte[] bytes = null;
    	
        //bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
       // System.out.println("Bytes Length--------- " + bytes.length);
        
       // System.out.println("Bytes Length--------- " + bytes.length);
        
        try {
        	
            FileOutputStream os = new FileOutputStream("LPT1");
            //wrap stream in "friendly" PrintStream
            PrintStream ps = new PrintStream(os);
 
            //print text here
            ps.println("Hello world!");
 
            //form feed -- this is important
            //Without the form feed, the text will simply sit
            // in print buffer until something else gets printed.
            ps.print("\f");
            //flush buffer and close
            ps.close();
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e);
        }
        
    	port = CommPortIdentifier.getPortIdentifier(PARALLEL_PORT);
    	 //port identified
         System.out.println("Port identified : " + port);
         // open the parallel port --
         //port(App name, timeout);
         parallelPort = (ParallelPort) port.open("LX300", 1000);
         //parallelPort = (ParallelPort) port.open("LX300", 90);
         //port opened
         System.out.println("Port opened : " + parallelPort);
         outputStream = parallelPort.getOutputStream();
         //get output
         System.out.println("Out put taken : " + outputStream);
         //outputStream.write(bytes);'
         outputStream.write(bytes, 0, bytes.length);
        // ouputStream.write(bytes, 0, bytes.length);

         File file = new File("E:/myfile.txt");
         FileOutputStream fos = new FileOutputStream(file);

         if (!file.exists()) {
    	     file.createNewFile();
    	  }         
        // byte[] bytesArray = mycontent.getBytes();

   	  fos.write(bytes);
   	  fos.flush();
            	  
         //data written
         System.out.println("Data Written : " + bytes);
         System.out.println("Bytes Length--------- " + bytes.length);
         outputStream.flush();
         outputStream.close();
         

         
         
         
         
         
         
        
    	 } catch (NoSuchPortException nspe) {
    	        System.out.println("\nPrinter Port LPT1 not found :     NoSuchPortException.\nException:\n" + nspe + "\n");
    	    } catch (PortInUseException piue) {
    	        System.out.println("\nPrinter Port LPT1 is in use : " +     "PortInUseException.\nException:\n" + piue + "\n");
    	    } catch (IOException ioe) {
    	        System.out.println("\nPrinter Port LPT1 failed to write : " + "IOException.\nException:\n" + ioe + "\n");
    	    }
    	    
    	  catch (Exception e) {
    	        System.out.println("\nFailed to open Printer Port LPT1 with exception : " + e +   "\n");
    	    } 
    	 finally {
    	        if (port != null && port.isCurrentlyOwned()) {
    	            parallelPort.close();
    	        }
    	        System.out.println("Closed all resources.\n");
    	    }
    	

    } 
 
}*/