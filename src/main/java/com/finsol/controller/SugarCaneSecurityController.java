package com.finsol.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.CompanyBranchesBean;
import com.finsol.bean.CompanyDetailsBean;
import com.finsol.bean.DepartmentMasterBean;
import com.finsol.bean.EmployeesBean;
import com.finsol.bean.RoleScreensBean;
import com.finsol.bean.RolesBean;
import com.finsol.bean.ScreenFieldsBean;
import com.finsol.bean.ScreensBean;
import com.finsol.bean.SetupDetailsBean;
import com.finsol.model.CaneReceiptSeasonSmry;
import com.finsol.model.CompanyBranches;
import com.finsol.model.CompanyDetails;
import com.finsol.model.DepartmentMaster;
import com.finsol.model.Employees;
import com.finsol.model.RoleScreens;
import com.finsol.model.Roles;
import com.finsol.model.Ryot;
import com.finsol.model.ScreenFields;
import com.finsol.model.Screens;
import com.finsol.model.SetupDetails;
import com.finsol.service.AuditTrailService;
import com.finsol.service.CommonService;
import com.finsol.service.CompanyDetailsService;
import com.finsol.service.DepartmentMasterServiceImpl;
import com.finsol.service.EmployeesService;
import com.finsol.service.RolesService;
import com.finsol.service.ScreenFieldsService;
import com.finsol.service.ScreensService;
import com.finsol.service.SetupDetailsService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
import com.finsol.utils.Digester;

/**
 * @author Rama Krishna
 *
 */
@Controller
public class SugarCaneSecurityController 
{
	
	private static final Logger logger = Logger.getLogger(SugarCaneSecurityController.class);
	org.json.JSONObject jsonResponse = null;

	@Autowired
	private DepartmentMasterServiceImpl departmentMasterServiceImpl;
	
	@Autowired
	private EmployeesService employeesService;
		
	@Autowired
	private Digester digester;
	
	@Autowired
	private CompanyDetailsService companyDetailsService;
	
	@Autowired
	private ScreensService screensService;
	
	@Autowired
	private RolesService rolesService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private SetupDetailsService setupDetailsService;
	
	@Autowired
	private ScreenFieldsService screenFieldsService;
	
	@Autowired
	private AuditTrailService auditTrailService;
	
	
	
	@RequestMapping(value = "/saveDepartmentMaster", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody boolean saveDepartmentMaster(@RequestBody String jsonData) throws Exception 
	 {
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			DepartmentMasterBean departmentMasterBean = new ObjectMapper().readValue(jsonData, DepartmentMasterBean.class);
			DepartmentMaster departmentMaster = prepareModel(departmentMasterBean);
			entityList.add(departmentMaster);
			if(departmentMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(departmentMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(departmentMasterBean.getId(),departmentMaster,departmentMasterBean.getScreenName());		
			}
			
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
	
			//departmentMasterServiceImpl.addDepartment(departmentMaster);
			//ObjectMapper ob = new ObjectMapper();
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	@RequestMapping(value = "/getAllDepartments", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody List<DepartmentMasterBean> getAllDepartments(@RequestBody String jsonData) throws Exception 
	 {

		//EmployeeBean employeeBean = new ObjectMapper().readValue(jsonData, EmployeeBean.class);
		List<DepartmentMasterBean> departmentMasterBeanBens=prepareListofBean(departmentMasterServiceImpl.listDepartments());
		logger.info("==== departmentMasterBeanBens.size()========"+departmentMasterBeanBens.size());
		return departmentMasterBeanBens;
	}


	
	private List<DepartmentMasterBean> prepareListofBean(List<DepartmentMaster> departmentMasters){
		List<DepartmentMasterBean> beans = null;
		if(departmentMasters != null && !departmentMasters.isEmpty()){
			beans = new ArrayList<DepartmentMasterBean>();
			DepartmentMasterBean bean = null;
			for(DepartmentMaster departmentMaster : departmentMasters){
				bean = new DepartmentMasterBean();
				
				bean.setId(departmentMaster.getId());
				bean.setDeptCode(departmentMaster.getDeptCode());
				String description = departmentMaster.getDescription();
				if ("".equals(description) || "null".equals(description) || (description == null)) 
				{
					description = "NA";
				}
				else
				{
					description = description.toUpperCase();
				}
				bean.setDescription(description);
				bean.setDepartment(departmentMaster.getDepartment());
				bean.setDeptShortcut(departmentMaster.getDeptShortcut());
				bean.setLocation(departmentMaster.getLocation());
				bean.setContactPerson(departmentMaster.getContactPerson());
				bean.setExtension(departmentMaster.getExtension());
				bean.setDirectNumber(departmentMaster.getDirectNumber());
				bean.setStatus(departmentMaster.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	private DepartmentMaster prepareModel(DepartmentMasterBean departmentMasterBean){
		DepartmentMaster departmentMaster = new DepartmentMaster();
		
		int deptCode = commonService.getMaxIDforColumn("DepartmentMaster","deptCode");
		String modifyFlag = departmentMasterBean.getModifyFlag();
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			departmentMaster.setDeptCode(departmentMasterBean.getDeptCode());
		}
		else
		{
			departmentMaster.setDeptCode(deptCode);
		}
			
		departmentMaster.setId(departmentMasterBean.getId());
		departmentMaster.setDeptCode(departmentMasterBean.getDeptCode());
		String description = departmentMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		departmentMaster.setDescription(description);
		departmentMaster.setDepartment(departmentMasterBean.getDepartment().toUpperCase());
		departmentMaster.setDeptShortcut(departmentMasterBean.getDeptShortcut().toUpperCase());
		String location=departmentMasterBean.getLocation();
		
		if ("".equals(location) || "null".equals(location) || (location == null)) 
		{
			location = "NA";
		}
		else
		{
			location = location.toUpperCase();
		}
		departmentMaster.setLocation(location);
		departmentMaster.setContactPerson(departmentMasterBean.getContactPerson().toUpperCase());
		departmentMaster.setExtension(departmentMasterBean.getExtension());
		departmentMaster.setDirectNumber(departmentMasterBean.getDirectNumber());
		departmentMaster.setStatus(departmentMasterBean.getStatus());
		//employee.setId(null);
		return departmentMaster;
	}
	

	//------------------------------Employees----------------------------------------//
	
	@RequestMapping(value = "/saveEmployees", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public @ResponseBody boolean saveEmployees(@RequestBody String jsonData) throws Exception
    {
	 	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveEmployees() from SecurityController========"+ jsonData.toString());
		    EmployeesBean employeesBean = new ObjectMapper().readValue(jsonData, EmployeesBean.class);
		    Employees employees = prepareModelForEmployees(employeesBean);
		    logger.info("====employees ========"+employees.toString());
		    entityList.add(employees);
		    if(employeesBean.getEmployeeID()!=null)
			{
				//boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("User Master","ok");
		    	boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(employeesBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(employeesBean.getEmployeeID(),employees,employeesBean.getScreenName());		
			}
		    
		    
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
		   // employeesService.addEmployees(employees);
		   return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
     }
		 
		 
		 private Employees prepareModelForEmployees(EmployeesBean employeesBean)
		 {
			 Employees employees = new Employees();				
				
			 employees.setEmployeeid(employeesBean.getEmployeeID());
			 employees.setEmployeename(employeesBean.getEmployeeName().toUpperCase());
			 employees.setDesignationcode(employeesBean.getDesignationCode());
			 employees.setDeptcode(employeesBean.getDeptCode());
			 employees.setLoginid(employeesBean.getLoginID());
			 employees.setDateofbirth(DateUtils.getSqlDateFromString(employeesBean.getDateOfBirth(),Constants.GenericDateFormat.DATE_FORMAT)); 
			 employees.setEmailid(employeesBean.getEmailID());
			 employees.setGender(employeesBean.getGender());
			 employees.setMaritalstatus(employeesBean.getMaritalStatus());
			 employees.setPhoneno(employeesBean.getPhoneNo());
			 employees.setMobileno(employeesBean.getMobileNo());
			 employees.setPhoneno2(employeesBean.getPhoneNo2());
			 employees.setRoleid(employeesBean.getRoleID());
			 employees.setStatus(employeesBean.getStatus());
			 employees.setPassword(digester.digest("ssserpuser"));
			 employees.setEmpStatus(employeesBean.getEmpStatus());
				
			 //employee.setId(null);
			 return employees;
		}
		 
		 
		 @RequestMapping(value = "/getAllEmployees", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
		 public @ResponseBody List<EmployeesBean> getAllEmployees(@RequestBody String jsonData) throws Exception 
		 {
			List<EmployeesBean> employeesBeanBens=prepareListofEmployeesBean(employeesService.listEmployee());
			return employeesBeanBens;
		}
		 private List<EmployeesBean> prepareListofEmployeesBean(List<Employees> employee)
		 {
				List<EmployeesBean> beans = null;
				if(employee != null && !employee.isEmpty())
				{
					beans = new ArrayList<EmployeesBean>();
					EmployeesBean bean = null;
					for(Employees employees : employee)
					{
						bean = new EmployeesBean();					
						bean.setEmployeeID(employees.getEmployeeid());
						bean.setEmployeeName(employees.getEmployeename());
						bean.setDesignationCode(employees.getDesignationcode());
						bean.setDeptCode(employees.getDeptcode());
						bean.setLoginID(employees.getLoginid());
						bean.setDateOfBirth(DateUtils.formatDate(employees.getDateofbirth(),Constants.GenericDateFormat.DATE_FORMAT));
						bean.setEmailID(employees.getEmailid());
						bean.setGender(employees.getGender());
						bean.setMaritalStatus(employees.getMaritalstatus());
						bean.setPhoneNo(employees.getPhoneno());
						bean.setMobileNo(employees.getMobileno());
						bean.setPhoneNo2(employees.getPhoneno());
						bean.setRoleID(employees.getRoleid());
						bean.setStatus(employees.getStatus());
						bean.setEmpStatus(employees.getEmpStatus());
						beans.add(bean);
					}
				}
				return beans;
			}

		 @RequestMapping(value = "/getAllEmployeesForUser", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody
			String getAllEmployeesForUser(@RequestBody String jsonData) throws Exception
			{
			 	EmployeesBean employeesBean =null;				
			 	jsonResponse = new org.json.JSONObject();
				
				Employees employees=employeesService.getEmployees(Integer.parseInt(jsonData));
			
				if(employees!=null)
					employeesBean =prepareBeanForEmployees(employees);
					
				org.json.JSONObject advanceJson=new org.json.JSONObject(employeesBean);
				jsonResponse.put("employees",advanceJson);	
				
				
				return jsonResponse.toString();
			}
		 private EmployeesBean prepareBeanForEmployees(Employees employees) 
		{
				
			 	EmployeesBean bean = new EmployeesBean();
				bean.setEmployeeID(employees.getEmployeeid());
				bean.setEmployeeName(employees.getEmployeename().toUpperCase());
				bean.setDesignationCode(employees.getDesignationcode());
				bean.setDeptCode(employees.getDeptcode());
				bean.setLoginID(employees.getLoginid());
				bean.setDateOfBirth(DateUtils.formatDate(employees.getDateofbirth(),Constants.GenericDateFormat.DATE_FORMAT)); 
				//bean.setDateOfBirth(employees.getDateofbirth().toString());
				bean.setEmailID(employees.getEmailid());
				bean.setGender(employees.getGender());
				bean.setMaritalStatus(employees.getMaritalstatus());
				bean.setPhoneNo(employees.getPhoneno());
				bean.setMobileNo(employees.getMobileno());
				bean.setPhoneNo2(employees.getPhoneno2());
				bean.setRoleID(employees.getRoleid());
				bean.setStatus(employees.getStatus());
				bean.setEmpStatus(employees.getEmpStatus());

			return bean;
		}


		//------------------------------CompanyDetails----------------------------------------//
	@RequestMapping(value = "/saveCompanyDetails", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveCompanyDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException	
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			JSONObject companyDetailsData=(JSONObject) jsonData.get("formData");
			JSONArray companybranchData=(JSONArray) jsonData.get("gridData");			
			
			CompanyDetailsBean companyDetailsBean = new ObjectMapper().readValue(companyDetailsData.toString(), CompanyDetailsBean.class);
		    CompanyDetails companyDetails = prepareModelForCompanyDetails(companyDetailsBean);
		    entityList.add(companyDetails);
		  
		    if(companyDetailsBean.getCompanyId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("COMPANY MASTER","ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(companyDetailsBean.getCompanyId(),companyDetails,"COMPANY MASTER");		
			}
		    

		    for(int i=0;i<companybranchData.size();i++)
			{
		    	CompanyBranchesBean companyBranchesBean = new ObjectMapper().readValue(companybranchData.get(i).toString(), CompanyBranchesBean.class);
		    	CompanyBranches companyBranches = prepareModelForCompanyBranch(companyBranchesBean,companyDetailsBean);
			    entityList.add(companyBranches);
		    
			    if(companyBranchesBean.getBranchId()!=null)
				{
					boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("COMPANY MASTER","ok");
					if(isAuditTrailRequired)
						auditTrailService.updateAuditTrail(companyBranchesBean.getBranchId(),companyBranches,"COMPANY MASTER");		
				}
			    
			    //companyDetailsService.addCompanyBranches(companyBranches);
			}
 			companyDetailsService.deleteAllCompanyBranches();
	    	//companyDetailsService.addCompanyDetails(companyDetails);
		    isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
			
		private CompanyBranches prepareModelForCompanyBranch(CompanyBranchesBean companyBranchesBean,CompanyDetailsBean companyDetailsBean) 
		{
			CompanyBranches companyBranches = new CompanyBranches();
			
			companyBranches.setId(companyBranchesBean.getId());
			companyBranches.setBranchid(companyBranchesBean.getId());
			companyBranches.setName(companyBranchesBean.getName().toUpperCase());
			companyBranches.setLocation(companyBranchesBean.getLocation().toUpperCase());
			companyBranches.setCity(companyBranchesBean.getCity().toUpperCase());
			companyBranches.setStatecode(companyBranchesBean.getStateCode());
			companyBranches.setDistrictcode(companyBranchesBean.getDistrictCode());
			companyBranches.setMobileno(companyBranchesBean.getMobileNo());
			companyBranches.setPhoneno(companyBranchesBean.getPhoneNo());
			companyBranches.setOtherno(companyBranchesBean.getOtherNo());
			companyBranches.setFax(companyBranchesBean.getFax());
			companyBranches.setEmail(companyBranchesBean.getEmail());
		
			
			return companyBranches;
		 }
			
		 private CompanyDetails prepareModelForCompanyDetails(CompanyDetailsBean companyDetailsBean) 
		 {
			 CompanyDetails companyDetails = new CompanyDetails();
			   		
			// companyDetails.setId(companyDetailsBean.getId());
			 companyDetails.setCompanyid(companyDetailsBean.getCompanyId());
			 companyDetails.setName(companyDetailsBean.getCompanyName().toUpperCase());
			 companyDetails.setWebsite(companyDetailsBean.getWebsite());
			 companyDetails.setAddress1(companyDetailsBean.getAddress1());
			 companyDetails.setAddress2(companyDetailsBean.getAddress2());
			 companyDetails.setCity(companyDetailsBean.getCity().toUpperCase());
			 companyDetails.setStatecode(companyDetailsBean.getStateCode());
			 companyDetails.setDistrictcode(companyDetailsBean.getDistrictCode());
			 companyDetails.setCountrycode(companyDetailsBean.getCountryCode());
			 companyDetails.setPincode(companyDetailsBean.getPinCode());
			 companyDetails.setPhoneno(companyDetailsBean.getPhoneNo());
			 companyDetails.setMobileno(companyDetailsBean.getMobileNo());
			 companyDetails.setEmailid(companyDetailsBean.getEmailId());
			 companyDetails.setFaxno(companyDetailsBean.getFaxNo());
			 companyDetails.setCin(companyDetailsBean.getCin());
			 companyDetails.setRoc(companyDetailsBean.getRoc());
			 companyDetails.setRegnno(companyDetailsBean.getRegistrationNo());
			 companyDetails.setGst(companyDetailsBean.getGst());
			 companyDetails.setTin(companyDetailsBean.getTin());
			 companyDetails.setPin(companyDetailsBean.getPin());
			 companyDetails.setPan(companyDetailsBean.getPanNo());
			 companyDetails.setProftaxregn(companyDetailsBean.getPanNo());
			 companyDetails.setStregnno(companyDetailsBean.getServiceTaxRegnNo());
		      return companyDetails;
	    }
	    @RequestMapping(value = "/getAllCompanyDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody String getAllCompanyDetails(@RequestBody String jsonData)throws Exception
		{			   		
	    	jsonResponse = new org.json.JSONObject();
	    	  
	    	CompanyDetailsBean companyDetailsBean = prepareListofCompanyDetailsBeans(companyDetailsService.listCompanyDetail());

	    	List<CompanyBranchesBean> companyDetailsBeanBeans = prepareListofCompanyBranchesBeans(companyDetailsService.listCompanyBranches());
	    	   
			org.json.JSONObject companyJson=new org.json.JSONObject(companyDetailsBean);
			org.json.JSONArray branchArray = new org.json.JSONArray(companyDetailsBeanBeans);
			
			jsonResponse.put("company",companyJson);
			jsonResponse.put("branch",branchArray);
	    	   
	    	return jsonResponse.toString();
		}
	    
	    private CompanyDetailsBean prepareListofCompanyDetailsBeans(List<CompanyDetails> companyDetails)
	    {
	    	CompanyDetailsBean bean = null;
	    	if (companyDetails != null && !companyDetails.isEmpty())
	    	{	    		
			   CompanyDetails companyDetail=companyDetails.get(0);	    		
			   bean = new CompanyDetailsBean();	
					   				
					   //bean.setId(companyDetails.getId());
			   bean.setCompanyId(companyDetail.getCompanyid());
			   bean.setCompanyName(companyDetail.getName());
			   bean.setWebsite(companyDetail.getWebsite());
			   bean.setAddress1(companyDetail.getAddress1());
			   bean.setAddress2(companyDetail.getAddress2());
			   bean.setCity(companyDetail.getCity());
			   bean.setStateCode(companyDetail.getStatecode());
			   bean.setDistrictCode(companyDetail.getDistrictcode());
			   bean.setCountryCode(companyDetail.getCountrycode());
			   bean.setPinCode(companyDetail.getPincode());
			   bean.setPhoneNo(companyDetail.getPhoneno());
			   bean.setMobileNo(companyDetail.getMobileno());
			   bean.setEmailId(companyDetail.getEmailid());
			   bean.setFaxNo(companyDetail.getFaxno());
			   bean.setCin(companyDetail.getCin());
			   bean.setRoc(companyDetail.getRoc());
			   bean.setRegistrationNo(companyDetail.getRegnno());
			   bean.setGst(companyDetail.getRegnno());
			   bean.setTin(companyDetail.getTin());
			   bean.setPin(companyDetail.getPin());
			   bean.setPanNo(companyDetail.getPan());
			   bean.setProfessionalTaxRegn(companyDetail.getProftaxregn());
			   bean.setServiceTaxRegnNo(companyDetail.getStregnno());
    	   }
    	   return bean;  
	      }   
	    
	    private List<CompanyBranchesBean> prepareListofCompanyBranchesBeans(List<CompanyBranches> companyBranches)
	    {
	    	List<CompanyBranchesBean> beans = null;
	    	if (companyBranches != null && ! companyBranches.isEmpty())
	    	{
	    		beans = new ArrayList<CompanyBranchesBean>();
	    		CompanyBranchesBean bean = null;
	    		for (CompanyBranches companyBranch : companyBranches)
	    		{
	    			bean = new CompanyBranchesBean();	
			   				
	    			  bean.setId(companyBranch.getId());
	    			  bean.setBranchId(companyBranch.getBranchid());
	    			  bean.setCity(companyBranch.getCity());
	    			  bean.setDistrictCode(companyBranch.getDistrictcode());
	    			  bean.setFax(companyBranch.getFax());
	    			  bean.setEmail(companyBranch.getEmail());
	    			  bean.setLocation(companyBranch.getLocation());
	    			  bean.setMobileNo(companyBranch.getMobileno());
	    			  bean.setName(companyBranch.getName());
	    			  bean.setPhoneNo(companyBranch.getPhoneno());
	    			  bean.setOtherNo(companyBranch.getOtherno());
	    			  bean.setStateCode(companyBranch.getStatecode());
	    			   
	    			   beans.add(bean);				
	    		   	}
	    	   }
	    	   return beans;  
	       }   
		//------------------------------End of CompanyDetails---------------------------//

	  //------------------------------Screens master----------------------------------------//    
	    @RequestMapping(value = "/saveScreens", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	    public @ResponseBody
	    boolean saveScreens(@RequestBody String jsonData) throws Exception
	  	 {
	    	boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			try 
			{
				 ScreensBean screensBean = new ObjectMapper().readValue(jsonData, ScreensBean.class);
			  	 Screens screens = prepareModelForScreens(screensBean);
			  	// screensService.addScreens(screens);
			  	entityList.add(screens);
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
			  	return isInsertSuccess;

			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
	  	 }  
	  	  private Screens prepareModelForScreens(ScreensBean screensBean) 
	  	   {
	  	     Screens screens = new Screens();
	  	    int screencode = commonService.getMaxIDforColumn("Screens","screencode");
			String modifyFlag = screensBean.getModifyFlag();

			if("Yes".equalsIgnoreCase(modifyFlag))
			{
		  		 screens.setScreencode(screensBean.getScreenCode());
			}
			/*else
			{
		  		 screens.setScreencode(screencode);
			}*/
			
	  		//screens.setScreencode(screensBean.getScreenCode());
	  		screens.setScreenname(screensBean.getScreenName().toUpperCase());
	  		String description = screensBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
	  		 screens.setDescription(description);
	  		 screens.setCanadd(screensBean.getCanAdd());
	  		 screens.setCanmodify(screensBean.getCanModify());
	  		 screens.setCanread(screensBean.getCanRead());
	  		 screens.setCandelete(screensBean.getCanDelete());
	  		 screens.setIsaudittrailrequired(screensBean.getIsAuditTrailRequired());
	  		 screens.setStatus(screensBean.getStatus());

	  	     return screens;
	      	}  
	  	  @RequestMapping(value = "/getAllScreens", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
  		 public @ResponseBody List<ScreensBean> getAllScreens(@RequestBody String jsonData) throws Exception 
  		 {
  			List<ScreensBean> screensBeanBens = prepareListofScreensBean(screensService.listScreen());
  			
  			return screensBeanBens;
  		} 
	  	  
	    private List<ScreensBean> prepareListofScreensBean(List<Screens> Screen)
	    {
	    	List<ScreensBean> beans = null;
	  	    if (Screen != null && !Screen.isEmpty())
	  	    {
	  	    	beans = new ArrayList<ScreensBean>();
	  	        ScreensBean bean = null;
	  	        for (Screens screens : Screen)
	  	        {
	  	        	bean = new ScreensBean();
	  	    	
		  			bean.setScreenCode(screens.getScreencode());
		  			bean.setScreenName(screens.getScreenname());
		  			bean.setDescription(screens.getDescription());
		  		//added by naidu on 03-12-2016
		  			/*bean.setCanAdd(screens.getCanadd());
		  			bean.setCanModify(screens.getCanmodify());
		  			bean.setCanRead(screens.getCanread());
		  			bean.setCanDelete(screens.getCandelete());
		  			 */
		  			bean.setCanAdd(false);
		  			bean.setCanModify(false);
		  			bean.setCanRead(false);
		  			bean.setCanDelete(false);
		  			bean.setIsAuditTrailRequired((byte)1);
		  			bean.setStatus(screens.getStatus());
		  			bean.setModifyFlag("Yes");
		  			int status = screens.getStatus();
		  			//if(status == 0)
		  			//{
			  	    	beans.add(bean);
		  			//}
	  						
	  	        }
	  		}
	  		return beans;
	  	}
	  //------------------------------End Of Screens Master----------------------------------------//  
	    
	  //------------------------------Roles---------------------------//	 
    @RequestMapping(value = "/saveRoles", method = RequestMethod.POST, headers = { "Content-type=application/json" })
     public @ResponseBody
     boolean saveRoles(@RequestBody JSONObject jsonData) throws Exception
     {
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			 logger.info("====saveRoles() from AgricultureHarvestingController========"+ jsonData.toString());
		   	 RolesBean rolesBean = new ObjectMapper().readValue(jsonData.toString(), RolesBean.class);
		   	 Roles roles = prepareModelForRoles(rolesBean);
		   	 
		   	entityList.add(roles);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
		   	 //rolesService.addRoles(roles);
		   	return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
     } 
	    
	    
	    
	     private Roles prepareModelForRoles(RolesBean rolesBean) 
	     {
	   	  List<RoleScreens> roleScreens=new ArrayList();
	   	  Roles roles = new Roles();
	   	  roles.setRoleid(rolesBean.getRoleId());
	   	  roles.setRole(rolesBean.getRole());
		   	String description = rolesBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
	   	  roles.setDescription(description);
	   	  roles.setStatus(rolesBean.getStatus());	  
	   	  roles.setRolescreens(roleScreens);
	   	  
	   	for(int i=0;i<rolesBean.getRoleScreensBean().size();i++)
	   	{
	   	  RoleScreens roleScreen = new RoleScreens();
	   	  //Screens screens=(Screens) commonService.getEntityById(roleScreen.getScreenid(),Screens.class);

	   	  roleScreen.setScreenid(rolesBean.getRoleScreensBean().get(i).getRollId());
	   	  roleScreen.setCanadd(rolesBean.getRoleScreensBean().get(i).getCanAdd());
	   	  roleScreen.setCandelete(rolesBean.getRoleScreensBean().get(i).getCanDelete());
	   	  roleScreen.setCanmodify(rolesBean.getRoleScreensBean().get(i).getCanModify());
	   	  roleScreen.setCanread(rolesBean.getRoleScreensBean().get(i).getCanRead());
	   	  roleScreen.setScreenid(rolesBean.getRoleScreensBean().get(i).getScreenCode());
	   	  roleScreen.setScreenname(rolesBean.getRoleScreensBean().get(i).getScreenName().toUpperCase());
		  	description = rolesBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
	   	  roleScreen.setDescription(description);
	   	  roleScreens.add(roleScreen);
	   	  
	   	}
	   	 
	   	 
	         return roles;
	      }   
	     /*@RequestMapping(value = "/getAllRoles", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	   	 public @ResponseBody List<RolesBean> getAllRoles(@RequestBody String jsonData) throws Exception 
	   	 {

	   		logger.info("====getAllRoles()  from SecurityController========");
	   		
	   		List<RolesBean> rolesBeanBens = prepareListofRolesBean(rolesService.listRole());
	   		logger.info("==== rolesBeanBens.size()========"+rolesBeanBens.size());
	   		
	   		return rolesBeanBens;
	   	 }
	     private List<RolesBean> prepareListofRolesBean(List<Roles> Role)
	     {
	   	  List<RolesBean> beans = null;
	   	  if (Role != null && !Role.isEmpty())
	   	  {
	   	      beans = new ArrayList<RolesBean>();
	   	      RolesBean bean = null;
	   	      for (Roles roles : Role)
	   	      {
	   	    	  bean = new RolesBean();
	   	    	  bean.setRoleId(roles.getRoleid());
	   	    	  bean.setRole(roles.getRole());
	   	    	  bean.setDescription(roles.getDescription());
	   	    	  bean.setStatus(roles.getStatus());
	   	    	  beans.add(bean);
	   		  }
	   	   }
	   		return beans;
	      }*/
	     
	     @RequestMapping(value = "/getAllRolesOnChange", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	   	public @ResponseBody
	   	String getAllRolesOnChange(@RequestBody String jsonData) throws Exception
	   	{
	   	 	RolesBean rolesBean =null;	
	   	 	List<RoleScreens> roleScreens=null;
	   	 	List<RoleScreens> roleScreensNewList=new ArrayList<RoleScreens>();
	   	 	RoleScreens rolescreen=null;
	   	 	jsonResponse = new org.json.JSONObject();
	   		
	   	 	Roles roles=rolesService.getRoles(Integer.parseInt(jsonData));
	   	
	   		if(roles!=null)
	   		{
	   			roleScreens=rolesService.getRoleScreensByRoleId(roles.getRoleid());
	   		}
	   		RoleScreens rs = roleScreens.get(roleScreens.size() - 1);
	   		logger.info("-------------------"+rs.getScreenid());
	   		
	   		List<Screens> ScreenMasterList = screensService.getScreensByCode(rs.getScreenid());
	   		logger.info("-------------------"+ScreenMasterList.toString());
	   		
	   		for(int i=0;i<ScreenMasterList.size();i++)
	   		{
	   					
	   			rolescreen=new RoleScreens();
	   			rolescreen.setScreenid(ScreenMasterList.get(i).getScreencode());
	   			rolescreen.setCanadd(ScreenMasterList.get(i).getCanadd());
	   			rolescreen.setCanmodify(ScreenMasterList.get(i).getCanmodify());
	   			rolescreen.setCanread(ScreenMasterList.get(i).getCanadd());
	   			rolescreen.setCandelete(ScreenMasterList.get(i).getCandelete());
	   			rolescreen.setScreenname(ScreenMasterList.get(i).getScreenname());
	   			
	   			String description = ScreenMasterList.get(i).getDescription();
				if ("".equals(description) || "null".equals(description) || (description == null)) 
				{
					description = "NA";
				}
				else
				{
					description = description.toUpperCase();
				}
	   			rolescreen.setDescription(description);
	   			roleScreensNewList.add(rolescreen);
	   				
	   		}
	   			
	   		if(roleScreensNewList.size()>0)
	   			roleScreens.addAll(roleScreensNewList);
	   		
	   		rolesBean =prepareListofRolesBean(roles,roleScreens);
	   			
	   		org.json.JSONObject advanceJson=new org.json.JSONObject(rolesBean);
	   		jsonResponse.put("roles",advanceJson);	
	   		
	   		return jsonResponse.toString();
	   	}
	     
	     private RolesBean prepareListofRolesBean(Roles roles,List<RoleScreens> roleScreens)
	     {
	   	  List<RoleScreensBean> beans = null;
	   	  
	   	  RolesBean bean = new RolesBean();
	   	  bean.setRoleId(roles.getRoleid());
	   	  bean.setRole(roles.getRole());
	   	  bean.setDescription(roles.getDescription());
	   	  bean.setStatus(roles.getStatus());
	   	  
	   	  
	   	  if (roleScreens != null && !roleScreens.isEmpty())
	   	  {
	   	      beans = new ArrayList<RoleScreensBean>();
	   	      RoleScreensBean rsbean = null;
	   	      for (RoleScreens roleScreen : roleScreens)
	   	      {
	   	    	  Screens screens=(Screens) commonService.getEntityById(roleScreen.getScreenid(),Screens.class);
	   	    	  rsbean = new RoleScreensBean();
	   	    	  rsbean.setRollId(roles.getRoleid());    
	   	    	  rsbean.setScreenCode(roleScreen.getScreenid());
	   	    	  rsbean.setCanAdd(roleScreen.getCanadd());
	   	    	  rsbean.setCanModify(roleScreen.getCanmodify());
	   	    	  rsbean.setCanRead(roleScreen.getCanread());
	   	    	  rsbean.setCanDelete(roleScreen.getCandelete());
	   	    	  rsbean.setScreenName(screens.getScreenname());
	   	    	  rsbean.setDescription(screens.getDescription());
	   	    	  rsbean.setId(roleScreen.getId());
	   	    	  beans.add(rsbean);
	   		  }
	   	   }
	   	  bean.setRoleScreensBean(beans);
	   		return bean;
	      }
	   //------------------------------End of Roles----------------------------------------//	 

	   //------------------------------SetupDetails----------------------------------------//
	 	@RequestMapping(value = "/saveSetupDetails", method = RequestMethod.POST)
	 	public @ResponseBody
	 	boolean saveSetupDetails(@RequestBody JSONArray jsonData) throws JsonParseException, JsonMappingException, IOException
	 	{		
	 		boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
	 		try
	 		{
	 			for(int i=0;i<jsonData.size();i++)
	 			{
	 				SetupDetailsBean setupDetailsBean = new ObjectMapper().readValue(jsonData.get(i).toString(), SetupDetailsBean.class);
	 				SetupDetails setupDetails = prepareModelForSetupDetails(setupDetailsBean);
	 				entityList.add(setupDetails);
	 			}
	 			isInsertSuccess = commonService.saveMultipleEntities(entityList);
	 			return isInsertSuccess;
	 		}
	 		catch (Exception e)
	         {
	             logger.info(e.getCause(), e);
	             return isInsertSuccess;
	         }
	 	}
	 	private SetupDetails prepareModelForSetupDetails(SetupDetailsBean setupDetailsBean)
	 	{
	 		SetupDetails setupDetails=new SetupDetails();
	 		
	 		setupDetails.setId(setupDetailsBean.getId());
	 		setupDetails.setConfigsc(setupDetailsBean.getConfigSc());
	 		String description = setupDetailsBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
	 		setupDetails.setDescription(description);
	 		setupDetails.setName(setupDetailsBean.getName().toUpperCase());
	 		setupDetails.setValue(setupDetailsBean.getValue());
	 		return setupDetails;
	 	}
	 	 @RequestMapping(value = "/getAllSetupDetails", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 	 public @ResponseBody List<SetupDetailsBean> getAllSetupDetails(@RequestBody String jsonData) throws Exception 
	 	 {
	 			
	 		 List<SetupDetailsBean> setupDetailsBeanBens = prepareListofSetupDetailsBean(setupDetailsService.listSetupDetail());
	 			
	 		 return setupDetailsBeanBens;
	 	 } 
	 	 private List<SetupDetailsBean> prepareListofSetupDetailsBean(List<SetupDetails> SetupDetail)
	 	 {
	 		 List<SetupDetailsBean> beans = null;
	 		 if (SetupDetail != null && !SetupDetail.isEmpty())
	 		 {
	 			beans = new ArrayList<SetupDetailsBean>();
	 	  	     SetupDetailsBean bean = null;
	 	  	     for (SetupDetails setupDetails : SetupDetail)
	 	  	     {
	 	  	    	 bean = new SetupDetailsBean();
	 	  	    	
	 	  			 bean.setId(setupDetails.getId());
	 	  			 bean.setConfigSc(setupDetails.getConfigsc());
	 	  			 bean.setDescription(setupDetails.getDescription());
	 	  			 bean.setName(setupDetails.getName());
	 	  			 bean.setValue(setupDetails.getValue());
	 	  	    	 beans.add(bean);
	 	  						
	 	  	     }
	 		 }
	 		 return beans;
	 	 }

	 //------------------------------ End of SetupDetails -------------------------------//
	 	 
		  
	 	//------------------------------ ScrenFilds ---------------------------------------//	
 		 @RequestMapping(value = "/saveScreenFields", method = RequestMethod.POST, headers = { "Content-type=application/json" })
 	     public @ResponseBody
 	    boolean saveScreenFields(@RequestBody JSONObject jsonData) throws Exception
 	 	 {
 			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			try 
			{
				 JSONObject formData=(JSONObject) jsonData.get("formData");		 
	 			 JSONArray screenFieldsData=(JSONArray) jsonData.get("gridData");	
	 			 Integer screenId=(Integer)formData.get("screenId");
	 			 
	 			 screenFieldsService.deleteScreenFields(screenId);
		 	 	  for(int i=0;i<screenFieldsData.size();i++)
		 		  {
		 	 		ScreenFieldsBean screenFieldsBean = new ObjectMapper().readValue(screenFieldsData.get(i).toString(), ScreenFieldsBean.class);
		 	 		ScreenFields screenFields = prepareModelForScreenFields(screenFieldsBean,screenId);
		 	 		entityList.add(screenFields);
		 	 		//.addScreenFields(screenFields);	
		 		   }
		 	  	  isInsertSuccess = commonService.saveMultipleEntities(entityList);
		 	 	  return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
			
 			
 	 	 }  	
	 		 private ScreenFields prepareModelForScreenFields(ScreenFieldsBean screenFieldsBean,Integer screenId)
	 		 {
	 			 ScreenFields screenFields=new ScreenFields();
	 			 
	 			 screenFields.setId(screenFieldsBean.getId());
	 			 screenFields.setFieldid(screenFieldsBean.getFieldId());
	 			 screenFields.setScreenid(screenFieldsBean.getScreenId());
	 			 screenFields.setFieldlabel(screenFieldsBean.getFieldLabel().toUpperCase());
	 			 screenFields.setIsaudittrail(screenFieldsBean.getIsAuditTrail());
	 			 screenFields.setScreenid(screenId);
	 		  return screenFields;
	 		 }
	 		 
	 		 @RequestMapping(value = "/getAllScreenFields", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 		 public @ResponseBody List<ScreenFieldsBean> getAllScreenFields(@RequestBody Integer jsonData) throws Exception 
	 		 {
	 			 logger.info("====getAllScreenFields()  from SecurityController========");
	 				
	 			 List<ScreenFieldsBean> screenFieldsBeanBens = prepareListofScreenFieldsBean(screenFieldsService.getScreenFieldsByCode(jsonData));
	 			 logger.info("==== screenFieldsBeanBens.size()========"+screenFieldsBeanBens.size());
	 			
	 			 return screenFieldsBeanBens;
	 		 }  
	 		 private List<ScreenFieldsBean> prepareListofScreenFieldsBean(List<ScreenFields> ScreenField)
	 		 {
	 			 List<ScreenFieldsBean> beans = null;
	 			 if (ScreenField != null && !ScreenField.isEmpty())
	 			 {
	 				beans = new ArrayList<ScreenFieldsBean>();
	 				ScreenFieldsBean bean = null;
	 		  	     for (ScreenFields screenFields : ScreenField)
	 		  	     {
	 		  	    	 bean = new ScreenFieldsBean();
	 		  			 bean.setId(screenFields.getId());
	 		  			 bean.setScreenId(screenFields.getScreenid());
	 		  			 bean.setFieldId(screenFields.getFieldid());
	 		  			 bean.setFieldLabel(screenFields.getFieldlabel());
	 		  			 bean.setIsAuditTrail(screenFields.getIsaudittrail());
	 		  	    	 beans.add(bean);
	 		  						
	 		  	     }
	 			 }
	 			 return beans;
	 		 }
	 	//------------------------------End of ScrenFilds -----------------------------------//	 
	 		 //----It is for the roles screens based on the employee logged in to the system------//
	 		@RequestMapping(value = "/getAllRoleScreensByEmployeeId", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
		   	 public @ResponseBody List getAllRoleScreensByEmployeeId(HttpServletRequest request,HttpServletResponse response) throws Exception 
		   	 {
		   		logger.info("====getAllRoles()  from SecurityController========");
		   		org.json.JSONObject jsonResponse = new org.json.JSONObject();
		   		HttpSession session = request.getSession(false);
		   		Employees employees=(Employees)session.getAttribute(Constants.USER);
		   		//Employees employees=(Employees)commonService.getEntityById(jsonData, Employees.class);
		   		List roleScreens = rolesService.getRoleScreensByRoleId(employees.getRoleid());
		   		
		   		logger.info("====getAllRoles()  from SecurityController========"+roleScreens.toString());
		   		//if(roleScreens!=null)
		   			return roleScreens;
		   		//else
		   		//	return "false";
		   	 }
	 		@RequestMapping(value = "/getRoleScreensByEmployeeId", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
		   	 public @ResponseBody String getRoleScreensByEmployeeId(HttpServletRequest request,HttpServletResponse response) throws Exception 
		   	 {
		   		logger.info("====getAllRoles()  from SecurityController========");
		   		org.json.JSONObject jsonResponse = new org.json.JSONObject();
		   		HttpSession session = request.getSession(false);
		   		Employees employees=(Employees)session.getAttribute(Constants.USER);
		   		//Employees employees=(Employees)commonService.getEntityById(jsonData, Employees.class);
		   		List roleScreens = rolesService.getRoleScreensByRoleId(employees.getRoleid());
		   		RoleScreens rs=null;
		   		for(int i=0;i<roleScreens.size();i++)
		   		{
		   			rs=(RoleScreens)roleScreens.get(i);
			   		if(rs.getScreenname().equalsIgnoreCase("GENERATE AGREEMENT"))
			   		{
			   			break;
			   		}
			   			

		   			
		   		}
		   		org.json.JSONObject advanceJson=new org.json.JSONObject(rs);
				//jsonResponse.put("employees",advanceJson);	
		   		logger.info("====getAllRoles()  from SecurityController========"+roleScreens.toString());
		   		//if(roleScreens!=null)
		   			return advanceJson.toString();
		   		//else
		   		//	return "false";
		   	 }
	 		
	 		 @RequestMapping(value = "/findValidUser", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 	     public @ResponseBody
	 	    boolean findValidUser(@RequestBody JSONObject jsonData) throws Exception
	 	 	 {
	 			boolean valid  = false;
				
				try 
				{
					 String user1=(String)jsonData.get("screenId");
		 			 
		 			 valid=commonService.checkUser(user1);
		 			 return valid;
				}
				catch (Exception e)
				{
					logger.info(e.getCause(), e);
					return valid;
				}
				
	 			
	 	 	 } 	
 		 //Added by DMurty on 02-12-2016
 		 @RequestMapping(value = "/resetPassword", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
 		 public @ResponseBody boolean resetPassword(@RequestBody JSONObject jsonData) throws Exception 
 		 {
 			 List entityList = new ArrayList();
 			 List UpdateList = new ArrayList();

 			boolean resetPwd = false;
 			String userId=(String)jsonData.get("username");
			Employees employees = null;
			List EmployeesList = commonService.getEmployeeDetails(userId);
			if(EmployeesList != null && EmployeesList.size()>0)
			{
				String pwd = digester.digest("ssserpuser");
				String strQry = "UPDATE Employees set password='"+pwd+"' WHERE loginid ='"+userId+"'";
				Object Qryobj = strQry;
				UpdateList.add(Qryobj);
				resetPwd = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			}
 			return resetPwd;
 		 }
 		 //Added by DMurty on 02-12-2016
 		 @RequestMapping(value = "/changePassword", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
 		 public @ResponseBody boolean changePassword(@RequestBody JSONObject jsonData) throws Exception 
 		 {
 			 List entityList = new ArrayList();
 			 List UpdateList = new ArrayList();

 			boolean resetPwd = false;
 			String userId=(String)jsonData.get("username");
 			String password=(String)jsonData.get("password");
			Employees employees = null;
			List EmployeesList = commonService.getEmployeeDetails(userId);
			if(EmployeesList != null && EmployeesList.size()>0)
			{
				String pwd = digester.digest(password);
				String strQry = "UPDATE Employees set password='"+pwd+"' WHERE loginid ='"+userId+"'";
				Object Qryobj = strQry;
				UpdateList.add(Qryobj);
				resetPwd = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			}
 			return resetPwd;
 		 }
	 		
}
