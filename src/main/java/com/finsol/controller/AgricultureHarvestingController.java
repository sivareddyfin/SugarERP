package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.finsol.bean.AdvanceCategoryBean;
import com.finsol.bean.AdvanceInterestRatesBean;
import com.finsol.bean.AdvanceSubCategoryBean;
import com.finsol.bean.BankMasterBean;
import com.finsol.bean.BranchBean;
import com.finsol.bean.CaneManagerBean;
import com.finsol.bean.CircleBean;
import com.finsol.bean.CompanyAdvanceBean;
import com.finsol.bean.DesignationBean;
import com.finsol.bean.FamilyGroupBean;
import com.finsol.bean.FieldAssistantBean;
import com.finsol.bean.FieldOfficerMasterBean;
import com.finsol.bean.LandTypeMasterBean;
import com.finsol.bean.MandalBean;
import com.finsol.bean.PermitRulesBean;
import com.finsol.bean.RegionBean;
import com.finsol.bean.RyotBankDetailsBean;
import com.finsol.bean.RyotMasterBean;
import com.finsol.bean.SampleCardRulesBean;
import com.finsol.bean.SeasonBean;
import com.finsol.bean.SeedAdvanceCostBean;
import com.finsol.bean.SeedlingTrayBean;
import com.finsol.bean.SeedlingTrayChargeByDistanceBean;
import com.finsol.bean.ShiftMasterBean;
import com.finsol.bean.VarietyMasterBean;
import com.finsol.bean.VillageMasterBean;
import com.finsol.bean.ZoneMasterBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountMaster;
import com.finsol.model.AccountType;
import com.finsol.model.AdvanceCategory;
import com.finsol.model.AdvanceInterestRates;
import com.finsol.model.AdvanceOrder;
import com.finsol.model.AdvanceSubCategory;
import com.finsol.model.Bank;
import com.finsol.model.Branch;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneManager;
import com.finsol.model.Circle;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.Designation;
import com.finsol.model.Employees;
import com.finsol.model.FamilyGroup;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;
import com.finsol.model.LandType;
import com.finsol.model.Mandal;
import com.finsol.model.PermitRules;
import com.finsol.model.Region;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;
import com.finsol.model.SampleCardRules;
import com.finsol.model.Season;
import com.finsol.model.SeedAdvanceCost;
import com.finsol.model.SeedlingTray;
import com.finsol.model.SeedlingTrayChargeByDistance;
import com.finsol.model.Shift;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.model.WeighmentDetails;
import com.finsol.model.Zone;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.AdvanceCategoryService;
import com.finsol.service.AdvanceSubCategoryService;
import com.finsol.service.AuditTrailService;
import com.finsol.service.BankService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CaneManagerService;
import com.finsol.service.CaneReceiptFunctionalService;
import com.finsol.service.CircleService;
import com.finsol.service.CommonService;
import com.finsol.service.CompanyAdvanceService;
import com.finsol.service.DesignationService;
import com.finsol.service.FamilyGroupService;
import com.finsol.service.FieldAssistantService;
import com.finsol.service.FieldOfficerService;
import com.finsol.service.LandTypeService;
import com.finsol.service.MandalService;
import com.finsol.service.PermitRulesService;
import com.finsol.service.RegionService;
import com.finsol.service.RyotService;
import com.finsol.service.SampleCardRulesService;
import com.finsol.service.SeasonService;
import com.finsol.service.SeedlingTrayService;
import com.finsol.service.ShiftService;
import com.finsol.service.VarietyService;
import com.finsol.service.VillageService;
import com.finsol.service.ZoneService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * @author Rama Krishna
 * 
 */
@Controller
public class AgricultureHarvestingController {

	private static final Logger logger = Logger.getLogger(AgricultureHarvestingController.class);

	@Autowired
	private CaneManagerService caneManagerService;

	@Autowired
	private LandTypeService landTypeService;

	@Autowired
	private FieldOfficerService fieldOfficerService;
	
	@Autowired
	private FieldAssistantService fieldAssistantService;
	
	@Autowired
	private VarietyService varietyService;
	
	@Autowired
	private ZoneService zoneService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private	AdvanceCategoryService advanceCategoryService;
	
	@Autowired
	private AdvanceSubCategoryService advanceSubCategoryService;
	
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private MandalService mandalService;
	
	@Autowired
	private VillageService villageService;
	
	@Autowired
	private CircleService circleService;
	
	@Autowired
	private SeasonService seasonService;
	
	@Autowired
	private FamilyGroupService familyGroupService;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private SampleCardRulesService sampleCardRulesService;
	
	@Autowired
	private	PermitRulesService permitRulesService;
	
	@Autowired
	private DesignationService designationService;	
	
	
	@Autowired
	private RyotService ryotService;
	
	@Autowired
	private SeedlingTrayService seedlingTrayService;	
	
	@Autowired
    private HttpServletRequest httpServletRequest;
	
	@Autowired
    private ServletContext servletContext;
	
	@Autowired
	private ShiftService shiftService;
	
	@Autowired
	private CompanyAdvanceService companyAdvanceService;
	
	@Autowired
	private AuditTrailService auditTrailService;
	
	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired	
	private LoginController loginController;
	@Autowired	
	private CaneReceiptFunctionalService caneReceiptFunctionalService;
	@Autowired
	private HibernateDao hibernatedao;
	
	@Autowired	
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	

	//------------------------- Common Methods --------------------------------------------//
	//Added by DMurty on 29-12-2016
	public double round(double amount)
	{
		logger.info("round() ========amount=========="+amount);
	    double dAbs = Math.abs(amount);
	    int i = (int) dAbs;
	    double result = dAbs - (double) i;
	    if(result<0.5)
	    {
	    	int Amt = amount<0 ? -i : i;
	    	double finalAmount = (double) Amt;
			logger.info("round() ========finalAmount=========="+finalAmount);
	        return finalAmount;            
	    }
	    else
	    {
	    	int Amt = amount<0 ? -(i+1) : i+1;
	    	double finalAmount = (double) Amt;
			logger.info("round() ========finalAmount=========="+finalAmount);
	        return finalAmount;          
	    }
	}
	
	
	@RequestMapping(value = "/getMaxId", method = RequestMethod.POST)
	public @ResponseBody
	String getMaxId(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tableName=(String)jsonData.get("tablename");
		Integer maxId = commonService.getMaxID(tableName);
		
		JSONObject json = new JSONObject();
		json.put("id", maxId);
		return json.toString();
	}
	
	@RequestMapping(value = "/loadDropDownNames", method = RequestMethod.POST)
	public @ResponseBody
	String loadDropDownNames(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("dropdownname");
		String columnName=Constants.DropdownName.get(dropdownName);		
		JSONArray dropDownArray = commonService.getDropdownNames(dropdownName,columnName);
		return dropDownArray.toString();
	}
	//added by naidu on 13-02-2017
	@RequestMapping(value = "/getBranchBatchNo", method = RequestMethod.POST)
	public @ResponseBody
	String getBranchBatchNo(@RequestBody JSONObject jsonData) throws Exception 
	{
		String season=(String)jsonData.get("season");
		int branchcode=(Integer)jsonData.get("branch");	
		JSONArray dropDownArray = commonService.getBatchnoForBranch(season,branchcode);
		return dropDownArray.toString();
	}
	
	
	
	//Added by DMurty on 12-06-2016
	@RequestMapping(value = "/loadDropDownNamesForVillage", method = RequestMethod.POST)
	public @ResponseBody
	String loadDropDownNamesForVillage(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("dropdownname");
		String columnName=Constants.DropdownName.get(dropdownName);		
		JSONArray dropDownArray = commonService.getDropdownNamesForVillage(dropdownName,columnName);
		return dropDownArray.toString();
	}
	
	@RequestMapping(value = "/loadMaxIdForVillage", method = RequestMethod.POST)
	public @ResponseBody
	String loadMaxIdForVillage(@RequestBody String jsonData) throws Exception 
	{
		JSONArray dropDownArray = commonService.loadMaxIdForVillage(Integer.parseInt(jsonData));
		return dropDownArray.toString();
	}
	
	@RequestMapping(value = "/getMaxPermitNum", method = RequestMethod.POST)
	public @ResponseBody
	int getMaxPermitNum(@RequestBody String jsonData) throws Exception 
	{
		int permitNo = commonService.getMaxPermitNum(jsonData);
		return permitNo;
	}
	
	
	//Added by DMurty on 13-06-2016
	@RequestMapping(value = "/ValidateRyotCode", method = RequestMethod.POST)
	public @ResponseBody
	boolean ValidateRyotCode(@RequestBody String jsonData) throws Exception 
	{
		boolean flag = commonService.ValidateRyotCode(jsonData);
		return flag;
	}
	
	@RequestMapping(value = "/getAgreementNosForRyot", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAgreementNosForRyot(@RequestBody JSONObject jsonData) throws Exception 
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		String agNumber = "";
		String season=(String)jsonData.get("season");
		String ryotCode=(String)jsonData.get("ryotcode");
		List AgreementList = ahFuctionalService.getAgreementsByRyot(ryotCode,season);
		if(AgreementList != null && AgreementList.size()>0)
		{
			for (int j = 0; j < AgreementList.size(); j++)
			{
				Map AgreementMap = new HashMap();
				AgreementMap = (Map) AgreementList.get(j);
				String AggNo = (String) AgreementMap.get("agreementnumber");
				agNumber += AggNo+",";
			}
		}
	
		jsonObj.put("agreementNumber", agNumber);
		jArray.add(jsonObj);
		
		return jArray;
	}
	@RequestMapping(value = "/loadWeighmentRyots", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray loadWeighmentRyots(@RequestBody String season) throws Exception 
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		
		List wList = ahFuctionalService.getWeighmentRyots(season);
		if(wList != null && wList.size()>0)
		{
			for (int j = 0; j < wList.size(); j++)
			{
				jsonObj=new JSONObject();
				Map wMap = new HashMap();
				wMap = (Map) wList.get(j);
				String ryotCode = (String) wMap.get("ryotcode");
				jsonObj.put("ryotCode", ryotCode);
				jArray.add(jsonObj);
			}
			
		}
	
		return jArray;
	}
	
	//Added by sahadeva  on 15-07-2016  
	@RequestMapping(value = "/getProgramNoForGenerateProgram", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getProgramNoForGenerateProgram(@RequestBody String jsonData) throws Exception 
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
	//	Integer PrgNo=0;
		List ProgramNoList = ahFuctionalService.getProgramNoByProgramSummary(jsonData);
		if(ProgramNoList != null && ProgramNoList.size()>0)
		{
			for (int j = 0; j < ProgramNoList.size(); j++)
			{
				Map PrgNoMap = new HashMap();
				PrgNoMap = (Map) ProgramNoList.get(j);
				Integer	PrgNo = (Integer) PrgNoMap.get("programnumber");
				jsonObj.put("programnumber", PrgNo);
				jArray.add(jsonObj);
			}
		}
		return jArray;
	}
	
	//Added by DMurty on 15-06-2016
	@RequestMapping(value = "/GetRyotNameForCode", method = RequestMethod.POST)
	public @ResponseBody
	String GetRyotNameForCode(@RequestBody String jsonData) throws Exception 
	{
		JSONArray dropDownArray = commonService.GetRyotNameForCode(jsonData);
		return dropDownArray.toString();
	}
	//Added by DMurty on 15-06-2016
	@RequestMapping(value = "/GetMaxRyotCodeByVillage", method = RequestMethod.POST)
	public @ResponseBody
	String GetMaxRyotCodeByVillage(@RequestBody String jsonData) throws Exception 
	{
		JSONArray dropDownArray = commonService.GetMaxRyotCodeByVillage(jsonData);
		return dropDownArray.toString();
	}
	
	

	@RequestMapping(value = "/loadDropDownNamesForSurietyRyot", method = RequestMethod.POST)
	public @ResponseBody
	String loadDropDownNamesForSurietyRyot(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("dropdownname");
		String columnName=Constants.DropdownName.get(dropdownName);		
		JSONArray dropDownArray = commonService.loadDropDownNamesForSurietyRyot(dropdownName,columnName);
		return dropDownArray.toString();
	}
	
	@RequestMapping(value = "/loadAddedDropDownsForMasters", method = RequestMethod.POST)
	public @ResponseBody
	String loadAddedDropDownsForMasters(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("tablename");
		String columnName=(String)jsonData.get("columnName");
		String columnName1=(String)jsonData.get("columnName1");

		JSONArray dropDownArray = commonService.loadAddedDropDownsForMasters(dropdownName,columnName,columnName1);
		return dropDownArray.toString();
	}
	
	
	@RequestMapping(value = "/onChangeDropDownValues", method = RequestMethod.POST)
	public @ResponseBody
	String onChangeDropDownValues(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("targetdropdownname");
		String whereColumnname=(String)jsonData.get("columnname");
		String value=jsonData.get("value").toString();
		String columnName=Constants.DropdownName.get(dropdownName);		
		//String columnname=(String)jsonData.get("columnname");;	
		JSONArray dropDownArray = commonService.onChangeDropDownValues(dropdownName,columnName,whereColumnname,value);
		return dropDownArray.toString();
	}
	
	@RequestMapping(value = "/onChangeDropDownValuesForVillage", method = RequestMethod.POST)
	public @ResponseBody
	String onChangeDropDownValuesForVillage(@RequestBody JSONObject jsonData) throws Exception 
	{
		String dropdownName=(String)jsonData.get("targetdropdownname");
		String whereColumnname=(String)jsonData.get("columnname");
		String value=jsonData.get("value").toString();
		String columnName=Constants.DropdownName.get(dropdownName);		
		//String columnname=(String)jsonData.get("columnname");;	
		JSONArray dropDownArray = commonService.onChangeDropDownValuesForVillage(dropdownName,columnName,whereColumnname,value);
		return dropDownArray.toString();
	}

	@RequestMapping(value = "/onChangeFieldValues", method = RequestMethod.POST)
	public @ResponseBody
	String onChangeFieldValues(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tablename=(String)jsonData.get("tablename");
		String columnname=(String)jsonData.get("columnname");
		String whereColumnname=(String)jsonData.get("wherecolumnname");		
		String value=jsonData.get("value").toString();
		
		JSONObject dropDownArray = commonService.onChangeFieldValues(tablename,columnname,whereColumnname,value);
		return dropDownArray.toString();
	}
	@RequestMapping(value = "/getLoansForSeason", method = RequestMethod.POST)
	public @ResponseBody
	String getLoansForSeason(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tablename=(String)jsonData.get("tablename");
		String columnname=(String)jsonData.get("columnname");
		String whereColumnname=(String)jsonData.get("wherecolumnname");		
		String value=jsonData.get("value").toString();
		
		JSONArray dropDownArray = commonService.getLoansForSeason(tablename,columnname,whereColumnname,value);
		return dropDownArray.toString();
	}
	
	@RequestMapping(value = "/getRyotsFromAgreementSummary", method = RequestMethod.POST)
	public @ResponseBody
	String getRyotsFromAgreementSummary(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tablename=(String)jsonData.get("tablename");//AgreementSummary
		String columnname=(String)jsonData.get("columnname");//ryotcode
		String whereColumnname=(String)jsonData.get("wherecolumnname");	//seasonyear	
		String value=jsonData.get("value").toString();//selected season in dropdown
		
		JSONArray dropDownArray = commonService.getRyotsFromAgreementSummaryNew(tablename,columnname,whereColumnname,value);
		return dropDownArray.toString();
	}

	
	@RequestMapping(value = "/getLoanNumberFromLoanDetails", method = RequestMethod.POST)
	public @ResponseBody
	String getLoanNumberFromLoanDetails(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tablename=(String)jsonData.get("tablename");   // loan table
		String columnname=(String)jsonData.get("columnname");
		String whereColumnname=(String)jsonData.get("wherecolumnname");		//season
		String value=jsonData.get("value").toString();
		
		String whereColumnname1=(String)jsonData.get("wherecolumnname1");	//ryotcode
		String value1=jsonData.get("value1").toString();
		
		JSONArray dropDownArray = commonService.getLoanNumberFromLoanDetails(tablename,columnname,whereColumnname,value,whereColumnname1,value1);
		return dropDownArray.toString();
	}
	//Added by DMurty on 02-09-2016
	@RequestMapping(value = "/getVillageNames", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getVillageNames() throws Exception 
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		List VillageList = ahFuctionalService.getVillageNames();
		logger.info("getVillageNames() ========VillageList=========="+VillageList);
		if(VillageList != null && VillageList.size()>0)
		{
			for (int j = 0; j < VillageList.size(); j++)
			{
				Map villageMap = new HashMap();
				villageMap = (Map) VillageList.get(j);
				String	villageName = (String) villageMap.get("village");
				logger.info("getVillageNames() ========villageName=========="+villageName);
				jsonObj.put("villageName", villageName);
				jArray.add(jsonObj);
			}
		}
		logger.info("getVillageNames() ========jArray=========="+jArray);
		return jArray;
	}
	
	//Added by DMurty on 07-11-2016
	@RequestMapping(value = "/getLoginUser", method = RequestMethod.POST)
	public @ResponseBody
	String getLoginUser(@RequestBody String jsonData) throws Exception 
	{
		String loginUser = loginController.getLoggedInUserName();
		if(loginUser==null ||loginUser.equalsIgnoreCase("null") || loginUser.equals("") )
		{
			loginUser = "NA";
		}
		return loginUser;
	}
	
	//Added by DMurty on 05-12-2016
	@RequestMapping(value = "/getUserNameById", method = RequestMethod.POST)
	public @ResponseBody
	String getUserNameById(@RequestBody JSONObject jsonData) throws Exception 
	{
		logger.info("getUserNameById() ========jsonData=========="+jsonData);
		String loginUser=(String)jsonData.get("loginid");
		String login = null;
		List EmployeesList = commonService.getEmployeeDetails(loginUser);
		if(EmployeesList != null && EmployeesList.size()>0)
		{
			Employees emp = (Employees) EmployeesList.get(0);
			login = emp.getEmployeename();
		}
		logger.info("getUserNameById() ========login=========="+login);
		return login;
	}
	
	
	//-------------------- End Of Common Methods -------------------------------------//
	
	
    /**
     * Save or Update Cane Manager
     */
	@RequestMapping(value = "/saveCaneManager", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveCaneManager(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			CaneManagerBean caneManagerBean = new ObjectMapper().readValue(jsonData, CaneManagerBean.class);
			CaneManager caneManager = prepareModel(caneManagerBean);
			entityList.add(caneManager);
			if(caneManagerBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(caneManagerBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(caneManagerBean.getId(),caneManager,caneManagerBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	

	private CaneManager prepareModel(CaneManagerBean caneManagerBean)
	{	
		
		Integer maxId = commonService.getMaxID("CaneManager");
		String employeeName=commonService.getEmployeeNameID(caneManagerBean.getEmployeeId());
		CaneManager caneManager = new CaneManager();
		
		String modifyFlag = caneManagerBean.getModifyFlag();
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			caneManager.setId(caneManagerBean.getId());
			caneManager.setCanemanagerid(caneManagerBean.getCaneManagerId());
		}
		else
		{
			
			caneManager.setId(caneManagerBean.getId());
			caneManager.setCanemanagerid(maxId);
		}
		
		caneManager.setCanemanagername(employeeName.toUpperCase());
		String description = caneManagerBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		caneManager.setDescription(description);
		caneManager.setStatus(caneManagerBean.getStatus());
		caneManager.setId(caneManagerBean.getId());
		caneManager.setEmployeeid(caneManagerBean.getEmployeeId());
		return caneManager;
	}

    /**
     * Loading all  Cane Managers into form
     */
	@RequestMapping(value = "/getAllCaneManagers", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<CaneManagerBean> getAllCaneManagers(@RequestBody String jsonData)throws Exception {
		
		List<CaneManagerBean> caneManagersBeans = prepareListofBean(caneManagerService.listCaneManagers());
		
		return caneManagersBeans;
	}
	

	private List<CaneManagerBean> prepareListofBean(List<CaneManager> caneManagers) 
	{
		List<CaneManagerBean> beans = null;
		
		if (caneManagers != null && !caneManagers.isEmpty()) 
		{
			beans = new ArrayList<CaneManagerBean>();
			CaneManagerBean bean = null;
			for (CaneManager caneManager : caneManagers) 
			{
				bean = new CaneManagerBean();

				bean.setId(caneManager.getId());
				bean.setCaneManagerId(caneManager.getCanemanagerid());
				bean.setCaneManagerName(caneManager.getCanemanagername());
				bean.setDescription(caneManager.getDescription());
				bean.setEmployeeId(caneManager.getEmployeeid());
				bean.setStatus(caneManager.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}

	//-------------------- LandType Master-----------------------------------------//

	@RequestMapping(value = "/saveLandType", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveLandType(@RequestBody String jsonData) throws Exception 
	{	
		boolean isInsertSuccess  = false;
		try 
		{
			List entityList = new ArrayList();
			
			LandTypeMasterBean landTypeMasterBean = new ObjectMapper().readValue(jsonData, LandTypeMasterBean.class);
			LandType landType = prepareModel(landTypeMasterBean);
			entityList.add(landType);
			
			//landTypeService.addLandType(landType);
			
			if(landTypeMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(landTypeMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(landTypeMasterBean.getId(),landType,landTypeMasterBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private LandType prepareModel(LandTypeMasterBean landTypeMasterBean) 
	{

		LandType landType = new LandType();
		String modifyFlag = landTypeMasterBean.getModifyFlag();
		int landtypeid = commonService.getMaxIDforColumn("LandType","landtypeid");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			landType.setLandtypeid(landTypeMasterBean.getLandTypeId());
		}
		else
		{
			landType.setLandtypeid(landtypeid);
		}
		
		landType.setId(landTypeMasterBean.getId());
		landType.setLandtype(landTypeMasterBean.getLandType().toUpperCase());
		
		String description = landTypeMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		landType.setDescription(description);
		landType.setStatus(landTypeMasterBean.getStatus());
		return landType;
	}


	@RequestMapping(value = "/getAllLandTypes", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<LandTypeMasterBean> getAllLandTypes(@RequestBody String jsonData) throws Exception 
	{
		
		List<LandTypeMasterBean> landTypeMasterBeans = prepareListofBeanRyotType(landTypeService.listLandTypes());
		return landTypeMasterBeans;
	}

	private List<LandTypeMasterBean> prepareListofBeanRyotType(List<LandType> landTypes) 
	{
		List<LandTypeMasterBean> beans = null;
		if (landTypes != null && !landTypes.isEmpty()) 
		{
			beans = new ArrayList<LandTypeMasterBean>();
			LandTypeMasterBean bean = null;
			for (LandType landType : landTypes) 
			{
				bean = new LandTypeMasterBean();
				bean.setId(landType.getId());
				bean.setLandTypeId(landType.getLandtypeid());
				bean.setLandType(landType.getLandtype());
				bean.setDescription(landType.getDescription());
				bean.setStatus(landType.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//---------------------------- End Of LandType Master-----------------------------------------//

	//-----------------------------Field Officer------------------------------------------------//

	@RequestMapping(value = "/saveFieldOfficer", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveFieldOfficer(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			FieldOfficerMasterBean fieldOfficerMasterBean = new ObjectMapper().readValue(jsonData, FieldOfficerMasterBean.class);
			FieldOfficer fieldOfficer = prepareModelFieldOfficer(fieldOfficerMasterBean);
			entityList.add(fieldOfficer);
			if(fieldOfficerMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(fieldOfficerMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(fieldOfficerMasterBean.getId(),fieldOfficer,fieldOfficerMasterBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}		
	}

	private FieldOfficer prepareModelFieldOfficer(FieldOfficerMasterBean fieldOfficerMasterBean) 
	{

		String employeeName=commonService.getEmployeeNameID(fieldOfficerMasterBean.getEmployeeId());
		
		FieldOfficer fieldOfficer = new FieldOfficer();
		
		

			String modifyFlag = fieldOfficerMasterBean.getModifyFlag();
			int fieldOfficerid = commonService.getMaxIDforColumn("FieldOfficer","fieldOfficerid");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				fieldOfficer.setFieldOfficerid(fieldOfficerMasterBean.getFieldOfficerId());
			}
			else
			{
				fieldOfficer.setFieldOfficerid(fieldOfficerid);
			}

		fieldOfficer.setCaneManagerId(fieldOfficerMasterBean.getCaneManagerId());
		fieldOfficer.setEmployeeId(fieldOfficerMasterBean.getEmployeeId());
		fieldOfficer.setFieldOfficer(employeeName.toUpperCase());
		fieldOfficer.setFieldOfficerid(fieldOfficerMasterBean.getFieldOfficerId());
		String description = fieldOfficerMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		fieldOfficer.setDescription(description);
		fieldOfficer.setStatus(fieldOfficerMasterBean.getStatus());
		fieldOfficer.setId(fieldOfficerMasterBean.getId());
		return fieldOfficer;
	}

	@RequestMapping(value = "/getAllFieldOfficers", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<FieldOfficerMasterBean> getAllFieldOfficers(
			@RequestBody String jsonData) throws Exception {

		List<FieldOfficerMasterBean> fieldOfficerMasterBeans = prepareListofBeanForFieldOfficer(fieldOfficerService
				.listFieldOfficers());
		return fieldOfficerMasterBeans;
	}

	private List<FieldOfficerMasterBean> prepareListofBeanForFieldOfficer(
			List<FieldOfficer> fieldOfficers) {
		List<FieldOfficerMasterBean> beans = null;
		if (fieldOfficers != null && !fieldOfficers.isEmpty()) {
			beans = new ArrayList<FieldOfficerMasterBean>();
			FieldOfficerMasterBean bean = null;
			for (FieldOfficer fieldOfficer : fieldOfficers) {
				bean = new FieldOfficerMasterBean();

				bean.setId(fieldOfficer.getId());
				bean.setDescription(fieldOfficer.getDescription());
				bean.setStatus(fieldOfficer.getStatus());
				bean.setCaneManagerId(fieldOfficer.getCaneManagerId());
				bean.setFieldOfficerId(fieldOfficer.getFieldOfficerid());
				bean.setFieldOfficer(fieldOfficer.getFieldOfficer());
				bean.setEmployeeId(fieldOfficer.getEmployeeId());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}

	//----------------------------- End Of Field Officer -------------------------------------//
	
	//----------------------------- Field Assistant ---------------------------------------//
	
	@RequestMapping(value = "/saveFieldAssistant", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveFieldAssistant(@RequestBody String jsonData) throws Exception 
	{
		List entityList = new ArrayList();
		boolean isInsertSuccess  = false;
		try 
		{
			FieldAssistantBean fieldAssistantBean = new ObjectMapper().readValue(jsonData, FieldAssistantBean.class);
			FieldAssistant fieldAssistant = prepareModelForFieldAssistant(fieldAssistantBean);
			entityList.add(fieldAssistant);
			if(fieldAssistantBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(fieldAssistantBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(fieldAssistantBean.getId(),fieldAssistant,fieldAssistantBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private FieldAssistant prepareModelForFieldAssistant(FieldAssistantBean fieldAssistantBean) {
		
		FieldAssistant fieldAssistant = new FieldAssistant();

			String modifyFlag = fieldAssistantBean.getModifyFlag();
			int fieldassistantid = commonService.getMaxIDforColumn("FieldAssistant","fieldassistantid");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				fieldAssistant.setFieldassistantid(fieldAssistantBean.getFieldAssistantId());
			}
			else
			{
				fieldAssistant.setFieldassistantid(fieldassistantid);
			}

		
		String employeeName=commonService.getEmployeeNameID(fieldAssistantBean.getEmployeeId());
		
		Integer caneMgrId=commonService.getCaneManagerID(fieldAssistantBean.getFieldOfficerId());
		
		fieldAssistant.setId(fieldAssistantBean.getId());
		fieldAssistant.setFieldassistantid(fieldAssistantBean.getFieldAssistantId());
		fieldAssistant.setCaneManagerid(caneMgrId);
		fieldAssistant.setFieldassistant(employeeName);
		fieldAssistant.setEmployeeid(fieldAssistantBean.getEmployeeId());
		String description = fieldAssistantBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		fieldAssistant.setDescription(description);
		fieldAssistant.setStatus(fieldAssistantBean.getStatus());
		fieldAssistant.setFieldofficerid(fieldAssistantBean.getFieldOfficerId());

		return fieldAssistant;
	}
	
	
	@RequestMapping(value = "/getAllFieldAssistants", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<FieldAssistantBean> getAllFieldAssistants(@RequestBody String jsonData) throws Exception {

		List<FieldAssistantBean> fieldAssistantBeans = prepareListofBeanForgetAllFieldAssistants(fieldAssistantService
				.listFieldAssistants());
		return fieldAssistantBeans;
	}

	private List<FieldAssistantBean> prepareListofBeanForgetAllFieldAssistants(
			List<FieldAssistant> fieldAssistants) {
		List<FieldAssistantBean> beans = null;
		if (fieldAssistants != null && !fieldAssistants.isEmpty()) {
			beans = new ArrayList<FieldAssistantBean>();
			FieldAssistantBean bean = null;
			for (FieldAssistant fieldAssistant : fieldAssistants) {
				bean = new FieldAssistantBean();

				bean.setId(fieldAssistant.getId());
				bean.setFieldAssistantId(fieldAssistant.getFieldassistantid());
				bean.setCaneManagerId(fieldAssistant.getCaneManagerid());
				bean.setEmployeeId(fieldAssistant.getEmployeeid());
				bean.setFieldOfficerId(fieldAssistant.getFieldofficerid());
				bean.setFieldAssistant(fieldAssistant.getFieldassistant());
				bean.setDescription(fieldAssistant.getDescription());
				bean.setStatus(fieldAssistant.getStatus());				
                bean.setModifyFlag("Yes"); 
				beans.add(bean);
			}
		}
		return beans;
	}
	//----------------------------- End Of Field Assistant ---------------------------------------//
	//----------------------------- Variety Master ----------------------------------------------//
	
	@RequestMapping(value = "/saveVarietyMaster", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveVarietyMaster(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveVarietyMaster() from AgricultureHarvestingController========"+ jsonData.toString());

			VarietyMasterBean varietyMasterBean = new ObjectMapper().readValue(jsonData, VarietyMasterBean.class);
			Variety variety = prepareModelForVarietyMaster(varietyMasterBean);
			entityList.add(variety);
			if(varietyMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(varietyMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(varietyMasterBean.getId(),variety,varietyMasterBean.getScreenName());		
			}
			
			isInsertSuccess = commonService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Variety prepareModelForVarietyMaster(VarietyMasterBean varietyMasterBean) {
		
		Variety variety = new Variety();
		
		

			String modifyFlag = varietyMasterBean.getModifyFlag();
			int varietycode = commonService.getMaxIDforColumn("Variety","varietycode");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				variety.setVarietycode(varietyMasterBean.getVarietyCode());
			}
			else
			{
				variety.setVarietycode(varietycode);
			}
		
		variety.setId(varietyMasterBean.getId());
		variety.setVariety(varietyMasterBean.getVariety().toUpperCase());
		String description = varietyMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		variety.setDescription(description);
		variety.setStatus(varietyMasterBean.getStatus());

		return variety;
	}
	
	
	@RequestMapping(value = "/getAllVarietyMasters", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<VarietyMasterBean> getAllVarietyMasters(
			@RequestBody String jsonData) throws Exception {

		List<VarietyMasterBean> varietyMasterBeans = prepareListofBeanForgetAllVarietyMasters(varietyService.listVarieties());
		return varietyMasterBeans;
	}

	private List<VarietyMasterBean> prepareListofBeanForgetAllVarietyMasters(
			List<Variety> varieties) {
		List<VarietyMasterBean> beans = null;
		if (varieties != null && !varieties.isEmpty()) {
			beans = new ArrayList<VarietyMasterBean>();
			VarietyMasterBean bean = null;
			for (Variety variety : varieties) {
				bean = new VarietyMasterBean();
				bean.setId(variety.getId());
				bean.setVarietyCode(variety.getVarietycode());
				bean.setVariety(variety.getVariety());
				bean.setDescription(variety.getDescription());
				bean.setStatus(variety.getStatus());	
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	//----------------------------- End Of Variety Master --------------------------------------------//
	
	//----------------------------- Advance Category Master ------------------------------------------//
	@RequestMapping(value = "/saveAdvanceCategory", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean saveAdvanceCategory(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveAdvanceCategory() from AgricultureHarvestingController========"+ jsonData.toString());
			AdvanceCategoryBean advanceCategoryBean = new ObjectMapper().readValue(jsonData, AdvanceCategoryBean.class);
			AdvanceCategory advanceCategory = prepareModelForAdvanceCategory(advanceCategoryBean);
			entityList.add(advanceCategory);
			
			if(advanceCategoryBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(advanceCategoryBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(advanceCategoryBean.getId(),advanceCategory,advanceCategoryBean.getScreenName());		
			}
			
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private AdvanceCategory prepareModelForAdvanceCategory(AdvanceCategoryBean advanceCategoryBean) {

		AdvanceCategory advanceCategory = new AdvanceCategory();

		String modifyFlag = advanceCategoryBean.getModifyFlag();
		int advancecategorycode = commonService.getMaxIDforColumn("AdvanceCategory","advancecategorycode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			advanceCategory.setAdvancecategorycode(advanceCategoryBean.getAdvanceCategoryCode());
		}
		else
		{
			advanceCategory.setAdvancecategorycode(advancecategorycode);
		}
		
		advanceCategory.setId(advanceCategoryBean.getId());
		advanceCategory.setAdvancecode(advanceCategoryBean.getAdvanceCode());
		String description = advanceCategoryBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		advanceCategory.setDescription(description);
		advanceCategory.setStatus(advanceCategoryBean.getStatus());
		advanceCategory.setName(advanceCategoryBean.getAdvanceCategoryName().toUpperCase());
		return advanceCategory;
	}
	
	@RequestMapping(value = "/getAllAdvanceCategories", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<AdvanceCategoryBean> getAllAdvanceCategories(@RequestBody String jsonData) throws Exception {

		List<AdvanceCategoryBean> advanceCategoryBeans = prepareListofBeanForAdvType(advanceCategoryService
				.listAdvanceCategories());
		return advanceCategoryBeans;
	}
	
	private List<AdvanceCategoryBean> prepareListofBeanForAdvType(List<AdvanceCategory> advanceCategories)
	{
		List<AdvanceCategoryBean> beans = null;
		if (advanceCategories != null && !advanceCategories.isEmpty())
		{
			beans = new ArrayList<AdvanceCategoryBean>();
			AdvanceCategoryBean bean = null;
			for (AdvanceCategory advanceCategory : advanceCategories)
			{
				bean = new AdvanceCategoryBean();
				bean.setId(advanceCategory.getId());
				bean.setAdvanceCategoryCode(advanceCategory.getAdvancecategorycode());
				bean.setDescription(advanceCategory.getDescription());
				bean.setAdvanceCode(advanceCategory.getAdvancecode());
				bean.setStatus(advanceCategory.getStatus());
				bean.setAdvanceCategoryName(advanceCategory.getName());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//----------------------------- End Of Advance Category Master ----------------------------//
	
	//----------------------------- Zone Master ---------------------------------------------//	
	
	@RequestMapping(value = "/saveZoneMaster", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveZoneMaster(@RequestBody String jsonData) throws Exception 
	{		
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			ZoneMasterBean zoneMasterBean = new ObjectMapper().readValue(jsonData, ZoneMasterBean.class);
			Zone zone = prepareModelForVarietyMaster(zoneMasterBean);
			entityList.add(zone);
			if(zoneMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(zoneMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(zoneMasterBean.getId(),zone,zoneMasterBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	private Zone prepareModelForVarietyMaster(ZoneMasterBean zoneMasterBean) {		
		Zone zone = new Zone();
		
		Integer caneMgrId=commonService.getCaneManagerID(zoneMasterBean.getFieldOfficerId());
		String modifyFlag=zoneMasterBean.getModifyFlag();
		int zonecode=commonService.getMaxIDforColumn("Zone", "zonecode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
		zone.setZonecode(zoneMasterBean.getZoneCode());	
		}
		else
		{
		zone.setZonecode(zonecode);	
		}
		zone.setId(zoneMasterBean.getId());
		//zone.setZonecode(zoneMasterBean.getZoneCode());
		zone.setZone(zoneMasterBean.getZone().toUpperCase());
		zone.setRegioncode(zoneMasterBean.getRegionCode());
		zone.setCanemanagerid(caneMgrId);
		zone.setFieldofficerid(zoneMasterBean.getFieldOfficerId());
		zone.setStatus(zoneMasterBean.getStatus());
		String description = zoneMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		zone.setDescription(description);
		
		return zone;
	}
	
	
	@RequestMapping(value = "/getAllZoneMasters", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	List<ZoneMasterBean> getAllZoneMasters(	@RequestBody String jsonData) throws Exception
	{
		List<ZoneMasterBean> zoneMasterBeans = prepareListofBeanForgetAllZones(zoneService.listZones());
		return zoneMasterBeans;
	}

	private List<ZoneMasterBean> prepareListofBeanForgetAllZones(
			List<Zone> zones) {
		List<ZoneMasterBean> beans = null;
		if (zones != null && !zones.isEmpty()) {
			beans = new ArrayList<ZoneMasterBean>();
			ZoneMasterBean bean = null;
			for (Zone zone : zones) {
				bean = new ZoneMasterBean();
				
				bean.setId(zone.getId());
				bean.setDescription(zone.getDescription());
				bean.setStatus(zone.getStatus());	
				bean.setCaneManagerId(zone.getCanemanagerid());
				bean.setRegionCode(zone.getRegioncode());
				bean.setFieldOfficerId(zone.getFieldofficerid());
				bean.setZone(zone.getZone());
				bean.setZoneCode(zone.getZonecode());	
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//---------------------------------- End Of Zone Master ------------------------------------//
	
	//----------------------------------- Advance SubCategory Master -------------------------//
	
	@RequestMapping(value = "/saveAdvanceSubCategory", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveAdvanceSubCategory(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			AdvanceSubCategoryBean advanceSubCategoryBean = new ObjectMapper().readValue(jsonData, AdvanceSubCategoryBean.class);
			AdvanceSubCategory advanceSubCategory = prepareModelForAdvanceSubCtgry(advanceSubCategoryBean);
			
			entityList.add(advanceSubCategory);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private AdvanceSubCategory prepareModelForAdvanceSubCtgry(AdvanceSubCategoryBean advanceSubCategoryBean) {

		AdvanceSubCategory advanceSubCategory = new AdvanceSubCategory();
		
		String modifyFlag=advanceSubCategoryBean.getModifyFlag();
		//int subcategorycode=commonService.getMaxIDforColumn("AdvanceSubCategory", "subcategorycode");
		//if("Yes".equalsIgnoreCase(modifyFlag))
		//{
			advanceSubCategory.setSubcategorycode(advanceSubCategoryBean.getSubCategoryCode());	
		//}
		//else
		//{
		//	advanceSubCategory.setSubcategorycode(subcategorycode);	
		//}
		advanceSubCategory.setAdvanceCategoryCode(advanceSubCategoryBean.getAdvanceCategoryCode());
		advanceSubCategory.setAdvanceCode(advanceSubCategoryBean.getAdvanceCode());
		advanceSubCategory.setName(advanceSubCategoryBean.getName().toUpperCase());
		advanceSubCategory.setId(advanceSubCategoryBean.getId());
		String description = advanceSubCategoryBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		advanceSubCategory.setDescription(description);
		advanceSubCategory.setStatus(advanceSubCategoryBean.getStatus());
		//advanceSubCategory.setSubcategorycode(advanceSubCategoryBean.getSubCategoryCode());
		return advanceSubCategory;
	}
	
	@RequestMapping(value = "/getAllAdvanceSubCategories", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<AdvanceSubCategoryBean> getAllAdvanceSubCategories(@RequestBody String jsonData)
			throws Exception
	{
		logger.info("====getAllAdvanceSubCategories()  from AgricultureHarvestingController========");
		List<AdvanceSubCategoryBean> advanceSubCategoryBeans = prepareListofAdvanceSubCategoriesBean(advanceSubCategoryService
				.listAllAdvanceSubCategories());
		return advanceSubCategoryBeans;
	}

	private List<AdvanceSubCategoryBean> prepareListofAdvanceSubCategoriesBean(List<AdvanceSubCategory> advanceSubCategories) 
	{
		List<AdvanceSubCategoryBean> beans = null;
		if (advanceSubCategories != null && !advanceSubCategories.isEmpty()) 
		{
			beans = new ArrayList<AdvanceSubCategoryBean>();
			AdvanceSubCategoryBean bean = null;
			for (AdvanceSubCategory advanceSubCategory : advanceSubCategories)
			{
				bean = new AdvanceSubCategoryBean();
				
				bean.setAdvanceCategoryCode(advanceSubCategory.getAdvanceCategoryCode());
				bean.setAdvanceCode(advanceSubCategory.getAdvanceCode());
				bean.setName(advanceSubCategory.getName());
				bean.setDescription(advanceSubCategory.getDescription());
				bean.setId(advanceSubCategory.getId());
				bean.setStatus(advanceSubCategory.getStatus());
				bean.setSubCategoryCode(advanceSubCategory.getSubcategorycode());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//----------------------------------- End OfAdvance SubCategory Master -------------------------//
	
	//------------------------------------ Region Master ----------------------------------------//
	@RequestMapping(value = "/saveRegion", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveRegion(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("---------Regions Saving---------------"+jsonData);
			RegionBean regionBean = new ObjectMapper().readValue(jsonData, RegionBean.class);
			Region region = prepareModelForRegion(regionBean);
			
			entityList.add(region);
			if(regionBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(regionBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(regionBean.getId(),region,regionBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
	
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	private Region prepareModelForRegion(RegionBean regionBean) 
	{
		Region region = new Region();
		String modifyFlag=regionBean.getModifyFlag();
		int regioncode=commonService.getMaxIDforColumn("Region","regioncode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			region.setRegioncode(regionBean.getRegionCode());
		}
		else
		{
			region.setRegioncode(regioncode);
		}
		region.setId(regionBean.getId());
		//region.setRegioncode(regionBean.getRegionCode());
		region.setRegion(regionBean.getRegion().toUpperCase());
		String description = regionBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		region.setDescription(description);
		region.setStatus(regionBean.getStatus());
		
		return region;
	}
	
	@RequestMapping(value = "/getAllRegions", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<RegionBean> getAllRegions(@RequestBody String jsonData)
			throws Exception {

		List<RegionBean> regionBeans = prepareListofRegionBean(regionService.listRegions());
		
		logger.info("---------Regions---------------"+regionBeans.toString());
		return regionBeans;
	}
	
	private List<RegionBean> prepareListofRegionBean(List<Region> regions)
	{
		List<RegionBean> beans = null;
		if (regions != null && !regions.isEmpty())
		{
			beans = new ArrayList<RegionBean>();
			RegionBean bean = null;
			for (Region region : regions)
			{
				bean = new RegionBean();

				bean.setId(region.getId());
				bean.setRegionCode(region.getRegioncode());
				bean.setRegion(region.getRegion());
				bean.setDescription(region.getDescription());
				bean.setStatus(region.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//------------------------------------ End Of Region Master ---------------------------------//
	//-------------------------------- Mandal Master --------------------------------------//
	
	@RequestMapping(value = "/saveMandal", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveMandal(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveMandal() from AgricultureHarvestingController========"+ jsonData.toString());
			MandalBean mandalBean = new ObjectMapper().readValue(jsonData, MandalBean.class);
			Mandal mandal = prepareModelForMandal(mandalBean);
			entityList.add(mandal);
			if(mandalBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(mandalBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(mandalBean.getId(),mandal,mandalBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Mandal prepareModelForMandal(MandalBean mandalBean) 
	{
		Mandal mandal = new Mandal();
		String modifyFlag=mandalBean.getModifyFlag();
		int mandalcode=commonService.getMaxIDforColumn("Mandal", "mandalcode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			mandal.setMandalcode(mandalBean.getMandalCode());
		
		}
		else
		{
		    mandal.setMandalcode(mandalcode);	
		}
		mandal.setId(mandalBean.getId());
		//mandal.setMandalcode(mandalBean.getMandalCode());
		mandal.setMandal(mandalBean.getMandal().toUpperCase());
		mandal.setZonecode(mandalBean.getZoneCode());
		String description = mandalBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		mandal.setDescription(description);
		mandal.setStatus(mandalBean.getStatus());
		
		return mandal;
	}
	
	@RequestMapping(value = "/getAllMandals", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<MandalBean> getAllMandals(@RequestBody String jsonData)
			throws Exception {

		logger.info("====getAllRegions()  from AgricultureHarvestingController========");

		List<MandalBean> mandalBeans = prepareListofMandalBean(mandalService.listMandals());
		return mandalBeans;
	}
	
	private List<MandalBean> prepareListofMandalBean(List<Mandal> mandals)
	{
		List<MandalBean> beans = null;
		if (mandals != null && !mandals.isEmpty())
		{
			beans = new ArrayList<MandalBean>();
			MandalBean bean = null;
			for (Mandal mandal : mandals)
			{
				bean = new MandalBean();
				
				bean.setId(mandal.getId());
				bean.setMandalCode(mandal.getMandalcode());
				bean.setMandal(mandal.getMandal());
				bean.setZoneCode(mandal.getZonecode());
				bean.setDescription(mandal.getDescription());
				bean.setStatus(mandal.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	

	//--------------------------------End of Mandal Master----------------------------------//
	
//------------------------------Village Master----------------------------------------//
	@RequestMapping(value = "/saveVillage", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveVillage(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveViilage() from AgricultureHarvestingController========"+ jsonData.toString());
			VillageMasterBean villageMasterBean = new ObjectMapper().readValue(jsonData, VillageMasterBean.class);
			Village village = prepareModelForVillage(villageMasterBean);
			entityList.add(village);
			if(villageMasterBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(villageMasterBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(villageMasterBean.getId(),village,villageMasterBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Village prepareModelForVillage(VillageMasterBean villageMasterBean) 
	{
		Village village = new Village();
		
		
		village.setId(villageMasterBean.getId());
		//village.setVillagecode(villageMasterBean.getVillageCode());
		
		village.setMandalcode(villageMasterBean.getMandalCode());
	
		int mandalCode = villageMasterBean.getMandalCode();
		String modifyFlag=villageMasterBean.getModifyFlag();
		//int villagecode=commonService.getMaxIDforColumn("Village", "villagecode");
		String villagecode = commonService.loadMaxIdForVillageForMaster(mandalCode);
		String strvillagecode = villagecode;//Integer.toString(villagecode);
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			village.setVillagecode(villageMasterBean.getVillageCode());
		}
		else
		{
			village.setVillagecode(strvillagecode);
		}
		//Modified by DMurty on 10-08-2016
		if(villageMasterBean.getStatus() != null)
		{
			village.setStatus(villageMasterBean.getStatus());
		}
		else
		{
			village.setStatus((byte) 0);
		}
		village.setVillage(villageMasterBean.getVillageName().toUpperCase());
		String description = villageMasterBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		village.setDescription(description);
		
		if(villageMasterBean.getDistance() != null)
		{
			village.setDistance(villageMasterBean.getDistance());
		}
		else
		{
			village.setDistance(0.0);
		}
		village.setPincode(villageMasterBean.getPincode());
		return village;
	}
	
	@RequestMapping(value = "/getAllVillages", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<VillageMasterBean> getAllVillages(@RequestBody String jsonData) throws Exception {

		List<VillageMasterBean> villageBeans = prepareListofVillageBean(villageService.listVillagesNew());
		return villageBeans;
	}
	
	/*private List<VillageMasterBean> prepareListofVillageBean(List<Village> villages)
	{
		List<VillageMasterBean> beans = null;
		if (villages != null && !villages.isEmpty())
		{
			beans = new ArrayList<VillageMasterBean>();
			VillageMasterBean bean = null;
			for (Village village : villages)
			{
				String mandalName="NA";
				bean = new VillageMasterBean();				
				bean.setId(village.getId());
				bean.setVillageCode(village.getVillagecode());
				bean.setVillageName(village.getVillage());
				bean.setMandalCode(village.getMandalcode());				
				bean.setDescription(village.getDescription());
				bean.setStatus(village.getStatus());
				bean.setDistance(village.getDistance());
			    bean.setModifyFlag("Yes");
			   			    
			    List MandalList = commonService.getMandal(village.getMandalcode());
			    if(MandalList!=null && MandalList.size()>0)
				{
					Map mandalMap=(Map)MandalList.get(0);
					mandalName=(String)mandalMap.get("mandal");
				}
			    bean.setMandal(mandalName);
				beans.add(bean);
			}
		}
		return beans;
	}*/
	
	private List<VillageMasterBean> prepareListofVillageBean(List<Village> villages)
	{
		List<VillageMasterBean> beans = null;
		if (villages != null && villages.size()>0)
		{
			beans = new ArrayList<VillageMasterBean>();
			VillageMasterBean bean = null;
			for(int i = 0;i<villages.size();i++)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) villages.get(i);
				
				int id = (Integer) tempMap.get("id");
				String villageCode = (String) tempMap.get("villagecode");
				String villageName = (String) tempMap.get("village");
				int mandalCode = (Integer) tempMap.get("mandalcode");
				String description = (String) tempMap.get("description");
				double distance = (Double) tempMap.get("distance");
				String mandalname = (String) tempMap.get("mandal");
				String pincode = (String) tempMap.get("pincode");
				byte status = (Byte) tempMap.get("status");

				bean = new VillageMasterBean();				
				bean.setId(id);
				bean.setVillageCode(villageCode);
				bean.setVillageName(villageName);
				bean.setMandalCode(mandalCode);				
				bean.setDescription(description);
				bean.setStatus(status);
				bean.setDistance(distance);
			    bean.setModifyFlag("Yes");
			    bean.setMandal(mandalname);
			    bean.setPincode(pincode);
				beans.add(bean);
			}
		}
		return beans;
	}
	
//-------------------------------- Circle Master----------------------------------//
	
	@RequestMapping(value = "/saveCircle", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveCircle(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			CircleBean circleBean = new ObjectMapper().readValue(jsonData, CircleBean.class);		
			Circle circle = prepareModelForCircle(circleBean);
			
			entityList.add(circle);
			
			if(circleBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(circleBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(circleBean.getId(),circle,circleBean.getScreenName());		
			}
			
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Circle prepareModelForCircle(CircleBean circleBean) 
	{
		Circle circle = new Circle();
		String modifyFlag=circleBean.getModifyFlag();
		int circlecode=commonService.getMaxIDforColumn("Circle", "circlecode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			circle.setCirclecode(circleBean.getCircleCode());
			
			String sqq1="update AgreementDetails set zonecode="+circleBean.getZone()+" where circlecode="+circleBean.getCircleCode()+" and seasonyear='2017-2018'";
			SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq1);
			query1.executeUpdate();
			String sqq2="update RandDAgreementDetails set zonecode="+circleBean.getZone()+" where circlecode="+circleBean.getCircleCode()+" and seasonyear='2017-2018'";
			SQLQuery query2=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq2);
			query2.executeUpdate();
			
		}
		else
		{
			circle.setCirclecode(circlecode);
		}
		
		
		FieldAssistant fieldAssistant=(FieldAssistant) commonService.getEntityById(circleBean.getFieldAssistantId(),FieldAssistant.class);
		
		logger.info("----------fieldAssistant---------"+fieldAssistant.toString());
		circle.setId(circleBean.getId());
		//circle.setCirclecode(circleBean.getCircleCode());
		circle.setVillagecode(circleBean.getVillageCode());
		circle.setCircle(circleBean.getCircle().toUpperCase());
		String description = circleBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		circle.setDescription(description);
		circle.setFieldassistantid(circleBean.getFieldAssistantId());
		circle.setFieldofficerid(fieldAssistant.getFieldofficerid());
		circle.setCanemanagerid(fieldAssistant.getCaneManagerid());
		circle.setZonecode(circleBean.getZone());
		circle.setStatus(circleBean.getStatus());
		return circle;
	}
	
	@RequestMapping(value = "/getAllCircles", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<CircleBean> getAllCircles(@RequestBody String jsonData) throws Exception {


		List<CircleBean> circleBeans = prepareListofCircleBean(circleService.listCircleNew());
		return circleBeans;
	}
	
	private List<CircleBean> prepareListofCircleBean(List circles)
	{
		String village = null;
		List<CircleBean> beans = null;
		if (circles != null && !circles.isEmpty())
		{
			beans = new ArrayList<CircleBean>();
			CircleBean bean = null;
			for(int i = 0;i<circles.size();i++)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) circles.get(i);
				
				int id = (Integer) tempMap.get("id");
				String villageCode = (String) tempMap.get("villagecode");
				String villageName = (String) tempMap.get("village");
				int circlecode = (Integer) tempMap.get("circlecode");
				String description = (String) tempMap.get("description");
				Integer faid = (Integer) tempMap.get("fieldassistantid");
				Integer foid = (Integer) tempMap.get("fieldofficerid");
				Integer cmid = (Integer) tempMap.get("canemanagerid");
				String circle = (String) tempMap.get("circle");
				byte status = (Byte) tempMap.get("status");
				String fieldAssistant=(String) tempMap.get("fieldassistant");
				Integer zonecode = (Integer) tempMap.get("zonecode");
				
				bean = new CircleBean();
				
				bean.setId(id);
				bean.setCircleCode(circlecode);
				bean.setVillageCode(villageCode);
				bean.setCircle(circle);
				bean.setDescription(description);
				bean.setFieldAssistantId(faid);
				bean.setFieldOfficerId(foid);
				bean.setCaneManagerId(cmid);
				bean.setStatus(status);
				bean.setModifyFlag("Yes");
				bean.setVillage(villageName);
				bean.setFieldAssistant(fieldAssistant);
				bean.setZone(zonecode);
				if(zonecode==null)
				{
					bean.setZoneName("");
				
				}
				else
				{
					bean.setZoneName(ahFuctionalService.GetZoneNameByZoneCode(zonecode));
				}
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
	//-------------------------------- Season Master----------------------------------//

	@RequestMapping(value = "/saveSeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveSeason(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveSeason() from AgricultureHarvestingController========"+ jsonData.toString());
			SeasonBean seasonBean = new ObjectMapper().readValue(jsonData, SeasonBean.class);
			Season season = prepareModelForSeason(seasonBean);
			
			entityList.add(season);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	private Season prepareModelForSeason(SeasonBean seasonBean) 
	{
		Season season = new Season();
		String modifyFlag=seasonBean.getModifyFlag();
		int seasonid=commonService.getMaxIDforColumn("Season", "seasonid");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			season.setSeasonid(seasonBean.getSeasonId());
		}
		else
		{
			season.setSeasonid(seasonid);
		}
		season.setId(seasonBean.getId());
		//season.setSeasonid(seasonBean.getSeasonId());
		season.setSeason(seasonBean.getSeason());
		season.setFromyear(seasonBean.getFromYear());
		season.setToyear(seasonBean.getToYear());
		String description = seasonBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		season.setDescription(description);
		season.setStatus(seasonBean.getStatus());
		return season;
	}
	
	@RequestMapping(value = "/getAllSeasons", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<SeasonBean> getAllSeasons(@RequestBody String jsonData)throws Exception
	{
		try
		{
			logger.info("====getAllSeasons()  from AgricultureHarvestingController========");
			List<SeasonBean> seasonBeans = prepareListofSeasonBean(seasonService.listSeasons());
			return seasonBeans;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return null;
        }
	}
	
	private List<SeasonBean> prepareListofSeasonBean(List<Season> seasons)
	{
		List<SeasonBean> beans = null;
		if (seasons != null && !seasons.isEmpty())
		{
			beans = new ArrayList<SeasonBean>();
			SeasonBean bean = null;
			for (Season season : seasons)
			{
				bean = new SeasonBean();
				
				bean.setId(season.getId());
				bean.setSeasonId(season.getSeasonid());
				bean.setSeason(season.getSeason());
				bean.setFromYear(season.getFromyear());
				bean.setToYear(season.getToyear());
				bean.setDescription(season.getDescription());
				bean.setStatus(season.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	//-------------------------------- End of Season Master----------------------------------//

	//-------------------------------- Family Group Master----------------------------------//
	@RequestMapping(value = "/saveFamilyGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveFamilyGroup(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveFamilyGroup() from AgricultureHarvestingController========"+ jsonData.toString());
			FamilyGroupBean familyGroupBean = new ObjectMapper().readValue(jsonData, FamilyGroupBean.class);
			FamilyGroup familyGroup = prepareModelForFamilyGroup(familyGroupBean);
			entityList.add(familyGroup);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private FamilyGroup prepareModelForFamilyGroup(FamilyGroupBean familyGroupBean) 
	{
		FamilyGroup familyGroup = new FamilyGroup();
		String modifyFlag=familyGroupBean.getModifyFlag();
		int familygroupcode=commonService.getMaxIDforColumn("FamilyGroup", "familygroupcode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			familyGroup.setFamilygroupcode(familyGroupBean.getFamilyGroupCode());
		}
		else
		{
			familyGroup.setFamilygroupcode(familygroupcode);
		}
		familyGroup.setId(familyGroupBean.getId());
		//familyGroup.setFamilygroupcode(familyGroupBean.getFamilyGroupCode());
		familyGroup.setGroupname(familyGroupBean.getFamilyGroupName().toUpperCase());
		familyGroup.setZonecode(familyGroupBean.getZoneCode());
		familyGroup.setCirclecode(familyGroupBean.getCircleCode());
		String description = familyGroupBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		familyGroup.setDescription(description);
		familyGroup.setStatus(familyGroupBean.getStatus());
		//Added by DMurty on 22-06-2016
		familyGroup.setFamilygroupmembers(familyGroupBean.getFamilyGroupMembers());
	
		return familyGroup;
	}
	
	
	@RequestMapping(value = "/getAllFamilyGroups", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<FamilyGroupBean> getAllFamilyGroups(@RequestBody String jsonData)throws Exception
	{
		logger.info("====getAllFamilyGroups()  from AgricultureHarvestingController========");
		List<FamilyGroupBean> familyGroupBeans = prepareListofFamilyGroupBeans(familyGroupService.listFamilyGroups());
		logger.info("====getAllFamilyGroups()  from AgricultureHarvestingController Returning ========"+familyGroupBeans);
		return familyGroupBeans;
	}

	private List<FamilyGroupBean> prepareListofFamilyGroupBeans(List<FamilyGroup> familyGroups)
	{
		List<FamilyGroupBean> beans = null;
		if (familyGroups != null && !familyGroups.isEmpty())
		{
			beans = new ArrayList<FamilyGroupBean>();
			FamilyGroupBean bean = null;
			for (FamilyGroup familyGroup : familyGroups)
			{
				bean = new FamilyGroupBean();				
				bean.setId(familyGroup.getId());
				bean.setFamilyGroupCode(familyGroup.getFamilygroupcode());
				bean.setFamilyGroupName(familyGroup.getGroupname());
				bean.setZoneCode(familyGroup.getZonecode());
				bean.setCircleCode(familyGroup.getCirclecode());
				bean.setDescription(familyGroup.getDescription());
				bean.setStatus(familyGroup.getStatus());
				bean.setFamilyGroupMembers(familyGroup.getFamilygroupmembers());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}	

	
	//-------------------------------- End of Family Group Master----------------------------------//
	
	//-------------------------------- Start of Bank Master----------------------------------//
	
	@RequestMapping(value = "/saveBankMaster", method = RequestMethod.POST)
	public @ResponseBody
	boolean SavebankMaster(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
	{		
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONObject bankData=(JSONObject) jsonData.get("formData");
			JSONArray branchData=(JSONArray) jsonData.get("gridData");			
			
			BankMasterBean bankMasterBean = new ObjectMapper().readValue(bankData.toString(), BankMasterBean.class);
			Bank bank = prepareModelForBank(bankMasterBean);
			
			entityList.add(bank);
			for(int i=0;i<branchData.size();i++)
			{
				BranchBean branchBean = new ObjectMapper().readValue(branchData.get(i).toString(), BranchBean.class);
				Branch branch = prepareModelForBranch(branchBean,bankMasterBean);
				//added by naidu
				bankService.addBranch(branch);
				//entityList.add(branch);
				//added by naidu
				AccountMaster acc=prepareModelForAccOfEachBranch(branchBean);
				entityList.add(acc);
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);			

			return isInsertSuccess;
		}
		 catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}
	private Bank prepareModelForBank(BankMasterBean bankMasterBean) 
	{
		Bank bank = new Bank();
		String modifyFlag=bankMasterBean.getModifyFlag();
		int bankcode=commonService.getMaxIDforColumn("Bank", "bankcode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			bank.setBankcode(bankMasterBean.getBankCode());
		}
		else
		{
			bank.setBankcode(bankcode);
		}
		bank.setId(bankMasterBean.getId());
		//bank.setBankcode(bankMasterBean.getBankCode());
		bank.setBankname(bankMasterBean.getBankName().toUpperCase());
		return bank;
	}

	
	private Branch prepareModelForBranch(BranchBean branchBean,BankMasterBean bankMasterBean) {

		Branch branch=new Branch();
		branch.setId(branchBean.getId());
		branch.setBankcode(bankMasterBean.getBankCode());
		branch.setBranchcode(branchBean.getBranchCode());
		branch.setBranchname(branchBean.getBranchName().toUpperCase());
		branch.setIfsccode(branchBean.getIfscCode());
		branch.setMicrcode(branchBean.getMicrCode());
		branch.setGlcode(branchBean.getGlCode());
		branch.setDistrictcode(branchBean.getStateCode());
		branch.setStatecode(branchBean.getStateCode());
		branch.setStatus(branchBean.getStatus());
		branch.setAddress(branchBean.getAddress().toUpperCase());
		branch.setCity(branchBean.getCity());	
		branch.setContactnumber(branchBean.getContactNo());
		branch.setContactperson(branchBean.getContactPerson().toUpperCase());
		branch.setEmail(branchBean.getEmail());
		
		return branch;
	}
	//added by naidu
	private AccountMaster prepareModelForAccOfEachBranch(BranchBean branchbean)
	{
		AccountMaster accountMaster = new AccountMaster();
		Integer glcode=bankService.getBranchGlcode(branchbean.getBranchCode());
		accountMaster.setAccountcode(String.valueOf(glcode));
		accountMaster.setAccountname(branchbean.getBranchName().toUpperCase());
		accountMaster.setAccounttype(3);
		int AccGrpCode = companyAdvanceService.GetAccGrpCode("Supplier Banks");
		accountMaster.setAccountgroupcode(AccGrpCode);
		//AccSubGrp for Branch is RYOT BANKS and code is 3
		accountMaster.setAccountsubgroupcode(3);
		return accountMaster;
	}
	
	
	@RequestMapping(value = "/getAllBanks", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllBanks(@RequestBody String jsonData) throws Exception
	{
		BankMasterBean bankMasterBean =null;
		
		org.json.JSONObject response = new org.json.JSONObject();
		
		List bank=bankService.getBank(Integer.parseInt(jsonData));
		//Bank bank=bankService.getBank(Integer.parseInt(jsonData));
		
		if(bank!=null)
			bankMasterBean =prepareBeanForBanks(bank);
		
		
		
	List<Branch> branches =bankService.getBranchByBankCode((Integer)((Map<Object,Object>)bank.get(0)).get("bankcode"));
		//List<Branch> branches =bankService.getBranchByBankCode(bank.getBankcode());
		
		List<BranchBean> brancheBeans =prepareListofBeanForBranches(branches);
		
		
		org.json.JSONObject bankJson=new org.json.JSONObject(bankMasterBean);
		org.json.JSONArray branchArray = new org.json.JSONArray(brancheBeans);
		
		response.put("bank",bankJson);
		response.put("branch",branchArray);		
		
		logger.info("------bank-----"+response.toString());
		
		return response.toString();
	}

	
	
	private BankMasterBean prepareBeanForBanks(List bank) {
		
			BankMasterBean bean = new BankMasterBean();
			
				bean.setId((Integer)((Map<Object,Object>)bank.get(0)).get("id"));
				bean.setBankCode((Integer)((Map<Object,Object>)bank.get(0)).get("bankcode"));
				bean.setBankName((String)((Map<Object,Object>)bank.get(0)).get("bankname"));
				bean.setModifyFlag("Yes");
		return bean;
	}
	/*private BankMasterBean prepareBeanForBanks(Bank bank)
	{
		
		BankMasterBean bean = new BankMasterBean();
		bean.setId(bank.getId());
		bean.setBankCode(bank.getBankcode());
		bean.setBankName(bank.getBankname());
		bean.setModifyFlag("Yes");
		return bean;
	}*/
	
	
	private List<BranchBean> prepareListofBeanForBranches(List<Branch> branches) 
	{
		List<BranchBean> beans = null;
		if (branches != null && !branches.isEmpty()) 
		{
			beans = new ArrayList<BranchBean>();
			BranchBean bean = null;
			for (Branch branch : branches)
			{
				bean = new BranchBean();

				bean.setId(branch.getId());
				bean.setBankCode(branch.getBankcode());
				bean.setBranchCode(branch.getBranchcode());
				bean.setBranchName(branch.getBranchname());
				bean.setCity(branch.getCity());
				bean.setContactNo(branch.getContactnumber());
				bean.setContactPerson(branch.getContactperson());
				bean.setDistrictCode(branch.getDistrictcode());
				bean.setStateCode(branch.getStatecode());
				bean.setStatus(branch.getStatus());
				bean.setEmail(branch.getEmail());
				bean.setIfscCode(branch.getIfsccode());
				bean.setMicrCode(branch.getMicrcode());
				bean.setGlCode(branch.getGlcode());
				bean.setAddress(branch.getAddress());
				
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
	@RequestMapping(value = "/saveSampleCardRules", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveSampleCardRules(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
	{		
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONArray sampleRules=(JSONArray) jsonData.get("samplerules");			
			
			for(int i=0;i<sampleRules.size();i++)
			{
				logger.info("========bankData=========="+sampleRules.get(i));
			}
		
			sampleCardRulesService.deleteAllSampleCardRules();
			
			for(int i=0;i<sampleRules.size();i++)
			{
				SampleCardRulesBean sampleCardRulesBean = new ObjectMapper().readValue(sampleRules.get(i).toString(), SampleCardRulesBean.class);
				SampleCardRules sampleCardRules = prepareModelForSampleCardRules(sampleCardRulesBean);
				entityList.add(sampleCardRules);
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}
	
	private SampleCardRules prepareModelForSampleCardRules(SampleCardRulesBean sampleCardRulesBean) {

		SampleCardRules sampleCardRules=new SampleCardRules();
		
		sampleCardRules.setSizefrom(sampleCardRulesBean.getSizeFrom());
		sampleCardRules.setSizeto(sampleCardRulesBean.getSizeTo());
		sampleCardRules.setNoofsamplecards(sampleCardRulesBean.getNoOfSampleCards());
		sampleCardRules.setId(sampleCardRulesBean.getId());

		
		return sampleCardRules;
	}
	
	@RequestMapping(value = "/getAllSampleCardRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllSampleCardRules(@RequestBody String jsonData) throws Exception 
	{
		List<SampleCardRulesBean> sampleCardRulesBeans = prepareListofBeanForSampleCardRules(sampleCardRulesService.listSampleCardRules());		
	
		org.json.JSONArray sampleCardRulesBeansArray = new org.json.JSONArray(sampleCardRulesBeans);		
		
		logger.info("------sampleCardRulesBeansArray-----"+sampleCardRulesBeansArray.toString());
		
		return sampleCardRulesBeansArray.toString();
	}
	
	
	private List<SampleCardRulesBean> prepareListofBeanForSampleCardRules(List<SampleCardRules> sampleCardRules) {
		List<SampleCardRulesBean> beans = null;
		if (sampleCardRules != null && !sampleCardRules.isEmpty()) {
			beans = new ArrayList<SampleCardRulesBean>();
			SampleCardRulesBean bean = null;
			for (SampleCardRules sampleCardRule : sampleCardRules) {
				bean = new SampleCardRulesBean();
				
				bean.setId(sampleCardRule.getId());
				bean.setSizeFrom(sampleCardRule.getSizefrom());
				bean.setSizeTo(sampleCardRule.getSizeto());
				bean.setNoOfSampleCards(sampleCardRule.getNoofsamplecards());
				
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
	/**
     * Save or Update Permit Rules
     */
	@RequestMapping(value = "/savePermitRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean savePermitRules(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			PermitRulesBean permitRulesBean = new ObjectMapper().readValue(jsonData, PermitRulesBean.class);
			PermitRules permitRules = prepareModelForPermitRules(permitRulesBean);
			
			entityList.add(permitRules);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private PermitRules prepareModelForPermitRules(PermitRulesBean permitRulesBean) {	
		
		PermitRules permitRules = new PermitRules();
		permitRules.setId(permitRulesBean.getId());
		permitRules.setNooftonsperacre(permitRulesBean.getNoOfTonsPerAcre());
		permitRules.setNooftonspereachpermit(permitRulesBean.getNoOfTonsPerEachPermit());
		permitRules.setPermittype(permitRulesBean.getPermitType());
		permitRules.setNoofpermitperacre(permitRulesBean.getNoOfPermitPerAcre());
	
		return permitRules;
	}
	

	/**
     * Get all PermitRules
     */
	@RequestMapping(value = "/getAllPermitRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	PermitRulesBean getAllPermitRules(@RequestBody String jsonData)
			throws Exception {
		List<PermitRulesBean> permitRulesBeans = prepareListofBeanPermitRules(permitRulesService.listPermitRules());
		PermitRulesBean permitRulesBean=permitRulesBeans.get(0);
		return permitRulesBean;
	}

	private List<PermitRulesBean> prepareListofBeanPermitRules(List<PermitRules> permitRules) {
		List<PermitRulesBean> beans = null;
		if (permitRules != null && !permitRules.isEmpty()) {
			beans = new ArrayList<PermitRulesBean>();
			PermitRulesBean bean = null;
			for (PermitRules permitRule : permitRules) {
				bean = new PermitRulesBean();	
				bean.setId(permitRule.getId());
				bean.setNoOfTonsPerAcre(permitRule.getNooftonsperacre());
				bean.setNoOfTonsPerEachPermit(permitRule.getNooftonspereachpermit());
				bean.setPermitType(permitRule.getPermittype());
				bean.setNoOfPermitPerAcre(permitRule.getNoofpermitperacre());
				beans.add(bean);
			}
		}
		return beans;
	}
	//--------------------------------- End Of Permit Rules ----------------------------------//
	
	//--------------------------------- Designation Master ----------------------------------//
	
	@RequestMapping(value = "/saveDesignation", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveDesignation(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveDesignation() from AgricultureHarvestingController========"+ jsonData.toString());
			DesignationBean designationBean = new ObjectMapper().readValue(jsonData, DesignationBean.class);
			
			Designation designation = prepareModelForDesignation(designationBean);
			entityList.add(designation);
			if(designationBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(designationBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(designationBean.getId(),designation,designationBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);

			//if(designationBean.getId()!=null)
				//auditTrailService.updateAuditTrail(designationBean.getId(),designation);			
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private Designation prepareModelForDesignation(DesignationBean designationBean) 
	{
		Designation designation = new Designation();
		
		int designationcode = commonService.getMaxIDforColumn("Designation","designationcode");
		String modifyFlag = designationBean.getModifyFlag();
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			designation.setDesignationcode(designationBean.getDesignationCode());
		}
		else
		{
			designation.setDesignationcode(designationcode);
		}
		
		designation.setId(designationBean.getId());
		designation.setDesignation(designationBean.getDesignation().toUpperCase());
		String description = designationBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		designation.setDescription(description);
		designation.setStatus(designationBean.getStatus());
		
		return designation;
	}
	
	@RequestMapping(value = "/getAllDesignations", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<DesignationBean> getAllDesignations(@RequestBody String jsonData)throws Exception
	{
		logger.info("====getAllDesignations()  from AgricultureHarvestingController========");
		
		List<DesignationBean> designationBeanBeans = prepareListofDesignationBeans(designationService.listDesignations());
		logger.info("====designationBeanBeans.size()========"+designationBeanBeans.size());
		return designationBeanBeans;
	}
	
	private List<DesignationBean> prepareListofDesignationBeans(List<Designation> designations)
	{
		logger.info("====designationBeanBeans.size()========"+designations.size());
		List<DesignationBean> beans = null;
		if (designations != null && !designations.isEmpty())
		{
			beans = new ArrayList<DesignationBean>();
			DesignationBean bean = null;
			for (Designation designation : designations)
			{
				bean = new DesignationBean();	
				
				bean.setId(designation.getId());
				bean.setDesignationCode(designation.getDesignationcode());
				bean.setDesignation(designation.getDesignation());
				bean.setDescription(designation.getDescription());
				bean.setStatus(designation.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}	
	
	//---------------------------------End of Designation Master ----------------------------------//




		//----------------------------------- Ryot Master---------------------------------//
		
		@RequestMapping(value = "/saveImage", method = RequestMethod.POST)
		public @ResponseBody
		String saveImage(@RequestParam("file") MultipartFile file,@RequestParam("path") String path) throws JsonParseException, JsonMappingException, Exception 
		{	
			try
			{
				byte[] bytes = file.getBytes();
				File dir1=new File(servletContext.getRealPath("/")+"angular/"+"img/"+"images/");
				logger.info("=======dir1-------------"+dir1);
				logger.info("=======path-------------"+path);
				if (!dir1.exists())
					dir1.mkdirs();
				
				String ext=Constants.getFileExtension(file.getOriginalFilename());
				String FileName=path+"."+ext.toLowerCase();
				
				String srcPath="img/"+"images/"+FileName;
				File serverFile1 = new File(dir1.getAbsolutePath()+ File.separator + FileName);
				
				BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
				stream1.write(bytes);
				stream1.close();
				
				logger.info("Server File Location="+ serverFile1.getAbsolutePath());
				logger.info("Relative Path----="+ srcPath);
				
				return srcPath;					
			}
			catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return null;
	        }
		}

		@RequestMapping(value = "/saveRyotMaster", method = RequestMethod.POST)
		public @ResponseBody	boolean saveRyotMaster(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception
		{	
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			try
			{				
				JSONObject ryotData=(JSONObject) jsonData.get("formData");
				JSONArray branchData=(JSONArray) jsonData.get("gridData");			
				
				logger.info("========ryotData=========="+ryotData);
				for(int i=0;i<branchData.size();i++)
				{
					logger.info("========branchData=========="+branchData.get(i));
				}

				RyotMasterBean ryotMasterBean = new ObjectMapper().readValue(ryotData.toString(), RyotMasterBean.class);
	
				Ryot ryot = prepareModelForRyot(ryotMasterBean);
				entityList.add(ryot);
				if(ryotMasterBean.getId()!=null)
				{
					boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(ryotMasterBean.getScreenName(),"ok");
					if(isAuditTrailRequired)
						auditTrailService.updateAuditTrail(ryotMasterBean.getId(),ryot,ryotMasterBean.getScreenName());		
				}
				ryotService.deleteRyotBankDetails(ryot);
				for(int i=0;i<branchData.size();i++)
				{
					RyotBankDetailsBean ryotBankDetailsBean = new ObjectMapper().readValue(branchData.get(i).toString(), RyotBankDetailsBean.class);
					RyotBankDetails ryotBankDetails = prepareModelForRyotBranch(ryotBankDetailsBean,ryotMasterBean);
					ryotBankDetails.setRyotcode(ryot.getRyotcode());
					entityList.add(ryotBankDetails);
				}
			
				AccountMaster accountMaster = prepareModelForAccountMaster(ryotMasterBean);
				entityList.add(accountMaster);
				
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
				
				return isInsertSuccess;
			}
			catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }
		}		
		
		private AccountMaster prepareModelForAccountMaster(RyotMasterBean ryotMasterBean)
		{
			AccountMaster accountMaster = new AccountMaster();
			
			accountMaster.setAccountcode(ryotMasterBean.getRyotCode());
			//Modified by DMurty on 26-07-2016
			//accountMaster.setAccountname("Sundry Creditors");
			accountMaster.setAccountname(ryotMasterBean.getRyotName().toUpperCase());
			int AccGrpCode = companyAdvanceService.GetAccGrpCode("Sundry Creditors");
			int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCode(AccGrpCode);

			accountMaster.setAccounttype(0);
			accountMaster.setAccountgroupcode(AccGrpCode);
			accountMaster.setAccountsubgroupcode(AccSubGrpCode); 
			
			return accountMaster;
		}
		
		private Ryot prepareModelForRyot(RyotMasterBean ryotMasterBean)
		{

			Ryot ryot = new Ryot();
			//Modified by DMurty on 30-06-2016
			String modifyFlag = ryotMasterBean.getModifyFlag();
			String villageCode = ryotMasterBean.getVillageCode();
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				ryot.setRyotcode(ryotMasterBean.getRyotCode());
				ryot.setRyotcodesequence(ryotMasterBean.getRyotCodeSequence());
			}
			else
			{
				String ryotCode = commonService.GetstrMaxRyotCodeByVillage(villageCode);
				String ryotSeqNo = ryotCode.substring(4,8);
				int nryotSeqNo = Integer.parseInt(ryotSeqNo);
				ryot.setRyotcode(ryotCode);
				ryot.setRyotcodesequence(nryotSeqNo);
			}
		
			ryot.setId(ryotMasterBean.getId());
			ryot.setAadhaarNumber(ryotMasterBean.getAadhaarNumber());
			ryot.setAadhaarimagepath(ryotMasterBean.getAadhaarImagePath());
			ryot.setCircleCode(ryotMasterBean.getCircleCode());
			ryot.setMandalcode(ryotMasterBean.getMandalCode());
			ryot.setVillagecode(ryotMasterBean.getVillageCode());
			ryot.setCity(ryotMasterBean.getCity().toUpperCase());
			ryot.setAddress(ryotMasterBean.getAddress().toUpperCase());
			ryot.setRelation(ryotMasterBean.getRelation().toUpperCase());
			ryot.setRelativename(ryotMasterBean.getRelativeName().toUpperCase());
			ryot.setRyotphotopath(ryotMasterBean.getRyotPhotoPath());
			ryot.setSalutation(ryotMasterBean.getSalutation());
			ryot.setSuretyryotcode(ryotMasterBean.getSuretyRyotCode());
			ryot.setStatus(ryotMasterBean.getStatus());
			ryot.setMobilenumber(ryotMasterBean.getMobileNumber());
			
			
			ahFuctionalService.updateRyotNameInAgreement(ryotMasterBean.getRyotCode(),ryotMasterBean.getRyotName().toUpperCase(),ryotMasterBean.getRelativeName().toUpperCase());
			ryot.setRyotname(ryotMasterBean.getRyotName().toUpperCase());
			
			String panNo = ryotMasterBean.getPanNumber();
			if("".equals(panNo) || "null".equals(panNo) || panNo == null)
			{
				panNo = "NA";
				ryot.setPannumber(panNo);	
			}
			else
			{
				ryot.setPannumber(ryotMasterBean.getPanNumber());	
			}
			//Added by DMurty on 02-09-2016
			ryot.setRyottype(ryotMasterBean.getRyotType());
			//Added by DMurty on 30-12-2016
			ryot.setIsweighmentcompleted((byte) 0);
			ryot.setRyotcategory((byte) 0);
			
			return ryot;
		}
		
		
		private RyotBankDetails prepareModelForRyotBranch(RyotBankDetailsBean ryotBankDetailsBean,RyotMasterBean ryotMasterBean) {

			RyotBankDetails ryotBankDetails=new RyotBankDetails();
			ryotBankDetails.setId(ryotBankDetailsBean.getId());
			ryotBankDetails.setAccountnumber(ryotBankDetailsBean.getAccountNumber());
			ryotBankDetails.setAccounttype(ryotBankDetailsBean.getAccountType());
			ryotBankDetails.setIfsccode(ryotBankDetailsBean.getIfscCode());
			ryotBankDetails.setRyotbankbranchcode(ryotBankDetailsBean.getRyotBankBranchCode());
			//ryotBankDetails.setRyotcode(ryotMasterBean.getRyotCode());			
			ryotBankDetails.setSeason(ryotBankDetailsBean.getSeason());
			return ryotBankDetails;
		}
		
		
		@RequestMapping(value = "/getAllRyots", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllRyots(@RequestBody String jsonData) throws Exception {
			RyotMasterBean ryotMasterBean =null;
			
			
			org.json.JSONObject response = new org.json.JSONObject();
			String ryotCode = jsonData.toString();
			//ryotCode.replace(oldChar, newChar);
			ryotCode=ryotCode.replaceAll("\"", "");
			Ryot ryot=ryotService.getRyot(ryotCode);
			
			if(ryot!=null)
				ryotMasterBean =prepareBeanForRyots(ryot);
			
			List<RyotBankDetails> ryotBankDetails =ryotService.getBranchByRyotCode(ryot.getRyotcode());
			
			List<RyotBankDetailsBean> ryotBankDetailsBeans =prepareListofBeanForRyotBanks(ryotBankDetails);
			
			
			org.json.JSONObject ryotJson=new org.json.JSONObject(ryotMasterBean);
			org.json.JSONArray bankDetailsArray = new org.json.JSONArray(ryotBankDetailsBeans);
			
			response.put("ryot",ryotJson);
			response.put("branch",bankDetailsArray);		
			
			logger.info("------Ryot Master-----"+response.toString());
			
			return response.toString();
		}

		private RyotMasterBean prepareBeanForRyots(Ryot ryot)
		{
			RyotMasterBean bean = new RyotMasterBean();			
				
			bean.setId(ryot.getId());
			bean.setVillageCode(ryot.getVillagecode());					
			bean.setMandalCode(ryot.getMandalcode());
			bean.setAadhaarImagePath(ryot.getAadhaarimagepath());
			bean.setAadhaarNumber(ryot.getAadhaarNumber());
			bean.setAddress(ryot.getAddress());
			bean.setPanNumber(ryot.getPannumber());
			bean.setCircleCode(ryot.getCircleCode());
			bean.setRelation(ryot.getRelation());
			bean.setRelativeName(ryot.getRelativename());
			bean.setRyotPhotoPath(ryot.getRyotphotopath());
			bean.setCity(ryot.getCity());
			bean.setRyotCode(ryot.getRyotcode());
			bean.setMobileNumber(ryot.getMobilenumber());
			bean.setSuretyRyotCode(ryot.getSuretyryotcode());
			bean.setSalutation(ryot.getSalutation());
			bean.setRyotCodeSequence(ryot.getRyotcodesequence());
			bean.setRyotName(ryot.getRyotname());
			bean.setStatus(ryot.getStatus());
			bean.setModifyFlag("Yes");
			//Added by DMurty on 18-10-2016
			bean.setRyotType(ryot.getRyottype());
			
			return bean;
		}
	
		private List<RyotBankDetailsBean> prepareListofBeanForRyotBanks(List<RyotBankDetails> ryotBankDetails)
		{
			List<RyotBankDetailsBean> beans = null;
			if (ryotBankDetails != null && !ryotBankDetails.isEmpty()) 
			{
				beans = new ArrayList<RyotBankDetailsBean>();
				RyotBankDetailsBean bean = null;
				for (RyotBankDetails ryotBankDetail : ryotBankDetails) 
				{
					bean = new RyotBankDetailsBean();

					bean.setId(ryotBankDetail.getId());
					bean.setAccountNumber(ryotBankDetail.getAccountnumber());
					bean.setAccountType(ryotBankDetail.getAccounttype());
					bean.setIfscCode(ryotBankDetail.getIfsccode());
					bean.setRyotBankBranchCode(ryotBankDetail.getRyotbankbranchcode());
					bean.setRyotCode(ryotBankDetail.getRyotcode());
					bean.setSeason(ryotBankDetail.getSeason());
					beans.add(bean);
				}
			}
			return beans;
		}
		
		//-----------------------------------End Of Ryot Master---------------------------------//
		
		//--------------------------------- Seedling Tray Master ----------------------------------//
		
		@RequestMapping(value = "/saveSeedlingTray", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		boolean saveSeedlingTray(@RequestBody JSONObject jsonData) throws Exception
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			JSONObject seedlingData=(JSONObject) jsonData.get("formData");
			JSONArray chargeData=(JSONArray) jsonData.get("gridData");	
			try 
			{
				SeedlingTrayBean seedlingTrayBean = new ObjectMapper().readValue(seedlingData.toString(), SeedlingTrayBean.class);
				SeedlingTray seedlingTray = prepareModelForSeedlingTray(seedlingTrayBean);
			
				for(int i=0;i<chargeData.size();i++)
				{
					SeedlingTrayChargeByDistanceBean seedlingTrayChargeByDistanceBean = new ObjectMapper().readValue(chargeData.get(i).toString(), SeedlingTrayChargeByDistanceBean.class);
					if(seedlingTrayBean.getId()!=null)
					{
						boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(seedlingTrayBean.getScreenName(),"ok");
						if(isAuditTrailRequired)
							auditTrailService.updateAuditTrail(seedlingTrayBean.getId(),seedlingTray,seedlingTrayBean.getScreenName());		
					}
				}
				
				seedlingTrayService.deleteAllSeedlingTrayChargesByDistance(seedlingTrayBean.getTrayCode());
				for(int i=0;i<chargeData.size();i++)
				{
					SeedlingTrayChargeByDistanceBean seedlingTrayChargeByDistanceBean = new ObjectMapper().readValue(chargeData.get(i).toString(), SeedlingTrayChargeByDistanceBean.class);
					SeedlingTrayChargeByDistance seedlingTrayChargeByDistance=prepareModelForSeedlingTrayChargeByDistance(seedlingTrayBean,seedlingTrayChargeByDistanceBean);
					entityList.add(seedlingTrayChargeByDistance);
				}
				
				entityList.add(seedlingTray);
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
				
				return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}
		
		private SeedlingTray prepareModelForSeedlingTray(SeedlingTrayBean seedlingTrayBean) 
		{
			SeedlingTray seedlingTray = new SeedlingTray();
			
			String modifyFlag=seedlingTrayBean.getModifyFlag();
			 int traycode=commonService.getMaxIDforColumn("SeedlingTray", "traycode");
			 
			 if("Yes".equalsIgnoreCase(modifyFlag))
			 {
				 seedlingTray.setTraycode(seedlingTrayBean.getTrayCode());
			 }
			 else
			 {
				 seedlingTray.setTraycode(traycode);
			 }
			 seedlingTray.setId(seedlingTrayBean.getId());	
			String description = seedlingTrayBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
			seedlingTray.setDescription(description);
			seedlingTray.setStatus(seedlingTrayBean.getStatus());			
			seedlingTray.setLifeoftray(seedlingTrayBean.getLifeOfTray());
			seedlingTray.setNoofseedspertray(seedlingTrayBean.getNoOfSeedlingPerTray());
			seedlingTray.setTray(seedlingTrayBean.getTrayName().toUpperCase());
			//seedlingTray.setTraycode(seedlingTrayBean.getTrayCode());
			seedlingTray.setTraytype(seedlingTrayBean.getTrayType());
			
			
			return seedlingTray;
		}
		
		private SeedlingTrayChargeByDistance prepareModelForSeedlingTrayChargeByDistance(SeedlingTrayBean seedlingTrayBean,SeedlingTrayChargeByDistanceBean seedlingTrayChargeByDistanceBean) 
		{
			String modifyFlag=seedlingTrayBean.getModifyFlag();
			int traycode=commonService.getMaxIDforColumn("SeedlingTray", "traycode");
			
			SeedlingTrayChargeByDistance seedlingTrayChargeByDistance = new SeedlingTrayChargeByDistance();			
			seedlingTrayChargeByDistance.setId(seedlingTrayChargeByDistanceBean.getId());	
			seedlingTrayChargeByDistance.setDistancefrom(seedlingTrayChargeByDistanceBean.getDistanceFrom());
			seedlingTrayChargeByDistance.setDistanceto(seedlingTrayChargeByDistanceBean.getDistanceTo());
			seedlingTrayChargeByDistance.setTraycharge(seedlingTrayChargeByDistanceBean.getTrayCharge());
			

			 
			 if("Yes".equalsIgnoreCase(modifyFlag))
			 {
				 seedlingTrayChargeByDistance.setTraycode(seedlingTrayBean.getTrayCode());
			 }
			 else
			 {
				 seedlingTrayChargeByDistance.setTraycode(traycode);
			 }
			
			return seedlingTrayChargeByDistance;
		}
		
		@RequestMapping(value = "/getAllSeedlingTrays", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllSeedlingTrays(@RequestBody String jsonData)throws Exception
		{
			
			SeedlingTrayBean seedlingTrayBean =null;
			org.json.JSONObject response = new org.json.JSONObject();
			SeedlingTray seedlingTray=seedlingTrayService.getSeedlingTray(Integer.parseInt(jsonData));
			if(seedlingTray!=null)
				seedlingTrayBean =prepareBeanForSeedlingTray(seedlingTray);
			
			
			
			List<SeedlingTrayChargeByDistance> seedlingTrayChargeByDistance =seedlingTrayService.getSeedlingTrayChargeByDistance(seedlingTray.getTraycode());
			
			List<SeedlingTrayChargeByDistanceBean> seedlingTrayChargeByDistanceBeans =prepareListofSeedlingTrayChargeBeans(seedlingTrayChargeByDistance);
			
			//List<SeedlingTrayBean> seedlingTrayBeans = prepareListofSeedlingTrayBeans(seedlingTrayService.listSeedlingTray());
			
			
			org.json.JSONObject trayJson=new org.json.JSONObject(seedlingTrayBean);
			org.json.JSONArray chargeArray = new org.json.JSONArray(seedlingTrayChargeByDistanceBeans);
			
			response.put("tray",trayJson);
			response.put("charge",chargeArray);		
			
			logger.info("------tray-----"+response.toString());
			
			return response.toString();
		}
		
		
		private SeedlingTrayBean prepareBeanForSeedlingTray(SeedlingTray seedlingTray) {
			
			SeedlingTrayBean bean = new SeedlingTrayBean();
			
			bean.setId(seedlingTray.getId());
			
			bean.setLifeOfTray(seedlingTray.getLifeoftray());
			bean.setNoOfSeedlingPerTray(seedlingTray.getNoofseedspertray());
			bean.setTrayCode(seedlingTray.getTraycode());
			bean.setTrayName(seedlingTray.getTray());
			bean.setTrayType(seedlingTray.getTraytype());		
		
			bean.setDescription(seedlingTray.getDescription());
			bean.setStatus(seedlingTray.getStatus());
				
		return bean;
	}
		
		
		private List<SeedlingTrayChargeByDistanceBean> prepareListofSeedlingTrayChargeBeans(List<SeedlingTrayChargeByDistance> seedlingTrays)
		{
			List<SeedlingTrayChargeByDistanceBean> beans = null;
			if (seedlingTrays != null && !seedlingTrays.isEmpty())
			{
				beans = new ArrayList<SeedlingTrayChargeByDistanceBean>();
				SeedlingTrayChargeByDistanceBean bean = null;
				for (SeedlingTrayChargeByDistance seedlingTray : seedlingTrays)
				{
					bean = new SeedlingTrayChargeByDistanceBean();	
					
					bean.setId(seedlingTray.getId());
					bean.setTrayCode(seedlingTray.getTraycode());
					bean.setDistanceFrom(seedlingTray.getDistancefrom());
					bean.setDistanceTo(seedlingTray.getDistanceto());
					bean.setTrayCharge(seedlingTray.getTraycharge());
	
					beans.add(bean);
				}
			}
			return beans;
		}	
		
		//--------------------------------- End Of Seedling Tray Master ----------------------------------//

		//-------------------------------------- Shift Master -----------------------------------------//
		@RequestMapping(value = "/SaveShiftMaster", method = RequestMethod.POST)
		public @ResponseBody
		boolean SaveShiftMaster(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			try
			{
				JSONArray shiftData=(JSONArray) jsonData.get("gridData");
				JSONObject totalData=(JSONObject) jsonData.get("formData");
				String totalHours=totalData.getString("totalHrs");
				
				shiftService.deleteShift();

				for(int i=0;i<shiftData.size();i++)
				{
					ShiftMasterBean shiftMasterBean = new ObjectMapper().readValue(shiftData.get(i).toString(), ShiftMasterBean.class);
					Shift shift = prepareModelForShift(shiftMasterBean,totalHours);
					entityList.add(shift);
				}
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
				
				return isInsertSuccess;
			}
			 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }
			
		}
		private Shift prepareModelForShift(ShiftMasterBean shiftMasterBean,String totalHours)
		{
			//String myTime = "10:00:00"; 
			//Time startingTime = new Time (myTime);
			shiftMasterBean.setTotalHrs(totalHours);
			Shift shift=new Shift();
			
			shift.setId(shiftMasterBean.getId());
			shift.setShiftid(shiftMasterBean.getShiftId());
			shift.setShiftname(shiftMasterBean.getShiftName());
			shift.setFromtime(java.sql.Time.valueOf(shiftMasterBean.getFromTime()));
			//shift.setTotime(shiftMasterBean.getToTime());
			shift.setTotime(java.sql.Time.valueOf(shiftMasterBean.getToTime()));
			
			shift.setShifthrs(java.sql.Time.valueOf(shiftMasterBean.getShiftHrs()));
			//shift.setTotalhrs(java.sql.Time.valueOf(shiftMasterBean.getTotalHrs()));
			shift.setTotalhrs(shiftMasterBean.getTotalHrs());
			
			return shift;
		}
		
		@RequestMapping(value = "/getAllShifts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllShifts(@RequestBody String jsonData) throws Exception 
		{
			org.json.JSONObject response = new org.json.JSONObject();
			
			List<Shift> shifts =shiftService.listShifts();
			List<ShiftMasterBean> ShiftBeans =prepareListofShiftBeans(shifts);

			org.json.JSONArray shiftArray = new org.json.JSONArray(ShiftBeans);
			
			response.put("Shifts",shiftArray);
			
			return response.toString();
		}

		private List<ShiftMasterBean> prepareListofShiftBeans(List<Shift> Shifts)
		{
			
			List<ShiftMasterBean> beans = null;
			if (Shifts != null && !Shifts.isEmpty())
			{
				beans = new ArrayList<ShiftMasterBean>();
				ShiftMasterBean bean = null;
				for (Shift shift : Shifts)
				{
					bean = new ShiftMasterBean();	
			
					bean.setId(shift.getId());
					bean.setShiftId(shift.getShiftid());
					bean.setShiftName(shift.getShiftname());
					bean.setFromTime(shift.getFromtime().toString());
					bean.setToTime(shift.getTotime().toString());
					bean.setShiftHrs(shift.getShifthrs().toString());
					bean.setTotalHrs(shift.getTotalhrs());
					
					beans.add(bean);
				}
			}
			return beans;
		}	
		//---------------------------------End of Shift Master ----------------------------------//

		//---------------------------------Company Advance Master ----------------------------------//
		@RequestMapping(value = "/saveCompanyMaster", method = RequestMethod.POST)
		public @ResponseBody
		boolean saveCompanyMaster(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException
		{		
			boolean isInsertSuccess = false;
			try
			{
				List advancesMasterList = new ArrayList();
				
				JSONObject advanceData=(JSONObject) jsonData.get("formData");
				JSONArray amountData=(JSONArray) jsonData.get("gridData");			
				JSONArray SeedlingCostData=(JSONArray) jsonData.get("gridData1");
				
				logger.info("========advanceData=========="+advanceData);
				logger.info("========amountData=========="+amountData);

				CompanyAdvanceBean companyAdvanceBean = new ObjectMapper().readValue(advanceData.toString(), CompanyAdvanceBean.class);
				CompanyAdvance companyAdvance = prepareModelForCompanyAdvance(companyAdvanceBean);
				
				//onloadIsKindorAmt
				int onloadStatus = companyAdvanceBean.getOnloadIsKindorAmt();
				int isSeedling = companyAdvanceBean.getIsSeedlingAdvance();
				
				if(onloadStatus == 0 && isSeedling == 1)
				{
					companyAdvanceService.deleteSeedlingCost(companyAdvance);
				}
				
				if(companyAdvanceBean.getId()!=null)
				{
					boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(companyAdvanceBean.getScreenName(),"ok");
					if(isAuditTrailRequired)
						auditTrailService.updateAuditTrail(companyAdvanceBean.getId(),companyAdvance,companyAdvanceBean.getScreenName());		
				}
				
				companyAdvanceService.deleteIntrestRate(companyAdvance);
				
				for(int i=0;i<amountData.size();i++)
				{
					AdvanceInterestRatesBean advanceInterestRatesBean = new ObjectMapper().readValue(amountData.get(i).toString(), AdvanceInterestRatesBean.class);
					AdvanceInterestRates advanceInterestRates = prepareModelForIntrestRates(advanceInterestRatesBean,companyAdvanceBean);
					advancesMasterList.add(advanceInterestRates);
				}
				
				//Added by DMurty on 04-07-2016
				int iskind = companyAdvance.getIsseedlingadvance();
				if(iskind==0)
				{
					companyAdvanceService.deleteSeedlingCost(companyAdvance);
					for(int i=0;i<SeedlingCostData.size();i++)
					{
						SeedAdvanceCostBean seedAdvanceCostBean = new ObjectMapper().readValue(SeedlingCostData.get(i).toString(), SeedAdvanceCostBean.class);
						SeedAdvanceCost seedAdvanceCost = prepareModelForSeedAdvanceCost(seedAdvanceCostBean,companyAdvanceBean);
						advancesMasterList.add(seedAdvanceCost);
					}
				}
				advancesMasterList.add(companyAdvance);
				
				//Insert Record in Account Table
				AccountMaster accountMaster = prepareModelForAccountMasterinCompanyAdvMaster(companyAdvanceBean);
				advancesMasterList.add(accountMaster);

				AdvanceOrder advanceOrder = prepareModelforAdvanceOrder(companyAdvanceBean);
				advancesMasterList.add(advanceOrder);

				AccountType accountType = prepareModelforAccountType();
				advancesMasterList.add(accountType);

				isInsertSuccess = commonService.saveMultipleEntities(advancesMasterList);
				return isInsertSuccess;
			}
			 catch (Exception e)
	        {
	            logger.info(e.getCause(), e);
	            return isInsertSuccess;
	        }
		}
		
		private SeedAdvanceCost prepareModelForSeedAdvanceCost(SeedAdvanceCostBean seedAdvanceCostBean,CompanyAdvanceBean companyAdvanceBean)
		{
			SeedAdvanceCost seedAdvanceCost=new SeedAdvanceCost();
			
			seedAdvanceCost.setAdvancecode(companyAdvanceBean.getAdvanceCode());
			seedAdvanceCost.setCostofeachunit(seedAdvanceCostBean.getCostOfEachUnit());
			seedAdvanceCost.setFromdistance(seedAdvanceCostBean.getFromDistance());
			seedAdvanceCost.setTodistance(seedAdvanceCostBean.getToDistance());
			seedAdvanceCost.setId(seedAdvanceCostBean.getId());
			return seedAdvanceCost;
		}
		
		private AccountType prepareModelforAccountType() 
		{
			AccountType accountType = new AccountType();
			
			accountType.setAccounttype("Asset");
			accountType.setAccounttypecode(3);
			accountType.setDescription("Towards Company Advance");
			
			return accountType;
		}
		
		private AdvanceOrder prepareModelforAdvanceOrder(CompanyAdvanceBean companyAdvanceBean) 
		{
			 Date date = new Date();
			 
			AdvanceOrder advanceOrder = new AdvanceOrder();
			
			String modifyFlag = companyAdvanceBean.getModifyFlag();
			
			int advancecode = commonService.getMaxIDforColumn("CompanyAdvance","advancecode");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				advanceOrder.setAdvanceCode(companyAdvanceBean.getAdvanceCode());
			}
			else
			{
				advanceOrder.setAdvanceCode(advancecode);
			}
			
			advanceOrder.setId(companyAdvanceBean.getId());	
			//advanceOrder.setAdvanceCode(companyAdvanceBean.getAdvanceCode());
			advanceOrder.setSeason("15-16");
			advanceOrder.setEffectiveDate(date);
			advanceOrder.setAdvanceOrder(0);
			
			
			return advanceOrder;
		}

		private AccountMaster prepareModelForAccountMasterinCompanyAdvMaster(CompanyAdvanceBean companyAdvanceBean) 
		{
			AccountMaster accountMaster = new AccountMaster();
					
			accountMaster.setAccountcode(companyAdvanceBean.getGlCode());
			accountMaster.setAccountname(companyAdvanceBean.getAdvance().toUpperCase());
			accountMaster.setAccounttype(3);
			int AccGrpCode = companyAdvanceService.GetAccGrpCode("current assets");
			accountMaster.setAccountgroupcode(AccGrpCode);
			//Here Acc Sub Group code for company advance is 3.Discussed with sir on 13-04-216
			accountMaster.setAccountsubgroupcode(1);
			
			return accountMaster;
		}

		private CompanyAdvance prepareModelForCompanyAdvance(CompanyAdvanceBean companyAdvanceBean) 
		{
			CompanyAdvance companyAdvance = new CompanyAdvance();
			
			String modifyFlag = companyAdvanceBean.getModifyFlag();
			
			int advancecode = commonService.getMaxIDforColumn("CompanyAdvance","advancecode");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				companyAdvance.setAdvancecode(companyAdvanceBean.getAdvanceCode());
			}
			else
			{
				companyAdvance.setAdvancecode(advancecode);
			}
			companyAdvance.setId(companyAdvanceBean.getId());
			companyAdvance.setAdvance(companyAdvanceBean.getAdvance().toUpperCase());
			companyAdvance.setMaximumamount(companyAdvanceBean.getMaximumAmount());
			companyAdvance.setGlCode(companyAdvanceBean.getGlCode());
			companyAdvance.setAdvancemode(companyAdvanceBean.getAdvanceMode());
			companyAdvance.setSubsidypart(companyAdvanceBean.getSubsidyPart());
			if(companyAdvanceBean.getIsAmount()==0)
			{
				companyAdvance.setAdvancepercent(companyAdvanceBean.getPercent());
				companyAdvance.setAmount(0.0);
			}
			else
			{
				companyAdvance.setAmount(companyAdvanceBean.getPercent());
				companyAdvance.setAdvancepercent(0.0);
			}
			
			companyAdvance.setAdvancefor(companyAdvanceBean.getAdvanceFor());
			companyAdvance.setIsseedlingadvance(companyAdvanceBean.getIsSeedlingAdvance());
			companyAdvance.setSeedlingcost(companyAdvanceBean.getSeedlingCost());
			companyAdvance.setStatus(companyAdvanceBean.getStatus());
			companyAdvance.setInterestapplicable(companyAdvanceBean.getInterestApplicable());
			String description = companyAdvanceBean.getDescription();
			if ("".equals(description) || "null".equals(description) || (description == null)) 
			{
				description = "NA";
			}
			else
			{
				description = description.toUpperCase();
			}
			companyAdvance.setDescription(description);
			companyAdvance.setIsamount(companyAdvanceBean.getIsAmount());
			
			companyAdvance.setCalculationdate(DateUtils.getSqlDateFromString(companyAdvanceBean.getCalculationDate(),Constants.GenericDateFormat.DATE_FORMAT));

			
			return companyAdvance;
		}
		
		private AdvanceInterestRates prepareModelForIntrestRates(AdvanceInterestRatesBean advanceInterestRatesBean,CompanyAdvanceBean companyAdvanceBean)
		{

			AdvanceInterestRates advanceInterestRates=new AdvanceInterestRates();
			String modifyFlag = companyAdvanceBean.getModifyFlag();

			int advancecode = commonService.getMaxIDforColumn("CompanyAdvance","advancecode");
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				advanceInterestRates.setAdvancecode(companyAdvanceBean.getAdvanceCode());
			}
			else
			{
				advanceInterestRates.setAdvancecode(advancecode);
			}
			//advanceInterestRates.setSlno(advanceInterestRatesBean.getId());
			advanceInterestRates.setId(advanceInterestRatesBean.getId());
			advanceInterestRates.setFromamount(advanceInterestRatesBean.getFromAmount());
			advanceInterestRates.setToamount(advanceInterestRatesBean.getToAmount());
			advanceInterestRates.setInterestrate(advanceInterestRatesBean.getInterestRate());
			
			return advanceInterestRates;
		}
		
		@RequestMapping(value = "/getAllCompanyAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllCompanyAdvances(@RequestBody String jsonData) throws Exception
		{
			CompanyAdvanceBean companyAdvanceBean =null;
			
			org.json.JSONObject response = new org.json.JSONObject();
			
			CompanyAdvance companyAdvance=companyAdvanceService.getCompanyAdvance(Integer.parseInt(jsonData));
		
			if(companyAdvance!=null)
				companyAdvanceBean =prepareBeanForCompanyAdvance(companyAdvance);
				
			List<AdvanceInterestRates> advanceInterestRatess =companyAdvanceService.getIntrestRatesByAdvanceCode(companyAdvance.getAdvancecode());
			List<AdvanceInterestRatesBean> AdvanceInterestRatesBeans =prepareListofBeanForIntrestRates(advanceInterestRatess);
			List<SeedAdvanceCost> seedAdvanceCosts = companyAdvanceService.getSeedlingCost(companyAdvance.getAdvancecode());
			
			int isSeedling = companyAdvanceBean.getIsSeedlingAdvance();
			
			
			org.json.JSONObject advanceJson=new org.json.JSONObject(companyAdvanceBean);
			org.json.JSONArray rateArray = new org.json.JSONArray(AdvanceInterestRatesBeans);

			
			response.put("companyadvance",advanceJson);
			response.put("rates",rateArray);		
			if(isSeedling == 0)
			{
				List<SeedAdvanceCostBean> seedAdvanceCostBeans =prepareListofBeanForSeedlingAdvanceCost(seedAdvanceCosts);
				org.json.JSONArray costArray = new org.json.JSONArray(seedAdvanceCostBeans);
				response.put("cost",costArray);		
			}
			
			logger.info("------companyadvance-----"+response.toString());
			
			return response.toString();
		}
		
		private List<SeedAdvanceCostBean> prepareListofBeanForSeedlingAdvanceCost(List<SeedAdvanceCost> seedAdvanceCosts)
		{			
			List<SeedAdvanceCostBean> beans = null;
			if (seedAdvanceCosts != null && !seedAdvanceCosts.isEmpty())
			{
				beans = new ArrayList<SeedAdvanceCostBean>();
				SeedAdvanceCostBean bean = null;
				for (SeedAdvanceCost seedAdvanceCost : seedAdvanceCosts)
				{
					bean = new SeedAdvanceCostBean();
					
					bean.setId(seedAdvanceCost.getId());
					bean.setCostOfEachUnit(seedAdvanceCost.getCostofeachunit());
					bean.setFromDistance(seedAdvanceCost.getFromdistance());
					bean.setToDistance(seedAdvanceCost.getTodistance());
							
					beans.add(bean);
				}
			}
			return beans;
		}
		
		private CompanyAdvanceBean prepareBeanForCompanyAdvance(CompanyAdvance companyAdvance) 
		{
			
			CompanyAdvanceBean bean = new CompanyAdvanceBean();
			
			bean.setId(companyAdvance.getId());
			bean.setAdvanceCode(companyAdvance.getAdvancecode());
			bean.setAdvance(companyAdvance.getAdvance());
			bean.setMaximumAmount(companyAdvance.getMaximumamount());
			bean.setGlCode(companyAdvance.getGlCode());
			bean.setAdvanceMode(companyAdvance.getAdvancemode());
			bean.setSubsidyPart(companyAdvance.getSubsidypart());
			
			bean.setAdvanceFor(companyAdvance.getAdvancefor());
			bean.setIsSeedlingAdvance(companyAdvance.getIsseedlingadvance());
			bean.setSeedlingCost(companyAdvance.getSeedlingcost());
			bean.setStatus(companyAdvance.getStatus());
			bean.setInterestApplicable(companyAdvance.getInterestapplicable());
			bean.setDescription(companyAdvance.getDescription());
			if(companyAdvance.getIsamount()==0)
			{
				bean.setPercent(companyAdvance.getAdvancepercent());
			}
			else
			{
				bean.setPercent(companyAdvance.getAmount());
			}
			bean.setIsAmount(companyAdvance.getIsamount());
			//bean.setAmount(companyAdvance.getAmount());	
			if(companyAdvance.getCalculationdate()!=null)
				bean.setCalculationDate(DateUtils.formatDate(companyAdvance.getCalculationdate(),Constants.GenericDateFormat.DATE_FORMAT));
			bean.setModifyFlag("Yes");
			
		return bean;
	}
		private List<AdvanceInterestRatesBean> prepareListofBeanForIntrestRates(List<AdvanceInterestRates> advanceInterestRatess)
		{
			List<AdvanceInterestRatesBean> beans = null;
			if (advanceInterestRatess != null && !advanceInterestRatess.isEmpty())
			{
				beans = new ArrayList<AdvanceInterestRatesBean>();
				AdvanceInterestRatesBean bean = null;
				for (AdvanceInterestRates advanceInterestRates : advanceInterestRatess)
				{
					bean = new AdvanceInterestRatesBean();
					
					bean.setId(advanceInterestRates.getId());
					bean.setAdvanceCode(advanceInterestRates.getAdvancecode());
					bean.setFromAmount(advanceInterestRates.getFromamount());
					bean.setToAmount(advanceInterestRates.getToamount());
					bean.setInterestRate(advanceInterestRates.getInterestrate());
								
					beans.add(bean);
				}
			}
			return beans;
		}
		//--------------------------- End of Company Advances Master -------------------------------------//

		//--------------------------- Order Advances Master -------------------------------------//
		@RequestMapping(value = "/getCompanyAdvancesForOrder", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		JSONArray getCompanyAdvancesForOrder(@RequestBody String jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			List AdvanceListList=companyAdvanceService.getCompanyAdvancesForOrder();
			try
			{
				if (AdvanceListList != null && AdvanceListList.size() > 0)
				{
					for (Iterator it = AdvanceListList.iterator(); it.hasNext();)
					{
						jsonObj = new JSONObject();
						Object[] myResult = (Object[]) it.next();
						Integer AdvCode = (Integer) myResult[0];
						String AdvanceName = (String) myResult[1];
						int id = (Integer) myResult[2];

						jsonObj.put("advanceCode", AdvCode);
						jsonObj.put("advanceName", AdvanceName);
						jsonObj.put("id", id);

						jArray.add(jsonObj);
					}
				}
				return jArray;
			}
			 catch (Exception e) 
			 {
		            logger.info(e.getCause(), e);
		            return null;
			 }
		}
		@RequestMapping(value = "/saveOrderForCompanyAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	    public @ResponseBody
	    boolean saveStandardDeductionsOrder(@RequestBody JSONArray jsonData) throws Exception
	    {
			String strQry = null;
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			Object Qryobj = null;
			try 
			{
				logger.info("====jsonData===="+jsonData.toString());
		    	for(int i=0;i<jsonData.size();i++)
				{
		    		JSONObject jsn=(JSONObject)jsonData.get(i);    	
		    		logger.info("====deductionOrder===="+jsn.get("AdvanceOrder"));
		    		logger.info("====stdDedCode===="+jsn.get("advanceCode"));  
		    		
		    		int advanceCode = Integer.parseInt(jsn.get("advanceCode").toString());
		    		int orderId = Integer.parseInt(jsn.get("AdvanceOrder").toString());
		    		//modified by naidu on 11-07-2016
		    		int id = i+1;//Integer.parseInt(jsn.get("id").toString());
		    		
		    		
		    		//companyAdvanceService.updateOrderForCompanyAdvance(Integer.parseInt(jsn.get("advanceCode").toString()),Integer.parseInt(jsn.get("AdvanceOrder").toString()),Integer.parseInt(jsn.get("id").toString()));
		    		
		    	//	strQry = "UPDATE AdvanceOrder set advanceOrder="+orderId+",id="+id+" WHERE advanceCode ="+advanceCode;
		    		strQry = "UPDATE CompanyAdvance set advorder="+id+" WHERE advanceCode ="+advanceCode;
		    		Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
		    	isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);

			     return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
	     }
		//--------------------------- End of Order Advances Master -------------------------------------//
		
		
		
		@RequestMapping(value = "/getAuditTrail", method = RequestMethod.POST)
		public @ResponseBody	List getAuditTrail(@RequestBody JSONObject jsonData)throws Exception 
		{

			//String season = (String) jsonData.get("season");
			
			String fromDate=(String)jsonData.get("fromDate");
			String toDate=(String)jsonData.get("toDate");
			Date date1=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
			Date date2=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
			
			//List auditTrailList = new ArrayList();//auditTrailService.getAuditTrailForSeason(date1,date2);
			List auditTrailList = auditTrailService.getAuditTrailForSeason(date1,date2);
			for(int i=0;i<auditTrailList.size();i++)
			{
				Map atMap=(Map)auditTrailList.get(i);
				Timestamp timeStamp=(Timestamp)atMap.get("audittraildate");
				String dateString=DateUtils.getStringFromTimestamp(timeStamp,"dd-MM-yyyy");
		       // SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
		       // String dateText = df2.format(date);
		       // System.out.println(dateText);
				atMap.put("audittraildate", dateString);
		        //ryotLedgerList.add(rlMap);
				Timestamp audittrailtime =(Timestamp)atMap.get("audittrailtime");
				String main=audittrailtime.toString();
				String s[] = main.split(" ",2);
				String date = s[0];
				String time = s[1];
				atMap.put("audittrailtime", time);
				
			}
			
			return auditTrailList;
		}

		
		
		
		//agcontroller	
		@RequestMapping(value = "/getMaxColumnValue", method = RequestMethod.POST)
		public @ResponseBody
		String getMaxColumnValue(@RequestBody JSONObject jsonData) throws Exception 
		{
			String tableName=(String)jsonData.get("tablename");
			String col1=(String)jsonData.get("col1");
			String col2=(String)jsonData.get("col2");
			String val=(String)jsonData.get("value");
			Integer maxId = commonService.getMaxColumnValue(tableName,col1,col2,val);
			
			JSONObject json = new JSONObject();
			json.put("id", maxId);
			return json.toString();
		}
		
		@RequestMapping(value = "/getLoanRyotCode", method = RequestMethod.POST)
		public @ResponseBody List getLoanRyotCode(@RequestBody JSONObject jsonData)throws Exception 
		{
			String season = (String) jsonData.get("season");
			Integer branchcode = (Integer) jsonData.get("branchcode");
			List LoanList = new ArrayList();
			HashMap hm= null;
			List LoanRyotCodeList = commonService.getLoanRyotCode(season,branchcode);
			
			for(int i=0;i<LoanRyotCodeList.size();i++)
			{
				hm= new HashMap();
				Map rlMap=(Map)LoanRyotCodeList.get(i);
				Integer loanNum=(Integer)rlMap.get("loannumber");
				String rCode=(String)rlMap.get("ryotcode");
				
				hm.put("loanRyotcode", loanNum+"-"+rCode);
		        LoanList.add(hm);
			}
				
			return LoanList;
		}
		//Added by sahoo on 07-07-2016
		@RequestMapping(value = "/getDatesFromCaneAccRules", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getDatesFromCaneAccRules(@RequestBody String jsonData) throws Exception 
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			List DateList = ahFuctionalService.getDatesByCalcruledate(jsonData);
			if(DateList != null && DateList.size()>0)
			{
				for (int j = 0; j < DateList.size(); j++)
				{
					Map DateMap = new HashMap();
					DateMap = (Map) DateList.get(j);
					Timestamp timeStamp=(Timestamp)DateMap.get("calcruledate");
					String dateString=null;
					if(timeStamp != null)
					{
						dateString=DateUtils.getStringFromTimestamp(timeStamp,"dd-MM-yyyy");			
						jsonObj.put("calcruledate", dateString);
						jArray.add(jsonObj);
					}
				}
			}
			return jArray;
		}
			//Added by sahadeva on 21-07-2016
		@RequestMapping(value = "/getSeedlingAdvanceRyots", method = RequestMethod.POST)
		public @ResponseBody List getSeedlingAdvanceRyots(@RequestBody JSONObject jsonData)throws Exception 
		{
			String season = (String) jsonData.get("season");
		
			List RyotList = new ArrayList();
			HashMap hm= null;
			List RyotListData = commonService.getRyotData(season);
			
			for(int i=0;i<RyotListData.size();i++)
			{
				hm= new HashMap();
				Map rlMap=(Map)RyotListData.get(i);
				
				String rCode=(String)rlMap.get("ryotcode");
				String rName=(String)rlMap.get("ryotname");
				
				hm.put("ryotcode",rCode);
				hm.put("ryotname",rName);
				
				RyotList.add(hm);
			}
				
			return RyotList;
		}
		
		//Added by sahadeva on 22-07-2016
		@RequestMapping(value = "/getAgreementsRyotsBySeason", method = RequestMethod.POST)
		public @ResponseBody List getAgreementsRyotsBySeason(@RequestBody JSONObject jsonData)throws Exception 
		{
			String season = (String) jsonData.get("season");
			String ryotcode = (String) jsonData.get("ryotcode");
		
			List RyotAgrNum = new ArrayList();
			HashMap hm= null;
			List RyotAgreementtListData = commonService.getRyotAgreementData(season,ryotcode);
			
			for(int i=0;i<RyotAgreementtListData.size();i++)
			{
				hm= new HashMap();
				Map rlMap=(Map)RyotAgreementtListData.get(i);
				
				String rAgreement=(String)rlMap.get("agreementnumber");
			
				hm.put("agreementnumber",rAgreement);
				
				RyotAgrNum.add(hm);
			}
				
			return RyotAgrNum;
		}
		//Added by sahadeva on 21-07-2016
		@RequestMapping(value = "/getSearchingData", method = RequestMethod.POST)
		public @ResponseBody List getSearchingData(@RequestBody JSONObject jsonData)throws Exception 
		{
			String ryotcode = (String) jsonData.get("ryotCode");
			String agreementno = (String) jsonData.get("agreementNumber");
			String village= (String) jsonData.get("villageCode");
			int  permitNo= (Integer) jsonData.get("permitNumber");
			logger.info("====jsonData===="+jsonData.toString());

			List SearchList = new ArrayList();
			HashMap hm= null;
			List SearchListData = commonService.getSearchData(ryotcode,agreementno,village,permitNo);
			logger.info("====SearchListData===="+SearchListData);
			for(int i=0;i<SearchListData.size();i++)
			{
				hm= new HashMap();
				Map rlMap=(Map)SearchListData.get(i);
				String rCode=(String)rlMap.get("ryotcode");
				String agrNo=(String)rlMap.get("agreementno");
				String season=(String)rlMap.get("season");
				String pltno=(String)rlMap.get("plotno");
				int mandalcode=(Integer)rlMap.get("mandalcode");
				String ryotName=(String)rlMap.get("ryotname");

				List<Mandal> mandal = ahFuctionalService.getMandalDtls(mandalcode);
				String mandalName = null;
				if(mandal != null && mandal.size()>0)
				{
					mandalName = mandal.get(0).getMandal();
				}
				String lvlg=(String)rlMap.get("landvilcode");
				Integer prgNo=(Integer)rlMap.get("programno");
				Integer permtNo=(Integer)rlMap.get("permitnumber");
				String season1=(String)rlMap.get("season");
				Date harvestDate = (Date) rlMap.get("harvestdt");
				
				hm.put("ryotcode", rCode);
				hm.put("agreementno", agrNo);
				hm.put("plotno", pltno);
				hm.put("landvilcode",lvlg);
				hm.put("programno",prgNo);
				hm.put("permitnumber",permtNo);
				hm.put("mandalName",mandalName);
				hm.put("ryotName",ryotName);
				hm.put("season",season1);
				if(harvestDate !=null)
				{
					SearchList.add(hm);
				}
			}
			logger.info("====getSearchingData() before return===="+SearchList);
			return SearchList;
		}
		//Added by DMurty on 26-07-2016
		@RequestMapping(value = "/getAccountNames", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getAccountNames(@RequestBody String jsonData) throws Exception 
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			List AccList = ahFuctionalService.getAccountNames(jsonData);
			if(AccList != null && AccList.size()>0)
			{
				for (int j = 0; j < AccList.size(); j++)
				{
					Map AccMap = new HashMap();
					AccMap = (Map) AccList.get(j);
					String accName = (String) AccMap.get("accountname");
					String accCode = (String) AccMap.get("accountcode");
					String finalAcc = accName+"-"+accCode;
					
					jsonObj.put("accName", finalAcc);
					jArray.add(jsonObj);
				}
			}
			return jArray;
		}
		
		//Added by sahadeva on 08-08-2016
		@RequestMapping(value = "/getLoginIdFromEmployeeId", method = RequestMethod.POST)
		public @ResponseBody boolean getLoginIdFromEmployeeId(@RequestBody JSONObject jsonData)throws Exception 
		{
			String loginid = (String) jsonData.get("loginId");
			List EmployeeList = new ArrayList();
			HashMap hm= null;
			boolean isRecordThere=false;
			List EmployeeData = commonService.getEmployeeData(loginid);
			if(EmployeeData != null && EmployeeData.size()>0)
			{
				isRecordThere=true;
			}
			return isRecordThere;
		}
		
		//Added by sahadeva on 08-08-2016
		@RequestMapping(value = "/getPermitRulesNew", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getPermitRulesNew(@RequestBody String jsonData) throws Exception 
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			List PermitList = commonService.getPermitRulesNew();
			if(PermitList != null && PermitList.size()>0)
			{
				Map PermitMap = new HashMap();
				PermitMap = (Map) PermitList.get(0);
				Integer id = (Integer) PermitMap.get("id");
				Integer noofpermitperacre = (Integer) PermitMap.get("noofpermitperacre");
				Integer nooftonsperacre = (Integer) PermitMap.get("nooftonsperacre");
				Integer nooftonspereachpermit = (Integer) PermitMap.get("nooftonspereachpermit");
				Byte permittype = (Byte) PermitMap.get("permittype");
				
				jsonObj.put("id", id);
				jsonObj.put("noofpermitperacre", noofpermitperacre);
				jsonObj.put("nooftonsperacre", nooftonsperacre);
				jsonObj.put("nooftonspereachpermit", nooftonspereachpermit);
				jsonObj.put("permittype", permittype);
				jArray.add(jsonObj);
			}
			return jArray;
		}
		//Added by sahadeva on 09-08-2016
		@RequestMapping(value = "/getVariety", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getVariety(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season = (String) jsonData.get("season");
			String ryotCode = (String) jsonData.get("ryotCode");
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			List VarietyList = commonService.getVariety(season,ryotCode);
			if(VarietyList != null && VarietyList.size()>0)
			{
				for (int j = 0; j < VarietyList.size(); j++)
				{
					Map VarietyMap = new HashMap();
					VarietyMap = (Map) VarietyList.get(j);
					Integer varietyCode = (Integer) VarietyMap.get("varietycode");
					
					List<Variety> varietyname= ahFuctionalService.getVarietyByVarietyCode(varietyCode);
					 String varietyName = null;
					 if(varietyname != null && varietyname.size()>0)
					 {
						 varietyName = varietyname.get(0).getVariety();
					 }
					jsonObj.put("varietyCode", varietyCode);
					jsonObj.put("varietyName", varietyName);
					jArray.add(jsonObj);
				}
			}
			return jArray;
		}

		//Added by sahadeva on 09-08-2016
		@RequestMapping(value = "/getSeedsCost", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getSeedsCost(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season = (String) jsonData.get("season");
			String ryotCode = (String) jsonData.get("ryotCode");
			Integer seedQty = Integer.parseInt((String) jsonData.get("seedlingsQuantity"));
			Integer advancecode=(Integer) jsonData.get("advanceCode");
			
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			
			List VillageCodeList = commonService.getVariety(season,ryotCode);
			if(VillageCodeList != null && VillageCodeList.size()>0)
			{
				for (int j = 0; j < VillageCodeList.size(); j++)
				{
				Map VCodeMap = new HashMap();
				VCodeMap = (Map) VillageCodeList.get(0);
				String villageCode = (String) VCodeMap.get("villagecode");
				
				List<Village> distance= ahFuctionalService.getVillageDistance(villageCode);
				 Double vDistance =0.0;
				 if(distance != null && distance.size()>0)
				 {
					 vDistance = distance.get(0).getDistance();
				 }
				 List<SeedAdvanceCost> cost= ahFuctionalService.getSeedCostUnitPerDistance(advancecode,vDistance);
				 Double villageDistanceCost =0.0;
				 if(cost != null && cost.size()>0)
				 {
					 villageDistanceCost = cost.get(0).getCostofeachunit();
				 }
				jsonObj.put("totalCost", villageDistanceCost*seedQty);
				jArray.add(jsonObj);
				}
			}
			return jArray;
		}
		//Added by sahadeva on 12-08-2016
		@RequestMapping(value = "/getSumOfAdvanceAmount", method = RequestMethod.POST)
		public @ResponseBody
		Double getSumOfAdvanceAmount(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season = (String) jsonData.get("season");
			String ryotCode = (String) jsonData.get("ryotcode");
			Integer advanceCode = (Integer) jsonData.get("advanceType");
			
			Double sumAmount = commonService.getSumOfAdvanceAmount(season,ryotCode,advanceCode);
			return sumAmount;
		}

	//Added by sahadeva on 13-09-2016
	@RequestMapping(value = "/getCaneLedger", method = RequestMethod.POST)
	public @ResponseBody
	JSONObject getCaneLedger(@RequestBody JSONObject jsonData) throws Exception 
	{
		String season = (String) jsonData.get("season");
		String ryotCode = (String) jsonData.get("ryotCode");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		JSONObject jsonObj1 =null;
		jsonObj1 = new JSONObject();
		Double agreedqty =0.0;
		
		List<WeighmentDetails> WeighmentData = commonService.getDataForCaneLedger(season,ryotCode);
		String village =null;
		String accountNum =null;
		
/*		if(WeighmentData != null && WeighmentData.size()>0)
		{
		  for (int j = 0; j < WeighmentData.size(); j++)
		  {
			  Map WdMap = new HashMap();
			  WdMap = (Map) WeighmentData.get(j);
				
			  Date crDate = (Date) WdMap.get("canereceiptdate");
			  Integer shiftId = (Integer) WdMap.get("shiftid");
			  Integer rectNo = (Integer) WdMap.get("serialnumber");
			  Double supQty = (Double) WdMap.get("netwt");
			  
			  jsonObj.put("crDate",crDate);
			  jsonObj.put("shiftId",shiftId);
			  jsonObj.put("rectNo",rectNo);
			  jsonObj.put("supQty",supQty);
		  }
		}*/
		List loanDetailsData = commonService.getloanDetailsData(season,ryotCode);
		Integer branchCode =0;
		String branch =null;
		if(loanDetailsData != null && loanDetailsData.size()>0)
		{
			for (int j = 0; j < loanDetailsData.size(); j++)
			{
				Map LoanMap = new HashMap();
				LoanMap = (Map) loanDetailsData.get(j);
				branchCode = (Integer) LoanMap.get("branchcode");
			
				List<Branch> branchName= ahFuctionalService.getBranchForLedger(branchCode);
				if(branchName != null && branchName.size()>0)
				{
					branch = branchName.get(0).getBranchname();
				}
				Integer loanNumber = (Integer) LoanMap.get("loannumber");
				String referenceNumber = (String) LoanMap.get("referencenumber");
				Double pendingAmount = (Double) LoanMap.get("pendingamount");
				jsonObj.put("branch", branch);
				jsonObj.put("referenceNumber", referenceNumber);
				jsonObj.put("branchCode", branchCode);
				jsonObj.put("loanNumber", loanNumber);
				jsonObj.put("pendingAmount", pendingAmount);
			}
		}
		List loanSummaryData = commonService.getloanSummaryData(season,ryotCode);
		if(loanSummaryData != null && loanSummaryData.size()>0)
		{
			for (int j = 0; j < loanSummaryData.size(); j++)
			{
				Map LoanSummaryMap = new HashMap();
				LoanSummaryMap = (Map) loanSummaryData.get(j);
				
				Double principle = (Double) LoanSummaryMap.get("Principle");
				Double interestAmount = (Double) LoanSummaryMap.get("interestamount");
				Double paidAmount = (Double) LoanSummaryMap.get("paidamount");
				Double pendingAmount = (Double) LoanSummaryMap.get("pendingamount");
				
				jsonObj.put("principle", principle);
				jsonObj.put("interestAmount", interestAmount);
				jsonObj.put("paidAmount", paidAmount);
				jsonObj.put("pendingAmount", pendingAmount);
			}
		}
		List AgreementdetailsData = commonService.getAgreementdetailsData(season,ryotCode);
	
		if(AgreementdetailsData != null && AgreementdetailsData.size()>0)
		{
			for (int j = 0; j < AgreementdetailsData.size(); j++)
			{
				Map AgreementMap = new HashMap();
				AgreementMap = (Map) AgreementdetailsData.get(j);
				String plantorratoon=(String) AgreementMap.get("plantorratoon");
				//  ptorrt=String.valueOf(plantorratoon);
				Double plant=0.0;
				Double ratoon=0.0;
				if(Integer.parseInt(plantorratoon)==1)
		  		{	
		  			plant=(Double) AgreementMap.get("plant"); 
		  		}
		  		else
		  		{
		  			plant=0.0;
		  		}	
				if(Integer.parseInt(plantorratoon)>1)
		  		{
		  			ratoon=(Double)AgreementMap.get("plant");
			  	}
		  		else
		  		{
		  			ratoon=0.0;
		  		}
				agreedqty = (Double) AgreementMap.get("agreedqty");
				Double progressiveqty = (Double) AgreementMap.get("progressiveqty");
				String ryotname=(String) AgreementMap.get("ryotname");
				String relativeName = (String) AgreementMap.get("relativename");
				String villageCode = (String) AgreementMap.get("villagecode");
				
				List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
				if(villageName != null && villageName.size()>0)
				{
					village = villageName.get(0).getVillage();
				}
				List<RyotBankDetails> accountNumber= ahFuctionalService.getAccountNumberForLedger(ryotCode);
				if(accountNumber != null && accountNumber.size()>0)
				{
					accountNum = accountNumber.get(0).getAccountnumber();
				}
				jsonObj.put("agreedqty",agreedqty);
				jsonObj.put("progressiveqty",progressiveqty);
				jsonObj.put("accountNum",accountNum);
				jsonObj.put("plant",plant);
				jsonObj.put("ratoon",ratoon);
				jsonObj.put("ryotname",ryotname);
				jsonObj.put("relativeName",relativeName);
				jsonObj.put("ryotcode",ryotCode);
				jsonObj.put("village",village);
			}
		}
		
		
		Double udcAmount = ahFuctionalService.GetUDCAmount(season,ryotCode);
		if(udcAmount !=null)
		{
			jsonObj.put("udcAmount", new DecimalFormat("#.##").format(udcAmount));
		}
		Double cdcAmount = ahFuctionalService.GetCDCAmount(season,ryotCode);
		if(cdcAmount !=null)
		{
			jsonObj.put("cdcAmount", new DecimalFormat("#.##").format(cdcAmount));
		}
		Double transport = ahFuctionalService.GetTransportAmount(season,ryotCode);
		if(transport !=null)
		{
			jsonObj.put("transport", new DecimalFormat("#.##").format(transport));
		}
		Double advanceAmount = ahFuctionalService.GetAdvanceAmount(season,ryotCode);
		if(advanceAmount !=null)
		{
			jsonObj.put("advanceAmount",new DecimalFormat("#.##").format(advanceAmount));
		}
		Double icpAmount = ahFuctionalService.GetICPAmount(season,ryotCode);
		if(icpAmount !=null)
		{
			jsonObj.put("icpAmount", new DecimalFormat("#.##").format(icpAmount));
		}
		Double harvAmountPay = ahFuctionalService.GetHarvestAmountPayble(season,ryotCode);
		if(harvAmountPay !=null)
		{
			jsonObj.put("harvAmountPay", new com.ibm.icu.text.DecimalFormat(" #,##,##0.00").format(harvAmountPay));
		}
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(season,WeighmentData.get(0).getCanereceiptdate().toString());
		Double canePrice=0.0;
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			 canePrice=caneAcctRules.get(0).getSeasoncaneprice();
		}
		//Double canePrice = ahFuctionalService.GetCanePrice(season);
		jsonObj.put("canePrice", new DecimalFormat("#.##").format(canePrice));
		jArray.add(jsonObj);
		jsonObj1.put("formData", jArray);
		
		JSONArray jArray2 = new JSONArray();
		int i=1;
		//int p=2495;
		for(WeighmentDetails l:WeighmentData)
		{
		
			JSONObject subjsonObj=new JSONObject();
			subjsonObj.put("id",i);
			if(l.getCanereceiptenddate()!=null)
			{
				subjsonObj.put("crDate",l.getCanereceiptenddate().toString());
			}
			  subjsonObj.put("shiftId",l.getShiftid1());
			  subjsonObj.put("rectNo",l.getSerialnumber());
			if(l.getNetwt()!=null)
			{ 
				 subjsonObj.put("supQty",l.getNetwt());
			}
			subjsonObj.put("amountPayable",canePrice*l.getNetwt());
			jArray2.add(subjsonObj);
			i++;
		}
		
		jsonObj1.put("gridData",jArray2);
		return jsonObj1;
	}
		
	//Added by sahadeva on 14-09-2016
	@RequestMapping(value = "/getCaneWeighmentRyots", method = RequestMethod.POST)
	public @ResponseBody List getCaneWeighmentRyots(@RequestBody JSONObject jsonData)throws Exception 
	{
		String season = (String) jsonData.get("season");
	
		List RyotList = new ArrayList();
		HashMap hm= null;
		List RyotListData = commonService.getRyotForCaneLedgerReport(season);
		
		for(int i=0;i<RyotListData.size();i++)
		{
			hm= new HashMap();
			Map rlMap=(Map)RyotListData.get(i);
			
			String rCode=(String)rlMap.get("ryotcode");
			String rName=(String)rlMap.get("ryotname");
			
			hm.put("ryotcode",rCode);
			hm.put("ryotname",rName);
			
			RyotList.add(hm);
		}
			
		return RyotList;
	}
	
	
	//Added by sahadeva on 28-11-2016
	@RequestMapping(value = "/getVehicleNumbers", method = RequestMethod.POST)
	public @ResponseBody
	List getVehicleNumbers(@RequestBody JSONObject jsonData) throws Exception 
	{
		
		String season = (String) jsonData.get("season");
		//String vehicleNo = (String) jsonData.get("vehicleNo");
		List DataList = new ArrayList();
		HashMap hm= null;
		List VehicleList = ahFuctionalService.getVehicleNumbers(season);
		if(VehicleList != null && VehicleList.size()>0)
		{
			for (int j = 0; j < VehicleList.size(); j++)
			{
				hm= new HashMap();
				Map VMap=(Map)VehicleList.get(j);
				String vehicleNos = (String) VMap.get("vehicleno");
		
				hm.put("vehicleNos", vehicleNos);
				DataList.add(hm);
			}
		}
		return DataList;
	}
	
		//Added by sahadeva on 29-11-2016
		@RequestMapping(value = "/getRyotFromCircle", method = RequestMethod.POST)
		public @ResponseBody
		List getRyotFromCircle(@RequestBody JSONObject jsonData) throws Exception 
		{
			/*JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();*/
			
			Integer circlecode=(Integer) jsonData.get("circle");
			List DataList = new ArrayList();
			HashMap hm= null;
			List RyotList = commonService.getRyotFromCircle(circlecode);
			if(RyotList != null && RyotList.size()>0)
			{
				for (int j = 0; j < RyotList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)RyotList.get(j);
					String ryotcode = (String) VMap.get("ryotcode");
					String ryotname = (String) VMap.get("ryotname");
					hm.put("ryotcode", ryotcode);
					hm.put("ryotname", ryotname);
					DataList.add(hm);
				}
			}
			return DataList;
		}
		//Added by Umanath on 29-11-2016
		@RequestMapping(value = "/getCirclesByZone", method = RequestMethod.POST)
		public @ResponseBody
		List getCirclesByZone(@RequestBody JSONObject jsonData) throws Exception 
		{
			Integer zonecode = (Integer) jsonData.get("zone");
			List DataList = new ArrayList();
			HashMap hm= null;
			List circleList = commonService.listGetCirclesFromZones(zonecode);
			if(circleList != null && circleList.size()>0)
			{
				for (int j = 0; j < circleList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)circleList.get(j);
					Integer code = (Integer) VMap.get("circlecode");
					String name = (String) VMap.get("circle");
					hm.put("circlecode", code);
					hm.put("circle", name);
					DataList.add(hm);
				}
			}
			return DataList;
		}
		
		
		
		
		public static Date addDays(Date date, int days)
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			cal.add(Calendar.DATE, days);

			return cal.getTime();
		}
		//Added by sahadeva on 19-12-2016
		@RequestMapping(value = "/getDateByCaneslno", method = RequestMethod.POST)
		public @ResponseBody
		List getDateByCaneslno(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season = (String) jsonData.get("season");
			Integer caneSlNo = (Integer) jsonData.get("foCode");
			List DataList = new ArrayList();
			HashMap hm= null;
			List dateList = commonService.getDateByCaneslnoForUpdation(season,caneSlNo);
			if(dateList != null && dateList.size()>0)
			{
				
					hm= new HashMap();
					Map VMap=(Map)dateList.get(0);
					Date code = (Date) VMap.get("dateofcalc");
					int a=1;
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strdatefrom = df.format(code);
					byte acctype = (Byte) VMap.get("acctype");
					hm.put("canedate", strdatefrom);
					hm.put("acctype", acctype);
					DataList.add(hm);
				
			}
			return DataList;
		}
		
		//Added by sahadeva on 02-03-2017
		@RequestMapping(value = "/getAdvanceAndLoanData", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getAdvanceAndLoanData(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season = (String) jsonData.get("season");
			String loan = (String) jsonData.get("loanDetails");
			/*String all = (String) jsonData.get("All");
			String advance=(String) jsonData.get("advance");*/
			int noOfDaysPerYear=365;
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			if("ADVANCE".equalsIgnoreCase(loan))
			{
				List AdvancePAmountList = commonService.getAdvPrncAmt(season);
				if(AdvancePAmountList != null && AdvancePAmountList.size()>0)
				{
					for (int j = 0; j < AdvancePAmountList.size(); j++)
					{
						Map DataMap = new HashMap();
						DataMap = (Map) AdvancePAmountList.get(j);
						Integer advCode = (Integer) DataMap.get("advancecode");
						String ryotCode = (String) DataMap.get("ryotcode");
						double principle = (Double) DataMap.get("principle");
						
						double nDays = 0.0;
						Date principleDate = (Date) DataMap.get("principledate");
						DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strprincipleDate = df1.format(principleDate);
						principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
											
						Date curDate = new Date();
						df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strcurDate = df1.format(curDate);
						curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

						double months=0.00;
						long earliertime=principleDate.getTime();
						long latertime=curDate.getTime();
						
						long days=DateUtils.dateDifference(latertime, earliertime);
						
						days = Math.abs(days);
						Long ldays = new Long(days);
						double dDays = ldays.doubleValue();
						nDays = dDays+1;
						months=dDays/30;
						months = Math.round(months * 100.0) / 100.0;
						
						Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,advCode);
						logger.info("getAllAdvancesDetails()------Advance interest"+interest);

						if(interest==null)
							interest=0.00;
						
						double intrate = interest/100;
						
						double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
						double intrestAmt = intrestPerDay*nDays;
						intrestAmt = round(intrestAmt);

						jsonObj.put("intrestAmt", intrestAmt);
						
						Double total=principle+intrestAmt;
						
						
						List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
						String  villageCode=null;
						if(rName != null && rName.size()>0)
						{
							String   ryotName = rName.get(0).getRyotname();
							villageCode = rName.get(0).getVillagecode();
							jsonObj.put("ryotName", ryotName);
						}
						List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
						if(villageName != null && villageName.size()>0)
						{
							String village = villageName.get(0).getVillage();
							jsonObj.put("village", village);
						}
						String	sql4="select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+"";
	    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	    		  		Double advanceamount=0.0;
	    		  		if(ls4.size()>0 && ls4!=null)
	    		  		{
	    		  			advanceamount=(Double)((Map<Object,Object>)ls4.get(0)).get("advanceamount");
	    		  			if(advanceamount !=null)
	    		  			{
	    		  				advanceamount = advanceamount+intrestAmt;
	    		  				advanceamount =Double.parseDouble(new DecimalFormat("##.##").format(advanceamount));
	    		  				jsonObj.put("TotalAmount", advanceamount);
	    		  			}
	    		  			else
	    		  				advanceamount=0.0;
	    		  		}
	    		  		String	sql5="select  sum(paidamt) paidamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and accountnum="+advCode+" and advloan=0";
	    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	    		  		Double paidamt=0.0;
	    		  		if(ls5.size()>0 && ls5!=null)
	    		  		{
	    		  			if((Double)((Map<Object,Object>)ls5.get(0)).get("paidamt") != null)
	    		  			{
	    		  				paidamt=(Double)((Map<Object,Object>)ls5.get(0)).get("paidamt");
		    		  			paidamt =Double.parseDouble(new DecimalFormat("##.##").format(paidamt));
	    		  			}
	    		  			
	    		  			if (paidamt !=null)
	    		  			{
	    		  				jsonObj.put("TotalRecovery", paidamt);
	    		  			}
	    		  			else 
	    		  				paidamt=0.0;
	    		  		}
	    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
	    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
	    		  		Double supply=0.0;
	    		  		if(ls6.size()>0 && ls6!=null)
	    		  		{
	    		  			if((Double)((Map<Object,Object>)ls6.get(0)).get("netwt") != null)
	    		  			{
	    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
		    		  			supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
	    		  			}
	    		  		}
	    		  		double pending = advanceamount-paidamt;
	    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
	    		  		if(pending<0 || pending==0)
	    		  		{
	    		  			continue;
	    		  		}
	    		  		jsonObj.put("Quantity", supply);
	    		  		jsonObj.put("balance", pending);
						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("loanNo", advCode);
						jsonObj.put("loanREfNo", advCode);
						
						
						jArray.add(jsonObj);
					}
				}
			}
			else if("LOAN".equalsIgnoreCase(loan))
			{
				List LoanPAmountList = commonService.getLoanPrncData(season);
				if(LoanPAmountList != null && LoanPAmountList.size()>0)
				{
					for (int j = 0; j < LoanPAmountList.size(); j++)
					{
						Map DataMap = new HashMap();
						DataMap = (Map) LoanPAmountList.get(j);
						Integer branchCode = (Integer) DataMap.get("branchcode");
						Integer loanNumber = (Integer) DataMap.get("loannumber");
						String ryotCode = (String) DataMap.get("ryotcode");
						String loanAccountNumber = (String) DataMap.get("loanaccountnumber");
						
						double principle = (Double) DataMap.get("principle");

						Date principleDate = (Date) DataMap.get("principledate");
						DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strprincipleDate = df1.format(principleDate);
						principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
							
						Date curDate = new Date();
						df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strcurDate = df1.format(curDate);
						curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

						
						int loanNo = loanNumber;
						double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotCode,season,branchCode,loanNo);
						double months=0.00;
						
						long earliertime=principleDate.getTime();
						long latertime=curDate.getTime();
						
						long days=DateUtils.dateDifference(latertime, earliertime);
						days = Math.abs(days);
						Long ldays = new Long(days);
						double dDays = ldays.doubleValue();
						if(previousIntrest == 0)
						{
							dDays = dDays+1;
						}
						months=dDays/30;
						months = Math.round(months * 100.0) / 100.0;
						
						Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(principle,branchCode,season);
						logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

						if(interestRate==null)
							interestRate=0.00;
						
						double intrate = interestRate/100;
						double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
						double intrestAmt = intrestPerDay*dDays;
						intrestAmt = round(intrestAmt);
						
						jsonObj.put("intrestAmt", intrestAmt);
						
						Double totalAmoutOnInterest=principle+intrestAmt;
						
						
						List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
						String  villageCode=null;
						if(rName != null && rName.size()>0)
						{
							String   ryotName = rName.get(0).getRyotname();
							villageCode = rName.get(0).getVillagecode();
							jsonObj.put("ryotName", ryotName);
						}
						List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
						if(villageName != null && villageName.size()>0)
						{
							String village = villageName.get(0).getVillage();
							jsonObj.put("village", village);
						}
						String	sql4="select  sum(disbursedamount) disbursedamount from LoanDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and disburseddate IS NOT NULL and referencenumber IS NOT NULL ";
	    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	    		  		Double disbursedamount=0.0;
	    		  		if(ls4.size()>0 && ls4!=null)
	    		  		{
	    		  			disbursedamount=(Double)((Map<Object,Object>)ls4.get(0)).get("disbursedamount");
	    		  			if(disbursedamount !=null)
	    		  			{
	    		  				//disbursedamount = disbursedamount+intrestAmt;
	    		  				disbursedamount =Double.parseDouble(new DecimalFormat("##.##").format(disbursedamount));
	    		  				jsonObj.put("TotalAmount", disbursedamount);
	    		  			}
	    		  			else
	    		  				disbursedamount=0.0;
	    		  		}
	    		  			
	    		  		String	sql5="select  sum(advisedamt) advisedamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advloan=1 and branchcode="+branchCode+" and loanno="+loanNumber+" ";
	    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	    		  		Double advisedamt=0.0;
	    		  		if(ls5.size()>0 && ls5!=null)
	    		  		{
	    		  			advisedamt=(Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt");
	    		  			if(advisedamt !=null)
	    		  			{
	    		  				advisedamt =Double.parseDouble(new DecimalFormat("##.##").format(advisedamt));
	    		  				jsonObj.put("TotalRecovery", advisedamt);
	    		  			}
	    		  			else 
	    		  				advisedamt=0.0;
	    		  		}
	    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
	    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
	    		  		Double supply=0.0;
	    		  		if(ls6.size()>0 && ls6!=null)
	    		  		{
	    		  			if((Double)((Map<Object	,Object>)ls6.get(0)).get("netwt") != null)
	    		  			{
	    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
	    		  				supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
	    		  			}
	    		  			jsonObj.put("Quantity", supply);
	    		  		}
	    		  		
	    		  		double pending = disbursedamount+intrestAmt-advisedamt;
	    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
	    		  		if(pending<0 || pending==0)
	    		  		{
	    		  			continue;
	    		  		}
	    		  		jsonObj.put("balance",pending);
						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("loanNo", branchCode);
						jsonObj.put("loanREfNo", loanAccountNumber);
						jArray.add(jsonObj);
					}
				}
			}
			else
			{
				List AdvancePAmountList = commonService.getAdvPrncAmt(season);
				if(AdvancePAmountList != null && AdvancePAmountList.size()>0)
				{
					for (int j = 0; j < AdvancePAmountList.size(); j++)
					{
						Map DataMap = new HashMap();
						DataMap = (Map) AdvancePAmountList.get(j);
						Integer advCode = (Integer) DataMap.get("advancecode");
						String ryotCode = (String) DataMap.get("ryotcode");
						double principle = (Double) DataMap.get("principle");
						
						double nDays = 0.0;
						Date principleDate = (Date) DataMap.get("principledate");
						DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strprincipleDate = df1.format(principleDate);
						principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
												
						Date curDate = new Date();
						df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strcurDate = df1.format(curDate);
						curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

						double months=0.00;
						long earliertime=principleDate.getTime();
						long latertime=curDate.getTime();
						
						long days=DateUtils.dateDifference(latertime, earliertime);
						
						days = Math.abs(days);
						Long ldays = new Long(days);
						double dDays = ldays.doubleValue();
						nDays = dDays+1;
						months=dDays/30;
						months = Math.round(months * 100.0) / 100.0;
						
						Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,advCode);
						logger.info("getAllAdvancesDetails()------Advance interest"+interest);

						if(interest==null)
							interest=0.00;
						
						double intrate = interest/100;
						
						double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
						double intrestAmt = intrestPerDay*nDays;
						intrestAmt = round(intrestAmt);
						
						jsonObj.put("intrestAmt", intrestAmt);
						
						Double total=principle+intrestAmt;
						
						
						List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
						String  villageCode=null;
						if(rName != null && rName.size()>0)
						{
							String   ryotName = rName.get(0).getRyotname();
							villageCode = rName.get(0).getVillagecode();
							jsonObj.put("ryotName", ryotName);
						}
						List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
						if(villageName != null && villageName.size()>0)
						{
							String village = villageName.get(0).getVillage();
							jsonObj.put("village", village);
						}
						String	sql4="select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+"";
	    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	    		  		Double advanceamount=0.0;
	    		  		if(ls4.size()>0 && ls4!=null)
	    		  		{
	    		  			advanceamount=(Double)((Map<Object,Object>)ls4.get(0)).get("advanceamount");
	    		  			if(advanceamount !=null)
	    		  			{
	    		  				advanceamount = advanceamount+intrestAmt;
	    		  				advanceamount =Double.parseDouble(new DecimalFormat("##.##").format(advanceamount));
	    		  				jsonObj.put("TotalAmount", advanceamount);
	    		  			}
	    		  			else
	    		  				advanceamount=0.0;
	    		  		}
	    		  		String	sql5="select  sum(paidamt) paidamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and accountnum="+advCode+" and advloan=0";
	    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	    		  		Double paidamt=0.0;
	    		  		if(ls5.size()>0 && ls5!=null)
	    		  		{
	    		  			paidamt=(Double)((Map<Object,Object>)ls5.get(0)).get("paidamt");
	    		  			if(paidamt!=null)
	    		  			{
	    		  				paidamt =Double.parseDouble(new DecimalFormat("##.##").format(paidamt));
	    		  				jsonObj.put("TotalRecovery", paidamt);
	    		  			}
	    		  			else paidamt=0.0;
	    		  		}
	    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
	    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
	    		  		Double supply=0.0;
	    		  		if(ls6.size()>0 && ls6!=null)
	    		  		{
	    		  			if((Double)((Map<Object,Object>)ls6.get(0)).get("netwt") != null)
	    		  			{
	    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
	    		  				supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
	    		  			}
	    		  			
	    		  			jsonObj.put("Quantity", supply);
	    		  		}
	    		  		double pending = advanceamount-paidamt;
	    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
	    		  		if(pending<0 || pending==0)
	    		  		{
	    		  			continue;
	    		  		}
	    		  		jsonObj.put("balance", pending);
						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("loanNo", advCode);
						jsonObj.put("loanREfNo", advCode);
						jArray.add(jsonObj);
					}
				}

				List LoanPAmountList = commonService.getLoanPrncData(season);
				if(LoanPAmountList != null && LoanPAmountList.size()>0)
				{
					for (int j = 0; j < LoanPAmountList.size(); j++)
					{
						Map DataMap = new HashMap();
						DataMap = (Map) LoanPAmountList.get(j);
						Integer branchCode = (Integer) DataMap.get("branchcode");
						Integer loanNumber = (Integer) DataMap.get("loannumber");
						String ryotCode = (String) DataMap.get("ryotcode");
						String loanAccountNumber = (String) DataMap.get("loanaccountnumber");
						
						double principle = (Double) DataMap.get("principle");

						Date principleDate = (Date) DataMap.get("principledate");
						DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strprincipleDate = df1.format(principleDate);
						principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
							
						Date curDate = new Date();
						df1 = new SimpleDateFormat("dd-MM-yyyy");
						String strcurDate = df1.format(curDate);
						curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

						
						int loanNo = loanNumber;
						double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotCode,season,branchCode,loanNo);
						double months=0.00;
						
						long earliertime=principleDate.getTime();
						long latertime=curDate.getTime();
						
						long days=DateUtils.dateDifference(latertime, earliertime);
						days = Math.abs(days);
						Long ldays = new Long(days);
						double dDays = ldays.doubleValue();
						if(previousIntrest == 0)
						{
							dDays = dDays+1;
						}
						months=dDays/30;
						months = Math.round(months * 100.0) / 100.0;
						
						Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(principle,branchCode,season);
						logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

						if(interestRate==null)
							interestRate=0.00;
						
						double intrate = interestRate/100;
						double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
						double intrestAmt = intrestPerDay*dDays;
						intrestAmt = round(intrestAmt);
						
						jsonObj.put("intrestAmt", intrestAmt);
						
						Double totalAmoutOnInterest=principle+intrestAmt;
						
						
						List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
						String  villageCode=null;
						if(rName != null && rName.size()>0)
						{
							String   ryotName = rName.get(0).getRyotname();
							villageCode = rName.get(0).getVillagecode();
							jsonObj.put("ryotName", ryotName);
						}
						List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
						if(villageName != null && villageName.size()>0)
						{
							String village = villageName.get(0).getVillage();
							jsonObj.put("village", village);
						}
						String	sql4="select  sum(disbursedamount) disbursedamount from LoanDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and disburseddate IS NOT NULL and referencenumber IS NOT NULL ";
	    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	    		  		Double disbursedamount=0.0;
	    		  		if(ls4.size()>0 && ls4!=null)
	    		  		{
	    		  			disbursedamount=(Double)((Map<Object,Object>)ls4.get(0)).get("disbursedamount");
	    		  			if(disbursedamount !=null)
	    		  			{
	    		  				//disbursedamount = disbursedamount+intrestAmt;
	    		  				disbursedamount =Double.parseDouble(new DecimalFormat("##.##").format(disbursedamount));
	    		  				jsonObj.put("TotalAmount", disbursedamount);
	    		  			}
	    		  			else
	    		  				disbursedamount=0.0;
	    		  		}
	    		  		String	sql5="select  sum(advisedamt) advisedamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advloan=1 and branchcode="+branchCode+" and loanno="+loanNumber+" ";
	    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	    		  		Double advisedamt=0.0;
	    		  		if(ls5.size()>0 && ls5!=null)
	    		  		{
	    		  			advisedamt=(Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt");
	    		  			if(advisedamt !=null)
	    		  			{
	    		  				advisedamt =Double.parseDouble(new DecimalFormat("##.##").format(advisedamt));
	    		  				jsonObj.put("TotalRecovery", advisedamt);
	    		  			}
	    		  			else
	    		  				advisedamt=0.0;
	    		  		}
	    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
	    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
	    		  		Double supply=0.0;
	    		  		if(ls6.size()>0 && ls6!=null)
	    		  		{
	    		  			if((Double)((Map<Object,Object>)ls6.get(0)).get("netwt") !=null)
	    		  			{
	    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
		    		  			supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
	    		  			}
	    		  			jsonObj.put("Quantity", supply);
	    		  		}
	    		  		double pending = disbursedamount+intrestAmt-advisedamt;
	    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
	    		  		if(pending<0 || pending==0)
	    		  		{
	    		  			continue;
	    		  		}
	    		  		jsonObj.put("balance", pending);
						jsonObj.put("ryotCode", ryotCode);
						jsonObj.put("loanNo", branchCode);
						jsonObj.put("loanREfNo", loanAccountNumber);
						jArray.add(jsonObj);
					}
				}
			}
			return jArray;
		}
		
		//Added by Naidu on 31-03-2017 For Zones Correction in Agreement
		@RequestMapping(value = "/getZonesForVillage", method = RequestMethod.POST)
		public @ResponseBody
		List getZonesForVillage(@RequestBody String villagecode) throws Exception 
		{
			List DataList = new ArrayList();
			HashMap hm= null;
			int zonecode = ahFuctionalService.getZoneCodeForVillage(villagecode);
			List zoneList = commonService.getZonesListByZone(zonecode);
			
			if((Map)zoneList.get(0) != null && zoneList.size()>0)
			{
					hm= new HashMap();
					Map VMap=(Map)zoneList.get(0);
					Integer code = (Integer) VMap.get("zonecode");
					String name = (String) VMap.get("zone");
					hm.put("id", code);
					hm.put("zone", name);
					DataList.add(hm);
			}
			
			return DataList;
		}
		//Added by Naidu on 31-03-2017 For Zones Correction in Agreement
		@RequestMapping(value = "/getZonesForCircle", method = RequestMethod.POST)
		public @ResponseBody
		List getZonesForCircle(@RequestBody String circleCode) throws Exception 
		{
			List DataList = new ArrayList();
			HashMap hm= null;
			
			List CircleList = ahFuctionalService.getCircleDetailsNew(Integer.parseInt(circleCode));
			List zoneList = commonService.getZonesListByZone((Integer)((Map<Object,Object>)CircleList.get(0)).get("zonecode"));
			if((Map)zoneList.get(0) != null && zoneList.size()>0)
			{
					hm= new HashMap();
					Map VMap=(Map)zoneList.get(0);
					Integer code = (Integer) VMap.get("zonecode");
					String name = (String) VMap.get("zone");
					hm.put("id", code);
					hm.put("zone", name);
					DataList.add(hm);
			}
			
			return DataList;
		}
	
}