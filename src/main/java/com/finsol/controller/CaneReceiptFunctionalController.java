package com.finsol.controller;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.CaneWeighmentBean;
import com.finsol.bean.PermitDetailsBean;
import com.finsol.bean.WeighmentDetailsBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AccruedAndDueCaneValueSummary;
import com.finsol.model.AdditionalCaneValue;
import com.finsol.model.AgreementDetails;
import com.finsol.model.CDCDetailsByRotAndDate;
import com.finsol.model.CDCDetailsByRyot;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.CaneReceiptDaySmry;
import com.finsol.model.CaneReceiptDaySmryByRyot;
import com.finsol.model.CaneReceiptSeasonSmry;
import com.finsol.model.CaneReceiptShiftSmry;
import com.finsol.model.CaneReceiptWBBook;
import com.finsol.model.CaneValueDetailsByRyot;
import com.finsol.model.CaneValueDetailsByRyotAndDate;
import com.finsol.model.CaneWeight;
import com.finsol.model.Circle;
import com.finsol.model.CompositePrKeyForPermitDetails;
import com.finsol.model.Employees;
import com.finsol.model.HarvChgDtlsByRyotContAndDate;
import com.finsol.model.HarvChgSmryByContAndDate;
import com.finsol.model.HarvChgSmryByRyotAndCont;
import com.finsol.model.HarvChgSmryBySeason;
import com.finsol.model.HarvChgSmryBySeasonByCont;
import com.finsol.model.HarvestingRateDetails;
import com.finsol.model.ICPDetailsByRyot;
import com.finsol.model.ICPDetailsByRyotAndDate;
import com.finsol.model.PermitDetails;
import com.finsol.model.PostSeasonValueSummary;
import com.finsol.model.Ryot;
import com.finsol.model.SetupDetails;
import com.finsol.model.Shift;
import com.finsol.model.SubsidyDetailsByRotAndDate;
import com.finsol.model.SubsidyDetailsByRyot;
import com.finsol.model.TranspChgSmryByContAndDate;
import com.finsol.model.TranspChgSmryByRyotAndCont;
import com.finsol.model.TranspChgSmryByRyotContAndDate;
import com.finsol.model.TranspChgSmryBySeason;
import com.finsol.model.TranspChgSmryBySeasonByCont;
import com.finsol.model.TransportAllowanceDetails;
import com.finsol.model.TransportingRateDetails;
import com.finsol.model.UCDetailsByRotAndDate;
import com.finsol.model.UCDetailsByRyot;
import com.finsol.model.Variety;
import com.finsol.model.WeighmentDetails;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CaneReceiptFunctionalService;
import com.finsol.service.CaneWeightService;
import com.finsol.service.CommonService;
import com.finsol.service.RyotService;
import com.finsol.service.SetupDetailsService;
import com.finsol.service.ShiftService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
import com.finsol.model.Employees;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import com.finsol.controller.AHFunctionalController;
/**
 * @author Rama Krishna
 * 
 */
@Controller
public class CaneReceiptFunctionalController {
	
	private static final Logger logger = Logger.getLogger(CaneReceiptFunctionalController.class);
	
	@Autowired	
	private CaneReceiptFunctionalService caneReceiptFunctionalService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private CaneWeightService caneWeightService;
	@Autowired
	private RyotService ryotService;
	
	@Autowired
	private  ShiftService shiftService;
	
	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired	
	private HibernateDao hibernateDao;
	@Autowired	
	private AHFunctionalController ahfController;
	@Autowired	
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	
	private File fileObj;
	
	DecimalFormat HMSDecFormatter = new DecimalFormat("0.00");
	
	@Autowired
	private SetupDetailsService setupDetailsService;
	
	@Autowired	
	private LoginController loginController;
	
	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value = "/getMaxIdForVehicleCheckSlno", method = RequestMethod.POST)
	public @ResponseBody
	String getMaxIdForVehicleCheckSlno(@RequestBody JSONObject jsonData) throws Exception 
	{
		String tableName=(String)jsonData.get("tablename");
		String vcheckindt=(String) jsonData.get("vehicleInDate");
		String todayTime=(String) jsonData.get("vehicleInTime");
		vcheckindt=getWeighDatePerShift(vcheckindt,todayTime);
		int shiftid=getShiftId();
		Integer maxId = caneReceiptFunctionalService.getMaxIDForVcheckIn(tableName,vcheckindt,shiftid);
		
		JSONObject json = new JSONObject();
		json.put("id", maxId);
		return json.toString();
	}
	@RequestMapping(value = "/getPermitDetailsByPermitNumber", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	PermitDetailsBean getPermitDetailsByPermitNumber(@RequestBody Integer permitnumber) throws Exception 
	{
		logger.info("========getPermitDetailsByPermitNumber()========== permitnumber--"+permitnumber);			

		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		compositePrKeyForPermitDetails.setPermitnumber(permitnumber);
		int programNo =  caneReceiptFunctionalService.getProgramNo(permitnumber);
		compositePrKeyForPermitDetails.setProgramno(programNo);
		try
		{
			PermitDetails pd=new PermitDetails();
			PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
			PermitDetailsBean permitDetailsBean = prepareBeanForPermitDetails(permitDetails,permitnumber);
		
			return permitDetailsBean;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
            return null;
		}
	}
	
	@RequestMapping(value = "/getPermitDetailsByPermitNumberNew", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getPermitDetailsByPermitNumberNew(@RequestBody Integer permitnumber) throws Exception 
	{
		logger.info("========getPermitDetailsByPermitNumberNew()========== permitnumber--"+permitnumber);			

		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		compositePrKeyForPermitDetails.setPermitnumber(permitnumber);
		int programNo =  caneReceiptFunctionalService.getProgramNo(permitnumber);
		compositePrKeyForPermitDetails.setProgramno(programNo);
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		try
		{
			List weighmentList =  caneReceiptFunctionalService.getWeighmentDetailsByPermit(permitnumber);
			if(weighmentList != null && weighmentList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) weighmentList.get(0);
				
				int slNo = (Integer) tempMap.get("serialnumber");
				String agrNo = (String) tempMap.get("agreementnumber");
				String ryotCode = (String) tempMap.get("ryotcode");
				int circleCode = (Integer) tempMap.get("circlecode");
				int permitNo = (Integer) tempMap.get("permitnumber");
				int varietyCode = (Integer) tempMap.get("varietycode");
				String ryotName = (String) tempMap.get("ryotname");
				String relativeName = (String) tempMap.get("fhgname");
				String villageCode = (String) tempMap.get("villagecode");
				String landVillageCode = (String) tempMap.get("landvilcode");
				Byte burntCane=(Byte) tempMap.get("burntcane");
				Integer vehicleType = (Integer) tempMap.get("vehicletype");
				Integer weighmentNo = (Integer) tempMap.get("weighmentno");
				Integer weighBridgeSlNo = (Integer) tempMap.get("weighbridgeslno");
				Byte partLoad=(Byte) tempMap.get("partload");
				String weightmenType = (String) tempMap.get("weightmentype");
				Integer secondPermitNo = (Integer) tempMap.get("secondpermitno");
				Integer ucCode = (Integer) tempMap.get("uccode");
				Integer serialNo = (Integer) tempMap.get("serialnumber");
				Byte plantOrRatoon=(Byte) tempMap.get("plantorratoon");
				Integer vehicle = (Integer) tempMap.get("vehicletype");
				String vehicleNo = (String) tempMap.get("vehicleno");
				String season = (String) tempMap.get("season");
				String plotNumber = (String) tempMap.get("plotnumber");
				Date weighmentDate =(Date) tempMap.get("canereceiptdate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strweighmentDate = df.format(weighmentDate);
				
				List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotCode);
				Ryot ryot=ryots.get(0);
				
				String circleName = "NA";
				List<String> circleList = ahFuctionalService.getCirclePerGivenCircle(circleCode);
				if (circleList != null && circleList.size() > 0) 
				{
					circleName = circleList.get(0);
				}
				String villageName = "NA";
				List<String> VillageList = new ArrayList<String>();
				VillageList = ahFuctionalService.getVillageDetailsByCode(ryot.getVillagecode());
				if (VillageList != null && VillageList.size() > 0) 
				{
					String myResult1 = VillageList.get(0);
					villageName = myResult1;
				}
				String variety = "NA";
				List<String> varietyList = new ArrayList<String>();
				varietyList = ahFuctionalService.getVarietye(varietyCode);
				if (varietyList != null && varietyList.size() > 0) 
				{
					String varietyn=varietyList.get(0);
					variety=varietyn;
				}
				jsonObj.put("circle", circleName);
				jsonObj.put("village", villageName);
				jsonObj.put("variety", variety);
				
				jsonObj.put("serialNo", slNo);  
				jsonObj.put("agreementNumber", agrNo);  
				jsonObj.put("ryotCode", ryotCode); 
				jsonObj.put("circleCode", circleCode);
				jsonObj.put("permitNumber", permitNo);
				jsonObj.put("varietyCode", varietyCode);
				jsonObj.put("ryotName", ryotName);
				jsonObj.put("relativeName", relativeName);
				jsonObj.put("villageCode", villageCode);
				jsonObj.put("landVillageCode", landVillageCode);
				jsonObj.put("burntCane", burntCane);
				jsonObj.put("vehicleType", vehicleType);
				jsonObj.put("weighmentNo", weighmentNo);
				jsonObj.put("weighBridgeSlNo", weighBridgeSlNo);
				jsonObj.put("partLoad", partLoad);
				jsonObj.put("weightmenType", weightmenType);
				jsonObj.put("secondPermitNo", secondPermitNo);
				jsonObj.put("ucCode", ucCode);
				jsonObj.put("serialNo", serialNo);
				jsonObj.put("plantOrRatoon", plantOrRatoon);
				jsonObj.put("vehicle", vehicle);
				jsonObj.put("vehicleNo", vehicleNo);
				jsonObj.put("season", season);
				jsonObj.put("plotNumber", plotNumber);
				jsonObj.put("weighmentDate", strweighmentDate);
				
				jArray.add(jsonObj);
			}
			else
			{
				PermitDetails pd=new PermitDetails();
				PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
				
				List<Ryot> ryots=ryotService.getRyotByRyotCode(permitDetails.getRyotcode());
				Ryot ryot=ryots.get(0);
				String circleName = "NA";
				 List<String> circleList = ahFuctionalService.getCirclePerGivenCircle(permitDetails.getCirclecode());
				 if (circleList != null && circleList.size() > 0) 
				 {
					 circleName = circleList.get(0);
				 }
				String villageName = "NA";
				List<String> VillageList = new ArrayList<String>();
				VillageList = ahFuctionalService.getVillageDetailsByCode(ryot.getVillagecode());
				if (VillageList != null && VillageList.size() > 0) 
				{
					String myResult1 = VillageList.get(0);
					villageName = myResult1;
				}
				
				String variety = "NA";
				List<String> varietyList = new ArrayList<String>();
				varietyList = ahFuctionalService.getVarietye(permitDetails.getVarietycode());
				if (varietyList != null && varietyList.size() > 0) 
				{
					String varietyn=varietyList.get(0);
					variety=varietyn;
				}
				String season = permitDetails.getSeason();
				String ryotCode = permitDetails.getRyotcode();
				String ryotName = ryot.getRyotname();
				String relativeName = ryot.getRelativename();
				String villageCode = permitDetails.getLandvilcode();
				byte plantOrRatoon = permitDetails.getPlantorratoon();
				int varietyCode = permitDetails.getVarietycode();
				double extentSize = permitDetails.getExtentsize();
				int circleCode = permitDetails.getCirclecode();
				String landVillageCode = permitDetails.getLandvilcode();
				int zoneCode = permitDetails.getZonecode();
				String agreementNumber = permitDetails.getAgreementno();
				String plotNumber = permitDetails.getPlotno();
				Date permitDate = permitDetails.getPermitdate();
				int rank = permitDetails.getRank();
				
				jsonObj.put("season", season);
				jsonObj.put("ryotCode", ryotCode);
				jsonObj.put("ryotName", ryotName);
				jsonObj.put("relativeName", relativeName);
				jsonObj.put("villageCode", villageCode);
				jsonObj.put("plantOrRatoon", plantOrRatoon);
				jsonObj.put("varietyCode", varietyCode);
				jsonObj.put("extentSize", extentSize);
				jsonObj.put("circleCode", circleCode);
				jsonObj.put("landVillageCode", landVillageCode);
				jsonObj.put("zoneCode", zoneCode);
				jsonObj.put("agreementNumber", agreementNumber);
				jsonObj.put("plotNumber", plotNumber);
				jsonObj.put("relativeName", relativeName);
				jsonObj.put("village", villageName);
				jsonObj.put("rank", rank);  
				jsonObj.put("variety", variety); 
				jsonObj.put("circle", circleName);  
				jsonObj.put("permitNumber", permitnumber);  
				jsonObj.put("programNumber", programNo);  

				jArray.add(jsonObj);
				logger.info("========getPermitDetailsByPermitNumberNew()========== jArray--"+jArray);			
			}
			return jArray;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
            return null;
		}
	}

	private  PermitDetailsBean prepareBeanForPermitDetails(PermitDetails permitDetails,int permitnumber) 
	{
	
		List<Ryot> ryots=ryotService.getRyotByRyotCode(permitDetails.getRyotcode());
		Ryot ryot=ryots.get(0);
		//Modified By naidu 27-09-2016
		String circleName = "NA";
		 List<String> circleList = ahFuctionalService.getCirclePerGivenCircle(permitDetails.getCirclecode());
		 if (circleList != null && circleList.size() > 0) 
		 {
			 circleName = circleList.get(0);
		 }
		//Modified by DMurty on 06-07-2016
		String villageName = "NA";
		List<String> VillageList = new ArrayList<String>();
		VillageList = ahFuctionalService.getVillageDetailsByCode(ryot.getVillagecode());
		if (VillageList != null && VillageList.size() > 0) 
		{
			String myResult1 = VillageList.get(0);
			villageName = myResult1;
		}
				
		Variety variety=(Variety) commonService.getEntityById(permitDetails.getVarietycode(),Variety.class);
		
		PermitDetailsBean bean = new PermitDetailsBean();		

		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		bean.setPermitNumber(compositePrKeyForPermitDetails.getPermitnumber());
		bean.setProgramNumber(permitDetails.getCompositePrKeyForPermitDetails().getProgramno());

		bean.setPermitNumber(permitnumber);
		bean.setAgreementNumber(permitDetails.getAgreementno());
		bean.setRyotCode(permitDetails.getRyotcode());
		bean.setCircleCode(permitDetails.getCirclecode());
		bean.setExtentSize(permitDetails.getExtentsize());
		bean.setPlantOrRatoon(permitDetails.getPlantorratoon());
		bean.setVarietyCode(permitDetails.getVarietycode());
		bean.setSeason(permitDetails.getSeason());
		bean.setRyotName(ryot.getRyotname());
		bean.setRelativeName(ryot.getRelativename());
		bean.setVillageCode(ryot.getVillagecode());
		bean.setCircle(circleName);
		bean.setVariety(variety.getVariety());
		//Modified by DMurty on 06-07-2016
		bean.setVillage(villageName);
		bean.setLandVillageCode(permitDetails.getLandvilcode());
		bean.setPlotNumber(permitDetails.getPlotno());
		bean.setZoneCode(permitDetails.getZonecode());
		//Added by DMurty on 10-08-2016
		Date permitDate = permitDetails.getPermitdate();
		if(permitDate != null)
		{
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strtestDate = df.format(permitDate);
			bean.setPermitDate(strtestDate);
		}
		List<WeighmentDetails> weighmentDetails = ahFuctionalService.getLorryandcanewBySeasonandpnum(permitDetails.getSeason(),permitnumber);
		Integer slno=0;
		if(weighmentDetails != null && weighmentDetails.size()>0)
		{
			 slno = weighmentDetails.get(0).getSerialnumber();
		}
		bean.setSlno(slno);
		//added by naidu 16-12-2016
		bean.setVehicle(permitDetails.getVehicletype());
		bean.setVehicleNumber(permitDetails.getVehicleno());
		//bean.setVehicleType(permitDetails.getVehicletype());
		
		return bean;
	}
	
	@RequestMapping(value = "/getSecondPermitNo", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	int getSecondPermitNo(@RequestBody Integer permitNumber) throws Exception 
	{
		logger.info("========getSecondPermitNo()========== permitNumber--"+permitNumber);
		int secPermitNumber =  caneReceiptFunctionalService.getPermitNoForPartLoad(permitNumber);
		logger.info("========getSecondPermitNo()========== secPermitNumber--"+secPermitNumber);
		return secPermitNumber;
	}
	
	//Added by DMurty on 13-07-2016
	@RequestMapping(value = "/getValidateSerialNo", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean getValidateSerialNo(@RequestBody Integer serialnumber) throws Exception 
	{
		logger.info("========getValidateSerialNo()========== serialnumber--"+serialnumber);			
		String validationFor="SerialNo";
		boolean flag = false;
		byte status =  caneReceiptFunctionalService.getStatusForWeighment(serialnumber,validationFor);
		if(status == 1)
		{
			flag = true;
		}
		return flag;
	}
	
	//Added by DMurty on 13-07-2016
	@RequestMapping(value = "/getValidatePermitNo", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean getValidatePermitNo(@RequestBody Integer permitno) throws Exception 
	{
		logger.info("========getValidatePermitNo()========== permitno--"+permitno);			
		boolean flag = false;
		String validationFor="PermitNo";
		byte status =  caneReceiptFunctionalService.getStatusForWeighment(permitno,validationFor);
		if(status == 1)
		{
			flag = true;
		}
		logger.info("========getValidatePermitNo()========== flag--"+flag);			
		return flag;
	}

	@RequestMapping(value = "/getWeigmentDetailsBySerialNumber", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	WeighmentDetailsBean getWeigmentDetailsBySerialNumber(@RequestBody Integer serialnumber) throws Exception 
	{
		logger.info("========getWeigmentDetailsBySerialNumber()========== serialnumber--"+serialnumber);			
		WeighmentDetails weigmentDetails=(WeighmentDetails) commonService.getEntityById(serialnumber,WeighmentDetails.class);
		WeighmentDetailsBean weighmentDetailsBean = prepareBeanForWeigmentDetails(weigmentDetails);
		return weighmentDetailsBean;
	}
	
	private  WeighmentDetailsBean prepareBeanForWeigmentDetails(WeighmentDetails weigmentDetails) 
	{
		List<Ryot> ryots=ryotService.getRyotByRyotCode(weigmentDetails.getRyotcode());
		Ryot ryot=ryots.get(0);
		String circleName = "NA";
		List<String> circleList = ahFuctionalService.getCirclePerGivenCircle(weigmentDetails.getCirclecode());
		if (circleList != null && circleList.size() > 0) 
		{
			circleName = circleList.get(0);
		}
		String villageName = "NA";
		List<String> VillageList = new ArrayList<String>();
		VillageList = ahFuctionalService.getVillageDetailsByCode(ryot.getVillagecode());
		if (VillageList != null && VillageList.size() > 0) 
		{
			String myResult1 = VillageList.get(0);
			villageName = myResult1;
		}
		Variety variety=(Variety) commonService.getEntityById(weigmentDetails.getVarietycode(),Variety.class);
		
		WeighmentDetailsBean bean = new WeighmentDetailsBean();		
		bean.setPermitNumber(weigmentDetails.getPermitnumber());
		bean.setAgreementNumber(weigmentDetails.getAgreementnumber());
		bean.setRyotCode(weigmentDetails.getRyotcode());
		bean.setCircleCode(weigmentDetails.getCirclecode());
		bean.setVarietyCode(weigmentDetails.getVarietycode());
		bean.setRyotName(ryot.getRyotname());
		bean.setRelativeName(ryot.getRelativename());
		bean.setVillageCode(ryot.getVillagecode());
		bean.setCircle(circleName);
		bean.setVariety(variety.getVariety());
		//Modified by DMurty on 08-07-2016
		bean.setVillage(villageName);
		bean.setLandVillageCode(weigmentDetails.getLandvilcode());
		bean.setBurntCane(weigmentDetails.getBurntcane());
		bean.setRelativeName(weigmentDetails.getFhgname());
		bean.setVehicleType(weigmentDetails.getVehicletype());
		bean.setWeighmentNo(weigmentDetails.getWeighmentno());
		bean.setWeighBridgeSlNo(weigmentDetails.getWeighbridgeslno());
		bean.setPartLoad(weigmentDetails.getPartload());
		bean.setWeightmenType(weigmentDetails.getWeightmentype());
		bean.setSecondPermitNo(weigmentDetails.getSecondpermitno());
		bean.setUcCode(weigmentDetails.getUccode());
		bean.setSerialNo(weigmentDetails.getSerialnumber());
		bean.setPlantOrRatoon(weigmentDetails.getPlantorratoon());
		bean.setVehicle(weigmentDetails.getLorryortruck());
		bean.setVehicleNo(weigmentDetails.getVehicleno());
		bean.setSeason(weigmentDetails.getSeason());
		bean.setPlotNumber(weigmentDetails.getPlotnumber());
		//Added by DMurty on 19-08-2016
		Date weighmentDate = weigmentDetails.getCanereceiptdate();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String strweighmentDate = df.format(weighmentDate);
		bean.setWeighmentDate(strweighmentDate);
		//Added by DMurty on 16-11-2016
		CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
		int programNo =  caneReceiptFunctionalService.getProgramNo(weigmentDetails.getPermitnumber());
		compositePrKeyForPermitDetails.setProgramno(programNo);
		compositePrKeyForPermitDetails.setPermitnumber(weigmentDetails.getPermitnumber());
		
		PermitDetails pd=new PermitDetails();
		PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
		bean.setExtentSize(permitDetails.getExtentsize());
		
		return bean;
	}
	
	private String getWeighDatePerShift(String sdate,String stime)
	{
		logger.info("========getWeighDatePerShift()========== sdate--"+sdate);			
		logger.info("========getWeighDatePerShift()========== stime--"+stime);			
		String ddd=null;
		String timediff[]=stime.split(":");
		String datediff[]=sdate.split("-");
		Integer dd=Integer.parseInt(datediff[0])-1;
		Integer yyy=Integer.parseInt(datediff[2]);
		if(Integer.parseInt(timediff[0])<4)
		{
			Integer mth=Integer.parseInt(datediff[1])-1;
			if(dd==0)
			{
				
				if(mth==4 || mth==6 || mth==9 || mth==11)
				{
					ddd="30";
					//mth=mth-1;
				}
				 else if(mth==2)
				 {
					 if((yyy % 4 == 0) && (yyy % 100 != 0) || (yyy % 400 == 0))
					 {
						 ddd="29";
						 //mth=mth-1;
					 }
					 else
					 {
						 ddd="28";
						// mth=mth-1;
					 }
				 }
				else if(mth==0)
				{
					yyy=yyy-1;
					ddd="31";
					//mth=11;
				}
				else
					ddd="31";
				
				if(mth==0)
					mth=11;
				else
					mth=mth-1;
			}
			else if(dd<10)
			{
				ddd="0"+dd;
			}
			else
			{
				ddd=String.valueOf(dd);
			}
			if((mth+1)<10)	
			{
				
				String month="0"+(mth+1);
				sdate=ddd+"-"+month+"-"+yyy;
			}
			
			else
			{
				sdate=ddd+"-"+(mth+1)+"-"+yyy;
			}
		}
	
		logger.info("========getWeighDatePerShift()==========Returning sdate--"+sdate);			
		return sdate;
	}

	@RequestMapping(value = "/getWeigmentDetailsByShiftByDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getWeigmentDetailsByShiftByDate(@RequestBody JSONObject jsonData) throws Exception 
	{
		logger.info("========getWeigmentDetailsByShiftByDate()==========jsonData--"+jsonData);			
		String todayDate=(String) jsonData.get("caneWeighmentDate");
		String todayTime=(String) jsonData.get("caneWeighmentTime");
		todayDate=getWeighDatePerShift(todayDate,todayTime);
		List lss=new ArrayList();
	
		List shiftsls=caneReceiptFunctionalService.getShifts();
		Shift ssls=null;
		
		for(int j=0;j<shiftsls.size();j++)
		{
			ssls=(Shift)shiftsls.get(j);
			List canereceiptshiftsmryList=caneReceiptFunctionalService.getCaneReceiptShiftSmry(todayDate,ssls.getShiftid());
			logger.info("========getWeigmentDetailsByShiftByDate()==========canereceiptshiftsmryList--"+canereceiptshiftsmryList);
			if(canereceiptshiftsmryList !=null)
			{
				Map hm1=new HashMap();
				Map hm2=new HashMap();
				hm1=(Map)canereceiptshiftsmryList.get(0);
				
				int rsc2=(Integer)hm1.get("rs2");
				Double netwt=0.0;
				if((Double)hm1.get("netwt") != null)
				netwt=(Double)hm1.get("netwt");
				Integer rsc1=caneReceiptFunctionalService.GetSecondRec(todayDate,ssls.getShiftid());
				logger.info("=====second reciept num==========rec--"+rsc2);
				hm2.put("shiftid1", ssls.getShiftid());
				hm2.put("rsc1", rsc1);
				hm2.put("netwt", netwt);
				hm2.put("rsc2", rsc2);
				logger.info("========getWeigmentDetailsByShiftByDate()==========rec--"+hm2.toString());
				lss.add(hm2);
			}
			else
			{
				Map hm5=new HashMap();
				hm5.put("shiftid1", ssls.getShiftid());
				hm5.put("rsc1", 0);
				hm5.put("netwt", 0);
				hm5.put("rsc2", 0);
			}
		}
		return lss;
	}
	
	@RequestMapping(value = "/getShiftId", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getShiftId(@RequestBody String curtime) throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		boolean flag=false;
		List<Shift> shifts =shiftService.listShifts();
		Integer shiftId=0;
		for(int i=0;i<shifts.size();i++)
		{
			flag=DateUtils.isTimeBetweenTwoTime(shifts.get(i).getFromtime().toString(),shifts.get(i).getTotime().toString(),curtime);
			
			if(flag)
			{
				shiftId=shifts.get(i).getShiftid();
				break;
			}
		}
		response.put("ShiftId",shiftId);
		return response.toString();
	}
	
	@RequestMapping(value = "/getShiftIdNew", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getShiftIdNew(@RequestBody String curtime) throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		boolean flag=false;
		List<Shift> shifts =shiftService.listShifts();
		Integer shiftId=0;
		for(int i=0;i<shifts.size();i++)
		{
			flag=DateUtils.isTimeBetweenTwoTime(shifts.get(i).getFromtime().toString(),shifts.get(i).getTotime().toString(),curtime);
			
			if(flag)
			{
				shiftId=shifts.get(i).getShiftid();
				break;
			}
		}
		String shiftName = null;
		if(shiftId == 1)
		{
			shiftName = "A";
		}
		else if(shiftId == 2)
		{
			shiftName = "B";
		}
		else
		{
			shiftName = "C";
		}
		response.put("ShiftId",shiftName);
		return response.toString();
	}
	
	//Added by DMurty on 02-11-2016
	@RequestMapping(value = "/validateHarvestDateForPermitForWeighment", method = RequestMethod.POST)
	public @ResponseBody Integer validateHarvestDateForPermitForWeighment(@RequestBody JSONObject jsonData)throws Exception 
	{
		int permitNo = (Integer) jsonData.get("permitNo");
		logger.info("========validateHarvestDateForPermitForWeighment()========== permitNo "+permitNo);			
		
		int nRet = 1;
		List weighmentList = new ArrayList();
		weighmentList = ahFuctionalService.getWeighmentList(permitNo);
		if(weighmentList != null && weighmentList.size()>0)
		{
			nRet = 1;
		}
		else
		{
			nRet = ahFuctionalService.validateHarvestDateForPermitForWeighment(permitNo);
		}
		logger.info("Controller========validateHarvestDateForPermitForWeighment()========== nRet"+nRet);			
		return nRet;
	}
	
	@RequestMapping(value = "/getCaneweightment", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getCaneweightment() throws Exception 
	{
		logger.info("========getCaneweightment()==========canereceiptshiftsmryList--");			

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		
		String ipAddress = request.getRemoteAddr();

		//fileObj = new File("\\\\10.1.0.20\\uma\\myfile.txt");
		// fileObj = new File("\\\\192.168.1.171\\rs232\\myfile.txt");
		 fileObj = new File("\\\\"+ipAddress+"\\rs232\\myfile.txt");

         FileReader fr=new FileReader(fileObj);
         BufferedReader br = new BufferedReader(fr);                                                 
           
         // Read br and store a line in 'data', print data
         String json = null ;
         String data;
         data = br.readLine();
			
		jsonObj.put("DATA", data);
		jArray.add(jsonObj);
				
		logger.info("getCaneweightment() ========data=========="+data);
		return jArray;
	}
	
	@RequestMapping(value = "/saveWeighmentDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray saveWeighmentDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{
	
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		logger.info("========saveWeighmentDetails()==========Start Time-----"+currentTime);			

		logger.info("========saveWeighmentDetails()==========jsonData-----"+jsonData);			
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		boolean  isInsertTrue=false;
		String strQry = null;
		Object Qryobj = null;
		double totalCaneWeight = 0.0;
		String variety = null;
		int permitNo = 0;
		//Added by DMurty on 12-11-2016
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		List caneWeighmentTablesList=new ArrayList();
		List AccountsList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formData=(JSONObject) jsonData.get("formData");			
			logger.info("========formData==========" + formData);

			CaneWeighmentBean caneWeighmentBean = new ObjectMapper().readValue(formData.toString(), CaneWeighmentBean.class);
		
			Double bindingMaterial=0.0;
			List caneWeighnment=caneWeightService.getCaneWeight(caneWeighmentBean.getSeason());
			logger.info("saveWeighmentDetails()========caneWeighnment==========" + caneWeighnment);
			if(caneWeighnment != null && caneWeighnment!=null)
			{
				CaneWeight caneWeight=(CaneWeight)caneWeighnment.get(0);
				bindingMaterial=caneWeight.getBmpercentage();
			}
			logger.info("========bindingMaterial==========" + bindingMaterial);
		
			double totalWt = caneWeighmentBean.getTotalWeight()/1000;
			logger.info("saveWeighmentDetails()========caneWeighmentBean.getTotalWeight()=========="+caneWeighmentBean.getTotalWeight());
			logger.info("saveWeighmentDetails()========totalWt==========" + totalWt);
		
			
			int transactionCode = commonService.GetMaxTransCode(caneWeighmentBean.getSeason());
			logger.info("========transactionCode=========="+transactionCode);

			//Set 1 
			WeighmentDetails weighmentDetails = prepareModelForWeighmentDetails(caneWeighmentBean,bindingMaterial,transactionCode);
			if(caneWeighmentBean.getGrossTare().equalsIgnoreCase("1"))
			{
				//Added by DMurty on 01-08-2016
				String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
				String dt[] = date.split("-");
				String dd = dt[0];
				String mm = dt[1];
				String yy = dt[2];
				String finalDate = yy+"-"+mm+"-"+dd;
				logger.info("========saveWeighmentDetails()==========finalDate-----"+finalDate);			

				//Double netCaneWeight=weighmentDetails.getGrossweight() - (weighmentDetails.getGrossweight()*(bindingMaterial/100));
				//Double netWeightAfterSecondWeighment=netCaneWeight-caneWeighmentBean.getTotalWeight();
				Double grossCaneWeight=weighmentDetails.getGrossweight();
				Double netCaneWeight=weighmentDetails.getNetwt();
				Double lorryCaneWeight=weighmentDetails.getLorryandcanewt();
				Double lorryWeight=weighmentDetails.getLorrywt();
				Integer nVehicleType = weighmentDetails.getVehicletype();
				int partLoad = weighmentDetails.getPartload();
				
				//Added by DMurty on 29-12-2016
				totalCaneWeight = netCaneWeight;
				//variety
				int varietyCode = weighmentDetails.getVarietycode();
				List<Variety> varities=ahFuctionalService.getVarietyByVarietyCode(varietyCode);
				variety = varities.get(0).getVariety();
				permitNo = weighmentDetails.getPermitnumber();
				
				CaneReceiptShiftSmry caneReceiptShiftSmry=prepareModelForCaneReceiptShiftSmry(caneWeighmentBean,grossCaneWeight,netCaneWeight,bindingMaterial);
				CaneReceiptDaySmry caneReceiptDaySmry=prepareModelForCaneReceiptDaySmry(caneWeighmentBean,bindingMaterial,grossCaneWeight,netCaneWeight);
				CaneReceiptDaySmryByRyot caneReceiptDaySmryByRyot=prepareModelForCaneReceiptDaySmryByRyot(caneWeighmentBean,bindingMaterial,grossCaneWeight,netCaneWeight);
				CaneReceiptSeasonSmry caneReceiptSeasonSmry=prepareModelForCaneReceiptSeasonSmry(caneWeighmentBean,bindingMaterial,grossCaneWeight,netCaneWeight);
				CaneReceiptWBBook caneReceiptWBBook=prepareModelForCaneReceiptWBBook(caneWeighmentBean,bindingMaterial,grossCaneWeight,netCaneWeight);
				
				//Set 2 
				TransportAllowanceDetails transportAllowanceDetails=prepareModelForTransportAllowanceDetails(caneWeighmentBean,netCaneWeight);
				AdditionalCaneValue additionalCaneValue=prepareModelForAdditionalCaneValue(caneWeighmentBean,netCaneWeight);
				PostSeasonValueSummary postSeasonValueSummary=prepareModelForPostSeasonValueSummary(caneWeighmentBean,netCaneWeight);
				
				//Set 3		 	  
				HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate=prepareModelForHarvChgDtlsByRyotContAndDate(caneWeighmentBean,netCaneWeight);
				double harvestingRate = harvChgDtlsByRyotContAndDate.getRate();
				List harvChgDtlsByRyotContAndDateList=caneReceiptFunctionalService.getHarvChgDtlsByRyotContAndDate(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),harvestingRate,finalDate);
				if(harvChgDtlsByRyotContAndDateList != null && harvChgDtlsByRyotContAndDateList.size()>0)
				{
					double suppliedWt = harvChgDtlsByRyotContAndDate.getHarvestedcanewt();
					double amt = harvChgDtlsByRyotContAndDate.getTotalamount();
					
					strQry = "UPDATE HarvChgDtlsByRyotContAndDate set harvestedcanewt="+suppliedWt+",totalamount="+amt+" WHERE ryotcode ='"+caneWeighmentBean.getRyotCode()+"' and season='"+caneWeighmentBean.getSeason()+"' and receiptdate='"+finalDate+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
				else
				{
					caneWeighmentTablesList.add(harvChgDtlsByRyotContAndDate);				
				}
				HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont=prepareModelForHarvChgSmryByRyotAndCont(caneWeighmentBean,netCaneWeight);
				
				//Set 4 
				double roundedWt = netCaneWeight;
				
				if ( partLoad == 1 )
				{
					if (nVehicleType == 1 )
					{
						if (netCaneWeight < 10.0)
							roundedWt = 10.0;
					}
					else if (nVehicleType == 2 )
					{
						if (netCaneWeight < 17.0)
							roundedWt = 17.0;						
					}						
				}
				else
				{
					int vehicleTpe = caneWeighmentBean.getVehicleType();
					if(vehicleTpe >0)
					{
						int firstPermitNo = caneWeighmentBean.getPermitNumber();
						int secPermitNo = caneWeighmentBean.getSecondPermitNumber();
						
						List weighmentList = ahFuctionalService.getWeighmentListForSecondPermit(secPermitNo,caneWeighmentBean.getSeason());
						if(weighmentList != null && weighmentList.size()>0)
						{
							logger.info("saveWeighmentDetails()=======weighmentList==========" + weighmentList);
							Map weighmentMap = new HashMap();
							weighmentMap = (Map) weighmentList.get(0);
							
							double firstWeight = (Double) weighmentMap.get("netwt");
							String oldRyot = (String) weighmentMap.get("ryotcode");
							
							double secondWeight = netCaneWeight;
							double totalWeight = firstWeight+secondWeight;
							double cf = 0.0;
							if(vehicleTpe == 1)
							{
								cf = 10;
							}
							else
							{
								cf = 17;
							}
							double firstNewWeight = (firstWeight*cf)/totalWeight;
							//added by sir on 21-01-2017
							firstNewWeight = (double)Math.round(firstNewWeight*1000)/1000;
							firstNewWeight = Double.parseDouble(new DecimalFormat("##.###").format(firstNewWeight));
							
							//added by sir on 21-01-2017
							double secondNewWeight = (secondWeight*cf)/totalWeight;
							secondNewWeight = (double)Math.round(secondNewWeight*1000)/1000;
							secondNewWeight = Double.parseDouble(new DecimalFormat("##.###").format(secondNewWeight));
							
							roundedWt = secondNewWeight;
							
							double transportRate = 0.0;
							List transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),oldRyot,finalDate);
							if(transportingRateDetailsList!=null && transportingRateDetailsList.size()>0)
							{ 
								TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
								transportRate = transportingRateDetails.getRate();
								transportRate =Double.parseDouble(new DecimalFormat("##.##").format(transportRate));
							}
							
							double oldRyotTpAmt = transportRate*firstNewWeight;
							oldRyotTpAmt = (double)Math.round(oldRyotTpAmt*100)/100;
							oldRyotTpAmt =Double.parseDouble(new DecimalFormat("##.##").format(oldRyotTpAmt));
							
							double oldAmt = firstWeight*transportRate;
							oldAmt = (double)Math.round(oldAmt*100)/100;
							oldAmt =Double.parseDouble(new DecimalFormat("##.##").format(oldAmt));
							
							List transpChgSmryByRyotContAndDateList=caneReceiptFunctionalService.getTranspChgSmryByRyotContAndDate(caneWeighmentBean.getSeason(),oldRyot,transportRate,finalDate);
							if(transpChgSmryByRyotContAndDateList != null && transpChgSmryByRyotContAndDateList.size()>0)
							{
								TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate1 =(TranspChgSmryByRyotContAndDate)transpChgSmryByRyotContAndDateList.get(0);
								double totalAmt = transpChgSmryByRyotContAndDate1.getTotalamount();
								double finalTpAmount = totalAmt-oldAmt;
								finalTpAmount = finalTpAmount+oldRyotTpAmt;
								finalTpAmount = (double)Math.round(finalTpAmount*100)/100;
								finalTpAmount =Double.parseDouble(new DecimalFormat("##.##").format(finalTpAmount));
								
								int id = transpChgSmryByRyotContAndDate1.getId();
								
								String qry = "Update TranspChgSmryByRyotContAndDate set totalamount="+finalTpAmount+",mincanewt="+firstNewWeight+" where id="+id;
								logger.info("saveWeighmentDetails()=======Qry==========" + qry);
								Qryobj = qry;
								UpdateList.add(Qryobj);
							}
						
							List transpChgSmryByRyotAndContList=caneReceiptFunctionalService.getTranspChgSmryByRyotAndCont(caneWeighmentBean.getSeason(),oldRyot);
							if(transpChgSmryByRyotAndContList != null && transpChgSmryByRyotAndContList.size()>0)
							{
								TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont=(TranspChgSmryByRyotAndCont)transpChgSmryByRyotAndContList.get(0);
								
								int id = transpChgSmryByRyotAndCont.getId();
								
								double totalAmt = transpChgSmryByRyotAndCont.getTotalamount();
								double finalTpAmount = totalAmt-oldAmt;
								finalTpAmount = finalTpAmount+oldRyotTpAmt;
								finalTpAmount = (double)Math.round(finalTpAmount*100)/100;
								finalTpAmount =Double.parseDouble(new DecimalFormat("##.##").format(finalTpAmount));

								double pendingAmount = transpChgSmryByRyotAndCont.getPendingamount();
								double finalpendingAmount = pendingAmount-oldAmt;
								finalpendingAmount = finalpendingAmount+oldRyotTpAmt;
								finalpendingAmount = (double)Math.round(finalpendingAmount*100)/100;
								finalpendingAmount =Double.parseDouble(new DecimalFormat("##.##").format(finalpendingAmount));

								String Qry = "Update TranspChgSmryByRyotAndCont set totalamount="+finalTpAmount+",pendingamount="+finalpendingAmount+",mincanewt="+firstNewWeight+" where id="+id;
								logger.info("saveWeighmentDetails()=======Qry==========" + Qry);
								Qryobj = Qry;
								UpdateList.add(Qryobj);
							}
						}
					}
				}
				
				TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate=prepareModelForTranspChgSmryByRyotContAndDate(caneWeighmentBean,netCaneWeight, roundedWt);
				double transportRate = transpChgSmryByRyotContAndDate.getRate();
				List transpChgSmryByRyotContAndDateList=caneReceiptFunctionalService.getTranspChgSmryByRyotContAndDate(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),transportRate,finalDate);
				
				if(transpChgSmryByRyotContAndDateList!=null && transpChgSmryByRyotContAndDateList.size()>0)
				{
					double suppliedWt = transpChgSmryByRyotContAndDate.getTransportedcanewt();						
					double amt = transpChgSmryByRyotContAndDate.getTotalamount();
					double RoundedAmt = transpChgSmryByRyotContAndDate.getMincanewt();
					strQry = "UPDATE TranspChgSmryByRyotContAndDate set mincanewt="+RoundedAmt+",transportedcanewt="+suppliedWt+",totalamount="+amt+" WHERE ryotcode ='"+caneWeighmentBean.getRyotCode()+"' and season='"+caneWeighmentBean.getSeason()+"' and receiptdate='"+finalDate+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
				else
				{
					caneWeighmentTablesList.add(transpChgSmryByRyotContAndDate);
				}
				TranspChgSmryByRyotAndCont transpChgSmryByRyotAndCont=prepareModelForTranspChgSmryByRyotAndCont(caneWeighmentBean,netCaneWeight,roundedWt);
				
				//Set 5   
				ICPDetailsByRyotAndDate iCPDetailsByRyotAndDate=prepareModelForICPDetailsByRyotAndDate(caneWeighmentBean,netCaneWeight);
				ICPDetailsByRyot iCPDetailsByRyot=prepareModelForICPDetailsByRyot(caneWeighmentBean,netCaneWeight);
				//SubsidyDetailsByRotAndDate subsidyDetailsByRotAndDate=prepareModelForSubsidyDetailsByRotAndDate(caneWeighmentBean,netCaneWeight);
				//SubsidyDetailsByRyot subsidyDetailsByRyot=prepareModelForSubsidyDetailsByRyot(caneWeighmentBean,netCaneWeight);
				CDCDetailsByRyot cDCDetailsByRyot=prepareModelForCDCDetailsByRyot(caneWeighmentBean,netCaneWeight);
				CDCDetailsByRotAndDate cDCDetailsByRotAndDate=prepareModelForCDCDetailsByRotAndDate(caneWeighmentBean,netCaneWeight);
				UCDetailsByRotAndDate uCDetailsByRotAndDate=prepareModelForUCDetailsByRotAndDate(caneWeighmentBean,netCaneWeight);
				UCDetailsByRyot uCDetailsByRyot=prepareModelForUCDetailsByRyot(caneWeighmentBean,netCaneWeight);
				 
				List caneValueDtlsByRyotAndDateList=caneReceiptFunctionalService.CaneValueDetailsByRyotAndDateList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
				if(caneValueDtlsByRyotAndDateList!=null && caneValueDtlsByRyotAndDateList.size()>0)
				{
					List caneValueDetailsByRyotAndDateList = prepareModefForCaneValueDetailsByRyotAndDate(caneWeighmentBean,netCaneWeight);
					if(caneValueDetailsByRyotAndDateList != null && caneValueDetailsByRyotAndDateList.size()>0)
					{
						for(int i = 0;i<caneValueDetailsByRyotAndDateList.size();i++)
						{
							UpdateList.add(caneValueDetailsByRyotAndDateList.get(i));
						}
					}
				}
				else
				{
					List caneValueDetailsByRyotAndDateList = prepareModefForCaneValueDetailsByRyotAndDate(caneWeighmentBean,netCaneWeight);
					if(caneValueDetailsByRyotAndDateList != null && caneValueDetailsByRyotAndDateList.size()>0)
					{
						for(int i = 0;i<caneValueDetailsByRyotAndDateList.size();i++)
						{
							caneWeighmentTablesList.add(caneValueDetailsByRyotAndDateList.get(i));
						}
					}
				}
				
				List CaneValDtlsByRyot = caneReceiptFunctionalService.CaneValueDetailsByRyotList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
				if(CaneValDtlsByRyot != null && CaneValDtlsByRyot.size()>0)
				{
					//Update
					List caneValueDetailsByRyotList = prepareModelForCaneValueDetailsByRyot(caneWeighmentBean,netCaneWeight);
					if(caneValueDetailsByRyotList != null && caneValueDetailsByRyotList.size()>0)
					{
						for(int i = 0;i<caneValueDetailsByRyotList.size();i++)
						{
							UpdateList.add(caneValueDetailsByRyotList.get(i));
						}
					}
				}
				else
				{
					//Insert
					List caneValueDetailsByRyotList = prepareModelForCaneValueDetailsByRyot(caneWeighmentBean,netCaneWeight);
					if(caneValueDetailsByRyotList != null && caneValueDetailsByRyotList.size()>0)
					{
						for(int i = 0;i<caneValueDetailsByRyotList.size();i++)
						{
							caneWeighmentTablesList.add(caneValueDetailsByRyotList.get(i));
						}
					}
				}
					
				List AcrdAndDueCaneValSmryList = caneReceiptFunctionalService.getAccruedAndDueCaneValueSummaryList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
				
				if(AcrdAndDueCaneValSmryList != null && AcrdAndDueCaneValSmryList.size()>0)
				{
					List AccruedAndDueCaneValueSummaryList = prepareModelForAccruedAndDueCaneValueSummary(caneWeighmentBean,netCaneWeight);
					if(AccruedAndDueCaneValueSummaryList != null && AccruedAndDueCaneValueSummaryList.size()>0)
					{
						for(int i = 0;i<AccruedAndDueCaneValueSummaryList.size();i++)
						{
							UpdateList.add(AccruedAndDueCaneValueSummaryList.get(i));
						}
					}
				}
				else
				{
					List AccruedAndDueCaneValueSummaryList = prepareModelForAccruedAndDueCaneValueSummary(caneWeighmentBean,netCaneWeight);
					if(AccruedAndDueCaneValueSummaryList != null && AccruedAndDueCaneValueSummaryList.size()>0)
					{
						for(int i = 0;i<AccruedAndDueCaneValueSummaryList.size();i++)
						{
							caneWeighmentTablesList.add(AccruedAndDueCaneValueSummaryList.get(i));
						}
					}
				}
				
				//Here we need to post Accounting
				//Added by DMurty on 28-07-2016
				double frp = 0.0;
				double purchaseTax = 0.0;
				double harvestingSubsidy = 0.0;
				double tpSubsidy = 0.0;
				double addCp = 0.0;
				double allwncePerKm = 0.0;
				double netCanePrice = 0.0;

				//Added by DMurty on 31-10-2016
				int isFrpPurTx = 0;
				int isPurTx = 0;
				int isHarPurTx = 0;
				int isTpPurTx = 0;
				int isAddCpPurTx = 0;
				
				List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
				if(caneAcctRules != null && caneAcctRules.size()>0)
				{
					int calcid = caneAcctRules.get(0).getCalcid();
					allwncePerKm = caneAcctRules.get(0).getAllowanceperkmperton();
					netCanePrice = caneAcctRules.get(0).getNetcaneprice();
					
					List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
					frp = caneAcctAmounts.get(0).getNetamt();
					isFrpPurTx = caneAcctAmounts.get(0).getIsthispurchasetax();
					
					List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
					purchaseTax = purcaneAcctAmounts.get(0).getAmount();
					isPurTx = purcaneAcctAmounts.get(0).getIsthispurchasetax();

					List <CaneAcctAmounts> HscaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Harvesting Subsidy");
					harvestingSubsidy = HscaneAcctAmounts.get(0).getAmount();
					isHarPurTx = HscaneAcctAmounts.get(0).getIsthispurchasetax();
					
					List <CaneAcctAmounts> TpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Transport Subsidy");
					tpSubsidy = TpcaneAcctAmounts.get(0).getAmount();
					isTpPurTx = TpcaneAcctAmounts.get(0).getIsthispurchasetax();
					
					List <CaneAcctAmounts> addCpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
					addCp = addCpcaneAcctAmounts.get(0).getAmount();
					isAddCpPurTx = addCpcaneAcctAmounts.get(0).getIsthispurchasetax();
				}
				List AmtList = new ArrayList();
				
				double finalfrp = frp*netCaneWeight;
				finalfrp = (double)Math.round(finalfrp*100)/100;
				
				double finalpurtax = purchaseTax*netCaneWeight;
				finalpurtax = (double)Math.round(finalpurtax*100)/100;
				
				double finalharvestingSubsidy = harvestingSubsidy*netCaneWeight;
				finalharvestingSubsidy = (double)Math.round(finalharvestingSubsidy*100)/100;
				
				double finalTpSubsidy = tpSubsidy*netCaneWeight;
				finalTpSubsidy = (double)Math.round(finalTpSubsidy*100)/100;
				
				double finalAddCp = addCp*netCaneWeight;
				finalAddCp = (double)Math.round(finalAddCp*100)/100;
				
				finalfrp =Double.parseDouble(new DecimalFormat("##.##").format(finalfrp));
				finalpurtax =Double.parseDouble(new DecimalFormat("##.##").format(finalpurtax));
				finalharvestingSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(finalharvestingSubsidy));
				finalTpSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(finalTpSubsidy));
				finalAddCp =Double.parseDouble(new DecimalFormat("##.##").format(finalAddCp));
				
				
				String strfinalfrp = HMSDecFormatter.format(frp);
				String strfinalpurtax = HMSDecFormatter.format(purchaseTax);
				String strfinalharvestingSubsidy = HMSDecFormatter.format(harvestingSubsidy);
				String strfinalTpSubsidy = HMSDecFormatter.format(tpSubsidy);
				String strfinalAddCp = HMSDecFormatter.format(addCp);

				List<Ryot> ryot = ryotService.getRyotByRyotCode(caneWeighmentBean.getRyotCode());
				byte ryotType = ryot.get(0).getRyottype();
				if(finalfrp>0)
				{
					if(isFrpPurTx == 0)
					{
						Map tempMap = new HashMap();
						tempMap.put("AMOUNT", finalfrp);
						tempMap.put("JOURNAL_MEMO", "To Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalfrp+" FRP");
						tempMap.put("ACCOUNT_CODE", "FRP");
						tempMap.put("TRANS_TYPE", "Dr");
						tempMap.put("GL_CODE",caneWeighmentBean.getRyotCode());
						AmtList.add(tempMap);
						
						Map tempMap1 = new HashMap();
						String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalfrp+" FRP";
						tempMap1.put("AMOUNT", finalfrp);
						tempMap1.put("JOURNAL_MEMO", jrnlMemo);
						tempMap1.put("ACCOUNT_CODE", caneWeighmentBean.getRyotCode());
						tempMap1.put("TRANS_TYPE", "Cr");
						tempMap1.put("GL_CODE","FRP");
						AmtList.add(tempMap1);
					}
				}
				if(finalpurtax>0)
				{
					if(ryotType>0)
					{
						if(isPurTx == 0)
						{
							Map tempMap = new HashMap();
							tempMap.put("AMOUNT", finalpurtax);
							tempMap.put("JOURNAL_MEMO", "To Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalpurtax+" Purchase Tax");
							tempMap.put("ACCOUNT_CODE", "CANEPURTAX");
							tempMap.put("TRANS_TYPE", "Cr");
							tempMap.put("GL_CODE",caneWeighmentBean.getRyotCode());
							AmtList.add(tempMap);
						}
					}
				}
				if(finalharvestingSubsidy>0)
				{
					if(ryotType>0)
					{
						if(isHarPurTx == 0)
						{
							Map tempMap = new HashMap();
							tempMap.put("AMOUNT", finalharvestingSubsidy);
							tempMap.put("JOURNAL_MEMO", "To Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalharvestingSubsidy+" Harvesting Subsidy");
							tempMap.put("ACCOUNT_CODE", "HARVESTSUBSIDY");
							tempMap.put("TRANS_TYPE", "Dr");
							tempMap.put("GL_CODE",caneWeighmentBean.getRyotCode());
							AmtList.add(tempMap);
							
							Map tempMap1 = new HashMap();
							String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalharvestingSubsidy+" Harvesting Subsidy";
							tempMap1.put("AMOUNT", finalharvestingSubsidy);
							tempMap1.put("JOURNAL_MEMO", jrnlMemo);
							tempMap1.put("ACCOUNT_CODE", caneWeighmentBean.getRyotCode());
							tempMap1.put("TRANS_TYPE", "Cr");
							tempMap1.put("GL_CODE","HARVESTSUBSIDY");
							AmtList.add(tempMap1);
						}
					}
				}
				if(finalTpSubsidy>0)
				{
					if(ryotType>0)
					{
						if(isTpPurTx == 0)
						{
							Map tempMap = new HashMap();
							tempMap.put("AMOUNT", finalTpSubsidy);
							tempMap.put("JOURNAL_MEMO", "To Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalTpSubsidy+" Transport Subsidy");
							tempMap.put("ACCOUNT_CODE", "TPSUBSIDY");
							tempMap.put("TRANS_TYPE", "Dr");
							tempMap.put("GL_CODE",caneWeighmentBean.getRyotCode());
							AmtList.add(tempMap);
							
							Map tempMap1 = new HashMap();
							String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalTpSubsidy+" Transport Subsidy";
							tempMap1.put("AMOUNT", finalTpSubsidy);
							tempMap1.put("JOURNAL_MEMO", jrnlMemo);
							tempMap1.put("ACCOUNT_CODE", caneWeighmentBean.getRyotCode());
							tempMap1.put("TRANS_TYPE", "Cr");
							tempMap1.put("GL_CODE","TPSUBSIDY");
							AmtList.add(tempMap1);
						}
					}
				}
				if(finalAddCp>0)
				{
					if(ryotType>0)
					{
						if(isAddCpPurTx == 0)
						{
							Map tempMap = new HashMap();
							tempMap.put("AMOUNT", finalAddCp);
							tempMap.put("JOURNAL_MEMO", "To Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalAddCp+" Additional Cane Price");
							tempMap.put("ACCOUNT_CODE", "ACP");
							tempMap.put("TRANS_TYPE", "Dr");
							tempMap.put("GL_CODE",caneWeighmentBean.getRyotCode());
							AmtList.add(tempMap);
							
							Map tempMap1 = new HashMap();
							String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strfinalAddCp+" Additional Cane Price";
							tempMap1.put("AMOUNT", finalAddCp);
							tempMap1.put("JOURNAL_MEMO", jrnlMemo);
							tempMap1.put("ACCOUNT_CODE", caneWeighmentBean.getRyotCode());
							tempMap1.put("TRANS_TYPE", "Cr");
							tempMap1.put("GL_CODE","ACP");
							AmtList.add(tempMap1);
						}
					}
				}
				
				//cDCDetailsByRyot
				//double cdcAmt = cDCDetailsByRyot.getCdcamount();
				double cdcRate = cDCDetailsByRyot.getCdcrate();
				double cdcAmt = cdcRate*netCaneWeight;
				cdcAmt =Double.parseDouble(new DecimalFormat("##.##").format(cdcAmt));
				String strcdcRate = HMSDecFormatter.format(cdcRate);
				if(cdcAmt>0)
				{
					Map tempMap1 = new HashMap();
					String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strcdcRate+" CDC";
					tempMap1.put("AMOUNT", cdcAmt);
					tempMap1.put("JOURNAL_MEMO", jrnlMemo);
					//CDC Modified to 1186 by DMurty on 25-11-2016 according to their glcode for cdc.
					tempMap1.put("ACCOUNT_CODE","1186");
					tempMap1.put("TRANS_TYPE", "Cr");
					tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
					AmtList.add(tempMap1);
				}
				//uCDetailsByRyot
				//double ulAmt = uCDetailsByRyot.getUdcamount();
				double ulRate = uCDetailsByRyot.getUdcrate();
				double ulAmt = ulRate*netCaneWeight;
				ulAmt =Double.parseDouble(new DecimalFormat("##.##").format(ulAmt));

				String strulRate = HMSDecFormatter.format(ulRate);
				if(ulAmt>0)
				{
					Map tempMap1 = new HashMap();
					String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strulRate+" UL";
					tempMap1.put("AMOUNT", ulAmt);
					tempMap1.put("JOURNAL_MEMO", jrnlMemo);
					tempMap1.put("ACCOUNT_CODE","UL");
					tempMap1.put("TRANS_TYPE", "Cr");
					tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
					AmtList.add(tempMap1);
				}
				//harvChgDtlsByRyotContAndDate
				String accCode = null;
				//double harvestAmt = harvChgDtlsByRyotContAndDate.getTotalamount();
				double harvestRate = harvChgDtlsByRyotContAndDate.getRate();
				double harvestAmt = harvestRate*netCaneWeight;
				harvestAmt =Double.parseDouble(new DecimalFormat("##.##").format(harvestAmt));

				String strharvestRate = HMSDecFormatter.format(harvestRate);
				int harvestContCode = harvChgDtlsByRyotContAndDate.getHarvestcontcode();
				List detailsList = caneAccountingFunctionalService.getAccountCode(harvestContCode,"HarvestingContractors","harvestercode");
				if(detailsList != null && detailsList.size()>0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) detailsList.get(0);
					accCode = (String) tempMap.get("accountcode");
				}	
				if(harvestAmt>0)
				{
					if(accCode != null)
					{
						Map tempMap1 = new HashMap();
						String jrnlMemo = "By Cane Supply "+netCaneWeight+ " tons @ Rs."+strharvestRate+" Harvesting Contractor";
						tempMap1.put("AMOUNT", harvestAmt);
						tempMap1.put("JOURNAL_MEMO", jrnlMemo);
						tempMap1.put("ACCOUNT_CODE",accCode);
						tempMap1.put("TRANS_TYPE", "Cr");
						tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
						AmtList.add(tempMap1);
					}
				}
				
				//Added by DMurty on 25-11-2016 for posting in Cane Purchases Account
				double totalCanePrice = netCanePrice*netCaneWeight;
				totalCanePrice =Double.parseDouble(new DecimalFormat("##.##").format(totalCanePrice));

				String strnetCanePrice = HMSDecFormatter.format(netCanePrice);
				if(totalCanePrice>0)
				{
					accCode = "4041";
					String jrnlMemo = "Towards Cane Purchases "+netCaneWeight+ " tons @ Rs."+strnetCanePrice+" ";
					Map tempMap1 = new HashMap();
					tempMap1.put("AMOUNT", totalCanePrice);
					tempMap1.put("JOURNAL_MEMO", jrnlMemo);
					tempMap1.put("ACCOUNT_CODE",accCode);
					tempMap1.put("TRANS_TYPE", "Dr");
					tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
					AmtList.add(tempMap1);
				}
				
				accCode = null;
				double tpRate = transpChgSmryByRyotContAndDate.getRate();
				double tpAmt = tpRate*netCaneWeight;
				tpAmt =Double.parseDouble(new DecimalFormat("##.##").format(tpAmt));

				String strtpRate = HMSDecFormatter.format(tpRate);
				int tpContractor = transpChgSmryByRyotContAndDate.getTransportcontcode();
				detailsList = caneAccountingFunctionalService.getAccountCode(tpContractor,"TransportingContractors","transportercode");
				if(detailsList != null && detailsList.size()>0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) detailsList.get(0);
					accCode = (String) tempMap.get("accountcode");
				}	
				if(tpAmt>0)
				{
					if(accCode != null)
					{
						Map tempMap1 = new HashMap();
						String jrnlMemo = "By Cane Supply "+roundedWt+ " tons @ Rs."+strtpRate+" Transport Contractor";
						tempMap1.put("AMOUNT", tpAmt);
						tempMap1.put("JOURNAL_MEMO", jrnlMemo);
						tempMap1.put("ACCOUNT_CODE",accCode);
						tempMap1.put("TRANS_TYPE", "Cr");
						tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
						AmtList.add(tempMap1);
					}
				}
				
				//Added by DMurty on 18-11-2016
				if(transportAllowanceDetails != null)
				{
					double distance = 0.0;
					if(transportAllowanceDetails.getDistance() != null)
					{
						distance = transportAllowanceDetails.getDistance();
					}
					
					double caneWeight = netCaneWeight;
					if(distance > 0)
					{
						double transportAlwnce = allwncePerKm*distance*caneWeight;
						transportAlwnce = (double)Math.round(transportAlwnce*100)/100;
						transportAlwnce =Double.parseDouble(new DecimalFormat("##.##").format(transportAlwnce));

						accCode = "TPSUBSIDY";
						Map tempMap1 = new HashMap();
						String jrnlMemo = "Towards Transport Subsidy";
						tempMap1.put("AMOUNT", transportAlwnce);
						tempMap1.put("JOURNAL_MEMO", jrnlMemo);
						tempMap1.put("ACCOUNT_CODE",accCode);
						tempMap1.put("TRANS_TYPE", "Dr");
						tempMap1.put("GL_CODE",caneWeighmentBean.getRyotCode());
						AmtList.add(tempMap1);
						
						accCode = caneWeighmentBean.getRyotCode();
						Map tempMap = new HashMap();
						tempMap.put("AMOUNT", transportAlwnce);
						tempMap.put("JOURNAL_MEMO", "Towards Transport Subsidy");
						tempMap.put("ACCOUNT_CODE", accCode);
						tempMap.put("TRANS_TYPE", "Cr");
						tempMap.put("GL_CODE","TPSUBSIDY");
						AmtList.add(tempMap);
					}
				}
				
				String season = caneWeighmentBean.getSeason();
				logger.info("========AmtList==========" + AmtList);
				logger.info("========AmtList==========" + AmtList.size());
				if(AmtList != null && AmtList.size()>0)
				{
					for(int i = 0;i<AmtList.size();i++)
					{
						logger.info("saveWeighmentDetails()========i==========" + i);
						Map amtMap = new HashMap();
						amtMap = (Map) AmtList.get(i);
						logger.info("========amtMap==========" + amtMap);

						double finalAmtToPost = (Double) amtMap.get("AMOUNT");
						Double Amt = finalAmtToPost;
						String JrnlMemo = (String) amtMap.get("JOURNAL_MEMO");
						String straccountCode = (String) amtMap.get("ACCOUNT_CODE");
						String transType = (String) amtMap.get("TRANS_TYPE");
						String glCode = (String) amtMap.get("GL_CODE");
						String accountCode = straccountCode;
						accCode = straccountCode;
						Map DtlsMap = new HashMap();
						
						DtlsMap.put("TRANSACTION_CODE", transactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						//added by sahadeva 28/10/2016
						DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						DtlsMap.put("ACCOUNT_CODE", accountCode);
						DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
						DtlsMap.put("TRANSACTION_TYPE", transType);
						//Added by DMurty on 18-10-2016
						DtlsMap.put("SEASON", caneWeighmentBean.getSeason());
						DtlsMap.put("GL_CODE", glCode);
						
						logger.info("========DtlsMap==========" + DtlsMap);

						AccountDetails accountDetails =  prepareModelForAccountDetails(DtlsMap);
						caneWeighmentTablesList.add(accountDetails);

						String TransactionType = transType;
						AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,caneWeighmentBean.getSeason());
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(accountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(accountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
							}
						}
						
						// Insert Rec in Acc Grp Details
						int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
						String strAccGrpCode = Integer.toString(accGrpCode);

						Map AccGrpDtlsMap = new HashMap();

						AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						//added by sahadeva 28/10/2016
						AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
						AccGrpDtlsMap.put("TRANSACTION_TYPE", transType);
						//Added by DMurty on 18-10-2016
						AccGrpDtlsMap.put("SEASON", caneWeighmentBean.getSeason());
						logger.info("========AccGrpDtlsMap==========" + AccGrpDtlsMap);

						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						caneWeighmentTablesList.add(accountGroupDetails);
						
						TransactionType = transType;
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType, caneWeighmentBean.getSeason());
						
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", accGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(accGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						Map AccSubGrpDtlsMap = new HashMap();

						AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", transType);
						AccSubGrpDtlsMap.put("SEASON", caneWeighmentBean.getSeason());
						logger.info("========AccSubGrpDtlsMap==========" + AccSubGrpDtlsMap);

						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

						
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						caneWeighmentTablesList.add(accountSubGroupDetails);
						
						TransactionType = transType; 
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,caneWeighmentBean.getSeason());
						
						Map AccSubGrpSmry = new HashMap();
						//AccSubGrpSmry.put("ACC_GRP_CODE", accGrpCode);
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(accGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
				logger.info("========After Accounting Postings==========");

				//Added by DMurty on 12-11-2016
				Iterator entries = AccountSummaryMap.entrySet().iterator();
				while (entries.hasNext())
				{
				    Map.Entry entry = (Map.Entry) entries.next();
				    Object value = entry.getValue();
				    AccountSummary accountSummary = (AccountSummary) value;
				    
				    String accountCode = accountSummary.getAccountcode();
				    String sesn = accountSummary.getSeason();
				    double runnbal = accountSummary.getRunningbalance();
				    String crOrDr = accountSummary.getBalancetype();
				    
				    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
				    if(AccList != null && AccList.size()>0)
				    {
				    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
				    	Qryobj = qry;
				    	UpdateList.add(Qryobj);
				    }
				    else
				    {
				    	AccountsList.add(accountSummary);
				    }
				}
				
				Iterator entries1 = AccountGroupMap.entrySet().iterator();
				while (entries1.hasNext())
				{
				    Map.Entry entry = (Map.Entry) entries1.next();
				    Object value = entry.getValue();
				    
				    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				    String sesn = accountGroupSummary.getSeason();
				    double runnbal = accountGroupSummary.getRunningbalance();
				    String crOrDr = accountGroupSummary.getBalancetype();
				    
				    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
				    if(AccList != null && AccList.size()>0)
				    {
				    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
				    	Qryobj = qry;
				    	UpdateList.add(Qryobj);
				    }
				    else
				    {
				    	AccountsList.add(accountGroupSummary);
				    }
				}
				
				Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
				while (entries2.hasNext())
				{
				    Map.Entry entry = (Map.Entry) entries2.next();
				    Object value = entry.getValue();
				    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
				   
				    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				    String sesn = accountSubGroupSummary.getSeason();
				    double runnbal = accountSubGroupSummary.getRunningbalance();
				    String crOrDr = accountSubGroupSummary.getBalancetype();
				    
				    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
				    if(AccList != null && AccList.size()>0)
				    {
				    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
				    	Qryobj = qry;
				    	UpdateList.add(Qryobj);
				    }
				    else
				    {
				    	AccountsList.add(accountSubGroupSummary);
				    }
				}
				logger.info("========After Accounting Postings=========="+AccountsList);

				caneWeighmentTablesList.add(caneReceiptShiftSmry);
				caneWeighmentTablesList.add(caneReceiptDaySmry);
				caneWeighmentTablesList.add(caneReceiptDaySmryByRyot);			
				caneWeighmentTablesList.add(caneReceiptSeasonSmry);
				caneWeighmentTablesList.add(caneReceiptWBBook);
				
				if(transportAllowanceDetails!=null)
					caneWeighmentTablesList.add(transportAllowanceDetails);
				if(additionalCaneValue != null)
				caneWeighmentTablesList.add(additionalCaneValue);
				caneWeighmentTablesList.add(postSeasonValueSummary);			
				caneWeighmentTablesList.add(harvChgSmryByRyotAndCont);
				caneWeighmentTablesList.add(transpChgSmryByRyotAndCont);
				caneWeighmentTablesList.add(iCPDetailsByRyotAndDate);
				caneWeighmentTablesList.add(iCPDetailsByRyot);
				caneWeighmentTablesList.add(cDCDetailsByRyot);
				caneWeighmentTablesList.add(cDCDetailsByRotAndDate);			
				caneWeighmentTablesList.add(uCDetailsByRotAndDate);
				caneWeighmentTablesList.add(uCDetailsByRyot);	
				
				strQry = "UPDATE PermitDetails set checkinstatus=2 WHERE permitnumber="+caneWeighmentBean.getPermitNumber()+" and season='"+caneWeighmentBean.getSeason()+"'";
				logger.info("========strQry==========" + strQry);
				Qryobj = strQry;
				UpdateList.add(Qryobj);
			}
			caneWeighmentTablesList.add(weighmentDetails);
			
			logger.info("========caneWeighmentTablesList==========" + caneWeighmentTablesList);
			logger.info("========UpdateList==========" + UpdateList);
			isInsertTrue = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(caneWeighmentTablesList,UpdateList,AccountsList);
			logger.info("========isInsertTrue==========" + isInsertTrue);
		
			currentTime = cal.getTime();
			logger.info("========saveWeighmentDetails()==========End Time-----"+currentTime);			

			//Modified by Sahu on 01-08-2016 
			if(isInsertTrue == true)
			{
				Integer sNo=caneWeighmentBean.getSerialNumber();
				int actualSlNo = caneReceiptFunctionalService.getActualSlNoBySerialNumber(sNo,caneWeighmentBean.getSeason());
				
				jsonObj.put("isInsertTrue", "true");
				jsonObj.put("serailNumber",sNo);
				jsonObj.put("actualSlNo",actualSlNo);
				jsonObj.put("season",caneWeighmentBean.getSeason());
				jsonObj.put("isTare",caneWeighmentBean.getGrossTare());
				if(actualSlNo == 0)
				{
					int lorryStatus = 0;
					jsonObj.put("lorryStatus", lorryStatus);
				}
				else
				{
					int lorryStatus = 1;
					jsonObj.put("lorryStatus", lorryStatus);
				}
				jArray.add(jsonObj);
				
				if(actualSlNo > 0)
				{
					if(totalCaneWeight >0)
					{
						//String strUserName = "FINSOL";
						List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(caneWeighmentBean.getRyotCode());
						String mobilenumber = ryot.get(0).getMobilenumber();
						logger.info("========saveWeighmentDetails()========== mobilenumber" + mobilenumber);
						if (!("".equals(mobilenumber)) && !(mobilenumber == null) && !("null".equals(mobilenumber)) && !("NA".equals(mobilenumber)))
						{
							String postData = "";
							String retval = "";
							String message = "";
							
							String User = "sarvar";
							String passwd = "sarvar";
							String sid = "SSSLTD";
							String mtype = "LNG";
							String DR = "Y";
														
							boolean bRet=false;
							
							String dt = caneWeighmentBean.getCaneWeighmentDate();
							int pno = caneWeighmentBean.getPermitNumber();
							String vNo = caneWeighmentBean.getVehicleNumber();
							
							message = "తేదీ "+dt+" న పర్మిట్ నెంబరు "+pno+" సంభందించి "+totalCaneWeight+" టన్నుల చెరుకును లారీ/ట్రాక్టరు నెంబరు "+vNo+" ద్వారా ఫ్యాక్టరీ కి సరఫరా చేసి ఉన్నారు.";
							//message = "తేదీ "+dt+" న పర్మిట్ నెంబరు "+pno+" సంభందించి "+totalCaneWeight+" టన్నుల చెరుకును ఫ్యాక్టరీ కి సరఫరా చేసి ఉన్నారు";
							logger.info("saveWeighmentDetails: message Length >> " + message.length());
							
							postData += "User=" + URLEncoder.encode(User, "UTF-8") + "&passwd=" + passwd + "&mobilenumber="
							        + mobilenumber + "&message=" + URLEncoder.encode(message, "UTF-8") + "&sid=" + sid + "&mtype="
							        + mtype + "&DR=" + DR;
							
							URL url = new URL("http://www.sms4finsol.org/WebserviceSMS.aspX");
							logger.info("saveWeighmentDetails: message >> " + message);
							HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();

							urlconnection.setRequestMethod("POST");
							urlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
							urlconnection.setDoOutput(true);
							OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
							out.write(postData);
							out.close();
							BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));
							String decodedString;
							while ((decodedString = in.readLine()) != null)
							{
								retval += decodedString;
							}
							in.close();
							logger.info("saveWeighmentDetails()====== retval >> " + retval);

							if ("Messages Sent Successfully".equals(retval))
							{
								bRet = true;
								logger.info("========Messages Sent Successfully to==========" + mobilenumber);
							}
						}
					}
				}
			}
			else
			{
				jsonObj.put("isInsertTrue", "false");
				jArray.add(jsonObj);
			}
			logger.info("========jArray==========" + jArray);
			return jArray;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return jArray;
        }
	}
	
	public Map returnAccountSummaryModel(Map accountMap)
	{
		AccountSummary accountSummary = new AccountSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		String accCode = (String) accountMap.get("ACCOUNT_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSummary.setRunningbalance(runbal);
		accountSummary.setBalancetype(strtype);
		accountSummary.setAccountcode(accCode);
		accountSummary.setSeason(ses);
		tempMap.put(key,accountSummary);
		
		return tempMap;
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountGroupSummaryModel(Map accountMap)
	{
		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountGroupSummary.setAccountgroupcode(accountGroupCode);
		accountGroupSummary.setRunningbalance(runbal);
		accountGroupSummary.setBalancetype(strtype);
		accountGroupSummary.setSeason(ses);
		tempMap.put(key,accountGroupSummary);
		
		return tempMap;
	}
	public Map returnAccountSubGroupSummaryModel(Map accountMap)
	{
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		int accountsubgroupcode = (Integer) accountMap.get("ACCOUNT_SUB_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSubGroupSummary.setAccountsubgroupcode(accountsubgroupcode);
		accountSubGroupSummary.setAccountgroupcode(accountGroupCode);
		accountSubGroupSummary.setRunningbalance(runbal);
		accountSubGroupSummary.setBalancetype(strtype);
		accountSubGroupSummary.setSeason(ses);
		tempMap.put(key,accountSubGroupSummary);
		
		return tempMap;
	}
	
	private List prepareModelForAccruedAndDueCaneValueSummary(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		logger.info("========entered to prepareModelForAccruedAndDueCaneValueSummary()==========");

		boolean isInsertTrue = false;
		List<Ryot> ryot = ryotService.getRyotByRyotCode(caneWeighmentBean.getRyotCode());
		List AcrdAndDueCaneValSmryList = new ArrayList();
		
		double frp = 0.0;
		byte frpAcrued = 0;
		
		double purchaseTax = 0.0;
		byte purchaseTaxAcrued = 0;

		double harvestingSubsidy = 0.0;
		byte harvestingSubsidyAcrued = 0;

		double tpSubsidy = 0.0;
		byte tpSubsidyAcrued = 0;

		double addCp = 0.0;
		byte addCpAcrued = 0;
		double seasonCanePrice = 0.0;
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			seasonCanePrice = caneAcctRules.get(0).getSeasoncaneprice();
			
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			frp = caneAcctAmounts.get(0).getNetamt();
			frp =Double.parseDouble(new DecimalFormat("##.##").format(frp));

			frpAcrued = caneAcctAmounts.get(0).getConsiderforseasonacts();
			
			List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			purchaseTaxAcrued = purcaneAcctAmounts.get(0).getConsiderforseasonacts();
			purchaseTax = purcaneAcctAmounts.get(0).getAmount();
			purchaseTax =Double.parseDouble(new DecimalFormat("##.##").format(purchaseTax));


			List <CaneAcctAmounts> HscaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Harvesting Subsidy");
			harvestingSubsidy = HscaneAcctAmounts.get(0).getAmount();
			harvestingSubsidyAcrued = HscaneAcctAmounts.get(0).getConsiderforseasonacts();
			harvestingSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(harvestingSubsidy));

			List <CaneAcctAmounts> TpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Transport Subsidy");
			tpSubsidy = TpcaneAcctAmounts.get(0).getAmount();
			tpSubsidyAcrued = TpcaneAcctAmounts.get(0).getConsiderforseasonacts();
			tpSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(tpSubsidy));

			List <CaneAcctAmounts> addCpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			addCp = addCpcaneAcctAmounts.get(0).getAmount();
			addCpAcrued = addCpcaneAcctAmounts.get(0).getConsiderforseasonacts();
			addCp =Double.parseDouble(new DecimalFormat("##.##").format(addCp));

		}
		List AccruedAndDueCaneValueSummaryList = caneReceiptFunctionalService.getAccruedAndDueCaneValueSummaryList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(AccruedAndDueCaneValueSummaryList != null && AccruedAndDueCaneValueSummaryList.size()>0)
		{
			String strQry = null;
			Object Qryobj = null;
			
			Map tempMap = new HashMap();
			tempMap = (Map) AccruedAndDueCaneValueSummaryList.get(0);
			
			double totalValue = (Double) tempMap.get("totalvalue");
			double suppliedCaneWt = (Double) tempMap.get("suppliedcanewt");
			String ryotCode = (String) tempMap.get("ryotcode");
			String season = (String) tempMap.get("season");
			double dueAmt = (Double) tempMap.get("dueamount");

			double frpCaneVal = 0.0;
			double purchaseTaxCaneVal = 0.0;
			double harvestingSubsidyCaneVal = 0.0;
			double tpSubsidyCaneVal = 0.0;
			double addCpCaneVal = 0.0;
			double totalVal = 0.0;
			if(frp>0)
			{
				if(frpAcrued == 0)
				{
					frpCaneVal = frp*netWeight;
				}
			}
			
			if(purchaseTax>0)
			{
				if(purchaseTaxAcrued == 0)
				{
					purchaseTaxCaneVal = purchaseTax*netWeight;
				}
			}
			
			if(harvestingSubsidy>0)
			{
				if(harvestingSubsidyAcrued == 0)
				{
					harvestingSubsidyCaneVal = harvestingSubsidy*netWeight;
				}
			}
			
			if(tpSubsidy>0)
			{
				if(tpSubsidyAcrued == 0)
				{
					tpSubsidyCaneVal = tpSubsidy*netWeight;
				}
			}
			
			if(addCp>0)
			{
				if(addCpAcrued == 0)
				{
					addCpCaneVal = addCp*netWeight;
				}
			}
			totalVal = frpCaneVal+purchaseTaxCaneVal+harvestingSubsidyCaneVal+tpSubsidyCaneVal+addCpCaneVal;
			
			double finalCaneValue = totalVal+totalValue;
			double finalWeight = netWeight+suppliedCaneWt;
			
			double due =  totalVal;
			double finalDueAmt = dueAmt+due;
		
			finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));
			finalDueAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalDueAmt));

			strQry = "Update AccruedAndDueCaneValueSummary set totalvalue="+finalCaneValue+",suppliedcanewt="+finalWeight+",dueamount="+finalDueAmt+" where ryotcode='"+ryotCode+"' and season='"+season+"'";
			Qryobj = strQry;
			AcrdAndDueCaneValSmryList.add(Qryobj);
		}
		else
		{
			//insert
			double frpCaneVal = 0.0;
			double purchaseTaxCaneVal = 0.0;
			double harvestingSubsidyCaneVal = 0.0;
			double tpSubsidyCaneVal = 0.0;
			double addCpCaneVal = 0.0;
			double CaneVal = 0.0;
			
			if(frp>0)
			{
				if(frpAcrued == 0)
				{
					frpCaneVal = frp*netWeight;
				}
			}
			
			if(purchaseTax>0)
			{
				if(purchaseTaxAcrued == 0)
				{
					purchaseTaxCaneVal = purchaseTax*netWeight;
				}
			}
			
			if(harvestingSubsidy>0)
			{
				if(harvestingSubsidyAcrued == 0)
				{
					harvestingSubsidyCaneVal = harvestingSubsidy*netWeight;
				}
			}
			
			if(tpSubsidy>0)
			{
				if(tpSubsidyAcrued == 0)
				{
					tpSubsidyCaneVal = tpSubsidy*netWeight;
				}
			}
			
			if(addCp>0)
			{
				if(addCpAcrued == 0)
				{
					addCpCaneVal = addCp*netWeight;
				}
			}

			CaneVal = frpCaneVal+purchaseTaxCaneVal+harvestingSubsidyCaneVal+tpSubsidyCaneVal+addCpCaneVal;
			CaneVal =Double.parseDouble(new DecimalFormat("##.##").format(CaneVal));

			AccruedAndDueCaneValueSummary accruedAndDueCaneValueSummary = new AccruedAndDueCaneValueSummary();
			accruedAndDueCaneValueSummary.setSeason(caneWeighmentBean.getSeason());
			accruedAndDueCaneValueSummary.setRyotcode(caneWeighmentBean.getRyotCode());
			accruedAndDueCaneValueSummary.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
			accruedAndDueCaneValueSummary.setVillagecode(ryot.get(0).getVillagecode());
			accruedAndDueCaneValueSummary.setPaidamount(0.0);
			accruedAndDueCaneValueSummary.setDueamount(CaneVal);
			accruedAndDueCaneValueSummary.setSuppliedcanewt(netWeight);
			accruedAndDueCaneValueSummary.setTotalvalue(CaneVal);
			
			AcrdAndDueCaneValSmryList.add(accruedAndDueCaneValueSummary);
		}
		return AcrdAndDueCaneValSmryList;
	}
	private List prepareModelForCaneValueDetailsByRyot(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		logger.info("========entered to prepareModelForCaneValueDetailsByRyot()==========");

		boolean isInsertTrue = false;
		List<Ryot> ryot = ryotService.getRyotByRyotCode(caneWeighmentBean.getRyotCode());
		List CaneValDtlsByRyotList = new ArrayList();

		double frp = 0.0;
		byte frpAcrued = 0;
		
		double purchaseTax = 0.0;
		byte purchaseTaxAcrued = 0;
		byte isPurchaseTax = 0;

		double harvestingSubsidy = 0.0;
		byte harvestingSubsidyAcrued = 0;

		double tpSubsidy = 0.0;
		byte tpSubsidyAcrued = 0;
		
		double addCp = 0.0;
		byte addCpAcrued = 0;
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;

		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			frp = caneAcctAmounts.get(0).getNetamt();
			frp =Double.parseDouble(new DecimalFormat("##.##").format(frp));

			frpAcrued = caneAcctAmounts.get(0).getConsiderforseasonacts();
			
			List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			purchaseTax = purcaneAcctAmounts.get(0).getAmount();
			purchaseTax =Double.parseDouble(new DecimalFormat("##.##").format(purchaseTax));

			purchaseTaxAcrued = purcaneAcctAmounts.get(0).getConsiderforseasonacts();
			isPurchaseTax = purcaneAcctAmounts.get(0).getIsthispurchasetax();
			
			List <CaneAcctAmounts> HscaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Harvesting Subsidy");
			harvestingSubsidy = HscaneAcctAmounts.get(0).getAmount();
			harvestingSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(harvestingSubsidy));
			harvestingSubsidyAcrued = HscaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> TpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Transport Subsidy");
			tpSubsidy = TpcaneAcctAmounts.get(0).getAmount();
			tpSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(tpSubsidy));
			tpSubsidyAcrued = TpcaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> addCpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			addCp = addCpcaneAcctAmounts.get(0).getAmount();
			addCp =Double.parseDouble(new DecimalFormat("##.##").format(addCp));
			addCpAcrued = addCpcaneAcctAmounts.get(0).getConsiderforseasonacts();
		}
		
		List CaneValDtlsByRyot = caneReceiptFunctionalService.CaneValueDetailsByRyotList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(CaneValDtlsByRyot != null && CaneValDtlsByRyot.size()>0)
		{
			String strQry = null;
			Object Qryobj = null;
			for(int j=0;j<CaneValDtlsByRyot.size();j++)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) CaneValDtlsByRyot.get(j);
				
				String itemOn = (String) tempMap.get("calculationitem");
				double caneValue = (Double) tempMap.get("canevalue");
				double suppliedCaneWt = (Double) tempMap.get("suppliedcanewt");
				String ryotCode = (String) tempMap.get("ryotcode");
				String season = (String) tempMap.get("season");
				if("FRP".equalsIgnoreCase(itemOn))
				{
					//if(frpAcrued == 0)
					//{
						double suppliedWt = suppliedCaneWt+netWeight;
						double finalCaneValue = caneValue+ netWeight*frp;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyot set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='FRP'";
						Qryobj = strQry;
						CaneValDtlsByRyotList.add(Qryobj);
					//}
				}
				
				if("Purchase Tax / ICP".equalsIgnoreCase(itemOn))
				{
					//if(purchaseTaxAcrued == 0)
					//{
						//if(isPurchaseTax == 0)
						//{
							/*double suppliedWt = suppliedCaneWt+netWeight;
							double finalCaneValue = caneValue+ netWeight*purchaseTax;
							
							strQry = "Update CaneValueDetailsByRyot set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Purchase Tax / ICP'";
							Qryobj = strQry;
							CaneValDtlsByRyotList.add(Qryobj);*/
						//}
					//}
				}
				
				if("Harvesting Subsidy".equalsIgnoreCase(itemOn))
				{
					//if(harvestingSubsidyAcrued == 0)
					//{
						double suppliedWt = suppliedCaneWt+netWeight;
						double finalCaneValue = caneValue+ netWeight*harvestingSubsidy;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyot set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Harvesting Subsidy'";
						Qryobj = strQry;
						CaneValDtlsByRyotList.add(Qryobj);
					//}
				}
				
				if("Transport Subsidy".equalsIgnoreCase(itemOn))
				{
					//if(tpSubsidyAcrued == 0)
					//{
						double suppliedWt = suppliedCaneWt+netWeight;
						double finalCaneValue = caneValue+ netWeight*tpSubsidy;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyot set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Transport Subsidy'";
						Qryobj = strQry;
						CaneValDtlsByRyotList.add(Qryobj);
					//}
				}
				if("Additional Cane Price".equalsIgnoreCase(itemOn))
				{
					//if(addCpAcrued == 0)
					//{
						double suppliedWt = suppliedCaneWt+netWeight;
						double finalCaneValue = caneValue+ netWeight*addCp;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyot set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Additional Cane Price'";
						Qryobj = strQry;
						CaneValDtlsByRyotList.add(Qryobj);
					//}
				}
			}
		}
		else
		{
			//insert
			if(frp>0)
			{
				//if(frpAcrued == 0)
				//{
					CaneValueDetailsByRyot caneValueDetailsByRyot = new CaneValueDetailsByRyot();
					caneValueDetailsByRyot.setCalculationitem("FRP");
					caneValueDetailsByRyot.setIsaccruedamountanddue(frpAcrued);
					caneValueDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
					caneValueDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
					caneValueDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
					caneValueDetailsByRyot.setVillagecode(ryot.get(0).getVillagecode());
					caneValueDetailsByRyot.setSuppliedcanewt(netWeight);
					
					double finalCaneValue = netWeight*frp;
					finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

					caneValueDetailsByRyot.setCanevalue(finalCaneValue);
					CaneValDtlsByRyotList.add(caneValueDetailsByRyot);
				//}
			}
			
			if(purchaseTax>0)
			{
				//if(purchaseTaxAcrued == 0)
				//{
					//if it is not purtax we should consider this in postings
					//if(isPurchaseTax == 0)
					//{
						/*CaneValueDetailsByRyot caneValueDetailsByRyot = new CaneValueDetailsByRyot();
						caneValueDetailsByRyot.setCalculationitem("Purchase Tax / ICP");
						caneValueDetailsByRyot.setIsaccruedamountanddue(purchaseTaxAcrued);
						caneValueDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
						caneValueDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
						caneValueDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
						caneValueDetailsByRyot.setVillagecode(ryot.get(0).getVillagecode());
						caneValueDetailsByRyot.setSuppliedcanewt(netWeight);
						caneValueDetailsByRyot.setCanevalue(netWeight*purchaseTax);
						CaneValDtlsByRyotList.add(caneValueDetailsByRyot);*/
					//}
				//}
			}
			
			if(harvestingSubsidy>0)
			{
				//if(harvestingSubsidyAcrued == 0)
				//{
					CaneValueDetailsByRyot caneValueDetailsByRyot = new CaneValueDetailsByRyot();
					caneValueDetailsByRyot.setCalculationitem("Harvesting Subsidy");
					caneValueDetailsByRyot.setIsaccruedamountanddue(harvestingSubsidyAcrued);
					caneValueDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
					caneValueDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
					caneValueDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
					caneValueDetailsByRyot.setVillagecode(ryot.get(0).getVillagecode());
					caneValueDetailsByRyot.setSuppliedcanewt(netWeight);
					
					double finalCaneValue = netWeight*harvestingSubsidy;
					finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

					caneValueDetailsByRyot.setCanevalue(finalCaneValue);
					CaneValDtlsByRyotList.add(caneValueDetailsByRyot);
				//}
			}
			if(tpSubsidy>0)
			{
				//if(tpSubsidyAcrued == 0)
				//{
					CaneValueDetailsByRyot caneValueDetailsByRyot = new CaneValueDetailsByRyot();
					caneValueDetailsByRyot.setCalculationitem("Transport Subsidy");
					caneValueDetailsByRyot.setIsaccruedamountanddue(tpSubsidyAcrued);
					caneValueDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
					caneValueDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
					caneValueDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
					caneValueDetailsByRyot.setVillagecode(ryot.get(0).getVillagecode());
					caneValueDetailsByRyot.setSuppliedcanewt(netWeight);
					
					double finalCaneValue = netWeight*tpSubsidy;
					finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

					caneValueDetailsByRyot.setCanevalue(finalCaneValue);
					CaneValDtlsByRyotList.add(caneValueDetailsByRyot);
				//}
			}
			
			if(addCp>0)
			{
				//if(addCpAcrued == 0)
				//{
					CaneValueDetailsByRyot caneValueDetailsByRyot = new CaneValueDetailsByRyot();
					caneValueDetailsByRyot.setCalculationitem("Additional Cane Price");
					caneValueDetailsByRyot.setIsaccruedamountanddue(addCpAcrued);
					caneValueDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
					caneValueDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
					caneValueDetailsByRyot.setRyotcodeseqno(ryot.get(0).getRyotcodesequence());
					caneValueDetailsByRyot.setVillagecode(ryot.get(0).getVillagecode());
					caneValueDetailsByRyot.setSuppliedcanewt(netWeight);
					
					double finalCaneValue = netWeight*addCp;
					finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

					caneValueDetailsByRyot.setCanevalue(finalCaneValue);
					CaneValDtlsByRyotList.add(caneValueDetailsByRyot);
				//}
			}
		}
        return CaneValDtlsByRyotList;
	}
	
	private List prepareModefForCaneValueDetailsByRyotAndDate(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		logger.info("========entered to prepareModefForCaneValueDetailsByRyotAndDate()==========");

		List CaneValDtlsByRyotDateList = new ArrayList();
		double frp = 0.0;
		byte frpAcrued = 0;
		
		double purchaseTax = 0.0;
		byte purchaseTaxAcrued = 0;
		byte isPurchaseTax = 0;
		
		double harvestingSubsidy = 0.0;
		byte harvestingSubsidyAcrued = 0;

		double tpSubsidy = 0.0;
		byte tpSubsidyAcrued = 0;

		double addCp = 0.0;
		byte addCpAcrued = 0;
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;

		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			frp = caneAcctAmounts.get(0).getNetamt();
			frp =Double.parseDouble(new DecimalFormat("##.##").format(frp));

			frpAcrued = caneAcctAmounts.get(0).getConsiderforseasonacts();
			
			List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			purchaseTax = purcaneAcctAmounts.get(0).getAmount();
			purchaseTax =Double.parseDouble(new DecimalFormat("##.##").format(purchaseTax));

			purchaseTaxAcrued = purcaneAcctAmounts.get(0).getConsiderforseasonacts();
			isPurchaseTax = purcaneAcctAmounts.get(0).getIsthispurchasetax();
			
			List <CaneAcctAmounts> HscaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Harvesting Subsidy");
			harvestingSubsidy = HscaneAcctAmounts.get(0).getAmount();
			harvestingSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(harvestingSubsidy));

			harvestingSubsidyAcrued = HscaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> TpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Transport Subsidy");
			tpSubsidy = TpcaneAcctAmounts.get(0).getAmount();
			tpSubsidy =Double.parseDouble(new DecimalFormat("##.##").format(tpSubsidy));
			tpSubsidyAcrued = TpcaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> addCpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			addCp = addCpcaneAcctAmounts.get(0).getAmount();
			addCp =Double.parseDouble(new DecimalFormat("##.##").format(addCp));
			addCpAcrued = addCpcaneAcctAmounts.get(0).getConsiderforseasonacts();
		}

		List caneValueDetailsByRyotAndDateList=caneReceiptFunctionalService.CaneValueDetailsByRyotAndDateList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(caneValueDetailsByRyotAndDateList==null)
		{
			if(frp>0)
			{
				CaneValueDetailsByRyotAndDate  caneValueDetailsByRyotAndDate= new CaneValueDetailsByRyotAndDate();		

				caneValueDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
				caneValueDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				caneValueDetailsByRyotAndDate.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				caneValueDetailsByRyotAndDate.setSuppliedcanewt(netWeight);
				caneValueDetailsByRyotAndDate.setCalculationitem("FRP");
				double totalVal = netWeight*frp;
				totalVal =Double.parseDouble(new DecimalFormat("##.##").format(totalVal));

				caneValueDetailsByRyotAndDate.setCanevalue(totalVal);
				caneValueDetailsByRyotAndDate.setIsaccruedanddue(frpAcrued);
				caneValueDetailsByRyotAndDate.setRate(frp);
				
				CaneValDtlsByRyotDateList.add(caneValueDetailsByRyotAndDate);
			}
			if(purchaseTax>0)
			{
				/*if(isPurchaseTax == 0)
				{
					CaneValueDetailsByRyotAndDate  caneValueDetailsByRyotAndDate= new CaneValueDetailsByRyotAndDate();		

					caneValueDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
					caneValueDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
					caneValueDetailsByRyotAndDate.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
					caneValueDetailsByRyotAndDate.setSuppliedcanewt(netWeight);
					caneValueDetailsByRyotAndDate.setCalculationitem("Purchase Tax / ICP");
					caneValueDetailsByRyotAndDate.setCanevalue(netWeight*purchaseTax);
					caneValueDetailsByRyotAndDate.setIsaccruedanddue(purchaseTaxAcrued);
					caneValueDetailsByRyotAndDate.setRate(purchaseTax);
					
					CaneValDtlsByRyotDateList.add(caneValueDetailsByRyotAndDate);
				}*/
				
			}
			if(harvestingSubsidy>0)
			{
				CaneValueDetailsByRyotAndDate  caneValueDetailsByRyotAndDate= new CaneValueDetailsByRyotAndDate();		

				caneValueDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
				caneValueDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				caneValueDetailsByRyotAndDate.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				caneValueDetailsByRyotAndDate.setSuppliedcanewt(netWeight);
				caneValueDetailsByRyotAndDate.setCalculationitem("Harvesting Subsidy");
				
				double totalVal = netWeight*harvestingSubsidy;
				totalVal =Double.parseDouble(new DecimalFormat("##.##").format(totalVal));

				caneValueDetailsByRyotAndDate.setCanevalue(totalVal);
				caneValueDetailsByRyotAndDate.setIsaccruedanddue(harvestingSubsidyAcrued);
				caneValueDetailsByRyotAndDate.setRate(harvestingSubsidy);
				
				CaneValDtlsByRyotDateList.add(caneValueDetailsByRyotAndDate);
			}
			
			if(tpSubsidy>0)
			{
				CaneValueDetailsByRyotAndDate  caneValueDetailsByRyotAndDate= new CaneValueDetailsByRyotAndDate();		

				caneValueDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
				caneValueDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				caneValueDetailsByRyotAndDate.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				caneValueDetailsByRyotAndDate.setSuppliedcanewt(netWeight);
				caneValueDetailsByRyotAndDate.setCalculationitem("Transport Subsidy");
				
				double totalVal = netWeight*tpSubsidy;
				totalVal =Double.parseDouble(new DecimalFormat("##.##").format(totalVal));

				caneValueDetailsByRyotAndDate.setCanevalue(totalVal);
				caneValueDetailsByRyotAndDate.setIsaccruedanddue(tpSubsidyAcrued);
				caneValueDetailsByRyotAndDate.setRate(tpSubsidy);
				
				CaneValDtlsByRyotDateList.add(caneValueDetailsByRyotAndDate);
			}
			
			if(addCp>0)
			{
				CaneValueDetailsByRyotAndDate  caneValueDetailsByRyotAndDate= new CaneValueDetailsByRyotAndDate();		

				caneValueDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
				caneValueDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				caneValueDetailsByRyotAndDate.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				caneValueDetailsByRyotAndDate.setSuppliedcanewt(netWeight);
				caneValueDetailsByRyotAndDate.setCalculationitem("Additional Cane Price");
				
				double totalVal = netWeight*addCp;
				totalVal =Double.parseDouble(new DecimalFormat("##.##").format(totalVal));

				caneValueDetailsByRyotAndDate.setCanevalue(totalVal);
				caneValueDetailsByRyotAndDate.setIsaccruedanddue(addCpAcrued);
				caneValueDetailsByRyotAndDate.setRate(addCp);
				
				CaneValDtlsByRyotDateList.add(caneValueDetailsByRyotAndDate);
			}
		}
		else
		{
			String strQry = null;
			Object Qryobj = null;
			
			List CaneValList = new ArrayList();
			CaneValList = caneReceiptFunctionalService.getCaneValDtlsByRyotDateList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
			if(CaneValList != null && CaneValList.size()>0)
			{
				for(int j=0;j<CaneValList.size();j++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) CaneValList.get(j);
					String itemOn = (String) tempMap.get("calculationitem");
					Date suplyDate = (Date) tempMap.get("canesupplieddt");
					
					String strsuplyDate = null;
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					strsuplyDate = df.format(suplyDate);
					
					double caneValue = (Double) tempMap.get("canevalue");
					byte isAcrued = (Byte) tempMap.get("isaccruedanddue");
					String ryotCode = (String) tempMap.get("ryotcode");
					String season = (String) tempMap.get("season");
					double suppliedCaneWt = (Double) tempMap.get("suppliedcanewt");

					if("FRP".equalsIgnoreCase(itemOn))
					{
						double suppliedWt = suppliedCaneWt+netWeight;
						suppliedWt =Double.parseDouble(new DecimalFormat("##.###").format(suppliedWt));

						double finalCaneValue = caneValue+ netWeight*frp;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyotAndDate set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='FRP' and canesupplieddt between '"+strsuplyDate+"' and '"+strsuplyDate+"'";
						Qryobj = strQry;
						CaneValDtlsByRyotDateList.add(Qryobj);
					}
					
					if("Purchase Tax / ICP".equalsIgnoreCase(itemOn))
					{
						/*if(isPurchaseTax == 0)
						{
							double suppliedWt = suppliedCaneWt+netWeight;
							double finalCaneValue = caneValue+ netWeight*purchaseTax;
							
							strQry = "Update CaneValueDetailsByRyotAndDate set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Purchase Tax / ICP' and canesupplieddt between '"+strsuplyDate+"' and '"+strsuplyDate+"'";
							Qryobj = strQry;
							CaneValDtlsByRyotDateList.add(Qryobj);
						}*/
					}
					
					if("Harvesting Subsidy".equalsIgnoreCase(itemOn))
					{
						double suppliedWt = suppliedCaneWt+netWeight;
						suppliedWt =Double.parseDouble(new DecimalFormat("##.###").format(suppliedWt));

						double finalCaneValue = caneValue+ netWeight*harvestingSubsidy;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyotAndDate set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Harvesting Subsidy' and canesupplieddt between '"+strsuplyDate+"' and '"+strsuplyDate+"'";
						Qryobj = strQry;
						CaneValDtlsByRyotDateList.add(Qryobj);
					}
					
					if("Transport Subsidy".equalsIgnoreCase(itemOn))
					{
						double suppliedWt = suppliedCaneWt+netWeight;
						suppliedWt =Double.parseDouble(new DecimalFormat("##.###").format(suppliedWt));

						double finalCaneValue = caneValue+ netWeight*tpSubsidy;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyotAndDate set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Transport Subsidy' and canesupplieddt between '"+strsuplyDate+"' and '"+strsuplyDate+"'";
						Qryobj = strQry;
						CaneValDtlsByRyotDateList.add(Qryobj);
					}
					if("Additional Cane Price".equalsIgnoreCase(itemOn))
					{
						double suppliedWt = suppliedCaneWt+netWeight;
						suppliedWt =Double.parseDouble(new DecimalFormat("##.###").format(suppliedWt));
						
						double finalCaneValue = caneValue+ netWeight*addCp;
						finalCaneValue =Double.parseDouble(new DecimalFormat("##.##").format(finalCaneValue));

						strQry = "Update CaneValueDetailsByRyotAndDate set canevalue="+finalCaneValue+",suppliedcanewt="+suppliedWt+" where ryotcode='"+ryotCode+"' and season='"+season+"'and calculationitem='Additional Cane Price' and canesupplieddt between '"+strsuplyDate+"' and '"+strsuplyDate+"'";
						Qryobj = strQry;
						CaneValDtlsByRyotDateList.add(Qryobj);
					}
				}
			}
		}
		return CaneValDtlsByRyotDateList;
	}
	
	private boolean updateAccounts(String ryotCode,Double totalPrice,String jrnlMemo,String season,String transactionType,String glCode)
	{
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String strQry = null;
		Object Qryobj = null;
		boolean isInsertSuccess = false;
		int transactionCode = commonService.GetMaxTransCode(season);
		String AccountCode=ryotCode;
		int AccGrpCode = ahFuctionalService.GetAccGrpCode(ryotCode);
		
		Map DtlsMap = new HashMap();
		DtlsMap.put("TRANSACTION_CODE", transactionCode);
		DtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		//added by sahadeva 28/10/2016
		DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
		
		DtlsMap.put("ACCOUNT_CODE", AccountCode);
		DtlsMap.put("JOURNAL_MEMO", jrnlMemo);
		DtlsMap.put("TRANSACTION_TYPE", transactionType);
		//Added by DMurty on 18-10-2016
		DtlsMap.put("SEASON",season);
		DtlsMap.put("GL_CODE",glCode);

		AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);//1
		entityList.add(accountDetails);
		
		String TransactionType = "Cr";
		AccountSummary accountSummary = prepareModelforAccSmry(totalPrice,AccountCode,TransactionType,season); //2
		
		double finalAmt = accountSummary.getRunningbalance();
		String strCrDr = accountSummary.getBalancetype();
		String AccCode = accountSummary.getAccountcode();
		List BalList = ahFuctionalService.BalDetails(AccCode,season);
		if (BalList != null && BalList.size() > 0)
		{
			strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
			Qryobj = strQry;
			UpdateList.add(Qryobj);
		}
		else
		{
			entityList.add(accountSummary);
		}
		
		//Insert Rec in AccGrpDetails
		Map AccGrpDtlsMap = new HashMap();
		
		AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		//added by sahadeva 28/10/2016
		AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
		
		AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccGrpDtlsMap.put("JOURNAL_MEMO", jrnlMemo);
		AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
		//Added by DMurty on 18-10-2016
		AccGrpDtlsMap.put("SEASON",season);
			
		AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);//3
		entityList.add(accountGroupDetails);
		//commonService.addAccountGrpDtls(accountGroupDetails);
			
		TransactionType = "Cr";
		AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//4
		List BalList1 = commonService.AccGrpBalDetails(AccGrpCode,season);
		if (BalList1 != null && BalList1.size() > 0)
		{
			double finalAmt1 = accountGroupSummary.getRunningbalance();
			String strCrDr1 = accountGroupSummary.getBalancetype();
			int AccGroupCode = accountGroupSummary.getAccountgroupcode();
			
			strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and season='"+season+"'";
			Qryobj = strQry;
			UpdateList.add(Qryobj);
		}
		else
		{
			entityList.add(accountGroupSummary);
		}
		//Insert Rec in AccSubGrpDtls
		Map AccSubGrpDtlsMap = new HashMap();
		
		AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
		AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", totalPrice);
		//added by sahadeva 28/10/2016
		AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
		
		AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
		AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
		AccSubGrpDtlsMap.put("JOURNAL_MEMO", jrnlMemo);
		AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
		//Added by DMurty on 18-10-2016
		AccSubGrpDtlsMap.put("SEASON",season);
		
		AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);//5
		entityList.add(accountSubGroupDetails);
		//commonService.addAccountSubGrpDtls(accountSubGroupDetails);
		
		TransactionType = "Cr";
		AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode,totalPrice,AccGrpCode,TransactionType,season);//6
		
		double finalAmt2 = accountSubGroupSummary.getRunningbalance();
		String strCrDr2 = accountSubGroupSummary.getBalancetype();
		int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
		int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
		
		List BalList2 = commonService.SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
		if (BalList2 != null && BalList2.size() > 0)
		{
			strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
			Qryobj = strQry;
			UpdateList.add(Qryobj);
		}
		else
		{
			entityList.add(accountSubGroupSummary);
		}
		isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
		return isInsertSuccess;
	}
	
	private AccountDetails prepareModelForAccountDetails(Map DtlsMap) 
	{
		
		int TransactionCode = (Integer) DtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) DtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) DtlsMap.get("LOGIN_USER");
		String AccountCode = (String) DtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) DtlsMap.get("JOURNAL_MEMO");
		String TransType = (String) DtlsMap.get("TRANSACTION_TYPE");
		String GlCode = (String) DtlsMap.get("GL_CODE");
		if ("".equals(GlCode) || "null".equals(GlCode) || (GlCode == null))
		{
			GlCode = "NA";
		}
		//Added by DMurty on 18-10-2016
		String season = (String) DtlsMap.get("SEASON");
		
		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();
	    AccountDetails accountDetails = new AccountDetails();				
		accountDetails.setTransactioncode(TransactionCode);
		accountDetails.setTransactiondate(date);
		accountDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountDetails.setAccountcode(AccountCode);
		accountDetails.setJournalmemo(journalMemo);
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountDetails.setDr(Amt);
			accountDetails.setCr(0.0);
		}
		else
		{
			accountDetails.setDr(0.0);
			accountDetails.setCr(Amt);
		}
		accountDetails.setRunningbalance(Amt);
		accountDetails.setLoginuser(loginuser);
		accountDetails.setIsapplyforledger((byte) 1);
		accountDetails.setGlcode(GlCode);
		accountDetails.setStatus((byte) 1);
		accountDetails.setSeason(season);
		
		return accountDetails;
	}
	
	private AccountSummary prepareModelforAccSmry(Double Amt,String AccountCode,String TransactionType,String season) 
	{			
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		String AccCode = AccountCode;
		double TotalAmount = Amt;
		
		//Getting Data from Account Summary
		List BalList = commonService.AccSmryBalanceDetails(AccCode,TotalAmount,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		
		AccountSummary accountSummary = new AccountSummary();
		accountSummary.setAccountcode(AccountCode);
		accountSummary.setRunningbalance(RunnBal);
		accountSummary.setBalancetype(TransType);
		accountSummary.setSeason(season);
		
		return accountSummary;
	}
	
	private AccountGroupDetails prepareModelForAccGrpDetails(Map AccGrpDtlsMap) 
	{
		
		int TransactionCode = (Integer) AccGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccGrpDtlsMap.get("TRANSACTION_TYPE");
		//Added by DMurty on 18-10-2016
		String season = (String) AccGrpDtlsMap.get("SEASON");
		
		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();
		AccountGroupDetails accountGroupDetails = new AccountGroupDetails();
		
		accountGroupDetails.setTransactioncode(TransactionCode);
		accountGroupDetails.setTransactiondate(date);
		accountGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountGroupDetails.setAccountgroupcode(AccGrpCode);
		accountGroupDetails.setJournalmemo(journalMemo);
		
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountGroupDetails.setDr(Amt);
			accountGroupDetails.setCr(0.0);
		}
		else
		{
			accountGroupDetails.setDr(0.0);
			accountGroupDetails.setCr(Amt);
		}
		
		accountGroupDetails.setRunningbalance(Amt);
		accountGroupDetails.setLoginuser(loginuser);
		accountGroupDetails.setIsapplyforledger((byte) 1);
		accountGroupDetails.setStatus((byte) 1);
		accountGroupDetails.setSeason(season);
		
		return accountGroupDetails;
	}
	
	private AccountGroupSummary prepareModelForAccGrpSmry(String AccountCode,Double Amt,int AccGrpCode,String TransactionType,String season) 
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
		String AccCode = AccountCode;
		double TotalAmt = Amt;

		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0)
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}
		
		List BalList = commonService.AccGrpBalanceDetails(AccGrpCode,TotalAmt,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		accountGroupSummary.setAccountgroupcode(AccGrpCode);
		accountGroupSummary.setRunningbalance(RunnBal);
		accountGroupSummary.setBalancetype(TransType);
		accountGroupSummary.setSeason(season);
		
		return accountGroupSummary;
	}
	
	
	private AccountSubGroupDetails prepareModelForAccSubGrpDtls(Map AccSubGrpDtlsMap) 
	{
		
		int TransactionCode = (Integer) AccSubGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccSubGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccSubGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccSubGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccSubGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccSubGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccSubGrpDtlsMap.get("TRANSACTION_TYPE");
		//Added by DMurty on 18-10-2016
		String season = (String) AccSubGrpDtlsMap.get("SEASON");

		Date date = new Date();
		java.util.Date datendtime= new java.util.Date();
		int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);

		AccountSubGroupDetails accountSubGroupDetails = new AccountSubGroupDetails();
		
		accountSubGroupDetails.setTransactioncode(TransactionCode);
		accountSubGroupDetails.setTransactiondate(date);
		accountSubGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountSubGroupDetails.setAccountgroupcode(AccGrpCode);
		accountSubGroupDetails.setAccountsubgroupcode(AccSubGrpCode);
		accountSubGroupDetails.setJournalmemo(journalMemo);
		
		if("Dr".equalsIgnoreCase(TransType))
		{
			accountSubGroupDetails.setDr(Amt);
			accountSubGroupDetails.setCr(0.0);
		}
		else
		{
			accountSubGroupDetails.setDr(0.0);
			accountSubGroupDetails.setCr(Amt);
		}
		
		accountSubGroupDetails.setRunningbalance(Amt);
		accountSubGroupDetails.setLoginuser(loginuser);
		accountSubGroupDetails.setIsapplyforledger((byte) 1);
		accountSubGroupDetails.setStatus((byte) 1);
		accountSubGroupDetails.setSeason(season);
		
		return accountSubGroupDetails;
	}
	
	private AccountSubGroupSummary prepareModelforAccSubGrpSmry(String AccountCode,Double Amt,int AccGrpCode,String TransactionType,String season) 
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		String AccCode = AccountCode;
		double TotalAmt = Amt;
		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0)
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}
		
		List BalList = commonService.SubGrpBalanceDetails(AccGrpCode,AccSubGroupCode,TotalAmt,TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		accountSubGroupSummary.setAccountgroupcode(AccGrpCode);
		accountSubGroupSummary.setAccountsubgroupcode(AccSubGroupCode);
		accountSubGroupSummary.setRunningbalance(RunnBal);
		accountSubGroupSummary.setBalancetype(TransType);
		accountSubGroupSummary.setSeason(season);
		
		return accountSubGroupSummary;
	}
	 public int getShiftId() throws ParseException
	 {
		boolean flag=false;
		String curtime = null;
		curtime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
		List<Shift> shifts =shiftService.listShifts();
		Integer shiftId=0;
		for(int i=0;i<shifts.size();i++)
		{
			flag=DateUtils.isTimeBetweenTwoTime(shifts.get(i).getFromtime().toString(),shifts.get(i).getTotime().toString(),curtime);
			
			if(flag)
			{
				shiftId=shifts.get(i).getShiftid();
				break;
			}
		}
		logger.info("========entered to getShiftId()==========shiftId"+shiftId);
		return shiftId;
	 }
	
	private WeighmentDetails prepareModelForWeighmentDetails(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,int transactionCode) throws ParseException 
	{		
			WeighmentDetails weighmentDetails =null;

			double totalWt = caneWeighmentBean.getTotalWeight()/1000;
			double net = totalWt-(totalWt*(bindingMaterial/100));
			
			Double netWeight=caneWeighmentBean.getTotalWeight()-(caneWeighmentBean.getTotalWeight()*(bindingMaterial/100));
			weighmentDetails=(WeighmentDetails) commonService.getEntityById(caneWeighmentBean.getSerialNumber(),WeighmentDetails.class);
			
			//Added by DMurty on 19-08-2016
			String ryotCode = caneWeighmentBean.getRyotCode();
			String season = caneWeighmentBean.getSeason();
			boolean  updateFlag = false;
			List RyotList = caneReceiptFunctionalService.getRyotDatainWeighmentDetails(season, ryotCode);
			if(RyotList != null && RyotList.size()>0)
			{
				//Updating only end date
				updateFlag=caneReceiptFunctionalService.updateCaneReceptEndDateInAgreement(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getSeason(),caneWeighmentBean.getPlotNumber(),DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			}
			else
			{
				//Updating start date and end date
				updateFlag=caneReceiptFunctionalService.updateCaneReceptSartEndDateInAgreement(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getSeason(),caneWeighmentBean.getPlotNumber(),DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT),DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			}
			
			if(caneWeighmentBean.getGrossTare().equalsIgnoreCase("0"))
			{			
				weighmentDetails = new WeighmentDetails();		
				weighmentDetails.setSerialnumber(caneWeighmentBean.getSerialNumber());
				weighmentDetails.setAgreementnumber(caneWeighmentBean.getAgreementNumber());
				weighmentDetails.setBindingmtlwght(bindingMaterial);
				weighmentDetails.setBurntcane(caneWeighmentBean.getBurntCane());		
				weighmentDetails.setCanereceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				// Added by sahadeva for grower wise hourly crushing report Purpose
				String stime=(caneWeighmentBean.getCaneWeighmentTime());
				
				weighmentDetails.setTime(stime);
				
				//Modified by DMurty on 01-12-2016

				Time time = DateUtils.getCurrentSystemTime();
				weighmentDetails.setCanereceipttime(time);
				
				int shiftId = getShiftId();
				weighmentDetails.setShiftid(shiftId);

				//weighmentDetails.setCanereceipttime(java.sql.Time.valueOf(caneWeighmentBean.getCaneWeighmentTime()));
				
				weighmentDetails.setCirclecode(caneWeighmentBean.getCircleCode());
				weighmentDetails.setFhgname(caneWeighmentBean.getRelativeName());
				weighmentDetails.setLandvilcode(caneWeighmentBean.getLandVillageCode());
				weighmentDetails.setPlantorratoon(caneWeighmentBean.getPlantOrRatoon());
				weighmentDetails.setPartload(caneWeighmentBean.getPartLoad());
				weighmentDetails.setPermitnumber(caneWeighmentBean.getPermitNumber());
				weighmentDetails.setRyotcode(caneWeighmentBean.getRyotCode());
				weighmentDetails.setRyotname(caneWeighmentBean.getRyotName());
				weighmentDetails.setVehicleno(caneWeighmentBean.getVehicleNumber());
				weighmentDetails.setGrossweight(0.0);
				
				if(caneWeighmentBean.getUnloadingContractor() != null)
				{
					weighmentDetails.setUccode(caneWeighmentBean.getUnloadingContractor());
				}
				else
				{
					weighmentDetails.setUccode(0);
				}
				
				//Modified by DMurty on 02-09-2016. Vehicle type is removed as per the meeting on 29-08-2016
				//Added by DMurty on 26-11-2016
				weighmentDetails.setVehicletype(caneWeighmentBean.getVehicle());
				
				weighmentDetails.setPermitnumber(caneWeighmentBean.getPermitNumber());
				weighmentDetails.setNetwt(0.0);
				weighmentDetails.setVarietycode(caneWeighmentBean.getVarietyCode());
				weighmentDetails.setVillagecode(caneWeighmentBean.getVillageCode());
				weighmentDetails.setWeighbridgeslno(caneWeighmentBean.getWeighBridgeId());//
				weighmentDetails.setWeightmentype(caneWeighmentBean.getGrossTare());
				weighmentDetails.setZonecode(caneWeighmentBean.getZoneCode());
				weighmentDetails.setPlotnumber(caneWeighmentBean.getPlotNumber());
				//added by sahadeva 28/10/2016
				weighmentDetails.setLoginid(loginController.getLoggedInUserName());
				
				weighmentDetails.setSeason(caneWeighmentBean.getSeason());
				weighmentDetails.setWeighmentno(caneWeighmentBean.getWeighmentorder());
				weighmentDetails.setLorryortruck(caneWeighmentBean.getVehicle());
				weighmentDetails.setCastatus((byte)0);				
				weighmentDetails.setStatus((byte)0);
				weighmentDetails.setSecondpermitno(caneWeighmentBean.getSecondPermitNumber());
				
				double lorryAndCaneWt = caneWeighmentBean.getTotalWeight()/1000;
				lorryAndCaneWt =Double.parseDouble(new DecimalFormat("##.###").format(lorryAndCaneWt));
				weighmentDetails.setLorryandcanewt(lorryAndCaneWt);
				
				//Added by DMurty on 31-08-2016
				int regionCode = 0;
				List ZoneList = new ArrayList();
				ZoneList = ahFuctionalService.getZoneDetails(caneWeighmentBean.getZoneCode());
				if (ZoneList != null && ZoneList.size() > 0) 
				{
					Map tempMap = new HashMap();
					tempMap = (Map) ZoneList.get(0);
					regionCode = (Integer) tempMap.get("regioncode");
				}
				weighmentDetails.setRegioncode(regionCode);
				
				int mandalCode = 0;
				List VillageList = new ArrayList();
				VillageList = ahFuctionalService.getVillageDetails(caneWeighmentBean.getLandVillageCode());
				if (VillageList != null && VillageList.size() > 0) 
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VillageList.get(0);
					mandalCode = (Integer) tempMap.get("mandalcode");
				}
				weighmentDetails.setMandalcode(mandalCode);
				
				String plotNo = caneWeighmentBean.getPlotNumber();
				
				//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(caneWeighmentBean.getSeason(), caneWeighmentBean.getAgreementNumber());
				//Added by DMurty on 28-11-2016
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(caneWeighmentBean.getAgreementNumber(),plotNo,caneWeighmentBean.getSeason());
				
				weighmentDetails.setFieldassistantid(agreementDetails.get(0).getFieldassistantid());
				weighmentDetails.setFieldofficerid(agreementDetails.get(0).getFieldofficerid());
				
				//Added by DMurty on 23-11-2016
				String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
				Date shiftDate = DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT);
				weighmentDetails.setShiftdate(weighmentDetails.getCanereceiptdate());
				weighmentDetails.setActualserialnumber(0);
				
				//Added by DMurty on 28-11-2016
				Date currDate = new Date();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strcurrDate = df.format(currDate);
				currDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
				weighmentDetails.setSystemstartdate(currDate);
				
				weighmentDetails.setTransportallowance(0.0);
				weighmentDetails.setTransportallowancedistance(0.0);
			}
			else
			{
				String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
				String dt[] = date.split("-");
				String dd = dt[0];
				String mm = dt[1];
				String yy = dt[2];
				String finalDate = yy+"-"+mm+"-"+dd;
				
				Double grossWeight=weighmentDetails.getLorryandcanewt()-caneWeighmentBean.getTotalWeight()/1000;
				grossWeight =Double.parseDouble(new DecimalFormat("##.###").format(grossWeight));
				//added by naidu for binding material on 12-12-2016
				double gWeight=grossWeight;
				float gs=(float)gWeight;
				int bMaterial=bindingMaterial.intValue();
				//float bgm=(float)(gs*bMaterial)/100;
				/*
				 float bgm=(float)Math.round(((grossWeight*bindingMaterial)/100)*10000)/10000;
					double z=Math.round (bgm* 1000.000) / 1000.000;
					
					Double x=grossWeight-z;
				 */
				double fbindingMaterial1=(double)Math.round (((gs*bMaterial)/100)* 10000) / 10000;  
				 double fbindingMaterial=Math.round (fbindingMaterial1* 1000.000) / 1000.000;
				Double netCaneWeight=gWeight-fbindingMaterial;
				//Double netCaneWeight=grossWeight - (Math.round (bgm * 1000.00) / 1000.00);
				netCaneWeight =Double.parseDouble(new DecimalFormat("##.###").format(netCaneWeight));
				logger.info("binding material: "+fbindingMaterial);
				//Double netWeightAfterSecondWeighment=netCaneWeight-caneWeighmentBean.getTotalWeight();
				weighmentDetails.setSerialnumber(weighmentDetails.getSerialnumber());
				weighmentDetails.setNetwt(netCaneWeight);
				
				weighmentDetails.setCanereceiptenddate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				
				//Modified by DMurty on 01-12-2016
				int shiftId = getShiftId();
				weighmentDetails.setShiftid1(shiftId);
				
				Time time = DateUtils.getCurrentSystemTime();
				weighmentDetails.setCanereceiptendtime(time);
				
				double dtotalWt = caneWeighmentBean.getTotalWeight()/1000;
				dtotalWt =Double.parseDouble(new DecimalFormat("##.###").format(dtotalWt));
				weighmentDetails.setLorrywt(dtotalWt);
				
				weighmentDetails.setGrossweight(grossWeight);
				weighmentDetails.setStatus((byte)1);
				weighmentDetails.setWeighmentno(caneWeighmentBean.getWeighmentorder());

				Double  progressiveQty=0.00;
				Object progressiveQtyObj=caneReceiptFunctionalService.getProgressiveQuantityFromAgreement(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getSeason(),caneWeighmentBean.getPlotNumber());
				progressiveQty=(Double)progressiveQtyObj;
				if(progressiveQtyObj==null)
				{
					progressiveQty=0.00;
				}
				boolean  updatePQFlag=caneReceiptFunctionalService.updateProgressiveQuantity(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getSeason(),caneWeighmentBean.getPlotNumber(),netCaneWeight+progressiveQty);
				//Added by DMurty on 24-08-2016
				int programNo =  caneReceiptFunctionalService.getProgramNo(caneWeighmentBean.getPermitNumber());
				boolean  updatePermitFlag=caneReceiptFunctionalService.updatePermitDetailsStatus(caneWeighmentBean.getPermitNumber(),programNo,caneWeighmentBean.getSeason());
				
				weighmentDetails.setShiftdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				//Added by DMurty on 23-11-2016 
				Date receiptDate = weighmentDetails.getCanereceiptenddate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strreceiptDate = df.format(receiptDate);
				String dt1[] = date.split("-");
				String dd1 = dt1[0];
				String mm1 = dt1[1];
				String yy1 = dt1[2];
				strreceiptDate = yy1+"-"+mm1+"-"+dd1;
				
				int actualserialnumber = commonService.getMaxSlNoForWeighment(strreceiptDate,caneWeighmentBean.getSeason());
				weighmentDetails.setActualserialnumber(actualserialnumber);
				weighmentDetails.setTransactioncode(transactionCode);
				//Added by DMurty on 28-11-2016
				Date currDate = new Date();
				String strcurrDate = df.format(currDate);
				currDate = DateUtils.getSqlDateFromString(strcurrDate,Constants.GenericDateFormat.DATE_FORMAT);
				weighmentDetails.setSystemenddate(currDate);
				
				//Added by DMurty on 01-12-2016
				String landVillCode = caneWeighmentBean.getLandVillageCode();
				double distance = 0.0;
				List<SetupDetails> setupDetails = setupDetailsService.listSetupDetail();
				byte distanceFrom = 0;//setupDetails.get(0).getValue();
				if(distanceFrom == 0)
				{
					List VillageList = new ArrayList<String>();
					VillageList = ahFuctionalService.getVillageDetailsByCodeNew(landVillCode);
					if (VillageList != null && VillageList.size() > 0) 
					{
						Map tempMap = new HashMap();
						tempMap = (Map) VillageList.get(0);
						distance = (Double) tempMap.get("distance");
					}
				}
				else
				{
					List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getPlotNumber(),caneWeighmentBean.getSeason());
					distance = agreementDetails.get(0).getPlotdistance();
				}
				double nokms = 0.0;
				double allwnceperkms = 0.0;
				
				List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
				if(caneAcctRules != null && caneAcctRules.size()>0)
				{
					nokms = caneAcctRules.get(0).getNoofkms();
					allwnceperkms = caneAcctRules.get(0).getAllowanceperkmperton();
					allwnceperkms =Double.parseDouble(new DecimalFormat("##.##").format(allwnceperkms));
				}
				Double taDistance= nokms;
				Double villageDistance=distance;
				double diff = villageDistance-nokms;
				if(villageDistance>taDistance)
				{	
					Double netDistance=villageDistance-taDistance;
					double tAllowance = allwnceperkms*netDistance*netCaneWeight;
					tAllowance =Double.parseDouble(new DecimalFormat("##.##").format(tAllowance));
					logger.info("========entered to CaneReceiptShiftSmry()==========tAllowance"+tAllowance);

					weighmentDetails.setTransportallowance(tAllowance);
					weighmentDetails.setTransportallowancedistance(diff);
				}
				//Added by DMurty on 06-12-2016
				if(caneWeighmentBean.getUnloadingContractor() != null)
				{
					weighmentDetails.setUccode(caneWeighmentBean.getUnloadingContractor());
				}
				else
				{
					weighmentDetails.setUccode(0);
				}
				
				//As we are not using shiftname, we are posting user name for second weighment
				String userName = loginController.getLoggedInUserName();
				weighmentDetails.setShiftname(userName);
		}
		return weighmentDetails;
	}
	
	private CaneReceiptShiftSmry prepareModelForCaneReceiptShiftSmry(CaneWeighmentBean caneWeighmentBean,Double grossWeight,Double netWeight,double bindingMaterial) throws ParseException 
	{
		logger.info("========entered to CaneReceiptShiftSmry()==========");
		//CaneReceiptShiftSmry caneReceiptShiftSmry=(CaneReceiptShiftSmry) commonService.getEntityById(caneWeighmentBean.getShiftId(),CaneReceiptShiftSmry.class);
		CaneReceiptShiftSmry caneReceiptShiftSmry = null;
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List CaneReceiptShiftSmryList=caneReceiptFunctionalService.getCaneReceiptShiftSmryList(caneWeighmentBean.getShiftId(),finalDate);
		if(CaneReceiptShiftSmryList==null || CaneReceiptShiftSmryList.size()==0)
		{
			//----New
			caneReceiptShiftSmry = new CaneReceiptShiftSmry();	
			int partLoad = caneWeighmentBean.getPartLoad();
			if(partLoad == 0)
			{
				caneReceiptShiftSmry.setNoofpartloads(1);
			}
			else
			{
				caneReceiptShiftSmry.setNoofpartloads(0);
			}
			
			caneReceiptShiftSmry.setCanereceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			
			int shiftId = getShiftId();
			
			caneReceiptShiftSmry.setShiftid(shiftId);//cumulative based on shift
			caneReceiptShiftSmry.setNoofrects(1);
			//Added by DMurty on 03-11-2016
			caneReceiptShiftSmry.setSeason(caneWeighmentBean.getSeason());
			//Tipper means tractor,10 tyre means small lorries and 12 tyres means big lorries as per the discussion with sir on 26-11-2016
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptShiftSmry.setNooftippers(1);
				caneReceiptShiftSmry.setNooftentires(0);
				caneReceiptShiftSmry.setNooftwelvetires(0);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptShiftSmry.setNooftentires(1);
				caneReceiptShiftSmry.setNooftippers(0);
				caneReceiptShiftSmry.setNooftwelvetires(0);
			}
			else
			{
				caneReceiptShiftSmry.setNooftwelvetires(1);
				caneReceiptShiftSmry.setNooftippers(0);
				caneReceiptShiftSmry.setNooftentires(0);
			}
			caneReceiptShiftSmry.setNooflorries(0);

			caneReceiptShiftSmry.setNetwt(netWeight);
			caneReceiptShiftSmry.setGrosswt(grossWeight);
			caneReceiptShiftSmry.setBindingmtlwt(bindingMaterial);
			//Modified by DMurty on 26-11-2016
			if(caneWeighmentBean.getBurntCane()==0)
			{
				caneReceiptShiftSmry.setBurnedcanewt(netWeight);
			}
			else
			{
				caneReceiptShiftSmry.setBurnedcanewt(0.0);
			}
		}
		else
		{		
			//--- Cumulative---//
			caneReceiptShiftSmry = (CaneReceiptShiftSmry) CaneReceiptShiftSmryList.get(0);
			caneReceiptShiftSmry.setId(caneReceiptShiftSmry.getId());
			
			//Modified by DMurty on 04-12-2016
			int shiftId = getShiftId();
			caneReceiptShiftSmry.setShiftid(shiftId);
			
			//No of lorries set 0 as per discussion with sir.
			caneReceiptShiftSmry.setNooflorries(0);			
			
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptShiftSmry.setNooftippers(caneReceiptShiftSmry.getNooftippers()+1);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptShiftSmry.setNooftentires(caneReceiptShiftSmry.getNooftentires()+1);
			}
			else
			{
				caneReceiptShiftSmry.setNooftwelvetires(caneReceiptShiftSmry.getNooftwelvetires()+1);
			}
			
			caneReceiptShiftSmry.setNetwt(caneReceiptShiftSmry.getNetwt()+netWeight);
			caneReceiptShiftSmry.setGrosswt(caneReceiptShiftSmry.getGrosswt()+grossWeight);
			caneReceiptShiftSmry.setBindingmtlwt(bindingMaterial);
			//Added by DMurty on 24-11-2016 
			caneReceiptShiftSmry.setNoofrects(caneReceiptShiftSmry.getNoofrects()+1);
			int partLoad = caneWeighmentBean.getPartLoad();
			if(partLoad == 0)
			{
				caneReceiptShiftSmry.setNoofpartloads(caneReceiptShiftSmry.getNoofpartloads()+1);
			}
			//Added by DMurty on 26-11-2016
			if(caneWeighmentBean.getBurntCane()==0)
			{
				caneReceiptShiftSmry.setBurnedcanewt(caneReceiptShiftSmry.getBurnedcanewt()+netWeight);
			}
		}
		return caneReceiptShiftSmry;
	}
	private CaneReceiptDaySmry prepareModelForCaneReceiptDaySmry(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,Double grossWeight,Double netWeight) 
	{
		logger.info("========entered to prepareModelForCaneReceiptDaySmry()==========");

		List caneReceiptDaySmryList=caneReceiptFunctionalService.getCaneReceiptDaySmryByDate(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()));
		CaneReceiptDaySmry caneReceiptDaySmry =null;

		if(caneReceiptDaySmryList!=null)
		{
			//--- Cumulative---//
			caneReceiptDaySmry=(CaneReceiptDaySmry)caneReceiptDaySmryList.get(0);
			double grossWt = caneReceiptDaySmry.getGrosswt();
			caneReceiptDaySmry.setNoofrects(caneReceiptDaySmry.getNoofrects()+1);
			caneReceiptDaySmry.setId(caneReceiptDaySmry.getId());			
			
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptDaySmry.setNooftippers(caneReceiptDaySmry.getNooftippers()+1);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptDaySmry.setNooftentires(caneReceiptDaySmry.getNooftentires()+1);
			}
			else
			{
				caneReceiptDaySmry.setNooftwelvetires(caneReceiptDaySmry.getNooftwelvetires()+1);		
			}
			//Modified by DMurty on 07-10-2016
			caneReceiptDaySmry.setGrosswt(grossWeight+grossWt);
			caneReceiptDaySmry.setNetwt(netWeight+caneReceiptDaySmry.getNetwt());
			caneReceiptDaySmry.setId(caneReceiptDaySmry.getId());
			
			//Modified by DMurty on 26-11-2016
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptDaySmry.setNoofpartloads(caneReceiptDaySmry.getNoofpartloads()+1);
			}
			caneReceiptDaySmry.setNooflorries(0);
			//Added by DMurty on 26-11-2016
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptDaySmry.setBurnedcanewt(caneReceiptDaySmry.getBurnedcanewt()+netWeight);
			}
			else
			{
				caneReceiptDaySmry.setBurnedcanewt(0.0);
			}
		}
		else
		{
			caneReceiptDaySmry=new CaneReceiptDaySmry();
			caneReceiptDaySmry.setBindingmtlwt(bindingMaterial);
			//Modified by DMurty on 26-11-2016
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptDaySmry.setBurnedcanewt(netWeight);
			}
			else
			{
				caneReceiptDaySmry.setBurnedcanewt(0.0);
			}
			
			caneReceiptDaySmry.setCanereceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));//cumulative based on date
			caneReceiptDaySmry.setGrosswt(grossWeight);
			caneReceiptDaySmry.setNetwt(netWeight);
			caneReceiptDaySmry.setNoofrects(1);
			//Added by DMurty on 03-11-2016
			caneReceiptDaySmry.setSeason(caneWeighmentBean.getSeason());
			
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptDaySmry.setNooftippers(1);
				caneReceiptDaySmry.setNooftentires(0);
				caneReceiptDaySmry.setNooftwelvetires(0);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptDaySmry.setNooftentires(1);
				caneReceiptDaySmry.setNooftippers(0);
				caneReceiptDaySmry.setNooftwelvetires(0);
			}
			else
			{
				caneReceiptDaySmry.setNooftwelvetires(1);	
				caneReceiptDaySmry.setNooftippers(0);
				caneReceiptDaySmry.setNooftentires(0);
			}
			//Modified by DMurty on 26-11-2016 as per the discussion with sir
			caneReceiptDaySmry.setNooflorries(0);
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptDaySmry.setNoofpartloads(1);
			}
			else
			{
				caneReceiptDaySmry.setNoofpartloads(0);
			}
		}	
		return caneReceiptDaySmry;
	}
	//Added by DMurty on 04-08-2016
	public List getSumofAccruedButNotDue(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,Double grossWeight,Double netWeight)
	{
		List FinalList = new ArrayList();
		double sum = 0.0;
		double frp = 0.0;
		byte frpAcrued = 0;
		double purchaseTax = 0.0;
		byte purchaseTaxAcrued = 0;
		double harvestingSubsidy = 0.0;
		byte harvestingSubsidyAcrued = 0;
		double tpSubsidy = 0.0;
		byte tpSubsidyAcrued = 0;
		double addCp = 0.0;
		byte addCpAcrued = 0;
		double seasonCanePrice = 0.0;
		double accruedButNotDueRate = 0.0;
		double accruedButNotDueAmount = 0.0;
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			accruedButNotDueRate = caneAcctRules.get(0).getSeasoncaneprice();
			
			/*List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			frp = caneAcctAmounts.get(0).getNetamt();
			frpAcrued = caneAcctAmounts.get(0).getConsiderforseasonacts();
			
			List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			purchaseTax = purcaneAcctAmounts.get(0).getAmount();
			purchaseTaxAcrued = purcaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> HscaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Harvesting Subsidy");
			harvestingSubsidy = HscaneAcctAmounts.get(0).getAmount();
			harvestingSubsidyAcrued = HscaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> TpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Transport Subsidy");
			tpSubsidy = TpcaneAcctAmounts.get(0).getAmount();
			tpSubsidyAcrued = TpcaneAcctAmounts.get(0).getConsiderforseasonacts();

			List <CaneAcctAmounts> addCpcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			addCp = addCpcaneAcctAmounts.get(0).getAmount();
			addCpAcrued = addCpcaneAcctAmounts.get(0).getConsiderforseasonacts();*/
		}
		/*if(frp>0)
		{
			if(frpAcrued == 1)
			{
				accruedButNotDueRate = accruedButNotDueRate+frp;
			}
		}
		if(purchaseTax>0)
		{
			if(purchaseTaxAcrued == 1)
			{
				accruedButNotDueRate = accruedButNotDueRate+purchaseTax;
			}
		}
		if(harvestingSubsidy>0)
		{
			if(harvestingSubsidyAcrued == 1)
			{
				accruedButNotDueRate = accruedButNotDueRate+harvestingSubsidy;
			}
		}
		if(tpSubsidy>0)
		{
			if(tpSubsidyAcrued == 1)
			{
				accruedButNotDueRate = accruedButNotDueRate+tpSubsidyAcrued;
			}
		}
		if(addCp>0)
		{
			if(addCpAcrued == 1)
			{
				accruedButNotDueRate = accruedButNotDueRate+addCpAcrued;
			}
		}*/
		accruedButNotDueAmount = accruedButNotDueRate*netWeight;
		
		Map tempMap = new HashMap();
		tempMap.put("ACCRUED_BUTNOT_DUE_RATE", accruedButNotDueRate);
		tempMap.put("ACCRUED_BUTNOT_DUE_AMOUNT", accruedButNotDueAmount);
		FinalList.add(tempMap);
		
		return FinalList;
	}
	
	private CaneReceiptDaySmryByRyot prepareModelForCaneReceiptDaySmryByRyot(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,Double grossWeight,Double netWeight) 
	{   
		logger.info("========entered to prepareModelForCaneReceiptDaySmryByRyot()==========");
		CaneReceiptDaySmryByRyot caneReceiptDaySmryByRyot=null;
		
		double accruedButNotDueRate = 0.0;
		double accruedButNotDueAmount = 0.0; 
		
		List AccruedButNotDueList = getSumofAccruedButNotDue(caneWeighmentBean,bindingMaterial,grossWeight,netWeight);
		if(AccruedButNotDueList != null && AccruedButNotDueList.size()>0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) AccruedButNotDueList.get(0);
			accruedButNotDueRate = (Double) tempMap.get("ACCRUED_BUTNOT_DUE_RATE");
			accruedButNotDueRate =Double.parseDouble(new DecimalFormat("##.##").format(accruedButNotDueRate));

			accruedButNotDueAmount = (Double) tempMap.get("ACCRUED_BUTNOT_DUE_AMOUNT");
			accruedButNotDueAmount =Double.parseDouble(new DecimalFormat("##.##").format(accruedButNotDueAmount));
		}
		
		//Modified by DMurty on 06-10-2016
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		//Added by DMurty on 25-11-2016
		double harvestingRate = 0.0;
		int harvestingContractor = 0;
		List harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(harvestingRateDetailsList!=null)
		{
			HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
			harvestingRate = harvestingRateDetails.getRate();
			harvestingContractor = harvestingRateDetails.getHarvestcontcode();
		}
		double harvestCharges = harvestingRate*netWeight;
		harvestCharges =Double.parseDouble(new DecimalFormat("##.##").format(harvestCharges));

		double transportRate = 0.0;
		int transportContractor = 0;
		List transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(transportingRateDetailsList!=null && transportingRateDetailsList.size()>0)
		{ 
			TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
			transportRate = transportingRateDetails.getRate();
			transportContractor = transportingRateDetails.getTransportcontcode();
		}
		double transportCharges = transportRate*netWeight;
		transportCharges =Double.parseDouble(new DecimalFormat("##.##").format(transportCharges));

		List caneReceiptDaySmryByRyotList=caneReceiptFunctionalService.getCaneReceiptDaySmryByRyot(caneWeighmentBean.getRyotCode(),finalDate);
		if(caneReceiptDaySmryByRyotList!=null)
		{
			//--- Cumulative---//
			caneReceiptDaySmryByRyot=(CaneReceiptDaySmryByRyot)caneReceiptDaySmryByRyotList.get(0);
			double accBtNtDueRate =0;
			double accBtNtDueAmt =0;
			if(caneReceiptDaySmryByRyot.getAccruedbutnotduerate() != null)
			{
				accBtNtDueRate = caneReceiptDaySmryByRyot.getAccruedbutnotduerate();
				accBtNtDueRate =Double.parseDouble(new DecimalFormat("##.##").format(accBtNtDueRate));
			}
			if(caneReceiptDaySmryByRyot.getAccruedbutnotdueamount() != null)
			{
				accBtNtDueAmt = caneReceiptDaySmryByRyot.getAccruedbutnotdueamount();
				accBtNtDueAmt =Double.parseDouble(new DecimalFormat("##.##").format(accBtNtDueAmt));
			}
			
			caneReceiptDaySmryByRyot.setNetwt(netWeight+caneReceiptDaySmryByRyot.getNetwt());
			caneReceiptDaySmryByRyot.setTotalextentsize(caneWeighmentBean.getExtentSize());//+caneReceiptDaySmryByRyot.getTotalextentsize());
			caneReceiptDaySmryByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
			//Added by DMurty on 04-08-2016
			caneReceiptDaySmryByRyot.setAccruedbutnotdueamount(accBtNtDueAmt+accruedButNotDueAmount);
			caneReceiptDaySmryByRyot.setAccruedbutnotduerate(accruedButNotDueRate);
			//Added by DMurty on 25-11-2016
			caneReceiptDaySmryByRyot.setNoofrects(caneReceiptDaySmryByRyot.getNoofrects()+1);
			
			//Added by DMurty on 25-11-2016
			caneReceiptDaySmryByRyot.setId(caneReceiptDaySmryByRyot.getId());
			
			double tpCharges = caneReceiptDaySmryByRyot.getTransportcharges();
			tpCharges =Double.parseDouble(new DecimalFormat("##.##").format(tpCharges));
			
			double harCharges = caneReceiptDaySmryByRyot.getHarvestingcharges();
			harCharges =Double.parseDouble(new DecimalFormat("##.##").format(harCharges));

			caneReceiptDaySmryByRyot.setTransportcharges(transportCharges+tpCharges);
			caneReceiptDaySmryByRyot.setHarvestingcharges(harvestCharges+harCharges);
		}
		else
		{
			//---New--//
			
			caneReceiptDaySmryByRyot = new CaneReceiptDaySmryByRyot();
			caneReceiptDaySmryByRyot.setCanereceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			caneReceiptDaySmryByRyot.setCastatus((byte)0);
			caneReceiptDaySmryByRyot.setCirclecode(caneWeighmentBean.getCircleCode());
			caneReceiptDaySmryByRyot.setNetwt(netWeight);
			caneReceiptDaySmryByRyot.setNoofrects(1);
			caneReceiptDaySmryByRyot.setRyotcode(caneWeighmentBean.getRyotCode());//cumulative based on ryotcode
			caneReceiptDaySmryByRyot.setTotalextentsize(caneWeighmentBean.getExtentSize());
			
			//Modified by DMurty on 25-11-2016
			caneReceiptDaySmryByRyot.setHarvestcontcode(harvestingContractor);
			caneReceiptDaySmryByRyot.setHarvestingcharges(harvestCharges);
			caneReceiptDaySmryByRyot.setTranspcontcode(transportContractor);
			caneReceiptDaySmryByRyot.setTransportcharges(transportCharges);	
			
			//Added by DMurty on 04-08-2016
			caneReceiptDaySmryByRyot.setAccruedbutnotduerate(accruedButNotDueRate);
			caneReceiptDaySmryByRyot.setAccruedbutnotdueamount(accruedButNotDueAmount);
			//Added by DMurty on 03-11-2016
			caneReceiptDaySmryByRyot.setSeason(caneWeighmentBean.getSeason());
		}
		return caneReceiptDaySmryByRyot;
	}
	
	private CaneReceiptSeasonSmry prepareModelForCaneReceiptSeasonSmry(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,Double grossWeight,Double netWeight) 
	{
		logger.info("========entered to prepareModelForCaneReceiptSeasonSmry()==========");
		CaneReceiptSeasonSmry caneReceiptSeasonSmry =null;
		//Double netWeight=caneWeighmentBean.getTotalWeight()-(caneWeighmentBean.getTotalWeight()*(bindingMaterial/100));

		List caneReceiptSeasonSmryList=caneReceiptFunctionalService.getCaneReceiptSeasonSmry(caneWeighmentBean.getSeason());
		if(caneReceiptSeasonSmryList!=null)
		{
			//--- Cumulative---//
			caneReceiptSeasonSmry=(CaneReceiptSeasonSmry)caneReceiptSeasonSmryList.get(0);
			
			caneReceiptSeasonSmry.setSeason(caneWeighmentBean.getSeason());
			caneReceiptSeasonSmry.setGrosswt(caneReceiptSeasonSmry.getGrosswt()+grossWeight);
			caneReceiptSeasonSmry.setNetwt(netWeight+caneReceiptSeasonSmry.getNetwt());
			//Modified by DMurty on 25-11-2016
			caneReceiptSeasonSmry.setNooflorries(0);
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptSeasonSmry.setNoofpartloads(caneReceiptSeasonSmry.getNoofpartloads()+1);
			}
			caneReceiptSeasonSmry.setNoofrects(caneReceiptSeasonSmry.getNoofrects()+1);
			//Modified by DMurty on 18-10-2016
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptSeasonSmry.setNooftractors(caneReceiptSeasonSmry.getNooftractors()+1);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptSeasonSmry.setNoofsmalllorries(caneReceiptSeasonSmry.getNoofsmalllorries()+1);
			}
			else
			{
				caneReceiptSeasonSmry.setNoofbiglorries(caneReceiptSeasonSmry.getNoofbiglorries()+1);
			}
			
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptSeasonSmry.setBurnedcanewt(caneReceiptSeasonSmry.getBurnedcanewt()+netWeight);
			}
		}
		else
		{
			//--New--
			caneReceiptSeasonSmry = new CaneReceiptSeasonSmry();	
			caneReceiptSeasonSmry.setBindingmtlwt(bindingMaterial);
			caneReceiptSeasonSmry.setGrosswt(grossWeight);
			caneReceiptSeasonSmry.setNetwt(netWeight);
			caneReceiptSeasonSmry.setNooflorries(0);
			
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptSeasonSmry.setNoofpartloads(1);
			}
			else
			{
				caneReceiptSeasonSmry.setNoofpartloads(0);
			}
			caneReceiptSeasonSmry.setNoofrects(1);
			
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptSeasonSmry.setNooftractors(1);
				caneReceiptSeasonSmry.setNoofsmalllorries(0);
				caneReceiptSeasonSmry.setNoofbiglorries(0);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptSeasonSmry.setNooftractors(0);
				caneReceiptSeasonSmry.setNoofsmalllorries(1);
				caneReceiptSeasonSmry.setNoofbiglorries(0);
			}
			else
			{
				caneReceiptSeasonSmry.setNooftractors(0);
				caneReceiptSeasonSmry.setNoofsmalllorries(0);
				caneReceiptSeasonSmry.setNoofbiglorries(1);
			}
			//Modified by DMurty on 26-11-2016
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptSeasonSmry.setBurnedcanewt(netWeight);
			}
			else
			{
				caneReceiptSeasonSmry.setBurnedcanewt(0.0);
			}
			
			caneReceiptSeasonSmry.setSeason(caneWeighmentBean.getSeason());//cumulative based on season
		}
		return caneReceiptSeasonSmry;
	}
	
	private CaneReceiptWBBook prepareModelForCaneReceiptWBBook(CaneWeighmentBean caneWeighmentBean,Double bindingMaterial,Double grossWeight,Double netWeight) 
	{
		logger.info("========entered to prepareModelForCaneReceiptWBBook()==========");
		CaneReceiptWBBook caneReceiptWBBook = null;
		List  caneReceiptWBBookList=caneReceiptFunctionalService.getCaneReceiptWBBook(caneWeighmentBean.getWeighBridgeId());
		//Double netWeight=caneWeighmentBean.getTotalWeight()-(caneWeighmentBean.getTotalWeight()*(bindingMaterial/100));
		if(caneReceiptWBBookList!=null)
		{
			//--- Cumulative---//
			caneReceiptWBBook=(CaneReceiptWBBook)caneReceiptWBBookList.get(0);
			caneReceiptWBBook.setWeighbridgeid(caneReceiptWBBook.getWeighbridgeid());
			caneReceiptWBBook.setGrosswt(caneReceiptWBBook.getGrosswt()+grossWeight);			
			caneReceiptWBBook.setNetwt(caneReceiptWBBook.getNetwt()+netWeight);
			caneReceiptWBBook.setNooflorries(0);
			//Modified by DMurty on 25-11-2016
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptWBBook.setNoofpartloads(caneReceiptWBBook.getNoofpartloads()+1);
			}
			caneReceiptWBBook.setNoofrects(caneReceiptWBBook.getNoofrects()+1);
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptWBBook.setNooftractors(caneReceiptWBBook.getNooftractors()+1);
			}
			else if(caneWeighmentBean.getVehicle()==1)
			{
				caneReceiptWBBook.setNoofsmalllorries(caneReceiptWBBook.getNoofsmalllorries()+1);
			}
			else
			{
				caneReceiptWBBook.setNoofbiglorries(caneReceiptWBBook.getNoofbiglorries()+1);
			}
			
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptWBBook.setBurnedcanewt(caneReceiptWBBook.getBurnedcanewt()+netWeight);
			}
		}
		else
		{
			caneReceiptWBBook = new CaneReceiptWBBook();		
			caneReceiptWBBook.setBindingmtlwt(bindingMaterial);
			caneReceiptWBBook.setGrosswt(grossWeight);
			caneReceiptWBBook.setNetwt(netWeight);
			caneReceiptWBBook.setNooflorries(0);
			
			//Modified by DMurty on 25-11-2016
			int isPartLoad = caneWeighmentBean.getPartLoad();
			if(isPartLoad == 0)
			{
				caneReceiptWBBook.setNoofpartloads(1);
			}
			else
			{
				caneReceiptWBBook.setNoofpartloads(0);
			}
			caneReceiptWBBook.setNoofrects(1);	
			if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptWBBook.setNooftractors(1);
				caneReceiptWBBook.setNoofsmalllorries(0);
				caneReceiptWBBook.setNoofbiglorries(0);
			}
			else if(caneWeighmentBean.getVehicle()==0)
			{
				caneReceiptWBBook.setNooftractors(0);
				caneReceiptWBBook.setNoofsmalllorries(1);
				caneReceiptWBBook.setNoofbiglorries(0);
			}
			else
			{
				caneReceiptWBBook.setNooftractors(0);
				caneReceiptWBBook.setNoofsmalllorries(0);
				caneReceiptWBBook.setNoofbiglorries(1);
			}
			if(caneWeighmentBean.getBurntCane() == 0)
			{
				caneReceiptWBBook.setBurnedcanewt(netWeight);
			}
			else
			{
				caneReceiptWBBook.setBurnedcanewt(0.0);
			}

			caneReceiptWBBook.setWeighbridgeid(caneWeighmentBean.getWeighBridgeId());//cumulative based on weighbridgeid
		}
		return caneReceiptWBBook;
	}
	
	private TransportAllowanceDetails prepareModelForTransportAllowanceDetails(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForTransportAllowanceDetails()==========");
		//Added by DMurty on 24-08-2016
		String landVillCode = caneWeighmentBean.getLandVillageCode();
		double distance = 0.0;
		List<SetupDetails> setupDetails = setupDetailsService.listSetupDetail();
		byte distanceFrom = 0;//setupDetails.get(0).getValue();
		if(distanceFrom == 0)
		{
			List VillageList = new ArrayList<String>();
			VillageList = ahFuctionalService.getVillageDetailsByCodeNew(landVillCode);
			if (VillageList != null && VillageList.size() > 0) 
			{
				Map tempMap = new HashMap();
				tempMap = (Map) VillageList.get(0);
				distance = (Double) tempMap.get("distance");
			}
		}
		else
		{
			//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(caneWeighmentBean.getSeason(), caneWeighmentBean.getAgreementNumber());
			List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(caneWeighmentBean.getAgreementNumber(),caneWeighmentBean.getPlotNumber(),caneWeighmentBean.getSeason());
			distance = agreementDetails.get(0).getPlotdistance();
		}
		
		double nokms = 0.0;
		double allwnceperkms = 0.0;
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			nokms = caneAcctRules.get(0).getNoofkms();
			allwnceperkms = caneAcctRules.get(0).getAllowanceperkmperton();
			allwnceperkms =Double.parseDouble(new DecimalFormat("##.##").format(allwnceperkms));

			int calcid = caneAcctRules.get(0).getCalcid();
			//List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
		}

		TransportAllowanceDetails transportAllowanceDetails=null;
		if(caneAcctRules.size()>0 &&caneAcctRules!=null)
		{			
			Double taDistance= nokms;//caneAccountingRule.getNoofkms();
			//Modified by DMurty on 11-07-2016
			Double villageDistance=distance;//village.getDistance();
			double diff = villageDistance-nokms;
			if(villageDistance>taDistance)
			{
				List  transportAllowanceDetailsList=caneReceiptFunctionalService.getTransportAllowanceDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),caneWeighmentBean.getPlotNumber(),finalDate);
				Double netDistance=villageDistance-taDistance;
				
				if(transportAllowanceDetailsList!=null)
				{
					transportAllowanceDetails=(TransportAllowanceDetails)transportAllowanceDetailsList.get(0);				
					
					double tAllowance = allwnceperkms*netDistance*netCaneWeight;
					double prevTAmount = transportAllowanceDetails.getAmount();
					
					double finalAmt = tAllowance+prevTAmount;
					finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

					transportAllowanceDetails.setId(transportAllowanceDetails.getId());
					transportAllowanceDetails.setSuppliedcanewt(transportAllowanceDetails.getSuppliedcanewt()+netCaneWeight);
					transportAllowanceDetails.setAmount(finalAmt);
					transportAllowanceDetails.setId(transportAllowanceDetails.getId());
				}
				else
				{
					transportAllowanceDetails = new TransportAllowanceDetails();	
					transportAllowanceDetails.setAgreementno(caneWeighmentBean.getAgreementNumber());
					transportAllowanceDetails.setAllowance(allwnceperkms);
					
					double tAllowance = allwnceperkms*netDistance*netCaneWeight;
					tAllowance =Double.parseDouble(new DecimalFormat("##.##").format(tAllowance));

					transportAllowanceDetails.setAmount(tAllowance);
					transportAllowanceDetails.setDistance(diff);
					transportAllowanceDetails.setLandvillagecode(caneWeighmentBean.getLandVillageCode());
					transportAllowanceDetails.setSeason(caneWeighmentBean.getSeason());
					transportAllowanceDetails.setRyotcode(caneWeighmentBean.getRyotCode());
					transportAllowanceDetails.setSuppliedcanewt(netCaneWeight);
					transportAllowanceDetails.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
					transportAllowanceDetails.setPlotnumber(caneWeighmentBean.getPlotNumber());
					transportAllowanceDetails.setReceipttime(java.sql.Time.valueOf(caneWeighmentBean.getCaneWeighmentTime()));
				}
			}
		}
		return transportAllowanceDetails;
	}
	private AdditionalCaneValue prepareModelForAdditionalCaneValue(CaneWeighmentBean caneWeighmentBean,double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForAdditionalCaneValue()==========");
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		double additionalcp = 0.0;
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			additionalcp = caneAcctAmounts.get(0).getAmount();
			additionalcp =Double.parseDouble(new DecimalFormat("##.##").format(additionalcp));
		}
		
		AdditionalCaneValue additionalCaneValue =null;
		if(caneAcctRules.size()>0 && caneAcctRules!=null)
		{
			List additionalCaneValues=caneReceiptFunctionalService.getAdditionalCaneValue(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
			if(additionalCaneValues!=null && additionalCaneValues.size()>0)
			{
				additionalCaneValue=(AdditionalCaneValue)additionalCaneValues.get(0);
				additionalCaneValue.setSeason(additionalCaneValue.getSeason());
				
				double suppliedcnwt = additionalCaneValue.getSuppliedcanewt();
				double totalwt = suppliedcnwt+netCaneWeight;
				
				double addCaneVal = additionalCaneValue.getAdditionalcanevalue();
				double val = netCaneWeight*additionalcp;
				double finalAddCaneVal = val+addCaneVal;
				finalAddCaneVal =Double.parseDouble(new DecimalFormat("##.##").format(finalAddCaneVal));
				
				additionalCaneValue.setSuppliedcanewt(totalwt);				
				additionalCaneValue.setAdditionalcanevalue(finalAddCaneVal);
			}
			else
			{
				additionalCaneValue=new AdditionalCaneValue();
				additionalCaneValue.setAdditionalcaneprice(additionalcp);

				additionalCaneValue.setCanesupplieddt(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				additionalCaneValue.setRyotcode(caneWeighmentBean.getRyotCode());
				additionalCaneValue.setSeason(caneWeighmentBean.getSeason());
				additionalCaneValue.setSuppliedcanewt(netCaneWeight);
				double finalAddCaneVal = netCaneWeight*additionalcp;
				finalAddCaneVal =Double.parseDouble(new DecimalFormat("##.##").format(finalAddCaneVal));
				additionalCaneValue.setAdditionalcanevalue(finalAddCaneVal);
			}	
		}
		return additionalCaneValue;
	}
	
	private PostSeasonValueSummary prepareModelForPostSeasonValueSummary(CaneWeighmentBean caneWeighmentBean,double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForPostSeasonValueSummary()==========");
		//Modified by DMurty on 13-07-2016
		double additionalcp = 0.0;
		double nokms = 0.0;
		double allwnceperkms = 0.0;
		
		//Added by DMurty on 24-08-2016
		String landVillCode = caneWeighmentBean.getLandVillageCode();
		double distance = 0.0;
		List VillageList = new ArrayList<String>();
		VillageList = ahFuctionalService.getVillageDetailsByCodeNew(landVillCode);
		if (VillageList != null && VillageList.size() > 0) 
		{
			Map tempMap = new HashMap();
			tempMap = (Map) VillageList.get(0);
			distance = (Double) tempMap.get("distance");
		}
		
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			nokms = caneAcctRules.get(0).getNoofkms();
			allwnceperkms = caneAcctRules.get(0).getAllowanceperkmperton();
			allwnceperkms =Double.parseDouble(new DecimalFormat("##.##").format(allwnceperkms));

			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Additional Cane Price");
			additionalcp = caneAcctAmounts.get(0).getAmount();
			additionalcp =Double.parseDouble(new DecimalFormat("##.##").format(additionalcp));
		}
		Double netDistance=0.0;
		if(distance>nokms)
		{
			netDistance = distance-nokms;
		}
		
		List<Ryot> ryots=ryotService.getRyotByRyotCode(caneWeighmentBean.getRyotCode());
		Ryot ryot=ryots.get(0);
		
		PostSeasonValueSummary postSeasonValueSummary =null;
		List  postSeasonValueSummaryList=caneReceiptFunctionalService.getPostSeasonValueSummary(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(postSeasonValueSummaryList!=null && postSeasonValueSummaryList.size()>0)
		{
			postSeasonValueSummary=(PostSeasonValueSummary)postSeasonValueSummaryList.get(0);
			postSeasonValueSummary.setId(postSeasonValueSummary.getId());
			postSeasonValueSummary.setSuppliedcanewt(postSeasonValueSummary.getSuppliedcanewt()+netCaneWeight);
			//Added by DMurty on 24-08-2016
			double transpAlwnce = postSeasonValueSummary.getTransportallowance();
			double ntpAlwnce = allwnceperkms*netDistance*netCaneWeight;
			
			double finalTp = transpAlwnce+ntpAlwnce;
			finalTp =Double.parseDouble(new DecimalFormat("##.##").format(finalTp));
			postSeasonValueSummary.setTransportallowance(finalTp);
			
			//Added by DMurty on 07-10-2016
			double pendingAmt = postSeasonValueSummary.getPendingamount();
			double totalValue = postSeasonValueSummary.getTotalvalue();
			
			double currPendingAmt = additionalcp*netCaneWeight;
			double currTotalVal = additionalcp*netCaneWeight;
			
			double finalPending = currPendingAmt+pendingAmt;
			finalPending =Double.parseDouble(new DecimalFormat("##.##").format(finalPending));

			double finalTotalVal = currTotalVal+totalValue;
			finalTotalVal =Double.parseDouble(new DecimalFormat("##.##").format(finalTotalVal));

			postSeasonValueSummary.setPendingamount(finalPending);
			postSeasonValueSummary.setTotalvalue(finalTotalVal);
		}
		else
		{
			postSeasonValueSummary = new PostSeasonValueSummary();				
			postSeasonValueSummary.setRyotcode(caneWeighmentBean.getRyotCode());
			postSeasonValueSummary.setSeason(caneWeighmentBean.getSeason());
			postSeasonValueSummary.setVillagecode(caneWeighmentBean.getVillageCode());
			postSeasonValueSummary.setSuppliedcanewt(netCaneWeight);
			postSeasonValueSummary.setPaidamount(0.00);
			postSeasonValueSummary.setAdditionalcanevalue(additionalcp);
			
			double tAllowance = allwnceperkms*netDistance*netCaneWeight;
			tAllowance =Double.parseDouble(new DecimalFormat("##.##").format(tAllowance));
			postSeasonValueSummary.setTransportallowance(tAllowance);
			
			//Added by DMurty on 07-10-2016
			postSeasonValueSummary.setRyotcodeseqno(ryot.getRyotcodesequence());
			
			double addCp = additionalcp*netCaneWeight;
			addCp = Double.parseDouble(new DecimalFormat("##.##").format(addCp));
			postSeasonValueSummary.setPendingamount(addCp);
			
			double totalVal = additionalcp*netCaneWeight;
			totalVal =Double.parseDouble(new DecimalFormat("##.##").format(totalVal));
			postSeasonValueSummary.setTotalvalue(totalVal);
		}
		return postSeasonValueSummary;
	}
	
	private HarvChgDtlsByRyotContAndDate prepareModelForHarvChgDtlsByRyotContAndDate(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		logger.info("========entered to prepareModelForHarvChgDtlsByRyotContAndDate()==========");
		HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate =null;
		double harvestingRate = 0.0;
		double totalAmount= 0.0;
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		List harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(harvestingRateDetailsList!=null)
		{
			HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
			harvestingRate = harvestingRateDetails.getRate();
			harvestingRate =Double.parseDouble(new DecimalFormat("##.##").format(harvestingRate));
		}
		
		double totAmt = 0.0;
		List harvChgDtlsByRyotContAndDateList=caneReceiptFunctionalService.getHarvChgDtlsByRyotContAndDate(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),harvestingRate,finalDate);
		if(harvChgDtlsByRyotContAndDateList != null && harvChgDtlsByRyotContAndDateList.size()>0)
		{
			HarvChgDtlsByRyotContAndDate harvChgDtlsByRyotContAndDate1 =(HarvChgDtlsByRyotContAndDate)harvChgDtlsByRyotContAndDateList.get(0);
			totAmt = harvChgDtlsByRyotContAndDate1.getTotalamount();	
			totAmt =Double.parseDouble(new DecimalFormat("##.##").format(totAmt));
		}
		
		if(harvChgDtlsByRyotContAndDateList==null)
		{
			if(harvestingRateDetailsList!=null)
			{
				HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				totalAmount=netWeight*harvestingRateDetails.getRate();
				totalAmount = totAmt+totalAmount;
				totalAmount =Double.parseDouble(new DecimalFormat("##.##").format(totalAmount));

				harvChgDtlsByRyotContAndDate = new HarvChgDtlsByRyotContAndDate();
				harvChgDtlsByRyotContAndDate.setHarvestcontcode(harvestingRateDetails.getHarvestcontcode());
				harvChgDtlsByRyotContAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				harvChgDtlsByRyotContAndDate.setSeason(caneWeighmentBean.getSeason());
				harvChgDtlsByRyotContAndDate.setHarvestedcanewt(netWeight);
				harvChgDtlsByRyotContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
		 		harvChgDtlsByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				harvChgDtlsByRyotContAndDate.setRate(harvestingRateDetails.getRate());
				harvChgDtlsByRyotContAndDate.setTotalamount(totalAmount);
				//Added by DMurty on 23-01-2017
				harvChgDtlsByRyotContAndDate.setCastatus((byte) 0);
			}
			else
			{
				harvChgDtlsByRyotContAndDate = new HarvChgDtlsByRyotContAndDate();	
				harvChgDtlsByRyotContAndDate.setHarvestcontcode(0);
				harvChgDtlsByRyotContAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				harvChgDtlsByRyotContAndDate.setSeason(caneWeighmentBean.getSeason());
				harvChgDtlsByRyotContAndDate.setHarvestedcanewt(netWeight);
				harvChgDtlsByRyotContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
		 		harvChgDtlsByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				harvChgDtlsByRyotContAndDate.setRate(0.00);
				harvChgDtlsByRyotContAndDate.setTotalamount(0.00);
				//Added by DMurty on 23-01-2017
				harvChgDtlsByRyotContAndDate.setCastatus((byte) 0);
			}
		}
		else
		{
			harvChgDtlsByRyotContAndDate=(HarvChgDtlsByRyotContAndDate)harvChgDtlsByRyotContAndDateList.get(0);
			double suppliedWt = harvChgDtlsByRyotContAndDate.getHarvestedcanewt();
			double totalWt = netWeight+suppliedWt;
			harvChgDtlsByRyotContAndDate.setHarvestedcanewt(totalWt);
			harvChgDtlsByRyotContAndDate.setRate(harvestingRate);
			
			double prevTotal = harvChgDtlsByRyotContAndDate.getTotalamount();
			double total = harvestingRate*netWeight;
			
			double finalVal = total+prevTotal;
			finalVal = Double.parseDouble(new DecimalFormat("##.##").format(finalVal));
			harvChgDtlsByRyotContAndDate.setTotalamount(finalVal);
			harvChgDtlsByRyotContAndDate.setId(harvChgDtlsByRyotContAndDate.getId());
		}
		return harvChgDtlsByRyotContAndDate;
	}
	
	private HarvChgSmryByRyotAndCont prepareModelForHarvChgSmryByRyotAndCont(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		logger.info("========entered to prepareModelForHarvChgSmryByRyotAndCont()==========");
		HarvChgSmryByRyotAndCont harvChgSmryByRyotAndCont =null;
		List  harvChgSmryByRyotAndContList=caneReceiptFunctionalService.getHarvChgSmryByRyotAndCont(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(harvChgSmryByRyotAndContList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			if(harvestingRateDetailsList!=null && harvestingRateDetailsList.size()>0)
			{
				//if there is rate 
				HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				Double totalAmount=netWeight*harvestingRateDetails.getRate();
				totalAmount =Double.parseDouble(new DecimalFormat("##.##").format(totalAmount));

				harvChgSmryByRyotAndCont = new HarvChgSmryByRyotAndCont();
				harvChgSmryByRyotAndCont.setHarvestcontcode(harvestingRateDetails.getHarvestcontcode());
				harvChgSmryByRyotAndCont.setRyotcode(caneWeighmentBean.getRyotCode());
				harvChgSmryByRyotAndCont.setSeason(caneWeighmentBean.getSeason());
				//Modified by DMurty on 11-07-2016
				//harvChgSmryByRyotAndCont.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryByRyotAndCont.setHarvestedcanewt(netWeight);
				harvChgSmryByRyotAndCont.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryByRyotAndCont.setPendingAmount(totalAmount);
				harvChgSmryByRyotAndCont.setPendingpayable(0.0);
				harvChgSmryByRyotAndCont.setTotalamount(totalAmount);
				harvChgSmryByRyotAndCont.setTotalpayableamount(0.0);
				//Added by DMurty on 05-11-2016
				harvChgSmryByRyotAndCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
			else
			{
				//If there are no rates for ryot
				harvChgSmryByRyotAndCont = new HarvChgSmryByRyotAndCont();	
				harvChgSmryByRyotAndCont.setHarvestcontcode(0);
				harvChgSmryByRyotAndCont.setRyotcode(caneWeighmentBean.getRyotCode());
				harvChgSmryByRyotAndCont.setSeason(caneWeighmentBean.getSeason());
				//Modified by DMurty on 11-07-2016
				//harvChgSmryByRyotAndCont.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryByRyotAndCont.setHarvestedcanewt(netWeight);
				harvChgSmryByRyotAndCont.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryByRyotAndCont.setPendingAmount(0.00);
				harvChgSmryByRyotAndCont.setPendingpayable(0.00);
				harvChgSmryByRyotAndCont.setTotalamount(0.00);
				harvChgSmryByRyotAndCont.setTotalpayableamount(0.00);
				harvChgSmryByRyotAndCont.setCompanypaidamount(0.00);
				harvChgSmryByRyotAndCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
		}
		else
		{
			harvChgSmryByRyotAndCont=(HarvChgSmryByRyotAndCont)harvChgSmryByRyotAndContList.get(0);
			double suppliedWt = harvChgSmryByRyotAndCont.getHarvestedcanewt();
			double totalWt = netWeight+suppliedWt;
			harvChgSmryByRyotAndCont.setHarvestedcanewt(totalWt);
			
			double harvestingRate = 0.0;
			double totalAmount= 0.0;
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			if(harvestingRateDetailsList!=null)
			{
				HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				harvestingRate = harvestingRateDetails.getRate();
				totalAmount=netWeight*harvestingRate;
			}
			//Added by DMurty on 25-11-2016
			double pendingPayable = harvChgSmryByRyotAndCont.getPendingpayable();
			double pendingAmt = harvChgSmryByRyotAndCont.getPendingAmount();
			double totalPayable = harvChgSmryByRyotAndCont.getTotalpayableamount();
			double harvestAmt = harvChgSmryByRyotAndCont.getTotalamount();
			
			double finalTotalVal = totalAmount+harvestAmt;
			finalTotalVal =Double.parseDouble(new DecimalFormat("##.##").format(finalTotalVal));
			harvChgSmryByRyotAndCont.setTotalamount(finalTotalVal);
			
			double finalPending = totalAmount+pendingAmt;
			finalPending =Double.parseDouble(new DecimalFormat("##.##").format(finalPending));

			harvChgSmryByRyotAndCont.setPendingAmount(finalPending);
			
			//This will go and update record
			harvChgSmryByRyotAndCont.setId(harvChgSmryByRyotAndCont.getId());
		}
		return harvChgSmryByRyotAndCont;
	}
	
	private HarvChgSmryByContAndDate prepareModelForHarvChgSmryByContAndDate(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		
		HarvChgSmryByContAndDate harvChgSmryByContAndDate = null;
		List  harvChgSmryByContAndDateList=caneReceiptFunctionalService.getHarvChgSmryByContAndDate(caneWeighmentBean.getSeason());
		
		if(harvChgSmryByContAndDateList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(harvestingRateDetailsList!=null)
			{
				HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				 
				Double totalAmount=netWeight*harvestingRateDetails.getRate();

				harvChgSmryByContAndDate = new HarvChgSmryByContAndDate();				
				harvChgSmryByContAndDate.setHarvestcontcode(harvestingRateDetails.getHarvestcontcode());
				harvChgSmryByContAndDate.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				harvChgSmryByContAndDate.setSeason(caneWeighmentBean.getSeason());
				harvChgSmryByContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryByContAndDate.setTotalamount(totalAmount);				
			}
			else
			{

				harvChgSmryByContAndDate = new HarvChgSmryByContAndDate();				
				harvChgSmryByContAndDate.setHarvestcontcode(0);
				harvChgSmryByContAndDate.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				harvChgSmryByContAndDate.setSeason(caneWeighmentBean.getSeason());
				harvChgSmryByContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryByContAndDate.setTotalamount(0.00);
				return harvChgSmryByContAndDate;
			}
		}
		else
			harvChgSmryByContAndDate=(HarvChgSmryByContAndDate)harvChgSmryByContAndDateList.get(0);
		
		return harvChgSmryByContAndDate;
	}
	
	
	
	private HarvChgSmryBySeasonByCont prepareModelForHarvChgSmryBySeasonByCont(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		
		HarvChgSmryBySeasonByCont harvChgSmryBySeasonByCont = null;
		List  harvChgSmryBySeasonByContList=caneReceiptFunctionalService.getHarvChgSmryBySeasonByCont(caneWeighmentBean.getSeason());
		if(harvChgSmryBySeasonByContList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			
			if(harvestingRateDetailsList!=null)
			{
				HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				 
				Double totalAmount=netWeight*harvestingRateDetails.getRate();
				 
				harvChgSmryBySeasonByCont  = new HarvChgSmryBySeasonByCont();
				harvChgSmryBySeasonByCont.setHarvestcontcode(harvestingRateDetails.getHarvestcontcode());
				harvChgSmryBySeasonByCont.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryBySeasonByCont.setSeason(caneWeighmentBean.getSeason());
				harvChgSmryBySeasonByCont.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryBySeasonByCont.setPaidAmount(0.00);
				harvChgSmryBySeasonByCont.setPendingAmount(totalAmount);
				harvChgSmryBySeasonByCont.setTotalamount(totalAmount);	
				harvChgSmryBySeasonByCont.setPendingpayable(totalAmount);
				harvChgSmryBySeasonByCont.setTotalpayableamount(totalAmount);
				 
			}
			else
			{
				harvChgSmryBySeasonByCont  = new HarvChgSmryBySeasonByCont();	
				
				harvChgSmryBySeasonByCont.setHarvestcontcode(0);
				harvChgSmryBySeasonByCont.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryBySeasonByCont.setSeason(caneWeighmentBean.getSeason());
				harvChgSmryBySeasonByCont.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryBySeasonByCont.setPaidAmount(0.00);
				harvChgSmryBySeasonByCont.setPendingAmount(0.00);
				harvChgSmryBySeasonByCont.setTotalamount(0.00);		
				harvChgSmryBySeasonByCont.setPendingpayable(0.00);
				harvChgSmryBySeasonByCont.setTotalpayableamount(0.00);
				harvChgSmryBySeasonByCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
		}
		else
			harvChgSmryBySeasonByCont=(HarvChgSmryBySeasonByCont)harvChgSmryBySeasonByContList.get(0);
		
		return harvChgSmryBySeasonByCont ;
	}
	
	private HarvChgSmryBySeason  prepareModelForHarvChgSmryBySeason(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		HarvChgSmryBySeason  harvChgSmryBySeason  = null;
		
		List  harvChgSmryBySeasonList=caneReceiptFunctionalService.getHarvChgSmryBySeason(caneWeighmentBean.getSeason());
		
		if(harvChgSmryBySeasonList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  harvestingRateDetailsList=caneReceiptFunctionalService.getHarvestingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(harvestingRateDetailsList!=null)
			{
					HarvestingRateDetails harvestingRateDetails =(HarvestingRateDetails)harvestingRateDetailsList.get(0);
				 
				 	Double totalAmount=netWeight*harvestingRateDetails.getRate();
				 
					harvChgSmryBySeason  = new HarvChgSmryBySeason();	
					harvChgSmryBySeason.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
					harvChgSmryBySeason.setPaidAmount(0.00);
					harvChgSmryBySeason.setSeason(caneWeighmentBean.getSeason());
					harvChgSmryBySeason.setTotalextent(caneWeighmentBean.getExtentSize());
					harvChgSmryBySeason.setPendingpayable(totalAmount);
					harvChgSmryBySeason.setPendingAmount(totalAmount);
					harvChgSmryBySeason.setPaidAmount(0.00);
					harvChgSmryBySeason.setTotalpayableamount(totalAmount);
			}
			else
			{
				harvChgSmryBySeason  = new HarvChgSmryBySeason();	
				harvChgSmryBySeason.setHarvestedcanewt(caneWeighmentBean.getTotalWeight());
				harvChgSmryBySeason.setPaidAmount(0.00);
				harvChgSmryBySeason.setSeason(caneWeighmentBean.getSeason());
				harvChgSmryBySeason.setTotalextent(caneWeighmentBean.getExtentSize());
				harvChgSmryBySeason.setPendingpayable(0.00);
				harvChgSmryBySeason.setPendingAmount(0.00);
				harvChgSmryBySeason.setTotalamount(0.00);
				harvChgSmryBySeason.setTotalpayableamount(0.00);
			}
		}
		else
			harvChgSmryBySeason=(HarvChgSmryBySeason)harvChgSmryBySeasonList.get(0);
		return harvChgSmryBySeason;
	}
	
	private TranspChgSmryByRyotContAndDate prepareModelForTranspChgSmryByRyotContAndDate(CaneWeighmentBean caneWeighmentBean,Double netWeight, Double Roundedwt) 
	{
		logger.info("========entered to prepareModelForTranspChgSmryByRyotContAndDate()==========");
		double transportRate = 0.0;
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		List transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(transportingRateDetailsList!=null && transportingRateDetailsList.size()>0)
		{ 
			TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
			transportRate = transportingRateDetails.getRate();
			transportRate =Double.parseDouble(new DecimalFormat("##.##").format(transportRate));
		}
		
		double totAmt = 0.0;
		TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate =null;
		List transpChgSmryByRyotContAndDateList=caneReceiptFunctionalService.getTranspChgSmryByRyotContAndDate(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),transportRate,finalDate);
		if(transpChgSmryByRyotContAndDateList!=null && transpChgSmryByRyotContAndDateList.size()>0)
		{
			TranspChgSmryByRyotContAndDate transpChgSmryByRyotContAndDate1 =(TranspChgSmryByRyotContAndDate)transpChgSmryByRyotContAndDateList.get(0);
			totAmt = transpChgSmryByRyotContAndDate1.getTotalamount();
		}
		
		if(transpChgSmryByRyotContAndDateList==null)
		{
			if(transportingRateDetailsList!=null)
			{ 
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
				
				Double totalAmount=Roundedwt*transportingRateDetails.getRate();
				totalAmount = totAmt+totalAmount;
				totalAmount = (double)Math.round(totalAmount*100)/100;
				totalAmount =Double.parseDouble(new DecimalFormat("##.##").format(totalAmount));

				transpChgSmryByRyotContAndDate  = new TranspChgSmryByRyotContAndDate();	
				transpChgSmryByRyotContAndDate.setTransportcontcode(transportingRateDetails.getTransportcontcode());
				transpChgSmryByRyotContAndDate.setTransportedcanewt(netWeight);
				transpChgSmryByRyotContAndDate.setMincanewt(Roundedwt);
				transpChgSmryByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				transpChgSmryByRyotContAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				transpChgSmryByRyotContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
				
				double rate = transportingRateDetails.getRate();
				rate =Double.parseDouble(new DecimalFormat("##.##").format(rate));

				transpChgSmryByRyotContAndDate.setRate(rate);
				transpChgSmryByRyotContAndDate.setTotalamount(totalAmount);
				transpChgSmryByRyotContAndDate.setSeason(caneWeighmentBean.getSeason());
				//Added by DMurty on 23-01-2017
				transpChgSmryByRyotContAndDate.setCastatus((byte) 0);
			}
			else
			{
				transpChgSmryByRyotContAndDate  = new TranspChgSmryByRyotContAndDate();	
				transpChgSmryByRyotContAndDate.setTransportcontcode(0);
				transpChgSmryByRyotContAndDate.setTransportedcanewt(netWeight);
				transpChgSmryByRyotContAndDate.setMincanewt(Roundedwt);				
				transpChgSmryByRyotContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				transpChgSmryByRyotContAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
				transpChgSmryByRyotContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryByRyotContAndDate.setRate(0.00);
				transpChgSmryByRyotContAndDate.setTotalamount(0.00);
				transpChgSmryByRyotContAndDate.setSeason(caneWeighmentBean.getSeason());
				//Added by DMurty on 23-01-2017
				transpChgSmryByRyotContAndDate.setCastatus((byte) 0);
			}
		}
		else
		{
			transpChgSmryByRyotContAndDate=(TranspChgSmryByRyotContAndDate)transpChgSmryByRyotContAndDateList.get(0);
			double suppliedWt = transpChgSmryByRyotContAndDate.getTransportedcanewt();
			double totalWt = suppliedWt+netWeight;
			transpChgSmryByRyotContAndDate.setTransportedcanewt(totalWt);
			
			double CurrentRoundedWt = transpChgSmryByRyotContAndDate.getMincanewt();
			double NewRoundedWt = CurrentRoundedWt+Roundedwt;
			transpChgSmryByRyotContAndDate.setMincanewt(NewRoundedWt);			
			
			double prevTotalAmt = transpChgSmryByRyotContAndDate.getTotalamount();
			double totalAmt = Roundedwt*transportRate;
			double finalTotal = totalAmt+prevTotalAmt;
			finalTotal = (double)Math.round(finalTotal*100)/100;
			Double.parseDouble(new DecimalFormat("##.##").format(finalTotal));
			
			transpChgSmryByRyotContAndDate.setTotalamount(finalTotal);
			transpChgSmryByRyotContAndDate.setRate(transportRate);
		}
		return transpChgSmryByRyotContAndDate;
	}
	
	private TranspChgSmryByRyotAndCont prepareModelForTranspChgSmryByRyotAndCont(CaneWeighmentBean caneWeighmentBean,Double netWeight, Double RoundedWt) 
	{
		logger.info("========entered to prepareModelForTranspChgSmryByRyotAndCont()==========");
		TranspChgSmryByRyotAndCont  transpChgSmryByRyotAndCont  = null;
		List transpChgSmryByRyotAndContList=caneReceiptFunctionalService.getTranspChgSmryByRyotAndCont(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(transpChgSmryByRyotAndContList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(transportingRateDetailsList!=null)
			{
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
				 
				Double totalAmount=RoundedWt*transportingRateDetails.getRate();
				totalAmount = (double)Math.round(totalAmount*100)/100;
				totalAmount =Double.parseDouble(new DecimalFormat("##.##").format(totalAmount));

				transpChgSmryByRyotAndCont  = new TranspChgSmryByRyotAndCont();	
				
				transpChgSmryByRyotAndCont.setTransportcontcode(transportingRateDetails.getTransportcontcode());
				//Modified by DMurty on 11-07-2016
				//transpChgSmryByRyotAndCont.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryByRyotAndCont.setTransportedcanewt(netWeight);
				transpChgSmryByRyotAndCont.setMincanewt(RoundedWt);				
				transpChgSmryByRyotAndCont.setRyotcode(caneWeighmentBean.getRyotCode());
				transpChgSmryByRyotAndCont.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryByRyotAndCont.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryByRyotAndCont.setTotalpayableamount(0.0);
				transpChgSmryByRyotAndCont.setPendingamount(totalAmount);
				transpChgSmryByRyotAndCont.setPendingpayable(0.0);
				transpChgSmryByRyotAndCont.setTotalamount(totalAmount);
				//Added by DMurty on 05-11-2016
				transpChgSmryByRyotAndCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
			else
			{
				transpChgSmryByRyotAndCont  = new TranspChgSmryByRyotAndCont();	
				transpChgSmryByRyotAndCont.setTransportcontcode(0);
				//Modified by DMurty on 11-07-2016
				transpChgSmryByRyotAndCont.setTransportedcanewt(netWeight);
				transpChgSmryByRyotAndCont.setMincanewt(RoundedWt);
				transpChgSmryByRyotAndCont.setRyotcode(caneWeighmentBean.getRyotCode());
				transpChgSmryByRyotAndCont.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryByRyotAndCont.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryByRyotAndCont.setTotalpayableamount(0.00);
				transpChgSmryByRyotAndCont.setPendingamount(0.00);
				transpChgSmryByRyotAndCont.setPendingpayable(0.00);
				transpChgSmryByRyotAndCont.setTotalamount(0.00);
				transpChgSmryByRyotAndCont.setCompanypaidamount(0.00);
				transpChgSmryByRyotAndCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
		}
		else
		{
			//Modified by DMurty on 19-08-2016
			//here we need to cummulate values
			transpChgSmryByRyotAndCont=(TranspChgSmryByRyotAndCont)transpChgSmryByRyotAndContList.get(0);
			
			double suppliedWt = transpChgSmryByRyotAndCont.getTransportedcanewt();
			double totalWeight = suppliedWt+netWeight;
			transpChgSmryByRyotAndCont.setTransportedcanewt(totalWeight);
			
			double CurrentMinWt = transpChgSmryByRyotAndCont.getMincanewt();
			double NewMinWt = CurrentMinWt+RoundedWt;
			//Modified by DMurty on 11-01-2017
			//transpChgSmryByRyotAndCont.setTransportedcanewt(NewMinWt);	
			transpChgSmryByRyotAndCont.setMincanewt(NewMinWt);
			
			double transportRate = 0.0;
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			if(transportingRateDetailsList!=null)
			{ 
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
				transportRate = transportingRateDetails.getRate();
				transportRate =Double.parseDouble(new DecimalFormat("##.##").format(transportRate));
			}
			//Modified by DMurty on 25-11-2016
			double totalAmt = transpChgSmryByRyotAndCont.getTotalamount();
			double pendingAmt = transpChgSmryByRyotAndCont.getPendingamount();
			
			double transportCharge = transportRate*RoundedWt;
			
			double finaltotalAmt = transportCharge+totalAmt;
			finaltotalAmt = (double)Math.round(finaltotalAmt*100)/100;
			finaltotalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finaltotalAmt));

			double finalPending = pendingAmt+transportCharge;
			finalPending = (double)Math.round(finalPending*100)/100;
			finalPending =Double.parseDouble(new DecimalFormat("##.##").format(finalPending));

			transpChgSmryByRyotAndCont.setTotalamount(finaltotalAmt);
			transpChgSmryByRyotAndCont.setPendingamount(finalPending);
			transpChgSmryByRyotAndCont.setId(transpChgSmryByRyotAndCont.getId());
			
		}
		return transpChgSmryByRyotAndCont;
	}
	
	private TranspChgSmryByContAndDate prepareModelForTranspChgSmryByContAndDate(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		
		TranspChgSmryByContAndDate  transpChgSmryByContAndDate  = null;
		
		List  transpChgSmryByContAndDateList=caneReceiptFunctionalService.getTranspChgSmryByContAndDate(caneWeighmentBean.getSeason());
		
		if(transpChgSmryByContAndDateList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(transportingRateDetailsList!=null)
			{
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);
				 
				Double totalAmount=netWeight*transportingRateDetails.getRate();
				
				transpChgSmryByContAndDate  = new TranspChgSmryByContAndDate();		
				
				transpChgSmryByContAndDate.setTransportcontcode(transportingRateDetails.getTransportcontcode());
				transpChgSmryByContAndDate.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				transpChgSmryByContAndDate.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryByContAndDate.setTotalamount(totalAmount);
				transpChgSmryByContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
				
			}
			else
			{
				transpChgSmryByContAndDate  = new TranspChgSmryByContAndDate();		
				transpChgSmryByContAndDate.setTransportcontcode(0);
				transpChgSmryByContAndDate.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryByContAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
				transpChgSmryByContAndDate.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryByContAndDate.setTotalamount(0.00);
				transpChgSmryByContAndDate.setTotalextent(caneWeighmentBean.getExtentSize());
			}
		}
		else
			transpChgSmryByContAndDate=(TranspChgSmryByContAndDate)transpChgSmryByContAndDateList.get(0);

		return transpChgSmryByContAndDate;
	}
	
	private TranspChgSmryBySeasonByCont prepareModelForTranspChgSmryBySeasonByCont(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		TranspChgSmryBySeasonByCont   transpChgSmryBySeasonByCont= null;
		List  transpChgSmryBySeasonByContList=caneReceiptFunctionalService.getTranspChgSmryBySeasonByCont(caneWeighmentBean.getSeason());
		
		if(transpChgSmryBySeasonByContList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List  transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(transportingRateDetailsList!=null)
			{
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);				 
				Double totalAmount=netWeight*transportingRateDetails.getRate();
				
				transpChgSmryBySeasonByCont= new TranspChgSmryBySeasonByCont();	
				transpChgSmryBySeasonByCont.setTransportcontcode(0);
				transpChgSmryBySeasonByCont.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryBySeasonByCont.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryBySeasonByCont.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryBySeasonByCont.setTotalpayableamount(totalAmount);
				transpChgSmryBySeasonByCont.setPaidAmount(0.00);
				transpChgSmryBySeasonByCont.setPendingamount(totalAmount);
				transpChgSmryBySeasonByCont.setTotalamount(totalAmount);
				transpChgSmryBySeasonByCont.setPendingpayable(totalAmount);
			}
			else
			{
				transpChgSmryBySeasonByCont= new TranspChgSmryBySeasonByCont();	
				transpChgSmryBySeasonByCont.setTransportcontcode(0);
				transpChgSmryBySeasonByCont.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryBySeasonByCont.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryBySeasonByCont.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryBySeasonByCont.setTotalpayableamount(0.00);
				transpChgSmryBySeasonByCont.setPaidAmount(0.00);
				transpChgSmryBySeasonByCont.setPendingamount(0.00);
				transpChgSmryBySeasonByCont.setTotalamount(0.00);
				transpChgSmryBySeasonByCont.setPendingpayable(0.00);
				transpChgSmryBySeasonByCont.setVillagecode(caneWeighmentBean.getVillageCode());
			}
		}
		else
			transpChgSmryBySeasonByCont=(TranspChgSmryBySeasonByCont)transpChgSmryBySeasonByContList.get(0);
		
		return transpChgSmryBySeasonByCont ;
	}
	
	
	private TranspChgSmryBySeason prepareModelForTranspChgSmryBySeason(CaneWeighmentBean caneWeighmentBean,Double netWeight) 
	{
		
		TranspChgSmryBySeason   transpChgSmryBySeason= null;
		List  transpChgSmryBySeasonList=caneReceiptFunctionalService.getTranspChgSmryBySeason(caneWeighmentBean.getSeason());
		
		if(transpChgSmryBySeasonList==null)
		{
			//Here we need to send date
			String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
			String dt[] = date.split("-");
			String dd = dt[0];
			String mm = dt[1];
			String yy = dt[2];
			String finalDate = yy+"-"+mm+"-"+dd;
			List transportingRateDetailsList=caneReceiptFunctionalService.getTransportingRateDetails(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
			
			if(transportingRateDetailsList!=null)
			{
				TransportingRateDetails transportingRateDetails =(TransportingRateDetails)transportingRateDetailsList.get(0);				 
				Double totalAmount=netWeight*transportingRateDetails.getRate();

				transpChgSmryBySeason= new TranspChgSmryBySeason();	
				transpChgSmryBySeason.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryBySeason.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryBySeason.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryBySeason.setPaidAmount(0.00);
				transpChgSmryBySeason.setTotalpayableamount(totalAmount);
				transpChgSmryBySeason.setPendingpayable(totalAmount);
				transpChgSmryBySeason.setTotalamount(totalAmount);
				
			}
			else
			{
				transpChgSmryBySeason= new TranspChgSmryBySeason();	
				transpChgSmryBySeason.setPaidAmount(0.00);
				transpChgSmryBySeason.setTransportedcanewt(caneWeighmentBean.getTotalWeight());
				transpChgSmryBySeason.setSeason(caneWeighmentBean.getSeason());
				transpChgSmryBySeason.setTotalextent(caneWeighmentBean.getExtentSize());
				transpChgSmryBySeason.setPaidAmount(0.00);
				transpChgSmryBySeason.setTotalpayableamount(0.00);
				transpChgSmryBySeason.setPendingpayable(0.00);
				transpChgSmryBySeason.setTotalamount(0.00);
				transpChgSmryBySeason.setPaidAmount(0.00);
			}
		}
		else
			transpChgSmryBySeason=(TranspChgSmryBySeason)transpChgSmryBySeasonList.get(0);

		return transpChgSmryBySeason ;
	}
	
	private ICPDetailsByRyotAndDate prepareModelForICPDetailsByRyotAndDate(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForICPDetailsByRyotAndDate()==========");
		double icp = 0.0;
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			icp = caneAcctAmounts.get(0).getAmount();
			icp =Double.parseDouble(new DecimalFormat("##.##").format(icp));
		}
		//Modified by DMurty on 06-10-2016
		ICPDetailsByRyotAndDate  iCPDetailsByRyotAndDate= null; 
		List ICPDetailsByRyotAndDateList=caneReceiptFunctionalService.getICPDetailsByRyotAndDate(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),finalDate);
		if(ICPDetailsByRyotAndDateList!=null)
		{
			iCPDetailsByRyotAndDate=(ICPDetailsByRyotAndDate)ICPDetailsByRyotAndDateList.get(0);
			iCPDetailsByRyotAndDate.setSuppliedcanewt(iCPDetailsByRyotAndDate.getSuppliedcanewt()+netCaneWeight);
			double amt = icp*netCaneWeight;
			
			double finalIcp = iCPDetailsByRyotAndDate.getIcpamount()+amt;
			finalIcp =Double.parseDouble(new DecimalFormat("##.##").format(finalIcp));
			iCPDetailsByRyotAndDate.setIcpamount(finalIcp);
		}
		else
		{
			iCPDetailsByRyotAndDate= new ICPDetailsByRyotAndDate();
			double amt = icp*netCaneWeight;
			amt =Double.parseDouble(new DecimalFormat("##.##").format(amt));
			iCPDetailsByRyotAndDate.setIcpamount(amt);
			iCPDetailsByRyotAndDate.setIcprate(icp);
			iCPDetailsByRyotAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			iCPDetailsByRyotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
			iCPDetailsByRyotAndDate.setSeason(caneWeighmentBean.getSeason());
			iCPDetailsByRyotAndDate.setSuppliedcanewt(netCaneWeight);
		}
		return iCPDetailsByRyotAndDate ;
	}

	private ICPDetailsByRyot prepareModelForICPDetailsByRyot(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForICPDetailsByRyot()==========");
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		double icp = 0.0;
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"Purchase Tax / ICP");
			icp = caneAcctAmounts.get(0).getAmount();
			icp =Double.parseDouble(new DecimalFormat("##.##").format(icp));
		}
		//Modified by DMurty on 06-10-2016
		ICPDetailsByRyot  iCPDetailsByRyot= null;
		List ICPDetailsByRyotList=caneReceiptFunctionalService.getICPDetailsByRyot(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode());
		if(ICPDetailsByRyotList!=null)
		{
			iCPDetailsByRyot=(ICPDetailsByRyot)ICPDetailsByRyotList.get(0);	
			iCPDetailsByRyot.setSuppliedcanewt(iCPDetailsByRyot.getSuppliedcanewt()+netCaneWeight);
			double amt = icp*netCaneWeight;
			double totalIcp = iCPDetailsByRyot.getIcpamount()+amt;
			totalIcp =Double.parseDouble(new DecimalFormat("##.##").format(totalIcp));
			iCPDetailsByRyot.setIcpamount(totalIcp);		
		}
		else
		{
			iCPDetailsByRyot= new ICPDetailsByRyot();	
			double amt = icp*netCaneWeight;
			amt =Double.parseDouble(new DecimalFormat("##.##").format(amt));
			iCPDetailsByRyot.setIcpamount(amt);
			iCPDetailsByRyot.setIcprate(icp);
			iCPDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
			iCPDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
			iCPDetailsByRyot.setSuppliedcanewt(netCaneWeight);
		}
		return iCPDetailsByRyot ;
	}
	
	private SubsidyDetailsByRotAndDate prepareModelForSubsidyDetailsByRotAndDate(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForSubsidyDetailsByRotAndDate()==========");
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		double frp = 0.0;
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			if(caneAcctAmounts.get(0).getSubsidy() != null)
			{
				frp = caneAcctAmounts.get(0).getSubsidy();
				frp =Double.parseDouble(new DecimalFormat("##.##").format(frp));
			}
		}
		//Modified by DMurty on 26-11-2016
		SubsidyDetailsByRotAndDate  subsidyDetailsByRotAndDate= null;
		List SubsidyDetailsByRotAndDateList = new ArrayList();
		SubsidyDetailsByRotAndDateList = caneReceiptFunctionalService.getSubsidyDetailsByRotAndDateList(caneWeighmentBean.getRyotCode(),caneWeighmentBean.getSeason(),finalDate);
		if(SubsidyDetailsByRotAndDateList != null && SubsidyDetailsByRotAndDateList.size()>0)
		{
			subsidyDetailsByRotAndDate=(SubsidyDetailsByRotAndDate)SubsidyDetailsByRotAndDateList.get(0);	
			
			double subsidyAmt = subsidyDetailsByRotAndDate.getSubsidyamount();
			double suppliedWt = subsidyDetailsByRotAndDate.getSuppliedcanewt();
			
			double Amt =  frp*netCaneWeight;
			double finalAmt = subsidyAmt+Amt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));
	
			subsidyDetailsByRotAndDate.setSubsidyamount(finalAmt);
			subsidyDetailsByRotAndDate.setSuppliedcanewt(suppliedWt+netCaneWeight);
			subsidyDetailsByRotAndDate.setId(subsidyDetailsByRotAndDate.getId());
		}
		else
		{
			subsidyDetailsByRotAndDate= new SubsidyDetailsByRotAndDate();	
			subsidyDetailsByRotAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			subsidyDetailsByRotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
			
			double finalAmt = frp*netCaneWeight;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));
			
			subsidyDetailsByRotAndDate.setSubsidyamount(finalAmt);
			subsidyDetailsByRotAndDate.setSubsidyrate(frp);
			subsidyDetailsByRotAndDate.setSuppliedcanewt(netCaneWeight);
			subsidyDetailsByRotAndDate.setSeason(caneWeighmentBean.getSeason());
		}
		return subsidyDetailsByRotAndDate ;
	}
	
	private SubsidyDetailsByRyot prepareModelForSubsidyDetailsByRyot(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForSubsidyDetailsByRyot()==========");
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		double frp = 0.0;
		List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(caneWeighmentBean.getSeason(),finalDate);
		if(caneAcctRules != null && caneAcctRules.size()>0)
		{
			int calcid = caneAcctRules.get(0).getCalcid();
			
			List <CaneAcctAmounts> caneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(caneWeighmentBean.getSeason(),calcid,"FRP");
			if(caneAcctAmounts.get(0).getSubsidy() != null)
			{
				frp = caneAcctAmounts.get(0).getSubsidy();
				frp =Double.parseDouble(new DecimalFormat("##.##").format(frp));
			}
		}
		
		SubsidyDetailsByRyot subsidyDetailsByRyot= null;
		List SubsidyDetailsByRotList = new ArrayList();
		SubsidyDetailsByRotList = caneReceiptFunctionalService.getSubsidyDetailsByRyotList(caneWeighmentBean.getRyotCode(),caneWeighmentBean.getSeason());
		if(SubsidyDetailsByRotList != null && SubsidyDetailsByRotList.size()>0)
		{
			subsidyDetailsByRyot = (SubsidyDetailsByRyot) SubsidyDetailsByRotList.get(0);
			
			double subsidyAmt = subsidyDetailsByRyot.getSubsidyamount();
			double Amt = frp*netCaneWeight;
			
			double finalAmt = subsidyAmt+Amt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

			subsidyDetailsByRyot.setSubsidyamount(finalAmt);
			subsidyDetailsByRyot.setSuppliedcanewt(subsidyDetailsByRyot.getSuppliedcanewt()+netCaneWeight);
			subsidyDetailsByRyot.setId(subsidyDetailsByRyot.getId());
		}
		else
		{
			subsidyDetailsByRyot= new SubsidyDetailsByRyot();	
			subsidyDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
			subsidyDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
			
			double finalAmt = frp*netCaneWeight;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));
		
			subsidyDetailsByRyot.setSubsidyamount(finalAmt);
			subsidyDetailsByRyot.setSubsidyrate(frp);
			subsidyDetailsByRyot.setSuppliedcanewt(netCaneWeight);
		}
		return subsidyDetailsByRyot ;
	}
	
	private CDCDetailsByRyot prepareModelForCDCDetailsByRyot(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForCDCDetailsByRyot()==========");

		//Modified by DMurty on 31-08-2016
		int dedCode = 1;
		double cdcRate = 0.0;
		List RateList = caneReceiptFunctionalService.getDeductionRates(dedCode);
		if(RateList != null && RateList.size()>0)
		{
			Map amtMap = new HashMap();
			amtMap = (Map) RateList.get(0);
			cdcRate = (Double) amtMap.get("amount");
			cdcRate =Double.parseDouble(new DecimalFormat("##.##").format(cdcRate));
		}
		double finalAmt = cdcRate*netCaneWeight;
		finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

		CDCDetailsByRyot cDCDetailsByRyot= null;//new CDCDetailsByRyot();	
		
		String table="CDCDetailsByRyot";
		List CDCDetailsByRyotList=caneReceiptFunctionalService.getCDCDetailsByRyotList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),table);
		if(CDCDetailsByRyotList != null && CDCDetailsByRyotList.size()>0)
		{
			cDCDetailsByRyot= new CDCDetailsByRyot();	

			cDCDetailsByRyot=(CDCDetailsByRyot)CDCDetailsByRyotList.get(0);
			
			double amt = cDCDetailsByRyot.getCdcamount();
			finalAmt = amt+finalAmt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

			Double supnwt=cDCDetailsByRyot.getSuppliedcanewt();
			supnwt=supnwt+netCaneWeight;
			cDCDetailsByRyot.setSuppliedcanewt(supnwt);
			cDCDetailsByRyot.setCdcamount(finalAmt);
		}
		else
		{
			cDCDetailsByRyot= new CDCDetailsByRyot();	

			cDCDetailsByRyot.setCdcamount(finalAmt);
			cDCDetailsByRyot.setCdcrate(cdcRate);
			cDCDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
			cDCDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
			cDCDetailsByRyot.setSuppliedcanewt(netCaneWeight);	
		}
		return cDCDetailsByRyot ;
	}
	private CDCDetailsByRotAndDate prepareModelForCDCDetailsByRotAndDate(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForCDCDetailsByRotAndDate()==========");

		//Modified by DMurty on 31-08-2016
		int dedCode = 1;
		double cdcRate = 0.0;
		List RateList = caneReceiptFunctionalService.getDeductionRates(dedCode);
		if(RateList != null && RateList.size()>0)
		{
			Map amtMap = new HashMap();
			amtMap = (Map) RateList.get(0);
			cdcRate = (Double) amtMap.get("amount");
			cdcRate =Double.parseDouble(new DecimalFormat("##.##").format(cdcRate));
		}
		double finalAmt = cdcRate*netCaneWeight;
		finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

		CDCDetailsByRotAndDate  cDCDetailsByRotAndDate= null;//new CDCDetailsByRotAndDate();	

		String table="CDCDetailsByRotAndDate";
		//Added by DMurty on 25-11-2016 
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List CDCDetailsByRotAndDateList=caneReceiptFunctionalService.getCDCDetailsByRotAndDateList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),table,finalDate);
		if(CDCDetailsByRotAndDateList != null && CDCDetailsByRotAndDateList.size()>0)
		{
			 cDCDetailsByRotAndDate= new CDCDetailsByRotAndDate();	

			cDCDetailsByRotAndDate=(CDCDetailsByRotAndDate)CDCDetailsByRotAndDateList.get(0);
			Double supwt=cDCDetailsByRotAndDate.getSuppliedcanewt();
			supwt=supwt+netCaneWeight;
			cDCDetailsByRotAndDate.setSuppliedcanewt(supwt);
			double amt = cDCDetailsByRotAndDate.getCdcamount();
			finalAmt = amt+finalAmt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

			cDCDetailsByRotAndDate.setCdcamount(finalAmt);
			//Added by DMurty on 25-11-2016
			cDCDetailsByRotAndDate.setId(cDCDetailsByRotAndDate.getId());
		}
		else
		{
			 cDCDetailsByRotAndDate= new CDCDetailsByRotAndDate();	
			 double cdcAmt = netCaneWeight*cdcRate;
			 cdcAmt =Double.parseDouble(new DecimalFormat("##.##").format(cdcAmt));

			cDCDetailsByRotAndDate.setCdcamount(cdcAmt);
			cDCDetailsByRotAndDate.setCdcrate(cdcRate);
			cDCDetailsByRotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
			cDCDetailsByRotAndDate.setSeason(caneWeighmentBean.getSeason());
			cDCDetailsByRotAndDate.setSuppliedcanewt(netCaneWeight);	
			cDCDetailsByRotAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
		}
		return cDCDetailsByRotAndDate ;
	}

	private UCDetailsByRotAndDate prepareModelForUCDetailsByRotAndDate(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForUCDetailsByRotAndDate()==========");
		//Modified by DMurty on 31-08-2016
		int dedCode = 2;
		double ulRate = 0.0;
		List RateList = caneReceiptFunctionalService.getDeductionRates(dedCode);
		if(RateList != null && RateList.size()>0)
		{
			Map amtMap = new HashMap();
			amtMap = (Map) RateList.get(0);
			ulRate = (Double) amtMap.get("amount");
			ulRate =Double.parseDouble(new DecimalFormat("##.##").format(ulRate));
		}
		double finalAmt = ulRate*netCaneWeight;
		finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

		UCDetailsByRotAndDate uCDetailsByRotAndDate = null;//new UCDetailsByRotAndDate();	

		String table="UCDetailsByRotAndDate";
		
		//Added by DMurty on 25-11-2016 
		String date = getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime());
		String dt[] = date.split("-");
		String dd = dt[0];
		String mm = dt[1];
		String yy = dt[2];
		String finalDate = yy+"-"+mm+"-"+dd;
		
		List UCDetailsByRotAndDateList=caneReceiptFunctionalService.getUCDetailsByRotAndDateList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),table,finalDate);
		if(UCDetailsByRotAndDateList != null && UCDetailsByRotAndDateList.size()>0)
		{
			uCDetailsByRotAndDate= new UCDetailsByRotAndDate();	

			uCDetailsByRotAndDate=(UCDetailsByRotAndDate)UCDetailsByRotAndDateList.get(0);
			double amt = uCDetailsByRotAndDate.getUdcamount();
			finalAmt = amt+finalAmt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

			Double supnwt=uCDetailsByRotAndDate.getSuppliedcanewt();
			supnwt=supnwt+netCaneWeight;
			uCDetailsByRotAndDate.setSuppliedcanewt(supnwt);
			uCDetailsByRotAndDate.setUdcamount(finalAmt);
			//Added by DMurty on 25-11-2016 to cummulate the record based on id.
			uCDetailsByRotAndDate.setId(uCDetailsByRotAndDate.getId());
		}
		else
		{
			uCDetailsByRotAndDate= new UCDetailsByRotAndDate();	

			uCDetailsByRotAndDate.setReceiptdate(DateUtils.getSqlDateFromString(getWeighDatePerShift(caneWeighmentBean.getCaneWeighmentDate(),caneWeighmentBean.getCaneWeighmentTime()),Constants.GenericDateFormat.DATE_FORMAT));
			uCDetailsByRotAndDate.setRyotcode(caneWeighmentBean.getRyotCode());
			uCDetailsByRotAndDate.setSeason(caneWeighmentBean.getSeason());
			uCDetailsByRotAndDate.setSuppliedcanewt(netCaneWeight);
			uCDetailsByRotAndDate.setUdcamount(finalAmt);
			uCDetailsByRotAndDate.setUdcrate(ulRate);
		}
		return uCDetailsByRotAndDate ;
	}
	
	private UCDetailsByRyot prepareModelForUCDetailsByRyot(CaneWeighmentBean caneWeighmentBean,Double netCaneWeight) 
	{
		logger.info("========entered to prepareModelForUCDetailsByRyot()==========");
		//Modified by DMurty on 31-08-2016
		int dedCode = 2;
		double ulRate = 0.0;
		List RateList = caneReceiptFunctionalService.getDeductionRates(dedCode);
		if(RateList != null && RateList.size()>0)
		{
			Map amtMap = new HashMap();
			amtMap = (Map) RateList.get(0);
			ulRate = (Double) amtMap.get("amount");
			ulRate =Double.parseDouble(new DecimalFormat("##.##").format(ulRate));
		}
		double finalAmt = ulRate*netCaneWeight;
		finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

		UCDetailsByRyot uCDetailsByRyot= new UCDetailsByRyot();		

		String table="UCDetailsByRyot";
		List UCDetailsByRyotList=caneReceiptFunctionalService.getUCDetailsByRyotList(caneWeighmentBean.getSeason(),caneWeighmentBean.getRyotCode(),table);
		if(UCDetailsByRyotList != null && UCDetailsByRyotList.size()>0)
		{
			uCDetailsByRyot= new UCDetailsByRyot();		

			uCDetailsByRyot=(UCDetailsByRyot)UCDetailsByRyotList.get(0);
			double amt = uCDetailsByRyot.getUdcamount();
			finalAmt = amt+finalAmt;
			finalAmt =Double.parseDouble(new DecimalFormat("##.##").format(finalAmt));

			Double supnwt=uCDetailsByRyot.getSuppliedcanewt();
			supnwt=supnwt+netCaneWeight;
			uCDetailsByRyot.setSuppliedcanewt(supnwt);
			uCDetailsByRyot.setUdcamount(finalAmt);
		}
		else
		{
			uCDetailsByRyot= new UCDetailsByRyot();		

			uCDetailsByRyot.setRyotcode(caneWeighmentBean.getRyotCode());
			uCDetailsByRyot.setSeason(caneWeighmentBean.getSeason());
			uCDetailsByRyot.setSuppliedcanewt(netCaneWeight);
			uCDetailsByRyot.setUdcamount(finalAmt);
			uCDetailsByRyot.setUdcrate(ulRate);
		}
		return uCDetailsByRyot ;
	}
	
	
	
	//----------------------------------------------Permit CheckIn ------------------------------------------------------------------
	@RequestMapping(value = "/getValidatePermitNoForCheckIn", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean getValidatePermitNoForCheckIn(@RequestBody Integer permitno) throws Exception 
	{
		logger.info("========getValidatePermitNo()========== permitno--"+permitno);			
		boolean flag = false;
		byte status =  caneReceiptFunctionalService.getStatusForVehicleCheckIn(permitno);
		if(status == 1)
		{
			flag = true;
		}
		return flag;
	}
	@RequestMapping(value = "/savePermitCheckInDetails", method = RequestMethod.POST)
	public @ResponseBody
	boolean savePermitCheckInDetails(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception 
	{
		logger.info("========saveWeighmentDetails()==========jsonData-----"+jsonData);
		
		String checkInDate=(String)jsonData.get("caneWeighmentDate");
		String todayTime=(String) jsonData.get("caneWeighmentTime");
		checkInDate=getWeighDatePerShift(checkInDate,todayTime);
		Integer vehicletype=(Integer)jsonData.get("vehicle");
		Integer serialNumber=(Integer)jsonData.get("serialNumber");
		Integer shiftId=(Integer)jsonData.get("shiftId");
		String vehicleregno=(String)jsonData.get("vehicleregno");
		String vehicleNumber=(String)jsonData.get("vehicleNumber");
		String permitNumber=(String)jsonData.get("permitNumber");
		String ryotCode=(String)jsonData.get("ryotCode");
		String season=(String)jsonData.get("season");
		//Time checkInTime = DateUtils.getCurrentSystemTime();
		boolean isInsert=false;
		try
		{
			isInsert=caneReceiptFunctionalService.insertVcheckIn(checkInDate,serialNumber,permitNumber,season,vehicleregno+vehicleNumber,shiftId,vehicletype);
			return isInsert;
	    }
	catch(Exception e)
	{
		logger.info("--The check in update fail--"+e);
	}
	return isInsert;
	}
	
	@RequestMapping(value = "/getTotalVehiclesByShiftAndDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getTotalVehiclesByShiftAndDate(@RequestBody JSONObject jsonData) throws Exception 
	{
		logger.info("========getWeigmentDetailsByShiftByDate()==========jsonData--"+jsonData);			
		String todayDate=(String) jsonData.get("caneWeighmentDate");
		String todayTime=(String) jsonData.get("caneWeighmentTime");
		todayDate=getWeighDatePerShift(todayDate,todayTime);
		List lss=new ArrayList();
		List shiftsls=caneReceiptFunctionalService.getShifts();

		Shift ssls=null;
		int stotal=0;
		int wtotal=0;
		int wstotal=0;
		for(int j=0;j<shiftsls.size();j++)
		{
			ssls=(Shift)shiftsls.get(j);
			List canereceiptshiftsmryList=caneReceiptFunctionalService.getPermitCheckInTotalPerShift(todayDate,ssls.getShiftid());
			
			List waitingVehicles=caneReceiptFunctionalService.getWaitingVehiclesForWeighment(todayDate,ssls.getShiftid());
			logger.info("========getWeigmentDetailsByShiftByDate()==========canereceiptshiftsmryList--"+canereceiptshiftsmryList);
			Map hm1=new HashMap();
			Map hm2=new HashMap();
			Map hm3=new HashMap();
			if(canereceiptshiftsmryList !=null)
		{
		hm1=(Map)canereceiptshiftsmryList.get(0);
		int vtotal=(Integer)hm1.get("vtotal");
		hm2.put("shiftid", ssls.getShiftid());
		hm2.put("vtotal", vtotal);
		logger.info("========getWeigmentDetailsByShiftByDate()==========rec--"+hm2.toString());
		stotal=stotal+vtotal;
		if(waitingVehicles !=null)
		{
		hm3=(Map)waitingVehicles.get(0);
		wstotal=(Integer)hm3.get("wtotal");
		wtotal=wtotal+wstotal;
		}
		hm2.put("wstotal", wstotal);
		}
		else
		{
			hm2.put("shiftid", ssls.getShiftid());
			hm2.put("vtotal", 0);
			hm2.put("wstotal", 0);
		}
			if(ssls.getShiftid()==3)
			{
				hm2.put("stotal",stotal);
				hm2.put("wtotal", wtotal);
			}
			lss.add(hm2);
		}
		return lss;
	}
	
	@RequestMapping(value = "/getVehiclesWaitingReport", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getVehiclesWaitingReport(@RequestBody JSONObject jsonData) throws Exception 
	{
	
		//need to sent current time and date from front end like caneWeighment or PermitCheckIn
		
		String sdate=(String) jsonData.get("sdate");
		String stime=(String) jsonData.get("stime");
		String shiftdate=getWeighDatePerShift(sdate,stime);
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date hDate =(Date) df.parse(shiftdate);
		Date newDate =ahfController.addDays(hDate,-2); 
		String strfinalDate = df.format(newDate);	
		List fordate=new ArrayList();
		List waitingVehicleListForWeighment=null;
		List wlist=new ArrayList();
			Map wvehicles=null;	
		waitingVehicleListForWeighment=caneReceiptFunctionalService.getVehiclesForWeighment(strfinalDate,shiftdate);
		if(waitingVehicleListForWeighment!=null || waitingVehicleListForWeighment.size()>0)
		{
			for(int i=0;i<waitingVehicleListForWeighment.size();i++)
			{
				wvehicles=(HashMap)waitingVehicleListForWeighment.get(i);
				Timestamp tm=(java.sql.Timestamp)wvehicles.get("checkintime");
				String x=tm.toString();
				String y[]=x.split(" ")[1].split(":");
				wvehicles.put("checkintime",y[0]+":"+y[1]+":"+y[2].substring(0,2));
				wvehicles.put("checkindate",DateUtils.formatDate((Date)wvehicles.get("checkindate"),Constants.GenericDateFormat.DATE_FORMAT));
				
				byte vehicletype = (Byte) wvehicles.get("vehicletype");
				String strVehicleType = null;
				if(vehicletype == 0)
				{
					strVehicleType = "TRACTOR";
				}
				else if(vehicletype == 1)
				{
					strVehicleType = "SMALL LORRY";
				}
				else
				{
					strVehicleType = "BIG LORRY";
				}
				wvehicles.put("vehicletype", strVehicleType);
				
				wlist.add(wvehicles);
			}
		}
		return wlist;
	}
	@RequestMapping(value = "/getLoginUserForCheckIn", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	String getServerDate() throws Exception
	{
		HttpSession session=request.getSession(false);
		Employees employ = (Employees) session.getAttribute(Constants.USER);
		String user = employ.getEmployeename();
		return user;
	}
	@RequestMapping(value = "/validatePermitCheckInForWeighment", method = RequestMethod.POST)
	public @ResponseBody boolean validatePermitCheckInForWeighment(@RequestBody JSONObject jsonData)throws Exception 
	{
		boolean vFlag=false;
		int permitNo = (Integer) jsonData.get("permitNo");
		logger.info("========validateHarvestDateForPermitForWeighment()========== permitNo "+permitNo);			
		List permitList = new ArrayList();
		permitList = caneReceiptFunctionalService.getPermitListAfterCheckIn(permitNo);
		logger.info("Controller========validateHarvestDateForPermitForWeighment()========== nRet"+permitList);			
		if(permitList.size()>0 && permitList != null)
			vFlag=true;
		return vFlag;
	}
	
	
	
}
