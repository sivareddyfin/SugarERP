package com.finsol.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.service.ChartService;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class ChartController {

	private static final Logger logger = Logger.getLogger(ChartController.class);
	
	@Autowired
	private ChartService chartService;
	
	@RequestMapping(value = "/extentSizeByFieldOfficer", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   extentSizeByFieldOfficer(HttpServletRequest req,HttpServletResponse res) 
	{		
		JSONObject keyforyears = new JSONObject();

		List extentData = chartService.getExtentSizeByFieldOfficer();
		keyforyears.put("data", extentData);

		return keyforyears;	    
	}
	
	@RequestMapping(value = "/extentSizeByFieldAssistant", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   extentSizeByFieldAssistant(HttpServletRequest req,HttpServletResponse res) 
	{
		JSONObject keyforyears = new JSONObject();
		
		String foid = (String) req.getParameter("foid");
		List extentData = chartService.getExtentSizeByFieldAssistant(Integer.parseInt(foid));
		keyforyears.put("data", extentData);

		return keyforyears;    
	}
	
	
	@RequestMapping(value = "/bankDetailsChart", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   bankDetailsChart(HttpServletRequest req,HttpServletResponse res) 
	{				
		JSONObject keyforyears = new JSONObject();

		List bankData = chartService.getBankDetailsChart();
		keyforyears.put("data", bankData);

		return keyforyears;   
	}
	
	@RequestMapping(value = "/branchDetailsChart", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   branchDetailsChart(HttpServletRequest req,HttpServletResponse res) 
	{
		JSONObject keyforyears = new JSONObject();
		
		String bankid = (String) req.getParameter("bankid");
		List branchData = chartService.getBranchDetailsChart(Integer.parseInt(bankid));
		keyforyears.put("data", branchData);

		return keyforyears;	    
	}	
	
	@RequestMapping(value = "/crushedCaneByPlantOrRatoon", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   crushedCaneByPlantOrRatoon(HttpServletRequest req,HttpServletResponse res) 
	{				
		JSONObject keyforyears = new JSONObject();

		List bankData = chartService.crushedCaneByPlantOrRatoon();
		keyforyears.put("data", bankData);

		return keyforyears;	    
	}
	
	@RequestMapping(value = "/crushedCaneByVariety", method = RequestMethod.GET)
	public  @ResponseBody JSONObject   crushedCaneByVariety(HttpServletRequest req,HttpServletResponse res) 
	{				
		JSONObject keyforyears = new JSONObject();

		List bankData = chartService.crushedCaneByVariety();
		keyforyears.put("data", bankData);

		return keyforyears;	    
	}
	
	//Added by DMurty on 05-12-2016
	@RequestMapping(value = "/crushedWeightByZone", method = RequestMethod.GET)
	public  @ResponseBody JSONObject crushedWeightByZone(HttpServletRequest req,HttpServletResponse res) 
	{
		JSONObject keyforyears = new JSONObject();
		
		String foid = (String) req.getParameter("foid");
		List extentData = chartService.crushedWeightByZone();
		keyforyears.put("data", extentData);
		return keyforyears;    
	}
	
	//
	@RequestMapping(value = "/circleWiseCrushedWeightByZones", method = RequestMethod.GET)
	public  @ResponseBody JSONObject circleWiseCrushedWeightByZones(HttpServletRequest req,HttpServletResponse res) 
	{
		JSONObject keyforyears = new JSONObject();
		
		String zoneCode = (String) req.getParameter("foid");
		int nzoneCode = Integer.parseInt(zoneCode);
		List extentData = chartService.circleWiseCrushedWeightByZones(nzoneCode);
		keyforyears.put("data", extentData);
		return keyforyears;    
	}
}
