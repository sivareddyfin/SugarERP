package com.finsol.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.CaneWeightBean;
import com.finsol.bean.PermitDetailsBean;
import com.finsol.bean.UnloadingContractorBean;
import com.finsol.bean.WeighBridgeBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AgreementDetails;
import com.finsol.model.CaneWeight;
import com.finsol.model.Circle;
import com.finsol.model.CompositePrKeyForPermitDetails;
import com.finsol.model.CropTypes;
import com.finsol.model.PermitDetails;
import com.finsol.model.Ryot;
import com.finsol.model.UnloadingContractor;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.model.WeighBridge;
import com.finsol.model.Zone;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneWeightService;
import com.finsol.service.CommonService;
import com.finsol.service.UnloadingContractorService;
import com.finsol.service.WeighBridgeService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author Rama Krishna
 * 
 */
@Controller
public class CaneReceiptController {
	
	private static final Logger logger = Logger.getLogger(CaneReceiptController.class);
	
	
	
	@Autowired
	private WeighBridgeService weighBridgeService;	
	
	@Autowired
	private UnloadingContractorService unloadingContractorService;
	
	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired
	private CaneWeightService caneWeightService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired	
	private HibernateDao hibernateDao;
	
	
	
	//----------------------------------- Weight Bridge Master ----------------------------------//
	@RequestMapping(value = "/saveWeighBridge", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveWeighBridge(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			WeighBridgeBean weighBridgeBean = new ObjectMapper().readValue(jsonData, WeighBridgeBean.class);
			WeighBridge weighBridge = prepareModelForWeighBridge(weighBridgeBean);
			//weighBridgeService.addWeighBridge(weighBridge);
			entityList.add(weighBridge);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private WeighBridge prepareModelForWeighBridge(WeighBridgeBean weightBridgeBean) 
	{
		WeighBridge weighBridge = new WeighBridge();
		String modifyFlag=weightBridgeBean.getModifyFlag();
		int bridgeid=commonService.getMaxIDforColumn("WeighBridge", "bridgeid");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			weighBridge.setBridgeid(weightBridgeBean.getBridgeId());
		}
		else
		{
			weighBridge.setBridgeid(bridgeid);
		}
		weighBridge.setId(weightBridgeBean.getId());
		weighBridge.setBridgename(weightBridgeBean.getBridgeName().toUpperCase());
		//weighBridge.setBridgeid(weightBridgeBean.getBridgeId());
		weighBridge.setStatus(weightBridgeBean.getStatus());
	
		return weighBridge;
	}
	@RequestMapping(value = "/getAllWeighBridge", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<WeighBridgeBean> getAllWeignBridge(@RequestBody String jsonData)throws Exception
	{
		
		List<WeighBridgeBean> weighBridgeBeans = prepareListofWeightBridgeBeans(weighBridgeService.listWeighBridge());
		return weighBridgeBeans;
	}
	private List<WeighBridgeBean> prepareListofWeightBridgeBeans(List<WeighBridge> weighBridges)
	{
		List<WeighBridgeBean> beans = null;
		if (weighBridges != null && !weighBridges.isEmpty())
		{
			beans = new ArrayList<WeighBridgeBean>();
			WeighBridgeBean bean = null;
			for (WeighBridge weighBridge : weighBridges)
			{
				bean = new WeighBridgeBean();	
				
				bean.setId(weighBridge.getId());
				bean.setBridgeName(weighBridge.getBridgename());
				bean.setBridgeId(weighBridge.getBridgeid());
				bean.setStatus(weighBridge.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	
	}

	//-------------------------------- End Of Weigh Bridge Master-----------------//

	
	//-------------------------Unloading Controctor-------------------------------//

    @RequestMapping(value = "/saveUnloadingContractor", method = RequestMethod.POST, headers = { "Content-type=application/json" })
     public @ResponseBody
     boolean saveUnloadingContractor(@RequestBody String jsonData) throws Exception
     {
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			 logger.info("====saveUnloadingContractor() from AgricultureHarvestingController========"+ jsonData.toString());
		     UnloadingContractorBean unloadingContractorBean = new ObjectMapper().readValue(jsonData, UnloadingContractorBean.class);
		     UnloadingContractor unloadingContractor = prepareModelForUnloadingContractor(unloadingContractorBean);
		     
		     //unloadingContractorService.addUnloadingContractor(unloadingContractor);
		     entityList.add(unloadingContractor);
			 isInsertSuccess = commonService.saveMultipleEntities(entityList);
				
		     return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
    }
    private UnloadingContractor prepareModelForUnloadingContractor(UnloadingContractorBean unloadingContractorBean) 
	{
    	UnloadingContractor unloadingContractor = new UnloadingContractor();
		
    	String modifyFlag=unloadingContractorBean.getModifyFlag();
    	int ucCode=commonService.getMaxIDforColumn("UnloadingContractor", "ucCode");
    	if("Yes".equalsIgnoreCase(modifyFlag))
    	{
    		unloadingContractor.setUcCode(unloadingContractorBean.getUcCode());
        		
    	}
    	else
    	{
    		unloadingContractor.setUcCode(ucCode);
    	}
    	unloadingContractor.setId(unloadingContractorBean.getId());
    	//unloadingContractor.setUcCode(unloadingContractorBean.getUcCode());
    	unloadingContractor.setContactNo(unloadingContractorBean.getContactNo());
    	unloadingContractor.setName(unloadingContractorBean.getName().toUpperCase());
    	unloadingContractor.setStatus(unloadingContractorBean.getStatus());
	
		return unloadingContractor;
	}
    @RequestMapping(value = "/getAllUnloadingContractor", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<UnloadingContractorBean> getAllUnloadingContractor(@RequestBody String jsonData)throws Exception
	{
		logger.info("====getAllUnloadingContractor()  from AgricultureHarvestingController========");
		
		List<UnloadingContractorBean> unloadingContractorBeanBeans = prepareListofUnloadingContractorBeans(unloadingContractorService.listUnloadingContractors());
		logger.info("==== UnloadingContractorBeanBeans.size()========"+unloadingContractorBeanBeans.size());
		return unloadingContractorBeanBeans;
	}
    private List<UnloadingContractorBean> prepareListofUnloadingContractorBeans(List<UnloadingContractor> unloadingContractors)
	{
		logger.info("====UnloadingContractorBeanBeans.size()========"+ unloadingContractors.size());
		List<UnloadingContractorBean> beans = null;
		if (unloadingContractors != null && !unloadingContractors.isEmpty())
		{
			beans = new ArrayList<UnloadingContractorBean>();
			UnloadingContractorBean bean = null;
			for (UnloadingContractor unloadingContractor : unloadingContractors)
			{
				bean = new UnloadingContractorBean();	
				
				bean.setId(unloadingContractor.getId());
				bean.setContactNo(unloadingContractor.getContactNo());
				bean.setUcCode(unloadingContractor.getUcCode());
				bean.setName(unloadingContractor.getName());
				bean.setStatus(unloadingContractor.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
    
  //-----------------End of UnloadingControctor-----------------------------//
  //.........................CaneWeight......................

    @RequestMapping(value = "/saveCaneWeight", method = RequestMethod.POST, headers = { "Content-type=application/json" })
     public @ResponseBody
     boolean saveCaneWeight(@RequestBody String jsonData) throws Exception
     {
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveCaneWeight() from AgricultureHarvestingController========"+ jsonData.toString());
		    CaneWeightBean caneWeightBean = new ObjectMapper().readValue(jsonData, CaneWeightBean.class);
		    logger.info("====getAllCaneWeight() ========"+caneWeightBean.getSeason()+"---"+caneWeightBean.getBmPercentage());
		    CaneWeight caneWeight = prepareModelForCaneWeight(caneWeightBean);
		    logger.info("====Model=="+caneWeight.getSeason()+"==="+caneWeight.getBmpercentage());
		    // caneWeightService.addCaneWeight(caneWeight);
		    
		    entityList.add(caneWeight);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);

		    return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
      }
    private CaneWeight prepareModelForCaneWeight(CaneWeightBean caneWeightBean) 
   	{
    	CaneWeight caneWeight = new CaneWeight();
    	logger.info("====getAllCaneWeight() ========"+caneWeightBean.getSeason()+"---"+caneWeightBean.getBmPercentage());
    	caneWeight.setId(caneWeightBean.getId());
    	caneWeight.setSeason(caneWeightBean.getSeason());
    	caneWeight.setBmpercentage(caneWeightBean.getBmPercentage());
    	logger.info("====Model=="+caneWeight.getSeason()+"==="+caneWeight.getBmpercentage());
    	
	  // 	logger.info("====getAllCaneWeight() ========"+caneWeight);

   		return caneWeight;
   	}
    
    @RequestMapping(value = "/getCaneWeightBySeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
   	public @ResponseBody
   	CaneWeightBean getAllCaneWeight(@RequestBody String jsonData)throws Exception
   	{
   		List caneWeightmentList=caneWeightService.getCaneWeight(jsonData);
   		CaneWeightBean caneWeightBean =null;
   		if(caneWeightmentList.size()>0 && caneWeightmentList!=null)
   		{
   		CaneWeight caneWeight=(CaneWeight)caneWeightmentList.get(0);
   		caneWeightBean = prepareListofCaneWeightBean(caneWeight);
   		}
   		return caneWeightBean;
   	}
    private CaneWeightBean prepareListofCaneWeightBean(CaneWeight caneWeight)
   	{
		CaneWeightBean bean = new CaneWeightBean();		
		
		bean.setId(caneWeight.getId());
		bean.setSeason(caneWeight.getSeason());
		bean.setBmPercentage(caneWeight.getBmpercentage());	
		return bean;
    }
    
    @RequestMapping(value = "/getHarvestingSchedulePermits", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody List getHarvestingSchedulePermits(@RequestBody JSONObject jsonData)throws Exception
	{
		String season=(String)jsonData.get("season");
		Integer programNumber=(Integer)jsonData.get("programNumber");
		Integer fromRank=(Integer)jsonData.get("fromRank");
		Integer toRank=(Integer)jsonData.get("toRank");
		Integer circleCode=(Integer)jsonData.get("circle");
		Integer permitDateFlag=(Integer)jsonData.get("permitDateFlag");
		String HarvestDate=(String)jsonData.get("HarvestDate");
		List ls=preparePermitDetailsForSchedulePermit(caneWeightService.getReviseSchedulePermits(circleCode,season,fromRank,toRank,permitDateFlag,programNumber));
		return ls;
	}

	private List preparePermitDetailsForSchedulePermit(List<PermitDetails> permitDetails)
	{
		List<PermitDetailsBean> beans = null;
		if (permitDetails != null && !permitDetails.isEmpty())
		{
			beans = new ArrayList<PermitDetailsBean>();
			PermitDetailsBean bean = null;
			for (PermitDetails permitDetail : permitDetails)
			{
				bean = new PermitDetailsBean();	
				bean.setAgreementNumber(permitDetail.getAgreementno());
				
				List circleList=ahFuctionalService.getCircleRecordDetails(permitDetail.getCirclecode());
				
				String circleName=((Circle)circleList.get(0)).getCircle();
				bean.setCircle(circleName);
				if(permitDetail.getHarvestdt()!=null)
				{
					bean.setHrvFlag(false);
					bean.setHarvestDate(DateUtils.formatDate(permitDetail.getHarvestdt(),Constants.GenericDateFormat.DATE_FORMAT));
				}
				else
				{
					bean.setHrvFlag(true);
					bean.setHarvestDate("");
				}
			
				bean.setPermitNumber(permitDetail.getCompositePrKeyForPermitDetails().getPermitnumber());
				bean.setProgramNumber(permitDetail.getCompositePrKeyForPermitDetails().getProgramno());
				bean.setRank(permitDetail.getRank());
				bean.setRyotCode(permitDetail.getRyotcode());
				bean.setRyotName(ahFuctionalService.getRyotRecordName(permitDetail.getRyotcode()));
				beans.add(bean);
			}
		}
		return beans;
	}
	public static Date addDays(Date date, int days)
	{
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}
	@RequestMapping(value = "/savePermitDetailsPerHarvestingDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray savePermitDetailsPerHarvestingDate(@RequestBody JSONArray jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String Qryobj=null;
		String strQry=null;
		List permitList = new ArrayList();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Map permitMap =null;
		jsonObj = new JSONObject();
		try 
		{
			for(int i=0;i<jsonData.size();i++)
			{
				JSONObject jsonobj=(JSONObject)jsonData.get(i);
				Integer permitNumber=(Integer)jsonobj.get("permitNumber");  
				Integer programNumber=(Integer)jsonobj.get("programNumber");
				String harvestDate=(String)jsonobj.get("harvestDate");
				Boolean hrvFlag=(Boolean)jsonobj.get("hrvFlag");
				if(harvestDate != null)
				{
					if(hrvFlag)
					{
						//Modified by DMurty on 23-11-2016
						Integer a=1;
						Date hDate =(Date) df.parse(harvestDate);
						
						Date newDate =addDays(hDate, a); 
						String strfinalDate = df.format(newDate);
						
						//Modified by DMurty on 02-11-2016
						String ryotCode = (String)jsonobj.get("ryotCode");
						String ryotName = (String)jsonobj.get("ryotName");
						String circleName=(String)jsonobj.get("circle");  
						String agreementNumber=(String)jsonobj.get("agreementNumber");
						int circleCode = ahFuctionalService.getCircleCodeFromAgreementDetailsNew(agreementNumber);
						
						permitMap = new HashMap();
						permitMap.put("PERMIT_NO", permitNumber);
						permitMap.put("PROGRAM_NO", programNumber);
						permitMap.put("HARVEST_DATE", harvestDate);
						permitMap.put("RYOT_CODE", ryotCode);
						permitMap.put("RYOT_NAME", ryotName);
						permitMap.put("CIRCLE_CODE", circleCode);
						permitMap.put("CIRCLE_NAME", circleName);
						permitMap.put("AGREEMET_NO", agreementNumber);
						permitList.add(permitMap);

						strQry = "UPDATE PermitDetails set harvestdt='"+DateUtils.getSqlDateFromString(harvestDate,Constants.GenericDateFormat.DATE_FORMAT)+"',validitydate='"+DateUtils.getSqlDateFromString(strfinalDate,Constants.GenericDateFormat.DATE_FORMAT)+"' WHERE permitnumber ="+permitNumber+" and programno="+programNumber;
						Qryobj = strQry;
						UpdateList.add(Qryobj);
						
					}
				}
			}
			logger.info("PermitSize:"+permitList.size()+" permitHarvestDateList:"+permitList);
			trmentityList=null;
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String cdate=(dateFormat.format(date));
			Calendar cal = Calendar.getInstance();
			String ctime=(dateFormat.format(cal.getTime()));
			PermitDetails pd=null;
		
			if(isInsertSuccess == true)
			{
				if(permitList != null && permitList.size()>0)
				{
					for(int i=0;i<permitList.size();i++)
					{
						permitMap = new HashMap();
						permitMap = (Map) permitList.get(i);
						
						int permitNo = (Integer) permitMap.get("PERMIT_NO");
						int programNo = (Integer) permitMap.get("PROGRAM_NO");
						String harvestDate = (String) permitMap.get("HARVEST_DATE");
						String ryotCode = (String) permitMap.get("RYOT_CODE");
						String ryotName = (String) permitMap.get("RYOT_NAME");
						int circleCode = (Integer) permitMap.get("CIRCLE_CODE");
						String agreementNumber = (String) permitMap.get("AGREEMET_NO");
						String circleName = (String) permitMap.get("CIRCLE_NAME");

						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason("2016-2017",agreementNumber);
						String ptorrt = agreementDetails.get(0).getPlantorratoon();
						int nptorrt = Integer.parseInt(ptorrt);
						//Added by sahadeva 14/11/2016
						List<PermitDetails> permitDetails1 = ahFuctionalService.getValidityDate(permitNo);
						Date vdate=permitDetails1.get(0).getValiditydate();
						
						String strvDate = df.format(vdate);
						//
						List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(ryotCode);
						
						CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
						compositePrKeyForPermitDetails.setPermitnumber(permitNo);
						compositePrKeyForPermitDetails.setProgramno(programNo);
						
						pd=new PermitDetails();
						PermitDetails permitDetails=(PermitDetails) hibernateDao.get(PermitDetails.class, compositePrKeyForPermitDetails);
						
						List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetails.getZonecode());
						List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetails.getLandvilcode());
						List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetails.getVarietycode());
						List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
						
						
						jsonObj.put("issuccess", "true");
						jsonObj.put("ryotcode",ryotCode);
						jsonObj.put("ryotname",ryot.get(0).getRyotname());
						jsonObj.put("fathername",ryot.get(0).getRelativename());
						jsonObj.put("circlecode",circleCode);
						jsonObj.put("landvilcode",permitDetails.getLandvilcode());
						jsonObj.put("permitnumber",permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
						jsonObj.put("programno",permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
						jsonObj.put("circle",circleName);
						jsonObj.put("zone",zone.get(0).getZone());
						jsonObj.put("variety",variety.get(0).getVariety());
						jsonObj.put("plantorratoon",crop.get(0).getCroptype());
						byte[] encoded = Base64.encodeBase64(permitDetails.getQrcodedata()); 
						jsonObj.put("qrcodedata",new String(encoded));
						jsonObj.put("currenttime",ctime);
						jsonObj.put("currentdate",cdate);
						//Added by DMurty on 03-11-2016
						jsonObj.put("village",village);
						jsonObj.put("rank",permitDetails.getRank());
						jsonObj.put("harvestDate",harvestDate);
						//Added by sahadeva on 14-11-2016
						jsonObj.put("validityDate",strvDate);
						jArray.add(jsonObj);
					}
				}
			}
			else
			{
				jsonObj.put("issuccess", "false");
				jArray.add(jsonObj);
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		return jArray;
	 }
	//Added by DMurty on 02-11-2016
	 @RequestMapping(value = "/getPermitDetailsByHarvestingDates", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	 public @ResponseBody JSONArray getPermitDetailsByHarvestingDates(@RequestBody JSONObject jsonData)throws Exception
	 {
		 String fromdate=(String)jsonData.get("fromdate");
		 String todate=(String)jsonData.get("todate");
		 logger.info("====getPermitDetailsByHarvestingDates()  from CaneReceiptController======== fromdate"+fromdate);
		 logger.info("====getPermitDetailsByHarvestingDates()  from CaneReceiptController======== todate"+todate);
		 JSONArray jArray = new JSONArray();
		 Date date1=DateUtils.getSqlDateFromString(fromdate,Constants.GenericDateFormat.DATE_FORMAT);
		 Date date2=DateUtils.getSqlDateFromString(todate,Constants.GenericDateFormat.DATE_FORMAT);
		 jArray=prepareBeanForPermitPrint(caneWeightService.getPermitPrintByHarvestingDates(date1,date2));
		 return jArray;
	 }
	private JSONArray prepareBeanForPermitPrint(List<PermitDetails> permitDetails)
	{
		logger.info("====prepareBeanForPermitPrint()=====");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String cdate=(dateFormat.format(date));
		Calendar cal = Calendar.getInstance();
		String ctime=(dateFormat.format(cal.getTime()));
		if (permitDetails != null && !permitDetails.isEmpty())
		{
			for (PermitDetails permitDetail : permitDetails)
			{
				List circleList=ahFuctionalService.getCircleRecordDetails(permitDetail.getCirclecode());
				String circleName=((Circle)circleList.get(0)).getCircle();
				logger.info("====prepareBeanForPermitPrint()===== circleName----"+circleName);

				jsonObj.put("issuccess", "true");
				jsonObj.put("ryotcode",permitDetail.getRyotcode());
				List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(permitDetail.getRyotcode());
				jsonObj.put("ryotname",ryot.get(0).getRyotname());
				jsonObj.put("fathername",ryot.get(0).getRelativename());
				jsonObj.put("circlecode",permitDetail.getCirclecode());
				jsonObj.put("landvilcode",permitDetail.getLandvilcode());
				
				CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
				jsonObj.put("permitnumber",permitDetail.getCompositePrKeyForPermitDetails().getPermitnumber());
				jsonObj.put("programno",permitDetail.getCompositePrKeyForPermitDetails().getProgramno());
				
				jsonObj.put("circle",circleName);
				List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetail.getZonecode());
				List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetail.getLandvilcode());
				List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetail.getVarietycode());
				
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(permitDetail.getSeason(),permitDetail.getAgreementno());
				String ptorrt = agreementDetails.get(0).getPlantorratoon();
				int nptorrt = Integer.parseInt(ptorrt);
				List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
				jsonObj.put("zone",zone.get(0).getZone());
				jsonObj.put("variety",variety.get(0).getVariety());
				jsonObj.put("plantorratoon",crop.get(0).getCroptype());
				byte[] encoded = Base64.encodeBase64(permitDetail.getQrcodedata()); 
				jsonObj.put("qrcodedata",new String(encoded));
				jsonObj.put("currenttime",ctime);
				jsonObj.put("currentdate",cdate);
				//Added by DMurty on 03-11-2016
				jsonObj.put("villagename",village.get(0).getVillage());
				jsonObj.put("rank",permitDetail.getRank());
				Date havestDate = permitDetail.getHarvestdt();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strhavestDate = df.format(havestDate);
				jsonObj.put("harvestDate",strhavestDate);
				//Added by DMurty on 24-11-2016
				Date validityDate = permitDetail.getValiditydate();
				if(validityDate != null)
				{
					String strvalidityDate = df.format(validityDate);
					jsonObj.put("validityDate",strvalidityDate);
				}
				jArray.add(jsonObj);
			}
		}
		else
		{
			jsonObj.put("issuccess", "false");
			jArray.add(jsonObj);
		}
		logger.info("====prepareBeanForPermitPrint()===== jArray"+jArray);
		return jArray;
	}
	
	
	
}
