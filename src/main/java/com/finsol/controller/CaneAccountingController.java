package com.finsol.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.AccountGroupBean;
import com.finsol.bean.AccountSubGroupBean;
import com.finsol.bean.CaneAccountingRulesBean;
import com.finsol.bean.CaneAccountingRulesForPCACBean;
import com.finsol.bean.CaneAccountingRulesSeasonBean;
import com.finsol.bean.CaneAcctAmountsSeasonBean;
import com.finsol.bean.HarvestingContractorBean;
import com.finsol.bean.HarvestingContractorSuretyBean;
import com.finsol.bean.StandardDeductionsBean;
import com.finsol.bean.TieUpLoanInterestBean;
import com.finsol.bean.TieUpLoanInterestDetailsBean;
import com.finsol.bean.TransportingContractorBean;
import com.finsol.bean.TransportingContractorSuretyBean;
import com.finsol.model.AccountGroup;
import com.finsol.model.AccountMaster;
import com.finsol.model.AccountSubGroup;
import com.finsol.model.BankLoanIntRates;
import com.finsol.model.BankLoanRepayDt;
import com.finsol.model.CaneAccountingRules;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.HCSuretyDetails;
import com.finsol.model.HarvestingContractors;
import com.finsol.model.StandardDeductions;
import com.finsol.model.TCSuretyDetails;
import com.finsol.model.TransportingContractors;
import com.finsol.service.AccountGroupService;
import com.finsol.service.AccountSubGroupService;
import com.finsol.service.AuditTrailService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CaneAccountingRulesService;
import com.finsol.service.CommonService;
import com.finsol.service.CompanyAdvanceService;
import com.finsol.service.HarvestingContractorService;
import com.finsol.service.StandardDeductionsService;
import com.finsol.service.TieUpLoanInterestService;
import com.finsol.service.TransportingContractorService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**
 * @author DMurty
 */
@Controller
public class CaneAccountingController
{
	private static final Logger logger = Logger.getLogger(CaneAccountingController.class);
	
	@Autowired
	private AccountGroupService accountGroupService;
	@Autowired
	private CompanyAdvanceService companyAdvanceService;
	@Autowired
	private AccountSubGroupService accountSubGroupService;

	@Autowired
	private StandardDeductionsService standardDeductionsService;
	
	@Autowired
	private HarvestingContractorService harvestingContractorService;
	
	@Autowired
	private TransportingContractorService transportingContractorService;
	
	@Autowired
	private TieUpLoanInterestService tieUpLoanInterestService;
	
	@Autowired
	private CaneAccountingRulesService caneAccountingRulesService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	
	@Autowired
	private AuditTrailService auditTrailService;

	@RequestMapping(value = "/saveAccountGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveAccountGroup(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			AccountGroupBean accountGroupBean = new ObjectMapper().readValue(jsonData, AccountGroupBean.class);
			AccountGroup accountGroup = prepareModel(accountGroupBean);
			//accountGroupService.addAccountGroup(accountGroup);
			entityList.add(accountGroup);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	private AccountGroup prepareModel(AccountGroupBean accountGroupBean)
	{
		String accgroup=null;
		AccountGroup accountGroup = new AccountGroup();
		String modifyFlag=accountGroupBean.getModifyFlag();
		int accountgroupcode=commonService.getMaxIDforColumn("AccountGroup", "accountgroupcode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			accountGroup.setAccountgroupcode(accountGroupBean.getAccountGroupCode());
		}
		else
		{
			accountGroup.setAccountgroupcode(accountgroupcode);
		}
		//accountGroup.setId(accountGroupBean.getId());
		//accountGroup.setAccountgroupcode(accountGroupBean.getAccountGroupCode());
		//uppercase done by naidu 12-07-2016
		accgroup=accountGroupBean.getAccountGroup();
		if ("".equals(accgroup) || "null".equals(accgroup) || (accgroup == null)) 
			accgroup = "NA";
		//else
		accountGroup.setAccountgroup(accgroup.toUpperCase());
		// end modification
		String description = accountGroupBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		accountGroup.setDescription(description);
		accountGroup.setStatus(accountGroupBean.getStatus());
		
		return accountGroup;
	}
	
	@RequestMapping(value = "/getAllAccountGroups", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<AccountGroupBean> getAllAccountGroups(@RequestBody String jsonData)throws Exception {
		
		List<AccountGroupBean> accountGroupBeans = prepareListofBean(accountGroupService.listAllAccountGroups());
		
		return accountGroupBeans;
	}
	
	private List<AccountGroupBean> prepareListofBean(List<AccountGroup> accountGroups) {
		List<AccountGroupBean> beans = null;
		
		if (accountGroups != null && !accountGroups.isEmpty()) 
		{
			beans = new ArrayList<AccountGroupBean>();
			AccountGroupBean bean = null;
			for (AccountGroup accountGroup : accountGroups) 
			{
				bean = new AccountGroupBean();

				bean.setId(accountGroup.getId());
				bean.setAccountGroupCode(accountGroup.getAccountgroupcode());
				bean.setAccountGroup(accountGroup.getAccountgroup());
				bean.setDescription(accountGroup.getDescription());
				bean.setStatus(accountGroup.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
	@RequestMapping(value = "/saveAccountSubGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveAccountSubGroup(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			AccountSubGroupBean accountSubGroupBean = new ObjectMapper().readValue(jsonData, AccountSubGroupBean.class);
			AccountSubGroup accountSubGroup = prepareModelForAccountSubGroup(accountSubGroupBean);
			entityList.add(accountSubGroup);
			//Added by sahadeva 31/10/2016
			if(accountSubGroupBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(accountSubGroupBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(accountSubGroupBean.getId(),accountSubGroup,accountSubGroupBean.getScreenName());		
			}
			// modification end
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			//accountSubGroupService.addAccountSubGroup(accountSubGroup);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	private AccountSubGroup prepareModelForAccountSubGroup(AccountSubGroupBean accountSubGroupBean)
	{
		AccountSubGroup accountSubGroup = new AccountSubGroup();
		
		String modifyFlag=accountSubGroupBean.getModifyFlag();
		int accountsubgroupcode=commonService.getMaxIDforColumn("AccountSubGroup", "accountsubgroupcode");
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
		accountSubGroup.setAccountsubgroupcode(accountSubGroupBean.getAccountSubGroupCode());	
		}
		else
		{
		accountSubGroup.setAccountsubgroupcode(accountsubgroupcode);
		}
		accountSubGroup.setId(accountSubGroupBean.getId());
		accountSubGroup.setAccountgroupcode(accountSubGroupBean.getAccountGroupCode());
		
		//uppercase done by naidu 13-07-2016
		String accSubGroup=accountSubGroupBean.getAccountSubGroup();
		if ("".equals(accSubGroup) || "null".equals(accSubGroup) || (accSubGroup == null)) 
			accSubGroup = "NA";
		//else
		accountSubGroup.setAccountsubgroup(accSubGroup.toUpperCase());
		
		String description = accountSubGroupBean.getDescription();
		if ("".equals(description) || "null".equals(description) || (description == null)) 
		{
			description = "NA";
		}
		else
		{
			description = description.toUpperCase();
		}
		accountSubGroup.setDescription(description);
		accountSubGroup.setStatus(accountSubGroupBean.getStatus());
		
		return accountSubGroup;
	}
	
	
	@RequestMapping(value = "/getAllAccountSubGroups", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<AccountSubGroupBean> getAllAccountSubGroups(@RequestBody String jsonData)throws Exception 
	{
		
		List<AccountSubGroupBean> accountSubGroupBeans = prepareListofForAccountSubGroupBean(accountSubGroupService.listAllAccountSubGroups());
		return accountSubGroupBeans;
	}
	
	private List<AccountSubGroupBean> prepareListofForAccountSubGroupBean(List<AccountSubGroup> accountSubGroups) {
		List<AccountSubGroupBean> beans = null;
		
		if (accountSubGroups != null && !accountSubGroups.isEmpty()) 
		{
			beans = new ArrayList<AccountSubGroupBean>();
			AccountSubGroupBean bean = null;
			for (AccountSubGroup accountSubGroup : accountSubGroups) 
			{
				bean = new AccountSubGroupBean();
				
				bean.setId(accountSubGroup.getId());
				bean.setAccountGroupCode(accountSubGroup.getAccountgroupcode());
				bean.setAccountSubGroupCode(accountSubGroup.getAccountsubgroupcode());
				bean.setAccountSubGroup(accountSubGroup.getAccountsubgroup());
				bean.setDescription(accountSubGroup.getDescription());
				bean.setStatus(accountSubGroup.getStatus());
				bean.setModifyFlag("Yes");
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//------------------------------ StandardDeduction ------------------------//

    @RequestMapping(value = "/saveStandardDeductions", method = RequestMethod.POST, headers = { "Content-type=application/json" })
     public @ResponseBody
     boolean saveStandardDeductions(@RequestBody String jsonData) throws Exception
     {
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			logger.info("====saveStandardDeductions() from AgricultureHarvestingController========"+ jsonData.toString());
			StandardDeductionsBean standardDeductionsBean = new ObjectMapper().readValue(jsonData, StandardDeductionsBean.class);
		    StandardDeductions standardDeductions = prepareModelForStandardDeductions(standardDeductionsBean);
		    //standardDeductionsService.addStandardDeductions(standardDeductions);
		    entityList.add(standardDeductions);
		    //Added by sahadeva 31/10/2016
		    if(standardDeductionsBean.getId()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(standardDeductionsBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrail(standardDeductionsBean.getId(),standardDeductions,standardDeductionsBean.getScreenName());		
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
		    return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
      }
    
    @RequestMapping(value = "/saveStandardDeductionsOrder", method = RequestMethod.POST, headers = { "Content-type=application/json" })
    public @ResponseBody
    boolean saveStandardDeductionsOrder(@RequestBody JSONArray jsonData) throws Exception
    {
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		String strQry = null;
		try 
		{
    		logger.info("====jsonData===="+jsonData);
	    	for(int i=0;i<jsonData.size();i++)
			{
	    		JSONObject jsn=(JSONObject)jsonData.get(i);    	
	    		
	    		String name = jsn.get("name").toString();
	    		int id = Integer.parseInt(jsn.get("stdDedCode").toString());
	    		int orderId = Integer.parseInt(jsn.get("deductionOrder").toString());
	    		
	    		logger.info("====name===="+name);
	    		logger.info("====id===="+id);
	    		logger.info("====orderId===="+orderId);
	    		//uppercase added by naidu on 12-07-2016
	    		
	    		
	    		strQry = "UPDATE StandardDeductions set name='"+name.toUpperCase()+"',deductionOrder="+orderId+" WHERE id ="+id;
	    		
	    		Qryobj = strQry;
				UpdateList.add(Qryobj);
	    		
	    		//strQry = "UPDATE StandardDeductions set name='"+jsn.get("name").toString()+"',deductionOrder="+Integer.parseInt(jsn.get("deductionOrder").toString())+" WHERE id ="+Integer.parseInt(jsn.get("stdDedCode").toString());
	    		//standardDeductionsService.updateStandardDeductionDetails(jsn.get("name").toString(),Integer.parseInt(jsn.get("stdDedCode").toString()),Integer.parseInt(jsn.get("deductionOrder").toString()));
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
		    return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
     }
    
    private StandardDeductions prepareModelForStandardDeductions(StandardDeductionsBean standardDeductionsBean) 
   	{
    	StandardDeductions standardDeductions = new StandardDeductions();
   		String modifyFlag=standardDeductionsBean.getModifyFlag();
   		int stdDedCode=commonService.getMaxIDforColumn("StandardDeductions", "stdDedCode");
   		if("Yes".equalsIgnoreCase(modifyFlag))
   		{
   			standardDeductions.setStdDedCode(standardDeductionsBean.getStdDedCode());	
   		}
   		else
   		{
   		standardDeductions.setStdDedCode(stdDedCode);
   		}
    	standardDeductions.setId(standardDeductionsBean.getId());
    	//standardDeductions.setStdDedCode(standardDeductionsBean.getStdDedCode());
    	//uppercase done by naidu 12-07-2016
    	String dedname=standardDeductionsBean.getName();
		if ("".equals(dedname) || "null".equals(dedname) || (dedname == null)) 
			dedname = "NA";
		//else
    	standardDeductions.setName(dedname.toUpperCase());
		
    	standardDeductions.setAmount(standardDeductionsBean.getAmount());
    	standardDeductions.setAmountValidity(standardDeductionsBean.getAmountValidity());
    	standardDeductions.setDeductionOrder(standardDeductionsBean.getDeductionOrder());
    	standardDeductions.setStatus(standardDeductionsBean.getStatus());
    	
    	if(standardDeductionsBean.getStdDedCode() == 1)
    	{
    		standardDeductions.setTablename("cdcdetailsbyrotanddate");
    		standardDeductions.setColumnname("cdcamount");
    	}
    	else if(standardDeductionsBean.getStdDedCode() == 2)
    	{
    		standardDeductions.setTablename("ucdetailsbyrotanddate");
    		standardDeductions.setColumnname("udcamount");
    	}
    	else if(standardDeductionsBean.getStdDedCode() == 3)
    	{
    		standardDeductions.setTablename("transpchgsmrybyryotcontanddate");
    		standardDeductions.setColumnname("totalamount");
    	}
    	else if(standardDeductionsBean.getStdDedCode() == 4)
    	{
    		standardDeductions.setTablename("harvchgdtlsbyryotcontanddate");
    		standardDeductions.setColumnname("totalamount");
    	}
    	else if(standardDeductionsBean.getStdDedCode() == 5)
    	{
    		standardDeductions.setTablename("NA");
    		standardDeductions.setColumnname("NA");
    	}
    	/*else if(standardDeductionsBean.getStdDedCode() == 6)
    	{
    		standardDeductions.setTablename("AdvancePrincipleAmounts");
    		standardDeductions.setColumnname("principle");
    	}*/
    	
   		return standardDeductions;
   	}
    
    @RequestMapping(value = "/getAllStandardDeductions", method = RequestMethod.POST, headers = { "Content-type=application/json" })
   	public @ResponseBody
   	List<StandardDeductionsBean> getAllStandardDeductions(@RequestBody String jsonData)throws Exception
   	{   		
   		List<StandardDeductionsBean> standardDeductionsBeanBeans = prepareListofStandardDeductionsBeans(standardDeductionsService.listStandardDeduction());
   		return standardDeductionsBeanBeans;
   	}
    
    private List<StandardDeductionsBean> prepareListofStandardDeductionsBeans(List<StandardDeductions> standardDeductions)
   	{
   		List<StandardDeductionsBean> beans = null;
   		if (standardDeductions != null && !standardDeductions.isEmpty())
   		{
   			beans = new ArrayList<StandardDeductionsBean>();
   			StandardDeductionsBean bean = null;
   			for (StandardDeductions standardDeduction : standardDeductions)
   			{
   				bean = new StandardDeductionsBean();	
   				
   				bean.setId(standardDeduction.getId());
   				bean.setStdDedCode(standardDeduction.getStdDedCode());
   				bean.setName(standardDeduction.getName());
   				bean.setAmount(standardDeduction.getAmount());
   				bean.setAmountValidity(standardDeduction.getAmountValidity());
   				bean.setDeductionOrder(standardDeduction.getDeductionOrder());
   				bean.setStatus(standardDeduction.getStatus());
   				bean.setModifyFlag("Yes");
   				beans.add(bean);
   					
   			}
   		}
   		return beans;
         }
    
    
	//------------------------------ End Of StandardDeduction ------------------------------//
    
	//------------------------------ HarvestingContractor --------------------------------//
    
    @RequestMapping(value = "/saveHarvestingContractor", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveHarvestingContractor(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception
	{	
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{				
			JSONObject harvestCOntractorData=(JSONObject) jsonData.get("formData");			
			JSONArray suretyData=(JSONArray) jsonData.get("gridData");			
			
			HarvestingContractorBean harvestingContractorBean = new ObjectMapper().readValue(harvestCOntractorData.toString(), HarvestingContractorBean.class);
			HarvestingContractors harvestingContractor = prepareModelForHarvestingContractors(harvestingContractorBean);
			entityList.add(harvestingContractor);
			
			harvestingContractorService.deleteHCSuretyDetails(harvestingContractor);
			
			for(int i=0;i<suretyData.size();i++)
			{
				HarvestingContractorSuretyBean harvestingContractorSuretyBean = new ObjectMapper().readValue(suretyData.get(i).toString(), HarvestingContractorSuretyBean.class);
				HCSuretyDetails hcSuretyDetails = prepareModelForHCSurety(harvestingContractorSuretyBean,harvestingContractor);	
				//harvestingContractorService.addHCSuretyDetails(hcSuretyDetails);
				entityList.add(hcSuretyDetails);

			}
			
			AccountMaster accountmaster=new AccountMaster();
			accountmaster.setAccountcode(harvestingContractor.getAccountcode());
			accountmaster.setAccountname(harvestingContractorBean.getHarvester());
			accountmaster.setAccounttype(4);
			int AccGrpCode = companyAdvanceService.GetAccGrpCode("Deductions");
			accountmaster.setAccountgroupcode(AccGrpCode);
			//Here Acc Sub Group code for company advance is 3.Discussed with sir on 13-04-216
			
			int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCodeNew("Cane Deductions");
			accountmaster.setAccountsubgroupcode(AccSubGrpCode);
			
			
			entityList.add(accountmaster);
			//harvestingContractorService.addHarvestingContractor(harvestingContractor);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}		
	
		
	private HarvestingContractors prepareModelForHarvestingContractors(HarvestingContractorBean harvestingContractorBean) {

		String accountnumber=null;
		HarvestingContractors harvestingContractors = new HarvestingContractors();
		
		String modifyFlag=harvestingContractorBean.getModifyFlag();
		int harvestercode=commonService.getMaxIDforColumn("HarvestingContractors", "harvestercode");
		
		int hcprefix=commonService.getMaxIDforContract("HarvestingContractors", "id","02");
		if(hcprefix<10)
			accountnumber="02"+"000"+hcprefix;
		else if(hcprefix>9 && hcprefix<100)
			accountnumber="02"+"00"+hcprefix;
		else if(hcprefix>9 && hcprefix<100)
			accountnumber="02"+"00"+hcprefix;
		else if(hcprefix>99 && hcprefix<1000)
			accountnumber="02"+hcprefix;
			
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			harvestingContractors.setHarvestercode(harvestingContractorBean.getHarvesterCode());
			harvestingContractors.setAccountcode(harvestingContractorBean.getAccountCode());
			
		}
		else
		{
			harvestingContractors.setHarvestercode(harvestercode);
			harvestingContractors.setAccountcode(accountnumber);
		}
		harvestingContractors.setId(harvestingContractorBean.getId());
		harvestingContractors.setAadhaarnumber(harvestingContractorBean.getAadhaarNumber());
		harvestingContractors.setAccountnumber(harvestingContractorBean.getAccountNumber());
		//uppercase done by naidu on 12-07-2016
		String address = harvestingContractorBean.getAddress();
		if ("".equals(address) || "null".equals(address) || (address == null)) 
			address = "NA";
	//	else	
		harvestingContractors.setAddress(address.toUpperCase());
		
		harvestingContractors.setBankcode(harvestingContractorBean.getBankCode());
		harvestingContractors.setBranchcode(harvestingContractorBean.getBranchCode());
		harvestingContractors.setContactnumber(harvestingContractorBean.getContactNumber());
		//done by naidu
		String harvester = harvestingContractorBean.getHarvester();
		if ("".equals(harvester) || "null".equals(harvester) || (harvester == null)) 
			harvester = "NA";
	//	else
		harvestingContractors.setHarvester(harvester.toUpperCase());
		//harvestingContractors.setHarvestercode(harvestingContractorBean.getHarvesterCode());
		harvestingContractors.setIfsccode(harvestingContractorBean.getIfscCode());
		harvestingContractors.setPanno(harvestingContractorBean.getPanNumber());
		harvestingContractors.setStatus(harvestingContractorBean.getStatus());
		harvestingContractors.setPrefix("02");
		
		
		return harvestingContractors;
	}

	
	private HCSuretyDetails prepareModelForHCSurety(HarvestingContractorSuretyBean harvestingContractorSuretyBean,HarvestingContractors harvestingContractor) {

		HCSuretyDetails hcSuretyDetails=new HCSuretyDetails();
		
		hcSuretyDetails.setId(harvestingContractorSuretyBean.getId());
		hcSuretyDetails.setHarvestercode(harvestingContractor.getHarvestercode());		
		hcSuretyDetails.setPanno(harvestingContractorSuretyBean.getPanNo());
		hcSuretyDetails.setContactno(harvestingContractorSuretyBean.getContactNo());
		//done by naidu 12-07-2016
		String surety =harvestingContractorSuretyBean.getSurety();
		if ("".equals(surety) || "null".equals(surety) || (surety == null)) 
			surety = "NA";
	//	else
		hcSuretyDetails.setSurety(surety.toUpperCase());
		
		hcSuretyDetails.setAadhaarnumber(harvestingContractorSuretyBean.getAadhaarNumber());
		
		return hcSuretyDetails;
	}
	
	
	@RequestMapping(value = "/getAllHarvestingContractors", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllHarvestingContractors(@RequestBody String jsonData) throws Exception
	{
		HarvestingContractorBean harvestingContractorBean =null;		
		org.json.JSONObject response = new org.json.JSONObject();
		
		HarvestingContractors harvestingContractors=harvestingContractorService.getHarvestingContractors(Integer.parseInt(jsonData));
		
		if(harvestingContractors!=null)
			harvestingContractorBean =prepareBeanForHarvestingContractors(harvestingContractors);
			
		
		List<HCSuretyDetails> hcSuretyDetails =harvestingContractorService.getHCDetailsByCode(harvestingContractors.getHarvestercode());
		
		List<HarvestingContractorSuretyBean> harvestingContractorSuretyBeans =prepareListofBeanForHCSuretyDetails(hcSuretyDetails);
		
		
		org.json.JSONObject harvesterJson=new org.json.JSONObject(harvestingContractorBean);
		org.json.JSONArray hcDetailsArray = new org.json.JSONArray(harvestingContractorSuretyBeans);
		
		response.put("formData",harvesterJson);
		response.put("gridData",hcDetailsArray);			
		
		return response.toString();
	}
	
	
	private HarvestingContractorBean prepareBeanForHarvestingContractors(HarvestingContractors harvestingContractor) 
	{
		
		HarvestingContractorBean bean = new HarvestingContractorBean();			
		
		bean.setId(harvestingContractor.getId());
		bean.setStatus(harvestingContractor.getStatus());
		bean.setAadhaarNumber(harvestingContractor.getAadhaarnumber());
		bean.setAccountNumber(harvestingContractor.getAccountnumber());
		bean.setAddress(harvestingContractor.getAddress());
		bean.setBankCode(harvestingContractor.getBankcode());
		bean.setBranchCode(harvestingContractor.getBranchcode());
		bean.setContactNumber(harvestingContractor.getContactnumber());
		bean.setPanNumber(harvestingContractor.getPanno());
		bean.setHarvester(harvestingContractor.getHarvester());
		bean.setHarvesterCode(harvestingContractor.getHarvestercode());
		bean.setIfscCode(harvestingContractor.getIfsccode());
		bean.setModifyFlag("Yes");	
		bean.setAccountCode(harvestingContractor.getAccountcode());
				
		return bean;
	}
	
	
	private List<HarvestingContractorSuretyBean> prepareListofBeanForHCSuretyDetails(List<HCSuretyDetails> hcSuretyDetails) 
	{
		List<HarvestingContractorSuretyBean> beans = null;
		if (hcSuretyDetails != null && !hcSuretyDetails.isEmpty())
		{
			beans = new ArrayList<HarvestingContractorSuretyBean>();
			HarvestingContractorSuretyBean bean = null;
			for (HCSuretyDetails hcSuretyDetail : hcSuretyDetails)
			{
				bean = new HarvestingContractorSuretyBean();

				bean.setId(hcSuretyDetail.getId());
				bean.setAadhaarNumber(hcSuretyDetail.getAadhaarnumber());
				bean.setContactNo(hcSuretyDetail.getContactno());
				bean.setHarvesterCode(hcSuretyDetail.getHarvestercode());
				bean.setPanNo(hcSuretyDetail.getPanno());
				bean.setSurety(hcSuretyDetail.getSurety());				
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//------------------------------End Of  HarvestingContractor ------------------------//

	//------------------------------ Transporting Contractor --------------------------------//
    
    @RequestMapping(value = "/saveTransportingContractor", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveTransportingContractor(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception
	{	
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{				
			JSONObject transportContractorData=(JSONObject) jsonData.get("formData");			
			JSONArray suretyData=(JSONArray) jsonData.get("gridData");			
			
			TransportingContractorBean transportingContractorBean = new ObjectMapper().readValue(transportContractorData.toString(), TransportingContractorBean.class);
			TransportingContractors transportingContractor = prepareModelForTransportingContractors(transportingContractorBean);
			entityList.add(transportingContractor);
			transportingContractorService.deleteHCSuretyDetails(transportingContractor);
			
			for(int i=0;i<suretyData.size();i++)
			{
				TransportingContractorSuretyBean transportingContractorSuretyBean = new ObjectMapper().readValue(suretyData.get(i).toString(), TransportingContractorSuretyBean.class);
				TCSuretyDetails tcSuretyDetails = prepareModelForTCSurety(transportingContractorSuretyBean,transportingContractor);	
				entityList.add(tcSuretyDetails);
			}
			
			AccountMaster accountmaster=new AccountMaster();
			accountmaster.setAccountcode(transportingContractor.getAccountcode());
			//done by naidu
			String transporter =transportingContractorBean.getTransporter();
			if ("".equals(transporter) || "null".equals(transporter) || (transporter == null)) 
				transporter = "NA";
			//else
			accountmaster.setAccountname(transporter.toUpperCase());
			
			accountmaster.setAccounttype(4);
			int AccGrpCode = companyAdvanceService.GetAccGrpCode("Deductions");
			accountmaster.setAccountgroupcode(AccGrpCode);
			//Here Acc Sub Group code for company advance is 3.Discussed with sir on 13-04-216
			int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCodeNew("Cane Deductions");
			accountmaster.setAccountsubgroupcode(AccSubGrpCode);
			
			entityList.add(accountmaster);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);	
			
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
	}		
	
		
	private TransportingContractors prepareModelForTransportingContractors(TransportingContractorBean transportingContractorBean) {
		String accountnumber=null;
		TransportingContractors transportingContractors = new TransportingContractors();
		String modifyFlag=transportingContractorBean.getModifyFlag();
		int transportercode=commonService.getMaxIDforColumn("TransportingContractors", "transportercode");
		int tcprefix=commonService.getMaxIDforContract("TransportingContractors", "id","01");
		if(tcprefix<10)
			accountnumber="01"+"000"+tcprefix;
		else if(tcprefix>9 && tcprefix<100)
			accountnumber="01"+"00"+tcprefix;
		else if(tcprefix>9 && tcprefix<100)
			accountnumber="01"+"00"+tcprefix;
		else if(tcprefix>99 && tcprefix<1000)
			accountnumber="01"+tcprefix;
			
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			transportingContractors.setTransportercode(transportingContractorBean.getTransporterCode());
			transportingContractors.setAccountcode(transportingContractorBean.getAccountCode());
		}
		else
		{
			transportingContractors.setTransportercode(transportercode);
			transportingContractors.setAccountcode(accountnumber);
		}
		transportingContractors.setId(transportingContractorBean.getId());
		transportingContractors.setAadhaarnumber(transportingContractorBean.getAadhaarNumber());
		transportingContractors.setAccountnumber(transportingContractorBean.getAccountNumber());
		//done by naidu 12-07-2016
		String address =transportingContractorBean.getAddress();
		if ("".equals(address) || "null".equals(address) || (address == null)) 
			address = "NA";
	//	else
		transportingContractors.setAddress(address.toUpperCase());
		
		transportingContractors.setBankcode(transportingContractorBean.getBankCode());
		transportingContractors.setBranchcode(transportingContractorBean.getBranchCode());
		transportingContractors.setContactnumber(transportingContractorBean.getContactNumber());
		//done by naidu
		
		String transporter =transportingContractorBean.getTransporter();
		if ("".equals(transporter) || "null".equals(transporter) || (transporter == null)) 
			transporter = "NA";
	//	else
		transportingContractors.setTransporter(transporter.toUpperCase());
		
		transportingContractors.setTransportercode(transportingContractorBean.getTransporterCode());
		transportingContractors.setIfsccode(transportingContractorBean.getIfscCode());
		transportingContractors.setPanno(transportingContractorBean.getPanNumber());
		transportingContractors.setStatus(transportingContractorBean.getStatus());
		
		return transportingContractors;
	}

	
	private TCSuretyDetails prepareModelForTCSurety(TransportingContractorSuretyBean transportingContractorSuretyBean,TransportingContractors transportingContractor) {

		TCSuretyDetails tcSuretyDetails=new TCSuretyDetails();
		
		tcSuretyDetails.setId(transportingContractorSuretyBean.getId());
		tcSuretyDetails.setTransportercode(transportingContractor.getTransportercode());		
		tcSuretyDetails.setPanno(transportingContractorSuretyBean.getPanNo());
		tcSuretyDetails.setContactno(transportingContractorSuretyBean.getContactNo());
		//done by naidu
		String surety =transportingContractorSuretyBean.getSurety();
		if ("".equals(surety) || "null".equals(surety) || (surety == null)) 
			surety = "NA";
		//else
		tcSuretyDetails.setSurety(surety.toUpperCase());
		
		tcSuretyDetails.setAadhaarnumber(transportingContractorSuretyBean.getAadhaarNumber());
		
		return tcSuretyDetails;
	}
	
	
	@RequestMapping(value = "/getAllTransportingContractors", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllTransportingContractors(@RequestBody String jsonData) throws Exception
	{
		TransportingContractorBean transportingContractorBean =null;		
		org.json.JSONObject response = new org.json.JSONObject();
		
		TransportingContractors transportingContractors=transportingContractorService.getTransportingContractors(Integer.parseInt(jsonData));
		
		if(transportingContractors!=null)
			transportingContractorBean =prepareBeanForTransportingContractors(transportingContractors);
			
		
		List<TCSuretyDetails> tcSuretyDetails =transportingContractorService.getTCDetailsByCode(transportingContractors.getTransportercode());
		
		List<TransportingContractorSuretyBean> transportingContractorSuretyBeans =prepareListofBeanForTCSuretyDetails(tcSuretyDetails);
		
		
		org.json.JSONObject harvesterJson=new org.json.JSONObject(transportingContractorBean);
		org.json.JSONArray hcDetailsArray = new org.json.JSONArray(transportingContractorSuretyBeans);
		
		response.put("formData",harvesterJson);
		response.put("gridData",hcDetailsArray);			
		
		return response.toString();
	}
	
	
	private TransportingContractorBean prepareBeanForTransportingContractors(TransportingContractors transportingContractor) 
	{
		
		TransportingContractorBean bean = new TransportingContractorBean();			
	
		bean.setId(transportingContractor.getId());
		bean.setStatus(transportingContractor.getStatus());
		bean.setAadhaarNumber(transportingContractor.getAadhaarnumber());
		bean.setAccountNumber(transportingContractor.getAccountnumber());
		bean.setAddress(transportingContractor.getAddress());
		bean.setBankCode(transportingContractor.getBankcode());
		bean.setBranchCode(transportingContractor.getBranchcode());
		bean.setContactNumber(transportingContractor.getContactnumber());
		bean.setPanNumber(transportingContractor.getPanno());
		bean.setTransporter(transportingContractor.getTransporter());
		bean.setTransporterCode(transportingContractor.getTransportercode());
		bean.setIfscCode(transportingContractor.getIfsccode());
		bean.setModifyFlag("Yes");		
		bean.setAccountCode(transportingContractor.getAccountcode());		
		return bean;
	}
	
	
	private List<TransportingContractorSuretyBean> prepareListofBeanForTCSuretyDetails(List<TCSuretyDetails> tcSuretyDetails) 
	{
		List<TransportingContractorSuretyBean> beans = null;
		if (tcSuretyDetails != null && !tcSuretyDetails.isEmpty())
		{
			beans = new ArrayList<TransportingContractorSuretyBean>();
			TransportingContractorSuretyBean bean = null;
			for (TCSuretyDetails tcSuretyDetail : tcSuretyDetails)
			{
				bean = new TransportingContractorSuretyBean();

				bean.setId(tcSuretyDetail.getId());
				bean.setAadhaarNumber(tcSuretyDetail.getAadhaarnumber());
				bean.setContactNo(tcSuretyDetail.getContactno());
				bean.setTransporterCode(tcSuretyDetail.getTransportercode());
				bean.setPanNo(tcSuretyDetail.getPanno());
				bean.setSurety(tcSuretyDetail.getSurety());				
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//------------------------------End Of  Transporting Contractor ------------------------//

	
//------------------------------ Transporting Contractor --------------------------------//

    @RequestMapping(value = "/saveTieUpLoanInterest", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveTieUpLoanInterest(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception
	{	
    	boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{				
			
			JSONObject transportContractorData=(JSONObject) jsonData.get("formData");			
			JSONArray suretyData=(JSONArray) jsonData.get("gridData");			

			TieUpLoanInterestBean tieUpLoanInterestBean = new ObjectMapper().readValue(transportContractorData.toString(), TieUpLoanInterestBean.class);
			
			BankLoanRepayDt bankLoanRepayDt = prepareModelForTransportingContractors(tieUpLoanInterestBean);
			entityList.add(bankLoanRepayDt);
			tieUpLoanInterestService.deleteBankLoanIntRates(bankLoanRepayDt);
			
			for(int i=0;i<suretyData.size();i++)
			{
				TieUpLoanInterestDetailsBean tieUpLoanInterestDetailsBean = new ObjectMapper().readValue(suretyData.get(i).toString(), TieUpLoanInterestDetailsBean.class);
				BankLoanIntRates bankLoanIntRates = prepareModelForInterestDetails(tieUpLoanInterestDetailsBean,bankLoanRepayDt);	
				entityList.add(bankLoanIntRates);
				//tieUpLoanInterestService.addTieUpLoanInterestDetails(bankLoanIntRates);
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			//tieUpLoanInterestService.addTieUpLoanInterest(bankLoanRepayDt);
			
			return isInsertSuccess;
		}
		catch (Exception e)
        {
            logger.info(e.getCause(), e);
            return isInsertSuccess;
        }
		//return null;
	}		
	
		
	private BankLoanRepayDt prepareModelForTransportingContractors(TieUpLoanInterestBean tieUpLoanInterestBean) {

		BankLoanRepayDt bankLoanRepayDt = new BankLoanRepayDt();
		bankLoanRepayDt.setId(tieUpLoanInterestBean.getId());
		bankLoanRepayDt.setBranchcode(tieUpLoanInterestBean.getBranchCode());
		bankLoanRepayDt.setPayableDate(DateUtils.getSqlDateFromString(tieUpLoanInterestBean.getPayableDate(),Constants.GenericDateFormat.DATE_FORMAT));
		bankLoanRepayDt.setSeason(tieUpLoanInterestBean.getSeason());
		
		return bankLoanRepayDt;
	}
	private BankLoanIntRates prepareModelForInterestDetails(TieUpLoanInterestDetailsBean tieUpLoanInterestDetailsBean,BankLoanRepayDt bankLoanRepayDt) {

		BankLoanIntRates bankLoanIntRates=new BankLoanIntRates();
		
		bankLoanIntRates.setId(tieUpLoanInterestDetailsBean.getId());
		bankLoanIntRates.setFromamount(tieUpLoanInterestDetailsBean.getFromAmount());
		bankLoanIntRates.setSeason(bankLoanRepayDt.getSeason());
		bankLoanIntRates.setToamount(tieUpLoanInterestDetailsBean.getToAmount());
		bankLoanIntRates.setBranchcode(bankLoanRepayDt.getBranchcode());
		bankLoanIntRates.setInterestrate(tieUpLoanInterestDetailsBean.getInterestRate());
		
		return bankLoanIntRates;
	}
	
	
	@RequestMapping(value = "/getAllTieUpInterests", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllTieUpInterests(@RequestBody JSONObject jsonData) throws Exception
	{
		TieUpLoanInterestBean tieUpLoanInterestBean =null;		
		org.json.JSONObject response = new org.json.JSONObject();
		String season=(String)jsonData.get("season");
		Integer branchCode=(Integer)jsonData.get("branchcode");
		
		BankLoanRepayDt bankLoanRepayDts=tieUpLoanInterestService.getBankLoanRepayDtls(season,branchCode);
		
		if(bankLoanRepayDts!=null)
			tieUpLoanInterestBean =prepareBeanForTieUpInterests(bankLoanRepayDts);
			
		
		List<BankLoanIntRates> bankLoanIntRates =tieUpLoanInterestService.getBankLoanIntRatesByCode(bankLoanRepayDts.getBranchcode());
		
		List<TieUpLoanInterestDetailsBean> tieUpLoanInterestDetailsBeans =prepareListofBeanForBankLoanIntRates(bankLoanIntRates);
		
		
		org.json.JSONObject tieUpLoanInteresJson=new org.json.JSONObject(tieUpLoanInterestBean);
		org.json.JSONArray tieUpLoanInteresDtldArray = new org.json.JSONArray(tieUpLoanInterestDetailsBeans);
		
		response.put("formData",tieUpLoanInteresJson);
		response.put("gridData",tieUpLoanInteresDtldArray);			
		
		return response.toString();
	}
	
	
	private TieUpLoanInterestBean prepareBeanForTieUpInterests(BankLoanRepayDt bankLoanRepayDts) 
	{
		
		TieUpLoanInterestBean bean = new TieUpLoanInterestBean();			
		
		bean.setId(bankLoanRepayDts.getId());
		bean.setBranchCode(bankLoanRepayDts.getBranchcode());
		
		bean.setPayableDate(DateUtils.formatDate(bankLoanRepayDts.getPayableDate(),Constants.GenericDateFormat.DATE_FORMAT));
		bean.setSeason(bankLoanRepayDts.getSeason());
		
				
		return bean;
	}
	
	
	private List<TieUpLoanInterestDetailsBean> prepareListofBeanForBankLoanIntRates(List<BankLoanIntRates> bankLoanIntRates) 
	{
		List<TieUpLoanInterestDetailsBean> beans = null;
		if (bankLoanIntRates != null && !bankLoanIntRates.isEmpty())
		{
			beans = new ArrayList<TieUpLoanInterestDetailsBean>();
			TieUpLoanInterestDetailsBean bean = null;
			for (BankLoanIntRates bankLoanIntRate : bankLoanIntRates)
			{
				bean = new TieUpLoanInterestDetailsBean();

				bean.setId(bankLoanIntRate.getId());
				bean.setBranchCode(bankLoanIntRate.getBranchcode());
				bean.setFromAmount(bankLoanIntRate.getFromamount());
				bean.setToAmount(bankLoanIntRate.getToamount());
				bean.setSeason(bankLoanIntRate.getSeason());
				bean.setInterestRate(bankLoanIntRate.getInterestrate());
			
				beans.add(bean);
			}
		}
		return beans;
	}
	
	//------------------------------End Of  Transporting Contractor ------------------------//
	//---------------------------------------- Cane Accounting Rules -------------------------------------//
		@RequestMapping(value = "/saveCaneAccountingRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		boolean saveCaneAccountingRules(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			try 
			{
				CaneAccountingRulesBean caneAccountingRulesBean = new ObjectMapper().readValue(jsonData, CaneAccountingRulesBean.class);
				CaneAccountingRules caneAccountingRules = prepareModelForCaneAccRules(caneAccountingRulesBean);
				entityList.add(caneAccountingRules);
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
				//caneAccountingRulesService.addCaneAccountingRules(caneAccountingRules);
				return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}
		
		private CaneAccountingRules prepareModelForCaneAccRules(CaneAccountingRulesBean caneAccountingRulesBean)
		{
			CaneAccountingRules caneAccountingRules = new CaneAccountingRules();
			
			caneAccountingRules.setId(caneAccountingRulesBean.getId());
			caneAccountingRules.setSeason(caneAccountingRulesBean.getSeason());		
			caneAccountingRules.setDate(DateUtils.getSqlDateFromString(caneAccountingRulesBean.getDate(),Constants.GenericDateFormat.DATE_FORMAT));
			caneAccountingRules.setFrp(caneAccountingRulesBean.getFrp());
			caneAccountingRules.setFrpsubsidyamt(caneAccountingRulesBean.getFrpSubsidyAmt());
			caneAccountingRules.setAdditionalcaneprice(caneAccountingRulesBean.getAdditionalCanePrice());
			caneAccountingRules.setIcp(caneAccountingRulesBean.getIcp());
			caneAccountingRules.setNetcaneprice(caneAccountingRulesBean.getNetCanePrice());
			caneAccountingRules.setPostseasoncaneprice(caneAccountingRulesBean.getPostSeasonCanePrice());
			caneAccountingRules.setSeasoncaneprice(caneAccountingRulesBean.getSeasonCanePrice());
			caneAccountingRules.setIspercentoramt(caneAccountingRulesBean.getIsPercentOrAmt());
			caneAccountingRules.setAmtforded(caneAccountingRulesBean.getAmtForDed());
			caneAccountingRules.setAmtforloans(caneAccountingRulesBean.getAmtForLoans());
			caneAccountingRules.setPercentforloans(caneAccountingRulesBean.getPercentForLoans());
			caneAccountingRules.setPercentforded(caneAccountingRulesBean.getPercentForDed());
			caneAccountingRules.setLoanRecovery(caneAccountingRulesBean.getLoanRecovery());
			caneAccountingRules.setIstaapplicable(caneAccountingRulesBean.getIsTAApplicable());
			caneAccountingRules.setNoofkms(caneAccountingRulesBean.getNoOfKms());
			caneAccountingRules.setAllowanceperkmperton(caneAccountingRulesBean.getAllowancePerKmPerTon());
			
			return caneAccountingRules;
		}
		
		
		
		
		
		
		/*@RequestMapping(value = "/getCaneAccountingRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getCaneAccountingRules(@RequestBody JSONObject jsonData) throws Exception
		{
			CaneAccountingRulesBean caneAccountingRulesBean =null;
			org.json.JSONObject jsonResponse = new org.json.JSONObject();
			
			String season=(String)jsonData.get("season");
			String Effictivedate=(String)jsonData.get("date");
			Date efDate=DateUtils.getSqlDateFromString(Effictivedate,Constants.GenericDateFormat.DATE_FORMAT);
			
			CaneAccountingRules caneAccountingRules = caneAccountingRulesService.getCaneAccRulesDtls(season,efDate);
			if(caneAccountingRules!=null)
				caneAccountingRulesBean =prepareBeanForCaneAccountingRules(caneAccountingRules);
			
			org.json.JSONObject caneAccountingRulesBeanJson=new org.json.JSONObject(caneAccountingRulesBean);
			
			jsonResponse.put("caneAccountingRules",caneAccountingRulesBeanJson);
			
			return jsonResponse.toString();
		}
		
		
		
		private CaneAccountingRulesBean prepareBeanForCaneAccountingRules(CaneAccountingRules caneAccountingRules) 
		{
			
			CaneAccountingRulesBean bean = new CaneAccountingRulesBean();
			
			bean.setId(caneAccountingRules.getId());
			bean.setSeason(caneAccountingRules.getSeason());
			bean.setDate(DateUtils.formatDate(caneAccountingRules.getDate(),Constants.GenericDateFormat.DATE_FORMAT));
			bean.setFrp(caneAccountingRules.getFrp());
			bean.setFrpSubsidyAmt(caneAccountingRules.getFrpsubsidyamt());
			bean.setAdditionalCanePrice(caneAccountingRules.getAdditionalcaneprice());
			bean.setIcp(caneAccountingRules.getIcp());
			bean.setNetCanePrice(caneAccountingRules.getNetcaneprice());
			bean.setPostSeasonCanePrice(caneAccountingRules.getPostseasoncaneprice());
			bean.setSeasonCanePrice(caneAccountingRules.getSeasoncaneprice());			
			bean.setIsPercentOrAmt(caneAccountingRules.getIspercentoramt());
			
			if(caneAccountingRules.getIspercentoramt()==0)
			{				
				bean.setPercentForLoans(caneAccountingRules.getPercentforloans());
				bean.setPercentForDed(caneAccountingRules.getPercentforded());			
			}
			else
			{
				bean.setAmtForDed(caneAccountingRules.getAmtforded());
				bean.setAmtForLoans(caneAccountingRules.getAmtforloans());
			}
			
			bean.setLoanRecovery(caneAccountingRules.getLoanRecovery());
			bean.setIsTAApplicable(caneAccountingRules.getIstaapplicable());
			bean.setNoOfKms(caneAccountingRules.getNoofkms());
			bean.setAllowancePerKmPerTon(caneAccountingRules.getAllowanceperkmperton());
			
			return bean;
		}
		*/
		@RequestMapping(value = "/saveCaneAccountingRulesFroSeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		boolean saveCaneAccountingRulesFroSeason(@RequestBody JSONObject jsonData) throws Exception 
		{		
			JSONObject formData=(JSONObject) jsonData.get("formData");
			JSONArray gridData=(JSONArray) jsonData.get("gridData");
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			
			try 
			{
				int calcid = 0;//commonService.getMaxIDforColumn("CaneAcctRules","calcid");
				CaneAccountingRulesSeasonBean caneAccountingRulesSeasonBean = new ObjectMapper().readValue(formData.toString(), CaneAccountingRulesSeasonBean.class);
				
				String modifyFlag = caneAccountingRulesSeasonBean.getModifyFlag();
				if("No".equalsIgnoreCase(modifyFlag))
				{
					calcid = commonService.getMaxIDforColumn("CaneAcctRules","calcid");
				}
				else
				{
					CaneAcctAmountsSeasonBean caneAcctAmountsSeasonBean = new ObjectMapper().readValue(gridData.get(0).toString(), CaneAcctAmountsSeasonBean.class);
					calcid = caneAcctAmountsSeasonBean.getCalcid();
					caneAccountingFunctionalService.deleteCaneAcctAmounts(calcid,caneAccountingRulesSeasonBean.getSeason());
				}
				
				CaneAcctRules caneAcctRules = prepareModelForCaneAcctRules(caneAccountingRulesSeasonBean,calcid);
				entityList.add(caneAcctRules);
				
				for(int i=0;i<gridData.size();i++)
				{
					CaneAcctAmountsSeasonBean caneAcctAmountsSeasonBean = new ObjectMapper().readValue(gridData.get(i).toString(), CaneAcctAmountsSeasonBean.class);
					CaneAcctAmounts caneAcctAmounts = prepareModelForCaneAcctAmounts(caneAcctAmountsSeasonBean,caneAccountingRulesSeasonBean,calcid);
					entityList.add(caneAcctAmounts);
				}
				
				isInsertSuccess = commonService.saveMultipleEntities(entityList);
				return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}
		
		private CaneAcctRules prepareModelForCaneAcctRules(CaneAccountingRulesSeasonBean caneAccountingRulesSeasonBean,Integer calcid)
		{
			CaneAcctRules caneAcctRules = new CaneAcctRules();
			
			
			caneAcctRules.setCalcid(calcid);
			
			//caneAcctRules.setCalcid(caneAccountingRulesSeasonBean.getCalcid());
			
			caneAcctRules.setSeason(caneAccountingRulesSeasonBean.getSeason());		
			caneAcctRules.setCalcruledate(DateUtils.getSqlDateFromString(caneAccountingRulesSeasonBean.getCaneaccruleDate(),Constants.GenericDateFormat.DATE_FORMAT));
			caneAcctRules.setPostseasoncaneprice(caneAccountingRulesSeasonBean.getPostSeasonCanePrice());
			caneAcctRules.setSeasoncaneprice(caneAccountingRulesSeasonBean.getSeasonCanePrice());
			caneAcctRules.setIspercentoramt(caneAccountingRulesSeasonBean.getIsPercentOrAmt());
			caneAcctRules.setAmtforded(caneAccountingRulesSeasonBean.getAmtForDed());
			caneAcctRules.setAmtforloans(caneAccountingRulesSeasonBean.getAmtForLoans());
			caneAcctRules.setPercentForloans(caneAccountingRulesSeasonBean.getPercentForLoans());
			caneAcctRules.setPercentforded(caneAccountingRulesSeasonBean.getPercentForDed());
			caneAcctRules.setIstaapplicable(caneAccountingRulesSeasonBean.getIsTAApplicable());
			caneAcctRules.setNoofkms(caneAccountingRulesSeasonBean.getNoOfKms());
			caneAcctRules.setAllowanceperkmperton(caneAccountingRulesSeasonBean.getAllowancePerKmPerTon());
			caneAcctRules.setNetcaneprice(caneAccountingRulesSeasonBean.getNetCanePrice());
			caneAcctRules.setPercentforded(caneAccountingRulesSeasonBean.getPercentForDed());
			return caneAcctRules;
		}
		
		private CaneAcctAmounts prepareModelForCaneAcctAmounts(CaneAcctAmountsSeasonBean caneAcctAmountsSeasonBean,CaneAccountingRulesSeasonBean caneAccountingRulesSeasonBean,Integer calcid)
		{
			CaneAcctAmounts caneAcctAmounts = new CaneAcctAmounts();
			caneAcctAmounts.setSeason(caneAccountingRulesSeasonBean.getSeason());
			caneAcctAmounts.setCalcid(calcid);
			caneAcctAmounts.setPricename(caneAcctAmountsSeasonBean.getPriceName());
			caneAcctAmounts.setSubsidy(caneAcctAmountsSeasonBean.getSubsidy());
			caneAcctAmounts.setNetamt(caneAcctAmountsSeasonBean.getNetAmt());
			
			String ispurtax = caneAcctAmountsSeasonBean.getIsThisPurchaseTax();
			int nispurtax = Integer.parseInt(ispurtax);
			
			caneAcctAmounts.setIsthispurchasetax((byte) nispurtax);
			String seasonacts=caneAcctAmountsSeasonBean.getConsiderForSeasonActs();
			int containseasonacts=Integer.parseInt(seasonacts);
			caneAcctAmounts.setConsiderforseasonacts((byte)containseasonacts);		
			caneAcctAmounts.setAmount(caneAcctAmountsSeasonBean.getAmount());
			return caneAcctAmounts;
		}
		
		
		
		@RequestMapping(value = "/getCaneAccountingRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getCaneAccountingRules(@RequestBody JSONObject jsonData) throws Exception
		{
			CaneAccountingRulesSeasonBean cneAccountingRulesSeasonBean =null;
			
			org.json.JSONObject jsonResponse = new org.json.JSONObject();
			
			String season=(String)jsonData.getString("season");
			String Effictivedate=(String)jsonData.getString("date");
			Date efDate=DateUtils.getSqlDateFromString(Effictivedate,Constants.GenericDateFormat.DATE_FORMAT);
			
			CaneAcctRules caneAcctRules=caneAccountingRulesService.getCaneAccRulesDtls(season,efDate);
			
			if(caneAcctRules!=null)
				cneAccountingRulesSeasonBean =prepareBeanForCaneAcctRules(caneAcctRules);
			
			List caneAccAmountsList =caneAccountingRulesService.getCaneAccAmounts(caneAcctRules.getCalcid());
			
			List<CaneAcctAmountsSeasonBean> caneAcctAmountsSeasonBean =prepareListofBeanForCaneAccAmounts(caneAccAmountsList);
			
			org.json.JSONObject rulesJson=new org.json.JSONObject(cneAccountingRulesSeasonBean);
			org.json.JSONArray accArray = new org.json.JSONArray(caneAcctAmountsSeasonBean);
			
			jsonResponse.put("rules",rulesJson);
			jsonResponse.put("accounts",accArray);		
			
			logger.info("------rules-----"+jsonResponse.toString());
			
			return jsonResponse.toString();
		}

		
		
		private CaneAccountingRulesSeasonBean prepareBeanForCaneAcctRules(CaneAcctRules caneAcctRules) 
		{			
			
			
			CaneAccountingRulesSeasonBean bean = new CaneAccountingRulesSeasonBean();
			bean.setModifyFlag("Yes");
			bean.setCalcid(caneAcctRules.getCalcid());
			bean.setSeasonCanePrice(caneAcctRules.getSeasoncaneprice());
			bean.setSeason(caneAcctRules.getSeason());
			bean.setPostSeasonCanePrice(caneAcctRules.getPostseasoncaneprice());
			//bean.setAmtForLoans(caneAcctRules.getAmtforloans());
			bean.setNetCanePrice(caneAcctRules.getNetcaneprice());
			
			bean.setIsPercentOrAmt(caneAcctRules.getIspercentoramt());
			
			
			Date curculdate = caneAcctRules.getCalcruledate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strcurculdate = df.format(curculdate);
			bean.setCaneaccruleDate(strcurculdate);
			
			
			if(caneAcctRules.getIspercentoramt()==0)
			{				
				bean.setPercentForLoans(caneAcctRules.getPercentForloans());
				bean.setPercentForDed(caneAcctRules.getPercentforded());			
			}
			else
			{
				bean.setAmtForDed(caneAcctRules.getAmtforded());
				bean.setAmtForLoans(caneAcctRules.getAmtforloans());
			}
			bean.setIsTAApplicable(caneAcctRules.getIstaapplicable());
			if(caneAcctRules.getIstaapplicable()==0)
			{
				bean.setNoOfKms(caneAcctRules.getNoofkms());;
				bean.setAllowancePerKmPerTon(caneAcctRules.getAllowanceperkmperton());
			}

			return bean;
		}
		
		
		private List<CaneAcctAmountsSeasonBean> prepareListofBeanForCaneAccAmounts(List<CaneAcctAmounts> caneAccAmountsList) 
		{
			List<CaneAcctAmountsSeasonBean> beans = null;
			if (caneAccAmountsList != null && !caneAccAmountsList.isEmpty()) 
			{
				beans = new ArrayList<CaneAcctAmountsSeasonBean>();
				CaneAcctAmountsSeasonBean bean = null;
				for (CaneAcctAmounts caneAccAmount : caneAccAmountsList)
				{
					bean = new CaneAcctAmountsSeasonBean();
					bean.setPriceName(caneAccAmount.getPricename());
					bean.setSubsidy(caneAccAmount.getSubsidy());
					bean.setNetAmt(caneAccAmount.getNetamt());
					
					byte ispurtax = caneAccAmount.getIsthispurchasetax();
					String strispurtax = Integer.toString(ispurtax);
					
					bean.setIsThisPurchaseTax(strispurtax);
					byte seasoncontacts=caneAccAmount.getConsiderforseasonacts();
					String sscontacts=String.valueOf(seasoncontacts);
					bean.setConsiderForSeasonActs(sscontacts);
					//bean.setCalcid(caneAccAmount.getCalcid());
					bean.setCalcid(caneAccAmount.getCalcid());
					bean.setAmount(caneAccAmount.getAmount());
					beans.add(bean);
				}
			}
			return beans;
		}
		///
		//-----------------------------------------------New Cane Accounting Rules ----------------------------------------------------//		
		
		@RequestMapping(value = "/getCaneAccountingRulesForCalculation", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		List<CaneAccountingRulesForPCACBean> getCaneAccountingRulesForCalculation(@RequestBody JSONObject jsonData) throws Exception
		{
			List<CaneAccountingRulesForPCACBean> caneAccountingRulesForPCACBean =null;
			org.json.JSONObject jsonResponse = new org.json.JSONObject();
			
			String season=(String)jsonData.get("season");
			String datefrom=(String)jsonData.get("datefrom");
			String dateto=(String)jsonData.get("dateto");
			Date date1=DateUtils.getSqlDateFromString(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			Date date2=DateUtils.getSqlDateFromString(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			
			List<CaneAccountingRules> caneAccountingRules = caneAccountingRulesService.getCaneAccRulesForCalc(season,date1,date2);
			if(caneAccountingRules!=null)
				caneAccountingRulesForPCACBean =prepareBeanForCaneAccountingRulesCalc(caneAccountingRules);
			
			org.json.JSONObject caneAccountingRulesBeanJson=new org.json.JSONObject(caneAccountingRulesForPCACBean);
			
			jsonResponse.put("caneAccountingRules",caneAccountingRulesBeanJson);
			
			return caneAccountingRulesForPCACBean;
		}
		
		private List<CaneAccountingRulesForPCACBean> prepareBeanForCaneAccountingRulesCalc(List<CaneAccountingRules> caneAccountingRules) 
		{
			
			List<CaneAccountingRulesForPCACBean> beans = null;
			if (caneAccountingRules != null && !caneAccountingRules.isEmpty())
			{
				beans = new ArrayList<CaneAccountingRulesForPCACBean>();				
				CaneAccountingRulesForPCACBean bean = null;
				for (CaneAccountingRules caneAccountingRule : caneAccountingRules)
				{		
					bean = new CaneAccountingRulesForPCACBean();			
					bean.setId(caneAccountingRule.getId());
					bean.setSeason(caneAccountingRule.getSeason());
					bean.setSeasonCanePrice(caneAccountingRule.getSeasoncaneprice());			
					bean.setIsPercentOrAmt(caneAccountingRule.getIspercentoramt());
					if(caneAccountingRule.getIspercentoramt()==0)
					{				
						bean.setPercentForDed(caneAccountingRule.getPercentforded());			
					}
					else
					{
						bean.setAmtForDed(caneAccountingRule.getAmtforded());
					}					
				
					beans.add(bean);
				}
			}
			return beans;
		}
	
/*		private List<CaneAccountingRulesBean> prepareBeanForCaneAccountingRulesCalc(List<CaneAccountingRules> caneAccountingRules) 
		{
			
			List<CaneAccountingRulesBean> beans = null;
			if (caneAccountingRules != null && !caneAccountingRules.isEmpty())
			{
				beans = new ArrayList<CaneAccountingRulesBean>();				
				CaneAccountingRulesBean bean = null;
				for (CaneAccountingRules caneAccountingRule : caneAccountingRules)
				{		
					bean = new CaneAccountingRulesBean();			
					bean.setId(caneAccountingRule.getId());
					bean.setSeason(caneAccountingRule.getSeason());
					//bean.setDate(DateUtils.formatDate(caneAccountingRule.getDate(),Constants.GenericDateFormat.DATE_FORMAT));
					//bean.setFrp(caneAccountingRule.getFrp());
					//bean.setFrpSubsidyAmt(caneAccountingRule.getFrpsubsidyamt());
					//bean.setAdditionalCanePrice(caneAccountingRule.getAdditionalcaneprice());
					//bean.setIcp(caneAccountingRule.getIcp());
					//bean.setNetCanePrice(caneAccountingRule.getNetcaneprice());
					//bean.setPostSeasonCanePrice(caneAccountingRule.getPostseasoncaneprice());
					bean.setSeasonCanePrice(caneAccountingRule.getSeasoncaneprice());			
					bean.setIsPercentOrAmt(caneAccountingRule.getIspercentoramt());
					
					if(caneAccountingRule.getIspercentoramt()==0)
					{				
						bean.setPercentForLoans(caneAccountingRule.getPercentforloans());
						bean.setPercentForDed(caneAccountingRule.getPercentforded());			
					}
					else
					{
						bean.setAmtForDed(caneAccountingRule.getAmtforded());
						bean.setAmtForLoans(caneAccountingRule.getAmtforloans());
					}
					
					//bean.setLoanRecovery(caneAccountingRule.getLoanRecovery());
					//bean.setIsTAApplicable(caneAccountingRule.getIstaapplicable());
					//bean.setNoOfKms(caneAccountingRule.getNoofkms());
					//bean.setAllowancePerKmPerTon(caneAccountingRule.getAllowanceperkmperton());
					beans.add(bean);
				}
			}
			return beans;
		}*/
}
