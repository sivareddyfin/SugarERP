package com.finsol.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finsol.bean.DcBean;
import com.finsol.bean.DcSummaryBean;
import com.finsol.bean.GrnDetailsBean;
import com.finsol.bean.GrnSummaryBean;
import com.finsol.bean.GroupMasterBean;
import com.finsol.bean.OpeningStockBean;
import com.finsol.bean.ProductMasterBean;
import com.finsol.bean.ProductSubGroupBean;
import com.finsol.bean.StockInDetailsBean;
import com.finsol.bean.StockInSummaryBean;
import com.finsol.bean.StockIssueDtlsBean;
import com.finsol.bean.StockIssueSmryBean;
import com.finsol.bean.StockReqDtlsBean;
import com.finsol.bean.StockReqSmryBean;
import com.finsol.bean.UomBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.DepartmentMaster;
import com.finsol.model.Employees;
import com.finsol.model.GrnDetails;
import com.finsol.model.GrnSummary;
import com.finsol.model.ProductGroup;
import com.finsol.model.ProductMaster;
import com.finsol.model.ProductSubGroup;
import com.finsol.model.SDCDetails;
import com.finsol.model.SDCSummary;
import com.finsol.model.StockDetails;
import com.finsol.model.StockInDetails;
import com.finsol.model.StockInSummary;
import com.finsol.model.StockIssueDtls;
import com.finsol.model.StockIssueSmry;
import com.finsol.model.StockReqDtls;
import com.finsol.model.StockReqSmry;
import com.finsol.model.StockSummary;
import com.finsol.model.StoreBatchMaster;
import com.finsol.model.Uom;
import com.finsol.service.StockService;
import com.finsol.service.StoreService;
import com.finsol.utils.Constants;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
/**
 * @author Umanath Ch
 * 
 */
@Controller

public class StoreController 
{
	

	private static final Logger logger = Logger.getLogger(AgricultureHarvestingController.class);

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HibernateDao hibernatedao;


	@Autowired
	private StoreService storeService;
	
	//Added by DMurty on 06-03-2017
	@Autowired
	private StockService stockService;
	
	
	//////////////////////////////////////////////////////////////////////////
	
	

	@RequestMapping(value = "/SaveUom", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveUom(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		try
		{
			UomBean uomBean = new ObjectMapper().readValue(jsonData, UomBean.class);
			Uom uom = storeService.prepareModelforUom(uomBean);
			entityList.add(uom);
			isInsertSuccess = storeService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	@RequestMapping(value = "/getMAXUOM", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getMAXUOM() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer maxuom = storeService.getMaxIDforColumn("Uom","id");
		logger.info("getMaxID() ========maxuom==========" + maxuom);
		jsonObj.put("MAXID", maxuom);
		jArray.add(jsonObj);
		return jArray;
	}
	@RequestMapping(value = "/getMAXProductGroup", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getMAXProductGroup() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer maxgroup = storeService.getMaxIDforColumn("ProductGroup","groupSeqNo");
		logger.info("getMAXProductGroup() ========maxuom==========" + maxgroup);
		String value = Integer.toString(maxgroup);
		if(value.length() == 1)
		{
			value = "0"+value;
		}
		jsonObj.put("MAXID", value);
		jArray.add(jsonObj);
		return jArray;
	}
	@RequestMapping(value = "/getAllUOM", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<UomBean> getAllUOM(@RequestBody String jsonData) throws Exception
	{
		List<UomBean> uomBean = storeService.prepareListofBeanForUom(storeService.getAllUOM());
		return uomBean;
	}
	
	@RequestMapping(value = "/SaveProductGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveProductGroup(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		try
		{
			GroupMasterBean groupMasterBean = new ObjectMapper().readValue(jsonData, GroupMasterBean.class);
			ProductGroup productGroup = storeService.prepareModelforProductGroup(groupMasterBean);
			entityList.add(productGroup);
			isInsertSuccess = storeService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	@RequestMapping(value = "/getAllProductGroups", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<GroupMasterBean> getAllProductGroups(@RequestBody String jsonData) throws Exception
	{
		List<GroupMasterBean> groupMasterBean = storeService.prepareListofBeanForProductGroup(storeService.getAllProductGroups());
		return groupMasterBean;
	}
	
	@RequestMapping(value = "/getMaxSubGroupsbyGroupid", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getMaxSubGroupsbyGroupid(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer SubGroupid = storeService.getMaxIDfortable("ProductSubGroup","subGroupSeq",jsonData,"productGroupCode");
		logger.info("getMaxSubGroupsbyGroupid() ========maxuom==========" + SubGroupid);
		String groupname = storeService.getGroupName(jsonData);
		logger.info("getGroupName() ========groupname==========" + groupname);

		String value = Integer.toString(SubGroupid);
		if(value.length() == 1)
		{
			value = "0"+value;
		}
		if(groupname.length() > 1)
		{
		jsonObj.put("SubGroupId", jsonData+""+value);
		jsonObj.put("GroupName", groupname);
		}

		jArray.add(jsonObj);
		return jArray;
	}
	
	@RequestMapping(value = "/SaveProductSubGroup", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveProductSubGroup(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		try
		{
			ProductSubGroupBean subgroupMasterBean = new ObjectMapper().readValue(jsonData, ProductSubGroupBean.class);
			ProductSubGroup productSubGroup = storeService.prepareModelforSubProductGroup(subgroupMasterBean);
			entityList.add(productSubGroup);
			isInsertSuccess = storeService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	@RequestMapping(value = "/getAllProductSubGroups", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<ProductSubGroupBean> getAllProductSubGroups(@RequestBody String jsonData) throws Exception
	{
		List<ProductSubGroupBean> productSubGroupBean = storeService.prepareListofBeanForProductSubGroup(storeService.getAllProductSubGroups());
		return productSubGroupBean;
	}
	
	@RequestMapping(value = "/getMaxProductbyid", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getMaxProductsid(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer SubGroupid = storeService.getMaxIDfortable("ProductMaster","productSeqNo",jsonData,"subCategoryCode");
		logger.info("getAllSubGroupsbyGroupid() ========maxuom==========" + SubGroupid);

		String value = Integer.toString(SubGroupid);
		if(value.length() == 1)
		{
			value = "000"+value;
		}
		else if(value.length() == 2)
		{
			value = "00"+value;
		}
		else if(value.length() == 3)
		{
			value = "0"+value;
		}
		jsonObj.put("productCode", jsonData+value);

		jArray.add(jsonObj);
		return jArray;
	}
	
	@RequestMapping(value = "/SaveProducts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveProducts(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		try
		{
			ProductMasterBean productMasterBean = new ObjectMapper().readValue(jsonData, ProductMasterBean.class);
			ProductMaster productMaster = storeService.prepareModelforProductMaster(productMasterBean);
			entityList.add(productMaster);
			isInsertSuccess = storeService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	@RequestMapping(value = "/getAllProducts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getAllProducts(@RequestBody String jsonData) throws Exception
	{
	
	
	JSONArray jArray = new JSONArray();
	JSONObject jsonObj = null;
	jsonObj = new JSONObject();
	List Productlist = storeService.getAllProducts(jsonData);
	logger.info("getAllProducts() ========Productlist==========" + Productlist);
	if (Productlist != null && Productlist.size() > 0)
	{
		for (int j = 0; j < Productlist.size(); j++)
		{
			Map batchMap = new HashMap();
			batchMap = (Map) Productlist.get(j);
			
			String productCode = (String) batchMap.get("productCode");
			String productName = (String) batchMap.get("productName");
			Integer uom = (Integer) batchMap.get("uom");
			Byte status=(Byte) batchMap.get("status");
			Integer stockMethod=(Integer) batchMap.get("stockMethodCode");

			jsonObj.put("productCode", productCode);
			jsonObj.put("productName", productName);
			
			String uomname="";
			List uomList = storeService.getUomNameById(uom);
			for (int k = 0; k < uomList.size(); k++)
			{
				Map batchMap1 = new HashMap();
				batchMap1 = (Map) uomList.get(k);
				uomname=(String) batchMap1.get("uom");
			}
			List stockMethodList = storeService.getStockMethodListById(stockMethod);
			String stockMethodname="";
			for (int p = 0; p < stockMethodList.size(); p++)
			{
				Map batchMap2 = new HashMap();
				batchMap2 = (Map) stockMethodList.get(p);
				stockMethodname=(String) batchMap2.get("stockMethod");

			}

			jsonObj.put("uom", uomname);
			jsonObj.put("stockMethod", stockMethodname);
			String statusname="";
				if(status==0)
				{
					statusname="Active";
				}
				else
				{
					statusname="InActive";
				}
			jsonObj.put("status", statusname);

			jArray.add(jsonObj);
		}
	}
	logger.info("getAllProducts() ========jArray==========" + jArray);
	
		return jArray;
	}
	
	@RequestMapping(value = "/getProductsDetailsByCode", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<ProductMasterBean> getProductsDetailsByCode(@RequestBody String jsonData) throws Exception
	{
		List<ProductMasterBean> productMasterBean = storeService.prepareListofBeanForProductMaster(storeService.getProductsNameByCode(jsonData));
		return productMasterBean;
	}
	
	@RequestMapping(value = "/getSubGroupsByGrp", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getSubGroupsByGrp(@RequestBody String group) throws Exception
	{
		//String group = (String) jsonData.get("groupcode");
	
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List SubGrpList = storeService.getSubGroupsListByGrp(group);
		logger.info("getSubGroupsListByGrp() ========SubGrpList==========" + SubGrpList);
		if (SubGrpList != null && SubGrpList.size() > 0)
		{
			for (int j = 0; j < SubGrpList.size(); j++)
			{
				Map batchMap = new HashMap();
				batchMap = (Map) SubGrpList.get(j);
				
				String subGroup = (String) batchMap.get("subGroupName");
				String subgroupcode = (String) batchMap.get("productSubGroupCode");
				
				jsonObj.put("subGroupCode", subgroupcode);
				jsonObj.put("subGroup", subGroup);
				jArray.add(jsonObj);
			}
		}
		logger.info("getSubGroupsListByGrp() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getGroups", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getGroups() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List GrpList = storeService.getGroupsList();
		logger.info("getGroups() ========GrpList==========" + GrpList);
		if (GrpList != null && GrpList.size() > 0)
		{
			for (int j = 0; j < GrpList.size(); j++)
			{
				Map batchMap = new HashMap();
				batchMap = (Map) GrpList.get(j);
				
				String Group = (String) batchMap.get("groupName");
				String groupcode = (String) batchMap.get("productGroupCode");
				
				jsonObj.put("GroupCode", groupcode);
				jsonObj.put("Group", Group);
				jArray.add(jsonObj);
			}
		}
		logger.info("getGroups() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getUoms", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getUoms() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List UomList = storeService.getUomsList();
		logger.info("getUoms() ========UomList==========" + UomList);
		if (UomList != null && UomList.size() > 0)
		{
			for (int j = 0; j < UomList.size(); j++)
			{
				Map batchMap = new HashMap();
				batchMap = (Map) UomList.get(j);
				
				String uom = (String) batchMap.get("uom");
				Integer uomcode = (Integer) batchMap.get("uomCode");
				
				jsonObj.put("UomCode", uomcode);
				jsonObj.put("Uom", uom);
				jArray.add(jsonObj);
			}
		}
		logger.info("getUoms() ========jArray==========" + jArray);
		return jArray;
	}
	

	@RequestMapping(value = "/getStockMethods", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getStockMethodList() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List StockList = storeService.getStockMethodList();
		logger.info("getStockMethodList() ========UomList==========" + StockList);
		if (StockList != null && StockList.size() > 0)
		{
			for (int j = 0; j < StockList.size(); j++)
			{
				Map batchMap = new HashMap();
				batchMap = (Map) StockList.get(j);
				
				String method = (String) batchMap.get("stockMethod");
				String methodcode = (String) batchMap.get("stockMethodCode");
				
				jsonObj.put("methodCode", methodcode);
				jsonObj.put("method", method);
				jArray.add(jsonObj);
			}
		}
		logger.info("getStockMethodList() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getStockSupplyDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getStockSupplyDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String fromDate = (String) jsonData.get("dateFrom");
		String toDate = (String) jsonData.get("dateTo");
		Integer department = (Integer) jsonData.get("department");
		
		//Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		//Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		List DateList = storeService.getStockSupplyDetails(fromDate,toDate,department);
		logger.info("getStockSupplyDetails() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date date = (Date) dateMap.get("stockdate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String stockDate = df.format(date);
				String stockinno = (String) dateMap.get("stockinno");
				Integer dept = (Integer) dateMap.get("fromdeptid");
				List<DepartmentMaster> deptName= storeService.getDeptName(dept);
				String departmentName=null;
				if(deptName != null && deptName.size()>0)
				{
					 departmentName = deptName.get(0).getDepartment();
				}
				List ItemsList = storeService.getQuantityForStockInNo(stockinno);
				String data="";
				if (ItemsList != null && ItemsList.size() > 0)
				{
					for (int i = 0; i < ItemsList.size(); i++)
					{
						Map qMap = new HashMap();
						qMap = (Map) ItemsList.get(i);
						Double quantity = (Double) qMap.get("quantity");
						String items = (String ) qMap.get("productcode");
						data+=items+"["+quantity+"]"+",";
					}
				}
				
				jsonObj.put("Items", data.substring(0,data.length()-1) );
				jsonObj.put("departmentName", departmentName );
				jsonObj.put("stockDate", stockDate );
				jsonObj.put("stockInNo", stockinno );
				

				jArray.add(jsonObj);
			}
		}
		logger.info("getweighmentDetailsSeedSourceReport() ========jArray==========" + jArray);
		return jArray;
	}
	

	@RequestMapping(value = "/getMaxStockInid", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getMaxStockInid(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		
				
		HttpSession session=request.getSession(false);
		String finYear =(String) session.getAttribute("FINYEAR");
	
		String FinYear=finYear;
		Integer SubGroupid = storeService.getMaxIDfortable("StockInSummary","seqno",FinYear,"finyr");
		logger.info("getMaxStockInid() ========maxuom==========" + SubGroupid);

		String value = Integer.toString(SubGroupid);
		if(value.length() == 1)
		{
			value = "0000"+value;
		}
		else if(value.length() == 2)
		{
			value = "000"+value;
		}
		else if(value.length() == 3)
		{
			value = "00"+value;
		}
		else if(value.length() == 4)
		{
			value = "0"+value;
		}
		
		
		value="SN"+value+"/"+FinYear;
		jsonObj.put("productCode", value);

		jArray.add(jsonObj);
		return jArray;
	}
	
	//Added by DMurty on 20-03-2017
	@RequestMapping(value = "/saveDelivaryChalan", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONObject saveDelivaryChalan(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		List AccountList = new ArrayList();
		
		JSONArray jArray2 = new JSONArray();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		JSONObject jsonObj1 = null;
		jsonObj1 = new JSONObject();
		
		String dcNum = null; 
		try
		{
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			int deptId=formdata.getInt("department");

			DcSummaryBean dcSummaryBean = new ObjectMapper().readValue(formdata.toString(), DcSummaryBean.class);
			
			String finYr = finYear;
			int transactionId = storeService.getMaxIDfortable("StockDetails","transactioncode",finYr,"finyear");

			SDCSummary sDCSummary = storeService.prepareModeForSDCSummary(dcSummaryBean,finYear);
			
			String modifyFlag = dcSummaryBean.getModifyFlag();
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				sDCSummary.setTransactioncode(dcSummaryBean.getTransactioncode());
				String strQry = "Delete from SDCDetails where sdcno='"+sDCSummary.getSdcno()+"'";
				Object obj = strQry;
				UpdateList.add(obj);
			}
			else
			{
				sDCSummary.setTransactioncode(transactionId);
			}
			
			entityList.add(sDCSummary);
			
			dcNum = sDCSummary.getSdcno();
			
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			//Added by DMurty on 15-03-2017
			List actualItems = new ArrayList();
			if("Yes".equalsIgnoreCase(modifyFlag))
			{
				for (int j = 0; j < griddata.size(); j++)
				{
					DcBean dcBean = new ObjectMapper().readValue(griddata.get(j).toString(),DcBean.class);
					
					String batchId = dcBean.getBatchId();
					Map tempMap1 = new HashMap();
					tempMap1.put("batchId", batchId);
					actualItems.add(tempMap1);
				}
			}
			
			for (int i = 0; i < griddata.size(); i++)
			{
				DcBean dcBean = new ObjectMapper().readValue(griddata.get(i).toString(),DcBean.class);
				
				SDCDetails sDCDetails =storeService.prepareModeForSDCDetails(dcSummaryBean,finYear,dcBean);
				entityList.add(sDCDetails);
				
				Map tempMap = new HashMap();
				String gropucode = null;
				String subgropucode=null;
				List ProductList = stockService.getProductDls(dcBean.getProductCode());
				if(ProductList != null && ProductList.size()>0)
				{
					Map productMap = (Map) ProductList.get(0);
					gropucode = (String) productMap.get("categoryCode");
					subgropucode = (String) productMap.get("subCategoryCode");
				}
				
				String productcode = dcBean.getProductCode();
				String batchFlag = dcBean.getBatchFlag();
				String batchId = dcBean.getBatchId();
				int batchSeqNo = dcBean.getBatchSeqNo();
				
				tempMap.put("finYr",finYear);
				tempMap.put("productCode", dcBean.getProductCode());
				int uom = dcBean.getUom();
				String strUom = Integer.toString(uom);
				
				tempMap.put("uom", strUom);
				tempMap.put("batch", dcBean.getBatch());
				tempMap.put("expDt", dcBean.getExpDt());
				tempMap.put("quantity", dcBean.getQuantity());
				tempMap.put("mrp", dcBean.getMrp());
				tempMap.put("productName", dcBean.getProductName());
				tempMap.put("productGrpCode",gropucode);
				tempMap.put("productSubGrpCode",subgropucode);
				tempMap.put("batchId",batchId);
				tempMap.put("batchSeqNo",batchSeqNo);
				tempMap.put("departmentId",deptId);
				tempMap.put("cp",dcBean.getCp());

				
				//This needs to come from FE 
				tempMap.put("batchFlag",batchFlag);
				tempMap.put("mfgdt","");
			
				byte invType = 0;
				byte transactionType = 0;
				String screen = "DC";
				List invoiceTypeList = stockService.getTransactionTypeByScreen(screen);
				if(invoiceTypeList != null && invoiceTypeList.size()>0)
				{
					Map invTypeMap = new HashMap();
					invTypeMap = (Map) invoiceTypeList.get(0);
					transactionType = (Byte) invTypeMap.get("transactiontype");
					invType = (Byte) invTypeMap.get("invoicetypecode");
				}
				tempMap.put("transactionType",transactionType);
				tempMap.put("invType",invType);
				tempMap.put("invoiceNo","0");
				
				List StockList = new ArrayList();
				
				tempMap.put("transactionId",transactionId);
				
				if("Yes".equalsIgnoreCase(modifyFlag))
				{
					tempMap.put("transactionId",dcSummaryBean.getTransactioncode());
					deptId = dcSummaryBean.getDepartment();
					if(i == 0)
					{
						List stockDetails = stockService.getStockDetailsDataForRevert(batchId,dcSummaryBean.getTransactioncode(),finYr,deptId);
						if(stockDetails != null && stockDetails.size()>0)
						{
							for(int k=0;k<stockDetails.size();k++)
							{
								StockDetails sd = (StockDetails) stockDetails.get(k);
								//Here 0 is active and 1 is inactive
								sd.setStatus((byte) 1);
								entityList.add(sd);
								String strbatchId = sd.getBatchid();
								
								Map TempMap = new HashMap();
								TempMap.put("batchId", strbatchId);
								
								//Added by DMurty on 15-03-2017
								//If user removed an item from grid, we are comparing grid with previous transaction and updating stock if record is not there.
								boolean istrue = actualItems.contains(TempMap);
								if(istrue == false)
								{
									double currentStock = 0.0;
									double prevStock =stockService.getStockRunBal(strbatchId,dcSummaryBean.getTransactioncode(),finYr,deptId);
									
									List StockSummaryList = stockService.getStockSummaryDetails(strbatchId,dcSummaryBean.getDepartment());
									if(StockSummaryList != null && StockSummaryList.size()>0)
									{
										StockSummary sm = (StockSummary) StockSummaryList.get(0);
										
										List batchList = stockService.getBatchDetails(batchId);
										if(batchList != null && batchList.size()>0)
										{
											StoreBatchMaster storeBatchMaster = (StoreBatchMaster) batchList.get(0);
											
											double stockvalueincp = currentStock*storeBatchMaster.getCp();
											double stockvalueinmrp = currentStock*storeBatchMaster.getMrp();
											
											sm.setStock(currentStock);
											sm.setStockvalueincp(stockvalueincp);
											sm.setStockvalueinmrp(stockvalueinmrp);
											entityList.add(sm);
										}
									}
								}
							}
						}
						StockList = stockService.RevertStockTables(tempMap);
						if(StockList != null && StockList.size()>0)
						{
							for(int s = 0;s<StockList.size();s++)
							{
								entityList.add(StockList.get(s));
							}
						}
					}
				}
				else
				{
					//Here we will get Stock Records
					StockList = stockService.UpdateStockTables(tempMap);
					if(StockList != null && StockList.size()>0)
					{
						for(int s = 0;s<StockList.size();s++)
						{
							entityList.add(StockList.get(s));
						}
					}
					else
					{
						return jsonObj1;
					}
				}
			}
			isInsertSuccess = storeService.saveMultipleEntities(entityList, UpdateList,AccountList);
			if(isInsertSuccess == true)
			{
				DcSummaryBean dcSmryBean = storeService.prepareDcSummaryBean(storeService.listAllSDCSummary(dcNum));

				jsonObj.put("address", dcSmryBean.getAddress());
				jsonObj.put("contactNumber", dcSmryBean.getContactnumber());
				jsonObj.put("customerName", dcSmryBean.getCustomername());
				jsonObj.put("entryDate", dcSmryBean.getDateOfEntry());
				jsonObj.put("dcDate", dcSmryBean.getDcdate());
				jsonObj.put("dcNumber", dcSmryBean.getDcnumber());
				jsonObj.put("dcSeqNo", dcSmryBean.getDcSeqNo());
				
				String departmentName = null;
				List<DepartmentMaster> deptName= storeService.getDeptName(dcSmryBean.getDepartment());
				if(deptName != null && deptName.size()>0)
				{
					 departmentName = deptName.get(0).getDepartment();
				}
				jsonObj.put("department",departmentName);

				jArray.add(jsonObj);
				jsonObj1.put("formData", jArray);
				
				List<DcBean> dcDetailsBean = storeService.prepareDcDetails(storeService.listAllSDcDetails(dcNum));
				for (DcBean dcBean : dcDetailsBean)
				{
					JSONObject gridJsonObj = new JSONObject();
					
					String productCode = dcBean.getProductCode();
					String productName = null;
					List ProductList = stockService.getProductDls(dcBean.getProductCode());
					if(ProductList != null && ProductList.size()>0)
					{
						Map productMap = (Map) ProductList.get(0);
						productName = (String) productMap.get("productName");
					}
					
					gridJsonObj.put("productName",productName);
					gridJsonObj.put("productCode",dcBean.getProductCode());
					gridJsonObj.put("batch",dcBean.getBatch());
					gridJsonObj.put("exp",dcBean.getExpDt());
					gridJsonObj.put("cp",dcBean.getCp());
					gridJsonObj.put("mrp",dcBean.getMrp());
					
					gridJsonObj.put("uom",dcBean.getUom());
					gridJsonObj.put("uomName",dcBean.getUomName());
					gridJsonObj.put("stockInDate",dcBean.getStockInDate());
					
					gridJsonObj.put("quantity",dcBean.getQuantity());
					gridJsonObj.put("totalCost",dcBean.getTotalCost());
					gridJsonObj.put("batchId",dcBean.getBatchId());
					gridJsonObj.put("batchSeqNo",dcBean.getBatchSeqNo());
					
					jArray2.add(gridJsonObj);
				}
				jsonObj1.put("gridData", jArray2);
				
				return jsonObj1;
			}
			else
			{
				return jsonObj1;
			}
		}
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return jsonObj1;
		}
	}
	
	@RequestMapping(value = "/getAllDcs", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllDcs(@RequestBody String dcNo) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		
		DcSummaryBean dcSummaryBean = storeService.prepareDcSummaryBean(storeService.listAllSDCSummary(dcNo));
		List<DcBean> dcBean = storeService.prepareDcDetails(storeService.listAllSDcDetails(dcNo));

		org.json.JSONObject DcSummaryBeanJson = new org.json.JSONObject(dcSummaryBean);
		org.json.JSONArray DcBeanArray = new org.json.JSONArray(dcBean);

		response.put("DcSummaryBean", DcSummaryBeanJson);
		response.put("DcBeanArray", DcBeanArray);

		logger.info("------getAllStockInDetails-----" + response.toString());
		return response.toString();
	}
	
	@RequestMapping(value = "/getSDCDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getSDCDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String fromDate = (String) jsonData.get("dateFrom");
		String toDate = (String) jsonData.get("dateTo");
		Integer department = (Integer) jsonData.get("department");
		
		List DcList = storeService.getSDCDetails(fromDate,toDate,department);
		logger.info("getSDCDetails() ========DcList==========" + DcList);
		if (DcList != null && DcList.size() > 0)
		{
			for (int j = 0; j < DcList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DcList.get(j);

				Date sdcdate = (Date) dateMap.get("sdcdate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strSdcDate = df.format(sdcdate);
				
				String sdcno = (String) dateMap.get("sdcno");
				Integer dept = (Integer) dateMap.get("deptid");
				byte status=(Byte) dateMap.get("status");
				
				List<DepartmentMaster> deptName= storeService.getDeptName(dept);
				String departmentName=null;
				if(deptName != null && deptName.size()>0)
				{
					 departmentName = deptName.get(0).getDepartment();
				}
				
				List ItemsList = storeService.getQuantityForDcDetails(sdcno);
				String data="";
				if (ItemsList != null && ItemsList.size() > 0)
				{
					for (int i = 0; i < ItemsList.size(); i++)
					{
						Map qMap = new HashMap();
						qMap = (Map) ItemsList.get(i);
						Double quantity = (Double) qMap.get("qty");
						String items = (String ) qMap.get("productcode");
						data+=items+"["+quantity+"]"+",";
					}
				}
				
				jsonObj.put("sdcdate", strSdcDate );
				jsonObj.put("sdcno", sdcno );
				jsonObj.put("status", status );
				jsonObj.put("departmentName", departmentName );
				jsonObj.put("dcItems", data.substring(0,data.length()-1) );
				
				jArray.add(jsonObj);
			}
		}
		logger.info("getSDCDetails() ========jArray==========" + jArray);
		return jArray;
	}
	
	
	
	
	
	
	
	//Added by DMurty on 15-03-2017
	@RequestMapping(value = "/saveOpeningStock", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveOpeningStock(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		List AccountList = new ArrayList();
		Map SMap = new HashMap();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
		
			int deptId=formdata.getInt("department");
			String entryDate=formdata.getString("dateOfEntry");
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			
			String finYr = finYear;//formdata.getString("finYr");
			if("".equals(finYr) || "null".equals(finYr) || finYr == null)
			{
				finYr = "17-18";
			}
			int transactionId = storeService.getMaxIDfortable("StockDetails","transactioncode",finYr,"finyear");

			
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			for (int i = 0; i < griddata.size(); i++)
			{
				OpeningStockBean OpeningStockBean = new ObjectMapper().readValue(griddata.get(i).toString(),OpeningStockBean.class);

				String isModify = OpeningStockBean.getModifyFlag();
				
				Map tempMap = new HashMap();
				
				String gropucode = null;
				String subgropucode=null;
				
				List ProductList = stockService.getProductDls(OpeningStockBean.getProductCode());
				if(ProductList != null && ProductList.size()>0)
				{
					Map productMap = (Map) ProductList.get(0);
					gropucode = (String) productMap.get("categoryCode");
					subgropucode = (String) productMap.get("subCategoryCode");
				}
				
				String batchId=null;
				int batchSeqNo =0;
				String productcode = OpeningStockBean.getProductCode();
				String batchFlag = OpeningStockBean.getBatchFlag();
				if("New".equalsIgnoreCase(batchFlag))
				{
					batchSeqNo = storeService.getMaxIDfortable("StoreBatchMaster","seqno",productcode,"productcode");
					String strBatchSeqNo = Integer.toString(batchSeqNo);
					String actualBatchId = productcode+strBatchSeqNo;
					batchId = actualBatchId;
				}
				else
				{
					batchId = OpeningStockBean.getBatchId();
					batchSeqNo = OpeningStockBean.getBatchSeqNo();
				}
				
				tempMap.put("finYr",finYear);
				tempMap.put("productCode", OpeningStockBean.getProductCode());
				tempMap.put("uom", OpeningStockBean.getUom());
				tempMap.put("batch", OpeningStockBean.getBatch());
				
				tempMap.put("expDt", OpeningStockBean.getExpDate());
				tempMap.put("quantity", OpeningStockBean.getQty());
				tempMap.put("mrp", OpeningStockBean.getMrp());
				tempMap.put("cp", OpeningStockBean.getCp());
				tempMap.put("productName", OpeningStockBean.getProductName());
				tempMap.put("productGrpCode",gropucode);
				tempMap.put("productSubGrpCode",subgropucode);
				tempMap.put("batchId",batchId);
				tempMap.put("batchSeqNo",batchSeqNo);
				tempMap.put("departmentId",deptId);
				
				//This needs to come from FE 
				tempMap.put("batchFlag",batchFlag);
				tempMap.put("mfgdt","");
			
				byte invType = 0;
				byte transactionType = 0;
				String screen = "Opening Balance";
				List invoiceTypeList = stockService.getTransactionTypeByScreen(screen);
				if(invoiceTypeList != null && invoiceTypeList.size()>0)
				{
					Map invTypeMap = new HashMap();
					invTypeMap = (Map) invoiceTypeList.get(0);
					transactionType = (Byte) invTypeMap.get("transactiontype");
					invType = (Byte) invTypeMap.get("invoicetypecode");
				}
				tempMap.put("transactionType",transactionType);
				tempMap.put("invType",invType);
				tempMap.put("invoiceNo","0");
				
				List StockList = new ArrayList();
				if("Yes".equalsIgnoreCase(isModify)) 
				{
					tempMap.put("New_TransactionId", transactionId);
					StockList = stockService.revertModiftStockOpngBal(tempMap);
					if(StockList != null && StockList.size()>0)
					{
						for(int s = 0;s<StockList.size();s++)
						{
							entityList.add(StockList.get(s));
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					tempMap.put("transactionId",transactionId);
					//Here we will get Stock Records
					StockList = stockService.UpdateStockTables(tempMap);
					if(StockList != null && StockList.size()>0)
					{
						for(int s = 0;s<StockList.size();s++)
						{
							entityList.add(StockList.get(s));
						}
					}
					else
					{
						return false;
					}
				}
			}
			isInsertSuccess = storeService.saveMultipleEntities(entityList, UpdateList,AccountList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	
	

	@RequestMapping(value = "/SaveStockSupply", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveStockSupply(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		List AccountList = new ArrayList();
		Map SMap = new HashMap();
		
		try
		{
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			
			StockInSummaryBean stockInSummaryBean = new ObjectMapper().readValue(formdata.toString(), StockInSummaryBean.class);
			StockInSummary stockInSummary = storeService.prepareModelforStockInSummary(stockInSummaryBean);
			
			String finYr =finYear; //stockInSummaryBean.getFinYr();
			if("".equals(finYr) || "null".equals(finYr) || finYr == null)
			{
				finYr = "17-18";
			}
			
			int transactionId = storeService.getMaxIDfortable("StockDetails","transactioncode",finYr,"finyear");

			String isModify = stockInSummaryBean.getModifyFlag();
			if("Yes".equalsIgnoreCase(isModify))
			{
				stockInSummary.setStocktransactionno(stockInSummaryBean.getStockTransactionNo());
			}
			else
			{
				stockInSummary.setStocktransactionno(transactionId);
			}
			entityList.add(stockInSummary);
			
			//Added by DMurty on 15-03-2017
			List actualItems = new ArrayList();
			if("Yes".equalsIgnoreCase(isModify))
			{
				for (int j = 0; j < griddata.size(); j++)
				{
					StockInDetailsBean stockInDetailsBean = new ObjectMapper().readValue(griddata.get(j).toString(),StockInDetailsBean.class);
					
					String batchId = stockInDetailsBean.getBatchId();
					Map tempMap1 = new HashMap();
					tempMap1.put("batchId", batchId);
					actualItems.add(tempMap1);
				}
			}
			
			
			for (int i = 0; i < griddata.size(); i++)
			{
				StockInDetailsBean stockInDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),StockInDetailsBean.class);
				
				String productcode = stockInDetailsBean.getProductCode();
				String batchId=null;
				int batchSeqNo =0;

				String batchFlag = stockInDetailsBean.getBatchFlag();
				if("New".equalsIgnoreCase(batchFlag))
				{
					batchSeqNo = storeService.getMaxIDfortable("StoreBatchMaster","seqno",productcode,"productcode");
					String strBatchSeqNo = Integer.toString(batchSeqNo);
					String actualBatchId = productcode+strBatchSeqNo;
					batchId = actualBatchId;
				}
				else
				{
					batchId = stockInDetailsBean.getBatchId();
					batchSeqNo = stockInDetailsBean.getBatchSeqNo();
				}
				
				StockInDetails stockInDetails = storeService.prepareModelforStockInDetails(stockInSummaryBean,stockInDetailsBean);
				stockInDetails.setBatchid(batchId);
				entityList.add(stockInDetails);
				
			
				Map tempMap = new HashMap();
				
				String gropucode = null;
				String subgropucode=null;
				
				List ProductList = stockService.getProductDls(productcode);
				if(ProductList != null && ProductList.size()>0)
				{
					Map productMap = (Map) ProductList.get(0);
					gropucode = (String) productMap.get("categoryCode");
					subgropucode = (String) productMap.get("subCategoryCode");
				}
				
				tempMap.put("finYr", finYr);
				tempMap.put("productCode", stockInDetailsBean.getProductCode());
				tempMap.put("uom", stockInDetailsBean.getUom());
				tempMap.put("batch", stockInDetailsBean.getBatch());
				tempMap.put("expDt", stockInDetailsBean.getExpDt());
				tempMap.put("quantity", stockInDetailsBean.getQuantity());
				tempMap.put("mrp", stockInDetailsBean.getMrp());
				if(stockInDetailsBean.getCp()==null)
				 {
					tempMap.put("cp", stockInDetailsBean.getMrp());
				 }
				 else
				 {
					tempMap.put("cp", stockInDetailsBean.getCp());
				 }
				tempMap.put("productName", stockInDetailsBean.getProductName());
				tempMap.put("productGrpCode",gropucode);
				tempMap.put("productSubGrpCode",subgropucode);
				tempMap.put("batchId",batchId);
				tempMap.put("batchSeqNo",batchSeqNo);

				
				//This needs to come from FE 
				tempMap.put("batchFlag",stockInDetailsBean.getBatchFlag());
				tempMap.put("mfgdt","");
				tempMap.put("departmentId",stockInSummaryBean.getDepartment());
				
				byte invType = 0;
				byte transactionType = 0;
				String screen = "Stock Supply";
				List invoiceTypeList = stockService.getTransactionTypeByScreen(screen);
				if(invoiceTypeList != null && invoiceTypeList.size()>0)
				{
					Map invTypeMap = new HashMap();
					invTypeMap = (Map) invoiceTypeList.get(0);
					transactionType = (Byte) invTypeMap.get("transactiontype");
					invType = (Byte) invTypeMap.get("invoicetypecode");
				}
				tempMap.put("transactionType",transactionType);
				tempMap.put("invType",invType);
				tempMap.put("invoiceNo",stockInSummaryBean.getStockInNo());
				
				List StockList = new ArrayList();
				
				if("Yes".equalsIgnoreCase(isModify)) 
				{
					//Need to revert here.
					tempMap.put("transactionId",stockInSummaryBean.getStockTransactionNo());
					int deptId = stockInSummaryBean.getDepartment();
					
					if(i == 0)
					{
						List stockDetails = stockService.getStockDetailsDataForRevert(batchId,stockInSummaryBean.getStockTransactionNo(),finYr,deptId);
						if(stockDetails != null && stockDetails.size()>0)
						{
							for(int k=0;k<stockDetails.size();k++)
							{
								StockDetails sd = (StockDetails) stockDetails.get(k);
								//Here 0 is active and 1 is inactive
								sd.setStatus((byte) 1);
								entityList.add(sd);
								String strbatchId = sd.getBatchid();
								
								Map TempMap = new HashMap();
								TempMap.put("batchId", strbatchId);
								
								//Added by DMurty on 15-03-2017
								//If user removed an item from grid, we are comparing grid with previous transaction and updating stock if record is not there.
								boolean istrue = actualItems.contains(TempMap);
								if(istrue == false)
								{
									double currentStock = 0.0;
									double prevStock =stockService.getStockRunBal(strbatchId,stockInSummaryBean.getStockTransactionNo(),finYr,deptId);
									
									List StockSummaryList = stockService.getStockSummaryDetails(strbatchId,stockInSummaryBean.getDepartment());
									if(StockSummaryList != null && StockSummaryList.size()>0)
									{
										StockSummary sm = (StockSummary) StockSummaryList.get(0);
										
										double stockvalueincp = currentStock*stockInDetailsBean.getCp();
										double stockvalueinmrp = currentStock*stockInDetailsBean.getMrp();
										
										sm.setStock(currentStock);
										sm.setStockvalueincp(stockvalueincp);
										sm.setStockvalueinmrp(stockvalueinmrp);
										entityList.add(sm);
									}
								}
							}
						}
					}
					
					
					StockList = stockService.RevertStockTables(tempMap);
					if(StockList != null && StockList.size()>0)
					{
						for(int s = 0;s<StockList.size();s++)
						{
							entityList.add(StockList.get(s));
						}
					}
				}
				else
				{
					tempMap.put("transactionId",transactionId);

					//Here we will get Stock Records
					StockList = stockService.UpdateStockTables(tempMap);
					if(StockList != null && StockList.size()>0)
					{
						for(int s = 0;s<StockList.size();s++)
						{
							entityList.add(StockList.get(s));
						}
					}
					else
					{
						return false;
					}
				}
			}
			isInsertSuccess = storeService.saveMultipleEntities(entityList, UpdateList,AccountList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	@RequestMapping(value = "/getProductCodes", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getProductCodes(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = storeService.getProductCodes(jsonData);
		logger.info("getProductCodes() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String productCode = (String) dateMap.get("productCode");
				jsonObj.put("ProductCode", productCode );
				jArray.add(jsonObj);
			}
		}
		logger.info("getProductCodes() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getProductDetailsByCodes", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getProductDetailsByCodes(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = storeService.getProductDetailsBycode(jsonData);
		logger.info("getProductDetailsByCodes() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String productCode = (String) dateMap.get("productCode");
				String productName = (String) dateMap.get("productName");
				Integer uom = (Integer) dateMap.get("uom");
				jsonObj.put("ProductCode", productCode );
				jsonObj.put("ProductName", productName );
				String uomname="";
				List uomList = storeService.getUomNameById(uom);
				if (uomList != null && uomList.size() > 0)
				{
					Map batchMap1 = new HashMap();
					batchMap1 = (Map) uomList.get(0);
					uomname=(String) batchMap1.get("uom");
				}
				jsonObj.put("Uom", uom );
				jsonObj.put("Uomname", uomname );

				jArray.add(jsonObj);
			}
		}
		logger.info("getProductDetailsByCodes() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAllStockInDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllStockInDetails(@RequestBody String stockInNo) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		

		StockInSummaryBean stockInSummaryBean = storeService.prepareStockInSummaryBean(storeService.listAllStockInSummary(stockInNo));
		List<StockInDetailsBean> stockInDetailsBean = storeService.prepareStockInDetails(storeService.listAllStockInDetails(stockInNo));

		org.json.JSONObject StockInSummaryJson = new org.json.JSONObject(stockInSummaryBean);
		org.json.JSONArray StockInDetailsBeanArray = new org.json.JSONArray(stockInDetailsBean);

		response.put("StockInSummaryBean", StockInSummaryJson);
		response.put("StockInDetailsBean", StockInDetailsBeanArray);

		logger.info("------getAllStockInDetails-----" + response.toString());
		return response.toString();

	}
	
	@RequestMapping(value = "/SaveStockIndent", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveStockIndent(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();

		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			StockReqSmryBean stockReqSmryBean = new ObjectMapper().readValue(formdata.toString(), StockReqSmryBean.class);

			StockReqSmry stockReqSmry = storeService.prepareModelforStockReqSmry(stockReqSmryBean);

			entityList.add(stockReqSmry);
			for (int i = 0; i < griddata.size(); i++)
			{
				StockReqDtlsBean stockReqDtlsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
						StockReqDtlsBean.class);
				StockReqDtls stockReqDtls = storeService.prepareModelforStockReqDtls(stockReqSmryBean,stockReqDtlsBean);

				entityList.add(stockReqDtls);
			}
		
			//isInsertSuccess = storeService.saveMultipleEntities(entityList);
			isInsertSuccess = storeService.saveandupdateMultipleEntities(entityList, UpdateList,stockReqSmryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	
	
	@RequestMapping(value = "/getAllStockIndentDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllStockIndentDetails(@RequestBody String stockIndentNo) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		

		StockReqSmryBean stockReqSmryBean = storeService.prepareStockIndentSummaryBean(storeService.listAllStockReqSummary(stockIndentNo));
		List<StockReqDtlsBean> stockReqDtlsBean = storeService.prepareStockIndentDetails(storeService.listAllStockReqDetails(stockIndentNo));

		org.json.JSONObject StockInSummaryJson = new org.json.JSONObject(stockReqSmryBean);
		org.json.JSONArray StockInDetailsBeanArray = new org.json.JSONArray(stockReqDtlsBean);

		response.put("SummaryBean", StockInSummaryJson);
		response.put("DetailsBean", StockInDetailsBeanArray);

		logger.info("------getAllStockIndentDetails-----" + response.toString());
		return response.toString();

	}
		@RequestMapping(value = "/getStockIndentDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getStockIndentDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String fromDate = (String) jsonData.get("dateFrom");
		String toDate = (String) jsonData.get("dateTo");
		Integer department = (Integer) jsonData.get("department");
		Integer userstatus = GetloginuserStatus();

		
		List DateList = storeService.getStockIndentDetails(fromDate,toDate,department,userstatus);
		logger.info("getStockIndentDetails() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date date = (Date) dateMap.get("reqdate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String reqDate = df.format(date);
				
				String stockReqNo = (String) dateMap.get("stockreqno");
				Integer dept = (Integer) dateMap.get("deptid");
				Integer status=(Integer) dateMap.get("status");
				List<DepartmentMaster> deptName= storeService.getDeptName(dept);
				String departmentName=null;
				if(deptName != null && deptName.size()>0)
				{
					 departmentName = deptName.get(0).getDepartment();
				}
				List ItemsList = storeService.getQuantityForStockIndent(stockReqNo);
				String data="";
				if (ItemsList != null && ItemsList.size() > 0)
				{
					for (int i = 0; i < ItemsList.size(); i++)
					{
						Map qMap = new HashMap();
						qMap = (Map) ItemsList.get(i);
						Double quantity = (Double) qMap.get("quantity");
						String items = (String ) qMap.get("productcode");
						data+=items+"["+quantity+"]"+",";
					}
				}
				
				jsonObj.put("Items", data.substring(0,data.length()-1) );
				jsonObj.put("departmentName", departmentName );
				jsonObj.put("reqDate", reqDate );
				jsonObj.put("stockReqNo", stockReqNo );
				jsonObj.put("status", status );
				jsonObj.put("deptid", dept );

				

				jArray.add(jsonObj);
			}
		}
		logger.info("getweighmentDetailsSeedSourceReport() ========jArray==========" + jArray);
		return jArray;
	}
	
		
		@RequestMapping(value = "/SaveStockIssue", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		boolean SaveStockIssue(@RequestBody JSONObject jsonData) throws Exception
		{
			boolean isInsertSuccess = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountList = new ArrayList();

			List entityList1 = new ArrayList();
			List UpdateList1 = new ArrayList();
			List ItemsList=new ArrayList();
			try
			{
				
				HttpSession session=request.getSession(false);
				String finYear =(String) session.getAttribute("FINYEAR");
				
				JSONObject formdata = (JSONObject) jsonData.get("formData");
				JSONArray griddata = (JSONArray) jsonData.get("gridData");

				StockIssueSmryBean stockIssueSmryBean = new ObjectMapper().readValue(formdata.toString(), StockIssueSmryBean.class);
				if("Yes".equalsIgnoreCase(stockIssueSmryBean.getModifFlag()))
				{
					ItemsList = storeService.getQuantityForStockIssue(stockIssueSmryBean.getStockIssueNo());
				}
				StockIssueSmry stockIssueSmry = storeService.prepareModelforStockIssueSmry(stockIssueSmryBean);
				String stockReqNo=stockIssueSmry.getStockreqno();
				
				entityList.add(stockIssueSmry);
			
				List ItemsList1 = storeService.getQuantityForStockIndent(stockReqNo);
					if (ItemsList1 != null && ItemsList1.size() > 0)
					{
						for (int i = 0; i < ItemsList1.size(); i++)
						{
							Map qMap = new HashMap();
							qMap = (Map) ItemsList1.get(i);
							String productcode = (String ) qMap.get("productcode");
							double pendingquantity = (Double) qMap.get("pendingqty");
							double issuedqty = (Double) qMap.get("issuedqty");

							double balanceqty=0;
							int status=3;
								
							if("Yes".equalsIgnoreCase(stockIssueSmryBean.getModifFlag()))
							{
								
								Double quantity = 0.0;
								if (ItemsList != null && ItemsList.size() > 0)
								{
									for (int k = 0; k < ItemsList.size(); k++)
									{
										Map qMap1 = new HashMap();
										qMap1 = (Map) ItemsList.get(k);
										String items = (String ) qMap1.get("productcode");
										 if(productcode.equals(items))
											{
											 quantity = (Double) qMap1.get("quantity");
											 quantity+=quantity;
											}
									}
								}
								Double Gridissuedqty=0.0;
								for (int k = 0; k < griddata.size(); k++)
								{
									
									StockIssueDtlsBean stockIssueDtlsBean = new ObjectMapper().readValue(griddata.get(k).toString(),
											StockIssueDtlsBean.class);
									StockIssueDtls stockIssueDtls = storeService.prepareModelforStockIssueDtls(stockIssueSmryBean,stockIssueDtlsBean);
									if(productcode.equals(stockIssueDtlsBean.getProductCode()))
									{
											Gridissuedqty=Gridissuedqty+stockIssueDtls.getIssuedqty();
									}
								}
								
								
								balanceqty =pendingquantity+quantity-Gridissuedqty;

								issuedqty=issuedqty+Gridissuedqty-quantity;
								
									
							}
							else
							{
								for (int k = 0; k < griddata.size(); k++)
								{
									
									StockIssueDtlsBean stockIssueDtlsBean = new ObjectMapper().readValue(griddata.get(k).toString(),
											StockIssueDtlsBean.class);
									StockIssueDtls stockIssueDtls = storeService.prepareModelforStockIssueDtls(stockIssueSmryBean,stockIssueDtlsBean);
									if(productcode.equals(stockIssueDtlsBean.getProductCode()))
									{
											issuedqty=issuedqty+stockIssueDtls.getIssuedqty();
									}
								}
								balanceqty =pendingquantity-issuedqty;
							}
								
							String strQry = "";
							Object Qryobj = "";
							strQry = "UPDATE StockReqDtls set status=" + status + ",pendingqty=" + balanceqty
							        + ",issuedqty=" + issuedqty+ " WHERE stockreqno ='" + stockReqNo + "' and productcode= '"+productcode+"' ";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
							
							
						}	
					
					}
				/////////////////Stock functionality//////////////////////////
					
					int transactionId = storeService.getMaxIDfortable("StockDetails","transactioncode",finYear,"finyear");

					String isModify = stockIssueSmryBean.getModifFlag();
					if("Yes".equalsIgnoreCase(isModify))
					{
						stockIssueSmry.setStockTransactionNo(stockIssueSmryBean.getStockTransactionNo());
					}
					else
					{
						stockIssueSmry.setStockTransactionNo(transactionId);
					}
					entityList.add(stockIssueSmry);
					
					List actualItems = new ArrayList();
					if("Yes".equalsIgnoreCase(isModify))
					{
						for (int j = 0; j < griddata.size(); j++)
						{
							StockIssueDtlsBean stockIssueDtlsBean = new ObjectMapper().readValue(griddata.get(j).toString(),StockIssueDtlsBean.class);
							
							String batchId = stockIssueDtlsBean.getBatchId();
							Map tempMap1 = new HashMap();
							tempMap1.put("batchId", batchId);
							actualItems.add(tempMap1);
						}
					}
					
					
					for (int i = 0; i < griddata.size(); i++)
					{
						StockIssueDtlsBean stockIssueDtlsBean = new ObjectMapper().readValue(griddata.get(i).toString(),StockIssueDtlsBean.class);
						
						String productcode = stockIssueDtlsBean.getProductCode();
						String batchId=null;
						int batchSeqNo =0;

						String batchFlag = "Old";
						if("New".equalsIgnoreCase(batchFlag))
						{
							batchSeqNo = storeService.getMaxIDfortable("StoreBatchMaster","seqno",productcode,"productcode");
							String strBatchSeqNo = Integer.toString(batchSeqNo);
							String actualBatchId = productcode+strBatchSeqNo;
							batchId = actualBatchId;
						}
						else
						{
							batchId = stockIssueDtlsBean.getBatchId();
							batchSeqNo = stockIssueDtlsBean.getBatchSeqNo();
						}
						
						
							 stockIssueDtlsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
									StockIssueDtlsBean.class);
							StockIssueDtls stockIssueDtls = storeService.prepareModelforStockIssueDtls(stockIssueSmryBean,stockIssueDtlsBean);
							entityList.add(stockIssueDtls);
						
						Map tempMap = new HashMap();
						
						String gropucode = null;
						String subgropucode=null;
						
						List ProductList = stockService.getProductDls(productcode);
						if(ProductList != null && ProductList.size()>0)
						{
							Map productMap = (Map) ProductList.get(0);
							gropucode = (String) productMap.get("categoryCode");
							subgropucode = (String) productMap.get("subCategoryCode");
						}
					
						List batchList = stockService.getBatchDetails(stockIssueDtlsBean.getBatchId());
						StoreBatchMaster storeBatchMaster1 = (StoreBatchMaster) batchList.get(0);
					
						String Strbatch = storeBatchMaster1.getBatch();
						Double cp = storeBatchMaster1.getCp();
						
						
						tempMap.put("finYr", finYear);
						tempMap.put("productCode", stockIssueDtlsBean.getProductCode());
						tempMap.put("uom", stockIssueDtlsBean.getUom());
						tempMap.put("batch",Strbatch);
						tempMap.put("expDt", "");
						tempMap.put("quantity", stockIssueDtlsBean.getIssuedQty());
						tempMap.put("mrp", stockIssueDtlsBean.getMrp());
						tempMap.put("cp", cp);
						
						 
						tempMap.put("productName", stockIssueDtlsBean.getProductName());
						tempMap.put("productGrpCode",gropucode);
						tempMap.put("productSubGrpCode",subgropucode);
						tempMap.put("batchId",batchId);
						tempMap.put("batchSeqNo",batchSeqNo);

						//This needs to come from FE 
						tempMap.put("batchFlag","Old");
						tempMap.put("mfgdt","");
						
						int csId=stockIssueSmryBean.getCentralStoreId();
						tempMap.put("departmentId",csId);
						
						byte invType = 0;
						byte transactionType = 0;
						String screen = "Stock Issue";
						List invoiceTypeList = stockService.getTransactionTypeByScreen(screen);
						if(invoiceTypeList != null && invoiceTypeList.size()>0)
						{
							Map invTypeMap = new HashMap();
							invTypeMap = (Map) invoiceTypeList.get(0);
							transactionType = (Byte) invTypeMap.get("transactiontype");
							invType = (Byte) invTypeMap.get("invoicetypecode");
						}
						tempMap.put("transactionType",transactionType);
						tempMap.put("invType",invType);
						tempMap.put("invoiceNo",stockIssueSmryBean.getStockIssueNo());
						
						List StockList = new ArrayList();
						
						if("Yes".equalsIgnoreCase(isModify)) 
						{
							//Need to revert here.
							tempMap.put("transactionId",stockIssueSmryBean.getStockTransactionNo());
							int deptId = stockIssueSmryBean.getDeptId();
							
							if(i == 0)
							{
								List stockDetails = stockService.getStockDetailsDataForRevert(batchId,stockIssueSmryBean.getStockTransactionNo(),finYear,deptId);
								if(stockDetails != null && stockDetails.size()>0)
								{
									for(int k=0;k<stockDetails.size();k++)
									{
										StockDetails sd = (StockDetails) stockDetails.get(k);
										//Here 0 is active and 1 is inactive
										sd.setStatus((byte) 1);
										entityList.add(sd);
										String strbatchId = sd.getBatchid();
										
										Map TempMap = new HashMap();
										TempMap.put("batchId", strbatchId);
									
										//If user removed an item from grid, we are comparing grid with previous transaction and updating stock if record is not there.
										boolean istrue = actualItems.contains(TempMap);
										if(istrue == false)
										{
											double currentStock = 0.0;
											double prevStock =stockService.getStockRunBal(strbatchId,stockIssueSmryBean.getStockTransactionNo(),finYear,deptId);
											
											List StockSummaryList = stockService.getStockSummaryDetails(strbatchId,stockIssueSmryBean.getDeptId());
											if(StockSummaryList != null && StockSummaryList.size()>0)
											{
												StockSummary sm = (StockSummary) StockSummaryList.get(0);
												
												double stockvalueincp = currentStock*cp;
												double stockvalueinmrp = currentStock*stockIssueDtlsBean.getMrp();
												
												sm.setStock(currentStock);
												sm.setStockvalueincp(stockvalueincp);
												sm.setStockvalueinmrp(stockvalueinmrp);
												entityList.add(sm);
											}
										}
									}
								}
							}
							
							
							StockList = stockService.RevertStockTables(tempMap);
							if(StockList != null && StockList.size()>0)
							{
								for(int s = 0;s<StockList.size();s++)
								{
									entityList.add(StockList.get(s));
								}
							}
						}
						else
						{
							tempMap.put("transactionId",transactionId);

							//Here we will get Stock Records
							StockList = stockService.UpdateStockTables(tempMap);
							if(StockList != null && StockList.size()>0)
							{
								for(int s = 0;s<StockList.size();s++)
								{
									entityList.add(StockList.get(s));
								}
							}
							else
							{
								return false;
							}
						}
					}
				isInsertSuccess = storeService.saveMultipleEntities(entityList, UpdateList,AccountList);

				String detailstatus="true";
				String summarystatus="false";

				if(isInsertSuccess)
				{
				List ItemsList2 = storeService.getQuantityForStockIndent(stockReqNo);
				if (ItemsList2 != null && ItemsList2.size() > 0)
				{
					for (int i = 0; i < ItemsList2.size(); i++)
					{
						Map qMap = new HashMap();
						qMap = (Map) ItemsList2.get(i);
						Integer status = (Integer) qMap.get("status");
						double pendingqty = (Double) qMap.get("pendingqty");

						if ((status == 1) || (status == 2))
						{
							detailstatus = "false";

						}
						else
						{
							if(pendingqty==0)
							{
								summarystatus = "true";
							}
							else
							{
								summarystatus = "false";
								break;

							}
							
						}
					}
						int status=2;		
						if("true".equals(summarystatus)&&"true".equals(detailstatus))
						{
						 status=3;//closed or complete or cancled
						}
						else
						{
						 status=2;//in pending
						}
						String strQry = "";
						Object Qryobj = "";
						strQry = "UPDATE StockReqSmry set status=" + status + " WHERE stockreqno ='" + stockReqNo + "' ";
						Qryobj = strQry;
						UpdateList1.add(Qryobj);
					}
				isInsertSuccess = storeService.saveandupdateMultipleEntities(entityList1, UpdateList1,"No");

				}

				return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}
		
		@RequestMapping(value = "/getStockIssueDetails", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getStockIssueDetails(@RequestBody JSONObject jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			String fromDate = (String) jsonData.get("dateFrom");
			String toDate = (String) jsonData.get("dateTo");
			Integer department = (Integer) jsonData.get("department");
			
			List DateList = storeService.getStockIssueDtls(fromDate,toDate,department);
			logger.info("getStockIssueDetails() ========DateList==========" + DateList);

			if (DateList != null && DateList.size() > 0)
			{
				for (int j = 0; j < DateList.size(); j++)
				{
					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(j);

					Date date = (Date) dateMap.get("issuedate");
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String issuedate = df.format(date);
					
					String StockIssueNo = (String) dateMap.get("stockissueno");
					Integer deptid = (Integer) dateMap.get("deptid");

					Integer status = (Integer) dateMap.get("status");
					
					List<DepartmentMaster> deptName= storeService.getDeptName(deptid);
					String departmentName=null;
					if(deptName != null && deptName.size()>0)
					{
						 departmentName = deptName.get(0).getDepartment();
					}
					List ItemsList = storeService.getQuantityForStockIssue(StockIssueNo);
					String data="";
					if (ItemsList != null && ItemsList.size() > 0)
					{
						for (int i = 0; i < ItemsList.size(); i++)
						{
							Map qMap = new HashMap();
							qMap = (Map) ItemsList.get(i);
							Double quantity = (Double) qMap.get("quantity");
							String items = (String ) qMap.get("productcode");
							data+=items+"["+quantity+"]"+",";
						}
					}
					
					jsonObj.put("items", data.substring(0,data.length()-1) );
					jsonObj.put("departmentName", departmentName );
					jsonObj.put("issueDate", issuedate );
					jsonObj.put("stockIssueNo", StockIssueNo );
					jsonObj.put("status", status );
					jArray.add(jsonObj);
				}
			}
			logger.info("getStockIssueDetails() ========jArray==========" + jArray);
			return jArray;
		}
		
		@RequestMapping(value = "/getMaxStockReqid", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getMaxStockIndentid(@RequestBody Integer jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			String FinYear=finYear;
			String deptid=Integer.toString(jsonData);
			Integer SubGroupid = storeService.getMaxIDfortablewithDept("StockReqSmry","seqno","deptid",deptid,"finyr",FinYear);
			logger.info("getMaxStockReqid() ========maxuom==========" + SubGroupid);

			String value = Integer.toString(SubGroupid);
			if(value.length() == 1)
			{
				value = "0000"+value;
			}
			else if(value.length() == 2)
			{
				value = "000"+value;
			}
			else if(value.length() == 3)
			{
				value = "00"+value;
			}
			else if(value.length() == 4)
			{
				value = "0"+value;
			}
			String dept=Integer.toString(jsonData);
			if(dept.length() == 1)
			{
				dept = "0"+dept;
			}
			value=dept+value;
			
			value="SR"+value+"/"+FinYear;
			jsonObj.put("StockReqid", value);

			jArray.add(jsonObj);
			return jArray;
		}
		
		@RequestMapping(value = "/getAllStockIssueDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllStockIssueDetails(@RequestBody String stockissueNo) throws Exception
		{
			org.json.JSONObject response = new org.json.JSONObject();
			

			StockIssueSmryBean stockIssueSmryBean = storeService.prepareStockIssueSummaryBean(storeService.listAllStockIssueSummary(stockissueNo));
			
			List<StockIssueDtlsBean> stockIssueDtlsBean = storeService.prepareStockIssueDetails(storeService.listAllStockIssueDetails(stockissueNo),stockIssueSmryBean.getStockReqNo());
			org.json.JSONObject StockInSummaryJson = new org.json.JSONObject(stockIssueSmryBean);
			org.json.JSONArray StockInDetailsBeanArray = new org.json.JSONArray(stockIssueDtlsBean);

			response.put("IssueSummaryBean", StockInSummaryJson);
			response.put("IssueDetailsBean", StockInDetailsBeanArray);

			logger.info("------getAllStockIssueDetails-----" + response.toString());
			return response.toString();

		}
		
		public Integer GetloginuserStatus()
		{
			int status = 0;
			String userId = storeService.getLoggedInUserName();
			List DateList = storeService.listGetEmployeeType(userId);
			logger.info("listGetEmployeeType() ========DateList==========" + DateList);
			if (DateList != null && DateList.size() > 0)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(0);
				status = (Integer) dateMap.get("empStatus");
			}
			return status;
		}
		
		@RequestMapping(value = "/getUserDeptStatus", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getUserDeptStatus() throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			HttpSession session = request.getSession(false);

			Employees employee = (Employees) session.getAttribute(Constants.USER);
			String userName = employee.getEmployeename();
			Integer depatment = employee.getDeptcode();
			Integer status = employee.getEmpStatus();

			jsonObj.put("Department", depatment);
			jsonObj.put("Status", status);
			jsonObj.put("Username", userName);

			jArray.add(jsonObj);
			return jArray;
		}
	
	
		@RequestMapping(value = "/SaveGrnDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		boolean SaveGrnDetails(@RequestBody JSONObject jsonData) throws Exception
		{
			boolean isInsertSuccess = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountList= new ArrayList();
			try
			{
				JSONObject formdata = (JSONObject) jsonData.get("formData");
				JSONArray griddata = (JSONArray) jsonData.get("gridData");
				
				HttpSession session=request.getSession(false);
				String finYear =(String) session.getAttribute("FINYEAR");

				GrnSummaryBean grnSummaryBean = new ObjectMapper().readValue(formdata.toString(), GrnSummaryBean.class);
				
				GrnSummary grnSummary = storeService.prepareModelforGrnSummary(grnSummaryBean);

				entityList.add(grnSummary);
			
				
					/////////////////Stock functionality//////////////////////////
				
					int transactionId = storeService.getMaxIDfortable("StockDetails","transactioncode",finYear,"finyear");

					String isModify = grnSummaryBean.getModifyFlag();
					if("Yes".equalsIgnoreCase(isModify))
					{
						grnSummary.setStockTransactionNo(grnSummaryBean.getStockTransactionNo());
					}
					else
					{
						grnSummary.setStockTransactionNo(transactionId);
					}
					entityList.add(grnSummary);
					
					List actualItems = new ArrayList();
					if("Yes".equalsIgnoreCase(isModify))
					{
						for (int j = 0; j < griddata.size(); j++)
						{
							GrnDetailsBean grnDetailsBean  = new ObjectMapper().readValue(griddata.get(j).toString(),GrnDetailsBean.class);
							
							String batchId = grnDetailsBean.getBatchId();
							Map tempMap1 = new HashMap();
							tempMap1.put("batchId", batchId);
							actualItems.add(tempMap1);
						}
					}
					
					
					for (int i = 0; i < griddata.size(); i++)
					{
						
						
						GrnDetailsBean grnDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),GrnDetailsBean.class);
						GrnDetails grnDetails = storeService.prepareModelforGrnDetails(grnSummaryBean,grnDetailsBean);
						entityList.add(grnDetails);
						
						
						
						String productcode = grnDetailsBean.getProductCode();
						String batchId=null;
						int batchSeqNo =0;

						String batchFlag = "Old";
						if("New".equalsIgnoreCase(batchFlag))
						{
							batchSeqNo = storeService.getMaxIDfortable("StoreBatchMaster","seqno",productcode,"productcode");
							String strBatchSeqNo = Integer.toString(batchSeqNo);
							String actualBatchId = productcode+strBatchSeqNo;
							batchId = actualBatchId;
						}
						else
						{
							batchId = grnDetailsBean.getBatchId();
							batchSeqNo = grnDetailsBean.getBatchSeqNo();
						}
						Map tempMap = new HashMap();
						
						String gropucode = null;
						String subgropucode=null;
						
						List ProductList = stockService.getProductDls(productcode);
						if(ProductList != null && ProductList.size()>0)
						{
							Map productMap = (Map) ProductList.get(0);
							gropucode = (String) productMap.get("categoryCode");
							subgropucode = (String) productMap.get("subCategoryCode");
						}
					
						List batchList = stockService.getBatchDetails(grnDetailsBean.getBatchId());
						StoreBatchMaster storeBatchMaster1 = (StoreBatchMaster) batchList.get(0);
					
						String Strbatch = storeBatchMaster1.getBatch();
						Double cp = storeBatchMaster1.getCp();
						
						
						tempMap.put("finYr", finYear);
						tempMap.put("productCode", grnDetailsBean.getProductCode());
						tempMap.put("uom", grnDetailsBean.getUom());
						tempMap.put("batch",Strbatch);
						tempMap.put("expDt", "");
						tempMap.put("quantity", grnDetailsBean.getIssuedQty());
						tempMap.put("mrp", grnDetailsBean.getMrp());
						tempMap.put("cp", cp);
						
						 
						tempMap.put("productName", grnDetailsBean.getProductName());
						tempMap.put("productGrpCode",gropucode);
						tempMap.put("productSubGrpCode",subgropucode);
						tempMap.put("batchId",batchId);
						tempMap.put("batchSeqNo",batchSeqNo);

						//This needs to come from FE 
						tempMap.put("batchFlag","Old");
						tempMap.put("mfgdt","");
						
						int csId=grnSummaryBean.getDeptId();
						tempMap.put("departmentId",csId);
						
						byte invType = 0;
						byte transactionType = 0;
						String screen = "GRN";
						List invoiceTypeList = stockService.getTransactionTypeByScreen(screen);
						if(invoiceTypeList != null && invoiceTypeList.size()>0)
						{
							Map invTypeMap = new HashMap();
							invTypeMap = (Map) invoiceTypeList.get(0);
							transactionType = (Byte) invTypeMap.get("transactiontype");
							invType = (Byte) invTypeMap.get("invoicetypecode");
						}
						tempMap.put("transactionType",transactionType);
						tempMap.put("invType",invType);
						tempMap.put("invoiceNo",grnSummaryBean.getStockIssueNo());
						
						List StockList = new ArrayList();
						
						if("Yes".equalsIgnoreCase(isModify)) 
						{
							//Need to revert here.
							tempMap.put("transactionId",grnSummaryBean.getStockTransactionNo());
							int deptId = grnSummaryBean.getDeptId();
							
							if(i == 0)
							{
								List stockDetails = stockService.getStockDetailsDataForRevert(batchId,grnSummaryBean.getStockTransactionNo(),finYear,deptId);
								if(stockDetails != null && stockDetails.size()>0)
								{
									for(int k=0;k<stockDetails.size();k++)
									{
										StockDetails sd = (StockDetails) stockDetails.get(k);
										//Here 0 is active and 1 is inactive
										sd.setStatus((byte) 1);
										entityList.add(sd);
										String strbatchId = sd.getBatchid();
										
										Map TempMap = new HashMap();
										TempMap.put("batchId", strbatchId);
									
										//If user removed an item from grid, we are comparing grid with previous transaction and updating stock if record is not there.
										boolean istrue = actualItems.contains(TempMap);
										if(istrue == false)
										{
											double currentStock = 0.0;
											double prevStock =stockService.getStockRunBal(strbatchId,grnSummaryBean.getStockTransactionNo(),finYear,deptId);
											
											List StockSummaryList = stockService.getStockSummaryDetails(strbatchId,grnSummaryBean.getDeptId());
											if(StockSummaryList != null && StockSummaryList.size()>0)
											{
												StockSummary sm = (StockSummary) StockSummaryList.get(0);
												
												double stockvalueincp = currentStock*cp;
												double stockvalueinmrp = currentStock*grnDetailsBean.getMrp();
												
												sm.setStock(currentStock);
												sm.setStockvalueincp(stockvalueincp);
												sm.setStockvalueinmrp(stockvalueinmrp);
												entityList.add(sm);
											}
										}
									}
								}
							}
							
							
							StockList = stockService.RevertStockTables(tempMap);
							if(StockList != null && StockList.size()>0)
							{
								for(int s = 0;s<StockList.size();s++)
								{
									entityList.add(StockList.get(s));
								}
							}
						}
						else
						{
							tempMap.put("transactionId",transactionId);

							//Here we will get Stock Records
							StockList = stockService.UpdateStockTables(tempMap);
							if(StockList != null && StockList.size()>0)
							{
								for(int s = 0;s<StockList.size();s++)
								{
									entityList.add(StockList.get(s));
								}
							}
							else
							{
								return false;
							}
						}
					}
				isInsertSuccess = storeService.saveMultipleEntities(entityList, UpdateList,AccountList);

			//isInsertSuccess = storeService.saveandupdateMultipleEntities(entityList, UpdateList,grnSummaryBean.getModifyFlag());
				return isInsertSuccess;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}
		
		
		@RequestMapping(value = "/getGrnDetails", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getGrnDetails(@RequestBody JSONObject jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			String fromDate = (String) jsonData.get("dateFrom");
			String toDate = (String) jsonData.get("dateTo");
			Integer department = (Integer) jsonData.get("department");
			
			List DateList = storeService.getGrnsDtls(fromDate,toDate,department);
			logger.info("getGrnsDtls() ========DateList==========" + DateList);

			if (DateList != null && DateList.size() > 0) 
			{
				for (int j = 0; j < DateList.size(); j++)
				{
					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(j);

					Date date = (Date) dateMap.get("grndate");
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String grnDate = df.format(date);
					
					String grnNo = (String) dateMap.get("grnno");
					Integer dept = (Integer) dateMap.get("deptid");
					List<DepartmentMaster> deptName= storeService.getDeptName(dept);
					String departmentName=null;
					if(deptName != null && deptName.size()>0)
					{
						 departmentName = deptName.get(0).getDepartment();
					}
					List ItemsList = storeService.getQuantityForGrn(grnNo);
					String data="";
					if (ItemsList != null && ItemsList.size() > 0)
					{
						for (int i = 0; i < ItemsList.size(); i++)
						{
							Map qMap = new HashMap();
							qMap = (Map) ItemsList.get(i);
							Double quantity = (Double) qMap.get("acceptedqty");
							String items = (String ) qMap.get("productcode");
							data+=items+"["+quantity+"]"+",";
						}
					}
					jsonObj.put("Items", data.substring(0,data.length()-1) );
					jsonObj.put("departmentName", departmentName );
					jsonObj.put("grnDate", grnDate );
					jsonObj.put("grnNo", grnNo );


					jArray.add(jsonObj);
				}
			}
			logger.info("getGrnDetails() ========jArray==========" + jArray);
			return jArray;
		}
		
		@RequestMapping(value = "/getAllGrnDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		String getAllGrnDetails(@RequestBody String grnNO) throws Exception
		{
			org.json.JSONObject response = new org.json.JSONObject();
			

			GrnSummaryBean grnSummaryBean = storeService.prepareGrnSummaryBean(storeService.listAllGrnSummary(grnNO));
			List<GrnDetailsBean> grnDetailsBean = storeService.prepareGrnDetails(storeService.listAllGrnDetails(grnNO));

			org.json.JSONObject GrnSummaryBeanJson = new org.json.JSONObject(grnSummaryBean);
			org.json.JSONArray GrnDetailsBeanArray = new org.json.JSONArray(grnDetailsBean);

			response.put("GrnSummaryBean", GrnSummaryBeanJson);
			response.put("GrnDetailsBean", GrnDetailsBeanArray);

			logger.info("------getAllGrnDetails-----" + response.toString());
			return response.toString();

		}
		

		@RequestMapping(value = "/getMaxStockIssueid", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getMaxStockIssueid(@RequestBody String jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			String FinYear=finYear;
			Integer SubGroupid = storeService.getMaxIDfortable("StockIssueSmry","seqno",FinYear,"finyr");
			logger.info("getMaxStockInid() ========maxuom==========" + SubGroupid);

			String value = Integer.toString(SubGroupid);
			if(value.length() == 1)
			{
				value = "0000"+value;
			}
			else if(value.length() == 2)
			{
				value = "000"+value;
			}
			else if(value.length() == 3)
			{
				value = "00"+value;
			}
			else if(value.length() == 4)
			{
				value = "0"+value;
			}
			
			
			value="SI"+value+"/"+FinYear;
			jsonObj.put("StockIssueid", value);

			jArray.add(jsonObj);
			return jArray;
		}
		
		
			//Added by DMurty on 07-03-2017
		@RequestMapping(value = "/getProductBatches", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getProductBatches(@RequestBody JSONObject jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			String productCode = (String) jsonData.get("productCode");
			//Added by DMurty on 15-03-2017
			int deptId = (Integer) jsonData.get("deptId");

			List BatchList = stockService.getProductBatches(productCode,deptId);
			logger.info("getProductBatches() ========BatchList==========" + BatchList);
			if (BatchList != null && BatchList.size() > 0)
			{
				for (int j = 0; j < BatchList.size(); j++)
				{
					Map batchMap = new HashMap();
					batchMap = (Map) BatchList.get(j);
					
					String batchId = (String) batchMap.get("batchid");
					double stock = (Double) batchMap.get("stock");
					double mrp = (Double) batchMap.get("mrp");
					double costprice = (Double) batchMap.get("costprice");
					int deptid = (Integer) batchMap.get("deptid");
					String batch = (String) batchMap.get("batch");
					String expdt = (String) batchMap.get("expdt");
					String mfgdt = (String) batchMap.get("mfgdt");
					int seqno = (Integer) batchMap.get("seqno");
					//Added by DMurty on 20-03-2017
					Date batchdate = (Date) batchMap.get("batchdate");
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strbatchdate = df.format(batchdate);
					
					
					JSONObject jsonObj = new JSONObject();
				
					jsonObj.put("batchId",batchId);
					jsonObj.put("batch",batch);
					jsonObj.put("expiryDate",expdt);
					jsonObj.put("costPrice",costprice);
					jsonObj.put("mrp",mrp);
					jsonObj.put("stock",stock);
					jsonObj.put("mfrDate",mfgdt);
					jsonObj.put("batchSeqNo",seqno);
					jsonObj.put("stockInDate",strbatchdate);

					jArray.add(jsonObj);
				}
			}
			logger.info("getProductBatches() ========jArray==========" + jArray);
			return jArray;
		}
		@RequestMapping(value = "/getMaxGrnid", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getMaxGrnid(@RequestBody String jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			String FinYear=finYear;
			Integer SubGroupid = storeService.getMaxIDfortablewithDept("GrnSummary","seqno","deptid",jsonData,"finyr",FinYear);
			logger.info("getMaxStockInid() ========maxuom==========" + SubGroupid);

			String value = Integer.toString(SubGroupid);
			if(value.length() == 1)
			{
				value = "0000"+value;
			}
			else if(value.length() == 2)
			{
				value = "000"+value;
			}
			else if(value.length() == 3)
			{
				value = "00"+value;
			}
			else if(value.length() == 4)
			{
				value = "0"+value;
			}
			if(jsonData.length() == 1)
			{
				jsonData = "0"+jsonData;
			}
			
			value=jsonData+value;
			
			value="GRN"+value+"/"+FinYear;
			jsonObj.put("MaxId", value);

			jArray.add(jsonObj);
			return jArray;
		}
		
		//Added by DMurty on 17-03-2017
		@RequestMapping(value = "/getStockForDepartment", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		double getStockForDepartment(@RequestBody JSONObject jsonData) throws Exception
		{
			String productCode = (String) jsonData.get("productCode");
			int deptId = (Integer) jsonData.get("deptId");
			String finYear = (String) jsonData.get("finYear");
			List entityList = new ArrayList();
			try
			{
				double currentStock = stockService.getCurrentStockForDepartmentByProductCode(productCode,deptId,finYear);
				return currentStock;
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return 0.0;
			}
		}
		
		//Added by DMurty on 17-03-2017
		@RequestMapping(value = "/getCentranStoreId", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		int getCentranStoreId(@RequestBody JSONObject jsonData) throws Exception
		{
			int deptId = stockService.getCentranStoreId();
			return deptId;
		}
		
		//Added by DMurty on 20-03-2017
		@RequestMapping(value = "/getMaxDcNo", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getMaxDcNo(@RequestBody int jsonData) throws Exception
		{
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			
			HttpSession session=request.getSession(false);
			String finYear =(String) session.getAttribute("FINYEAR");
			
			String strDeptId = Integer.toString(jsonData);
			if(strDeptId.length() == 1)
			{
				strDeptId = "0"+strDeptId;
			}
			
			String FinYear=finYear;
			Integer SubGroupid = storeService.getMaxIDfortable("SDCSummary","seqno",FinYear,"finyr");
			logger.info("getMaxStockInid() ========maxuom==========" + SubGroupid);

			String value = Integer.toString(SubGroupid);
			if(value.length() == 1)
			{
				value = "000"+value;
			}
			else if(value.length() == 2)
			{
				value = "00"+value;
			}
			else if(value.length() == 3)
			{
				value = "0"+value;
			}
			String valForInv = value;
			value="SDC"+strDeptId+value+"/"+FinYear;
			
			//SDC070002/16-17
			String saleInvoiceNo = "SINV"+strDeptId+valForInv+"/"+FinYear;
			
			jsonObj.put("dcNum", value);
			jsonObj.put("dcSeqNo", SubGroupid);
			
			jsonObj.put("saleInvoiceNo", saleInvoiceNo);

			jArray.add(jsonObj);
			return jArray;
		}
		
		@RequestMapping(value = "/getRequestDetailsForPrint", method = RequestMethod.POST)
		public @ResponseBody
		JSONArray getRequestDetailsForPrint(@RequestBody JSONObject jsonData) throws Exception
		{
			String requestNo = (String) jsonData.get("RequestNo");
		
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj = null;
			jsonObj = new JSONObject();
			List DateList = storeService.getIndentDetailsForPrint(requestNo);
			StockReqSmryBean stockReqSmryBean = storeService.prepareStockIndentSummaryBean(storeService.listAllStockReqSummary(requestNo));
			String ReqDate = stockReqSmryBean.getReqDate();
			logger.info("getDispatchNoDetailsForPrint() ========DateList==========" + DateList);
			if (DateList != null && DateList.size() > 0)
			{
				for (int j = 0; j < DateList.size(); j++)
				{
					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(j);
					String quantity = (String) dateMap.get("quantity");
					String productcode = (String) dateMap.get("productcode");
					String  remarks = (String) dateMap.get("purpose");
					
					String productName = null;
					List ProductList = stockService.getProductDls(productcode);
					if(ProductList != null && ProductList.size()>0)
					{
						Map productMap = (Map) ProductList.get(0);
						productName = (String) productMap.get("productName");
					}
					jsonObj.put("id", j + 1);
					jsonObj.put("ProductName", productName);
					jsonObj.put("Quantity", quantity);
					jsonObj.put("NoOfAcres", "NA");
					jsonObj.put("Remarks", remarks);
					jsonObj.put("ReqDate", ReqDate);

					jArray.add(jsonObj);
				}
			}
			logger.info("getRequestDetailsForPrint() ========jArray==========" + jArray);
			return jArray;
		}
		
		
		
		
		
	}
