package com.finsol.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.finsol.bean.DepartmentMasterBean;
import com.finsol.bean.UserBean;
import com.finsol.model.DepartmentMaster;
import com.finsol.model.Employees;
import com.finsol.model.Users;
import com.finsol.service.DepartmentMasterServiceImpl;
import com.finsol.service.EmployeeService;
import com.finsol.service.EmployeesService;
import com.finsol.service.UserSeviceImpl;
import com.finsol.utils.Constants;
import com.finsol.utils.Digester;
import com.finsol.utils.SessionUtils;
import com.finsol.utils.UserData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * @author Rama Krishna
 *
 */
@Controller
public class LoginController {
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired
	private EmployeesService employeesService;
	@Autowired
	private UserSeviceImpl userService;
	
	@Autowired
	private Digester digester;
	
	@Autowired
	private HttpServletRequest request;
	
	/*	@Autowired
	private DepartmentMasterServiceImpl departmentMasterServiceImpl;*/
	
	@RequestMapping(value = "/login", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody String sugarCaneValidate(@RequestBody String jsonData,HttpServletRequest request) throws Exception 
	 {
		
		Users user=null;
		UserBean userBean = new ObjectMapper().readValue(jsonData, UserBean.class);
		StringBuffer url=request.getRequestURL();
		String uri=request.getRequestURI();
		logger.info("====url--------"+url+"---"+"------------uri---------"+uri);
		
		//Employee employee = prepareModel(employeeBean);
		String uName=userBean.getName();
		String uPassword=userBean.getPassword();
		String finYear=userBean.getFinYear();

		logger.info("====User Name====="+userBean.getName()+"====PassWord===="+userBean.getPassword());
		

				
		//user=userService.getUser(userBean.getName());
		//createConnection();
		Employees employees=employeesService.getEmployeeByLoginId(userBean.getName());
		
		logger.info("====employees---------"+employees);
		//SessionUtils.setSessionData(user, new UserData(), request);

		//logger.info("====User Name Fom Hibernate====="+user.getName()+"====PassWord  Fom Hibernate===="+user.getPassword());	
		HttpSession session=request.getSession();
		session.setAttribute(Constants.USER,employees);
		session.setAttribute("FINYEAR",finYear);

		session.setMaxInactiveInterval(5*60);
		byte status = employees.getStatus();

		
		
		//employeeService.addEmployee(employee);
		//ObjectMapper ob = new ObjectMapper();

		logger.info("----------------In Login Controller-----------");

		
		logger.info("==============session Object====="+session.toString());
		
		logger.info("User password-----"+digester.digest(uPassword));
		logger.info("Table password-----"+employees.getPassword());
		
		if(employees.getLoginid().equalsIgnoreCase(uName) && employees.getPassword().equals(digester.digest(uPassword)) && status==0)
		{
			return "angular/login.jsp";
		}
		else
		{
			return "/SugarERP";
			
		}
		
		//if(uName.equalsIgnoreCase(user.getName()) && uPassword.equalsIgnoreCase(user.getPassword()))
/*		if("makella".equalsIgnoreCase(uName) && "finsol".equalsIgnoreCase(uPassword))
		{
			return "angular/login.jsp";
		}
		else
		{
			return "/SugarERP";
			
		}*/
	}
	
	
	public String getLoggedInUserName()
	{
		HttpSession session=request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		return userName;
	}
	
	public String createConnection() throws ParseException 
	{
	    Connection conn = null;
	    try 
	    {	    	
	        try
	        {	 
	             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	        } 
	        catch (ClassNotFoundException e) 
	        {
	        	System.out.println("Please include Classpath Where your MySQL Driver is located");
	            e.printStackTrace();
	        }  
	        conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=sugarerpdb","admin","finsol");
	        logger.info("------Connection---------------"+conn);
		    if (conn != null)
		    {
		        System.out.println("Database Connected");
		        logger.info("---Database Connected--------");
		    }
		    else
		    {
		        System.out.println(" connection Failed ");
		        logger.info("--Connection Failed--");
		    }
	    }
	    catch (Exception sqlExp) 
	    {
	    	System.out.println( "Exception::" + sqlExp.toString());
	    } 
	    finally 
	    {
	    	try
	        {
	    		if (conn != null)
	            {
	                conn.close();
	                conn = null;
	            }
	        }
	        catch (SQLException expSQL)
	        {
	            System.out.println("SQLExp::CLOSING::" + expSQL.toString());
	        }
	     }
	    return null;
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public @ResponseBody  String logout(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		HttpSession session=request.getSession(false);
		logger.info("==============session Object====="+session.toString());
		if(session!=null)
		{
			session.invalidate();
		}
		logger.info("==============session Object after invalidate====="+request.getSession(false));
		//return new ModelAndView("index.html");
		return "/SugarERP";
	}
	
	
		
		
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView welcome() {
		System.out.println("==============Inside Welcome=====");
		logger.info("==============Inside welcome()=====");
		return new ModelAndView("index");
	}
	
	@RequestMapping(value = "/gotopage", method = RequestMethod.GET)
	public ModelAndView redirect() {
		System.out.println("==============Inside Welcome=====");
		logger.info("==============Inside welcome()=====");
		return new ModelAndView("index");
	}
	

	
	
	
	@RequestMapping(value = "/loginggg", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String uName = request.getParameter("name");
		String uPass = request.getParameter("password");
		//System.out.println("============Inside checkLogin()========="+uName);
		logger.info("============Inside Login========="+uName);
		logger.info("============Inside Login========="+uPass);
		return new ModelAndView("redirect:/angular/login.jsp");
	}
	
	
	
	@RequestMapping(value = "/pollToCheckSession",method = RequestMethod.GET)
	public @ResponseBody boolean pollToCheckSession(HttpServletRequest request, HttpServletResponse response) throws Exception  
	{		
		HttpSession session=request.getSession(false);
		if(session!=null)
		{
			Employees employees=(Employees)session.getAttribute(Constants.USER);
			if(employees!=null)
			{
				return true;
			}
			else
				return false;
			
		}
		else
		{
			return false;
		}
	}

	
	
	/*--Reference Code---
	 * 
	 * 	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveEmployee(@ModelAttribute("command") EmployeeBean employeeBean, 
			BindingResult result) {
		Employee employee = prepareModel(employeeBean);
		employeeService.addEmployee(employee);
		return new ModelAndView("redirect:/add.html");
	}
		@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView editEmployee(@ModelAttribute("command")  EmployeeBean employeeBean,
			BindingResult result) {
		employeeService.deleteEmployee(prepareModel(employeeBean));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("employee", null);
		model.put("employees",  prepareListofBean(employeeService.listEmployeess()));
		return new ModelAndView("addEmployee", model);
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView deleteEmployee(@ModelAttribute("command")  EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("employee", prepareEmployeeBean(employeeService.getEmployee(employeeBean.getId())));
		model.put("employees",  prepareListofBean(employeeService.listEmployeess()));
		return new ModelAndView("addEmployee", model);
	}
	
	private Employee prepareModel(EmployeeBean employeeBean){
		Employee employee = new Employee();
		employee.setEmpAddress(employeeBean.getAddress());
		employee.setEmpAge(employeeBean.getAge());
		employee.setEmpName(employeeBean.getName());
		employee.setSalary(employeeBean.getSalary());
		employee.setEmpId(employeeBean.getId());
		return employee;
	}
	
	private Employee prepareModel1(JSONObject json){
		Employee employee = new Employee();
		System.out.println("====Name========"+(String) json.get("name"));
		System.out.println("====Age========"+(Integer) json.get("age"));
		System.out.println("====Sal========"+(Long) json.get("salary"));
		System.out.println("====Address========"+(String) json.get("address"));
		

		employee.setEmpAddress((String) json.get("address"));
		employee.setEmpAge((Integer) json.get("age"));
		employee.setEmpName((String) json.get("name"));
		employee.setSalary((Long) json.get("salary"));
		//employee.setId(null);
		return employee;
	}
	private List<EmployeeBean> prepareListofBean(List<Employee> employees){
		List<EmployeeBean> beans = null;
		if(employees != null && !employees.isEmpty()){
			beans = new ArrayList<EmployeeBean>();
			EmployeeBean bean = null;
			for(Employee employee : employees){
				bean = new EmployeeBean();
				bean.setName(employee.getEmpName());
				bean.setId(employee.getEmpId());
				bean.setAddress(employee.getEmpAddress());
				bean.setSalary(employee.getSalary());
				bean.setAge(employee.getEmpAge());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	private EmployeeBean prepareEmployeeBean(Employee employee){
		EmployeeBean bean = new EmployeeBean();
		bean.setAddress(employee.getEmpAddress());
		bean.setAge(employee.getEmpAge());
		bean.setName(employee.getEmpName());
		bean.setSalary(employee.getSalary());
		bean.setId(employee.getEmpId());
		return bean;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody EmployeeBean storeEmployee(@RequestBody String jsonData) throws Exception {
		System.out.println("====jsonMessage========"+jsonData.toString());

		EmployeeBean employeeBean = new ObjectMapper().readValue(jsonData, EmployeeBean.class);
		Employee employee = prepareModel(employeeBean);
		System.out.println("====employee====="+employee.getEmpAge()+"==="+employee.getEmpAddress()+"=="+employee.getEmpName()+"====="+employee.getSalary());
		employeeService.addEmployee(employee);
		//ObjectMapper ob = new ObjectMapper();
       return employeeBean;
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody List<EmployeeBean> getAllEmployee(@RequestBody String jsonData) throws Exception {

		//EmployeeBean employeeBean = new ObjectMapper().readValue(jsonData, EmployeeBean.class);
		List<EmployeeBean> employeeBens=prepareListofBean(employeeService.listEmployeess());
		return employeeBens;
	}
	
	
	@RequestMapping(value="/employees", method = RequestMethod.GET)
	public ModelAndView listEmployees() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("employees",  prepareListofBean(employeeService.listEmployeess()));
		return new ModelAndView("employeesList", model);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addEmployee(@ModelAttribute("command")  EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("employees",  prepareListofBean(employeeService.listEmployeess()));
		return new ModelAndView("addEmployee", model);
	}


@RequestMapping(value = "/save", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody String storeEmployee(@RequestBody String jsonData) throws Exception {
		System.out.println("====jsonMessage========"+jsonData.toString());

		DepartmentMasterBean departmentMasterBean = new ObjectMapper().readValue(jsonData, DepartmentMasterBean.class);
		
		DepartmentMaster departmentMaster = prepareModel(departmentMasterBean);
		
		departmentMasterServiceImpl.addDepartment(departmentMaster);
		//ObjectMapper ob = new ObjectMapper();
      return "Department Added";
	}
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST,headers = {"Content-type=application/json"})		
	 public @ResponseBody List<DepartmentMasterBean> getAllEmployee(@RequestBody String jsonData) throws Exception {

		//EmployeeBean employeeBean = new ObjectMapper().readValue(jsonData, EmployeeBean.class);
		List<DepartmentMasterBean> employeeBens=prepareListofBean(departmentMasterServiceImpl.listDepartments());
		return employeeBens;
	}
	
		
	
	private List<DepartmentMasterBean> prepareListofBean(List<DepartmentMaster> departmentMasters){
		List<DepartmentMasterBean> beans = null;
		if(departmentMasters != null && !departmentMasters.isEmpty()){
			beans = new ArrayList<DepartmentMasterBean>();
			DepartmentMasterBean bean = null;
			for(DepartmentMaster departmentMaster : departmentMasters){
				bean = new DepartmentMasterBean();
				
				bean.setStoreName(departmentMaster.getStorename());
				bean.setDeptShortCut(departmentMaster.getDeptshortcut());				
				bean.setLocation(departmentMaster.getLocation());				
				bean.setPhoneNumber(departmentMaster.getPhonenumber());
				bean.setPhoneNumberExt(departmentMaster.getPhonenumberext());
				bean.setVendorName(departmentMaster.getVendorname());
				bean.setStatus(departmentMaster.getStatus());
				bean.setContactPerson(departmentMaster.getContactperson());
				bean.setId(departmentMaster.getId());
				beans.add(bean);
			}
		}
		return beans;
	}
	
	
		
	private DepartmentMaster prepareModel(DepartmentMasterBean departmentMasterBean){
		DepartmentMaster departmentMaster = new DepartmentMaster();
		
		departmentMaster.setStorename(departmentMasterBean.getStoreName());		
		departmentMaster.setDeptshortcut(departmentMasterBean.getDeptShortCut());
		departmentMaster.setContactperson(departmentMasterBean.getContactPerson());		
		departmentMaster.setLocation(departmentMasterBean.getLocation());
		departmentMaster.setPhonenumber(departmentMasterBean.getPhoneNumber());
		departmentMaster.setPhonenumberext(departmentMasterBean.getPhoneNumberExt());
		departmentMaster.setVendorname(departmentMasterBean.getVendorName());
		departmentMaster.setStatus(departmentMasterBean.getStatus());
		
		//employee.setId(null);
		return departmentMaster;
	}
	

	 */
	

}
