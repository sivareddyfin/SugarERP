package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.persistence.Column;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.finsol.bean.AgreementDetailsBean;
import com.finsol.bean.AgreementSummaryBean;
import com.finsol.bean.AuthorisationAcknowledgementDetailsBean;
import com.finsol.bean.AuthorisationAcknowledgementSummaryBean;
import com.finsol.bean.AuthorisationFormDetailsBean;
import com.finsol.bean.AuthorisationFormSummaryBean;
import com.finsol.bean.BudCuttingDetailsBean;
import com.finsol.bean.BudCuttingSummaryBean;
import com.finsol.bean.BudRemovedCaneBean;
import com.finsol.bean.CaneShiftingBean;
import com.finsol.bean.ChemicalTreatmentDetailsBean;
import com.finsol.bean.ChemicalTreatmentInventoryDetailsBean;
import com.finsol.bean.ChemicalTreatmentSummaryBean;
import com.finsol.bean.DetrashingBean;
import com.finsol.bean.DetrashingDetailsBean;
import com.finsol.bean.DispatchAckDetailsBean;
import com.finsol.bean.DispatchAckSummaryBean;
import com.finsol.bean.DispatchDetailsBean;
import com.finsol.bean.DispatchSummaryBean;
import com.finsol.bean.GradingDetailsBean;
import com.finsol.bean.GradingSummaryBean;
import com.finsol.bean.GreenHouseDetailsBean;
import com.finsol.bean.GreenHouseSummaryBean;
import com.finsol.bean.HardeningDetailsBean;
import com.finsol.bean.HardeningSummaryBean;
import com.finsol.bean.KindIndentDetailsBean;
import com.finsol.bean.KindIndentSummaryBean;
import com.finsol.bean.KindTypeBean;
import com.finsol.bean.MachineBean;
import com.finsol.bean.PlantingUpdateDetailsBean;
import com.finsol.bean.RyotAdvanceBean;
import com.finsol.bean.SeedlingEstimateBean;
import com.finsol.bean.ShiftedCaneDetailsBean;
import com.finsol.bean.SourceOfSeedBean;
import com.finsol.bean.TrayFillingDetailsBean;
import com.finsol.bean.TrayFillingSummaryBean;
import com.finsol.bean.TrayUpdateDetailsBean;
import com.finsol.bean.TrayUpdateSummaryBean;
import com.finsol.bean.TrimmingDetailsBean;
import com.finsol.bean.TrimmingSummaryBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AdvanceDetails;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementDetails;
import com.finsol.model.AuthorisationAcknowledgementDetails;
import com.finsol.model.AuthorisationAcknowledgementSummary;
import com.finsol.model.AuthorisationFormDetails;
import com.finsol.model.AuthorisationFormSummary;
import com.finsol.model.BatchMaster;
import com.finsol.model.BatchServicesMaster;
import com.finsol.model.BudCuttingDetails;
import com.finsol.model.BudCuttingSummary;
import com.finsol.model.BudRemovedCane;
import com.finsol.model.CaneShiftingSummary;
import com.finsol.model.ChemicalTreatmentDetails;
import com.finsol.model.ChemicalTreatmentInventoryDetails;
import com.finsol.model.ChemicalTreatmentSummary;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.DetrashCaneDetails;
import com.finsol.model.DetrashSummary;
import com.finsol.model.DispatchAckDetails;
import com.finsol.model.DispatchAckSummary;
import com.finsol.model.DispatchDetails;
import com.finsol.model.DispatchSummary;
import com.finsol.model.Employees;
import com.finsol.model.GradingDetails;
import com.finsol.model.GradingSummary;
import com.finsol.model.GreenHouseDetails;
import com.finsol.model.GreenHouseSummary;
import com.finsol.model.HardeningDetails;
import com.finsol.model.HardeningSummary;
import com.finsol.model.KindIndentDetails;
import com.finsol.model.KindIndentSummary;
import com.finsol.model.KindType;
import com.finsol.model.LabourContractor;
import com.finsol.model.Machine;
import com.finsol.model.PlantingUpdateDetails;
import com.finsol.model.Ryot;
import com.finsol.model.SeedAdvanceCost;
import com.finsol.model.SeedConsumerDetails;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.SeedlingEstimate;
import com.finsol.model.SeedlingTray;
import com.finsol.model.SeedlingTrayChargeByDistance;
import com.finsol.model.ShiftedCaneDetails;
import com.finsol.model.TrayFillingDetails;
import com.finsol.model.TrayFillingSummary;
import com.finsol.model.TrayReturnDetails;
import com.finsol.model.TrayReturnSummary;
import com.finsol.model.TrayTracker;
import com.finsol.model.TrimmingDetails;
import com.finsol.model.TrimmingSummary;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CommonService;
import com.finsol.service.ReasearchAndDevelopmentService;
import com.finsol.service.RyotService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;

/**saveSeedSource
 * @author Umanath Ch
 * 
 */
@Controller
public class ReasearchAndDevelopmentController
{
	private File fileObj;

	private static final Logger logger = Logger.getLogger(AgricultureHarvestingController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private ReasearchAndDevelopmentService reasearchAndDevelopmentService;

	@Autowired
	private AHFuctionalService ahFuctionalService;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private AHFunctionalController ahFunctionalController;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private CaneAccountingFunctionalController caneAccountingFunctionalController;

	@Autowired
	private HibernateDao hibernatedao;

	@Autowired
	private RyotService ryotService;
	
	@Autowired	
	private LoginController loginController;
	@Autowired	
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	@Autowired	
	private AgricultureHarvestingController agricultureHarvestingController;
	//
	DecimalFormat HMSDecFormatter = new DecimalFormat("0.00");


	@RequestMapping(value = "/saveMachine", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveMachine(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		try
		{
			MachineBean machineBean = new ObjectMapper().readValue(jsonData, MachineBean.class);
			Machine machine = prepareModelforMachine(machineBean);
			entityList.add(machine);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private Machine prepareModelforMachine(MachineBean machineBean)
	{
		Integer maxId = commonService.getMaxID("Machine");
		Machine machine = new Machine();

		String modifyFlag = machineBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			machine.setId(machineBean.getId());
		}
		else
		{
			machine.setId(maxId);
		}
		machine.setMachinecode(machineBean.getMachineCode());
		machine.setMachinenumber(machineBean.getMachineNumber());
		machine.setStatus(machineBean.getStatus());
		return machine;
	}

	@RequestMapping(value = "/getAllMachines", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<MachineBean> getAllMachines(@RequestBody String jsonData) throws Exception
	{
		List<MachineBean> machineBeans = prepareListofBeanForMachines(reasearchAndDevelopmentService.listMachines());
		return machineBeans;
	}

	private List<MachineBean> prepareListofBeanForMachines(List<Machine> Machines)
	{
		List<MachineBean> beans = null;
		if (Machines != null && !Machines.isEmpty())
		{
			beans = new ArrayList<MachineBean>();
			MachineBean bean = null;
			for (Machine machine : Machines)
			{
				bean = new MachineBean();

				bean.setId(machine.getId());
				bean.setMachineCode(machine.getMachinecode());
				bean.setMachineNumber(machine.getMachinenumber());
				bean.setModifyFlag("Yes");
				bean.setStatus(machine.getStatus());

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveKindType", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveKindType(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();

		try
		{
			KindTypeBean kindtypeBean = new ObjectMapper().readValue(jsonData, KindTypeBean.class);
			KindType kindtype = prepareModelforKindType(kindtypeBean);
			entityList.add(kindtype);
			isInsertSuccess = commonService
			        .saveMultipleEntitiesForAgreement(entityList, UpdateList, kindtypeBean.getModifyFlag());

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private KindType prepareModelforKindType(KindTypeBean kindTypeBean)
	{
		Integer maxId = commonService.getMaxID("KindType");
		KindType kindType = new KindType();

		String modifyFlag = kindTypeBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			kindType.setId(kindTypeBean.getId());
		}
		else
		{
			kindType.setId(maxId);
		}
		kindType.setKindType(kindTypeBean.getKindType().toUpperCase());
		kindType.setDescription(kindTypeBean.getDescription());
		kindType.setUom(kindTypeBean.getUom().toUpperCase());
		kindType.setStatus(kindTypeBean.getStatus());
		//added on 24-2-2017
		kindType.setPlantmaxcount(kindTypeBean.getPlantMaxCount());
		kindType.setRatoonmaxcount(kindTypeBean.getRatoonMaxCount());

		kindType.setRoundingTo(kindTypeBean.getRoundingTo());
		kindType.setLimitperAcre(kindTypeBean.getLimitperAcre());
		kindType.setCostofeachItem(kindTypeBean.getCostofeachItem());
		kindType.setAdvance(kindTypeBean.getAdvance());
		return kindType;
	}

	@RequestMapping(value = "/getAllKindType", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<KindTypeBean> getAllKindType(@RequestBody String jsonData) throws Exception
	{
		List<KindTypeBean> kindTypeBeans = prepareListofBeanForKindTypes(reasearchAndDevelopmentService.listKindTypes());
		return kindTypeBeans;
	}

	private List<KindTypeBean> prepareListofBeanForKindTypes(List<KindType> KindTypes)
	{
		List<KindTypeBean> beans = null;
		if (KindTypes != null && !KindTypes.isEmpty())
		{
			beans = new ArrayList<KindTypeBean>();
			KindTypeBean bean = null;
			for (KindType kindType : KindTypes)
			{
				bean = new KindTypeBean();

				bean.setId(kindType.getId());
				bean.setDescription(kindType.getDescription());
				bean.setKindType(kindType.getKindType());
				bean.setModifyFlag("Yes");
				bean.setStatus(kindType.getStatus());
				bean.setUom(kindType.getUom());
			
				//added on 24-2-2017
				bean.setRatoonMaxCount(kindType.getRatoonmaxcount());
				bean.setPlantMaxCount(kindType.getPlantmaxcount());
				bean.setRoundingTo(kindType.getRoundingTo());
				bean.setLimitperAcre(kindType.getLimitperAcre());
				bean.setCostofeachItem(kindType.getCostofeachItem());
				bean.setAdvance(kindType.getAdvance());
				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveBudRemovedCane", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveBudRemovedCane(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			BudRemovedCaneBean budremovedbean = new ObjectMapper().readValue(jsonData, BudRemovedCaneBean.class);
			BudRemovedCane budRemovedCane = prepareModelforBudRemovedCane(budremovedbean);
			entityList.add(budRemovedCane);
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        budremovedbean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private BudRemovedCane prepareModelforBudRemovedCane(BudRemovedCaneBean budremovedcanebean)
	{
		Integer maxId = commonService.getMaxID("BudRemovedCane");
		BudRemovedCane budRemovedCane = new BudRemovedCane();

		String modifyFlag = budremovedcanebean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			budRemovedCane.setId(budremovedcanebean.getId());
		}
		else
		{
			budRemovedCane.setId(maxId);
		}

		String budRemovedDate = budremovedcanebean.getDate();
		budRemovedCane.setBudremoveddate(DateUtils.getSqlDateFromString(budRemovedDate, Constants.GenericDateFormat.DATE_FORMAT));

		budRemovedCane.setDestination(budremovedcanebean.getDestination());
		budRemovedCane.setDrivermobilenumber(budremovedcanebean.getDriverMobileNumber());
		budRemovedCane.setDrivername(budremovedcanebean.getDriverName());
		budRemovedCane.setSentbyvehicle(budremovedcanebean.getSentByVehicle());
		budRemovedCane.setSentto(budremovedcanebean.getSentTo());
		budRemovedCane.setBudremovetime(budremovedcanebean.getTime());
		budRemovedCane.setWeight(budremovedcanebean.getWeight());
		budRemovedCane.setVehiclenumber(budremovedcanebean.getVehicleNumber());
		return budRemovedCane;
	}

	@RequestMapping(value = "/getAllBudRemovedCane", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<BudRemovedCaneBean> getAllBudRemovedCane(@RequestBody String jsonData) throws Exception
	{
		List<BudRemovedCaneBean> budRemovedCaneBeans = prepareListofBeanForBudRemovedCanes(reasearchAndDevelopmentService
		        .listBudRemovedCanes(jsonData));
		return budRemovedCaneBeans;
	}

	private List<BudRemovedCaneBean> prepareListofBeanForBudRemovedCanes(List<BudRemovedCane> BudRemovedCanes)
	{
		List<BudRemovedCaneBean> beans = null;
		if (BudRemovedCanes != null && !BudRemovedCanes.isEmpty())
		{
			beans = new ArrayList<BudRemovedCaneBean>();
			BudRemovedCaneBean bean = null;
			for (BudRemovedCane budRemovedCane : BudRemovedCanes)
			{
				bean = new BudRemovedCaneBean();
				Date budRemovedDate = budRemovedCane.getBudremoveddate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strbudRemovedDate = df.format(budRemovedDate);
				bean.setDate(strbudRemovedDate);
				bean.setVehicleNumber(budRemovedCane.getVehiclenumber());
				bean.setDriverMobileNumber(budRemovedCane.getDrivermobilenumber());
				bean.setModifyFlag("Yes");
				bean.setDriverName(budRemovedCane.getDrivername());
				bean.setSentByVehicle(budRemovedCane.getSentbyvehicle());
				bean.setSentTo(budRemovedCane.getSentto());
				bean.setTime(budRemovedCane.getBudremovetime());
				bean.setWeight(budRemovedCane.getWeight());
				bean.setDestination(budRemovedCane.getDestination());
				bean.setId(budRemovedCane.getId());
				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/getEstimateDates", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getEstimateDates() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getEstimateDates();
		logger.info("getEstimateDates() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				Date estimateDate = (Date) dateMap.get("budremoveddate");

				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strestimateDate = df.format(estimateDate);
				jsonObj.put("addedDates", strestimateDate);
				jArray.add(jsonObj);
			}
		}
		logger.info("getEstimateDates() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getTrayType", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getTrayType() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getTrayType();
		logger.info("getTrayType() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				Integer id = (Integer) dateMap.get("traycode");
				String traytype = (String) dateMap.get("tray");

				jsonObj.put("id", id);
				jsonObj.put("TrayType", traytype);

				jArray.add(jsonObj);
			}
		}
		logger.info("getTrayType() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/saveSeedlingEstimate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveSeedlingEstimate(@RequestBody String jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			SeedlingEstimateBean seedlingEstimateBean = new ObjectMapper().readValue(jsonData, SeedlingEstimateBean.class);
			SeedlingEstimate seedlingestimate = prepareModelforSeedlingEstimate(seedlingEstimateBean);
			entityList.add(seedlingestimate);
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        seedlingEstimateBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private SeedlingEstimate prepareModelforSeedlingEstimate(SeedlingEstimateBean seedlingEstimateBean)
	{
		Integer maxId = commonService.getMaxID("SeedlingEstimate");
		SeedlingEstimate seedlingEstimate = new SeedlingEstimate();
		String modifyFlag = seedlingEstimateBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			seedlingEstimate.setId(seedlingEstimateBean.getId());
		}
		else
		{
			seedlingEstimate.setId(maxId);
		}
		seedlingEstimate.setSeason(seedlingEstimateBean.getSeason());
		seedlingEstimate.setEmployeeid(seedlingEstimateBean.getEmployeeId());
		seedlingEstimate.setVarietycode(seedlingEstimateBean.getVarietyCode());
		seedlingEstimate.setQuantity(seedlingEstimateBean.getQuantity());
		seedlingEstimate.setYearofplanting(seedlingEstimateBean.getYearOfPlanting());
		seedlingEstimate.setMonthid(seedlingEstimateBean.getMonthId());
		seedlingEstimate.setPeriodid(seedlingEstimateBean.getPeriodId());
		return seedlingEstimate;
	}

	@RequestMapping(value = "/getAllSeedingEstimate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<SeedlingEstimateBean> getAllSeedingEstimate(@RequestBody String jsonData) throws Exception
	{
		List<SeedlingEstimateBean> seedingEstimateBeans = prepareListofBeanForSeedingEstimates(reasearchAndDevelopmentService
		        .listSeedlingEstimate());
		return seedingEstimateBeans;
	}

	private List<SeedlingEstimateBean> prepareListofBeanForSeedingEstimates(List<SeedlingEstimate> SeedlingEstimates)
	{
		List<SeedlingEstimateBean> beans = null;
		if (SeedlingEstimates != null && !SeedlingEstimates.isEmpty())
		{
			beans = new ArrayList<SeedlingEstimateBean>();
			SeedlingEstimateBean bean = null;
			for (SeedlingEstimate seedlingEstimate : SeedlingEstimates)
			{
				bean = new SeedlingEstimateBean();
				bean.setEmployeeId(seedlingEstimate.getEmployeeid());
				bean.setSeason(seedlingEstimate.getSeason());
				bean.setVarietyCode(seedlingEstimate.getVarietycode());
				bean.setQuantity(seedlingEstimate.getQuantity());
				bean.setYearOfPlanting(seedlingEstimate.getYearofplanting());
				bean.setMonthId(seedlingEstimate.getMonthid());
				bean.setPeriodId(seedlingEstimate.getPeriodid());
				bean.setModifyFlag("Yes");
				bean.setId(seedlingEstimate.getId());

				beans.add(bean);
			}
		}
		return beans;
	}

	//Added by Umanath Ch on 14-10-2016
	@RequestMapping(value = "/GetMaxSourceOfSeed", method = RequestMethod.POST)
	public @ResponseBody
	String GetMaxSourceOfSeed(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String seedType = (String) jsonData.get("seedType");

		JSONArray dropDownArray = commonService.GetMaxSourceOfSeed(seedType,Season);
		return dropDownArray.toString();
	}

	//Added by Umanath Ch on 12-12-2016
	@RequestMapping(value = "/GetMaxAuthoriseFertiliser", method = RequestMethod.POST)
	public @ResponseBody
	String GetMaxAuthoriseFertiliser(@RequestBody String jsonData) throws Exception
	{
		String maxid = commonService.GetMaxAuthoriseFertiliser(jsonData);
		return maxid;
	}

	//Added by Umanath Ch on 08-11-2016
	@RequestMapping(value = "/GetMaxKindIndent", method = RequestMethod.POST)
	public @ResponseBody
	String GetMaxKindIndent(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		Integer zone = (Integer) jsonData.get("zone");

		JSONArray dropDownArray = commonService.GetMaxKindIndent(Season,zone);
		return dropDownArray.toString();
	}
	
	//Added by Umanath Ch on 10-1-2017
		@RequestMapping(value = "/GetZonesForIndent", method = RequestMethod.POST)
		public @ResponseBody
		String GetZonesForIndent(@RequestBody Integer jsonData) throws Exception
		{
			org.json.JSONObject response = new org.json.JSONObject();
	
			JSONArray dropDownArray = reasearchAndDevelopmentService.GetZonesforIndent(jsonData);
			return dropDownArray.toString();
		}

	@RequestMapping(value = "/GetMaxDispatchNo", method = RequestMethod.POST)
	public @ResponseBody
	String GetMaxDispatchNo(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		JSONArray dropDownArray = commonService.GetMaxDispatchNo(Season);
		return dropDownArray.toString();
	}

	//Added by Umanath Ch on 15-11-2016
	@RequestMapping(value = "/GetLoginUserStatus", method = RequestMethod.POST)
	public @ResponseBody
	String GetLoginUserStatus() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		int Status = GetloginuserStatus();
		String Status1 = Integer.toString(Status);
		logger.info("GetLoginUserStatus() ========Status==========" + Status);
		return Status1;
	}

	public String getLoggedInUserName()
	{
		HttpSession session = request.getSession(false);
		Employees employee = (Employees) session.getAttribute(Constants.USER);
		String userName = employee.getLoginid();
		return userName;
	}

	public String getFileExtension(String fileName)
	{
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	@RequestMapping(value = "/saveSeedSource", method = RequestMethod.POST)
	public @ResponseBody
	String saveSeedSource( MultipartHttpServletRequest req) throws SQLIntegrityConstraintViolationException,Exception
	{
		String  isInsertSuccess = "false";
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();

		Map AccountSummaryMap = new HashMap();
		Map AccountSummaryMap1 = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountGroupMap1 = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		Map AccountSubGroupMap1 = new HashMap();
		List AccountCodeList = new ArrayList();
		List AccountCodeList1 = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountGrpCodeList1 = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList1 = new ArrayList();
		AccountDetails adchg=null;
		AccountGroupDetails agdchg=null;
		AccountSubGroupDetails asgdchg=null;
		AccountDetails adchg1=null;
		AccountGroupDetails agdchg1=null;
		AccountSubGroupDetails asgdchg1=null;
		List adchgls=null;
		List agdchgls=null;
		List asgdchgls=null;
		List adchgls1=null;
		List agdchgls1=null;
		List asgdchgls1=null;
		int AccGroupCode1 = 0;
		int AccSubGroupCode1 = 0;
		boolean isInsertSuccess1=false;
		
		try
		{
			String formdata=(String)req.getParameter("formData");
			SourceOfSeedBean sourceofseedbean = new ObjectMapper().readValue(formdata, SourceOfSeedBean.class);
			//added by naidu on 04-03-2017
		//	String consumerCodeString=(String)req.getParameter("consumerCode");
			Integer type1 = sourceofseedbean.getSeedType();
			String type = Integer.toString(type1);
			JSONArray dropDownArray = commonService.GetMaxSourceOfSeed(type,sourceofseedbean.getSeason());
			JSONObject batchseries1 = (JSONObject) dropDownArray.get(0);
			String batchseries = sourceofseedbean.getBatchNo();//batchseries1.getString("batchNo");

			String ext = null;
			String photoName = null;
			String photoPath = null;
			MultipartFile file=req.getFile("userphoto");

			if (sourceofseedbean.getFileLocation() == null)
			{
				if (file  != null)
				{
				ext = getFileExtension(file.getOriginalFilename());
				photoName = batchseries + "_photo" + "." + ext;
				photoPath = "images/" + photoName;
				sourceofseedbean.setFileLocation(photoPath);
				}
			}
			else
			{
				if (file  != null)
				{
					ext = getFileExtension(file.getOriginalFilename());
					photoName = batchseries + "_photo" + "." + ext;
					photoPath = "images/" + photoName;
					sourceofseedbean.setFileLocation(photoPath);
				}
			}
			String ss[]=sourceofseedbean.getSeason().split("-");
			String supseason=(Integer.parseInt(ss[0])-1)+"-"+ss[0];
			int transactionCode=0;
			int suptransactionCode=0;
			if (sourceofseedbean.getUpdateType()==1) 
			{
				suptransactionCode = sourceofseedbean.getSuptransactionCode();
				transactionCode = sourceofseedbean.getTransactionCode();
			} 
			else 
			{
				transactionCode = commonService.GetMaxTransCode(sourceofseedbean.getSeason());
				suptransactionCode = commonService.GetMaxTransCode(supseason);
			}
			SeedSource seedSource = prepareModelforSourceOfSeed(sourceofseedbean,transactionCode,suptransactionCode);
			String status = sourceofseedbean.getModifyFlag1();
			Byte updatetype = sourceofseedbean.getUpdateType();
			if (!("Yes".equals(status) ))//&& updatetype == 0
			{
				BatchServicesMaster batchServicesMaster = prepareModelforBatchServicesMaster(sourceofseedbean);
				entityList.add(batchServicesMaster);
				seedSource.setBatchno(batchServicesMaster.getBatchseries());
			}
			entityList.add(seedSource);
			
			////////////////////////////seed supply summary/////////////////////
			
				SeedSuppliersSummary seedSuppliersSummary =null;
				List seedSupplieramtsList=null;
				if(sourceofseedbean.getSeedSrCode()!=null)
				seedSupplieramtsList=caneAccountingFunctionalService.getSeedSupplieramts(sourceofseedbean.getSeason(),sourceofseedbean.getSeedSrCode());
				if(seedSupplieramtsList!=null)
				{
					double advpayable=0.0;
					double pendingamt=0.0;
					Object Qryobj3=null;
					int sid=0;
					seedSuppliersSummary=(SeedSuppliersSummary)seedSupplieramtsList.get(0);
					sid=seedSuppliersSummary.getId();
				if(sourceofseedbean.getUpdateType()==1)
				{
					List seedlist=reasearchAndDevelopmentService.listSourceSeeds(sourceofseedbean.getBatchNo(), "", sourceofseedbean.getDates());
					SeedSource ss1=(SeedSource)seedlist.get(0);
					advpayable=seedSuppliersSummary.getAdvancepayable()-ss1.getTotalcost()+sourceofseedbean.getTotalCost();
					pendingamt=seedSuppliersSummary.getPendingamount()-ss1.getTotalcost()+sourceofseedbean.getTotalCost();
				
				}
				else
				{
					advpayable=seedSuppliersSummary.getAdvancepayable()+sourceofseedbean.getTotalCost();
					pendingamt=seedSuppliersSummary.getPendingamount()+sourceofseedbean.getTotalCost();
				}
				String qry = "Update SeedSuppliersSummary set advancepayable="+advpayable+",pendingamount="+pendingamt+" where id="+sid+"";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj3 = qry;
		    	UpdateList.add(Qryobj3);
				}
				else
				{
					seedSuppliersSummary = new SeedSuppliersSummary();
					seedSuppliersSummary.setLoginid(sourceofseedbean.getSeedCertifiedBy());
					seedSuppliersSummary.setAdvancepayable(sourceofseedbean.getTotalCost());
					seedSuppliersSummary.setPaidamount(0.0);
					seedSuppliersSummary.setPendingamount(sourceofseedbean.getTotalCost());
					seedSuppliersSummary.setSeason(sourceofseedbean.getSeason());
					if(sourceofseedbean.getSeedSrCode()==null||sourceofseedbean.getSeedSrCode()=="")
					{
					seedSuppliersSummary.setSeedsuppliercode(sourceofseedbean.getOtherRyot());
					}
					else
					{
					seedSuppliersSummary.setSeedsuppliercode(sourceofseedbean.getSeedSrCode());
					}
					seedSuppliersSummary.setTotalextentsize(0.0);
					seedSuppliersSummary.setAdvancestatus((byte) 0);
					entityList.add(seedSuppliersSummary);
				}

			//cr dr upto previous transaction
			//acc
			
			
			//accounting revert for AccountDetails only
			if(sourceofseedbean.getUpdateType()==1)
			{
				//Consumer  Accounting details status updation
				adchgls= ahFuctionalService.getAccountDetailsForSeed(sourceofseedbean.getSeason(),transactionCode);
				for(int i=0;i<adchgls.size();i++)
				{
				adchg=(AccountDetails)adchgls.get(i);
				adchg.setStatus((byte)0);
				entityList.add(adchg);
				}
				agdchgls=ahFuctionalService.getAccountGroupDetailsForSeed(sourceofseedbean.getSeason(),transactionCode);
				for(int j=0;j<agdchgls.size();j++)
				{
				agdchg=(AccountGroupDetails)agdchgls.get(j);
				agdchg.setStatus((byte)0);
				entityList.add(agdchg);
				}
				asgdchgls=ahFuctionalService.getAccountSubGroupDetailsForSeed(sourceofseedbean.getSeason(), transactionCode);
				for(int k=0;k<asgdchgls.size();k++)
				{
					asgdchg=(AccountSubGroupDetails)asgdchgls.get(k);
					asgdchg.setStatus((byte)0);
					entityList.add(asgdchg);
				}
				//For Supplier Accounting details status updation
				adchgls1= ahFuctionalService.getAccountDetailsForSeed(supseason,suptransactionCode);
				for(int i=0;i<adchgls1.size();i++)
				{
				adchg1=(AccountDetails)adchgls1.get(i);
				adchg1.setStatus((byte)0);
				entityList.add(adchg1);
				}
				agdchgls1=ahFuctionalService.getAccountGroupDetailsForSeed(supseason,suptransactionCode);
				for(int j=0;j<agdchgls1.size();j++)
				{
					agdchg1=(AccountGroupDetails)agdchgls1.get(j);
					agdchg1.setStatus((byte)0);
				entityList.add(agdchg1);
				}
				asgdchgls1=ahFuctionalService.getAccountSubGroupDetailsForSeed(supseason,suptransactionCode);
				for(int k=0;k<asgdchgls1.size();k++)
				{
					asgdchg1=(AccountSubGroupDetails)asgdchgls1.get(k);
					asgdchg1.setStatus((byte)0);
					entityList.add(asgdchg1);
				}
				
			}
			//Ending accounting Addition and revert for AccountDetails Tables For SeedSupplier & consumer

			//  accounting Addition and revert for AccountDetails Tables For SeedSupplier 
			String strQry = "";
			Object Qryobj = "";

			String userId = getLoggedInUserName();
			String AccountCode = seedSource.getSeedsrcode();
			int purpose = seedSource.getPurpose();

			String flag = sourceofseedbean.getModifyFlag();
			String season1 = supseason;

			String ryotcode = seedSource.getSeedsrcode();
			if (ryotcode == null || ryotcode == "null" || ryotcode == "")
			{
				ryotcode = seedSource.getOtherryot();
			}
			//Ryot Account (Seed Supplier) Cr
			
			//cr     5000;
			double Amt = seedSource.getTotalcost();
			String accCode = ryotcode;
			String strglCode = "SEEDPUR";
			String TransactionType = "Cr";
			String journalMemo ="";
			if(sourceofseedbean.getPurpose()==1)
			{
				if(sourceofseedbean.getAggrementNo()!=null)
					journalMemo = "Seed Supply For R&D :"+sourceofseedbean.getAggrementNo();
				else
					journalMemo = "Seed Supply For R&D";
			}
			else
			{
					journalMemo = "Seed Supply For R&D";
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				AccSubGrpCode = 2;
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season1);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", suptransactionCode);
				tempMap1.put("IsSeedSupplier", "Yes");
				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveseedsource()------AccMap" + AccMap);
				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));
			
				Map AccSmry = new HashMap(); 
				AccSmry.put("ACC_CODE", accCode);
				
				boolean ryotAccCodetrue = AccountCodeList1.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList1.add(AccSmry);
					if(sourceofseedbean.getUpdateType()==1)
						{
					List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season1, suptransactionCode,"accSmry",0);
					 AccountSummary a = (AccountSummary) AccMap.get("accSmry");
					if(!ls.isEmpty() &&  ls.size()>0)
					{
					double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
					String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
					
						if(TransactionType.equalsIgnoreCase(balType))
						{
							a.setRunningbalance(runBal+Amt);
							a.setBalancetype(balType);
						}
						else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
						{
							if(runBal>Amt)
							{
								a.setRunningbalance(runBal-Amt);
								a.setBalancetype(balType);
							}
							else
							{
								a.setRunningbalance(Amt-runBal);
								a.setBalancetype(TransactionType);
							}
						}
						else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
						{
							if(runBal>Amt)
							{
								a.setRunningbalance(runBal-Amt);
								a.setBalancetype(balType);
							}
							else
							{
								a.setRunningbalance(Amt-runBal);
								a.setBalancetype(TransactionType);
							}
						}
					}
					else
					{
						a.setRunningbalance(0.0);
						a.setBalancetype("Cr");
					}
						AccountSummaryMap1.put(accCode, a);
						}
					else
					{
					AccountSummaryMap1.put(accCode, AccMap.get("accSmry"));
					}
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap1.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season1.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap1.put(accCode, finalAccSmryMap.get(accCode));
					}
				}
				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList1.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList1.add(AccGrpSmry);
					if(sourceofseedbean.getUpdateType()==1)
						{
						List CodeList = commonService.getAccCodes(accCode);
						if (CodeList != null && CodeList.size() > 0)
						{
							Object[] myResult = (Object[]) CodeList.get(0);
							AccGroupCode1 = (Integer) myResult[0];
							AccSubGroupCode1 = (Integer) myResult[1];
						}

							//ahFuctionalService.BalDetails(AccCode, season);
						List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season1, suptransactionCode,"accGrpSmry",0);
						AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
						if(!ls.isEmpty() &&  ls.size()>0)
						{
						double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
						String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
						
						if(TransactionType.equalsIgnoreCase(balType))
						{
							aa.setRunningbalance(runBal+Amt);
							aa.setBalancetype(balType);
						}
						else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
						{
							if(runBal>Amt)
							{
								aa.setRunningbalance(runBal-Amt);
								aa.setBalancetype(balType);
							}
							else
							{
								aa.setRunningbalance(Amt-runBal);
								aa.setBalancetype(TransactionType);
							}
						}
						else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
						{
							if(runBal>Amt)
							{
								aa.setRunningbalance(runBal-Amt);
								aa.setBalancetype(balType);
							}
							else
							{
								aa.setRunningbalance(Amt-runBal);
								aa.setBalancetype(TransactionType);
							}
						}
						}
						else
						{
							aa.setRunningbalance(0.0);
							aa.setBalancetype("Cr");
						}
						AccountGroupMap1.put(strAccGrpCode, aa);
						}
					else
					{
					AccountGroupMap1.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap1.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season1.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap1.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList1.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList1.add(AccSubGrpSmry);
					if(sourceofseedbean.getUpdateType()==1)
					{
						//ahFuctionalService.BalDetails(AccCode, season);
					List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season1, suptransactionCode,"accGrpSmry",AccSubGroupCode1);
					AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
					if(!ls.isEmpty() &&  ls.size()>0)
					{
					double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
					String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
					
					if(TransactionType.equalsIgnoreCase(balType))
					{
						aaa.setRunningbalance(runBal+Amt);
						aaa.setBalancetype(balType);
					}
					else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
					{
						if(runBal>Amt)
						{
							aaa.setRunningbalance(runBal-Amt);
							aaa.setBalancetype(balType);
						}
						else
						{
							aaa.setRunningbalance(Amt-runBal);
							aaa.setBalancetype(TransactionType);
						}
					}
					else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
					{
						if(runBal>Amt)
						{
							aaa.setRunningbalance(runBal-Amt);
							aaa.setBalancetype(balType);
						}
						else
						{
							aaa.setRunningbalance(Amt-runBal);
							aaa.setBalancetype(TransactionType);
						}
					}
					}
					else
					{
						aaa.setRunningbalance(0.0);
						aaa.setBalancetype("Cr");
					}
					AccountSubGroupMap1.put(strAccSubGrpCode, aaa);
					}
					else
					{
					AccountSubGroupMap1.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
					}
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap1.get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					accsubgrpcode = 2;
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season1.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap1.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
				//Seed Purchases Dr
				ryotcode = seedSource.getSeedsrcode();
				if (ryotcode == null || ryotcode == "null" || ryotcode == "")
				{
					ryotcode = seedSource.getOtherryot();
				}
				Amt = seedSource.getTotalcost();
				accCode = "SEEDPUR";
				strglCode = ryotcode;
				TransactionType = "Dr";
				String journalMemo1 ="";
				if(sourceofseedbean.getPurpose()==1)
				{
					
					if(sourceofseedbean.getAggrementNo()!=null)
						journalMemo1 = "Seed Purchases For R&D :"+sourceofseedbean.getAggrementNo();
					else
						journalMemo1 = "Seed Purchases For R&D";
				}
				else
				{
					journalMemo1 = "Seed Purchases For R&D";
				}
				
				if (accCode != null)
				{
					int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);

					int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
					String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

					Map tempMap1 = new HashMap();
					tempMap1.put("ACCOUNT_CODE", accCode);
					tempMap1.put("AMOUNT", Amt);
					tempMap1.put("JRNL_MEM", journalMemo1);
					tempMap1.put("SEASON", season1);
					tempMap1.put("TRANS_TYPE", TransactionType);
					tempMap1.put("GL_CODE", strglCode);
					tempMap1.put("TRANSACTION_CODE", suptransactionCode);
					Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
					logger.info("saveseedsource()------AccMap" + AccMap);

					entityList.add(AccMap.get("accDtls"));
					entityList.add(AccMap.get("accGrpDtls"));
					entityList.add(AccMap.get("accSubGrpDtls"));

					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", accCode);
					boolean ryotAccCodetrue = AccountCodeList1.contains(AccSmry);
					if (ryotAccCodetrue == false)
					{
						AccountCodeList1.add(AccSmry);
						if(sourceofseedbean.getUpdateType()==1)
						{
							//ahFuctionalService.BalDetails(AccCode, season);
							
						List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season1, suptransactionCode,"accSmry",0);
						AccountSummary a = (AccountSummary) AccMap.get("accSmry");
						if(!ls.isEmpty() &&  ls.size()>0)
						{
						double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
						String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
						
						if(TransactionType.equalsIgnoreCase(balType))
						{
							a.setRunningbalance(runBal+Amt);
							a.setBalancetype(balType);
						}
						else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
						{
							if(runBal>Amt)
							{
								a.setRunningbalance(runBal-Amt);
								a.setBalancetype(balType);
							}
							else
							{
								a.setRunningbalance(Amt-runBal);
								a.setBalancetype(TransactionType);
							}
						}
						else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
						{
							if(runBal>Amt)
							{
								a.setRunningbalance(runBal-Amt);
								a.setBalancetype(balType);
							}
							else
							{
								a.setRunningbalance(Amt-runBal);
								a.setBalancetype(TransactionType);
							}
						}
						}
						else
						{
							a.setRunningbalance(0.0);
							a.setBalancetype("Cr");
						}
						AccountSummaryMap1.put(accCode, a);
						}
					else
					{
					AccountSummaryMap1.put(accCode, AccMap.get("accSmry"));
					}
					}
					else
					{
						AccountSummary accountSummary = (AccountSummary) AccountSummaryMap1.get(accCode);
						String straccCode = accountSummary.getAccountcode();
						if (straccCode.equalsIgnoreCase(accCode) && season1.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", accCode);
							tmpMap.put("KEY", accCode);
							Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
							AccountSummaryMap1.put(accCode, finalAccSmryMap.get(accCode));
						}
					}

					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean loanAccGrpCodetrue = AccountGrpCodeList1.contains(AccGrpSmry);
					if (loanAccGrpCodetrue == false)
					{
						AccountGrpCodeList1.add(AccGrpSmry);
						if(sourceofseedbean.getUpdateType()==1)
						{
							//ahFuctionalService.BalDetails(AccCode, season);
						List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season1, suptransactionCode,"accGrpSmry",0);
						AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
						if(!ls.isEmpty() &&  ls.size()>0)
						{
						double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
						String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
						
						if(TransactionType.equalsIgnoreCase(balType))
						{
							aa.setRunningbalance(runBal+Amt);
							aa.setBalancetype(balType);
						}
						else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
						{
							if(runBal>Amt)
							{
								aa.setRunningbalance(runBal-Amt);
								aa.setBalancetype(balType);
							}
							else
							{
								aa.setRunningbalance(Amt-runBal);
								aa.setBalancetype(TransactionType);
							}
						}
						else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
						{
							if(runBal>Amt)
							{
								aa.setRunningbalance(runBal-Amt);
								aa.setBalancetype(balType);
							}
							else
							{
								aa.setRunningbalance(Amt-runBal);
								aa.setBalancetype(TransactionType);
							}
						}
						}
						else
						{
							aa.setRunningbalance(0.0);
							aa.setBalancetype("Cr");
						}
						AccountGroupMap1.put(strAccGrpCode, aa);
						}
					else
					{
					AccountGroupMap1.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
					}
					else
					{
						AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap1.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if (AccGrpCode == accountGrpCode && season1.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);

							Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap1.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
					boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList1.contains(AccSubGrpSmry);
					if (ryotLoanAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList1.add(AccSubGrpSmry);
						if(sourceofseedbean.getUpdateType()==1)
						{
							//ahFuctionalService.BalDetails(AccCode, season);
						List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season1, suptransactionCode,"accSubGrpSmry",AccSubGroupCode1);
						AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
						if(!ls.isEmpty() &&  ls.size()>0)
						{
						double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
						String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
						 
						if(TransactionType.equalsIgnoreCase(balType))
						{
							aaa.setRunningbalance(runBal+Amt);
							aaa.setBalancetype(balType);
						}
						else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
						{
							if(runBal>Amt)
							{
								aaa.setRunningbalance(runBal-Amt);
								aaa.setBalancetype(balType);
							}
							else
							{
								aaa.setRunningbalance(Amt-runBal);
								aaa.setBalancetype(TransactionType);
							}
						}
						else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
						{
							if(runBal>Amt)
							{
								aaa.setRunningbalance(runBal-Amt);
								aaa.setBalancetype(balType);
							}
							else
							{
								aaa.setRunningbalance(Amt-runBal);
								aaa.setBalancetype(TransactionType);
							}
						}
						}
						else
						{
							aaa.setRunningbalance(0.0);
							aaa.setBalancetype("Cr");
						}
						AccountSubGroupMap1.put(strAccSubGrpCode, aaa);
						}
						else
						{
						AccountSubGroupMap1.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
						}
					}
					else
					{
						AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap1.get(strAccSubGrpCode);
						        
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season1.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
							        .returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap1.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}//end of supplier posting
			
			//For Consumer Posting
			if(sourceofseedbean.getPurpose()==1)
			{
				String season=sourceofseedbean.getSeason();
				if(sourceofseedbean.getUpdateType()==1)
				{
				if(sourceofseedbean.getPrvConsumer().equalsIgnoreCase(sourceofseedbean.getConsumerCode()))
				{
					int id=0;
					double advprvamt=0;
					
					//Advance tables updation for the consumer previous advance
					
						AdvanceDetails advanceDetails =null;
						List lls=ahFuctionalService.getAdvanceDetailsBySeasonForSeed(season, seedSource.getConsumercode(), 3,sourceofseedbean.getTransactionCode());
						advanceDetails=(AdvanceDetails)lls.get(0);
						id=advanceDetails.getId();
						advprvamt=advanceDetails.getAdvanceamount();
						String ryotcode1=seedSource.getConsumercode();
						advanceDetails = new AdvanceDetails();
						
						advanceDetails.setAdvanceamount(Amt);
						advanceDetails.setAdvancecateogrycode(2);
						advanceDetails.setAdvancecode(3);
						String advDate = sourceofseedbean.getDate();
						advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
						advanceDetails.setAdvancesubcategorycode(1);
						advanceDetails.setAgreements(sourceofseedbean.getAggrementNo());
						advanceDetails.setExtentsize(0.0);
						advanceDetails.setIsitseedling((byte)0);
						advanceDetails.setIsSeedlingAdvance((byte)0);
						advanceDetails.setLoginId(loginController.getLoggedInUserName());
						advanceDetails.setRyotcode(seedSource.getConsumercode());
						advanceDetails.setRyotpayable(Amt);
						advanceDetails.setSeason(season);
						advanceDetails.setSeedingsquantity(0);
						String seedsuppliercode = seedSource.getSeedsrcode();
					if (seedsuppliercode == null || seedsuppliercode == "null" || seedsuppliercode == "")
					{
						seedsuppliercode = seedSource.getOtherryot();
					}
						advanceDetails.setSeedsuppliercode(seedsuppliercode);
						advanceDetails.setTotalamount(Amt);
						advanceDetails.setTransactioncode(transactionCode);
						advanceDetails.setId(id);
						entityList.add(advanceDetails);
					
					//AdvanceSummary
					List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(ryotcode1,3,season);
					if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
					{
						AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);

						double advanceAmt = AdvanceSummary1.getAdvanceamount();
						double pendingAmount = AdvanceSummary1.getPendingamount();
						double pendingpayable = AdvanceSummary1.getPendingpayable();
						double totalamount = AdvanceSummary1.getTotalamount();
						
						advanceAmt = Amt+advanceAmt-advprvamt;
						pendingAmount = Amt+pendingAmount-advprvamt;
						pendingpayable = Amt+pendingpayable-advprvamt;
						totalamount = Amt+totalamount-advprvamt;
						
						String qry = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
						logger.info("saveRyotAdvances()========qry==========" + qry);
						Qryobj = qry;
				    	UpdateList.add(Qryobj);
					}
					List advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotcode1,3,season);
					if(advPrincipleList != null && advPrincipleList.size()>0)
					{
						AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);

						double principle = advanceprincipleamounts1.getPrinciple();
						logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
						double finalPrinciple =0.0;
						finalPrinciple = Amt+principle-advprvamt;
						String qry = "Update AdvancePrincipleAmounts set principle="+finalPrinciple+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
						logger.info("saveRyotAdvances()========qry==========" + qry);
						Qryobj = qry;
				    	UpdateList.add(Qryobj);
					}
					
					//Ryot Account (seed taken ryot) Dr
					
					Amt = seedSource.getTotalcost();
					accCode = seedSource.getConsumercode();
					strglCode = "2311";
					TransactionType = "Dr";
					if(sourceofseedbean.getAggrementNo()!=null)
						journalMemo = "Seed  Taken From R&D :"+sourceofseedbean.getAggrementNo();
					else
						journalMemo = "Seed  Taken From R&D";
					
					if (accCode != null)
					{
						int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						AccSubGrpCode = 9;
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
						
						Map tempMap1 = new HashMap();
						tempMap1.put("ACCOUNT_CODE", accCode);
						tempMap1.put("AMOUNT", Amt);
						tempMap1.put("JRNL_MEM", journalMemo);
						tempMap1.put("SEASON", season);
						tempMap1.put("TRANS_TYPE", TransactionType);
						tempMap1.put("GL_CODE", strglCode);
						tempMap1.put("TRANSACTION_CODE", transactionCode);
						tempMap1.put("IsCaneSupplier", "Yes");
						Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
						logger.info("saveseedsource()------AccMap" + AccMap);
						entityList.add(AccMap.get("accDtls"));
						entityList.add(AccMap.get("accGrpDtls"));
						entityList.add(AccMap.get("accSubGrpDtls"));

						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
						if (ryotAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
								//ahFuctionalService.BalDetails(AccCode, season);
							
							List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season, transactionCode,"accSmry",0);
							AccountSummary a = (AccountSummary) AccMap.get("accSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							
							if(TransactionType.equalsIgnoreCase(balType))
							{
								a.setRunningbalance(runBal+Amt);
								a.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									a.setRunningbalance(runBal-Amt);
									a.setBalancetype(balType);
								}
								else
								{
									a.setRunningbalance(Amt-runBal);
									a.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									a.setRunningbalance(runBal-Amt);
									a.setBalancetype(balType);
								}
								else
								{
									a.setRunningbalance(Amt-runBal);
									a.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								a.setRunningbalance(0.0);
								a.setBalancetype("Cr");	
							}
							AccountSummaryMap.put(accCode, a);
						}
						else
						{
							AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
							String straccCode = accountSummary.getAccountcode();
							if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
							}
						}

						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (loanAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							List CodeList = commonService.getAccCodes(accCode);
							if (CodeList != null && CodeList.size() > 0)
							{
								Object[] myResult = (Object[]) CodeList.get(0);
								AccGroupCode1 = (Integer) myResult[0];
								AccSubGroupCode1 = (Integer) myResult[1];
							}

							List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accGrpSmry",0);
							AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							if(TransactionType.equalsIgnoreCase(balType))
							{
								aa.setRunningbalance(runBal+Amt);
								aa.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									aa.setRunningbalance(runBal-Amt);
									aa.setBalancetype(balType);
								}
								else
								{
									aa.setRunningbalance(Amt-runBal);
									aa.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									aa.setRunningbalance(runBal-Amt);
									aa.setBalancetype(balType);
								}
								else
								{
									aa.setRunningbalance(Amt-runBal);
									aa.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								aa.setRunningbalance(0.0);
								aa.setBalancetype("Cr");
							}
							AccountGroupMap.put(strAccGrpCode, aa);
						}
						else
						{
							AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);

								Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}

						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if (ryotLoanAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accSubGrpSmry",AccSubGroupCode1);
							AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							if(TransactionType.equalsIgnoreCase(balType))
							{
								aaa.setRunningbalance(runBal+Amt);
								aaa.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									aaa.setRunningbalance(runBal-Amt);
									aaa.setBalancetype(balType);
								}
								else
								{
									aaa.setRunningbalance(Amt-runBal);
									aaa.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									aaa.setRunningbalance(runBal-Amt);
									aaa.setBalancetype(balType);
								}
								else
								{
									aaa.setRunningbalance(Amt-runBal);
									aaa.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								aaa.setRunningbalance(0.0);
								aaa.setBalancetype("Cr");
							}
							AccountSubGroupMap.put(strAccSubGrpCode, aaa);
							
						}
						else
						{
							AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							accsubgrpcode = 9;
							String ses = accountSubGroupSummary.getSeason();
							if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
								        .returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}

					/////////for seed advance posting in accounts table
					//Advance Account (Here this is towards Seed Advance) Dr
					Amt = seedSource.getTotalcost();
					accCode = "2311";
					strglCode = seedSource.getConsumercode();
					TransactionType = "Dr";
					if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed Advance From R&D :"+sourceofseedbean.getAggrementNo();
					else
							journalMemo = "Seed Advance From R&D :";
					
				
					if (accCode != null)
					{
						int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

						Map tempMap1 = new HashMap();
						tempMap1.put("ACCOUNT_CODE", accCode);
						tempMap1.put("AMOUNT", Amt);
						tempMap1.put("JRNL_MEM", journalMemo);
						tempMap1.put("SEASON", season);
						tempMap1.put("TRANS_TYPE", TransactionType);
						tempMap1.put("GL_CODE", strglCode);
						tempMap1.put("TRANSACTION_CODE", transactionCode);
						Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
						logger.info("saveseedsource()------AccMap" + AccMap);
						entityList.add(AccMap.get("accDtls"));
						entityList.add(AccMap.get("accGrpDtls"));
						entityList.add(AccMap.get("accSubGrpDtls"));
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
						if (ryotAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season, transactionCode,"accSmry",0);
							AccountSummary a = (AccountSummary) AccMap.get("accSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							if(TransactionType.equalsIgnoreCase(balType))
							{
								a.setRunningbalance(runBal+Amt);
								a.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									a.setRunningbalance(runBal-Amt);
									a.setBalancetype(balType);
								}
								else
								{
									a.setRunningbalance(Amt-runBal);
									a.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									a.setRunningbalance(runBal-Amt);
									a.setBalancetype(balType);
								}
								else
								{
									a.setRunningbalance(Amt-runBal);
									a.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								a.setRunningbalance(0.0);
								a.setBalancetype("Cr");
							}
							AccountSummaryMap.put(accCode, a);
						}
						else
						{
							AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
							String straccCode = accountSummary.getAccountcode();
							if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
							}
						}

						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (loanAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							List CodeList = commonService.getAccCodes(accCode);
							if (CodeList != null && CodeList.size() > 0)
							{
								Object[] myResult = (Object[]) CodeList.get(0);
								AccGroupCode1 = (Integer) myResult[0];
								AccSubGroupCode1 = (Integer) myResult[1];
							}

							List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accGrpSmry",0);
							AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							if(TransactionType.equalsIgnoreCase(balType))
							{
								aa.setRunningbalance(runBal+Amt);
								aa.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									aa.setRunningbalance(runBal-Amt);
									aa.setBalancetype(balType);
								}
								else
								{
									aa.setRunningbalance(Amt-runBal);
									aa.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									aa.setRunningbalance(runBal-Amt);
									aa.setBalancetype(balType);
								}
								else
								{
									aa.setRunningbalance(Amt-runBal);
									aa.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								aa.setRunningbalance(0.0);
								aa.setBalancetype("Cr");
							}
							AccountGroupMap.put(strAccGrpCode, aa);
						}
						else
						{
							AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);

								Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}

						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if (ryotLoanAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accSubGrpSmry",AccSubGroupCode1);
							AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							if(TransactionType.equalsIgnoreCase(balType))
							{
								aaa.setRunningbalance(runBal+Amt);
								aaa.setBalancetype(balType);
							}
							else if(TransactionType.equalsIgnoreCase("Cr") && balType.equalsIgnoreCase("Dr"))
							{
								if(runBal>Amt)
								{
									aaa.setRunningbalance(runBal-Amt);
									aaa.setBalancetype(balType);
								}
								else
								{
									aaa.setRunningbalance(Amt-runBal);
									aaa.setBalancetype(TransactionType);
								}
							}
							else if(TransactionType.equalsIgnoreCase("Dr") && balType.equalsIgnoreCase("Cr"))
							{
								if(runBal>Amt)
								{
									aaa.setRunningbalance(runBal-Amt);
									aaa.setBalancetype(balType);
								}
								else
								{
									aaa.setRunningbalance(Amt-runBal);
									aaa.setBalancetype(TransactionType);
								}
							}
							}
							else
							{
								aaa.setRunningbalance(0.0);
								aaa.setBalancetype("Cr");
							}
							AccountSubGroupMap.put(strAccSubGrpCode, aaa);
						}
						else
						{
							AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
				else
					{
						List seedlist=reasearchAndDevelopmentService.listSourceSeeds(sourceofseedbean.getBatchNo(), "", sourceofseedbean.getDates());
						SeedSource ss1=(SeedSource)seedlist.get(0);
						double supPrvCost=ss1.getTotalcost();
						
						//Adding Advance table for New Consumer and delete  for  prvRyot
						String ryotcode1=seedSource.getConsumercode();
						String prvRyotCode=sourceofseedbean.getPrvConsumer();
						AdvanceDetails advanceDetails = new AdvanceDetails();
						
						advanceDetails.setAdvanceamount(Amt);
						advanceDetails.setAdvancecateogrycode(2);
						advanceDetails.setAdvancecode(3);
						String advDate = sourceofseedbean.getDate();
						advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
						advanceDetails.setAdvancesubcategorycode(1);
						advanceDetails.setAgreements("NA");
						advanceDetails.setExtentsize(0.0);
						advanceDetails.setIsitseedling((byte)0);
						advanceDetails.setIsSeedlingAdvance((byte)0);
						advanceDetails.setLoginId(loginController.getLoggedInUserName());
						advanceDetails.setRyotcode(seedSource.getConsumercode());
						advanceDetails.setRyotpayable(Amt);
						advanceDetails.setSeason(season);
						advanceDetails.setSeedingsquantity(0);
						String seedsuppliercode = seedSource.getSeedsrcode();
						if (seedsuppliercode == null || seedsuppliercode == "null" || seedsuppliercode == "")
						{
							seedsuppliercode = seedSource.getOtherryot();
						}
						advanceDetails.setSeedsuppliercode(seedsuppliercode);
						advanceDetails.setTotalamount(Amt);
						advanceDetails.setTransactioncode(transactionCode);
						entityList.add(advanceDetails);
						//For Previous Ryot Advance Deletion
						String qry = "delete from AdvanceDetails where season ='" + season+"' and ryotcode='" + prvRyotCode+"' and advancecode=3 and transactioncode="+sourceofseedbean.getTransactionCode()+"";
				    	Qryobj = qry;
				    	UpdateList.add(Qryobj);
						
				    	//AdvanceSummary For Previous Ryot deduction
				    	List AdvanceSummaryList1 = ahFuctionalService.getAdvanceSummaryList(prvRyotCode,3,season);
				    	if(AdvanceSummaryList1 != null && AdvanceSummaryList1.size()>0)
						{
							AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList1.get(0);
	
							double advanceAmt = AdvanceSummary1.getAdvanceamount();
							double pendingAmount = AdvanceSummary1.getPendingamount();
							double pendingpayable = AdvanceSummary1.getPendingpayable();
							double totalamount = AdvanceSummary1.getTotalamount();
							
							advanceAmt = advanceAmt-supPrvCost;
							pendingAmount =pendingAmount-supPrvCost;
							pendingpayable =pendingpayable-supPrvCost;
							totalamount =totalamount-supPrvCost;
							
							String qry1 = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode=3 and ryotcode='"+prvRyotCode+"' and season='"+season+"'";
							logger.info("saveRyotAdvances()========qry==========" + qry);
							Qryobj = qry1;
					    	UpdateList.add(Qryobj);
						}
				    	//AdvanceSummary For New Ryot addition
				    	List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(ryotcode1,3,season);
						if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
						{
							AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);
	
							double advanceAmt = AdvanceSummary1.getAdvanceamount();
							double pendingAmount = AdvanceSummary1.getPendingamount();
							double pendingpayable = AdvanceSummary1.getPendingpayable();
							double totalamount = AdvanceSummary1.getTotalamount();
							
							advanceAmt = Amt+advanceAmt;
							pendingAmount = Amt+pendingAmount;
							pendingpayable = Amt+pendingpayable;
							totalamount = Amt+totalamount;
							
							String qry1 = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
							logger.info("saveRyotAdvances()========qry==========" + qry);
							Qryobj = qry1;
					    	UpdateList.add(Qryobj);
						}
						else
						{
							AdvanceSummary advanceSummary = new AdvanceSummary();
							List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotcode1);
							Ryot ryot=ryots.get(0);
							
							advanceSummary.setAdvanceamount(Amt);
							advanceSummary.setAdvancecode(3);
							advanceSummary.setAdvancestatus((byte) 0);
							advanceSummary.setInterestamount(0.0);
							advanceSummary.setLoginid(loginController.getLoggedInUserName());
							advanceSummary.setPaidamount(0.0);
							advanceSummary.setPendingamount(Amt);
							advanceSummary.setPendingpayable(Amt);
							advanceSummary.setRecoveredamount(0.0);
							advanceSummary.setRyotcode(ryotcode1);
							advanceSummary.setRyotcodeSeq(ryot.getRyotcodesequence());
							advanceSummary.setSeason(season);
							advanceSummary.setTotalamount(Amt);
							advanceSummary.setTransactioncode(transactionCode);
							advanceSummary.setVillagecode(ryot.getVillagecode());
							entityList.add(advanceSummary);
						}
						//AdvancePrincipleAmounts For Previous Ryot deduction
						List advPrincipleList1 = ahFuctionalService.getAdvancePrinciples(prvRyotCode,3,season);
						if(advPrincipleList1 != null && advPrincipleList1.size()>0)
						{
							AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList1.get(0);
	
							double principle = advanceprincipleamounts1.getPrinciple();
							logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
							double finalPrinciple = principle-supPrvCost;
							String qry2 = "Update AdvancePrincipleAmounts set principle="+finalPrinciple+" where advancecode=3 and ryotcode='"+prvRyotCode+"' and season='"+season+"'";
							logger.info("saveRyotAdvances()========qry==========" + qry2);
							Qryobj = qry2;
					    	UpdateList.add(Qryobj);
						}
						//AdvancePrincipleAmounts For New Ryot addition
						List advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotcode1,3,season);
						if(advPrincipleList != null && advPrincipleList.size()>0)
						{
							AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);
	
							double principle = advanceprincipleamounts1.getPrinciple();
							logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
							double finalPrinciple = Amt+principle;
							String qry2 = "Update AdvancePrincipleAmounts set principle="+finalPrinciple+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
							logger.info("saveRyotAdvances()========qry==========" + qry2);
							Qryobj = qry2;
					    	UpdateList.add(Qryobj);
						}
						else
						{
							AdvancePrincipleAmounts advancePrincipleAmounts = new AdvancePrincipleAmounts();
							advancePrincipleAmounts.setAdvancecode(3);
							advancePrincipleAmounts.setPrinciple(Amt);
							String principleDt = sourceofseedbean.getDate();
							advancePrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(principleDt,Constants.GenericDateFormat.DATE_FORMAT));
							advancePrincipleAmounts.setRyotcode(ryotcode1);
							advancePrincipleAmounts.setSeason(season);
							entityList.add(advancePrincipleAmounts);
						}
						// Ending Advance Accounting Tables  For New Ryot addition and previous ryot Deletion
						
						//For New Ryot addition  Account (seed taken ryot) Dr
						
						Amt = seedSource.getTotalcost();
						accCode = seedSource.getConsumercode();
						strglCode = "2311";
						TransactionType = "Dr";
						
						if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed  Taken From R&D :"+sourceofseedbean.getAggrementNo();
						else
							journalMemo = "Seed  Taken From R&D :";
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
	
							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							AccSubGrpCode = 9;
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
							
							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							tempMap1.put("IsCaneSupplier", "Yes");
							Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);
							entityList.add(AccMap.get("accDtls"));
							entityList.add(AccMap.get("accGrpDtls"));
							entityList.add(AccMap.get("accSubGrpDtls"));
	
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}
	
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
	
									Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
	
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								accsubgrpcode = 9;
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
									        .returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}
						
						/////////for seed advance posting in accounts table
						//Advance Account (Here this is towards Seed Advance) Dr
						Amt = seedSource.getTotalcost();
						accCode = "2311";
						strglCode = seedSource.getConsumercode();
						TransactionType = "Dr";
						
						if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed Advance From R&D :"+sourceofseedbean.getAggrementNo();
						else
							journalMemo = "Seed Advance From R&D :";
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
	
							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);
							entityList.add(AccMap.get("accDtls"));
							entityList.add(AccMap.get("accGrpDtls"));
							entityList.add(AccMap.get("accSubGrpDtls"));
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}
	
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
	
									Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
	
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}
						//Previous Ryot Accounting Deduction
						Amt = supPrvCost;
						accCode = sourceofseedbean.getPrvConsumer();
						strglCode = "2311";
						//For Deduction we are adding Cr in place of Dr
						TransactionType = "Cr";
						if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed Taken From R&D :"+sourceofseedbean.getAggrementNo();
						else
							journalMemo = "Seed Taken From R&D :";
						
						
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
	
							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							AccSubGrpCode = 9;
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
							
							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							tempMap1.put("IsCaneSupplier", "Yes");
							Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);
							//For Accounting tables status change For Previous ryot
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
							AccountCodeList.add(AccSmry);
							List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season, transactionCode,"accSmry",0);
							AccountSummary a = (AccountSummary) AccMap.get("accSmry");
							if(!ls.isEmpty() &&  ls.size()>0)
							{
							double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
							String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
							 
							a.setRunningbalance(runBal);
							a.setBalancetype(balType);
							}
							else
							{
								a.setRunningbalance(0.0);
								a.setBalancetype("Cr");
							}
							AccountSummaryMap.put(accCode, a);
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}
	
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								List CodeList = commonService.getAccCodes(accCode);
								if (CodeList != null && CodeList.size() > 0)
								{
									Object[] myResult = (Object[]) CodeList.get(0);
									AccGroupCode1 = (Integer) myResult[0];
									AccSubGroupCode1 = (Integer) myResult[1];
								}
	
								List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accGrpSmry",0);
								AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
								if(!ls.isEmpty() &&  ls.size()>0)
								{
								double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
								String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
								 
								aa.setRunningbalance(runBal);
								aa.setBalancetype(balType);
								}
								else
								{
									aa.setRunningbalance(0.0);
									aa.setBalancetype("Cr");
								}
								AccountGroupMap.put(strAccGrpCode, aa);
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
	
									Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
	
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accSubGrpSmry",AccSubGroupCode1);
									AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
									if(!ls.isEmpty() &&  ls.size()>0)
									{
									double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
									String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
									 
									aaa.setRunningbalance(runBal);
									aaa.setBalancetype(balType);
									}
									else
									{
										aaa.setRunningbalance(0.0);
										aaa.setBalancetype("Cr");
									}
									AccountSubGroupMap.put(strAccSubGrpCode, aaa);
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								accsubgrpcode = 9;
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}//end of prvryot accounting deduction
						
						// for seed advance posting in accounts table deduction for PrvRyotAccounting
						Amt = supPrvCost;
						accCode = "2311";
						strglCode = sourceofseedbean.getPrvConsumer();
						//For Reverse operation For Previous Consumer
						TransactionType = "Cr";
						if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed Advance From R&D :"+sourceofseedbean.getAggrementNo();
						else
							journalMemo = "Seed Advance From R&D :";
						
						if (accCode != null)
						{
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
	
							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
							Map tempMap1 = new HashMap();
							tempMap1.put("ACCOUNT_CODE", accCode);
							tempMap1.put("AMOUNT", Amt);
							tempMap1.put("JRNL_MEM", journalMemo);
							tempMap1.put("SEASON", season);
							tempMap1.put("TRANS_TYPE", TransactionType);
							tempMap1.put("GL_CODE", strglCode);
							tempMap1.put("TRANSACTION_CODE", transactionCode);
							Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
							logger.info("saveseedsource()------AccMap" + AccMap);
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accCode);
							boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
							if (ryotAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								List ls=ahFuctionalService.getRunningBalForSeedAccount(accCode, season, transactionCode,"accSmry",0);
								AccountSummary a = (AccountSummary) AccMap.get("accSmry");
								if(!ls.isEmpty() &&  ls.size()>0)
								{
								double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
								String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
								 
								a.setRunningbalance(runBal);
								a.setBalancetype(balType);
								}
								else
								{
									a.setRunningbalance(0.0);
									a.setBalancetype("Cr");
								}
								AccountSummaryMap.put(accCode, a);
							}
							else
							{
								AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
								String straccCode = accountSummary.getAccountcode();
								if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
								}
							}
	
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (loanAccGrpCodetrue == false)
							{
									AccountGrpCodeList.add(AccGrpSmry);
									List CodeList = commonService.getAccCodes(accCode);
									if (CodeList != null && CodeList.size() > 0)
									{
										Object[] myResult = (Object[]) CodeList.get(0);
										AccGroupCode1 = (Integer) myResult[0];
										AccSubGroupCode1 = (Integer) myResult[1];
									}
	
									List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accGrpSmry",0);
									AccountGroupSummary aa = (AccountGroupSummary) AccMap.get("accGrpSmry");
									if(!ls.isEmpty() &&  ls.size()>0)
									{
									double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
									String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
									 
									aa.setRunningbalance(runBal);
									aa.setBalancetype(balType);
									}
									else
									{
										aa.setRunningbalance(0.0);
										aa.setBalancetype("Cr");
									}
									AccountGroupMap.put(strAccGrpCode, aa);
							}
							else
							{
								AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
	
									Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
	
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if (ryotLoanAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								List ls=ahFuctionalService.getRunningBalForSeedAccount(String.valueOf(AccGroupCode1), season, transactionCode,"accSubGrpSmry",AccSubGroupCode1);
								AccountSubGroupSummary aaa = (AccountSubGroupSummary) AccMap.get("accSubGrpSmry");
								if(!ls.isEmpty() &&  ls.size()>0)
								{
								double runBal=(Double)((Map<Object,Object>)ls.get(0)).get("runBal");
								String balType=(String)((Map<Object,Object>)ls.get(0)).get("balType");
								 
								aaa.setRunningbalance(runBal);
								aaa.setBalancetype(balType);
								}
								else
								{
									aaa.setRunningbalance(0.0);
									aaa.setBalancetype("Cr");
								}
								AccountSubGroupMap.put(strAccSubGrpCode, aaa);
							}
							else
							{
								AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								        
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
	
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}
						
					}//end of changing New Consumer Account
				}
				else
				{
						Amt=seedSource.getTotalcost();
						String ryotcode1=seedSource.getConsumercode();
						AdvanceDetails advanceDetails = new AdvanceDetails();
						
						advanceDetails.setAdvanceamount(Amt);
						advanceDetails.setAdvancecateogrycode(2);
						advanceDetails.setAdvancecode(3);
						String advDate = sourceofseedbean.getDate();
						advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
						advanceDetails.setAdvancesubcategorycode(1);
						advanceDetails.setAgreements("NA");
						advanceDetails.setExtentsize(0.0);
						advanceDetails.setIsitseedling((byte)0);
						advanceDetails.setIsSeedlingAdvance((byte)0);
						advanceDetails.setLoginId(loginController.getLoggedInUserName());
						advanceDetails.setRyotcode(seedSource.getConsumercode());
						advanceDetails.setRyotpayable(Amt);
						advanceDetails.setSeason(season);
						advanceDetails.setSeedingsquantity(0);
						String seedsuppliercode = seedSource.getSeedsrcode();
					if (seedsuppliercode == null || seedsuppliercode == "null" || seedsuppliercode == "")
					{
						seedsuppliercode = seedSource.getOtherryot();
					}
						advanceDetails.setSeedsuppliercode(seedsuppliercode);
						advanceDetails.setTotalamount(Amt);
						advanceDetails.setTransactioncode(transactionCode);
						entityList.add(advanceDetails);
					
					//AdvanceSummary
					List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(ryotcode1,3,season);
					if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
					{
						AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);
						double advanceAmt = AdvanceSummary1.getAdvanceamount();
						double pendingAmount = AdvanceSummary1.getPendingamount();
						double pendingpayable = AdvanceSummary1.getPendingpayable();
						double totalamount = AdvanceSummary1.getTotalamount();
						advanceAmt = Amt+advanceAmt;
						pendingAmount = Amt+pendingAmount;
						pendingpayable = Amt+pendingpayable;
						totalamount = Amt+totalamount;
						String qry = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
						logger.info("saveRyotAdvances()========qry==========" + qry);
						Qryobj = qry;
				    	UpdateList.add(Qryobj);
					}
					else
					{
						AdvanceSummary advanceSummary = new AdvanceSummary();
						List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotcode1);
						Ryot ryot=ryots.get(0);
						
						advanceSummary.setAdvanceamount(Amt);
						advanceSummary.setAdvancecode(3);
						advanceSummary.setAdvancestatus((byte) 0);
						advanceSummary.setInterestamount(0.0);
						advanceSummary.setLoginid(loginController.getLoggedInUserName());
						advanceSummary.setPaidamount(0.0);
						advanceSummary.setPendingamount(Amt);
						advanceSummary.setPendingpayable(Amt);
						advanceSummary.setRecoveredamount(0.0);
						advanceSummary.setRyotcode(ryotcode1);
						advanceSummary.setRyotcodeSeq(ryot.getRyotcodesequence());
						advanceSummary.setSeason(season);
						advanceSummary.setTotalamount(Amt);
						advanceSummary.setTransactioncode(transactionCode);
						advanceSummary.setVillagecode(ryot.getVillagecode());
						entityList.add(advanceSummary);
					}
					
					List advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotcode1,3,season);
					if(advPrincipleList != null && advPrincipleList.size()>0)
					{
						AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);

						double principle = advanceprincipleamounts1.getPrinciple();
						logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
						double finalPrinciple =0.0;
						finalPrinciple = Amt+principle;
						String qry = "Update AdvancePrincipleAmounts set principle="+finalPrinciple+" where advancecode=3 and ryotcode='"+ryotcode1+"' and season='"+season+"'";
						logger.info("saveRyotAdvances()========qry==========" + qry);
						Qryobj = qry;
				    	UpdateList.add(Qryobj);
					}
					else
					{
						AdvancePrincipleAmounts advancePrincipleAmounts = new AdvancePrincipleAmounts();
						advancePrincipleAmounts.setAdvancecode(3);
						advancePrincipleAmounts.setPrinciple(Amt);
						String principleDt = sourceofseedbean.getDate();
						advancePrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(principleDt,Constants.GenericDateFormat.DATE_FORMAT));
						advancePrincipleAmounts.setRyotcode(ryotcode1);
						advancePrincipleAmounts.setSeason(season);
						entityList.add(advancePrincipleAmounts);
					}

					Amt = seedSource.getTotalcost();
					accCode = seedSource.getConsumercode();
					strglCode = "2311";
					TransactionType = "Dr";
						if(sourceofseedbean.getAggrementNo()!=null)
							journalMemo = "Seed  Taken From R&D"+sourceofseedbean.getAggrementNo();
						else
							journalMemo = "Seed  Taken From R&D";
					if (accCode != null)
					{
						int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						AccSubGrpCode = 9;
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
						
						Map tempMap1 = new HashMap();
						tempMap1.put("ACCOUNT_CODE", accCode);
						tempMap1.put("AMOUNT", Amt);
						tempMap1.put("JRNL_MEM", journalMemo);
						tempMap1.put("SEASON", season);
						tempMap1.put("TRANS_TYPE", TransactionType);
						tempMap1.put("GL_CODE", strglCode);
						tempMap1.put("TRANSACTION_CODE", transactionCode);
						tempMap1.put("IsCaneSupplier", "Yes");
						Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
						logger.info("saveseedsource()------AccMap" + AccMap);
						entityList.add(AccMap.get("accDtls"));
						entityList.add(AccMap.get("accGrpDtls"));
						entityList.add(AccMap.get("accSubGrpDtls"));

						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
						if (ryotAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
						}
						else
						{
							AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
							String straccCode = accountSummary.getAccountcode();
							if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
							}
						}

						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (loanAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
						}
						else
						{
							AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);

								Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}

						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if (ryotLoanAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
						}
						else
						{
							AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							accsubgrpcode = 9;
							String ses = accountSubGroupSummary.getSeason();
							if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
								        .returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
					
					/////////for seed advance posting in accounts table
					//Advance Account (Here this is towards Seed Advance) Dr
					Amt = seedSource.getTotalcost();
					accCode = "2311";
					strglCode = seedSource.getConsumercode();
					TransactionType = "Dr";
					if(sourceofseedbean.getAggrementNo()!=null)
						journalMemo = "Seed Advance From R&D"+sourceofseedbean.getAggrementNo();
					else
						journalMemo = "Seed Advance From R&D";
					if (accCode != null)
					{
						int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

						Map tempMap1 = new HashMap();
						tempMap1.put("ACCOUNT_CODE", accCode);
						tempMap1.put("AMOUNT", Amt);
						tempMap1.put("JRNL_MEM", journalMemo);
						tempMap1.put("SEASON", season);
						tempMap1.put("TRANS_TYPE", TransactionType);
						tempMap1.put("GL_CODE", strglCode);
						tempMap1.put("TRANSACTION_CODE", transactionCode);
						Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
						logger.info("saveseedsource()------AccMap" + AccMap);
						entityList.add(AccMap.get("accDtls"));
						entityList.add(AccMap.get("accGrpDtls"));
						entityList.add(AccMap.get("accSubGrpDtls"));
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
						if (ryotAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
						}
						else
						{
							AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
							String straccCode = accountSummary.getAccountcode();
							if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
							}
						}

						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (loanAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
						}
						else
						{
							AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);

								Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}

						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if (ryotLoanAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
						}
						else
						{
							AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();

								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
			}
			////for seed advance posting in advance table
			
			
			//for Seed Consumer Accounting Summary Posting
			Iterator sentries = AccountSummaryMap1.entrySet().iterator();
			while (sentries.hasNext())
			{
				Map.Entry entry = (Map.Entry) sentries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator sentries1 = AccountGroupMap1.entrySet().iterator();
			while (sentries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) sentries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator sentries2 = AccountSubGroupMap1.entrySet().iterator();
			while (sentries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) sentries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			
			//for Seed Consumer Accounting Summary Posting
			
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				runnbal=agricultureHarvestingController.round(runnbal);
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
		

			isInsertSuccess1 = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        sourceofseedbean.getModifyFlag());
			//isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList) ;
			if (isInsertSuccess1)
			{
				isInsertSuccess = "false";

				File dir1 = new File(servletContext.getRealPath("/") + "images/");
				
				if (!dir1.exists())
					dir1.mkdirs();

				if (file!=null)
				{
					byte[] bytes1 = file.getBytes();

					File serverFile1 = new File(dir1.getAbsolutePath() + File.separator + photoName);
					BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
					stream1.write(bytes1);
					stream1.close();
				}
				isInsertSuccess = "true";
			}

			return isInsertSuccess;
		}
		catch (Exception e)
		{
			
		    	logger.info(e.getCause(), e);
				return isInsertSuccess;
			
		}
	}

	private SeedSource prepareModelforSourceOfSeed(SourceOfSeedBean sourceofseedbean,int transactionCode,int suptransactionCode)
	{
		Integer maxId = commonService.getMaxID("SeedSource");
		SeedSource seedSource = new SeedSource();
		String modifyFlag = sourceofseedbean.getModifyFlag();
		seedSource.setSeedtype(sourceofseedbean.getSeedType());
		String batchseries = "";
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			seedSource.setId(sourceofseedbean.getId());
			batchseries = sourceofseedbean.getBatchNo();

		}
		else
		{
			if(("Yes".equalsIgnoreCase(sourceofseedbean.getModifyFlag1())))
				batchseries = sourceofseedbean.getBatchNo();
			
			
			seedSource.setId(maxId);
/*
			Integer type1 = sourceofseedbean.getSeedType();
			String type = Integer.toString(type1);

			JSONArray dropDownArray = commonService.GetMaxSourceOfSeed(type,sourceofseedbean.getSeason());
			JSONObject batchseries1 = (JSONObject) dropDownArray.get(0);
			batchseries = batchseries1.getString("batchNo");*/

		}
		

		seedSource.setSeason(sourceofseedbean.getSeason());
		String sourcesuppliedDate = sourceofseedbean.getDate();
		seedSource.setSourcesupplieddate(DateUtils.getSqlDateFromString(sourcesuppliedDate,
		        Constants.GenericDateFormat.DATE_FORMAT));

		seedSource.setSeason(sourceofseedbean.getSeason());
		seedSource.setSeedtype(sourceofseedbean.getSeedType());
		if(sourceofseedbean.getCircleCode()!=null)
		{
			seedSource.setCirclecode(sourceofseedbean.getCircleCode());
		}
		else
		{
			seedSource.setCirclecode(0);
		}
		seedSource.setPurpose(sourceofseedbean.getPurpose());
		if(sourceofseedbean.getPurpose()==1)
		{
		seedSource.setConsumercode(sourceofseedbean.getConsumerCode());
		}
		String strSeedSrCode = sourceofseedbean.getSeedSrCode();
		
		if (strSeedSrCode == null || strSeedSrCode == "null" || strSeedSrCode == "")
		{
			strSeedSrCode = sourceofseedbean.getOtherRyot();
		}
		seedSource.setSeedsrcode(strSeedSrCode);
		seedSource.setOtherryot(sourceofseedbean.getOtherRyot());
		seedSource.setVarietycode(sourceofseedbean.getVarietyCode());
		seedSource.setAggrementNo(sourceofseedbean.getAggrementNo());
		String village=sourceofseedbean.getVillageCode();
		if (village == null)
		{
			seedSource.setVillagecode(sourceofseedbean.getOtherRyotVillage());
		}
		else
		{
			seedSource.setVillagecode(sourceofseedbean.getVillageCode());
		}
		seedSource.setBatchno(batchseries);
		seedSource.setCaneweight(sourceofseedbean.getCaneWeight());
		seedSource.setLorryweight(sourceofseedbean.getLorryWeight());
		seedSource.setNetweight(sourceofseedbean.getNetWeight());
		seedSource.setBm(sourceofseedbean.getBm());
		seedSource.setFinalweight(sourceofseedbean.getFinalWeight());
		seedSource.setSeedcostperton(sourceofseedbean.getSeedCostPerTon());
		seedSource.setTotalcost(sourceofseedbean.getTotalCost());
		seedSource.setAgeofcrop(sourceofseedbean.getAgeOfCrop());
		seedSource.setAvgbudspercane(sourceofseedbean.getAvgBudsPerCane());
		seedSource.setPhysicalcondition(sourceofseedbean.getPhysicalCondition());
		seedSource.setSeedcertifiedby(sourceofseedbean.getSeedCertifiedBy());
		seedSource.setFilelocation(sourceofseedbean.getFileLocation());
		seedSource.setNoofacre(sourceofseedbean.getNoofacre());
		seedSource.setCostperacre(sourceofseedbean.getCostperacre());
		seedSource.setAcreage(sourceofseedbean.getAcreage());
		seedSource.setUpdateType(sourceofseedbean.getUpdateType());
		seedSource.setOtherRyotVillage(sourceofseedbean.getOtherRyotVillage());
		seedSource.setVehicleNo(sourceofseedbean.getVehicleNo());
		String certifydate = sourceofseedbean.getCertDate();
		seedSource.setCertdate(DateUtils.getSqlDateFromString(certifydate, Constants.GenericDateFormat.DATE_FORMAT));

		Date currentDate = new Date();
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
		String PresentYear[] = todayDate.split("-");
		String PYear = PresentYear[0];
		seedSource.setRunningyear(PYear);
		seedSource.setHarvestingcharges(sourceofseedbean.getHarvestingCharges());
		seedSource.setTranscharges(sourceofseedbean.getTransCharges());
		seedSource.setTransactioncode(transactionCode);
		seedSource.setSuptransactioncode(suptransactionCode);
		return seedSource;
	}

	private BatchServicesMaster prepareModelforBatchServicesMaster(SourceOfSeedBean sourceofseedbean)
	{
		Integer maxId = commonService.getMaxID("BatchServicesMaster");
		BatchServicesMaster batchservicesmaster = new BatchServicesMaster();
		String modifyFlag = sourceofseedbean.getModifyFlag();
		String batchseries = "";
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			batchservicesmaster.setId(sourceofseedbean.getId());
			batchseries = sourceofseedbean.getBatchNo();
		}
		else
		{
			batchservicesmaster.setId(maxId);

			Integer type1 = sourceofseedbean.getSeedType();
			String type = Integer.toString(type1);
			JSONArray dropDownArray = commonService.GetMaxSourceOfSeed(type,sourceofseedbean.getSeason());
			JSONObject batchseries1 = (JSONObject) dropDownArray.get(0);
			batchseries = batchseries1.getString("batchNo");

		}
		Integer seedtype = sourceofseedbean.getSeedType();
		String type = "O";
		if (seedtype == 0)
		{
			type = "S";
		}
		else if (seedtype == 1)
		{
			type = "V";
		}
		else if (seedtype == 2)
		{
			type = "F";
		}
		int Seqno = 0;

		batchservicesmaster.setSeason(sourceofseedbean.getSeason());
		batchservicesmaster.setBatchseries(batchseries);
		batchservicesmaster.setSeedType(type);
		String batchno = batchseries;//sourceofseedbean.getBatchNo();
		
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);

		Seqno = Integer.parseInt(batchno);
		batchservicesmaster.setSeqno(Seqno);
		batchservicesmaster.setVarietycode(sourceofseedbean.getVarietyCode());
		int vCode = 0;
		vCode = sourceofseedbean.getVarietyCode();

		//vCode=Integer.parseInt(varietyCode);
		String varietyName = "NA";
		List VarietyList = new ArrayList();
		VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(vCode);
		if (VarietyList != null && VarietyList.size() > 0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) VarietyList.get(0);
			varietyName = (String) tempMap.get("variety");
		}
		batchservicesmaster.setVariety(varietyName);
		String strSeedSrCode = sourceofseedbean.getSeedSrCode();
				
		if (strSeedSrCode == null || strSeedSrCode == "null" || strSeedSrCode == "")
		{
			strSeedSrCode = sourceofseedbean.getOtherRyot();
		}
		batchservicesmaster.setSeedsuppliercode(strSeedSrCode);
			
		String village=sourceofseedbean.getVillageCode();

		if (village == null)
		{
			batchservicesmaster.setLandvillagecode(sourceofseedbean.getOtherRyotVillage());
		}
		else
		{
		batchservicesmaster.setLandvillagecode(sourceofseedbean.getVillageCode());
		}

		Date currentDate = new Date();
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
		String PresentYear[] = todayDate.split("-");
		String PYear = PresentYear[0];
		batchservicesmaster.setRunningyear(PYear);

		batchservicesmaster.setIsbatchprepared((byte) 0);

		return batchservicesmaster;
	}

	@RequestMapping(value = "/getAllSeedSources", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<SourceOfSeedBean> getAllSeedSources(@RequestBody JSONObject jsonData) throws Exception
	{

		String batchno1 = (String) jsonData.get("addedBatch");
		String batchno2[] = batchno1.split("-");
		String batchno=batchno2[0];
		String currentYear = (String) jsonData.get("addedYear");
		String dateid = (String) jsonData.get("dateid");

		List<SourceOfSeedBean> sourceofseedbeans = prepareListofBeanForSeedSources(reasearchAndDevelopmentService.listSourceSeeds(batchno, currentYear, dateid), dateid);
		return sourceofseedbeans;
	}

	private List<SourceOfSeedBean> prepareListofBeanForSeedSources(List<SeedSource> SeedSources, String dateid)
	{
		List<SourceOfSeedBean> beans = null;
		if (SeedSources != null && !SeedSources.isEmpty())
		{
			beans = new ArrayList<SourceOfSeedBean>();
			SourceOfSeedBean bean = null;
			for (SeedSource seedsource : SeedSources)
			{
				bean = new SourceOfSeedBean();
				bean.setAgeOfCrop(seedsource.getAgeofcrop());
				bean.setAvgBudsPerCane(seedsource.getAvgbudspercane());
				bean.setModifyFlag("Yes");
				bean.setModifyFlag1("Yes");
				bean.setId(seedsource.getId());
				bean.setSeason(seedsource.getSeason());
				Date sourceDate = seedsource.getSourcesupplieddate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String seedsourcedate = df.format(sourceDate);
				bean.setDate(seedsourcedate);
				bean.setSeedType(seedsource.getSeedtype());
				bean.setCircleCode(seedsource.getCirclecode());
				bean.setPurpose(seedsource.getPurpose());
				bean.setConsumerCode(seedsource.getConsumercode());
				bean.setSeedSrCode(seedsource.getSeedsrcode());
				bean.setVarietyCode(seedsource.getVarietycode());
				bean.setVillageCode(seedsource.getVillagecode());
				bean.setBatchNo(seedsource.getBatchno());
				bean.setCaneWeight(seedsource.getCaneweight());
				bean.setLorryWeight(seedsource.getLorryweight());
				bean.setNetWeight(seedsource.getNetweight());
				bean.setBm(seedsource.getBm());
				bean.setFinalWeight(seedsource.getFinalweight());
				bean.setSeedCostPerTon(seedsource.getSeedcostperton());
				bean.setTotalCost(seedsource.getTotalcost());
				bean.setPhysicalCondition(seedsource.getPhysicalcondition());
				bean.setSeedCertifiedBy(seedsource.getSeedcertifiedby());
				Date certidate = seedsource.getCertdate();
				if(certidate !=null)
				{
				DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
				String certdate = df1.format(certidate);
				bean.setCertDate(certdate);
				}
				bean.setFileLocation(seedsource.getFilelocation());
				bean.setHarvestingCharges(seedsource.getHarvestingcharges());
				bean.setTransCharges(seedsource.getTranscharges());
				bean.setOtherRyotVillage(seedsource.getOtherRyotVillage());
				bean.setVehicleNo(seedsource.getVehicleNo());
				bean.setNoofacre(seedsource.getNoofacre());
				bean.setCostperacre(seedsource.getCostperacre());
				bean.setAcreage(seedsource.getAcreage());
				bean.setAggrementNo(seedsource.getAggrementNo());
				bean.setTransactionCode(seedsource.getTransactioncode());
				bean.setSuptransactionCode(seedsource.getSuptransactioncode());
				byte a = 1;
				bean.setUpdateType(a);
				bean.setDates(dateid);

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/getYearSourceofSeeds", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getYearSourceofSeeds() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getYearSourceofSeeds();
		logger.info("getYearSourceofSeeds() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedYear = (String) dateMap.get("runningyear");
				jsonObj.put("addedYear", addedYear);
				jArray.add(jsonObj);
			}
		}
		logger.info("getYearSourceofSeeds() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getbatchSourceofSeeds", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getbatchSourceofSeeds(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		//JSONObject jsonObj = null;
		//jsonObj = new JSONObject();
		org.json.JSONObject response = new org.json.JSONObject();
		String year = (String) jsonData.get("addedYear");
		String seedsupplier = (String) jsonData.get("seedSrCode");
		String otherryot = (String) jsonData.get("otherRyot");
		
		List DateList = reasearchAndDevelopmentService.getbatchSourceofSeeds(year,seedsupplier,otherryot);
		logger.info("getbatchSourceofSeeds() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedbatches = (String) dateMap.get("batchseries");
				String variety = (String) dateMap.get("variety");
				List DateList1 = reasearchAndDevelopmentService.getseedsourceChangeSummary(addedbatches);
				logger.info("getbatchSourceofSeeds() ========DateList==========" + DateList1);
				String village="";
				if(seedsupplier!=null)
				{
					if (DateList1 != null && DateList1.size() > 0)
					{
						
							Map dateMap1 = new HashMap();
							dateMap1 = (Map) DateList1.get(0);
							String villagecode= (String) dateMap1.get("villagecode");
							List<Village> villageNamelist = ahFuctionalService.getVillageForLedger(villagecode);
							if (villageNamelist != null && villageNamelist.size() > 0)
							{
								village = villageNamelist.get(0).getVillage();
							}
					}
				}
				else
				{
					
					List DateList3 = reasearchAndDevelopmentService.getseedsourceChangeSummary(addedbatches);
					logger.info("getseedsourceLoadSummary() ========DateList3==========" + DateList3);
					if (DateList3 != null && DateList3.size() > 0)
					{
						Map dateMap1 = new HashMap();
						dateMap1 = (Map) DateList3.get(0);

						 village = (String) dateMap1.get("otherRyotVillage");
					}
				}
				
				jsonObj.put("addedBatch", addedbatches+"-"+variety+","+village);
				jArray.add(jsonObj);
			}
		}
		logger.info("getbatchSourceofSeeds() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/saveDetrashing", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveDetrashing(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			
			
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			int transactionCode = 0;
			DetrashingBean detrashingbean = new ObjectMapper().readValue(formdata.toString(), DetrashingBean.class);
			String season = detrashingbean.getSeason();
			transactionCode = commonService.GetMaxTransCode(season);
			DetrashSummary detrashingSummary = prepareModelforDetrashingSummary(detrashingbean,transactionCode);

			entityList.add(detrashingSummary);
			
			for (int i = 0; i < griddata.size(); i++)
			{
				DetrashingDetailsBean detrashingdetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        DetrashingDetailsBean.class);
				DetrashCaneDetails detrashCaneDetails = prepareModelforDetrashCaneDetails(detrashingdetailsBean, detrashingbean);
				entityList.add(detrashCaneDetails);
			}
			byte labourstype = detrashingbean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(detrashingSummary.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			

			String flag = detrashingbean.getModifyFlag();
			if ("Yes".equalsIgnoreCase(flag)) 
			{
				// Revert Acc smry here
				transactionCode = detrashingbean.getTransactionCode();
				logger.info("========TransactionCode=========="	+ transactionCode);

				// Reverting Account tables here
				commonService.RevertAccountTables(transactionCode);
			} 

			
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = detrashingbean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = detrashingbean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = detrashingbean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = detrashingbean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			
			

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,detrashingbean.getModifyFlag());
					
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private DetrashSummary prepareModelforDetrashingSummary(DetrashingBean detrashingbean,Integer transactionCode)
	{
		Integer maxId = commonService.getMaxID("DetrashSummary");
		DetrashSummary detrashSummary = new DetrashSummary();
		String modifyFlag = detrashingbean.getModifyFlag();

		String season = detrashingbean.getSeason();
		Integer shift = detrashingbean.getShift();
		String detrashingdate = detrashingbean.getDetrashingDate();
		Date detrashingdate1 = DateUtils.getSqlDateFromString(detrashingdate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(detrashingdate1);
		reasearchAndDevelopmentService.deleteDetrashDetails(season, todayDate, shift);

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			detrashSummary.setId(detrashingbean.getId());
			detrashSummary.setTransactioncode(detrashingbean.getTransactionCode());
		}
		else
		{
			detrashSummary.setId(maxId);
			detrashSummary.setTransactioncode(transactionCode);
		}
		detrashSummary.setSeason(detrashingbean.getSeason());
		detrashSummary.setDetrashingDate(DateUtils.getSqlDateFromString(detrashingdate, Constants.GenericDateFormat.DATE_FORMAT));
		detrashSummary.setShift(detrashingbean.getShift());
		detrashSummary.setLabourSpplr(detrashingbean.getLabourSpplr());
		detrashSummary.setNoOfMen(detrashingbean.getNoOfMen());
		detrashSummary.setManCost(detrashingbean.getManCost());
		detrashSummary.setManTotal(detrashingbean.getMenTotal());
		detrashSummary.setNoOfMen(detrashingbean.getNoOfMen());
		detrashSummary.setNoOfWomen(detrashingbean.getNoOfWomen());
		detrashSummary.setWomanCost(detrashingbean.getWomenCost());
		detrashSummary.setWomenTotal(detrashingbean.getWomenTotal());
		detrashSummary.setTotalLabourCost(detrashingbean.getTotalLabourCost());
		detrashSummary.setCostPerTon(detrashingbean.getCostPerTon());
		detrashSummary.setTotalContractAmt(detrashingbean.getTotalContractAmt());
		detrashSummary.setTotalCaneWt(detrashingbean.getTotalCaneWt());
		detrashSummary.setTotalTons(detrashingbean.getTotalTons());
		detrashSummary.setLc(detrashingbean.getLc());
		
		
		return detrashSummary;
	}

	private DetrashCaneDetails prepareModelforDetrashCaneDetails(DetrashingDetailsBean detrashingdetailsBean,
	        DetrashingBean detrashingbean)
	{
		//Integer maxId = commonService.getMaxID("DetrashCaneDetails");
		DetrashCaneDetails detrashcanedetails = new DetrashCaneDetails();
		String modifyFlag = detrashingbean.getModifyFlag();
		detrashcanedetails.setId(detrashingdetailsBean.getId());
		detrashcanedetails.setLandVillageCode(detrashingdetailsBean.getLandVillageCode());
		detrashcanedetails.setWtWithTrash(detrashingdetailsBean.getWtWithTrash());
		detrashcanedetails.setSeedSupr(detrashingdetailsBean.getSeedSupr());
		detrashcanedetails.setWtWithoutTrash(detrashingdetailsBean.getWtWithoutTrash());
		detrashcanedetails.setBatchSeries(detrashingdetailsBean.getBatchSeries());
		detrashcanedetails.setVariety(detrashingdetailsBean.getVariety());
		detrashcanedetails.setSeason(detrashingbean.getSeason());
		String detrashingDetailsDate = detrashingbean.getDetrashingDate();
		detrashcanedetails.setDetrashingDetailsDate(DateUtils.getSqlDateFromString(detrashingDetailsDate,Constants.GenericDateFormat.DATE_FORMAT));
		detrashcanedetails.setDetrashingshift(detrashingbean.getShift());
		return detrashcanedetails;
	}

	@RequestMapping(value = "/getAllBatchNumbers", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllBatchNumbers(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.listAllBatchNumbers(jsonData);
		logger.info("getAllBatchNumbers() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedbatches = (String) dateMap.get("batchseries");
				jsonObj.put("batchseries", addedbatches);
				jArray.add(jsonObj);
			}
		}
		logger.info("getAllBatchNumbers() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getAllBatchNumbersDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllBatchNumbersDetails(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		try
		{
			List DateList = reasearchAndDevelopmentService.listAllBatchNumbersDetails(jsonData);
			logger.info("getAllBatchNumbersDetails() ========DateList==========" + DateList);
			if (DateList != null && DateList.size() > 0)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(0);
				int addedvariety = (Integer) dateMap.get("varietycode");
				String addedlandcode = (String) dateMap.get("landvillagecode");
				String seedsupplier = (String) dateMap.get("seedsuppliercode");
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}

				jsonObj.put("variety", addedvariety);
				jsonObj.put("seedSupr", seedsupplier);
				jsonObj.put("landVillageCode", addedlandcode);
				jsonObj.put("varietyName", varietyName);
				jsonObj.put("ryotName", RyotName);
				jsonObj.put("LVName", LVName);

				jArray.add(jsonObj);

			}
			logger.info("getAllBatchNumbersDetails() ========jArray==========" + jArray);

		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		finally
		{
			return jArray;
		}

	}

	@RequestMapping(value = "/getAllDetrashingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllDetrashingDetails(@RequestBody JSONObject jsonData) throws Exception
	{

		DetrashingBean detrashingbean = null;
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String detrashDate = (String) jsonData.get("detrashingDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date detrashDate1 = DateUtils.getSqlDateFromString(detrashDate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(detrashDate1);

		List<DetrashSummary> detrashSummary = reasearchAndDevelopmentService.listAllDetrashingSummaryDetails(Season, todayDate,
		        Shift);
		DetrashingBean detrashingBean = prepareBeanDetrashingSummaryDetails(detrashSummary);

		List<DetrashCaneDetails> detrashcanedetails = reasearchAndDevelopmentService.listAllDetrashingDetails(Season, todayDate,
		        Shift);
		List<DetrashingDetailsBean> detrashingDetailsBean = prepareListofBeanForBranches(detrashcanedetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(detrashingBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(detrashingDetailsBean);

		response.put("DetachSummary", SummaryJson);
		response.put("DetachDetails", DetailsArray);

		logger.info("------getAllDetrashingDetails-----" + response.toString());
		return response.toString();

	}

	private DetrashingBean prepareBeanDetrashingSummaryDetails(List<DetrashSummary> detrashSummary)
	{
		DetrashingBean bean = null;
		if (detrashSummary != null)
		{
			bean = new DetrashingBean();
			bean = new DetrashingBean();
			bean.setId(detrashSummary.get(0).getId());
			bean.setSeason(detrashSummary.get(0).getSeason());
			Date detrashingdate = detrashSummary.get(0).getDetrashingDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String detrasheddate = df.format(detrashingdate);
			bean.setDetrashingDate(detrasheddate);
			bean.setShift(detrashSummary.get(0).getShift());
			bean.setLabourSpplr(detrashSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(detrashSummary.get(0).getNoOfMen());
			bean.setManCost(detrashSummary.get(0).getManCost());
			bean.setMenTotal(detrashSummary.get(0).getManTotal());
			bean.setNoOfWomen(detrashSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(detrashSummary.get(0).getWomenTotal());
			bean.setWomenCost(detrashSummary.get(0).getWomanCost());
			bean.setTotalLabourCost(detrashSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(detrashSummary.get(0).getCostPerTon());
			bean.setTotalTons(detrashSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(detrashSummary.get(0).getTotalContractAmt());
			bean.setTotalCaneWt(detrashSummary.get(0).getTotalCaneWt());
			bean.setLc(detrashSummary.get(0).getLc());
			bean.setTransactionCode(detrashSummary.get(0).getTransactioncode());

			bean.setModifyFlag("Yes");
		}
		return bean;
	}

	private List<DetrashingDetailsBean> prepareListofBeanForBranches(List<DetrashCaneDetails> detrashCaneDetails)
	{
		List<DetrashingDetailsBean> beans = null;
		if (detrashCaneDetails != null && !detrashCaneDetails.isEmpty())
		{
			beans = new ArrayList<DetrashingDetailsBean>();
			DetrashingDetailsBean bean = null;
			for (DetrashCaneDetails detrashcanedetail : detrashCaneDetails)
			{
				bean = new DetrashingDetailsBean();
				bean.setId(detrashcanedetail.getId());
				bean.setSeason(detrashcanedetail.getSeason());
				Date detrashingdate = detrashcanedetail.getDetrashingDetailsDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strdetrasheddate = df.format(detrashingdate);
				bean.setDetrashingDetailsDate(strdetrasheddate);
				bean.setDetrashingshift(detrashcanedetail.getDetrashingshift());
				bean.setBatchSeries(detrashcanedetail.getBatchSeries());
				bean.setVariety(detrashcanedetail.getVariety());
				bean.setSeedSupr(detrashcanedetail.getSeedSupr());
				bean.setLandVillageCode(detrashcanedetail.getLandVillageCode());

				int addedvariety = detrashcanedetail.getVariety();
				String seedsupplier = detrashcanedetail.getSeedSupr();
				String addedlandcode = detrashcanedetail.getLandVillageCode();

				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}
				bean.setRyotName(RyotName);
				bean.setVarietyName(varietyName);
				bean.setLvName(LVName);

				bean.setWtWithTrash(detrashcanedetail.getWtWithTrash());
				bean.setWtWithoutTrash(detrashcanedetail.getWtWithoutTrash());
				bean.setId(detrashcanedetail.getId());

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveCaneShifting", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveCaneShifting(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			CaneShiftingBean caneshiftingBean = new ObjectMapper().readValue(formdata.toString(), CaneShiftingBean.class);

			CaneShiftingSummary caneShiftingSummary = prepareModelforCaneShiftingSummary(caneshiftingBean);

			entityList.add(caneShiftingSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				ShiftedCaneDetailsBean shiftedcanedetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        ShiftedCaneDetailsBean.class);
				ShiftedCaneDetails shiftedcaneDetails = prepareModelforShiftedCaneDetails(shiftedcanedetailsBean,
				        caneshiftingBean);

				entityList.add(shiftedcaneDetails);
			}
			byte labourstype = caneshiftingBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(caneshiftingBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

		

			String flag = caneshiftingBean.getModifyFlag();
			String season = caneshiftingBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = caneshiftingBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = caneshiftingBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = caneshiftingBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = caneshiftingBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        caneshiftingBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private CaneShiftingSummary prepareModelforCaneShiftingSummary(CaneShiftingBean caneshiftingBean)
	{
		Integer maxId = commonService.getMaxID("CaneShiftingSummary");
		String modifyFlag = caneshiftingBean.getModifyFlag();
		CaneShiftingSummary caneShiftingSummary = new CaneShiftingSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			caneShiftingSummary.setId(caneshiftingBean.getId());
		}
		else
		{
			caneShiftingSummary.setId(maxId);
		}

		String season = caneshiftingBean.getSeason();
		Integer shift = caneshiftingBean.getShift();
		String canseshiftindate = caneshiftingBean.getCaneShiftingDate();
		Date canseshiftindate1 = DateUtils.getSqlDateFromString(canseshiftindate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(canseshiftindate1);
		reasearchAndDevelopmentService.deleteShiftedCaneDetails(season, todayDate, shift);
		caneShiftingSummary.setSeason(caneshiftingBean.getSeason());
		String detrashingdate = caneshiftingBean.getCaneShiftingDate();
		caneShiftingSummary.setCaneShiftingDate(DateUtils.getSqlDateFromString(detrashingdate,
		        Constants.GenericDateFormat.DATE_FORMAT));
		caneShiftingSummary.setShift(caneshiftingBean.getShift());
		caneShiftingSummary.setLabourSpplr(caneshiftingBean.getLabourSpplr());
		caneShiftingSummary.setNoOfMen(caneshiftingBean.getNoOfMen());
		caneShiftingSummary.setManCost(caneshiftingBean.getManCost());
		caneShiftingSummary.setMenTotal(caneshiftingBean.getMenTotal());
		caneShiftingSummary.setNoOfWomen(caneshiftingBean.getNoOfWomen());
		caneShiftingSummary.setWomanCost(caneshiftingBean.getWomenCost());
		caneShiftingSummary.setWomenTotal(caneshiftingBean.getWomenTotal());
		caneShiftingSummary.setTotalLabourCost(caneshiftingBean.getTotalLabourCost());
		caneShiftingSummary.setCostPerTon(caneshiftingBean.getCostPerTon());
		caneShiftingSummary.setTotalTons(caneshiftingBean.getTotalTons());
		caneShiftingSummary.setTotalContractAmt(caneshiftingBean.getTotalContractAmt());
		caneShiftingSummary.setTotalCaneWt(caneshiftingBean.getTotalCaneWt());
		caneShiftingSummary.setLc(caneshiftingBean.getLc());
		return caneShiftingSummary;
	}

	private ShiftedCaneDetails prepareModelforShiftedCaneDetails(ShiftedCaneDetailsBean shiftedcanedetailsBean,
	        CaneShiftingBean caneshiftingBean)
	{
		//Integer maxId = commonService.getMaxID("CaneShiftingSummary");
		ShiftedCaneDetails shiftedcaneDetails = new ShiftedCaneDetails();
		String modifyFlag = caneshiftingBean.getModifyFlag();
		shiftedcaneDetails.setId(shiftedcanedetailsBean.getId());
		shiftedcaneDetails.setLandVillageCode(shiftedcanedetailsBean.getLandVillageCode());
		shiftedcaneDetails.setCaneWt(shiftedcanedetailsBean.getCaneWt());
		shiftedcaneDetails.setSeedSupr(shiftedcanedetailsBean.getSeedSupr());
		shiftedcaneDetails.setBatchSeries(shiftedcanedetailsBean.getBatchSeries());
		shiftedcaneDetails.setVariety(shiftedcanedetailsBean.getVariety());
		shiftedcaneDetails.setSeason(caneshiftingBean.getSeason());
		String detrashingDetailsDate = caneshiftingBean.getCaneShiftingDate();
		shiftedcaneDetails.setShiftedCaneDate(DateUtils.getSqlDateFromString(detrashingDetailsDate,
		        Constants.GenericDateFormat.DATE_FORMAT));
		shiftedcaneDetails.setShiftedCaneShift(caneshiftingBean.getShift());
		return shiftedcaneDetails;
	}

	@RequestMapping(value = "/getAllCaneShiftingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllCaneShiftingDetails(@RequestBody JSONObject jsonData) throws Exception
	{

		CaneShiftingBean caneshiftingBean = null;
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String caneshiftingDate = (String) jsonData.get("caneShiftingDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date caneshiftdate = DateUtils.getSqlDateFromString(caneshiftingDate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(caneshiftdate);

		List<CaneShiftingSummary> caneshiftingSummary = reasearchAndDevelopmentService.listAllCaneShiftingSummary(Season,
		        todayDate, Shift);
		CaneShiftingBean caneShiftingBean = prepareBeanCaneShiftingSummary(caneshiftingSummary);

		List<ShiftedCaneDetails> shiftedcanedetails = reasearchAndDevelopmentService.listAllShiftedCaneDetails(Season, todayDate,
		        Shift);
		List<ShiftedCaneDetailsBean> shiftedcanedetailsBean = prepareBeanShiftedCaneDetails(shiftedcanedetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(caneShiftingBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(shiftedcanedetailsBean);

		response.put("DetachSummary", SummaryJson);
		response.put("DetachDetails", DetailsArray);

		logger.info("------getAllCaneShiftingDetails-----" + response.toString());
		return response.toString();

	}

	private CaneShiftingBean prepareBeanCaneShiftingSummary(List<CaneShiftingSummary> caneShiftingSummary)
	{
		CaneShiftingBean bean = null;
		if (caneShiftingSummary != null)
		{
			bean = new CaneShiftingBean();
			bean.setId(caneShiftingSummary.get(0).getId());
			bean.setSeason(caneShiftingSummary.get(0).getSeason());
			Date caneshiftingdate = caneShiftingSummary.get(0).getCaneShiftingDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String caneshiftdate = df.format(caneshiftingdate);
			bean.setCaneShiftingDate(caneshiftdate);
			bean.setShift(caneShiftingSummary.get(0).getShift());
			bean.setLabourSpplr(caneShiftingSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(caneShiftingSummary.get(0).getNoOfMen());
			bean.setManCost(caneShiftingSummary.get(0).getManCost());
			bean.setNoOfWomen(caneShiftingSummary.get(0).getWomanCost());
			bean.setWomenTotal(caneShiftingSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(caneShiftingSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(caneShiftingSummary.get(0).getCostPerTon());
			bean.setTotalTons(caneShiftingSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(caneShiftingSummary.get(0).getTotalContractAmt());
			bean.setTotalCaneWt(caneShiftingSummary.get(0).getTotalCaneWt());
			bean.setMenTotal(caneShiftingSummary.get(0).getMenTotal());
			bean.setWomenCost(caneShiftingSummary.get(0).getWomanCost());
			bean.setLc(caneShiftingSummary.get(0).getLc());

			bean.setModifyFlag("Yes");
		}
		return bean;
	}

	private List<ShiftedCaneDetailsBean> prepareBeanShiftedCaneDetails(List<ShiftedCaneDetails> shiftedCaneDetails)
	{
		List<ShiftedCaneDetailsBean> beans = null;
		if (shiftedCaneDetails != null && !shiftedCaneDetails.isEmpty())
		{
			beans = new ArrayList<ShiftedCaneDetailsBean>();
			ShiftedCaneDetailsBean bean = null;
			for (ShiftedCaneDetails shiftedCaneDetail : shiftedCaneDetails)
			{
				bean = new ShiftedCaneDetailsBean();
				bean.setId(shiftedCaneDetail.getId());
				bean.setSeason(shiftedCaneDetail.getSeason());
				Date detrashingdate = shiftedCaneDetail.getShiftedCaneDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strdetrasheddate = df.format(detrashingdate);
				bean.setShiftingCaneDate(strdetrasheddate);
				bean.setShiftingCaneshift(shiftedCaneDetail.getShiftedCaneShift());
				bean.setBatchSeries(shiftedCaneDetail.getBatchSeries());
				bean.setVariety(shiftedCaneDetail.getVariety());
				bean.setSeedSupr(shiftedCaneDetail.getSeedSupr());
				bean.setLandVillageCode(shiftedCaneDetail.getLandVillageCode());
				bean.setCaneWt(shiftedCaneDetail.getCaneWt());
				int addedvariety = shiftedCaneDetail.getVariety();
				String seedsupplier = shiftedCaneDetail.getSeedSupr();
				String addedlandcode = shiftedCaneDetail.getLandVillageCode();

				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}
				bean.setRyotName(RyotName);
				bean.setVarietyName(varietyName);
				bean.setLvName(LVName);

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveBudCutting", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveBudCutting(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			BudCuttingSummaryBean budCuttingSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        BudCuttingSummaryBean.class);

			BudCuttingSummary budCuttingSummary = prepareModelforBudCuttingSummary(budCuttingSummaryBean);

			entityList.add(budCuttingSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				BudCuttingDetailsBean budCuttingDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        BudCuttingDetailsBean.class);
				BudCuttingDetails budCuttingDetails = prepareModelforBudCuttingDetails(budCuttingDetailsBean,
				        budCuttingSummaryBean);

				entityList.add(budCuttingDetails);
			}
			byte labourstype = budCuttingSummaryBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(budCuttingSummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			

			String flag = budCuttingSummaryBean.getModifyFlag();
			String season = budCuttingSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = budCuttingSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = budCuttingSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = budCuttingSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = budCuttingSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        budCuttingSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private BudCuttingSummary prepareModelforBudCuttingSummary(BudCuttingSummaryBean budCuttingSummaryBean)
	{
		Integer maxId = commonService.getMaxID("BudCuttingSummary");
		String modifyFlag = budCuttingSummaryBean.getModifyFlag();
		BudCuttingSummary budCuttingSummary = new BudCuttingSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			budCuttingSummary.setId(budCuttingSummaryBean.getId());
		}
		else
		{
			budCuttingSummary.setId(maxId);
		}

		String season = budCuttingSummaryBean.getSeason();
		Integer shift = budCuttingSummaryBean.getShift();
		String budcuttingdate = budCuttingSummaryBean.getDate();
		Date budcuttingdate1 = DateUtils.getSqlDateFromString(budcuttingdate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(budcuttingdate1);
		reasearchAndDevelopmentService.deleteBudCuttingDetails(season, todayDate, shift);
		budCuttingSummary.setSeason(budCuttingSummaryBean.getSeason());
		String budcuttingdate2 = budCuttingSummaryBean.getDate();
		budCuttingSummary.setBudCuttingDate(DateUtils.getSqlDateFromString(budcuttingdate2,
		        Constants.GenericDateFormat.DATE_FORMAT));
		budCuttingSummary.setShift(budCuttingSummaryBean.getShift());
		budCuttingSummary.setLabourSpplr(budCuttingSummaryBean.getLabourSpplr());
		budCuttingSummary.setNoOfMen(budCuttingSummaryBean.getNoOfMen());
		budCuttingSummary.setManCost(budCuttingSummaryBean.getManCost());
		budCuttingSummary.setMenTotal(budCuttingSummaryBean.getMenTotal());
		budCuttingSummary.setNoOfWomen(budCuttingSummaryBean.getNoOfWomen());
		budCuttingSummary.setWomanCost(budCuttingSummaryBean.getWomanCost());
		budCuttingSummary.setWomenTotal(budCuttingSummaryBean.getWomenTotal());
		budCuttingSummary.setTotalLabourCost(budCuttingSummaryBean.getTotalLabourCost());
		budCuttingSummary.setCostPerTon(budCuttingSummaryBean.getCostPerTon());
		budCuttingSummary.setTotalTons(budCuttingSummaryBean.getTotalTons());
		budCuttingSummary.setTotalContractAmt(budCuttingSummaryBean.getTotalContractAmt());
		budCuttingSummary.setTotalCaneWt(budCuttingSummaryBean.getTotalCaneWt());
		budCuttingSummary.setBudGrandTotal(budCuttingSummaryBean.getBudGrandTotal());
		budCuttingSummary.setTotalNoOfBuds(budCuttingSummaryBean.getTotalNoOfBuds());
		budCuttingSummary.setMachineOrMen(budCuttingSummaryBean.getMachineOrMen());
		budCuttingSummary.setNoOfMachines(budCuttingSummaryBean.getNoOfMachines());
		budCuttingSummary.setLc(budCuttingSummaryBean.getLc());
		return budCuttingSummary;
	}

	private BudCuttingDetails prepareModelforBudCuttingDetails(BudCuttingDetailsBean budCuttingDetailsBean,
	        BudCuttingSummaryBean budCuttingSummaryBean)
	{

		BudCuttingDetails budCuttingDetails = new BudCuttingDetails();

		budCuttingDetails.setId(budCuttingDetailsBean.getId());
		budCuttingDetails.setSeason(budCuttingSummaryBean.getSeason());
		String budcuttingdate = budCuttingSummaryBean.getDate();
		budCuttingDetails.setCuttingDetailsdate(DateUtils.getSqlDateFromString(budcuttingdate,
		        Constants.GenericDateFormat.DATE_FORMAT));
		budCuttingDetails.setBatchSeries(budCuttingDetailsBean.getBatchSeries());
		budCuttingDetails.setShift(budCuttingSummaryBean.getShift());
		budCuttingDetails.setVariety(budCuttingDetailsBean.getVariety());
		budCuttingDetails.setSeedSupr(budCuttingDetailsBean.getSeedSupr());
		budCuttingDetails.setLandVillageCode(budCuttingDetailsBean.getLandVillageCode());
		budCuttingDetails.setMachine(budCuttingDetailsBean.getMachine());
		budCuttingDetails.setBudsPerKG(budCuttingDetailsBean.getBudsPerKG());
		budCuttingDetails.setNoOfTubs(budCuttingDetailsBean.getNoOfTubs());
		budCuttingDetails.setTubWt(budCuttingDetailsBean.getTubWt());
		budCuttingDetails.setTotalBuds(budCuttingDetailsBean.getTotalBuds());

		return budCuttingDetails;
	}

	@RequestMapping(value = "/getAllBudCuttingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllBudCuttingDetails(@RequestBody JSONObject jsonData) throws Exception
	{

		CaneShiftingBean caneshiftingBean = null;
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String caneshiftingDate = (String) jsonData.get("caneShiftingDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date caneshiftdate = DateUtils.getSqlDateFromString(caneshiftingDate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(caneshiftdate);

		List<BudCuttingSummary> budCuttingSummary = reasearchAndDevelopmentService.listAllBudCuttingSummary(Season, todayDate,
		        Shift);
		BudCuttingSummaryBean budCuttingSummaryBean = prepareBeanBudCuttingSummary(budCuttingSummary);

		List<BudCuttingDetails> budCuttingDetails = reasearchAndDevelopmentService.listAllBudCuttingDetails(Season, todayDate,
		        Shift);
		List<BudCuttingDetailsBean> budCuttingDetailsBean = prepareBeanBudCuttingDetails(budCuttingDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(budCuttingSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(budCuttingDetailsBean);

		response.put("DetachSummary", SummaryJson);
		response.put("DetachDetails", DetailsArray);

		logger.info("------getAllBudCuttingDetails-----" + response.toString());
		return response.toString();

	}

	private BudCuttingSummaryBean prepareBeanBudCuttingSummary(List<BudCuttingSummary> budCuttingSummary)
	{
		BudCuttingSummaryBean bean = null;
		if (budCuttingSummary != null)
		{
			bean = new BudCuttingSummaryBean();
			bean.setId(budCuttingSummary.get(0).getId());
			bean.setSeason(budCuttingSummary.get(0).getSeason());
			Date budcuttingdate = budCuttingSummary.get(0).getBudCuttingDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String budcuttingdate1 = df.format(budcuttingdate);
			bean.setDate(budcuttingdate1);
			bean.setShift(budCuttingSummary.get(0).getShift());
			bean.setLabourSpplr(budCuttingSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(budCuttingSummary.get(0).getNoOfMen());
			bean.setManCost(budCuttingSummary.get(0).getManCost());
			bean.setNoOfWomen(budCuttingSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(budCuttingSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(budCuttingSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(budCuttingSummary.get(0).getCostPerTon());
			bean.setTotalTons(budCuttingSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(budCuttingSummary.get(0).getTotalContractAmt());
			bean.setTotalCaneWt(budCuttingSummary.get(0).getTotalCaneWt());
			bean.setMenTotal(budCuttingSummary.get(0).getMenTotal());
			bean.setWomanCost(budCuttingSummary.get(0).getWomanCost());
			bean.setBudGrandTotal(budCuttingSummary.get(0).getBudGrandTotal());
			bean.setTotalNoOfBuds(budCuttingSummary.get(0).getTotalNoOfBuds());
			bean.setMachineOrMen(budCuttingSummary.get(0).getMachineOrMen());
			bean.setLc(budCuttingSummary.get(0).getLc());
			bean.setNoOfMachines(budCuttingSummary.get(0).getNoOfMachines());

			bean.setModifyFlag("Yes");
		}
		return bean;
	}

	private List<BudCuttingDetailsBean> prepareBeanBudCuttingDetails(List<BudCuttingDetails> budCuttingDetails)
	{
		List<BudCuttingDetailsBean> beans = null;
		if (budCuttingDetails != null && !budCuttingDetails.isEmpty())
		{
			beans = new ArrayList<BudCuttingDetailsBean>();
			BudCuttingDetailsBean bean = null;
			for (BudCuttingDetails budCuttingDetail : budCuttingDetails)
			{
				bean = new BudCuttingDetailsBean();
				bean.setId(budCuttingDetail.getId());
				bean.setSeason(budCuttingDetail.getSeason());
				bean.setShift(budCuttingDetail.getShift());
				Date budcuttingdate = budCuttingDetail.getCuttingDetailsdate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String cuttingdate = df.format(budcuttingdate);
				bean.setDate(cuttingdate);
				bean.setBatchSeries(budCuttingDetail.getBatchSeries());
				bean.setVariety(budCuttingDetail.getVariety());
				bean.setSeedSupr(budCuttingDetail.getSeedSupr());
				bean.setLandVillageCode(budCuttingDetail.getLandVillageCode());

				bean.setMachine(budCuttingDetail.getMachine());
				bean.setBudsPerKG(budCuttingDetail.getBudsPerKG());
				bean.setNoOfTubs(budCuttingDetail.getNoOfTubs());
				bean.setTubWt(budCuttingDetail.getTubWt());
				bean.setTotalBuds(budCuttingDetail.getTotalBuds());

				String addedvariety1 = budCuttingDetail.getVariety();
				String seedsupplier = budCuttingDetail.getSeedSupr();
				String addedlandcode = budCuttingDetail.getLandVillageCode();
				int addedvariety = Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}
				bean.setRyotName(RyotName);
				bean.setVarietyName(varietyName);
				bean.setLvName(LVName);

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveChemicalTreatment", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveChemicalTreatment(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray middledata = (JSONArray) jsonData.get("middleData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			ChemicalTreatmentSummaryBean chemicaltreatmentsummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        ChemicalTreatmentSummaryBean.class);
			ChemicalTreatmentSummary chemicalTreatmentSummary = prepareModelforChemicalTreatmentSummary(chemicaltreatmentsummaryBean);
			entityList.add(chemicalTreatmentSummary);

			for (int i = 0; i < middledata.size(); i++)
			{
				ChemicalTreatmentInventoryDetailsBean chemicaltreatmentinventorydetailsBean = new ObjectMapper().readValue(
				        middledata.get(i).toString(), ChemicalTreatmentInventoryDetailsBean.class);
				ChemicalTreatmentInventoryDetails chemicalTreatmentInventoryDetails = prepareModelforChemicalTreatmentInventoryDetails(
				        chemicaltreatmentinventorydetailsBean, chemicaltreatmentsummaryBean);

				entityList.add(chemicalTreatmentInventoryDetails);
			}
			for (int i = 0; i < griddata.size(); i++)
			{
				ChemicalTreatmentDetailsBean chemicaltreatmentdetailsBean = new ObjectMapper().readValue(griddata.get(i)
				        .toString(), ChemicalTreatmentDetailsBean.class);
				ChemicalTreatmentDetails chemicalTreatmentDetails = prepareModelforChemicalTreatmentDetails(
				        chemicaltreatmentdetailsBean, chemicaltreatmentsummaryBean);

				entityList.add(chemicalTreatmentDetails);
			}
			byte labourstype = chemicaltreatmentsummaryBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(chemicaltreatmentsummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

		

			String flag = chemicaltreatmentsummaryBean.getModifyFlag();
			String season = chemicaltreatmentsummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = chemicaltreatmentsummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = chemicaltreatmentsummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = chemicaltreatmentsummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = chemicaltreatmentsummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        chemicaltreatmentsummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private ChemicalTreatmentSummary prepareModelforChemicalTreatmentSummary(
	        ChemicalTreatmentSummaryBean chemicaltreatmentsummaryBean)
	{
		Integer maxId = commonService.getMaxID("ChemicalTreatmentSummary");
		String modifyFlag = chemicaltreatmentsummaryBean.getModifyFlag();
		ChemicalTreatmentSummary chemicaltreatmentsummary = new ChemicalTreatmentSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			chemicaltreatmentsummary.setId(chemicaltreatmentsummaryBean.getId());
		}
		else
		{
			chemicaltreatmentsummary.setId(maxId);
		}

		String season = chemicaltreatmentsummaryBean.getSeason();
		Integer shift = chemicaltreatmentsummaryBean.getShift();
		String treatmentdate = chemicaltreatmentsummaryBean.getDate();
		Date treatmentdate1 = DateUtils.getSqlDateFromString(treatmentdate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(treatmentdate1);

		reasearchAndDevelopmentService.deleteChemicalDetails(season, todayDate, shift);
		reasearchAndDevelopmentService.deleteChemicalinventoryDetails(season, todayDate, shift);

		chemicaltreatmentsummary.setSeason(chemicaltreatmentsummaryBean.getSeason());
		String treatmentdatee = chemicaltreatmentsummaryBean.getDate();
		chemicaltreatmentsummary.setTreatmentdate(DateUtils.getSqlDateFromString(treatmentdatee,
		        Constants.GenericDateFormat.DATE_FORMAT));
		chemicaltreatmentsummary.setShift(chemicaltreatmentsummaryBean.getShift());
		chemicaltreatmentsummary.setLabourSpplr(chemicaltreatmentsummaryBean.getLabourSpplr());
		chemicaltreatmentsummary.setNoOfMen(chemicaltreatmentsummaryBean.getNoOfMen());
		chemicaltreatmentsummary.setManCost(chemicaltreatmentsummaryBean.getManCost());
		chemicaltreatmentsummary.setMenTotal(chemicaltreatmentsummaryBean.getMenTotal());
		chemicaltreatmentsummary.setNoOfWomen(chemicaltreatmentsummaryBean.getNoOfWomen());
		chemicaltreatmentsummary.setWomanCost(chemicaltreatmentsummaryBean.getWomanCost());
		chemicaltreatmentsummary.setWomenTotal(chemicaltreatmentsummaryBean.getWomenTotal());
		chemicaltreatmentsummary.setTotalLabourCost(chemicaltreatmentsummaryBean.getTotalLabourCost());
		chemicaltreatmentsummary.setCostPerTon(chemicaltreatmentsummaryBean.getCostPerTon());
		chemicaltreatmentsummary.setTotalTons(chemicaltreatmentsummaryBean.getTotalTons());
		chemicaltreatmentsummary.setTotalContractAmt(chemicaltreatmentsummaryBean.getTotalContractAmt());
		chemicaltreatmentsummary.setBudGrandTotal(chemicaltreatmentsummaryBean.getBudGrandTotal());
		chemicaltreatmentsummary.setTotalNoOfBuds(chemicaltreatmentsummaryBean.getTotalNoOfBuds());
		chemicaltreatmentsummary.setLv(chemicaltreatmentsummaryBean.getLc());

		return chemicaltreatmentsummary;
	}

	private ChemicalTreatmentInventoryDetails prepareModelforChemicalTreatmentInventoryDetails(
	        ChemicalTreatmentInventoryDetailsBean chemicaltreatmentinventorydetailsBean,
	        ChemicalTreatmentSummaryBean chemicaltreatmentsummaryBean)
	{

		ChemicalTreatmentInventoryDetails chemicaltreatmentinventorydetails = new ChemicalTreatmentInventoryDetails();

		chemicaltreatmentinventorydetails.setId(chemicaltreatmentinventorydetailsBean.getId());
		chemicaltreatmentinventorydetails.setProductSeason(chemicaltreatmentsummaryBean.getSeason());
		String productdate = chemicaltreatmentsummaryBean.getDate();
		chemicaltreatmentinventorydetails.setProductDate(DateUtils.getSqlDateFromString(productdate,
		        Constants.GenericDateFormat.DATE_FORMAT));
		chemicaltreatmentinventorydetails.setProductCode(chemicaltreatmentinventorydetailsBean.getProductCode());
		chemicaltreatmentinventorydetails.setProductShift(chemicaltreatmentsummaryBean.getShift());
		chemicaltreatmentinventorydetails.setProductQuantity(chemicaltreatmentinventorydetailsBean.getProductQuantity());
		chemicaltreatmentinventorydetails.setProductName(chemicaltreatmentinventorydetailsBean.getProductName());

		return chemicaltreatmentinventorydetails;
	}

	private ChemicalTreatmentDetails prepareModelforChemicalTreatmentDetails(
	        ChemicalTreatmentDetailsBean chemicaltreatmentdetailsBean, ChemicalTreatmentSummaryBean chemicaltreatmentsummaryBean)
	{

		ChemicalTreatmentDetails chemicalTreatmentDetails = new ChemicalTreatmentDetails();

		chemicalTreatmentDetails.setId(chemicaltreatmentdetailsBean.getId());
		chemicalTreatmentDetails.setSeason(chemicaltreatmentsummaryBean.getSeason());
		String budcuttingdate = chemicaltreatmentsummaryBean.getDate();
		chemicalTreatmentDetails.setTreatmentDate(DateUtils.getSqlDateFromString(budcuttingdate,
		        Constants.GenericDateFormat.DATE_FORMAT));
		chemicalTreatmentDetails.setBatchSeries(chemicaltreatmentdetailsBean.getBatchSeries());
		chemicalTreatmentDetails.setShift(chemicaltreatmentsummaryBean.getShift());
		chemicalTreatmentDetails.setVariety(chemicaltreatmentdetailsBean.getVariety());
		chemicalTreatmentDetails.setSeedSupr(chemicaltreatmentdetailsBean.getSeedSupr());
		chemicalTreatmentDetails.setLandVillageCode(chemicaltreatmentdetailsBean.getLandVillageCode());
		chemicalTreatmentDetails.setNoOfTubs(chemicaltreatmentdetailsBean.getNoOfTubs());
		chemicalTreatmentDetails.setTubWt(chemicaltreatmentdetailsBean.getTubWt());

		return chemicalTreatmentDetails;
	}

	@RequestMapping(value = "/getAllChemicalTreatments", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllChemicalTreatments(@RequestBody JSONObject jsonData) throws Exception
	{

		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String date = (String) jsonData.get("chemicaltreatmentDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date treatmentdate = DateUtils.getSqlDateFromString(date, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(treatmentdate);

		List<ChemicalTreatmentSummary> chemicalTreatmentSummary = reasearchAndDevelopmentService.listAllChemicalTreatmentSummary(
		        Season, todayDate, Shift);
		ChemicalTreatmentSummaryBean chemicalTreatmentSummaryBean = prepareChemicalTreatmentSummary(chemicalTreatmentSummary);

		List<ChemicalTreatmentInventoryDetails> chemicalTreatmentInventoryDetails = reasearchAndDevelopmentService
		        .listAllChemicalTreatmentInventoryDetails(Season, todayDate, Shift);
		List<ChemicalTreatmentInventoryDetailsBean> chemicalTreatmentInventoryDetailsBean = prepareChemicalTreatmentInventoryDetails(chemicalTreatmentInventoryDetails);

		List<ChemicalTreatmentDetails> chemicalTreatmentDetails = reasearchAndDevelopmentService.listAllChemicalTreatmentDetails(
		        Season, todayDate, Shift);
		List<ChemicalTreatmentDetailsBean> chemicalTreatmentDetailsBean = prepareChemicalTreatmentDetails(chemicalTreatmentDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(chemicalTreatmentSummaryBean);
		org.json.JSONArray InvDetailsArray = new org.json.JSONArray(chemicalTreatmentInventoryDetailsBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(chemicalTreatmentDetailsBean);

		response.put("TreatmentSummary", SummaryJson);
		response.put("InvTreatmentDetails", InvDetailsArray);
		response.put("TreatmentDetails", DetailsArray);

		logger.info("------getAllChemicalTreatments-----" + response.toString());
		return response.toString();

	}

	private ChemicalTreatmentSummaryBean prepareChemicalTreatmentSummary(List<ChemicalTreatmentSummary> chemicalTreatmentSummary)
	{
		ChemicalTreatmentSummaryBean bean = null;
		if (chemicalTreatmentSummary != null)
		{
			bean = new ChemicalTreatmentSummaryBean();
			bean.setId(chemicalTreatmentSummary.get(0).getId());
			bean.setSeason(chemicalTreatmentSummary.get(0).getSeason());
			Date treatmentdate = chemicalTreatmentSummary.get(0).getTreatmentdate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String treatmentdate1 = df.format(treatmentdate);
			bean.setDate(treatmentdate1);
			bean.setShift(chemicalTreatmentSummary.get(0).getShift());
			bean.setLabourSpplr(chemicalTreatmentSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(chemicalTreatmentSummary.get(0).getNoOfMen());
			bean.setManCost(chemicalTreatmentSummary.get(0).getManCost());
			bean.setNoOfWomen(chemicalTreatmentSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(chemicalTreatmentSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(chemicalTreatmentSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(chemicalTreatmentSummary.get(0).getCostPerTon());
			bean.setTotalTons(chemicalTreatmentSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(chemicalTreatmentSummary.get(0).getTotalContractAmt());
			bean.setTotalNoOfBuds(chemicalTreatmentSummary.get(0).getTotalNoOfBuds());
			bean.setMenTotal(chemicalTreatmentSummary.get(0).getMenTotal());
			bean.setWomanCost(chemicalTreatmentSummary.get(0).getWomanCost());
			bean.setBudGrandTotal(chemicalTreatmentSummary.get(0).getBudGrandTotal());
			bean.setTotalNoOfBuds(chemicalTreatmentSummary.get(0).getTotalNoOfBuds());
			bean.setLc(chemicalTreatmentSummary.get(0).getLv());

			bean.setModifyFlag("Yes");
		}
		return bean;
	}

	private List<ChemicalTreatmentInventoryDetailsBean> prepareChemicalTreatmentInventoryDetails(
	        List<ChemicalTreatmentInventoryDetails> chemicalTreatmentInventoryDetails)
	{
		List<ChemicalTreatmentInventoryDetailsBean> beans = null;
		if (chemicalTreatmentInventoryDetails != null && !chemicalTreatmentInventoryDetails.isEmpty())
		{
			beans = new ArrayList<ChemicalTreatmentInventoryDetailsBean>();
			ChemicalTreatmentInventoryDetailsBean bean = null;
			for (ChemicalTreatmentInventoryDetails chemicaltreatmentinventoryDetails : chemicalTreatmentInventoryDetails)
			{
				bean = new ChemicalTreatmentInventoryDetailsBean();
				bean.setId(chemicaltreatmentinventoryDetails.getId());
				bean.setProductSeason(chemicaltreatmentinventoryDetails.getProductSeason());
				bean.setProductShift(chemicaltreatmentinventoryDetails.getProductShift());
				Date productdate1 = chemicaltreatmentinventoryDetails.getProductDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String productdate = df.format(productdate1);
				bean.setProductDate(productdate);
				bean.setProductName(chemicaltreatmentinventoryDetails.getProductName());
				bean.setProductCode(chemicaltreatmentinventoryDetails.getProductCode());
				bean.setProductQuantity(chemicaltreatmentinventoryDetails.getProductQuantity());
				beans.add(bean);
			}
		}
		return beans;
	}

	private List<ChemicalTreatmentDetailsBean> prepareChemicalTreatmentDetails(
	        List<ChemicalTreatmentDetails> chemicalTreatmentDetails)
	{
		List<ChemicalTreatmentDetailsBean> beans = null;
		if (chemicalTreatmentDetails != null && !chemicalTreatmentDetails.isEmpty())
		{
			beans = new ArrayList<ChemicalTreatmentDetailsBean>();
			ChemicalTreatmentDetailsBean bean = null;
			for (ChemicalTreatmentDetails chemicalTreatmentDetail : chemicalTreatmentDetails)
			{

				bean = new ChemicalTreatmentDetailsBean();
				bean.setId(chemicalTreatmentDetail.getId());
				bean.setSeason(chemicalTreatmentDetail.getSeason());
				bean.setShift(chemicalTreatmentDetail.getShift());
				Date treatmentdate = chemicalTreatmentDetail.getTreatmentDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String treatmentday1 = df.format(treatmentdate);
				bean.setDate(treatmentday1);
				bean.setBatchSeries(chemicalTreatmentDetail.getBatchSeries());
				bean.setVariety(chemicalTreatmentDetail.getVariety());
				bean.setSeedSupr(chemicalTreatmentDetail.getSeedSupr());
				bean.setLandVillageCode(chemicalTreatmentDetail.getLandVillageCode());
				bean.setNoOfTubs(chemicalTreatmentDetail.getNoOfTubs());
				bean.setTubWt(chemicalTreatmentDetail.getTubWt());

				String addedvariety1 = chemicalTreatmentDetail.getVariety();
				String seedsupplier = chemicalTreatmentDetail.getSeedSupr();
				String addedlandcode = chemicalTreatmentDetail.getLandVillageCode();
				int addedvariety = Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}
				bean.setRyotName(RyotName);
				bean.setVarietyName(varietyName);
				bean.setLvName(LVName);

				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/getAllSpourtGradingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllSpourtGradingDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String gradingdate = (String) jsonData.get("gradingDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date date = DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

		List<GradingSummary> gradingSummary = reasearchAndDevelopmentService.listAllGradingSummary(Season, todayDate, Shift);
		GradingSummaryBean gradingSummaryBean = prepareGradingSummary(gradingSummary);

		List<GradingDetails> gradingDetails = reasearchAndDevelopmentService.listAllGradingDetails(Season, todayDate, Shift);
		List<GradingDetailsBean> gradingDetailsBean = preparegradingDetails(gradingDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(gradingSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(gradingDetailsBean);

		response.put("GradingSummary", SummaryJson);
		response.put("GradingDetails", DetailsArray);

		logger.info("------getAllSpourtGradingDetails-----" + response.toString());
		return response.toString();

	}

	private List<GradingDetailsBean> preparegradingDetails(List<GradingDetails> gradingDetails)
	{
		List<GradingDetailsBean> beans = null;
		if (gradingDetails != null && !gradingDetails.isEmpty())
		{
			beans = new ArrayList<GradingDetailsBean>();
			GradingDetailsBean bean = null;
			for (GradingDetails gradingDetail : gradingDetails)
			{

				bean = new GradingDetailsBean();
				bean.setId(gradingDetail.getId());
				bean.setSeason(gradingDetail.getSeason());
				bean.setShift(gradingDetail.getShift());
				Date gradingdate = gradingDetail.getGradingDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(gradingdate);
				bean.setGradingDate(date);
				bean.setBatch(gradingDetail.getBatch());
				bean.setVariety(gradingDetail.getVariety());
				bean.setSeedSupr(gradingDetail.getSupplier());
				bean.setLandVillageCode(gradingDetail.getVillage());
				bean.setTotalNoOfBuds(gradingDetail.getTotalNoOfBuds());
				bean.setTransferToTrayFilling(gradingDetail.getTransferToTrayFilling());
				bean.setSentBackToSprouting(gradingDetail.getSentBackToSprouting());
				bean.setDisposedToRejection(gradingDetail.getDisposedToRejection());
				bean.setGerminationPerc(gradingDetail.getGerminationPerc());
				bean.setRemarks(gradingDetail.getRemarks());

				String addedvariety1 = gradingDetail.getVariety();
				String seedsupplier = gradingDetail.getSupplier();
				String addedlandcode = gradingDetail.getVillage();
				int addedvariety = Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}

				//get the Ryotname
				List RyotList = new ArrayList();
				String RyotName = "";
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(seedsupplier);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					RyotName = (String) tempMap.get("ryotname");
				}

				List LVList = new ArrayList();
				String LVName = "";
				LVList = (List) ahFuctionalService.getLVName(addedlandcode);

				if (LVList != null && LVList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) LVList.get(0);
					LVName = (String) tempMap.get("village");
				}
				bean.setRyotName(RyotName);
				bean.setVarietyName(varietyName);
				bean.setLvName(LVName);

				beans.add(bean);
			}
		}
		return beans;
	}

	private GradingSummaryBean prepareGradingSummary(List<GradingSummary> gradingSummary)
	{
		GradingSummaryBean bean = null;
		if (gradingSummary != null)
		{
			bean = new GradingSummaryBean();
			bean.setId(gradingSummary.get(0).getId());
			bean.setSeason(gradingSummary.get(0).getSeason());
			Date gradingdate = gradingSummary.get(0).getGradingDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(gradingdate);
			bean.setGradingDate(date);
			bean.setShift(gradingSummary.get(0).getShift());
			bean.setLabourSpplr(gradingSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(gradingSummary.get(0).getNoOfMen());
			bean.setManCost(gradingSummary.get(0).getManCost());
			bean.setNoOfWomen(gradingSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(gradingSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(gradingSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(gradingSummary.get(0).getCostPerTon());
			bean.setTotalTons(gradingSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(gradingSummary.get(0).getTotalContractAmt());
			bean.setMenTotal(gradingSummary.get(0).getMenTotal());
			bean.setWomenCost(gradingSummary.get(0).getWomanCost());
			bean.setLc(gradingSummary.get(0).getLc());
			bean.setModifyFlag("Yes");
		}
		return bean;
	}

	@RequestMapping(value = "/saveSpourtGrading", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveSpourtGrading(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			GradingSummaryBean gradingSummaryBean = new ObjectMapper().readValue(formdata.toString(), GradingSummaryBean.class);
			GradingSummary gradingSummary = prepareModelforGradingSummary(gradingSummaryBean);
			entityList.add(gradingSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				GradingDetailsBean gradingDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        GradingDetailsBean.class);
				GradingDetails gradingDetails = prepareModelforGradingDetails(gradingDetailsBean, gradingSummaryBean);
				entityList.add(gradingDetails);
			}
			byte labourstype = gradingSummaryBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(gradingSummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			

			String flag = gradingSummaryBean.getModifyFlag();
			String season = gradingSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = gradingSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = gradingSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = gradingSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = gradingSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        gradingSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private GradingSummary prepareModelforGradingSummary(GradingSummaryBean gradingSummaryBean)
	{
		Integer maxId = commonService.getMaxID("GradingSummary");
		String modifyFlag = gradingSummaryBean.getModifyFlag();
		GradingSummary gradingSummary = new GradingSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			gradingSummary.setId(gradingSummaryBean.getId());
		}
		else
		{
			gradingSummary.setId(maxId);
		}

		String season = gradingSummaryBean.getSeason();
		Integer shift = gradingSummaryBean.getShift();
		String graddate = gradingSummaryBean.getGradingDate();
		Date date = DateUtils.getSqlDateFromString(graddate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		reasearchAndDevelopmentService.deleteGradingDetails(season, todayDate, shift);
		gradingSummary.setSeason(gradingSummaryBean.getSeason());
		String gradingdate = gradingSummaryBean.getGradingDate();
		gradingSummary.setGradingDate(DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT));
		gradingSummary.setShift(gradingSummaryBean.getShift());
		gradingSummary.setLabourSpplr(gradingSummaryBean.getLabourSpplr());
		gradingSummary.setNoOfMen(gradingSummaryBean.getNoOfMen());
		gradingSummary.setManCost(gradingSummaryBean.getManCost());
		gradingSummary.setMenTotal(gradingSummaryBean.getMenTotal());
		gradingSummary.setNoOfWomen(gradingSummaryBean.getNoOfWomen());
		gradingSummary.setWomanCost(gradingSummaryBean.getWomenCost());
		gradingSummary.setWomenTotal(gradingSummaryBean.getWomenTotal());
		gradingSummary.setTotalLabourCost(gradingSummaryBean.getTotalLabourCost());
		gradingSummary.setCostPerTon(gradingSummaryBean.getCostPerTon());
		gradingSummary.setTotalTons(gradingSummaryBean.getTotalTons());
		gradingSummary.setTotalContractAmt(gradingSummaryBean.getTotalContractAmt());
		gradingSummary.setLc(gradingSummaryBean.getLc());

		return gradingSummary;
	}

	private GradingDetails prepareModelforGradingDetails(GradingDetailsBean gradingDetailsBean,
	        GradingSummaryBean gradingSummaryBean)
	{
		GradingDetails gradingDetails = new GradingDetails();

		gradingDetails.setId(gradingDetailsBean.getId());
		gradingDetails.setSeason(gradingSummaryBean.getSeason());
		gradingDetails.setShift(gradingSummaryBean.getShift());
		String gradingdate = gradingSummaryBean.getGradingDate();
		gradingDetails.setGradingDate(DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT));
		gradingDetails.setBatch(gradingDetailsBean.getBatch());
		gradingDetails.setVariety(gradingDetailsBean.getVariety());
		gradingDetails.setSupplier(gradingDetailsBean.getSeedSupr());
		gradingDetails.setVillage(gradingDetailsBean.getLandVillageCode());
		gradingDetails.setTotalNoOfBuds(gradingDetailsBean.getTotalNoOfBuds());
		gradingDetails.setTransferToTrayFilling(gradingDetailsBean.getTransferToTrayFilling());
		gradingDetails.setSentBackToSprouting(gradingDetailsBean.getSentBackToSprouting());
		gradingDetails.setDisposedToRejection(gradingDetailsBean.getDisposedToRejection());
		gradingDetails.setGerminationPerc(gradingDetailsBean.getGerminationPerc());
		gradingDetails.setRemarks(gradingDetailsBean.getRemarks());
		return gradingDetails;
	}

	@RequestMapping(value = "/saveTrayFilling", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveTrayFiling(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			TrayFillingSummaryBean trayFillingSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        TrayFillingSummaryBean.class);
			TrayFillingSummary trayFillingSummary = prepareModelforTrayFillingSummary(trayFillingSummaryBean);
			entityList.add(trayFillingSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				TrayFillingDetailsBean trayFillingDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        TrayFillingDetailsBean.class);
				TrayFillingDetails trayFillingDetails = prepareModelfortrayFillingDetailsBean(trayFillingDetailsBean,
				        trayFillingSummaryBean);
				String modifyFlag = trayFillingSummaryBean.getModifyFlag();
				String batchseries = trayFillingDetails.getBatchSeries();
				String strQry = "";
				Object Qryobj = "";
				strQry = "UPDATE BatchServicesMaster set isbatchprepared=" + 1 + " WHERE batchseries ='" + batchseries + "' ";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				if ("No".equalsIgnoreCase(modifyFlag))
				{
					BatchMaster batchMaster = prepareModelfortrayFillingBatchMasterDetailsBean(trayFillingDetailsBean,
					        trayFillingSummaryBean);
					entityList.add(batchMaster);
				}
				else
				{
					String season = trayFillingSummaryBean.getSeason();
					String Batchseries = trayFillingDetails.getBatchSeries();
					String fillingdate = trayFillingSummaryBean.getDate();
					Date date = DateUtils.getSqlDateFromString(fillingdate, Constants.GenericDateFormat.DATE_FORMAT);
					String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
					String Presentdetails[] = fillingdate.split("-");
					Integer batchmonth = Integer.parseInt(Presentdetails[1]);
					Integer batchdate = Integer.parseInt(Presentdetails[0]);

					List<BatchMaster> batchMaster = reasearchAndDevelopmentService.getbatchMasterDetails(season, todayDate,
					        Batchseries, batchdate, batchmonth);
					if (batchMaster != null && !batchMaster.isEmpty())
					{
						double totalSeedlings = trayFillingDetails.getTotalNoOfSeedlingss();
						double delivered = batchMaster.get(0).getDelivered();

						Double survivalper = reasearchAndDevelopmentService.getbatchsurvivalper(delivered, totalSeedlings);
						Double balance = reasearchAndDevelopmentService.getbatchbalance(totalSeedlings, delivered);
						Double takenoff = reasearchAndDevelopmentService.getbatchtakenoff(totalSeedlings, delivered, balance);

						reasearchAndDevelopmentService.updateBatchMasterDetails(totalSeedlings, survivalper, balance, takenoff,
						        season, todayDate, Batchseries, batchdate, batchmonth);
					}
					else
					{
						BatchMaster batchMaster1 = prepareModelfortrayFillingBatchMasterDetailsBean(trayFillingDetailsBean,
						        trayFillingSummaryBean);
						entityList.add(batchMaster1);
					}
				}

				entityList.add(trayFillingDetails);

			}
			byte labourstype = trayFillingSummaryBean.getLabourSpplr();
			String lcAccCode = null;
			if(labourstype!=0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(trayFillingSummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			

			String flag = trayFillingSummaryBean.getModifyFlag();
			String season = trayFillingSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = trayFillingSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = trayFillingSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = trayFillingSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = trayFillingSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        trayFillingSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private TrayFillingSummary prepareModelforTrayFillingSummary(TrayFillingSummaryBean trayFillingSummaryBean)
	{
		Integer maxId = commonService.getMaxID("TrayFillingSummary");
		String modifyFlag = trayFillingSummaryBean.getModifyFlag();
		TrayFillingSummary trayFillingSummary = new TrayFillingSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			trayFillingSummary.setId(trayFillingSummaryBean.getId());
		}
		else
		{
			trayFillingSummary.setId(maxId);
		}

		String season = trayFillingSummaryBean.getSeason();
		Integer shift = trayFillingSummaryBean.getShift();
		String fillingdate = trayFillingSummaryBean.getDate();
		Date date = DateUtils.getSqlDateFromString(fillingdate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		reasearchAndDevelopmentService.deleteTrayfillingDetalis(season, todayDate, shift);
		trayFillingSummary.setSeason(trayFillingSummaryBean.getSeason());
		trayFillingSummary.setTrayFillingDate(DateUtils
		        .getSqlDateFromString(fillingdate, Constants.GenericDateFormat.DATE_FORMAT));
		trayFillingSummary.setShift(trayFillingSummaryBean.getShift());
		trayFillingSummary.setLabourSpplr(trayFillingSummaryBean.getLabourSpplr());
		trayFillingSummary.setMachineOrMen(trayFillingSummaryBean.getMachineOrMen());
		trayFillingSummary.setNoOfMen(trayFillingSummaryBean.getNoOfMen());
		trayFillingSummary.setManCost(trayFillingSummaryBean.getManCost());
		trayFillingSummary.setMenTotal(trayFillingSummaryBean.getMenTotal());
		trayFillingSummary.setNoOfWomen(trayFillingSummaryBean.getNoOfWomen());
		trayFillingSummary.setWomanCost(trayFillingSummaryBean.getWomenCost());
		trayFillingSummary.setWomenTotal(trayFillingSummaryBean.getWomenTotal());
		trayFillingSummary.setTotalLabourCost(trayFillingSummaryBean.getTotalLabourCost());
		trayFillingSummary.setCostPerTon(trayFillingSummaryBean.getCostPerTon());
		trayFillingSummary.setTotalTons(trayFillingSummaryBean.getTotalTons());
		trayFillingSummary.setTotalContractAmt(trayFillingSummaryBean.getTotalContractAmt());
		trayFillingSummary.setNoOfMachines(trayFillingSummaryBean.getNoOfMachines());
		trayFillingSummary.setTotalNoOfSeedlings(trayFillingSummaryBean.getTotalNoOfSeedlings());
		trayFillingSummary.setTotalCost(trayFillingSummaryBean.getTotalCost());
		trayFillingSummary.setLc(trayFillingSummaryBean.getLc());

		return trayFillingSummary;
	}

	private TrayFillingDetails prepareModelfortrayFillingDetailsBean(TrayFillingDetailsBean trayFillingDetailsBean,
	        TrayFillingSummaryBean trayFillingSummaryBean)
	{

		TrayFillingDetails trayFillingDetails = new TrayFillingDetails();

		trayFillingDetails.setId(trayFillingDetailsBean.getId());
		trayFillingDetails.setSeason(trayFillingSummaryBean.getSeason());
		trayFillingDetails.setShift(trayFillingSummaryBean.getShift());
		String fillingdate = trayFillingSummaryBean.getDate();
		trayFillingDetails.setTrayFillingDate(DateUtils
		        .getSqlDateFromString(fillingdate, Constants.GenericDateFormat.DATE_FORMAT));
		trayFillingDetails.setBatchSeries(trayFillingDetailsBean.getBatchSeries());
		trayFillingDetails.setVariety(trayFillingDetailsBean.getVariety());
		trayFillingDetails.setSeedlingType(trayFillingDetailsBean.getSeedlingType());
		trayFillingDetails.setBatchNo(trayFillingDetailsBean.getBatchNo());
		trayFillingDetails.setTotalTime(java.sql.Time.valueOf(trayFillingDetailsBean.getTotalTime()));
		trayFillingDetails.setTrayType(trayFillingDetailsBean.getTrayType());
		trayFillingDetails.setNoOfCavitiesPerTray(trayFillingDetailsBean.getNoOfCavitiesPerTray());
		trayFillingDetails.setTotalTrays(trayFillingDetailsBean.getTotalTrays());
		trayFillingDetails.setTotalNoOfSeedlingss(trayFillingDetailsBean.getTotalNoOfSeedlingss());
		trayFillingDetails.setStartTime(java.sql.Time.valueOf(trayFillingDetailsBean.getStartTime()));
		trayFillingDetails.setEndTime(java.sql.Time.valueOf(trayFillingDetailsBean.getEndTime()));

		return trayFillingDetails;
	}

	private BatchMaster prepareModelfortrayFillingBatchMasterDetailsBean(TrayFillingDetailsBean trayFillingDetailsBean,
	        TrayFillingSummaryBean trayFillingSummaryBean)
	{
		BatchMaster batchMaster = new BatchMaster();
		batchMaster.setId(trayFillingDetailsBean.getId());
		batchMaster.setSeason(trayFillingSummaryBean.getSeason());
		batchMaster.setShift(trayFillingSummaryBean.getShift());
		String fillingdate = trayFillingSummaryBean.getDate();
		batchMaster.setTrayFillingDate(DateUtils.getSqlDateFromString(fillingdate, Constants.GenericDateFormat.DATE_FORMAT));
		batchMaster.setBatchSeries(trayFillingDetailsBean.getBatchSeries());
		batchMaster.setVariety(trayFillingDetailsBean.getVariety());
		String Presentdetails[] = fillingdate.split("-");

		/*Calendar now = Calendar.getInstance();
		Integer month=now.get(Calendar.MONTH) + 1;
		Integer date=now.get(Calendar.DATE);*/
		Integer month = Integer.parseInt(Presentdetails[1]);
		Integer date = Integer.parseInt(Presentdetails[0]);
		String batch = trayFillingDetailsBean.getBatchSeries() + month + date;//BatchSeries+Month+Date
		batchMaster.setBatchMonth(month);
		batchMaster.setBatchDate(date);
		batchMaster.setBatch(trayFillingDetailsBean.getBatchNo());
		batchMaster.setTotalSeedlings(trayFillingSummaryBean.getTotalNoOfSeedlings());
		batchMaster.setDelivered(0.0);
		batchMaster.setTakenOffOnes(0.0);
		batchMaster.setBalance(trayFillingDetailsBean.getTotalNoOfSeedlingss());
		batchMaster.setSurvivalPercent(0.0); //Delivered*100/TotalSeedlings
		batchMaster.setIsBatchCompleted((byte) 0);
		return batchMaster;
	}

	@RequestMapping(value = "/getAllTrayfilling", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllTrayfilling(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String Date = (String) jsonData.get("trayFillingDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date fillingdate = DateUtils.getSqlDateFromString(Date, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(fillingdate);

		List<TrayFillingSummary> trayFillingSummary = reasearchAndDevelopmentService.listAllTrayFillingSummary(Season, todayDate,
		        Shift);
		TrayFillingSummaryBean trayFillingSummaryBean = prepareTrayFillingSummary(trayFillingSummary);

		List<TrayFillingDetails> trayFillingDetails = reasearchAndDevelopmentService.listAllTrayFillingDetails(Season, todayDate,
		        Shift);
		List<TrayFillingDetailsBean> trayFillingDetailsBean = prepareTrayFillingDetails(trayFillingDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(trayFillingSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(trayFillingDetailsBean);

		response.put("TrayfillingSummary", SummaryJson);
		response.put("TrayfillingDetails", DetailsArray);

		logger.info("------getAllTrayfilling-----" + response.toString());
		return response.toString();

	}

	private List<TrayFillingDetailsBean> prepareTrayFillingDetails(List<TrayFillingDetails> trayFillingDetails)
	{
		List<TrayFillingDetailsBean> beans = null;
		if (trayFillingDetails != null && !trayFillingDetails.isEmpty())
		{
			beans = new ArrayList<TrayFillingDetailsBean>();
			TrayFillingDetailsBean bean = null;
			for (TrayFillingDetails trayFillingDetail : trayFillingDetails)
			{

				bean = new TrayFillingDetailsBean();
				bean.setId(trayFillingDetail.getId());
				bean.setSeason(trayFillingDetail.getSeason());
				bean.setShift(trayFillingDetail.getShift());
				Date gradingdate = trayFillingDetail.getTrayFillingDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(gradingdate);
				bean.setDate(date);
				bean.setBatchSeries(trayFillingDetail.getBatchSeries());
				bean.setVariety(trayFillingDetail.getVariety());
				bean.setSeedlingType(trayFillingDetail.getSeedlingType());
				bean.setBatchNo(trayFillingDetail.getBatchNo());
				bean.setStartTime(trayFillingDetail.getStartTime().toString());
				bean.setEndTime(trayFillingDetail.getEndTime().toString());
				bean.setTotalTime(trayFillingDetail.getTotalTime().toString());
				bean.setTrayType(trayFillingDetail.getTrayType());
				bean.setNoOfCavitiesPerTray(trayFillingDetail.getNoOfCavitiesPerTray());
				bean.setTotalTrays(trayFillingDetail.getTotalTrays());
				bean.setTotalNoOfSeedlingss(trayFillingDetail.getTotalNoOfSeedlingss());

				String addedvariety1 = trayFillingDetail.getVariety();
				int addedvariety = Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				bean.setVarietyName(varietyName);
				beans.add(bean);
			}
		}
		return beans;
	}

	private TrayFillingSummaryBean prepareTrayFillingSummary(List<TrayFillingSummary> trayFillingSummary)
	{
		TrayFillingSummaryBean bean = null;
		if (trayFillingSummary != null)
		{
			bean = new TrayFillingSummaryBean();
			bean.setId(trayFillingSummary.get(0).getId());
			bean.setSeason(trayFillingSummary.get(0).getSeason());
			Date gradingdate = trayFillingSummary.get(0).getTrayFillingDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(gradingdate);
			bean.setDate(date);
			bean.setShift(trayFillingSummary.get(0).getShift());
			bean.setLabourSpplr(trayFillingSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(trayFillingSummary.get(0).getNoOfMen());
			bean.setManCost(trayFillingSummary.get(0).getManCost());
			bean.setNoOfWomen(trayFillingSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(trayFillingSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(trayFillingSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(trayFillingSummary.get(0).getCostPerTon());
			bean.setTotalTons(trayFillingSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(trayFillingSummary.get(0).getTotalContractAmt());
			bean.setMenTotal(trayFillingSummary.get(0).getMenTotal());
			bean.setWomenCost(trayFillingSummary.get(0).getWomanCost());
			bean.setModifyFlag("Yes");
			bean.setMachineOrMen(trayFillingSummary.get(0).getMachineOrMen());
			bean.setNoOfMachines(trayFillingSummary.get(0).getNoOfMachines());
			bean.setTotalNoOfSeedlings(trayFillingSummary.get(0).getTotalNoOfSeedlings());
			bean.setLc(trayFillingSummary.get(0).getLc());
			bean.setTotalCost(trayFillingSummary.get(0).getTotalCost());
		}
		return bean;
	}

	@RequestMapping(value = "/saveGreenHouseDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveGreenHouseDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			GreenHouseSummaryBean greenHouseSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        GreenHouseSummaryBean.class);
			GreenHouseSummary greenHouseSummary = prepareModelforGreenHouseSummary(greenHouseSummaryBean);
			entityList.add(greenHouseSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				GreenHouseDetailsBean greenHouseDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        GreenHouseDetailsBean.class);
				GreenHouseDetails greenHouseDetails = prepareModelforGreenHouseDetails(greenHouseDetailsBean,
				        greenHouseSummaryBean);
				entityList.add(greenHouseDetails);
			}
			byte labourstype = greenHouseSummaryBean.getLabourSpplr();
			String lcAccCode = null;
			if(labourstype!=0)
			{
				List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(greenHouseSummaryBean.getLc());
				lcAccCode = labourContractor.get(0).getGlcode();
			}
			
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			

			String flag = greenHouseSummaryBean.getModifyFlag();
			String season = greenHouseSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = greenHouseSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = greenHouseSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = greenHouseSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = greenHouseSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			
			
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        greenHouseSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private GreenHouseSummary prepareModelforGreenHouseSummary(GreenHouseSummaryBean summaryBean)
	{
		Integer maxId = commonService.getMaxID("GreenHouseSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		GreenHouseSummary summaryModel = new GreenHouseSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}

		String season = summaryBean.getSeason();
		Integer shift = summaryBean.getShift();
		String summarydate = summaryBean.getDate();
		Date date = DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		reasearchAndDevelopmentService.deleteGreenHouseDetails(season, todayDate, shift);
		summaryModel.setSeason(summaryBean.getSeason());
		String gradingdate = summaryBean.getDate();
		summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setShift(summaryBean.getShift());
		summaryModel.setLabourSpplr(summaryBean.getLabourSpplr());
		summaryModel.setNoOfMen(summaryBean.getNoOfMen());
		summaryModel.setManCost(summaryBean.getManCost());
		summaryModel.setMenTotal(summaryBean.getMenTotal());
		summaryModel.setNoOfWomen(summaryBean.getNoOfWomen());
		summaryModel.setWomanCost(summaryBean.getWomenCost());
		summaryModel.setWomenTotal(summaryBean.getWomenTotal());
		summaryModel.setTotalLabourCost(summaryBean.getTotalLabourCost());
		summaryModel.setCostPerTon(summaryBean.getCostPerTon());
		summaryModel.setTotalTons(summaryBean.getTotalTons());
		summaryModel.setTotalContractAmt(summaryBean.getTotalContractAmt());
		summaryModel.setGrandTotalIn(summaryBean.getGrandTotalIn());
		summaryModel.setGrandTotalOut(summaryBean.getGrandTotalOut());
		summaryModel.setLc(summaryBean.getLc());
		return summaryModel;
	}

	private GreenHouseDetails prepareModelforGreenHouseDetails(GreenHouseDetailsBean detailsBean,
	        GreenHouseSummaryBean summaryBean)
	{
		GreenHouseDetails modelDetials = new GreenHouseDetails();

		modelDetials.setId(detailsBean.getId());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setShift(summaryBean.getShift());
		String summarydate = summaryBean.getDate();
		modelDetials.setDetailsDate(DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setBatchSeries(detailsBean.getBatch());
		modelDetials.setVariety(detailsBean.getVariety());
		int traytypein=detailsBean.getTrayTypeIn();
		String traytypein1=Integer.toString(traytypein);
		modelDetials.setTrayTypeIn(traytypein1);
		modelDetials.setNoOfTraysIn(detailsBean.getNoOfTraysIn());
		modelDetials.setNoOfCavitiesPertrayin(detailsBean.getNoOfCavitiesPertrayin());
		modelDetials.setTotalSeedlingsIn(detailsBean.getTotalSeedlingsIn());
		int traytypeout=detailsBean.getTrayTypeOut();
		String traytypeout1=Integer.toString(traytypeout);
		modelDetials.setTrayTypeOut(traytypeout1);
		modelDetials.setNoOfTraysOut(detailsBean.getNoOfTraysOut());
		modelDetials.setNoOfCavitiesPertrayOut(detailsBean.getNoOfCavitiesPertrayOut());
		modelDetials.setTotalSeedlingsOut(detailsBean.getTotalSeedlingsOut());

		return modelDetials;
	}

	@RequestMapping(value = "/saveTrimmingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveTrimmingDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			TrimmingSummaryBean trimmingSummaryBean = new ObjectMapper()
			        .readValue(formdata.toString(), TrimmingSummaryBean.class);
			TrimmingSummary trimmingSummary = prepareModelfortrimmingSummary(trimmingSummaryBean);
			entityList.add(trimmingSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				TrimmingDetailsBean trimmingDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        TrimmingDetailsBean.class);
				TrimmingDetails trimmingDetails = prepareModelfortrimmingDetails(trimmingDetailsBean, trimmingSummaryBean);
				entityList.add(trimmingDetails);
			}
			byte labourstype = trimmingSummaryBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(trimmingSummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			Map AccountSummaryMap = new HashMap();
			Map AccountGroupMap = new HashMap();
			Map AccountSubGroupMap = new HashMap();
			List AccountsList = new ArrayList();

			List AccountCodeList = new ArrayList();
			List AccountGrpCodeList = new ArrayList();
			List AccountSubGrpCodeList = new ArrayList();
			
			
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

			String flag = trimmingSummaryBean.getModifyFlag();
			String season = trimmingSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = trimmingSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = trimmingSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = trimmingSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = trimmingSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        trimmingSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private TrimmingSummary prepareModelfortrimmingSummary(TrimmingSummaryBean summaryBean)
	{
		Integer maxId = commonService.getMaxID("TrimmingSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		TrimmingSummary summaryModel = new TrimmingSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}

		String season = summaryBean.getSeason();
		Integer shift = summaryBean.getShift();
		String summarydate = summaryBean.getDate();
		Date date = DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		reasearchAndDevelopmentService.deleteTrimmingDetails(season, todayDate, shift);
		summaryModel.setSeason(summaryBean.getSeason());
		String gradingdate = summaryBean.getDate();
		summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setShift(summaryBean.getShift());
		summaryModel.setLabourSpplr(summaryBean.getLabourSpplr());
		summaryModel.setNoOfMen(summaryBean.getNoOfMen());
		summaryModel.setManCost(summaryBean.getManCost());
		summaryModel.setMenTotal(summaryBean.getMenTotal());
		summaryModel.setNoOfWomen(summaryBean.getNoOfWomen());
		summaryModel.setWomanCost(summaryBean.getWomenCost());
		summaryModel.setWomenTotal(summaryBean.getWomenTotal());
		summaryModel.setTotalLabourCost(summaryBean.getTotalLabourCost());
		summaryModel.setCostPerTon(summaryBean.getCostPerTon());
		summaryModel.setTotalTons(summaryBean.getTotalTons());
		summaryModel.setTotalContractAmt(summaryBean.getTotalContractAmt());
		summaryModel.setGrandTotalIn(summaryBean.getGrandTotalIn());
		summaryModel.setGrandTotalOut(summaryBean.getGrandTotalOut());
		summaryModel.setLc(summaryBean.getLc());

		return summaryModel;
	}

	private TrimmingDetails prepareModelfortrimmingDetails(TrimmingDetailsBean detailsBean, TrimmingSummaryBean summaryBean)
	{
		TrimmingDetails modelDetials = new TrimmingDetails();

		modelDetials.setId(detailsBean.getId());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setShift(summaryBean.getShift());
		String summarydate = summaryBean.getDate();
		modelDetials.setDetailsDate(DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setBatchSeries(detailsBean.getBatch());
		modelDetials.setVariety(detailsBean.getVariety());
		
	
		int traytypein=detailsBean.getTrayTypeIn();
		String traytypein1=Integer.toString(traytypein);
		int traytypeout=detailsBean.getTrayTypeOut();
		String traytypeout1=Integer.toString(traytypeout);
		modelDetials.setTrayTypeIn(traytypein1);
		modelDetials.setTrayTypeOut(traytypeout1);
		
		modelDetials.setNoOfTraysIn(detailsBean.getNoOfTraysIn());
		modelDetials.setNoOfCavitiesPertrayin(detailsBean.getNoOfCavitiesPertrayin());
		modelDetials.setTotalSeedlingsIn(detailsBean.getTotalSeedlingsIn());
		modelDetials.setNoOfTraysOut(detailsBean.getNoOfTraysOut());
		modelDetials.setNoOfCavitiesPertrayOut(detailsBean.getNoOfCavitiesPertrayOut());
		modelDetials.setTotalSeedlingsOut(detailsBean.getTotalSeedlingsOut());

		return modelDetials;
	}

	@RequestMapping(value = "/SaveHardeningDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveHardeningDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		List AccountsList = new ArrayList();

		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			HardeningSummaryBean hardeningSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        HardeningSummaryBean.class);
			HardeningSummary hardeningSummary = prepareModelforHardeningSummary(hardeningSummaryBean);
			entityList.add(hardeningSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				HardeningDetailsBean hardeningDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        HardeningDetailsBean.class);
				HardeningDetails hardeningDetails = prepareModelforHardeningDetails(hardeningDetailsBean, hardeningSummaryBean);
				entityList.add(hardeningDetails);
			}
			byte labourstype = hardeningSummaryBean.getLabourSpplr();
			String lcAccCode =null;
			if (labourstype != 0)
			{
			List<LabourContractor> labourContractor = ahFuctionalService.getLabourContractorAccCode(hardeningSummaryBean.getLc());
			lcAccCode = labourContractor.get(0).getGlcode();
			}
			String strQry = "";
			String Qryobj = "";
			String userId = getLoggedInUserName();

		

			String flag = hardeningSummaryBean.getModifyFlag();
			String season = hardeningSummaryBean.getSeason();

			int transactionCode = 0;
			transactionCode = commonService.GetMaxTransCode(season);
			String loginuser = "";

			String accCode = "PaySour";
			String strglCode = "LabChar";
			String journalMemo = "For Labour Charges";
			String TransactionType = "Cr";
			double Amt=0;
			if (labourstype == 0)
			{
				strglCode = "LabChar";
				journalMemo = "For Labour Charges";
				 Amt = hardeningSummaryBean.getTotalLabourCost();

			}
			else
			{
				strglCode = lcAccCode;
				journalMemo = "For Contract Charge";
				 Amt = hardeningSummaryBean.getTotalContractAmt();
			}
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			/////Dr Transaction 

			if (labourstype == 0)
			{
				accCode = "LabChar";
				journalMemo = "For Labour Charges";
				Amt = hardeningSummaryBean.getTotalLabourCost();


			}
			else
			{
				accCode = lcAccCode;
				journalMemo = "For Contract Charges";
				Amt = hardeningSummaryBean.getTotalContractAmt();

			}

			strglCode = "PaySour";
			TransactionType = "Dr";

			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}

			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        hardeningSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private HardeningSummary prepareModelforHardeningSummary(HardeningSummaryBean summaryBean)
	{
		Integer maxId = commonService.getMaxID("HardeningSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		HardeningSummary summaryModel = new HardeningSummary();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}

		String season = summaryBean.getSeason();
		Integer shift = summaryBean.getShift();
		String summarydate = summaryBean.getDate();
		Date date = DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		reasearchAndDevelopmentService.deleteHardeningDetails(season, todayDate, shift);
		summaryModel.setSeason(summaryBean.getSeason());
		String gradingdate = summaryBean.getDate();
		summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(gradingdate, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setShift(summaryBean.getShift());
		summaryModel.setLabourSpplr(summaryBean.getLabourSpplr());
		summaryModel.setNoOfMen(summaryBean.getNoOfMen());
		summaryModel.setManCost(summaryBean.getManCost());
		summaryModel.setMenTotal(summaryBean.getMenTotal());
		summaryModel.setNoOfWomen(summaryBean.getNoOfWomen());
		summaryModel.setWomanCost(summaryBean.getWomenCost());
		summaryModel.setWomenTotal(summaryBean.getWomenTotal());
		summaryModel.setTotalLabourCost(summaryBean.getTotalLabourCost());
		summaryModel.setCostPerTon(summaryBean.getCostPerTon());
		summaryModel.setTotalTons(summaryBean.getTotalTons());
		summaryModel.setTotalContractAmt(summaryBean.getTotalContractAmt());
		summaryModel.setGrandTotalIn(summaryBean.getGrandTotalIn());
		summaryModel.setGrandTotalOut(summaryBean.getGrandTotalOut());
		summaryModel.setLc(summaryBean.getLc());

		return summaryModel;
	}

	private HardeningDetails prepareModelforHardeningDetails(HardeningDetailsBean detailsBean, HardeningSummaryBean summaryBean)
	{
		HardeningDetails modelDetials = new HardeningDetails();

		modelDetials.setId(detailsBean.getId());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setShift(summaryBean.getShift());
		String summarydate = summaryBean.getDate();
		modelDetials.setDetailsDate(DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setBatchSeries(detailsBean.getBatch());
		modelDetials.setVariety(detailsBean.getVariety());
		
		int traytypein=detailsBean.getTrayTypeIn();
		String traytypein1=Integer.toString(traytypein);
		int traytypeout=detailsBean.getTrayTypeOut();
		String traytypeout1=Integer.toString(traytypeout);
		
		modelDetials.setTrayTypeIn(traytypein1);
		modelDetials.setTrayTypeOut(traytypeout1);
		
		modelDetials.setTrayTypeIn(traytypein1);
		modelDetials.setNoOfTraysIn(detailsBean.getNoOfTraysIn());
		modelDetials.setNoOfCavitiesPertrayin(detailsBean.getNoOfCavitiesPertrayin());
		modelDetials.setTotalSeedlingsIn(detailsBean.getTotalSeedlingsIn());
		modelDetials.setTrayTypeOut(traytypeout1);
		modelDetials.setNoOfTraysOut(detailsBean.getNoOfTraysOut());
		modelDetials.setNoOfCavitiesPertrayOut(detailsBean.getNoOfCavitiesPertrayOut());
		modelDetials.setTotalSeedlingsOut(detailsBean.getTotalSeedlingsOut());

		return modelDetials;
	}

	@RequestMapping(value = "/getAllGreenHouseDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllGreenHouseDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String Date = (String) jsonData.get("summaryDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date summaryDate = DateUtils.getSqlDateFromString(Date, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(summaryDate);

		List<GreenHouseSummary> greenHouseSummary = reasearchAndDevelopmentService.listAllGreenHouseSummary(Season, todayDate,
		        Shift);
		GreenHouseSummaryBean greenHouseSummaryBean = prepareGreenHouseSummary(greenHouseSummary);

		List<GreenHouseDetails> greenHouseDetails = reasearchAndDevelopmentService.listAllGreenHouseDetails(Season, todayDate,
		        Shift);
		List<GreenHouseDetailsBean> greenHouseDetailsBean = prepareGreenHouseDetails(greenHouseDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(greenHouseSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(greenHouseDetailsBean);

		response.put("GreenHouseSummary", SummaryJson);
		response.put("GreenHouseDetails", DetailsArray);

		logger.info("------getAllGreenHouseDetails-----" + response.toString());
		return response.toString();

	}

	private List<GreenHouseDetailsBean> prepareGreenHouseDetails(List<GreenHouseDetails> greenHouseDetails)
	{
		List<GreenHouseDetailsBean> beans = null;
		if (greenHouseDetails != null && !greenHouseDetails.isEmpty())
		{
			beans = new ArrayList<GreenHouseDetailsBean>();
			GreenHouseDetailsBean bean = null;
			for (GreenHouseDetails greenHouseDetail : greenHouseDetails)
			{
				bean = new GreenHouseDetailsBean();
				bean.setId(greenHouseDetail.getId());
				bean.setSeason(greenHouseDetail.getSeason());
				bean.setShift(greenHouseDetail.getShift());
				Date gradingdate = greenHouseDetail.getDetailsDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(gradingdate);
				bean.setDate(date);
				bean.setBatch(greenHouseDetail.getBatchSeries());
				bean.setVariety(greenHouseDetail.getVariety());
				String traytypeout1=greenHouseDetail.getTrayTypeOut();
				String traytypein1=greenHouseDetail.getTrayTypeIn();
				int traytypeout=Integer.parseInt(traytypeout1);
				int traytypein=Integer.parseInt(traytypein1);
				bean.setTrayTypeIn(traytypein);
				bean.setNoOfTraysIn(greenHouseDetail.getNoOfTraysIn());
				bean.setNoOfCavitiesPertrayin(greenHouseDetail.getNoOfCavitiesPertrayin());
				bean.setTotalSeedlingsIn(greenHouseDetail.getTotalSeedlingsIn());
				bean.setNoOfCavitiesPertrayOut(greenHouseDetail.getNoOfCavitiesPertrayOut());
				bean.setTrayTypeOut(traytypeout);
				bean.setNoOfTraysOut(greenHouseDetail.getNoOfTraysOut());
				bean.setTotalSeedlingsOut(greenHouseDetail.getTotalSeedlingsOut());

				String addedvariety1 = greenHouseDetail.getVariety();
				int addedvariety = Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				bean.setVarietyName(varietyName);

				beans.add(bean);
			}
		}
		return beans;
	}

	private GreenHouseSummaryBean prepareGreenHouseSummary(List<GreenHouseSummary> greenHouseSummary)
	{
		GreenHouseSummaryBean bean = null;
		if (greenHouseSummary != null)
		{
			bean = new GreenHouseSummaryBean();
			bean.setId(greenHouseSummary.get(0).getId());
			bean.setSeason(greenHouseSummary.get(0).getSeason());
			Date gradingdate = greenHouseSummary.get(0).getSummaryDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(gradingdate);
			bean.setDate(date);
			bean.setShift(greenHouseSummary.get(0).getShift());
			bean.setLabourSpplr(greenHouseSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(greenHouseSummary.get(0).getNoOfMen());
			bean.setManCost(greenHouseSummary.get(0).getManCost());
			bean.setNoOfWomen(greenHouseSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(greenHouseSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(greenHouseSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(greenHouseSummary.get(0).getCostPerTon());
			bean.setTotalTons(greenHouseSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(greenHouseSummary.get(0).getTotalContractAmt());
			bean.setMenTotal(greenHouseSummary.get(0).getMenTotal());
			bean.setWomenCost(greenHouseSummary.get(0).getWomanCost());
			bean.setModifyFlag("Yes");
			bean.setGrandTotalIn(greenHouseSummary.get(0).getGrandTotalIn());
			bean.setGrandTotalOut(greenHouseSummary.get(0).getGrandTotalOut());
			bean.setLc(greenHouseSummary.get(0).getLc());

		}
		return bean;
	}

	@RequestMapping(value = "/getAllHardenningDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllHardenningDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String Date = (String) jsonData.get("summaryDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date summaryDate = DateUtils.getSqlDateFromString(Date, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(summaryDate);

		List<HardeningSummary> hardeningSummary = reasearchAndDevelopmentService.listAllHardenningSummary(Season, todayDate,
		        Shift);
		HardeningSummaryBean hardeningSummaryBean = prepareHardenningSummary(hardeningSummary);

		List<HardeningDetails> hardeningDetails = reasearchAndDevelopmentService.listAllHardenningDetails(Season, todayDate,
		        Shift);
		List<HardeningDetailsBean> hardeningDetailsBean = prepareHardenningDetails(hardeningDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(hardeningSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(hardeningDetailsBean);

		response.put("HardenningSummary", SummaryJson);
		response.put("HardenningDetails", DetailsArray);

		logger.info("------getAllHardenningDetails-----" + response.toString());
		return response.toString();

	}

	private List<HardeningDetailsBean> prepareHardenningDetails(List<HardeningDetails> hardeningDetails)
	{
		List<HardeningDetailsBean> beans = null;
		if (hardeningDetails != null && !hardeningDetails.isEmpty())
		{
			beans = new ArrayList<HardeningDetailsBean>();
			HardeningDetailsBean bean = null;
			for (HardeningDetails hardeningDetail : hardeningDetails)
			{
				bean = new HardeningDetailsBean();
				bean.setId(hardeningDetail.getId());
				bean.setSeason(hardeningDetail.getSeason());
				bean.setShift(hardeningDetail.getShift());
				Date gradingdate = hardeningDetail.getDetailsDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(gradingdate);
				bean.setDate(date);
				bean.setBatch(hardeningDetail.getBatchSeries());
				bean.setVariety(hardeningDetail.getVariety());
				
				String traytypeout1=hardeningDetail.getTrayTypeOut();
				String traytypein1=hardeningDetail.getTrayTypeIn();
				int traytypeout=Integer.parseInt(traytypeout1);
				int traytypein=Integer.parseInt(traytypein1);
				bean.setTrayTypeIn(traytypein);
				
				bean.setTrayTypeIn(traytypein);
				bean.setNoOfTraysIn(hardeningDetail.getNoOfTraysIn());
				bean.setNoOfCavitiesPertrayin(hardeningDetail.getNoOfCavitiesPertrayin());
				bean.setTotalSeedlingsIn(hardeningDetail.getTotalSeedlingsIn());
				bean.setNoOfCavitiesPertrayOut(hardeningDetail.getNoOfCavitiesPertrayOut());
				bean.setTrayTypeOut(traytypeout);
				bean.setNoOfTraysOut(hardeningDetail.getNoOfTraysOut());
				bean.setTotalSeedlingsOut(hardeningDetail.getTotalSeedlingsOut());
				String  addedvariety1 = hardeningDetail.getVariety();
				int addedvariety=Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				bean.setVarietyName(varietyName);
				beans.add(bean);
			}
		}
		return beans;
	}

	private HardeningSummaryBean prepareHardenningSummary(List<HardeningSummary> hardeningSummary)
	{
		HardeningSummaryBean bean = null;
		if (hardeningSummary != null)
		{
			bean = new HardeningSummaryBean();
			bean.setId(hardeningSummary.get(0).getId());
			bean.setSeason(hardeningSummary.get(0).getSeason());
			Date gradingdate = hardeningSummary.get(0).getSummaryDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(gradingdate);
			bean.setDate(date);
			bean.setShift(hardeningSummary.get(0).getShift());
			bean.setLabourSpplr(hardeningSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(hardeningSummary.get(0).getNoOfMen());
			bean.setManCost(hardeningSummary.get(0).getManCost());
			bean.setNoOfWomen(hardeningSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(hardeningSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(hardeningSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(hardeningSummary.get(0).getCostPerTon());
			bean.setTotalTons(hardeningSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(hardeningSummary.get(0).getTotalContractAmt());
			bean.setMenTotal(hardeningSummary.get(0).getMenTotal());
			bean.setWomenCost(hardeningSummary.get(0).getWomanCost());
			bean.setModifyFlag("Yes");
			bean.setGrandTotalIn(hardeningSummary.get(0).getGrandTotalIn());
			bean.setGrandTotalOut(hardeningSummary.get(0).getGrandTotalOut());
			bean.setLc(hardeningSummary.get(0).getLc());

		}
		return bean;
	}

	@RequestMapping(value = "/getAllTrimmingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllTrimmingDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String Date = (String) jsonData.get("summaryDate");
		int Shift = (Integer) jsonData.get("shift");

		Date currentDate = new Date();

		Date summaryDate = DateUtils.getSqlDateFromString(Date, Constants.GenericDateFormat.DATE_FORMAT);
		String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(summaryDate);

		List<TrimmingSummary> trimmingSummary = reasearchAndDevelopmentService.listAllTrimmingSummary(Season, todayDate, Shift);
		TrimmingSummaryBean trimmingSummaryBean = prepareTrimmingSummary(trimmingSummary);

		List<TrimmingDetails> trimmingDetails = reasearchAndDevelopmentService.listAllTrimmingDetails(Season, todayDate, Shift);
		List<TrimmingDetailsBean> trimmingDetailsBean = prepareTrimmingDetails(trimmingDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(trimmingSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(trimmingDetailsBean);

		response.put("TrimmingSummary", SummaryJson);
		response.put("TrimmingDetails", DetailsArray);

		logger.info("------getAllTrimmingDetails-----" + response.toString());
		return response.toString();

	}

	private List<TrimmingDetailsBean> prepareTrimmingDetails(List<TrimmingDetails> trimmingDetails)
	{
		List<TrimmingDetailsBean> beans = null;
		if (trimmingDetails != null && !trimmingDetails.isEmpty())
		{
			beans = new ArrayList<TrimmingDetailsBean>();
			TrimmingDetailsBean bean = null;
			for (TrimmingDetails trimmingDetail : trimmingDetails)
			{
				bean = new TrimmingDetailsBean();
				bean.setId(trimmingDetail.getId());
				bean.setSeason(trimmingDetail.getSeason());
				bean.setShift(trimmingDetail.getShift());
				Date gradingdate = trimmingDetail.getDetailsDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(gradingdate);
				bean.setDate(date);
				bean.setBatch(trimmingDetail.getBatchSeries());
				bean.setVariety(trimmingDetail.getVariety());
				
				int traytypeout=Integer.parseInt(trimmingDetail.getTrayTypeOut());
				int traytypein=Integer.parseInt(trimmingDetail.getTrayTypeIn());
				bean.setTrayTypeIn(traytypein);
				bean.setNoOfTraysIn(trimmingDetail.getNoOfTraysIn());
				bean.setNoOfCavitiesPertrayin(trimmingDetail.getNoOfCavitiesPertrayin());
				bean.setTotalSeedlingsIn(trimmingDetail.getTotalSeedlingsIn());
				bean.setNoOfCavitiesPertrayOut(trimmingDetail.getNoOfCavitiesPertrayOut());
				bean.setTrayTypeOut(traytypeout);
				bean.setNoOfTraysOut(trimmingDetail.getNoOfTraysOut());
				bean.setTotalSeedlingsOut(trimmingDetail.getTotalSeedlingsOut());
				
				String  addedvariety1 = trimmingDetail.getVariety();
				int addedvariety=Integer.parseInt(addedvariety1);
				List VarietyList = new ArrayList();
				String varietyName = "";
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(addedvariety);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				bean.setVarietyName(varietyName);
				
				beans.add(bean);
			}
		}
		return beans;
	}

	private TrimmingSummaryBean prepareTrimmingSummary(List<TrimmingSummary> trimmingSummary)
	{
		TrimmingSummaryBean bean = null;
		if (trimmingSummary != null)
		{
			bean = new TrimmingSummaryBean();
			bean.setId(trimmingSummary.get(0).getId());
			bean.setSeason(trimmingSummary.get(0).getSeason());
			Date gradingdate = trimmingSummary.get(0).getSummaryDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(gradingdate);
			bean.setDate(date);
			bean.setShift(trimmingSummary.get(0).getShift());
			bean.setLabourSpplr(trimmingSummary.get(0).getLabourSpplr());
			bean.setNoOfMen(trimmingSummary.get(0).getNoOfMen());
			bean.setManCost(trimmingSummary.get(0).getManCost());
			bean.setNoOfWomen(trimmingSummary.get(0).getNoOfWomen());
			bean.setWomenTotal(trimmingSummary.get(0).getWomenTotal());
			bean.setTotalLabourCost(trimmingSummary.get(0).getTotalLabourCost());
			bean.setCostPerTon(trimmingSummary.get(0).getCostPerTon());
			bean.setTotalTons(trimmingSummary.get(0).getTotalTons());
			bean.setTotalContractAmt(trimmingSummary.get(0).getTotalContractAmt());
			bean.setMenTotal(trimmingSummary.get(0).getMenTotal());
			bean.setWomenCost(trimmingSummary.get(0).getWomanCost());
			bean.setModifyFlag("Yes");
			bean.setGrandTotalIn(trimmingSummary.get(0).getGrandTotalIn());
			bean.setGrandTotalOut(trimmingSummary.get(0).getGrandTotalOut());
			bean.setLc(trimmingSummary.get(0).getLc());

		}
		return bean;
	}

	@RequestMapping(value = "/getAllKindTypeDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllKindTypeDetails() throws Exception
	{
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		JSONArray jArray = new JSONArray();
		List DateList = reasearchAndDevelopmentService.listAllKindTypeDetails();
		logger.info("getAllKindTypeDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedbatches = (String) dateMap.get("kindType");
				int id = (Integer) dateMap.get("id");
				jsonObj.put("KindType", addedbatches);
				jsonObj.put("id", id);

				jArray.add(jsonObj);
			}
		}
		logger.info("getAllKindTypeDetails() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getAllKindDetailsByType", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllKindDetailsByType(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();

		org.json.JSONObject response = new org.json.JSONObject();
		int id = (Integer) jsonData.get("kindType");
		List DateList = reasearchAndDevelopmentService.listAllKindDetailsByType(id);
		logger.info("getAllKindDetailsByType() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{

			Map dateMap = new HashMap();
			dateMap = (Map) DateList.get(0);
			String uom = (String) dateMap.get("uom");
			jsonObj.put("uom", uom);
			jArray.add(jsonObj);
		}
		logger.info("getAllKindDetailsByType() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/SaveSeedlingIndentSummary", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveSeedlingIndentSummary(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			KindIndentSummaryBean seedlingIndentSummaryBean = new ObjectMapper().readValue(formdata.toString(),KindIndentSummaryBean.class);
			KindIndentSummary seedlingIndentSummary = prepareModelforIndentSummary(seedlingIndentSummaryBean);
			entityList.add(seedlingIndentSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				KindIndentDetailsBean seedlingIndentDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        KindIndentDetailsBean.class);
				KindIndentDetails seedlingIndentDetails = prepareModelforIndentDetails(seedlingIndentDetailsBean,
				        seedlingIndentSummaryBean);
				entityList.add(seedlingIndentDetails);
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        seedlingIndentSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private KindIndentSummary prepareModelforIndentSummary(KindIndentSummaryBean summaryBean)
	{
		int status = 0;
		Integer maxId = commonService.getMaxID("KindIndentSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		KindIndentSummary summaryModel = new KindIndentSummary();
		status = GetloginuserStatus();
		String userId = getLoggedInUserName();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}
		String season = summaryBean.getSeason();
		String indentno = summaryBean.getIndentNo();
		reasearchAndDevelopmentService.deleteIndentDetails(season, indentno);
		String indentno1[] = indentno.split("/");
		String batchno = indentno1[0].substring(1);
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);
		batchno = batchno.substring(1);
		int Seqno = Integer.parseInt(batchno);
		summaryModel.setSeqNo(Seqno);
		//String gradingdate = summaryBean.getDate();
		//summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(gradingdate,Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setIndentNo(summaryBean.getIndentNo());
		summaryModel.setSeason(summaryBean.getSeason());
		summaryModel.setRyotCode(summaryBean.getRyotCode());
		summaryModel.setRyotName(summaryBean.getRyotName());
		summaryModel.setVillage(summaryBean.getVillage());
		summaryModel.setFatherName(summaryBean.getFatherName());
		summaryModel.setAadhaarNumber(summaryBean.getAadhaarNumber());
		summaryModel.setPhoneNumber(summaryBean.getPhoneNumber());
		summaryModel.setFieldMan(summaryBean.getFieldMan());
		summaryModel.setDispatchNo("NA");
		summaryModel.setIndentStatus(status);
		summaryModel.setFoCode(summaryBean.getFoCode());
		///Code added on 20-2-2017
		summaryModel.setStorecode(summaryBean.getStoreCode());
		String authdate = summaryBean.getAuthorisationDate();
		String indentdate = summaryBean.getIndentedDate();
		///Code added on 24-2-2017
		summaryModel.setAgreementNo(summaryBean.getAgreementNo());
		summaryModel.setNoOfAcr(summaryBean.getNoOfAcr());

		summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(indentdate,Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setLoginUser(userId);
		Date date = new Date();
		summaryModel.setAuthorisationdate(date);

		java.util.Date datendtime = new java.util.Date();
		summaryModel.setSummaryTime(new Timestamp(datendtime.getTime()));

		int feildempofficer = 0;
	
			List DateList2 = reasearchAndDevelopmentService.GetFeildOfficerempid(summaryBean.getFoCode());
			if (DateList2 != null && DateList2.size() > 0)
			{
				Map dMap1 = new HashMap();
				dMap1 = (Map) DateList2.get(0);
				feildempofficer = (Integer) dMap1.get("employeeId");
			}
		
		summaryModel.setFieldofficerEmpId(feildempofficer);
		summaryModel.setZone(summaryBean.getZone());

		return summaryModel;
	}

	private KindIndentDetails prepareModelforIndentDetails(KindIndentDetailsBean detailsBean, KindIndentSummaryBean summaryBean)
	{
		KindIndentDetails modelDetials = new KindIndentDetails();
		String userId = getLoggedInUserName();
		int status = GetloginuserStatus();

		modelDetials.setId(detailsBean.getId());
		modelDetials.setKind(detailsBean.getKind());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setIndentNo(summaryBean.getIndentNo());
		String detailsdate = detailsBean.getBatchDate();
		modelDetials.setBatchDate(DateUtils.getSqlDateFromString(detailsdate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setQuantity(detailsBean.getQuantity());
		modelDetials.setUom(detailsBean.getUom());
		modelDetials.setDispatchNo("NA");
		modelDetials.setLandVillageCode(detailsBean.getLandVillageCode());
		modelDetials.setIndentStatus(status);
		String indentno = summaryBean.getIndentNo();
		String indentno1[] = indentno.split("/");
		String batchno = indentno1[0].substring(1);
		batchno = batchno.substring(1);
		int Seqno = Integer.parseInt(batchno);
		modelDetials.setSeqNo(Seqno);
		modelDetials.setDispatchedQuantity(0.00);
		modelDetials.setPendingQuantity(detailsBean.getQuantity());
		modelDetials.setVariety(detailsBean.getVariety());
		modelDetials.setRyotCode(summaryBean.getRyotCode());

		return modelDetials;
	}

	@RequestMapping(value = "/getAllModifySeedlingIndentDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllModifySeedlingIndentDetails(@RequestBody String jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		List<KindIndentSummary> kindIndentSummary = reasearchAndDevelopmentService.listGetAllIndentSummary(jsonData);
		KindIndentSummaryBean kindIndentSummaryBean = prepareindentSummary(kindIndentSummary);

		List<KindIndentDetails> kindIndentDetails = reasearchAndDevelopmentService.listGetAllIndentDetails(jsonData);
		List<KindIndentDetailsBean> kindIndentDetailsBean = prepareindentDetails(kindIndentDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(kindIndentSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(kindIndentDetailsBean);

		response.put("KindIndentSummary", SummaryJson);
		response.put("KindIndentDetails", DetailsArray);

		logger.info("------getAllModifySeedlingIndentDetails-----" + response.toString());
		return response.toString();
	}

	private List<KindIndentDetailsBean> prepareindentDetails(List<KindIndentDetails> seedlingIndentDetails)
	{
		List<KindIndentDetailsBean> beans = null;
		if (seedlingIndentDetails != null && !seedlingIndentDetails.isEmpty())
		{
			beans = new ArrayList<KindIndentDetailsBean>();
			KindIndentDetailsBean bean = null;
			for (KindIndentDetails seedlingIndentDetail : seedlingIndentDetails)
			{
				bean = new KindIndentDetailsBean();
				bean.setKind(seedlingIndentDetail.getKind());
				bean.setId(seedlingIndentDetail.getId());
				bean.setSeason(seedlingIndentDetail.getSeason());
				bean.setIndentNo(seedlingIndentDetail.getIndentNo());
				Date batchdate = seedlingIndentDetail.getBatchDate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String date = df.format(batchdate);
				bean.setBatchDate(date);
				bean.setQuantity(seedlingIndentDetail.getQuantity());
				bean.setUom(seedlingIndentDetail.getUom());
				bean.setLandVillageCode(seedlingIndentDetail.getLandVillageCode());
				bean.setVariety(seedlingIndentDetail.getVariety());
				bean.setStatus(seedlingIndentDetail.getIndentStatus());

				beans.add(bean);
			}
		}
		return beans;
	}

	private KindIndentSummaryBean prepareindentSummary(List<KindIndentSummary> seedlingIndentSummary)
	{
		KindIndentSummaryBean bean = null;
		if (seedlingIndentSummary != null)
		{//Code added on 20-2-2017
			bean = new KindIndentSummaryBean();
			bean.setId(seedlingIndentSummary.get(0).getId());
			bean.setSeason(seedlingIndentSummary.get(0).getSeason());
			bean.setIndentNo(seedlingIndentSummary.get(0).getIndentNo());
			bean.setRyotCode(seedlingIndentSummary.get(0).getRyotCode());
			bean.setRyotName(seedlingIndentSummary.get(0).getRyotName());
			bean.setVillage(seedlingIndentSummary.get(0).getVillage());
			bean.setFatherName(seedlingIndentSummary.get(0).getFatherName());
			bean.setAadhaarNumber(seedlingIndentSummary.get(0).getAadhaarNumber());
			bean.setPhoneNumber(seedlingIndentSummary.get(0).getPhoneNumber());
			bean.setFieldMan(seedlingIndentSummary.get(0).getFieldMan());
			bean.setFoCode(seedlingIndentSummary.get(0).getFoCode());
			bean.setStoreCode(seedlingIndentSummary.get(0).getStorecode());
			Date batchdate = seedlingIndentSummary.get(0).getSummaryDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(batchdate);
			bean.setIndentedDate(date);
			bean.setModifyFlag("Yes");
			bean.setIndentStatus(seedlingIndentSummary.get(0).getIndentStatus());
			bean.setZone(seedlingIndentSummary.get(0).getZone());
			///code added on 24-2-2017
			bean.setAgreementNo(seedlingIndentSummary.get(0).getAgreementNo());
			bean.setNoOfAcr(seedlingIndentSummary.get(0).getNoOfAcr());
		}
		return bean;
	}

	@RequestMapping(value = "/GetIndentsUserListsDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray GetIndentsUserLists(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String Status = (String) jsonData.get("status");
		String Dfromdate = (String) jsonData.get("fromDate");
		String Dtodate = (String) jsonData.get("toDate");
		Integer variety = (Integer) jsonData.get("varietyOfCane");
		Integer fieldman = (Integer) jsonData.get("fieldMan");
		String userId = getLoggedInUserName();
		Date fromDate = DateUtils.getSqlDateFromString(Dfromdate, Constants.GenericDateFormat.DATE_FORMAT);
		String Frmdate = new SimpleDateFormat("yyyy-MM-dd").format(fromDate);
		Date toDate = DateUtils.getSqlDateFromString(Dtodate, Constants.GenericDateFormat.DATE_FORMAT);
		String toDdate = new SimpleDateFormat("yyyy-MM-dd").format(toDate);

		List DateList = reasearchAndDevelopmentService.listGetIndents(Status, Season, Frmdate, toDdate, variety, fieldman);
		logger.info("GetIndentsUserListsDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			int feildempofficer = 0;
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String indentno = (String) dateMap.get("indentNo");
				Date dateofindent = (Date) dateMap.get("SummaryDate");
				String k2 = DateUtils.formatDate(dateofindent, Constants.GenericDateFormat.DATE_FORMAT);
				//String variety1  = (String) dateMap.get("Variety");
				Integer feildman = (Integer) dateMap.get("fieldMan");
				Integer foCode = (Integer) dateMap.get("foCode");
				//code added on 20-2-2017
				String ryotcode=(String) dateMap.get("ryotCode");
				String fieldofficername="";
					List DateList2 = reasearchAndDevelopmentService.GetFeildOfficerempid(foCode);
					if (DateList2 != null && DateList2.size() > 0)
					{
						Map dMap1 = new HashMap();
						dMap1 = (Map) DateList2.get(0);
						feildempofficer = (Integer) dMap1.get("employeeId");
						//code added on 27-2-2017
						fieldofficername = (String) dMap1.get("fieldOfficer");

					}
				
				Integer kind = (Integer) dateMap.get("kind");
				double qty = (Double) dateMap.get("quantity");
				String kindtype="";
				List DateList3 = reasearchAndDevelopmentService.listAllKindDetailsByType(kind);
				logger.info("getAllKindDetailsByType() ========DateList3==========" + DateList3);
				if (DateList3 != null && DateList3.size() > 0)
				{

					Map dateMap2 = new HashMap();
					dateMap2 = (Map) DateList3.get(0);
					kindtype = (String) dateMap2.get("kindType");
				}
				//String varietykind = kindtype + "," + "(" + qty + ")";
				jsonObj.put("indent", indentno);
				jsonObj.put("dateofIndent", k2);
				jsonObj.put("varietyOfCane", kindtype);
				//code added on 27-2-2017
				jsonObj.put("fieldOfficer", fieldofficername);
				jsonObj.put("ryotCode", ryotcode);

				jArray.add(jsonObj);
			}
		}
		logger.info("GetIndentsUserListsDetails() ========jArray==========" + jArray);
		return jArray;
	}

	public Integer GetloginuserStatus()
	{
		int status = 0;
		String userId = getLoggedInUserName();
		List DateList = reasearchAndDevelopmentService.listGetEmployeeType(userId);
		logger.info("listGetEmployeeType() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			Map dateMap = new HashMap();
			dateMap = (Map) DateList.get(0);
			status = (Integer) dateMap.get("empStatus");
		}
		return status;
	}

	@RequestMapping(value = "/SaveDispatchSummary", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveDispatchSummary(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();

		double delevered = 0;
		double balance = 0;
		double deliveredquantity = 0;
		double balancequantity = 0;
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			
			DispatchSummaryBean dispatchSummaryBean = new ObjectMapper()
	        .readValue(formdata.toString(), DispatchSummaryBean.class);
			
			double issuedquantity=0;
			double totquantity=dispatchSummaryBean.getSeedlingQty();
			String kkind="";
			for (int i = 0; i < griddata.size(); i++)
			{
				DispatchDetailsBean dispatchDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        DispatchDetailsBean.class);
				if(i==0)
					kkind=dispatchDetailsBean.getKind();
				DispatchDetails dispatchDetails = prepareModelforDispatchDetails(dispatchDetailsBean, dispatchSummaryBean,kkind);
				entityList.add(dispatchDetails);
				double dispatchedquantity = dispatchDetails.getNoOfSeedlings();//dispatchedquantity
				issuedquantity=issuedquantity+dispatchedquantity;
			}	
			//added by naidu on 29-01-2017
			double pendingqty=0.0;
			double pqty=totquantity-issuedquantity;
			if(pqty>0)
				pendingqty=pqty;
			DispatchSummary dispatchSummary = prepareModelforDispatchSummary(dispatchSummaryBean,issuedquantity,pendingqty);
			entityList.add(dispatchSummary);
			
			for (int i = 0; i < griddata.size(); i++)
			{
				DispatchDetailsBean dispatchDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),DispatchDetailsBean.class);
				
				DispatchDetails dispatchDetails = prepareModelforDispatchDetails(dispatchDetailsBean, dispatchSummaryBean,kkind);
				double dispatchedquantity = dispatchDetails.getNoOfSeedlings();//dispatchedquantity
				String batchseries = dispatchDetails.getBatch();
				byte batchstatus = dispatchDetails.getIsBatchCompleted();
				String indentno = dispatchDetails.getIndentNo();
				String dispatchSummaryno = dispatchSummary.getDispatchNo();
				Integer variety = dispatchDetails.getVariety();

				String season = dispatchSummary.getSeason();
				Date summarydata = dispatchSummary.getDateOfDispatch();
				//summaryModel.setDateOfDispatch(DateUtils.getSqlDateFromString(summarydata,Constants.GenericDateFormat.DATE_FORMAT));
				TrayTracker trayTracker = prepareModelforTrayTracker(dispatchDetailsBean, dispatchSummaryBean);
				entityList.add(trayTracker);
				String ryotcode = dispatchSummary.getRyotCode();
				Integer traystype = dispatchDetails.getTrayTypeCode();
				Double nooftrays = dispatchDetails.getNoOfTrays();

				List<KindIndentDetails> kindIndentDetails = reasearchAndDevelopmentService.listGetAllvarietyIndentDetails(indentno,variety);
				if (kindIndentDetails != null && !kindIndentDetails.isEmpty())
				{
					delevered = kindIndentDetails.get(0).getDispatchedQuantity();
					balance = kindIndentDetails.get(0).getPendingQuantity();
					String dispatchno = kindIndentDetails.get(0).getDispatchNo();
					if ("NA".equals(dispatchno)||"".equals(dispatchno)||dispatchno==null)
					{
						dispatchSummaryno = dispatchSummaryno;
					}
					else
					{
						dispatchSummaryno = dispatchno + ',' + dispatchSummaryno;
					}
					deliveredquantity = delevered + dispatchedquantity;
					balancequantity = balance - dispatchedquantity;

					String strQry = "";
					Object Qryobj = "";
					strQry = "UPDATE KindIndentDetails set indentStatus=" + 3 + ",dispatchNo='" + dispatchSummaryno
					        + "',dispatchedQuantity='" + deliveredquantity + "',pendingQuantity='" + balancequantity
					        + "' WHERE indentNo ='" + indentno + "' and variety= "+variety+" ";
					Qryobj = strQry;
					UpdateList.add(Qryobj);

					String strQry1 = "";
					Object Qryobj1 = "";
					strQry1 = "UPDATE KindIndentSummary set dispatchNo='" + dispatchSummaryno + "' WHERE indentNo ='" + indentno
					        + "'  ";
					Qryobj1 = strQry1;
					UpdateList.add(Qryobj1);
					
					

				}

				if (batchstatus != 0)
				{

					List<BatchMaster> batchMaster = reasearchAndDevelopmentService.getbatchMasterDetailsforupdate(batchseries);

					if (batchMaster != null && !batchMaster.isEmpty())
					{
						delevered = batchMaster.get(0).getDelivered();
						balance = batchMaster.get(0).getBalance();
						deliveredquantity = delevered + dispatchedquantity;
						balancequantity = balance - dispatchedquantity;
					}

					String strQry2 = "";
					Object Qryobj2 = "";
					strQry2 = "UPDATE BatchMaster set isBatchCompleted=" + 1 + ", balance='" + balancequantity + "',delivered='"
					        + deliveredquantity + "' WHERE batch ='" + batchseries + "' ";
					Qryobj2 = strQry2;
					UpdateList.add(Qryobj2);

				}
				else
				{
					List<BatchMaster> batchMaster = reasearchAndDevelopmentService.getbatchMasterDetailsforupdate(batchseries);

					if (batchMaster != null && !batchMaster.isEmpty())
					{
						delevered = batchMaster.get(0).getDelivered();
						balance = batchMaster.get(0).getBalance();
						deliveredquantity = delevered + dispatchedquantity;
						balancequantity = balance - dispatchedquantity;
					}

					String strQry2 = "";
					Object Qryobj2 = "";
					strQry2 = "UPDATE BatchMaster set balance='" + balancequantity + "',delivered='" + deliveredquantity
					        + "' WHERE batch ='" + batchseries + "' ";
					Qryobj2 = strQry2;
					UpdateList.add(Qryobj2);

				}
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        dispatchSummaryBean.getModifyFlag());

			if (isInsertSuccess)
			{
				isInsertSuccess = false;
				List entityList1 = new ArrayList();
				List UpdateList1 = new ArrayList();

				for (int i = 0; i < griddata.size(); i++)
				{
					DispatchDetailsBean dispatchDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
					        DispatchDetailsBean.class);
					String indentno = dispatchDetailsBean.getIndentNumbers();
					String BatchSeries = dispatchDetailsBean.getBatchNo();
					String status = "true";
					String status1 = "false";
					List DateList = reasearchAndDevelopmentService.listUpdateStatusForAllIndentDetails(indentno);
					for (int j = 0; j < DateList.size(); j++)
					{
						Map dateMap = new HashMap();
						dateMap = (Map) DateList.get(j);
						Integer indentstatus = (Integer) dateMap.get("indentStatus");
						double pendingqty1 = (Double) dateMap.get("pendingQuantity");

						if ((indentstatus == 1) || (indentstatus == 2))
						{
							status = "false";

						}
						else
						{
							if(pendingqty1==0)
							{
							status1 = "true";
							}
							else
							{
							status1 = "false";
							break;

							}
							
						}

					}

					if (("true".equals(status)) && ("true".equals(status1)))
					{
						String strQry = "";
						Object Qryobj = "";
						strQry = "UPDATE KindIndentSummary set indentStatus=" + 3 + " WHERE indentNo ='" + indentno + "' ";
						Qryobj = strQry;
						UpdateList1.add(Qryobj);
					}
					/*List<BatchMaster> batchMaster = reasearchAndDevelopmentService.getbatchMasterDetailsforupdate(BatchSeries);
					if (batchMaster != null && !batchMaster.isEmpty())
					{
						double totalSeedlings = batchMaster.get(0).getTotalSeedlings();
						double delivered = batchMaster.get(0).getDelivered();

						Double survivalper = reasearchAndDevelopmentService.getbatchsurvivalper(delivered, totalSeedlings);
						Double balance1 = reasearchAndDevelopmentService.getbatchbalance(totalSeedlings, delivered);
						Double takenoff = reasearchAndDevelopmentService.getbatchtakenoff(totalSeedlings, delivered, balance1);

						//reasearchAndDevelopmentService.updateDispatchBatchMasterDetails(totalSeedlings, survivalper, balance,
						      //  takenoff, BatchSeries);
						
						
					}*/
					
					List<BatchMaster> batchMaster = reasearchAndDevelopmentService.getbatchMasterDetailsforupdate(BatchSeries);
					if (batchMaster != null && !batchMaster.isEmpty())
					{
						double totalSeedlings = batchMaster.get(0).getTotalSeedlings();
						double delivered = batchMaster.get(0).getDelivered();

						Double survivalper = reasearchAndDevelopmentService.getbatchsurvivalper(delivered, totalSeedlings);
						Double balance1 = reasearchAndDevelopmentService.getbatchbalance(totalSeedlings, delivered);
						Double takenoff = reasearchAndDevelopmentService.getbatchtakenoff(totalSeedlings, delivered, balance1);

						//reasearchAndDevelopmentService.updateDispatchBatchMasterDetails(totalSeedlings, survivalper, balance,
						      //  takenoff, BatchSeries);
						
							String strQry2 = "";
							Object Qryobj2 = "";
							strQry2 = "UPDATE BatchMaster set  survivalPercent='" + survivalper + "' WHERE batch ='" + BatchSeries + "' ";
							Qryobj2 = strQry2;
							UpdateList1.add(Qryobj2);

						}
					
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList1, UpdateList1, "No");
				/*if (isInsertSuccess)
				{
					boolean vsl = commonService.SendSMSForRyot();
				}*/
				return isInsertSuccess;

			}
			else
			{
				isInsertSuccess = false;
				return isInsertSuccess;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private TrayTracker prepareModelforTrayTracker(DispatchDetailsBean detailsBean, DispatchSummaryBean summaryBean)
	{
		TrayTracker modelDetials = new TrayTracker();
		String userId = getLoggedInUserName();
		//int status=	GetloginuserStatus();

		modelDetials.setLoginUser(userId);
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setRyotCode(summaryBean.getRyotcode());
		String detailsdate = summaryBean.getDateOfDispatch();
		modelDetials.setTransactionDate(DateUtils.getSqlDateFromString(detailsdate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setWorking("0");
		modelDetials.setTranInandOut(detailsBean.getNoOfTrays());
		modelDetials.setDamaged("0");
		modelDetials.setTrayType(detailsBean.getTrayType());
		modelDetials.setTransactionRefNo(summaryBean.getDispatchNo());
		return modelDetials;
	}
	private TrayTracker prepareModelforSeedTrayTracker(TrayUpdateDetailsBean detailsBean, TrayReturnSummary summaryBean)
	{
		TrayTracker modelDetials = new TrayTracker();
		String userId = getLoggedInUserName();
		//int status=	GetloginuserStatus();

		modelDetials.setLoginUser(userId);
		modelDetials.setSeason(summaryBean.getSeason());
		
		String ryotcode=summaryBean.getRyotCode();
		modelDetials.setRyotCode(ryotcode);
		String detailsdate = detailsBean.getTrayDateTaken();
		modelDetials.setTransactionDate(DateUtils.getSqlDateFromString(detailsdate, Constants.GenericDateFormat.DATE_FORMAT));
		String goodtrays=Double.toString(detailsBean.getTraysinGoodCondition());
		modelDetials.setWorking(goodtrays);
		double trayinout=detailsBean.getNoofTraysReturn();
		modelDetials.setTranInandOut(trayinout);
		String  damagedtrays=Double.toString(detailsBean.getDamagedTray());
		modelDetials.setDamaged(damagedtrays);
		modelDetials.setTrayType(detailsBean.getTrayType());
		modelDetials.setTransactionRefNo(Integer.toString(summaryBean.getId()));
		return modelDetials;
	}
	private DispatchSummary prepareModelforDispatchSummary(DispatchSummaryBean summaryBean,Double issuedQty,Double pendingQty)
	{

		int status = 0;
		Integer maxId = commonService.getMaxID("DispatchSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		DispatchSummary summaryModel = new DispatchSummary();
		String userId = getLoggedInUserName();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}
		String season = summaryBean.getSeason();
		String summarydata = summaryBean.getDateOfDispatch();
		summaryModel.setDateOfDispatch(DateUtils.getSqlDateFromString(summarydata, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setSeason(summaryBean.getSeason());

		String dispatchno = summaryBean.getDispatchNo();
		//reasearchAndDevelopmentService.deleteDispatchDetails(season,dispatchno);
		String dispatchno1[] = dispatchno.split("/");
		String dispno = dispatchno1[0].substring(1);
		dispno = dispno.substring(1);
		int Seqno = Integer.parseInt(dispno);
		summaryModel.setDispatchSeq(Seqno);

		summaryModel.setDispatchNo(summaryBean.getDispatchNo());
		summaryModel.setRyotCode(summaryBean.getRyotcode());
		summaryModel.setfACode(summaryBean.getFaCode());
		summaryModel.setfOCode(summaryBean.getFoCode());
		summaryModel.setDriverName(summaryBean.getDriverName());
		summaryModel.setDrPhoneNo(summaryBean.getDrPhoneNo());
		summaryModel.setDispatchBy(userId);
		summaryModel.setIndentNumbers(summaryBean.getIndentNumbers());
		summaryModel.setRyotPhoneNumber(summaryBean.getRyotPhoneNumber());
		summaryModel.setDispatchedStatus(0);
		summaryModel.setAgreementSign(summaryBean.getAgreementSign());
		summaryModel.setLandSurvey(summaryBean.getLandSurvey());
		summaryModel.setRyotName(summaryBean.getRyotName());
		summaryModel.setSeedlingQty(summaryBean.getSeedlingQty());
		summaryModel.setSoilWaterAnalyis(summaryBean.getSoilWaterAnalyis());
		summaryModel.setVehicalNo(summaryBean.getVehicle());
		summaryModel.setIssuedQty(issuedQty);
		summaryModel.setPendingQty(pendingQty);
		summaryModel.setDeliveryTime(java.sql.Time.valueOf(summaryBean.getDeliveryTime()));
		summaryModel.setTrasportcontractor(summaryBean.getLc());
		summaryModel.setAgreementnumber(summaryBean.getAgreementnumber());
		String deliverydate = summaryBean.getDeliveryDate();
		summaryModel.setDeliveryDate(DateUtils.getSqlDateFromString(deliverydate, Constants.GenericDateFormat.DATE_FORMAT));
		return summaryModel;
	}

	private DispatchDetails prepareModelforDispatchDetails(DispatchDetailsBean detailsBean, DispatchSummaryBean summaryBean,String kind)
	{

		DispatchDetails modelDetials = new DispatchDetails();
		String userId = getLoggedInUserName();
		int status = GetloginuserStatus();
		if (detailsBean.getLoadFlag() == null)
		{
			byte status1 = 0;
			modelDetials.setIsBatchCompleted(status1);
		}
		else
		{
			modelDetials.setIsBatchCompleted(detailsBean.getLoadFlag());
		}
		String dispatchno = summaryBean.getDispatchNo();
		String dispatchno1[] = dispatchno.split("/");
		String dispno = dispatchno1[0].substring(1);
		dispno = dispno.substring(1);
		int Seqno = Integer.parseInt(dispno);
		modelDetials.setId(Seqno);
		modelDetials.setVariety(detailsBean.getVariety());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setIndentNo(detailsBean.getIndentNumbers());
		modelDetials.setDispatchNo(summaryBean.getDispatchNo());
		modelDetials.setBatch(detailsBean.getBatchNo());
		modelDetials.setNoOfSeedlings(detailsBean.getNoOfSeedlings());
		modelDetials.setSeedlingCost(detailsBean.getCostOfEachItem());
		modelDetials.setTotalSeedlingCost(detailsBean.getTotalCost());
		modelDetials.setNoOfTrays(detailsBean.getNoOfTrays());
		modelDetials.setTrayCost(detailsBean.getTrayCost());
		modelDetials.setTrayTotalCost(detailsBean.getTrayTotalCost());
		modelDetials.setTrayTypeCode(detailsBean.getTrayType());
		modelDetials.setKind(kind);
		return modelDetials;
	}

	@RequestMapping(value = "/getAuthorisedIndentsforDispatch", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAuthorisedIndentsforDispatch(@RequestBody JSONObject jsonData) throws Exception
	{
		String RyotCode = (String) jsonData.get("RyotCode");
		String season = (String) jsonData.get("Season");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		
		List DateList = reasearchAndDevelopmentService.getAuthorisedIndentsforDispatch1(RyotCode,season);
		logger.info("getAuthorisedIndentsforDispatch() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String IndentNo = (String) dateMap.get("indentNo");
				jsonObj.put("IndentNo", IndentNo);
				jArray.add(jsonObj);
			}
		}
		logger.info("getAuthorisedIndentsforDispatch() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getAuthorisedIndentsforAuthorisation", method = RequestMethod.POST)
	public @ResponseBody
	String getAuthorisedIndentsforAuthorisation(@RequestBody JSONObject jsonData) throws Exception
	{
		String RyotCode = (String) jsonData.get("RyotCode");
		String season = (String) jsonData.get("Season");

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		
		//List DateList = reasearchAndDevelopmentService.getAuthorisedIndentsforDispatch(RyotCode, FieldOfficer);
		List DateList = reasearchAndDevelopmentService.getAuthorisedIndentsforDispatch1(RyotCode,season);
		logger.info("getAuthorisedIndentsforAuthorisation() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String IndentNo = (String) dateMap.get("indentNo");
				jsonObj.put("IndentNo", IndentNo);
				jArray.add(jsonObj);
			}
		}
		logger.info("getAuthorisedIndentsforAuthorisation() ========jArray==========" + jArray);
		return jArray.toString();
	}

	@RequestMapping(value = "/getAuthorisedIndentsforDispatchPopUp", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAuthorisedIndentsforDispatchPopUp(@RequestBody JSONObject jsonData) throws Exception
	{
		String indentno = (String) jsonData.get("Indentno");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getAuthorisedIndentsforDispatchpopup(indentno);
		logger.info("getAuthorisedIndentsforDispatchPopUp() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String IndentNo = (String) dateMap.get("indentNo");
				List<KindIndentSummary> kindIndentSummary = reasearchAndDevelopmentService.listGetAllIndentSummary(IndentNo);
				Date Dateofdelivery1 = (Date) dateMap.get("batchDate");
				Integer kind = (Integer) dateMap.get("kind");
				Integer variety = (Integer) dateMap.get("variety");
				double quantity = (Double) dateMap.get("pendingQuantity");

				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String Dateofdelivery = df.format(Dateofdelivery1);
				String Dateofindent = df.format(kindIndentSummary.get(0).getSummaryDate());
				String Ryotcode = kindIndentSummary.get(0).getRyotCode();
				String village = kindIndentSummary.get(0).getVillage();
				Integer foCode=kindIndentSummary.get(0).getFoCode();
				Integer faCode=kindIndentSummary.get(0).getFieldMan();

				List<Village> distance = ahFuctionalService.getVillageDistance(village);
				Double vDistance = 0.0;
				if (distance != null && distance.size() > 0)
				{
					vDistance = distance.get(0).getDistance();
				}
				List<SeedAdvanceCost> cost = ahFuctionalService.getSeedCostUnitPerDistance(3, vDistance);
				Double villageDistanceCost = 0.0;
				if (cost != null && cost.size() > 0)
				{
					villageDistanceCost = cost.get(0).getCostofeachunit();
				}
				jsonObj.put("id", j + 1);
				jsonObj.put("IndentNo", IndentNo);
				jsonObj.put("dateofdeliver", Dateofdelivery);
				jsonObj.put("kind", kind);
				jsonObj.put("variety", variety);
				jsonObj.put("quantity", quantity);
				jsonObj.put("dateofindent", Dateofindent);
				jsonObj.put("ryotcode", Ryotcode);
				jsonObj.put("costOfEachItem", villageDistanceCost);
				jsonObj.put("totalCost", villageDistanceCost * quantity);
				jsonObj.put("foCode", foCode);
				jsonObj.put("faCode", faCode);

				jArray.add(jsonObj);
			}
		}
		logger.info("getAuthorisedIndentsforDispatchPopUp() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAuthorisedIndentsforAuthorisePopUp", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAuthorisedIndentsforAuthorisePopUp(@RequestBody JSONObject jsonData) throws Exception
	{
		String indentno = (String) jsonData.get("Indentno");
		String aggrementno = (String) jsonData.get("agreementnumber");
		String season = (String) jsonData.get("season");

		double seedlingacr=reasearchAndDevelopmentService.getMaxSeedlingsExtentForAgrauth(aggrementno,season);
		double seedlingqty=seedlingacr*10000;

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getAuthorisedIndentsforDispatchpopup(indentno);
		logger.info("getAuthorisedIndentsforAuthorisePopUp() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String IndentNo = (String) dateMap.get("indentNo");
				List<KindIndentSummary> kindIndentSummary = reasearchAndDevelopmentService.listGetAllIndentSummary(IndentNo);
				Date Dateofdelivery1 = (Date) dateMap.get("batchDate");
				Integer kind = (Integer) dateMap.get("kind");
				List<KindType> kindType = reasearchAndDevelopmentService.listGetDetailsbyKind(kind);
				double limitbags=kindType.get(0).getLimitperAcre();
				Integer variety = (Integer) dateMap.get("variety");
				double quantity = (Double) dateMap.get("pendingQuantity");
				String ryotcode = kindIndentSummary.get(0).getRyotCode();
				Integer foCode=kindIndentSummary.get(0).getFoCode();
				Integer faCode=kindIndentSummary.get(0).getFieldMan();
				//Added by sahadeva  20-02-2017
				String storeCode=kindIndentSummary.get(0).getStorecode();
				//
				double costofeachbag= kindType.get(0).getCostofeachItem();
				 byte rountoffto=kindType.get(0).getRoundingTo();
				 double allowed= 0.0;
				 double pending= 0.0;
				 
				 //code modified on 24-2-2017
				 List<AgreementDetailsBean> agreementDetailsBeans = ahFunctionalController.prepareListofAgreementDetailsBeans(ahFuctionalService.getAgreementDetailsBySeason(season, aggrementno));
					String planttype="";
					
				 Integer plantmaxcount=kindType.get(0).getPlantmaxcount();
				 Integer ratoonmaxcount=kindType.get(0).getRatoonmaxcount();
				 Integer allowerperacr=0;
				 Double plantacr=0.0;
				 Double ratoonacr=0.0;
				 for(int x=0;x<agreementDetailsBeans.size();x++)
				 {
				 planttype=agreementDetailsBeans.get(x).getPlantorRatoon();
				 if(Integer.parseInt(planttype)==1)
				 {
					 //allowerperacr=plantmaxcount;
					 plantacr=plantacr+agreementDetailsBeans.get(x).getExtentSize();
				 }
				 else
				 {
					// allowerperacr=ratoonmaxcount;
					 ratoonacr=ratoonacr+agreementDetailsBeans.get(x).getExtentSize();
 				 }
				 }

				allowed= Math.round(plantacr*plantmaxcount)+Math.round(ratoonacr*ratoonmaxcount);
				 //allowed= Math.round(seedlingacr)*allowerperacr;
				 
				 Double sum = reasearchAndDevelopmentService.getSumOfDispatchedqty(season,ryotcode,kind);
					if(sum==0.0)
					{
						allowed= allowed;
					}
					else
					{
						allowed=allowed-sum;
					}
					if(rountoffto==0)//0 always lower
					{
						
					}
					else//1  nearest integer
					{
						/*
							float f = 9.3f;
							Math.round(f); //Output is 9
							Math.ceil(f); //Output is 10
							Math.floor(f); //Output is 9
						 */
						//issuedqty=Math.round(issuedqty);
					}
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String Dateofdelivery = df.format(Dateofdelivery1);
				String Dateofindent = df.format(kindIndentSummary.get(0).getSummaryDate());
				String Ryotcode = kindIndentSummary.get(0).getRyotCode();
				String village = kindIndentSummary.get(0).getVillage();
				
				jsonObj.put("id", j + 1);
				jsonObj.put("IndentNo", IndentNo);
				jsonObj.put("dateofdeliver", Dateofdelivery);
				jsonObj.put("kind", kind);
				jsonObj.put("variety", variety);
				jsonObj.put("quantity", quantity);
				jsonObj.put("dateofindent", Dateofindent);
				jsonObj.put("ryotcode", Ryotcode);
				jsonObj.put("costOfEachItem", costofeachbag);
				jsonObj.put("totalCost", costofeachbag * quantity);
				jsonObj.put("allowed", allowed);
				jsonObj.put("pending", pending);
				jsonObj.put("foCode", foCode);
				jsonObj.put("faCode", faCode);
					//Added by sahadeva  20-02-2017
				jsonObj.put("storeCode", storeCode);
				
				jArray.add(jsonObj);
			}
		}
		logger.info("getAuthorisedIndentsforAuthorisePopUp() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getAllBatchDetailsNumbers", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllBatchDetailsNumbers(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.listAllBatchSeries(jsonData);
		logger.info("getAllBatchDetailsNumbers() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedbatches = (String) dateMap.get("batch");
				jsonObj.put("batchseries", addedbatches);
				jArray.add(jsonObj);
			}
		}
		logger.info("getAllBatchDetailsNumbers() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAllBatchDetailsNumbersForDispatch", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAllBatchDetailsNumbers(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		int variety = (Integer) jsonData.get("variety");
		String batch = (String) jsonData.get("batch");
		List DateList = reasearchAndDevelopmentService.listAllBatchSeriesForDispatch(batch,variety);
		logger.info("getAllBatchDetailsNumbers() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String addedbatches = (String) dateMap.get("batch");
				jsonObj.put("batchseries", addedbatches);
				jArray.add(jsonObj);
			}
		}
		logger.info("getAllBatchDetailsNumbers() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getAllBatchSeriesNoDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray listAllBatchSeriesDetails(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List VarietyList = new ArrayList();
		List DateList = reasearchAndDevelopmentService.listAllBatchSeriesDetails(jsonData);
		logger.info("listAllBatchSeriesDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{

			Map dateMap = new HashMap();
			dateMap = (Map) DateList.get(0);
			String addedvariety = (String) dateMap.get("variety");
			Double balance = (Double) dateMap.get("balance");
			int variety = Integer.parseInt(addedvariety);
			String varietyName = "";
			VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(variety);
			if (VarietyList != null && VarietyList.size() > 0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) VarietyList.get(0);
				varietyName = (String) tempMap.get("variety");
			}
			jsonObj.put("variety", addedvariety);
			jsonObj.put("varietyName", varietyName);
			jsonObj.put("pendingQty", balance);

			jArray.add(jsonObj);

		}
		logger.info("listAllBatchSeriesDetails() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/GetOnloadIndentsstatus", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray GetOnloadIndentsstatus(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		org.json.JSONObject response = new org.json.JSONObject();
		String userId = getLoggedInUserName();

		List DateList = reasearchAndDevelopmentService.listGetOnloadIndentsstatus(jsonData);
		logger.info("getYearSourceofSeeds() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			int feildempofficer = 0;
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String indentno = (String) dateMap.get("indentNo");
				Date dateofindent = (Date) dateMap.get("SummaryDate");
				String k2 = DateUtils.formatDate(dateofindent, Constants.GenericDateFormat.DATE_FORMAT);
				//String variety1  = (String) dateMap.get("Variety");
				Integer feildman = (Integer) dateMap.get("fieldMan");
				Integer foCode = (Integer) dateMap.get("foCode");
				
				//code added on 20-2-2017
				String ryotcode=(String) dateMap.get("ryotCode");
				String fieldofficername="";
					List DateList2 = reasearchAndDevelopmentService.GetFeildOfficerempid(foCode);
					if (DateList2 != null && DateList2.size() > 0)
					{
						Map dMap1 = new HashMap();
						dMap1 = (Map) DateList2.get(0);
						feildempofficer = (Integer) dMap1.get("employeeId");
						//code added on 27-2-2017
						fieldofficername = (String) dMap1.get("fieldOfficer");

					}
				//added by naidu on 01-03-2017
				/*	
				Integer kind = (Integer) dateMap.get("kind");
				double qty = (Double) dateMap.get("quantity");
				String kindtype="";
				List DateList3 = reasearchAndDevelopmentService.listAllKindDetailsByType(kind);
				logger.info("getAllKindDetailsByType() ========DateList3==========" + DateList3);
				if (DateList3 != null && DateList3.size() > 0)
				{

					Map dateMap2 = new HashMap();
					dateMap2 = (Map) DateList3.get(0);
					 kindtype = (String) dateMap2.get("kindType");
				}
				*/
				//String varietykind = kindtype + "," + "(" + qty + ")";
				jsonObj.put("indent", indentno);
				jsonObj.put("dateofIndent", k2);
				//jsonObj.put("varietyOfCane", kindtype);
				//code added on 27-2-2017
				jsonObj.put("fieldOfficer", fieldofficername);
					//Added by sahadeva  20-02-2017
				jsonObj.put("ryotCode", ryotcode);


				jArray.add(jsonObj);
			}
		}
		logger.info("getYearSourceofSeeds() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/SaveSeedlingDispatchAckSummary", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveSeedlingDispatchAckSummary(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		List AccountsList = new ArrayList();
		int transactionCode =0;
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		try
		{

			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			DispatchAckSummaryBean dispatchAckSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        DispatchAckSummaryBean.class);
			
			DispatchAckSummary dispatchAckSummary = prepareModelforAcknowledgeSummary(dispatchAckSummaryBean);
			entityList.add(dispatchAckSummary);
			
			String dispatchsq=dispatchAckSummary.getDispatchNo();
			String strQry2 = "";
			Object Qryobj2 = "";
			strQry2 = "UPDATE DispatchSummary set dispatchedStatus=2 WHERE dispatchNo ='" + dispatchsq + "' ";
			Qryobj2 = strQry2;
			UpdateList.add(Qryobj2);
			
			
			
			for (int i = 0; i < griddata.size(); i++)
			{
				DispatchAckDetailsBean dispatchAckDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),DispatchAckDetailsBean.class);
				DispatchAckDetails dispatchAckDetails = prepareModelforAcknowledgeDetails(dispatchAckDetailsBean,dispatchAckSummaryBean);
				entityList.add(dispatchAckDetails);

				String userId = getLoggedInUserName();

				String flag = "No";
				String season = dispatchAckSummary.getSeason();
				
				//Ryot Account (seedling taken ryot) - Towards Seedlings Dr
				String ryotcode = dispatchAckSummary.getRyotCode();
				transactionCode = commonService.GetMaxTransCode(season);
				double Amt = dispatchAckDetails.getTotalSeedlingCost();
				String accCode = ryotcode;
				String strglCode = "SEEDSAL";
				String TransactionType = "Dr";	
				String journalMemo = "Towards Seedlings Taken :-"+dispatchAckSummaryBean.getDispatchNo()+" On "+dispatchAckSummaryBean.getDateOfAcknowledge();
				if (accCode != null)
				{
					int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);

					//int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
					int AccSubGrpCode = 9;
					String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

					Map tempMap1 = new HashMap();
					tempMap1.put("ACCOUNT_CODE", accCode);
					tempMap1.put("AMOUNT", Amt);
					tempMap1.put("JRNL_MEM", journalMemo);
					tempMap1.put("SEASON", season);
					tempMap1.put("TRANS_TYPE", TransactionType);
					tempMap1.put("GL_CODE", strglCode);
					tempMap1.put("TRANSACTION_CODE", transactionCode);
					tempMap1.put("IsCaneSupplier", "Yes");

					Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
					logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

					entityList.add(AccMap.get("accDtls"));
					entityList.add(AccMap.get("accGrpDtls"));
					entityList.add(AccMap.get("accSubGrpDtls"));

					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", accCode);
					boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
					if (ryotAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
					}
					else
					{
						AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
						String straccCode = accountSummary.getAccountcode();
						if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", accCode);
							tmpMap.put("KEY", accCode);
							Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
						}
					}

					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (loanAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
					else
					{
						AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);

							Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					Map AccSubGrpSmry = new HashMap();
					//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					AccSubGrpCode = 9;//for ryot
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
					boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if (ryotLoanAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
					}
					else
					{
						AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
						        .get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						accsubgrpcode = 9;
						String ses = accountSubGroupSummary.getSeason();
						if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
							        .returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				
				//Ryot Account (seedling taken ryot) - Towards Seedling Trays Dr
				/*
				ryotcode = dispatchAckSummary.getRyotCode();
				Amt = dispatchAckDetails.getTotalSeedlingCost();
				accCode = ryotcode;
				strglCode = "SeedlingTrays";
				TransactionType = "Dr";
				double trays = dispatchAckDetails.getNoOfTrays();
				double trayscost = dispatchAckDetails.getTrayCost();
				Amt = dispatchAckDetails.getTrayTotalCost();
				journalMemo = "Towards (" + trays + "@" + trayscost + ")SeedlingsTrays Taken";
				if (accCode != null)
				{
					int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);
					//int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
					int AccSubGrpCode = 9;
					String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
					Map tempMap1 = new HashMap();
					tempMap1.put("ACCOUNT_CODE", accCode);
					tempMap1.put("AMOUNT", Amt);
					tempMap1.put("JRNL_MEM", journalMemo);
					tempMap1.put("SEASON", season);
					tempMap1.put("TRANS_TYPE", TransactionType);
					tempMap1.put("GL_CODE", strglCode);
					tempMap1.put("TRANSACTION_CODE", transactionCode);
					tempMap1.put("IsCaneSupplier", "Yes");

					Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
					logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

					entityList.add(AccMap.get("accDtls"));
					entityList.add(AccMap.get("accGrpDtls"));
					entityList.add(AccMap.get("accSubGrpDtls"));

					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", accCode);
					boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
					if (ryotAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
					}
					else
					{
						AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
						String straccCode = accountSummary.getAccountcode();
						if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", accCode);
							tmpMap.put("KEY", accCode);
							Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
						}
					}

					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (loanAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
					else
					{
						AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);

							Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					Map AccSubGrpSmry = new HashMap();
					//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					AccSubGrpCode = 9;
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
					boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if (ryotLoanAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
					}
					else
					{
						AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
						        .get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						accsubgrpcode = 9;
						String ses = accountSubGroupSummary.getSeason();
						if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
							        .returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				*/
				
				//Seedling Sales Cr
				Amt = dispatchAckDetails.getTotalSeedlingCost();
				accCode = "SEEDSAL";
				strglCode = dispatchAckSummary.getRyotCode();
				TransactionType = "Cr";
				journalMemo = "Towards Seedling Sales :-"+dispatchAckSummaryBean.getDispatchNo()+" On "+dispatchAckSummaryBean.getDateOfAcknowledge();
				if (accCode != null)
				{
					int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);

					int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
					String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

					Map tempMap1 = new HashMap();
					tempMap1.put("ACCOUNT_CODE", accCode);
					tempMap1.put("AMOUNT", Amt);
					tempMap1.put("JRNL_MEM", journalMemo);
					tempMap1.put("SEASON", season);
					tempMap1.put("TRANS_TYPE", TransactionType);
					tempMap1.put("GL_CODE", strglCode);
					tempMap1.put("TRANSACTION_CODE", transactionCode);

					Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
					logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

					entityList.add(AccMap.get("accDtls"));
					entityList.add(AccMap.get("accGrpDtls"));
					entityList.add(AccMap.get("accSubGrpDtls"));

					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", accCode);
					boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
					if (ryotAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
					}
					else
					{
						AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
						String straccCode = accountSummary.getAccountcode();
						if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", accCode);
							tmpMap.put("KEY", accCode);
							Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
						}
					}

					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (loanAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
					else
					{
						AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);

							Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					Map AccSubGrpSmry = new HashMap();
					//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
					boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if (ryotLoanAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
					}
					else
					{
						AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
						        .get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = caneAccountingFunctionalController
							        .returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				
				//////////////////////imp///////////////////////
				//Advance Account (one account for each advance. So the entry will be posted into respective account) Dr
				Amt = dispatchAckDetails.getTotalSeedlingCost();
				accCode = "2311";
				strglCode = ryotcode;
				TransactionType = "Dr";
				journalMemo = "Towards Seedling Taken :-"+dispatchAckSummaryBean.getDispatchNo()+" On "+dispatchAckSummaryBean.getDateOfAcknowledge();
				if (accCode != null)
				{
					int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);

					int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
					String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

					Map tempMap1 = new HashMap();
					tempMap1.put("ACCOUNT_CODE", accCode);
					tempMap1.put("AMOUNT", Amt);
					tempMap1.put("JRNL_MEM", journalMemo);
					tempMap1.put("SEASON", season);
					tempMap1.put("TRANS_TYPE", TransactionType);
					tempMap1.put("GL_CODE", strglCode);
					tempMap1.put("TRANSACTION_CODE", transactionCode);

					Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
					logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

					entityList.add(AccMap.get("accDtls"));
					entityList.add(AccMap.get("accGrpDtls"));
					entityList.add(AccMap.get("accSubGrpDtls"));

					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", accCode);
					boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
					if (ryotAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
					}
					else
					{
						AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
						String straccCode = accountSummary.getAccountcode();
						if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", accCode);
							tmpMap.put("KEY", accCode);
							Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
						}
					}

					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (loanAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
					}
					else
					{
						AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);

							Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					Map AccSubGrpSmry = new HashMap();
					//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
					boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if (ryotLoanAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
					}
					else
					{
						AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();

							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				
				//If it is subsidy, we need to post accounting as follows but here we are not posting right now.
				List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(8);
				byte advMode = companyAdvance.get(0).getAdvancemode();
				if(advMode == 1)
				{
					//needs to get total extent size for agreements and their plots
					//multiply that with 10000 to get the total elgible amount
					//from dispatch summary get the total seedlings given for the ryot for season
					// if this amount is less, then post the seedling cost of dispatch for which
					//we are doing acknowledgement
					//if it is more, subtract the last dispatch amount from the total dispatched
					//subtract that difference from total elgible amount and that amount 
					//needs to be posted to Cr
					//if the final difference is > 0, then only post the Cr entry
					
					//Seedling Subsidy Dr
					/*Amt = dispatchAckDetails.getTotalSeedlingCost();
					accCode = "SeedlingSubsidy";
					strglCode = ryotcode;
					TransactionType = "Dr";
					journalMemo = "Towards Seedling Subsidy";
					if (accCode != null)
					{
						int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);
	
						int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
						String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
	
						Map tempMap1 = new HashMap();
						tempMap1.put("ACCOUNT_CODE", accCode);
						tempMap1.put("AMOUNT", Amt);
						tempMap1.put("JRNL_MEM", journalMemo);
						tempMap1.put("SEASON", season);
						tempMap1.put("TRANS_TYPE", TransactionType);
						tempMap1.put("GL_CODE", strglCode);
						tempMap1.put("TRANSACTION_CODE", transactionCode);
	
						Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
						logger.info("saveAckDispatchDetails()------AccMap" + AccMap);
						
						entityList.add(AccMap.get("accDtls"));
						entityList.add(AccMap.get("accGrpDtls"));
						entityList.add(AccMap.get("accSubGrpDtls"));
	
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", accCode);
						boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
						if (ryotAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
						}
						else
						{
							AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
							String straccCode = accountSummary.getAccountcode();
							if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
	
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", accCode);
								tmpMap.put("KEY", accCode);
								Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
							}
						}
	
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (loanAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
						}
						else
						{
							AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
	
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
	
								Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
						boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if (ryotLoanAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
						}
						else
						{
							AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
							        .get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
	
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}*/
			
					//if it is subsidy, we need to post cr entry for ryot account
					//Ryot account for subsidy amount Cr
					//Amt = dispatchAckDetails.getTotalSeedlingCost();
					/*accCode = ryotcode;
					strglCode = "SeedlingSubsidy";
					TransactionType = "Cr";
					journalMemo = "Towards Seedling Subsidy";
					if (accCode != null)
					{
						//added by naidu on 29-01-2017
						String agrno=reasearchAndDevelopmentService.getAgrnoForDispatchno(dispatchAckSummaryBean.getDispatchNo());
						double tExtentForAgr =reasearchAndDevelopmentService.getMaxSeedlingsExtentForAgr(agrno,dispatchAckSummaryBean.getSeason());
						double maxSeedlingsForAcr=10000;
						double maxseedlings =tExtentForAgr*maxSeedlingsForAcr;
						double totaldisseedlings =reasearchAndDevelopmentService.getTotalSeedlingsDispatchForAgr(agrno,dispatchAckSummaryBean.getSeason());
						if(totaldisseedlings>maxseedlings)
						{
							double prevDisSeedlings=totaldisseedlings-dispatchAckDetailsBean.getNoOfSeedlings();
							double ryotCrseedlings=maxseedlings-prevDisSeedlings;
							if(ryotCrseedlings>0)
							{
								Amt=ryotCrseedlings*dispatchAckDetailsBean.getCostOfEachItem();
								int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
								String strAccGrpCode = Integer.toString(AccGrpCode);
			
								int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
								String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
			
								Map tempMap1 = new HashMap();
								tempMap1.put("ACCOUNT_CODE", accCode);
								tempMap1.put("AMOUNT", Amt);
								tempMap1.put("JRNL_MEM", journalMemo);
								tempMap1.put("SEASON", season);
								tempMap1.put("TRANS_TYPE", TransactionType);
								tempMap1.put("GL_CODE", strglCode);
								tempMap1.put("TRANSACTION_CODE", transactionCode);
			
								Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
								logger.info("saveAckDispatchDetails()------AccMap" + AccMap);
								
								entityList.add(AccMap.get("accDtls"));
								entityList.add(AccMap.get("accGrpDtls"));
								entityList.add(AccMap.get("accSubGrpDtls"));
			
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", accCode);
								boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
								if (ryotAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
								}
								else
								{
									AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
									String straccCode = accountSummary.getAccountcode();
									if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
			
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", accCode);
										tmpMap.put("KEY", accCode);
										Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
									}
								}
			
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (loanAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
								}
								else
								{
									AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
			
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
			
										Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
								boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if (ryotLoanAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
								}
								else
								{
									AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
									        .get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
			
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
						}	
							//end of cr of ryot
						
						
					}*/
				}
			}
			
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					String Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					String Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					String Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			
			//////////Advance tables postings//////////////////
			
			 String strQry = null;
			Object Qryobj = null;
			
			String flag = dispatchAckSummaryBean.getModifyFlag();
			String season = dispatchAckSummaryBean.getSeason();
			String advDate = dispatchAckSummaryBean.getDateOfAcknowledge();
			
			AdvanceDetails advanceDetails = prepareModelForAdvanceDetails(dispatchAckSummaryBean, transactionCode);
			AdvanceSummary advanceSummary = prepareModelForAdvanceSummary(dispatchAckSummaryBean, transactionCode);
			List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(dispatchAckSummaryBean.getRyotcode(),3,dispatchAckSummaryBean.getSeason());
			if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
			{
				AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);
				double Amt = advanceSummary.getAdvanceamount();

				double advanceAmt = AdvanceSummary1.getAdvanceamount();
				double pendingAmount = AdvanceSummary1.getPendingamount();
				double pendingpayable = AdvanceSummary1.getPendingpayable();
				double totalamount = AdvanceSummary1.getTotalamount();

				advanceAmt = Amt+advanceAmt;
				pendingAmount = Amt+pendingAmount;
				pendingpayable = Amt+pendingpayable;
				totalamount = Amt+totalamount;
				String qry = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode='3' and ryotcode='"+dispatchAckSummaryBean.getRyotcode()+"' and season='"+dispatchAckSummaryBean.getSeason()+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				entityList.add(advanceSummary);
			}
			
			int advCode = advanceSummary.getAdvancecode();
			//ahFuctionalService.deleteIntrestAmounts(dispatchAckSummaryBean.getSeason(), dispatchAckSummaryBean.getRyotcode(),advCode);

			
			AdvancePrincipleAmounts advanceprincipleamounts = prepareModelForAdvancePrincipleAmounts(dispatchAckSummaryBean);
			List advPrincipleList = new ArrayList();
			advPrincipleList = ahFuctionalService.getAdvancePrinciples(dispatchAckSummaryBean.getRyotcode(),3,dispatchAckSummaryBean.getSeason());
			if(advPrincipleList != null && advPrincipleList.size()>0)
			{
				Integer totalseedlings=0;
				Double totalseedlingsamt=0.0;
				

				String dispatchno=dispatchAckSummaryBean.getDispatchNo();
				List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.getDataForAcknowledgePrint(dispatchno);
				//Integer i = 0;
				for (DispatchDetails l : dispatchDetails)
				{
					Double noOfSeedlings1 = l.getNoOfSeedlings();
					Integer noOfSeedlings = noOfSeedlings1.intValue();
					totalseedlings=totalseedlings+noOfSeedlings;
					Double Seedlingsamt = l.getTotalSeedlingCost();
					totalseedlingsamt=totalseedlingsamt+Seedlingsamt;
				}
				
				AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);

				double principle = advanceprincipleamounts1.getPrinciple();
				logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
				double advAmt = totalseedlingsamt;
				double finalAdvAmt = principle+advAmt;
				logger.info("AdvancePrincipleAmounts()========finalAdvAmt==========" + finalAdvAmt);
				
				String qry = "Update AdvancePrincipleAmounts set principle="+finalAdvAmt+" where advancecode='3' and ryotcode='"+dispatchAckSummaryBean.getRyotcode()+"' and season='"+dispatchAckSummaryBean.getSeason()+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				entityList.add(advanceprincipleamounts);
			}
			
			entityList.add(advanceDetails);
			
		isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        dispatchAckSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private DispatchAckSummary prepareModelforAcknowledgeSummary(DispatchAckSummaryBean summaryBean)
	{
		int status = 0;
		Integer maxId = commonService.getMaxID("DispatchAckSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		DispatchAckSummary summaryModel = new DispatchAckSummary();
		status = GetloginuserStatus();
		String userId = getLoggedInUserName();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}
		String season = summaryBean.getSeason();
		String summarydata = summaryBean.getDateOfDispatch();
		summaryModel.setDateOfAcknowledge(DateUtils.getSqlDateFromString(summarydata, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setSeason(summaryBean.getSeason());

		String dispatchno = summaryBean.getDispatchNo();
		//reasearchAndDevelopmentService.deleteDispatchDetails(season,dispatchno);
		String dispatchno1[] = dispatchno.split("/");
		String dispno = dispatchno1[0].substring(1);
		dispno = dispno.substring(1);
		int Seqno = Integer.parseInt(dispno);
		summaryModel.setDispatchSeq(Seqno);

		summaryModel.setDispatchNo(summaryBean.getDispatchNo());
		summaryModel.setRyotCode(summaryBean.getRyotcode());
		summaryModel.setfACode(summaryBean.getFaCode());
		summaryModel.setfOCode(summaryBean.getFoCode());
		summaryModel.setDriverName(summaryBean.getDriverName());
		summaryModel.setDrPhoneNo(summaryBean.getDrPhoneNo());
		summaryModel.setAcknowledgeBy(userId);
		summaryModel.setIndentNumbers(summaryBean.getIndentNumbers());
		summaryModel.setRyotPhoneNumber(summaryBean.getRyotPhoneNumber());
		summaryModel.setDispatchedStatus("Closed");
		summaryModel.setAgreementSign(summaryBean.getAgreementSign());
		summaryModel.setLandSurvey(summaryBean.getLandSurvey());
		summaryModel.setRyotName(summaryBean.getRyotName());
		summaryModel.setSeedlingQty(summaryBean.getSeedlingQty());
		summaryModel.setSoilWaterAnalyis(summaryBean.getSoilWaterAnalyis());
		summaryModel.setVehicalNo(summaryBean.getVehicle());
		summaryModel.setLoginUser(userId);

		return summaryModel;
	}

	private DispatchAckDetails prepareModelforAcknowledgeDetails(DispatchAckDetailsBean detailsBean,
	        DispatchAckSummaryBean summaryBean)
	{
		DispatchAckDetails modelDetials = new DispatchAckDetails();
		String userId = getLoggedInUserName();
		int status = GetloginuserStatus();
		modelDetials.setVariety(detailsBean.getVariety());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setIndentNo(detailsBean.getIndentNo());
		modelDetials.setDispatchNo(summaryBean.getDispatchNo());
		modelDetials.setBatch(detailsBean.getBatchNo());
		modelDetials.setNoOfSeedlings(detailsBean.getNoOfSeedlings());
		modelDetials.setSeedlingCost(detailsBean.getCostOfEachItem());
		modelDetials.setTotalSeedlingCost(detailsBean.getTotalCost());
		modelDetials.setNoOfTrays(detailsBean.getNoOfTrays());
		modelDetials.setTrayCost(detailsBean.getTrayCost());
		modelDetials.setTrayTotalCost(detailsBean.getTrayTotalCost());
		modelDetials.setTrayTypeCode(detailsBean.getTrayType());
		modelDetials.setKind(detailsBean.getKind());
		modelDetials.setDispatchedStatus("Closed");

		return modelDetials;
	}
	@RequestMapping(value = "/getAcknowledgePrintDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONObject getAcknowledgePrintDetails(@RequestBody JSONObject dispatchData) throws Exception
	{
		JSONArray jArray2 = new JSONArray();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		JSONObject jsonObj1 = null;
		jsonObj1 = new JSONObject();
		List getRyotDetailsList =null;
		//String DispatchNo=(String) jsonData.get("DispatchNo");
		//	String rptFormat=(String) jsonData.get("mode");

		String DispatchNo=(String)dispatchData.getString("DispatchNo");
		int agrType=(Integer)dispatchData.getInt("agrType");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat DtFormat = new SimpleDateFormat(pattern);

		List AckDataList = reasearchAndDevelopmentService.getAckPrintData(DispatchNo);
		String circleName = "NA";
		String ryotname = "NA";
		String villageName = "NA";
		String zoneName = "NA";
		String village = "0";

		String ryotcode = "";
		String dispatno = "";
		String dispatchdate = "";
		String indentno = "";
		String indentdate = "";
		String vehicalno="";
		
		if (AckDataList != null && AckDataList.size() > 0)
		{
			Map ackMap = new HashMap();
			ackMap = (Map) AckDataList.get(0);
			ryotcode = (String) ackMap.get("ryotCode");
			dispatno = (String) ackMap.get("dispatchNo");
			Date dispatchdate1 = (Date) ackMap.get("dateOfDispatch");
			dispatchdate = DtFormat.format(dispatchdate1);
			
			vehicalno = (String) ackMap.get("vehicalNo");
			indentno = (String)ackMap.get("indentNumbers");
			List IndentDtList = reasearchAndDevelopmentService.getIndentDtList(indentno);
			if (IndentDtList != null && IndentDtList.size() > 0)
			{
				Map IndentMap = (Map)IndentDtList.get(0);
				Date IndentDt = (Date)IndentMap.get("SummaryDate");		
				indentdate = DtFormat.format(IndentDt);
			}
			String strAgrNo = (String)ackMap.get("agreementnumber");
			//added by naidu on 21-03-2017
			getRyotDetailsList = reasearchAndDevelopmentService.getryotDetailsForDispatch(strAgrNo,agrType);
			
			
			if (getRyotDetailsList != null && getRyotDetailsList.size() > 0)
			{
				Map rytMap = new HashMap();
				rytMap = (Map) getRyotDetailsList.get(0);
				ryotname = (String) rytMap.get("ryotname");
				String villagecode = (String) rytMap.get("villagecode");
				int circleCode = (Integer) rytMap.get("circlecode");
				int zoneCode = (Integer) rytMap.get("zonecode");
				//int zoneCode=Integer.parseInt(zone);
				//int circleCode=Integer.parseInt(circle);
				List<String> CircleList = new ArrayList<String>();
				CircleList = ahFuctionalService.getCircle(circleCode);
				if (CircleList != null && CircleList.size() > 0)
				{
					String myResult1 = CircleList.get(0);
					circleName = myResult1;
				}
				List<Village> villageNamelist = ahFuctionalService.getVillageForLedger(villagecode);
				if (villageNamelist != null && villageNamelist.size() > 0)
				{
					village = villageNamelist.get(0).getVillage();
				}
				List<String> ZoneList = new ArrayList<String>();
				ZoneList = ahFuctionalService.getZone(zoneCode);
				if (ZoneList != null && ZoneList.size() > 0)
				{
					String myResult1 = ZoneList.get(0);
					zoneName = myResult1;
				}
			}

			jsonObj.put("RyotName", ryotname);
			jsonObj.put("Circle", circleName);
			jsonObj.put("Zone", zoneName);
			jsonObj.put("Village", village);
			jsonObj.put("RyotCode", ryotcode);
			jsonObj.put("DispatchNo", dispatno);
			jsonObj.put("DispatchDate", dispatchdate);
			jsonObj.put("Vehical", vehicalno);

			jsonObj.put("IndentNo", indentno);
			jsonObj.put("IndentDate", indentdate);
			jArray.add(jsonObj);
			jsonObj1.put("formData", jArray);

		}

		List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.getDataForAcknowledgePrint(DispatchNo);
		Integer i = 0;
		for (DispatchDetails l : dispatchDetails)
		{

			/*jsonObj.put("IndentNo", indentno);
			jsonObj.put("IndentDate", indentdate);*/
			String transactiondate = dispatchdate;
			Integer ryotCode = Integer.parseInt(ryotcode);
			String ryotName = ryotname;
			villageName = village;
			circleName = circleName;
			zoneName = zoneName;
			String dispatchNo = dispatno;

			JSONObject subjsonObj = new JSONObject();
			int varity = l.getVariety();
			List<Variety> varietyname = ahFuctionalService.getVarietyByVarietyCode(varity);
			String varietyName = null;
			if (varietyname != null && varietyname.size() > 0)
			{
				varietyName = varietyname.get(0).getVariety();
			}
			Double noOfTrays1 = l.getNoOfTrays();
			Double noOfSeedlings1 = l.getNoOfSeedlings();
			Integer traytype = l.getTrayTypeCode(); //this needs to be replaced by code and then the tray type from master
			String traytypename = ""; 
			List<SeedlingTray> traytypename1 = reasearchAndDevelopmentService.getTraytype(traytype);
			if (traytypename1 != null && traytypename1.size() > 0)
			{
				traytypename = traytypename1.get(0).getTray();
			}
			
			Integer noOfSeedlings = noOfSeedlings1.intValue();
			String noOfTrays = Double.toString(noOfTrays1);

			subjsonObj.put("NoofSeedlings", l.getNoOfSeedlings());
			subjsonObj.put("NoofTrays", l.getNoOfTrays());
			subjsonObj.put("TrayType", traytypename);
			subjsonObj.put("varietyName", varietyName);
			jArray2.add(subjsonObj);
		}

		jsonObj1.put("gridData", jArray2);
		return jsonObj1;

	}

	@RequestMapping(value = "/SaveTrayUpdate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveTrayUpdate(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		String strQry = "";
		Object Qryobj = "";
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			Double	totaltrays=0.0;
			double Amt =0; 
			TrayUpdateSummaryBean trayUpdateSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        TrayUpdateSummaryBean.class);
			TrayReturnSummary trayReturnSummary = prepareModelforReturnSummary(trayUpdateSummaryBean);
			entityList.add(trayReturnSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				TrayUpdateDetailsBean trayUpdateDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        TrayUpdateDetailsBean.class);
				TrayReturnDetails trayReturnDetails = prepareModelforReturnDetails(trayUpdateDetailsBean, trayUpdateSummaryBean);
				entityList.add(trayReturnDetails);
				
				//Integer ryotdamagedtray=trayReturnDetails.getRyotDamagedTray();
				Double nooftraysreturn=trayReturnDetails.getTraysInGoodCondition();
				totaltrays=nooftraysreturn;
				Integer traytype=trayReturnDetails.getTrayType();
				String ryotcode=trayUpdateSummaryBean.getRyotCode();

				String village="";
				List RyotList = new ArrayList();
				RyotList = (List) reasearchAndDevelopmentService.getRyotName(ryotcode);
				if (RyotList != null && RyotList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) RyotList.get(0);
					village = (String) tempMap.get("villagecode");
				}
				List<Village> distance = ahFuctionalService.getVillageDistance(village);
				Double vDistance = 0.0;
				if (distance != null && distance.size() > 0)
				{
					vDistance = distance.get(0).getDistance();
				}
				List<SeedlingTrayChargeByDistance> cost = reasearchAndDevelopmentService.getTraytypeCostUnitPerDistance(vDistance,
				        traytype);
				Double villageDistanceCost = 0.0;
				if (cost != null && cost.size() > 0)
				{
					villageDistanceCost = cost.get(0).getTraycharge();
				}
				double traytotalamt=villageDistanceCost*totaltrays;
				
				Amt=traytotalamt+Amt;
				
				TrayTracker trayTracker = prepareModelforSeedTrayTracker(trayUpdateDetailsBean, trayReturnSummary);
				entityList.add(trayTracker);
				
			
			}
			/*
			String ryotcode = trayUpdateSummaryBean.getRyotCode();
			int transactionCode = commonService.GetMaxTransCode(trayUpdateSummaryBean.getSeason());
			String accCode = "SeedlingTrays" ;
			String strglCode =	ryotcode ;
			String TransactionType = "Cr";
			String journalMemo = "Towards SeedlingsTrays Return";
			Amt=Amt;
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);
				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);
				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", trayUpdateSummaryBean.getSeason());
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);
				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && trayUpdateSummaryBean.getSeason().equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}
				
				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && trayUpdateSummaryBean.getSeason().equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && trayUpdateSummaryBean.getSeason().equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
				
				Iterator entries = AccountSummaryMap.entrySet().iterator();
				while (entries.hasNext())
				{
					Map.Entry entry = (Map.Entry) entries.next();
					Object value = entry.getValue();
					AccountSummary accountSummary = (AccountSummary) value;

					String accountCode = accountSummary.getAccountcode();
					String sesn = accountSummary.getSeason();
					double runnbal = accountSummary.getRunningbalance();
					String crOrDr = accountSummary.getBalancetype();

					List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
					if (AccList != null && AccList.size() > 0)
					{
						String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
						        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
						Qryobj = qry;
						UpdateList.add(Qryobj);
					}
					else
					{
						entityList.add(accountSummary);
					}
				}

				Iterator entries1 = AccountGroupMap.entrySet().iterator();
				while (entries1.hasNext())
				{
					Map.Entry entry = (Map.Entry) entries1.next();
					Object value = entry.getValue();

					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
					int accountGroupCode = accountGroupSummary.getAccountgroupcode();
					String sesn = accountGroupSummary.getSeason();
					double runnbal = accountGroupSummary.getRunningbalance();
					String crOrDr = accountGroupSummary.getBalancetype();

					List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
					logger.info("updateAccountsNew()------AccList" + AccList);

					if (AccList != null && AccList.size() > 0)
					{
						String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
						        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
						Qryobj = qry;
						UpdateList.add(Qryobj);
					}
					else
					{
						entityList.add(accountGroupSummary);
					}
				}

				Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
				while (entries2.hasNext())
				{
					Map.Entry entry = (Map.Entry) entries2.next();
					Object value = entry.getValue();
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

					int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
					String sesn = accountSubGroupSummary.getSeason();
					double runnbal = accountSubGroupSummary.getRunningbalance();
					String crOrDr = accountSubGroupSummary.getBalancetype();

					List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
					logger.info("updateAccountsNew()------tempMap" + AccList);
					if (AccList != null && AccList.size() > 0)
					{
						String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
						        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
						        + accountSubGroupCode + " and season='" + sesn + "'";
						Qryobj = qry;
						UpdateList.add(Qryobj);
					}
					else
					{
						entityList.add(accountSubGroupSummary);
					}
				}
			}
			*/
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList, trayUpdateSummaryBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private TrayReturnSummary prepareModelforReturnSummary(TrayUpdateSummaryBean summaryBean)
	{
		int status = 0;
		Integer maxId = commonService.getMaxID("TrayReturnSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		TrayReturnSummary summaryModel = new TrayReturnSummary();
		status = GetloginuserStatus();
		String userId = getLoggedInUserName();

		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			summaryModel.setId(summaryBean.getId());
		}
		else
		{
			summaryModel.setId(maxId);
		}
		String season = summaryBean.getSeason();
		String summarydate = summaryBean.getSummaryDate();
		summaryModel.setSummaryDate(DateUtils.getSqlDateFromString(summarydate, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setRyotCode(summaryBean.getRyotCode());
		summaryModel.setZone(summaryBean.getZone());
		summaryModel.setVillage(summaryBean.getVillage());
		summaryModel.setCircle(summaryBean.getCircle());
		summaryModel.setTestedBy(userId);
		summaryModel.setVehicalNo(summaryBean.getVehicle());
		summaryModel.setDriverPhNo(summaryBean.getDriverNo());
		return summaryModel;
	}

	private TrayReturnDetails prepareModelforReturnDetails(TrayUpdateDetailsBean detailsBean, TrayUpdateSummaryBean summaryBean)
	{

		TrayReturnDetails modelDetials = new TrayReturnDetails();
		modelDetials.setId(summaryBean.getId());
		modelDetials.setSeason(summaryBean.getSeason());
		modelDetials.setTrayType(detailsBean.getTrayType());
		String detailsdate = detailsBean.getTrayDateTaken();
		modelDetials.setTraysTakenDate(DateUtils.getSqlDateFromString(detailsdate, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setNoOfTraysReturn(detailsBean.getNoofTraysReturn());
		modelDetials.setTraysInGoodCondition(detailsBean.getTraysinGoodCondition());
		modelDetials.setDamagedTrays(detailsBean.getDamagedTray());
		modelDetials.setDamagedTrayTravelling(detailsBean.getDamagedTrayTravelling());
		modelDetials.setRyotDamagedTray(detailsBean.getRyotDamagedTray());
		return modelDetials;
	}

	@RequestMapping(value = "/SavePlantingDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SavePlantingDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{
			Integer fieldassit = (Integer) jsonData.get("fieldMan");
			String dateofentry = (String) jsonData.get("dateOfEntry");
			String season = (String) jsonData.get("season");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");

			PlantingUpdateDetailsBean plantingUpdateDetailsBean = new PlantingUpdateDetailsBean();

			for (int i = 0; i < griddata.size(); i++)
			{

				plantingUpdateDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
				        PlantingUpdateDetailsBean.class);
				PlantingUpdateDetails plantingUpdateDetails = prepareModelforPlantingUpdateDetails(plantingUpdateDetailsBean,
				        season, dateofentry, fieldassit);
				entityList.add(plantingUpdateDetails);
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,
			        plantingUpdateDetailsBean.getModifyFlag());
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private PlantingUpdateDetails prepareModelforPlantingUpdateDetails(PlantingUpdateDetailsBean detailsBean, String season,
	        String dateofentry, Integer fieldassit)
	{
		PlantingUpdateDetails modelDetials = new PlantingUpdateDetails();
		modelDetials.setSeason(season);
		modelDetials.setFieldOfficer(fieldassit);
		modelDetials.setDateOfEntry(DateUtils.getSqlDateFromString(dateofentry, Constants.GenericDateFormat.DATE_FORMAT));
		String dateofPlanting = detailsBean.getDateofPlanting();
		modelDetials.setDateofPlanting(DateUtils.getSqlDateFromString(dateofPlanting, Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setVariety(detailsBean.getVariety());
		modelDetials.setRyotCode(detailsBean.getRyotCode());
		modelDetials.setNoOfSeedling(detailsBean.getNoOfSeedling());
		return modelDetials;
	}

	@RequestMapping(value = "/SaveAuthorisationForm", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String SaveAuthorisationForm(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONObject json = new JSONObject();
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String maxseqno = "false";

		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		String strQry = "";
		Object Qryobj = "";
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			org.json.JSONObject response = new org.json.JSONObject();
			AuthorisationFormDetails authorisationFormDetails =null;
			AuthorisationFormSummaryBean authorisationFormSummaryBean = new ObjectMapper().readValue(formdata.toString(),
			        AuthorisationFormSummaryBean.class);
			String season = authorisationFormSummaryBean.getSeason();
			maxseqno = commonService.GetMaxAuthoriseFertiliser(season);
			int transactionCode = commonService.GetMaxTransCode(season);

			AuthorisationFormSummary authorisationFormSummary = prepareModelforAuthorisationSummary(authorisationFormSummaryBean,maxseqno,transactionCode);
			entityList.add(authorisationFormSummary);
			int advcodeForKind =0;
			for (int i = 0; i < griddata.size(); i++)
			{
				double deliveredquantity = 0;
				double balancequantity = 0;
				AuthorisationFormDetailsBean authorisationFormDetailsBean = new ObjectMapper().readValue(griddata.get(i)
				        .toString(), AuthorisationFormDetailsBean.class);
				authorisationFormDetails = prepareModelforAuthorisationDetails(
				        authorisationFormDetailsBean, authorisationFormSummaryBean,maxseqno);
				entityList.add(authorisationFormDetails);
				String indentno = authorisationFormDetailsBean.getIndentNumbers();
				double dispatchedquantity = authorisationFormDetailsBean.getQuantity();

				Integer kind = authorisationFormDetailsBean.getKind();
				advcodeForKind=commonService.getAdvcodeForkind(kind);
				
				
				
				String dispatchSummaryno = authorisationFormDetails.getAuthorisationSeqNo();
				List<KindIndentDetails> kindIndentDetails = reasearchAndDevelopmentService.listGetAllIndentDetailsbykind(indentno,kind);
				if (kindIndentDetails != null && !kindIndentDetails.isEmpty())
				{
					double delevered = kindIndentDetails.get(0).getDispatchedQuantity();
					double balance = kindIndentDetails.get(0).getPendingQuantity();
					String dispatchno = kindIndentDetails.get(0).getDispatchNo();
					if ("NA".equals(dispatchno)||"".equals(dispatchno)||dispatchno==null)
					{
						dispatchSummaryno = dispatchSummaryno;
					}
					else
					{
						dispatchSummaryno = dispatchno + ',' + dispatchSummaryno;
					}
					deliveredquantity = delevered + dispatchedquantity;
					balancequantity = balance - dispatchedquantity;
				}

				strQry = "UPDATE KindIndentDetails set indentStatus=" + 3 + ",dispatchNo='" + dispatchSummaryno
				        + "' ,pendingQuantity='" + balancequantity + "' ,dispatchedQuantity='" + deliveredquantity
				        + "' WHERE indentNo ='" + indentno + "' and season='" + season + "' and kind=" + kind + " ";
				Qryobj = strQry;
				UpdateList.add(Qryobj);

				String strQry2 = "";
				Object Qryobj2 = "";
				strQry2 = "UPDATE KindIndentSummary set dispatchNo='" + dispatchSummaryno + "' WHERE indentNo ='" + indentno
				        + "'  ";
				Qryobj2 = strQry2;
				UpdateList.add(Qryobj2);

				String strQry1 = "";
				Object Qryobj1 = "";
				strQry1 = "UPDATE AuthorisationFormDetails set indentStatus=" + 3 + " WHERE indentNumbers ='" + indentno
				        + "' and season='" + season + "' and itemtype=" + kind + "";
				Qryobj1 = strQry1;
				UpdateList.add(Qryobj1);

			}

			//Ryot Account (Authorised Ryot) Dr
			List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcodeForKind);
			String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
			String advname=companyAdvance.get(0).getAdvance();
			String ryotcode = authorisationFormSummaryBean.getRyotCode();
			double Amt = authorisationFormSummaryBean.getGrandTotal();
			String accCode = ryotcode;
			String strglCode = "FERTSAL";
			String TransactionType = "Dr";
			String journalMemo = "Charges towards "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpCode = 9;//for ryot
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Advance Account (Here this is towards Fertiliser Advance) Dr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getGrandTotal();
			//advcodeForKind
			accCode = GlCodeforAdvance;
			strglCode = ryotcode;
			TransactionType = "Dr";
			journalMemo = "Towards "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Fertiliser Sales Cr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getGrandTotal();
			accCode = "FERTSAL";
			strglCode = ryotcode;
			TransactionType = "Cr";
			journalMemo = "Towards Fertilizers Sales"+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			
			//Advance Postings
			//AdvanceDetails
			String ryotcode1 = authorisationFormSummaryBean.getRyotCode();

			AdvanceDetails advanceDetails = new AdvanceDetails();
			
			advanceDetails.setAdvanceamount(Amt);
			advanceDetails.setAdvancecateogrycode(2);
			advanceDetails.setAdvancecode(advcodeForKind);
			String advDate = authorisationFormSummaryBean.getAuthorisationDate();
			advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
			advanceDetails.setAdvancesubcategorycode(1);
			advanceDetails.setAgreements("NA");
			advanceDetails.setExtentsize(0.0);
			advanceDetails.setIsitseedling((byte)0);
			advanceDetails.setIsSeedlingAdvance((byte)0);
			advanceDetails.setLoginId(loginController.getLoggedInUserName());
			advanceDetails.setRyotcode(ryotcode1);
			advanceDetails.setRyotpayable(Amt);
			advanceDetails.setSeason(season);
			advanceDetails.setSeedingsquantity(0);
			advanceDetails.setSeedsuppliercode("NA");
			advanceDetails.setTotalamount(Amt);
			advanceDetails.setTransactioncode(transactionCode);
			entityList.add(advanceDetails);
			
			//AdvanceSummary
			List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(ryotcode1,advcodeForKind,season);
			if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
			{
				AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);

				double advanceAmt = AdvanceSummary1.getAdvanceamount();
				double pendingAmount = AdvanceSummary1.getPendingamount();
				double pendingpayable = AdvanceSummary1.getPendingpayable();
				double totalamount = AdvanceSummary1.getTotalamount();
				
				
				advanceAmt = Amt+advanceAmt;
				pendingAmount = Amt+pendingAmount;
				pendingpayable = Amt+pendingpayable;
				totalamount = Amt+totalamount;
				
				String qry = "Update AdvanceSummary set transactioncode="+transactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode="+advcodeForKind+" and ryotcode='"+ryotcode1+"' and season='"+season+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				AdvanceSummary advanceSummary = new AdvanceSummary();
				List<Ryot> ryots=ryotService.getRyotByRyotCode(ryotcode1);
				Ryot ryot=ryots.get(0);
				
				advanceSummary.setAdvanceamount(Amt);
				advanceSummary.setAdvancecode(advcodeForKind);
				advanceSummary.setAdvancestatus((byte) 0);
				advanceSummary.setInterestamount(0.0);
				advanceSummary.setLoginid(loginController.getLoggedInUserName());
				advanceSummary.setPaidamount(0.0);
				advanceSummary.setPendingamount(Amt);
				advanceSummary.setPendingpayable(Amt);
				advanceSummary.setRecoveredamount(0.0);
				advanceSummary.setRyotcode(ryotcode1);
				advanceSummary.setRyotcodeSeq(ryot.getRyotcodesequence());
				advanceSummary.setSeason(season);
				advanceSummary.setTotalamount(Amt);
				advanceSummary.setTransactioncode(transactionCode);
				advanceSummary.setVillagecode(ryot.getVillagecode());
				entityList.add(advanceSummary);
			}
			
			List advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotcode1,advcodeForKind,season);
			if(advPrincipleList != null && advPrincipleList.size()>0)
			{
				AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);

				double principle = advanceprincipleamounts1.getPrinciple();
				logger.info("AdvancePrincipleAmounts()========principle==========" + principle);
				
				double finalPrinciple = Amt+principle;
				String qry = "Update AdvancePrincipleAmounts set principle="+finalPrinciple+" where advancecode="+advcodeForKind+" and ryotcode='"+ryotcode1+"' and season='"+season+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				AdvancePrincipleAmounts advancePrincipleAmounts = new AdvancePrincipleAmounts();
				
				advancePrincipleAmounts.setAdvancecode(advcodeForKind);
				advancePrincipleAmounts.setPrinciple(Amt);
				String principleDt = authorisationFormSummaryBean.getAuthorisationDate();
				advancePrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(principleDt,Constants.GenericDateFormat.DATE_FORMAT));
				advancePrincipleAmounts.setRyotcode(ryotcode1);
				advancePrincipleAmounts.setSeason(season);
				entityList.add(advancePrincipleAmounts);
			}
			
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}

			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,authorisationFormSummaryBean.getModifyFlag());

			if (isInsertSuccess)
			{
				maxseqno = maxseqno;
				List entityList1 = new ArrayList();
				List UpdateList1 = new ArrayList();

				for (int i = 0; i < griddata.size(); i++)
				{
					AuthorisationFormDetailsBean authorisationFormDetailsBean = new ObjectMapper().readValue(griddata.get(i)
					        .toString(), AuthorisationFormDetailsBean.class);
					String indentno = authorisationFormDetailsBean.getIndentNumbers();
					String status = "true";
					String status1 = "false";
					List DateList = reasearchAndDevelopmentService.listUpdateStatusForAllIndentDetails(indentno);
					for (int j = 0; j < DateList.size(); j++)
					{
						Map dateMap = new HashMap();
						dateMap = (Map) DateList.get(j);
						Integer indentstatus = (Integer) dateMap.get("indentStatus");
						double pendingqty1 = (Double) dateMap.get("pendingQuantity");

						if ((indentstatus == 1) || (indentstatus == 2))
						{
							status = "false";

						}
						else
						{
							if(pendingqty1==0)
							{
							status1 = "true";
							}
							else
							{
							status1 = "false";
							break;

							}
							
						}
					}
					if (("true".equals(status)) && ("true".equals(status1)))
					{
						 strQry = "";
						 Qryobj = "";
						strQry = "UPDATE KindIndentSummary set indentStatus=" + 3 + " WHERE indentNo ='" + indentno + "' ";
						Qryobj = strQry;
						UpdateList1.add(Qryobj);
					}
					/*for (int j = 0; j < DateList.size(); j++)
					{
						Map dateMap = new HashMap();
						dateMap = (Map) DateList.get(j);
						Integer indentstatus = (Integer) dateMap.get("indentStatus");
						if ((indentstatus == 1) || (indentstatus == 2))
						{
							status = "false";

						}
						else
						{
							status1 = "true";
						}

					}

					if (("true".equals(status)) && ("true".equals(status1)))
					{
						String strQry = "";
						Object Qryobj = "";
						strQry = "UPDATE KindIndentSummary set indentStatus=" + 3 + " WHERE indentNo ='" + indentno
						        + "' and season='" + season + "'";
						Qryobj = strQry;
						UpdateList1.add(Qryobj);
					}*/
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList1, UpdateList1, "No");
			}
			else
			{
				maxseqno = "false";
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return "false";
		}
		return maxseqno;
	}

	private AuthorisationFormSummary prepareModelforAuthorisationSummary(AuthorisationFormSummaryBean summaryBean,String maxseqno,Integer transacode)
	{
		int status = 0;
		Integer maxId = commonService.getMaxID("AuthorisationFormSummary");
		String modifyFlag = summaryBean.getModifyFlag();
		AuthorisationFormSummary summaryModel = new AuthorisationFormSummary();
		status = GetloginuserStatus();
		String userId = getLoggedInUserName();
		String season = summaryBean.getSeason();
		summaryModel.setId(maxId);
		summaryModel.setSeason(summaryBean.getSeason());
		//String maxseqno = commonService.GetMaxAuthoriseFertiliser(season);
		String authdate = summaryBean.getAuthorisationDate();
		summaryModel.setAuthorisationDate(DateUtils.getSqlDateFromString(authdate, Constants.GenericDateFormat.DATE_FORMAT));
		summaryModel.setRyotCode(summaryBean.getRyotCode());
		summaryModel.setRyotName(summaryBean.getRyotName());
		summaryModel.setFieldOfficer(summaryBean.getFieldOfficer());
		summaryModel.setAuthorisedBy(userId);
		summaryModel.setIndentNumbers(summaryBean.getIndentNumbers());
		summaryModel.setPhoneNo(summaryBean.getPhoneNo());
		summaryModel.setAuthorisationSeqNo(maxseqno);
		summaryModel.setGrandTotal(summaryBean.getGrandTotal());
		summaryModel.setAgreementnumber(summaryBean.getAgreementnumber());
		summaryModel.setFaCode(summaryBean.getFaCode());
		summaryModel.setAckstatus(0);
		summaryModel.setTransactioncode(transacode);

		//Added by sahadeva  20-02-2017
		summaryModel.setStorecode(summaryBean.getStoreCode());
		//added by naidu on 05-03-2017
		summaryModel.setPrintflag(0);
		return summaryModel;
	}

	private AuthorisationFormDetails prepareModelforAuthorisationDetails(AuthorisationFormDetailsBean detailsBean,AuthorisationFormSummaryBean summaryBean,String maxseqno)
	{
		AuthorisationFormDetails modelDetials = new AuthorisationFormDetails();
		modelDetials.setSeason(summaryBean.getSeason());
		//String maxseqno = commonService.GetMaxAuthoriseFertiliser(summaryBean.getSeason());
		modelDetials.setAuthorisationSeqNo(maxseqno);
		String dateofAutorisation = summaryBean.getAuthorisationDate();
		modelDetials.setDateofAutorisation(DateUtils.getSqlDateFromString(dateofAutorisation,
		        Constants.GenericDateFormat.DATE_FORMAT));
		modelDetials.setTotalCost(detailsBean.getTotalCost());
		modelDetials.setCostofEachItem(detailsBean.getCostofEachItem());
		modelDetials.setQuantity(detailsBean.getQuantity());
		modelDetials.setIndentNumbers(detailsBean.getIndentNumbers());
		modelDetials.setItemtype(detailsBean.getKind());
		modelDetials.setAckstatus(0);

		modelDetials.setAllowedqty(detailsBean.getAllowed());
		modelDetials.setPendingqty(detailsBean.getPending());
		modelDetials.setIndentqty(detailsBean.getIndent());
		return modelDetials;
	}

	@RequestMapping(value = "/getLoginNameFromEmployeeId", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getLoginIdFromEmployeeId(@RequestBody JSONObject jsonData) throws Exception
	{
		String loginid = (String) jsonData.get("loginId");
		List EmployeeList = new ArrayList();
		HashMap hm = null;
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		boolean isRecordThere = false;
		List EmployeeData = commonService.getEmployeeData(loginid);
		if (EmployeeData != null && EmployeeData.size() > 0)
		{
			Map rytMap = new HashMap();
			rytMap = (Map) EmployeeData.get(0);
			Integer feildofficername = (Integer) rytMap.get("employeeid");

			jsonObj.put("fieldOfficer", feildofficername);
			jArray.add(jsonObj);
		}
		return jArray;

	}

	@RequestMapping(value = "/getAuthorisePrintDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONObject getAuthorisePrintDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		String authoriseno = (String) jsonData.get("AuthFerNo");
		String Season = (String) jsonData.get("Season");

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		JSONObject jsonObj1 = null;
		jsonObj1 = new JSONObject();
		String kind = "";
		Integer totalbags = 0;
		List AckDataList = reasearchAndDevelopmentService.getAuthorisePrintsummary(authoriseno, Season);
		String ryotcode = "";
		String ryotname = "";
		if (AckDataList != null && AckDataList.size() > 0)
		{
			Map ackMap = new HashMap();
			ackMap = (Map) AckDataList.get(0);
			Date authorisationdate = (Date) ackMap.get("authorisationDate");
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strauthDate = df.format(authorisationdate);
			ryotcode = (String) ackMap.get("ryotCode");
			ryotname = (String) ackMap.get("ryotName");
			//Added by sahadeva 20-02-2017
			String storeCode=(String) ackMap.get("storecode");
			String storeCity=reasearchAndDevelopmentService.getAuthozationStoreCity(storeCode);
			List getRyotDetailsList = reasearchAndDevelopmentService.getryotDetails(ryotcode);
			String village = "NA";
			String mandal = "NA";
			String ryotAadharNo=reasearchAndDevelopmentService.getRyotAadharNo(ryotcode);
			if (getRyotDetailsList != null && getRyotDetailsList.size() > 0)
			{
				Map rytMap = new HashMap();
				rytMap = (Map) getRyotDetailsList.get(0);
				String villagecode = (String) rytMap.get("landvillagecode");
				Integer mandalcode = (Integer) rytMap.get("mandalcode");
				List<Village> villageNamelist = ahFuctionalService.getVillageForLedger(villagecode);
				if (villageNamelist != null && villageNamelist.size() > 0)
				{
					village = villageNamelist.get(0).getVillage();
				}
				List mandalData = ahFuctionalService.getMandalDetailsNew(mandalcode);
				if (mandalData != null && mandalData.size() > 0)
				{
					Map rytMap1 = new HashMap();
					rytMap1 = (Map) mandalData.get(0);
					mandal = (String) rytMap1.get("mandal");
				}
			}
			jsonObj.put("authSeqNo", authoriseno);
			jsonObj.put("AuthDate", strauthDate);
			jsonObj.put("RyotName", ryotname);
			jsonObj.put("RyotCode", ryotcode);
			jsonObj.put("village", village);
			jsonObj.put("mandal", mandal);
			jsonObj.put("storeCode", storeCode);
			jsonObj.put("storeCity", storeCity);
			jsonObj.put("season", Season);
			jsonObj.put("ryotAadharNo", ryotAadharNo);
			jArray.add(jsonObj);
			jsonObj1.put("formData", jArray);
		}
		Integer nooffertilisers = 0;
		Integer noofbags = 0;
		JSONArray jArray2 = new JSONArray();
		List getauthdetailsList = reasearchAndDevelopmentService.getAuthorisePrintdetals(authoriseno, Season);
		if (getauthdetailsList != null && getauthdetailsList.size() > 0)
		{
			for (int j = 0; j < getauthdetailsList.size(); j++)
			{
				JSONObject subjsonObj = new JSONObject();
				Map rytMap2 = new HashMap();
				rytMap2 = (Map) getauthdetailsList.get(j);
				nooffertilisers = (Integer) rytMap2.get("itemtype");
				noofbags = (Integer) rytMap2.get("quantity");
				totalbags = totalbags + noofbags;

				List DateList = reasearchAndDevelopmentService.listAllKindDetailsByType(nooffertilisers);
				if (DateList != null && DateList.size() > 0)
				{

					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(0);
					kind = (String) dateMap.get("kindType");
				}

				subjsonObj.put("Fertiliser", kind);
				subjsonObj.put("NoOfBags", noofbags);

				jArray2.add(subjsonObj);
			}
			JSONObject totalObj = new JSONObject();
			JSONArray jArray3 = new JSONArray();
			totalObj.put("totalBags", totalbags);

			jArray3.add(totalObj);
			jsonObj1.put("Total", jArray3);
		}

		jsonObj1.put("gridData", jArray2);
		return jsonObj1;
	}

	@RequestMapping(value = "/getAllDispatchDetailsforAcK", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllDispatchDetailsforAcK(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String DispatchNo = (String) jsonData.get("dispatchNo");

		List<DispatchSummary> dispatchSummary = reasearchAndDevelopmentService.listAllDispatchAuthorisationSummary(Season,
		        DispatchNo);
		DispatchAckSummaryBean dispatchAckSummaryBean = prepareAuthorisationSummary(dispatchSummary);

		List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.listAllDispatchAuthorisationDetails(Season,
		        DispatchNo);
		List<DispatchAckDetailsBean> dispatchAckDetailsBean = prepareAuthorisationDetails(dispatchDetails);

		org.json.JSONObject SummaryJson = new org.json.JSONObject(dispatchAckSummaryBean);
		org.json.JSONArray DetailsArray = new org.json.JSONArray(dispatchAckDetailsBean);

		response.put("DispatchAckSummary", SummaryJson);
		response.put("DispatchAckDetails", DetailsArray);

		logger.info("------getAllDispatchDetailsforAcK-----" + response.toString());
		return response.toString();

	}

	private List<DispatchAckDetailsBean> prepareAuthorisationDetails(List<DispatchDetails> dispatchDetails)
	{
		List<DispatchAckDetailsBean> beans = null;
		if (dispatchDetails != null && !dispatchDetails.isEmpty())
		{
			beans = new ArrayList<DispatchAckDetailsBean>();
			DispatchAckDetailsBean bean = null;
			for (DispatchDetails dispatchDetail : dispatchDetails)
			{
				bean = new DispatchAckDetailsBean();
				bean.setId(dispatchDetail.getId());
				bean.setSeason(dispatchDetail.getSeason());
				bean.setVariety(dispatchDetail.getVariety());
				bean.setIndentNo(dispatchDetail.getIndentNo());
				bean.setBatchNo(dispatchDetail.getBatch());
				bean.setNoOfSeedlings(dispatchDetail.getNoOfSeedlings());
				bean.setCostOfEachItem(dispatchDetail.getSeedlingCost());
				bean.setTotalCost(dispatchDetail.getTotalSeedlingCost());
				bean.setNoOfTrays(dispatchDetail.getNoOfTrays());
				bean.setTrayType(dispatchDetail.getTrayTypeCode());
				bean.setTrayCost(dispatchDetail.getTrayCost());
				bean.setTrayTotalCost(dispatchDetail.getTrayTotalCost());
				bean.setKind(dispatchDetail.getKind());
				beans.add(bean);
			}
		}
		return beans;
	}

	private DispatchAckSummaryBean prepareAuthorisationSummary(List<DispatchSummary> dispatchSummary)
	{
		DispatchAckSummaryBean bean = null;
		if (dispatchSummary != null)
		{
			bean = new DispatchAckSummaryBean();
			bean.setId(dispatchSummary.get(0).getId());
			bean.setSeason(dispatchSummary.get(0).getSeason());
			Date dispatchdate = dispatchSummary.get(0).getDateOfDispatch();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(dispatchdate);
			bean.setDateOfDispatch(date);
			bean.setDispatchNo(dispatchSummary.get(0).getDispatchNo());
			bean.setFaCode(dispatchSummary.get(0).getfACode());
			bean.setFoCode(dispatchSummary.get(0).getfOCode());
			bean.setDriverName(dispatchSummary.get(0).getDrPhoneNo());
			bean.setRyotPhoneNumber(dispatchSummary.get(0).getRyotPhoneNumber());
			bean.setSeedlingQty(dispatchSummary.get(0).getSeedlingQty());
			bean.setIndentNumbers(dispatchSummary.get(0).getIndentNumbers());
			bean.setVehicle(dispatchSummary.get(0).getVehicalNo());
			bean.setAgreementSign(dispatchSummary.get(0).getAgreementSign());
			bean.setSoilWaterAnalyis(dispatchSummary.get(0).getSoilWaterAnalyis());
			bean.setLandSurvey(dispatchSummary.get(0).getLandSurvey());
			bean.setRyotName(dispatchSummary.get(0).getRyotName());
			bean.setRyotcode(dispatchSummary.get(0).getRyotCode());
			bean.setDrPhoneNo(dispatchSummary.get(0).getDrPhoneNo());

		}
		return bean;
	}

	@RequestMapping(value = "/getTrayTypeamount", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getTrayTypeamount(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer traytype =(Integer) jsonData.get("trayType");
		String ryotcode = (String) jsonData.get("ryotCode");
		
		List RyotList = new ArrayList();
		String village = "";
		RyotList = (List) reasearchAndDevelopmentService.getRyotName(ryotcode);
		if (RyotList != null && RyotList.size() > 0)
		{
			Map tempMap = new HashMap();
			tempMap = (Map) RyotList.get(0);
			village = (String) tempMap.get("villagecode");
		}
		
		

		List<Village> distance = ahFuctionalService.getVillageDistance(village);
		Double vDistance = 0.0;
		if (distance != null && distance.size() > 0)
		{
			vDistance = distance.get(0).getDistance();
		}
		List<SeedlingTrayChargeByDistance> cost = reasearchAndDevelopmentService.getTraytypeCostUnitPerDistance(vDistance,
		        traytype);
		Double villageDistanceCost = 0.0;
		if (cost != null && cost.size() > 0)
		{
			villageDistanceCost = cost.get(0).getTraycharge();
		}

		jsonObj.put("TrayCost", villageDistanceCost);

		jArray.add(jsonObj);

		logger.info("getTrayTypeamount() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getOtherRyots", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getOtherRyots() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getOtherRyots();
		logger.info("getOtherRyots() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String otherRyotcode = (String) dateMap.get("ryotCode");
				Integer otherRyotcodeSeq = (Integer) dateMap.get("id");
				String otherRyotName = (String) dateMap.get("ryotName");

				jsonObj.put("Otherryotcode", otherRyotcode);
				jsonObj.put("id", otherRyotcodeSeq);
				jsonObj.put("Otherryotname", otherRyotName);

				jArray.add(jsonObj);
			}
		}
		logger.info("getOtherRyots() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getOtherRyotsnames", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getOtherRyotsnames(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getOtherRyotsnames(jsonData);
		logger.info("getOtherRyotsnames() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{

			Map dateMap = new HashMap();
			dateMap = (Map) DateList.get(0);

			String otherRyotName = (String) dateMap.get("ryotName");
			String village = (String) dateMap.get("village");

			jsonObj.put("Otherryotname", otherRyotName);
			jsonObj.put("village", village);

			jArray.add(jsonObj);

		}
		logger.info("getEstimateDates() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getseedsourceChangeSummary", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getSeedSourceChangeDetails(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String batch[] = jsonData.split("-");

		List DateList = reasearchAndDevelopmentService.getseedsourceChangeSummary(batch[0]);
		logger.info("getseedsourceChangeSummary() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date seedsupplydate1 = (Date) dateMap.get("sourcesupplieddate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String seedsupplydate = df.format(seedsupplydate1);
				Integer id = (Integer) dateMap.get("id");

				jsonObj.put("Dates", seedsupplydate + "-" + id);

				jArray.add(jsonObj);
			}
		}
		logger.info("getseedsourceChangeSummary() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getseedsourceLoadSummary", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getseedsourceLoadSummary(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String batch[] = jsonData.split("-");
		List DateList = reasearchAndDevelopmentService.getseedsourceLoadSummary(batch[0]);
		logger.info("getseedsourceLoadSummary() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{

			Map dateMap = new HashMap();
			dateMap = (Map) DateList.get(0);

			String seedtype = (String) dateMap.get("SeedType");
			String batchSeries = (String) dateMap.get("batchseries");
			String villagecode = (String) dateMap.get("landvillagecode");
			String runningyr = (String) dateMap.get("runningyear");
			String season = (String) dateMap.get("season");
			String seedsupplier = (String) dateMap.get("seedsuppliercode");
			Integer variety = (Integer) dateMap.get("varietycode");

			jsonObj.put("SeedType", seedtype);
			jsonObj.put("batchseries", batchSeries);
			jsonObj.put("landvillagecode", villagecode);
			jsonObj.put("runningyear", runningyr);
			jsonObj.put("season", season);
			jsonObj.put("seedsuppliercode", seedsupplier);
			jsonObj.put("varietycode", variety);

			
			List DateList1 = reasearchAndDevelopmentService.getseedsourceChangeSummary(batch[0]);
			logger.info("getseedsourceLoadSummary() ========DateList1==========" + DateList1);
			if (DateList1 != null && DateList1.size() > 0)
			{
				Map dateMap1 = new HashMap();
				dateMap1 = (Map) DateList1.get(0);
				Date date1 = (Date) dateMap1.get("sourcesupplieddate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String sourceofseeddate = df.format(date1);
				double avgbuds = (Double) dateMap1.get("avgbudspercane");
				double ageofcrop = (Double) dateMap1.get("ageofcrop");
				String physicalcondition = (String) dateMap1.get("physicalcondition");
				String seedcertifiedby = (String) dateMap1.get("seedcertifiedby");
				Date certifydate = (Date) dateMap1.get("certdate");
				String certificatedate = df.format(certifydate);
				String filelocation = (String) dateMap1.get("filelocation");
				double harvestingcharges = (Double) dateMap1.get("harvestingcharges");
				double transportingcharges	 = (Double) dateMap1.get("transcharges");
				String oryotvillage	 = (String) dateMap1.get("otherRyotVillage");
				//added on 24-2-2017
				String consumerno	 = (String) dateMap1.get("consumercode");
				String agrementno	 = (String) dateMap1.get("aggrementNo");
				Integer purpose	 = (Integer) dateMap1.get("purpose");

				Integer circle	 = (Integer) dateMap1.get("circlecode");

				//jsonObj.put("date", sourceofseeddate);
				jsonObj.put("avgBudsPerCane", avgbuds);
				jsonObj.put("ageOfCrop", ageofcrop);
				jsonObj.put("physicalCondition", physicalcondition);
				jsonObj.put("seedCertifiedBy", seedcertifiedby);
				jsonObj.put("fileLocation", filelocation);
				jsonObj.put("harvestingCharges", harvestingcharges);
				jsonObj.put("transCharges", transportingcharges);
				jsonObj.put("certDate", certificatedate);
				jsonObj.put("circleCode", circle);
				jsonObj.put("otherRyotVillage", oryotvillage);
				//added on 24-2-2017
				jsonObj.put("agreementno", agrementno);
				jsonObj.put("consumercode", consumerno);
				jsonObj.put("purpose", purpose);

				jArray.add(jsonObj);

			}
		}
		logger.info("getseedsourceLoadSummary() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getLabourContratorDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getLabourContratorDetails(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		//List DateList = reasearchAndDevelopmentService.getseedsourceChangeSummary(jsonData);
		List DateList = reasearchAndDevelopmentService.getContratorDetails();
		logger.info("getContratorDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				//exception is coming here, so commented out - makella 
				//Integer  sattus = (Integer) dateMap.get("status");
				String name = (String) dateMap.get("lcName");
				Integer code = (Integer) dateMap.get("lcCode");
				String description = (String) dateMap.get("description");
				Integer id = (Integer) dateMap.get("id");

				//jsonObj.put("status", sattus);
				jsonObj.put("lcName", name);
				jsonObj.put("lcCode", code);
				jsonObj.put("description", description);
				jsonObj.put("id", id);

				jArray.add(jsonObj);
			}
		}
		logger.info("getContratorDetails() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getTransportContratorDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getTransportContratorDetails(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();

		List DateList = reasearchAndDevelopmentService.getTransportContratorDetails();
		logger.info("getTransportContratorDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				//exception is coming here, so commented out - makella 
				//Integer  sattus = (Integer) dateMap.get("status");
				String name = (String) dateMap.get("tcName");
				Integer code = (Integer) dateMap.get("tcCode");
				String description = (String) dateMap.get("description");
				Integer id = (Integer) dateMap.get("id");

				//jsonObj.put("status", sattus);
				jsonObj.put("lcName", name);
				jsonObj.put("lcCode", code);
				jsonObj.put("description", description);
				jsonObj.put("id", id);

				jArray.add(jsonObj);
			}
		}
		logger.info("getContratorDetails() ========jArray==========" + jArray);
		return jArray;
	}

	@RequestMapping(value = "/getaggrementDetails", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getaggrementDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		String ryotcode=(String)jsonData.get("ryotcode");
		String season=(String)jsonData.get("season");
		
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getaggrementDetails(ryotcode,season);
		logger.info("getaggrementDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				String aggrementno = (String) dateMap.get("agreementnumber");

				jsonObj.put("agreementnumber", aggrementno);

				jArray.add(jsonObj);
			}
		}
		logger.info("getaggrementDetails() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getaggrementDetailsauth", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getaggrementDetailsauth(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		String season=(String)jsonData.get("season");
		String ryotcode=(String)jsonData.get("ryotcode");
		
		
		JSONObject jsonObj = null;
		
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getauthaggrementDetails(season,ryotcode);
		logger.info("getaggrementDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				String aggrementno = (String) dateMap.get("agreementnumber");

				jsonObj.put("agreementnumber", aggrementno);

				jArray.add(jsonObj);
			}
		}
		logger.info("getaggrementDetails() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAllAgreementDetailsfordispatch", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONObject getAllAgreementDetails(@RequestBody JSONObject jsonData)	throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		String agreementnumber = (String) jsonData.get("agreementnumber");
		String screen = (String) jsonData.get("Screen");

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		JSONArray jArray1 = new JSONArray();
		JSONObject jsonObj1 = null;
		jsonObj1 = new JSONObject();
		JSONArray jArray2 = new JSONArray();
		JSONObject jsonObj3 =new JSONObject();
		double seedlingacr=0;
		
		//added by naidu on 29-01-2017
		
		/*
			List<AgreementDetailsBean> agreementDetailsBeans = ahFunctionalController.prepareListofRandDAgreementDetailsBeans(ahFuctionalService.getranddAgreementDetailsBySeason(season, agreementnumber));
			double seedlingacr=0;
			seedlingacr=agreementDetailsBeans.get(0).getExtentSize();
			*/
		if("auth".equals(screen))
		{
		 seedlingacr=reasearchAndDevelopmentService.getMaxSeedlingsExtentForAgrauth(agreementnumber,season);
		}
		else
		{
		 seedlingacr=reasearchAndDevelopmentService.getMaxSeedlingsExtentForAgr(agreementnumber,season);
		}
			double seedlingqty=seedlingacr*10000;
			AgreementSummaryBean agreementSummaryBean =null;
			if("auth".equals(screen))
			{
				agreementSummaryBean = ahFunctionalController.prepareBeanForAgreementSummary(ahFuctionalService.getAgreementSummaryBySeason(season, agreementnumber));

			}
			else
			{
				agreementSummaryBean = ahFunctionalController.prepareBeanForRandDAgreementSummary(ahFuctionalService.getranddAgreementSummaryBySeason(season, agreementnumber));
			}
			String ryotcode=agreementSummaryBean.getRyotCode();

			List DateList3 = reasearchAndDevelopmentService.getryotdetailsforpendingseedlings(ryotcode,season);
			logger.info("getAllAgreementDetailsfordispatch() ========ryotcode==========" + ryotcode);
			int seqno =0;
			if(DateList3 != null && DateList3.size()>0)
			{
				if (!DateList3.contains(null)) 
				{
					seqno = (Integer) DateList3.get(0);
					List DateList = reasearchAndDevelopmentService.getdispatchdetailsforpendingseedlings(seqno,season);
					if (DateList != null && DateList.size() > 0)
					{
							Map dateMap = new HashMap();
							dateMap = (Map) DateList.get(0);
							seedlingqty = (Double) dateMap.get("pendingQty");
					}	
				}
				else
				{
					 seedlingqty=seedlingacr*10000;
				}
			}
			Integer circle=0;
			circle=agreementSummaryBean.getCircleCode();
			List DateList = reasearchAndDevelopmentService.getcircledetailsfordispatch(circle);
			logger.info("getAllAgreementDetailsfordispatch() ========DateList==========" + DateList);
			if (DateList != null && DateList.size() > 0)
			{
				for (int j = 0; j < DateList.size(); j++)
				{
					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(j);

					Integer fa = (Integer) dateMap.get("fieldassistantid");
					Integer fo = (Integer) dateMap.get("fieldofficerid");

					
					String feildofficerName="";
					String feildassistantName="";
					List DateList1 = reasearchAndDevelopmentService.GetFeildOfficer(fa);
					if (DateList1 != null && DateList1.size() > 0)
					{
						Map dMap = new HashMap();
						dMap = (Map) DateList1.get(0);
						feildassistantName = (String) dMap.get("fieldassistant");
					}
					List DateList2 = reasearchAndDevelopmentService.GetFeildOfficerempid(fo);
					if (DateList2 != null && DateList2.size() > 0)
					{
						Map dMap1 = new HashMap();
						dMap1 = (Map) DateList2.get(0);
						feildofficerName = (String) dMap1.get("fieldOfficer");
					}
					
					jsonObj.put("id", fo);
					jsonObj.put("employeename", feildofficerName);
					
					jsonObj1.put("id", fa);
					jsonObj1.put("fieldassistant", feildassistantName);

					jArray.add(jsonObj);
					jArray1.add(jsonObj1);
					
					jsonObj3.put("FO", jArray);
					jsonObj3.put("FA", jArray1);
					jsonObj3.put("SeedlingQty", seedlingqty);
					jsonObj3.put("noOfAcr", seedlingacr);
	

				}
			}
			return jsonObj3;
	}
	
	private AdvanceDetails prepareModelForAdvanceDetails(DispatchAckSummaryBean ryotAdvanceBean, int TransactionCode)
	{
		AdvanceDetails advanceDetails = new AdvanceDetails();
		
		Integer totalseedlings=0;
		Double totalseedlingsamt=0.0;
		

		String dispatchno=ryotAdvanceBean.getDispatchNo();
		List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.getDataForAcknowledgePrint(dispatchno);
		//Integer i = 0;
		for (DispatchDetails l : dispatchDetails)
		{
			Integer noOfSeedlings = l.getNoOfSeedlings().intValue();
			totalseedlings=totalseedlings+noOfSeedlings;
			Double Seedlingsamt = l.getTotalSeedlingCost();
			totalseedlingsamt=totalseedlingsamt+Seedlingsamt;
		}
		
		List AckDataList = reasearchAndDevelopmentService.getAckPrintData(dispatchno);
		
		String aggmntno = "";
		
		if (AckDataList != null && AckDataList.size() > 0)
		{
			Map ackMap = new HashMap();
			ackMap = (Map) AckDataList.get(0);
			aggmntno = (String) ackMap.get("agreementnumber");
		}
		List<AgreementDetailsBean> agreementDetailsBeans =null;
		if(aggmntno.contains("T"))
		{
			agreementDetailsBeans = ahFunctionalController.prepareListofRandDAgreementDetailsBeans(ahFuctionalService.getranddAgreementDetailsBySeason(ryotAdvanceBean.getSeason(), aggmntno));	
		}
		else
		{
			agreementDetailsBeans = ahFunctionalController.prepareListofAgreementDetailsBeans(ahFuctionalService.getAgreementDetailsBySeason(ryotAdvanceBean.getSeason(), aggmntno));
		}
		
		double seedlingacr=0;
		seedlingacr=agreementDetailsBeans.get(0).getExtentSize();
		
		String modifyFlag = ryotAdvanceBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			advanceDetails.setId(ryotAdvanceBean.getId());
		}
		advanceDetails.setRyotcode(ryotAdvanceBean.getRyotcode());
		advanceDetails.setSeason(ryotAdvanceBean.getSeason());
		advanceDetails.setAdvancecode(3);
		
		advanceDetails.setAdvanceamount(totalseedlingsamt);
		advanceDetails.setAgreements(aggmntno);
		advanceDetails.setExtentsize(seedlingacr);
		advanceDetails.setAdvancesubcategorycode(1);
		advanceDetails.setTotalamount(totalseedlingsamt);
		byte i=0;
		advanceDetails.setIsitseedling(i);
		advanceDetails.setIsSeedlingAdvance(i);
		advanceDetails.setSeedsuppliercode("25011127");
		advanceDetails.setSeedingsquantity(totalseedlings);
		advanceDetails.setRyotpayable(totalseedlingsamt);
		advanceDetails.setTransactioncode(TransactionCode);
		advanceDetails.setLoginId(loginController.getLoggedInUserName());
		/*
		byte isSeedlingAdvance = 0;
		int advcode = advanceDetails.getAdvancecode();
		List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcode);
		byte isSeedAdvance = 0;
		*/
			
		advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(ryotAdvanceBean.getDateOfDispatch(),Constants.GenericDateFormat.DATE_FORMAT));
		
		return advanceDetails;
	}

	private AdvanceSummary prepareModelForAdvanceSummary(DispatchAckSummaryBean ryotAdvanceBean, int TransactionCode)
	{
		
		Integer totalseedlings=0;
		Double totalseedlingsamt=0.0;
		

		String dispatchno=ryotAdvanceBean.getDispatchNo();
		List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.getDataForAcknowledgePrint(dispatchno);
		//Integer i = 0;
		for (DispatchDetails l : dispatchDetails)
		{
			Double noOfSeedlings1 = l.getNoOfSeedlings();
			Integer noOfSeedlings = noOfSeedlings1.intValue();
			totalseedlings=totalseedlings+noOfSeedlings;
			Double Seedlingsamt = l.getTotalSeedlingCost();
			totalseedlingsamt=totalseedlingsamt+Seedlingsamt;
		}
		List<Ryot> ryot = ryotService.getRyotByRyotCode(ryotAdvanceBean.getRyotcode());

		AdvanceSummary advanceSummary = new AdvanceSummary();

		String modifyFlag = ryotAdvanceBean.getModifyFlag();
		
		advanceSummary.setRyotcode(ryotAdvanceBean.getRyotcode());
		
		advanceSummary.setVillagecode(ryot.get(0).getVillagecode());
	
		advanceSummary.setAdvanceamount(totalseedlingsamt);
		advanceSummary.setAdvancecode(3);
		advanceSummary.setSeason(ryotAdvanceBean.getSeason());
		advanceSummary.setPendingamount(totalseedlingsamt);
		
		double intrestRate = 0;
		int advanceCode = 3;
		List<Double> IntrestList = new ArrayList<Double>();
		IntrestList = ahFuctionalService.getIntrestRatesByAmount(totalseedlingsamt);
		if (IntrestList != null && IntrestList.size() > 0)
		{
			double myResult1 = IntrestList.get(0);
			intrestRate = myResult1;
		}
		advanceSummary.setInterestamount(intrestRate);
		advanceSummary.setRecoveredamount(0.00);
		advanceSummary.setTotalamount(totalseedlingsamt);
		advanceSummary.setAdvancestatus((byte) 0);
		advanceSummary.setPaidamount(0.00);
		advanceSummary.setPendingpayable(totalseedlingsamt);
		advanceSummary.setTransactioncode(TransactionCode);
		advanceSummary.setLoginid(loginController.getLoggedInUserName());
		advanceSummary.setRyotcodeSeq(ryot.get(0).getRyotcodesequence());
	
		return advanceSummary;
	}

	private AdvancePrincipleAmounts prepareModelForAdvancePrincipleAmounts(	DispatchAckSummaryBean ryotAdvanceBean)
	{
		Integer totalseedlings=0;
		Double totalseedlingsamt=0.0;
		String dispatchno=ryotAdvanceBean.getDispatchNo();
		List<DispatchDetails> dispatchDetails = reasearchAndDevelopmentService.getDataForAcknowledgePrint(dispatchno);
		//Integer i = 0;
		for (DispatchDetails l : dispatchDetails)
		{
			Double noOfSeedlings1 = l.getNoOfSeedlings();
			Integer noOfSeedlings = noOfSeedlings1.intValue();
			totalseedlings=totalseedlings+noOfSeedlings;
			Double Seedlingsamt = l.getTotalSeedlingCost();
			totalseedlingsamt=totalseedlingsamt+Seedlingsamt;
		}
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AdvancePrincipleAmounts advanceprincipleamounts = new AdvancePrincipleAmounts();
		advanceprincipleamounts.setAdvancecode(3);
		advanceprincipleamounts.setRyotcode(ryotAdvanceBean.getRyotcode());
		advanceprincipleamounts.setPrinciple(totalseedlingsamt);
		advanceprincipleamounts.setSeason(ryotAdvanceBean.getSeason());
		String advDate = ryotAdvanceBean.getDateOfDispatch();
		advanceprincipleamounts.setPrincipledate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		
		return advanceprincipleamounts;
	}
	/*
	private SeedSuppliersSummary prepareModelForSeedSuppliersSummary(SourceOfSeedBean ryotAdvanceBean) 
	{
		
		
			SeedSuppliersSummary seedSuppliersSummary =null;
			List seedSupplieramtsList=null;
			//seedsupplier updation
			if(ryotAdvanceBean.getSeedSrCode()!=null)
			seedSupplieramtsList=caneAccountingFunctionalService.getSeedSupplieramts(ryotAdvanceBean.getSeason(),ryotAdvanceBean.getSeedSrCode());
			
			if(seedSupplieramtsList!=null)
			{
				seedSuppliersSummary=(SeedSuppliersSummary)seedSupplieramtsList.get(0);
				if(ryotAdvanceBean.getUpdateType()==1)
				{
					List seedlist=reasearchAndDevelopmentService.listSourceSeeds(ryotAdvanceBean.getBatchNo(), "", ryotAdvanceBean.getDates());
					SeedSource ss=(SeedSource)seedlist.get(0);
					seedSuppliersSummary.setAdvancepayable(seedSuppliersSummary.getAdvancepayable()-ss.getTotalcost()+ryotAdvanceBean.getTotalCost());
					seedSuppliersSummary.setPendingamount(seedSuppliersSummary.getPendingamount()-ss.getTotalcost()+ryotAdvanceBean.getTotalCost());
				
				}
				else
				{
			seedSuppliersSummary.setAdvancepayable(seedSuppliersSummary.getAdvancepayable()+ryotAdvanceBean.getTotalCost());
			seedSuppliersSummary.setPendingamount(seedSuppliersSummary.getPendingamount()+ryotAdvanceBean.getTotalCost());
				}
			}
			else
			{
			seedSuppliersSummary = new SeedSuppliersSummary();
			seedSuppliersSummary.setLoginid(ryotAdvanceBean.getSeedCertifiedBy());
			seedSuppliersSummary.setAdvancepayable(ryotAdvanceBean.getTotalCost());
			seedSuppliersSummary.setPaidamount(0.0);
			seedSuppliersSummary.setPendingamount(ryotAdvanceBean.getTotalCost());
			seedSuppliersSummary.setSeason(ryotAdvanceBean.getSeason());
			if(ryotAdvanceBean.getSeedSrCode()==null||ryotAdvanceBean.getSeedSrCode()=="")
			{
			seedSuppliersSummary.setSeedsuppliercode(ryotAdvanceBean.getOtherRyot());
			}
			else
			{
			seedSuppliersSummary.setSeedsuppliercode(ryotAdvanceBean.getSeedSrCode());
			}
			seedSuppliersSummary.setTotalextentsize(0.0);
			seedSuppliersSummary.setAdvancestatus((byte) 0);
			}
			return seedSuppliersSummary;
	}
	*/
	//added by naidu 04-02-2017
	private SeedConsumerDetails prepareModelForSeedConsumerDetails(SourceOfSeedBean sourceOfSeedBean,String seedConsumers) 
	{
		
		
			SeedConsumerDetails seedConsumerDetails =new SeedConsumerDetails();
			seedConsumerDetails.setBatchno(sourceOfSeedBean.getBatchNo());
			seedConsumerDetails.setConsumercode(seedConsumers);
			seedConsumerDetails.setSeason(sourceOfSeedBean.getSeason());
			seedConsumerDetails.setSeedsrcode(sourceOfSeedBean.getSeedSrCode());
			seedConsumerDetails.setSourcesupplieddate(DateUtils.getSqlDateFromString(sourceOfSeedBean.getDate(),Constants.GenericDateFormat.DATE_FORMAT));
		
		return seedConsumerDetails;
	}
	
	@RequestMapping(value = "/getDispatchNoDetailsForPrint", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getDispatchNoDetailsForPrint(@RequestBody JSONObject jsonData) throws Exception
	{
		String fromdate = (String) jsonData.get("dateFrom");
		String todate = (String) jsonData.get("dateTo");
		String season = (String) jsonData.get("season");
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getDispatchNoDetailsForPrint(season,fromdate,todate);
		logger.info("getDispatchNoDetailsForPrint() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String dispatchno = (String) dateMap.get("dispatchNo");
				String ryotcode = (String) dateMap.get("ryotCode");
				String ryotname = (String) dateMap.get("ryotName");
				Integer status = (Integer) dateMap.get("dispatchedStatus");
				String agrnum = (String) dateMap.get("agreementnumber");
				
				if(agrnum.contains("T"))
				{
					jsonObj.put("agrType", 1);
				}
				else
				{
					jsonObj.put("agrType", 0);
				}
				
				String smsstatus="No";
				String acknowledgestatus="No";
				if(status==0)
				{
					smsstatus="Yes";
					acknowledgestatus="No";
				}
				else if(status==1)
				{
					smsstatus="No";
					acknowledgestatus="Yes";
				}
				else if(status==2)
				{
					smsstatus="No";
					acknowledgestatus="No";
				}
				jsonObj.put("id", j + 1);
				jsonObj.put("dispatchNo", dispatchno);
				jsonObj.put("ryotCode", ryotcode);
				jsonObj.put("ryotName", ryotname);
				jsonObj.put("smsStatus", smsstatus);
				jsonObj.put("acknowledgeStatus", acknowledgestatus);


				jArray.add(jsonObj);
			}
		}
		logger.info("getDispatchNoDetailsForPrint() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getAuthorisationDetailsForPrint", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAuthorisationDetailsForPrint(@RequestBody JSONObject jsonData) throws Exception
	{
		String fromdate = (String) jsonData.get("dateFrom");
		String todate = (String) jsonData.get("dateTo");
		String season = (String) jsonData.get("season");
		//added by naidu on 04-03-2017
		String loginid = (String) jsonData.get("loginId");
		Integer roleId =0;
		Integer printFlag=0;
		List EmployeeData = commonService.getEmployeeData(loginid);
		if (EmployeeData != null && EmployeeData.size() > 0)
		{
			Map rytMap = new HashMap();
			rytMap = (Map) EmployeeData.get(0);
			roleId = (Integer) rytMap.get("roleid");

		}
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getAuthorisationDetailsForPrint(season,fromdate,todate);
		logger.info("getAuthorisationDetailsForPrint() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String dispatchno = (String) dateMap.get("authorisationSeqNo");
				String ryotcode = (String) dateMap.get("ryotCode");
				String ryotname = (String) dateMap.get("ryotName");
				String storeCode = (String) dateMap.get("storecode");
				int pFlag=(Integer) dateMap.get("printflag");
				
				String storeCity =reasearchAndDevelopmentService.getAuthozationStoreCity(storeCode);

				jsonObj.put("id", j + 1);
				jsonObj.put("authorisationSeqNo", dispatchno);
				jsonObj.put("ryotCode", ryotcode);
				jsonObj.put("ryotName", ryotname);
				jsonObj.put("storeCity", storeCity);
				jsonObj.put("storeCode", storeCode);
				
				if(roleId==12)
				{
					printFlag=0;
				}
				else
				{
					if(pFlag==0)
						printFlag=0;
					else
						printFlag=1;
				}
				jsonObj.put("printFlag", printFlag);
				
				jArray.add(jsonObj);
			}
		}
		logger.info("getAuthorisationDetailsForPrint() ========jArray==========" + jArray);
		return jArray;
	}
	
	
	
	@RequestMapping(value = "/GetSMSStatus", method = RequestMethod.POST)
	public @ResponseBody
	boolean GetSMSStatus(@RequestBody String jsonData) throws Exception
	{
		
			boolean isInsertSuccess = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String Dateofindent = "NA";
			String indentNumbers = "NA";
			Double issuedQty = 0.0;
			String deliveryDate = "NA";
			String variety = "";
			String ryotPhoneNumber="NA";
			String vehicalno="NA";
			List DateList = reasearchAndDevelopmentService.getDispatchDetails(jsonData);
			logger.info("getDispatchDetails() ========DateList==========" + DateList);
			if (DateList != null && DateList.size() > 0)
			{
				
					Map dateMap = new HashMap();
					dateMap = (Map) DateList.get(0);
							
					 indentNumbers = (String) dateMap.get("indentNumbers");
					 List<KindIndentSummary> kindIndentSummary = reasearchAndDevelopmentService.listGetAllIndentSummary(indentNumbers);
					 Dateofindent = df.format(kindIndentSummary.get(0).getSummaryDate());
					 //Date deliveryDate1 = (Date) dateMap.get("deliveryDate");
						Date currentDate = new Date();
					 deliveryDate = df.format(currentDate);
					 issuedQty = (Double) dateMap.get("issuedQty");
					 ryotPhoneNumber = (String) dateMap.get("ryotPhoneNumber");
					 vehicalno = (String) dateMap.get("vehicalNo");

					 List DateList1 = reasearchAndDevelopmentService.listUpdateStatusForAllIndentDetails1(indentNumbers,issuedQty);
						for (int j = 0; j < DateList.size(); j++)
						{
							Map dateMap1 = new HashMap();
							dateMap1 = (Map) DateList1.get(j);
							Integer vCode = (Integer) dateMap1.get("variety");
							String varietyName = "NA";
							List VarietyList = new ArrayList();
							if(vCode!=null)
							{
								VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(vCode);
								if (VarietyList != null && VarietyList.size() > 0)
								{
									Map tempMap = new HashMap();
									tempMap = (Map) VarietyList.get(0);
									varietyName = (String) tempMap.get("variety");
								}
							
								if(j==0)
								{
									variety=varietyName;
								}
								else
								{
								variety=variety+ ","+varietyName;
								}
							}
						}
			}
			boolean vsl = commonService.SendSMSForRyot(ryotPhoneNumber,indentNumbers,Dateofindent,variety,issuedQty,deliveryDate,vehicalno);
		String strQry2 = "";
		Object Qryobj2 = "";
		strQry2 = "UPDATE DispatchSummary set dispatchedStatus=1 WHERE dispatchNo ='" + jsonData + "' ";
		Qryobj2 = strQry2;
		UpdateList.add(Qryobj2);
		isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList, "No");
		
		logger.info("GetSMSStatus() ========jArray==========" + isInsertSuccess);
		return isInsertSuccess;
	}
	
	@RequestMapping(value = "/getweighmentDetailsSeedSource", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getweighmentDetailsSeedSource(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getseedsourceChangeSummary(jsonData);
		logger.info("getseedsourceChangeSummary() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date seedsupplydate1 = (Date) dateMap.get("sourcesupplieddate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String seedsupplydate = df.format(seedsupplydate1);
				Integer id = (Integer) dateMap.get("id");
				
				double finalweight = (Double) dateMap.get("finalweight");
				double totalcost = (Double) dateMap.get("totalcost");
				   
			    
			    /* finalweight=(double)Math.round(finalweight * 1000d) / 1000d;
			     totalcost=(double)Math.round(totalcost * 100d) / 100d;*/
			    /* DecimalFormat df1 = new DecimalFormat("####0.00");
			     String Strtotalcost=df1.format(totalcost);
			     DecimalFormat df2 = new DecimalFormat("####0.000");
			     String strfinalweight=df2.format(finalweight);*/

			     
				jsonObj.put("Date", seedsupplydate );
				jsonObj.put("finalweight", finalweight );
				jsonObj.put("totalcost", totalcost );
				jArray.add(jsonObj);
			}
		}
		logger.info("getweighmentDetailsSeedSource() ========jArray==========" + jArray);
		return jArray;
	}
	///////////////////////////////Reports///////////////////////////////////
	
	@RequestMapping(value = "/getweighmentDetailsSeedSourceReport", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getweighmentDetailsSeedSourceReport(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		String fromdate = (String) jsonData.get("dateFrom");
		String todate = (String) jsonData.get("dateTo");
		String seedsupplier = (String) jsonData.get("seedsupplier");

		List DateList = reasearchAndDevelopmentService.getseedsourceChangeSummaryreport(fromdate,todate,seedsupplier);
		logger.info("getseedsourceChangeSummary() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date seedsupplydate1 = (Date) dateMap.get("sourcesupplieddate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String seedsupplydate = df.format(seedsupplydate1);
				Integer id = (Integer) dateMap.get("id");
				
				double finalweight = (Double) dateMap.get("finalweight");
				double totalcost = (Double) dateMap.get("totalcost");
				
				String vehicleNo = (String) dateMap.get("vehicleNo");
				String seedsrcode = (String) dateMap.get("seedsrcode");

			   
			     DecimalFormat df2 = new DecimalFormat("####0.000");
			     String strfinalweight=df2.format(finalweight);

			     
				jsonObj.put("Date", seedsupplydate );
				jsonObj.put("finalweight", strfinalweight );
				jsonObj.put("vehicalNo", vehicleNo );
				jsonObj.put("seedSupplier", seedsrcode );

				jArray.add(jsonObj);
			}
		}
		logger.info("getweighmentDetailsSeedSourceReport() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getSeedRyotsForReport", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getSeedRyotsForReport() throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getseedSupplyRyots();
		logger.info("getseedSupplyRyots() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);
				String seedsrcode = (String) dateMap.get("seedsrcode");

				jsonObj.put("seedSupplier", seedsrcode );

				jArray.add(jsonObj);
			}
		}
		logger.info("getSeedRyotsForReport() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getIndentDetailsForReport", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getIndentDetailsForReport(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		Integer Circle = (Integer) jsonData.get("Circle");
		Integer Zone = (Integer) jsonData.get("Zone");
		Integer variety = (Integer) jsonData.get("Variety");
		Integer kind = (Integer) jsonData.get("KindType");
		
		String kindtype="";
		List DateList3 = reasearchAndDevelopmentService.listAllKindDetailsByType(kind);
		logger.info("getAllKindDetailsByType() ========DateList3==========" + DateList3);
		if (DateList3 != null && DateList3.size() > 0)
		{

			Map dateMap2 = new HashMap();
			dateMap2 = (Map) DateList3.get(0);
			kindtype = (String) dateMap2.get("kindType");
		}
	
		
		List DateList = reasearchAndDevelopmentService.getIndentSummaryreport(Zone,variety,kind);
		logger.info("getIndentSummaryreport() ========DateList==========" + DateList);

		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				String zonename="";
				String varietyName="";
				String circleName="";
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				Date deliverydate1 = (Date) dateMap.get("batchDate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String deliverydate = df.format(deliverydate1);
				Date indentdate1 = (Date) dateMap.get("SummaryDate");
				String indentdate = df.format(indentdate1);
				String IndentNo = (String) dateMap.get("indentNo");
				Integer zone = (Integer) dateMap.get("zone");
				Integer fieldMan = (Integer) dateMap.get("fieldMan");
				Integer varietycode= (Integer) dateMap.get("variety");
				Double pendingQuantity = (Double) dateMap.get("pendingQuantity");
				
				List<String> ZoneList = new ArrayList<String>();
				ZoneList = ahFuctionalService.getZone(zone);
				if (ZoneList != null && ZoneList.size() > 0)
				{
					String myResult1 = ZoneList.get(0);
					zonename = myResult1;
				}
				
				List VarietyList = new ArrayList();
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(varietycode);
				if (VarietyList != null && VarietyList.size() > 0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				
				
				
				List CircleList = new ArrayList<String>();
				CircleList = reasearchAndDevelopmentService.getcircledetailsbyfieldman(fieldMan);
				Integer circleCode=0;
				if (CircleList != null && CircleList.size() > 0)
				{
						Map dateMap1 = new HashMap();
						dateMap1 = (Map) CircleList.get(0);
						circleCode = (Integer) dateMap1.get("circlecode");
					
				}
				
				List<String> CircleList1 = new ArrayList<String>();
				CircleList1 = ahFuctionalService.getCircle(circleCode);
				if (CircleList1 != null && CircleList1.size() > 0)
				{
					String myResult1 = CircleList1.get(0);
					circleName = myResult1;
				}
				if(Circle==0)
				{
					jsonObj.put("Kind", kindtype );
					jsonObj.put("indentNo", IndentNo );
					jsonObj.put("SummaryDate", indentdate );
					jsonObj.put("batchDate", deliverydate );
					jsonObj.put("Circle", circleName );
					jsonObj.put("zone", zonename );
					jsonObj.put("variety", varietyName );
					jsonObj.put("pendingQuantity", pendingQuantity );
					jArray.add(jsonObj);
				}
				else
				{	//if(Circle == circleCode)
					if (Circle.intValue() == circleCode.intValue())
					{
						jsonObj.put("Kind", kindtype );
						jsonObj.put("indentNo", IndentNo );
						jsonObj.put("SummaryDate", indentdate );
						jsonObj.put("batchDate", deliverydate );
						jsonObj.put("Circle", circleName );
						jsonObj.put("zone", zonename );
						jsonObj.put("variety", varietyName );
						jsonObj.put("pendingQuantity", pendingQuantity );
						jArray.add(jsonObj);
					}
				}

			}
		}
		logger.info("getIndentDetailsForReport() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getBatchMasterData", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getBatchMasterData(@RequestBody JSONObject jsonData) throws Exception
	{
		/*String fromdate = (String) jsonData.get("dateFrom");
		String todate = (String) jsonData.get("dateTo");
		String season = (String) jsonData.get("season");*/
		
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List BatchList = reasearchAndDevelopmentService.getBatchData();
		logger.info("getAuthorisationDetailsForPrint() ========DateList==========" + BatchList);
		if (BatchList != null && BatchList.size() > 0)
		{
			for (int j = 0; j < BatchList.size(); j++)
			{
				Map batchMap = new HashMap();
				batchMap = (Map) BatchList.get(j);
				Double total = (Double) batchMap.get("totalSeedlings");
				String batch = (String) batchMap.get("batch");
				Double delivered = (Double) batchMap.get("delivered");
				Double pending = (Double) batchMap.get("balance");
				String variety = (String) batchMap.get("variety");
				String varietyName=commonService.getVarietyName(Integer.parseInt(variety));
				jsonObj.put("totalSeedlings", total);
				jsonObj.put("batch", batch);
				jsonObj.put("delivered", delivered);
				jsonObj.put("pending", pending);
				jsonObj.put("variety", varietyName);
				jArray.add(jsonObj);
			}
		}
		logger.info("getBatchMasterData() ========jArray==========" + jArray);
		return jArray;
	}
	
	@RequestMapping(value = "/getStoreAuthorization", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getStoreAuthorization(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		
		
		List DataList = reasearchAndDevelopmentService.getStoreAuthorization();
		logger.info("getStoreAuthorization() ========DateList==========" + DataList);

		if (DataList != null && DataList.size() > 0)
		{
			for (int j = 0; j < DataList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DataList.get(j);

				String storeCode = (String) dateMap.get("storecode");
				String city = (String) dateMap.get("city");
				jsonObj.put("storeCode", storeCode );
				jsonObj.put("city", city );

				jArray.add(jsonObj);
			}
		}
		logger.info("getStoreAuthorization() ========jArray==========" + jArray);
		return jArray;
	}
	
	
	@RequestMapping(value = "/getAllAuthorizationDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllAuthorizationDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String Season = (String) jsonData.get("season");
		String authorisationSeqNo = (String) jsonData.get("authorisationSeqNo");

		List<AuthorisationFormSummary> authorisationFormSummary = reasearchAndDevelopmentService.listAllAuthorisationFormSummary(Season,authorisationSeqNo);
		AuthorisationAcknowledgementSummaryBean authorisationFormSummaryBean = prepareAuthorisationFormSummary(authorisationFormSummary);

		List<AuthorisationFormDetails> authorisationAcknowledgementDetails = reasearchAndDevelopmentService.listAllAuthorisationFormDetails(Season,authorisationSeqNo);
		List<AuthorisationAcknowledgementDetailsBean> authorisationFormDetailsBean = prepareAuthorisationFormDetails(authorisationAcknowledgementDetails);

		org.json.JSONObject AuthorisationSummaryJson = new org.json.JSONObject(authorisationFormSummaryBean);
		org.json.JSONArray AuthorisationDetailsArray = new org.json.JSONArray(authorisationFormDetailsBean);

		response.put("AuthorisationFormSummary", AuthorisationSummaryJson);
		response.put("AuthorisationFormDetails", AuthorisationDetailsArray);

		logger.info("------getAllAuthorizationDetails-----" + response.toString());
		return response.toString();

	}
	
	private AuthorisationAcknowledgementSummaryBean prepareAuthorisationFormSummary(List<AuthorisationFormSummary> authorisationFormSummary)
	{
		AuthorisationAcknowledgementSummaryBean bean = null;
		if (authorisationFormSummary != null)
		{
			bean = new AuthorisationAcknowledgementSummaryBean();
			bean.setId(authorisationFormSummary.get(0).getId());
			bean.setAgreementnumber(authorisationFormSummary.get(0).getAgreementnumber());
			Date aDate = authorisationFormSummary.get(0).getAuthorisationDate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String authorizationDate = df.format(aDate);
			bean.setAuthorisationDate(authorizationDate);
			bean.setAuthorisationSeqNo(authorisationFormSummary.get(0).getAuthorisationSeqNo());
			bean.setAuthorisedBy(authorisationFormSummary.get(0).getAuthorisedBy());
			bean.setFaCode(authorisationFormSummary.get(0).getFaCode());
			bean.setFieldOfficer(authorisationFormSummary.get(0).getFieldOfficer());
			bean.setIndentNumbers(authorisationFormSummary.get(0).getIndentNumbers());
			bean.setPhoneNo(authorisationFormSummary.get(0).getPhoneNo());
			bean.setRyotCode(authorisationFormSummary.get(0).getRyotCode());
			bean.setRyotName(authorisationFormSummary.get(0).getRyotName());
			bean.setSeason(authorisationFormSummary.get(0).getSeason());
			bean.setStoreCode(authorisationFormSummary.get(0).getStorecode());
			bean.setGrandTotal(authorisationFormSummary.get(0).getGrandTotal());

		}
		return bean;
	}
	
	private List<AuthorisationAcknowledgementDetailsBean> prepareAuthorisationFormDetails(List<AuthorisationFormDetails> authorisationFormDetails)
	{
		List<AuthorisationAcknowledgementDetailsBean> beans = null;
		if (authorisationFormDetails != null && !authorisationFormDetails.isEmpty())
		{
			beans = new ArrayList<AuthorisationAcknowledgementDetailsBean>();
			AuthorisationAcknowledgementDetailsBean bean = null;
			for (AuthorisationFormDetails authorisationFormDetail : authorisationFormDetails)
			{
				bean = new AuthorisationAcknowledgementDetailsBean();
				bean.setId(authorisationFormDetail.getId());
				bean.setSeason(authorisationFormDetail.getSeason());
				bean.setAuthorisationSeqNo(authorisationFormDetail.getAuthorisationSeqNo());
				bean.setIndentNumbers(authorisationFormDetail.getIndentNumbers());
				bean.setQuantity(authorisationFormDetail.getQuantity());
				bean.setCostofEachItem(authorisationFormDetail.getCostofEachItem());
				bean.setTotalCost(authorisationFormDetail.getTotalCost());
				bean.setIndentStatus(authorisationFormDetail.getIndentStatus());
				bean.setPending(authorisationFormDetail.getPendingqty());
				bean.setAllowed(authorisationFormDetail.getAllowedqty());
				bean.setIndent(authorisationFormDetail.getIndentqty());
				bean.setKind(authorisationFormDetail.getItemtype());

				Date auDate=authorisationFormDetail.getDateofAutorisation();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String authorizationDate = df.format(auDate);
				
				bean.setDateofAutorisation(authorizationDate);
		 	beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/SaveAuthorisationAcknowledgement", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean SaveAuthorisationAcknowledgement(@RequestBody JSONObject jsonData) throws Exception
	{
		
		/*
		 * 
			AuthorisationAcknowledgementSummaryBean authorisationAcknowledgementSummaryBean = new ObjectMapper().readValue(formdata.toString(), AuthorisationAcknowledgementSummaryBean.class);

			AuthorisationAcknowledgementSummary authorisationAcknowledgementSummary = prepareModelforauthorisationAcknowledgementSummary(authorisationAcknowledgementSummaryBean);

			entityList.add(authorisationAcknowledgementSummary);
			for (int i = 0; i < griddata.size(); i++)
			{
				AuthorisationAcknowledgementDetailsBean authorisationAcknowledgementDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(),
						AuthorisationAcknowledgementDetailsBean.class);
				AuthorisationAcknowledgementDetails authorisationAcknowledgementDetails = prepareModelforAuthAckmentDetails(authorisationAcknowledgementSummaryBean,authorisationAcknowledgementDetailsBean);

				entityList.add(authorisationAcknowledgementDetails);
			}
		 */
		

		JSONObject json = new JSONObject();
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();

		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		String strQry = "";
		Object Qryobj = "";
		try
		{
			JSONObject formdata = (JSONObject) jsonData.get("formData");
			JSONArray griddata = (JSONArray) jsonData.get("gridData");
			
			org.json.JSONObject response = new org.json.JSONObject();
			
			AuthorisationAcknowledgementDetails authorisationFormDetails =null;
			AuthorisationAcknowledgementSummaryBean authorisationFormSummaryBean = new ObjectMapper().readValue(formdata.toString(),
					AuthorisationAcknowledgementSummaryBean.class);
			String season = authorisationFormSummaryBean.getSeason();
			int transactionCode = commonService.GetMaxTransCode(season);


			AuthorisationAcknowledgementSummary authorisationFormSummary = prepareModelforauthorisationAcknowledgementSummary(authorisationFormSummaryBean,transactionCode);
			entityList.add(authorisationFormSummary);
			int advcodeForKind =0;
			for (int i = 0; i < griddata.size(); i++)
			{
				double deliveredquantity = 0;
				double balancequantity = 0;
				AuthorisationAcknowledgementDetailsBean authorisationFormDetailsBean = new ObjectMapper().readValue(griddata.get(i).toString(), AuthorisationAcknowledgementDetailsBean.class);
				authorisationFormDetails = prepareModelforAuthAckmentDetails(authorisationFormSummaryBean,authorisationFormDetailsBean );
				entityList.add(authorisationFormDetails);
				String indentno = authorisationFormDetailsBean.getIndentNumbers();
				double dispatchedquantity = authorisationFormDetailsBean.getQuantity();

				Integer kind = authorisationFormDetailsBean.getKind();
				advcodeForKind=commonService.getAdvcodeForkind(kind);
			}
		
			////////////////////////////////////////////////////Wrong entry posting ///////////////////////////////////
			List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcodeForKind);
			String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
			String advname=companyAdvance.get(0).getAdvance();
			String ryotcode = authorisationFormSummaryBean.getRyotCode();
			double Amt = authorisationFormSummaryBean.getTotalCost();
			String accCode = ryotcode;
			String strglCode = "FERTSAL";
			String TransactionType = "Cr";
			String journalMemo = "Charges towards wrong "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			
			if(authorisationFormSummaryBean.getGrandRyotTotal()!=0)
			{
			//Ryot Account (Authorised Ryot) Cr
			 GlCodeforAdvance = companyAdvance.get(0).getGlCode();
			 advname=companyAdvance.get(0).getAdvance();
			 ryotcode = authorisationFormSummaryBean.getRyotCode();
			 transactionCode = commonService.GetMaxTransCode(season);
			 Amt = authorisationFormSummaryBean.getTotalCost();
			 accCode = ryotcode;
			 strglCode = "FERTSAL";
			 TransactionType = "Cr";
			 journalMemo = "Charges towards wrong "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpCode = 9;//for ryot
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Advance Account (Here this is towards Fertiliser Advance) Cr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getTotalCost();
			//advcodeForKind
			accCode = GlCodeforAdvance;
			strglCode = ryotcode;
			TransactionType = "Cr";
			journalMemo = "Towards wrong "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Fertiliser Sales Dr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getTotalCost();
			accCode = "FERTSAL";
			strglCode = ryotcode;
			TransactionType = "Dr";
			journalMemo = "Towards Wrong Fertilizers Sales"+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			////////////////////////////////////////////////////Correct entry posting ///////////////////////////////////

			//Ryot Account (Authorised Ryot) Dr
			 ryotcode = authorisationFormSummaryBean.getRyotCode();
			 transactionCode = commonService.GetMaxTransCode(season);
			 Amt = authorisationFormSummaryBean.getGrandConsumerTotal();
			 accCode = ryotcode;
			 strglCode = "FERTSAL";
			 TransactionType = "Dr";
			 journalMemo = "Charges towards Correct "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpCode = 9;//for ryot
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Advance Account (Here this is towards Fertiliser Advance) Dr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getGrandConsumerTotal();
			//advcodeForKind
			accCode = GlCodeforAdvance;
			strglCode = ryotcode;
			TransactionType = "Dr";
			journalMemo = "Towards Correct "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			//Fertiliser Sales Cr
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getGrandConsumerTotal();
			accCode = "FERTSAL";
			strglCode = ryotcode;
			TransactionType = "Cr";
			journalMemo = "Towards Correct Fertilizers Sales"+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			///////////////For Corromondal Suppllier/////////////////
			
			ryotcode = authorisationFormSummaryBean.getRyotCode();
			transactionCode = commonService.GetMaxTransCode(season);
			Amt = authorisationFormSummaryBean.getGrandConsumerTotal();
			
			accCode = "CC012";
			strglCode = ryotcode;
			TransactionType = "Cr";
			journalMemo = "Towards "+advname+"--RefNo : "+authorisationFormDetails.getAuthorisationSeqNo();
			if (accCode != null)
			{
				int AccGrpCode = ahFuctionalService.GetAccGrpCode(accCode);
				String strAccGrpCode = Integer.toString(AccGrpCode);

				int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
				String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

				Map tempMap1 = new HashMap();
				tempMap1.put("ACCOUNT_CODE", accCode);
				tempMap1.put("AMOUNT", Amt);
				tempMap1.put("JRNL_MEM", journalMemo);
				tempMap1.put("SEASON", season);
				tempMap1.put("TRANS_TYPE", TransactionType);
				tempMap1.put("GL_CODE", strglCode);
				tempMap1.put("TRANSACTION_CODE", transactionCode);

				Map AccMap = caneAccountingFunctionalController.updateAccountsNew(tempMap1);
				logger.info("saveAckDispatchDetails()------AccMap" + AccMap);

				entityList.add(AccMap.get("accDtls"));
				entityList.add(AccMap.get("accGrpDtls"));
				entityList.add(AccMap.get("accSubGrpDtls"));

				Map AccSmry = new HashMap();
				AccSmry.put("ACC_CODE", accCode);
				boolean ryotAccCodetrue = AccountCodeList.contains(AccSmry);
				if (ryotAccCodetrue == false)
				{
					AccountCodeList.add(AccSmry);
					AccountSummaryMap.put(accCode, AccMap.get("accSmry"));
				}
				else
				{
					AccountSummary accountSummary = (AccountSummary) AccountSummaryMap.get(accCode);
					String straccCode = accountSummary.getAccountcode();
					if (straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
					{
						double runbal = accountSummary.getRunningbalance();
						String baltype = accountSummary.getBalancetype();
						String ses = accountSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_CODE", accCode);
						tmpMap.put("KEY", accCode);
						Map finalAccSmryMap = caneAccountingFunctionalController.returnAccountSummaryModel(tmpMap);
						AccountSummaryMap.put(accCode, finalAccSmryMap.get(accCode));
					}
				}

				Map AccGrpSmry = new HashMap();
				AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				boolean loanAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
				if (loanAccGrpCodetrue == false)
				{
					AccountGrpCodeList.add(AccGrpSmry);
					AccountGroupMap.put(strAccGrpCode, AccMap.get("accGrpSmry"));
				}
				else
				{
					AccountGroupSummary accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
					int accountGrpCode = accountGroupSummary.getAccountgroupcode();
					if (AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
					{
						double runbal = accountGroupSummary.getRunningbalance();
						String baltype = accountGroupSummary.getBalancetype();
						String ses = accountGroupSummary.getSeason();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("KEY", strAccGrpCode);

						Map finalAccGrpSmryMap = caneAccountingFunctionalController.returnAccountGroupSummaryModel(tmpMap);
						AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
					}
				}

				Map AccSubGrpSmry = new HashMap();
				//AccSubGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
				AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
				boolean ryotLoanAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
				if (ryotLoanAccSubGrpCodetrue == false)
				{
					AccountSubGrpCodeList.add(AccSubGrpSmry);
					AccountSubGroupMap.put(strAccSubGrpCode, AccMap.get("accSubGrpSmry"));
				}
				else
				{
					AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap
					        .get(strAccSubGrpCode);
					int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
					int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
					String ses = accountSubGroupSummary.getSeason();
					if (AccGrpCode == accgrpcode && AccSubGrpCode == accsubgrpcode && season.equalsIgnoreCase(ses))
					{
						double runbal = accountSubGroupSummary.getRunningbalance();
						String baltype = accountSubGroupSummary.getBalancetype();

						Map tmpMap = new HashMap();
						tmpMap.put("RUN_BAL", runbal);
						tmpMap.put("AMT", Amt);
						tmpMap.put("BAL_TYPE", baltype);
						tmpMap.put("SEASON", ses);
						tmpMap.put("TRANSACTION_TYPE", TransactionType);
						tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
						tmpMap.put("KEY", strAccSubGrpCode);
						Map finalAccSubGrpSmryMap = caneAccountingFunctionalController.returnAccountSubGroupSummaryModel(tmpMap);
						AccountSubGroupMap.put(strAccSubGrpCode, finalAccSubGrpSmryMap.get(strAccSubGrpCode));
					}
				}
			}
			
			String ryotcode2 = authorisationFormSummaryBean.getRyotCode();
			String authorisationcode = authorisationFormSummaryBean.getAuthorisationNo();
			double suppAmt = authorisationFormSummaryBean.getGrandConsumerTotal();
			double ryotAmt = authorisationFormSummaryBean.getGrandRyotTotal();


			Integer transactionno = reasearchAndDevelopmentService.getTransactioncodedetails(ryotcode2 ,"FERTSAL" ,authorisationcode);
			
			double adavanceprinciplecost = reasearchAndDevelopmentService.getPrincipalamtdetails(ryotcode2 ,advcodeForKind ,season);
			double finalprincipleamt=adavanceprinciplecost-ryotAmt;
			
			 reasearchAndDevelopmentService.getadvsummryamtdetails(ryotcode2 ,advcodeForKind ,season,ryotAmt);

			
		
			
			String qry1 = "Update AdvanceDetails set advanceamount='" + suppAmt + "',ryotpayable='" + suppAmt
			        + "' , totalamount ='" + suppAmt + "'  where transactioncode=" + transactionno + " and season='" + season + "'";
			Qryobj = qry1;
			UpdateList.add(Qryobj);
			
		
			
			String qry2 = "Update AdvancePrincipleAmounts set principle=" + finalprincipleamt + " where season='" + season + "' and ryotcode='" + ryotcode2 + "' and advancecode=11";
			Qryobj = qry2;
			UpdateList.add(Qryobj);
			
			
			
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries.next();
				Object value = entry.getValue();
				AccountSummary accountSummary = (AccountSummary) value;

				String accountCode = accountSummary.getAccountcode();
				String sesn = accountSummary.getSeason();
				double runnbal = accountSummary.getRunningbalance();
				String crOrDr = accountSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode, sesn);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountcode='" + accountCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSummary);
				}
			}

			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries1.next();
				Object value = entry.getValue();

				AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
				int accountGroupCode = accountGroupSummary.getAccountgroupcode();
				String sesn = accountGroupSummary.getSeason();
				double runnbal = accountGroupSummary.getRunningbalance();
				String crOrDr = accountGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode, sesn);
				logger.info("updateAccountsNew()------AccList" + AccList);

				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountGroupSummary);
				}
			}

			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
				Map.Entry entry = (Map.Entry) entries2.next();
				Object value = entry.getValue();
				AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;

				int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
				String sesn = accountSubGroupSummary.getSeason();
				double runnbal = accountSubGroupSummary.getRunningbalance();
				String crOrDr = accountSubGroupSummary.getBalancetype();

				List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode, accountSubGroupCode, sesn);
				logger.info("updateAccountsNew()------tempMap" + AccList);
				if (AccList != null && AccList.size() > 0)
				{
					String qry = "Update AccountSubGroupSummary set runningbalance=" + runnbal + ",balancetype='" + crOrDr
					        + "' where accountgroupcode='" + accountGroupCode + "' and accountsubgroupcode="
					        + accountSubGroupCode + " and season='" + sesn + "'";
					Qryobj = qry;
					UpdateList.add(Qryobj);
				}
				else
				{
					entityList.add(accountSubGroupSummary);
				}
			}

		
			
			
			
			
		}
			String authorisationcode = authorisationFormSummaryBean.getAuthorisationNo();

			String qry4 = "Update AuthorisationFormDetails set ackstatus=1 where authorisationSeqNo ='"+authorisationcode+"' ";
			Qryobj = qry4;
			UpdateList.add(Qryobj);
			String qry5 = "Update AuthorisationFormSummary set ackstatus=1 where authorisationSeqNo='"+authorisationcode+"'";
			Qryobj = qry5;
			UpdateList.add(Qryobj);
						
			
			
			
			
		
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList, UpdateList,authorisationFormSummaryBean.getModifyFlag());
			
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		
	}
	
	@RequestMapping(value = "/getaggrementDetailsForConsumer", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getaggrementDetailsForConsumer(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		String ryotcode=(String)jsonData.get("ryotcode");
		String season=(String)jsonData.get("season");
		
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		List DateList = reasearchAndDevelopmentService.getaggrementDetailsForConsumer(ryotcode,season);
		logger.info("getaggrementDetails() ========DateList==========" + DateList);
		if (DateList != null && DateList.size() > 0)
		{
			for (int j = 0; j < DateList.size(); j++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) DateList.get(j);

				String aggrementno = (String) dateMap.get("agreementnumber");

				jsonObj.put("agreementnumber", aggrementno);

				jArray.add(jsonObj);
			}
		}
		logger.info("getaggrementDetails() ========jArray==========" + jArray);
		return jArray;
	}
	@RequestMapping(value = "/getExtraColumnAgrExtent", method = RequestMethod.POST)
	public @ResponseBody
	JSONObject getExtraColumnAgrExtent(@RequestBody JSONObject jsonData) throws Exception
	{
		String extent="";
		JSONObject jsonObj = null;
		Double plantext=0.0;
		Double ratoonext=0.0;
		Double totalExtent=0.0;
		try
		{
		String agrNo=(String)jsonData.get("agreementnumber");
		String season=(String)jsonData.get("season");
		jsonObj = new JSONObject();
		List agrList = reasearchAndDevelopmentService.getExtentForAuthorizationAgr(agrNo,season);
		if(agrList!=null && agrList.size()>0)
		{
			
			AgreementDetails ag=null;
			for(int i=0;i<agrList.size();i++)
			{
				ag=(AgreementDetails)agrList.get(i);
				if(ag.getPlantorratoon().equalsIgnoreCase("1"))
				plantext=plantext+ag.getExtentsize();
				else
				ratoonext=ratoonext+ag.getExtentsize();
				
			}
			extent=plantext.toString()+" , "+ratoonext.toString();
			
			totalExtent=plantext+ratoonext;
			
		}
		jsonObj.put("individualExtent", extent);
		jsonObj.put("noOfAcr", totalExtent);
			return jsonObj;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(),e);
			return jsonObj;
		}
	}

//modified by naidu on 07-03-2017
	@RequestMapping(value = "/getAuthorisationStatusReport", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray getAuthorisationStatusReport(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String extent="";
		String season =null;
		Double totalExtent=0.0;
		try
		{
		String frmdate1=(String)jsonData.get("from");
		String todate1=(String)jsonData.get("to");
		Date frmdate=DateUtils.getSqlDateFromString(frmdate1,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(todate1,Constants.GenericDateFormat.DATE_FORMAT);
		jsonObj = new JSONObject();
		List authList = reasearchAndDevelopmentService.getAuthorizationStatusDetails(frmdate,todate);
		if(authList!=null && authList.size()>0)
		{
			for(int i=0;i<authList.size();i++)
			{
				Map dateMap = new HashMap();
				dateMap = (Map) authList.get(i);

				Double total = (Double) dateMap.get("GrandTotal");
				String storecode = (String) dateMap.get("storecode");
				String storeCity=reasearchAndDevelopmentService.getAuthozationStoreCity(storecode);
				String indentno = (String) dateMap.get("indentNumbers");
				String agrNo = (String) dateMap.get("agreementnumber");
				season = (String) dateMap.get("season");
				String authorisationSeqNo= (String) dateMap.get("authorisationSeqNo");

				List agrList = reasearchAndDevelopmentService.getExtentForAuthorizationAgr(agrNo,season);
				if(agrList!=null && agrList.size()>0)
				{
					Double plantext=0.0;
					Double ratoonext=0.0;
					AgreementDetails ag=null;
					for(int p=0;p<agrList.size();p++)
					{
						ag=(AgreementDetails)agrList.get(p);
						if(ag.getPlantorratoon().equalsIgnoreCase("1"))
						{
						plantext=plantext+ag.getExtentsize();
						}
						else
						{
						ratoonext=ratoonext+ag.getExtentsize();
						}
						
					}
				 	extent=plantext.toString()+" , "+ratoonext.toString();
					
					totalExtent=plantext+ratoonext;
					
				}
				String village="";
				List<KindIndentSummary> KindIndentSummary = reasearchAndDevelopmentService.listGetAllIndentSummary(indentno);
				String ryotvillagecode=KindIndentSummary.get(0).getVillage();
				List<Village> villageNamelist = ahFuctionalService.getVillageForLedger(ryotvillagecode);
				if (villageNamelist != null && villageNamelist.size() > 0)
				{
					village = villageNamelist.get(0).getVillage();
				}
				Date  indentdate1=KindIndentSummary.get(0).getSummaryDate();
				Date authorisationDate = (Date) dateMap.get("authorisationDate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String indentdate = df.format(indentdate1);
				String authdate = df.format(authorisationDate);

				String ryotName = (String) dateMap.get("ryotName");
				String ryotCode = (String) dateMap.get("ryotCode");
		
				
				int noofbags=0;
				String kind="";
				Double costofEachIte=0.0;
				List getauthdetailsList = reasearchAndDevelopmentService.getAuthorisePrintdetals(authorisationSeqNo, season);
				if (getauthdetailsList != null && getauthdetailsList.size() > 0)
				{
					for (int j = 0; j < getauthdetailsList.size(); j++)
					{
						JSONObject subjsonObj = new JSONObject();
						Map rytMap2 = new HashMap();
						rytMap2 = (Map) getauthdetailsList.get(j);
						int nooffertilisers = (Integer) rytMap2.get("itemtype");
						 noofbags = (Integer) rytMap2.get("quantity");
						 costofEachIte=(Double)rytMap2.get("costofEachItem");

						List DateList = reasearchAndDevelopmentService.listAllKindDetailsByType(nooffertilisers);
						if (DateList != null && DateList.size() > 0)
						{

							Map dateMap1 = new HashMap();
							dateMap1 = (Map) DateList.get(0);
							kind = (String) dateMap1.get("kindType");
						}
						
						if(j==0)
						{
							jsonObj.put("indentno", indentno);
							jsonObj.put("indentdate", indentdate);
							jsonObj.put("ryotCode", ryotCode);
							jsonObj.put("ryotName", ryotName);
							jsonObj.put("village", village);
							jsonObj.put("agrNo", agrNo);
							jsonObj.put("extent", extent);
							jsonObj.put("authorisationSeqNo", authorisationSeqNo);
							jsonObj.put("authorisationDate", authdate);
							jsonObj.put("total", total);
							jsonObj.put("storeCity", storeCity);
							jsonObj.put("Kind", kind);
							jsonObj.put("qty", noofbags);
							jsonObj.put("cost", costofEachIte);
							
						}
						else
						{
							jsonObj.put("indentno", "");
							jsonObj.put("indentdate", "");
							jsonObj.put("ryotCode", "");
							jsonObj.put("ryotName", "");
							jsonObj.put("village", "");
							jsonObj.put("agrNo", "");
							jsonObj.put("extent", "");
							jsonObj.put("authorisationSeqNo", "");
							jsonObj.put("authorisationDate", "");
							jsonObj.put("total", "");
							jsonObj.put("storeCity", "");
							jsonObj.put("Kind", "");
							jsonObj.put("qty", "");
							jsonObj.put("cost", "");	
							
						jsonObj.put("Kind", kind);
						jsonObj.put("qty", noofbags);
						jsonObj.put("cost", costofEachIte);
						}
						jArray.add(jsonObj);

						
					}
				}
			}
			
			
			List<KindIndentSummary> rejectIndents = reasearchAndDevelopmentService.getRejectedIndents(season);
			
			if (rejectIndents != null && rejectIndents.size() > 0)
			{
				
				for (int k = 0; k < rejectIndents.size(); k++)
				{
					String fext="";
					String kindName =null;
					String village1=null;
					KindIndentSummary rjindent=rejectIndents.get(k);
					String rvillagecode=rjindent.getVillage();
					List<Village> vNamelist = ahFuctionalService.getVillageForLedger(rvillagecode);
					if (vNamelist != null && vNamelist.size() > 0)
					{
						village1 = vNamelist.get(0).getVillage();
					}
					
					if(rjindent.getAgreementNo()!=null)
					{
						List agrList1 = reasearchAndDevelopmentService.getExtentForAuthorizationAgr(rjindent.getAgreementNo(),season);
						if(agrList1!=null && agrList1.size()>0)
						{
							Double pext=0.0;
							Double rext=0.0;
							
							AgreementDetails ag1=null;
							for(int q=0;q<agrList1.size();q++)
							{
								ag1=(AgreementDetails)agrList1.get(q);
								if(ag1.getPlantorratoon().equalsIgnoreCase("1"))
								{
									pext=pext+ag1.getExtentsize();
								}
								else
								{
									rext=rext+ag1.getExtentsize();
								}
								
							}
							fext=pext.toString()+" , "+rext.toString();
						}
							
					}
					
					
					List<KindIndentDetails>  rjIndentDetails= reasearchAndDevelopmentService.getRejectedIndentsDetails(rjindent.getIndentNo());
					
					for (int l = 0; l < rjIndentDetails.size(); l++)
					{
						JSONObject rjObject = new JSONObject();
						KindIndentDetails rjDetails=rjIndentDetails.get(l);
						//indentNo,kind,quantity
						int kind=rjDetails.getKind();
						List kindNameList = reasearchAndDevelopmentService.listAllKindDetailsByType(kind);
						if (kindNameList != null && kindNameList.size() > 0)
						{

							Map dateMap5 = new HashMap();
							dateMap5 = (Map) kindNameList.get(0);
							kindName = (String) dateMap5.get("kindType");
						}
						if(l==0)
						{
							rjObject.put("indentno", rjindent.getIndentNo());
							rjObject.put("indentdate", DateUtils.formatDate(rjindent.getSummaryDate(),Constants.GenericDateFormat.DATE_FORMAT));
							rjObject.put("ryotCode", rjindent.getRyotCode());
							rjObject.put("ryotName", rjindent.getRyotName());
							rjObject.put("village", village1);
							if(rjindent.getAgreementNo()!=null)
							{
								rjObject.put("agrNo", rjindent.getAgreementNo());
								rjObject.put("extent", fext);
							}
							else
							{
								rjObject.put("agrNo", "");
								rjObject.put("extent", "");
							}
							rjObject.put("authorisationSeqNo", "");
							rjObject.put("authorisationDate", "");
							rjObject.put("total", "");
							rjObject.put("storeCity", "");
							rjObject.put("Kind", kindName);
							rjObject.put("qty", rjDetails.getQuantity());
							rjObject.put("cost", "");
							
						}
						else
						{
							rjObject.put("indentno", "");
							rjObject.put("indentdate", "");
							rjObject.put("ryotCode", "");
							rjObject.put("ryotName", "");
							rjObject.put("village", "");
							rjObject.put("agrNo", "");
							rjObject.put("extent", "");
							rjObject.put("authorisationSeqNo", "");
							rjObject.put("authorisationDate", "");
							rjObject.put("total", "");
							rjObject.put("storeCity", "");
							rjObject.put("Kind", "");
							rjObject.put("qty", "");
							rjObject.put("cost", "");	
							
							rjObject.put("Kind", kindName);
							rjObject.put("qty", rjDetails.getQuantity());
							rjObject.put("cost", "");
						}
						jArray.add(rjObject);
					}
					
				}
			}
			
		}
			return jArray;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(),e);
			return jArray;
		}
	}
	
	
	//added by naidu on 03-03-2017
	@RequestMapping(value = "/indentReject", method = RequestMethod.POST)
	public @ResponseBody
	boolean indentReject(@RequestBody JSONObject jsonData) throws Exception
	{
	
		boolean response=false;
		try
		{
		String ryotCode=(String)jsonData.get("ryotCode");
		String season=(String)jsonData.get("season");
		String indent=(String)jsonData.get("indent");
		
		response=reasearchAndDevelopmentService.closingIndent(season,ryotCode,indent);
			return response;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(),e);
			return response;
		}
	}
	
	@RequestMapping(value = "/updatePrintFlag", method = RequestMethod.POST)
	public @ResponseBody
	boolean updatePrintFlag(@RequestBody JSONObject jsonData) throws Exception
	{
	
		boolean response=false;
		try
		{
			String season=(String)jsonData.get("season");
			String authSeqNo=(String)jsonData.get("authSeqNo");
		response=reasearchAndDevelopmentService.updatePrintFlag(authSeqNo,season);
		return response;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(),e);
			return response;
		}
	}
	
public AuthorisationAcknowledgementSummary prepareModelforauthorisationAcknowledgementSummary(AuthorisationAcknowledgementSummaryBean Bean,Integer transacode)
	  {
		  AuthorisationAcknowledgementSummary model = new AuthorisationAcknowledgementSummary();
		 	model.setId(Bean.getId());
		 	model.setSeason(Bean.getSeason());
		 	String adate=Bean.getAuthorisationDate();
		 	Date authorisationDate=DateUtils.getSqlDateFromString(adate,Constants.GenericDateFormat.DATE_FORMAT);	
		 	model.setAuthorisationDate(authorisationDate);
			model.setRyotCode(Bean.getRyotCode());
			model.setRyotName(Bean.getRyotName());
			model.setFieldOfficer(Bean.getFieldOfficer());
			model.setIndentNumbers(Bean.getIndentNumbers());
			model.setAuthorisedBy(Bean.getAuthorisedBy());
			model.setPhoneNo(Bean.getPhoneNo());
			model.setAuthorisationSeqNo(Bean.getAuthorisationSeqNo());
			model.setGrandTotal(Bean.getTotalCost());
			model.setFaCode(Bean.getFaCode());
			model.setAgreementnumber(Bean.getAgreementnumber());
			model.setStorecode(Bean.getStoreCode());
			model.setSuppliertotal(Bean.getGrandConsumerTotal());
			model.setTransactioncode(transacode);
			model.setRyottotal(Bean.getGrandRyotTotal());
			model.setBillRefDate(DateUtils.getSqlDateFromString(Bean.getBillRefDate(),Constants.GenericDateFormat.DATE_FORMAT));
			model.setBillRefNum(Bean.getBillRefNum());
		
			return model;
		}
	  public AuthorisationAcknowledgementDetails prepareModelforAuthAckmentDetails(AuthorisationAcknowledgementSummaryBean au,AuthorisationAcknowledgementDetailsBean Bean)
	  {
		  AuthorisationAcknowledgementDetails model = new AuthorisationAcknowledgementDetails();
		 	model.setId(Bean.getId());
		 	model.setSeason(Bean.getSeason());
		 	model.setAuthorisationSeqNo(Bean.getAuthorisationSeqNo());
		 	model.setIndentNumbers(Bean.getIndentNumbers());
		 	model.setQuantity(Bean.getQuantity());
		 	model.setCostofEachItem(Bean.getCostofEachItem());
		 	model.setTotalCost(Bean.getTotalCost());
		 	String adate=Bean.getDateofAutorisation();
		 	Date dateofAutorisation=DateUtils.getSqlDateFromString(adate,Constants.GenericDateFormat.DATE_FORMAT);	
		 	model.setDateofAutorisation(dateofAutorisation);
		 	model.setTaken(Bean.getTaken());
		 	model.setConsumaercost(Bean.getConsumerAmt());
		 	model.setRyotcost(Bean.getRyotAmt());
		 	model.setIndentStatus(4);
			model.setItemtype(Bean.getKind());
			return model;
		}
//added by naidu on 23-03-2017
//For SeedSupplier Summary Migration from SeedSource
	//Added by DMurty on 16-02-2017
	@RequestMapping(value = "/seedSupplierSummaryMigration", method = RequestMethod.POST)
	public @ResponseBody
	boolean seedSupplierSummaryMigration(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List sourceList = new ArrayList();
		SeedSource seedsource=null;
		SeedSuppliersSummary sssup=null;
		Double suptotalcost=0.0;
		Double textent=0.0;
		List ssList = reasearchAndDevelopmentService.getSeedSourceDetailsForMigration();
		
		if(ssList != null && ssList.size()>0)
		{
			for (int j = 0; j < ssList.size(); j++)
			{
			 	String slryotcode=(String)((Map<Object,Object>)ssList.get(j)).get("seedsrcode"); 
				
			 	
				List seedSupplieramtsList=reasearchAndDevelopmentService.getSeedSourceSummary(slryotcode);
			
				if(seedSupplieramtsList!=null && !(seedSupplieramtsList.isEmpty()) && seedSupplieramtsList.size()>0)
				{	
					suptotalcost=(Double)((Map<Object,Object>)seedSupplieramtsList.get(0)).get("totalcost");
					textent=(Double)((Map<Object,Object>)seedSupplieramtsList.get(0)).get("noofacre");
					sssup=new SeedSuppliersSummary();
					sssup.setAdvancepayable(suptotalcost);
					sssup.setAdvancestatus((byte)0);
					sssup.setLoginid("makella");
					sssup.setPaidamount(0.0);
					sssup.setPendingamount(suptotalcost);
					sssup.setSeason("2017-2018");
					sssup.setSeedsuppliercode(slryotcode);
					sssup.setTotalextentsize(textent);
				}
				entityList.add(sssup);
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);	
		}
		return isInsertSuccess;
	}
	//added  by naidu on 28-03-2017 for role based hiding screens 
	@RequestMapping(value = "/checkinLoginRole", method = RequestMethod.POST)
	public @ResponseBody
	int checkinLoginRole(@RequestBody String loginId) throws Exception 
	{
	int roleId =0;
	List EmployeeData = commonService.getEmployeeData(loginId);
	if (EmployeeData != null && EmployeeData.size() > 0)
	{
		Map rytMap = new HashMap();
		rytMap = (Map) EmployeeData.get(0);
		roleId = (Integer) rytMap.get("roleid");

	}
	return roleId;
	}

	
	//Added by sahadeva on 20-04-2017
	@RequestMapping(value = "/getCircleVillage", method = RequestMethod.POST)
	public @ResponseBody
	List getCircleVillage(@RequestBody JSONObject jsonData) throws Exception 
	{
		String supplierCode=(String)jsonData.get("supplierCode");
		String season=(String)jsonData.get("season");
		List DataList = new ArrayList();
		HashMap hm= null;
		List villagecircle = reasearchAndDevelopmentService.getVillageCircleFromSupplierCode(supplierCode,season);
		if(villagecircle.size()==0)
		{
			villagecircle = reasearchAndDevelopmentService.getVillageCircleFromRyot(supplierCode);
		}
		if(villagecircle!=null && villagecircle.size()>0)
		{
			
				hm= new HashMap();
				Map VMap=(Map)villagecircle.get(0);
				
				String village = (String) VMap.get("village");
				String circle = (String) VMap.get("circle");
				String villagecode = (String) VMap.get("villagecode");
				Integer circlecode = (Integer) VMap.get("circlecode");
				
				hm.put("villagecode", villagecode);
				hm.put("circlecode", circlecode);
				hm.put("village", village);
				hm.put("circle", circle);
				DataList.add(hm);
			
		}
		return DataList;
	}
	
	
	@RequestMapping(value = "/validateDispatchAck", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean validateDispatchAck(@RequestBody JSONObject jsonData) throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		try
		{
		String Season = (String) jsonData.get("season");
		String DispatchNo = (String) jsonData.get("dispatchNo");

		List<DispatchAckSummary> dispatchSummary = reasearchAndDevelopmentService.listOfDispatchAckForDispatchNo(Season,DispatchNo);
		if(dispatchSummary.size()>0 && dispatchSummary!=null && !dispatchSummary.isEmpty())
		return true;
		else
		return false;
		}
		catch(Exception e)
		{
			logger.info(e.getCause(),e);
			return false;
		}
	}
	
	
}


