package com.finsol.controller;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.ParallelPort;
import javax.comm.PortInUseException;
import javax.naming.NamingException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleHtmlReportConfiguration;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.impl.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.finsol.dao.HibernateDao;
import com.finsol.model.AgreementSummary;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.Ryot;
import com.finsol.model.Temp_AdvanceCheckListReport;
import com.finsol.model.Temp_AgricultureLoanApp;
import com.finsol.model.Temp_AgrmasterRyotwiseVillage;
import com.finsol.model.Temp_AreaWisePendingReport;
import com.finsol.model.Temp_AuthorisationAckDetails;
import com.finsol.model.Temp_BankLoanAndAdvance;
import com.finsol.model.Temp_BankWisePaymentToRyotSBACsForCaneAC;
import com.finsol.model.Temp_BankwiseGrowersLoanCollection;
import com.finsol.model.Temp_BruntCane;
import com.finsol.model.Temp_CaneAccDetails;
import com.finsol.model.Temp_CaneCrushingReport;
import com.finsol.model.Temp_CaneGrowersListCircleProgramWise;
import com.finsol.model.Temp_CaneLedgerReport;
import com.finsol.model.Temp_CanePaymentReport;
import com.finsol.model.Temp_CanePaymentVoucher;
import com.finsol.model.Temp_CanePricePaymentsStatement;
import com.finsol.model.Temp_CaneSampleCard;
import com.finsol.model.Temp_CaneSamplesCheckList;
import com.finsol.model.Temp_CaneSuppliers;
import com.finsol.model.Temp_CaneSupplyCheckListForWeek;
import com.finsol.model.Temp_CaneSupplyPaymentStatementForCaneAC;
import com.finsol.model.Temp_CaneToFAReport;
import com.finsol.model.Temp_CaneWeighmentReport;
import com.finsol.model.Temp_CheckListForSBAccountPortions;
import com.finsol.model.Temp_CirclePlantRatoonWiseRyot;
import com.finsol.model.Temp_DailyLogSheet;
import com.finsol.model.Temp_DeductionCheckList;
import com.finsol.model.Temp_GrowerWiseHourlyCrushing;
import com.finsol.model.Temp_HarvestContractorDtailsByRyot;
import com.finsol.model.Temp_HarvestRecoverySummaryForCaneAC;
import com.finsol.model.Temp_HarvestingContractorServiceRates;
import com.finsol.model.Temp_HarvestingDeductionsForCaneAC;
import com.finsol.model.Temp_InactiveModifiedAgreements;
import com.finsol.model.Temp_IncentiveCanePricePayment;
import com.finsol.model.Temp_LoanCheckListReport;
import com.finsol.model.Temp_LoanRecoveryReport;
import com.finsol.model.Temp_PaymentCheckList;
import com.finsol.model.Temp_PrintPermit;
import com.finsol.model.Temp_ProcurementReport;
import com.finsol.model.Temp_RankingStatementReport;
import com.finsol.model.Temp_RyotWiseWeighmentReport;
import com.finsol.model.Temp_SeedDevlopment;
import com.finsol.model.Temp_SeedDistribution;
import com.finsol.model.Temp_SeedProduction;
import com.finsol.model.Temp_SeedSeedlingLoanReport;
import com.finsol.model.Temp_SeedUtilizationReport;
import com.finsol.model.Temp_SeedUtilizationReportZoneWise;
import com.finsol.model.Temp_SeedlingProcess;
import com.finsol.model.Temp_ShiftWiseCaneCrushed;
import com.finsol.model.Temp_TransportContractorDtails;
import com.finsol.model.Temp_TransportDeductionsCheckList;
import com.finsol.model.Temp_TransportDeductionsForCane;
import com.finsol.model.Temp_TransportRecoverySummary;
import com.finsol.model.Temp_TransportSubsidyListCrushing;
import com.finsol.model.Temp_TransportingContractorServiceRates;
import com.finsol.model.Temp_UnloadContractorReport;
import com.finsol.model.Temp_UpdatePaymentsReport;
import com.finsol.model.Temp_VarietyWiseCaneCrushingCumulative;
import com.finsol.model.Temp_VarietyWiseCaneCrushingDatewise;
import com.finsol.model.Temp_VarietywiseSummary;
import com.finsol.model.Temp_VarityWiseCaneCrushingShiftsReport;
import com.finsol.model.Temp_VegetableFrootsProduction;
import com.finsol.model.Temp_VehicleTypeWiseCaneCrushingCumulative;
import com.finsol.model.Temp_VehicleTypeWiseCaneCrushingShiftsWise;
import com.finsol.model.Temp_VehicleWiseDetails;
import com.finsol.model.Temp_VillageWiseIncentiveCanePrice;
import com.finsol.model.Temp_VillageWiseTransportSubsidyList;
import com.finsol.model.Temp_YearlyLedger;
import com.finsol.model.Temp_YearlyLedgerForRyot;
import com.finsol.model.Temp_ZoneWiseWeighmentData;
import com.finsol.model.Temp_agrchecklist;
import com.finsol.model.Temp_growerregryotwise;
import com.finsol.model.Temp_villagemandalsummary;
import com.finsol.model.Tepm_CumulativeCaneCrushing;
import com.finsol.model.Village;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CommonService;
import com.finsol.service.JasperReportService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
import com.finsol.utils.EnglishNumberToWords;
import com.finsol.utils.NumberToWord;
import com.ibm.icu.util.Calendar;

@Controller
public class JasperController {
	
	private static final Logger logger = Logger.getLogger(JasperController.class);
	
	private static OutputStream outputStream;
	private static InputStream inputStream;
	private static ParallelPort parallelPort;
	private static CommPortIdentifier port;
	static byte dat = 0x02;
	public static final String PARALLEL_PORT = "LPT1";
	File reportFile =null;
	
	@Autowired
	private JasperReportService jasperReportService;
	
	@Autowired
	private CaneAccountingFunctionalService caneAccountingFunctionalService;
	
	@Autowired	
	private AgricultureHarvestingController agricultureHarvestingController;
	
	
	@Autowired	
	private AHFuctionalService ahFuctionalService;
	@Autowired
	private CommonService commonService;
	

	@Autowired
	private HibernateDao hibernatedao;

	private String plantorratoon;
	
	
	DecimalFormat HMSDecFormatter = new DecimalFormat("0.000");

	
/*	@RequestMapping(value = "/getBranchReport", method = RequestMethod.POST)
	public void geBranchReport(@RequestBody JSONObject jsonData,HttpServletRequest request,HttpServletResponse response) throws Exception 
	{
		logger.info("---json--"+jsonData);
		Integer bankId=(Integer)jsonData.get("AddedBankName");

		List branchlist=jasperReportService.getBranchDetailsByBankCode(bankId);
		logger.info("branch list-----"+branchlist.toString());
		//String s=generateReport(bankId,request,response);

	}*/
	
	@RequestMapping(value = "/generateBranchReport", method = RequestMethod.POST)
	public String generateBranchReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String bankId=request.getParameter("bankid");		
		String rptFormat = request.getParameter("type");		
	    String reportFileName = "BranchRpt";
	    
        String QueryString="branchname,city,ifsccode,contactperson from branch where bankcode ="+bankId+"";
        //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD., CHELLURU.");
        //hmParams.put("dt", frmDate);
        hmParams.put("qrs", QueryString);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		
		return null;
		
	}
	
	@RequestMapping(value = "/generateCanToFinancialAccounting", method = RequestMethod.POST)
	public String generateCanToFinancialAccounting(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("mode");	
		String season=request.getParameter("season");
		String season1[]=season.split(":");
	    String reportFileName = "CanToFinancialAccountingReport";
	    Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	    Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);		
        String QueryString="* from CanToFinancialAccounting where season ='"+season1[1]+"' and tdt between '"+fromdate+"' and '"+todate+"'";
        //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD., CHELLURU.");
        //hmParams.put("dt", frmDate);
        hmParams.put("qrs", QueryString);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		
		return null;
		
	}
	
	
	/*@RequestMapping(value = "/generateAgreementCheckListReport", method = RequestMethod.POST)
	public void generateAgreementCheckListReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("mode");
		String season=request.getParameter("season");
		String season1[]=season.split(":");
	    Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	    Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
		logger.info("currenttime="+ctime.format(new Date()));
		String sqq="truncate table temp_agrchecklist";
		try{
		Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_agrchecklist obj=null;
		String sql="select slno, ryotcode,ryotname,agreementnumber,relativename,villagecode,landvillagecode,varietycode,cropdate from agreementdetails where seasonyear='"+season1[1]+"' and cropdate between '"+fromdate+"' and '"+todate+"'";
		// Query query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
  		List ls=hibernatedao.findBySqlCriteria(sql); 
  		String villagecode=null;
  		String landvillagecode=null;
		Integer varietycode=null;
		Integer SlNo = null;
		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_agrchecklist();
	  		//Map<Object,Object> map = (Map) ls.get(k);
  			ctime = new SimpleDateFormat("HH:mm:ss");
  			logger.info("-----Getting Ryotcode of agreement------="+ctime.format(new Date()));      
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
			String agreementnumber=(String)((Map<Object,Object>)ls.get(k)).get("agreementnumber");
			String agr[] = agreementnumber.split("/");
			String agrno = agr[0];
			int nagrno = Integer.parseInt(agrno);
			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");	  			
  			landvillagecode=(String)((Map<Object,Object>)ls.get(k)).get("landvillagecode");
			varietycode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
			Date cpdate=(Date)((Map<Object,Object>)ls.get(k)).get("cropdate");
  			String cropdate=format.format(cpdate);
  			SlNo=(Integer)((Map<Object,Object>)ls.get(k)).get("slno");
			obj.setRyotname(ryotname);
  			obj.setRelativename(relativename);
			obj.setAgreementnumber(agreementnumber);
			obj.setCropdate(cropdate);
			obj.setLandvillagecode(landvillagecode);
			obj.setAgreementsqeqno(nagrno);
	       	obj.setRyotcode(ryotcode);	
	    	
	        String sql2="select isnull(extentsize,0) as plant,isnull(agreedqty,0) as pestqty from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1' and cropdate between '"+fromdate+"' and '"+todate+"' and agreementnumber = '"+agreementnumber+"' and slno = '"+SlNo+"'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double plant1=null;
			Double pestqty1=null;
	  		if(ls2.size()>0)
	  		{	
	  			plant1=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
				pestqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("pestqty"); 
	  		}
	  		else
	  		{
	  			plant1=0.0;
	  			pestqty1=0.0;
	  		}	
	  		obj.setPlant1(plant1);
			obj.setPqty1(pestqty1);
	  		
	        String sql3="select isnull(extentsize,0) as ratoon,isnull(agreedqty,0) as rqty from agreementdetails where ryotcode='"+ryotcode+"' and plantorratoon in(2,3,4) and cropdate between '"+fromdate+"' and '"+todate+"' and agreementnumber = '"+agreementnumber+"' and slno = '"+SlNo+"'";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		Double ratoon1;
			Double rqty1;
	  		if(ls3.size()>0)
	  		{
	  			ratoon1=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
	  			rqty1=(Double)((Map<Object,Object>)ls3.get(0)).get("rqty");
		  	}
	  		else
	  		{
	  			ratoon1=0.0;
	  			rqty1=0.0;
	  		}
	  		obj.setRatoon1(ratoon1);
			obj.setRqty1(rqty1);
	  
	  	
			
			String sql7="select variety from variety where varietycode="+varietycode;
	  		List ls7=hibernatedao.findBySqlCriteria(sql7);
	  		if(ls7.size()>0)
	  		{
		  		String variety=(String)((Map<Object,Object>)ls7.get(0)).get("variety");
		  		obj.setVariety(variety);
	  		}		
	  		
	  		String sql8="select village from village where villagecode="+villagecode;
			List ls8=hibernatedao.findBySqlCriteria(sql8);
			if(ls8.size()>0)
			{
				String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
				obj.setVillage(village);
			}
			
			session.save(obj);
		 
			// ls2.clear();ls3.clear();ls6.clear();ls7.clear();ls8.clear();
			
		} 
  		session.flush();
		session.close();
  		ls.clear();
  		ctime = new SimpleDateFormat("HH:mm:ss");
  		logger.info("-----Getting Ryotcode of agreement After------="+ctime.format(new Date()));   
  		
  		String reportFileName = "AgreementCheckListRpt";
  		String Query1="distinct ryotcode,ryotname,plant1,ratoon1,pqty1,rqty1,relativename,agreementnumber,village,landvillagecode,variety,cropdate,agreementsqeqno FROM temp_agrchecklist order by agreementsqeqno asc";
  		//String Query1="distinct ryotcode,ryotname,relativename,plant,agreementnumber,village,variety,cropdate FROM temp_agrchecklist order by ryotcode asc";
  		//Parameters as Map to be passed to Jasper
  		ctime = new SimpleDateFormat("HH:mm:ss");
  		logger.info("-----After  SQL Query------="+ctime.format(new Date())); 
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        ctime = new SimpleDateFormat("HH:mm:ss");
        logger.info("-----Going To Print------="+ctime.format(new Date()));
        generateReport(request,response,hmParams,reportFileName,rptFormat);
        ctime = new SimpleDateFormat("HH:mm:ss");
        logger.info("-----AfterPrint------="+ctime.format(new Date())); 
  		}  	
		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		ctime = new SimpleDateFormat("HH:mm:ss");
		logger.info("-------------------End-----------------="+ctime.format(new Date()));
	}
	*/
	
	
	@RequestMapping(value = "/generateAgreementCheckListReport", method = RequestMethod.POST)
	public void generateAgreementCheckListReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("mode");
		String season=request.getParameter("season");
		String season1[]=season.split(":");
	    Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	    Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
		logger.info("currenttime="+ctime.format(new Date()));
		String sqq="truncate table temp_agrchecklist";
		try{
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_agrchecklist obj=null;
		String sql="select a.slno,a.ryotcode,a.ryotname,a.agreementnumber,a.relativename,a.villagecode,a.plantorratoon,a.landvillagecode,a.varietycode,cropdate,isnull(a.extentsize,0) as plant,isnull(a.agreedqty,0) as pestqty,b.village,c.variety from agreementdetails a,village b,variety c where a.seasonyear='"+season1[1]+"' and a.agreementdate between '"+fromdate+"' and '"+todate+"' and a.villagecode=b.villagecode and a.varietycode=c.varietycode";
  		List ls=hibernatedao.findBySqlCriteria(sql); 
  		String villagecode=null;
  		String landvillagecode=null;
		Integer varietycode=0;
		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_agrchecklist();
	  		//Map<Object,Object> map = (Map) ls.get(k);
  			ctime = new SimpleDateFormat("HH:mm:ss");
  			logger.info("-----Getting Ryotcode of agreement------="+ctime.format(new Date()));      
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
			String agreementnumber=(String)((Map<Object,Object>)ls.get(k)).get("agreementnumber");
			String agr[] = agreementnumber.split("/");
			String agrno = agr[0];
			int nagrno = Integer.parseInt(agrno);
		
			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");	  			
  			landvillagecode=(String)((Map<Object,Object>)ls.get(k)).get("landvillagecode");
			varietycode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
			Date cpdate=(Date)((Map<Object,Object>)ls.get(k)).get("cropdate");
  			String cropdate=format.format(cpdate);
			obj.setRyotname(ryotname);
  			obj.setRelativename(relativename);
			obj.setAgreementnumber(agreementnumber);
			obj.setCropdate(cropdate);
			obj.setLandvillagecode(landvillagecode);
			obj.setAgreementsqeqno(nagrno);
	       	obj.setRyotcode(ryotcode);	
	    	
	  		Double plant1=0.00;
	  		Double pestqty1=0.00;
			String plantorratoorn=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon");
	  		if(Integer.parseInt(plantorratoorn)==1)
	  		{	
	  			plant1=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
				pestqty1=(Double)((Map<Object,Object>)ls.get(k)).get("pestqty"); 
	  		}
	  		else
	  		{
	  			plant1=0.0;
	  			pestqty1=0.0;
	  		}	
	  		obj.setPlant1(plant1);
			obj.setPqty1(pestqty1);
	  		Double ratoon1=0.00;
			Double rqty1=0.00;
	  		if(Integer.parseInt(plantorratoorn)>1)
	  		{
	  			ratoon1=(Double)((Map<Object,Object>)ls.get(k)).get("plant");
	  			rqty1=(Double)((Map<Object,Object>)ls.get(k)).get("pestqty");
		  	}
	  		else
	  		{
	  			ratoon1=0.0;
	  			rqty1=0.0;
	  		}
	  		obj.setRatoon1(ratoon1);
			obj.setRqty1(rqty1);
			String variety=(String)((Map<Object,Object>)ls.get(k)).get("variety");
			obj.setVariety(variety);
			
			String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
			obj.setVillage(village);
	  		session.save(obj);
	  		
		
		} 
  		ls.clear();
		session.flush();
		session.close();
  		ctime = new SimpleDateFormat("HH:mm:ss");
  		logger.info("-----Getting Ryotcode of agreement After------="+ctime.format(new Date()));   
  		
  		String reportFileName = "AgreementCheckListRpt";
  		String Query1="ryotcode,ryotname,plant1,ratoon1,pqty1,rqty1,relativename,agreementnumber,village,landvillagecode,variety,cropdate,agreementsqeqno FROM temp_agrchecklist order by agreementsqeqno asc";
  		//String Query1="distinct ryotcode,ryotname,relativename,plant,agreementnumber,village,variety,cropdate FROM temp_agrchecklist order by ryotcode asc";
  		//Parameters as Map to be passed to Jasper
  		ctime = new SimpleDateFormat("HH:mm:ss");
  		logger.info("-----After  SQL Query------="+ctime.format(new Date())); 
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        hmParams.put("period", fromDate+" TO "+toDate);
        ctime = new SimpleDateFormat("HH:mm:ss");
        logger.info("-----Going To Print------="+ctime.format(new Date()));
        generateReport(request,response,hmParams,reportFileName,rptFormat);
        ctime = new SimpleDateFormat("HH:mm:ss");
        logger.info("-----AfterPrint------="+ctime.format(new Date())); 
  		}  	
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		ctime = new SimpleDateFormat("HH:mm:ss");
		logger.info("-------------------End-----------------="+ctime.format(new Date()));
	}
	
	
	
	@RequestMapping(value = "/generateAgreementCircleWiseCheckListReport", method = RequestMethod.POST)
	public String generateAgreementCheckListCircleWiseReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("mode");	
		String season=request.getParameter("season");
		String season1[]=season.split(":");
	    String reportFileName = "AgreementCircleWiseCheckListRpt";
	    Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	    Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
	    String sqq="truncate table Temp_CircleWise";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		String sql="insert into Temp_CircleWise(variety,agreementnumber,ryotcode,ryotname,relativename,landvillagecode,plotnumber,cropdate,surveynumber,extentsize,aadhaarNumber,mobilenumber,accountnumber,bankcode,branchname,city,circlecode,circle) select vr.variety,a.agreementnumber,a.ryotcode,a.ryotname,a.relativename,a.landvillagecode,a.plotnumber,a.cropdate,a.surveynumber,a.extentsize,r.aadhaarNumber,r.mobilenumber,agr.accountnumber,b.bankcode,b.branchname,b.city,c.circlecode,c.circle FROM  variety vr,agreementdetails as a,agreementsummary as agr,branch AS b,ryot as r,circle as c where vr.varietycode=a.varietycode and a.agreementnumber=agr.agreementnumber and agr.branchcode = b.branchcode and a.ryotcode=r.ryotcode and a.circlecode=c.circlecode and a.seasonyear='2017-2018' and a.cropdate between '"+fromdate+"' and '"+todate+"' and a.seasonyear='"+season1[1]+"' order by c.circle asc";
		try{
			SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
		query1.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		String Query1="distinct variety,agreementnumber,ryotcode,ryotname,relativename,landvillagecode,plotnumber,cropdate,surveynumber,extentsize,aadhaarNumber,mobilenumber,accountnumber,bankcode,branchname,city,circlecode,circle FROM Temp_CircleWise order by circlecode,ryotcode";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        //hmParams.put("dt", frmDate);
        hmParams.put("qrs", Query1);
        hmParams.put("period", fromDate+" TO "+toDate);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		return null;
		
	}
	
/*
	@RequestMapping(value = "/generateAgreementMasterListRyotWiseInVillage", method = RequestMethod.POST)
	public void generateAgreementMasterListRyotWiseInVillage(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		//this  for picking numbers from combined string with number
		String mandalCode=request.getParameter("mandal");	
		String mandalCode1 = mandalCode.replaceAll("\\D", "");
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_AgrmasterRyotwiseVillage";
		try{
		Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_AgrmasterRyotwiseVillage obj=null;
		String sql="select ryotcode,ryotname,relativename,villagecode,mandalcode from agreementdetails where seasonyear='"+sName[1]+"'";
		List ls=hibernatedao.findBySqlCriteria(sql); 
		String villagecode=null;
  		Integer mandalcode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_AgrmasterRyotwiseVillage();
	  		//Map<Object,Object> map = (Map) ls.get(k);
	         	          
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	    	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  			mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
	       	obj.setRyotcode(ryotcode);	
	       	obj.setRyotname(ryotname);
  			obj.setRelativename(relativename);
  			obj.setVillagecode(villagecode);
  			obj.setMandalcode(mandalcode);
	    	
	        String sql2="select isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double plant=null;
	  		if(ls2.size()>0)
	  		{	
	  			plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
	  		   //obj.setPlant(Double.parseDouble(plant));
	  		}
	  		else
	  		{
	  			plant=0.0;
	  		}	
	  		obj.setPlant(plant);
	  		
	        String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where ryotcode="+ryotcode+" and plantorratoon in(2,3,4)";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		Double ratoon;
	  		if(ls3.size()>0)
	  		{
	  			ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
		  	}
	  		else
	  		{
	  			ratoon=0.0;
	  		}
	  		obj.setRatoon(ratoon);
	  		
		
			String sql7="select aadhaarNumber from ryot where ryotcode="+ryotcode+"";
	  		List ls7=hibernatedao.findBySqlCriteria(sql7);
	  		if(ls7.size()>0 && ls7!=null)
	  		{
		  		String aadhaarNumber=(String)((Map<Object,Object>)ls7.get(0)).get("aadhaarNumber");
		  		obj.setAadhaarnumber(aadhaarNumber);
	  		}		
	  		 String sql8="select accountnumber,ryotbankbranchcode from ryotbankdetails where ryotcode="+ryotcode+"";
	  		List ls8=hibernatedao.findBySqlCriteria(sql8);
	  		Integer ryotbankbranchcode=null;
	  		if(ls8.size()>0 && ls8!=null)
	  		{
		  		String accountnumber=(String)((Map<Object,Object>)ls8.get(0)).get("accountnumber");
		  		ryotbankbranchcode=(Integer)((Map<Object,Object>)ls8.get(0)).get("ryotbankbranchcode");
				obj.setAccountnumber(accountnumber);
				obj.setRyotbankbranchcode(ryotbankbranchcode);
	  		}
	  		String sql9="select branchname,bankcode from branch where branchcode="+ryotbankbranchcode;
	  		List ls9=hibernatedao.findBySqlCriteria(sql9);
	  		if(ls9.size()>0 && ls9!=null)
	  		{
		  		String branchname=(String)((Map<Object,Object>)ls9.get(0)).get("branchname");
		  		Integer bankcode=(Integer)((Map<Object,Object>)ls9.get(0)).get("bankcode");
				obj.setBranchname(branchname);
				obj.setBankcode(bankcode);
	  		 }
	  		 String sql0="select village from village where villagecode="+villagecode+"";
			 List ls10=hibernatedao.findBySqlCriteria(sql0);
			 if(ls10.size()>0 && ls10!=null)
			 {
		  		String village=(String)((Map<Object,Object>)ls10.get(0)).get("village");
				obj.setVillage(village);
			 }
			 String sql11="select mandal from mandal where mandalcode="+mandalcode+"";
			 List ls11=hibernatedao.findBySqlCriteria(sql11);
			 if(ls11.size()>0 && ls11!=null)
			 {
		  		String mandal=(String)((Map<Object,Object>)ls11.get(0)).get("mandal");
				obj.setMandal(mandal);
			 }
  		//for save	
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		// ls2.clear();ls3.clear();ls7.clear();ls8.clear();ls9.clear();ls10.clear();
		} 
  		ls.clear();
        String Query1;
		String reportFileName;
		if(mandalCode1.equals("0"))
		{
			reportFileName = "AgreementMasterListRyotWiseInVillage";
			Query1="distinct ryotcode,ryotname,plant,ratoon,relativename,aadhaarnumber,accountnumber,branchname,bankcode,village,villagecode FROM Temp_AgrmasterRyotwiseVillage order by villagecode";
		}
		else  
		{
			reportFileName = "AgreementMasterListRyotWiseInMandal";   
			Query1="distinct ryotcode,ryotname,relativename,plant,ratoon,mandal,aadhaarnumber,accountnumber,mandalcode,branchname,bankcode,village,villagecode FROM Temp_AgrmasterRyotwiseVillage";
		}
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	*/
	@RequestMapping(value = "/generateAgreementMasterListRyotWiseInVillage", method = RequestMethod.POST)
	public void generateAgreementMasterListRyotWiseInVillage(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		//this  for picking numbers from combined string with number
		String mandalCode=request.getParameter("mandal");	
		String mandalCode1 = mandalCode.replaceAll("\\D", "");
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_AgrmasterRyotwiseVillage";
		try{
			SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_AgrmasterRyotwiseVillage obj=null;
		String sql="select distinct a.ryotcode,a.ryotname,a.relativename,a.villagecode,a.mandalcode,a.plantorratoon,isnull(a.extentsize,0) as plant,v.village,m.mandal,r.aadhaarNumber,rb.accountnumber,rb.ryotbankbranchcode,b.branchname,b.bankcode from agreementdetails a ,village v,ryot r,ryotbankdetails rb,branch b,mandal m where seasonyear='"+sName[1]+"'and a.villagecode=v.villagecode and a.mandalcode=m.mandalcode and a.ryotcode=r.ryotcode and a.ryotcode=rb.ryotcode and rb.ryotbankbranchcode=b.branchcode;";
		List ls=hibernatedao.findBySqlCriteria(sql); 
		String villagecode=null;
  		Integer mandalcode=0;
  		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_AgrmasterRyotwiseVillage();
	  		//Map<Object,Object> map = (Map) ls.get(k);
	         	          
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	    	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  			mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
	       	obj.setRyotcode(ryotcode);	
	       	obj.setRyotname(ryotname);
  			obj.setRelativename(relativename);
  			obj.setVillagecode(villagecode);
  			obj.setMandalcode(mandalcode);
  			Double plant=0.0;
	  		String plantorratoorn=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon");
	  		if(Integer.parseInt(plantorratoorn)==1)
	  		{	
	  			plant=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
	  		   //obj.setPlant(Double.parseDouble(plant));
	  		}
	  		else
	  		{
	  			plant=0.0;
	  		}	
	  		obj.setPlant(plant);
	  		Double ratoon=0.0;
	  		if(Integer.parseInt(plantorratoorn)>1)
	  		{
	  			ratoon=(Double)((Map<Object,Object>)ls.get(k)).get("plant");
		  	}
	  		else
	  		{
	  			ratoon=0.0;
	  		}
	  		obj.setRatoon(ratoon);
	  		
		
	  		String aadhaarNumber=(String)((Map<Object,Object>)ls.get(k)).get("aadhaarNumber");
	  		obj.setAadhaarnumber(aadhaarNumber);
	  		
	  		Integer ryotbankbranchcode=0;
	  		String accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("accountnumber");
	  		ryotbankbranchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("ryotbankbranchcode");
	  		obj.setAccountnumber(accountnumber);
	  		obj.setRyotbankbranchcode(ryotbankbranchcode);
	  
	  		String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
	  		Integer bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
	  		obj.setBranchname(branchname);
	  		obj.setBankcode(bankcode);
	  	
	  		
	  		String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
	  		obj.setVillage(village);
	  		
	  		String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal");
	  		obj.setMandal(mandal);
	  		session.save(obj);
		}
  		
  		session.flush();
  		session.close();
        String Query1;
		String reportFileName;
		if(mandalCode1.equals("0"))
		{
			reportFileName = "AgreementMasterListRyotWiseInVillage";
			Query1="distinct ryotcode,ryotname,plant,ratoon,relativename,aadhaarnumber,accountnumber,branchname,bankcode,village,villagecode FROM Temp_AgrmasterRyotwiseVillage order by villagecode";
		}
		else  
		{
			reportFileName = "AgreementMasterListRyotWiseInMandal";   
			Query1="distinct ryotcode,ryotname,relativename,plant,ratoon,mandal,aadhaarnumber,accountnumber,mandalcode,branchname,bankcode,village,villagecode FROM Temp_AgrmasterRyotwiseVillage order by mandal";
		}
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	
/*	@RequestMapping(value = "/generateGrowersRegisterRyotwiseReport", method = RequestMethod.POST)
	public void generateGrowersRegisterRyotwiseReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String group=request.getParameter("GROWER");
		String ssn=request.getParameter("season");
		String rptFormat = request.getParameter("status");
		//this  for picking numbers from combined string with number
		 //String villagecd =village.replaceAll("\\D", "");
		String season[]=ssn.split(":");
		String vm[]=group.split(":");
		       
		String sqq="truncate table Temp_growerregryotwise";
		Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
 		Temp_growerregryotwise obj=null;
 		String sql="select ryotcode,ryotname,relativename,villagecode,mandalcode,surveynumber from agreementdetails where seasonyear='"+season[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql); 
  		String villagecode=null;
  		Integer mandalcode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_growerregryotwise();
	  		//Map<Object,Object> map = (Map) ls.get(k);
	         	          
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
			String surveynumber=(String)((Map<Object,Object>)ls.get(k)).get("surveynumber");
  			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  			mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
			obj.setRyotname(ryotname);
  			obj.setRelativename(relativename);
			obj.setSurveynumber(surveynumber);
			obj.setVillagecode(villagecode);
			obj.setMandalcode(mandalcode);
	       	obj.setRyotcode(ryotcode);	
	        String sql2="select isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double plant=null;
	  		if(ls2.size()>0)
	  		{	
	  			plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
	  		}
	  		else
	  		{
	  			plant=0.0;
	  		}	
	  		obj.setPlant(plant);
	        String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where ryotcode='"+ryotcode+"' and plantorratoon in(2,3,4)";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		Double ratoon;
	  		if(ls3.size()>0)
	  		{
	  			ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
		  	}
	  		else
	  		{
	  			ratoon=0.0;
	  		}
	  		obj.setRatoon(ratoon);

	  		String sql4="select nooftonsperacre from permitrules";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		if(ls4.size()>0)
	  		{
	  			Integer nooftonsperacre=(Integer)((Map<Object,Object>)ls4.get(0)).get("nooftonsperacre");
		  		obj.setNooftonsperacre(nooftonsperacre);
	  		}
	  		String sql7="select mandal from mandal where mandalcode="+mandalcode;
	  		List ls7=hibernatedao.findBySqlCriteria(sql7);
	  		if(ls7.size()>0)
	  		{
	  			String mandal=(String)((Map<Object,Object>)ls7.get(0)).get("mandal");
	  			obj.setMandal(mandal);
	  		}
	  		String sql8="select village from village where villagecode="+villagecode;
	  		List ls8=hibernatedao.findBySqlCriteria(sql8);
	  		if(ls8.size()>0)
	  		{
	  			String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
				obj.setVillage(village);
	  		}
		
	  		Session session=hibernatedao.getSessionFactory().openSession();
	  		session.save(obj);
	  		session.flush();
	  		session.close();
	  		ls2.clear();ls3.clear();ls7.clear();ls8.clear();
		} 
  		ls.clear();
        String Query1;
		String reportFileName;
		try{
		if(vm[1].equals("Village"))
		{
			reportFileName = "GrowersRegisterVillagewiseReport";
			Query1="distinct ryotcode,ryotname,relativename,plant,ratoon,surveynumber,village,nooftonsperacre,villagecode FROM Temp_growerregryotwise order by ryotname asc";
		}
		else
		{
			reportFileName="GrowersRegisterMandalwiseReport";
			Query1="distinct ryotcode,ryotname,relativename,plant,ratoon,surveynumber,mandal,nooftonsperacre,mandalcode FROM Temp_growerregryotwise order by ryotname asc";
		}
		//Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}   */
	
	@RequestMapping(value = "/generateGrowersRegisterRyotwiseReport", method = RequestMethod.POST)
	public void generateGrowersRegisterRyotwiseReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String group=request.getParameter("GROWER");
		String ssn=request.getParameter("season");
		String rptFormat = request.getParameter("status");
		//this  for picking numbers from combined string with number
		 //String villagecd =village.replaceAll("\\D", "");
		String season[]=ssn.split(":");
		String vm[]=group.split(":");
		       
		String sqq="truncate table Temp_growerregryotwise";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
 		Temp_growerregryotwise obj=null;
 		Session session=hibernatedao.getSessionFactory().openSession();
 		String sql5="select distinct(mandalcode)  from agreementdetails where seasonyear='"+season[1]+"' order by mandalcode";
  		List ls5=hibernatedao.findBySqlCriteria(sql5); 
  		String villagecode=null;
  		Integer mandalcode=0;
  		for (int i = 0; i <ls5.size(); i++) 
		{
  			mandalcode=(Integer)((Map<Object,Object>)ls5.get(i)).get("mandalcode");
  			
 		String sql="select a.slno,a.agreedqty,a.ryotcode,a.ryotname,a.relativename,a.villagecode,a.plantorratoon,a.surveynumber,isnull(a.extentsize,0) as plant,m.mandal,v.village  from agreementdetails a,mandal m,village v where seasonyear='"+season[1]+"' and a.mandalcode="+mandalcode+" and a.villagecode=v.villagecode and m.mandalcode="+mandalcode+"";
  		List ls=hibernatedao.findBySqlCriteria(sql); 
  	
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_growerregryotwise();
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       	
	       	List rlist=hibernatedao.findByNativeSql("select * from Temp_growerregryotwise where ryotcode = '"+ryotcode+"'",Temp_growerregryotwise.class);;
	       	
	       	if(rlist.size()>0)
	       	{
	       		Session session1=hibernatedao.getSessionFactory().openSession();
	       		Temp_growerregryotwise ll=(Temp_growerregryotwise)rlist.get(0);
/*	       		Map rlMap1=(Map)rlist.get(0);
	       		
	       		Double agqty=(Double)rlMap1.get("agreedqty");
	       		Double plant=(Double)rlMap1.get("plant");
	       		Double ratoon=(Double)rlMap1.get("ratoon");*/
	       		Double agqty=(Double)ll.getAgreedqty();
	       		Double plants=(Double)ll.getPlant();
	       		Double ratoons=(Double)ll.getRatoon();
	       		String plantorRatoons=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon"); 
	       		Double extSize=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
	       		Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
	       		//logger.info("Ryot Code================"+ryotcode+"======extSize==="+extSize);
	       		if(Integer.parseInt(plantorRatoons)==1)
	       		{
	       			plants=plants+extSize;
	       		}
	       		if(Integer.parseInt(plantorRatoons)>1)
	       		{
	       			ratoons=ratoons+extSize;
	       		}
	       		//logger.info("plants================"+plants+"======ratoons==="+ratoons);
	       		agqty=agqty+agreedqty;
	       		ll.setPlant(plants);
	       		ll.setRatoon(ratoons);
	       		ll.setAgreedqty(agqty);
	       		ll.setId(ll.getId());
	       		session1.saveOrUpdate(ll);
	       		session1.flush();
	       		session1.close();
	       	}
	       	else
	       	{
		       	String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
	  			String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
				String surveynumber=(String)((Map<Object,Object>)ls.get(k)).get("surveynumber");
				Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
	  			villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
	  			
				obj.setRyotname(ryotname);
	  			obj.setRelativename(relativename);
				obj.setSurveynumber(surveynumber);
				obj.setVillagecode(villagecode);
				obj.setMandalcode(mandalcode);
		       	obj.setRyotcode(ryotcode);	
		       	obj.setAgreedqty(agreedqty);
		  		Double plant=0.0;
		  		String plantorratoon=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon");
		  		if(Integer.parseInt(plantorratoon)==1)
		  		{	
		  			plant=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
		  		}
		  		else
		  		{
		  			plant=0.0;
		  		}	
		  		obj.setPlant(plant);
		  		Double ratoon;
		  		if(Integer.parseInt(plantorratoon)>1)
		  		{
		  			ratoon=(Double)((Map<Object,Object>)ls.get(k)).get("plant");
			  	}
		  		else
		  		{
		  			ratoon=0.0;
		  		}
		  		obj.setRatoon(ratoon);
		  		String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal");
		  		obj.setMandal(mandal);
		  		String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
		  		obj.setVillage(village);
		  		session.save(obj);
	       	}
	  	
	  		
			} 
		}
  		session.flush();session.close();
        String Query1;
		String reportFileName;
		try{
		if(vm[1].equals("Village"))
		{
			reportFileName = "GrowersRegisterVillagewiseReport";
			Query1="distinct agreedqty,ryotcode,ryotname,relativename,plant,ratoon,surveynumber,village,villagecode FROM Temp_growerregryotwise order by ryotcode asc";
		}
		else
		{
			reportFileName="GrowersRegisterMandalwiseReport";
			Query1="distinct agreedqty,ryotcode,ryotname,relativename,plant,ratoon,surveynumber,village,mandal,villagecode,mandalcode FROM Temp_growerregryotwise order by villagecode";
		}
		//Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	
	/*@RequestMapping(value = "/generateGrowerListByVillageMandalSummary", method = RequestMethod.POST)
	public void generateGrowerListByVillageMandalSummary(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		//this  for picking numbers from combined string with number
		String mandalCode=request.getParameter("mandal");	
		String mandalCode1[]=mandalCode.split(":");
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_villagemandalsummary";
		try{
		Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		 Temp_villagemandalsummary obj=null;
		 String sql="select distinct ryotcode,villagecode,mandalcode,extentsize from agreementdetails where seasonyear='"+sName[1]+"'";
		// Query query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
		 
		 String sql0="select nooftonsperacre from permitrules";
		 List ls0=hibernatedao.findBySqlCriteria(sql0);
		 //obj=new Temp_vms();
  		List ls=hibernatedao.findBySqlCriteria(sql); 
  		String villagecode=null;
  		Integer mandalcode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
			obj=new Temp_villagemandalsummary();
			Integer nooftonsperacre=(Integer)((Map<Object,Object>)ls0.get(0)).get("nooftonsperacre");
			obj.setNooftonsperacre(nooftonsperacre);
			
	  		//Map<Object,Object> map = (Map) ls.get(k);
	         	          
	       	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       	villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  			mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
  			Double extentsize=(Double)((Map<Object,Object>)ls.get(k)).get("extentsize");
  			obj.setExtentsize(extentsize);
  			obj.setVillagecode(villagecode);
  			obj.setMandalcode(mandalcode);
	       	obj.setRyotcode(ryotcode);	
	    	
	        String sql1="select distinct isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1'";
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		Double plant=null;
	  		if(ls1.size()>0)
	  		{	
	  			plant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
	  		   //obj.setPlant(Double.parseDouble(plant));
	  		}
	  		else
	  		{
	  			plant=0.0;
	  		}	
	  		obj.setPlant(plant);
	  		
	        String sql2="select distinct isnull(extentsize,0) as ratoon from agreementdetails where ryotcode="+ryotcode+" and plantorratoon in(2,3,4)";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double ratoon;
	  		if(ls2.size()>0)
	  		{
	  			ratoon=(Double)((Map<Object,Object>)ls2.get(0)).get("ratoon");
		  	}
	  		else
	  		{
	  			ratoon=0.0;
	  		}
	  		obj.setRatoon(ratoon);
	  		
		 	
	  		 String sql4="select distinct village from village where villagecode="+villagecode+"";
			 List ls4=hibernatedao.findBySqlCriteria(sql4);
			 if(ls4.size()>0)
			 {
		  		String village=(String)((Map<Object,Object>)ls4.get(0)).get("village");
				obj.setVillage(village);
			 }
			 String sql5="select distinct mandal from mandal where mandalcode="+mandalcode+"";
			 List ls5=hibernatedao.findBySqlCriteria(sql5);
			 if(ls5.size()>0 && ls5!=null)
			 {
		  		String mandal=(String)((Map<Object,Object>)ls5.get(0)).get("mandal");
				obj.setMandal(mandal);
			 }
	  		
  		// For save	
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		 
		 
		// ls1.clear();ls2.clear();ls4.clear();ls5.clear();
			
		} 
  		ls.clear(); ls0.clear();
  		
		
        String Query1;
		String reportFileName;
	
  		
		if(mandalCode1[1].equals("ALL"))
		{
			reportFileName = "GrowerListByVillageMandalSummaryRpt";
			Query1="distinct plant,ratoon,village,villagecode,mandal,mandalcode,nooftonsperacre,extentsize FROM Temp_villagemandalsummary order by village asc";
		}
		else  
		{
			reportFileName = "GrowerListByVillageMandalSummaryRpt1";   
			Query1="distinct plant,ratoon,mandal,mandalcode,village,villagecode,nooftonsperacre,extentsize FROM Temp_villagemandalsummary where mandal='"+mandalCode1[1]+"' order by village asc";
		}
		
		 //Parameters as Map to be passed to Jasper
  		
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	*/
	
/*	@RequestMapping(value = "/generateGrowerListByVillageMandalSummary", method = RequestMethod.POST)
	public void generateGrowerListByVillageMandalSummary(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
      String season=request.getParameter("season");	
      String sName[]=season.split(":");
      //this  for picking numbers from combined string with number
      String mandalCode=request.getParameter("mandal");	
      String mandalCode1[]=mandalCode.split(":");
      String rptFormat = request.getParameter("status");
      String sqq="truncate table Temp_villagemandalsummary";
      try{
       Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
       query.executeUpdate();
       Session session=hibernatedao.getSessionFactory().openSession();
       Temp_villagemandalsummary obj=new Temp_villagemandalsummary();
       String sql1="select distinct(villagecode) from agreementdetails where seasonyear='"+sName[1]+"'";
       List ls1=hibernatedao.findBySqlCriteria(sql1);
       for (int i = 0; i <ls1.size(); i++) 
       {
    	   String villagecode=(String)((Map<Object,Object>)ls1.get(i)).get("villagecode"); 
    	   String sql2="select sum(isnull(extentsize,0)) plant,plantorratoon from AgreementDetails where seasonyear='"+sName[1]+"' and villagecode='"+villagecode+"'group by plantorratoon";
    	   List ls2=hibernatedao.findBySqlCriteria(sql2);
    	   for (int a = 0; a <ls2.size(); a++) 
           {
    	   String plantorratoon=(String)((Map<Object,Object>)ls2.get(a)).get("plantorratoon"); 
    	   Double plant=0.0;
    	   Double r1=0.0;
    	   Double r2=0.0; Double r3=0.0; Double r4=0.0;
    	   if(Integer.parseInt(plantorratoon)==1)
    	   {	
    		   plant=(Double)((Map<Object,Object>)ls2.get(a)).get("plant"); 
    		   //obj.setPlant(Double.parseDouble(plant));
    	   }
    	   else
    	   {
    		   plant=0.0;
    	   }	
    	   obj.setPlant(plant);
    	   if(Integer.parseInt(plantorratoon)==2)
    	   {
    		   r1=(Double)((Map<Object,Object>)ls2.get(a)).get("plant");
    	   }
    	   else
    	   {
    		   r1=0.0;
    	   }
    	   if(Integer.parseInt(plantorratoon)==3)
    	   {
    		   r2=(Double)((Map<Object,Object>)ls2.get(a)).get("plant");
    	   }
    	   else
    	   {
    		   r2=0.0;
    	   }
    	   if(Integer.parseInt(plantorratoon)==4)
    	   {
    		   r3=(Double)((Map<Object,Object>)ls2.get(a)).get("plant");
    	   }
    	   else
    	   {
    		   r3=0.0;
    	   }
    	   if(Integer.parseInt(plantorratoon)==5)
    	   {
    		   r4=(Double)((Map<Object,Object>)ls2.get(a)).get("plant");
    	   }
    	   else
    	   {
    		   r4=0.0;
    	   }
    	   Double ratoon=r1+r2+r3+r4;
    	   obj.setRatoon(ratoon);
          
    	   String sql="select distinct a.mandalcode,a.extentsize,v.village,m.mandal from agreementdetails a,village v,mandal m where seasonyear='"+sName[1]+"' and v.villagecode='"+villagecode+"' and a.mandalcode=m.mandalcode";
    	   // String sql="select distinct a.ryotcode,a.villagecode,a.mandalcode,a.extentsize,isnull(a.extentsize,0) as plant,a.plantorratoon,v.village,m.mandal from agreementdetails a,village v,mandal m where seasonyear='"+sName[1]+"' and a.villagecode=v.villagecode and a.mandalcode=m.mandalcode";
    	   String sql0="select nooftonsperacre from permitrules";
    	   List ls0=hibernatedao.findBySqlCriteria(sql0);
    	 
    	   List ls=hibernatedao.findBySqlCriteria(sql); 
    	   Integer mandalcode=0;
    	   for (int k = 0; k <ls.size(); k++) 
    	   {
    		   
    		   Integer nooftonsperacre=(Integer)((Map<Object,Object>)ls0.get(0)).get("nooftonsperacre");
    		   obj.setNooftonsperacre(nooftonsperacre);
    		   mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
    		   Double extentsize=(Double)((Map<Object,Object>)ls.get(k)).get("extentsize");
    		   obj.setExtentsize(extentsize);
    		   obj.setVillagecode(villagecode);
    		   obj.setMandalcode(mandalcode);
    		   String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
    		   obj.setVillage(village);
    		   String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal");
    		   obj.setMandal(mandal);
    		   session.save(obj);
    	   } 
           }
       }
       //	ls.clear(); ls0.clear();
       session.flush();
       session.close();
       String Query1;
       String reportFileName;
       if(mandalCode1[1].equals("ALL"))
       {
    	   reportFileName = "GrowerListByVillageMandalSummaryRpt";
    	   Query1="distinct plant,ratoon,village,villagecode,mandal,mandalcode,nooftonsperacre,extentsize FROM Temp_villagemandalsummary order by village asc";
       }
       else  
       {
    	   reportFileName = "GrowerListByVillageMandalSummaryRpt1";   
    	   Query1="distinct plant,ratoon,mandal,mandalcode,village,villagecode,nooftonsperacre,extentsize FROM Temp_villagemandalsummary where mandal='"+mandalCode1[1]+"' order by village asc";
       }
       //Parameters as Map to be passed to Jasper
       HashMap<String,Object> hmParams=new HashMap<String,Object>();
       hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
       hmParams.put("qrs", Query1);
       generateReport(request,response,hmParams,reportFileName,rptFormat);
      }  		
      catch(Exception e)
      {
    	  System.out.println("SQLExp::CLOSING::" + e.toString());
      }
	}
	*/
	@RequestMapping(value = "/generateGrowerListByVillageMandalSummary", method = RequestMethod.POST)
	public void generateGrowerListByVillageMandalSummary(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		//this  for picking numbers from combined string with number
		String mandalCode=request.getParameter("mandal");	
		String mandalCode1[]=mandalCode.split(":");
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_villagemandalsummary";
		try{
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Session session=hibernatedao.getSessionFactory().openSession();
		Temp_villagemandalsummary obj=null;
		String Query1;
		String reportFileName;
			
		 if(mandalCode1[1].equals("ALL"))
		 {
		 String sql="select distinct a.slno,a.agreedqty,isnull(a.extentsize,0) as plant,a.plantorratoon,a.villagecode,a.mandalcode,v.village,m.mandal from agreementdetails a,village v,mandal m where seasonyear='"+sName[1]+"'and a.villagecode=v.villagecode and a.mandalcode=m.mandalcode";
		 List ls=hibernatedao.findBySqlCriteria(sql); 
  		for (int k = 0; k <ls.size(); k++) 
		{
			obj=new Temp_villagemandalsummary();
			String villagecode=null;
	  		Integer mandalcode=null;
	       //	String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       //	obj.setRyotcode(ryotcode);
	       	villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
	       	obj.setVillagecode(villagecode);
	       	List vclist=hibernatedao.findByNativeSql("select * from Temp_villagemandalsummary where villagecode = '"+villagecode+"'",Temp_villagemandalsummary.class);;
	       	
	       	if(vclist.size()>0)
	       	{
	       		Session session1=hibernatedao.getSessionFactory().openSession();
	       		Temp_villagemandalsummary vData=(Temp_villagemandalsummary)vclist.get(0);

	       		Double agqty=(Double)vData.getAgreedqty();
	       		Double plants=(Double)vData.getPlant();
	       		Double ratoons=(Double)vData.getRatoon();
	       		String plantorRatoons=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon"); 
	       		Double extSize=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
	       		Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
	       	
	       		if(Integer.parseInt(plantorRatoons)==1)
	       		{
	       			plants=plants+extSize;
	       		}
	       		if(Integer.parseInt(plantorRatoons)>1)
	       		{
	       			ratoons=ratoons+extSize;
	       		}
	       		agqty=agqty+agreedqty;
	       		vData.setPlant(plants);
	       		vData.setRatoon(ratoons);
	       		vData.setAgreedqty(agqty);
	       		vData.setId(vData.getId());
	       		session1.saveOrUpdate(vData);
	       		session1.flush();
	       		session1.close();
	       	}
	       	else
	       	{
	       	String plantorratoon=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon"); 
	  		Double plant1=0.0;
	  		Double ratoon=0.0;
	  		if(Integer.parseInt(plantorratoon)==1)
	  		{	
	  			plant1=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
	  		}
	  		else
	  		{
	  			plant1=0.0;
	  		}
	  		obj.setPlant(plant1);
	  		if(Integer.parseInt(plantorratoon)>1)
	  		{
	  			ratoon=(Double)((Map<Object,Object>)ls.get(k)).get("plant");
		  	}
	  		else
	  		{
	  			ratoon=0.0;
	  		}
	  		obj.setRatoon(ratoon);
	  		mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
	  		Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
	  		obj.setAgreedqty(agreedqty);
	  		obj.setMandalcode(mandalcode);
	  		String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
	  		obj.setVillage(village);
	  		String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal");
	  		obj.setMandal(mandal);
	  		session.save(obj);
		}
		} 
  		//ls.clear(); 
  		session.flush();
  		session.close();
  		reportFileName = "GrowerListByVillageMandalSummaryRpt";
  		Query1="distinct agreedqty,plant,ratoon,village,villagecode,mandal,mandalcode FROM Temp_villagemandalsummary order by mandalcode asc";
		}
		else  
		{
			String sql="select distinct a.slno,a.agreedqty,isnull(a.extentsize,0) as plant,a.plantorratoon,a.villagecode,a.mandalcode,v.village,m.mandal from agreementdetails a,village v,mandal m where a.seasonyear='"+sName[1]+"'and a.villagecode=v.villagecode and a.mandalcode="+mandalCode1[1]+" and a.mandalcode=m.mandalcode";
			List ls=hibernatedao.findBySqlCriteria(sql); 
	  		for (int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_villagemandalsummary();
				
				String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
				
		       	obj.setVillagecode(villagecode);
		       	List vclist=hibernatedao.findByNativeSql("select * from Temp_villagemandalsummary where villagecode = '"+villagecode+"'",Temp_villagemandalsummary.class);;
		       	
		       	if(vclist.size()>0)
		       	{
		       		Session session1=hibernatedao.getSessionFactory().openSession();
		       		Temp_villagemandalsummary vData=(Temp_villagemandalsummary)vclist.get(0);

		       		Double agqty=(Double)vData.getAgreedqty();
		       		Double plants=(Double)vData.getPlant();
		       		Double ratoons=(Double)vData.getRatoon();
		       		String plantorRatoons=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon"); 
		       		Double extSize=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
		       		Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
		       	
		       		if(Integer.parseInt(plantorRatoons)==1)
		       		{
		       			plants=plants+extSize;
		       		}
		       		if(Integer.parseInt(plantorRatoons)>1)
		       		{
		       			ratoons=ratoons+extSize;
		       		}
		       		agqty=agqty+agreedqty;
		       		vData.setPlant(plants);
		       		vData.setRatoon(ratoons);
		       		vData.setAgreedqty(agqty);
		       		vData.setId(vData.getId());
		       		session1.saveOrUpdate(vData);
		       		session1.flush();
		       		session1.close();
		       	}
		       	else
		       	{
		       	String plantorratoon=(String)((Map<Object,Object>)ls.get(k)).get("plantorratoon"); 
		  		Double plant1=0.0;
		  		Double ratoon=0.0;
		  		if(Integer.parseInt(plantorratoon)==1)
		  		{	
		  			plant1=(Double)((Map<Object,Object>)ls.get(k)).get("plant"); 
		  		}
		  		else
		  		{
		  			plant1=0.0;
		  		}
		  		obj.setPlant(plant1);
		  		if(Integer.parseInt(plantorratoon)>1)
		  		{
		  			ratoon=(Double)((Map<Object,Object>)ls.get(k)).get("plant");
			  	}
		  		else
		  		{
		  			ratoon=0.0;
		  		}
		  		obj.setRatoon(ratoon);
		  		Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode");
		  		Double agreedqty=(Double)((Map<Object,Object>)ls.get(k)).get("agreedqty");
		  		obj.setAgreedqty(agreedqty);
		  		obj.setMandalcode(mandalcode);
		  		String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
		  		obj.setVillage(village);
		  		String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal");
		  		obj.setMandal(mandal);
		  		session.save(obj);
			}
			} 
	  		session.flush();
	  		session.close();
			reportFileName = "GrowerListByVillageMandalSummaryRpt1";   
			Query1="distinct agreedqty,plant,ratoon,mandal,mandalcode,village,villagecode FROM Temp_villagemandalsummary order by villagecode asc";
		}
		
		 //Parameters as Map to be passed to Jasper
  		
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	

	@RequestMapping(value = "/generateLoanMasterListReport", method = RequestMethod.POST)
	public String generateLoanMasterListReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		
		String rptFormat = request.getParameter("status");
	    String reportFileName = "LoanMasterListReport";
	    String season=request.getParameter("season");
		String season1[]=season.split(":");
		
		
	try{
		String sqq="truncate table temploanmasterreport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		String sql="insert into temploanmasterreport(relativename,ryotname,ryotcode,loannumber,disbursedamount,disburseddate,interestamount,totalamount,referencenumber,branchcode,village,villagecode,mandal,mandalcode) select distinct a.relativename,a.ryotname,l.ryotcode,l.loannumber,isnull(l.disbursedamount,0),l.disburseddate,isnull(l.interestamount,0),(isnull(l.disbursedamount,0)+isnull(l.interestamount,0)),l.referencenumber,l.branchcode,v.village,v.villagecode,m.mandal,m.mandalcode from branch as b inner join loandetails as l on b.branchcode=l.branchcode inner join agreementdetails as a on l.ryotcode=a.ryotcode inner join village as v on a.villagecode=v.villagecode inner join mandal as m on v.mandalcode=m.mandalcode where a.seasonyear='"+season1[1]+"'";
		SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
		query1.executeUpdate();
	    
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	    String Query1="relativename,ryotname,ryotcode,loannumber,disbursedamount,disburseddate,interestamount,totalamount,referencenumber,branchcode,village,villagecode,mandal,mandalcode from temploanmasterreport order by villagecode";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		return null;
		
	}

/*	@RequestMapping(value = "/generateAgricultureLoanAppReport", method = RequestMethod.POST)
	public void generateAgricultureLoanAppReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchnm=request.getParameter("branchName");
		String rptFormat = request.getParameter("status");
	    String season1[]=season.split(":");
	    String branch1[]=branchnm.split(":");
	    Temp_AgricultureLoanApp obj=new Temp_AgricultureLoanApp();
	    String sqq="truncate table Temp_AgricultureLoanApp";
	    Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		if(branch1[1].equalsIgnoreCase("All"))
		{
		 String sql="select ryotcode from AgreementSummary where seasonyear='"+season1[1]+"'";
		 List ls=hibernatedao.findBySqlCriteria(sql); 
		 for (int k = 0; k <ls.size(); k++) 
		 {
			 String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
			 //Map<Object,Object> map = (Map) ls.get(k);
		       
			 String sql1="select loannumber,principle from loandetails where ryotcode='"+ryotcode+"' and disbursedamount=0 or disbursedamount is null;" ;
			 List ls1=hibernatedao.findBySqlCriteria(sql1);
			 if(ls1.size()>0)
			 {
				 Integer loannumber=(Integer)((Map<Object,Object>)ls1.get(0)).get("loannumber");
				 Double principle=(Double)((Map<Object,Object>)ls1.get(0)).get("principle");
				 obj.setRyotcode(ryotcode);
				 obj.setLoannumber(loannumber);
				 obj.setPrinciple(principle);
				 String sql2="select isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='0'";
				 List ls2=hibernatedao.findBySqlCriteria(sql2);
				 Double plant=null;
				 if(ls2.size()>0)
				 {	
					 plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
				 }
				 else
				 {
					 plant=0.0;
				 }	
				 obj.setPlant(plant);
				 String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where ryotcode='"+ryotcode+"' and plantorratoon in(1,2,3)";
				 List ls3=hibernatedao.findBySqlCriteria(sql3);
				 Double ratoon;
				 if(ls3.size()>0)
				 {
		  			ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
				 }
				 else
				 {
		  			ratoon=0.0;
				 }
				 obj.setRatoon(ratoon);
				 String sql4="select aadhaarNumber from ryot where ryotcode='"+ryotcode+"'";
				 List ls4=hibernatedao.findBySqlCriteria(sql4);
				 if(ls4.size()>0);
				 {
					 String aadhaarNumber=(String)((Map<Object,Object>)ls4.get(0)).get("aadhaarNumber");
					 obj.setAadhaarNumber(aadhaarNumber);
				 }
				 String sql6="select relativename,ryotname,villagecode from agreementdetails where ryotcode='"+ryotcode+"'";
				 List ls6=hibernatedao.findBySqlCriteria(sql6);
				 String villagecode=null;
				 if(ls6.size()>0)
				 {
					 String ryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname");
					 String relativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename");
					 villagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode");
					 obj.setRelativename(relativename);
					 obj.setRyotname(ryotname);
		  		}		
		  		String sql8="select village from village where villagecode="+villagecode;
		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
		  		if(ls8.size()>0)
		  		{
			  		String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
					obj.setVillage(village);
		  		}
		  		Session session=hibernatedao.getSessionFactory().openSession();
		  		session.save(obj);
		  		session.flush();
		  		session.close();
		  		ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls6.clear();ls8.clear();
			 }
			 ls1.clear();
		 }
		 ls.clear();
		}
		else
		{
			String sql9="select branchcode from branch where branchname='"+branch1[1]+"'";
			List ls9=hibernatedao.findBySqlCriteria(sql9);
			Integer branchcode=null;
			if(ls9.size()>0)
			{	
				branchcode=(Integer)((Map<Object,Object>)ls9.get(0)).get("branchcode"); 
			}
			String sql="select ryotcode from AgreementSummary where seasonyear='"+season1[1]+"' and branchcode="+branchcode;
			List ls=hibernatedao.findBySqlCriteria(sql); 
			for (int k = 0; k <ls.size(); k++) 
			{
				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
				//Map<Object,Object> map = (Map) ls.get(k);
		  		String sql1="select loannumber,principle from loandetails where ryotcode='"+ryotcode+"'and season='"+season1[1]+"' and disbursedamount=0 or disbursedamount is null;" ;
		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
		  		if(ls1.size()>0)
		  		{
		  			Integer loannumber=(Integer)((Map<Object,Object>)ls1.get(0)).get("loannumber");
		  			Double principle=(Double)((Map<Object,Object>)ls1.get(0)).get("principle");
		  			obj.setRyotcode(ryotcode);
		  			obj.setLoannumber(loannumber);
		  			obj.setPrinciple(principle);
				  		
		  			String sql2="select isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1'";
		  			List ls2=hibernatedao.findBySqlCriteria(sql2);
		  			Double plant=null;
		  			if(ls2.size()>0)
		  			{	
		  				plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
		  			}
		  			else
		  			{
		  				plant=0.0;
		  			}	
		  			obj.setPlant(plant);
		  			String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where ryotcode='"+ryotcode+"' and plantorratoon in(2,3,4)";
		  			List ls3=hibernatedao.findBySqlCriteria(sql3);
		  			Double ratoon;
		  			if(ls3.size()>0)
		  			{
		  				ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
		  			}
		  			else
		  			{
		  				ratoon=0.0;
		  			}
		  			obj.setRatoon(ratoon);
		  			String sql4="select aadhaarNumber from ryot where ryotcode='"+ryotcode+"'";
		  			List ls4=hibernatedao.findBySqlCriteria(sql4);
		  			if(ls4.size()>0);
		  			{
		  				String aadhaarNumber=(String)((Map<Object,Object>)ls4.get(0)).get("aadhaarNumber");
		  			
		  			obj.setAadhaarNumber(aadhaarNumber);
		  			}
		  			String sql6="select relativename,ryotname,villagecode from agreementdetails where ryotcode='"+ryotcode+"'";
		  			List ls6=hibernatedao.findBySqlCriteria(sql6);
		  			String villagecode=null;
		  			if(ls6.size()>0)
		  			{
		  				String ryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname");
		  				String relativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename");
		  				villagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode");
		  				obj.setRelativename(relativename);
		  				obj.setRyotname(ryotname);
		  			}		
		  			String sql8="select village from village where villagecode="+villagecode;
		  			List ls8=hibernatedao.findBySqlCriteria(sql8);
		  			if(ls8.size()>0)
		  			{
		  				String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
		  				obj.setVillage(village);
		  			}
		  			Session session=hibernatedao.getSessionFactory().openSession();
		  			session.save(obj);
		  			session.flush();
		  			session.close();
		  			ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls6.clear();ls8.clear();
		  		}
		  		ls1.clear();
			}
			ls.clear();
			ls9.clear();
		}
		String reportFileName = "AgricultureLoanAppReport";
        String Query1="distinct ryotcode,loannumber,village,ryotname,relativename,plant,ratoon,isnull(principle,0) as principle,aadhaarNumber from Temp_AgricultureLoanApp order by ryotcode";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	} */
	
	/*@RequestMapping(value = "/generateAgricultureLoanAppReport", method = RequestMethod.POST)
	public void generateAgricultureLoanAppReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchnm=request.getParameter("branchName");
		String rptFormat = request.getParameter("status");
	    String season1[]=season.split(":");
	    String branch1[]=branchnm.split(":");
	    Temp_AgricultureLoanApp obj=null;
	    Session session=hibernatedao.getSessionFactory().openSession();
	    String sqq="truncate table Temp_AgricultureLoanApp";
	    Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		if(branch1[1].equalsIgnoreCase("All"))
		{
		 String sql="select ryotcode from AgreementSummary where seasonyear='"+season1[1]+"'";
		 List ls=hibernatedao.findBySqlCriteria(sql); 
		 for (int k = 0; k <ls.size(); k++) 
		 {
			 
			 obj=new Temp_AgricultureLoanApp();
			 String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
			 obj.setRyotcode(ryotcode);
			 String sql1="select l.loannumber,l.principle,a.slno,a.plantorratoon,isnull(a.extentsize,0) as plant,a.relativename,a.ryotname,a.villagecode,v.village,r.aadhaarNumber from loandetails l,AgreementDetails a,village v,ryot r where l.ryotcode='"+ryotcode+"' and a.ryotcode='"+ryotcode+"' and a.villagecode=v.villagecode and r.ryotcode='"+ryotcode+"'";
			 List ls1=hibernatedao.findBySqlCriteria(sql1);
			 for (int i = 0; i <ls1.size(); i++) 
			 {
			 if(ls1.size()>0)
			 {
				 Integer loannumber=(Integer)((Map<Object,Object>)ls1.get(i)).get("loannumber");
				 obj.setLoannumber(loannumber);
				 List llist=hibernatedao.findByNativeSql("select * from Temp_AgricultureLoanApp where loannumber = "+loannumber+"",Temp_AgricultureLoanApp.class);;
				 if(llist.size()>0)
				 {
					 Session session1=hibernatedao.getSessionFactory().openSession();
					 Temp_AgricultureLoanApp ll=(Temp_AgricultureLoanApp)llist.get(0);
					 Double plants=(Double)ll.getPlant();
					 Double ratoons=(Double)ll.getRatoon();
					 String plantorRatoons=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon"); 
					 Double extSize=(Double)((Map<Object,Object>)ls1.get(i)).get("plant"); 
					 //logger.info("Ryot Code================"+ryotcode+"======extSize==="+extSize);
					 if(Integer.parseInt(plantorRatoons)==1)
					 {
						 plants=plants+extSize;
					 }
					 if(Integer.parseInt(plantorRatoons)>1)
					 {
						 ratoons=ratoons+extSize;
					 }
					 //logger.info("plants================"+plants+"======ratoons==="+ratoons);
					 ll.setPlant(plants);
					 ll.setRatoon(ratoons);
					 ll.setSlno(ll.getSlno());
					 session1.saveOrUpdate(ll);
					 session1.flush();
					 session1.close();
				 }
				 else
				 {
					
					 Double principle=(Double)((Map<Object,Object>)ls1.get(i)).get("principle");
					 obj.setRyotcode(ryotcode);
					
					 obj.setPrinciple(principle);
					 Double plant=0.0;
					 Double ratoon=0.0;
					 String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon");
					 if(Integer.parseInt(plantorratoon)==1)
					 {	
						 plant=(Double)((Map<Object,Object>)ls1.get(i)).get("plant"); 
					 }
					 else
					 {
						 plant=0.0;
					 }	
					 obj.setPlant(plant);
					 if(Integer.parseInt(plantorratoon)>1)
					 {	 
						 ratoon=(Double)((Map<Object,Object>)ls1.get(i)).get("plant");
					 }
					 else
					 {
						 ratoon=0.0;
					 }
					 obj.setRatoon(ratoon);
					 String aadhaarNumber=(String)((Map<Object,Object>)ls1.get(i)).get("aadhaarNumber");
					 obj.setAadhaarNumber(aadhaarNumber);
					 String villagecode=null;
					 String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
					 String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename");
					 villagecode=(String)((Map<Object,Object>)ls1.get(i)).get("villagecode");
					 obj.setRelativename(relativename);
					 obj.setRyotname(ryotname);
					 String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
					 obj.setVillage(village);
					 session.save(obj);
				 	}
			 }
			 }
			 ls1.clear();
		 }
		 ls.clear();
		 session.flush();
		 session.close();
		}
		else
		{
			String sql9="select branchcode from branch where branchcode='"+branch1[1]+"'";
			List ls9=hibernatedao.findBySqlCriteria(sql9);
			Integer branchcode=null;
			if(ls9.size()>0)
			{	
				branchcode=(Integer)((Map<Object,Object>)ls9.get(0)).get("branchcode"); 
			}
			String sql="select ryotcode from AgreementSummary where seasonyear='"+season1[1]+"' and branchcode="+branchcode;
			List ls=hibernatedao.findBySqlCriteria(sql); 
			for (int k = 0; k <ls.size(); k++) 
			{
				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
				//Map<Object,Object> map = (Map) ls.get(k);
		  		String sql1="select l.loannumber,l.principle,a.relativename,a.ryotname,a.villagecode,a.plantorratoon,isnull(a.extentsize,0) as plant,r.aadhaarNumber,v.village from loandetails l,agreementdetails a,ryot r,village v where l.ryotcode='"+ryotcode+"'and l.season='"+season1[1]+"' and a.ryotcode='"+ryotcode+"' and r.ryotcode='"+ryotcode+"' and a.villagecode=v.villagecode";
		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
		  		for (int i = 0; i <ls.size(); i++) 
				{
		  			if(ls1.size()>0)
		  			{
		  				Integer loannumber=(Integer)((Map<Object,Object>)ls1.get(i)).get("loannumber");
		  				Double principle=(Double)((Map<Object,Object>)ls1.get(i)).get("principle");
		  				obj.setRyotcode(ryotcode);
		  				obj.setLoannumber(loannumber);
		  				obj.setPrinciple(principle);
		  				Double plant=0.0;
		  				Double ratoon=0.0;
		  				String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon");
		  				if(Integer.parseInt(plantorratoon)==1)
		  				{	
		  					plant=(Double)((Map<Object,Object>)ls1.get(i)).get("plant"); 
		  				}
		  				else
		  				{
		  					plant=0.0;
		  				}	
		  				obj.setPlant(plant);
		  				if(Integer.parseInt(plantorratoon)>1)
		  				{
		  					ratoon=(Double)((Map<Object,Object>)ls1.get(i)).get("plant");
		  				}
		  				else
		  				{
		  					ratoon=0.0;
		  				}
		  				obj.setRatoon(ratoon);
		  				String aadhaarNumber=(String)((Map<Object,Object>)ls1.get(i)).get("aadhaarNumber");
		  				obj.setAadhaarNumber(aadhaarNumber);
		  				String villagecode=null;
		  				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
		  				String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename");
		  				villagecode=(String)((Map<Object,Object>)ls1.get(i)).get("villagecode");
		  				obj.setRelativename(relativename);
		  				obj.setRyotname(ryotname);
		  				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
		  				obj.setVillage(village);
		  				session.save(obj);
		  			}
				}
		  		ls1.clear();
				}
			ls.clear();
			ls9.clear();
			session.flush();
			session.close();
		}
		String reportFileName = "AgricultureLoanAppReport";
        String Query1="distinct ryotcode,loannumber,village,ryotname,relativename,plant,ratoon,isnull(principle,0) as principle,aadhaarNumber from Temp_AgricultureLoanApp order by loannumber ";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	} 
	
	*/
	
	/*@RequestMapping(value = "/generateSeedSeedlingLoansReport", method = RequestMethod.POST)
	public void generateSeedSeedlingLoansReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{
		String season=request.getParameter("season");
		String season1[]=season.split(":");
		String rptFormat = request.getParameter("status");
	try
	{
		Temp_SeedSeedlingLoanReport obj=new Temp_SeedSeedlingLoanReport();
		String sqq="truncate table Temp_SeedSeedlingLoanReport";
		Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		String sql="select distinct seedsuppliercode from SeedSuppliersSummary where season='"+season1[1]+"'";
		List ls=hibernatedao.findBySqlCriteria(sql); 
		for (int k = 0; k <ls.size(); k++) 
		{
			String seedsuppliercode=(String)((Map<Object,Object>)ls.get(k)).get("seedsuppliercode"); 
			String sql2="select isnull(extentsize,0) as plant,ryotcode,isnull(totalamount,0) as totalamount,agreements from AdvanceDetails where seedsuppliercode='"+seedsuppliercode+"'";
			List ls2=hibernatedao.findBySqlCriteria(sql2);
			Double plant=null;
			Double totalamount=null;
			String ryotcode=null;
			String agreementnumber=null;
			for (int n = 0; n <ls2.size(); n++) 
			{
				plant=(Double)((Map<Object,Object>)ls2.get(n)).get("plant"); 
				totalamount=(Double)((Map<Object,Object>)ls2.get(n)).get("totalamount"); 
				ryotcode=(String)((Map<Object,Object>)ls2.get(n)).get("ryotcode");  
				agreementnumber=(String)((Map<Object,Object>)ls2.get(n)).get("agreements"); 
				obj.setPlant(plant);
				obj.setLoanamount(totalamount);
				obj.setAgreementnumber(agreementnumber);
				String sql3="select  ryotname,relativename,villagecode from Ryot where ryotcode='"+ryotcode+"'";
				List ls3=hibernatedao.findBySqlCriteria(sql3);
				if(ls3.size()>0)
				{
					String ryotname=(String)((Map<Object,Object>)ls3.get(0)).get("ryotname"); 
					String relativename=(String)((Map<Object,Object>)ls3.get(0)).get("relativename"); 
					String villagecode=(String)((Map<Object,Object>)ls3.get(0)).get("villagecode"); 
					
					String sql4="select village from village where villagecode="+villagecode;
					List ls4=hibernatedao.findBySqlCriteria(sql4);
					String village=(String)((Map<Object,Object>)ls4.get(0)).get("village");
					
					String sql5="select accountnumber from RyotBankDetails where ryotcode='"+ryotcode+"' and season='"+season1[1]+"'";
					List ls5=hibernatedao.findBySqlCriteria(sql5);
					String accountnumber=(String)((Map<Object,Object>)ls5.get(0)).get("accountnumber");
					obj.setRyotcode(ryotcode);
					obj.setRyotname(ryotname);
					obj.setRelativename(relativename);
					obj.setVillage(village);
					obj.setAccountnumber(accountnumber);
					ls4.clear();
					ls5.clear();
					ls3.clear();
				}
				String sql6="select ryotname,relativename,villagecode from Ryot where ryotcode='"+seedsuppliercode+"'";
				List ls6=hibernatedao.findBySqlCriteria(sql6);
				if(ls6.size()>0)
				{
					String sryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname"); 
					String srelativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename"); 
					String svillagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode"); 
					String sql7="select village from village where villagecode="+svillagecode;
					List ls7=hibernatedao.findBySqlCriteria(sql6);
					String svillage=(String)((Map<Object,Object>)ls7.get(0)).get("village");
					obj.setSeedsellercode(seedsuppliercode);
					obj.setSeedryotname(sryotname);
					obj.setSeedfathername(srelativename);
					obj.setSeedvillage(svillage);
					// For save	
					Session session=hibernatedao.getSessionFactory().openSession();
					session.save(obj);
					session.flush();
					session.close();
					ls6.clear();
					ls7.clear();
				}	
			}
			ls2.clear();
		}	
		ls.clear();
  		String reportFileName = "SeedSeedlingLoansReport";
        String Query1="distinct ryotcode,ryotname,seedsellercode,seedryotname,seedvillage,seedfathername,accountnumber,relativename,agreementnumber,village,plant,loanamount from Temp_SeedSeedlingLoanReport order by ryotcode";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);	  		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/
	@RequestMapping(value = "/generateSeedSeedlingLoansReport", method = RequestMethod.POST)
	public void generateSeedSeedlingLoansReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{
		String season=request.getParameter("season");
		String season1[]=season.split(":");
		String rptFormat = request.getParameter("status");
	try
	{
		Temp_SeedSeedlingLoanReport obj=new Temp_SeedSeedlingLoanReport();
		  String sqq="truncate table Temp_SeedSeedlingLoanReport";
		  SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
			query.executeUpdate();
			 String sql="select distinct seedsuppliercode from SeedSuppliersSummary where season='"+season1[1]+"'";
		  		List ls=hibernatedao.findBySqlCriteria(sql); 
		Session session=hibernatedao.getSessionFactory().openSession();
		  		for (int k = 0; k <ls.size(); k++) 
				{
			      	String seedsuppliercode=(String)((Map<Object,Object>)ls.get(k)).get("seedsuppliercode"); 
		//	String sql2="select isnull(extentsize,0) as plant,ryotcode,isnull(totalamount,0) as totalamount,agreements from AdvanceDetails where seedsuppliercode='"+seedsuppliercode+"'";
			String sql2="select isnull(a.extentsize,0) as plant,isnull(a.totalamount,0) as totalamount,a.ryotcode,a.agreements,r.ryotname,r.relativename,r.villagecode,v.village,rb.accountnumber from AdvanceDetails a,ryot r,village v,RyotBankDetails rb where a.seedsuppliercode='"+seedsuppliercode+"' and a.ryotcode=r.ryotcode and r.villagecode=v.villagecode and a.ryotcode=rb.ryotcode and season='"+season1[1]+"'";
			  		List ls2=hibernatedao.findBySqlCriteria(sql2);
			Double plant=0.0;
			Double totalamount=0.0;
			String ryotcode=null;
			String agreementnumber=null;
			for (int n = 0; n <ls2.size(); n++) 
			{
				plant=(Double)((Map<Object,Object>)ls2.get(n)).get("plant"); 
				totalamount=(Double)((Map<Object,Object>)ls2.get(n)).get("totalamount"); 
				ryotcode=(String)((Map<Object,Object>)ls2.get(n)).get("ryotcode");  
				agreementnumber=(String)((Map<Object,Object>)ls2.get(n)).get("agreements"); 
				String ryotname=(String)((Map<Object,Object>)ls2.get(n)).get("ryotname"); 
				String relativename=(String)((Map<Object,Object>)ls2.get(n)).get("relativename"); 
				String villagecode=(String)((Map<Object,Object>)ls2.get(n)).get("villagecode"); 
				String village=(String)((Map<Object,Object>)ls2.get(n)).get("village");
				String accountnumber=(String)((Map<Object,Object>)ls2.get(n)).get("accountnumber");
				obj.setPlant(plant);
				obj.setLoanamount(totalamount);
				obj.setAgreementnumber(agreementnumber);
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setRelativename(relativename);
				obj.setVillage(village);
				obj.setAccountnumber(accountnumber);
				
				String sql6="select ryotname,relativename,villagecode from Ryot where ryotcode='"+seedsuppliercode+"'";
				List ls6=hibernatedao.findBySqlCriteria(sql6);
				if(ls6.size()>0)
				{
					String sryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname"); 
					String srelativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename"); 
					String svillagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode"); 
					String sql7="select village from village where villagecode="+svillagecode;
					List ls7=hibernatedao.findBySqlCriteria(sql6);
					String svillage=(String)((Map<Object,Object>)ls7.get(0)).get("village");
					obj.setSeedsellercode(seedsuppliercode);
					obj.setSeedryotname(sryotname);
					obj.setSeedfathername(srelativename);
					obj.setSeedvillage(svillage);
					session.save(obj);
				}	
			}
			ls2.clear();
		}	
		ls.clear();
		session.flush();
		session.close();
  		String reportFileName = "SeedSeedlingLoansReport";
        String Query1="distinct ryotcode,ryotname,seedsellercode,seedryotname,seedvillage,seedfathername,accountnumber,relativename,agreementnumber,village,plant,loanamount from Temp_SeedSeedlingLoanReport order by ryotcode";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);	  		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/generateBankOrder", method = RequestMethod.POST)
	public void generateBankOrder(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String branchName=request.getParameter("branchName");	
    	String bName[]=branchName.split(":");
		// String branchName1 = branchName.replaceAll("\\D", "");
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		
		 String pattern = "MM/dd/yyyy";
	     SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_BankOrderReport";
		
		try{
			SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		
		String sql="insert into Temp_BankOrderReport(ryotcode,ryotname,progressiveqty,village,loannumber,pendingamount,referencenumber,branchcode,branchname) select a.ryotcode,a.ryotname,a.progressiveqty,v.village,l.loannumber,isnull(l.pendingamount,0),l.referencenumber,b.branchcode,b.branchname FROM village AS v INNER JOIN  agreementdetails a ON v.villagecode=a.villagecode INNER JOIN loandetails l ON a.ryotcode=l.ryotcode INNER JOIN branch as b ON l.branchcode=b.branchcode where b.branchname='"+bName[1]+"' and a.seasonyear='"+sName[1]+"'";  
		SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
		query1.executeUpdate();
		//String sql="create table temp_bank select a.ryotcode,a.ryotname,a.progressiveqty,v.village,l.loannumber,l.pendingamount,l.referencenumber,b.branchcode,b.branchname FROM village AS v INNER JOIN  agreementdetails a ON v.villagecode=a.villagecode INNER JOIN loandetails l ON a.ryotcode=l.ryotcode INNER JOIN branch as b ON l.branchcode=b.branchcode where 1=2";
			
        String Query1;
		String reportFileName;
	
		 reportFileName = "BankOrderRpt";
		 Query1="distinct ryotcode,ryotname,progressiveqty,village,loannumber,pendingamount,referencenumber,branchname,branchcode FROM Temp_BankOrderReport";
		
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        hmParams.put("cdate",format.format(new Date())); //date
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateRyotLoanForwardingReport", method = RequestMethod.POST)
	public void generateRyotLoanForwardingReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchCode=request.getParameter("branchCode");
		String loannumber=request.getParameter("loanRyotcode");
		String rptFormat = request.getParameter("status");
	    String reportFileName = "RyotLoanForwardingReport";
	    
	    String season1[]=season.split(":");
	    String branchCode1[]=branchCode.split(":");
	    String loannumber1[]=loannumber.split(":");
	    String loancombined[]=loannumber1[1].split("-");
	    String ryotCode = loancombined[1];
	    String pattern = "dd/MM/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    com.finsol.model.Temp_ryotLoanForward obj=null;
	    Session session=hibernatedao.getSessionFactory().openSession();
	    try{
	    String sqq="truncate table Temp_ryotLoanForward";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		 String sql="select ryotcode from loandetails where season='"+season1[1]+"' and branchcode='"+branchCode1[1]+"'";
			
	  		List ls=hibernatedao.findBySqlCriteria(sql); 
	  		//for (int k = 0; k <ls.size(); k++) 
			//{
	  			obj=new com.finsol.model.Temp_ryotLoanForward();
		  		//Map<Object,Object> map = (Map) ls.get(k);
		         	          
		       	String ryotcode=ryotCode;//(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
		       	obj.setRyotcode(ryotcode);	
		       		
		        String sql2="select isnull(extentsize,0) as plant from agreementdetails where seasonyear='"+season1[1]+"' and ryotcode="+ryotcode+" and plantorratoon='1'";
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		Double plant=null;
		  		if(ls2.size()>0)
		  		{	
		  			plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
					
	    	}
	    	else
	    	{
	    		plant=0.0;
	    	}	
	    	obj.setPlant(plant);
	    	String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where seasonyear='"+season1[1]+"' and  ryotcode='"+ryotcode+"' and plantorratoon in(2,3,4,5)";
	    	List ls3=hibernatedao.findBySqlCriteria(sql3);
	    	Double ratoon;
	    	if(ls3.size()>0)
	    	{
	    		ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
	    	}
	    	else
	    	{
	    		ratoon=0.0;
	    	}
	    	obj.setRatoon(ratoon);
	    	//ryotcode,ryotname,relativename,extentsize,surveynumber,plantorratoon,village,mandal,nooftonsperacre,villagecode,mandalcode
	    	String sql6="select relativename,villagecode,ryotname from agreementdetails where seasonyear='"+season1[1]+"' and  ryotcode='"+ryotcode+"'";
	    	List ls6=hibernatedao.findBySqlCriteria(sql6);
	    	String villagecode=null;
	    	if(ls6.size()>0)
	    	{
	    		String relativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename");
	    		villagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode");
	    		String ryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname");
	    		obj.setRelativename(relativename);
	    		obj.setRyotname(ryotname);
	    	}		
	    	String loannum = loancombined[0];
	    	int nloannum = Integer.parseInt(loannum);
	    	String sql7="select disbursedamount,principle,recommendeddate,branchcode from loandetails where season='"+season1[1]+"' and  loannumber="+nloannum+" and principle >0 and branchcode="+branchCode1[1] ;
	    	List ls7=hibernatedao.findBySqlCriteria(sql7);
	    	Integer branchcode=null;
	    	if(ls7.size()>0)
	    	{
	    		Date recommended=(Date)((Map<Object,Object>)ls7.get(0)).get("recommendeddate");
	    		String recommendeddate=DateUtils.formatDate(recommended,Constants.GenericDateFormat.DATE_FORMAT);
	    		branchcode=(Integer)((Map<Object,Object>)ls7.get(0)).get("branchcode");
	    		Double disbursedamount=(Double)((Map<Object,Object>)ls7.get(0)).get("disbursedamount");
	    		Double principle=(Double)((Map<Object,Object>)ls7.get(0)).get("principle");
	    		obj.setDisburseddate(recommendeddate);
	    		obj.setDisbursedamount(disbursedamount);
	    		obj.setPrinciple(principle);
	    	}
	    	String sql8="select village from village where villagecode="+villagecode;
	    	List ls8=hibernatedao.findBySqlCriteria(sql8);
	    	if(ls8.size()>0)
	    	{
	    		String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
	    		obj.setVillage(village);
	    	}
	    	String sql9="select branchname from branch where branchcode="+branchcode;
	    	List ls9=hibernatedao.findBySqlCriteria(sql9);
	    	if(ls9.size()>0 && ls9!=null)
	    	{
	    		String branchname=(String)((Map<Object,Object>)ls9.get(0)).get("branchname");
	    		obj.setBranchname(branchname);
	    	}
	    	obj.setLoannumber(Integer.parseInt(loancombined[0]));
	    	//	obj.setRyotname(ryotname);
	    	session.save(obj);
	    	session.flush();
	    	session.close();
	    	//ls2.clear();ls3.clear();ls6.clear();ls7.clear();ls8.clear();ls9.clear();
			//} 
	    	//ls.clear();
	    	String Query1="distinct branchname,loannumber,isnull(disbursedamount,0) as disbursedamount,isnull(principle,0) as principle,disburseddate,ryotcode,plant,ratoon,ryotname,relativename,village from Temp_ryotLoanForward order by ryotcode";
	    	//String Query1="b.branchname as brhname,l.loannumber as loanno,l.disbursedamount as damount,l.principle as pamount,l.disburseddate as loandate,r.ryotcode as rtycode,a.extentsize as extsize,a.plantorratoon as ptorrt,r.ryotname as rtyname,a.relativename as rltname,v.village as vlg from branch as b inner join loandetails as l on b.branchcode=l.branchcode inner join agreementdetails as a on l.ryotcode=a.ryotcode inner join village as v on a.villagecode=v.villagecode inner join ryot as r on v.villagecode=r.villagecode where r.ryotname='"+ryotname1[1]+"' and '"+season1[1]+"' and '"+loannumber1[1]+"'";
	    	HashMap<String,Object> hmParams=new HashMap<String,Object>();
	    	hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	    	hmParams.put("qrs", Query1);
	    	hmParams.put("cdate",format.format(new Date()));
	    	generateReport(request,response,hmParams,reportFileName,rptFormat);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}
	/*
	@RequestMapping(value = "/generateRyotLoanForwardingReport", method = RequestMethod.POST)
	public void generateRyotLoanForwardingReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchCode=request.getParameter("branchCode");
		String loannumber=request.getParameter("loanRyotcode");
		String rptFormat = request.getParameter("status");
	    String reportFileName = "RyotLoanForwardingReport";
	    
	    String season1[]=season.split(":");
	    String branchCode1[]=branchCode.split(":");
	    String loannumber1[]=loannumber.split(":");
	    String loancombined[]=loannumber1[1].split("-");
	    String ryotCode = loancombined[1];
	    String loannum = loancombined[0];
	    int nloannum = Integer.parseInt(loannum);
	    
	    String pattern = "dd/MM/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    com.finsol.model.Temp_ryotLoanForward obj=null;
	    try{
	    String sqq="truncate table Temp_ryotLoanForward";
	    Query query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
	    query.executeUpdate();
	    String sql="select ryotcode,disbursedamount,principle,disburseddate,branchcode from loandetails where season='"+season1[1]+"' and branchcode='"+branchCode1[1]+"' and loannumber="+nloannum+"";
	    List ls=hibernatedao.findBySqlCriteria(sql); 
	    //for (int k = 0; k <ls.size(); k++) 
	    //{
	    	obj=new com.finsol.model.Temp_ryotLoanForward();
	    //Map<Object,Object> map = (Map) ls.get(k);
	    	String ryotcode=ryotCode;//(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	    	obj.setRyotcode(ryotcode);	
	    	String sql2="selecta.slno,a.plantorratoon,a.relativename,a.villagecode,a.ryotname,isnull(a.extentsize,0) as plant,v.village from agreementdetails a,village v where a.ryotcode="+ryotcode+" and a.villagecode=v.villagecode";
	    	List ls2=hibernatedao.findBySqlCriteria(sql2);
	    	Double plant=0.0;
	    	String plantorratoon=(String)((Map<Object,Object>)ls2.get(0)).get("plantorratoon"); 
	    	if(Integer.parseInt(plantorratoon)==1)
	    	{	
	    		plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
	    	}
	    	else
	    	{
	    		plant=0.0;
	    	}	
	    	obj.setPlant(plant);
	    	Double ratoon=0.0;
	    	if(Integer.parseInt(plantorratoon)>1)
	    	{
	    		ratoon=(Double)((Map<Object,Object>)ls2.get(0)).get("plant");
	    	}
	    	else
	    	{
	    		ratoon=0.0;
	    	}
	    	obj.setRatoon(ratoon);
	    	String villagecode=null;
	    	String relativename=(String)((Map<Object,Object>)ls2.get(0)).get("relativename");
	    	villagecode=(String)((Map<Object,Object>)ls2.get(0)).get("villagecode");
	   		String ryotname=(String)((Map<Object,Object>)ls2.get(0)).get("ryotname");
	   		String village=(String)((Map<Object,Object>)ls2.get(0)).get("village");
	    	obj.setVillage(village);
	   		obj.setRelativename(relativename);
	   		obj.setRyotname(ryotname);
	    	Integer branchcode=0;
	    	String disburseddate=(String)((Map<Object,Object>)ls.get(0)).get("disburseddate");
	    	branchcode=(Integer)((Map<Object,Object>)ls.get(0)).get("branchcode");
	    	Double disbursedamount=(Double)((Map<Object,Object>)ls.get(0)).get("disbursedamount");
	    	Double principle=(Double)((Map<Object,Object>)ls.get(0)).get("principle");
	    	obj.setDisburseddate(disburseddate);
	    	obj.setDisbursedamount(disbursedamount);
	    	obj.setPrinciple(principle);
	    	String sql9="select branchname from branch where branchcode="+branchcode;
	    	List ls9=hibernatedao.findBySqlCriteria(sql9);
	    	if(ls9.size()>0 && ls9!=null)
	    	{
	    		String branchname=(String)((Map<Object,Object>)ls9.get(0)).get("branchname");
	    		obj.setBranchname(branchname);
	    	}
	    	obj.setLoannumber(Integer.parseInt(loancombined[0]));
	    	//	obj.setRyotname(ryotname);
	    	Session session=hibernatedao.getSessionFactory().openSession();
	    	session.save(obj);
	    	session.flush();
	    	session.close();
	    	ls2.clear();ls9.clear();
			//} 
	    	ls.clear();
	    	String Query1="distinct branchname,loannumber,isnull(disbursedamount,0) as disbursedamount,isnull(principle,0) as principle,disburseddate,ryotcode,plant,ratoon,ryotname,relativename,village from Temp_ryotLoanForward order by ryotcode";
	    	//String Query1="b.branchname as brhname,l.loannumber as loanno,l.disbursedamount as damount,l.principle as pamount,l.disburseddate as loandate,r.ryotcode as rtycode,a.extentsize as extsize,a.plantorratoon as ptorrt,r.ryotname as rtyname,a.relativename as rltname,v.village as vlg from branch as b inner join loandetails as l on b.branchcode=l.branchcode inner join agreementdetails as a on l.ryotcode=a.ryotcode inner join village as v on a.villagecode=v.villagecode inner join ryot as r on v.villagecode=r.villagecode where r.ryotname='"+ryotname1[1]+"' and '"+season1[1]+"' and '"+loannumber1[1]+"'";
	    	HashMap<String,Object> hmParams=new HashMap<String,Object>();
	    	hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	    	hmParams.put("qrs", Query1);
	    	hmParams.put("cdate",format.format(new Date()));
	    	generateReport(request,response,hmParams,reportFileName,rptFormat);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}*/
	
	@RequestMapping(value = "/generateRyotLoanForwardingReportForReport", method = RequestMethod.GET)
	public void generateRyotLoanForwardingReportForReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchCode=request.getParameter("branchCode");
		String loannumber=request.getParameter("loanRyotcode");
		String rptFormat = request.getParameter("status");
	    String reportFileName = "RyotLoanForwardingReport";
	    
	    String season1[]=season.split(":");
	    String branchCode1[]=branchCode.split(":");
	    String loannumber1[]=loannumber.split(":");
	    String loancombined[]=loannumber1[1].split("-");
	    String ryotCode = loancombined[1];

	    String pattern = "dd/MM/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    
	    
	    com.finsol.model.Temp_ryotLoanForward obj=null;
	    try{
	    String sqq="truncate table Temp_ryotLoanForward";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		 String sql="select ryotcode from loandetails where season='"+season1[1]+"' and branchcode='"+branchCode1[1]+"'";
			
	  		List ls=hibernatedao.findBySqlCriteria(sql); 
	  		//for (int k = 0; k <ls.size(); k++) 
			//{
	  			obj=new com.finsol.model.Temp_ryotLoanForward();
		  		//Map<Object,Object> map = (Map) ls.get(k);
		         	          
		       	String ryotcode=ryotCode;//(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
		       	obj.setRyotcode(ryotcode);	
		       		
		        String sql2="select isnull(extentsize,0) as plant from agreementdetails where ryotcode="+ryotcode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		Double plant=null;
		  		if(ls2.size()>0)
		  		{	
		  			plant=(Double)((Map<Object,Object>)ls2.get(0)).get("plant"); 
					
		  		}
		  		else
		  		{
		  			plant=0.0;
		  		}	
		  		obj.setPlant(plant);
				
		  		
		        String sql3="select isnull(extentsize,0) as ratoon from agreementdetails where ryotcode='"+ryotcode+"' and plantorratoon in(2,3,4,5) and seasonyear='"+season1[1]+"'";
		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  		Double ratoon;
				
		  		if(ls3.size()>0)
		  		{
		  			ratoon=(Double)((Map<Object,Object>)ls3.get(0)).get("ratoon");
					
			  	}
		  		else
		  		{
		  			ratoon=0.0;
		  		}
		  		obj.setRatoon(ratoon);
		  	
	//ryotcode,ryotname,relativename,extentsize,surveynumber,plantorratoon,village,mandal,nooftonsperacre,villagecode,mandalcode

		  		String sql6="select relativename,villagecode,ryotname from agreementdetails where ryotcode='"+ryotcode+"' and seasonyear='"+season1[1]+"'";
		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
		  		String villagecode=null;
		  		if(ls6.size()>0)
		  		{
		  	
		  			String relativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename");
		  			villagecode=(String)((Map<Object,Object>)ls6.get(0)).get("villagecode");
		  			String ryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname");
					
		  			obj.setRelativename(relativename);
		  			obj.setRyotname(ryotname);
					
		  		}		
		  		
		  		
		  		 String sql7="select disbursedamount,principle,recommendeddate,branchcode from loandetails where loannumber="+Integer.parseInt(loancombined[0])+" and branchcode="+branchCode1[1]+" and season='"+season1[1]+"'";
				 List ls7=hibernatedao.findBySqlCriteria(sql7);
				 Integer branchcode=null;
				 if(ls7.size()>0)
				 {
					Date recommended=(Date)((Map<Object,Object>)ls7.get(0)).get("recommendeddate");
			    	String recommendeddate=DateUtils.formatDate(recommended,Constants.GenericDateFormat.DATE_FORMAT);
			  		branchcode=(Integer)((Map<Object,Object>)ls7.get(0)).get("branchcode");
			  		Double disbursedamount=(Double)((Map<Object,Object>)ls7.get(0)).get("disbursedamount");
			  		Double principle=(Double)((Map<Object,Object>)ls7.get(0)).get("principle");
					obj.setDisburseddate(recommendeddate);
					obj.setDisbursedamount(disbursedamount);
					obj.setPrinciple(principle);
				 }
							
			
		  		 String sql8="select village from village where villagecode="+villagecode;
				 List ls8=hibernatedao.findBySqlCriteria(sql8);
				 if(ls8.size()>0)
				 {
			  		String village=(String)((Map<Object,Object>)ls8.get(0)).get("village");
			  		
					obj.setVillage(village);
				 }
			
				 String sql9="select branchname from branch where branchcode="+branchcode;
				 List ls9=hibernatedao.findBySqlCriteria(sql9);
				 if(ls9.size()>0 && ls9!=null)
				 {
			  		String branchname=(String)((Map<Object,Object>)ls9.get(0)).get("branchname");
					obj.setBranchname(branchname);
				 }
				 
				 
					obj.setLoannumber(Integer.parseInt(loancombined[0]));
				//	obj.setRyotname(ryotname);
					Session session=hibernatedao.getSessionFactory().openSession();
					session.save(obj);
					session.flush();
					session.close();
			 
			 
					ls2.clear();ls3.clear();ls6.clear();ls7.clear();ls8.clear();ls9.clear();
				
			//} 
	  		ls.clear();
	  		
	      
	
       String Query1="distinct branchname,loannumber,isnull(disbursedamount,0) as disbursedamount,isnull(principle,0) as principle,disburseddate,ryotcode,plant,ratoon,ryotname,relativename,village from Temp_ryotLoanForward order by ryotcode";
 
	    
	    
	    
	    //String Query1="b.branchname as brhname,l.loannumber as loanno,l.disbursedamount as damount,l.principle as pamount,l.disburseddate as loandate,r.ryotcode as rtycode,a.extentsize as extsize,a.plantorratoon as ptorrt,r.ryotname as rtyname,a.relativename as rltname,v.village as vlg from branch as b inner join loandetails as l on b.branchcode=l.branchcode inner join agreementdetails as a on l.ryotcode=a.ryotcode inner join village as v on a.villagecode=v.villagecode inner join ryot as r on v.villagecode=r.villagecode where r.ryotname='"+ryotname1[1]+"' and '"+season1[1]+"' and '"+loannumber1[1]+"'";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        hmParams.put("cdate",format.format(new Date()));
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    }catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}
	
	@RequestMapping(value = "/generateVarietywiseSummaryReport", method = RequestMethod.POST)
	public void generateVarietywiseSummaryReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String rptFormat = request.getParameter("status");	
		String season=request.getParameter("season");
		String group=request.getParameter("GROWER");
		String group1[]=group.split(":");
		String season1[]=season.split(":");
	    String sqq="truncate table Temp_VarietywiseSummary";
	   
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
	    Temp_VarietywiseSummary obj=new Temp_VarietywiseSummary();

	    String Query1=null;
		String reportFileName=null;
		String sql;
		List ls;
		
		if("Variety".equals(group1[1]))
		{
			sql="select distinct varietycode from agreementdetails where seasonyear='"+season1[1]+"'";
			try
			{
				ls=hibernatedao.findBySqlCriteria(sql); 
		 	    for(int k = 0; k <ls.size(); k++) 
				{
			 		Integer varietycode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode"); 
				    obj.setVarietycode(varietycode);	
				    String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where varietycode="+varietycode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
				  	List ls1=hibernatedao.findBySqlCriteria(sql1);
				  	Double vplant=null;
				  	Double vpestqty=null;
				  	if(ls1.size()>0 && ls1!=null)
				  	{	
				  		vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
				  		vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
				  		if(vplant==null)
				  			vplant=0.0;   
				  		if(vpestqty==null)
				  			vpestqty=0.0;
				  	}
				  	else
				  	{
				  		vplant=0.0;
				  		vpestqty=0.0;
				  	}	
				  	obj.setPlant(vplant);
				  	obj.setPqty(vpestqty);
				  	String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where varietycode="+varietycode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
					List ls2=hibernatedao.findBySqlCriteria(sql2);
					Double vr1=null;
					Double vr1estqty=null;
				  	if(ls2.size()>0)
				  	{
				  		vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
				  		vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
				  		if(vr1==null)
				  			vr1=0.0;   
				  		if(vr1estqty==null)
				  			vr1estqty=0.0;
				  	}
				  	else
				  	{
				  		vr1=0.0;
				  		vr1estqty=0.0;
				  	}
				  	obj.setR1(vr1);
				  	obj.setR1qty(vr1estqty);
				  	String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where varietycode="+varietycode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
					List ls3=hibernatedao.findBySqlCriteria(sql3);
					Double vr2=null;
					Double vr2estqty=null;
				  	if(ls3.size()>0)
				  	{
				  		vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
				  		vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
				  		if(vr2==null)
				  			vr2=0.0;   
				  		if(vr2estqty==null)
				  			vr2estqty=0.0;
					}
				  	else
				  	{
				  		vr2=0.0;
				  		vr2estqty=0.0;
				  	}
				  	obj.setR2(vr2);
				  	obj.setR2qty(vr2estqty);
				  	String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where varietycode="+varietycode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
					List ls4=hibernatedao.findBySqlCriteria(sql4);
					Double vr3=null;
					Double vr3estqty=null;
				  	if(ls4.size()>0)
				  	{
				  		vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
				  		vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
				  		if(vr3==null)
				  			vr3=0.0;   
				  		if(vr3estqty==null)
						vr3estqty=0.0;
					}
				  	else
				  	{
				  		vr3=0.0;
				  		vr3estqty=0.0;
				  	}
				  	obj.setR3(vr3);
				  	obj.setR3qty(vr3estqty);
				  	obj.setRt(vr1+vr2+vr3);
				  	obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
				  	String sql5="select variety from variety where varietycode="+varietycode+"";
				  	List ls5=hibernatedao.findBySqlCriteria(sql5);
				  	if(ls5.size()>0 )
				  	{
				  		String variety=(String)((Map<Object,Object>)ls5.get(0)).get("variety");
				  		obj.setVarietyname(variety);
				  	}
				  	// For save	
					Session session=hibernatedao.getSessionFactory().openSession();
					session.save(obj);
					session.flush();
					session.close();
					ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
				} 
		  		ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		  		reportFileName = "VarietywiseSummaryVarietywiseReport";
				Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
		}
		else if("Circle".equals(group1[1]))
		{
			sql="select distinct circlecode from agreementdetails where seasonyear='"+season1[1]+"'";
			try
			{
				ls=hibernatedao.findBySqlCriteria(sql); 
				for (int k = 0; k <ls.size(); k++) 
				{
					Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode"); 
			       	obj.setVarietycode(circlecode);	
			        String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where circlecode="+circlecode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
			  		List ls1=hibernatedao.findBySqlCriteria(sql1);
			  		Double vplant=null;
			  		Double vpestqty=null;
				  	if(ls1.size()>0 && ls1!=null)
			  		{	
				  		vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
				  		vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
				  		if(vplant==null)
				  			vplant=0.0;   
				  		if(vpestqty==null)
				  			vpestqty=0.0;
				  	}
				  	else
				  	{
				  		vplant=0.0;
				  		vpestqty=0.0;
				  	}	
				  	obj.setPlant(vplant);
				  	obj.setPqty(vpestqty);
				  		
				  	String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where circlecode="+circlecode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
			 		List ls2=hibernatedao.findBySqlCriteria(sql2);
			  		Double vr1=null;
			  		Double vr1estqty=null;
			  		if(ls2.size()>0)
			  		{
			  			vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
			  			vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
				  			
			  			if(vr1==null)
			  				vr1=0.0;   
			  			if(vr1estqty==null)
			  				vr1estqty=0.0;
				  			
					}
			  		else
			  		{
			  			vr1=0.0;
			  			vr1estqty=0.0;
			  		}
			  		obj.setR1(vr1);
			  		obj.setR1qty(vr1estqty);
			  		
			  		 String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where circlecode="+circlecode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
				  		List ls3=hibernatedao.findBySqlCriteria(sql3);
				  		Double vr2=null;
				  		Double vr2estqty=null;
			  		if(ls3.size()>0)
			  		{
			  			vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
			  			vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
			  			 if(vr2==null)
			  				vr2=0.0;   
					  		   if(vr2estqty==null)
					  			 vr2estqty=0.0;
				  			
				  	}
			  		else
			  		{
			  			vr2=0.0;
			  			vr2estqty=0.0;
			  		}
			  		obj.setR2(vr2);
			  		obj.setR2qty(vr2estqty);
			  		 String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where circlecode="+circlecode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
				  		List ls4=hibernatedao.findBySqlCriteria(sql4);
				  		Double vr3=null;
				  		Double vr3estqty=null;
			  		if(ls4.size()>0)
			  		{
			  			vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
			  			vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
			  			 if(vr3==null)
				  				vr3=0.0;   
						  		   if(vr3estqty==null)
						  			 vr3estqty=0.0;
				  	}
			  		else
			  		{
			  			vr3=0.0;
			  			vr3estqty=0.0;
			  		}
			  		obj.setR3(vr3);
			  		obj.setR3qty(vr3estqty);
			  		obj.setRt(vr1+vr2+vr3);
			  		obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
			  		
			  		
				 	String sql5="select circle from circle where circlecode="+circlecode+"";
			  		List ls5=hibernatedao.findBySqlCriteria(sql5);
			  		
			  		if(ls5.size()>0 )
			  		{
			  			String circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
			  			obj.setVarietyname(circle);
			  		}
			  		
				
		  		// For save	
				 Session session=hibernatedao.getSessionFactory().openSession();
				session.save(obj);
				 session.flush();
				 session.close();
				 
				 
				 ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
						
			} 
	  		ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	  		reportFileName = "VarietywiseSummaryCirclewiseReport";
			Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
			
		}
		else if("Region".equals(group1[1]))
		{
			sql="select distinct regioncode from agreementdetails where seasonyear='"+season1[1]+"'";
			try{	
		ls=hibernatedao.findBySqlCriteria(sql); 
		for(int k = 0; k <ls.size(); k++) 
		{
		Integer regioncode=(Integer)((Map<Object,Object>)ls.get(k)).get("regioncode"); 
		obj.setVarietycode(regioncode);	
		String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where regioncode="+regioncode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		Double vplant=null;
		Double vpestqty=null;
		if(ls1.size()>0 && ls1!=null)
		{	
		vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
		vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
		if(vplant==null)
		vplant=0.0;   
		if(vpestqty==null)
		vpestqty=0.0;
		}
		else
		{
		vplant=0.0;
		vpestqty=0.0;
		}	
		obj.setPlant(vplant);
		obj.setPqty(vpestqty);
		String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where regioncode="+regioncode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		Double vr1=null;
		Double vr1estqty=null;
		if(ls2.size()>0)
		{
		vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
		vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
		if(vr1==null)
		vr1=0.0;   
		if(vr1estqty==null)
		vr1estqty=0.0;
		}
		else
		{
		vr1=0.0;
		vr1estqty=0.0;
		}
		obj.setR1(vr1);
		obj.setR1qty(vr1estqty);
		String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where regioncode="+regioncode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
		List ls3=hibernatedao.findBySqlCriteria(sql3);
		Double vr2=null;
		Double vr2estqty=null;
		if(ls3.size()>0)
		{
		vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
		vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
		if(vr2==null)
		vr2=0.0;   
		if(vr2estqty==null)
		vr2estqty=0.0;
		}
		else
		{
		vr2=0.0;
		vr2estqty=0.0;
		}
  		obj.setR2(vr2);
  		obj.setR2qty(vr2estqty);
  		String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where regioncode="+regioncode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
	  	List ls4=hibernatedao.findBySqlCriteria(sql4);
	  	Double vr3=null;
	  	Double vr3estqty=null;
  		if(ls4.size()>0)
  		{
  		vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
  		vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
  		if(vr3==null)
	  	vr3=0.0;   
		if(vr3estqty==null)
		vr3estqty=0.0;
	  	}
  		else
  		{
  		vr3=0.0;
  		vr3estqty=0.0;
  		}
  		obj.setR3(vr3);
  		obj.setR3qty(vr3estqty);
  		
  		obj.setRt(vr1+vr2+vr3);
  		obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
  		
			  		
		String sql5="select region from region where regioncode="+regioncode+"";
		List ls5=hibernatedao.findBySqlCriteria(sql5);
		
		if(ls5.size()>0 )
		{
			String region=(String)((Map<Object,Object>)ls5.get(0)).get("region");
			obj.setVarietyname(region);
		}
		
		// For save	
		 Session session=hibernatedao.getSessionFactory().openSession();
		session.save(obj);
		 session.flush();
		 session.close();
		 
		 
		 ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
			
		} 
		ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		reportFileName = "VarietywiseSummaryRegionwiseReport";
		Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
		
		}
		else if("Mandal".equals(group1[1]))
		{
			try{
				sql="select distinct mandalcode from agreementdetails where seasonyear='"+season1[1]+"'";
					
				ls=hibernatedao.findBySqlCriteria(sql); 
					  		
				for (int k = 0; k <ls.size(); k++) 
				{
			
				Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode"); 
		       	obj.setVarietycode(mandalcode);	
		        String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where mandalcode="+mandalcode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
		  		Double vplant=null;
		  		Double vpestqty=null;
		  		if(ls1.size()>0 && ls1!=null)
		  		{	
				vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
				vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
			   if(vplant==null)
			      vplant=0.0;   
			   if(vpestqty==null)
				   vpestqty=0.0;
				
		  		}
		  		else
		  		{
				vplant=0.0;
				vpestqty=0.0;
		  		}	
		  		obj.setPlant(vplant);
		  		obj.setPqty(vpestqty);
		  		
		  		 String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where mandalcode="+mandalcode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		Double vr1=null;
		  		Double vr1estqty=null;
		  		if(ls2.size()>0)
		  		{
				vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
				vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
				
				 if(vr1==null)
					vr1=0.0;   
		  		   if(vr1estqty==null)
		  			 vr1estqty=0.0;
		  			
			  	}
		  		else
		  		{
				vr1=0.0;
				vr1estqty=0.0;
		  		}
		  		obj.setR1(vr1);
		  		obj.setR1qty(vr1estqty);
		  		
		  		 String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where mandalcode="+mandalcode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  		Double vr2=null;
		  		Double vr2estqty=null;
		  		if(ls3.size()>0)
		  		{
		  			vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
		  			vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
		  			 if(vr2==null)
					vr2=0.0;   
		  		   if(vr2estqty==null)
		  			 vr2estqty=0.0;
		  			
			  	}
		  		else
		  		{
		  			vr2=0.0;
		  			vr2estqty=0.0;
		  		}
		  		obj.setR2(vr2);
		  		obj.setR2qty(vr2estqty);
		  		String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where mandalcode="+mandalcode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
		  		Double vr3=null;
		  		Double vr3estqty=null;
		  		if(ls4.size()>0)
		  		{
		  			vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
		  			vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
		  			 if(vr3==null)
			  				vr3=0.0;   
					  		   if(vr3estqty==null)
					  			 vr3estqty=0.0;
			  	}
		  		else
		  		{
		  			vr3=0.0;
		  			vr3estqty=0.0;
		  		}
		  		obj.setR3(vr3);
		  		obj.setR3qty(vr3estqty);
		  		obj.setRt(vr1+vr2+vr3);
		  		obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
		  		
		  		
			 	String sql5="select mandal from mandal where mandalcode="+mandalcode+"";
		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
		  		
		  		if(ls5.size()>0 )
		  		{
		  			String mandal=(String)((Map<Object,Object>)ls5.get(0)).get("mandal");
		  			obj.setVarietyname(mandal);
		  		}
		  		
			
				// For save	
				 Session session=hibernatedao.getSessionFactory().openSession();
				session.save(obj);
				 session.flush();
				 session.close();
				 
			 
			 ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
				
			} 
			ls.clear();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				reportFileName = "VarietywiseSummaryMandalwiseReport";
				Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
		}
		
		else if("Village".equals(group1[1]))
		{
			sql="select distinct villagecode from agreementdetails where seasonyear='"+season1[1]+"'";
			try{
				ls=hibernatedao.findBySqlCriteria(sql); 
				for (int k = 0; k <ls.size(); k++) 
				{
					String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode"); 
					obj.setVarietycode(Integer.parseInt(villagecode));	
					String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where villagecode="+villagecode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
					List ls1=hibernatedao.findBySqlCriteria(sql1);
					Double vplant=null;
					Double vpestqty=null;
					if(ls1.size()>0 && ls1!=null)
					{	
						vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
						vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
						if(vplant==null)
							vplant=0.0;   
						if(vpestqty==null)
							vpestqty=0.0;
	  			
					}
					else
					{
						vplant=0.0;
						vpestqty=0.0;
					}	
					obj.setPlant(vplant);
					obj.setPqty(vpestqty);
					String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where villagecode="+villagecode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
					List ls2=hibernatedao.findBySqlCriteria(sql2);
					Double vr1=null;
					Double vr1estqty=null;
					if(ls2.size()>0)
					{
						vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
						vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
						if(vr1==null)
							vr1=0.0;   
						if(vr1estqty==null)
							vr1estqty=0.0;
					}
					else
					{
						vr1=0.0;
						vr1estqty=0.0;
					}
					obj.setR1(vr1);
					obj.setR1qty(vr1estqty);
					String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where villagecode="+villagecode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
					List ls3=hibernatedao.findBySqlCriteria(sql3);
					Double vr2=null;
					Double vr2estqty=null;
					if(ls3.size()>0)
					{
						vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
						vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
						if(vr2==null)
							vr2=0.0;   
						if(vr2estqty==null)
							vr2estqty=0.0;
					}
					else
					{
						vr2=0.0;
						vr2estqty=0.0;
					}
					obj.setR2(vr2);
					obj.setR2qty(vr2estqty);
					String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where villagecode="+villagecode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
			  		List ls4=hibernatedao.findBySqlCriteria(sql4);
			  		Double vr3=null;
			  		Double vr3estqty=null;
			  		if(ls4.size()>0)
			  		{
			  			vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
			  			vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
			  			if(vr3==null)
			  				vr3=0.0;   
			  			if(vr3estqty==null)
			  				vr3estqty=0.0;
			  		}
			  		else
			  		{
			  			vr3=0.0;
			  			vr3estqty=0.0;
			  		}
			  		obj.setR3(vr3);
			  		obj.setR3qty(vr3estqty);
			  		obj.setRt(vr1+vr2+vr3);
			  		obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
			  		String sql5="select village from village where villagecode="+villagecode+"";
			  		List ls5=hibernatedao.findBySqlCriteria(sql5);
			  		if(ls5.size()>0 )
			  		{
			  			String village=(String)((Map<Object,Object>)ls5.get(0)).get("village");
			  			obj.setVarietyname(village);
			  		}
			  		// For save	
			  		Session session=hibernatedao.getSessionFactory().openSession();
			  		session.save(obj);
			  		session.flush();
			  		session.close();
			  		ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
					} 
				ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			reportFileName = "VarietywiseSummaryVillagewiseReport";
	  		Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
			}
			else if("Zone".equals(group1[1]))
			{
				sql="select distinct zonecode from agreementdetails where seasonyear='"+season1[1]+"'";
				try{	
					ls=hibernatedao.findBySqlCriteria(sql); 
					for (int k = 0; k <ls.size(); k++) 
					{
						Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode"); 
						obj.setVarietycode(zonecode);	
						String sql1="select sum(isnull(extentsize,0)) as plant,sum(isnull(agreedqty,0)) as pqty from agreementdetails where zonecode="+zonecode+" and plantorratoon='1' and seasonyear='"+season1[1]+"'";
						List ls1=hibernatedao.findBySqlCriteria(sql1);
						Double vplant=null;
						Double vpestqty=null;
						if(ls1.size()>0 && ls1!=null)
						{	
							vplant=(Double)((Map<Object,Object>)ls1.get(0)).get("plant"); 
							vpestqty=(Double)((Map<Object,Object>)ls1.get(0)).get("pqty"); 
							if(vplant==null)
								vplant=0.0;   
							if(vpestqty==null)
								vpestqty=0.0;
						}
						else
						{
							vplant=0.0;
							vpestqty=0.0;
						}	
						obj.setPlant(vplant);
						obj.setPqty(vpestqty);
						String sql2="select sum(isnull(extentsize,0)) as r1,sum(isnull(agreedqty,0)) as r1qty from agreementdetails where zonecode="+zonecode+" and plantorratoon='2' and seasonyear='"+season1[1]+"'";
						List ls2=hibernatedao.findBySqlCriteria(sql2);
				  		Double vr1=null;
				  		Double vr1estqty=null;
				  		if(ls2.size()>0)
				  		{
				  			vr1=(Double)((Map<Object,Object>)ls2.get(0)).get("r1"); 
				  			vr1estqty=(Double)((Map<Object,Object>)ls2.get(0)).get("r1qty"); 
				  			if(vr1==null)
				  				vr1=0.0;   
				  			if(vr1estqty==null)
				  				vr1estqty=0.0;
				  		}
				  		else
				  		{
				  			vr1=0.0;
				  			vr1estqty=0.0;
				  		}
				  		obj.setR1(vr1);
				  		obj.setR1qty(vr1estqty);
				  		String sql3="select sum(isnull(extentsize,0)) as r2,sum(isnull(agreedqty,0)) as r2qty from agreementdetails where zonecode="+zonecode+" and plantorratoon='3' and seasonyear='"+season1[1]+"'";
				  		List ls3=hibernatedao.findBySqlCriteria(sql3);
				  		Double vr2=null;
				  		Double vr2estqty=null;
				  		if(ls3.size()>0)
				  		{
				  			vr2=(Double)((Map<Object,Object>)ls3.get(0)).get("r2"); 
				  			vr2estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("r2qty"); 
				  			if(vr2==null)
				  				vr2=0.0;   
				  			if(vr2estqty==null)
				  				vr2estqty=0.0;
				  		}
				  		else
				  		{
				  			vr2=0.0;
				  			vr2estqty=0.0;
				  		}
				  		obj.setR2(vr2);
			  			obj.setR2qty(vr2estqty);
			  			String sql4="select sum(isnull(extentsize,0)) as r3,sum(isnull(agreedqty,0)) as r3qty from agreementdetails where zonecode="+zonecode+" and plantorratoon in(4,5) and seasonyear='"+season1[1]+"'";
				  		List ls4=hibernatedao.findBySqlCriteria(sql4);
				  		Double vr3=null;
				  		Double vr3estqty=null;
				  		if(ls4.size()>0)
				  		{
				  			vr3=(Double)((Map<Object,Object>)ls4.get(0)).get("r3"); 
				  			vr3estqty=(Double)((Map<Object,Object>)ls4.get(0)).get("r3qty"); 
				  			if(vr3==null)
				  				vr3=0.0;   
				  			if(vr3estqty==null)
				  				vr3estqty=0.0;
				  		}
				  		else
				  		{
				  			vr3=0.0;
				  			vr3estqty=0.0;
				  		}
				  		obj.setR3(vr3);
				  		obj.setR3qty(vr3estqty);
				  		obj.setRt(vr1+vr2+vr3);
				  		obj.setRtqty(vr1estqty+vr2estqty+vr3estqty);
				  		String sql5="select zone from zone where zonecode="+zonecode+"";
				  		List ls5=hibernatedao.findBySqlCriteria(sql5);
				  		if(ls5.size()>0 )
				  		{
				  			String zone=(String)((Map<Object,Object>)ls5.get(0)).get("zone");
				  			obj.setVarietyname(zone);
				  		}
				  		// For save	
				  		Session session=hibernatedao.getSessionFactory().openSession();
				  		session.save(obj);
				  		session.flush();
				  		session.close();
				  		ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
					} 
					ls.clear();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				reportFileName = "VarietywiseSummaryZonewiseReport";
				Query1="distinct plant,pqty,r1,r1qty,r2,r2qty,r3,r3qty,rt,rtqty,varietycode,varietyname from Temp_VarietywiseSummary order by varietycode";
				
			}
  		// end of variety wise report data
		
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}

	@RequestMapping(value = "/generateCaneCrushingReport", method = RequestMethod.POST)
	public void generateCaneCrushingReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("status");	
		String season=request.getParameter("season");
		String group=request.getParameter("GROWER");
		String group1[]=group.split(":");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
			

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
	   
	    String sqq="truncate table Temp_CaneCrushingReport";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneCrushingReport obj=new Temp_CaneCrushingReport();
	    String Query1=null;
		String reportFileName=null;
		String sql;
		List ls;
		Session session=hibernatedao.getSessionFactory().openSession();
		if("Circle".equals(group1[1]))
		{
		  sql="select distinct circlecode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
		  try
		  {
			  ls=hibernatedao.findBySqlCriteria(sql); 
			  for(int k = 0; k <ls.size(); k++) 
			  {
				  obj=new Temp_CaneCrushingReport();
				  Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode"); 
				  
				  String sql4="select circle from Circle where circlecode="+circlecode+"";
				  List ls4=hibernatedao.findBySqlCriteria(sql4);	 
				  if(ls4.size()>0 && ls4!=null)
				  {	
					 String circle=(String)((Map<Object,Object>)ls4.get(0)).get("circle"); 
					 obj.setNametype(circle);
				  }
				  String sql1="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  circlecode="+circlecode+" and shiftid1='1' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  Double netwt=null;
				  if(ls1.size()>0 && ls1!=null)
				  {	
					  netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
					  
					  if(netwt==null)
					  {	
						  netwt=0.0;
					  }	
					  obj.setShift1(netwt);
				  }
				  String sql2="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  circlecode="+circlecode+" and shiftid1='2' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				  List ls2=hibernatedao.findBySqlCriteria(sql2);
					  // Double lorryandcanewt=null;
				  if(ls2.size()>0 && ls2!=null)
				  {	
					  netwt=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
						  		
					  if(netwt==null)
					  {	
						  netwt=0.0;
					  }	
						  obj.setShift2(netwt);
					   }
				  String sql3="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  circlecode="+circlecode+" and shiftid1='3' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				  List ls3=hibernatedao.findBySqlCriteria(sql3);	  
				  if(ls3.size()>0 && ls3!=null)
				  {	
					  netwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					  
					  if(netwt==null)
					  {	
						  netwt=0.0;
					  }	
					  obj.setShift3(netwt);
				  }
				  session.save(obj);
			  } 
			  session.flush();
			  session.close();
			 // ls.clear();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CaneCrushingCircleWiseReport";
		  Query1="distinct nametype,isnull(shift1,0) shift1,isnull(shift2,0) shift2,isnull(shift3,0) shift3 from Temp_CaneCrushingReport ";
		}
		else if("Zone".equals(group1[1]))
		{
		  sql="select distinct zonecode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
		  try
		  {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_CaneCrushingReport();
				Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode"); 
				String sql4="select zone from Zone where zonecode="+zonecode+"";
				List ls4=hibernatedao.findBySqlCriteria(sql4);	 
				if(ls4.size()>0 && ls4!=null)
				{	
					String zone=(String)((Map<Object,Object>)ls4.get(0)).get("zone"); 
					obj.setNametype(zone);
				}
				
				String sql1="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  zonecode="+zonecode+" and shiftid1='1' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				Double netwt=null;
				if(ls1.size()>0 && ls1!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
					  		
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift1(netwt);
				}
				String sql2="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  zonecode="+zonecode+" and shiftid1='2' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls2=hibernatedao.findBySqlCriteria(sql2);
				
				if(ls2.size()>0 && ls2!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift2(netwt);
				}
				String sql3="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  zonecode="+zonecode+" and shiftid1='3'and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls3=hibernatedao.findBySqlCriteria(sql3);	  
				if(ls3.size()>0 && ls3!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift3(netwt);
				}
				//Session session=hibernatedao.getSessionFactory().openSession();
				session.save(obj);
				
			}
			session.flush();
			session.close();
			ls.clear();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CaneCrushingZoneWiseReport";
		  Query1="distinct nametype,isnull(shift1,0) shift1,isnull(shift2,0) shift2,isnull(shift3,0) shift3 from Temp_CaneCrushingReport ";
		}	
		
		else if("Village".equals(group1[1]))
		{
		  sql="select distinct villagecode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
		  try
		  {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_CaneCrushingReport();
				String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode"); 
				
				String sql4="select village from Village where villagecode="+villagecode+"";
				List ls4=hibernatedao.findBySqlCriteria(sql4);	 
				if(ls4.size()>0 && ls4!=null)
				{	
					String village=(String)((Map<Object,Object>)ls4.get(0)).get("village"); 
					obj.setNametype(village);
				}
				String sql1="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  villagecode='"+villagecode+"' and shiftid1='1' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				Double netwt=null;
				if(ls1.size()>0 && ls1!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
					  		
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift1(netwt);
				}
				String sql2="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  villagecode='"+villagecode+"' and shiftid1='2' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls2=hibernatedao.findBySqlCriteria(sql2);
				
				if(ls2.size()>0 && ls2!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift2(netwt);
				}
				String sql3="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  villagecode='"+villagecode+"' and shiftid1='3' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls3=hibernatedao.findBySqlCriteria(sql3);	  
				if(ls3.size()>0 && ls3!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift3(netwt);
				}
				session.save(obj);
			}
			session.flush();
			session.close();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CaneCrushingVillageWiseReport";
		  Query1="distinct nametype,isnull(shift1,0) shift1,isnull(shift2,0) shift2,isnull(shift3,0) shift3 from Temp_CaneCrushingReport ";
		}
		else if("Mandal".equals(group1[1]))
		{
		  sql="select distinct mandalcode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
		  try
		  {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_CaneCrushingReport();
				Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode"); 
				
				String sql4="select mandal from Mandal where mandalcode="+mandalcode+"";
				List ls4=hibernatedao.findBySqlCriteria(sql4);	 
				if(ls4.size()>0 && ls4!=null)
				{	
					String mandal=(String)((Map<Object,Object>)ls4.get(0)).get("mandal"); 
					obj.setNametype(mandal);
				}
				String sql1="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  mandalcode="+mandalcode+" and shiftid1='1' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				Double netwt=null;
				if(ls1.size()>0 && ls1!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
					  		
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift1(netwt);
				}
				String sql2="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  mandalcode="+mandalcode+" and shiftid1='2' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls2=hibernatedao.findBySqlCriteria(sql2);
				
				if(ls2.size()>0 && ls2!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift2(netwt);
				}
				String sql3="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  mandalcode="+mandalcode+" and shiftid1='3' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls3=hibernatedao.findBySqlCriteria(sql3);	  
				if(ls3.size()>0 && ls3!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift3(netwt);
				}
				session.save(obj);
			}
			session.flush();
			session.close();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CaneCrushingMandalWiseReport";
		  Query1="distinct nametype,isnull(shift1,0) shift1,isnull(shift2,0) shift2,isnull(shift3,0) shift3 from Temp_CaneCrushingReport ";
		}	
		else if("Region".equals(group1[1]))
		{
		  sql="select distinct regioncode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
		  try
		  {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_CaneCrushingReport();
				Integer regioncode=(Integer)((Map<Object,Object>)ls.get(k)).get("regioncode"); 
				String sql5="select region from Region where regioncode="+regioncode+"";
				List ls5=hibernatedao.findBySqlCriteria(sql5);	 
				if(ls5.size()>0 && ls5!=null)
				{	
					String region=(String)((Map<Object,Object>)ls5.get(0)).get("region"); 
					obj.setNametype(region);
				}
				String sql1="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  regioncode="+regioncode+" and shiftid1='1' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				Double netwt=null;
				if(ls1.size()>0 && ls1!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
					  		
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift1(netwt);
				}
				String sql2="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  regioncode="+regioncode+" and shiftid1='2' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls2=hibernatedao.findBySqlCriteria(sql2);
				
				if(ls2.size()>0 && ls2!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift2(netwt);
				}
				String sql3="select sum(netwt)as netwt from WeighmentDetails where season='"+season1[1]+"' and  regioncode="+regioncode+" and shiftid1='3' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
				List ls3=hibernatedao.findBySqlCriteria(sql3);	  
				if(ls3.size()>0 && ls3!=null)
				{	
					netwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					if(netwt==null)
					{	
						netwt=0.0;
					}	
					obj.setShift3(netwt);
				}
				session.save(obj);
				
			} 
			session.flush();
			session.close();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CaneCrushingRegionWiseReport";
		  Query1="distinct nametype,isnull(shift1,0) shift1,isnull(shift2,0) shift2,isnull(shift3,0) shift3 from Temp_CaneCrushingReport ";
		}	
	
		HashMap<String,Object> hmParams=new HashMap<String,Object>();
		hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
		hmParams.put("qrs", Query1);
		hmParams.put("cdate", dateFormat.format(date));
		generateReport(request,response,hmParams,reportFileName,rptFormat);
	}
	
	
 	@RequestMapping(value = "/generateCumulativeCaneCrushingReport", method = RequestMethod.POST)
	public void generateCumulativeCaneCrushingReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String rptFormat = request.getParameter("status");	
		String season=request.getParameter("season");
		String group=request.getParameter("GROWER");
		String group1[]=group.split(":");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
			
		/*String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);*/
	   
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
				
	    String sqq="truncate table Tepm_CumulativeCaneCrushing";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Tepm_CumulativeCaneCrushing obj=new Tepm_CumulativeCaneCrushing();
	    String Query1=null;
		String reportFileName=null;
		String sql;
		List ls;
		Session session=hibernatedao.getSessionFactory().openSession();
		if("Circle".equals(group1[1]))
		{
		  sql="select distinct circlecode,circle from circle where status=0 ";
		  try
		  {
		  ls=hibernatedao.findBySqlCriteria(sql); 
		  for(int k = 0; k <ls.size(); k++) 
		  {
			  obj=new Tepm_CumulativeCaneCrushing();
			  Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode"); 
			  String circle=(String)((Map<Object,Object>)ls.get(k)).get("circle"); 
			  obj.setNametype(circle);
			  String sql1="select sum(progressiveqty) progressiveqty,sum(estimatedqty) estimatedqty from AgreementDetails where circlecode="+circlecode+" and seasonyear='"+season1[1]+"'";
			  List ls1=hibernatedao.findBySqlCriteria(sql1);
					
			  if(ls1.size()>0 && ls1!=null)
			  {	
				  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
				  if(progressiveqty==null)
				  {	
					  progressiveqty=0.0;
				  }	
				  obj.setProgressiveqty(progressiveqty);
			  }
			  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
			  if(estimatedqty==null)
			  {	
				  estimatedqty=0.0;
			  }	
			  obj.setEstimatedqty(estimatedqty);
			  session.save(obj);
		  }
		  session.flush();
		  session.close();	
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CumulativeCaneCrushingCircleWise";
		  Query1="distinct nametype,progressiveqty,estimatedqty from Tepm_CumulativeCaneCrushing ";
		}
		else if("Village".equals(group1[1]))
		{
			sql="select villagecode,village from Village where status=0";
			try{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
			  obj=new Tepm_CumulativeCaneCrushing();
			  String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode"); 
			  String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
			  obj.setNametype(village);
		
			  String sql1="select sum(progressiveqty) progressiveqty,sum(estimatedqty) estimatedqty from AgreementDetails where villagecode='"+villagecode+"' and seasonyear='"+season1[1]+"'";
			  List ls1=hibernatedao.findBySqlCriteria(sql1);
			  if(ls1.size()>0 && ls1!=null)
			  {	
				  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
				  if(progressiveqty==null)
				  {	
					  progressiveqty=0.0;
				  }	
				  obj.setProgressiveqty(progressiveqty);
			  }
			  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
			  if(estimatedqty==null)
			  {	
				  estimatedqty=0.0;
			  }	
			  obj.setEstimatedqty(estimatedqty);
			  session.save(obj);
			}
			session.flush();
			session.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			reportFileName = "CumulativeCaneCrushingVillageWise";
			Query1=" nametype,progressiveqty,estimatedqty from Tepm_CumulativeCaneCrushing ";
		}
		
		else if("Zone".equals(group1[1]))
		{
		  sql="select distinct zonecode,zone from Zone where status=0";
		  try
		  {
			  ls=hibernatedao.findBySqlCriteria(sql); 
			  for(int k = 0; k <ls.size(); k++) 
			  {
				  obj=new Tepm_CumulativeCaneCrushing();
				  Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode"); 
				  String zone=(String)((Map<Object,Object>)ls.get(k)).get("zone"); 
				  obj.setNametype(zone);
				  String sql1="select sum(progressiveqty) progressiveqty,sum(estimatedqty) estimatedqty from AgreementDetails where zonecode="+zonecode+" and seasonyear='"+season1[1]+"'";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  if(ls1.size()>0 && ls1!=null)
				  {	
					  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					  if(progressiveqty==null)
					  {	
						  progressiveqty=0.0;
					  }	
					  obj.setProgressiveqty(progressiveqty);
				  }
				  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
				  if(estimatedqty==null)
				  {	
					  estimatedqty=0.0;
				  }	
				  obj.setEstimatedqty(estimatedqty);
				  session.save(obj);
			  }
			  session.flush();
			  session.close();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CumulativeCaneCrushingZoneWise";
		  Query1="distinct nametype,progressiveqty,estimatedqty from Tepm_CumulativeCaneCrushing ";
		}
		else if("Mandal".equals(group1[1]))
		{
		sql="select distinct mandalcode,mandal from Mandal where status=0";
		try
		{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Tepm_CumulativeCaneCrushing();
				Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode"); 
				String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal"); 
				obj.setNametype(mandal);
				String sql1="select sum(progressiveqty) progressiveqty,sum(estimatedqty) estimatedqty from AgreementDetails where mandalcode="+mandalcode+" and seasonyear='"+season1[1]+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
					
				if(ls1.size()>0 && ls1!=null)
				{	
					Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					if(progressiveqty==null)
					{	
						progressiveqty=0.0;
					}	
					obj.setProgressiveqty(progressiveqty);
				}
					
				Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
				if(estimatedqty==null)
				{	
					estimatedqty=0.0;
				}	
				obj.setEstimatedqty(estimatedqty);
				session.save(obj);
			}
			session.flush();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		reportFileName = "CumulativeCaneCrushingMandalWise";
		Query1="distinct nametype,progressiveqty,estimatedqty from Tepm_CumulativeCaneCrushing ";
		}
		else if("Region".equals(group1[1]))
		{
		sql="select distinct regioncode,region from Region where status=0";
		try
		{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Tepm_CumulativeCaneCrushing();
				Integer regioncode=(Integer)((Map<Object,Object>)ls.get(k)).get("regioncode"); 
				String region=(String)((Map<Object,Object>)ls.get(k)).get("region"); 
				obj.setNametype(region);
				String sql1="select sum(progressiveqty) progressiveqty,sum(estimatedqty) estimatedqty from AgreementDetails where regioncode="+regioncode+" and seasonyear='"+season1[1]+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				if(ls1.size()>0 && ls1!=null)
				{	
					Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					if(progressiveqty==null)
					{	
						progressiveqty=0.0;
					}	
					obj.setProgressiveqty(progressiveqty);
				}
				Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
				if(estimatedqty==null)
				{	
					estimatedqty=0.0;
				}	
				obj.setEstimatedqty(estimatedqty);
				session.save(obj);
			}
			session.flush();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		reportFileName = "CumulativeCaneCrushingRegionWise";
		Query1="distinct nametype,progressiveqty,estimatedqty from Tepm_CumulativeCaneCrushing ";
		}
		HashMap<String,Object> hmParams=new HashMap<String,Object>();
		hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
		hmParams.put("qrs", Query1);
		hmParams.put("cdate", dateFormat.format(date));
		generateReport(request,response,hmParams,reportFileName,rptFormat);
	}
	
	@RequestMapping(value = "/generateLoginUser", method = RequestMethod.POST)
	public void generateLoginUser(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_LoginUser";
		
		try{
			SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		//String sql="insert into Temp_BankOrderReport(ryotcode,ryotname,progressiveqty,village,loannumber,pendingamount,referencenumber,branchcode,branchname) select a.ryotcode,a.ryotname,a.progressiveqty,v.village,l.loannumber,isnull(l.pendingamount,0),l.referencenumber,b.branchcode,b.branchname FROM village AS v INNER JOIN  agreementdetails a ON v.villagecode=a.villagecode INNER JOIN loandetails l ON a.ryotcode=l.ryotcode INNER JOIN branch as b ON l.branchcode=b.branchcode where b.branchname='"+bName[1]+"' and a.seasonyear='"+sName[1]+"'";  

		String sql="insert into Temp_LoginUser(employeename,loginid,mobileno,status,role) select e.employeename,e.loginid,e.mobileno,e.status,isnull(r.role,0) from Employees as e inner join roles r on r.roleid=e.roleid";
		SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sql);
		query1.executeUpdate();
		//String sql="create table temp_bank select a.ryotcode,a.ryotname,a.progressiveqty,v.village,l.loannumber,l.pendingamount,l.referencenumber,b.branchcode,b.branchname FROM village AS v INNER JOIN  agreementdetails a ON v.villagecode=a.villagecode INNER JOIN loandetails l ON a.ryotcode=l.ryotcode INNER JOIN branch as b ON l.branchcode=b.branchcode where 1=2";
			
        String Query1;
		String reportFileName;
	
		 reportFileName = "UserLoginReport";
		 Query1="distinct employeename,loginid,mobileno,status,role FROM Temp_LoginUser";
		
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        //hmParams.put("cdate",format.format(new Date())); //date
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	@RequestMapping(value = "/generatePermitPrintForRegenerate", method = RequestMethod.GET)
	public void generatePermitPrintForRegenerate(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		//String pNum = (String) jsonData.get("permitNumber");
		String pNum=request.getParameter("permitNumber");
		String rptFormat = "PDF";//request.getParameter("status");
		String sqq="truncate table Temp_PrintPermit";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_PrintPermit obj=new Temp_PrintPermit();
		String sql="select distinct * from PermitDetails where permitnumber="+pNum+"";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		//obj.setPermitnumber(pNum);
  		Integer zonecode=0;
  		Integer circlecode=0;
  		String ryotcode=null;
  		Integer varietycode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  		 if(ls.size()>0 && ls!=null)
		  {	       
	        ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       Integer permitnumber=(Integer)((Map<Object,Object>)ls.get(0)).get("permitnumber");
	       	zonecode=(Integer)((Map<Object,Object>)ls.get(0)).get("zonecode");
	       	circlecode=(Integer)((Map<Object,Object>)ls.get(0)).get("circlecode");
	     	varietycode=(Integer)((Map<Object,Object>)ls.get(0)).get("varietycode");
	     	String landvilcode=(String)((Map<Object,Object>)ls.get(0)).get("landvilcode");
	     	Integer programno=(Integer)((Map<Object,Object>)ls.get(0)).get("programno");
	     	Integer rank=(Integer)((Map<Object,Object>)ls.get(0)).get("rank");
	     	
	       	obj.setRyotcode(ryotcode);	
	     	obj.setPermitnumber(permitnumber);
	       	obj.setCirclecode(circlecode);
	       	obj.setLandvilcode(landvilcode);
	       	obj.setProgramno(programno);
	       	obj.setRank(rank);
		  }
	      
 		String sql1="select zone from zone where zonecode="+zonecode+"";
   		List ls1=hibernatedao.findBySqlCriteria(sql1); 
   		if(ls1.size()>0 && ls1!=null)
   		{
   			String zone=(String)((Map<Object,Object>)ls1.get(0)).get("zone");
   			obj.setZone(zone);
   		}
   		String sql2="select variety from Variety where varietycode="+varietycode+"";
   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
   		if(ls2.size()>0 && ls2!=null)
   		{
   			String variety=(String)((Map<Object,Object>)ls2.get(0)).get("variety");
   			obj.setVariety(variety);
   		}
   		String sql3="select plantorratoon from AgreementDetails where ryotcode="+ryotcode+"";
   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
   		Integer ptorrt=0;
   		if(ls3.size()>0 && ls3!=null)
   		{
   			String plantorratoon=(String)((Map<Object,Object>)ls3.get(0)).get("plantorratoon");
   			 ptorrt=Integer.parseInt(plantorratoon);
   			//obj.setPlantorratoon(plantorratoon);
   		}
   		String sql4="select croptype from CropTypes where croptypecode="+ptorrt+"";
   		List ls4=hibernatedao.findBySqlCriteria(sql4); 
   		if(ls4.size()>0 && ls4!=null)
   		{
   			String croptype=(String)((Map<Object,Object>)ls4.get(0)).get("croptype");
   			
   			obj.setPlantorratoon(croptype);
   		}
   		String sql5="select circle from Circle where circlecode="+circlecode+"";
   		List ls5=hibernatedao.findBySqlCriteria(sql5); 
   		if(ls5.size()>0 && ls5!=null)
   		{
   			String circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
   			
   			obj.setCircle(circle);
   		}
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
		} 
  		ls.clear();
  		
      //  String Query1;
		//String reportFileName;
		try{
	
		String reportFileName ="ReprintPermitReport";
		String Query1="ryotcode,permitnumber,circlecode,landvilcode,programno,rank,zone,variety,plantorratoon,circle from Temp_PrintPermit";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
       // hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generatePermit", method = RequestMethod.GET)
	public void generatePermit(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		//String pNum = (String) jsonData.get("permitNumber");
		String pNum=request.getParameter("permitNumber");
		String rptFormat = "PDF";//request.getParameter("status");
		String sqq="truncate table Temp_PrintPermit";
		String circle=null;
		Date d1 = new Date();
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
		String formattedDate = df.format(d1);
		String[] time=formattedDate.split(" ");
		
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_PrintPermit obj=new Temp_PrintPermit();
		String sql="select distinct * from PermitDetails where permitnumber="+pNum+"";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		//obj.setPermitnumber(pNum);
  		Integer zonecode=0;
  		Integer circlecode=0;
  		String ryotcode=null;
  		Integer varietycode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  		 if(ls.size()>0 && ls!=null)
		  {	       
	        ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       Integer permitnumber=(Integer)((Map<Object,Object>)ls.get(0)).get("permitnumber");
	       	zonecode=(Integer)((Map<Object,Object>)ls.get(0)).get("zonecode");
	       	circlecode=(Integer)((Map<Object,Object>)ls.get(0)).get("circlecode");
	     	varietycode=(Integer)((Map<Object,Object>)ls.get(0)).get("varietycode");
	     	String landvilcode=(String)((Map<Object,Object>)ls.get(0)).get("landvilcode");
	     	Integer programno=(Integer)((Map<Object,Object>)ls.get(0)).get("programno");
	     	Integer rank=(Integer)((Map<Object,Object>)ls.get(0)).get("rank");
	     	
	       	obj.setRyotcode(ryotcode);	
	     	obj.setPermitnumber(permitnumber);
	       	obj.setCirclecode(circlecode);
	       	obj.setLandvilcode(landvilcode);
	       	obj.setProgramno(programno);
	       	obj.setRank(rank);
		  }
	      
 		String sql1="select zone from zone where zonecode="+zonecode+"";
   		List ls1=hibernatedao.findBySqlCriteria(sql1); 
   		if(ls1.size()>0 && ls1!=null)
   		{
   			String zone=(String)((Map<Object,Object>)ls1.get(0)).get("zone");
   			obj.setZone(zone);
   		}
   		String sql2="select variety from Variety where varietycode="+varietycode+"";
   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
   		if(ls2.size()>0 && ls2!=null)
   		{
   			String variety=(String)((Map<Object,Object>)ls2.get(0)).get("variety");
   			obj.setVariety(variety);
   		}
   		String sql3="select plantorratoon from AgreementDetails where ryotcode="+ryotcode+"";
   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
   		Integer ptorrt=0;
   		if(ls3.size()>0 && ls3!=null)
   		{
   			String plantorratoon=(String)((Map<Object,Object>)ls3.get(0)).get("plantorratoon");
   			 ptorrt=Integer.parseInt(plantorratoon);
   			//obj.setPlantorratoon(plantorratoon);
   		}
   		String sql4="select croptype from CropTypes where croptypecode="+ptorrt+"";
   		List ls4=hibernatedao.findBySqlCriteria(sql4); 
   		if(ls4.size()>0 && ls4!=null)
   		{
   			String croptype=(String)((Map<Object,Object>)ls4.get(0)).get("croptype");
   			
   			obj.setPlantorratoon(croptype);
   		}
   		String sql5="select circle from Circle where circlecode="+circlecode+"";
   		List ls5=hibernatedao.findBySqlCriteria(sql5); 
   		if(ls5.size()>0 && ls5!=null)
   		{
   			circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
   		}
   		else
   			circle="NA";
 
   		obj.setCircle(circle);
   		
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		ls1.clear();ls2.clear();ls3.clear();ls4.clear();ls5.clear();
		} 
  		ls.clear();
  		
      //  String Query1;
		//String reportFileName;
		try{
	
		String reportFileName ="ReprintPermitReport";
		String Query1="ryotcode,permitnumber,circlecode,landvilcode,programno,rank,zone,variety,plantorratoon,circle from Temp_PrintPermit";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
       // hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        hmParams.put("time", time[1]+" "+time[2]);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateCaneSampleCardReport", method = RequestMethod.GET)
	public void generateCaneSampleCard(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String sampleCard=request.getParameter("sampleCard");
		String rptFormat = "PDF";//request.getParameter("status");
		String sqq="truncate table Temp_CaneSampleCard";
		String circle=null;
		
		
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneSampleCard obj=new Temp_CaneSampleCard();
		
		
		if (sampleCard.contains(","))
		{
			String totalsamplecards[] = sampleCard.split(",");
			for (int i = 0; i < totalsamplecards.length; i++)
			{
				String smpleCardNo = totalsamplecards[i];
				
				String sql="select distinct * from SampleCards where samplecard='"+smpleCardNo+"'";
		  		List ls=hibernatedao.findBySqlCriteria(sql);
		  		
		  		Integer circlecode=0;
		  		Integer varietycode=0;
		  		String ryotcode=null;
		  		String villagecode=null;
		  		for (int k = 0; k <ls.size(); k++) 
				{
		  		 if(ls.size()>0 && ls!=null)
				  {	       
			        ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
			     	Integer programnumber=(Integer)((Map<Object,Object>)ls.get(0)).get("programnumber");
			     	Double extentsize=(Double)((Map<Object,Object>)ls.get(0)).get("extentsize");
			     	String brix=(String)((Map<Object,Object>)ls.get(0)).get("brix");
			     	String pol=(String)((Map<Object,Object>)ls.get(0)).get("pol");
			     	String purity=(String)((Map<Object,Object>)ls.get(0)).get("purity");
			     	Double ccsrating=(Double)((Map<Object,Object>)ls.get(0)).get("ccsrating");
			       	obj.setRyotcode(ryotcode);	
			       	obj.setProgramnumber(programnumber);
			       	obj.setExtentsize(extentsize);
			       	obj.setBrix(brix);
			       	obj.setPol(pol);
			       	obj.setPurity(purity);
			       	obj.setCcsrating(ccsrating);
				  }
		   		String sql3="select villagecode,varietycode,circlecode from AgreementDetails where ryotcode="+ryotcode+"";
		   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
		   		if(ls3.size()>0 && ls3!=null)
		   		{
		   			villagecode=(String)((Map<Object,Object>)ls3.get(0)).get("villagecode");
		   			varietycode=(Integer)((Map<Object,Object>)ls3.get(0)).get("varietycode");
		   			circlecode=(Integer)((Map<Object,Object>)ls3.get(0)).get("circlecode");
		   		}
		   		String sql1="select ryotname,relativename from Ryot where ryotcode="+ryotcode+"";
		   		List ls1=hibernatedao.findBySqlCriteria(sql1); 
		   		if(ls1.size()>0 && ls1!=null)
		   		{
		   			String ryotname=(String)((Map<Object,Object>)ls1.get(0)).get("ryotname");
		   			String relativename=(String)((Map<Object,Object>)ls1.get(0)).get("relativename");
		   			obj.setRyotname(ryotname);
		   			obj.setRelativename(relativename);
		   		}
		   		String sql2="select village from Village where villagecode='"+villagecode+"'";
		   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
		   		if(ls2.size()>0 && ls2!=null)
		   		{
		   			String village=(String)((Map<Object,Object>)ls2.get(0)).get("village");
		   			obj.setVillage(village);
		   		}
		   		String sql4="select variety from Variety where varietycode="+varietycode+"";
		   		List ls4=hibernatedao.findBySqlCriteria(sql4); 
		   		if(ls4.size()>0 && ls4!=null)
		   		{
		   			String variety=(String)((Map<Object,Object>)ls4.get(0)).get("variety");
		   			obj.setVariety(variety);
		   		}
		   		String sql5="select circle from Circle where circlecode="+circlecode+"";
		   		List ls5=hibernatedao.findBySqlCriteria(sql5); 
		   		if(ls5.size()>0 && ls5!=null)
		   		{
		   			circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
		   		}
		   		else
		   			circle="NA";
		 
		   		obj.setCircle(circle);
		   		
				 Session session=hibernatedao.getSessionFactory().openSession();
				 session.save(obj);
				 session.flush();
				 session.close();
				 ls3.clear();ls5.clear();ls1.clear();ls2.clear();ls4.clear();
				} 
		  		ls.clear();
			}
		}
		else
		{
			String sql="select distinct * from SampleCards where samplecard='"+sampleCard+"'";
	  		List ls=hibernatedao.findBySqlCriteria(sql);
	  		Integer circlecode=0;
	  		Integer varietycode=0;
	  		String ryotcode=null;
	  		String villagecode=null;
	  		for (int k = 0; k <ls.size(); k++) 
			{
	  		 if(ls.size()>0 && ls!=null)
			  {	       
		        ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
		     	Integer programnumber=(Integer)((Map<Object,Object>)ls.get(0)).get("programnumber");
		     	Double extentsize=(Double)((Map<Object,Object>)ls.get(0)).get("extentsize");
		     	String brix=(String)((Map<Object,Object>)ls.get(0)).get("brix");
		     	String pol=(String)((Map<Object,Object>)ls.get(0)).get("pol");
		     	String purity=(String)((Map<Object,Object>)ls.get(0)).get("purity");
		     	Double ccsrating=(Double)((Map<Object,Object>)ls.get(0)).get("ccsrating");
		     	
		       	obj.setRyotcode(ryotcode);	
		       	obj.setProgramnumber(programnumber);
		       	obj.setExtentsize(extentsize);
		       	obj.setBrix(brix);
		       	obj.setPol(pol);
		       	obj.setPurity(purity);
		       	obj.setCcsrating(ccsrating);
			  }
	   		String sql3="select villagecode,varietycode,circlecode from AgreementDetails where ryotcode="+ryotcode+"";
	   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
	   		if(ls3.size()>0 && ls3!=null)
	   		{
	   			villagecode=(String)((Map<Object,Object>)ls3.get(0)).get("villagecode");
	   			varietycode=(Integer)((Map<Object,Object>)ls3.get(0)).get("varietycode");
	   			circlecode=(Integer)((Map<Object,Object>)ls3.get(0)).get("circlecode");
	   		}
	   		String sql1="select ryotname,relativename from Ryot where ryotcode="+ryotcode+"";
	   		List ls1=hibernatedao.findBySqlCriteria(sql1); 
	   		if(ls1.size()>0 && ls1!=null)
	   		{
	   			String ryotname=(String)((Map<Object,Object>)ls1.get(0)).get("ryotname");
	   			String relativename=(String)((Map<Object,Object>)ls1.get(0)).get("relativename");
	   			obj.setRyotname(ryotname);
	   			obj.setRelativename(relativename);
	   		}
	   		String sql2="select village from Village where villagecode='"+villagecode+"'";
	   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
	   		if(ls2.size()>0 && ls2!=null)
	   		{
	   			String village=(String)((Map<Object,Object>)ls2.get(0)).get("village");
	   			obj.setVillage(village);
	   		}
	   		String sql4="select variety from Variety where varietycode="+varietycode+"";
	   		List ls4=hibernatedao.findBySqlCriteria(sql4); 
	   		if(ls4.size()>0 && ls4!=null)
	   		{
	   			String variety=(String)((Map<Object,Object>)ls4.get(0)).get("variety");
	   			obj.setVariety(variety);
	   		}
	   		String sql5="select circle from Circle where circlecode="+circlecode+"";
	   		List ls5=hibernatedao.findBySqlCriteria(sql5); 
	   		if(ls5.size()>0 && ls5!=null)
	   		{
	   			circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
	   		}
	   		else
	   			circle="NA";
	   		obj.setCircle(circle);
			 Session session=hibernatedao.getSessionFactory().openSession();
			 session.save(obj);
			 session.flush();
			 session.close();
			 ls3.clear();ls5.clear();ls1.clear();ls2.clear();ls4.clear();
			} 
	  		ls.clear();
		}
		try{
		String reportFileName ="CaneSampleCardReport";
		String Query1="ryotcode,ryotname,relativename,circle,variety,village,programnumber,extentsize,brix,pol,purity,ccsrating from Temp_CaneSampleCard";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
       // hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateCaneWeighmentReport", method = RequestMethod.GET)
	public void generateCaneWeighmentReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String sNum=request.getParameter("serialNumber");
		Integer serialNo=Integer.parseInt(sNum);
		String rptFormat = "PDF";//request.getParameter("status");
		String sqq="truncate table Temp_CaneWeighmentReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneWeighmentReport obj=new Temp_CaneWeighmentReport();
		String sql="select distinct * from WeighmentDetails where serialnumber='"+sNum+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		obj.setSerialnumber(serialNo);
  		
  		Integer circlecode=0;
  		String ryotcode=null;
  		String villagecode=null;
  		Integer varietycode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  		 if(ls.size()>0 && ls!=null)
		  {	       
	        ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode"); 
	       
	        villagecode=(String)((Map<Object,Object>)ls.get(0)).get("villagecode"); 
	       	circlecode=(Integer)((Map<Object,Object>)ls.get(0)).get("circlecode");
	     	varietycode=(Integer)((Map<Object,Object>)ls.get(0)).get("varietycode");
	     	String landvilcode=(String)((Map<Object,Object>)ls.get(0)).get("landvilcode");
	     	Integer shiftid=(Integer)((Map<Object,Object>)ls.get(0)).get("shiftid");
	     	Date canereceiptdate=(Date)((Map<Object,Object>)ls.get(0)).get("canereceiptdate");
	     	Timestamp canereceipttime=(Timestamp)((Map<Object,Object>)ls.get(0)).get("canereceipttime");
	     	Integer weighbridgeslno=(Integer)((Map<Object,Object>)ls.get(0)).get("weighbridgeslno");
	     	Integer vehicletype=(Integer)((Map<Object,Object>)ls.get(0)).get("vehicletype");
	     	String vehicleno=(String)((Map<Object,Object>)ls.get(0)).get("vehicleno");
	     	Double grossweight=(Double)((Map<Object,Object>)ls.get(0)).get("grossweight");
	     	
	       	obj.setRyotcode(ryotcode);	
	       	obj.setLandvilcode(landvilcode);
	       	obj.setShiftid(shiftid);
	       	obj.setCanereceiptdate(canereceiptdate);
	      	obj.setCanereceipttime(canereceipttime);
	       	obj.setWeighbridgeslno(weighbridgeslno);
	       	obj.setVehicletype(vehicletype);
	       	obj.setVehicleno(vehicleno);
	       	obj.setGrossweight(grossweight);
	     
		  }
  		String sql1="select village from Village where villagecode="+villagecode+"";
   		List ls1=hibernatedao.findBySqlCriteria(sql1); 
   		if(ls1.size()>0 && ls1!=null)
   		{
   			String village=(String)((Map<Object,Object>)ls1.get(0)).get("village");
   			obj.setVillage(village);
   		}
 		
   		String sql2="select variety from Variety where varietycode="+varietycode+"";
   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
   		if(ls2.size()>0 && ls2!=null)
   		{
   			String variety=(String)((Map<Object,Object>)ls2.get(0)).get("variety");
   			obj.setVariety(variety);
   		}
   		String sql3="select plantorratoon from WeighmentDetails where ryotcode="+ryotcode+"";
   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
   		Integer ptorrt=0;
   		if(ls3.size()>0 && ls3!=null)
   		{
   			Byte plantorratoon=(Byte)((Map<Object,Object>)ls3.get(0)).get("plantorratoon");
   			
   			 ptorrt=plantorratoon.intValue();
   			//obj.setPlantorratoon(plantorratoon);
   		}
   		String sql4="select croptype from CropTypes where croptypecode="+ptorrt+"";
   		List ls4=hibernatedao.findBySqlCriteria(sql4); 
   		if(ls4.size()>0 && ls4!=null)
   		{
   			String croptype=(String)((Map<Object,Object>)ls4.get(0)).get("croptype");
   			
   			obj.setPlantorratoon(croptype);
   		}
   		String sql5="select circle from Circle where circlecode="+circlecode+"";
   		List ls5=hibernatedao.findBySqlCriteria(sql5); 
   		if(ls5.size()>0 && ls5!=null)
   		{
   			String circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
   			
   			obj.setCircle(circle);
   		}
   		String sql6="select ryotname,relativename from Ryot where ryotcode="+ryotcode+"";
   		List ls6=hibernatedao.findBySqlCriteria(sql6); 
   		if(ls6.size()>0 && ls6!=null)
   		{
   			String ryotname=(String)((Map<Object,Object>)ls6.get(0)).get("ryotname");
   			String relativename=(String)((Map<Object,Object>)ls6.get(0)).get("relativename");
   			obj.setRyotname(ryotname);
   			obj.setRelativename(relativename);
   		}
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		 ls1.clear(); ls2.clear();ls3.clear();ls4.clear();ls5.clear();
		} 
  		ls.clear();
  		
      //  String Query1;
		//String reportFileName;
		try{
	
		String reportFileName ="CaneWeighmentReport";
		String Query1="ryotcode,grossweight,variety,circle,ryotname,relativename,plantorratoon,serialnumber,landvilcode,shiftid,canereceiptdate,weighbridgeslno,vehicletype,vehicleno,village from Temp_CaneWeighmentReport";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
       // hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	

	@RequestMapping(value = "/generateVarietyWiseCaneCrushingShiftsWise", method = RequestMethod.POST)
	public void generateVarietyWiseCaneCrushingShiftsWise(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		
		String sqq="truncate table Temp_VarityWiseCaneCrushingShiftsReport";
		//Query query=(Query) hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		//((org.hibernate.Query) query).executeUpdate();
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_VarityWiseCaneCrushingShiftsReport obj=new Temp_VarityWiseCaneCrushingShiftsReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct varietycode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer varietyCode=0;
  		
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_VarityWiseCaneCrushingShiftsReport();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				varietyCode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
  			}
  			String sql1="select distinct variety from Variety where varietycode="+varietyCode+"";
  			List ls1=hibernatedao.findBySqlCriteria(sql1); 
  			if(ls1.size()>0 && ls1!=null)
  			{
  				String variety=(String)((Map<Object,Object>)ls1.get(0)).get("variety");
  				obj.setVariety(variety);
  			}
  			String sql7="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=1 and plantorratoon='1'";
  			List ls7=hibernatedao.findBySqlCriteria(sql7); 
  			Double plantNetwt1=0.0;
  			if(ls7.size()>0 && ls7!=null)
  			{
  				plantNetwt1=(Double)((Map<Object,Object>)ls7.get(0)).get("netwt");
  			}
  			else
  				plantNetwt1=0.0;
  			obj.setPlant1(plantNetwt1);
  			String sql2="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=1 and plantorratoon in(2,3,4,5)";
  			List ls2=hibernatedao.findBySqlCriteria(sql2); 
  			Double ratoonNetwt1=0.0;
  			if(ls2.size()>0 && ls2!=null)
  				ratoonNetwt1=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
  			else
  				ratoonNetwt1=0.0;
  			obj.setRatoon1(ratoonNetwt1);
  			Double total1=plantNetwt1+ratoonNetwt1;
  			Double percentage1=0.0;
  			String sql8="select isnull(sum(netwt),1) as netwt from WeighmentDetails  where  season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=1";
  			List ls8=hibernatedao.findBySqlCriteria(sql8); 
  			if(ls8.size()>0 && ls8!=null)
  			{
  				
  				Double Gtotal1=(Double)((Map<Object,Object>)ls8.get(0)).get("netwt");
  				
  				percentage1=(total1*100)/Gtotal1;
  			}
  			
  			obj.setPercentage1(percentage1);
  			
  			
  			obj.setTotal1(total1);
  			
  			String sql3="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=2 and plantorratoon='1'";
  			List ls3=hibernatedao.findBySqlCriteria(sql3); 
  			Double plantNetwt2=0.0;
  			if(ls3.size()>0 && ls3!=null)
  			{
  				plantNetwt2=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
  			}
  			else
  				plantNetwt2=0.0;
  			obj.setPlant2(plantNetwt2);
  			String sql4="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=2 and plantorratoon in(2,3,4,5)";
  			List ls4=hibernatedao.findBySqlCriteria(sql4); 
  			Double ratoonNetwt2=0.0;
  			if(ls4.size()>0 && ls4!=null)
  			{
  				ratoonNetwt2=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt");
  			}
  			else
  				ratoonNetwt2=0.0;
  			obj.setRatoon2(ratoonNetwt2);
  			Double total2=plantNetwt2+ratoonNetwt2;
  			
  			String sql9="select isnull(sum(netwt),1) as netwt from WeighmentDetails  where  season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=2";
  			List ls9=hibernatedao.findBySqlCriteria(sql9);
  			Double percentage2=0.0;
  			if(ls9.size()>0 && ls9!=null)
  			{
  				
  				Double Gtotal2=(Double)((Map<Object,Object>)ls9.get(0)).get("netwt");
  				
  				percentage2=(total2*100)/Gtotal2;
  			}
  			
  			obj.setPercentage2(percentage2);
  			
  			obj.setTotal2(total2);
  			
  			
  			String sql5="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=3 and plantorratoon='1'";
  			List ls5=hibernatedao.findBySqlCriteria(sql5); 
  			Double plantNetwt3=0.0;
  			if(ls5.size()>0 && ls5!=null)
  			{
  				plantNetwt3=(Double)((Map<Object,Object>)ls5.get(0)).get("netwt");
  			}
  			else
  				plantNetwt3=0.0;
  			obj.setPlant3(plantNetwt3);
  			String sql6="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=3 and plantorratoon in(2,3,4,5)";
  			List ls6=hibernatedao.findBySqlCriteria(sql6); 
  			Double ratoonNetwt3=0.0;
  			if(ls6.size()>0 && ls6!=null)
  			{
  				ratoonNetwt3=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
  			}
  			else
  				ratoonNetwt3=0.0;
  			obj.setRatoon3(ratoonNetwt3);
  			Double total3=plantNetwt3+ratoonNetwt3;
  			
  			Double percentage3=0.0;
  			
  			String sql10="select isnull(sum(netwt),1) as netwt from WeighmentDetails  where  season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and shiftid1=3";
  			List ls10=hibernatedao.findBySqlCriteria(sql10);
  			if(ls10.size()>0 && ls10!=null)
  			{
  				
  				Double Gtotal3=(Double)((Map<Object,Object>)ls10.get(0)).get("netwt");
  				
  				percentage3=(total3*100)/Gtotal3;
  			}
  			
  			obj.setPercentage3(percentage3);
  			
  			
  			obj.setTotal3(total3);
  			session.save(obj);
  			
		} 
  		
  		session.flush();
  		session.close();
		try
		{
			String reportFileName ="VarietyWiseCaneCrushingShiftsWiseReport";
			String Query1="percentage1,percentage2,percentage3, variety,plant1,ratoon1,total1,plant2,ratoon2,total2,plant3,ratoon3,total3 from Temp_VarityWiseCaneCrushingShiftsReport";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVarietyWiseCaneCrushingReportDatewise", method = RequestMethod.POST)
	public void generateVarietyWiseCaneCrushingReportDatewise(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();

		String sqq="truncate table Temp_VarietyWiseCaneCrushingDatewise";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_VarietyWiseCaneCrushingDatewise obj=new Temp_VarietyWiseCaneCrushingDatewise();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct varietycode from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer varietyCode=0;
  		
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_VarietyWiseCaneCrushingDatewise();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				varietyCode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
  			}
  			String sql1="select variety from Variety where varietycode="+varietyCode+"";
  			List ls1=hibernatedao.findBySqlCriteria(sql1); 
  			if(ls1.size()>0 && ls1!=null)
  			{
  				String variety=(String)((Map<Object,Object>)ls1.get(0)).get("variety");
  				obj.setVariety(variety);
  			}
  			String sql7="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and plantorratoon='1'";
  			List ls7=hibernatedao.findBySqlCriteria(sql7); 
  			Double plantNetwt1=0.0;
  			if(ls7.size()>0 && ls7!=null)
  			{
  				plantNetwt1=(Double)((Map<Object,Object>)ls7.get(0)).get("netwt");
  			}
  			else
  				plantNetwt1=0.0;
  			obj.setPlant1(plantNetwt1);
   		
  			String sql2="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and plantorratoon in(2,3,4,5)";
  			List ls2=hibernatedao.findBySqlCriteria(sql2); 
  			Double ratoonNetwt1=0.0;
  			if(ls2.size()>0 && ls2!=null)
  				ratoonNetwt1=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
  			else
  				ratoonNetwt1=0.0;
  			obj.setRatoon1(ratoonNetwt1);
  			Double total1=plantNetwt1+ratoonNetwt1;
  			obj.setTotal1(total1);
  			

  			String sql3="select isnull(sum(netwt),1) as netwt from WeighmentDetails  where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
  			List ls3=hibernatedao.findBySqlCriteria(sql3); 
  			Double percentage=0.0;
  			if(ls3.size()>0 && ls3!=null)
  			{
  				Double gtotal=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
  				percentage=(total1*100)/gtotal;
  			}
  			obj.setPercentage(percentage);
  			session.save(obj);
  			//ls1.clear();ls2.clear();ls7.clear();
			} 
  		//ls.clear();
  		session.flush();
  		session.close();
  		try
		{
	
			String reportFileName ="VarietyWiseCaneCrushingReportDatewise";
			String Query1="variety,plant1,ratoon1, total1,percentage from Temp_VarietyWiseCaneCrushingDatewise";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate",fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVarietyWiseCaneCrushingCumulative", method = RequestMethod.POST)
	public void generateVarietyWiseCaneCrushingCumulative(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String sqq="truncate table Temp_VarietyWiseCaneCrushingCumulative";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_VarietyWiseCaneCrushingCumulative obj=new Temp_VarietyWiseCaneCrushingCumulative();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct varietycode from WeighmentDetails where season='"+season1[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer varietyCode=0;
  		for (int k = 0; k <ls.size(); k++) 
  		{
  			obj=new Temp_VarietyWiseCaneCrushingCumulative();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				varietyCode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
  			}
  			String sql1="select variety from Variety where varietycode="+varietyCode+"";
  			List ls1=hibernatedao.findBySqlCriteria(sql1); 
   			if(ls1.size()>0 && ls1!=null)
   			{
   				String variety=(String)((Map<Object,Object>)ls1.get(0)).get("variety");
   				obj.setVariety(variety);
   			}
   			String sql7="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and plantorratoon='1'";
   			List ls7=hibernatedao.findBySqlCriteria(sql7); 
   			Double plantNetwt1=0.0;
   			if(ls7.size()>0 && ls7!=null)
   			{
   				plantNetwt1=(Double)((Map<Object,Object>)ls7.get(0)).get("netwt");
   			}
   			obj.setPlant1(plantNetwt1);
   			String sql2="select isnull(sum(netwt),0) as netwt from WeighmentDetails  where varietycode="+varietyCode+" and season='"+season1[1]+"' and plantorratoon in(2,3,4,5)";
   			List ls2=hibernatedao.findBySqlCriteria(sql2); 
   			Double ratoonNetwt1=0.0;
   			if(ls2.size()>0 && ls2!=null)
   			{	
   				ratoonNetwt1=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
   			}
   			obj.setRatoon1(ratoonNetwt1);
   			Double total1=plantNetwt1+ratoonNetwt1;
   			obj.setTotal1(total1);
   			String sql3="select isnull(sum(netwt),1) as netwt from WeighmentDetails  where season='"+season1[1]+"'";
   			List ls3=hibernatedao.findBySqlCriteria(sql3); 
   			Double percentage=0.0;
   			if(ls3.size()>0 && ls3!=null)
   			{	
   				Double gtotal=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
   				percentage=(total1*100)/gtotal;
   			}
   			obj.setPercentage(percentage);
   			session.save(obj);
  		} 
  		session.flush();
  		session.close();
  		try
  		{
			String reportFileName ="VarietyWiseCaneCrushingCumulativeReport";
			String Query1="variety,plant1,ratoon1,total1,percentage from Temp_VarietyWiseCaneCrushingCumulative";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", dateFormat.format(date));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVehicleTypeWiseCaneCrushingShiftsWise", method = RequestMethod.POST)
	public void generateVehicleTypeWiseCaneCrushingShiftsWise(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_VehicleTypeWiseCaneCrushingShiftsWise";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Integer vehicleType=null;
		Integer zonecode=null;
		String zone=null;
		Temp_VehicleTypeWiseCaneCrushingShiftsWise obj=new Temp_VehicleTypeWiseCaneCrushingShiftsWise();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql5="select distinct w.zonecode,z.zone from WeighmentDetails w,zone z where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and w.zonecode=z.zonecode  order by w.zonecode";
  		List ls5=hibernatedao.findBySqlCriteria(sql5);
  		for (int i = 0; i <ls5.size(); i++) 
		{
  			if(ls5.size()>0 && ls5!=null)
  			{	       
  				zonecode=(Integer)((Map<Object,Object>)ls5.get(i)).get("zonecode");
  				zone=(String)((Map<Object,Object>)ls5.get(i)).get("zone");
  			}
		
		String sql="select distinct lorryortruck from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and zonecode="+zonecode+"";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_VehicleTypeWiseCaneCrushingShiftsWise();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				vehicleType=(Integer)((Map<Object,Object>)ls.get(k)).get("lorryortruck");
  			}
  			String sql1="select distinct vehicletype from VehicleTypes where vehicletypecode="+vehicleType+"";
  			List ls1=hibernatedao.findBySqlCriteria(sql1); 
  			if(ls1.size()>0 && ls1!=null)
  			{
  				String vehicletype=(String)((Map<Object,Object>)ls1.get(0)).get("vehicletype");
	   			obj.setVehicletype(vehicletype);
  			} 
  			obj.setZone(zone);
  			obj.setZonecode(zonecode);
	  		
	  		String sql7="select isnull(sum(netwt),0)as netwt,count(Vehicletype) as vcount from WeighmentDetails where season='"+season1[1]+"' and  lorryortruck="+vehicleType+"  and shiftid1=1 and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and zonecode="+zonecode+"";
	   		List ls7=hibernatedao.findBySqlCriteria(sql7); 
	   		Double netwt1=0.0;
	   		Integer vcount=0;
	   		if(ls7.size()>0 && ls7!=null)
	   		{
	   			netwt1=(Double)((Map<Object,Object>)ls7.get(0)).get("netwt");
	   			vcount=(Integer)((Map<Object,Object>)ls7.get(0)).get("vcount");
	   			
	   		}
	   		else
	   		{
	   			netwt1=0.0;
	   			vcount=0;
	   		}
	   		obj.setNetwt1(netwt1);
	   		obj.setNooftrips1(vcount);
	   		String sql2="select isnull(sum(netwt),0)as netwt,count(Vehicletype) as vcount from WeighmentDetails where season='"+season1[1]+"' and  lorryortruck="+vehicleType+"  and shiftid1=2 and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and zonecode="+zonecode+"";
	   		List ls2=hibernatedao.findBySqlCriteria(sql2); 
	   		
	   		if(ls2.size()>0 && ls2!=null)
	   		{
	   			netwt1=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
	   			vcount=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");
	   			
	   		}
	   		else
	   		{
	   			netwt1=0.0;
	   			vcount=0;
	   		}
	   		obj.setNetwt2(netwt1);
	   		obj.setNooftrips2(vcount);
	   		String sql3="select isnull(sum(netwt),0)as netwt,count(Vehicletype) as vcount from WeighmentDetails where season='"+season1[1]+"' and  lorryortruck="+vehicleType+"  and shiftid1=3 and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and zonecode="+zonecode+"";
	   		List ls3=hibernatedao.findBySqlCriteria(sql3); 
	   		
	   		if(ls3.size()>0 && ls3!=null)
	   		{
	   			netwt1=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
	   			vcount=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");
	   			
	   		}
	   		else
	   		{
	   			netwt1=0.0;
	   			vcount=0;
	   		}
	   		obj.setNetwt3(netwt1);
	   		obj.setNooftrips3(vcount);
	   		session.save(obj);
		}
		}
  		session.flush();
  		session.close();
  		//ls.clear();
		try
		{
	
		    String reportFileName ="VehicleTypeWiseCaneCrushingShiftsWiseReport";
			String Query1=" zone,zonecode,vehicletype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingShiftsWise";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendates", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVehicleTypeWiseCaneCrushingCumulative", method = RequestMethod.POST)
	public void generateVehicleTypeWiseCaneCrushingCumulative(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{	
		String rptFormat = request.getParameter("status");	
		String season=request.getParameter("season");
		String group=request.getParameter("GROWER");
		String group1[]=group.split(":");
		String season1[]=season.split(":");

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();

	    String sqq="truncate table Temp_VehicleTypeWiseCaneCrushingCumulative";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_VehicleTypeWiseCaneCrushingCumulative obj=new Temp_VehicleTypeWiseCaneCrushingCumulative();
	    String Query1=null;
		String reportFileName=null;
		String sql;
		List ls;
		if("Circle".equals(group1[1]))
		{
		  sql="select distinct circlecode from WeighmentDetails where season='"+season1[1]+"'";
		  try
		  {
		  ls=hibernatedao.findBySqlCriteria(sql); 
		  for(int k = 0; k <ls.size(); k++) 
		  {
			  Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode"); 
				  
			  String sql4="select circle from Circle where circlecode="+circlecode+"";
			  List ls4=hibernatedao.findBySqlCriteria(sql4);	 
			  if(ls4.size()>0 && ls4!=null)
			  {	
				  String circle=(String)((Map<Object,Object>)ls4.get(0)).get("circle"); 
				  obj.setNametype(circle);
			  } 
			  String sql1="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where circlecode="+circlecode+" and season='"+season1[1]+"' and lorryortruck=0";
			  List ls1=hibernatedao.findBySqlCriteria(sql1);
			  Double  netwt1=0.0;
			  Integer  nooftrips1=0;
			  if(ls1.size()>0 && ls1!=null)
			  {	
			  	  netwt1=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt"); 
			  	  nooftrips1=(Integer)((Map<Object,Object>)ls1.get(0)).get("vcount"); 
			  }
			  else
			  {
				  netwt1=0.0;
				  nooftrips1=0;
			  }
			  obj.setNetwt1(netwt1);
			  obj.setNooftrips1(nooftrips1);
			  				  
			  String sql2="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where circlecode="+circlecode+" and season='"+season1[1]+"' and lorryortruck=1";
			  List ls2=hibernatedao.findBySqlCriteria(sql2);
			  Double  netwt2=0.0;
			  Integer  nooftrips2=0;
			  if(ls2.size()>0 && ls2!=null)
			  {	
				  netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
			  	  nooftrips2=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");  
			  }		  		
			  else
			  {
				  netwt2=0.0;
				  nooftrips2=0;
			  }
			  obj.setNetwt2(netwt2);
			  obj.setNooftrips2(nooftrips2);
			  String sql3="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where circlecode="+circlecode+" and season='"+season1[1]+"' and lorryortruck=2";
			  List ls3=hibernatedao.findBySqlCriteria(sql3);
			  Double  netwt3=0.0;
			  Integer  nooftrips3=0;
			  if(ls3.size()>0 && ls3!=null)
			  {	
				  netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
			  	  nooftrips3=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");  
			  }		  		
			  else
			  {
				  netwt3=0.0;
				  nooftrips3=0;
			  }
			  obj.setNetwt3(netwt3);
			  obj.setNooftrips3(nooftrips3);
			
					 
			  Session session=hibernatedao.getSessionFactory().openSession();
			  session.save(obj);
			  session.flush();
			  session.close();
			  ls4.clear(); ls1.clear();ls2.clear();ls3.clear();
		  }
				
		  ls.clear();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "VehicleTypeCaneCrushingCumulativeCircleReport";
		  Query1="distinct nametype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingCumulative ";
		}
		else if("Village".equals(group1[1]))
		{
		 sql="select distinct villagecode from WeighmentDetails where season='"+season1[1]+"'";
		 try
		 {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
			String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode"); 
			
			String sql1="select village from Village where villagecode='"+villagecode+"'";
			List ls1=hibernatedao.findBySqlCriteria(sql1);
			if(ls1.size()>0 && ls1!=null)
			{	
				String village=(String)((Map<Object,Object>)ls1.get(0)).get("village"); 
				obj.setNametype(village); 		
			}
			String sql4="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where villagecode='"+villagecode+"' and season='"+season1[1]+"' and lorryortruck=0";
			List ls4=hibernatedao.findBySqlCriteria(sql4);
			Double  netwt1=0.0;
			Integer  nooftrips1=0;
			if(ls4.size()>0 && ls4!=null)
			{	
				netwt1=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt"); 
				nooftrips1=(Integer)((Map<Object,Object>)ls4.get(0)).get("vcount"); 
			}
			else
			{
				netwt1=0.0;
				nooftrips1=0;
			}
			obj.setNetwt1(netwt1);
			obj.setNooftrips1(nooftrips1);
			  				  
			String sql2="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where villagecode='"+villagecode+"' and season='"+season1[1]+"' and lorryortruck=1";
			List ls2=hibernatedao.findBySqlCriteria(sql2);
			Double  netwt2=0.0;
			Integer  nooftrips2=0;
			if(ls2.size()>0 && ls2!=null)
			{	
				netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
				nooftrips2=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");  
			}		  		
			else
			{
				netwt2=0.0;
				nooftrips2=0;
			}
			obj.setNetwt2(netwt2);
			obj.setNooftrips2(nooftrips2);
			String sql3="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where villagecode='"+villagecode+"' and season='"+season1[1]+"' and lorryortruck=2";
			List ls3=hibernatedao.findBySqlCriteria(sql3);
			Double  netwt3=0.0;
			Integer  nooftrips3=0;
			if(ls3.size()>0 && ls3!=null)
			{	
				netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
				nooftrips3=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");  
			}		  		
			else
			{
				netwt3=0.0;
				nooftrips3=0;
			}
			obj.setNetwt3(netwt3);
			obj.setNooftrips3(nooftrips3);
		// For save	
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		 ls1.clear(); ls2.clear(); ls3.clear(); ls4.clear();
		} 
		ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		reportFileName = "VehicleTypeCaneCrushingCumulativeVillageReport";
		Query1="distinct nametype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingCumulative ";
		}
		else if("Zone".equals(group1[1]))
		{
		 sql="select distinct zonecode from WeighmentDetails where season='"+season1[1]+"'";
		 try
		 {
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
			Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode"); 
			
			String sql1="select zone from Zone where zonecode="+zonecode+"";
			List ls1=hibernatedao.findBySqlCriteria(sql1);
			if(ls1.size()>0 && ls1!=null)
			{	
				String zone=(String)((Map<Object,Object>)ls1.get(0)).get("zone"); 
				obj.setNametype(zone); 		
			}
			String sql4="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where zonecode="+zonecode+" and season='"+season1[1]+"' and lorryortruck=0";
			List ls4=hibernatedao.findBySqlCriteria(sql4);
			Double  netwt1=0.0;
			Integer  nooftrips1=0;
			if(ls4.size()>0 && ls4!=null)
			{	
				netwt1=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt"); 
				nooftrips1=(Integer)((Map<Object,Object>)ls4.get(0)).get("vcount"); 
			}
			else
			{
				netwt1=0.0;
				nooftrips1=0;
			}
			obj.setNetwt1(netwt1);
			obj.setNooftrips1(nooftrips1);
			  				  
			String sql2="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where zonecode="+zonecode+" and season='"+season1[1]+"' and lorryortruck=1";
			List ls2=hibernatedao.findBySqlCriteria(sql2);
			Double  netwt2=0.0;
			Integer  nooftrips2=0;
			if(ls2.size()>0 && ls2!=null)
			{	
				netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
				nooftrips2=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");  
			}		  		
			else
			{
				netwt2=0.0;
				nooftrips2=0;
			}
			obj.setNetwt2(netwt2);
			obj.setNooftrips2(nooftrips2);
			String sql3="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where zonecode="+zonecode+" and season='"+season1[1]+"' and lorryortruck=2";
			List ls3=hibernatedao.findBySqlCriteria(sql3);
			Double  netwt3=0.0;
			Integer  nooftrips3=0;
			if(ls3.size()>0 && ls3!=null)
			{	
				netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
				nooftrips3=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");  
			}		  		
			else
			{
				netwt3=0.0;
				nooftrips3=0;
			}
			obj.setNetwt3(netwt3);
			obj.setNooftrips3(nooftrips3);
		// For save	
		 Session session=hibernatedao.getSessionFactory().openSession();
		 session.save(obj);
		 session.flush();
		 session.close();
		 ls1.clear(); ls2.clear(); ls3.clear(); ls4.clear();
		} 
		ls.clear();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		reportFileName = "VehicleTypeCaneCrushingCumulativeZoneReport";
		Query1="distinct nametype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingCumulative ";
		}
		else if("Region".equals(group1[1]))
		{
			 sql="select distinct regioncode from WeighmentDetails where season='"+season1[1]+"'";
			 try
			 {
				ls=hibernatedao.findBySqlCriteria(sql); 
				for(int k = 0; k <ls.size(); k++) 
				{
				Integer regioncode=(Integer)((Map<Object,Object>)ls.get(k)).get("regioncode"); 
				
				String sql1="select region from Region where regioncode="+regioncode+"";
				List ls1=hibernatedao.findBySqlCriteria(sql1);
				if(ls1.size()>0 && ls1!=null)
				{	
					String region=(String)((Map<Object,Object>)ls1.get(0)).get("region"); 
					obj.setNametype(region); 		
				}
				String sql4="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where regioncode="+regioncode+" and season='"+season1[1]+"' and lorryortruck=0";
				List ls4=hibernatedao.findBySqlCriteria(sql4);
				Double  netwt1=0.0;
				Integer  nooftrips1=0;
				if(ls4.size()>0 && ls4!=null)
				{	
					netwt1=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt"); 
					nooftrips1=(Integer)((Map<Object,Object>)ls4.get(0)).get("vcount"); 
				}
				else
				{
					netwt1=0.0;
					nooftrips1=0;
				}
				obj.setNetwt1(netwt1);
				obj.setNooftrips1(nooftrips1);
				  				  
				String sql2="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where regioncode="+regioncode+" and season='"+season1[1]+"' and lorryortruck=1";
				List ls2=hibernatedao.findBySqlCriteria(sql2);
				Double  netwt2=0.0;
				Integer  nooftrips2=0;
				if(ls2.size()>0 && ls2!=null)
				{	
					netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					nooftrips2=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");  
				}		  		
				else
				{
					netwt2=0.0;
					nooftrips2=0;
				}
				obj.setNetwt2(netwt2);
				obj.setNooftrips2(nooftrips2);
				String sql3="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where regioncode="+regioncode+" and season='"+season1[1]+"' and lorryortruck=2";
				List ls3=hibernatedao.findBySqlCriteria(sql3);
				Double  netwt3=0.0;
				Integer  nooftrips3=0;
				if(ls3.size()>0 && ls3!=null)
				{	
					netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					nooftrips3=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");  
				}		  		
				else
				{
					netwt3=0.0;
					nooftrips3=0;
				}
				obj.setNetwt3(netwt3);
				obj.setNooftrips3(nooftrips3);
			// For save	
				Session session=hibernatedao.getSessionFactory().openSession();
				session.save(obj);
				session.flush();
				session.close();
				ls1.clear(); ls2.clear(); ls3.clear(); ls4.clear();
				} 
				ls.clear();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
		reportFileName = "VehicleTypeCaneCrushingCumulativeRegionReport";
		Query1="distinct nametype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingCumulative ";
		}
		else if("Mandal".equals(group1[1]))
		{
		  sql="select distinct mandalcode from WeighmentDetails where season='"+season1[1]+"'";
		  try
		  {
			  ls=hibernatedao.findBySqlCriteria(sql); 
			  for(int k = 0; k <ls.size(); k++) 
			  {
				  Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode"); 
				
				  String sql1="select mandal from Mandal where mandalcode="+mandalcode+"";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  String mandal=null;
				  if(ls1.size()>0 && ls1!=null)
				  {	
					   mandal=(String)((Map<Object,Object>)ls1.get(0)).get("mandal"); 
				  }
				  else 
				  {
					  mandal="NA";
				  }
				  obj.setNametype(mandal); 	
				  String sql4="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where mandalcode="+mandalcode+" and season='"+season1[1]+"' and lorryortruck=0";
				  List ls4=hibernatedao.findBySqlCriteria(sql4);
				  Double  netwt1=0.0;
				  Integer  nooftrips1=0;
				  if(ls4.size()>0 && ls4!=null)
				  {	
					  netwt1=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt"); 
					  nooftrips1=(Integer)((Map<Object,Object>)ls4.get(0)).get("vcount"); 
				  }
				  else
				  {
					  netwt1=0.0;
					  nooftrips1=0;
				  }
				  obj.setNetwt1(netwt1);
				  obj.setNooftrips1(nooftrips1);
				  				  
				  String sql2="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where mandalcode="+mandalcode+" and season='"+season1[1]+"' and lorryortruck=1";
				  List ls2=hibernatedao.findBySqlCriteria(sql2);
				  Double  netwt2=0.0;
				  Integer  nooftrips2=0;
				  if(ls2.size()>0 && ls2!=null)
				  {	
					  netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt"); 
					  nooftrips2=(Integer)((Map<Object,Object>)ls2.get(0)).get("vcount");  
				  }		  		
				  else
				  {
					  netwt2=0.0;
					  nooftrips2=0;
				  }
				  obj.setNetwt2(netwt2);
				  obj.setNooftrips2(nooftrips2);
				  String sql3="select isnull(sum(netwt),0)as netwt,count(lorryortruck) as vcount  from WeighmentDetails where mandalcode="+mandalcode+" and season='"+season1[1]+"' and lorryortruck=2";
				  List ls3=hibernatedao.findBySqlCriteria(sql3);
				  Double  netwt3=0.0;
				  Integer  nooftrips3=0;
				  if(ls3.size()>0 && ls3!=null)
				  {	
					  netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt"); 
					  nooftrips3=(Integer)((Map<Object,Object>)ls3.get(0)).get("vcount");  
				  }		  		
				  else
				  {
					  netwt3=0.0;
					  nooftrips3=0;
				  }
				  obj.setNetwt3(netwt3);
				  obj.setNooftrips3(nooftrips3);
				  // For save	
				  Session session=hibernatedao.getSessionFactory().openSession();
				  session.save(obj);
				  session.flush();
				  session.close();
				  ls1.clear(); ls2.clear(); ls3.clear(); ls4.clear();
					} 
			  ls.clear();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "VehicleTypeCaneCrushingCumulativeMandalReport";
		  Query1="distinct nametype,netwt1,netwt2,netwt3,nooftrips1,nooftrips2,nooftrips3,(netwt1+netwt2+netwt3) as totnetwt,(nooftrips1+nooftrips2+nooftrips3) as tottrips from Temp_VehicleTypeWiseCaneCrushingCumulative ";
		}
		HashMap<String,Object> hmParams=new HashMap<String,Object>();
		hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
		hmParams.put("qrs", Query1);
		hmParams.put("cdate", dateFormat.format(date));
		generateReport(request,response,hmParams,reportFileName,rptFormat);
	}
	
	@RequestMapping(value = "/generateDailyLogSheet", method = RequestMethod.POST)
	public void generateDailyLogSheet(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_DailyLogSheet";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_DailyLogSheet obj=new Temp_DailyLogSheet();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct weighbridgeslno from WeighmentDetails where canereceiptenddate between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer weighbridgeslno=0;
  		Integer shiftid=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			if(ls.size()>0 && ls!=null)
  			{	       
  				weighbridgeslno=(Integer)((Map<Object,Object>)ls.get(k)).get("weighbridgeslno");
  			}	
  			String sql5="select distinct shiftid1 from WeighmentDetails where weighbridgeslno="+weighbridgeslno+" and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'";
  			List ls5=hibernatedao.findBySqlCriteria(sql5);
  			for (int i = 0; i<ls5.size(); i++) 
  			{
  				if(ls5.size()>0 && ls5!=null)
  				{
  					shiftid=(Integer)((Map<Object,Object>)ls5.get(i)).get("shiftid1");
  				}
  				String sql1="select distinct actualserialnumber,bindingmtlwght,grossweight,netwt,ryotcode,ryotname,varietycode,lorryortruck,plantorratoon,canereceiptenddate from WeighmentDetails where shiftid1="+shiftid+" and weighbridgeslno="+weighbridgeslno+" and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and actualserialnumber !=0 and season='"+season1[1]+"'";
  				List ls1=hibernatedao.findBySqlCriteria(sql1); 
  				Integer varietycode=0;
  				Integer vehicletype=0;
  				Integer ptorrt=0;
  				if(ls1.size()>0 && ls1!=null)
  				{
  					for(int j = 0; j<ls1.size(); j++)
  					{
  						obj=new Temp_DailyLogSheet();
  						obj.setBridgeno(weighbridgeslno);
  						obj.setShiftid(shiftid);
	  					Integer serialnumber=(Integer)((Map<Object,Object>)ls1.get(j)).get("actualserialnumber");
	  					//Double bindingmtlwght=(Double)((Map<Object,Object>)ls1.get(j)).get("bindingmtlwght");
	  					Double grossweight=(Double)((Map<Object,Object>)ls1.get(j)).get("grossweight");
	  					Double netwt=(Double)((Map<Object,Object>)ls1.get(j)).get("netwt");
	  					Double bindingmtlwght = grossweight - netwt; 
	  					bindingmtlwght =Double.parseDouble(new DecimalFormat("##.##").format(bindingmtlwght));
	  					String ryotcode=(String)((Map<Object,Object>)ls1.get(j)).get("ryotcode");
	  					String ryotname=(String)((Map<Object,Object>)ls1.get(j)).get("ryotname");
	  					varietycode=(Integer)((Map<Object,Object>)ls1.get(j)).get("varietycode");
	  					vehicletype=(Integer)((Map<Object,Object>)ls1.get(j)).get("lorryortruck");
	  					Byte plantorratoon=(Byte)((Map<Object,Object>)ls1.get(j)).get("plantorratoon");
	  					Date shitfdate=(Date)((Map<Object,Object>)ls1.get(j)).get("canereceiptenddate");
	  					String sdate=format.format(shitfdate);
	  					obj.setShiftdate(sdate);
	  					ptorrt=plantorratoon.intValue();
	  					obj.setReceiptno(serialnumber);
	  					obj.setBindingmtlwght(bindingmtlwght);
	  					obj.setGrossweight(grossweight);
	  					obj.setNetwt(netwt);
	  					obj.setRyotcode(ryotcode);
	  					obj.setRyotname(ryotname);
	  				
		  				String sql2="select croptype from CropTypes where croptypecode="+ptorrt+"";
		  				List ls2=hibernatedao.findBySqlCriteria(sql2); 
		  				if(ls2.size()>0 && ls2!=null)
		  				{
		  					String croptype=(String)((Map<Object,Object>)ls2.get(0)).get("croptype");
		  					obj.setPlantorratoon(croptype);
		  				}
		  				String sql3="select vehicletype from VehicleTypes where vehicletypecode="+vehicletype+"";
		  				List ls3=hibernatedao.findBySqlCriteria(sql3); 
		  				if(ls3.size()>0 && ls3!=null)
		  				{
		  					String vehicleType=(String)((Map<Object,Object>)ls3.get(0)).get("vehicletype");
		  					obj.setVehicletype(vehicleType);
		  				} 
		  				String sql4="select variety from Variety where varietycode="+varietycode+"";
		  				List ls4=hibernatedao.findBySqlCriteria(sql4); 
		  				if(ls4.size()>0 && ls4!=null)
		  				{
		  					String variety=(String)((Map<Object,Object>)ls4.get(0)).get("variety");
		  					obj.setVariety(variety);
		  				} 
		  				session.save(obj);
  					}
  				}
  			}
		}
  		session.flush();
  		session.close();
		try
		{
			String reportFileName ="DailyLogSheetReport";
			String Query1=" shiftdate,receiptno,ryotcode,ryotname,plantorratoon,variety,vehicletype,grossweight,bindingmtlwght,netwt,shiftid,bridgeno from Temp_DailyLogSheet order by bridgeno,shiftid";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendates", fromDate+" To "+toDate);
	        
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateGrowerWiseHourlyCrushing", method = RequestMethod.POST)
	public void generateGrowerWiseHourlyCrushing(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String fromDate=request.getParameter("Date");
		String fromTime=request.getParameter("fromTime");
		String toTime=request.getParameter("toTime");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Time fTime=DateUtils.getSqlTimeFromString(fromTime);
		Time tTime=DateUtils.getSqlTimeFromString(toTime);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_GrowerWiseHourlyCrushing";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_GrowerWiseHourlyCrushing obj=null;new Temp_GrowerWiseHourlyCrushing();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct w.actualserialnumber,w.weighbridgeslno,w.ryotcode,w.ryotname,w.netwt,w.canereceiptendtime,v.village,vi.variety from WeighmentDetails w,village v,variety vi where w.season='"+season1[1]+"' and w.canereceiptenddate='"+fromdate+"' and w.time between'"+fromTime+"' and '"+toTime+"' and w.villagecode=v.villagecode and w.varietycode=vi.varietycode and w.actualserialnumber !=0";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer varietycode=0;
  		String villagecode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_GrowerWiseHourlyCrushing();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer serialnumber=(Integer)((Map<Object,Object>)ls.get(k)).get("actualserialnumber");
  				Integer weighbridgeslno=(Integer)((Map<Object,Object>)ls.get(k)).get("weighbridgeslno");
  				varietycode=(Integer)((Map<Object,Object>)ls.get(k)).get("varietycode");
  				villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Timestamp canereceiptendtime=(Timestamp)((Map<Object,Object>)ls.get(k)).get("canereceiptendtime");
  				String caneDate=String.valueOf(canereceiptendtime);
  				String j=caneDate.replace(" ","/");
  				String[] cTime=j.split("/");
  				int k1=cTime.length;
  				ArrayList<String> al=new ArrayList<String>();
  				for(int i=0;i<cTime.length;i++)
  				{
  					al.add(cTime[i]);
  				}
  				String p=(String)al.get(1);
  				String ct=p.substring(0, 5);
  				/*String t=ct.substring(0,2);
  			    int time=Integer.parseInt(t);
  			   	String type=null;
  			    if(1 <=time &&  time <12)
  			    {
  			    	type="AM";
  			    }
  			    
  			    else
  			    {
  			    	type="PM";
  			    }
  			    obj.setType(type);*/
  				Double grossweight=(Double)((Map<Object,Object>)ls.get(k)).get("netwt");
  				obj.setReceiptno(serialnumber);
  				obj.setBridgeno(weighbridgeslno);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setBridgetime(ct);
  				obj.setGrossweight(grossweight);
  			
  				String variety=(String)((Map<Object,Object>)ls.get(k)).get("variety");
  				obj.setVariety(variety);
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				obj.setVillage(village);
  			}
  			session.save(obj);
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="GrowerWiseHourlyCrushingReport";
  			String Query1="distinct receiptno,ryotcode,ryotname,variety,grossweight,bridgeno,village,bridgetime from Temp_GrowerWiseHourlyCrushing order by receiptno";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateTransportDeductionsCheckList", method = RequestMethod.POST)
	public void generateTransportDeductionsCheckList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_TransportDeductionsCheckList";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_TransportDeductionsCheckList obj=new Temp_TransportDeductionsCheckList();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct t.transportcontcode,t.ryotcode,t.slno,t.rate,tc.transporter,r.ryotname from TransportingRateDetails t,TransportingContractors tc,ryot r where t.season='"+season1[1]+"' and t.effectivedate between '"+fromdate+"' and '"+todate+"' and t.transportcontcode=tc.transportercode and t.ryotcode=r.ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer transportcontCode=0;
  		String ryotCode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_TransportDeductionsCheckList();
  			if(ls.size()>0 && ls!=null) 
  			{	   
  				transportcontCode=(Integer)((Map<Object,Object>)ls.get(k)).get("transportcontcode");
  				ryotCode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				Double slNo=(Double)((Map<Object,Object>)ls.get(k)).get("slno");
  				Double rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				String transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				obj.setRyotcode(ryotCode);
  				obj.setSlno(slNo);
  				obj.setRate(rate);
  				obj.setTransporter(transporter);
  				obj.setRyotname(ryotname);
  			}
  			session.save(obj);
		}
  		session.flush();
  		session.close();
  		ls.clear();
		try
		{
			String reportFileName ="TransportDeductionsCheckListReport";
			String Query1="distinct ryotcode,ryotname,transporter,rate,slno from Temp_TransportDeductionsCheckList";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateTransportDeductionsForCane", method = RequestMethod.POST)
	public void generateTransportDeductionsForCane(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String caneno=request.getParameter("ftDate");
		String cslnum[]=caneno.split(":");
		String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		Date datefrom=null;
		Date dateto=null;
		String ftdate=null;
		if(ls2.size()>0 && ls2!=null)
		{
			datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
		}
		String sqq="truncate table Temp_TransportDeductionsForCane";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_TransportDeductionsForCane obj=new Temp_TransportDeductionsForCane();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql="select distinct t.harvestcontcode,t.ryotcode,td.rate,tc.transporter,r.ryotname,td.slno from DedDetails t,TransportingContractors tc,TransportingRateDetails td,ryot r where   DedCode=3 and t.season='"+season1[1]+"' and t.caneacslno="+cslnum[1]+" and t.harvestcontcode !=0 and t.harvestcontcode=tc.transportercode and t.ryotcode=r.ryotcode and t.ryotcode= td.ryotcode and t.harvestcontcode=td.transportcontcode and t.deductionamt>0 order by harvestcontcode,ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		//Integer transportcontCode=0;
  		String ryotCode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_TransportDeductionsForCane();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				Integer transportcontCode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				ryotCode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				Double rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				//Double transportedcanewt=(Double)((Map<Object,Object>)ls.get(k)).get("transportedcanewt");
  				//Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamount");
  				String transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double slno=(Double)((Map<Object,Object>)ls.get(k)).get("slno");
  				obj.setRyotcode(ryotCode);
  				obj.setRate(rate);
  				obj.setTransportcontCode(transportcontCode);
  			//	obj.setTotalamount(totalamount);
  			//	obj.setTransportedcanewt(transportedcanewt);
  				obj.setTransporter(transporter);
  				obj.setRyotname(ryotname);
  				obj.setSnumber(slno);
  			}
  			String sql1="select sum(suppliedcaneWt) harvestedcanewt,sum(deductionamt) totalamount  from DedDetails where DedCode=3 and season='"+season1[1]+"' and caneacslno='"+cslnum[1]+"' and ryotcode='"+ryotCode+"'";
  	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		if(ls1.size()>0 && ls1!=null)
			{
  	  			Double PendingAmount=(Double)((Map<Object,Object>)ls1.get(0)).get("totalamount");
  	  			Double qty=(Double)((Map<Object,Object>)ls1.get(0)).get("harvestedcanewt");
  				obj.setTotalamount(PendingAmount);
  				
  				obj.setTransportedcanewt(qty);
			}
  	  		String sql3="select sum(transport) transport from CanAccSBDetails where season='"+season1[1]+"' and caneacctslno="+cslnum[1]+" and ryotcode='"+ryotCode+"'";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && ls3!=null)
	  		{
	  			Double PendingAmount=(Double)((Map<Object,Object>)ls3.get(0)).get("transport");
				obj.setAmountpayed(PendingAmount);
	  		}
  			
  			session.save(obj);
		} 
  		ls.clear(); 
  		session.flush();
  		session.close();
		try
		{
			String reportFileName ="TransportDeductionsForCaneReport";
			String Query1="distinct * from Temp_TransportDeductionsForCane";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendate", ftdate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateTransportRecoverySummaryForCane", method = RequestMethod.POST)
	public void generateTransportRecoverySummaryForCane(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String caneslno=request.getParameter("ftDate");
		String cslnum[]=caneslno.split(":");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		
		String sqq="truncate table Temp_TransportRecoverySummary";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_TransportRecoverySummary obj=null;
		Session session=hibernatedao.getSessionFactory().openSession();
		String ftdate=null;
		String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		if(ls2.size()>0 && ls2!=null)
		{
			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
	  		
		}
		String sql="select distinct d.harvestcontcode,a.aadhaarnumber,a.accountnumber,a.bankcode,a.branchcode,a.transportercode,a.transporter,b.bankname from DedDetails d ,TransportingContractors a,bank b  where d.season='"+season1[1]+"' and d.caneacslno='"+cslnum[1]+"'and d. DedCode=3 and d.harvestcontcode>0 and d.harvestcontcode=a.transportercode and a.bankcode=b.bankcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer branchcode=0;
  		Integer bankcode=0;
  		Integer tcode=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_TransportRecoverySummary();
  			if(ls.size()>0 && ls!=null)
  			{	       
  				tcode=(Integer)((Map<Object,Object>)ls.get(k)).get("transportercode");
  				
  				bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("branchcode");
  				String aadhaarnumber=(String)((Map<Object,Object>)ls.get(k)).get("aadhaarnumber");
  				String accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("accountnumber");
  				//Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("tamount");
  				String transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("bankname");
  				obj.setBranchname(branchname);
  				obj.setTcode(tcode);
  				obj.setAadhaarNumber(aadhaarnumber);
  				obj.setAccountnumber(accountnumber);
  				obj.setBankcode(bankcode);
  				obj.setTransporter(transporter);
  				//obj.setTotalamount(totalamount);
  			}
  			String sql1="select sum(netpayable) amount from DedDetails where season='"+season1[1]+"' and caneacslno='"+cslnum[1]+"' and harvestcontcode="+tcode+" and  DedCode=3";
  			List ls1=hibernatedao.findBySqlCriteria(sql1);
  			if(ls1.size()>0)
  			{
  				Double amount=(Double)((Map<Object,Object>)ls1.get(0)).get("amount");
  				amount=(double)Math.round(amount * 100d) / 100d;
  				obj.setTotalamount(amount);
  			}
  			session.save(obj);
		} 
  		session.flush();
  		session.close();
  	//	ls.clear();
		try
		{
			String reportFileName ="TransportRecoverySummaryForCaneReport";
			String Query1="distinct tcode,bankcode,aadhaarNumber,accountnumber,branchname,transporter,totalamount from Temp_TransportRecoverySummary";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendate", ftdate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	
	
	
	@RequestMapping(value = "/generateGrowerListByVillageCircleSummary", method = RequestMethod.POST)
	public void generateGrowerListByVillageCircleSummary(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		//this  for picking numbers from combined string with number
		//String mandalCode=request.getParameter("mandal");	
	//	String mandalCode1[]=mandalCode.split(":");
		String rptFormat = request.getParameter("mode");
		String sqq="truncate table Temp_CirclePlantRatoonWiseRyot";
		try{
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		 Session session=hibernatedao.getSessionFactory().openSession();
		 Temp_CirclePlantRatoonWiseRyot obj=null;
		 
		 String sql="select distinct circlecode from agreementdetails  where seasonyear='"+sName[1]+"' order by circlecode";
		 List ls=hibernatedao.findBySqlCriteria(sql); 
		 for (int k = 0; k <ls.size(); k++) 
		 {
			// obj=new Temp_CirclePlantRatoonWiseRyot();
			 Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode");
			// obj.setCirclecode(circlecode);
			 String sql1="select a.slno,a.cropdate,a.agreementnumber,a.ryotcode,a.ryotname,a.relativename,a.plantorratoon,a.villagecode,a.varietycode,isnull(a.extentsize,0) plant,v.village,vi.variety,c.circle from agreementdetails a,village v,variety vi,circle c where a.circlecode="+circlecode+" and a.seasonyear='"+sName[1]+"'and a.villagecode=v.villagecode and a.varietycode=vi.varietycode and c.circlecode="+circlecode+"";
			 List ls1=hibernatedao.findBySqlCriteria(sql1); 
			 for (int i = 0; i <ls1.size(); i++) 
			 { 
				 obj=new Temp_CirclePlantRatoonWiseRyot();
				 String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode"); 
				 String circle=(String)((Map<Object,Object>)ls1.get(i)).get("circle"); 
				 String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname"); 
				 String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename"); 
				 String villagecode=(String)((Map<Object,Object>)ls1.get(i)).get("villagecode");
				 String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				 String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
				 String agreementnumber=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
				 Date agreementdate=(Date)((Map<Object,Object>)ls1.get(i)).get("cropdate");
				 obj.setVillage(village);
				 obj.setCirclecode(circlecode);
				 obj.setCircle(circle);
				 obj.setRyotcode(ryotcode);
				 obj.setRyotname(ryotname);
				 obj.setRelativename(relativename);
				 obj.setVillagecode(villagecode);
				 obj.setVariety(variety);
				 obj.setAgreementnumber(agreementnumber);
				 String todate=DateUtils.formatDate(agreementdate,Constants.GenericDateFormat.DATE_FORMAT);
				 
				 obj.setDateofplanting(todate);
				 String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon"); 
				 Double plant1=0.0;
				 Double ratoon=0.0;
				 if(Integer.parseInt(plantorratoon)==1)
				 {	
					 plant1=(Double)((Map<Object,Object>)ls1.get(i)).get("plant"); 
				 }
				 else
				 {
					 plant1=0.0;
				 }
				 obj.setPlant(plant1);
				 if(Integer.parseInt(plantorratoon)>1)
				 {
					 ratoon=(Double)((Map<Object,Object>)ls1.get(i)).get("plant");
				 }
				 obj.setRatoon(ratoon);
				 session.save(obj);
			 }
		 }
	
  		//ls.clear(); 
  		session.flush();
  		session.close();
  		String Query1;
  		String reportFileName;
  		reportFileName = "CirclePlantRatoonWiseRyotReport";
  		Query1=" dateofplanting,agreementnumber,ryotcode,ryotname,relativename,variety,plant,ratoon,village,villagecode,circlecode,circle FROM Temp_CirclePlantRatoonWiseRyot order by circlecode";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	@RequestMapping(value = "/generateVillageWiseTransportSubsidyList", method = RequestMethod.POST)
	public void generateVillageWiseTransportSubsidyList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		
		String rptFormat = request.getParameter("mode");
		String sqq="truncate table Temp_VillageWiseTransportSubsidyList";
		try{
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Session session=hibernatedao.getSessionFactory().openSession();
		Temp_VillageWiseTransportSubsidyList obj=null;
		 
		String sql="select distinct w.villagecode,v.village,v.distance from WeighmentDetails w,village v where season='"+sName[1]+"' and w.villagecode=v.villagecode and v.distance>40";
		List ls=hibernatedao.findBySqlCriteria(sql); 
		for (int k = 0; k <ls.size(); k++) 
		{
		 if(ls.size()>0 && ls!=null)
		 {
			obj=new Temp_VillageWiseTransportSubsidyList();
			String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
			String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
			Double distance=(Double)((Map<Object,Object>)ls.get(k)).get("distance");
			
		//	if(distance.intValue()>40)
		//	{	
				obj.setDistance(distance);
				obj.setVillage(village);
				obj.setVillagecode(villagecode);
				String sql2="select count(distinct ryotcode) rcount,sum(netwt) qty  from WeighmentDetails where season='"+sName[1]+"' and villagecode='"+villagecode+"'";
				List ls2=hibernatedao.findBySqlCriteria(sql2); 
				if(ls2.size()>0)
				{	
					Double qty=(Double)((Map<Object,Object>)ls2.get(0)).get("qty");
					Integer rcount=(Integer)((Map<Object,Object>)ls2.get(0)).get("rcount");
					double  rc = (double) rcount;  
					obj.setRcount(rc);
					obj.setQuantity(qty);
				}
				String sql1="select noofkms,allowanceperkmperton from CaneAcctRules where season='"+sName[1]+"'";
				List ls1=hibernatedao.findBySqlCriteria(sql1); 
				Double noofkms=0.0;
				Double allowanceperkmperton=0.0;
				if(ls1.size()>0)
				{	
					noofkms=(Double)((Map<Object,Object>)ls1.get(0)).get("noofkms");
					allowanceperkmperton=(Double)((Map<Object,Object>)ls1.get(0)).get("allowanceperkmperton");
					obj.setAmount(allowanceperkmperton);
				}
				Double addl=distance-noofkms;
				obj.setAddl(addl);
		//	} 
		 }
		 session.save(obj);
		}
  		session.flush();
  		session.close();
  		String Query1;
  		String reportFileName;
  		reportFileName = "VillageWiseTransportSubsidyReport";
  		Query1=" * FROM Temp_VillageWiseTransportSubsidyList";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	@RequestMapping(value = "/generateTransportSubsidyList", method = RequestMethod.POST)
	public void generateTransportSubsidyList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");	
		String sName[]=season.split(":");
		
		String rptFormat = request.getParameter("mode");
		String sqq="truncate table Temp_TransportSubsidyListCrushing";
		try{
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Session session=hibernatedao.getSessionFactory().openSession();
		Temp_TransportSubsidyListCrushing obj=null;
		 
		String sql="select distinct w.ryotcode,w.ryotname,w.villagecode,v.village,v.distance from WeighmentDetails w,village v where season='"+sName[1]+"' and w.villagecode=v.villagecode and v.distance>40";
		List ls=hibernatedao.findBySqlCriteria(sql); 
		for (int k = 0; k <ls.size(); k++) 
		{
		 if(ls.size()>0 && ls!=null)
		 {
			obj=new Temp_TransportSubsidyListCrushing();
			String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
			String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
			String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
			String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
			Double distance=(Double)((Map<Object,Object>)ls.get(k)).get("distance");
			obj.setRyotcode(ryotcode);
			obj.setRyotname(ryotname);
			obj.setDistance(distance);
			obj.setVillage(village);
			obj.setVillagecode(villagecode);
			String sql2="select sum(netwt) qty  from WeighmentDetails where season='"+sName[1]+"' and ryotcode='"+ryotcode+"'";
			List ls2=hibernatedao.findBySqlCriteria(sql2); 
			if(ls2.size()>0)
			{	
				Double qty=(Double)((Map<Object,Object>)ls2.get(0)).get("qty");
				obj.setQuantity(qty);
			}
			String sql1="select noofkms,allowanceperkmperton from CaneAcctRules where season='"+sName[1]+"'";
			List ls1=hibernatedao.findBySqlCriteria(sql1); 
			Double noofkms=0.0;
			Double allowanceperkmperton=0.0;
			if(ls1.size()>0)
			{	
				noofkms=(Double)((Map<Object,Object>)ls1.get(0)).get("noofkms");
				allowanceperkmperton=(Double)((Map<Object,Object>)ls1.get(0)).get("allowanceperkmperton");
				obj.setAmount(allowanceperkmperton);
			}
			Double addl=distance-noofkms;
			obj.setAddl(addl);
		 }
		 session.save(obj);
		}
  		session.flush();
  		session.close();
  		String Query1;
  		String reportFileName;
  		reportFileName = "TransportSubsidyListCrushingReport";
  		Query1=" * FROM Temp_TransportSubsidyListCrushing";
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	@RequestMapping(value = "/generateHarvestingContractorServiceProviderRates", method = RequestMethod.POST)
	public void generateHarvestingContractorServiceProviderRates(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_HarvestingContractorServiceRates";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_HarvestingContractorServiceRates obj=new Temp_HarvestingContractorServiceRates();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select h.ryotcode,h.rate,h.sno,h.harvestcontcode,r.ryotname,hc.harvester from HarvestingRateDetails h,ryot r,HarvestingContractors hc where h.season='"+season1[1]+"' and h.effectivedate between'"+fromdate+"' and '"+todate+"' and h.ryotcode=r.ryotcode and h.harvestcontcode=hc.harvestercode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_HarvestingContractorServiceRates();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Double slno=(Double)((Map<Object,Object>)ls.get(k)).get("sno");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String harvestorname=(String)((Map<Object,Object>)ls.get(k)).get("harvester");
  				Double rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				Integer harvestcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				obj.setHarvestcontcode(harvestcontcode);
  				obj.setSlno(slno);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setHarvestorname(harvestorname);
  				obj.setRate(rate);
  				session.save(obj);
  			}	
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="HarvestingContractorServiceRatesReport";
  			String Query1="distinct * from Temp_HarvestingContractorServiceRates";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateHarvestingDeductionsForCaneAC", method = RequestMethod.POST)
	public void generateHarvestingDeductionsForCaneAC(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String caneno=request.getParameter("ftDate");
		String cslnum[]=caneno.split(":");
		String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		Date datefrom=null;
		Date dateto=null;
		String ftdate=null;
		if(ls2.size()>0 && ls2!=null)
		{
			datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
			
		}
		/*Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);*/
		
		String sqq="truncate table Temp_HarvestingDeductionsForCaneAC";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_HarvestingDeductionsForCaneAC obj=new Temp_HarvestingDeductionsForCaneAC();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql=" select distinct h.harvestcontcode, h.ryotcode,r.ryotname,hc.harvester,hrd.sno,hrd.rate from DedDetails h,ryot r,HarvestingContractors hc,HarvestingRateDetails hrd where  h.DedCode=4 and  h.season='"+season1[1]+"' and h.caneacslno='"+cslnum[1]+"' and h.ryotcode=r.ryotcode and h.harvestcontcode=hc.harvestercode  and h.ryotcode=hrd.ryotcode and h.harvestcontcode=hrd.harvestcontcode and h.deductionamt>0";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		String ryotcode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_HarvestingDeductionsForCaneAC();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer harvestcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				Double slno=(Double)((Map<Object,Object>)ls.get(k)).get("sno");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String harvestorname=(String)((Map<Object,Object>)ls.get(k)).get("harvester");
  				Double rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				obj.setHarvestcontcode(harvestcontcode);
  				obj.setSlno(slno);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setHarvestorname(harvestorname);
  				obj.setRate(rate);
  			}	
  			String sql1="select sum(suppliedcaneWt) harvestedcanewt,sum(deductionamt) totalamount  from DedDetails where DedCode=4 and season='"+season1[1]+"' and caneacslno='"+cslnum[1]+"' and ryotcode='"+ryotcode+"'";
  	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		if(ls1.size()>0 && ls1!=null)
			{
  	  			Double PendingAmount=(Double)((Map<Object,Object>)ls1.get(0)).get("totalamount");
  	  			Double qty=(Double)((Map<Object,Object>)ls1.get(0)).get("harvestedcanewt");
  				obj.setAmountpayble(PendingAmount);
  				
  				obj.setQty(qty);
			}
  	  		String sql3="select sum(harvesting) harvamt from CanAccSBDetails where season='"+season1[1]+"' and caneacctslno="+cslnum[1]+" and ryotcode='"+ryotcode+"'";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && ls3!=null)
	  		{
	  			Double PendingAmount=(Double)((Map<Object,Object>)ls3.get(0)).get("harvamt");
				obj.setAmountpayed(PendingAmount);
	  		}
	  		session.save(obj);
  			
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="HarvestingDeductionsForCaneACReport";
  			String Query1="distinct * from Temp_HarvestingDeductionsForCaneAC";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendate", ftdate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateAgricultureLoanAppReport", method = RequestMethod.POST)
	public void generateAgricultureLoanAppReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String branchnm=request.getParameter("branchCode");
		String batchno=request.getParameter("batchno");
		String batchno1[]=batchno.split(":");
		/*String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		*/
		String rptFormat = request.getParameter("status");
	    String season1[]=season.split(":");
	    String branch1[]=branchnm.split(":");
	    String pattern = "dd/MM/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    Temp_AgricultureLoanApp obj=null;
	    String reportFileName =null;
	    String Query1=null;
	    String sqq="truncate table Temp_AgricultureLoanApp";
	    try{
	    Session session=hibernatedao.getSessionFactory().openSession();
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		String sql1="select l.recommendeddate,l.loannumber,l.principle,l.ryotcode,l.plantextent,l.ratoonextent,r.ryotname,r.relativename,r.aadhaarNumber,v.village,b.branchname from loandetails l,ryot r,village v,branch b where Season='"+season1[1]+"' and l.branchcode="+branch1[1]+" and l.ryotcode=r.ryotcode and r.villagecode=v.villagecode and b.branchcode="+branch1[1]+" and l.principle > 0 and l.batchno="+Integer.parseInt(batchno1[1])+"";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		for (int i = 0; i <ls1.size(); i++) 
		{	
			obj=new Temp_AgricultureLoanApp();
			if(ls1.size()>0)
			{
				Date recommended=(Date)((Map<Object,Object>)ls1.get(i)).get("recommendeddate");
	    		String recommendeddate=DateUtils.formatDate(recommended,Constants.GenericDateFormat.DATE_FORMAT);
				Integer loannumber=(Integer)((Map<Object,Object>)ls1.get(i)).get("loannumber");
				Double principle=(Double)((Map<Object,Object>)ls1.get(i)).get("principle");
				String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				
				String plantextent=(String)((Map<Object,Object>)ls1.get(i)).get("plantextent");
				String pextent1[]=plantextent.split(",");
				double value = 0.0;
				for(int j=0;j<pextent1.length;j++)
				{
					value=value+Double.parseDouble(pextent1[j]);
				}
				
				String ratoonextent=(String)((Map<Object,Object>)ls1.get(i)).get("ratoonextent");
				String rextent1[]=ratoonextent.split(",");
				double value1 = 0.0;
				for(int k=0;k<rextent1.length;k++)
				{
					value1=value1+Double.parseDouble(rextent1[k]);
				}
				
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename");
				String aadhaarNumber=(String)((Map<Object,Object>)ls1.get(i)).get("aadhaarNumber");
				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				String branchname=(String)((Map<Object,Object>)ls1.get(i)).get("branchname");
				obj.setLoandate(recommendeddate);
				obj.setBranchname(branchname);
				obj.setLoannumber(loannumber);
				obj.setPrinciple(principle);
				obj.setRyotcode(ryotcode);
				obj.setVillage(village);
				obj.setAadhaarNumber(aadhaarNumber);
				obj.setRelativename(relativename);
				obj.setRyotname(ryotname);
				obj.setPlant(value);
				obj.setRatoon(value1);
				obj.setBatchno(Integer.parseInt(batchno1[1]));
				session.save(obj);
			}
		}
		session.flush();
		session.close();
		reportFileName = "LoanForwardingReport";
		Query1="distinct batchno,loandate,ryotcode,loannumber,village,branchname,ryotname,relativename,plant,ratoon,isnull(principle,0) as principle,aadhaarNumber from Temp_AgricultureLoanApp order by loannumber";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        hmParams.put("cdate",format.format(new Date()));
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    } 
	    catch(Exception e)
	    {
	    	System.out.println("SQLExp::CLOSING::" + e.toString());
	    }
	}
	
	@RequestMapping(value = "/generateCaneSamplesCheckList", method = RequestMethod.POST)
	public void generateCaneSamplesCheckList(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String season=request.getParameter("season");
		String programNo=request.getParameter("programNumber");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String programNo1[]=programNo.split(":");
	   
	    Temp_CaneSamplesCheckList obj=null;
	    String reportFileName =null;
	    String Query1=null;
	    String sqq="truncate table Temp_CaneSamplesCheckList";
	    try{
	    Session session=hibernatedao.getSessionFactory().openSession();
	    SQLQuery query=session.createSQLQuery(sqq);
		query.executeUpdate();
		
		String sql1=" select s.*,r.ryotname,r.villagecode,v.village,c.croptype,a.varietycode,vi.variety from sampleCards s,ryot r,village v,CropTypes c,agreementdetails a,variety vi where s.season='"+season1[1]+"'and s.programnumber="+programNo1[1]+" and s.ryotcode=r.ryotcode and r.villagecode=v.villagecode and s.plantorratoon=c.croptypecode and s.ryotcode=a.ryotcode and a.varietycode=vi.varietycode";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		for (int i = 0; i <ls1.size(); i++) 
		{	
			obj=new Temp_CaneSamplesCheckList();
			if(ls1.size()>0)
			{
				String aggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("aggrementno");
				String plotno=(String)((Map<Object,Object>)ls1.get(i)).get("plotno");
				Integer samplecard=(Integer)((Map<Object,Object>)ls1.get(i)).get("samplecard");
				String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("croptype");
				String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
				Double extentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
				Double ccsrating=(Double)((Map<Object,Object>)ls1.get(i)).get("ccsrating");
				String brix=(String)((Map<Object,Object>)ls1.get(i)).get("brix");
				String pol=(String)((Map<Object,Object>)ls1.get(i)).get("pol");
				String purity=(String)((Map<Object,Object>)ls1.get(i)).get("purity");
				obj.setAggrementno(aggrementno);
				obj.setPlotno(plotno);
				obj.setSamplecard(samplecard);
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setVillage(village);
				obj.setPlantorratoon(plantorratoon);
				obj.setVariety(variety);
				obj.setExtentsize(extentsize);
				obj.setCcsrating(ccsrating);
				obj.setBrix(brix);
				obj.setPol(pol);
				obj.setPurity(purity);
				session.save(obj);
			}
		}
		session.flush();
		session.close();
		reportFileName = "CaneSamplesCheckListReport";
		Query1="distinct aggrementno,plotno,samplecard,ryotcode,ryotname,village,plantorratoon,variety,extentsize,ccsrating,brix,pol,purity from Temp_CaneSamplesCheckList";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
       
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    } 
	    catch(Exception e)
	    {
	    	System.out.println("SQLExp::CLOSING::" + e.toString());
	    }
	}
	@RequestMapping(value = "/generateRankingStatementReport", method = RequestMethod.POST)
	public void generateRankingStatementReport(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		Temp_RankingStatementReport obj=null;
	    String reportFileName =null;
	    String Query1=null;
	    String sqq="truncate table Temp_RankingStatementReport";
	    try{
	    Session session=hibernatedao.getSessionFactory().openSession();
	    SQLQuery query=session.createSQLQuery(sqq);
		query.executeUpdate();
		
		String sql1=" select s.*,r.ryotname,r.relativename,r.villagecode,v.village,c.croptype,a.varietycode,a.rank,vi.variety from sampleCards s,ryot r,village v,CropTypes c,agreementdetails a,variety vi where s.season='"+season1[1]+"' and s.sampledate between '"+fromdate+"' and '"+todate+"' and s.ryotcode=r.ryotcode and r.villagecode=v.villagecode and s.plantorratoon=c.croptypecode and s.ryotcode=a.ryotcode and a.varietycode=vi.varietycode";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		for (int i = 0; i <ls1.size(); i++) 
		{	
			obj=new Temp_RankingStatementReport();
			if(ls1.size()>0)
			{
				String aggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("aggrementno");
			//	String plotno=(String)((Map<Object,Object>)ls1.get(i)).get("plotno");
			//	Integer samplecard=(Integer)((Map<Object,Object>)ls1.get(i)).get("samplecard");
				Integer programnumber=(Integer)((Map<Object,Object>)ls1.get(i)).get("programnumber");
				Integer rank=(Integer)((Map<Object,Object>)ls1.get(i)).get("rank");
				String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename");
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("croptype");
				String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
				Double extentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
				Double ccsrating=(Double)((Map<Object,Object>)ls1.get(i)).get("ccsrating");
				String brix=(String)((Map<Object,Object>)ls1.get(i)).get("brix");
				String pol=(String)((Map<Object,Object>)ls1.get(i)).get("pol");
				String purity=(String)((Map<Object,Object>)ls1.get(i)).get("purity");
				Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("sampledate");
				String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
				obj.setAggrementno(aggrementno);
			//	obj.setPlotno(plotno);
			//	obj.setSamplecard(samplecard);
				obj.setDateofsample(sdate);
				obj.setRank(rank);
				obj.setProgramnumber(programnumber);
				obj.setRelativename(relativename);
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setVillage(village);
				obj.setPlantorratoon(plantorratoon);
				obj.setVariety(variety);
				obj.setExtentsize(extentsize);
				obj.setCcsrating(ccsrating);
				obj.setBrix(brix);
				obj.setPol(pol);
				obj.setPurity(purity);
				session.save(obj);
			}
		}
		session.flush();
		session.close();
		reportFileName = "RankingStatementReport";
		//reportFileName = "CaneGrowersListCircleProgramWise";
		Query1="distinct rank,dateofsample,aggrementno,programnumber,relativename,ryotcode,ryotname,village,plantorratoon,variety,extentsize,ccsrating,brix,pol,purity from Temp_RankingStatementReport";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
       
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    } 
	    catch(Exception e)
	    {
	    	System.out.println("SQLExp::CLOSING::" + e.toString());
	    }
	}
	
	
	@RequestMapping(value = "/generateCaneGrowersListCircleProgramWise", method = RequestMethod.POST)
	public void generateCaneGrowersListCircleProgramWise(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		
		String season=request.getParameter("season");
		String programNo=request.getParameter("programNumber");
		String circleCode=request.getParameter("circle");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String programNo1[]=programNo.split(":");
		String circleCode1[]=circleCode.split(":");
		
		Temp_CaneGrowersListCircleProgramWise obj=null;
	    String reportFileName =null;
	    String Query1=null;
	    String sqq="truncate table Temp_CaneGrowersListCircleProgramWise";
	    try{
	    Session session=hibernatedao.getSessionFactory().openSession();
	    SQLQuery query=session.createSQLQuery(sqq);
		query.executeUpdate();
		
		if(circleCode1[1].equals("ALL"))
		{
		String sql1="select a.ryotcode,a.ryotname,v.village,a.programnumber,a.agreementnumber,vi.variety,a.extentsize,a.agreedqty,a.cropdate,a.plantorratoon,c.circle from AgreementDetails a,village v,variety vi,circle c where a.programnumber="+programNo1[1]+" and a.seasonyear='"+season1[1]+"' and a.villagecode=v.villagecode and a.varietycode=vi.varietycode and c.circlecode=a.circlecode";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		for (int i = 0; i <ls1.size(); i++) 
		{	
			obj=new Temp_CaneGrowersListCircleProgramWise();
			if(ls1.size()>0)
			{
				
				String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				String circle=(String)((Map<Object,Object>)ls1.get(i)).get("circle");
				String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon");
				
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setVillage(village);
				obj.setCircle(circle);
				if (Integer.parseInt(plantorratoon)==1)
				{
					String aggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
					String programnumber=(String)((Map<Object,Object>)ls1.get(i)).get("programnumber");
					String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
					Double extentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
					Double agreedqty=(Double)((Map<Object,Object>)ls1.get(i)).get("agreedqty");
					Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("cropdate");
					String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
					
					obj.setCropdate(sdate);
					obj.setVariety(variety);
					obj.setExtentsize(extentsize);
					obj.setAgreedqty(agreedqty);
					obj.setAggrementno(aggrementno);
					obj.setProgramnumber(programnumber);
				}
				if(Integer.parseInt(plantorratoon)>1)
				{
					String raggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
					String rprogramnumber=(String)((Map<Object,Object>)ls1.get(i)).get("programnumber");
					String rvariety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
					Double rextentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
					Double ragreedqty=(Double)((Map<Object,Object>)ls1.get(i)).get("agreedqty");
					Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("cropdate");
					String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
					
					obj.setRcropdate(sdate);
					obj.setRvariety(rvariety);
					obj.setRextentsize(rextentsize);
					obj.setRagreedqty(ragreedqty);
					obj.setRaggrementno(raggrementno);
					obj.setRprogramnumber(rprogramnumber);
				}
				session.save(obj);
			}
		   }
		}
		else
		{
		String sql2="select a.ryotcode,a.ryotname,v.village,a.programnumber,a.agreementnumber,vi.variety,a.extentsize,a.agreedqty,a.cropdate,a.plantorratoon,c.circle from AgreementDetails a,village v,variety vi,circle c where a.programnumber="+programNo1[1]+" and a.circlecode="+circleCode1[1]+" and a.seasonyear='"+season1[1]+"' and a.villagecode=v.villagecode and a.varietycode=vi.varietycode and c.circlecode="+circleCode1[1]+"";
		List ls1=hibernatedao.findBySqlCriteria(sql2);
		for (int i = 0; i <ls1.size(); i++) 
		{	
			obj=new Temp_CaneGrowersListCircleProgramWise();
			if(ls1.size()>0)
			{
				
				String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
				String circle=(String)((Map<Object,Object>)ls1.get(i)).get("circle");
				String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("plantorratoon");
				
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setVillage(village);
				obj.setCircle(circle);
				if (Integer.parseInt(plantorratoon)==1)
				{
					String aggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
					String programnumber=(String)((Map<Object,Object>)ls1.get(i)).get("programnumber");
					String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
					Double extentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
					Double agreedqty=(Double)((Map<Object,Object>)ls1.get(i)).get("agreedqty");
					Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("cropdate");
					String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
					
					obj.setCropdate(sdate);
					obj.setVariety(variety);
					obj.setExtentsize(extentsize);
					obj.setAgreedqty(agreedqty);
					obj.setAggrementno(aggrementno);
					obj.setProgramnumber(programnumber);
				}
				if(Integer.parseInt(plantorratoon)>1)
				{
					String raggrementno=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
					String rprogramnumber=(String)((Map<Object,Object>)ls1.get(i)).get("programnumber");
					String rvariety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
					Double rextentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
					Double ragreedqty=(Double)((Map<Object,Object>)ls1.get(i)).get("agreedqty");
					Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("cropdate");
					String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
					
					obj.setRcropdate(sdate);
					obj.setRvariety(rvariety);
					obj.setRextentsize(rextentsize);
					obj.setRagreedqty(ragreedqty);
					obj.setRaggrementno(raggrementno);
					obj.setRprogramnumber(rprogramnumber);
				}
				
				session.save(obj);
			}
		}
		}
		session.flush();
		session.close();
		reportFileName = "CaneGrowersListCircleProgramWise";
		Query1="distinct * from Temp_CaneGrowersListCircleProgramWise order by circle";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
       
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    } 
	    catch(Exception e)
	    {
	    	System.out.println("SQLExp::CLOSING::" + e.toString());
	    }
	}
	
	@RequestMapping(value = "/generateInactiveModifiedAgreementsList", method = RequestMethod.POST)
	public void generateInactiveModifiedAgreementsList(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("status");
		String season1[]=season.split(":");
		
		Temp_InactiveModifiedAgreements obj=null;
	    String reportFileName =null;
	    String Query1=null;
	    String sqq="truncate table Temp_InactiveModifiedAgreements";
	    try{
	    Session session=hibernatedao.getSessionFactory().openSession();
	    SQLQuery query=session.createSQLQuery(sqq);
		query.executeUpdate();
		
		String sql2="select a.agreementdate,a.ryotcode,a.accountnumber,a.branchcode,a.agreementnumber,r.aadhaarNumber,r.relativename,r.ryotname,r.villagecode,v.village,c.circle,b.branchname,ad.extentsize,ad.surveynumber,ad.plotnumber, vi.variety,cp.croptype from AgreementSummary a,ryot r,village v,circle c,branch b,AgreementDetails ad,variety vi,croptypes cp where a.seasonyear='"+season1[1]+"' and a.ryotcode=r.ryotcode and r.villagecode=v.villagecode and a.circlecode=c.circlecode and a.branchcode=b.branchcode and a.agreementnumber=ad.agreementnumber and a.status=1 and ad.varietycode=vi.varietycode and ad.plantorratoon=cp.croptypecode";
		List ls1=hibernatedao.findBySqlCriteria(sql2);
		for (int i = 0; i <ls1.size(); i++) 
		{	
		  obj=new Temp_InactiveModifiedAgreements();
		  if(ls1.size()>0)
		  {
			  
			  String ryotcode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
			  Date sDate=(Date)((Map<Object,Object>)ls1.get(i)).get("agreementdate");
			  String sdate=DateUtils.formatDate(sDate,Constants.GenericDateFormat.DATE_FORMAT);
			  String accountnumber=(String)((Map<Object,Object>)ls1.get(i)).get("accountnumber");
			  Integer branchcode=(Integer)((Map<Object,Object>)ls1.get(i)).get("branchcode");
			  String agreementnumber=(String)((Map<Object,Object>)ls1.get(i)).get("agreementnumber");
			  String aadhaarNumber=(String)((Map<Object,Object>)ls1.get(i)).get("aadhaarNumber");
			  String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
			  String relativename=(String)((Map<Object,Object>)ls1.get(i)).get("relativename");
			  String villagecode=(String)((Map<Object,Object>)ls1.get(i)).get("villagecode");
			  String village=(String)((Map<Object,Object>)ls1.get(i)).get("village");
			  String circle=(String)((Map<Object,Object>)ls1.get(i)).get("circle");
			  String branchname=(String)((Map<Object,Object>)ls1.get(i)).get("branchname");
			  Double extentsize=(Double)((Map<Object,Object>)ls1.get(i)).get("extentsize");
			  String plotnumber=(String)((Map<Object,Object>)ls1.get(i)).get("plotnumber");
			  String surveynumber=(String)((Map<Object,Object>)ls1.get(i)).get("surveynumber");
			  String variety=(String)((Map<Object,Object>)ls1.get(i)).get("variety");
			  String plantorratoon=(String)((Map<Object,Object>)ls1.get(i)).get("croptype");
			  obj.setRyotcode(ryotcode);
			  obj.setAgreementdate(sdate);
			  obj.setAccountnumber(accountnumber);
			  obj.setBranchcode(branchcode);
			  obj.setAgreementnumber(agreementnumber);
			  obj.setAadhaarNumber(aadhaarNumber);
			  obj.setRyotname(ryotname);
			  obj.setRelativename(relativename);
			  obj.setVillagecode(villagecode);
			  obj.setVillage(village);
			  obj.setCircle(circle);
			  obj.setBranchname(branchname);
			  obj.setExtentsize(extentsize);
			  obj.setPlotnumber(plotnumber);
			  obj.setSurveynumber(surveynumber);
			  obj.setVariety(variety);
			  obj.setPlantorratoon(plantorratoon);
			  session.save(obj);
			}
		}
		
		session.flush();
		session.close();
		reportFileName = "InactiveModifiedAgreementsList";
		Query1="distinct * from Temp_InactiveModifiedAgreements order by circle";
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
	    } 
	    catch(Exception e)
	    {
	    	System.out.println("SQLExp::CLOSING::" + e.toString());
	    }
	}
	
	@RequestMapping(value = "/generateHarvestRecoverySummaryForCaneAC", method = RequestMethod.POST)
	public void generateHarvestRecoverySummaryForCaneAC(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String caneslno=request.getParameter("ftDate");
		String cslnum[]=caneslno.split(":");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_HarvestRecoverySummaryForCaneAC";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_HarvestRecoverySummaryForCaneAC obj=new Temp_HarvestRecoverySummaryForCaneAC();
		Session session=hibernatedao.getSessionFactory().openSession();
		String ftdate=null;
		String sql2="select datefrom,dateto  from CaneAccountSmry  where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		if(ls2.size()>0 && ls2!=null)
		{
			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
		}
		
		String sql="select distinct d.harvestcontcode,hc.aadhaarnumber,hc.accountnumber,hc.bankcode,hc.harvester,b.bankname from DedDetails d,HarvestingContractors hc,bank b where d.season='"+season1[1]+"' and d.caneacslno='"+cslnum[1]+"'and d. DedCode=4 and hc.harvestercode=d.harvestcontcode and hc.bankcode=b.bankcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_HarvestRecoverySummaryForCaneAC();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				Integer harvestcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				String harvester=(String)((Map<Object,Object>)ls.get(k)).get("harvester");
  				String aadhaarnumber=(String)((Map<Object,Object>)ls.get(k)).get("aadhaarnumber");
  				String accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("accountnumber");
  				String bankname=(String)((Map<Object,Object>)ls.get(k)).get("bankname");
  				obj.setBankcode(bankcode);
  				obj.setHarvestercode(harvestcontcode);
  				obj.setHarvester(harvester);
  				obj.setAadhaarnumber(aadhaarnumber);
  				obj.setAccountnumber(accountnumber);
  				obj.setBankname(bankname);
  				String sql1="select sum(netpayable) amount from DedDetails where season='"+season1[1]+"' and caneacslno='"+cslnum[1]+"' and harvestcontcode="+harvestcontcode+" and  DedCode=4";
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0)
  		  		{
  		  		Double amount=(Double)((Map<Object,Object>)ls1.get(0)).get("amount");
  		  		obj.setAmount(amount);
  		  		}
  		  	//	obj.setBetweendate(ftdate);
  		  		session.save(obj);
  			}	
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="HarvestRecoverySummaryForCaneACReport";
  			String Query1="distinct * from Temp_HarvestRecoverySummaryForCaneAC where amount >1";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("betweendate", ftdate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateTransportingContractorServiceProviderRates", method = RequestMethod.POST)
	public void generateTransportingContractorServiceProviderRates(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_TransportingContractorServiceRates";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_TransportingContractorServiceRates obj=new Temp_TransportingContractorServiceRates();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select t.transportcontcode, t.rate,t.ryotcode,t.slno,r.ryotname,tc.transporter from TransportingRateDetails t,ryot r,TransportingContractors tc where t.season='"+season1[1]+"' and t.effectivedate between'"+fromdate+"' and '"+todate+"' and t.ryotcode=r.ryotcode and t.transportcontcode=tc.transportercode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_TransportingContractorServiceRates();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer transportcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("transportcontcode");
  				Double slno=(Double)((Map<Object,Object>)ls.get(k)).get("slno");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				Double rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				obj.setTransportcontcode(transportcontcode);
  				obj.setSlno(slno);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setTransporter(transporter);
  				obj.setRate(rate);
  				session.save(obj);
  			}	
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="TransportingContractorServiceRatesReport";
  			String Query1="distinct * from Temp_TransportingContractorServiceRates order by transportcontcode, slno";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("datebetween", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateCheckListForSBAccountPortions", method = RequestMethod.POST)
	public void generateCheckListForSBAccountPortions(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		//String fromDate=request.getParameter("fromDate");
	//	String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
	//	Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	//	Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CheckListForSBAccountPortions";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CheckListForSBAccountPortions obj=new Temp_CheckListForSBAccountPortions();
	
		String sql="select c.bankcode,c.sbaccno,c.loans,c.sbac,c.ulamt,c.cdcamt,c.ryotcode,c.suppqty,c.totalamt ,c.transport ,c.harvesting,c.otherded, r.ryotname from CanAccSBDetails c,ryot r where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and  c.ryotcode=r.ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			
  			obj=new Temp_CheckListForSBAccountPortions();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				String sbaccno=(String)((Map<Object,Object>)ls.get(k)).get("sbaccno");
  				Double loanamt=(Double)((Map<Object,Object>)ls.get(k)).get("loans");
  				Double sbac=(Double)((Map<Object,Object>)ls.get(k)).get("sbac");
  				
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double supplyqty=(Double)((Map<Object,Object>)ls.get(k)).get("suppqty");
  				Double amtpayble=(Double)((Map<Object,Object>)ls.get(k)).get("totalamt");
  				Double trptamount=(Double)((Map<Object,Object>)ls.get(k)).get("transport");
  				Double harvamount=(Double)((Map<Object,Object>)ls.get(k)).get("harvesting");
  				Double otherded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
  				Double uc=(Double)((Map<Object,Object>)ls.get(k)).get("ulamt");
  				Double cdc=(Double)((Map<Object,Object>)ls.get(k)).get("cdcamt");
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setSupplyqty(supplyqty);
  				obj.setAmtpayble(amtpayble);
  				obj.setTrptamount(trptamount);
  				obj.setHarvamount(harvamount);
  				obj.setOtherded(otherded);
  				obj.setUc(uc);
  				obj.setCdc(cdc);
  				obj.setBankcode(bankcode);
  				obj.setSbaccno(sbaccno);
  				obj.setLoanamt(loanamt);
  				obj.setSbac(sbac);
  			}	
  			String sql2="select  datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  	  	session.save(obj);
		}
  		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CheckListForSBAccountPortionsReport";
  			String Query1="distinct * from Temp_CheckListForSBAccountPortions where sbac>1";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateCaneSupplyPaymentStatementForCaneAC", method = RequestMethod.POST)
	public void generateCaneSupplyPaymentStatementForCaneAC(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CaneSupplyPaymentStatementForCaneAC";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CaneSupplyPaymentStatementForCaneAC obj=new Temp_CaneSupplyPaymentStatementForCaneAC();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select   isnull(c.forsbacs,0) forsbacs,isnull(c.totalloanpayable,0) totalloanpayable,isnull(c.lotaladvpayable,0) lotaladvpayable,isnull(c.totalvalue,0) totalvalue,isnull(c.totalvalue,0) totalvalue,isnull(c.netprice,0) netprice,isnull(c.uc,0) uc,isnull(c.cdc,0) cdc,c.ryotcode,c.suppliedcanewt,isnull(c.totalsbacpayable,0) totalsbacpayable,isnull(c.transportchargespayable,0) transportchargespayable,isnull(c.harvestchargespayable,0) harvestchargespayable,isnull(c.otherded,0) otherded,r.ryotname,r.aadhaarNumber,rb.accountnumber,rb.ryotbankbranchcode,b.branchname from CaneValueSplitupDetailsByDate c,ryot r,ryotbankdetails rb,branch b  where c.season='"+season1[1]+"' and c.caneaccountingdate between'"+fromdate+"' and '"+todate+"' and c.ryotcode=r.ryotcode and c.ryotcode=rb.ryotcode and rb.ryotbankbranchcode=b.branchcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_CaneSupplyPaymentStatementForCaneAC();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double otherded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
  				Double uc=(Double)((Map<Object,Object>)ls.get(k)).get("uc");
  				Double cdc=(Double)((Map<Object,Object>)ls.get(k)).get("cdc");
  				Double forsbacs=(Double)((Map<Object,Object>)ls.get(k)).get("forsbacs");
  				Double totalloanpayable=(Double)((Map<Object,Object>)ls.get(k)).get("totalloanpayable");
  				Double lotaladvpayable=(Double)((Map<Object,Object>)ls.get(k)).get("lotaladvpayable");
  				Double totalvalue=(Double)((Map<Object,Object>)ls.get(k)).get("totalvalue");
  				Double netprice=(Double)((Map<Object,Object>)ls.get(k)).get("netprice");
  				Double suppliedcanewt=(Double)((Map<Object,Object>)ls.get(k)).get("suppliedcanewt");
  				Double totalsbacpayable=(Double)((Map<Object,Object>)ls.get(k)).get("totalsbacpayable");
  				Double transportchargespayable=(Double)((Map<Object,Object>)ls.get(k)).get("transportchargespayable");
  				Double harvestchargespayable=(Double)((Map<Object,Object>)ls.get(k)).get("harvestchargespayable");
  				String aadhaarNumber=(String)((Map<Object,Object>)ls.get(k)).get("aadhaarNumber");
  				String accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("accountnumber");
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
  				Integer ryotbankbranchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("ryotbankbranchcode");
  		
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setOtherded(otherded);
  				obj.setUc(uc);
  				obj.setCdc(cdc);
  				obj.setForsbacs(forsbacs);
  				obj.setTotalloanpayable(totalloanpayable);
  				obj.setLotaladvpayable(lotaladvpayable);
  				obj.setTotalvalue(totalvalue);
  				obj.setNetprice(netprice);
  				obj.setSuppliedcanewt(suppliedcanewt);
  				obj.setTotalsbacpayable(totalsbacpayable);
  				obj.setTransportchargespayable(transportchargespayable);
  				obj.setHarvestchargespayable(harvestchargespayable);
  				obj.setAadhaarnumber(aadhaarNumber);
  				obj.setAccountnumber(accountnumber);
  				obj.setBranchname(branchname);
  				obj.setRyotbankcode(ryotbankbranchcode);
  				
  				session.save(obj);
  			}	
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CaneSupplyPaymentStatementReport";
  			String Query1="distinct * from Temp_CaneSupplyPaymentStatementForCaneAC";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateAdvanceCheckListReport", method = RequestMethod.POST)
	public void generateAdvanceCheckListReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String advCode=request.getParameter("advanceType");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String advanceCode[]=advCode.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_AdvanceCheckListReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_AdvanceCheckListReport obj=new Temp_AdvanceCheckListReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		if(advanceCode[1].equals("ALL"))
		{
		String sql=" select a.ryotcode,a.advanceamount,a.advancedate,r.ryotname,r.relativename,c.advance from AdvanceDetails a ,ryot r,CompanyAdvance c where a.season='"+season1[1]+"' and a.advancedate between'"+fromdate+"' and '"+todate+"' and a.ryotcode=r.ryotcode and a.advancecode=c.advancecode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_AdvanceCheckListReport();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  				Double advanceamount=(Double)((Map<Object,Object>)ls.get(k)).get("advanceamount");
  				String advance=(String)((Map<Object,Object>)ls.get(k)).get("advance");
  				Date advancedate=(Date)((Map<Object,Object>)ls.get(k)).get("advancedate");
  				String adate=DateUtils.formatDate(advancedate,Constants.GenericDateFormat.DATE_FORMAT);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setRelativename(relativename);
  				obj.setAdvanceamount(advanceamount);
  				obj.setAdvance(advance);
  				obj.setAdvancedate(adate);
  				session.save(obj);
  			}	
		}
		}
		else
		{
			String sql=" select a.ryotcode,a.advanceamount,a.advancedate,r.circleCode,r.ryotname,r.relativename,c.advance from AdvanceDetails a ,ryot r,CompanyAdvance c where a.season='"+season1[1]+"' and a.advancedate between'"+fromdate+"' and '"+todate+"' and a.advancecode="+advanceCode[1]+" and a.ryotcode=r.ryotcode and a.advancecode=c.advancecode";
	  		List ls=hibernatedao.findBySqlCriteria(sql);
	  		for (int k = 0; k <ls.size(); k++) 
			{
	  			obj=new Temp_AdvanceCheckListReport();
	  			if(ls.size()>0 && ls!=null)
	  			{	 
	  				
	  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
	  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
	  				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
	  				Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circleCode");
	  				Double advanceamount=(Double)((Map<Object,Object>)ls.get(k)).get("advanceamount");
	  				String advance=(String)((Map<Object,Object>)ls.get(k)).get("advance");
	  				Date advancedate=(Date)((Map<Object,Object>)ls.get(k)).get("advancedate");
	  				String adate=DateUtils.formatDate(advancedate,Constants.GenericDateFormat.DATE_FORMAT);
	  				obj.setRyotcode(ryotcode);
	  				obj.setRyotname(ryotname);
	  				obj.setRelativename(relativename);
	  				obj.setAdvanceamount(advanceamount);
	  				obj.setAdvance(advance);
	  				obj.setAdvancedate(adate);
	  				obj.setCirclecode(circlecode);
	  				session.save(obj);
	  			}	
			}
			
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="AdvanceCheckListReport";
  			String Query1="distinct * from Temp_AdvanceCheckListReport order by circlecode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateBankwiseGrowersLoanCollectionAbstractForCane", method = RequestMethod.POST)
	public void generateBankwiseGrowersLoanCollectionAbstractForCane(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String caneSlNo[]=caneslno.split(":");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_BankwiseGrowersLoanCollection";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_BankwiseGrowersLoanCollection obj=new Temp_BankwiseGrowersLoanCollection();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct branchcode from CanAccLoansDetails  where season ='"+season1[1]+"' and caneacctslno='"+caneSlNo[1]+"' ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_BankwiseGrowersLoanCollection();
  			if(ls.size()>0 && ls!=null)
  			{	
  				Integer branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("branchcode");
  				if(branchcode<= 9)
  				{
  					String sql1="select  advance,glCode from CompanyAdvance  where  advancecode="+branchcode+" ";
  			  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  			  		String branchname=(String)((Map<Object,Object>)ls1.get(0)).get("advance");
  			  		String glcode=(String)((Map<Object,Object>)ls1.get(0)).get("glCode");
  			  		obj.setGlcode(Long.parseLong(glcode));
  					obj.setBranchcode(branchcode);
  					obj.setBranchname(branchname);
  				}
  				else
  				{
  					String sql2="select  branchname,glcode from Branch  where  branchcode="+branchcode+" ";
  			  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  					obj.setBranchcode(branchcode);
  					String branchname=(String)((Map<Object,Object>)ls2.get(0)).get("branchname");
  	  				BigDecimal glcode=(BigDecimal)((Map<Object,Object>)ls2.get(0)).get("glcode");
  	  				obj.setBranchname(branchname);
  	  				obj.setGlcode(glcode.longValue());
  					
  				}
  				/*List bclist=hibernatedao.findByNativeSql("select * from Temp_BankwiseGrowersLoanCollection where branchcode = '"+branchcode+"'",Temp_BankwiseGrowersLoanCollection.class);;
		       	if(bclist.size()>0)
		       	{
		       		Session session1=hibernatedao.getSessionFactory().openSession();
		       		Temp_BankwiseGrowersLoanCollection vData=(Temp_BankwiseGrowersLoanCollection)bclist.get(0);
		       		Double tamount=(Double)vData.getAmount();
		       		Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamount");
		       		tamount=tamount+totalamount;
		       		vData.setAmount(tamount);
		       		vData.setId(vData.getId());
		       		session1.saveOrUpdate(vData);
		       		session1.flush();
		       		session1.close();
		       	}*/
  				
  				String sql1="select sum(advisedamt) totalamount from CanAccLoansDetails where season ='"+season1[1]+"' and  branchcode="+branchcode+" and caneacctslno='"+caneSlNo[1]+"'";
  		  		List ls2=hibernatedao.findBySqlCriteria(sql1);
  		  		Double totalamount=0.0;
  		  		if(ls2.size()>0 && ls2!=null)
  		  		{
  		  			totalamount=(Double)((Map<Object,Object>)ls2.get(0)).get("totalamount");
  		  		}
  				obj.setAmount((double)Math.round(totalamount));
  				String sql3="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+caneSlNo[1]+" and season='"+season1[1]+"'";
  	  	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  	  	  		if(ls3.size()>0 && ls3!=null)
  				{
  	  	  			Date datefrom=(Date)((Map<Object,Object>)ls3.get(0)).get("datefrom");
  	  	  			Date dateto=(Date)((Map<Object,Object>)ls3.get(0)).get("dateto");
  	  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  	  			String ftdate=fdate+" To "+tdate;
  	  	  			obj.setBetweendate(ftdate);
  				}
  				session.save(obj);
		       	}
  			}	
		       	
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="BankwiseGrowersLoanCollectionReport";
  			String Query1="distinct * from Temp_BankwiseGrowersLoanCollection order by branchcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateBankWisePaymentToRyotSBACsForCaneAC", method = RequestMethod.POST)
	public void generateBankWisePaymentToRyotSBACsForCaneAC(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String caneSlNo[]=caneslno.split(":");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_BankWisePaymentToRyotSBACsForCaneAC";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_BankWisePaymentToRyotSBACsForCaneAC obj=new Temp_BankWisePaymentToRyotSBACsForCaneAC();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct c.bankcode,b.branchname,b.glcode from CanAccSBDetails c ,branch b where c.season ='"+season1[1]+"' and c.bankcode=b.branchcode and caneacctslno='"+caneSlNo[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_BankWisePaymentToRyotSBACsForCaneAC();
  			if(ls.size()>0 && ls!=null)
  			{	
  				Integer branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				obj.setBranchcode(branchcode);
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
  				BigDecimal glcode=(BigDecimal)((Map<Object,Object>)ls.get(k)).get("glcode");
  				obj.setBranchname(branchname);
  				obj.setGlcode(glcode.longValue());
  				
  				String sql1="select sum(paidamt) totalamount from CanAccSBDetails where season ='"+season1[1]+"' and bankcode="+branchcode+" and caneacctslno='"+caneSlNo[1]+"'";
  		  		List ls2=hibernatedao.findBySqlCriteria(sql1);
  		  		Double totalamount=0.0;
  		  		if(ls2.size()>0 && ls2!=null)
  		  		{
  		  			totalamount=(Double)((Map<Object,Object>)ls2.get(0)).get("totalamount");
  		  		}
  				obj.setAmount((double)Math.round(totalamount));
  				String sql3="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+caneSlNo[1]+" and season='"+season1[1]+"'";
  	  	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  	  	  		if(ls3.size()>0 && ls3!=null)
  				{
  	  	  			Date datefrom=(Date)((Map<Object,Object>)ls3.get(0)).get("datefrom");
  	  	  			Date dateto=(Date)((Map<Object,Object>)ls3.get(0)).get("dateto");
  	  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  	  			String ftdate=fdate+" To "+tdate;
  	  	  			obj.setBetweendate(ftdate);
  				}
  				
  				session.save(obj);
		       	}
  			}	
		       	
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="BankWisePaymentToRyotSBACsForCaneACReport";
  			String Query1="distinct * from Temp_BankWisePaymentToRyotSBACsForCaneAC";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVillageWiseIncentiveCanePrice", method = RequestMethod.POST)
	public void generateVillageWiseIncentiveCanePrice(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		String sqq="truncate table Temp_VillageWiseIncentiveCanePrice";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_VillageWiseIncentiveCanePrice obj=new Temp_VillageWiseIncentiveCanePrice();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct c.villagecode,v.village from CaneValueSplitupDetailsByDate c,village v where season ='"+season1[1]+"' and c.villagecode=v.villagecode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_VillageWiseIncentiveCanePrice();
  			if(ls.size()>0 && ls!=null)
  			{	
  				String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				
  				
  				String sql1="select count(ryotcode) ryotcount,sum(suppliedcanewt) suppliedcanewt from CaneValueSplitupDetailsByDate where season ='"+season1[1]+"' ";
  		  		List ls2=hibernatedao.findBySqlCriteria(sql1);
  		  		Double rcount=0.0;
  		  		Double supqty=0.0;
  		  		if(ls2.size()>0 && ls2!=null)
  		  		{
  		  			 supqty=(Double)((Map<Object,Object>)ls2.get(0)).get("suppliedcanewt");
  		  			 rcount=(Double)((Map<Object,Object>)ls2.get(0)).get("ryotcount");
  		  		}
  		  		String sql2=" select distinct amount from CaneAcctAmounts where pricename='Purchase Tax / ICP' and season ='"+season1[1]+"' ";
		  		List ls3=hibernatedao.findBySqlCriteria(sql2);
		  		Double icpamount=0.0;
		  		if(ls3.size()>0 && ls3!=null)
  		  		{
		  			icpamount=(Double)((Map<Object,Object>)ls3.get(0)).get("amount");
  		  			
  		  		}
		  		obj.setIcpamount(icpamount);
  				obj.setRcount(rcount);
  				obj.setSupqty(supqty);
  				obj.setVillagecode(villagecode);
  				obj.setVillage(village);
  				session.save(obj);
		       	}
  			}	
		       	
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="VillageWiseIncentiveCanePriceReport";
  			String Query1="distinct * from Temp_VillageWiseIncentiveCanePrice";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateIncentiveCanePricePayment", method = RequestMethod.POST)
	public void generateIncentiveCanePricePayment(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		String sqq="truncate table Temp_IncentiveCanePricePayment";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_IncentiveCanePricePayment obj=new Temp_IncentiveCanePricePayment();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct c.ryotcode,c.villagecode,v.village,r.ryotname,r.relativename from CaneValueSplitupDetailsByDate c,village v,ryot r where season ='"+season1[1]+"' and c.villagecode=v.villagecode and c.ryotcode=r.ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_IncentiveCanePricePayment();
  			if(ls.size()>0 && ls!=null)
  			{	
  				String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode");
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  				
  				String sql1="select sum(suppliedcanewt) suppliedcanewt from CaneValueSplitupDetailsByDate where season ='"+season1[1]+"' and ryotcode='"+ryotcode+"' ";
  		  		List ls2=hibernatedao.findBySqlCriteria(sql1);
  		  		Double rcount=0.0;
  		  		Double supqty=0.0;
  		  		if(ls2.size()>0 && ls2!=null)
  		  		{
  		  			 supqty=(Double)((Map<Object,Object>)ls2.get(0)).get("suppliedcanewt");
  		  			 rcount=(Double)((Map<Object,Object>)ls2.get(0)).get("ryotcount");
  		  		}
  		  		String sql2=" select distinct amount from CaneAcctAmounts where pricename='Purchase Tax / ICP' and season ='"+season1[1]+"' ";
		  		List ls3=hibernatedao.findBySqlCriteria(sql2);
		  		Double icpamount=0.0;
		  		if(ls3.size()>0 && ls3!=null)
  		  		{
		  			icpamount=(Double)((Map<Object,Object>)ls3.get(0)).get("amount");
  		  			
  		  		}
		  		obj.setIcpamount(icpamount);
		  		obj.setRyotcode(ryotcode);
		  		obj.setRelativename(relativename);
		  		obj.setRyotname(ryotname);
  				obj.setSupqty(supqty);
  				obj.setVillagecode(villagecode);
  				obj.setVillage(village);
  				session.save(obj);
		       	}
  			}	
		       	
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="IncentiveCanePricePaymentReport";
  			String Query1="distinct * from Temp_IncentiveCanePricePayment";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateLoanCheckListReport", method = RequestMethod.POST)
	public void generateLoanCheckListReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String branchCod=request.getParameter("branchName");
		String rptFormat = request.getParameter("status");
		String season1[]=season.split(":");
		String branchCode[]=branchCod.split(":");
		
	//	Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
	//	Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
	//	String pattern = "dd/MM/yyyy";
	//	SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_LoanCheckListReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_LoanCheckListReport obj=new Temp_LoanCheckListReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		
			String sql=" select l.referencenumber,l.ryotcode,l.disbursedamount,l.disburseddate,l.loannumber,r.ryotname,b.branchname from ryot r inner join loandetails l on r.ryotcode=l.ryotcode inner join branch b on l.branchcode=b.branchcode where l.season='"+season1[1]+"' and l.disburseddate between'"+DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and '"+DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT)+"' and l.branchcode="+branchCode[1]+" and l.disburseddate is not null";
	  		List ls=hibernatedao.findBySqlCriteria(sql);
	  		for (int k = 0; k <ls.size(); k++) 
			{
	  			obj=new Temp_LoanCheckListReport();
	  			if(ls.size()>0 && ls!=null)
	  			{	 
	  				
	  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
	  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
	  				String branch=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
	  				Integer lnumber=(Integer)((Map<Object,Object>)ls.get(k)).get("loannumber");
	  				String referencenumber=(String)((Map<Object,Object>)ls.get(k)).get("referencenumber");
	  				Double disbursedamount=(Double)((Map<Object,Object>)ls.get(k)).get("disbursedamount");
	  				Date ldate=(Date)((Map<Object,Object>)ls.get(k)).get("disburseddate");
	  				String disburseddate=DateUtils.formatDate(ldate,Constants.GenericDateFormat.DATE_FORMAT);
	  				obj.setRyotcode(ryotcode);
	  				obj.setRyotname(ryotname);
	  				obj.setDisbursedamount(disbursedamount);
	  				obj.setDisburseddate(disburseddate);
	  				obj.setBranch(branch);
	  				obj.setReferencenumber(referencenumber);
	  				obj.setLoannumber(lnumber);
	  				session.save(obj);
	  			}	
			}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="VerifyingLoansAsPerBranchReport";
  			String Query1="distinct * from Temp_LoanCheckListReport";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}

	@RequestMapping(value = "/generateBruntCaneReport", method = RequestMethod.POST)
	public void generateBruntCaneReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String sqq="truncate table Temp_BruntCane";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		Temp_BruntCane obj=new Temp_BruntCane();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql="select distinct weighbridgeslno from WeighmentDetails where season='"+season1[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer weighbridgeslno=0;
  		Integer shiftid=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			if(ls.size()>0 && ls!=null)
  			{	       
  				weighbridgeslno=(Integer)((Map<Object,Object>)ls.get(k)).get("weighbridgeslno");
  				
  			}	
  			String sql1="select w.serialnumber,w.netwt,w.ryotcode,v.Variety,w.plantorratoon,s.shiftname,w.canereceiptenddate from WeighmentDetails w,variety v,shift s where  w.shiftid1=s.shiftid and w.varietycode=v.varietycode and burntcane=0 and weighbridgeslno="+weighbridgeslno+" and season='"+season1[1]+"'";
  			List ls1=hibernatedao.findBySqlCriteria(sql1); 
  			Integer varietycode=0;
  			Integer ptorrt=0;
  			if(ls1.size()>0 && ls1!=null)
  			{
  				for(int j = 0; j<ls1.size(); j++)
  				{
  					obj=new Temp_BruntCane();
  					
  					String ryotcode=(String)((Map<Object,Object>)ls1.get(j)).get("ryotcode");
  					Integer receiptno=(Integer)((Map<Object,Object>)ls1.get(j)).get("serialnumber");
  					Double weight=(Double)((Map<Object,Object>)ls1.get(j)).get("netwt");
  					String variety=(String)((Map<Object,Object>)ls1.get(j)).get("Variety");
  					String shift=(String)((Map<Object,Object>)ls1.get(j)).get("shiftname");
  					Byte plantorratoon=(Byte)((Map<Object,Object>)ls1.get(j)).get("plantorratoon");
  					ptorrt=plantorratoon.intValue();
  					Date cwdate=(Date)((Map<Object,Object>)ls1.get(j)).get("canereceiptenddate");
	  				String weighmentdate=DateUtils.formatDate(cwdate,Constants.GenericDateFormat.DATE_FORMAT);
  					obj.setBridgeno(weighbridgeslno);
  					obj.setReceiptno(receiptno);
  					obj.setRyotcode(ryotcode);
  					obj.setWeight(weight);
  					obj.setVariety(variety);
  					obj.setShift(shift);
  					obj.setWbdate(weighmentdate);
  					String sql2="select croptype from CropTypes where croptypecode="+ptorrt+"";
  					List ls2=hibernatedao.findBySqlCriteria(sql2); 
  					if(ls2.size()>0 && ls2!=null)
  					{
  						String croptype=(String)((Map<Object,Object>)ls2.get(0)).get("croptype");
  						obj.setPlantorratoon(croptype);
  					}
  					session.save(obj);
  				}
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="BurntCaneReport";
  			String Query1="distinct bridgeno,ryotcode,receiptno,weight,variety,shift,plantorratoon,wbdate from Temp_BruntCane order by bridgeno";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", dateFormat.format(date));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}

	@RequestMapping(value = "/generateCaneSuppliersReport", method = RequestMethod.POST)
	public void generateCaneSuppliersReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CaneSuppliers";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CaneSuppliers obj=new Temp_CaneSuppliers();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct zonecode from zone";
  		List ls10=hibernatedao.findBySqlCriteria(sql);
  		Integer zonecode=0;
  		for (int m = 0; m <ls10.size(); m++) 
		{
  			if(ls10.size()>0 && ls10!=null)
  			{	       
  				zonecode=(Integer)((Map<Object,Object>)ls10.get(m)).get("zonecode");
  			}
			String sql10="select distinct w.zonecode,w.ryotcode,w.ryotname,z.zone,w.circlecode,c.circle from WeighmentDetails w,zone z,circle c where w.circlecode=c.circlecode  and w.zonecode=z.zonecode and w.canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.season='"+season1[1]+"' and w.zonecode="+zonecode+"";
	  		List ls=hibernatedao.findBySqlCriteria(sql10);
	  		
	  		Integer circlecode=0;
	  		String ryotcode=null;
	  		for (int k = 0; k <ls.size(); k++) 
			{
  			if(ls.size()>0 && ls!=null)
  			{	       
  				obj=new Temp_CaneSuppliers();
  			//	zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode");
  				circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");
  			 	ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				obj.setZone(zone);
  				obj.setZonecode(zonecode);
  				obj.setCircle(circle);
  				obj.setCirclecode(circlecode);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  			}
  			String sql1="select sum(netwt) netwt from WeighmentDetails  where canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and  circlecode='"+circlecode+"' and zonecode="+zonecode+"";
  	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		if(ls1.size()>0 && ls1!=null)
			{
  	  			Double 	suplyqty=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt");
  	  			obj.setSuplyqty(suplyqty);
			}
  	  	//	String sql2="select sum(netwt) netwt from WeighmentDetails  where season='"+season1[1]+"' and circlecode="+circlecode+"and ryotcode='"+ryotcode+"' and zonecode="+zonecode+"";
  	  		String sql2="select sum(netwt) netwt from WeighmentDetails  where season='"+season1[1]+"' and ryotcode='"+ryotcode+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		if(ls2.size()>0 && ls2!=null)
	  		{
	  			Double 	cumqty=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
	  			obj.setCumqty(cumqty);
	  		}
	  		String sql3="select sum(estimatedqty) estimatedqty from AgreementDetails  where seasonyear='"+season1[1]+"' and circlecode="+circlecode+" and ryotcode='"+ryotcode+"' and zonecode="+zonecode+"";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && ls3!=null)
	  		{
	  			Double 	estqty=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty");
	  			obj.setEstqty(estqty);
	  		}
	  		
	  		if(k==0)
	  		{
	  			String sql4="select sum(netwt) netwt from WeighmentDetails  where canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"' and circlecode="+circlecode+"  and zonecode="+zonecode+"";
		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
		  		if(ls4.size()>0 && ls4!=null)
		  		{
		  			Double 	suptotal=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt");
		  			obj.setSuptotal(suptotal);
		  		}
		  		String sql5="select sum(netwt) netwt from WeighmentDetails  where  season='"+season1[1]+"' and ryotcode='"+ryotcode+"'";
		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
		  		if(ls5.size()>0 && ls5!=null)
		  		{
		  			Double 	cumtotal=(Double)((Map<Object,Object>)ls5.get(0)).get("netwt");
		  			obj.setCumtotal(cumtotal);
		  		}
		  		String sql6="select sum(estimatedqty) estimatedqty from AgreementDetails   where  seasonyear='"+season1[1]+"' and circlecode="+circlecode+"  and zonecode="+zonecode+"";
		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
		  		if(ls6.size()>0 && ls6!=null)
		  		{
		  			Double 	esttotal=(Double)((Map<Object,Object>)ls6.get(0)).get("estimatedqty");
		  			obj.setEsttotal(esttotal);
		  		}
		  		
		  	}
	  		session.save(obj);
		}

	  		/*String sql7="select sum(netwt) netwt from WeighmentDetails  where canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'  and zonecode="+zonecode+"";
	  		List ls7=hibernatedao.findBySqlCriteria(sql7);
	  		if(ls7.size()>0 && ls7!=null)
	  		{
	  			Double 	supgrandtotal=(Double)((Map<Object,Object>)ls7.get(0)).get("netwt");
	  			obj.setSupgrandtotal(supgrandtotal);
	  		}
	  		String sql8="select sum(netwt) netwt from WeighmentDetails  where  season='"+season1[1]+"'  and zonecode="+zonecode+"";
	  		List ls8=hibernatedao.findBySqlCriteria(sql8);
	  		if(ls8.size()>0 && ls8!=null)
	  		{
	  			Double 	cumgrandtotal=(Double)((Map<Object,Object>)ls8.get(0)).get("netwt");
	  			obj.setCumgrandtotal(cumgrandtotal);
	  		}
	  		String sql9="select sum(estimatedqty) estimatedqty from AgreementDetails where  seasonyear='"+season1[1]+"'  and zonecode="+zonecode+"";
	  		List ls9=hibernatedao.findBySqlCriteria(sql9);
	  		if(ls9.size()>0 && ls9!=null)
	  		{
	  			Double 	estgrandtotal=(Double)((Map<Object,Object>)ls9.get(0)).get("estimatedqty");
	  			obj.setEstgrandtotal(estgrandtotal);
	  		}*/
	  		
		}
	
  		session.flush();
  		session.close();
		try
		{
			String reportFileName ="CaneSuppliersReport";
			String Query1="zone,zonecode,circle,circlecode,ryotcode,ryotname,suplyqty,cumqty,estqty from Temp_CaneSuppliers order by zonecode,circlecode";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVehicleWiseDetails", method = RequestMethod.POST)
	public void generateVehicleWiseDetails(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String vehicleNo1 = request.getParameter("vehicleNo");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String vehicleNo[]=vehicleNo1.split(":");
		String sqq="truncate table Temp_VehicleWiseDetails";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_VehicleWiseDetails obj=new Temp_VehicleWiseDetails();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql="select distinct w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,z.zone,c.circle from WeighmentDetails w,shift s,zone z,circle c where w.zonecode=z.zonecode and w.circlecode=c.circlecode  and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and w.vehicleno='"+vehicleNo[1]+"' ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer weighbridgeslno=0;
  		Integer shiftid=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			if(ls.size()>0 && ls!=null)
  			{
  				obj=new Temp_VehicleWiseDetails();
  				Double 	netwt=(Double)((Map<Object,Object>)ls.get(k)).get("netwt");	
  				Integer serialnumber=(Integer)((Map<Object,Object>)ls.get(k)).get("actualserialnumber");
  				String 	ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");	
  				String 	shift=(String)((Map<Object,Object>)ls.get(k)).get("shiftname");	
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");	
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");	
  				Date cdate=(Date)((Map<Object,Object>)ls.get(k)).get("canereceiptenddate");	
  				String vdate=DateUtils.formatDate(cdate,Constants.GenericDateFormat.DATE_FORMAT);
  				obj.setCircle(circle);
  				obj.setZone(zone);
  				obj.setNetwt(netwt);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setShift(shift);
  				obj.setVdate(vdate);
  				obj.setSerialnumber(serialnumber);
  				session.save(obj);
  				
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="VehicleNumberWiseDetailsReport";
  			String Query1="distinct netwt,ryotcode,ryotname,shift,vdate,serialnumber,zone,circle from Temp_VehicleWiseDetails order by zone,circle";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("vnum", vehicleNo[1]);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateZoneWiseWeighmentData", method = RequestMethod.POST)
	public void generateZoneWiseWeighmentData(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String zone=request.getParameter("zoneCode");
		String circle=request.getParameter("circle");
		String variety = request.getParameter("varietyOfCane");
		String ryot = request.getParameter("ryotcode");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String zonecod[]=zone.split(":");
		String varietycod[]=variety.split(":");
		String circlecode="0";
		String ryotCode="0";
		if(ryot!=null)
		{
		String ryotCod[]=ryot.split(":");
		 ryotCode=ryotCod[1];
		}
		if(circle!=null)
		{
		String circlecod[]=circle.split(":");
		 circlecode=circlecod[1];
		}
		
		String zonecode=zonecod[1];
		String varietycode=varietycod[1];	
		
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_ZoneWiseWeighmentData";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_ZoneWiseWeighmentData obj=new Temp_ZoneWiseWeighmentData();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="";
		ryotCode="0";
		if("0".equals(zonecode))
		{
			zonecode="ALL";
		}
		if("0".equals(varietycode))
		{
			varietycode="ALL";
		}
		if("0".equals(ryotCode))
		{
			ryotCode="ALL";
		}
		if("0".equals(circlecode))
		{
			circlecode="ALL";
		}
		
		if("ALL".equals(zonecode) )
		{
			if("ALL".equals(zonecode) && "ALL".equals(varietycode))
			{
		 sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode  and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"'";
			}
			else
			{
		 sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode  and  w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"'and w.varietycode='"+varietycode+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"'";
	
			}
		}
		else 
		{
			if("ALL".equals(varietycode))
			{
				if("ALL".equals(circlecode))
				{
					if("ALL".equals(ryotCode))
					{
					sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" ";
					}
					else
					{
					sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.ryotcode='"+ryotCode+"' ";
					}				
				}
				else
				{
					if("ALL".equals(ryotCode))
					{
					sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.circlecode="+circlecode+" ";
					}
					else
					{
					sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.circlecode="+circlecode+" and w.ryotcode='"+ryotCode+"' ";
					}					
				}
			}
			else
			{
				if("ALL".equals(circlecode))
				{
					if("ALL".equals(ryotCode))
					{
						sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.varietycode="+varietycode+"";
					}
					else
					{
						sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where  w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.ryotcode='"+ryotCode+"' and w.varietycode="+varietycode+"";
					}
				}
				else
				{
					if("ALL".equals(ryotCode))
					{
						sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.circlecode="+circlecode+"  and w.varietycode="+varietycode+"";
					}
					else
					{
						sql="select distinct w.circlecode,w.canereceiptenddate,w.actualserialnumber,w.ryotcode,w.ryotname,w.netwt,s.shiftname,c.circle,v.variety,w.zonecode,z.zone from WeighmentDetails w,shift s,circle c,variety v,zone z where  w.zonecode=z.zonecode and w.varietycode=v.varietycode and w.circlecode=c.circlecode and w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' and w.zonecode="+zonecode+" and w.circlecode="+circlecode+" and w.ryotcode='"+ryotCode+"' and w.varietycode="+varietycode+"";
					}					
				}

			}
		}
			
			
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		
  		Integer shiftid=0;
  		if(ls.size()>0 && ls!=null)
			{
				for (int k = 0; k <ls.size(); k++) 
				{
  				obj=new Temp_ZoneWiseWeighmentData();
  				Double 	netwt=(Double)((Map<Object,Object>)ls.get(k)).get("netwt");	
  				Integer serialnumber=(Integer)((Map<Object,Object>)ls.get(k)).get("actualserialnumber");
  				Integer circleCode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode");
  				Integer zoneCode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode");
  				String 	zoneName=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				String 	ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");	
  				String 	shift=(String)((Map<Object,Object>)ls.get(k)).get("shiftname");	
  				String 	circl=(String)((Map<Object,Object>)ls.get(k)).get("circle");	
  				String 	variet=(String)((Map<Object,Object>)ls.get(k)).get("variety");	
  				Date cdate=(Date)((Map<Object,Object>)ls.get(k)).get("canereceiptenddate");	
  				String wdate=DateUtils.formatDate(cdate,Constants.GenericDateFormat.DATE_FORMAT);
  				obj.setZone(zoneName);
  				obj.setCirclecode(circleCode);
  				obj.setZonecode(zoneCode);
  				obj.setNetwt(netwt);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setShift(shift);
  				obj.setWdate(wdate);
  				obj.setSerialnumber(serialnumber);
  				obj.setCircle(circl);
  				obj.setVariety(variet);
  				session.save(obj);
  				
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="ZoneWiseWeighmentData";
  			String Query1="distinct circlecode,zone,zonecode,netwt,ryotcode,ryotname,shift,wdate,serialnumber,circle,variety from Temp_ZoneWiseWeighmentData order by zone,circle,serialnumber";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateShiftWiseCaneCrushed", method = RequestMethod.POST)
	public void generateShiftWiseCaneCrushed(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_ShiftWiseCaneCrushed";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_ShiftWiseCaneCrushed obj=new Temp_ShiftWiseCaneCrushed();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql4="select isnull(sum(netwt),0) netwt from WeighmentDetails where  season='"+season1[1]+"' and canereceiptenddate <'"+fromdate+"' ";
		List ls4=hibernatedao.findBySqlCriteria(sql4);
		Double openQty=0.0;
		if(ls4.size()>0 && ls4!=null)
		{	
			openQty=(Double)((Map<Object,Object>)ls4.get(0)).get("netwt");
		}
		else 
			openQty=0.0;
		/*obj.setOpenQty(openQty);
		obj.setShift("0");
		obj.setNetwt1(0.0);
		obj.setNetwt3(0.0);
		obj.setNetwt2(0.0);*/
		String sql="select distinct w.shiftid1,s.shiftname from WeighmentDetails w,shift s where w.shiftid1=s.shiftid and w.season='"+season1[1]+"' and w.canereceiptenddate  between '"+fromdate+"' and '"+todate+"' ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls!=null)
		{
  			Integer shiftid=null;
  	  		for (int k = 0; k <ls.size(); k++) 
  			{
  	  			
  				obj=new Temp_ShiftWiseCaneCrushed();
  				shiftid=(Integer)((Map<Object,Object>)ls.get(k)).get("shiftid1");	
  				String shiftname=(String)((Map<Object,Object>)ls.get(k)).get("shiftname");
  				obj.setShift(shiftname);
  				obj.setOpenQty(openQty);
  	  			
  	  			String sql1="select isnull(sum(netwt),0) netwt from WeighmentDetails where  weighbridgeslno=1  and shiftid1="+shiftid+" and season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' ";
  	  			List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  			Double netwt1=0.0;
  	  			if(ls1.size()>0 && ls1!=null)
  	  			{
  	  				 netwt1=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt");
  	  			}
  	  			obj.setNetwt1(netwt1);
  	  			String sql2="select isnull(sum(netwt),0) netwt from WeighmentDetails where  weighbridgeslno=2  and shiftid1="+shiftid+" and season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' ";
  	  			List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  			Double netwt2=0.0;
  	  			if(ls2.size()>0 && ls2!=null)
  	  			{
  	  				 netwt2=(Double)((Map<Object,Object>)ls2.get(0)).get("netwt");
  	  			}
  	  			obj.setNetwt2(netwt2);
  	  			String sql3="select isnull(sum(netwt),0) netwt from WeighmentDetails where  weighbridgeslno=3  and shiftid1="+shiftid+" and season='"+season1[1]+"' and canereceiptenddate  between '"+fromdate+"' and '"+todate+"' ";
  	  			List ls3=hibernatedao.findBySqlCriteria(sql3);
  	  			Double netwt3=0.0;
  	  			if(ls3.size()>0 && ls3!=null)
  	  			{
  	  				 netwt3=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
  	  			}
  	  		  	obj.setNetwt3(netwt3);	
  	  		session.save(obj);
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="ShiftWiseCaneCrushedReport";
  			String Query1="distinct netwt1,netwt2,netwt3,shift,openQty from Temp_ShiftWiseCaneCrushed order by shift";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateHarvestContractorDtails", method = RequestMethod.POST)
	public void generateHarvestContractorDtails(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String Contractor=request.getParameter("Contractor");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String contractorCode[]=Contractor.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_HarvestContractorDtailsByRyot";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_HarvestContractorDtailsByRyot obj=new Temp_HarvestContractorDtailsByRyot();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql=null;
		if("0".equals(contractorCode[1]))
		{
		sql=" select distinct h.rate,h.harvestcontcode,h.totalamount, h.ryotcode,h.harvestedcanewt,h.receiptdate,w.ryotname,c.circle,hc.harvester,z.zone from HarvChgDtlsByRyotContAndDate h,WeighmentDetails w,circle c,HarvestingContractors hc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and h.ryotcode=w.ryotcode and h.season='"+season1[1]+"' and h.receiptdate  between '"+fromdate+"' and '"+todate+"'  and hc.harvestercode=h.harvestcontcode ";
		}
		else
		{
			sql=" select distinct h.rate,h.harvestcontcode,h.totalamount, h.ryotcode,h.harvestedcanewt,h.receiptdate,w.ryotname,c.circle,hc.harvester,z.zone from HarvChgDtlsByRyotContAndDate h,WeighmentDetails w,circle c,HarvestingContractors hc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and h.ryotcode=w.ryotcode and h.season='"+season1[1]+"' and h.receiptdate  between '"+fromdate+"' and '"+todate+"' and h.harvestcontcode='"+contractorCode[1]+"' and hc.harvestercode='"+contractorCode[1]+"'";
		}
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls!=null)
		{
  			Integer shiftid=null;
  	  		for (int k = 0; k <ls.size(); k++) 
  			{
  	  			
  				obj=new Temp_HarvestContractorDtailsByRyot();
  				Integer harvestcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				String 	ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double 	netwt=(Double)((Map<Object,Object>)ls.get(k)).get("harvestedcanewt");
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");
  				String 	harvester=(String)((Map<Object,Object>)ls.get(k)).get("harvester");
  				Double 	totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamount");
  				Double 	rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				obj.setZone(zone);
  				obj.setHarvestcontcode(harvestcontcode);
  				obj.setRate(rate);
  				obj.setTotalamount(totalamount);
  	  			obj.setCircle(circle);
  	  			obj.setHarvester(harvester);
  	  			obj.setNetwt(netwt);
  	  			obj.setRyotcode(ryotcode);
  	  			obj.setRyotname(ryotname);
  	  		session.save(obj);
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="HarvestContractorRyotWiseReport";
  			String Query1="zone,harvestcontcode,totalamount,rate,harvester,ryotcode,ryotname,circle,netwt from Temp_HarvestContractorDtailsByRyot  order by harvestcontcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", fromDate+" TO "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateAreaWisePendingReport", method = RequestMethod.POST)
	public void AreaWisePendingReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{	
		String rptFormat = request.getParameter("status");	
		String season=request.getParameter("season");
		String group=request.getParameter("GROWER");
		String group1[]=group.split(":");
		String season1[]=season.split(":");
	    String sqq="truncate table Temp_AreaWisePendingReport";
	    SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_AreaWisePendingReport obj=new Temp_AreaWisePendingReport();
	    String Query1=null;
		String reportFileName=null;
		String sql;
		List ls;
		Date currentDate=new Date();
		String today=DateUtils.formatDate(currentDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		Session session=hibernatedao.getSessionFactory().openSession();
		if("Circle".equals(group1[1]))
		{
		  sql="select distinct circlecode,circle from circle where status=0 ";
		  try
		  {
		  ls=hibernatedao.findBySqlCriteria(sql); 
		  for(int k = 0; k <ls.size(); k++) 
		  {
			  obj=new Temp_AreaWisePendingReport();
			  Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode"); 
			  String circle=(String)((Map<Object,Object>)ls.get(k)).get("circle"); 
			  obj.setNametype(circle);
			  String sql1="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where circlecode="+circlecode+" and seasonyear='"+season1[1]+"' and programnumber=1";
			  List ls1=hibernatedao.findBySqlCriteria(sql1);
			  if(ls1.size()>0 && ls1!=null)
			  {	
				  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
				  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty(progressiveqty);
				  obj.setEstimatedqty(estimatedqty);
			  }
			  String sql2="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where circlecode="+circlecode+" and seasonyear='"+season1[1]+"' and programnumber=2";
			  List ls2=hibernatedao.findBySqlCriteria(sql2);
			  if(ls2.size()>0 && ls2!=null)
			  {	
				  Double  progressiveqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("progressiveqty"); 
				  Double  estimatedqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty1(progressiveqty1);
				  obj.setEstimatedqty1(estimatedqty1);
			  }
			  String sql3="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where circlecode="+circlecode+" and seasonyear='"+season1[1]+"' and programnumber=3";
			  List ls3=hibernatedao.findBySqlCriteria(sql3);
			  if(ls3.size()>0 && ls3!=null)
			  {	
				  Double  progressiveqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("progressiveqty"); 
				  Double  estimatedqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty2(progressiveqty2);
				  obj.setEstimatedqty2(estimatedqty2);
			  }
			  
			  String sql4="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where circlecode="+circlecode+" and seasonyear='"+season1[1]+"'";
			  List ls4=hibernatedao.findBySqlCriteria(sql4);
			  if(ls4.size()>0 && ls4!=null)
			  {	
				  Double  totprogressiveqty=(Double)((Map<Object,Object>)ls4.get(0)).get("progressiveqty"); 
				  Double  totestimatedqty=(Double)((Map<Object,Object>)ls4.get(0)).get("estimatedqty"); 
				  obj.setTotprogressiveqty(totprogressiveqty);
				  obj.setTotestimatedqty(totestimatedqty);
			  }
			
			  session.save(obj);
		  }
		  session.flush();
		  session.close();	
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "CircleWisePendingReport";
		  Query1="distinct nametype,totprogressiveqty,totestimatedqty,progressiveqty,estimatedqty,progressiveqty1,estimatedqty1,progressiveqty2,estimatedqty2 from Temp_AreaWisePendingReport ";
		}
		else if("Village".equals(group1[1]))
		{
			sql="select villagecode,village from Village where status=0";
			try{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
			  obj=new Temp_AreaWisePendingReport();
			  String villagecode=(String)((Map<Object,Object>)ls.get(k)).get("villagecode"); 
			  String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
			  obj.setNametype(village);
		
			  String sql1="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where villagecode='"+villagecode+"' and seasonyear='"+season1[1]+"' and programnumber=1";
			  List ls1=hibernatedao.findBySqlCriteria(sql1);
			  if(ls1.size()>0 && ls1!=null)
			  {	
				  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
				  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty(progressiveqty);
				  obj.setEstimatedqty(estimatedqty);
			  }
			  String sql2="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where villagecode='"+villagecode+"' and seasonyear='"+season1[1]+"' and programnumber=2";
			  List ls2=hibernatedao.findBySqlCriteria(sql2);
			  if(ls2.size()>0 && ls2!=null)
			  {	
				  Double  progressiveqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("progressiveqty"); 
				  Double  estimatedqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty1(progressiveqty1);
				  obj.setEstimatedqty1(estimatedqty1);
			  }
			  String sql3="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where villagecode='"+villagecode+"' and seasonyear='"+season1[1]+"' and programnumber=3";
			  List ls3=hibernatedao.findBySqlCriteria(sql3);
			  if(ls3.size()>0 && ls3!=null)
			  {	
				  Double  progressiveqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("progressiveqty"); 
				  Double  estimatedqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty"); 
				  obj.setProgressiveqty2(progressiveqty2);
				  obj.setEstimatedqty2(estimatedqty2);
			  }
			  String sql4="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where villagecode='"+villagecode+"' and seasonyear='"+season1[1]+"'";
			  List ls4=hibernatedao.findBySqlCriteria(sql4);
			  if(ls4.size()>0 && ls4!=null)
			  {	
				  Double  totprogressiveqty=(Double)((Map<Object,Object>)ls4.get(0)).get("progressiveqty"); 
				  Double  totestimatedqty=(Double)((Map<Object,Object>)ls4.get(0)).get("estimatedqty"); 
				  obj.setTotprogressiveqty(totprogressiveqty);
				  obj.setTotestimatedqty(totestimatedqty);
			  }
			  session.save(obj);
			}
			session.flush();
			session.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			reportFileName = "VillageWisePendingReport";
			 Query1=" nametype,totprogressiveqty,totestimatedqty,progressiveqty,estimatedqty,progressiveqty1,estimatedqty1,progressiveqty2,estimatedqty2 from Temp_AreaWisePendingReport ";
		}
		
		else if("Zone".equals(group1[1]))
		{
		  sql="select distinct zonecode,zone from Zone where status=0";
		  try
		  {
			  ls=hibernatedao.findBySqlCriteria(sql); 
			  for(int k = 0; k <ls.size(); k++) 
			  {
				  obj=new Temp_AreaWisePendingReport();
				  Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode"); 
				  String zone=(String)((Map<Object,Object>)ls.get(k)).get("zone"); 
				  obj.setNametype(zone);
				  String sql1="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where zonecode="+zonecode+" and seasonyear='"+season1[1]+"' and programnumber=1";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  if(ls1.size()>0 && ls1!=null)
				  {	
					  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty(progressiveqty);
					  obj.setEstimatedqty(estimatedqty);
				  }
				  String sql2="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where zonecode="+zonecode+" and seasonyear='"+season1[1]+"' and programnumber=2";
				  List ls2=hibernatedao.findBySqlCriteria(sql2);
				  if(ls2.size()>0 && ls2!=null)
				  {	
					  Double  progressiveqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("progressiveqty"); 
					  Double  estimatedqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty1(progressiveqty1);
					  obj.setEstimatedqty1(estimatedqty1);
				  }
				  String sql3="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where zonecode="+zonecode+" and seasonyear='"+season1[1]+"' and programnumber=3";
				  List ls3=hibernatedao.findBySqlCriteria(sql3);
				  if(ls3.size()>0 && ls3!=null)
				  {	
					  Double  progressiveqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("progressiveqty"); 
					  Double  estimatedqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty2(progressiveqty2);
					  obj.setEstimatedqty2(estimatedqty2);
				  }
				  String sql4="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where zonecode="+zonecode+" and seasonyear='"+season1[1]+"'";
				  List ls4=hibernatedao.findBySqlCriteria(sql4);
				  if(ls4.size()>0 && ls4!=null)
				  {	
					  Double  totprogressiveqty=(Double)((Map<Object,Object>)ls4.get(0)).get("progressiveqty"); 
					  Double  totestimatedqty=(Double)((Map<Object,Object>)ls4.get(0)).get("estimatedqty"); 
					  obj.setTotprogressiveqty(totprogressiveqty);
					  obj.setTotestimatedqty(totestimatedqty);
				  }
				  session.save(obj);
			  }
			  session.flush();
			  session.close();
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  reportFileName = "ZoneWisePendingReport";
		  Query1=" nametype,totprogressiveqty,totestimatedqty,progressiveqty,estimatedqty,progressiveqty1,estimatedqty1,progressiveqty2,estimatedqty2 from Temp_AreaWisePendingReport ";
		}
		else if("Mandal".equals(group1[1]))
		{
		sql="select distinct mandalcode,mandal from Mandal where status=0";
		try
		{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_AreaWisePendingReport();
				Integer mandalcode=(Integer)((Map<Object,Object>)ls.get(k)).get("mandalcode"); 
				String mandal=(String)((Map<Object,Object>)ls.get(k)).get("mandal"); 
				obj.setNametype(mandal);
				 String sql1="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where mandalcode="+mandalcode+" and seasonyear='"+season1[1]+"' and programnumber=1";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  if(ls1.size()>0 && ls1!=null)
				  {	
					  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty(progressiveqty);
					  obj.setEstimatedqty(estimatedqty);
				  }
				  String sql2="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where mandalcode="+mandalcode+" and seasonyear='"+season1[1]+"' and programnumber=2";
				  List ls2=hibernatedao.findBySqlCriteria(sql2);
				  if(ls2.size()>0 && ls2!=null)
				  {	
					  Double  progressiveqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("progressiveqty"); 
					  Double  estimatedqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty1(progressiveqty1);
					  obj.setEstimatedqty1(estimatedqty1);
				  }
				  String sql3="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where mandalcode="+mandalcode+" and seasonyear='"+season1[1]+"' and programnumber=3";
				  List ls3=hibernatedao.findBySqlCriteria(sql3);
				  if(ls3.size()>0 && ls3!=null)
				  {	
					  Double  progressiveqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("progressiveqty"); 
					  Double  estimatedqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty2(progressiveqty2);
					  obj.setEstimatedqty2(estimatedqty2);
				  }
				  String sql4="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where mandalcode="+mandalcode+" and seasonyear='"+season1[1]+"'";
				  List ls4=hibernatedao.findBySqlCriteria(sql4);
				  if(ls4.size()>0 && ls4!=null)
				  {	
					  Double  totprogressiveqty=(Double)((Map<Object,Object>)ls4.get(0)).get("progressiveqty"); 
					  Double  totestimatedqty=(Double)((Map<Object,Object>)ls4.get(0)).get("estimatedqty"); 
					  obj.setTotprogressiveqty(totprogressiveqty);
					  obj.setTotestimatedqty(totestimatedqty);
				  }
				session.save(obj);
			}
			session.flush();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		reportFileName = "MandalWisePendingReport";
		 Query1=" nametype,totprogressiveqty,totestimatedqty,progressiveqty,estimatedqty,progressiveqty1,estimatedqty1,progressiveqty2,estimatedqty2 from Temp_AreaWisePendingReport ";
		}
		else if("Region".equals(group1[1]))
		{
		sql="select distinct regioncode,region from Region where status=0";
		try
		{
			ls=hibernatedao.findBySqlCriteria(sql); 
			for(int k = 0; k <ls.size(); k++) 
			{
				obj=new Temp_AreaWisePendingReport();
				Integer regioncode=(Integer)((Map<Object,Object>)ls.get(k)).get("regioncode"); 
				String region=(String)((Map<Object,Object>)ls.get(k)).get("region"); 
				obj.setNametype(region);
				 String sql1="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where regioncode="+regioncode+" and seasonyear='"+season1[1]+"' and programnumber=1";
				  List ls1=hibernatedao.findBySqlCriteria(sql1);
				  if(ls1.size()>0 && ls1!=null)
				  {	
					  Double  progressiveqty=(Double)((Map<Object,Object>)ls1.get(0)).get("progressiveqty"); 
					  Double  estimatedqty=(Double)((Map<Object,Object>)ls1.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty(progressiveqty);
					  obj.setEstimatedqty(estimatedqty);
				  }
				  String sql2="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where regioncode="+regioncode+" and seasonyear='"+season1[1]+"' and programnumber=2";
				  List ls2=hibernatedao.findBySqlCriteria(sql2);
				  if(ls2.size()>0 && ls2!=null)
				  {	
					  Double  progressiveqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("progressiveqty"); 
					  Double  estimatedqty1=(Double)((Map<Object,Object>)ls2.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty1(progressiveqty1);
					  obj.setEstimatedqty1(estimatedqty1);
				  }
				  String sql3="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where regioncode="+regioncode+" and seasonyear='"+season1[1]+"' and programnumber=3";
				  List ls3=hibernatedao.findBySqlCriteria(sql3);
				  if(ls3.size()>0 && ls3!=null)
				  {	
					  Double  progressiveqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("progressiveqty"); 
					  Double  estimatedqty2=(Double)((Map<Object,Object>)ls3.get(0)).get("estimatedqty"); 
					  obj.setProgressiveqty2(progressiveqty2);
					  obj.setEstimatedqty2(estimatedqty2);
				  }
				  String sql4="select isnull(sum(progressiveqty),0) progressiveqty,isnull(sum(estimatedqty),0) estimatedqty from AgreementDetails where regioncode="+regioncode+" and seasonyear='"+season1[1]+"'";
				  List ls4=hibernatedao.findBySqlCriteria(sql4);
				  if(ls4.size()>0 && ls4!=null)
				  {	
					  Double  totprogressiveqty=(Double)((Map<Object,Object>)ls4.get(0)).get("progressiveqty"); 
					  Double  totestimatedqty=(Double)((Map<Object,Object>)ls4.get(0)).get("estimatedqty"); 
					  obj.setTotprogressiveqty(totprogressiveqty);
					  obj.setTotestimatedqty(totestimatedqty);
				  }
				session.save(obj);
			}
			session.flush();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		reportFileName = "RegionWisePendingReport";
		 Query1=" nametype,totprogressiveqty,totestimatedqty,progressiveqty,estimatedqty,progressiveqty1,estimatedqty1,progressiveqty2,estimatedqty2 from Temp_AreaWisePendingReport ";
		}
		HashMap<String,Object> hmParams=new HashMap<String,Object>();
		hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
		hmParams.put("qrs", Query1);
		hmParams.put("today", today);
		generateReport(request,response,hmParams,reportFileName,rptFormat);
	}
	
	@RequestMapping(value = "/generateDeductionsCheckList", method = RequestMethod.POST)
	public void generateDeductionsCheckList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String cslnum[]=caneslno.split(":");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
	
		String sqq="truncate table Temp_DeductionCheckList";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_DeductionCheckList obj=new Temp_DeductionCheckList();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		/*String sql="select distinct caneacslno  from  CaneAccountSmry where season='"+season1[1]+"' and caneacslno='"+cslnum[1]+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Integer caneSlNu=0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			
  			if(ls.size()>0 && ls!=null) 
  			{	       
  				caneSlNu=(Integer)((Map<Object,Object>)ls.get(k)).get("caneacslno");
  			}*/
		String sql1="select c.cdcamt,c.ulamt,c.sbac,c.loans,c.ryotcode,c.harvesting,c.ded,c.otherded,c.totalamt,c.transport,c.suppqty,r.ryotname  from  CanAccSBDetails c,ryot r where c.season='"+season1[1]+"' and c.caneacctslno="+cslnum[1]+" and c.ryotcode=r.ryotcode";
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		if(ls1.size()>0 && ls1!=null) 
		{
			for (int i = 0; i <ls1.size(); i++) 
			{
				obj=new Temp_DeductionCheckList();
				String ryotCode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
				String ryotname=(String)((Map<Object,Object>)ls1.get(i)).get("ryotname");
				Double supplyqty=(Double)((Map<Object,Object>)ls1.get(i)).get("suppqty");
				Double harvesting=(Double)((Map<Object,Object>)ls1.get(i)).get("harvesting");
				Double transport=(Double)((Map<Object,Object>)ls1.get(i)).get("transport");
				Double otherded=(Double)((Map<Object,Object>)ls1.get(i)).get("otherded");
				Double cdcamt=(Double)((Map<Object,Object>)ls1.get(i)).get("cdcamt");
				Double ulamt=(Double)((Map<Object,Object>)ls1.get(i)).get("ulamt");				
				
				//Double ded=(Double)((Map<Object,Object>)ls1.get(i)).get("ded");
				Double ded=cdcamt + ulamt + otherded + harvesting + transport;
				Double totalamt=(Double)((Map<Object,Object>)ls1.get(i)).get("totalamt");
				Double loans=(Double)((Map<Object,Object>)ls1.get(i)).get("loans");
				Double sbac=(Double)((Map<Object,Object>)ls1.get(i)).get("sbac");
				
				Double payable=totalamt-ded;
				Double banktfer=payable-sbac;
				Double bycash=sbac;
				
				obj.setBanktfer(banktfer);
				obj.setBycash(bycash);
				obj.setOdedamount(otherded);
				obj.setAmountpayble(payable);
				obj.setTrptamount(transport);
				obj.setHarvamount(harvesting);
				obj.setRyotcode(ryotCode);
				obj.setRyotname(ryotname);
				obj.setSupplyqty(supplyqty);
				
				session.save(obj);
			}
			
		}
	
  		session.flush();
  		session.close();
  		
		try
		{
			String reportFileName ="DeductionsCheckListReport";
			String Query1=" banktfer,bycash,ryotcode,ryotname,supplyqty,amountpayble,trptamount,harvamount,odedamount from Temp_DeductionCheckList";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	
	@RequestMapping(value = "/generateLoanRecoveryReport", method = RequestMethod.POST)
	public void generateLoanRecoveryReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String branch=request.getParameter("branchCode");
		//String branch=branch1.split(":");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		String branchCode[]=branch.split(":");
		String sql=null;
		String sql1=null;
		String pattern = "dd/MM/yyyy";
		Integer branchcode=0;
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		NumberToWord obj1=new NumberToWord();
		
		String sqq="truncate table Temp_LoanRecoveryReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_LoanRecoveryReport obj=new Temp_LoanRecoveryReport();
		 if(branchCode[1].equals("ALL"))
		 {
		sql="select c.finalinterest,c.branchcode,c.accountnum,c.paidamt,c.ryotcode,r.ryotname,b.branchname from CanAccLoansDetails c,ryot r,Branch b  where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and  c.ryotcode=r.ryotcode and c.branchcode=b.branchcode and c.branchcode != 0 and advloan='1'";
		 }
		 else
		 {
			 sql="select c.finalinterest,c.branchcode,c.accountnum,c.paidamt,c.ryotcode,r.ryotname,b.branchname from CanAccLoansDetails c,ryot r,Branch b  where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and  c.ryotcode=r.ryotcode and c.branchcode='"+branchCode[1]+"' and b.branchcode='"+branchCode[1]+"' and c.branchcode != 0 and advloan='1'"; 
		 }
		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		Double loanamount=0.0;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			Double gtotal=0.0;
  			obj=new Temp_LoanRecoveryReport();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("branchcode");
  				String accountnum=(String)((Map<Object,Object>)ls.get(k)).get("accountnum");
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
  				Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("paidamt");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double finalinterest=(Double)((Map<Object,Object>)ls.get(k)).get("finalinterest");
  				if(totalamount<finalinterest)
  				{
  				 loanamount= 0.0;
  				finalinterest=totalamount;
  				}
  				else
  				{
  					loanamount=totalamount-finalinterest;
  				}
  				obj.setLoanamount((double)Math.round(loanamount));
  				obj.setFinalinterest((double)Math.round(finalinterest));
  				obj.setAccountnum(accountnum);
  				obj.setBranch(branchname);
  				obj.setBranchcode(branchcode);
  				obj.setTotalamount((double)Math.round(totalamount));
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  			}
  			
  			sql1="select  sum(paidamt) gtotal from CanAccLoansDetails   where caneacctslno="+cslnum[1]+" and season='"+season1[1]+"' and branchcode='"+branchcode+"' and branchcode != 0 and advloan='1'";
  			List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		if(ls1.size()>0 && ls1!=null)
			{
  	  		 gtotal=(Double)((Map<Object,Object>)ls1.get(0)).get("gtotal");
			}
  	  		
	       // obj1.inputNumber();
	        String numword=obj1.convertNumberToWords(gtotal.intValue());
  			obj.setGtotalwords(numword);
  			String sql2="select  datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  			
  			session.save(obj);
		}
  		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="loanRecoveryReport";
  			String Query1="distinct betweendate,gtotalwords,branchcode,accountnum,totalamount,ryotcode,ryotname,branch,finalinterest,loanamount  from Temp_LoanRecoveryReport  where totalamount >1 order by branch,ryotcode ";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate",format.format(new Date()));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateCanePricePaymentsStatement", method = RequestMethod.POST)
	public void generateCanePricePaymentsStatement(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String branch=request.getParameter("branchCode");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		String branchCode[]=branch.split(":");
		String sql=null;
		String sql1=null;
		String pattern = "dd/MM/yyyy";
		Integer branchcode=0;
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		NumberToWord obj1=new NumberToWord();
		String sqq="truncate table Temp_CanePricePaymentsStatement";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CanePricePaymentsStatement obj=new Temp_CanePricePaymentsStatement();
		 if(branchCode[1].equals("ALL"))
		 {
		
		sql="select c.bankcode,c.sbaccno,c.sbac,c.ryotcode,r.ryotname,b.branchname from CanAccSBDetails c,ryot r,Branch b  where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and  c.ryotcode=r.ryotcode and c.bankcode=b.branchcode";
		 }
		 else
		 {
			 sql="select c.bankcode,c.sbaccno,c.sbac,c.ryotcode,r.ryotname,b.branchname from CanAccSBDetails c,ryot r,Branch b  where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and  c.ryotcode=r.ryotcode and c.bankcode='"+branchCode[1]+"' and b.branchcode='"+branchCode[1]+"'";	 
		 }
		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			Double gtotal=0.0;
  			obj=new Temp_CanePricePaymentsStatement();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				String accountnum=(String)((Map<Object,Object>)ls.get(k)).get("sbaccno");
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
  				Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("sbac");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				if(totalamount>0)
  				{
  				obj.setAccountnum(accountnum);
  				obj.setBranch(branchname);
  				obj.setBranchcode(branchcode);
  				obj.setTotalamount((double)Math.round(totalamount));
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				
  				sql1="select  sum(sbac) gtotal from CanAccSBDetails   where caneacctslno="+cslnum[1]+" and season='"+season1[1]+"' and bankcode="+branchcode+"";
  	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		if(ls1.size()>0 && ls1!=null)
			{
  	  		 gtotal=(Double)((Map<Object,Object>)ls1.get(0)).get("gtotal");
			}
  	  		
	       // obj1.inputNumber();
	        String numword=obj1.convertNumberToWords(gtotal.intValue());
  			//String word=EnglishNumberToWords.convert(gtotal.longValue());
  			obj.setGtotalwords(numword);
  			String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  			session.save(obj);
  				}
  			}
		}
  		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CanePricePaymentsStatementReport";
  			String Query1="distinct *  from Temp_CanePricePaymentsStatement order by branch,ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("SubTitle", "CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate",format.format(new Date()));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateCanePaymentVoucher", method = RequestMethod.POST)
	public void generateCanePaymentVoucher(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CanePaymentVoucher";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CanePaymentVoucher obj=new Temp_CanePaymentVoucher();
	
		String sql="select c.*,r.ryotname,r.relativename,v.village from CanAccSBDetails c,ryot r,village v where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and c.ryotcode=r.ryotcode and r.villagecode=v.villagecode ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			
  			obj=new Temp_CanePaymentVoucher();
  			Double paidamt=0.0;
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Double advamt=(Double)((Map<Object,Object>)ls.get(k)).get("advamt");
  				Double cdcamt=(Double)((Map<Object,Object>)ls.get(k)).get("cdcamt");
  				Double ded=(Double)((Map<Object,Object>)ls.get(k)).get("ded");
  				Double harvesting=(Double)((Map<Object,Object>)ls.get(k)).get("harvesting");
  				Double loanamt=(Double)((Map<Object,Object>)ls.get(k)).get("loanamt");
  				Double otherded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
  				Double suppqty=(Double)((Map<Object,Object>)ls.get(k)).get("suppqty");
  				paidamt=(Double)((Map<Object,Object>)ls.get(k)).get("paidamt");
  				Double transport=(Double)((Map<Object,Object>)ls.get(k)).get("transport");
  				Double ulamt=(Double)((Map<Object,Object>)ls.get(k)).get("ulamt");
  				Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamt");
  				String ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				
  				
  				
  				Double otherdedu=cdcamt+harvesting+transport+ulamt+otherded;
  				Double loans=loanamt+advamt;
  				obj.setPayamount(paidamt);
  				obj.setOtherded(otherdedu);
  				obj.setLoan(loans);
  				obj.setTotalamount(totalamount);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setVillage(village);
  				obj.setSuppqty(suppqty);
  				obj.setRelativename(relativename);
  			}
  	  		
  			String word=EnglishNumberToWords.convert(paidamt.longValue());
  			obj.setGtotalwords(word);
  			String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  			session.save(obj);
		}
  		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CanePaymentVoucherReport";
  			String Query1="distinct *  from Temp_CanePaymentVoucher";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate",format.format(new Date()));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}

	////////////
	
	@RequestMapping(value = "/generateCanePaymentReport", method = RequestMethod.POST)
	public void generateCanePaymentReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CanePaymentReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_CanePaymentReport obj=new Temp_CanePaymentReport();
	
		String sql="select c.*,r.ryotname,r.relativename,r.circlecode,v.village from CanAccSBDetails c,ryot r,village v where c.caneacctslno="+cslnum[1]+" and c.season='"+season1[1]+"' and c.ryotcode=r.ryotcode and r.villagecode=v.villagecode order by c.ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		for (int k = 0; k <ls.size(); k++) 
		{
  			double totalAdvanceAmt = 0.0;
  			obj=new Temp_CanePaymentReport();
  			Double paidamt=0.0;
  			String ryotcode=null;
  			double advAmt = 0.0;
  			Date dateto=null;
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Double cdcamt=(Double)((Map<Object,Object>)ls.get(k)).get("cdcamt");
  				Double harvesting=(Double)((Map<Object,Object>)ls.get(k)).get("harvesting");
  				Double otherded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
  				Double suppqty=(Double)((Map<Object,Object>)ls.get(k)).get("suppqty");
  				paidamt=(Double)((Map<Object,Object>)ls.get(k)).get("paidamt");
  				Double transport=(Double)((Map<Object,Object>)ls.get(k)).get("transport");
  				Double ulamt=(Double)((Map<Object,Object>)ls.get(k)).get("ulamt");
  				Double totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamt");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode");
  				Double sbac=(Double)((Map<Object,Object>)ls.get(k)).get("sbac");
  				Integer bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				String sbaccno=(String)((Map<Object,Object>)ls.get(k)).get("sbaccno");
  				Double loanamt=(Double)((Map<Object,Object>)ls.get(k)).get("loanamt");
  				
  				totalAdvanceAmt = (Double)((Map<Object,Object>)ls.get(k)).get("advamt");
  				//obj.setAdvisedamt(loanamt); 
  				obj.setBankcode(bankcode);
  				obj.setSbaccno(sbaccno);
  				obj.setCdcamt(cdcamt);
  				obj.setOtherded(otherded);
  				obj.setHarvesting(harvesting);
  				obj.setTransport(transport);
  				obj.setUlamt(ulamt);
  				obj.setPayamount(sbac);
  				obj.setPaid(paidamt);
  				obj.setTotalamount(totalamount);
  				obj.setRyotcode(ryotcode);
  				obj.setCirclecode(circlecode);
  				obj.setRyotname(ryotname);
  				obj.setVillage(village);
  				obj.setSuppqty(suppqty);
  				obj.setRelativename(relativename);
  			}
  			String word=EnglishNumberToWords.convert(paidamt.longValue());
  			obj.setGtotalwords(word);
  		
  			
  			String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  	  		
  	  		String sql3="select sum(netwt) netwt  from WeighmentDetails  where  season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and canereceiptenddate <='"+dateto+"'";
  	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  	  		if(ls3.size()>0 && ls3!=null)
  	  		{
  	  			Double gnetwt=(Double)((Map<Object,Object>)ls3.get(0)).get("netwt");
  	  			obj.setGnetwt(gnetwt);
  	  		}
  	  		
  	  		String sql4="select isnull(sum(advisedamt),0) loantotal,accountnum,isnull(sum(finalprinciple),0) outstanding from CanAccLoansDetails  where  season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and advloan=0  and caneacctslno="+cslnum[1]+" group by accountnum";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		if(ls4.size()>0 && ls4!=null)
	  		{
	  			
	  			for (int i = 0; i <ls4.size(); i++) 
	  			{
	  				String accountnum=(String)((Map<Object,Object>)ls4.get(i)).get("accountnum");
	  				if("8".equals(accountnum))
	  				{
	  					Double seedAdv=(Double)((Map<Object,Object>)ls4.get(i)).get("loantotal");
	  					Double seedAdvoutstanding=(Double)((Map<Object,Object>)ls4.get(i)).get("outstanding");
	  					advAmt += seedAdv;
	  					obj.setSeedtotal(seedAdv);
	  					obj.setSeedoutstanding(seedAdvoutstanding);
	  				}
	  				if("7".equals(accountnum))
	  				{
	  					Double biototal=(Double)((Map<Object,Object>)ls4.get(i)).get("loantotal");
	  					Double biooutstanding=(Double)((Map<Object,Object>)ls4.get(i)).get("outstanding");
	  					advAmt += biototal;

	  					obj.setBiototal(biototal);
	  					obj.setBiooutstanding(biooutstanding);
	  				}
	  				if("9".equals(accountnum))
	  				{
	  					Double harvtotal=(Double)((Map<Object,Object>)ls4.get(i)).get("loantotal");
	  					Double harvtotaloutstanding=(Double)((Map<Object,Object>)ls4.get(i)).get("outstanding");
	  					advAmt += harvtotal;

	  					obj.setHarvtotal(harvtotal);
	  					obj.setHarvestoutstanding(harvtotaloutstanding);
	  				}
	  			}
	  		}
	  		
	  		double otherAdvancesAmt = 0.0;
	  		String qry="select isnull(sum(advisedamt),0) loantotal from CanAccLoansDetails  where  season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and advloan=0  and caneacctslno="+cslnum[1]+" and accountnum not in (7,8,9)";
	  		List otherAdvances=hibernatedao.findBySqlCriteria(qry);
	  		if(otherAdvances.size()>0 && otherAdvances!=null)
	  		{
	  			otherAdvancesAmt=(Double)((Map<Object,Object>)otherAdvances.get(0)).get("loantotal");
	  			int canAccSlNO = Integer.parseInt(cslnum[1]);
	  			if(canAccSlNO == 4 && "06020258".equals(ryotcode))
	  			{
	  				otherAdvancesAmt = 0;
	  			}
	  		}
	  		obj.setOtheradvances(otherAdvancesAmt);
	  		
	  		String sql5="select isnull(sum(advisedamt),0) loantotal,isnull(sum(finalprinciple),0) outstanding from CanAccLoansDetails  where  season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and advloan=1  and caneacctslno="+cslnum[1]+"";
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		if(ls5.size()>0 && ls5!=null)
	  		{
	  			Double loanPrinciple=(Double)((Map<Object,Object>)ls5.get(0)).get("loantotal");
	  			int canAccSlNO = Integer.parseInt(cslnum[1]);
	  			if(canAccSlNO == 4 && "06020258".equals(ryotcode))
	  			{
	  				double val = 2400.0;
	  				loanPrinciple = loanPrinciple+val;
	  			}
	  			Double loanOutStanding=(Double)((Map<Object,Object>)ls5.get(0)).get("outstanding");

	  			obj.setLoantotal(loanPrinciple);
				obj.setLoanoutstanding(loanOutStanding);
	  		}
  	  		session.save(obj);
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CanePaymentReport";
  			String Query1="distinct *  from Temp_CanePaymentReport order by ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD., CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate",format.format(new Date()));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	///////////////
	
	///////////////
	@RequestMapping(value = "/generateCaneAccountCheckList", method = RequestMethod.POST)
	public void generateCaneAccountCheckList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_CaneAccDetails";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneAccDetails obj=new Temp_CaneAccDetails();
		//Modified by DMurty on 10-02-2017
		String sql=" select c.*,cs.otherded,cs.cdcamt,cs.harvesting,cs.transport,cs.ulamt,cs.sbac,cs.sbaccno,cs.bankcode,b.branchname from CaneValueSplitup c,CanAccSBDetails cs,branch b where cs.bankcode=b.branchcode and c.caneacslno='"+cslnum[1]+"' and c.season='"+season1[1]+"' and c.season=cs.season and cs.caneacctslno='"+cslnum[1]+"' and c.ryotcode=cs.ryotcode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		Session session=hibernatedao.getSessionFactory().openSession();
  		String ryotcode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			obj=new Temp_CaneAccDetails();
  			if(ls.size()>0 && ls!=null)
  			{	 
  				Integer bankcode=(Integer)((Map<Object,Object>)ls.get(k)).get("bankcode");
  				String accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("sbaccno");
  				String branchname=(String)((Map<Object,Object>)ls.get(k)).get("branchname");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double suppliedqty=(Double)((Map<Object,Object>)ls.get(k)).get("canesupplied");
  				Double totalamt=(Double)((Map<Object,Object>)ls.get(k)).get("totalvalue");
  				Double tp=(Double)((Map<Object,Object>)ls.get(k)).get("transport");
  				Double harvest=(Double)((Map<Object,Object>)ls.get(k)).get("harvesting");
  				//Modified by DMurty on 10-02-2017
  				Double sb=(Double)((Map<Object,Object>)ls.get(k)).get("sbac");
  				Double ul=(Double)((Map<Object,Object>)ls.get(k)).get("ulamt");
  				Double cdc=(Double)((Map<Object,Object>)ls.get(k)).get("cdcamt");
  				Double otherded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
  				obj.setOtherded(otherded);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setAccountnumber(accountnumber);
  				obj.setBankcode(bankcode);
  				obj.setSuppliedqty(suppliedqty);
  				obj.setTotalamt(totalamt);
  				obj.setTp(tp);
  				obj.setHarvest(harvest);
  				obj.setSb(sb);
  				obj.setUl(ul);
  				obj.setCdc(cdc);
  				obj.setBranchname(branchname);
  			}	
  			String sql2="select  datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
  	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  	  		if(ls2.size()>0 && ls2!=null)
			{
  	  			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  	  			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
  	  			String ftdate=fdate+" To "+tdate;
  	  			obj.setBetweendate(ftdate);
			}
  	  		String sql3 = "select sum(paidamt) as amt,sum(interestamount) as intrest from CanAccLoansDetails where ryotcode='"+ryotcode+"' and advloan=1 and season='"+season1[1]+"' and caneacctslno='"+cslnum[1]+"' ";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		Double loanAmt=0.0;
	  		Double intrest=0.0;
	  		if(ls3.size()>0 && ls3!=null)
	  		{
	  			loanAmt=(Double)((Map<Object,Object>)ls3.get(0)).get("amt");
	  			if(loanAmt==null)
	  			{
	  				loanAmt=0.0;
	  			}
	  			obj.setLoan(loanAmt);
	  			intrest=(Double)((Map<Object,Object>)ls3.get(0)).get("intrest");
	  			if(intrest==null)
	  			{
	  				intrest=0.0;
	  			}
	  			obj.setIntrest(intrest);
	  		}
	  		
	  		String sql4 = "select sum(paidamt) as advance from CanAccLoansDetails where ryotcode='"+ryotcode+"' and advloan=0 and season='"+season1[1]+"' and caneacctslno='"+cslnum[1]+"'";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		Double advances=0.0;
	  		if(ls4.size()>0 && ls4!=null)
	  		{
	  			advances=(Double)((Map<Object,Object>)ls4.get(0)).get("advance");
	  			if(advances==null)
	  			{
	  				advances=0.0;
	  			}
	  			obj.setAdvances(advances);
	  		}
	  		String sql5 = "select accountnum,branchcode from CanAccLoansDetails where ryotcode='"+ryotcode+"' and advloan=1 and season='"+season1[1]+"' and  caneacctslno='"+cslnum[1]+"'";
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		
	  		if(ls5.size()>0 && ls5!=null)
	  		{
	  			Integer Lbranchcode=(Integer)((Map<Object,Object>)ls5.get(0)).get("branchcode");
	  			String Laccountnum=(String)((Map<Object,Object>)ls5.get(0)).get("accountnum");
	  			obj.setLaccountnum(Laccountnum);
	  			obj.setLbranchcode(Lbranchcode);
	  		}
	  		
	  		
  	  	session.save(obj);
		}
  		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="CaneAccountCheckReport";
  			String Query1="distinct * from Temp_CaneAccDetails order by ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}

	@RequestMapping(value = "/generateTransportContractorDtails", method = RequestMethod.POST)
	public void generateTransportContractorDtails(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String Transporter=request.getParameter("Transporter");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String transporterCode[]=Transporter.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_TransportContractorDtails";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_TransportContractorDtails obj=new Temp_TransportContractorDtails();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql=null;
		if("0".equals(transporterCode[1]))
		{
		sql=" select distinct t.rate,t.transportcontcode,t.totalamount, t.ryotcode,t.transportedcanewt,t.receiptdate,w.ryotname,c.circle,tc.transporter,z.zone from TranspChgSmryByRyotContAndDate t,WeighmentDetails w,circle c,TransportingContractors tc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and t.ryotcode=w.ryotcode and t.season='"+season1[1]+"' and t.receiptdate  between '"+fromdate+"' and '"+todate+"' and w.canereceiptenddate=t.receiptdate  and t.transportcontcode=tc.transportercode ";
	  	}
		else
		{
			sql=" select distinct t.rate,t.transportcontcode,t.totalamount, t.ryotcode,t.transportedcanewt,t.receiptdate,w.ryotname,c.circle,tc.transporter,z.zone from TranspChgSmryByRyotContAndDate t,WeighmentDetails w,circle c,TransportingContractors tc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and t.ryotcode=w.ryotcode and t.season='"+season1[1]+"' and t.receiptdate  between '"+fromdate+"' and '"+todate+"' and w.canereceiptenddate=t.receiptdate and t.transportcontcode='"+transporterCode[1]+"' and tc.transportercode='"+transporterCode[1]+"'";
		}
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls!=null)
		{
  			Integer shiftid=null;
  	  		for (int k = 0; k <ls.size(); k++) 
  			{
  	  			
  				obj=new Temp_TransportContractorDtails();
  				Integer transportcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("transportcontcode");
  				String 	ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double 	netwt=(Double)((Map<Object,Object>)ls.get(k)).get("transportedcanewt");
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");
  				String 	transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				Double 	totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamount");
  				totalamount = agricultureHarvestingController.round(totalamount);
  				Double 	rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				obj.setZone(zone);
  				obj.setTransportcontcode(transportcontcode);
  				obj.setRate(rate);
  				obj.setTotalamount(totalamount);
  	  			obj.setCircle(circle);
  	  			obj.setTransporter(transporter);
  	  			obj.setNetwt(netwt);
  	  			obj.setRyotcode(ryotcode);
  	  			obj.setRyotname(ryotname);
  	  		session.save(obj);
  			}
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="TransporterContractorRyotWiseReport";
  			String Query1="zone,transportcontcode,totalamount,rate,transporter,ryotcode,ryotname,circle,netwt from Temp_TransportContractorDtails  order by transportcontcode,ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", fromDate+" TO "+toDate);

	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generatePaymentCheckList", method = RequestMethod.POST)
	public void generatePaymentCheckList(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_PaymentCheckList";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_PaymentCheckList obj=new Temp_PaymentCheckList();
		Session session=hibernatedao.getSessionFactory().openSession();
		String ryotcode=null;
		String	sql=" select distinct w.circlecode,w.zonecode,w.ryotcode,w.ryotname,z.zone,v.village from WeighmentDetails w,zone z,village v where w.season='"+season1[1]+"' and w.canereceiptenddate between '"+fromdate+"' and '"+todate+"' and w.zonecode=z.zonecode and w.villagecode=v.villagecode";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  	  	for (int k = 0; k <ls.size(); k++) 
  		{
  	  		
  	  		if(ls.size()>0 && ls!=null)
  	  		{	
  				obj=new Temp_PaymentCheckList();
  				Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode");
  				Integer circlecode=(Integer)((Map<Object,Object>)ls.get(k)).get("circlecode");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
  				obj.setVillage(village);
  				obj.setCirclecode(circlecode);
  				obj.setZone(zone);
  				obj.setZonecode(zonecode);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  	  		}
  	  		
	  	  		
			String agreementNo = caneAccountingFunctionalService.getAgreementNoByDates(fromdate,todate,season1[1],ryotcode);
			logger.info("getCaneUpdationCheckList()========agreementNo=========="+agreementNo);
			String accountnumber =null;
			int branchCode = 0;
			String bankName = null;
			List ryotBankList=caneAccountingFunctionalService.getRyotSbAccNo(ryotcode,season1[1],agreementNo);
			if(ryotBankList!=null)
			{
				AgreementSummary agreement = (AgreementSummary)ryotBankList.get(0);
				accountnumber = agreement.getAccountnumber();
				branchCode = agreement.getBranchcode();
				logger.info("========getRyotSbAccNo() accountnumber=========="+accountnumber);
			}
			
			obj.setAccountnumber(accountnumber);
			obj.setBank(branchCode);
  	  		
  	  		String	sql2="select sum(netwt) netwt  from WeighmentDetails  where ryotcode='"+ryotcode+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double netwt=0.0;
	  		Double totalvalue=0.0;
	  		if(ls2.size()>0 && ls2!=null)
  	  		{
	  			netwt=(Double) ((Map<Object,Object>)ls2.get(0)).get("netwt");
	  			obj.setCanesupplied(netwt);
  	  		}
	  		totalvalue=2765*netwt;
	  		totalvalue = agricultureHarvestingController.round(totalvalue);

	  		obj.setTotalvalue(totalvalue);
	  		double deductions = 0.0;
	  		double tp = 0.0;
  			double harvest = 0.0;
	  		//Added by DMurty on 12-01-2017
	  		String tpqry = "select sum(totalamount) as tamt from TranspChgSmryByRyotContAndDate where ryotcode='"+ryotcode+"' and receiptdate between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'";
	  		List TpDedList=hibernatedao.findBySqlCriteria(tpqry);
	  		if(TpDedList.size()>0 && TpDedList != null)
  	  		{
	  			Map dedMap = new HashMap();
	  			dedMap = (Map) TpDedList.get(0);
	  			
	  			if((Double) dedMap.get("tamt") != null)
	  			{
	  				tp = (Double) dedMap.get("tamt");
	  				tp = agricultureHarvestingController.round(tp);
	  			}
	  			obj.setTp(tp);
  	  		}
	  		String hqry = "select sum(totalamount) as hamt from HarvChgDtlsByRyotContAndDate where ryotcode='"+ryotcode+"' and receiptdate between '"+fromdate+"' and '"+todate+"' and season='"+season1[1]+"'";
	  		List HDedList=hibernatedao.findBySqlCriteria(hqry);
	  		if(HDedList.size()>0 && HDedList != null)
  	  		{
	  			Map dedMap = new HashMap();
	  			dedMap = (Map) HDedList.get(0);
	  			if((Double) dedMap.get("hamt") != null)
	  			{
	  				harvest = (Double) dedMap.get("hamt");
	  				harvest = agricultureHarvestingController.round(harvest);
	  			}
  	  			obj.setHarvesting(harvest);
  	  		}
	  		double cdc = 0.0;
	  		double ul = 0.0;
	  		
	  		cdc = 4*netwt;
  			cdc = agricultureHarvestingController.round(cdc);
  			
  			ul = 11*netwt;
  			ul = agricultureHarvestingController.round(ul);	
  			
  			obj.setCdc(cdc);
  			obj.setUl(ul);
  			obj.setOtherded(0.0);
	  			
  	  		deductions = tp+harvest+cdc+ul;
  	  		
  	  		double ryotLoanAmount = 0.0;
  	  		List loanList=caneAccountingFunctionalService.getLoanDetails(season1[1],ryotcode);
  	  		logger.info("generatePaymentCheckList()------loanList"+loanList);
  	  		double months=0.00;
  	  		if(loanList!=null)
  	  		{
  	  			for(int j=0;j<loanList.size();j++)
  	  			{
  	  				
  	  				Map loanMap=(Map)loanList.get(j);
  	  				Double loanAmount = 0.00;						
  	  				Date loanDate=null;
  	  				int branchcode=(Integer)loanMap.get("branchcode");
  	  				String loanAccNo = (String) loanMap.get("referencenumber");
  	  				int ryotLoanNo=(Integer)loanMap.get("loannumber");
  	  				
  	  				List principleList=caneAccountingFunctionalService.getLoanCalcDetailsForPaymentChecklist(season1[1],ryotcode,branchcode,ryotLoanNo,todate);
  	  				logger.info("generatePaymentCheckList()------ principleList"+principleList);
  	  				logger.info("LoanList------ ryotCode"+ryotcode);
  	  				if(principleList!=null)
  	  				{
  	  					Map principleMap=(Map)principleList.get(0);
  	  					if(principleMap.get("principle")==null)
  	  						loanAmount=0.00;
  	  					else
  	  						loanAmount=(Double)principleMap.get("principle");
  	  					Date lDate=(Date)principleMap.get("principledate");
  	  					
  	  					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
  	  					String strlDate = df1.format(lDate);
					
  	  					if(lDate != null)
  	  					{
  	  						loanDate=DateUtils.getSqlDateFromString(strlDate,Constants.GenericDateFormat.DATE_FORMAT);
  	  					}
  	  					else
  	  					{
  	  						loanDate = new Date();
  	  					}		
  	  					int loanNo = (Integer) loanMap.get("loannumber");
  	  					double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotcode,season1[1],branchcode,loanNo);
  	  					long earliertime=loanDate.getTime();
  	  					//Date curDate = new Date();
  	  					Calendar cal = Calendar.getInstance();
  	  					
  	  					Long latertime= todate.getTime();
  	  					
  	  					long days=DateUtils.dateDifference(latertime, earliertime);
  	  					days = Math.abs(days);
  	  					Long ldays = new Long(days);
  	  					double dDays = ldays.doubleValue();
  	  					if(previousIntrest == 0)
  	  					{
  	  						dDays = dDays+1;
  	  					}
  	  					months=dDays/30;
  	  					months = Math.round(months * 100.0) / 100.0;							
  	  					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(loanAmount,branchcode,season1[1]);
  	  					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

  	  					if(interestRate==null)
  	  						interestRate=0.00;
			
  	  					if(loanAmount==null)
  	  						loanAmount=0.00;
			
  	  					double intrate = interestRate/100;
					
  	  					double intrestPerDay = 	loanAmount*intrate/365;
  	  					double intrestAmt = intrestPerDay*dDays;
  	  					intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;

  	  					Double totalAmoutOnInterest=loanAmount+intrestAmt;
  	  					totalAmoutOnInterest = agricultureHarvestingController.round(totalAmoutOnInterest);

  	  					ryotLoanAmount += totalAmoutOnInterest;	
  	  					//obj.setLoan(ryotLoanAmount);
  	  				}
  	  			}
  	  		}
  	  		
  	  	double ryotAdvanceAmount = 0.0;
  	    List advancesList=caneAccountingFunctionalService.getAllAdvances(fromdate,todate,ryotcode,season);
		logger.info("generatePaymentCheckList()------advancesList"+advancesList);
		if(advancesList != null && advancesList.size()>0)
		{
			for(int j=0;j<advancesList.size();j++)
			{		
				
				Map advListMap=(Map)advancesList.get(j);
				Double principle=0.00;
				Date principleDate=null;
				double nDays = 0.0;
				List principleList=caneAccountingFunctionalService.getAdvanceCalcDetailsForPaymentChecklist(season1[1],ryotcode,(Integer)advListMap.get("advancecode"),todate);
				logger.info("generatePaymentCheckList()------ principleList"+principleList);
				if(principleList!=null)
				{
					Map principleMap=(Map)principleList.get(0);
					
					if(principleMap.get("principle")==null)
						principle=0.00;
					else
						principle=(Double)principleMap.get("principle");
					
					principleDate=(Date)principleMap.get("principledate");					
					//caneReceiptDate=(Date)caneAccountingFunctionalService.getCaneReceiptDate(season,caneValueMap.get("ryotcode").toString());
					long earliertime=principleDate.getTime();
					
					Calendar cal = Calendar.getInstance();
	  				Long latertime= todate.getTime();
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					nDays = dDays+1;
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
				}
				Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,(Integer)advListMap.get("advancecode"));
				logger.info("generatePaymentCheckList()------Advance interest"+interest);
				if(interest==null)
					interest=0.00;
				double intrate = interest/100;
				double intrestPerDay = 	principle*intrate/365;
				double intrestAmt = intrestPerDay*nDays;
				intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;
				Double total=principle+intrestAmt;
				total = agricultureHarvestingController.round(total);
				ryotAdvanceAmount +=total;
				obj.setAdvances(ryotAdvanceAmount);
			}
		}
		Double sb=totalvalue-deductions-ryotLoanAmount-ryotAdvanceAmount;
		/*double val = totalvalue-deductions-ryotAdvanceAmount;
		if(ryotLoanAmount>val)
		{
			obj.setLoan(val);
		}
		else
		{
			obj.setLoan(ryotLoanAmount);
		}*/
		
		if(sb>0)
		{
			sb = agricultureHarvestingController.round(sb);
			obj.setSb(sb);
		}
		session.save(obj);
  		}
  	 
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="PaymentCheckListReport";
  			String Query1="distinct * from Temp_PaymentCheckList order by zonecode,ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("fromdate", fromDate);
	        hmParams.put("todate", toDate);

	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	
	@RequestMapping(value = "/generateRyotWiseWeighmentDetail", method = RequestMethod.POST)
	public void generateRyotWiseWeighmentDetail(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		//String fromDate=request.getParameter("fromDate");
		String ryotCd=request.getParameter("ryotCode");
		String season=request.getParameter("season");
		
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String ryotCode[]=ryotCd.split(":");
		//Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		//Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		//String pattern = "dd/MM/yyyy";
		//SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_RyotWiseWeighmentReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_RyotWiseWeighmentReport obj=new Temp_RyotWiseWeighmentReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		//String sql=null;
		String ryotcode=null;
		String	sql=" select distinct zonecode,ryotcode,ryotname,netwt,canereceiptenddate from WeighmentDetails  where season='"+season1[1]+"' and ryotcode='"+ryotCode[1]+"' ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  	  	for (int k = 0; k <ls.size(); k++) 
  		{
  	  		if(ls.size()>0 && ls!=null)
  	  		{	
  				obj=new Temp_RyotWiseWeighmentReport();
  				Integer zonecode=(Integer)((Map<Object,Object>)ls.get(k)).get("zonecode");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double netwt=(Double)((Map<Object,Object>)ls.get(k)).get("netwt");
  				Date cdate=(Date)((Map<Object,Object>)ls.get(k)).get("canereceiptenddate");	
  	  			String hdate=DateUtils.formatDate(cdate,Constants.GenericDateFormat.DATE_FORMAT);
  	  			obj.setWdate(hdate);
  				obj.setCanesupplied(netwt);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  	  		}
  	  		session.save(obj);
  		}
  	 
  	  	session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="RyotWiseWeighmentReport";
  			String Query1="distinct * from Temp_RyotWiseWeighmentReport  order by wdate";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD., CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateTransportContractorCumulative", method = RequestMethod.POST)
	public void generateTransportContractorCumulative(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String Transporter=request.getParameter("Transporter");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String transporterCode[]=Transporter.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_TransportContractorDtails";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_TransportContractorDtails obj=new Temp_TransportContractorDtails();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql=null;
		String 	ryotcode=null;
		if("0".equals(transporterCode[1]))
		{
		sql=" select distinct t.rate,t.transportcontcode,t.ryotcode,w.ryotname,c.circle,tc.transporter,z.zone from TranspChgSmryByRyotContAndDate t,WeighmentDetails w,circle c,TransportingContractors tc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and t.ryotcode=w.ryotcode and t.season='"+season1[1]+"' and t.receiptdate  between '"+fromdate+"' and '"+todate+"'  and w.canereceiptenddate=t.receiptdate  and t.transportcontcode=tc.transportercode ";
	  	}
		else
		{
			sql=" select distinct t.rate,t.transportcontcode,t.ryotcode,w.ryotname,c.circle,tc.transporter,z.zone from TranspChgSmryByRyotContAndDate t,WeighmentDetails w,circle c,TransportingContractors tc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and t.ryotcode=w.ryotcode and t.season='"+season1[1]+"' and t.receiptdate  between '"+fromdate+"' and '"+todate+"' and w.canereceiptenddate=t.receiptdate and t.transportcontcode='"+transporterCode[1]+"' and tc.transportercode='"+transporterCode[1]+"'";
		}
  		List ls=hibernatedao.findBySqlCriteria(sql);
  	  	for (int k = 0; k <ls.size(); k++) 
  		{
  	  		if(ls.size()>0 && ls!=null)
  	  		{
  	  			//Note this model is used for another report 
  				obj=new Temp_TransportContractorDtails();
  				Integer transportcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("transportcontcode");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");
  				String 	transporter=(String)((Map<Object,Object>)ls.get(k)).get("transporter");
  				Double 	rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				obj.setZone(zone);
  				obj.setTransportcontcode(transportcontcode);
  				obj.setRate(rate);
  	  			obj.setCircle(circle);
  	  			obj.setTransporter(transporter);
  	  			obj.setRyotcode(ryotcode);
  	  			obj.setRyotname(ryotname);
  	  		}
  	  		String	sql1="select sum(transportedcanewt) as netwt,sum(totalamount) as totalamount from TranspChgSmryByRyotContAndDate where ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and receiptdate  between '"+fromdate+"' and '"+todate+"'  ";
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		
	  		if(ls1.size()>0 && ls1!=null)
	  		{
	  			Double 	netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt");
	  			Double 	totalamount=(Double)((Map<Object,Object>)ls1.get(0)).get("totalamount");
	  			totalamount = agricultureHarvestingController.round(totalamount);
	  			obj.setNetwt(netwt);
	  			obj.setTotalamount(totalamount);
	  		}
  	  		session.save(obj);
		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="TransporterContractorRyotWiseCumulative";
  			String Query1="zone,transportcontcode,totalamount,rate,transporter,ryotcode,ryotname,circle,netwt from Temp_TransportContractorDtails  order by transportcontcode,ryotcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generatePaymentCheckListPeriod", method = RequestMethod.POST)
	public void generatePaymentCheckListPeriod(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		
		String rptFormat = request.getParameter("mode");  
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_CaneSupplyCheckListForWeek";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneSupplyCheckListForWeek obj=new Temp_CaneSupplyCheckListForWeek();
		Session session=hibernatedao.getSessionFactory().openSession();
		String ryotcode=null;
		Double cdc=0.0;
		Double ul=0.0;
		String sql3=" select amount,stdDedCode from StandardDeductions  where stdDedCode = 1 or stdDedCode= 2";
  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  		for (int h = 0; h <ls3.size(); h++) 
  		{
  	  		if(ls3.size()>0 && ls3!=null)
  	  		{	
  	  			Integer stdDedCode=(Integer)((Map<Object,Object>)ls3.get(h)).get("stdDedCode");
  	  			if(stdDedCode==1)
  	  			{
  	  				cdc=(Double) ((Map<Object,Object>)ls3.get(h)).get("amount");
  	  			}
  	  			if(stdDedCode==2)
  	  			{
  	  				ul=(Double) ((Map<Object,Object>)ls3.get(h)).get("amount");
  	  			}
  	  			
  	  		}
  		}
		String sql=" select distinct ryotcode,ryotname from WeighmentDetails where season='"+season1[1]+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		
  	  	for (int k = 0; k <ls.size(); k++) 
  		{
  	  		if(ls.size()>0 && ls!=null)
  	  		{	
  				obj=new Temp_CaneSupplyCheckListForWeek();
  				
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  	  		}
  	  		String sql10=" select distinct branchcode,accountnumber from AgreementSummary  where seasonyear='"+season1[1]+"' and ryotcode='"+ryotcode+"' ";
  	  		List ls10=hibernatedao.findBySqlCriteria(sql10);
  	  		Integer branchCode=(Integer)((Map<Object,Object>)ls10.get(0)).get("branchcode");
			String 	accountnumber=(String)((Map<Object,Object>)ls10.get(0)).get("accountnumber");
				obj.setAcnumber(accountnumber);
				obj.setBkcode(branchCode);
  	  		String	sql2="select sum(netwt) netwt  from WeighmentDetails  where ryotcode='"+ryotcode+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double netwt=0.0;
	  		Double totalvalue=0.0;
	  		if(ls2.size()>0 && ls2!=null)
  	  		{
	  			netwt=(Double) ((Map<Object,Object>)ls2.get(0)).get("netwt");
	  			obj.setCanesupplied(netwt);
  	  		}
	  		 totalvalue=2765*netwt;
	  		// totalvalue = agricultureHarvestingController.round(totalvalue);
	  		 obj.setTotalvalue(totalvalue);
	  		
  	  		String	sql1="select sum (totalamount) hrate from HarvChgDtlsByRyotContAndDate  where ryotcode='"+ryotcode+"' and receiptdate between '"+fromdate+"' and '"+todate+"' ";
  	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  		String	sql4="select sum (totalamount) trate from TranspChgSmryByRyotContAndDate where receiptdate  between '"+fromdate+"' and '"+todate+"' and ryotcode='"+ryotcode+"' ";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		Double trate=(Double) ((Map<Object,Object>)ls4.get(0)).get("trate");
  	  		Double deductions=0.0;
  	  		if(ls1.size()>0 && ls1!=null)
  	  		{
  	  			Double hrate=(Double) ((Map<Object,Object>)ls1.get(0)).get("hrate");
  	  		//	hrate = agricultureHarvestingController.round(hrate);
  	  		//	trate = agricultureHarvestingController.round(trate);
  	  			
  	  			obj.setHrate(hrate);
  	  			obj.setTrate(trate);
  	  			
  	  			double ulAmt = ul*netwt;
  	  		//	ulAmt = agricultureHarvestingController.round(ulAmt);
  	  			obj.setUl(ulAmt);
  	  			
  	  			double cdcAmt = cdc*netwt;
  	  		//	cdcAmt = agricultureHarvestingController.round(cdcAmt);
  	  			obj.setCdc(cdcAmt);
  	  			
  	  			deductions=ulAmt+cdcAmt+hrate+trate;
  	  			obj.setDeductions(deductions);
  	  		}
  	  		double ryotLoanAmount = 0.0;
  	  		List loanList=caneAccountingFunctionalService.getLoanDetails(season1[1],ryotcode);
  	  		logger.info("getAllAdvancesDetails()------loanList"+loanList);
  	  		double months=0.00;
  	  		if(loanList!=null)
  	  		{
  	  			for(int j=0;j<loanList.size();j++)
  	  			{
  	  				
  	  				Map loanMap=(Map)loanList.get(j);
  	  				Double loanAmount = 0.00;						
  	  				Date loanDate=null;
  	  				int branchcode=(Integer)loanMap.get("branchcode");
  	  				String loanAccNo = (String) loanMap.get("referencenumber");
  	  				int ryotLoanNo=(Integer)loanMap.get("loannumber");

  	  				List principleList=caneAccountingFunctionalService.getLoanCalcDetails(season1[1],ryotcode,branchcode,ryotLoanNo);
  	  				logger.info("getAllAdvancesDetails()------ principleList"+principleList);
  	  				logger.info("LoanList------ ryotCode"+ryotcode);
  	  				if(principleList!=null)
  	  				{
  	  					Map principleMap=(Map)principleList.get(0);
  	  					if(principleMap.get("principle")==null)
  	  						loanAmount=0.00;
  	  					else
  	  						loanAmount=(Double)principleMap.get("principle");
  	  					Date lDate=(Date)principleMap.get("principledate");
  	  					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
  	  					String strlDate = df1.format(lDate);
					
  	  					if(lDate != null)
  	  					{
  	  						loanDate=DateUtils.getSqlDateFromString(strlDate,Constants.GenericDateFormat.DATE_FORMAT);
  	  					}
  	  					else
  	  					{
  	  						loanDate = new Date();
  	  					}		
  	  					int loanNo = (Integer) loanMap.get("loannumber");
  	  					double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotcode,season1[1],branchcode,loanNo);
  	  					long earliertime=loanDate.getTime();
  	  					//Date curDate = new Date();
  	  					Calendar cal = Calendar.getInstance();
  	  					
  	  					Long latertime= cal.getTimeInMillis();
  	  					long days=DateUtils.dateDifference(latertime, earliertime);
  	  					days = Math.abs(days);
  	  					Long ldays = new Long(days);
  	  					double dDays = ldays.doubleValue();
  	  					if(previousIntrest == 0)
  	  					{
  	  						dDays = dDays+1;
  	  					}
  	  					months=dDays/30;
  	  					months = Math.round(months * 100.0) / 100.0;							
  	  					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(loanAmount,branchcode,season1[1]);
  	  					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

  	  					if(interestRate==null)
  	  						interestRate=0.00;
			
  	  					if(loanAmount==null)
  	  						loanAmount=0.00;
			
  	  					double intrate = interestRate/100;
					
  	  					double intrestPerDay = 	loanAmount*intrate/365;
  	  					double intrestAmt = intrestPerDay*dDays;
  	  					intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;

  	  					Double totalAmoutOnInterest=loanAmount+intrestAmt;
  	  					totalAmoutOnInterest = agricultureHarvestingController.round(totalAmoutOnInterest);	
  	  					
  	  					ryotLoanAmount += totalAmoutOnInterest;	
  	  					obj.setLoan(ryotLoanAmount);
  	  				}
  	  				//session.save(obj);
  	  			}
  	  		}
  	  		
  	  	double ryotAdvanceAmount = 0.0;
  	    List advancesList=caneAccountingFunctionalService.getAllAdvances(fromdate,todate,ryotcode,season);
		logger.info("getAllAdvancesDetails()------advancesList"+advancesList);
		//double months=0.00;
		if(advancesList != null && advancesList.size()>0)
		{
			for(int j=0;j<advancesList.size();j++)
			{		
				Map advListMap=(Map)advancesList.get(j);
				Double principle=0.00;
				Date principleDate=null;
				double nDays = 0.0;
				List principleList=caneAccountingFunctionalService.getAdvanceCalcDetails(season1[1],ryotcode,(Integer)advListMap.get("advancecode"));
				logger.info("getAllAdvancesDetails()------ principleList"+principleList);
				if(principleList!=null)
				{
					Map principleMap=(Map)principleList.get(0);
					
					if(principleMap.get("principle")==null)
						principle=0.00;
					else
						principle=(Double)principleMap.get("principle");
					
					principleDate=(Date)principleMap.get("principledate");					
					//caneReceiptDate=(Date)caneAccountingFunctionalService.getCaneReceiptDate(season,caneValueMap.get("ryotcode").toString());
					long earliertime=principleDate.getTime();
					Calendar cal = Calendar.getInstance();
	  				Long latertime= cal.getTimeInMillis();
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					nDays = dDays+1;
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
				}
				Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,(Integer)advListMap.get("advancecode"));
				logger.info("getAllAdvancesDetails()------Advance interest"+interest);
				if(interest==null)
					interest=0.00;
				double intrate = interest/100;
				double intrestPerDay = 	principle*intrate/365;
				double intrestAmt = intrestPerDay*nDays;
				intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;
				Double total=principle+intrestAmt;
				total = agricultureHarvestingController.round(total);	
				ryotAdvanceAmount +=total;
				obj.setAdvances(ryotAdvanceAmount);
			}
		}
		Double sb=totalvalue-deductions-ryotLoanAmount-ryotAdvanceAmount;
		if(sb>0)
		{
			sb = agricultureHarvestingController.round(sb);
			obj.setSb(sb);
		}
		session.save(obj);
  		}
  	 
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName =null;
  			if("Excel".equalsIgnoreCase(rptFormat))
  			{
  				reportFileName ="PaymentCheckListPeriodForExcelFormat";
  			}
  			else
  				reportFileName ="PaymentCheckListPeriodReport";
  			String Query1="distinct * from Temp_CaneSupplyCheckListForWeek ";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	/*
	@RequestMapping(value = "/generatePaymentCheckListPeriod", method = RequestMethod.POST)
	public void generatePaymentCheckListPeriod(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_CaneSupplyCheckListForWeek";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneSupplyCheckListForWeek obj=new Temp_CaneSupplyCheckListForWeek();
		Session session=hibernatedao.getSessionFactory().openSession();
		String ryotcode=null;
		Double cdc=0.0;
		Double ul=0.0;
		String sql3=" select amount,stdDedCode from StandardDeductions  where stdDedCode = 1 or stdDedCode= 2";
  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  		for (int h = 0; h <ls3.size(); h++) 
  		{
  	  		if(ls3.size()>0 && ls3!=null)
  	  		{	
  	  			Integer stdDedCode=(Integer)((Map<Object,Object>)ls3.get(h)).get("stdDedCode");
  	  			if(stdDedCode==1)
  	  			{
  	  				cdc=(Double) ((Map<Object,Object>)ls3.get(h)).get("amount");
  	  			}
  	  			if(stdDedCode==2)
  	  			{
  	  				ul=(Double) ((Map<Object,Object>)ls3.get(h)).get("amount");
  	  			}
  	  		}
  		}
		String sql=" select distinct w.ryotcode,w.ryotname,a.branchcode,a.accountnumber from WeighmentDetails w,AgreementSummary a  where w.season='"+season1[1]+"' and w.canereceiptenddate between '"+fromdate+"' and '"+todate+"' and w.agreementnumber=a.agreementnumber ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  	  	for (int k = 0; k <ls.size(); k++) 
  		{
  	  		if(ls.size()>0 && ls!=null)
  	  		{	
  				obj=new Temp_CaneSupplyCheckListForWeek();
  				Integer branchcode=(Integer)((Map<Object,Object>)ls.get(k)).get("branchcode");
  				String 	accountnumber=(String)((Map<Object,Object>)ls.get(k)).get("accountnumber");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				
  				obj.setAcnumber(accountnumber);
  				obj.setBkcode(branchcode);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  	  		}
  	  		String	sql2="select sum(netwt) netwt  from WeighmentDetails  where ryotcode='"+ryotcode+"' and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		Double netwt=0.0;
	  		Double totalvalue=0.0;
	  		if(ls2.size()>0 && ls2!=null)
  	  		{
	  			netwt=(Double) ((Map<Object,Object>)ls2.get(0)).get("netwt");
	  			obj.setCanesupplied(netwt);
  	  		}
	  		totalvalue=2765*netwt;
	  		totalvalue = agricultureHarvestingController.round(totalvalue);
	  		obj.setTotalvalue(totalvalue);
	  		
	  		double deductions = 0.0;
	  		double tp = 0.0;
  			double harvest = 0.0;
	  		//Added by DMurty on 12-01-2017
	  		String tpqry = "select sum(totalamount) as tamt from TranspChgSmryByRyotContAndDate where ryotcode='"+ryotcode+"' and receiptdate between '"+fromdate+"' and '"+todate+"'";
	  		List TpDedList=hibernatedao.findBySqlCriteria(tpqry);
	  		if(TpDedList.size()>0 && TpDedList != null)
  	  		{
	  			Map dedMap = new HashMap();
	  			dedMap = (Map) TpDedList.get(0);
	  			if((Double) dedMap.get("tamt") != null)
	  			{
	  				tp = (Double) dedMap.get("tamt");
	  				tp = agricultureHarvestingController.round(tp);
	  			}
	  			obj.setTrate(tp);
  	  		}
	  		String hqry = "select sum(totalamount) as hamt from HarvChgDtlsByRyotContAndDate where ryotcode='"+ryotcode+"' and receiptdate between '"+fromdate+"' and '"+todate+"'";
	  		List HDedList=hibernatedao.findBySqlCriteria(hqry);
	  		if(HDedList.size()>0 && HDedList != null)
  	  		{
	  			Map dedMap = new HashMap();
	  			dedMap = (Map) HDedList.get(0);
	  			if((Double) dedMap.get("hamt") != null)
	  			{
	  				harvest = (Double) dedMap.get("hamt");
	  				harvest = agricultureHarvestingController.round(harvest);
	  			}
  	  			obj.setHrate(harvest);
  	  		}
	  		double cdcAmt = 0.0;
	  		double ulAmt = 0.0;
	  		
	  		cdcAmt = cdc*netwt;
	  		cdcAmt = agricultureHarvestingController.round(cdcAmt);
  			
	  		ulAmt = ul*netwt;
	  		ulAmt = agricultureHarvestingController.round(ulAmt);	
  			
  			obj.setCdc(cdcAmt);
  			obj.setUl(ulAmt);
	  			
  	  		deductions = tp+harvest+cdcAmt+ulAmt;
	  		obj.setDeductions(deductions);

  	  		double ryotLoanAmount = 0.0;
  	  		List loanList=caneAccountingFunctionalService.getLoanDetails(season1[1],ryotcode);
  	  		logger.info("getAllAdvancesDetails()------loanList"+loanList);
  	  		double months=0.00;
  	  		if(loanList!=null)
  	  		{
  	  			for(int j=0;j<loanList.size();j++)
  	  			{
  	  				
  	  				Map loanMap=(Map)loanList.get(j);
  	  				Double loanAmount = 0.00;						
  	  				Date loanDate=null;
  	  				int branchcode=(Integer)loanMap.get("branchcode");
  	  				String loanAccNo = (String) loanMap.get("referencenumber");
  	  				int ryotLoanNo=(Integer)loanMap.get("loannumber");

  	  				List principleList=caneAccountingFunctionalService.getLoanCalcDetails(season1[1],ryotcode,branchcode,ryotLoanNo);
  	  				logger.info("getAllAdvancesDetails()------ principleList"+principleList);
  	  				logger.info("LoanList------ ryotCode"+ryotcode);
  	  				if(principleList!=null)
  	  				{
  	  					Map principleMap=(Map)principleList.get(0);
  	  					if(principleMap.get("principle")==null)
  	  						loanAmount=0.00;
  	  					else
  	  						loanAmount=(Double)principleMap.get("principle");
  	  					Date lDate=(Date)principleMap.get("principledate");
  	  					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
  	  					String strlDate = df1.format(lDate);
					
  	  					if(lDate != null)
  	  					{
  	  						loanDate=DateUtils.getSqlDateFromString(strlDate,Constants.GenericDateFormat.DATE_FORMAT);
  	  					}
  	  					else
  	  					{
  	  						loanDate = new Date();
  	  					}		
  	  					int loanNo = (Integer) loanMap.get("loannumber");
  	  					double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotcode,season1[1],branchcode,loanNo);
  	  					long earliertime=loanDate.getTime();
  	  					
  	  					Long latertime= todate.getTime();
  	  					long days=DateUtils.dateDifference(latertime, earliertime);
  	  					days = Math.abs(days);
  	  					Long ldays = new Long(days);
  	  					double dDays = ldays.doubleValue();
  	  					if(previousIntrest == 0)
  	  					{
  	  						dDays = dDays+1;
  	  					}
  	  					months=dDays/30;
  	  					months = Math.round(months * 100.0) / 100.0;							
  	  					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(loanAmount,branchcode);
  	  					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

  	  					if(interestRate==null)
  	  						interestRate=0.00;
			
  	  					if(loanAmount==null)
  	  						loanAmount=0.00;
			
  	  					double intrate = interestRate/100;
					
  	  					double intrestPerDay = 	loanAmount*intrate/365;
  	  					double intrestAmt = intrestPerDay*dDays;
  	  					intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;

  	  					Double totalAmoutOnInterest=loanAmount+intrestAmt;
  	  					totalAmoutOnInterest = agricultureHarvestingController.round(totalAmoutOnInterest);	
  	  					
  	  					ryotLoanAmount += totalAmoutOnInterest;	
  	  					obj.setLoan(ryotLoanAmount);
  	  				}
  	  				//session.save(obj);
  	  			}
  	  		}
  	  		
  	  	double ryotAdvanceAmount = 0.0;
  	    List advancesList=caneAccountingFunctionalService.getAllAdvances(fromdate,todate,ryotcode);
		logger.info("getAllAdvancesDetails()------advancesList"+advancesList);
		//double months=0.00;
		if(advancesList != null && advancesList.size()>0)
		{
			for(int j=0;j<advancesList.size();j++)
			{		
				Map advListMap=(Map)advancesList.get(j);
				Double principle=0.00;
				Date principleDate=null;
				double nDays = 0.0;
				List principleList=caneAccountingFunctionalService.getAdvanceCalcDetails(season1[1],ryotcode,(Integer)advListMap.get("advancecode"));
				logger.info("getAllAdvancesDetails()------ principleList"+principleList);
				if(principleList!=null)
				{
					Map principleMap=(Map)principleList.get(0);
					
					if(principleMap.get("principle")==null)
						principle=0.00;
					else
						principle=(Double)principleMap.get("principle");
					
					principleDate=(Date)principleMap.get("principledate");					
					long earliertime=principleDate.getTime();
	  				Long latertime= todate.getTime();
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					nDays = dDays+1;
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
				}
				Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,(Integer)advListMap.get("advancecode"));
				logger.info("getAllAdvancesDetails()------Advance interest"+interest);
				if(interest==null)
					interest=0.00;
				double intrate = interest/100;
				double intrestPerDay = 	principle*intrate/365;
				double intrestAmt = intrestPerDay*nDays;
				intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;
				Double total=principle+intrestAmt;
				total = agricultureHarvestingController.round(total);	
				ryotAdvanceAmount +=total;
				obj.setAdvances(ryotAdvanceAmount);
			}
		}
		Double sb=totalvalue-deductions-ryotLoanAmount-ryotAdvanceAmount;
		if(sb>0)
		{
			sb = agricultureHarvestingController.round(sb);
			obj.setSb(sb);
		}
		session.save(obj);
  		}
  	 
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName =null;
  			if("Excel".equalsIgnoreCase(rptFormat))
  			{
  				reportFileName ="PaymentCheckListPeriodForExcelFormat";
  			}
  			else
  				reportFileName ="PaymentCheckListPeriodReport";
  			String Query1="distinct * from Temp_CaneSupplyCheckListForWeek ";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", "From "+fromDate+" TO "+toDate);

	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	*/
	
	///////////////
	@RequestMapping(value = "/generateCaneLedgerReport", method = RequestMethod.POST)
	public void generateCaneLedgerReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_CaneLedgerReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneLedgerReport obj=new Temp_CaneLedgerReport();
		Date datefrom=null;
		Date dateto=null;
		String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		String ftdate=null;
		if(ls2.size()>0 && ls2!=null)
		{
			datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			String fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			String tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
		}
		String sql="select distinct cs.ryotcode,r.ryotname,r.relativename, v.village,cs.sbac,cs.harvesting,cs.cdcamt,cs.otherded,cs.ulamt,cs.transport,cs.sbaccno from CanAccSBDetails cs,village v,ryot r where cs.ryotcode=r.ryotcode and cs.season='"+season1[1]+"'  and cs.caneacctslno='"+cslnum[1]+"'  and r.villagecode=v.villagecode ";
		List ls=hibernatedao.findBySqlCriteria(sql);
		Session session=hibernatedao.getSessionFactory().openSession();
		Double finalinterest=0.0; 
		String ryotcode=null;
		for (int k = 0; k <ls.size(); k++) 
		{
			obj=new Temp_CaneLedgerReport();
		
		  	if(ls.size()>0 && ls!=null)
			{	
				obj.setBetweendate(ftdate);
				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
				String relativename=(String)((Map<Object,Object>)ls.get(k)).get("relativename");
				String village=(String)((Map<Object,Object>)ls.get(k)).get("village");
				String sbaccno=(String)((Map<Object,Object>)ls.get(k)).get("sbaccno");
				
			//	Integer loancode=(Integer)((Map<Object,Object>)ls.get(k)).get("loanno");
				
				obj.setSbaccno(sbaccno);
				/*Double principleAmt = ahFuctionalService.GetprincipleAmt(season,ryotcode,loancode);
				if(principleAmt !=null)
				{
					obj.setPamount(principleAmt);
				}*/
				
				List LonsummaryData = commonService.getLoanSummaryData(season1[1],ryotcode);
				if(LonsummaryData != null && LonsummaryData.size()>0)
				{
					String pamount="";
					String intrestAmt="";
					String loancode="";
					String branchcode="";
					String agrdate="";
					for (int j = 0; j < LonsummaryData.size(); j++)
					{
						Map LoanMap = new HashMap();
						LoanMap = (Map) LonsummaryData.get(j);
						Double	disbursedamount=(Double) LoanMap.get("disbursedamount");
						Double	interestamount=(Double) LoanMap.get("interestamount");
						//DecimalFormat df = new DecimalFormat("#.##");
						if(interestamount!=null)
						{
							 interestamount = Math.round(interestamount*100) / 100.0;
						}
						Integer	loannumber=(Integer) LoanMap.get("loannumber");
						Integer	branchcd=(Integer) LoanMap.get("branchcode");
						Date	disburseddate=(Date) LoanMap.get("disburseddate");
						String ddate=format.format(disburseddate);
						agrdate +=ddate+"  ";
						pamount +=String.valueOf(disbursedamount)+"   ";
						intrestAmt +=String.valueOf(interestamount)+"   ";
						loancode +=loannumber.toString()+"  ";
						branchcode +=branchcd.toString()+"  ";
					}
					obj.setAgrmntdate(agrdate);
					obj.setPamount(pamount);
					obj.setIamount(intrestAmt);
					obj.setBranchcode(branchcode);
					obj.setLoancode(loancode);
				}
				List CaneAccLoanData = commonService.getCaneAccLoanData(season1[1],ryotcode);
				if(CaneAccLoanData != null && CaneAccLoanData.size()>0)
				{
					String advLoanNos="";
					String bankcodes="";
					String advdates="";
					String advAmts="";
					String accountNos="";
					
					for (int i = 0; i< CaneAccLoanData.size(); i++)
					{
						Map CaneAccMap = new HashMap();
						CaneAccMap = (Map) CaneAccLoanData.get(i);
						Double advanceamount=0.0;
						String adate=null;
						Integer advancecode=(Integer) CaneAccMap.get("advancecode");
						String sql4="select sum(advanceamount) advanceamount,advancedate from AdvanceDetails where season='"+season1[1]+"' and ryotcode='"+ryotcode+"' and advancecode="+advancecode+" group by advancedate";
						List ls4=hibernatedao.findBySqlCriteria(sql4);
						if(ls4.size()>0 && ls4!=null)
						{
							advanceamount=(Double)((Map<Object,Object>)ls4.get(0)).get("advanceamount");
							Date advancedate=(Date)((Map<Object,Object>)ls4.get(0)).get("advancedate");
							adate=DateUtils.formatDate(advancedate,Constants.GenericDateFormat.DATE_FORMAT);
						}
					
						bankcodes+=advancecode.toString()+"      ";
						advdates+=adate+"   ";
						advAmts+=String.valueOf(advanceamount) +"   ";
					//	advLoanNos+=loanno.toString()+"      ";
					}
					obj.setLoanacnos("0");
					obj.setBankcodes(bankcodes);
					obj.setAdvdates(advdates);
					obj.setAdvamounts(advAmts);
					obj.setAdvloannos("0");
				}
				
				List CaneAccLoanData1 = commonService.getCaneAccLoanData1(season1[1],ryotcode,Integer.parseInt(cslnum[1]));
				Double irc=0.0;
				Double lrc=0.0;
				if(CaneAccLoanData1 != null && CaneAccLoanData1.size()>0)
				{
					for (int i = 0; i< CaneAccLoanData1.size(); i++)
					{
						Map CaneAccMap = new HashMap();
						CaneAccMap = (Map) CaneAccLoanData1.get(i);
						Double advisedamt=(Double) CaneAccMap.get("advisedamt");
						finalinterest=(Double) CaneAccMap.get("finalinterest");
						if(advisedamt>finalinterest)
						{
							irc+=finalinterest;
							Double dTemp = advisedamt-finalinterest;
							lrc+= dTemp;
						}
						else
						{
							irc+=advisedamt;
							Double dTemp = advisedamt-advisedamt;							
							lrc+=dTemp;							
						}
					}
					obj.setIrec(irc);
					obj.setLrec(lrc);
				}
				List CaneAccLoanData2 = commonService.getCaneAccLoanData2(season1[1],ryotcode,Integer.parseInt(cslnum[1]));
				Double irc1=0.0;
				Double lrc1=0.0;
				Double advisedamt = 0.0;
				Double ibal=0.0;
				Double lbal = 0.0;
				Double dLoanPrinciple = 0.0;
				String accountnum=null;
				
				if(CaneAccLoanData2 != null && CaneAccLoanData2.size()>0)
				{
					Map CaneAccMap = new HashMap();
					CaneAccMap = (Map) CaneAccLoanData2.get(0);
					advisedamt=(Double) CaneAccMap.get("advisedamt");
					finalinterest=(Double) CaneAccMap.get("finalinterest");
					dLoanPrinciple =(Double) CaneAccMap.get("advanceamount");
					lbal =(Double) CaneAccMap.get("adjustedprincipleamount");
					accountnum =(String) CaneAccMap.get("accountnum");
					
					if(advisedamt>finalinterest)
					{
						irc1=finalinterest;
					}
					else
					{
						irc1=advisedamt;
					}
					lrc1=advisedamt-irc1;
					obj.setLrec1(lrc1);
					obj.setIrec1(irc1);
					
					if(advisedamt<finalinterest)
					{
						ibal = 	finalinterest - advisedamt;
					}
					
					/*if(advisedamt>finalinterest)
					{
						lbal = dLoanPrinciple - (advisedamt-finalinterest);
					}
					else
					{
						lbal = dLoanPrinciple;
					}*/				
				}
				obj.setLbal(lbal);
				obj.setIbal(ibal);
				Double sbac=(Double)((Map<Object,Object>)ls.get(k)).get("sbac");
		
				Double cdc=(Double)((Map<Object,Object>)ls.get(k)).get("cdcamt");
				Double unload=(Double)((Map<Object,Object>)ls.get(k)).get("ulamt");
				Double harvest=(Double)((Map<Object,Object>)ls.get(k)).get("harvesting");
				Double transport=(Double)((Map<Object,Object>)ls.get(k)).get("transport");
				Double oded=(Double)((Map<Object,Object>)ls.get(k)).get("otherded");
				
				obj.setPayableamt(sbac);
				obj.setAccountnum(accountnum);
				obj.setRyotcode(ryotcode);
				obj.setRyotname(ryotname);
				obj.setVillage(village);
				obj.setRelativename(relativename);
				obj.setCdc(cdc);
				obj.setUnload(unload);
				obj.setTransport(transport);
				obj.setHarvest(harvest);
				obj.setOded(oded);
			}
			String sql4="select distinct rate from canevaluesplitup where season='"+season1[1]+"'  and caneacslno='"+cslnum[1]+"'";
			List ls4=hibernatedao.findBySqlCriteria(sql4);
			if(ls4.size()>0 && ls4!=null)
			{
				Double frpprice=(Double)((Map<Object,Object>)ls4.get(0)).get("rate");
				obj.setFrpprice(frpprice);
			}
			
				String sql3="select sum(suppqty) as suppqty  from CanAccSBDetails  where caneacctslno < '"+cslnum[1]+"' and season='"+season1[1]+"' and ryotcode='"+ryotcode+"'";
			List ls3=hibernatedao.findBySqlCriteria(sql3);
			Double proqty=0.0;
			if(ls3.size()>0 && ls3!=null)
			{
				 proqty=(Double)((Map<Object,Object>)ls3.get(0)).get("suppqty");
				if(proqty==null)
				{
					 proqty=0.0;
				}
				obj.setProqty(proqty);
			}
			String sql6="select agreedqty,extentsize,plantorratoon  from AgreementDetails  where  seasonyear='"+season1[1]+"' and ryotcode='"+ryotcode+"'";// and agreementnumber='"+agreementnumber+"'";
			List ls6=hibernatedao.findBySqlCriteria(sql6);
			if(ls6.size()>0 && ls6!=null)
			{
				Double ratoon=0.0;
				Double plant=0.0;
				Double agrqty=0.0;
				 for (int i = 0; i <ls6.size(); i++)
				 {
					String plantorratoon=(String)((Map<Object,Object>)ls6.get(i)).get("plantorratoon");
					if (Integer.parseInt(plantorratoon)==1)
					{
						 Double pt=(Double)((Map<Object,Object>)ls6.get(i)).get("extentsize");
						 plant +=pt;
						
					}
					if (Integer.parseInt(plantorratoon)>1)
					{
						Double rt=(Double)((Map<Object,Object>)ls6.get(i)).get("extentsize");
						ratoon +=rt;
					}
					Double agrqt=(Double)((Map<Object,Object>)ls6.get(i)).get("agreedqty");
					agrqty+=agrqt;
				}
				obj.setAgrqty(agrqty);
				obj.setPlant(plant);
				obj.setRatoon(ratoon);
			}
			String sql5="select w.weighbridgeslno, w.netwt,s.shiftname,w.actualserialnumber,w.canereceiptenddate from WeighmentDetails w,Shift s  where  w.season='"+season1[1]+"' and w.ryotcode='"+ryotcode+"' and w.canereceiptenddate between '"+datefrom+"' and '"+dateto+"' and w.shiftid1=s.shiftid ";
			List ls5=hibernatedao.findBySqlCriteria(sql5);
			String agreementnumber=null;
			if(ls5.size()>0 && ls5!=null)
			{
				for (int i = 0; i <ls5.size(); i++)
				{
					Temp_CaneLedgerReport obj1=new Temp_CaneLedgerReport();
					org.apache.commons.beanutils.BeanUtils.copyProperties(obj1,obj);
					
					
					//agreementnumber=(String)((Map<Object,Object>)ls5.get(i)).get("agreementnumber");
					Double supqty=(Double)((Map<Object,Object>)ls5.get(i)).get("netwt");
					String shiftname=(String)((Map<Object,Object>)ls5.get(i)).get("shiftname");
					Date dates=(Date)((Map<Object,Object>)ls5.get(i)).get("canereceiptenddate");
					String wdate=DateUtils.formatDate(dates,Constants.GenericDateFormat.DATE_FORMAT);
					Integer actualserialnumber=(Integer)((Map<Object,Object>)ls5.get(i)).get("actualserialnumber");
					Integer weighbridgeslno=(Integer)((Map<Object,Object>)ls5.get(i)).get("weighbridgeslno");
					
					obj1.setActualserialnumber(actualserialnumber);
					obj1.setWdate(wdate);
					obj1.setShift(shiftname);
					obj1.setSupqty(supqty);
					obj1.setWeighbridgeslno(weighbridgeslno);
					session.save(obj1);
				}
				
			}
			
			
		}
		session.flush();
		session.close();
		try
		{
			  String reportFileName ="CaneLedgerReport";
			  String Query1="distinct *  from Temp_CaneLedgerReport order by ryotcode";
				//Parameters as Map to be passed to Jasper
			  HashMap<String,Object> hmParams=new HashMap<String,Object>();
			  hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
			  hmParams.put("qrs", Query1);
			  hmParams.put("cdate",format.format(new Date()));
			  generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}

	@RequestMapping(value = "/generateCaneToFAReport", method = RequestMethod.POST)
	public void generateCaneToFAReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneno.split(":");
		//String pattern = "dd/MM/yyyy";
		//SimpleDateFormat format = new SimpleDateFormat(pattern);
		Integer caneslno=Integer.valueOf(cslnum[1]);
		String sqq="truncate table Temp_CaneToFAReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneToFAReport obj=new Temp_CaneToFAReport();
		
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+cslnum[1]+" and season='"+season1[1]+"'";
		List ls2=hibernatedao.findBySqlCriteria(sql2);
		String ftdate=null;
		String fdate=null;String p=null;
		String tdate=null;String tdc=null;
		if(ls2.size()>0 && ls2!=null)
		{
			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
			String frmdate[]=fdate.split("-");
			p=frmdate[0];
			/*int tdoc=Integer.parseInt(p);
			int ldate=tdoc;
			if(ldate>=12)
			{
				tdc="C"+ldate;
			}
			else
				tdc="C0"+ldate;*/
		}
		Double totalValues=0.0;
		for(int i=1;i<=3;i++)
		{
			obj=new Temp_CaneToFAReport();
			 totalValues= ahFuctionalService.getCaneValueSplitUp(season1[1],caneslno);
		   String sds=	 new DecimalFormat("##,##,##0.##").format(totalValues);
			Double totalSupply= ahFuctionalService.getTotalSupply(season1[1],caneslno);
			totalSupply=(double)Math.round(totalSupply * 1000d) / 1000d;
			String supply=String.valueOf(totalSupply);
			if(i==1)
			{
				obj.setCreditamount(null);
				obj.setDebiteamount(totalValues);
				obj.setGlcode("4041");
				obj.setParticulars("SUGARCANE PURCHASED A/C");
				//obj.setSupplyqty(null);
				
				obj.setSlno("C"+p+"1");
			}
			if(i==2)
			{
				obj.setCreditamount(totalValues);
				obj.setDebiteamount(null);
				obj.setGlcode("1132");
				obj.setParticulars("SUGARCANE SUPPLIERS A/C  Cane Value: "+supply+" M.Ts  week ending "+tdate);
				//obj.setSupplyqty("(Being the value of    "+supply +"  M.Ts of SugarCane Purchased during the week ending"+tdate);
				
				obj.setSlno(null);
			}
			if(i==3)
			{
				obj.setDebiteamount(totalValues);
				obj.setParticulars("SUGARCANE SUPPLIERS A/C");
			
				obj.setSlno("C"+p+"2");
				obj.setGlcode("1132");
			}
			session.save(obj);	
		}
		List dropdownList1= caneAccountingFunctionalService.getDedutionsData(season1[1],caneslno);
		Double roundvalue=0.0;
		Double cdc=0.0;
		Double trans=0.0;
		Double ulamt=0.0;
		Double harv=0.0;
		Double od=0.0;
		Double lamt=0.0;
		Double sbac=0.0;
		Double grandtotal=0.0;
		Double advamt=0.0;
		Double loans=0.0;
		ArrayList<Double>  al=new ArrayList<Double>();
		if (dropdownList1 != null && dropdownList1.size() > 0)
		{
			for (int j = 0;j<dropdownList1.size();j++)
			{
				obj=new Temp_CaneToFAReport();
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList1.get(j);
				
				cdc = (Double) tempMap.get("cdc");
				//cdc=(double)Math.round(cdc * 100d) / 100d;
				trans = (Double) tempMap.get("trans");
				ulamt = (Double) tempMap.get("ulamt");
				harv = (Double) tempMap.get("harv");
				od = (Double) tempMap.get("od");
				//lamt = (Double) tempMap.get("lamt");
				//advamt = (Double) tempMap.get("advamt");
				sbac = (Double) tempMap.get("sbac");
				//loans=advamt+lamt;
				grandtotal=cdc+trans+ulamt+harv+od+sbac;
				//grandtotal=(double)Math.round(grandtotal * 100d) / 100d;
				//roundvalue=grandtotal-totalValues;
				//roundvalue=totalValues-grandtotal;
				roundvalue=(double)Math.round(roundvalue * 100d) / 100d;
				
				al.add(cdc);
				al.add(trans);
				al.add(ulamt);
				al.add(harv);
				al.add(od);
				//al.add(lamt);
			//	al.add(sbac); 
				//al.add(advamt);
			}
			ArrayList<String> names=new ArrayList<String>();
			names.add("C.D.C.FUND PAYABLE  A/C");
			names.add("Transport Amount A/C ");
			names.add("Unloading Amount A/C");
			names.add("Harvesting Amount A/C  ");
			names.add("SUGARCANE PURCHASED(B.Cane) A/C ");
		//	names.add("S.B.A/c Transfers ");
			
			ArrayList<String> glcodes=new ArrayList<String>();
			glcodes.add("1186");
			glcodes.add("1132");
			glcodes.add("1132");
			glcodes.add(null);
			glcodes.add("4041");
			//glcodes.add("1132 ");
			
			
			for(int i=0;i<al.size();i++)
			{
				obj=new Temp_CaneToFAReport();
				obj.setCreditamount(al.get(i));
				obj.setParticulars(names.get(i));
				obj.setGlcode(glcodes.get(i));
				session.save(obj);
				
			}
			
			//session.save(obj);
		}
		List dropdownList= caneAccountingFunctionalService.getCaneToFAData(season1[1],caneslno);
	    logger.info("getCaneToFAData()========dropdownList=========="+dropdownList);
		if (dropdownList != null && dropdownList.size() > 0)
		{
			
			for (int j= 0;j<dropdownList.size();j++)
			{
				obj=new Temp_CaneToFAReport();
				/*Map tempMap = new HashMap();*/
				Map	tempMap10 = (Map) dropdownList.get(j);
				
				Integer bkcode = (Integer) tempMap10.get("branchcode");
				Double bktotal=0.0;
				if(bkcode<= 9)
  				{
					//String glcode = ahFuctionalService.getGlcode(bkcode);
					String sql3="select glCode,advance  from CompanyAdvance  where advancecode="+bkcode+" ";
					List ls3=hibernatedao.findBySqlCriteria(sql3);
					String advances=(String)((Map<Object,Object>)ls3.get(0)).get("advance");
					String glCode=(String)((Map<Object,Object>)ls3.get(0)).get("glCode");
					Double banktotal = ahFuctionalService.bankwiseData1(season1[1],caneslno,bkcode);
					if(banktotal !=null)  
					{
						 bktotal=(double)Math.round(banktotal * 100d) / 100d;
					}
					grandtotal+=bktotal;
					obj.setCreditamount(bktotal);

					obj.setGlcode(glCode);
					obj.setParticulars(advances);
					session.save(obj);
  				}
				else
				{
					//Long glcode=ahFuctionalService.getgrcodefromBranch(bkcode);
					String sql4="select glcode,branchname  from Branch  where branchcode="+bkcode+" ";
					List ls4=hibernatedao.findBySqlCriteria(sql4);
					String branchname=(String)((Map<Object,Object>)ls4.get(0)).get("branchname");
					BigDecimal glCode=(BigDecimal)((Map<Object,Object>)ls4.get(0)).get("glcode");
					String glcd =String.valueOf(glCode);
					Double banktotal = ahFuctionalService.bankwiseData(season1[1],caneslno,bkcode);
					if(banktotal !=null)  
					{
						 bktotal=(double)Math.round(banktotal * 100d) / 100d;
					}
					grandtotal+=bktotal;
					obj.setCreditamount(bktotal);
					obj.setGlcode(glcd);
					obj.setParticulars(branchname);
					session.save(obj);
				}
				
		 	}
			
			session.save(obj);
		}
		if (dropdownList1 != null && dropdownList1.size() > 0)
		{
			for (int j = 0;j<dropdownList1.size();j++)
			{
				obj=new Temp_CaneToFAReport();
				Map tempMap = new HashMap();
				tempMap = (Map) dropdownList1.get(j);
				sbac = (Double) tempMap.get("sbac");
				obj.setCreditamount(sbac);
				obj.setParticulars("S.B.A/c Transfers ");
				obj.setGlcode("1132");
				
				Double diffAmt=totalValues-grandtotal;
				diffAmt=(double)Math.round(diffAmt * 100d) / 100d;
				String difference=String.valueOf(diffAmt);
				obj.setSupplyqty(difference);
				session.save(obj);
			}
		}
		session.flush();
		session.close();
		try
		{
			String reportFileName ="CaneToFAReport";
			String Query1="distinct *  from Temp_CaneToFAReport ";
			//Parameters as Map to be passed to Jasper
			HashMap<String,Object> hmParams=new HashMap<String,Object>();
			hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
			hmParams.put("qrs", Query1);
			hmParams.put("betweendate",ftdate);
			generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateHarvestContractorDtailsCumulative", method = RequestMethod.POST)
	public void generateHarvestContractorDtailsCumulative(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season=request.getParameter("season");
		String Contractor=request.getParameter("Contractor");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String contractorCode[]=Contractor.split(":");
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_HarvestContractorDtailsByRyot";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_HarvestContractorDtailsByRyot obj=new Temp_HarvestContractorDtailsByRyot();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql=null;
		if("0".equals(contractorCode[1]))
		{
		sql=" select distinct h.rate,h.harvestcontcode,h.ryotcode,w.ryotname,c.circle,hc.harvester,z.zone from HarvChgDtlsByRyotContAndDate h,WeighmentDetails w,circle c,HarvestingContractors hc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and h.ryotcode=w.ryotcode and h.season='"+season1[1]+"' and h.receiptdate  between '"+fromdate+"' and '"+todate+"'  and hc.harvestercode=h.harvestcontcode ";
		}
		else
		{
			sql=" select distinct h.rate,h.harvestcontcode, h.ryotcode,w.ryotname,c.circle,hc.harvester,z.zone from HarvChgDtlsByRyotContAndDate h,WeighmentDetails w,circle c,HarvestingContractors hc,zone z where w.zonecode=z.zonecode and w.circlecode=c.circlecode and h.ryotcode=w.ryotcode and h.season='"+season1[1]+"' and h.receiptdate  between '"+fromdate+"' and '"+todate+"' and h.harvestcontcode='"+contractorCode[1]+"' and hc.harvestercode='"+contractorCode[1]+"'";
		}
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		String 	ryotcode=null;
  		for (int k = 0; k <ls.size(); k++) 
		{
  			
  			if(ls.size()>0 && ls!=null)
  			{
  				//Note this model is used for another report 
  				obj=new Temp_HarvestContractorDtailsByRyot();
  				Integer harvestcontcode=(Integer)((Map<Object,Object>)ls.get(k)).get("harvestcontcode");
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");	
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				//Double 	netwt=(Double)((Map<Object,Object>)ls.get(k)).get("harvestedcanewt");
  				String 	circle=(String)((Map<Object,Object>)ls.get(k)).get("circle");
  				String 	harvester=(String)((Map<Object,Object>)ls.get(k)).get("harvester");
  				//Double 	totalamount=(Double)((Map<Object,Object>)ls.get(k)).get("totalamount");
  				Double 	rate=(Double)((Map<Object,Object>)ls.get(k)).get("rate");
  				String 	zone=(String)((Map<Object,Object>)ls.get(k)).get("zone");
  				obj.setZone(zone);
  				obj.setHarvestcontcode(harvestcontcode);
  				obj.setRate(rate);
  				//obj.setTotalamount(totalamount);
  	  			obj.setCircle(circle);
  	  			obj.setHarvester(harvester);
  	  			//obj.setNetwt(netwt);
  	  			obj.setRyotcode(ryotcode);
  	  			obj.setRyotname(ryotname);
  			}
  	  		String	sql1="select sum(harvestedcanewt) as netwt,sum(totalamount) as totalamount from HarvChgDtlsByRyotContAndDate where ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and receiptdate  between '"+fromdate+"' and '"+todate+"'  ";
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		
	  		if(ls1.size()>0 && ls1!=null)
	  		{
	  			Double 	netwt=(Double)((Map<Object,Object>)ls1.get(0)).get("netwt");
	  			Double 	totalamount=(Double)((Map<Object,Object>)ls1.get(0)).get("totalamount");
	  			totalamount = agricultureHarvestingController.round(totalamount);
	  			obj.setNetwt(netwt);
	  			obj.setTotalamount(totalamount);
	  		}
  	  			
  	  			
  	  		session.save(obj);
  			}
		
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName ="HarvestContractorRyotWiseCumulative";
  			String Query1="zone,harvestcontcode,totalamount,rate,harvester,ryotcode,ryotname,circle,netwt from Temp_HarvestContractorDtailsByRyot  order by harvestcontcode";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("period", fromDate+" TO "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateCanToFinancial", method = RequestMethod.POST)
	public String generateCanToFinancial(HttpServletRequest request,HttpServletResponse response) throws ParseException 
	{		
		String caneslno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");	
		String season=request.getParameter("season");
		String season1[]=season.split(":");
		String cslnum[]=caneslno.split(":");
	    String reportFileName = "CaneAccountCheckSample";
	    
        String QueryString="c.*,b.branchname from CaneAccDetailsTemp c,branch b where c.bankcode=b.branchcode and c.caneslno='"+cslnum[1]+"' ";
        //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD., CHELLURU.");
        //hmParams.put("dt", frmDate);
        hmParams.put("qrs", QueryString);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
		
		return null;
		
	}
	
	@RequestMapping(value = "/generateBankAndAdvanceData", method = RequestMethod.POST)
	public void generateBankAndAdvanceData(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season1=request.getParameter("season");	
		String sName[]=season1.split(":");
		String season=sName[1];
		
		String Code=request.getParameter("GROWER");	
		String reqData[]=Code.split(":");
		String rptFormat = request.getParameter("status");
		String sqq="truncate table Temp_BankLoanAndAdvance";
		try{
			SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_BankLoanAndAdvance obj=null;
		int noOfDaysPerYear=365;
		Session session=hibernatedao.getSessionFactory().openSession();
		//if(reqData[1].equalsIgnoreCase("Advance"))
		if("ADVANCE".equalsIgnoreCase(reqData[1]))
		{
			List AdvancePAmountList = commonService.getAdvPrncAmt(season);
			if(AdvancePAmountList != null && AdvancePAmountList.size()>0)
			{
				for (int j = 0; j < AdvancePAmountList.size(); j++)
				{
					obj=new Temp_BankLoanAndAdvance();
					Map DataMap = new HashMap();
					DataMap = (Map) AdvancePAmountList.get(j);
					Integer advCode = (Integer) DataMap.get("advancecode");
					String ryotCode = (String) DataMap.get("ryotcode");
					double principle = (Double) DataMap.get("principle");
					String	sql8="select  advance from companyadvance where  advancecode="+advCode+"";
    		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
    		  		
    		  		if(ls8.size()>0 && ls8!=null)
    		  		{
    		  			String bankname=(String)((Map<Object,Object>)ls8.get(0)).get("advance");
    		  			obj.setBankname(bankname);
    		  		}
					
					double nDays = 0.0;
					Date principleDate = (Date) DataMap.get("principledate");
					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strprincipleDate = df1.format(principleDate);
					principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
										
					Date curDate = new Date();
					df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strcurDate = df1.format(curDate);
					curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

					double months=0.00;
					long earliertime=principleDate.getTime();
					long latertime=curDate.getTime();
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					nDays = dDays+1;
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
					
					Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,advCode);
					logger.info("getAllAdvancesDetails()------Advance interest"+interest);

					if(interest==null)
						interest=0.00;
					
					double intrate = interest/100;
					double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
					double intrestAmt = intrestPerDay*nDays;
					intrestAmt = Double.parseDouble(new DecimalFormat("##.##").format(intrestAmt));
					obj.setIntrestamount(intrestAmt);
				//	jsonObj.put("intrestAmt", intrestAmt);
					Double total=principle+intrestAmt;
					List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
					String  villageCode=null;
					if(rName != null && rName.size()>0)
					{
						String   ryotName = rName.get(0).getRyotname();
						villageCode = rName.get(0).getVillagecode();
					//	jsonObj.put("ryotName", ryotName);
						obj.setRyotname(ryotName);
					}
					List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
					if(villageName != null && villageName.size()>0)
					{
						String village = villageName.get(0).getVillage();
					//	jsonObj.put("village", village);
						obj.setVillage(village);
					}
					String	sql4="select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+"";
    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
    		  		Double advanceamount=0.0;
    		  		if(ls4.size()>0 && ls4!=null)
    		  		{
    		  			advanceamount=(Double)((Map<Object,Object>)ls4.get(0)).get("advanceamount");
    		  			if(advanceamount !=null)
    		  			{
    		  				advanceamount = advanceamount+intrestAmt;
    		  				advanceamount =Double.parseDouble(new DecimalFormat("##.##").format(advanceamount));
    		  				//jsonObj.put("TotalAmount", advanceamount);
    		  				obj.setTotalamount(advanceamount);
    		  			}
    		  			else
    		  				advanceamount=0.0;
    		  		}
    		  		String	sql5="select  sum(paidamt) paidamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and accountnum="+advCode+" and advloan=0";
    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
    		  		Double paidamt=0.0;
    		  		if(ls5.size()>0 && ls5!=null)
    		  		{
    		  			if((Double)((Map<Object,Object>)ls5.get(0)).get("paidamt") != null)
    		  			{
    		  				paidamt=(Double)((Map<Object,Object>)ls5.get(0)).get("paidamt");
	    		  			paidamt =Double.parseDouble(new DecimalFormat("##.##").format(paidamt));
    		  			}
    		  			
    		  			if (paidamt !=null)
    		  			{
    		  			//	jsonObj.put("TotalRecovery", paidamt);
    		  				obj.setTotalrecovery(paidamt);
    		  			}
    		  			else 
    		  				paidamt=0.0;
    		  		}
    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
    		  		Double supply=0.0;
    		  		if(ls6.size()>0 && ls6!=null)
    		  		{
    		  			if((Double)((Map<Object,Object>)ls6.get(0)).get("netwt") != null)
    		  			{
    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
	    		  			supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
    		  			}
    		  		}
    		  		double pending = advanceamount-paidamt;
    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
    		  		if(pending<0 || pending==0)
    		  		{
    		  			continue;
    		  		}
    		  		obj.setQuantity(supply);
    		  		obj.setBalance(pending);
    		  		obj.setRyotcode(ryotCode);
    		  		obj.setLoannumber(advCode);
					obj.setLoanrefnumber(advCode.toString());
				
					session.save(obj);
				}
			}
		}else if("LOAN".equalsIgnoreCase(reqData[1]))
		{
			List LoanPAmountList = commonService.getLoanPrncData(season);
			if(LoanPAmountList != null && LoanPAmountList.size()>0)
			{
				for (int j = 0; j < LoanPAmountList.size(); j++)
				{
					obj=new Temp_BankLoanAndAdvance();
					Map DataMap = new HashMap();
					DataMap = (Map) LoanPAmountList.get(j);
					Integer branchCode = (Integer) DataMap.get("branchcode");
					Integer loanNumber = (Integer) DataMap.get("loannumber");
					String ryotCode = (String) DataMap.get("ryotcode");
					String loanAccountNumber= (String) DataMap.get("loanaccountnumber");
					
					
					String	sql8="select  branchname from branch where  branchcode="+branchCode+"";
    		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
    		  		
    		  		if(ls8.size()>0 && ls8!=null)
    		  		{
    		  			String bankname=(String)((Map<Object,Object>)ls8.get(0)).get("branchname");
    		  			obj.setBankname(bankname);
    		  		}
					double principle = (Double) DataMap.get("principle");

					Date principleDate = (Date) DataMap.get("principledate");
					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strprincipleDate = df1.format(principleDate);
					principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
						
					Date curDate = new Date();
					df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strcurDate = df1.format(curDate);
					curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

					
					int loanNo = loanNumber;
					double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotCode,season,branchCode,loanNo);
					double months=0.00;
					
					long earliertime=principleDate.getTime();
					long latertime=curDate.getTime();
					
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					if(previousIntrest == 0)
					{
						dDays = dDays+1;
					}
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
					
					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(principle,branchCode,season);
					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

					if(interestRate==null)
						interestRate=0.00;
					
					double intrate = interestRate/100;
					double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
					double intrestAmt = intrestPerDay*dDays;
					intrestAmt = Double.parseDouble(new DecimalFormat("##.##").format(intrestAmt));
					obj.setIntrestamount(intrestAmt);
					
					Double totalAmoutOnInterest=principle+intrestAmt;
					
					
					List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
					String  villageCode=null;
					if(rName != null && rName.size()>0)
					{
						String   ryotName = rName.get(0).getRyotname();
						villageCode = rName.get(0).getVillagecode();
						obj.setRyotname(ryotName);
						
					}
					List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
					if(villageName != null && villageName.size()>0)
					{
						String village = villageName.get(0).getVillage();
						obj.setVillage(village);
					}
					String	sql4="select  sum(pendingamount) disbursedamount from LoanDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and disburseddate IS NOT NULL and referencenumber IS NOT NULL ";
    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
    		  		Double disbursedamount=0.0;
    		  		if(ls4.size()>0 && ls4!=null)
    		  		{
    		  			disbursedamount=(Double)((Map<Object,Object>)ls4.get(0)).get("disbursedamount");
    		  			if(disbursedamount !=null)
    		  			{
    		  				//disbursedamount = disbursedamount+intrestAmt;
    		  				disbursedamount =Double.parseDouble(new DecimalFormat("##.##").format(disbursedamount));
    		  				obj.setTotalamount(disbursedamount);
    		  			}
    		  			else
    		  				disbursedamount=0.0;
    		  		}
    		  			
    		  		String	sql5="select  sum(advisedamt) advisedamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advloan=1 and branchcode="+branchCode+" and loanno="+loanNumber+" ";
    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
    		  		Double advisedamt=0.0;
    		  		if(ls5.size()>0 && ls5!=null)
    		  		{
    		  			advisedamt=(Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt");
    		  			if(advisedamt !=null)
    		  			{
    		  				advisedamt =Double.parseDouble(new DecimalFormat("##.##").format(advisedamt));
    		  				obj.setTotalrecovery(advisedamt);
    		  			}
    		  			else 
    		  				advisedamt=0.0;
    		  		}
    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
    		  		Double supply=0.0;
    		  		if(ls6.size()>0 && ls6!=null)
    		  		{
    		  			if((Double)((Map<Object	,Object>)ls6.get(0)).get("netwt") != null)
    		  			{
    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
    		  				supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
    		  			}
    		  			obj.setQuantity(supply);
    		  		}
    		  		
    		  		double pending = disbursedamount+intrestAmt-advisedamt;
    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
    		  		if(pending<0 || pending==0)
    		  		{
    		  			continue;
    		  		}
    		  		obj.setBalance(pending);
    		  		obj.setRyotcode(ryotCode);
					obj.setLoannumber(branchCode);
					obj.setLoanrefnumber(loanAccountNumber);
					
					session.save(obj);
				}
			}
		}
		else{
			List AdvancePAmountList = commonService.getAdvPrncAmt(season);
			if(AdvancePAmountList != null && AdvancePAmountList.size()>0)
			{
				for (int j = 0; j < AdvancePAmountList.size(); j++)
				{
					obj=new Temp_BankLoanAndAdvance();
					Map DataMap = new HashMap();
					DataMap = (Map) AdvancePAmountList.get(j);
					Integer advCode = (Integer) DataMap.get("advancecode");
					String ryotCode = (String) DataMap.get("ryotcode");
					double principle = (Double) DataMap.get("principle");
					String	sql8="select  advance from companyadvance where  advancecode="+advCode+"";
    		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
    		  		
    		  		if(ls8.size()>0 && ls8!=null)
    		  		{
    		  			String bankname=(String)((Map<Object,Object>)ls8.get(0)).get("advance");
    		  			obj.setBankname(bankname);
    		  		}
					
					double nDays = 0.0;
					Date principleDate = (Date) DataMap.get("principledate");
					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strprincipleDate = df1.format(principleDate);
					principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
										
					Date curDate = new Date();
					df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strcurDate = df1.format(curDate);
					curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

					double months=0.00;
					long earliertime=principleDate.getTime();
					long latertime=curDate.getTime();
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					nDays = dDays+1;
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
					
					Double interest=(Double)caneAccountingFunctionalService.getInterestAdvance(principle,advCode);
					logger.info("getAllAdvancesDetails()------Advance interest"+interest);

					if(interest==null)
						interest=0.00;
					
					double intrate = interest/100;
					double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
					double intrestAmt = intrestPerDay*nDays;
					intrestAmt = Double.parseDouble(new DecimalFormat("##.##").format(intrestAmt));
					obj.setIntrestamount(intrestAmt);
				//	jsonObj.put("intrestAmt", intrestAmt);
					Double total=principle+intrestAmt;
					List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
					String  villageCode=null;
					if(rName != null && rName.size()>0)
					{
						String   ryotName = rName.get(0).getRyotname();
						villageCode = rName.get(0).getVillagecode();
					//	jsonObj.put("ryotName", ryotName);
						obj.setRyotname(ryotName);
					}
					List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
					if(villageName != null && villageName.size()>0)
					{
						String village = villageName.get(0).getVillage();
					//	jsonObj.put("village", village);
						obj.setVillage(village);
					}
					String	sql4="select  sum(advanceamount) advanceamount from AdvanceDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advancecode="+advCode+"";
    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
    		  		Double advanceamount=0.0;
    		  		if(ls4.size()>0 && ls4!=null)
    		  		{
    		  			advanceamount=(Double)((Map<Object,Object>)ls4.get(0)).get("advanceamount");
    		  			if(advanceamount !=null)
    		  			{
    		  				advanceamount = advanceamount+intrestAmt;
    		  				advanceamount =Double.parseDouble(new DecimalFormat("##.##").format(advanceamount));
    		  				//jsonObj.put("TotalAmount", advanceamount);
    		  				obj.setTotalamount(advanceamount);
    		  			}
    		  			else
    		  				advanceamount=0.0;
    		  		}
    		  		String	sql5="select  sum(paidamt) paidamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and accountnum="+advCode+" and advloan=0";
    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
    		  		Double paidamt=0.0;
    		  		if(ls5.size()>0 && ls5!=null)
    		  		{
    		  			if((Double)((Map<Object,Object>)ls5.get(0)).get("paidamt") != null)
    		  			{
    		  				paidamt=(Double)((Map<Object,Object>)ls5.get(0)).get("paidamt");
	    		  			paidamt =Double.parseDouble(new DecimalFormat("##.##").format(paidamt));
    		  			}
    		  			
    		  			if (paidamt !=null)
    		  			{
    		  			//	jsonObj.put("TotalRecovery", paidamt);
    		  				obj.setTotalrecovery(paidamt);
    		  			}
    		  			else 
    		  				paidamt=0.0;
    		  		}
    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
    		  		Double supply=0.0;
    		  		if(ls6.size()>0 && ls6!=null)
    		  		{
    		  			if((Double)((Map<Object,Object>)ls6.get(0)).get("netwt") != null)
    		  			{
    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
	    		  			supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
    		  			}
    		  		}
    		  		double pending = advanceamount-paidamt;
    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
    		  		if(pending<0 || pending==0)
    		  		{
    		  			continue;
    		  		}
    		  		obj.setQuantity(supply);
    		  		obj.setBalance(pending);
    		  		obj.setRyotcode(ryotCode);
    		  		obj.setLoannumber(advCode);
					obj.setLoanrefnumber(advCode.toString());
				
					session.save(obj);
				}
			}
			List LoanPAmountList = commonService.getLoanPrncData(season);
			if(LoanPAmountList != null && LoanPAmountList.size()>0)
			{
				for (int j = 0; j < LoanPAmountList.size(); j++)
				{
					obj=new Temp_BankLoanAndAdvance();
					Map DataMap = new HashMap();
					DataMap = (Map) LoanPAmountList.get(j);
					Integer branchCode = (Integer) DataMap.get("branchcode");
					Integer loanNumber = (Integer) DataMap.get("loannumber");
					String ryotCode = (String) DataMap.get("ryotcode");
					String loanAccountNumber= (String) DataMap.get("loanaccountnumber");
					
					
					String	sql8="select  branchname from branch where  branchcode="+branchCode+"";
    		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
    		  		
    		  		if(ls8.size()>0 && ls8!=null)
    		  		{
    		  			String bankname=(String)((Map<Object,Object>)ls8.get(0)).get("branchname");
    		  			obj.setBankname(bankname);
    		  		}
					double principle = (Double) DataMap.get("principle");

					Date principleDate = (Date) DataMap.get("principledate");
					DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strprincipleDate = df1.format(principleDate);
					principleDate=DateUtils.getSqlDateFromString(strprincipleDate,Constants.GenericDateFormat.DATE_FORMAT);
						
					Date curDate = new Date();
					df1 = new SimpleDateFormat("dd-MM-yyyy");
					String strcurDate = df1.format(curDate);
					curDate=DateUtils.getSqlDateFromString(strcurDate,Constants.GenericDateFormat.DATE_FORMAT);

					
					int loanNo = loanNumber;
					double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotCode,season,branchCode,loanNo);
					double months=0.00;
					
					long earliertime=principleDate.getTime();
					long latertime=curDate.getTime();
					
					long days=DateUtils.dateDifference(latertime, earliertime);
					days = Math.abs(days);
					Long ldays = new Long(days);
					double dDays = ldays.doubleValue();
					if(previousIntrest == 0)
					{
						dDays = dDays+1;
					}
					months=dDays/30;
					months = Math.round(months * 100.0) / 100.0;
					
					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(principle,branchCode,season);
					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

					if(interestRate==null)
						interestRate=0.00;
					
					double intrate = interestRate/100;
					double intrestPerDay = 	principle*intrate/noOfDaysPerYear;
					double intrestAmt = intrestPerDay*dDays;
					intrestAmt = Double.parseDouble(new DecimalFormat("##.##").format(intrestAmt));
					obj.setIntrestamount(intrestAmt);
					
					Double totalAmoutOnInterest=principle+intrestAmt;
					
					
					List<Ryot> rName= ahFuctionalService.getRyotNameAndVilCode(ryotCode);
					String  villageCode=null;
					if(rName != null && rName.size()>0)
					{
						String   ryotName = rName.get(0).getRyotname();
						villageCode = rName.get(0).getVillagecode();
						obj.setRyotname(ryotName);
						
					}
					List<Village> villageName= ahFuctionalService.getVillageForLedger(villageCode);
					if(villageName != null && villageName.size()>0)
					{
						String village = villageName.get(0).getVillage();
						obj.setVillage(village);
					}
					String	sql4="select  sum(pendingamount) disbursedamount from LoanDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and disburseddate IS NOT NULL and referencenumber IS NOT NULL ";
    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
    		  		Double disbursedamount=0.0;
    		  		if(ls4.size()>0 && ls4!=null)
    		  		{
    		  			disbursedamount=(Double)((Map<Object,Object>)ls4.get(0)).get("disbursedamount");
    		  			if(disbursedamount !=null)
    		  			{
    		  				//disbursedamount = disbursedamount+intrestAmt;
    		  				disbursedamount =Double.parseDouble(new DecimalFormat("##.##").format(disbursedamount));
    		  				obj.setTotalamount(disbursedamount);
    		  			}
    		  			else
    		  				disbursedamount=0.0;
    		  		}
    		  			
    		  		String	sql5="select  sum(advisedamt) advisedamt from CanAccLoansDetails where season='"+season+"' and  ryotcode='"+ryotCode+"' and advloan=1 and branchcode="+branchCode+" and loanno="+loanNumber+" ";
    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
    		  		Double advisedamt=0.0;
    		  		if(ls5.size()>0 && ls5!=null)
    		  		{
    		  			advisedamt=(Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt");
    		  			if(advisedamt !=null)
    		  			{
    		  				advisedamt =Double.parseDouble(new DecimalFormat("##.##").format(advisedamt));
    		  				obj.setTotalrecovery(advisedamt);
    		  			}
    		  			else 
    		  				advisedamt=0.0;
    		  		}
    		  		String	sql6="select  sum(netwt) netwt from WeighmentDetails where season='"+season+"' and  ryotcode='"+ryotCode+"'";
    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
    		  		Double supply=0.0;
    		  		if(ls6.size()>0 && ls6!=null)
    		  		{
    		  			if((Double)((Map<Object	,Object>)ls6.get(0)).get("netwt") != null)
    		  			{
    		  				supply=(Double)((Map<Object,Object>)ls6.get(0)).get("netwt");
    		  				supply =Double.parseDouble(new DecimalFormat("##.###").format(supply));
    		  			}
    		  			obj.setQuantity(supply);
    		  		}
    		  		
    		  		double pending = disbursedamount+intrestAmt-advisedamt;
    		  		pending =Double.parseDouble(new DecimalFormat("##.##").format(pending));
    		  		if(pending<0 || pending==0)
    		  		{
    		  			continue;
    		  		}
    		  		obj.setBalance(pending);
    		  		obj.setRyotcode(ryotCode);
					obj.setLoannumber(branchCode);
					obj.setLoanrefnumber(loanAccountNumber);
					
					session.save(obj);
				}
			}
		}
			
		
  		session.flush();
  		session.close();
     
		String reportFileName = "BanlLoanAndAdvanceData";
		String Query1="distinct * FROM Temp_BankLoanAndAdvance order by loannumber ";
		
		 //Parameters as Map to be passed to Jasper
        HashMap<String,Object> hmParams=new HashMap<String,Object>();
        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
        hmParams.put("qrs", Query1);
        generateReport(request,response,hmParams,reportFileName,rptFormat);
  		}  		
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		
	}
	
	@RequestMapping(value = "/generateAuthorisationAckDetailsReport", method = RequestMethod.POST)
	public void generateAuthorisationAckDetailsReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_AuthorisationAckDetails";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_AuthorisationAckDetails obj=new Temp_AuthorisationAckDetails();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select a.* ,s.city from AuthorisationAcknowledgementSummary a,StoreAuthorization s  where a.season='"+season+"' and  a.billRefDate between '"+fromdate+"' and '"+todate+"' and a.storecode=s.storecode " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_AuthorisationAckDetails();
  			
  				String authorisationSeqNo=(String)((Map<Object,Object>)ls.get(i)).get("authorisationSeqNo");
  			 	String ryotcode=(String)((Map<Object,Object>)ls.get(i)).get("ryotCode");
  				String 	ryotname=(String)((Map<Object,Object>)ls.get(i)).get("ryotName");
  				String 	billRefNum=(String)((Map<Object,Object>)ls.get(i)).get("billRefNum");
  				Date 	billRefDate=(Date)((Map<Object,Object>)ls.get(i)).get("billRefDate");
  				Double 	suppliertotal=(Double)((Map<Object,Object>)ls.get(i)).get("suppliertotal");
  				String bilreffdate=format.format(billRefDate);
  				String 	storecode=(String)((Map<Object,Object>)ls.get(i)).get("storecode");
  				String 	storename=(String)((Map<Object,Object>)ls.get(i)).get("city");
  				
  				obj.setStorecode(storecode);
  				obj.setStorename(storename);
  				obj.setTotalamout(suppliertotal);
  				obj.setBilldate(bilreffdate);
  				obj.setRefnumber(billRefNum);
  				obj.setAuthseqno(authorisationSeqNo);
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="AuthorisationAckDetailsReport";
			String Query1=" * from Temp_AuthorisationAckDetails order by storecode";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	public static 	class Days 
	{
	    public static java.util.Date getDateOfDaysSub( int year, int month, int day, int days )
	    {
	        java.util.Calendar cal = java.util.Calendar.getInstance();
	        // System.out.println(cal.getTime());
	        cal.set(year, month, day);
	        // System.out.println(cal.getTime());
	        cal.add(java.util.Calendar.DATE, days);
	        // System.out.println(cal.getTime()); 
	        return cal.getTime();
	    }
	}

	

	@RequestMapping(value = "/generateProcurementReport", method = RequestMethod.POST)
	public void generateProcurementReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		String seedcode=request.getParameter("GROWER");
		String seed[]=seedcode.split(":");
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		String sqq="truncate table Temp_ProcurementReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_ProcurementReport obj=new Temp_ProcurementReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql=null;
		if("Seedling Nursary".equalsIgnoreCase(seed[1]))
		{
			 sql="select s.*,r.ryotname,v.village,vi.variety from SeedSource s,ryot r,village v,variety vi  where s.season='"+season+"' and s.sourcesupplieddate between '"+fromdate+"' and '"+todate+"'  and s.purpose=0 and s.seedsrcode=r.ryotcode and s.villagecode=v.villagecode and s.varietycode=vi.varietycode" ;
		}
		else
		{
			 sql="select s.*,r.ryotname,v.village,vi.variety from SeedSource s,ryot r,village v,variety vi  where s.season='"+season+"'  and s.sourcesupplieddate between '"+fromdate+"' and '"+todate+"' and s.purpose=1 and s.seedsrcode=r.ryotcode and s.villagecode=v.villagecode and s.varietycode=vi.varietycode" ;
		}
			
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_ProcurementReport();
  			
  				Double ageofcrop=(Double)((Map<Object,Object>)ls.get(i)).get("ageofcrop");
  			 	String supplier=(String)((Map<Object,Object>)ls.get(i)).get("seedsrcode");
  				String 	suppliername=(String)((Map<Object,Object>)ls.get(i)).get("ryotname");
  				String 	village=(String)((Map<Object,Object>)ls.get(i)).get("village");
  				String 	variety=(String)((Map<Object,Object>)ls.get(i)).get("variety");
  				Double 	nooftons=(Double)((Map<Object,Object>)ls.get(i)).get("finalweight");
  				Double 	rate=(Double)((Map<Object,Object>)ls.get(i)).get("seedcostperton");
  				Date 	dateofHarvest=(Date)((Map<Object,Object>)ls.get(i)).get("sourcesupplieddate");
  				Double 	noofacre=(Double)((Map<Object,Object>)ls.get(i)).get("noofacre");
  				Double 	value=(Double)((Map<Object,Object>)ls.get(i)).get("totalcost");
  				
  				Double cage=ageofcrop*30;
  				
  				if(dateofHarvest!=null)
  				{
  				String dateofharvest=format.format(dateofHarvest);
  				String hdate[]=dateofharvest.split("-");
  				
  				Integer year=Integer.valueOf(hdate[2]);
  				Integer mon=Integer.valueOf(hdate[1]);
  				Integer day=Integer.valueOf(hdate[0]);
 				 Days daysObj = new Days();
 				 Date d1 = daysObj.getDateOfDaysSub(year, mon, day, -cage.intValue());
 				//String pdate=format.format(d1);
 				 
   			/*	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
   				Calendar cal = Calendar.getInstance();
   				cal = Calendar.getInstance();*/
   				//cal.add(Calendar.DATE, -cage.intValue());
   				//String dad=dateFormat.format(cal.getTime());
   				String sadd=format.format(d1);
   				obj.setDateofharvest(dateofharvest);
  				obj.setDateofplant(sadd);
  				}
  				obj.setSupplier(supplier);
  				obj.setSuppliername(suppliername);
  				obj.setVillage(village);
  				obj.setVariety(variety);
  				obj.setNooftons(nooftons);
  				obj.setRate(rate);
  				obj.setAcer(noofacre);
  				obj.setValue(value);
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="ProcurementReport";
			String Query1=" * from Temp_ProcurementReport order by supplier ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}


	@RequestMapping(value = "/generateYearlyLedgerReportByRyotWiseForSeason", method = RequestMethod.POST)
	public void generateYearlyLedgerReportByRyotWiseForSeason(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		Double	canePrice = 0.0;
		
		Integer caneSlno=0;
		Double	sbac = 0.0;
		Double	otherded = 0.0;
		Double	transportamt = 0.0;
		Double	ulamt = 0.0;
		Double	cdcamt = 0.0;
		Double	harvestamt = 0.0;
		Double	laAmt = 0.0;
		int serialnumber=0;
		Date caneAccFrmdate=null;
		Date caneAccTodate=null;
		String pattern = "dd-MM-yyyy";
		String ryotname="";
		String relativename="";
		String village="";
		int sbaccbranchcode=0;
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String sqq="truncate table Temp_YearlyLedger";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		String sqq1="truncate table Temp_YearlyLedgerForRyot";
		SQLQuery query1=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq1);
		query1.executeUpdate();
		Temp_YearlyLedger obj=null;
		Temp_YearlyLedger obj1=null;
		Temp_YearlyLedgerForRyot yl=null;
		Session session=hibernatedao.getSessionFactory().openSession();
		try
		{
		String sql="select distinct ryotcode from CaneValueSplitup where season='"+season+"' and acctype=0 order by ryotcode" ;	
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			Double	supValue = 0.0;
  			Double	prgqty = 0.0;
  			Double	prgSupValue = 0.0;
  			yl=new Temp_YearlyLedgerForRyot();
  			String ryotcode=(String)((Map<Object,Object>)ls.get(i)).get("ryotcode");
  			String sql1="select distinct caneacslno from CaneValueSplitup where season='"+season+"' and ryotcode='"+ryotcode+"' order by caneacslno" ;
  			List ls1=hibernatedao.findBySqlCriteria(sql1);
  	  	  	if(ls1.size()>0 && ls1 !=null)
  	  		{
  	  		for (int j= 0; j <ls1.size(); j++) 
  			{
  			caneSlno=(Integer)((Map<Object,Object>)ls1.get(j)).get("caneacslno");
  			String sql2="select * from CaneAccountSmry where season='"+season+"' and caneacslno="+caneSlno+"" ;
  			List ls2=hibernatedao.findBySqlCriteria(sql2);
  			if(ls2.size()>0 && ls2 !=null)
  	  		{
  				caneAccFrmdate=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
  				caneAccTodate=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
  	  		}
			String sql4="select ryotcode,sbac,harvesting,cdcamt,otherded,ulamt,transport from CanAccSBDetails where season='"+season+"'  and caneacctslno='"+caneSlno+"'  and ryotcode='"+ryotcode+"'";
		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
		  		if(ls4.size()>0 && ls4 !=null)
		  		{
		  		sbac=(Double)((Map<Object,Object>)ls4.get(0)).get("sbac");
		  		otherded=(Double)((Map<Object,Object>)ls4.get(0)).get("otherded");
		  		transportamt=(Double)((Map<Object,Object>)ls4.get(0)).get("transport");
				ulamt=(Double)((Map<Object,Object>)ls4.get(0)).get("ulamt");
				harvestamt=(Double)((Map<Object,Object>)ls4.get(0)).get("harvesting");
				cdcamt=(Double)((Map<Object,Object>)ls4.get(0)).get("cdcamt");
		  		}
		  		String sql5="select sum(advisedamt) as laAmt from CanAccLoansDetails where season='"+season+"' and caneacctslno="+caneSlno+" and ryotcode='"+ryotcode+"'" ;
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		if(ls5.size()>0 && ls5 !=null)
	  		{
	  		laAmt=(Double)((Map<Object,Object>)ls5.get(0)).get("laAmt");
	  		}
	  		String sql3="select w.weighbridgeslno, w.netwt,s.shiftname,w.actualserialnumber,w.canereceiptenddate from WeighmentDetails w,Shift s  where  w.season='"+season+"' and w.ryotcode='"+ryotcode+"' and w.canereceiptenddate between '"+caneAccFrmdate+"' and '"+caneAccTodate+"' and w.shiftid1=s.shiftid ";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && ls3 !=null)
	  		{
	  		for (int l= 0; l <ls3.size(); l++) 
			{
  				obj=new Temp_YearlyLedger();
  				int weighbridgeslno=(Integer)((Map<Object,Object>)ls3.get(l)).get("weighbridgeslno");
  				serialnumber=(Integer)((Map<Object,Object>)ls3.get(l)).get("actualserialnumber");
  				String	shiftname=(String)((Map<Object,Object>)ls3.get(l)).get("shiftname");
  				Date canereceiptenddate=(Date)((Map<Object,Object>)ls3.get(l)).get("canereceiptenddate");
  				
  		  		List<CaneAcctRules> caneAccountingRules = caneAccountingFunctionalService.getCaneAccRulesForCalcForCaneAccounting(season,canereceiptenddate,canereceiptenddate);
  				if(caneAccountingRules.size()>0 && caneAccountingRules!=null)	
  					canePrice = caneAccountingRules.get(0).getSeasoncaneprice();

  				Double 	supqty=(Double)((Map<Object,Object>)ls3.get(l)).get("netwt");
  				String weighmentDate=format.format(canereceiptenddate);
  				supValue=supqty*canePrice;
  				prgqty=prgqty+supqty;
  				prgSupValue=prgSupValue+supValue;
  				obj.setPrgqty(prgqty);
  				obj.setPrgSupValue(prgSupValue);
  				obj.setRyotcode(ryotcode);
  				obj.setSerialnumber(serialnumber);
  				obj.setWeighmentDate(weighmentDate);
  				obj.setSupqty(supqty);
  				obj.setSupValue(supValue);
  				obj.setShiftname(shiftname);
  				obj.setWeighbridgeslno(weighbridgeslno);
  				
  				session.save(obj);
  			}
	  		}
	  		obj1=new Temp_YearlyLedger();
	  		obj1.setSerialnumber(caneSlno);
	  		obj1.setWeighmentDate(format.format(caneAccTodate));
	  		obj1.setLaAmt(laAmt);
	  		obj1.setHarvestamt(harvestamt);
	  		obj1.setOtherded(otherded);
	  		obj1.setSbac(sbac);
	  		obj1.setTransportamt(transportamt);
	  		obj1.setUlamt(ulamt);
	  		obj1.setCdcamt(cdcamt);
	  		obj1.setRyotcode(ryotcode);
	  		if(harvestamt==null)
	  			harvestamt=0.0;
	  		if(otherded==null)
	  			otherded=0.0;
	  		if(transportamt==null)
	  			transportamt=0.0;
	  		if(ulamt==null)
	  			ulamt=0.0;
	  		if(cdcamt==null)
	  			cdcamt=0.0;
	  		obj1.setTotalDeductions(harvestamt+otherded+transportamt+ulamt+cdcamt);
	  		session.save(obj1);
		}
  	}
  	  	  	
  	  	String sql8="select distinct cs.ryotcode,r.ryotname,r.relativename, v.village,cs.sbac,cs.harvesting,cs.cdcamt,cs.otherded,cs.ulamt,cs.transport,cs.sbaccno,cs.bankcode from CanAccSBDetails cs,village v,ryot r where cs.ryotcode=r.ryotcode and cs.season='"+season+"'  and cs.caneacctslno='"+caneSlno+"'  and r.villagecode=v.villagecode and cs.ryotcode='"+ryotcode+"'";
	  		List ls8=hibernatedao.findBySqlCriteria(sql8);
	  		String sbaccno="";
	  		if(ls8.size()>0 && ls8 !=null)
	  		{
	  			for (int k = 0; k <ls8.size(); k++) 
	  			{
				ryotname=(String)((Map<Object,Object>)ls8.get(k)).get("ryotname");
				relativename=(String)((Map<Object,Object>)ls8.get(k)).get("relativename");
				village=(String)((Map<Object,Object>)ls8.get(k)).get("village");
				sbaccno=(String)((Map<Object,Object>)ls8.get(k)).get("sbaccno");
				sbaccbranchcode=(Integer)((Map<Object,Object>)ls8.get(k)).get("bankcode");
				
	  			}
	  		}
	  		yl.setRyotcode(ryotcode);
	  		yl.setRyotname(ryotname);
	  		yl.setVillage(village);
	  		yl.setSbaccbranchcode(sbaccbranchcode);
	  		yl.setSbaccno(sbaccno);
	  		yl.setRelativename(relativename);
	  		String pamount="";
			String intrestAmt="";
			String loancode="";
			String branchcode="";
			String agrdate="";
			String	referencenumber=" ";
	  		List LonsummaryData = commonService.getLoanSummaryDataForYearlyLedger(season,ryotcode);
			if(LonsummaryData != null && LonsummaryData.size()>0)
			{
				for (int j = 0; j < LonsummaryData.size(); j++)
				{
					Map LoanMap = new HashMap();
					LoanMap = (Map) LonsummaryData.get(j);
					Double	disbursedamount=(Double) LoanMap.get("disbursedamount");
					Double	interestamount=(Double) LoanMap.get("interestamount");
					if(interestamount!=null)
					{
						 interestamount = Math.round(interestamount*100) / 100.0;
					}
					Integer	loannumber=(Integer) LoanMap.get("loannumber");
					Integer	branchcd=(Integer) LoanMap.get("branchcode");
					Date	disburseddate=(Date) LoanMap.get("disburseddate");
					String referencenumber1=(String) LoanMap.get("referencenumber");
					
					String ddate=format.format(disburseddate);
					agrdate +=ddate+"\t\t";
					pamount +=String.valueOf(disbursedamount)+"\t\t    ";
					intrestAmt +=String.valueOf(interestamount)+"\t\t   ";
					loancode +=loannumber.toString()+"\t\t\t";
					referencenumber +=referencenumber1+"\t\t";
					branchcode +=branchcd.toString()+"\t\t\t";
				}
				
			}
			List CaneAccLoanData = commonService.getCaneAccLoanData(season,ryotcode);
			if(CaneAccLoanData != null && CaneAccLoanData.size()>0 && (Map) CaneAccLoanData.get(0)!=null)
			{
				for (int m = 0; m< CaneAccLoanData.size(); m++)
				{
					Map CaneAccMap = new HashMap();
					CaneAccMap = (Map) CaneAccLoanData.get(m);
					Double advanceamount=0.0;
					String adate=null;
					Integer advancecode=(Integer) CaneAccMap.get("advancecode");
					String sql6="select sum(advanceamount) advanceamount,advancedate from AdvanceDetails where season='"+season+"' and ryotcode='"+ryotcode+"' and advancecode="+advancecode+" group by advancedate";
					List ls6=hibernatedao.findBySqlCriteria(sql6);
					if(ls6.size()>0 && ls6!=null)
					{
						advanceamount=(Double)((Map<Object,Object>)ls6.get(0)).get("advanceamount");
						Date advancedate=(Date)((Map<Object,Object>)ls6.get(0)).get("advancedate");
						adate=DateUtils.formatDate(advancedate,Constants.GenericDateFormat.DATE_FORMAT);
					}
				
					agrdate +=adate+"\t\t";
					pamount +=String.valueOf(advanceamount)+"\t\t  ";
					branchcode +=advancecode.toString()+"\t\t\t\t";
					intrestAmt+="\t\t";
					loancode+="\t\t\t\t";
					referencenumber+="\t\t";
				}
			}
			yl.setAgrmntdate(agrdate);
			yl.setPamount(pamount);
			yl.setIamount(intrestAmt);
			yl.setBranchcode(branchcode);
			yl.setLoancode(loancode);
			yl.setReferencenumber(referencenumber);
			String sql9="select rate from canevaluesplitup where season='"+season+"'  and caneacslno='"+caneSlno+"'";
			List ls9=hibernatedao.findBySqlCriteria(sql9);
			if(ls9.size()>0 && ls9!=null)
			{
				Double frpprice=(Double)((Map<Object,Object>)ls9.get(0)).get("rate");
				yl.setFrpprice(frpprice);
			}
			String sql7="select agreedqty,extentsize,plantorratoon  from AgreementDetails  where  seasonyear='"+season+"' and ryotcode='"+ryotcode+"'";// and agreementnumber='"+agreementnumber+"'";
			List ls7=hibernatedao.findBySqlCriteria(sql7);
			if(ls7.size()>0 && ls7!=null)
			{
				Double ratoon=0.0;
				Double plant=0.0;
				Double agrqty=0.0;
				 for (int p = 0; p <ls7.size(); p++)
				 {
					String plantorratoon=(String)((Map<Object,Object>)ls7.get(p)).get("plantorratoon");
					if (Integer.parseInt(plantorratoon)==1)
					{
						 Double pt=(Double)((Map<Object,Object>)ls7.get(p)).get("extentsize");
						 plant +=pt;
						
					}
					if (Integer.parseInt(plantorratoon)>1)
					{
						Double rt=(Double)((Map<Object,Object>)ls7.get(p)).get("extentsize");
						ratoon +=rt;
					}
					Double agrqt=(Double)((Map<Object,Object>)ls7.get(p)).get("agreedqty");
					agrqty+=agrqt;
				}
				 yl.setAgrqty(agrqty);
				 yl.setPlant(plant);
				 yl.setRatoon(ratoon);
			}
			session.save(yl);
  	  		}
  		}
  		session.flush();
  		session.close();
		}
		catch(Exception e)
		{
		e.printStackTrace();	
		}
  		try
		{
			String reportFileName ="YearlyLedgerReport";
			String Query1=" yl.prgqty,yl.prgSupValue,yl.serialnumber,yl.weighmentDate,yl.supqty,yl.supvalue,yl.shiftname,yl.weighbridgeslno,yl.laAmt,yl.harvestamt,yl.otherded,yl.sbac,yl.transportamt,yl.ulamt,yl.cdcamt,yl.totalDeductions,yls.ryotcode,yls.ryotname,yls.relativename,yls.village,yls.sbaccno,yls.agrmntdate,yls.pamount,yls.iamount,yls.branchcode,yls.loancode,yls.frpprice,yls.agrqty,yls.plant,yls.ratoon,yls.sbaccbranchcode,yls.referencenumber,yls.sbaccno from Temp_YearlyLedger as yl,Temp_YearlyLedgerForRyot as yls where yl.ryotcode=yls.ryotcode order by  yls.ryotcode";
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
  		

	@RequestMapping(value = "/generateSeedUtilizationReport", method = RequestMethod.POST)
	public void generateSeedUtilizationReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		
		
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_SeedUtilizationReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedUtilizationReport obj=new Temp_SeedUtilizationReport();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select s.*,r.ryotname,v.village,vi.variety,ry.ryotname as consumer from SeedSource s,ryot r,village v,variety vi,ryot ry  where s.season='"+season+"' and s.sourcesupplieddate between '"+fromdate+"' and '"+todate+"' and s.seedsrcode=r.ryotcode and s.villagecode=v.villagecode and s.varietycode=vi.varietycode and purpose=1  and s.consumercode=ry.ryotcode " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_SeedUtilizationReport();
  				
  				String consumercode=(String)((Map<Object,Object>)ls.get(i)).get("consumercode");
  				String consumername=(String)((Map<Object,Object>)ls.get(i)).get("consumer");
  			 	String supplier=(String)((Map<Object,Object>)ls.get(i)).get("seedsrcode");
  				String 	suppliername=(String)((Map<Object,Object>)ls.get(i)).get("ryotname");
  				String 	village=(String)((Map<Object,Object>)ls.get(i)).get("village");
  				String 	variety=(String)((Map<Object,Object>)ls.get(i)).get("variety");
  				Double 	nooftons=(Double)((Map<Object,Object>)ls.get(i)).get("finalweight");
  				Double 	transportvalue=(Double)((Map<Object,Object>)ls.get(i)).get("transcharges");
  				Double 	value=(Double)((Map<Object,Object>)ls.get(i)).get("totalcost");
  				Date 	dateofApp=(Date)((Map<Object,Object>)ls.get(i)).get("certdate");
  				if(dateofApp !=null)
  				{
  					String dateofapproval=format.format(dateofApp);
  					obj.setDateofapproval(dateofapproval);
  				}
  				
  				String sql2="select v.village consumervillage from  Ryot r ,village v  where r.ryotcode='"+consumercode+"'  and r.villagecode=v.villagecode" ;
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  		  		if(ls2.size()>0 && ls2 !=null)
  		  		{
  		  			String consumervillage=(String)((Map<Object,Object>)ls2.get(0)).get("consumervillage");
  		  			obj.setConsumervillage(consumervillage);
  		  		}
  				Double 	noofacre=(Double)((Map<Object,Object>)ls.get(i)).get("noofacre");
  				String sql1="select isnull(sum(finalweight),0) finalweight ,isnull(sum(noofacre),0) noofacre from SeedSource  where season='"+season+"' and purpose=1 and sourcesupplieddate between '"+fromdate+"' and '"+todate+"' and seedsrcode='"+supplier+"' " ;
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0 && ls1!=null)
  		  		{
  		  			Double 	tfinalweight=(Double)((Map<Object,Object>)ls1.get(0)).get("finalweight");
  		  			Double 	tnoofacre=(Double)((Map<Object,Object>)ls1.get(0)).get("noofacre");
  		  			
  		  			obj.setTfinalweight(tfinalweight);
  		  			obj.setTnoofacre(tnoofacre);
  		  		
  		  		}
  		  		obj.setValue(value);
  				obj.setNoofacre(noofacre);
  				obj.setConsumer(consumercode);
  				obj.setConsumername(consumername);
  				obj.setSupplier(supplier);
  				obj.setSuppliername(suppliername);
  				obj.setVillage(village);
  				obj.setVariety(variety);
  				obj.setNooftons(nooftons);
  				obj.setTransportvalue(transportvalue);
  				
  				obj.setQty(0.0);
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedUtilizationReport";
			String Query1=" * from Temp_SeedUtilizationReport order by supplier ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateSeedDevlopmentReport", method = RequestMethod.POST)
	public void generateSeedDevlopmentReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_SeedDevlopment";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedDevlopment obj=new Temp_SeedDevlopment();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct batchno  from SeedSource  where season='"+season+"'  and  purpose=0  " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		String batchSeries=null;
  		Integer varietycode=null;
  		if(ls.size()>0 && ls !=null)
  		{
  			for (int i= 0; i <ls.size(); i++) 
  			{
  				obj=new Temp_SeedDevlopment();
  				batchSeries=(String)((Map<Object,Object>)ls.get(i)).get("batchno");
  			
  				String sql1=" select Sum(finalweight) finalweight,varietycode,seedsrcode from SeedSource where season='"+season+"' and  purpose=0  and batchno='"+batchSeries+"' group by varietycode,seedsrcode" ;
  				List ls1=hibernatedao.findBySqlCriteria(sql1);
  				if(ls1.size()>0 && ls1 !=null)
  				{
	  	  			Double	finalweight=(Double)((Map<Object,Object>)ls1.get(0)).get("finalweight");
	  	  			varietycode=(Integer)((Map<Object,Object>)ls1.get(0)).get("varietycode");
	  	  			String seedsrcode=(String)((Map<Object,Object>)ls1.get(0)).get("seedsrcode");
	  	  			String sql2=" select v.variety,r.ryotname from variety v,ryot r  where v.varietycode="+varietycode+" and r.ryotcode='"+seedsrcode+"'" ;
	  	  			List ls2=hibernatedao.findBySqlCriteria(sql2);
	  	  			
	  	  			String variety=(String)((Map<Object,Object>)ls2.get(0)).get("variety");
	  	  			String suppliername=(String)((Map<Object,Object>)ls2.get(0)).get("ryotname");
	  	  			
	  	  			obj.setTons(finalweight);
	  	  			obj.setSupplier(seedsrcode);
	  	  			obj.setVariety(variety);
	  	  			obj.setSuppliername(suppliername);
  				}
	  	  		String sql3=" select Sum(totalSeedlings) totalSeedlings from  BatchMaster where batchSeries='"+batchSeries+"' and variety="+varietycode+"" ;
		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  		if(ls3.size()>0 && ls3 !=null)
		  		{
		  			Double	totalSeedlings=(Double)((Map<Object,Object>)ls3.get(0)).get("totalSeedlings");
		  			obj.setNoofseedlings(totalSeedlings);
		  		}
	  			
		  		obj.setBatch(batchSeries);
		  		session.save(obj);
  			}
  		
  		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedDevlopmentReport";
			String Query1=" * from Temp_SeedDevlopment ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", season);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	
	@RequestMapping(value = "/generateSeedDistrubution", method = RequestMethod.POST)
	public void generateSeedDistrubution(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_SeedDistribution";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedDistribution obj=new Temp_SeedDistribution();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="SELECT  dateOfDispatch,dispatchNo,indentNumbers, ryotCode, ryotName  FROM DispatchSummary   where  season='"+season+"' and  dateOfDispatch between '"+fromdate+"' and '"+todate+"' " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_SeedDistribution();
  			
  				
  			 	String ryotCode=(String)((Map<Object,Object>)ls.get(i)).get("ryotCode");
  				String 	ryotName=(String)((Map<Object,Object>)ls.get(i)).get("ryotName");
  			//	String 	village=(String)((Map<Object,Object>)ls.get(i)).get("village");
  				String 	dispatchNo=(String)((Map<Object,Object>)ls.get(i)).get("dispatchNo");
  				String 	indentNo=(String)((Map<Object,Object>)ls.get(i)).get("indentNumbers");
  				Date 	dateOfDispatch=(Date)((Map<Object,Object>)ls.get(i)).get("dateOfDispatch");
	  			String dispatchdate=format.format(dateOfDispatch);
	  			obj.setDispatchdate(dispatchdate);
	  			
				String sql1="select k.landVillageCode, v.variety from KindIndentDetails k, variety v   where k.indentNo='"+indentNo+"' and k.variety=v.varietycode and k.season='"+season+"' " ;
		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
		  		if(ls1.size()>0 && ls1 !=null)
		  		{
		  			String 	variety=(String)((Map<Object,Object>)ls1.get(0)).get("variety");
		  			String 	vldcode=(String)((Map<Object,Object>)ls1.get(0)).get("landVillageCode");
		  			List<Village> villageName= ahFuctionalService.getVillageForLedger(vldcode);
					if(villageName != null && villageName.size()>0)
					{
						String villageNam = villageName.get(0).getVillage();
					
						obj.setVillage(villageNam);
					}
		  			
		  			obj.setVariety(variety);
		  		}
		  		
		  		String sql2="select SummaryDate from KindIndentSummary   where indentNo='"+indentNo+"' and season='"+season+"' " ;
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		if(ls2.size()>0 && ls2 !=null)
		  		{
		  			Date 	SummaryDate=(Date)((Map<Object,Object>)ls2.get(0)).get("SummaryDate");
	  				String indentdate=format.format(SummaryDate);
	  				obj.setIndentdate(indentdate);
		  		}
  				obj.setRyotcode(ryotCode);
  				obj.setRyotname(ryotName);
  				obj.setDispatchno(dispatchNo);
  				obj.setIndentno(indentNo);
  			
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedDistributionReport";
			String Query1=" * from Temp_SeedDistribution  ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}		
		@RequestMapping(value = "/generateSeedProduction", method = RequestMethod.POST)
	public void generateSeedProduction(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_SeedProduction";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedProduction obj=new Temp_SeedProduction();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select t.trayFillingDate,t.batchNo,t.batchSeries,t.totalTrays,t.variety as varietycode,  v.variety from TrayFillingDetails t,variety v where   t. season='"+season+"' and  t.trayFillingDate between '"+fromdate+"' and '"+todate+"' and t.variety=v.varietycode " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_SeedProduction();
  			
  				String varietycode=(String)((Map<Object,Object>)ls.get(i)).get("varietycode");
  			 	String batchNo=(String)((Map<Object,Object>)ls.get(i)).get("batchNo");
  				String 	batchSeries=(String)((Map<Object,Object>)ls.get(i)).get("batchSeries");
  				String 	variety=(String)((Map<Object,Object>)ls.get(i)).get("variety");
  				Integer 	totalTrays=(Integer)((Map<Object,Object>)ls.get(i)).get("totalTrays");
  				Date 	trayFillingDate=(Date)((Map<Object,Object>)ls.get(i)).get("trayFillingDate");
	  			String trayfilldate=format.format(trayFillingDate);
	  			
	  			obj.setBatchno(batchNo);
	  			obj.setVariety(variety);
	  			obj.setNooftrays(totalTrays);
	  			obj.setTrayfilldate(trayfilldate);
		  		String sql2="SELECT sum(tubWt) tubWt,count(*) count FROM BudCuttingDetails where season='"+season+"' and  batchSeries='"+batchSeries+"' and variety='"+varietycode+"'" ;
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		if(ls2.size()>0 && ls2 !=null)
		  		{
		  			Double 	tubWt=(Double)((Map<Object,Object>)ls2.get(0)).get("tubWt");
		  			Integer 	count=(Integer)((Map<Object,Object>)ls2.get(0)).get("count");
		  			if(tubWt !=null)
		  			{
		  				Double stubwt = (tubWt/count.doubleValue())*100;
		  				obj.setStubwt(stubwt);
		  			}
		  		}
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedProductionReport";
			String Query1=" * from Temp_SeedProduction  ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	

	@RequestMapping(value = "/generateSeedlingProcess", method = RequestMethod.POST)
	public void generateSeedlingProcess(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		 
		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		String sqq="truncate table Temp_SeedlingProcess";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedlingProcess obj=new Temp_SeedlingProcess();
		Session session=hibernatedao.getSessionFactory().openSession();
		

		
		List<Date> dates = new ArrayList<Date>();
		DateFormat formatter ; 
		formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date  startDate = (Date)formatter.parse(fromDate); 
		Date  endDate = (Date)formatter.parse(toDate);
		long interval = 24*1000 * 60 * 60; // 1 hour in millis
		long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
		long curTime = startDate.getTime();
		while (curTime <= endTime)
		{
		    dates.add(new Date(curTime));
		    curTime += interval;
		}
		for(int i=0;i<dates.size();i++)
		{
		    Date lDate =(Date)dates.get(i);
		    String ds = formatter.format(lDate); 
		    String dsds[]=ds.split("/");
		    String dsd=dsds[0]+"-"+dsds[1]+"-"+dsds[2];
		    Date fromdate=DateUtils.getSqlDateFromString(dsd,Constants.GenericDateFormat.DATE_FORMAT);
		    obj=new Temp_SeedlingProcess();
		    
		    String sql=" select sum(totalBuds) totalBuds,machine from budcuttingdetails where cuttingDetailsdate='"+fromdate+"' group by machine " ;
	  		List ls=hibernatedao.findBySqlCriteria(sql);
	  		if(ls.size()>0 && ls !=null)
	  		{
	  			Double totalBud=0.0;
	  			String machine="";
	  			for(int j=0;j<ls.size();j++)
	  			{
	  				Double totalBuds=(Double)((Map<Object,Object>)ls.get(j)).get("totalBuds");
	  				if(totalBuds !=null)
		  			{
	  					totalBud+=totalBuds;
		  			}
	  				String mac=(String)((Map<Object,Object>)ls.get(j)).get("machine");
	  				machine +=mac+",";
	  			}
	  			obj.setBudcutting(totalBud);
	  			obj.setMachine(machine);
	  		}
	  		String sql1="  select isnull(sum(noOfTubs),0) noOfTubs from ChemicalTreatmentDetails where treatmentDate='"+fromdate+"' " ;
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		if(ls1.size()>0 && ls1 !=null)
	  		{
	  			Double noOfTubs=(Double)((Map<Object,Object>)ls1.get(0)).get("noOfTubs");
	  			if(noOfTubs !=null)
	  			{
	  				obj.setTreatmentandhardning(noOfTubs);
	  			}
	  		}
	  		String sql2=" select isnull(sum(totalNoOfSeedlings),0) totalNoOfSeedlings from TrayFillingSummary where trayFillingDate='"+fromdate+"' " ;
	  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		if(ls2.size()>0 && ls2 !=null)
	  		{
	  			Double totalNoOfSeedlings=(Double)((Map<Object,Object>)ls2.get(0)).get("totalNoOfSeedlings");
	  			if(totalNoOfSeedlings !=null)
	  			{
	  				obj.setTrayfilling(totalNoOfSeedlings);
	  			}
	  		}
	  		String sql3=" select isnull(sum(caneWt),0) caneWt from ShiftedCaneDetails where shiftedCaneDate='"+fromdate+"' " ;
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && ls3 !=null)
	  		{
	  			Double caneWt=(Double)((Map<Object,Object>)ls3.get(0)).get("caneWt");
	  			if(caneWt !=null)
	  			{
	  				obj.setTrayshifting(caneWt);
	  			}
	  		}
	  		String sql4=" select isnull(sum(totalSeedlingsIn),0)- isnull(sum(totalSeedlingsOut),0) irrigation from GreenHouseDetails where detailsDate='"+fromdate+"' " ;
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		if(ls4.size()>0 && ls4 !=null)
	  		{
	  			Double irrigation=(Double)((Map<Object,Object>)ls4.get(0)).get("irrigation");
	  			if(irrigation !=null)
	  			{
	  				obj.setIrrigation(irrigation);
	  			}
	  		}
	  		String sql5="select isnull(sum(totalSeedlingsOut),0)  totalSeedlingsOut from TrimmingDetails where detailsDate='"+fromdate+"' " ;
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		if(ls5.size()>0 && ls5 !=null)
	  		{
	  			Double totalSeedlingsOut=(Double)((Map<Object,Object>)ls5.get(0)).get("totalSeedlingsOut");
	  			if(totalSeedlingsOut !=null)
	  			{
	  				obj.setTrimming(totalSeedlingsOut);
	  			}
	  		}
	  		obj.setSdate(ds);
	  		session.save(obj);
		}
	
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedlingProcessReport";
			String Query1=" * from Temp_SeedlingProcess ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	@RequestMapping(value = "/generateUnloadContractorUnloadingReport", method = RequestMethod.POST)
	public void generateUnloadingContractorUnloadReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		String urate1=request.getParameter("unloadingRate");
		
		double ulrate=Double.parseDouble(urate1);
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern); 
		
		try
		{
		String sqq="truncate table Temp_UnloadContractorReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Integer serialnumber=0;
		Integer ulcode=0;
		Temp_UnloadContractorReport obj=null;
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select distinct uccode from WeighmentDetails  where season='"+season+"' and uccode!=0 and canereceiptenddate between '"+fromdate+"' and '"+todate+"'";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			ulcode=(Integer)((Map<Object,Object>)ls.get(i)).get("uccode");
			String ulname=ahFuctionalService.GetUnloadingContractorForWeighment(ulcode);
			String sql1="select distinct canereceiptenddate from WeighmentDetails  where season='"+season+"' and uccode="+ulcode+" and canereceiptenddate between '"+fromdate+"' and '"+todate+"' order by canereceiptenddate";
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		if(ls1.size()>0 && ls1 !=null)
	  		{
	  		for (int j= 0; j <ls1.size();j++) 
			{
  				
  				String sql3="select  w.netwt,s.shiftname,w.actualserialnumber,w.canereceiptenddate,w.vehicleno from WeighmentDetails w,Shift s  where  w.season='"+season+"' and uccode="+ulcode+" and w.canereceiptenddate='"+(Date)((Map<Object,Object>)ls1.get(j)).get("canereceiptenddate")+"' and w.shiftid1=s.shiftid ";
  		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  		  		if(ls3.size()>0 && ls3 !=null)
  		  		{
  		  		for (int l= 0; l <ls3.size(); l++) 
  				{
  		  			obj=new Temp_UnloadContractorReport();
  	  				
  	  				serialnumber=(Integer)((Map<Object,Object>)ls3.get(l)).get("actualserialnumber");
  	  				String	shiftname=(String)((Map<Object,Object>)ls3.get(l)).get("shiftname");
  	  				Date canereceiptenddate=(Date)((Map<Object,Object>)ls3.get(l)).get("canereceiptenddate");
  	  				String	vehicleno=(String)((Map<Object,Object>)ls3.get(l)).get("vehicleno");
  	  				Double 	supqty=(Double)((Map<Object,Object>)ls3.get(l)).get("netwt");
  	  				String weighmentDate=format.format(canereceiptenddate);
  	  				Double ulamt=supqty*ulrate;
  	  				obj.setRecieptnumber(serialnumber);
  	  				obj.setShiftname(shiftname);
  	  				obj.setWeighmentDate(weighmentDate);
  	  				obj.setShiftname(shiftname);
  	  				obj.setVehiclenumber(vehicleno);
  	  				obj.setUnloadinamount(ulamt);
  	  				obj.setUlname(ulname);
  	  				obj.setUnloadingqty(supqty);
  	  				obj.setUlcode(ulcode);
  	  				session.save(obj);
  	  			}
  		  		}
			}
	  		}
		}
  		}
  		session.flush();
  		session.close();
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
  		try
		{
			String reportFileName ="UnloadContractorUnloadingReport";
			String Query1=" ulname,unloadinamount,unloadingqty,weighmentDate,shiftname,recieptnumber,vehiclenumber,ulcode from Temp_UnloadContractorReport order by ulname,weighmentDate ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("season", season);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	//For Pending Loans & Advances Report
	/*
	@RequestMapping(value = "/generatePendingLoansAndAdvances", method = RequestMethod.POST)
	public void generatePendingLoansAndAdvances(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		
		String season=sName[1];
		String rptFormat = request.getParameter("status");
		String pendingType = request.getParameter("PendingType");
		String loanOrAdv = pendingType.split(":")[1];
		try
		{
		String sqq="truncate table Temp_PendingLoansAndAdvances";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Integer serialnumber=0;
		Integer advcode=0;
		Temp_PendingLoansAndAdvances obj=null;
		Session session=hibernatedao.getSessionFactory().openSession();
		
		if(loanOrAdv.equalsIgnoreCase("advances"))
		{
		String sql="select distinct advancecode from AdvancePrincipleAmounts  where season='"+season+"' and principle>0";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  			advcode=(Integer)((Map<Object,Object>)ls.get(i)).get("advancecode");
			String advname=ahFuctionalService.GetAdvanceNameByAdvCode(advcode);
			String sql1="select ryotcode from AdvancePrincipleAmounts  where season='"+season+"' and advancecode="+advcode+" and principle>0 order by ryotcode";
	  		List ls1=hibernatedao.findBySqlCriteria(sql1);
	  		if(ls1.size()>0 && ls1 !=null)
	  		{
	  			
	  			
	  		for (int j= 0; j <ls1.size();j++) 
			{
	  			Double advprinciple=0.0;
	  			Double pendingadv=0.0;
	  			Double supqty=0.0;
	  			Double paidAmt=0.0;
	  			
	  			String	ryotCode=(String)((Map<Object,Object>)ls1.get(j)).get("ryotcode");
  				String sql2="select  r.ryotname,v.village from Ryot r,Village v  where   r.villagecode=v.villagecode and ryotcode='"+ryotCode+"'";
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
	  		  	String	ryotname=(String)((Map<Object,Object>)ls2.get(0)).get("ryotname");
				String village=(String)((Map<Object,Object>)ls2.get(0)).get("village");
  		  		
  		  		String sql3="select sum(advanceamount) as advanceamount from AdvanceDetails  where season='"+season+"' and advancecode="+advcode+" and ryotcode='"+ryotCode+"'";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
	  		if(ls3.size()>0 && (Double)((Map<Object,Object>)ls3.get(0)).get("advanceamount") !=null)
	  		{
	  			advprinciple=(Double)((Map<Object,Object>)ls3.get(0)).get("advanceamount");
	  		}
	  		String sql4="select sum(principle) as principle from AdvancePrincipleAmounts  where season='"+season+"' and advancecode="+advcode+" and ryotcode='"+ryotCode+"'";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		if(ls4.size()>0 && (Double)((Map<Object,Object>)ls4.get(0)).get("principle") !=null)
	  		{
	  			pendingadv=(Double)((Map<Object,Object>)ls4.get(0)).get("principle");
	  		}
	  		String sql5="select sum(advisedamt) as paidamt from CanAccLoansDetails  where season='"+season+"' and ryotcode='"+ryotCode+"' and branchcode="+advcode+" and advloan=0";
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		if(ls5.size()>0 && (Double)((Map<Object,Object>)ls5.get(0)).get("paidamt") !=null)
	  		{
	  			paidAmt=(Double)((Map<Object,Object>)ls5.get(0)).get("paidamt");
	  		}
	  		String sql6="select sum(netwt) as supqty from WeighmentDetails  where season='"+season+"' and ryotcode='"+ryotCode+"'";
	  		List ls6=hibernatedao.findBySqlCriteria(sql6);
	  		if(ls6.size()>0 && (Double)((Map<Object,Object>)ls6.get(0)).get("supqty") !=null)
	  		{
	  			supqty=(Double)((Map<Object,Object>)ls6.get(0)).get("supqty");
	  		}
	  			obj=new Temp_PendingLoansAndAdvances();
	  			obj.setAdvloanamt(advprinciple);
	  			obj.setBankcode(advcode);
	  			obj.setRyotcode(ryotCode);
	  			obj.setRyotname(ryotname);
	  			obj.setSupquantity(supqty);
	  			obj.setTotalbal(pendingadv);
	  			obj.setTotalrecovery(paidAmt);
	  			obj.setVillage(village);
  				session.save(obj);
  	  			}
  		  		}
			}
	  		}
		}
		else
		{
			Integer branchCode=0;
			String sql="select distinct branchcode from LoanPrincipleAmounts  where season='"+season+"' and principle>0";
	  		List ls=hibernatedao.findBySqlCriteria(sql);
	  		if(ls.size()>0 && ls !=null)
	  		{
	  		for (int i= 0; i <ls.size(); i++) 
			{
	  			
	  			branchCode=(Integer)((Map<Object,Object>)ls.get(i)).get("branchcode");
	  			
				String branchName=ahFuctionalService.GetBranchNameByBranchCode(branchCode);
				String sql1="select ryotcode from LoanPrincipleAmounts  where season='"+season+"' and branchcode="+branchCode+"  and principle>0 order by ryotcode";
		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
		  		if(ls1.size()>0 && ls1 !=null)
		  		{
		  			
		  			
		  			
		  		for (int j= 0; j <ls1.size();j++) 
				{
		  			
		  			Double loanprinciple=0.0;
		  			Double pendingLoan=0.0;
		  			Double supqty=0.0;
		  			Double paidAmt=0.0;
		  			Double spaidAmt=0.0;
		  			Integer loanno=0;
		  			String loannum="";
		  			String accnum="";
		  			String accnum1="";
		  			Double intramt=0.0;
		  			
		  			String	ryotCode=(String)((Map<Object,Object>)ls1.get(j)).get("ryotcode");
	  				String sql2="select  r.ryotname,v.village from Ryot r,Village v  where   r.villagecode=v.villagecode and ryotcode='"+ryotCode+"'";
	  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		  	String	ryotname=(String)((Map<Object,Object>)ls2.get(0)).get("ryotname");
					String village=(String)((Map<Object,Object>)ls2.get(0)).get("village");
	  		  		
	  		  		String sql3="select sum(disbursedamount) as loanprinciple from LoanDetails  where Season='"+season+"' and branchcode="+branchCode+" and ryotcode='"+ryotCode+"' and disburseddate is not null";
		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  		if(ls3.size()>0 && (Double)((Map<Object,Object>)ls3.get(0)).get("loanprinciple") !=null)
		  		{
		  			loanprinciple=(Double)((Map<Object,Object>)ls3.get(0)).get("loanprinciple");
		  		}
		  		String sql8="select loannumber,referencenumber from LoanDetails  where Season='"+season+"' and branchcode="+branchCode+" and ryotcode='"+ryotCode+"' and disburseddate is not null";
		  		List ls8=hibernatedao.findBySqlCriteria(sql8);
		  		if(ls8.size()>0 && (String)((Map<Object,Object>)ls8.get(0)).get("referencenumber") !=null)
		  		{
		  		for(int m=0;m<ls8.size();m++)
		  		{
		  			loanno=(Integer)((Map<Object,Object>)ls8.get(m)).get("loannumber");
		  			loannum=loannum+loanno.toString()+"\t";
		  			accnum=(String)((Map<Object,Object>)ls8.get(m)).get("referencenumber");
		  			accnum1+=accnum+"\t";
		  		}
		  		}
		  		String sql4="select sum(principle) as pendingamt from LoanPrincipleAmounts  where season='"+season+"' and branchcode="+branchCode+" and ryotcode='"+ryotCode+"'";
		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
		  		if(ls4.size()>0 && (Double)((Map<Object,Object>)ls4.get(0)).get("pendingamt") !=null)
		  		{
		  			pendingLoan=(Double)((Map<Object,Object>)ls4.get(0)).get("pendingamt");
		  		}
		  		String sql5="select sum(advisedamt) as advisedamt from CanAccLoansDetails  where season='"+season+"' and ryotcode='"+ryotCode+"' and branchcode="+branchCode+" and advloan=1";
		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
		  		if(ls5.size()>0 && (Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt") !=null)
		  		{
		  			spaidAmt=(Double)((Map<Object,Object>)ls5.get(0)).get("advisedamt");
		  		}
		  		String sql7="select sum(interestamount) as intramt from LoanSummary  where season='"+season+"' and branchcode="+branchCode+" and ryotcode='"+ryotCode+"'";
		  		List ls7=hibernatedao.findBySqlCriteria(sql7);
		  		if(ls7.size()>0 && (Double)((Map<Object,Object>)ls7.get(0)).get("intramt") !=null)
		  		{
		  			intramt=(Double)((Map<Object,Object>)ls7.get(0)).get("intramt");
		  		}
		  		String sql6="select sum(netwt) as supqty from WeighmentDetails  where season='"+season+"' and ryotcode='"+ryotCode+"'";
		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
		  		if(ls6.size()>0 && (Double)((Map<Object,Object>)ls6.get(0)).get("supqty") !=null)
		  		{
		  			supqty=(Double)((Map<Object,Object>)ls6.get(0)).get("supqty");
		  		}
		  			obj=new Temp_PendingLoansAndAdvances();
		  			obj.setAdvloanamt(loanprinciple);
		  			obj.setBankcode(branchCode);
		  			obj.setRyotcode(ryotCode);
		  			obj.setRyotname(ryotname);
		  			obj.setSupquantity(supqty);
		  			obj.setTotalbal(pendingLoan);
		  			obj.setTotalrecovery(spaidAmt);
		  			obj.setVillage(village);
		  			obj.setRefno(accnum1);
		  			obj.setLoanno(loannum);
		  			obj.setIntramt(intramt);
	  				session.save(obj);
	  	  			}
	  		  		}
				}
		  		}
		}
  		session.flush();
  		session.close();
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
  		try
		{
  			String reportFileName ="";
  			if(loanOrAdv.equalsIgnoreCase("advances"))
  			{
			reportFileName ="PendingAdvancesReport";
  			}
  			else
  			{
			reportFileName ="PendingLoansReport";
  			}
			String Query1=" * from Temp_PendingLoansAndAdvances order by bankcode,ryotcode ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("season", season);
	        Date sysdate = new Date();
	        hmParams.put("cdate", DateUtils.formatDate(sysdate,Constants.GenericDateFormat.DATE_FORMAT));
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	*/
	
	@RequestMapping(value = "/generateUpdatePaymentsReport", method = RequestMethod.POST)
	public void generateUpdatePaymentsReport(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		String season=request.getParameter("season");
		String caneno=request.getParameter("ftDate");
		String rptFormat = request.getParameter("mode");
		String season1[]=season.split(":");
		String cslnum[]=caneno.split(":");
		String ftdate=null;
		//String pattern = "dd/MM/yyyy";
		//SimpleDateFormat format = new SimpleDateFormat(pattern);
		Integer caneslno=Integer.parseInt(cslnum[1]);
		try
		{
		String sqq="truncate table Temp_UpdatePaymentsReport";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_UpdatePaymentsReport obj=null;
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql1=null;
		if(caneslno==0)
		{
			sql1="select distinct ryotcode,caneacslno from CaneValueSplitup where season='"+season1[1]+"' and acctype=0 order by caneacslno,ryotcode" ;
		}
		else
		{
			sql1="select distinct ryotcode,caneacslno from CaneValueSplitup where season='"+season1[1]+"' and acctype=0 and caneacslno="+caneslno+" order by caneacslno,ryotcode" ;
		}
		List ls1=hibernatedao.findBySqlCriteria(sql1);
		if(ls1.size()>0 && (String)((Map<Object,Object>)ls1.get(0)).get("ryotcode") !=null)
		{
		for(int i=0;i<ls1.size();i++)
		{
			Integer	caneslno1=(Integer)((Map<Object,Object>)ls1.get(i)).get("caneacslno");
			Double lpaidamt=0.0;
			Double sbpaidamt=0.0;
			String paymentSource="";
			String fdate=null;
			String tdate=null;
			String sql2="select datefrom,dateto  from CaneAccountSmry   where caneacslno="+caneslno1+" and season='"+season1[1]+"'";
			List ls2=hibernatedao.findBySqlCriteria(sql2);
		if(ls2.size()>0 && ls2!=null)
		{
			Date datefrom=(Date)((Map<Object,Object>)ls2.get(0)).get("datefrom");
			Date dateto=(Date)((Map<Object,Object>)ls2.get(0)).get("dateto");
			fdate=DateUtils.formatDate(datefrom,Constants.GenericDateFormat.DATE_FORMAT);
			tdate=DateUtils.formatDate(dateto,Constants.GenericDateFormat.DATE_FORMAT);
			ftdate=fdate+" To "+tdate;
		}
			String	ryotCode=(String)((Map<Object,Object>)ls1.get(i)).get("ryotcode");
			String sql3="select  r.ryotname,v.village from Ryot r,Village v  where   r.villagecode=v.villagecode and ryotcode='"+ryotCode+"'";
	  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  	String	ryotname=(String)((Map<Object,Object>)ls3.get(0)).get("ryotname");
		  	String village=(String)((Map<Object,Object>)ls3.get(0)).get("village");
		  	
		  	String sql4="select sum(advisedamt) as advisedamt from CanAccLoansDetails  where season='"+season1[1]+"' and ryotcode='"+ryotCode+"' and  caneacctslno="+caneslno1+" and advloan=1";
	  		List ls4=hibernatedao.findBySqlCriteria(sql4);
	  		if(ls4.size()>0 && (Double)((Map<Object,Object>)ls4.get(0)).get("advisedamt") !=null)
	  		{
	  			lpaidamt=(Double)((Map<Object,Object>)ls4.get(0)).get("advisedamt");
	  		}
	  		String sql5="select advamt,paymentsource from CanAccSBDetails  where season='"+season1[1]+"' and ryotcode='"+ryotCode+"' and caneacctslno="+caneslno1+"";
	  		List ls5=hibernatedao.findBySqlCriteria(sql5);
	  		if(ls5.size()>0 && (Double)((Map<Object,Object>)ls5.get(0)).get("advamt") !=null)
	  		{
	  			sbpaidamt=(Double)((Map<Object,Object>)ls5.get(0)).get("advamt");
	  			paymentSource=(String)((Map<Object,Object>)ls5.get(0)).get("paymentsource");
	  		}
	  		obj=new Temp_UpdatePaymentsReport();
  			obj.setCaneaccperiod(ftdate);
  			obj.setLoanpaid(lpaidamt);
  			obj.setPaymetsource(paymentSource);
  			obj.setRyotcode(ryotCode);
  			obj.setRyotname(ryotname);
  			obj.setSbpaid(sbpaidamt);
  			obj.setTotalpaid(lpaidamt+sbpaidamt);
  			obj.setVillage(village);
			session.save(obj);
		}
		}
		session.flush();
		session.close();
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
		try
		{
			String reportFileName ="UpdatePaymentsReport";
			String Query1=" village,ryotcode,ryotname,caneaccperiod,paymetsource,loanpaid,sbpaid,totalpaid from Temp_UpdatePaymentsReport ";
			//Parameters as Map to be passed to Jasper
			HashMap<String,Object> hmParams=new HashMap<String,Object>();
			hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
			hmParams.put("qrs", Query1);
			if(caneslno==0)
			{
			Date sysdate = new Date();
			hmParams.put("caneAccDate",DateUtils.formatDate(sysdate,Constants.GenericDateFormat.DATE_FORMAT));
			}
			else
			{
				hmParams.put("caneAccDate",ftdate);
			}
			generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateVegetablesProduction", method = RequestMethod.POST)
	public void generateVegetablesProduction(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_VegetableFrootsProduction";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_VegetableFrootsProduction obj=new Temp_VegetableFrootsProduction();
		Session session=hibernatedao.getSessionFactory().openSession();
		
		String sql="select distinct batchSeries from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'S'  " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  				obj=new Temp_VegetableFrootsProduction();
  				String batchSeries=(String)((Map<Object,Object>)ls.get(i)).get("batchSeries");
  				String sql2="select seedsrcode from SeedSource   where season='"+season+"' and  batchno='"+batchSeries+"' " ;
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  		  		if(ls2.size()>0 && ls2 !=null)
  		  		{
  		  			String seedsrcode=(String)((Map<Object,Object>)ls2.get(0)).get("seedsrcode");
  		  			obj.setSourceofseed(seedsrcode);
  		  		}
  				
  				
  				String sql1="select isnull(sum(TotalNoOfSeedlingss),0) TotalNoOfSeedlingss ,isnull(sum(totalTrays),0) totalTrays from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'S' and batchSeries='"+batchSeries+"' " ;
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0 && ls1 !=null)
  		  		{
  		  			Double TotalNoOfSeedlingss=(Double)((Map<Object,Object>)ls1.get(0)).get("TotalNoOfSeedlingss");
  		  			Integer totalTrays=(Integer)((Map<Object,Object>)ls1.get(0)).get("totalTrays");
  		  			obj.setTotalNoOfSeedlingss(TotalNoOfSeedlingss);
  		  			obj.setTotalTrays(totalTrays);
  		  			
  		  		}
  				obj.setDescription("FOR SUGAR CANE");
	  			
  				session.save(obj);
		}
		}
  		String sql4="select distinct batchSeries from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'V'  " ;
  		List ls4=hibernatedao.findBySqlCriteria(sql4);
  		if(ls4.size()>0 && ls4 !=null)
  		{
  		for (int i= 0; i <ls4.size(); i++) 
		{
  				obj=new Temp_VegetableFrootsProduction();
  				String batchSeries=(String)((Map<Object,Object>)ls4.get(i)).get("batchSeries");
  				String sql2="select seedsrcode from SeedSource   where season='"+season+"' and  batchno='"+batchSeries+"' " ;
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  		  		if(ls2.size()>0 && ls2 !=null)
  		  		{
  		  			String seedsrcode=(String)((Map<Object,Object>)ls2.get(0)).get("seedsrcode");
  		  			obj.setSourceofseed(seedsrcode);
  		  		}
  				
  				
  				String sql1="select isnull(sum(TotalNoOfSeedlingss),0) TotalNoOfSeedlingss ,isnull(sum(totalTrays),0) totalTrays from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'S' and batchSeries='"+batchSeries+"' " ;
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0 && ls1 !=null)
  		  		{
  		  			Double TotalNoOfSeedlingss=(Double)((Map<Object,Object>)ls1.get(0)).get("TotalNoOfSeedlingss");
  		  			Integer totalTrays=(Integer)((Map<Object,Object>)ls1.get(0)).get("totalTrays");
  		  			obj.setTotalNoOfSeedlingss(TotalNoOfSeedlingss);
  		  			obj.setTotalTrays(totalTrays);
  		  			
  		  		}
  				obj.setDescription("FOR VEGETABLES");
	  			
  				session.save(obj);
		}
		}
  		String sql5="select distinct batchSeries from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'F'  " ;
  		List ls5=hibernatedao.findBySqlCriteria(sql5);
  		if(ls5.size()>0 && ls5 !=null)
  		{
  		for (int i= 0; i <ls5.size(); i++) 
		{
  				obj=new Temp_VegetableFrootsProduction();
  				String batchSeries=(String)((Map<Object,Object>)ls5.get(i)).get("batchSeries");
  				String sql2="select seedsrcode from SeedSource   where season='"+season+"' and  batchno='"+batchSeries+"' " ;
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  		  		if(ls2.size()>0 && ls2 !=null)
  		  		{
  		  			String seedsrcode=(String)((Map<Object,Object>)ls2.get(0)).get("seedsrcode");
  		  			obj.setSourceofseed(seedsrcode);
  		  		}
  				
  				
  				String sql1="select isnull(sum(TotalNoOfSeedlingss),0) TotalNoOfSeedlingss ,isnull(sum(totalTrays),0) totalTrays from TrayFillingDetails where season='"+season+"' and trayFillingDate between '"+fromdate+"' and '"+todate+"' and seedlingType like'S' and batchSeries='"+batchSeries+"' " ;
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0 && ls1 !=null)
  		  		{
  		  			Double TotalNoOfSeedlingss=(Double)((Map<Object,Object>)ls1.get(0)).get("TotalNoOfSeedlingss");
  		  			Integer totalTrays=(Integer)((Map<Object,Object>)ls1.get(0)).get("totalTrays");
  		  			obj.setTotalNoOfSeedlingss(TotalNoOfSeedlingss);
  		  			obj.setTotalTrays(totalTrays);
  		  			
  		  		}
  				obj.setDescription("FOR FROOTS");
	  			
  				session.save(obj);
		}
		}
  		
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="VegetablesFrootsProductionReport";
			String Query1=" * from Temp_VegetableFrootsProduction  ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	@RequestMapping(value = "/generateSeedUtilizationReportZoneWise", method = RequestMethod.POST)
	public void generateSeedUtilizationReportZoneWise(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
	
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String season1=request.getParameter("season");
		String sName[]=season1.split(":");
		String season=sName[1];
		String rptFormat = request.getParameter("mode");
		
		Date fromdate=DateUtils.getSqlDateFromString(fromDate,Constants.GenericDateFormat.DATE_FORMAT);
		Date todate=DateUtils.getSqlDateFromString(toDate,Constants.GenericDateFormat.DATE_FORMAT);
		
		
		
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		
		String sqq="truncate table Temp_SeedUtilizationReportZoneWise";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		
		Temp_SeedUtilizationReportZoneWise obj=new Temp_SeedUtilizationReportZoneWise();
		Session session=hibernatedao.getSessionFactory().openSession();
		String sql="select s.*,r.ryotname,v.village,vi.variety,ry.ryotname as consumer from SeedSource s,ryot r,village v,variety vi,ryot ry  where s.season='"+season+"' and s.sourcesupplieddate between '"+fromdate+"' and '"+todate+"' and s.seedsrcode=r.ryotcode and s.villagecode=v.villagecode and s.varietycode=vi.varietycode and purpose=1  and s.consumercode=ry.ryotcode " ;
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls !=null)
  		{
  		for (int i= 0; i <ls.size(); i++) 
		{
  			
  				obj=new Temp_SeedUtilizationReportZoneWise();
  				
  				Integer circlecode=(Integer)((Map<Object,Object>)ls.get(i)).get("circlecode");
  				String sql3="select c.circle,c.zonecode,z.zone from  circle c,zone z where c.circlecode="+circlecode+" and c.zonecode=z.zonecode " ;
  		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
  		  		if(ls3.size()>0 && ls3 !=null)
  		  		{
  		  			String zone=(String)((Map<Object,Object>)ls3.get(0)).get("zone");
  		  			String circle=(String)((Map<Object,Object>)ls3.get(0)).get("circle");
  		  			Integer zonecode=(Integer)((Map<Object,Object>)ls3.get(0)).get("zonecode");
  		  			
  		  			obj.setZone(zone);
  		  			obj.setZonecode(zonecode);
  		  			obj.setCircle(circle);
  		  			obj.setCirclecode(circlecode);
  		  		}
  				
  				String consumercode=(String)((Map<Object,Object>)ls.get(i)).get("consumercode");
  				String consumername=(String)((Map<Object,Object>)ls.get(i)).get("consumer");
  			 	String supplier=(String)((Map<Object,Object>)ls.get(i)).get("seedsrcode");
  				String 	suppliername=(String)((Map<Object,Object>)ls.get(i)).get("ryotname");
  				String 	village=(String)((Map<Object,Object>)ls.get(i)).get("village");
  				String 	variety=(String)((Map<Object,Object>)ls.get(i)).get("variety");
  				Double 	nooftons=(Double)((Map<Object,Object>)ls.get(i)).get("finalweight");
  				Double 	transportvalue=(Double)((Map<Object,Object>)ls.get(i)).get("transcharges");
  				Double 	value=(Double)((Map<Object,Object>)ls.get(i)).get("totalcost");
  				Date 	dateofApp=(Date)((Map<Object,Object>)ls.get(i)).get("certdate");
  				if(dateofApp !=null)
  				{
  					String dateofapproval=format.format(dateofApp);
  					obj.setDateofapproval(dateofapproval);
  				}
  				
  				String sql2="select v.village consumervillage from  Ryot r ,village v  where r.ryotcode='"+consumercode+"'  and r.villagecode=v.villagecode" ;
  		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
  		  		if(ls2.size()>0 && ls2 !=null)
  		  		{
  		  			String consumervillage=(String)((Map<Object,Object>)ls2.get(0)).get("consumervillage");
  		  			obj.setConsumervillage(consumervillage);
  		  		}
  				Double 	noofacre=(Double)((Map<Object,Object>)ls.get(i)).get("noofacre");
  				String sql1="select isnull(sum(finalweight),0) finalweight ,isnull(sum(noofacre),0) noofacre from SeedSource  where season='"+season+"' and purpose=1 and sourcesupplieddate between '"+fromdate+"' and '"+todate+"' and seedsrcode='"+supplier+"' " ;
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		if(ls1.size()>0 && ls1!=null)
  		  		{
  		  			Double 	tfinalweight=(Double)((Map<Object,Object>)ls1.get(0)).get("finalweight");
  		  			Double 	tnoofacre=(Double)((Map<Object,Object>)ls1.get(0)).get("noofacre");
  		  			
  		  			obj.setTfinalweight(tfinalweight);
  		  			obj.setTnoofacre(tnoofacre);
  		  		
  		  		}
  		  		obj.setValue(value);
  				obj.setNoofacre(noofacre);
  				obj.setConsumer(consumercode);
  				obj.setConsumername(consumername);
  				obj.setSupplier(supplier);
  				obj.setSuppliername(suppliername);
  				obj.setVillage(village);
  				obj.setVariety(variety);
  				obj.setNooftons(nooftons);
  				obj.setTransportvalue(transportvalue);
  				
  				obj.setQty(0.0);
  				session.save(obj);
  			}
	  		
		}
  		session.flush();
  		session.close();
  		try
		{
			String reportFileName ="SeedUtilizationReportZoneWise";
			String Query1=" * from Temp_SeedUtilizationReportZoneWise order by zonecode, supplier ";
			 //Parameters as Map to be passed to Jasper
	        HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        hmParams.put("cdate", fromDate+" To "+toDate);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			System.out.println("SQLExp::CLOSING::" + e.toString());
		}
	}
	
	/*@RequestMapping(value = "/caneSupplyAndPaymentStatement", method = RequestMethod.POST)
	public void caneSupplyAndPaymentStatement(HttpServletRequest request,HttpServletResponse response) throws Exception 
	{		
		
		String season=request.getParameter("season");
		String rptFormat = request.getParameter("mode");  
		String season1[]=season.split(":");
		String sqq="truncate table Temp_CaneSupplyAndPaymentStatement";
		SQLQuery query=hibernatedao.getSessionFactory().openSession().createSQLQuery(sqq);
		query.executeUpdate();
		Temp_CaneSupplyAndPaymentStatement obj=new Temp_CaneSupplyAndPaymentStatement();
		Session session=hibernatedao.getSessionFactory().openSession();
		String ryotcode=null;
		Double balancedue=0.0;
		
		String sql=" select distinct ryotcode,ryotname,sum(canesupplied) canesupplied,sum(totalvalue) totalvalue from CaneValueSplitup where season='"+season1[1]+"' group by ryotcode,ryotname order by ryotcode ";
  		List ls=hibernatedao.findBySqlCriteria(sql);
  		if(ls.size()>0 && ls!=null)
  		{	
  			for (int k = 0; k <ls.size(); k++) 
  			{
  				logger.info("------k---------------"+ k);
  				obj=new Temp_CaneSupplyAndPaymentStatement();
  				double tfr = 0.0;
  				double sbpaid = 0.0;
  				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
 				logger.info("------k---------------ryotcode"+ ryotcode);

  				String ryotname=(String)((Map<Object,Object>)ls.get(k)).get("ryotname");
  				Double canesupplied=(Double)((Map<Object,Object>)ls.get(k)).get("canesupplied");
  				Double totalvalue=(Double)((Map<Object,Object>)ls.get(k)).get("totalvalue");
  				obj.setRyotcode(ryotcode);
  				obj.setRyotname(ryotname);
  				obj.setCanesupply(canesupplied);
  				obj.setCanevalue(totalvalue);
  				String	sql1=" select sum(cdcamt) cdc,sum(ulamt) ul,sum(transport) transport,sum(harvesting) harvesting,sum(otherded) otherded,sum(sbac) sbac,sum(paidamt) paidamt from CanAccSBDetails where ryotcode='"+ryotcode+"' and season='"+season1[1]+"'";
  		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
  		  		Double sbpaidamt=0.0;
  		  		Double transport=0.0;
  		  		if(ls1.size()>0 && ls1!=null)
  		  		{
  		  			Double cdc=(Double)((Map<Object,Object>)ls1.get(0)).get("cdc");
  		  			Double ul=(Double)((Map<Object,Object>)ls1.get(0)).get("ul");
  		  			transport=(Double)((Map<Object,Object>)ls1.get(0)).get("transport");
		  			Double harvesting=(Double)((Map<Object,Object>)ls1.get(0)).get("harvesting");
		  			Double otherded=(Double)((Map<Object,Object>)ls1.get(0)).get("otherded");
  		  			Double sbacTransfer=(Double)((Map<Object,Object>)ls1.get(0)).get("sbac");
  		  			tfr += sbacTransfer;
  		  			sbpaidamt=(Double)((Map<Object,Object>)ls1.get(0)).get("paidamt");
  		  			sbpaid +=sbpaidamt;
  		  			obj.setCdc(cdc);
  		  			obj.setUl(ul);
  		  			obj.setTransport(transport);
  		  			obj.setHarvesting(harvesting);
  		  			obj.setOdd(otherded);
  		  			obj.setSbpaid(sbpaidamt);
  		  			obj.setSbtransfer(sbacTransfer);
  		  		}
  		  		String	sql2=" SELECT sum(disbursedamount) disbursedamount  FROM LoanDetails where ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and disburseddate  IS NOT NULL  ";
		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
		  		if(ls2.size()>0 && ls2!=null)
		  		{
		  			Double disbursedamount=(Double)((Map<Object,Object>)ls2.get(0)).get("disbursedamount");
		  			obj.setLprinciple(disbursedamount);
		  		}
		  		String	sql3="  select sum(finalinterest) finalinterest, sum(paidamt) paidamt, sum(advisedamt) advisedamt from CanAccLoansDetails where advloan=1 and ryotcode='"+ryotcode+"' and season='"+season1[1]+"' ";
		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
		  		Double advisedamt=0.0;
		  		Double paidamt=0.0;
		  		if(ls3.size()>0 && ls3!=null)
		  		{
		  			Double finalinterest=(Double)((Map<Object,Object>)ls3.get(0)).get("finalinterest");
		  			paidamt=(Double)((Map<Object,Object>)ls3.get(0)).get("paidamt");
		  			if(paidamt!=null)
		  			{
		  				tfr += paidamt;
		  			}
		  			advisedamt=(Double)((Map<Object,Object>)ls3.get(0)).get("advisedamt");
		  			if(advisedamt!=null)
		  			{
		  				sbpaid +=advisedamt;
		  			}
		  			obj.setLinterest(finalinterest);
		  			obj.setLtransfer(paidamt);
		  			obj.setLpaid(advisedamt);
		  		}
		  		String	sql4="select  sum(paidamt) advance from CanAccLoansDetails where advloan=0 and ryotcode='"+ryotcode+"' and season='"+season1[1]+"'";
		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
		  		if(ls4.size()>0 && ls4!=null)
		  		{
		  			Double advance=(Double)((Map<Object,Object>)ls4.get(0)).get("advance");
		  			obj.setAdvance(advance);
		  		}
		  		String	sql5=" SELECT distinct c.circle,z.zone,v.village FROM WeighmentDetails w ,circle c,zone z,village v where  w.ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and w.circlecode=c.circlecode and w.zonecode=z.zonecode and w.villagecode=v.villagecode ";
		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
		  		if(ls5.size()>0 && ls5!=null)
		  		{
		  			String circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
		  			String zone=(String)((Map<Object,Object>)ls5.get(0)).get("zone");
		  			String village=(String)((Map<Object,Object>)ls5.get(0)).get("village");
		  			obj.setCircle(circle);
		  			obj.setVillage(village);
		  			obj.setZone(zone);
		  		}
		  		
		  		List sourceList=new ArrayList();
		  		String	sql6=" select distinct  paymentsource from CanAccLoansDetails where advloan=1 and ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and paidamt>0 ";
		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
		  		if(ls6!=null && ls6.size()>0 )
		  		{
		  			for(int j=0;j<ls6.size();j++)
		  			{
		  				String paymentsource=(String)((Map<Object,Object>)ls6.get(j)).get("paymentsource");
		  				Map tempMap=new HashMap();
		  				if(paymentsource!=null)
		  				{
		  					tempMap.put("Source", paymentsource);
		  					sourceList.add(tempMap);
		  				}
		  			}
		  		}
		  		String	sql7=" select distinct paymentsource from CanAccSBDetails where  ryotcode='"+ryotcode+"' and season='"+season1[1]+"' and paidamt>0 ";
		  		List ls7=hibernatedao.findBySqlCriteria(sql7);
		  		if( ls7!=null && ls7.size()>0 )
		  		{
		  			for(int j=0;j<ls7.size();j++)
		  			{
		  				String paymentsource=(String)((Map<Object,Object>)ls7.get(j)).get("paymentsource");
		  				Map tempMap=new HashMap();
		  				if(paymentsource!=null)
		  				{
		  					tempMap.put("Source", paymentsource);
		  					boolean isAcctrue = sourceList.contains(tempMap);
							if(isAcctrue == false)
							{
								sourceList.add(tempMap);
							}
		  				}					
		  			}
		  		}
		  		String paySource = "";
		  		if(sourceList != null && sourceList.size()>0)
		  		{
		  			for(int j=0;j<sourceList.size();j++)
		  			{
		  				String paymentsource=(String)((Map<Object,Object>)sourceList.get(j)).get("Source");
		  				paySource += paymentsource+",";
		  			}
		  			paySource = paySource.substring(0, paySource.length()-1);
		  		}
		  		obj.setPayamantresourcebank(paySource);
		  		
		  		
		  		balancedue=(tfr)-(sbpaid);
		  		if(balancedue>0)
		  		{
		  			obj.setBalancedue(balancedue);
		  		}
		  		else
		  			obj.setBalancedue(0.0);
  				
  				session.save(obj);
  				logger.info("------k--------------- inserted Successfully"+k);
  			}
  		}
  		session.flush();
  		session.close();
  		try
  		{
  			String reportFileName =null;
  			if("Excel".equalsIgnoreCase(rptFormat))
  			{
  				reportFileName ="CaneSupplyAndPaymentExcelFormate";
  			}
  			else
  				reportFileName ="CaneSupplyAndPaymentPdfFormate";
  			String Query1="distinct * from Temp_CaneSupplyAndPaymentStatement ";
  			//Parameters as Map to be passed to Jasper
  			HashMap<String,Object> hmParams=new HashMap<String,Object>();
	        hmParams.put("Title", "SRI SARVARAYA SUGARS LTD CHELLURU");
	        hmParams.put("qrs", Query1);
	        generateReport(request,response,hmParams,reportFileName,rptFormat);
		}
		catch(Exception e)
		{
			 logger.info("------SQLExp::CLOSING::---------------"+ e.toString());
		}
	}*/
	
	
	

	
	//@RequestMapping(value = "/generateReport", method = RequestMethod.POST)
	public String generateReport(HttpServletRequest request,HttpServletResponse response,HashMap hmParams,String reportFileName,String rptFormat) throws ParseException 
	{
		SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
		 logger.info("------generateReport---------------"+ctime.format(new Date()));
	    Connection conn = null;
	    Session session=null;
	    try 
	    {	    	
	        try
	        {	 
	             //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	        	session=hibernatedao.getSessionFactory().openSession();
	            SessionImpl sessionImpl = (SessionImpl) session;
	            conn = sessionImpl.connection();
	        } 
	        catch (Exception e) 
	        {
	                System.out.println("Please include Classpath Where your MySQL Driver is located");
	                e.printStackTrace();
	        }  
	 
	         //conn = DriverManager.getConnection("jdbc:sqlserver://10.1.0.26:1433;databaseName=sugarerpdb","admin","finsol@1");
	 
	         logger.info("------Connection---------------"+conn);
	         
	     if (conn != null)
	     {
	         System.out.println("Database Connected");
	         logger.info("---Database Connected--------");
	     }
	     else
	     {
	         System.out.println(" connection Failed ");
	         logger.info("--Connection Failed--");
	     }
	     

	       JasperReport jasperReport = getCompiledFile(reportFileName, request);
	 
	        if (rptFormat.equalsIgnoreCase("html") ) {
	 
	            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hmParams, conn);
	            generateReportHtml(jasperPrint, request, response); // For HTML report
	 
	        }
	        else  if  (rptFormat.equalsIgnoreCase("pdf") )  {
	        	
	            generateReportPDF(response, hmParams, jasperReport, conn); // For PDF report
	            ctime = new SimpleDateFormat("hh:mm:ss");
	            logger.info("---AfterPdfFormate--------"+ctime.format(new Date()));
	            }
	        
	        else if  (rptFormat.equalsIgnoreCase("LPT") )  {
	        	generateReportLPT(response, hmParams, jasperReport, conn);
	        	
	        }
	        else  if (rptFormat.equalsIgnoreCase("Excel") )
	        	{
	            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hmParams, conn);
	            generateReportExcel(jasperPrint, request, response,reportFileName); // For HTML report
	 
	        } 
	       /* else if  (rptFormat.equalsIgnoreCase("LPT") )  {
	        	generateReportLPT(response, hmParams, jasperReport, conn);
	        	
	        }
	 
	        else if  (rptFormat.equalsIgnoreCase("Dynamic") )  {
	        	
	        	//getDynamicJasper(response,conn);
	        	
	        }*/
	       } catch (Exception sqlExp) {
	 
	           System.out.println( "Exception::" + sqlExp.toString());
	 
	       } finally {
	 
	            try {
	 
	            if (conn != null) {
	                conn.close();
	                session.close();
	                conn = null;
	            }
	 
	            } catch (SQLException expSQL) {
	 
	                System.out.println("SQLExp::CLOSING::" + expSQL.toString());
	 
	            }
	 
	           }
	 
	return null;
	 
	}
	
    private void generateReportPDF (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException {
    	SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
    	logger.info("------------generateReportPDF"+ctime.format(new Date()));
    	logger.info("------------generateReportPDF");
        byte[] bytes = null;
        logger.info("--parameters--"+parameters);
        logger.info("--conn--"+conn);
        logger.info("-----------------bytes---"+bytes);
        bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
        logger.info("--parameters--"+parameters);
        logger.info("--conn--"+conn);
        logger.info("-----------------bytes---"+bytes);
        resp.reset();
        resp.resetBuffer();
        resp.setContentType("application/pdf");
        resp.setContentLength(bytes.length);
       // resp.setHeader("Content-Disposition: inline ;filename=rpt.pdf");
        //resp.addHeader("Content-Disposition", "attachment; filename=rpt.pdf");
        //resp.addHeader("content-disposition", "inline; filename=link_to_pdf.pdf");
        ServletOutputStream ouputStream = resp.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        logger.info("------------bytes-------"+bytes.length);
        ouputStream.flush();
        ouputStream.close();
        ctime = new SimpleDateFormat("hh:mm:ss");
        logger.info("------------AfterGenerateReportPDF"+ctime.format(new Date()));
    } 
    
    private JasperReport getCompiledFile(String fileName, HttpServletRequest request) throws JRException {
        System.out.println("path " + request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        logger.info("path " + request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        reportFile = new File( request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        // If compiled file is not found, then compile XML template
        if (!reportFile.exists()) {
                   JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jrxml"),request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
                   
            }
            //JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromLocation(reportFile.getPath());
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
           return jasperReport;
        } 
    
    private void generateReportHtml( JasperPrint jasperPrint, HttpServletRequest req, HttpServletResponse resp) throws IOException, JRException {
    	try
    	{    	
    	HtmlExporter exporter=new HtmlExporter();
        List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
        jasperPrintList.add(jasperPrint);
        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
        exporter.setExporterOutput( new SimpleHtmlExporterOutput(resp.getWriter()));
        SimpleHtmlReportConfiguration configuration =new SimpleHtmlReportConfiguration();
        exporter.setConfiguration(configuration);
         exporter.exportReport();
    	}
    	catch(Exception e)
    	{
    		System.out.println("Exception is ::" + e.toString());
    	}
   }
    

    private void generateReportExcel(JasperPrint jasperPrint, HttpServletRequest request, HttpServletResponse response,String reportFileName) throws IOException, JRException 
    {
    	//String title="one";
    	//String fileName=request.getRealPath("/reports") + "/" +reportFileName+".xls";
    	
    	File f=new File(request.getRealPath("/reports") + "/" +reportFileName+".xls");
    	if(f.exists())
    	{
    		f.delete();
    		f=new File(request.getRealPath("/reports") + "/" +reportFileName+".xls");
    	}
       	JRXlsExporter exporterXLS = new JRXlsExporter();
       	exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
       	exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
       	exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
       	exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
       	exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
       	exporterXLS.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, request.getRealPath("/reports") + "/" +reportFileName+".xls");
       	exporterXLS.exportReport();
       	
       
 /*      	FileInputStream fin = new FileInputStream(f);
       	ServletOutputStream outStream = response.getOutputStream();
       	// SET THE MIME TYPE.
       	//response.setContentType("application/vnd.ms-excel");
       	// set content dispostion to attachment in with file name.
       	// case the open/save dialog needs to appear.
       	//response.setHeader("Content-Disposition", "attachment;filename="+reportFileName+".xls");

       	byte[] buffer = new byte[1024];
       	int n = 0;
       	while ((n = fin.read(buffer)) != -1) {
       	outStream.write(buffer, 0, n);
       	}*/
       }
    
    
    private void generateReportLPT (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException
    {
    	 System.out.println("Port : " + PARALLEL_PORT + " is detected");
    	 
    	 try {
    	 
   /* 		 Enumeration port_list = CommPortIdentifier.getPortIdentifiers();

    		 System.out.println ("port_list port:" + port_list.nextElement());
    		 
    		 while (port_list.hasMoreElements())
    		 {
    		 CommPortIdentifier port_id = (CommPortIdentifier)port_list.nextElement();

    		 if (port_id.getPortType() == CommPortIdentifier.PORT_SERIAL)
    		 {
    		 System.out.println ("Serial port:" + port_id.getName());
    		 }
    		 else if (port_id.getPortType() == CommPortIdentifier.PORT_PARALLEL)
    		 {
    		 System.out.println ("Parallel port: " + port_id.getName());
    		 }
    		 else
    		 System.out.println ("Other port:" + port_id.getName());
    		 }
*/ 	 
    			//byte[] bytes = null;
  		 
    		 port = CommPortIdentifier.getPortIdentifier(PARALLEL_PORT);
        	 //port identified
    		 logger.info("OWNER-----------------"+port.getCurrentOwner());
             System.out.println("Port identified : " + port);
             // open the parallel port --
             //port(App name, timeout);
             parallelPort = (ParallelPort) port.open("LX300", 90);
             //parallelPort = (ParallelPort) port.open("LX300", 90);
             //port opened
             System.out.println("Port opened : " + parallelPort);
             outputStream = parallelPort.getOutputStream();
             
    	 Map parameters1 = new HashMap();
		JRTextExporter exporter = new JRTextExporter();
		File sourceFile = new File(reportFile.getAbsolutePath());
		//File sourceFile = new File("C:\\JREmp1.jasper");
		System.out.println("get path------------------"+sourceFile.getAbsolutePath());
		JasperReport report = (JasperReport) JRLoader.loadObject(sourceFile);
		JasperPrint jasperPrint = JasperFillManager.fillReport(report,parameters, conn);

		File destFile = new File(sourceFile.getParent(), jasperPrint.getName() + ".txt");
		FileWriter fr=new FileWriter(destFile);
		System.out.println("get path------------------"+destFile.getAbsolutePath());
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRTextExporterParameter.BETWEEN_PAGES_TEXT ,"\f");       
		exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT        , new Float(798));
		exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH         , new Float(581));
		exporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH    , new Float(  7));
		exporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT   , new Float( 14));
		//exporter.setParameter(JRExporterParameter.OUTPUT_WRITER,resp.getWriter());
		//exporter.setParameter(JRExporterParameter.OUTPUT_WRITER,fr);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);

		exporter.exportReport();

		outputStream.flush();
		outputStream.close(); 
    	 
 
    	
    	//byte[] bytes = null;
    	
        //bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
       // System.out.println("Bytes Length--------- " + bytes.length);
        
       // System.out.println("Bytes Length--------- " + bytes.length);
        
       /* try {
        	
            FileOutputStream os = new FileOutputStream("LPT1");
            //wrap stream in "friendly" PrintStream
            PrintStream ps = new PrintStream(os);
 
            //print text here
            ps.println("Hello world!");
 
            //form feed -- this is important
            //Without the form feed, the text will simply sit
            // in print buffer until something else gets printed.
            ps.print("\f");
            //flush buffer and close
            ps.close();
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e);
        }*/
        
/*    	port = CommPortIdentifier.getPortIdentifier(PARALLEL_PORT);
    	 //port identified
         System.out.println("Port identified : " + port);
         // open the parallel port --
         //port(App name, timeout);
         parallelPort = (ParallelPort) port.open("LX300", 1000);
         //parallelPort = (ParallelPort) port.open("LX300", 90);
         //port opened
         System.out.println("Port opened : " + parallelPort);
         outputStream = parallelPort.getOutputStream();
         //get output
         System.out.println("Out put taken : " + outputStream);
         //outputStream.write(bytes);'
         outputStream.write(bytes, 0, bytes.length);
        // ouputStream.write(bytes, 0, bytes.length);*/

/*         File file = new File("E:/myfile.txt");
         FileOutputStream fos = new FileOutputStream(file);

         if (!file.exists()) {
    	     file.createNewFile();
    	  }         
        // byte[] bytesArray = mycontent.getBytes();

   	  fos.write(bytes);
   	  fos.flush();*/
            	  
         //data written
 /*        System.out.println("Data Written : " + bytes);
         System.out.println("Bytes Length--------- " + bytes.length);
         outputStream.flush();
         outputStream.close();
*/         

         
         
         
         
         
         
        
    	 } catch (NoSuchPortException nspe) {
    	        System.out.println("\nPrinter Port LPT1 not found :     NoSuchPortException.\nException:\n" + nspe + "\n");
    	        logger.info("\nPrinter Port LPT1 not found :     NoSuchPortException.\nException:\n" + nspe + "\n");
    	    } catch (PortInUseException piue) {
    	        System.out.println("\nPrinter Port LPT1 is in use : " +     "PortInUseException.\nException:\n" + piue + "\n");
    	        logger.info("\nPrinter Port LPT1 is in use : " +     "PortInUseException.\nException:\n" + piue + "\n");
    	    } catch (IOException ioe) {
    	        System.out.println("\nPrinter Port LPT1 failed to write : " + "IOException.\nException:\n" + ioe + "\n");
    	        logger.info("\nPrinter Port LPT1 failed to write : " + "IOException.\nException:\n" + ioe + "\n");
    	    }
    	    
    	  catch (Exception e) {
    	        System.out.println("\nFailed to open Printer Port LPT1 with exception : " + e +   "\n");
    	        logger.info("\nFailed to open Printer Port LPT1 with exception : " + e +   "\n");
    	    } 
    	 finally {
    	        if (port != null && port.isCurrentlyOwned()) {
    	            parallelPort.close();
    	        }
    	        System.out.println("Closed all resources.\n");
    	    }
    	

    } 
    
    
    private void generateReportLPT1 (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException
    {
    
    try {


       // String report = JasperCompileManager.compileReportToFile("sourceFileName");
    	// File reportFile = new File( request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
//
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, conn);


        PrinterJob printerJob = PrinterJob.getPrinterJob();


        PageFormat pageFormat = PrinterJob.getPrinterJob().defaultPage();
        printerJob.defaultPage(pageFormat);

        int selectedService = 0;


        AttributeSet attributeSet = new HashPrintServiceAttributeSet(new PrinterName("DotMatrix", null));


        PrintService[] printService = PrintServiceLookup.lookupPrintServices(null, attributeSet);

        try {
            printerJob.setPrintService(printService[selectedService]);

        } catch (Exception e) {

            System.out.println(e);
        }
        JRPrintServiceExporter exporter;
        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
        printRequestAttributeSet.add(MediaSizeName.NA_LETTER);
        printRequestAttributeSet.add(new Copies(1));

        // these are deprecated
        exporter = new JRPrintServiceExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, printService[selectedService]);
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, printService[selectedService].getAttributes());
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, printRequestAttributeSet);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
        exporter.exportReport();

    } catch (JRException e) {
        e.printStackTrace();
    }
}
	
}
