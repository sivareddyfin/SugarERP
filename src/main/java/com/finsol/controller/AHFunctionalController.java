package com.finsol.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.glxn.qrgen.QRCode;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.finsol.bean.AgreementDetailsBean;
import com.finsol.bean.AgreementSummaryBean;
import com.finsol.bean.ExtentDetailsBean;
import com.finsol.bean.GenerateRankingBean;
import com.finsol.bean.LabRecommendationsBean;
import com.finsol.bean.LabRecommendationsTestBean;
import com.finsol.bean.LoanPrincipleAmountsBean;
import com.finsol.bean.PrepareTieupLoanBean;
import com.finsol.bean.ProgramDetailsBean;
import com.finsol.bean.ProgramSummaryBean;
import com.finsol.bean.RyotAdvanceBean;
import com.finsol.bean.SampleCardsBean;
import com.finsol.bean.SearchRyotLoanBean;
import com.finsol.bean.SeedlingTrayChargesDetailsBean;
import com.finsol.bean.SeedlingTrayChargesSummaryBean;
import com.finsol.bean.SeedlingTrayDetailsBean;
import com.finsol.bean.SpecialPermitBean;
import com.finsol.bean.UpdateCcsBean;
import com.finsol.bean.UpdateEstimateQtyBean;
import com.finsol.bean.UpdateLoanDetailsBean;
import com.finsol.bean.WeighBridgeTestDataBean;
import com.finsol.dao.HibernateDao;
import com.finsol.model.AccountDetails;
import com.finsol.model.AccountGroupDetails;
import com.finsol.model.AccountGroupSummary;
import com.finsol.model.AccountMaster;
import com.finsol.model.AccountSubGroupDetails;
import com.finsol.model.AccountSubGroupSummary;
import com.finsol.model.AccountSummary;
import com.finsol.model.AdvanceDetails;
import com.finsol.model.AdvancePrincipleAmounts;
import com.finsol.model.AdvanceSummary;
import com.finsol.model.AgreementDetails;
import com.finsol.model.AgreementSummary;
import com.finsol.model.Bank;
import com.finsol.model.BatchServicesMaster;
import com.finsol.model.Branch;
import com.finsol.model.CaneAcctAmounts;
import com.finsol.model.CaneAcctRules;
import com.finsol.model.Circle;
import com.finsol.model.CompanyAdvance;
import com.finsol.model.CompositePrKeyForLoans;
import com.finsol.model.CompositePrKeyForPermitDetails;
import com.finsol.model.CropTypes;
import com.finsol.model.Employees;
import com.finsol.model.ExtentDetails;
import com.finsol.model.FieldAssistant;
import com.finsol.model.FieldOfficer;
import com.finsol.model.GenerateRankingTemp;
import com.finsol.model.LabRecommendations;
import com.finsol.model.LabRecommendations_temp;
import com.finsol.model.LoanDetails;
import com.finsol.model.LoanPrincipleAmounts;
import com.finsol.model.LoanReconsilation;
import com.finsol.model.LoanSummary;
import com.finsol.model.Mandal;
import com.finsol.model.NewRyots;
import com.finsol.model.PermitDetails;
import com.finsol.model.PermitRules;
import com.finsol.model.ProgramSummary;
import com.finsol.model.ProgramVariety;
import com.finsol.model.RandDAgreementDetails;
import com.finsol.model.RandDAgreementSummary;
import com.finsol.model.Ryot;
import com.finsol.model.RyotBankDetails;
import com.finsol.model.SampleCards;
import com.finsol.model.SampleCardsTemp;
import com.finsol.model.SeedSource;
import com.finsol.model.SeedSuppliersSummary;
import com.finsol.model.SeedlingTrayChargeSummary;
import com.finsol.model.SeedlingTrayChargesDetails;
import com.finsol.model.SeedlingTrayDetails;
import com.finsol.model.Temp_BatchServiceMaster;
import com.finsol.model.Temp_SeedUtilizationReport;
import com.finsol.model.Variety;
import com.finsol.model.Village;
import com.finsol.model.WeighBridgeTestData;
import com.finsol.model.WeighmentDetails;
import com.finsol.model.Zone;
import com.finsol.service.AHFuctionalService;
import com.finsol.service.AuditTrailService;
import com.finsol.service.BankService;
import com.finsol.service.CaneAccountingFunctionalService;
import com.finsol.service.CaneReceiptFunctionalService;
import com.finsol.service.CircleService;
import com.finsol.service.CommonService;
import com.finsol.service.CompanyAdvanceService;
import com.finsol.service.RyotService;
import com.finsol.service.VillageService;
import com.finsol.utils.Constants;
import com.finsol.utils.DateUtils;
/**
 * @author Rama Krishna
 * 
 */
@Controller
public class AHFunctionalController {

	private static final Logger logger = Logger.getLogger(AHFunctionalController.class);
	
	DecimalFormat HMSDecFormatter = new DecimalFormat("0.000");

	@Autowired
	private AHFuctionalService ahFuctionalService;
	
	@Autowired	
	private CaneAccountingFunctionalService caneAccountingFunctionalService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private RyotService ryotService;

	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private CompanyAdvanceService companyAdvanceService;
	
	@Autowired
	private CircleService circleService;
	
	@Autowired
	private VillageService villageService;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private AuditTrailService auditTrailService;
	
	@Autowired	
	private CaneReceiptFunctionalService caneReceiptFunctionalService;
	
	@Autowired	
	private HibernateDao hibernatedao;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired	
	private LoginController loginController;
	
	@Autowired
	private CaneAccountingFunctionalController caneAccountingFunctionalController;
	
	@Autowired	
	private AgricultureHarvestingController agricultureHarvestingController;
	

	@RequestMapping(value = "/getServerDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	String getServerDate() throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		Date sysdate = new Date();
		response.put("serverdate", DateUtils.formatDate(sysdate,Constants.GenericDateFormat.DATE_FORMAT));
		return response.toString();
	}

	@RequestMapping(value = "/getServerTime", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody String getServerTime() throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		response.put("servertime", DateUtils.getCurrentSystemTime());
		return response.toString();
	}

	// -------------Extent Details for Soil Test---//

	@RequestMapping(value = "/getExtentDetailsForSoilTest", method = RequestMethod.POST)
	public @ResponseBody List<ExtentDetailsBean> getExtentDetailsForSoilTest(@RequestBody String jsonData) throws JsonParseException,JsonMappingException, IOException 
	{
		List<ExtentDetailsBean> extentDetailsBeans = null;
		try
		{
			List<ExtentDetails> extentDetails = ahFuctionalService.getExtentDetailsByRyotCode(jsonData);
			if (!(extentDetails.isEmpty()) && !(extentDetails == null))
			{
				extentDetailsBeans = prepareListofExtentDetailsBeans(extentDetails);
				return extentDetailsBeans;
			} 
			else
			{
				JSONArray jArray = ahFuctionalService.getDetails(jsonData);
				return jArray;
			}
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}

	private List<ExtentDetailsBean> prepareListofExtentDetailsBeans(List<ExtentDetails> extentDetails) 
	{
		List<ExtentDetailsBean> beans = null;
		if (extentDetails != null && !extentDetails.isEmpty())
		{
			beans = new ArrayList<ExtentDetailsBean>();
			ExtentDetailsBean bean = null;
			for (ExtentDetails extentDetail : extentDetails) 
			{
				bean = new ExtentDetailsBean();

				String villageCode = extentDetail.getVillagecode();
				String villageName = "NA";
				List<String> VillageList = new ArrayList<String>();
				
				VillageList = ahFuctionalService.getVillageDetailsByCode(villageCode);
				if (VillageList != null && VillageList.size() > 0) 
				{
					String myResult1 = VillageList.get(0);
					villageName = myResult1;
				}
				List<Ryot> ryot = ryotService.getRyotByRyotCode(extentDetail.getRyotcode());
				bean.setId(extentDetail.getId());
				bean.setExtentSize(extentDetail.getExtentsize());
				bean.setIsSuitableForCultivation(extentDetail.getIsSuitableForCultivation());
				bean.setSurveyNo(extentDetail.getSurveyno());
				bean.setPlotSlNumber(extentDetail.getPlotslnumber());
				bean.setTestDate(extentDetail.getTestDate().toString());
				bean.setVarietyCode(extentDetail.getVarietycode());
				bean.setRyotCode(extentDetail.getRyotcode());
				bean.setVillageCode(extentDetail.getVillagecode());
				bean.setVillageName(villageName);
				bean.setSoilTestStatus(extentDetail.getSoilteststatus());
				bean.setSlno(extentDetail.getSlno());
				
				if (ryot.size() > 0 && !(ryot == null))
				{
					bean.setRyotName(ryot.get(0).getRyotname());
				}
				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/SaveExtentDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean SaveExtentDetails(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject totalData = (JSONObject) jsonData.get("formData");
			String ryotCode = totalData.getString("ryotCode");
			String villageCode = totalData.getString("villageCode");
			
			ExtentDetailsBean extentDetailsBean=null;
			ExtentDetails extentDetails=null;
			for (int i = 0; i < gridData.size(); i++)
			{
				 extentDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(),ExtentDetailsBean.class);
				 extentDetails = prepareModelForExtentDetails(extentDetailsBean, ryotCode, villageCode);
				entityList.add(extentDetails);
				
				if(extentDetailsBean.getSlno()!=null)
				{
					boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(extentDetailsBean.getScreenName(),"ok");
					if(isAuditTrailRequired)
						auditTrailService.updateAuditTrail(extentDetailsBean.getSlno(),extentDetails,extentDetailsBean.getScreenName());		
				}
			}
			
			ahFuctionalService.deleteExtentDetails(ryotCode);

			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private ExtentDetails prepareModelForExtentDetails(	ExtentDetailsBean extentDetailsBean, String ryotCode,String villageCode)
	{

		ExtentDetails extentDetails = new ExtentDetails();
		extentDetails.setId(extentDetailsBean.getId());
		extentDetails.setExtentsize(extentDetailsBean.getExtentSize());
		extentDetails.setIsSuitableForCultivation(extentDetailsBean	.getIsSuitableForCultivation());
		extentDetails.setSurveyno(extentDetailsBean.getSurveyNo());
		extentDetails.setVarietycode(extentDetailsBean.getVarietyCode());
		extentDetails.setRyotcode(ryotCode);
		//Modified by DMurty on 16-06-2016
		extentDetails.setVillagecode(villageCode);
		extentDetails.setTestDate(new Date());
		extentDetails.setPlotslnumber(extentDetailsBean.getPlotSlNumber());
		extentDetails.setSoilteststatus((byte) 0);

		return extentDetails;
	}

	@RequestMapping(value = "/saveUpdateSoilTestPopUpDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveUpdateSoilTestPopUpDetails(@RequestBody String jsonData)	throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			LabRecommendationsTestBean labRecommendationsTestBean = new ObjectMapper().readValue(jsonData, LabRecommendationsTestBean.class);
			LabRecommendations_temp labRecommendations_temp = prepareModelForLabRecommendationsPopUpBean(labRecommendationsTestBean);
			
			ahFuctionalService.deleteLabRecomendations_temp(labRecommendationsTestBean.getrCode(),labRecommendationsTestBean.getPlotNo(),labRecommendationsTestBean.getSurveyNo());
			
			entityList.add(labRecommendations_temp);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			return isInsertSuccess;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private LabRecommendations_temp prepareModelForLabRecommendationsPopUpBean(	LabRecommendationsTestBean labRecommendationsTestBean)
	{
		LabRecommendations_temp labRecommendations_temp = new LabRecommendations_temp();
		labRecommendations_temp.setId(labRecommendationsTestBean.getId());
		labRecommendations_temp.setPlotslno(labRecommendationsTestBean.getPlotNo());
		labRecommendations_temp.setRyotcode(labRecommendationsTestBean.getrCode());
		labRecommendations_temp.setSurveynumber(labRecommendationsTestBean.getSurveyNo());
		labRecommendations_temp.setRecommendation(labRecommendationsTestBean.getRecommendation());

		return labRecommendations_temp;
	}

	@RequestMapping(value = "/saveUpdateSoilTestResults", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveUpdateSoilTestResults(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			String enteredBy = formData.getString("enteredBy");
			JSONArray gridData = (JSONArray) jsonData.get("gridData");

			for (int i = 0; i < gridData.size(); i++)
			{
				LabRecommendationsBean labRecommendationsBean = new ObjectMapper().readValue(gridData.get(i).toString(),LabRecommendationsBean.class);
				LabRecommendations labRecommendations = prepareModelForLabRecommendationsBean(labRecommendationsBean,enteredBy);
				if(labRecommendationsBean.getId()!=null)
				{
					boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("Update Soil Test Results","ok");
					if(isAuditTrailRequired)
						auditTrailService.updateAuditTrail(labRecommendationsBean.getId(),labRecommendations,"Update Soil Test Results");		
				}
			}
			
			ahFuctionalService.deleteLabRecomendations(formData.get("ryotCode").toString());
		
			for (int i = 0; i < gridData.size(); i++)
			{
				LabRecommendationsBean labRecommendationsBean = new ObjectMapper().readValue(gridData.get(i).toString(),LabRecommendationsBean.class);
				LabRecommendations labRecommendations = prepareModelForLabRecommendationsBean(labRecommendationsBean,enteredBy);
				entityList.add(labRecommendations);
				
				//ahFuctionalService.addLabTestResults(labRecommendations);
				ahFuctionalService.updateExtedDetails(labRecommendationsBean.getSuitableForCultivation(), labRecommendationsBean.getRyotCode(), Integer.parseInt(labRecommendationsBean.getPlotSlNumber()));
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			return isInsertSuccess;
		}
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private LabRecommendations prepareModelForLabRecommendationsBean(LabRecommendationsBean labRecommendationsBean,String enteredBy) 
	{
		LabRecommendations labRecommendations = new LabRecommendations();

		List<LabRecommendations_temp> LabRecommendations_temp = ahFuctionalService.getLabRecomendationsTempByRyotCode(labRecommendationsBean.getRyotCode(), labRecommendationsBean.getPlotSlNumber(),labRecommendationsBean.getSurveyNo());

		labRecommendations.setPlotslno(labRecommendationsBean.getPlotSlNumber());
		labRecommendations.setRyotcode(labRecommendationsBean.getRyotCode());
		labRecommendations.setSurveynumber(labRecommendationsBean.getSurveyNo());
		
		if (!(LabRecommendations_temp == null) && LabRecommendations_temp.size() > 0)
			labRecommendations.setRecommendation(LabRecommendations_temp.get(0).getRecommendation());
		
		labRecommendations.setEnteredBy(enteredBy);
		
		return labRecommendations;
	}

	@RequestMapping(value = "/getUpdateSoilTestResults", method = RequestMethod.POST)
	public @ResponseBody	List getUpdateSoilTestResults(@RequestBody String jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		List<ExtentDetailsBean> extentDetailsBeans = null;
		List<LabRecommendationsBean> labRecommendationsBeans = null;
		try 
		{
			String Screen = "UpdateSoilTestResults";

			List<LabRecommendations> labRecommendations = ahFuctionalService.getLabRecommendationsByRyotCode(jsonData);
			if (labRecommendations.size() > 0 && !(labRecommendations == null))
			{
				labRecommendationsBeans = prepareListofLabRecommendationsBeans(labRecommendations,Screen);
				return labRecommendationsBeans;
			} 
			else 
			{
				extentDetailsBeans = prepareListofExtentDetailsBeans(ahFuctionalService.getExtentDetailsByRyotCode(jsonData));
				return extentDetailsBeans;
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}
	//
	@RequestMapping(value = "/getUpdateSoilTestResultsForPopup", method = RequestMethod.POST)
	public @ResponseBody	List getUpdateSoilTestResultsForPopup(@RequestBody String jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		List<ExtentDetailsBean> extentDetailsBeans = null;
		List<LabRecommendationsBean> labRecommendationsBeans = null;
		try 
		{
			String Screen = "Generate Agreement";
			List<LabRecommendations> labRecommendations = ahFuctionalService.getLabRecommendationsByRyotCode(jsonData);
			if (labRecommendations.size() > 0 && !(labRecommendations == null))
			{
				labRecommendationsBeans = prepareListofLabRecommendationsBeans(labRecommendations,Screen);
				return labRecommendationsBeans;
			} 
			else 
			{
				extentDetailsBeans = prepareListofExtentDetailsBeans(ahFuctionalService.getExtentDetailsByRyotCode(jsonData));
				return extentDetailsBeans;
			}
			// List<ExtentDetails> extentDetails =
			// ahFuctionalService.getExtentDetailsByRyotCode(jsonData);

		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}
	private List<LabRecommendationsBean> prepareListofLabRecommendationsBeans(List<LabRecommendations> labRecommendations,String Screen)
	{
		List<LabRecommendationsBean> beans = null;
		if (labRecommendations != null && !labRecommendations.isEmpty())
		{
			beans = new ArrayList<LabRecommendationsBean>();
			LabRecommendationsBean bean = null;
			for (LabRecommendations labRecommendation : labRecommendations)
			{
				bean = new LabRecommendationsBean();

				List<ExtentDetails> extentDetails = ahFuctionalService.getExtentDetailsByRyotCode(labRecommendation.getRyotcode(), Integer.parseInt(labRecommendation.getPlotslno()),labRecommendation.getSurveynumber(),Screen);
				if(extentDetails.size()>0)
				{
					List<Ryot> ryot = ryotService.getRyotByRyotCode(labRecommendation.getRyotcode());
					bean.setId(labRecommendation.getId());
					bean.setExtentSize(extentDetails.get(0).getExtentsize());
					bean.setIsSuitableForCultivation(extentDetails.get(0).getIsSuitableForCultivation());
					bean.setSurveyNo(labRecommendation.getSurveynumber());
					bean.setPlotSlNumber(labRecommendation.getPlotslno());
					bean.setVarietyCode(extentDetails.get(0).getVarietycode());
					bean.setRyotCode(labRecommendation.getRyotcode());
					bean.setRyotName(ryot.get(0).getRyotname());
					bean.setRecommendation(labRecommendation.getRecommendation());
					bean.setSuitableForCultivation(extentDetails.get(0).getIsSuitableForCultivation());
					bean.setSoilTestStatus(extentDetails.get(0).getSoilteststatus());
					
					Date testDate = extentDetails.get(0).getTestDate();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strtestDate = df.format(testDate);
					bean.setTestDate(strtestDate);
			
					bean.setEnteredBy(labRecommendation.getEnteredBy());
					
					beans.add(bean);
				}
			}
		}
		return beans;
	}

	@RequestMapping(value = "/getRecomendationsFromTemp", method = RequestMethod.POST)
	public @ResponseBody List getRecomendationsFromTemp(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		List<LabRecommendations_temp> LabRecommendations_temp = ahFuctionalService.getLabRecomendationsTempByRyotCode(jsonData.getString("ryotcode").toString(), jsonData.getString("plotno").toString(), jsonData.getString("surveyno").toString());
		if (LabRecommendations_temp == null	|| LabRecommendations_temp.isEmpty())
			LabRecommendations_temp = ahFuctionalService.getLabRecomendationsFromRecomendations(jsonData.getString("ryotcode").toString(), jsonData.getString("plotno").toString(), jsonData.getString("surveyno").toString());

		return LabRecommendations_temp;
	}

	// --------------------------- End of Order Advances Master	// -------------------------------------//

	// ----------------------------------- Update Seedling Tray Charges ----------------------------//
	@RequestMapping(value = "/saveSeedlingTrayCharges", method = RequestMethod.POST)
	public @ResponseBody
	boolean saveSeedlingTrayCharges(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try 
		{
			JSONObject SmryData = (JSONObject) jsonData.get("formData");
			JSONArray DtlsData = (JSONArray) jsonData.get("gridData");

			logger.info("========SmryData==========" + SmryData);
			logger.info("========DtlsData==========" + DtlsData);

			int TransactionCode = 0;

			SeedlingTrayChargesSummaryBean seedlingTrayChargesSummaryBean = new ObjectMapper().readValue(SmryData.toString(),SeedlingTrayChargesSummaryBean.class);
			SeedlingTrayChargeSummary seedlingTrayChargeSummary = prepareModelForSeedlingTraySmry(seedlingTrayChargesSummaryBean);
			entityList.add(seedlingTrayChargeSummary);
			
			String flag = seedlingTrayChargesSummaryBean.getModifyFlag();
			double Amt = seedlingTrayChargesSummaryBean.getTotalAmount();
			String loginuser = seedlingTrayChargesSummaryBean.getLoginId();
			String AccountCode = seedlingTrayChargesSummaryBean.getRyotCode();
			String RyotCode = seedlingTrayChargesSummaryBean.getRyotCode();
			String season = seedlingTrayChargesSummaryBean.getSeason();
			if ("Yes".equalsIgnoreCase(flag)) 
			{
				// Revert Acc smry here
				TransactionCode = ahFuctionalService.GetTransactionCodefromDtlsCode(seedlingTrayChargeSummary.getReturnid());
				logger.info("========TransactionCode=========="	+ TransactionCode);

				// Reverting Account tables here
				commonService.RevertAccountTables(TransactionCode);
			} 
			else
			{
				TransactionCode = commonService.GetMaxTransCode(season);
				logger.info("========TransactionCode=========="	+ TransactionCode);
			}
			String strQry = null;
			Object Qryobj = null;

			int AccGrpCode = ahFuctionalService.GetAccGrpCode(seedlingTrayChargesSummaryBean.getRyotCode());
			logger.info("========AccGrpCode==========" + AccGrpCode);

			for (int i = 0; i < DtlsData.size(); i++)
			{
				SeedlingTrayChargesDetailsBean seedlingTrayChargesDetailsBean = new ObjectMapper().readValue(DtlsData.get(i).toString(),SeedlingTrayChargesDetailsBean.class);
				SeedlingTrayChargesDetails seedlingTrayChargesDetails = prepareModelForSeedlingTrayDtls(seedlingTrayChargesDetailsBean,seedlingTrayChargesSummaryBean, TransactionCode);
				entityList.add(seedlingTrayChargesDetails);
				if(seedlingTrayChargesSummaryBean.getModifyFlag().equalsIgnoreCase("Yes"))
				{	
					if(seedlingTrayChargesDetailsBean.getSlno()!=null)
					{
						//boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("SEEDLING TRAY CHARGES","ok");
						boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(seedlingTrayChargesDetailsBean.getScreenName(),"ok");
						if(isAuditTrailRequired)
						//	auditTrailService.updateAuditTrail(seedlingTrayChargesDetailsBean.getSlno(),seedlingTrayChargesDetails,"SEEDLING TRAY CHARGES");
						
						auditTrailService.updateAuditTrail(seedlingTrayChargesDetailsBean.getSlno(),seedlingTrayChargesDetails,seedlingTrayChargesDetailsBean.getScreenName());
					}
				}
				//ahFuctionalService.addSeedlingTrayChargeDtls(seedlingTrayChargesDetails);
			}
			ahFuctionalService.deleteSeedlingTrayCharges(seedlingTrayChargeSummary);
			logger.info("========Before Summary==========");
			ahFuctionalService.addSeedlingTrayChargeSummary(seedlingTrayChargeSummary);
			logger.info("========After Summary==========");

			// Insert Rec in Acc Dtls
			Map DtlsMap = new HashMap();
			
			DtlsMap.put("TRANSACTION_CODE", TransactionCode);
			DtlsMap.put("TRANSACTION_AMOUNT", Amt);
			DtlsMap.put("LOGIN_USER", loginuser);
			DtlsMap.put("ACCOUNT_CODE", AccountCode);
			DtlsMap.put("JOURNAL_MEMO", "Towards Seedling Tray Charges");
			DtlsMap.put("TRANSACTION_TYPE", "Dr");
			//Added by DMurty on 18-10-2016
			DtlsMap.put("SEASON", seedlingTrayChargesSummaryBean.getSeason());

			AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
			entityList.add(accountDetails);

			// Insert Rec in Acc Smry
			String TransactionType = "Dr";
			AccountSummary accountSummary = prepareModelforAccSmry(Amt,AccountCode, TransactionType,season);
			if ("Yes".equalsIgnoreCase(flag))
			{
				double finalAmt = accountSummary.getRunningbalance();
				String strCrDr = accountSummary.getBalancetype();
				String AccCode = accountSummary.getAccountcode();
				
				strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
			} 
			else 
			{
				List BalList = ahFuctionalService.BalDetails(AccountCode,season);
				if (BalList != null && BalList.size() > 0) 
				{
					double finalAmt = accountSummary.getRunningbalance();
					String strCrDr = accountSummary.getBalancetype();
					String AccCode = accountSummary.getAccountcode();

					strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				} 
				else 
				{
					entityList.add(accountSummary);
				}
			}

			// Insert Rec in Acc Grp Details
			Map AccGrpDtlsMap = new HashMap();

			AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
			AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
			AccGrpDtlsMap.put("LOGIN_USER", loginuser);
			AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
			AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
			AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Seedling Tray Charges");
			AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
			//Added by DMurty on 18-10-2016
			AccGrpDtlsMap.put("SEASON", seedlingTrayChargesSummaryBean.getSeason());

			AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
			entityList.add(accountGroupDetails);

			// Insert Rec in Acc Grp Smry
			TransactionType = "Dr";
			AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(RyotCode, Amt, AccGrpCode, TransactionType,season);
			if ("Yes".equalsIgnoreCase(flag))
			{
				double finalAmt = accountGroupSummary.getRunningbalance();
				String strCrDr = accountGroupSummary.getBalancetype();
				int AccGroupCode = accountGroupSummary.getAccountgroupcode();

				strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and season='"+season+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);

				//commonService.updateRunningBalinAccGrpSmry(finalAmt, strCrDr,AccGroupCode);
			} 
			else 
			{
				List BalList = ahFuctionalService.AccGrpBalDetails(AccGrpCode,season);
				if (BalList != null && BalList.size() > 0)
				{
					double finalAmt = accountGroupSummary.getRunningbalance();
					String strCrDr = accountGroupSummary.getBalancetype();
					int AccGroupCode = accountGroupSummary.getAccountgroupcode();

					strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and season = '"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
				else 
				{
					entityList.add(accountGroupSummary);
				}
			}

			// Insert Rec in Acc Sub Grp Dtls
			Map AccSubGrpDtlsMap = new HashMap();

			AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
			AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
			AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
			AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
			AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
			AccSubGrpDtlsMap.put("JOURNAL_MEMO","Towards Seedling Tray Charges");
			AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
			//Added by DMurty on 18-10-2016
			AccSubGrpDtlsMap.put("SEASON", seedlingTrayChargesSummaryBean.getSeason());
					
			AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
			entityList.add(accountSubGroupDetails);

			// Insert Rec in Acc Sub Grp Smry
			TransactionType = "Dr";
			AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(RyotCode, Amt, AccGrpCode, TransactionType,season);
			if ("Yes".equalsIgnoreCase(flag))
			{
				double finalAmt = accountSubGroupSummary.getRunningbalance();
				String strCrDr = accountSubGroupSummary.getBalancetype();
				int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
				
				strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
			} 
			else
			{
				double finalAmt = accountSubGroupSummary.getRunningbalance();
				String strCrDr = accountSubGroupSummary.getBalancetype();
				int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

				List BalList = ahFuctionalService.SubGrpBalDetails(AccGrpCode,AccSubGrpCode,season);
				if (BalList != null && BalList.size() > 0) 
				{
					strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+AccGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season = '"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				} 
				else 
				{
					entityList.add(accountSubGroupSummary);
				}
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	public AccountDetails prepareModelForAccountDetails(Map DtlsMap) 
	{

		int TransactionCode = (Integer) DtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) DtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) DtlsMap.get("LOGIN_USER");
		String AccountCode = (String) DtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) DtlsMap.get("JOURNAL_MEMO");
		String TransType = (String) DtlsMap.get("TRANSACTION_TYPE");
		String GlCode = (String) DtlsMap.get("GL_CODE");
		if ("".equals(GlCode) || "null".equals(GlCode) || (GlCode == null))
		{
			GlCode = "NA";
		}
		//Added by DMurty on 18-10-2016
		String season = (String) DtlsMap.get("SEASON");
		String advDate = (String) DtlsMap.get("TRANSACTION_DATE");
		
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setTransactioncode(TransactionCode);
		if ("".equals(advDate) || "null".equals(advDate) || (advDate == null))
		{
			accountDetails.setTransactiondate(date);
		}
		else
		{
			accountDetails.setTransactiondate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		}
		accountDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountDetails.setAccountcode(AccountCode);
		accountDetails.setJournalmemo(journalMemo);
		if ("Dr".equalsIgnoreCase(TransType))
		{
			accountDetails.setDr(Amt);
			accountDetails.setCr(0.0);
		} 
		else
		{
			accountDetails.setDr(0.0);
			accountDetails.setCr(Amt);
		}
		accountDetails.setRunningbalance(Amt);
		//added by sahadeva 27/10/2016
		accountDetails.setLoginuser(loginController.getLoggedInUserName());
		accountDetails.setIsapplyforledger((byte) 0);
		accountDetails.setGlcode(GlCode);
		accountDetails.setStatus((byte) 1);
		accountDetails.setSeason(season);
		
		return accountDetails;
	}

	public AccountSummary prepareModelforAccSmry(Double Amt,String AccountCode, String TransactionType,String season) 
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		String AccCode = AccountCode;
		double TotalAmount = Amt;

		// Getting Data from Account Summary
		List BalList = commonService.AccSmryBalanceDetails(AccCode,TotalAmount, TransactionType,season);
		if (BalList != null && BalList.size() > 0)
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}

		AccountSummary accountSummary = new AccountSummary();
		accountSummary.setAccountcode(AccountCode);
		accountSummary.setRunningbalance(RunnBal);
		accountSummary.setBalancetype(TransType);
		accountSummary.setSeason(season);
		return accountSummary;
	}

	public AccountGroupDetails prepareModelForAccGrpDetails(Map AccGrpDtlsMap)
	{
		int TransactionCode = (Integer) AccGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccGrpDtlsMap.get("TRANSACTION_TYPE");
		//Added by DMurty on 18-10-2016
		String season = (String) AccGrpDtlsMap.get("SEASON");
		String advDate = (String) AccGrpDtlsMap.get("TRANSACTION_DATE");
		

		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AccountGroupDetails accountGroupDetails = new AccountGroupDetails();

		accountGroupDetails.setTransactioncode(TransactionCode);
		if ("".equals(advDate) || "null".equals(advDate) || (advDate == null))
		{
			accountGroupDetails.setTransactiondate(date);
		}
		else
		{
			accountGroupDetails.setTransactiondate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		}
		//accountGroupDetails.setTransactiondate(date);
		
		accountGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountGroupDetails.setAccountgroupcode(AccGrpCode);
		accountGroupDetails.setJournalmemo(journalMemo);
		if ("Dr".equalsIgnoreCase(TransType)) 
		{
			accountGroupDetails.setDr(Amt);
			accountGroupDetails.setCr(0.0);
		}
		else
		{
			accountGroupDetails.setDr(0.0);
			accountGroupDetails.setCr(Amt);
		}

		accountGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		accountGroupDetails.setLoginuser(loginController.getLoggedInUserName());
		accountGroupDetails.setIsapplyforledger((byte) 1);
		accountGroupDetails.setStatus((byte) 1);
		accountGroupDetails.setSeason(season);
		
		return accountGroupDetails;
	}

	public AccountGroupSummary prepareModelForAccGrpSmry(String AccountCode,Double Amt, int AccGrpCode, String TransactionType,String season)
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;

		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();

		String AccCode = AccountCode;
		double TotalAmt = Amt;

		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0)
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}

		List BalList = commonService.AccGrpBalanceDetails(AccGrpCode, TotalAmt,	TransactionType,season);
		if (BalList != null && BalList.size() > 0) 
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}
		accountGroupSummary.setAccountgroupcode(AccGrpCode);
		accountGroupSummary.setRunningbalance(RunnBal);
		accountGroupSummary.setBalancetype(TransType);
		accountGroupSummary.setSeason(season);
		
		return accountGroupSummary;
	}

	public AccountSubGroupDetails prepareModelForAccSubGrpDtls(Map AccSubGrpDtlsMap)
	{
		int TransactionCode = (Integer) AccSubGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccSubGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccSubGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccSubGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccSubGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccSubGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccSubGrpDtlsMap.get("TRANSACTION_TYPE");

		//Added by DMurty on 18-10-2016
		String season = (String) AccSubGrpDtlsMap.get("SEASON");
		String advDate = (String) AccSubGrpDtlsMap.get("TRANSACTION_DATE");
		
		//Added by DMurty on 18-11-2016
		String isSeedSupplier = (String) AccSubGrpDtlsMap.get("IS_SEED_SUPPLIER");
		//Added by Umanath  on 27-12-2016
		String isSeedPurchase = (String) AccSubGrpDtlsMap.get("IS_SEED_PURCHASES");
		String isConsumer = (String) AccSubGrpDtlsMap.get("IsConsumer");

		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
		if("Yes".equalsIgnoreCase(isSeedSupplier))
		{
			AccSubGrpCode = 2;
		}
		 if("Yes".equalsIgnoreCase(isSeedPurchase))
		{
			AccSubGrpCode = 11;
		}
		 if("Yes".equalsIgnoreCase(isConsumer))
			{
				AccSubGrpCode = 9;
			}
		AccountSubGroupDetails accountSubGroupDetails = new AccountSubGroupDetails();

		accountSubGroupDetails.setTransactioncode(TransactionCode);
		if ("".equals(advDate) || "null".equals(advDate) || (advDate == null))
		{
			accountSubGroupDetails.setTransactiondate(date);
		}
		else
		{
			accountSubGroupDetails.setTransactiondate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		}
		//accountSubGroupDetails.setTransactiondate(date);
		accountSubGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		accountSubGroupDetails.setAccountgroupcode(AccGrpCode);
		accountSubGroupDetails.setAccountsubgroupcode(AccSubGrpCode);
		accountSubGroupDetails.setJournalmemo(journalMemo);

		if ("Dr".equalsIgnoreCase(TransType)) 
		{
			accountSubGroupDetails.setDr(Amt);
			accountSubGroupDetails.setCr(0.0);
		} 
		else
		{
			accountSubGroupDetails.setDr(0.0);
			accountSubGroupDetails.setCr(Amt);
		}

		accountSubGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		accountSubGroupDetails.setLoginuser(loginController.getLoggedInUserName());
		accountSubGroupDetails.setIsapplyforledger((byte) 1);
		accountSubGroupDetails.setStatus((byte) 1);
		accountSubGroupDetails.setSeason(season);
		return accountSubGroupDetails;
	}

	public AccountSubGroupSummary prepareModelforAccSubGrpSmry(String AccountCode, Double Amt, int AccGrpCode,	String TransactionType,String season)
	{
		Double RunnBal = 0.0;
		String TransType = TransactionType;
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		String AccCode = AccountCode;
		double TotalAmt = Amt;
		int AccGroupCode = 0;
		int AccSubGroupCode = 0;
		List CodeList = commonService.getAccCodes(AccCode);
		if (CodeList != null && CodeList.size() > 0) 
		{
			Object[] myResult = (Object[]) CodeList.get(0);
			AccGroupCode = (Integer) myResult[0];
			AccSubGroupCode = (Integer) myResult[1];
		}

		List BalList = commonService.SubGrpBalanceDetails(AccGrpCode,AccSubGroupCode, TotalAmt, TransactionType,season);
		if (BalList != null && BalList.size() > 0) 
		{
			Map TempMap = new HashMap();
			TempMap = (Map) BalList.get(0);
			RunnBal = (Double) TempMap.get("RUNNING_BAL");
			TransType = (String) TempMap.get("TRANS_TYPE");
		}

		accountSubGroupSummary.setAccountgroupcode(AccGrpCode);
		accountSubGroupSummary.setAccountsubgroupcode(AccSubGroupCode);
		accountSubGroupSummary.setRunningbalance(RunnBal);
		accountSubGroupSummary.setBalancetype(TransType);
		accountSubGroupSummary.setSeason(season);
		
		return accountSubGroupSummary;
	}

	private SeedlingTrayChargeSummary prepareModelForSeedlingTraySmry(SeedlingTrayChargesSummaryBean seedlingTrayChargesSummaryBean) 
	{
		Date date = new Date();
		SeedlingTrayChargeSummary seedlingTrayChargeSummary = new SeedlingTrayChargeSummary();

		String flag = seedlingTrayChargesSummaryBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(flag))
		{
			seedlingTrayChargeSummary.setId(seedlingTrayChargesSummaryBean.getReturnId());
		}
		seedlingTrayChargeSummary.setReturnid(seedlingTrayChargesSummaryBean.getReturnId());
		seedlingTrayChargeSummary.setSeason(seedlingTrayChargesSummaryBean.getSeason());
		seedlingTrayChargeSummary.setRyotcode(seedlingTrayChargesSummaryBean.getRyotCode());
		//added by sahadeva 27/10/2016
		seedlingTrayChargeSummary.setLoginid(loginController.getLoggedInUserName());
		seedlingTrayChargeSummary.setTotalamount(seedlingTrayChargesSummaryBean.getTotalAmount());
		seedlingTrayChargeSummary.setSeedlingdate(date);

		return seedlingTrayChargeSummary;
	}

	private SeedlingTrayChargesDetails prepareModelForSeedlingTrayDtls(	SeedlingTrayChargesDetailsBean seedlingTrayChargesDetailsBean,SeedlingTrayChargesSummaryBean seedlingTrayChargesSummaryBean,int TransactionCode) 
	{
		SeedlingTrayChargesDetails seedlingTrayChargesDetails = new SeedlingTrayChargesDetails();
		seedlingTrayChargesDetails.setId(seedlingTrayChargesDetailsBean.getId());
		seedlingTrayChargesDetails.setReturnid(seedlingTrayChargesSummaryBean.getReturnId());
		seedlingTrayChargesDetails.setRyotcode(seedlingTrayChargesSummaryBean.getRyotCode());
		seedlingTrayChargesDetails.setSeason(seedlingTrayChargesSummaryBean.getSeason());
		seedlingTrayChargesDetails.setTraytype(seedlingTrayChargesDetailsBean.getTrayTypeCode());
		seedlingTrayChargesDetails.setNooftrays(seedlingTrayChargesDetailsBean.getNoofTrays());
		seedlingTrayChargesDetails.setCost(seedlingTrayChargesDetailsBean.getCost());
		seedlingTrayChargesDetails.setTotalcost(seedlingTrayChargesDetailsBean.getTotalCost());
		seedlingTrayChargesDetails.setStatus(seedlingTrayChargesDetailsBean.getStatus());
		seedlingTrayChargesDetails.setTransactioncode(TransactionCode);
		seedlingTrayChargesDetails.setVillagecode(seedlingTrayChargesDetailsBean.getVillageCode());

		return seedlingTrayChargesDetails;
	}

	
	
	@RequestMapping(value = "/getTrayChangesByDistance", method = RequestMethod.POST)
	public @ResponseBody
	String getTrayChangesByDistance(@RequestBody JSONObject jsonData) throws Exception 
	{
		Integer villageCode=jsonData.getInt("villagecode");
		Integer trayCode=jsonData.getInt("traycode");
		Double trayCharges=0.0;
		
		Village village=(Village) commonService.getEntityById(villageCode,Village.class);
		
		Double distance=village.getDistance();
		Object trayCharge=ahFuctionalService.getTrayChargeByDistance(distance,trayCode);

		if(trayCharge!=null)
			trayCharges=(Double)trayCharge;
		
		return trayCharges.toString();
	}
	// Modify
	@RequestMapping(value = "/getAllSeedlingTrayCharges", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody String getAllSeedlingTrayCharges(@RequestBody JSONObject jsonData)	throws Exception 
	{
		SeedlingTrayChargesSummaryBean seedlingTrayChargesSummaryBean = null;
		org.json.JSONObject response = new org.json.JSONObject();

		String Season = (String) jsonData.get("season");
		String RyotCode = (String) jsonData.get("ryotCode");

		SeedlingTrayChargeSummary seedlingTrayChargeSummary = ahFuctionalService.getSeedlingTrayCharges(Season, RyotCode);

		if (seedlingTrayChargeSummary != null)
			seedlingTrayChargesSummaryBean = prepareBeanForSeedlingTrayCharges(seedlingTrayChargeSummary);

		List<SeedlingTrayChargesDetails> seedlingTrayChargesDetails = ahFuctionalService.getSeedlingTrayChargesDetails(seedlingTrayChargeSummary.getReturnid());
		List<SeedlingTrayChargesDetailsBean> SeedlingTrayChargesDetailsBeans = prepareListofBeanForSeedlingTrayChargeDtls(seedlingTrayChargesDetails);

		org.json.JSONObject seedlingTrayChargeSummaryJson = new org.json.JSONObject(seedlingTrayChargesSummaryBean);
		org.json.JSONArray seedlingTrayChargesDetailsJson = new org.json.JSONArray(SeedlingTrayChargesDetailsBeans);

		response.put("formData", seedlingTrayChargeSummaryJson);
		response.put("gridData", seedlingTrayChargesDetailsJson);

		return response.toString();
	}

	private SeedlingTrayChargesSummaryBean prepareBeanForSeedlingTrayCharges(SeedlingTrayChargeSummary seedlingTrayChargeSummary) 
	{
		SeedlingTrayChargesSummaryBean bean = new SeedlingTrayChargesSummaryBean();

		bean.setReturnId(seedlingTrayChargeSummary.getReturnid());
		bean.setRyotCode(seedlingTrayChargeSummary.getRyotcode());
		bean.setSeason(seedlingTrayChargeSummary.getSeason());
		bean.setLoginId(seedlingTrayChargeSummary.getLoginid());
		bean.setTotalAmount(seedlingTrayChargeSummary.getTotalamount());

		return bean;
	}

	private List<SeedlingTrayChargesDetailsBean> prepareListofBeanForSeedlingTrayChargeDtls(List<SeedlingTrayChargesDetails> seedlingTrayChargesDetailss) 
	{
		List<SeedlingTrayChargesDetailsBean> beans = null;
		if (seedlingTrayChargesDetailss != null	&& !seedlingTrayChargesDetailss.isEmpty())
		{
			beans = new ArrayList<SeedlingTrayChargesDetailsBean>();
			SeedlingTrayChargesDetailsBean bean = null;
			for (SeedlingTrayChargesDetails seedlingTrayChargesDetails : seedlingTrayChargesDetailss)
			{
				bean = new SeedlingTrayChargesDetailsBean();

				bean.setId(seedlingTrayChargesDetails.getId());
				bean.setNoofTrays(seedlingTrayChargesDetails.getNooftrays());
				bean.setCost(seedlingTrayChargesDetails.getCost());
				bean.setTotalCost(seedlingTrayChargesDetails.getTotalcost());
				bean.setStatus(seedlingTrayChargesDetails.getStatus());
				bean.setTrayTypeCode(seedlingTrayChargesDetails.getTraytype());
				bean.setVillageCode(seedlingTrayChargesDetails.getVillagecode());
				bean.setSlno(seedlingTrayChargesDetails.getSlno());

				beans.add(bean);
			}
		}
		return beans;
	}

	// Agreement Master
	public static Date addDays(Date date, int days)
	{
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}

	public Date ConvertDate(Date date)
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String s = df.format(date);
		String result = s;
		try 
		{
			date = df.parse(result);
		} 
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	public static String NextSeasonYear(String season)
	{
		String finalyear=null;
		if(season != null)
		{
			String s[]=season.split("-");
			String year=s[1];
			int nyear=Integer.parseInt(year);
			int newyear=nyear+1;
			String strnextyear=String.valueOf(newyear);
			finalyear=year+"-"+strnextyear;
		}
		return finalyear;
	}
	
	
	
	
	/*@RequestMapping(value = "/getValidateRyotForAgreement", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getValidateRyotForAgreement(@RequestBody JSONObject jsonData) throws Exception
	{
		String season = (String) jsonData.get("season");
		String ryotCode = (String) jsonData.get("ryotCode");

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
	
		List<Integer> ExtentList = new ArrayList<Integer>();
		ExtentList = ahFuctionalService.getValidateRyotForAgreement(season, ryotCode);
		logger.info("========ExtentList==========" + ExtentList);
		jsonObj = new JSONObject();
		int isSutableforCultivation = 0;
		String cultivationFlag = null;
		try 
		{
			if (ExtentList != null && ExtentList.size() > 0) 
			{
				int myResult1 = ExtentList.get(0);
				isSutableforCultivation = myResult1;
			}
			if(isSutableforCultivation == 0)
			{
				cultivationFlag = "Yes";
			}
			else
			{
				cultivationFlag = "No";
			}
			
			jsonObj.put("flag", cultivationFlag);
			jArray.add(jsonObj);
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}*/
	
	@RequestMapping(value = "/getAgreementNumbersForSeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getAgreementNumbersForSeason(@RequestBody JSONObject jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		String Season = (String) jsonData.get("season");
		String tablename = (String) jsonData.get("tablename");
		List dropdownList = ahFuctionalService.getAgreementNumbersForSeason(Season,tablename);
		logger.info("========dropdownList==========" + dropdownList);
		try
		{
			if (dropdownList != null && dropdownList.size() > 0) 
			{
				for (int i=0;i<dropdownList.size();i++) 
				{
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					String agreementNo = (String) TempMap.get("agreementnumber");
					jsonObj = new JSONObject();
					jsonObj.put("agreementnumber", agreementNo);
					jArray.add(jsonObj);
				}
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}
	
	@RequestMapping(value = "/getVarietyForSeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getVarietyForSeason(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		List dropdownList = ahFuctionalService.getVarityForSeason(jsonData);
		try
		{
			if (dropdownList != null && dropdownList.size() > 0) 
			{
				for (int i=0;i<dropdownList.size();i++) 
				{
					jsonObj = new JSONObject();
					Map rlMap=(Map)dropdownList.get(i);
					Integer vcode=(Integer)rlMap.get("varietycode");
					String vname=(String)rlMap.get("variety");
					jsonObj.put("varietycode", vcode);
					jsonObj.put("variety", vname);
					jArray.add(jsonObj);
				}
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}
	

	@RequestMapping(value = "/getPreviousAgreementsForRatoon", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	JSONArray getPreviousAgreementsForRatoon(@RequestBody JSONObject jsonData)	throws Exception
	{
		String season = (String) jsonData.get("season");
		String ryotCode = (String) jsonData.get("ryotCode");

		String SeasonYear[] = season.split("-");
		String firstYear = SeasonYear[0];
		int nfirstYear = Integer.parseInt(firstYear);
		int beforeYear = nfirstYear - 1;
		String strbeforeYear = Integer.toString(beforeYear);
		String finalPrevSeasonYear = strbeforeYear + "-" + firstYear;

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;

		List<AgreementDetails> agreementDetails = ahFuctionalService.getPreviousAgreementsForRatoon(finalPrevSeasonYear, ryotCode);
		logger.info("========agreementDetails==========" + agreementDetails);
		jsonObj = new JSONObject();
		try
		{
			if (agreementDetails != null && !agreementDetails.isEmpty())
			{
				for (AgreementDetails agreementDetail : agreementDetails)
				{
					String agreementNo = agreementDetail.getAgreementnumber();
					String plotNo = agreementDetail.getPlotnumber();
					String surveyNo = agreementDetail.getSurveynumber();

					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

					Date startDate = agreementDetail.getCanesupplystartdate();
					Date endDate = agreementDetail.getCanesupplyenddate();

					String strstartDate = df.format(startDate);
					String strendDate = df.format(endDate);
					
					if (startDate != null && endDate != null) 
					{
						long diff = endDate.getTime() - startDate.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						float daysdiff = (float) diffDays;
						float nodays = daysdiff / 2;
						nodays = Math.round(nodays);
						int nnodays = (int) nodays;
						//Here we need to add 2 days to start date and consider that as mean date 
						Date newDate = addDays(startDate, nnodays);
						String strfinalDate = df.format(newDate);

						jsonObj.put("agreementNo", agreementNo);
						jsonObj.put("plotNo", plotNo);
						jsonObj.put("surveyNo", surveyNo);
						jsonObj.put("startDate", strstartDate);
						jsonObj.put("endDate", strendDate);
						jsonObj.put("meanDate", strfinalDate);

						jArray.add(jsonObj);
					}
				}
			}
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}

	@RequestMapping(value = "/saveAgreementImage", method = RequestMethod.POST)
	public @ResponseBody	String saveAgreementImage(@RequestParam("file") MultipartFile file,	@RequestParam("path") String path) throws JsonParseException,JsonMappingException, Exception
	{
		try 
		{
			byte[] bytes = file.getBytes();

			File dir1 = new File(servletContext.getRealPath("/") + "angular/"+ "img/" + "images/");
			logger.info("=======dir1-------------" + dir1);
			logger.info("=======path-------------" + path);
			if (!dir1.exists())
				dir1.mkdirs();

			String ext = Constants.getFileExtension(file.getOriginalFilename());
			String FileName = path + "." + ext.toLowerCase();

			String srcPath = "img/" + "images/" + FileName;
			File serverFile1 = new File(dir1.getAbsolutePath() + File.separator	+ FileName);

			BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
			stream1.write(bytes);
			stream1.close();

			logger.info("Server File Location="+ serverFile1.getAbsolutePath());
			logger.info("Relative Path----=" + srcPath);

			return srcPath;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}
	
	//Added by DMurty on 01-02-2017
	@RequestMapping(value = "/getTempAgreementsForRyot", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getTempAgreementsForRyot(@RequestBody JSONObject jsonData)	throws Exception
	{
		String season = (String) jsonData.get("season");
		String ryotCode = (String) jsonData.get("ryotCode");
		List<Ryot> ryot = ryotService.getRyotByRyotCode(ryotCode);

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		
		List TempAgrList = ahFuctionalService.getTempAgreementsForRyot(ryotCode,season);
		logger.info("========getTempAgreementsForRyot()==========" + TempAgrList);
		jsonObj = new JSONObject();
		try
		{
			if (TempAgrList != null && TempAgrList.size()>0)
			{
				for (int i=0;i<TempAgrList.size();i++)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) TempAgrList.get(0);
					String agreementNo = (String) tempMap.get("agreementnumber");
					
					Date agrDate = (Date) tempMap.get("");
					int circleCode = (Integer) tempMap.get("circlecode");
					List<Circle> circlels=ahFuctionalService.getCircleRecordDetails(circleCode);
					
					
					
					jsonObj.put("agreementNo", agreementNo);
					jsonObj.put("ryotName", ryot.get(0).getRyotname());
					
					//Agreement no, Circle, Agreement Dt,Zone
					
					jArray.add(jsonObj);
				}
			}
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/saveAgreement", method = RequestMethod.POST)
	public @ResponseBody	boolean saveAgreement(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		try
		{

			JSONObject formData = (JSONObject) jsonData.get("formData");
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			AgreementSummary agreementSummary=new AgreementSummary();

			AgreementSummaryBean agreementSummaryBean = new ObjectMapper().readValue(formData.toString(), AgreementSummaryBean.class);
			if(agreementSummaryBean.getTypeOfAgreement()==0)
			{
				RandDAgreementSummary randDAgreementSummary = prepareModelForRANDAgreementSummary(agreementSummaryBean);
				entityList.add(randDAgreementSummary);
			}
			else
			{
			 agreementSummary = prepareModelForAgreementSummary(agreementSummaryBean);
			 entityList.add(agreementSummary);
			}
			
			if(agreementSummaryBean.getAgreementSeqNo()!=null)
			{
				//boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired("GENERATE AGREEMENT","ok");
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(agreementSummaryBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrailAgSummary(agreementSummaryBean.getAgreementNumber(),agreementSummary,agreementSummaryBean.getScreenName());		
			}
			String accNum = agreementSummaryBean.getAccountNumber();
			int branch = agreementSummaryBean.getBranchCode();
			String agreementNo = agreementSummaryBean.getAgreementNumber();
			String season = agreementSummaryBean.getSeasonYear();
			String ryotCode = agreementSummaryBean.getRyotCode();
			
			List AgreementList = new ArrayList();
			
			 List<RyotBankDetails> rbanklist=ahFuctionalService.getRyotBankAccListForValidation(accNum);
			 if(rbanklist.size()>0 && rbanklist!=null)
			{
				 
			}
			else{
				
				Map TempMap = new HashMap();
				TempMap.put("ACCOUNT_NUMBER", accNum);
				TempMap.put("BRANCH_CODE", branch);
				TempMap.put("RYOT_CODE", ryotCode);
				int accType = 0;
				TempMap.put("ACC_TYPE", accType);

				RyotBankDetails ryotBankDetails = prepareModelForRyotBranch(TempMap);
				//ryotService.addRyotBankDetails(ryotBankDetails);
				entityList.add(ryotBankDetails);
				
			}
	
			for (int i = 0; i < gridData.size(); i++)
			{
				AgreementDetails agreementDetails=new AgreementDetails();
				AgreementDetailsBean agreementDetailsBean = new ObjectMapper().readValue(gridData.get(i).toString(),AgreementDetailsBean.class);
				if(agreementSummaryBean.getTypeOfAgreement()==0)
				{
				RandDAgreementDetails randDAgreementDetails = prepareModelForRANDAgreementDetails(agreementDetailsBean, agreementSummaryBean);
				entityList.add(randDAgreementDetails);
				} 
				else
				{
					 agreementDetails = prepareModelForAgreementDetails(agreementDetailsBean, agreementSummaryBean);
					 entityList.add(agreementDetails);
				}
				
				//ahFuctionalService.addAgreementDetails(agreementDetails);
				if(agreementSummaryBean.getTypeOfAgreement()!=0)
				{
					if(agreementDetailsBean.getSlno()!=null)
					{
						boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(agreementDetailsBean.getScreenName(),"ok");
						if(isAuditTrailRequired)
							auditTrailService.updateAuditTrail(agreementDetailsBean.getSlno(),agreementDetails,agreementDetailsBean.getScreenName());		
					}
				}
			}
				if(agreementSummaryBean.getTypeOfAgreement()!=0)
				{
					ahFuctionalService.deleteAgreementDetails(agreementSummary.getAgreementnumber());
				}
			String modifyFlag = agreementSummaryBean.getModifyFlag();
			//ahFuctionalService.addAgreementSummary(agreementSummary,modifyFlag);
			
			isInsertSuccess = commonService.saveMultipleEntitiesForAgreement(entityList,UpdateList,modifyFlag);
			return isInsertSuccess;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	private RyotBankDetails prepareModelForRyotBranch(Map TempMap)
	{

		RyotBankDetails ryotBankDetails=new RyotBankDetails();

		String accNum = (String) TempMap.get("ACCOUNT_NUMBER");
		int branchCode = (Integer) TempMap.get("BRANCH_CODE");
		String ryotCode = (String) TempMap.get("RYOT_CODE");
		//Added by DMurty on 27-08-2016
		int accType = (Integer) TempMap.get("ACC_TYPE");


		String IfscCode = "";
		List<String> IfscList = new ArrayList<String>();
		IfscList = ahFuctionalService.getIfscCodeByBrancgCode(branchCode);
		if (IfscList != null && IfscList.size() > 0)
		{
			String myResult1 = IfscList.get(0);
			IfscCode = myResult1;
		}
		List<Integer> MaxIdList = new ArrayList<Integer>();
		int maxid=0;
		MaxIdList = ahFuctionalService.getMaxIdForBankDetails(ryotCode);
		if (MaxIdList != null && MaxIdList.size() > 0)
		{
			if (!MaxIdList.contains(null)) // oldDate.equals(returndate.trim()))
			{
				int myResult1 = MaxIdList.get(0);
				maxid = myResult1+1;
			}
			else
			{
				maxid = 1;
			}
		}
		
		ryotBankDetails.setId(maxid);
		ryotBankDetails.setAccountnumber(accNum);
		ryotBankDetails.setAccounttype((byte) accType);
		ryotBankDetails.setIfsccode(IfscCode);
		ryotBankDetails.setRyotbankbranchcode(branchCode);
		ryotBankDetails.setRyotcode(ryotCode);
				
		return ryotBankDetails;
	}
	
	private RyotBankDetails prepareModelForRyotBranchNew(Map TempMap)
	{

		RyotBankDetails ryotBankDetails=new RyotBankDetails();

		String accNum = (String) TempMap.get("ACCOUNT_NUMBER");
		int branchCode = (Integer) TempMap.get("BRANCH_CODE");
		String ryotCode = (String) TempMap.get("RYOT_CODE");
		String season = (String) TempMap.get("SEASON");

		String IfscCode = "";
		List<String> IfscList = new ArrayList<String>();
		IfscList = ahFuctionalService.getIfscCodeByBrancgCodeNew(branchCode);
		if (IfscList != null && IfscList.size() > 0)
		{
			String myResult1 = IfscList.get(0);
			IfscCode = myResult1;
		}
		List<Integer> MaxIdList = new ArrayList<Integer>();
		int maxid=0;
		MaxIdList = ahFuctionalService.getMaxIdForBankDetailsNew(ryotCode);
		if (MaxIdList != null && MaxIdList.size() > 0)
		{
			if (!MaxIdList.contains(null)) // oldDate.equals(returndate.trim()))
			{
				int myResult1 = MaxIdList.get(0);
				maxid = myResult1+1;
			}
			else
			{
				maxid = 1;
			}
		}
		ryotBankDetails.setId(maxid);
		ryotBankDetails.setAccountnumber(accNum);
		ryotBankDetails.setAccounttype((byte) 0);
		ryotBankDetails.setIfsccode(IfscCode);
		ryotBankDetails.setRyotbankbranchcode(branchCode);
		ryotBankDetails.setRyotcode(ryotCode);
		ryotBankDetails.setSeason(season);		
		return ryotBankDetails;
	}
	
	private AgreementSummary prepareModelForAgreementSummary(AgreementSummaryBean agreementSummaryBean) throws Exception
	{
		AgreementSummary agreementSummary = new AgreementSummary();
		String modifyFlag = agreementSummaryBean.getModifyFlag();
		Integer maxId = commonService.getMaxColumnValue("AgreementSummary","agreementseqno","seasonyear",agreementSummaryBean.getSeasonYear());
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			agreementSummary.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
			agreementSummary.setAgreementseqno(agreementSummaryBean.getAgreementSeqNo());
		}
		else
		{
			String season = "";
			agreementSummary.setAgreementseqno(maxId);
			String strSeqNo = Integer.toString(maxId);
			
			String AgNo = agreementSummaryBean.getAgreementNumber();
			if(AgNo.contains("/"))
			{
				String agreement[] = AgNo.split("/");
				season = agreement[1];
			}
			String strAgreementNo = strSeqNo+"/"+season;
			agreementSummary.setAgreementnumber(strAgreementNo);
		}
		agreementSummary.setAgreementdate(DateUtils.getSqlDateFromString(agreementSummaryBean.getAgreementDate(),Constants.GenericDateFormat.DATE_FORMAT));
		agreementSummary.setBranchcode(agreementSummaryBean.getBranchCode());
		agreementSummary.setAccountnumber(agreementSummaryBean.getAccountNumber());
		agreementSummary.setMandalcode(agreementSummaryBean.getMandalCode());
	//	agreementSummary.setCirclecode(0);
		agreementSummary.setCirclecode(agreementSummaryBean.getCircleCode());
		agreementSummary.setFamilygroupcode(agreementSummaryBean.getFamilyGroupCode());
		agreementSummary.setSeasonyear(agreementSummaryBean.getSeasonYear());
		agreementSummary.setRyotcode(agreementSummaryBean.getRyotCode());
		agreementSummary.setRemarks(agreementSummaryBean.getRemarks());
		agreementSummary.setStatus(agreementSummaryBean.getStatus());
		agreementSummary.setHasfamilygroup(agreementSummaryBean.getHasFamilyGroup());
		//added by sahadeva 27/10/2016
		agreementSummary.setLoginId(loginController.getLoggedInUserName());
		agreementSummary.setSurityPerson("0");
		
		
		if(agreementSummaryBean.getSuretyRyotCode().equals("0"))	//Here 0 means suriety person is ryot else is for others
		{
			agreementSummary.setSurityPersonaadhaarNumber(agreementSummaryBean.getSurityPersonaadhaarNumber());
			agreementSummary.setSurityPersonName(agreementSummaryBean.getSurityPersonName());
		}
		else
		{
			List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getSuretyRyotCode());
			String aadharNo = ryot.get(0).getAadhaarNumber();
			agreementSummary.setSurityPersonaadhaarNumber(aadharNo);
			agreementSummary.setSurityPersonName( ryot.get(0).getRyotname());
			agreementSummary.setSurityPerson(agreementSummaryBean.getSuretyRyotCode());
		}
		
		List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getRyotCode());
		int familygrpCode = 0;
		if(agreementSummaryBean.getFamilyGroupCode() != null)
		{
			familygrpCode = agreementSummaryBean.getFamilyGroupCode();
		}
		logger.info("========familygrpCode=========="+familygrpCode);
		if(familygrpCode != 0)
		{
			String ryotName = ryot.get(0).getRyotname();
			List FamilyList = new ArrayList();
			FamilyList = ahFuctionalService.getFamilyMembers(familygrpCode);
			logger.info("========FamilyList=========="+FamilyList);
			List checkList = new ArrayList();
			String strfmlymember = "";
			Map FmlyMap = new HashMap();

			if(FamilyList != null && FamilyList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) FamilyList.get(0);
				String familyMembers = (String) tempMap.get("familygroupmembers");
				logger.info("========familyMembers=========="+familyMembers);
				if ("null".equals(familyMembers) || "".equals(familyMembers) || familyMembers == null)
				{
					strfmlymember = ryot.get(0).getRyotname();
					strfmlymember = strfmlymember+",";
					logger.info("========strfmlymember=========="+strfmlymember);
					ahFuctionalService.UpdateFamilyMembers(strfmlymember,familygrpCode);
				}
				else
				{
					List FamlyList = new ArrayList();
					String family[] = familyMembers.split(",");
					for (int k = 0; k < family.length; k++)
					{
						String member = family[k];
						Map TempMap = new HashMap();
						TempMap.put("MEMBER", member);
						boolean istrue = checkList.contains(TempMap);
						if (istrue == false)
						{
							FamlyList.add(TempMap);
						}
					}
					Map TmpMap = new HashMap();
					TmpMap.put("MEMBER", ryotName);
					boolean istrue = FamlyList.contains(TmpMap);
					if (istrue == false)
					{
						FamlyList.add(TmpMap);
					}
					if(FamlyList != null && FamlyList.size()>0)
					{
						for (int j = 0; j < FamlyList.size(); j++)
						{
							Map FamilyMap = new HashMap();
							FamilyMap = (Map) FamlyList.get(j);
							String member = (String) FamilyMap.get("MEMBER");
							if ("null".equals(strfmlymember) || "".equals(strfmlymember) || strfmlymember == null)
							{
								strfmlymember = member;
							}
							else
							{
								strfmlymember = strfmlymember+","+member;
							}
						}
					}
					logger.info("========strfmlymember=========="+strfmlymember);
					ahFuctionalService.UpdateFamilyMembers(strfmlymember,familygrpCode);
				}		
			}
		}
		return agreementSummary;
	}
	
	private RandDAgreementSummary prepareModelForRANDAgreementSummary(AgreementSummaryBean agreementSummaryBean) throws Exception
	{
		RandDAgreementSummary agreementSummary = new RandDAgreementSummary();
		String modifyFlag = agreementSummaryBean.getModifyFlag();
		Integer maxId = commonService.getMaxColumnValue("RandDAgreementSummary","agreementseqno","seasonyear",agreementSummaryBean.getSeasonYear());
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			agreementSummary.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
			agreementSummary.setAgreementseqno(agreementSummaryBean.getAgreementSeqNo());
		}
		else
		{
			String season = "";
			agreementSummary.setAgreementseqno(maxId);
			String strSeqNo = Integer.toString(maxId);
			
			String AgNo = agreementSummaryBean.getAgreementNumber();
			if(AgNo.contains("/"))
			{
				String agreement[] = AgNo.split("/");
				season = agreement[1];
			}
			String strAgreementNo = "T"+strSeqNo+"/"+season;
			agreementSummary.setAgreementnumber(strAgreementNo);
		}
		agreementSummary.setAgreementdate(DateUtils.getSqlDateFromString(agreementSummaryBean.getAgreementDate(),Constants.GenericDateFormat.DATE_FORMAT));
		agreementSummary.setBranchcode(agreementSummaryBean.getBranchCode());
		agreementSummary.setAccountnumber(agreementSummaryBean.getAccountNumber());
		agreementSummary.setMandalcode(agreementSummaryBean.getMandalCode());
	//	agreementSummary.setCirclecode(0);
		agreementSummary.setCirclecode(agreementSummaryBean.getCircleCode());
		agreementSummary.setFamilygroupcode(agreementSummaryBean.getFamilyGroupCode());
		agreementSummary.setSeasonyear(agreementSummaryBean.getSeasonYear());
		agreementSummary.setRyotcode(agreementSummaryBean.getRyotCode());
		agreementSummary.setRemarks(agreementSummaryBean.getRemarks());
		agreementSummary.setStatus(agreementSummaryBean.getStatus());
		agreementSummary.setHasfamilygroup(agreementSummaryBean.getHasFamilyGroup());
		//added by sahadeva 27/10/2016
		agreementSummary.setLoginId(loginController.getLoggedInUserName());
		agreementSummary.setSurityPerson("0");
		
		
		if(agreementSummaryBean.getSuretyRyotCode().equals("0"))	//Here 0 means suriety person is ryot else is for others
		{
			agreementSummary.setSurityPersonaadhaarNumber(agreementSummaryBean.getSurityPersonaadhaarNumber());
			agreementSummary.setSurityPersonName(agreementSummaryBean.getSurityPersonName());
		}
		else
		{
			List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getSuretyRyotCode());
			String aadharNo = ryot.get(0).getAadhaarNumber();
			agreementSummary.setSurityPersonaadhaarNumber(aadharNo);
			agreementSummary.setSurityPerson(agreementSummaryBean.getSuretyRyotCode());
			agreementSummary.setSurityPersonName(ryot.get(0).getRyotname());
			
		}
		
		List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getRyotCode());
		int familygrpCode = 0;
		if(agreementSummaryBean.getFamilyGroupCode() != null)
		{
			familygrpCode = agreementSummaryBean.getFamilyGroupCode();
		}
		logger.info("========familygrpCode=========="+familygrpCode);
		if(familygrpCode != 0)
		{
			String ryotName = ryot.get(0).getRyotname();
			List FamilyList = new ArrayList();
			FamilyList = ahFuctionalService.getFamilyMembers(familygrpCode);
			logger.info("========FamilyList=========="+FamilyList);
			List checkList = new ArrayList();
			String strfmlymember = "";
			Map FmlyMap = new HashMap();

			if(FamilyList != null && FamilyList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) FamilyList.get(0);
				String familyMembers = (String) tempMap.get("familygroupmembers");
				logger.info("========familyMembers=========="+familyMembers);
				if ("null".equals(familyMembers) || "".equals(familyMembers) || familyMembers == null)
				{
					strfmlymember = ryot.get(0).getRyotname();
					strfmlymember = strfmlymember+",";
					logger.info("========strfmlymember=========="+strfmlymember);
					ahFuctionalService.UpdateFamilyMembers(strfmlymember,familygrpCode);
				}
				else
				{
					List FamlyList = new ArrayList();
					String family[] = familyMembers.split(",");
					for (int k = 0; k < family.length; k++)
					{
						String member = family[k];
						Map TempMap = new HashMap();
						TempMap.put("MEMBER", member);
						boolean istrue = checkList.contains(TempMap);
						if (istrue == false)
						{
							FamlyList.add(TempMap);
						}
					}
					Map TmpMap = new HashMap();
					TmpMap.put("MEMBER", ryotName);
					boolean istrue = FamlyList.contains(TmpMap);
					if (istrue == false)
					{
						FamlyList.add(TmpMap);
					}
					if(FamlyList != null && FamlyList.size()>0)
					{
						for (int j = 0; j < FamlyList.size(); j++)
						{
							Map FamilyMap = new HashMap();
							FamilyMap = (Map) FamlyList.get(j);
							String member = (String) FamilyMap.get("MEMBER");
							if ("null".equals(strfmlymember) || "".equals(strfmlymember) || strfmlymember == null)
							{
								strfmlymember = member;
							}
							else
							{
								strfmlymember = strfmlymember+","+member;
							}
						}
					}
					logger.info("========strfmlymember=========="+strfmlymember);
					ahFuctionalService.UpdateFamilyMembers(strfmlymember,familygrpCode);
				}		
			}
		}
		return agreementSummary;
	}

	private AgreementDetails prepareModelForAgreementDetails(AgreementDetailsBean agreementDetailsBean,	AgreementSummaryBean agreementSummaryBean) throws Exception
	{
		List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getRyotCode());
		AgreementDetails agreementDetails = new AgreementDetails();
		
		String modifyFlag = agreementSummaryBean.getModifyFlag();
		Integer maxId = commonService.getMaxColumnValue("AgreementSummary","agreementseqno","seasonyear",agreementSummaryBean.getSeasonYear());
		
		//Added by sahadeva/Dmurthy 16-11-2016
		String plotNo = agreementDetailsBean.getPlotNumber();
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			List<AgreementDetails> agreementDetails1 = ahFuctionalService.getAgreementDetailsBySeasonByPlot(agreementSummaryBean.getSeasonYear(), agreementSummaryBean.getAgreementNumber(),plotNo);
			
			if(agreementDetails1.size()>0 && agreementDetails1 != null)
			{
			if(agreementDetails1.get(0).getCanesupplyenddate() != null)
			{
				Date canesupplyenddate = agreementDetails1.get(0).getCanesupplyenddate();
				agreementDetails.setCanesupplyenddate(canesupplyenddate);
			}
			if(agreementDetails1.get(0).getCanesupplystartdate() != null)
			{
				Date canesupplystartdate = agreementDetails1.get(0).getCanesupplystartdate();
				agreementDetails.setCanesupplystartdate(canesupplystartdate);
			}
			if(agreementDetails1.get(0).getEstimatedqty() != null)
			{
				double estimatedqty = agreementDetails1.get(0).getEstimatedqty();
				agreementDetails.setEstimatedqty(estimatedqty);
			}
			if(agreementDetails1.get(0).getFamilygroupavgccsrating() != null)
			{
				double familygroupavgccsrating = agreementDetails1.get(0).getFamilygroupavgccsrating();
				agreementDetails.setFamilygroupavgccsrating(familygroupavgccsrating);
			}
			if(agreementDetails1.get(0).getAverageccsrating() != null)
			{
				double averageccsrating = agreementDetails1.get(0).getAverageccsrating();
				agreementDetails.setAverageccsrating(averageccsrating);
			}
			if(agreementDetails1.get(0).getPermitnumbers() != null)
			{
				String permitnumbers=agreementDetails1.get(0).getPermitnumbers();
				agreementDetails.setPermitnumbers(permitnumbers);
			}
			if(agreementDetails1.get(0).getProgramnumber() != null)
			{
				String programnumber=agreementDetails1.get(0).getProgramnumber();
				agreementDetails.setProgramnumber(programnumber);
			}
			if(agreementDetails1.get(0).getProgressiveqty()!= null)
			{
				double progressiveqty = agreementDetails1.get(0).getProgressiveqty();
				agreementDetails.setProgressiveqty(progressiveqty);
			}
			if(agreementDetails1.get(0).getRank()!= null)
			{
				Integer rank=agreementDetails1.get(0).getRank();
				agreementDetails.setRank(rank);
			}
			if(agreementDetails1.get(0).getSamplecards()!= null)
			{
				String samplecards=agreementDetails1.get(0).getSamplecards();
				agreementDetails.setSamplecards(samplecards);
			}
			}
		}
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			agreementDetails.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
		}
		else
		{
			String season = "";
			String strSeqNo = Integer.toString(maxId);
			
			String AgNo = agreementSummaryBean.getAgreementNumber();
			if(AgNo.contains("/"))
			{
				String agreement[] = AgNo.split("/");
				season = agreement[1];
			}
			
			String strAgreementNo = strSeqNo+"/"+season;
			agreementDetails.setAgreementnumber(strAgreementNo);

		}
		//added by naidu 15-09-2016
		List circleList=ahFuctionalService.getCircleRecordDetails(agreementSummaryBean.getCircleCode());
		Circle circle = (Circle)circleList.get(0);
	//	Circle circle = (Circle) commonService.getEntityById(agreementDetailsBean.getCircleCode(), Circle.class);
		Mandal mandal = (Mandal) commonService.getEntityById(agreementSummaryBean.getMandalCode(), Mandal.class);

		agreementDetails.setPlotseqno(agreementDetailsBean.getPlotSeqNo());
		//agreementDetails.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
		agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(agreementSummaryBean.getAgreementDate(),Constants.GenericDateFormat.DATE_FORMAT));

		agreementDetails.setCirclecode(agreementSummaryBean.getCircleCode());//
		agreementDetails.setLandtype(agreementDetailsBean.getLandTypeCode());
		
		agreementDetails.setLandvillagecode(agreementDetailsBean.getLandVillageCode());
		
		
		agreementDetails.setPlotnumber(agreementDetailsBean.getPlotNumber());
		agreementDetails.setPlotseqno(agreementDetailsBean.getPlotSeqNo());
		agreementDetails.setExtentsize(agreementDetailsBean.getExtentSize());
		agreementDetails.setAgreedqty(agreementDetailsBean.getAgreedQty());
		agreementDetails.setPlantorratoon(agreementDetailsBean.getPlantorRatoon());

		String cropDate = agreementDetailsBean.getCropDate();
		agreementDetails.setCropdate(DateUtils.getSqlDateFromString(cropDate,Constants.GenericDateFormat.DATE_FORMAT));

		agreementDetails.setRyotcode(agreementSummaryBean.getRyotCode());
		agreementDetails.setRyotname(ryot.get(0).getRyotname());
		agreementDetails.setRelativename(agreementSummaryBean.getRelativeName());
		agreementDetails.setRelation(ryot.get(0).getRelation());
		agreementDetails.setRyotsalutation(agreementSummaryBean.getSalutation());
		agreementDetails.setSuretyryotcode(agreementSummaryBean.getSuretyRyotCode().toString());
		agreementDetails.setSurveynumber(agreementDetailsBean.getSurveyNumber());
		agreementDetails.setMandalcode(agreementSummaryBean.getMandalCode());
		agreementDetails.setVarietycode(agreementDetailsBean.getVarietyCode());
	
		agreementDetails.setVillagecode(ryot.get(0).getVillagecode());
		agreementDetails.setZonecode(agreementSummaryBean.getZoneCode());
		agreementDetails.setSeasonyear(agreementSummaryBean.getSeasonYear());
		agreementDetails.setCanemanagerid(circle.getCanemanagerid());
		//agreementDetails.setFieldofficerid(circle.getFieldofficerid());
		//agreementDetails.setFieldassistantid(circle.getFieldassistantid());
		agreementDetails.setFieldofficerid(agreementSummaryBean.getFoCode());
		agreementDetails.setFieldassistantid(agreementSummaryBean.getFaCode());
		agreementDetails.setProgressiveqty(0.0);
		agreementDetails.setRyotseqno(ryot.get(0).getRyotcodesequence());
		agreementDetails.setCommercialOrSeed(agreementDetailsBean.getPlotIdentity());//
		agreementDetails.setExtentimagepath(agreementDetailsBean.getExtentImage());
		//Added by DMurty on 06-09-2016  Modified by sahadeva 26-10-2016
		if(agreementDetailsBean.getPlotDistance()!=null)
		{
			agreementDetails.setPlotdistance(agreementDetailsBean.getPlotDistance());
		}
		else
		{
			agreementDetails.setPlotdistance(0.0);
		}
		//end of modification
		String address = agreementDetailsBean.getAddress();
		if ("".equals(address) || "null".equals(address) || (address == null))
		{
			address = "NA";
		}
		agreementDetails.setAddress(address);
		
		//Added by DMurty on 03-12-2016
		int regionCode = 0;
		//List<Zone> zone= ahFuctionalService.getRegionCode(mandal.getZonecode());
		List<Zone> zone= ahFuctionalService.getRegionCode(agreementSummaryBean.getZoneCode());
		if(zone != null && zone.size()>0)
		{
			regionCode = zone.get(0).getRegioncode();
		}
		agreementDetails.setRegioncode(regionCode);
		
		return agreementDetails;
	}
	
	private RandDAgreementDetails prepareModelForRANDAgreementDetails(AgreementDetailsBean agreementDetailsBean,	AgreementSummaryBean agreementSummaryBean) throws Exception
	{
		List<Ryot> ryot = ryotService.getRyotByRyotCode(agreementSummaryBean.getRyotCode());
		RandDAgreementDetails agreementDetails = new RandDAgreementDetails();
		
		String modifyFlag = agreementSummaryBean.getModifyFlag();
		Integer maxId = commonService.getMaxColumnValue("RandDAgreementSummary","agreementseqno","seasonyear",agreementSummaryBean.getSeasonYear());
		
		//Added by sahadeva/Dmurthy 16-11-2016
		String plotNo = agreementDetailsBean.getPlotNumber();
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			List<RandDAgreementDetails> agreementDetails1 = ahFuctionalService.getranddAgreementDetailsBySeasonByPlot(agreementSummaryBean.getSeasonYear(), agreementSummaryBean.getAgreementNumber(),plotNo);
			if(agreementDetails1.get(0).getCanesupplyenddate() != null)
			{
				Date canesupplyenddate = agreementDetails1.get(0).getCanesupplyenddate();
				agreementDetails.setCanesupplyenddate(canesupplyenddate);
			}
			if(agreementDetails1.get(0).getCanesupplystartdate() != null)
			{
				Date canesupplystartdate = agreementDetails1.get(0).getCanesupplystartdate();
				agreementDetails.setCanesupplystartdate(canesupplystartdate);
			}
			if(agreementDetails1.get(0).getEstimatedqty() != null)
			{
				double estimatedqty = agreementDetails1.get(0).getEstimatedqty();
				agreementDetails.setEstimatedqty(estimatedqty);
			}
			if(agreementDetails1.get(0).getFamilygroupavgccsrating() != null)
			{
				double familygroupavgccsrating = agreementDetails1.get(0).getFamilygroupavgccsrating();
				agreementDetails.setFamilygroupavgccsrating(familygroupavgccsrating);
			}
			if(agreementDetails1.get(0).getAverageccsrating() != null)
			{
				double averageccsrating = agreementDetails1.get(0).getAverageccsrating();
				agreementDetails.setAverageccsrating(averageccsrating);
			}
			if(agreementDetails1.get(0).getPermitnumbers() != null)
			{
				String permitnumbers=agreementDetails1.get(0).getPermitnumbers();
				agreementDetails.setPermitnumbers(permitnumbers);
			}
			if(agreementDetails1.get(0).getProgramnumber() != null)
			{
				String programnumber=agreementDetails1.get(0).getProgramnumber();
				agreementDetails.setProgramnumber(programnumber);
			}
			if(agreementDetails1.get(0).getProgressiveqty()!= null)
			{
				double progressiveqty = agreementDetails1.get(0).getProgressiveqty();
				agreementDetails.setProgressiveqty(progressiveqty);
			}
			if(agreementDetails1.get(0).getRank()!= null)
			{
				Integer rank=agreementDetails1.get(0).getRank();
				agreementDetails.setRank(rank);
			}
			if(agreementDetails1.get(0).getSamplecards()!= null)
			{
				String samplecards=agreementDetails1.get(0).getSamplecards();
				agreementDetails.setSamplecards(samplecards);
			}
		}
		
		if("Yes".equalsIgnoreCase(modifyFlag))
		{
			agreementDetails.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
		}
		else
		{
			String season = "";
			String strSeqNo = Integer.toString(maxId);
			
			String AgNo = agreementSummaryBean.getAgreementNumber();
			if(AgNo.contains("/"))
			{
				String agreement[] = AgNo.split("/");
				season = agreement[1];
			}
			
			String strAgreementNo = "T"+strSeqNo+"/"+season;
			agreementDetails.setAgreementnumber(strAgreementNo);

		}
		//added by naidu 15-09-2016
		List circleList=ahFuctionalService.getCircleRecordDetails(agreementSummaryBean.getCircleCode());
		Circle circle = (Circle)circleList.get(0);
	//	Circle circle = (Circle) commonService.getEntityById(agreementDetailsBean.getCircleCode(), Circle.class);
		Mandal mandal = (Mandal) commonService.getEntityById(agreementSummaryBean.getMandalCode(), Mandal.class);

		agreementDetails.setPlotseqno(agreementDetailsBean.getPlotSeqNo());
		//agreementDetails.setAgreementnumber(agreementSummaryBean.getAgreementNumber());
		agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(agreementSummaryBean.getAgreementDate(),Constants.GenericDateFormat.DATE_FORMAT));

		agreementDetails.setCirclecode(agreementSummaryBean.getCircleCode());//
		agreementDetails.setLandtype(agreementDetailsBean.getLandTypeCode());
		
		agreementDetails.setLandvillagecode(agreementDetailsBean.getLandVillageCode());
		
		
		agreementDetails.setPlotnumber(agreementDetailsBean.getPlotNumber());
		agreementDetails.setPlotseqno(agreementDetailsBean.getPlotSeqNo());
		agreementDetails.setExtentsize(agreementDetailsBean.getExtentSize());
		agreementDetails.setAgreedqty(agreementDetailsBean.getAgreedQty());
		agreementDetails.setPlantorratoon(agreementDetailsBean.getPlantorRatoon());

		String cropDate = agreementDetailsBean.getCropDate();
		agreementDetails.setCropdate(DateUtils.getSqlDateFromString(cropDate,Constants.GenericDateFormat.DATE_FORMAT));

		agreementDetails.setRyotcode(agreementSummaryBean.getRyotCode());
		agreementDetails.setRyotname(ryot.get(0).getRyotname());
		agreementDetails.setRelativename(agreementSummaryBean.getRelativeName());
		agreementDetails.setRelation(ryot.get(0).getRelation());
		agreementDetails.setRyotsalutation(agreementSummaryBean.getSalutation());
		agreementDetails.setSuretyryotcode(agreementSummaryBean.getSuretyRyotCode().toString());
		agreementDetails.setSurveynumber(agreementDetailsBean.getSurveyNumber());
		agreementDetails.setMandalcode(agreementSummaryBean.getMandalCode());
		agreementDetails.setVarietycode(agreementDetailsBean.getVarietyCode());
	
		agreementDetails.setVillagecode(ryot.get(0).getVillagecode());
		agreementDetails.setZonecode(agreementSummaryBean.getZoneCode());
		agreementDetails.setSeasonyear(agreementSummaryBean.getSeasonYear());
		agreementDetails.setCanemanagerid(circle.getCanemanagerid());
		agreementDetails.setFieldofficerid(agreementSummaryBean.getFoCode());
		agreementDetails.setFieldassistantid(agreementSummaryBean.getFaCode());
		agreementDetails.setProgressiveqty(0.0);
		agreementDetails.setRyotseqno(ryot.get(0).getRyotcodesequence());
		agreementDetails.setCommercialOrSeed(agreementDetailsBean.getPlotIdentity());//
		agreementDetails.setExtentimagepath(agreementDetailsBean.getExtentImage());
		//Added by DMurty on 06-09-2016  Modified by sahadeva 26-10-2016
		if(agreementDetailsBean.getPlotDistance()!=null)
		{
			agreementDetails.setPlotdistance(agreementDetailsBean.getPlotDistance());
		}
		else
		{
			agreementDetails.setPlotdistance(0.0);
		}
		//end of modification
		String address = agreementDetailsBean.getAddress();
		if ("".equals(address) || "null".equals(address) || (address == null))
		{
			address = "NA";
		}
		agreementDetails.setAddress(address);
		
		//Added by DMurty on 03-12-2016
		int regionCode = 0;
		List<Zone> zone= ahFuctionalService.getRegionCode(agreementSummaryBean.getZoneCode());
		if(zone != null && zone.size()>0)
		{
			regionCode = zone.get(0).getRegioncode();
		}
		agreementDetails.setRegioncode(regionCode);
		
		return agreementDetails;
	}
	
	

	@RequestMapping(value = "/getAllAgreementDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllAgreementDetails(@RequestBody JSONObject jsonData)	throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		String agreementnumber = (String) jsonData.get("agreementnumber");
		String aggrementtype = (String) jsonData.get("aggType");

		if("1".equals(aggrementtype))
		{
			List<AgreementDetailsBean> agreementDetailsBeans = prepareListofAgreementDetailsBeans(ahFuctionalService.getAgreementDetailsBySeason(season, agreementnumber));

			AgreementSummaryBean agreementSummaryBean = prepareBeanForAgreementSummary(ahFuctionalService.getAgreementSummaryBySeason(season, agreementnumber));
			
				org.json.JSONObject agreementSummaryBeanJson = new org.json.JSONObject(agreementSummaryBean);
				org.json.JSONArray agreementDetailsBeansJson = new org.json.JSONArray(agreementDetailsBeans);

				response.put("formData", agreementSummaryBeanJson);
				response.put("gridData", agreementDetailsBeansJson);
		}
		else
		{
			List<AgreementDetailsBean> agreementDetailsBeans = prepareListofRandDAgreementDetailsBeans(ahFuctionalService.getranddAgreementDetailsBySeason(season, agreementnumber));

			AgreementSummaryBean agreementSummaryBean = prepareBeanForRandDAgreementSummary(ahFuctionalService.getranddAgreementSummaryBySeason(season, agreementnumber));
				
				org.json.JSONObject agreementSummaryBeanJson = new org.json.JSONObject(agreementSummaryBean);
				org.json.JSONArray agreementDetailsBeansJson = new org.json.JSONArray(agreementDetailsBeans);

				response.put("formData", agreementSummaryBeanJson);
				response.put("gridData", agreementDetailsBeansJson);
		}
	
		return response.toString();
	}
	public List<AgreementDetailsBean> prepareListofRandDAgreementDetailsBeans(List<RandDAgreementDetails> agreementDetails) 
	{
		List<AgreementDetailsBean> beans = null;
		if (agreementDetails != null && !agreementDetails.isEmpty())
		{
			beans = new ArrayList<AgreementDetailsBean>();
			AgreementDetailsBean bean = null;
			for (RandDAgreementDetails agreementDetail : agreementDetails)
			{
				bean = new AgreementDetailsBean();

				bean.setPlotNumber(agreementDetail.getPlotnumber());
				bean.setVarietyCode(agreementDetail.getVarietycode());
				bean.setPlantorRatoon(agreementDetail.getPlantorratoon());
				Date cropDate = agreementDetail.getCropdate();
				String strcropDate = "";
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				if(cropDate != null)
				{
					strcropDate = df.format(cropDate);
				}

				bean.setCropDate(strcropDate);
				bean.setAgreedQty(agreementDetail.getAgreedqty());
				bean.setExtentSize(agreementDetail.getExtentsize());
				bean.setSurveyNumber(agreementDetail.getSurveynumber());
				bean.setLandVillageCode(agreementDetail.getLandvillagecode());
				bean.setLandTypeCode(agreementDetail.getLandtype());
				bean.setPlotSeqNo(agreementDetail.getPlotseqno());
				bean.setCircleCode(agreementDetail.getCirclecode());
				bean.setPlotIdentity(agreementDetail.getCommercialOrSeed());
				bean.setExtentImage(agreementDetail.getExtentimagepath());
				bean.setSlno(agreementDetail.getSlno());
				//Added by DMurty on 06-09-2016
				bean.setPlotDistance(agreementDetail.getPlotdistance());
				bean.setAddress(agreementDetail.getAddress());
				bean.setFaCode(agreementDetail.getFieldassistantid());
				bean.setFoCode(agreementDetail.getFieldofficerid());

				beans.add(bean);
			}
		}
		return beans;
	}

	public AgreementSummaryBean prepareBeanForRandDAgreementSummary(List<RandDAgreementSummary> agreementSummary)
	{
		AgreementSummaryBean bean = null;
		RandDAgreementSummary agreementSmry = null;
		RandDAgreementDetails agreementDetails = null;
		if (agreementSummary != null && !agreementSummary.isEmpty()) 
		{

			bean = new AgreementSummaryBean();
			if (agreementSummary.size() > 0 && agreementSummary != null)
				agreementSmry = agreementSummary.get(0);

			List<RandDAgreementDetails> agDetails = ahFuctionalService.getranddAgreementByAgreementNumber(agreementSmry.getAgreementnumber());
			if (agDetails.size() > 0 && agDetails != null)
				agreementDetails = agDetails.get(0);
			
			Date agreementDate = agreementSmry.getAgreementdate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String stragreementDate = df.format(agreementDate);
			bean.setAgreementDate(stragreementDate);
			
			bean.setRyotCode(agreementSmry.getRyotcode());
			bean.setCircleCode(agreementSmry.getCirclecode());
			bean.setMandalCode(agreementSmry.getMandalcode());
			bean.setBranchCode(agreementSmry.getBranchcode());
			bean.setAccountNumber(agreementSmry.getAccountnumber());
			bean.setHasFamilyGroup(agreementSmry.getHasfamilygroup());
			bean.setFamilyGroupCode(agreementSmry.getFamilygroupcode());
			bean.setRemarks(agreementSmry.getRemarks());
			bean.setAgreementNumber(agreementSmry.getAgreementnumber());
			bean.setRyotName(agreementSmry.getRyotcode());
			bean.setStatus(agreementSmry.getStatus());
			bean.setRelativeName(agreementDetails.getRelativename());
			bean.setSalutation(agreementDetails.getRyotsalutation());
			bean.setModifyFlag("Yes");
			//Modified by DMurty on 07-06-2016
			bean.setSuretyRyotCode(agreementSmry.getSurityPerson());
			byte a=0;
			bean.setTypeOfAgreement(a);

			bean.setAgreementSeqNo(agreementSmry.getAgreementseqno());
			bean.setSeasonYear(agreementSmry.getSeasonyear());
			
			bean.setSurityPersonName(agreementSmry.getSurityPersonName());
			bean.setSurityPersonaadhaarNumber(agreementSmry.getSurityPersonaadhaarNumber());	
			//added by naidu on 03-04-2017
			List zoneList = commonService.getZonesListByZone(agreementDetails.getZonecode());
			bean.setZoneName((String)((Map<Object,Object>)zoneList.get(0)).get("zone"));
			bean.setZoneCode(agreementDetails.getZonecode());
		}
		return bean;
	}
	
//modified on 24-2-2017
	public List<AgreementDetailsBean> prepareListofAgreementDetailsBeans(List<AgreementDetails> agreementDetails) 
	{
		List<AgreementDetailsBean> beans = null;
		if (agreementDetails != null && !agreementDetails.isEmpty())
		{
			beans = new ArrayList<AgreementDetailsBean>();
			AgreementDetailsBean bean = null;
			for (AgreementDetails agreementDetail : agreementDetails)
			{
				bean = new AgreementDetailsBean();

				bean.setPlotNumber(agreementDetail.getPlotnumber());
				bean.setVarietyCode(agreementDetail.getVarietycode());
				bean.setPlantorRatoon(agreementDetail.getPlantorratoon());
				Date cropDate = agreementDetail.getCropdate();
				String strcropDate = "";
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				if(cropDate != null)
				{
					strcropDate = df.format(cropDate);
				}

				bean.setCropDate(strcropDate);
				bean.setAgreedQty(agreementDetail.getAgreedqty());
				bean.setExtentSize(agreementDetail.getExtentsize());
				bean.setSurveyNumber(agreementDetail.getSurveynumber());
				bean.setLandVillageCode(agreementDetail.getLandvillagecode());
				bean.setLandTypeCode(agreementDetail.getLandtype());
				bean.setPlotSeqNo(agreementDetail.getPlotseqno());
				bean.setCircleCode(agreementDetail.getCirclecode());
				bean.setPlotIdentity(agreementDetail.getCommercialOrSeed());
				bean.setExtentImage(agreementDetail.getExtentimagepath());
				bean.setSlno(agreementDetail.getSlno());
				//Added by DMurty on 06-09-2016
				bean.setPlotDistance(agreementDetail.getPlotdistance());
				bean.setAddress(agreementDetail.getAddress());
				bean.setFaCode(agreementDetail.getFieldassistantid());
				bean.setFoCode(agreementDetail.getFieldofficerid());
				beans.add(bean);
			}
		}
		return beans;
	}

	public AgreementSummaryBean prepareBeanForAgreementSummary(List<AgreementSummary> agreementSummary)
	{
		AgreementSummaryBean bean = null;
		AgreementSummary agreementSmry = null;
		AgreementDetails agreementDetails = null;
		if (agreementSummary != null && !agreementSummary.isEmpty()) 
		{

			bean = new AgreementSummaryBean();
			if (agreementSummary.size() > 0 && agreementSummary != null)
				agreementSmry = agreementSummary.get(0);

			List<AgreementDetails> agDetails = ahFuctionalService.getAgreementByAgreementNumber(agreementSmry.getAgreementnumber());
			if (agDetails.size() > 0 && agDetails != null)
				agreementDetails = agDetails.get(0);
			
			Date agreementDate = agreementSmry.getAgreementdate();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String stragreementDate = df.format(agreementDate);
			bean.setAgreementDate(stragreementDate);
			
			bean.setRyotCode(agreementSmry.getRyotcode());
			bean.setCircleCode(agreementSmry.getCirclecode());
			bean.setMandalCode(agreementSmry.getMandalcode());
			bean.setBranchCode(agreementSmry.getBranchcode());
			bean.setAccountNumber(agreementSmry.getAccountnumber());
			bean.setHasFamilyGroup(agreementSmry.getHasfamilygroup());
			bean.setFamilyGroupCode(agreementSmry.getFamilygroupcode());
			bean.setRemarks(agreementSmry.getRemarks());
			bean.setAgreementNumber(agreementSmry.getAgreementnumber());
			bean.setRyotName(agreementSmry.getRyotcode());
			bean.setStatus(agreementSmry.getStatus());
			bean.setRelativeName(agreementDetails.getRelativename());
			bean.setSalutation(agreementDetails.getRyotsalutation());
			bean.setModifyFlag("Yes");
			//Modified by DMurty on 07-06-2016
			bean.setSuretyRyotCode(agreementSmry.getSurityPerson());
			byte a=1;
			bean.setTypeOfAgreement(a);
			bean.setAgreementSeqNo(agreementSmry.getAgreementseqno());
			bean.setSeasonYear(agreementSmry.getSeasonyear());
			
			bean.setSurityPersonName(agreementSmry.getSurityPersonName());
			bean.setSurityPersonaadhaarNumber(agreementSmry.getSurityPersonaadhaarNumber());
			//added by naidu on 03-04-2017
			List zoneList = commonService.getZonesListByZone(agreementDetails.getZonecode());
			bean.setZoneName((String)((Map<Object,Object>)zoneList.get(0)).get("zone"));
			bean.setZoneCode(agreementDetails.getZonecode());
		}
		return bean;
	}
	
	
	@RequestMapping(value = "/getLoanNumbersForBranch", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getLoanNumbersForBranch(@RequestBody JSONObject jsonData)	throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		Integer branchCode = (Integer) jsonData.get("branchCode");

		List<String> RyotNamesList = new ArrayList<String>();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		try 
		{
			List dropdownList = ahFuctionalService.getLoanNumbersForBranch(season, branchCode);
			if(dropdownList.size()>0 && dropdownList!=null)
			{
				for (int i=0;i<dropdownList.size();i++)
				{
					jsonObj = new JSONObject();
					
					Map TempMap = new HashMap();
					TempMap = (Map) dropdownList.get(i);
					int loannumber = (Integer) TempMap.get("loannumber");
					
					jsonObj.put("loanNumber", loannumber);
					
					jArray.add(jsonObj);
				}
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}


	@RequestMapping(value = "/savePrepareTieUpLoans", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray savePrepareTieUpLoans(@RequestBody JSONObject jsonData)	throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		InetAddress ipAddr = InetAddress.getLocalHost(); 
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try 
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			String agNumber = (String) jsonData.get("agreementNumber");
			PrepareTieupLoanBean prepareTieupLoanBean = new ObjectMapper().readValue(formData.toString(), PrepareTieupLoanBean.class);

			//added by naidu on 13-02-2017
			int batchno=commonService.getBatchForLoanRecommendedBranch(prepareTieupLoanBean.getSeason(),prepareTieupLoanBean.getBranchCode(),prepareTieupLoanBean.getRecommendedDate());
			
			if(batchno<1)
				batchno=commonService.getNewBatchForLoanrecommeddedBranch(prepareTieupLoanBean.getSeason(),prepareTieupLoanBean.getBranchCode());
			
			LoanDetails loanDetails = prepareModelForLoanDetails(prepareTieupLoanBean, agNumber,batchno);
			LoanSummary loanSummary = prepareModelForLoanSummary(prepareTieupLoanBean,batchno);
			
			entityList.add(loanDetails);
			entityList.add(loanSummary);
			
			/*if(prepareTieupLoanBean.getRyotCode()!=null)
			{
				boolean isAuditTrailRequired=auditTrailService.isAuditTrailRequired(prepareTieupLoanBean.getScreenName(),"ok");
				if(isAuditTrailRequired)
					auditTrailService.updateAuditTrailAgSummary(prepareTieupLoanBean.getRyotCode(),loanSummary,prepareTieupLoanBean.getScreenName());		
			}*/
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			
			//Modified by Sahu on 28-06-2016 for loan forwarding statement
			if(isInsertSuccess == true)
			{
				int loanNo = prepareTieupLoanBean.getLoanNumber();
				String strloanno = Integer.toString(loanNo);

				String season = prepareTieupLoanBean.getSeason();
				int branchCode = prepareTieupLoanBean.getBranchCode();
				String rptformat = "pdf";
				String ryotCode = prepareTieupLoanBean.getRyotCode();
				
				jsonObj.put("season", "string:"+season);
				jsonObj.put("branchCode", "number:"+branchCode);
				jsonObj.put("loanRyotcode", "string:"+strloanno+"-"+ryotCode);
				jsonObj.put("status", "pdf");
				jsonObj.put("issuccess", "true");
				jsonObj.put("serverip", ipAddr.getHostAddress());
				jArray.add(jsonObj);
			}
			else
			{
				jsonObj.put("issuccess", "false");
				jArray.add(jsonObj);
			}
			return jArray;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return jArray;
		}
	}

	private LoanDetails prepareModelForLoanDetails(	PrepareTieupLoanBean prepareTieupLoanBean, String agNumber,int batchno)
	{
		LoanDetails loanDetails = new LoanDetails();
		String[] aNumber = agNumber.split(",");
		//agNumber.toString().replace(",", "");
		Integer circleCode = (Integer) commonService.getCircleCodeFromAgreementDetails(aNumber[0]);
		List<Circle> circlels=ahFuctionalService.getCircleRecordDetails(circleCode);
		Circle circle = (Circle) circlels.get(0);
		//Modified by DMurty on 02-06-2016 for migration for loan data
		CompositePrKeyForLoans compositePrKeyForLoans = new CompositePrKeyForLoans();
		compositePrKeyForLoans.setBranchcode(prepareTieupLoanBean.getBranchCode());
		compositePrKeyForLoans.setLoannumber(prepareTieupLoanBean.getLoanNumber());
		//Modified by DMurty on 21-02-2017
		compositePrKeyForLoans.setSeason(prepareTieupLoanBean.getSeason());
		//loanDetails.setSeason(prepareTieupLoanBean.getSeason());
		loanDetails.setCompositePrKeyForLoans(compositePrKeyForLoans);
		loanDetails.setAgreementnumber(agNumber);
		//loanDetails.setBranchcode(prepareTieupLoanBean.getBranchCode());
		//loanDetails.setLoannumber(prepareTieupLoanBean.getLoanNumber());
		loanDetails.setSuveynumber(prepareTieupLoanBean.getSurveyNumber());
		
		//Modified by DMurty on 01-06-2016
		//loanDetails.setRecommendeddate(prepareTieupLoanBean.getRecommendedDate());
		String strdate = prepareTieupLoanBean.getRecommendedDate();
		loanDetails.setRecommendeddate(DateUtils.getSqlDateFromString(strdate,Constants.GenericDateFormat.DATE_FORMAT));
		
		loanDetails.setRatoonextent(prepareTieupLoanBean.getRatoonExtent());
		loanDetails.setRyotcode(prepareTieupLoanBean.getRyotCode());
		loanDetails.setPlantextent(prepareTieupLoanBean.getPlantExtent());
		loanDetails.setPrinciple(prepareTieupLoanBean.getRecommendedLoan());
		
		loanDetails.setLoanstatus(0);
		loanDetails.setPaidamount(0.00);
		
		
		loanDetails.setTotalamount(prepareTieupLoanBean.getRecommendedLoan());
		loanDetails.setAddedloan(prepareTieupLoanBean.getAddedLoan());

		loanDetails.setCanemanagerid(circle.getCanemanagerid());
		loanDetails.setFieldofficerid(circle.getFieldofficerid());
		loanDetails.setFieldassistantid(circle.getFieldassistantid());
		//Added by DMurty on 08-06-2016 For Update Loan
		loanDetails.setTransactioncode(0);
		//Added by DMurty on 17-11-2016
		loanDetails.setLoginid(loginController.getLoggedInUserName());
		//added by naidu on 13-02-2017
		loanDetails.setBatchno(batchno);
		return loanDetails;
			
	}

	private LoanSummary prepareModelForLoanSummary(PrepareTieupLoanBean prepareTieupLoanBean,int batchno)
	{
		LoanSummary loanSummary = new LoanSummary();
		loanSummary.setSeason(prepareTieupLoanBean.getSeason());
		loanSummary.setRyotcode(prepareTieupLoanBean.getRyotCode());
		loanSummary.setPrinciple(prepareTieupLoanBean.getRecommendedLoan());
		// Double
		// totAmt=prepareTieupLoanBean.getRecommendedLoan()+(InterestRate*1/100);
		loanSummary.setLoanstatus(0);
		loanSummary.setPaidamount(0.00);
		loanSummary.setTotalamount(prepareTieupLoanBean.getRecommendedLoan());
		loanSummary.setLoannumber(prepareTieupLoanBean.getLoanNumber());
		loanSummary.setBranchcode(prepareTieupLoanBean.getBranchCode());
		//added by naidu on 13-02-2017
		loanSummary.setBatchno(batchno);
		
		return loanSummary;
	}

	@RequestMapping(value = "/getLoanDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getLoanDetails(@RequestBody JSONArray jsonData) throws Exception {
		
		String aNumber = jsonData.toString().replace("[", "").replace("]", "").replace("\"", "'");
		List ll = ahFuctionalService.getLoanDetailsByAgreement(aNumber);
		return ll;
	}

	@RequestMapping(value = "/getAllLoanDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List<PrepareTieupLoanBean> getAllLoanDetails(@RequestBody JSONObject jsonData) throws Exception
	{
		String season = (String) jsonData.get("season");
		int loanno = (Integer) jsonData.get("loannumber");
		//int nloanno = Integer.parseInt(loanno);
		int branchCode = (Integer) jsonData.get("branchCode");

		List<PrepareTieupLoanBean> prepareTieupLoanBeans = prepareListofLoanDetailsBeans(ahFuctionalService.getLoanDetailsByRyotCode(season, loanno,branchCode));
		return prepareTieupLoanBeans;
	}

	private List<PrepareTieupLoanBean> prepareListofLoanDetailsBeans(List<LoanDetails> loanDetails) 
	{
		List<PrepareTieupLoanBean> beans = null;
		if (loanDetails != null && !loanDetails.isEmpty()) 
		{
			beans = new ArrayList<PrepareTieupLoanBean>();
			PrepareTieupLoanBean bean = null;
			for (LoanDetails loanDetail : loanDetails) 
			{
				bean = new PrepareTieupLoanBean();
				bean.setAgreementNumber(loanDetail.getAgreementnumber());
				
				String surveyNo = "";
				double exent = 0.0;
				double plantExent = 0.0;
				double ratoonExtent = 0.0;
				
				/*if(loanDetail.getAgreementnumber().contains(","))
				{
					String agNo = loanDetail.getAgreementnumber();
					agNo = agNo.substring(0,agNo.length()-1);
					List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByCode(agNo,loanDetail.getSeason());
					exent = agreementDetails.get(0).getExtentsize();
					String isPlantOrRatoon = agreementDetails.get(0).getPlantorratoon();
					if("1".equals(isPlantOrRatoon))
					{
						plantExent = exent;
					}
					else if("2".equals(isPlantOrRatoon))
					{
						ratoonExtent = exent;
					}
				}*/
				
				
				bean.setPlantExtent(loanDetail.getPlantextent());
				bean.setRatoonExtent(loanDetail.getRatoonextent());
				
				bean.setRyotCode(loanDetail.getRyotcode());
				bean.setSurveyNumber(loanDetail.getSuveynumber());
				//Bug Id : 05245
				bean.setRecommendedDate(DateUtils.formatDate(loanDetail.getRecommendeddate(),Constants.GenericDateFormat.DATE_FORMAT));
				bean.setRecommendedLoan(loanDetail.getPrinciple());
				//Modified by DMurty on 21-02-2017
				bean.setSeason(loanDetail.getCompositePrKeyForLoans().getSeason());
				bean.setBranchCode(loanDetail.getCompositePrKeyForLoans().getBranchcode());
				bean.setLoanNumber(loanDetail.getCompositePrKeyForLoans().getLoannumber());
				beans.add(bean);
			}
		}
		return beans;
	}

	// ------------------------------- Update Estimated Quantity
	// ------------------------------

	@RequestMapping(value = "/SaveEstimateQty", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveEstimateQty(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		try 
		{
			JSONArray totalData= null;
			String flag = (String) jsonData.get("flag");
			if("Single".equalsIgnoreCase(flag))
			{
				JSONObject totalData1=(JSONObject) jsonData.get("gridData");
				double dqty = 0;
				String ryotCode=totalData1.getString("ryotCode");
				String agreementNo=totalData1.getString("agreementNumber");
				String surveyNo=totalData1.getString("surveyNumber");
				String EstQty=totalData1.getString("estimateQty"); 
				String plotNo = totalData1.getString("plotNumber");

				logger.info("========ryotCode=========="+ryotCode);
				logger.info("========agreementNo=========="+agreementNo);
				logger.info("========surveyNo=========="+surveyNo);
				logger.info("========EstQty=========="+EstQty);
				logger.info("========plotNo=========="+plotNo);

				if ("".equals(EstQty) || "null".equals(EstQty) || (EstQty == null))
				{
			    	dqty = 0;
				}
				else
				{
					dqty = Double.parseDouble(EstQty);
				}	
				String strQry = "UPDATE AgreementDetails set estimatedqty= "+EstQty+" WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+agreementNo+"' and surveynumber='"+surveyNo+"' and plotnumber='"+plotNo+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
			}
			else
			{
				totalData=(JSONArray) jsonData.get("gridData");	
				for(int i=0;i<totalData.size();i++)
				{
					UpdateEstimateQtyBean updateEstimateQtyBean = new ObjectMapper().readValue(totalData.get(i).toString(), UpdateEstimateQtyBean.class);
					
					double dqty = 0;
					String ryotCode = updateEstimateQtyBean.getRyotCode();
					String agreementNo = updateEstimateQtyBean.getAgreementNumber();
					String surveyNo = updateEstimateQtyBean.getSurveyNumber();
					double EstQty = updateEstimateQtyBean.getEstimateQty();
					String plotNo = updateEstimateQtyBean.getPlotNumber();
					
					logger.info("========ryotCode==========" + ryotCode);
					logger.info("========agreementNo==========" + agreementNo);
					logger.info("========surveyNo==========" + surveyNo);
					logger.info("========EstQty==========" + EstQty);
					
					String strQry = "UPDATE AgreementDetails set estimatedqty= "+EstQty+" WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+agreementNo+"' and surveynumber='"+surveyNo+"' and plotnumber='"+plotNo+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
					//ahFuctionalService.updateEstQty(ryotCode,agreementNo,surveyNo,dqty,plotNo);	
				}
			}
		
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	@RequestMapping(value = "/getAllEstimatedQty", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody List getAllEstimatedQty(@RequestBody JSONObject jsonData) throws Exception 
	{
		UpdateEstimateQtyBean updateEstimateQtyBean = null;
		String season = (String) jsonData.get("season");
		Integer circle = (Integer) jsonData.get("circle");

		List<UpdateEstimateQtyBean> updateEstimateQtyBeans = prepareListofBeanForgetAllEstimatedQty(ahFuctionalService.listAggrements(season, circle),season);

		return updateEstimateQtyBeans;
	}

	private List<UpdateEstimateQtyBean> prepareListofBeanForgetAllEstimatedQty(List<AgreementDetails> AgreementDetailss,String season)
	{
		List<UpdateEstimateQtyBean> beans = null;
		if (AgreementDetailss != null && !AgreementDetailss.isEmpty()) 
		{
			beans = new ArrayList<UpdateEstimateQtyBean>();
			UpdateEstimateQtyBean bean = null;
			for (AgreementDetails agreementDetails : AgreementDetailss)
			{
				bean = new UpdateEstimateQtyBean();
				bean.setCircleCode(agreementDetails.getCirclecode());
				bean.setZoneCode(agreementDetails.getZonecode());
				bean.setRyotCode(agreementDetails.getRyotcode());
				bean.setRyotName(agreementDetails.getRyotname());
				bean.setFghName(agreementDetails.getRelativename());
				bean.setAgreementNumber(agreementDetails.getAgreementnumber());
				bean.setAgreementDate(DateUtils.formatDate(agreementDetails.getAgreementdate(),Constants.GenericDateFormat.DATE_FORMAT).toString());
				bean.setPlotNumber(agreementDetails.getPlotnumber());
				bean.setSurveyNumber(agreementDetails.getSurveynumber());
				bean.setExtentArea(agreementDetails.getExtentsize());
				bean.setAgreedQty(agreementDetails.getAgreedqty());
				
				//bean.setSlno(agreementDetails.getSlno());
				
				double dqty = 0.0;
				if (agreementDetails.getEstimatedqty() == null) 
				{
					dqty = 0.0;
				} 
				else 
				{
					dqty = agreementDetails.getEstimatedqty();
				}
				bean.setEstimateQty(dqty);

				String agreementNum = agreementDetails.getAgreementnumber();
				List<AgreementSummary> agrmntSmry = ahFuctionalService.getAgreementSmryByAgreementNo(season,agreementNum);
				Byte status = agrmntSmry.get(0).getStatus();
				if(status == 0)
				{
					beans.add(bean);
				}
			}
		}
		return beans;
	}

	@RequestMapping(value = "/searchRyotLoans", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	List<SearchRyotLoanBean> getAllRyotLoansByRyotCode(	@RequestBody JSONObject jsonData) throws Exception
	{
		String ryotcode = (String) jsonData.get("ryotcode");
		String season = (String) jsonData.get("season");
		ryotcode=ryotcode.replaceAll("\"", "");
		season=season.replaceAll("\"", "");

		List<SearchRyotLoanBean> searchRyotLoanBeans = prepareListofSearchRyotLoanBeans(ahFuctionalService.getAllRyotLoansByRyotCode(ryotcode, season));
		return searchRyotLoanBeans;
	}

	private List<SearchRyotLoanBean> prepareListofSearchRyotLoanBeans(List<LoanDetails> loanDetails) 
	{
		List<SearchRyotLoanBean> beans = null;
		if (loanDetails != null && !loanDetails.isEmpty()) 
		{
			beans = new ArrayList<SearchRyotLoanBean>();
			SearchRyotLoanBean bean = null;
			for (LoanDetails loanDetail : loanDetails)
			{
				bean = new SearchRyotLoanBean();
				FieldOfficer fieldOfficer = (FieldOfficer) commonService.getEntityById(loanDetail.getFieldofficerid(),FieldOfficer.class);
				//bean.setLoanNumber(loanDetail.getLoannumber());
				bean.setReferenceNumber(loanDetail.getReferencenumber());
				bean.setFieldOfficer(fieldOfficer.getFieldOfficer());
				bean.setDisbursedAmount(loanDetail.getDisbursedamount());
				//Modified by DMurty on 05-11-2016
				Date disburseDate = loanDetail.getDisburseddate();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strdisburseDate = df.format(disburseDate);
				bean.setDisbursedDate(strdisburseDate);
				
				bean.setRecommendedDate(loanDetail.getRecommendeddate().toString());
				bean.setLoanNumber(loanDetail.getCompositePrKeyForLoans().getLoannumber());
				
				beans.add(bean);
			}
		}
		return beans;

	}

	// ---------Generate Programs-----------------------------------//
	@RequestMapping(value = "/getValidatreProgramNumForSeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getValidatreProgramNumForSeason(@RequestBody JSONObject jsonData) throws Exception
	{
		String season = (String) jsonData.get("season");
		int programNo = (Integer) jsonData.get("programnumber");

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;

		List<ProgramSummary> programSummary = ahFuctionalService.getValidatreProgramNumForSeason(season, programNo);
		logger.info("========programSummary==========" + programSummary);
		jsonObj = new JSONObject();
		try 
		{
			if (programSummary != null && !programSummary.isEmpty()) 
			{
				jsonObj.put("flag", "true");
				jArray.add(jsonObj);
			} 
			else 
			{
				jsonObj.put("flag", "false");
				jArray.add(jsonObj);
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}
	
	@RequestMapping(value = "/getMaxProgramNoBySeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getMaxProgramNoBySeason(@RequestBody JSONObject jsonData) throws Exception
	{
		String season = (String) jsonData.get("season");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		try 
		{
			int maxProgramNo = ahFuctionalService.getProgramNoBySeason(season);
			logger.info("========maxProgramNo==========" + maxProgramNo);
			
			jsonObj.put("programNumber", maxProgramNo);
			jArray.add(jsonObj);
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}
	//Added by sahadeva on 15-07-2016
	@RequestMapping(value = "/getAllAgreementDetailsForGenerateProgramsByProgramNo", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllAgreementDetailsForGenerateProgramsByProgramNo(	@RequestBody JSONObject jsonData) throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();

		String season = (String) jsonData.get("season");
		Integer programNumber = (Integer) jsonData.get("programNumber");
		
		List<ProgramDetailsBean> programDetailsBeans = null;
		List<ProgramSummaryBean> programSummaryBeans = null;
		Map fm=new HashMap();
		programDetailsBeans = prepareListofGenerateProgramFromAgreementDetails(ahFuctionalService.getAllAgreementDetailsForGenerateProgramsByProgramNo(season, programNumber),"Yes");
		programSummaryBeans  = prepareListForProgramSummaryBean(ahFuctionalService.getDetailsFromProgramSummary(season,programNumber));
		if(programSummaryBeans!=null && programSummaryBeans.size()>0)
		{
		fm.put("season",programSummaryBeans.get(0).getSeason());
		fm.put("programnumber",programNumber);
		fm.put("modifyFlag",programSummaryBeans.get(0).getModifyFlag());
		
		}
		org.json.JSONArray programDetailsBeansJson = new org.json.JSONArray(programDetailsBeans);
		org.json.JSONArray programSummaryBeansJson = new org.json.JSONArray(programSummaryBeans);
		org.json.JSONObject programSummaryMapJson = new org.json.JSONObject(fm);
		response.put("gridData", programDetailsBeansJson);
		response.put("formData", programSummaryMapJson);
		response.put("middleData", programSummaryBeansJson);
		
		logger.info("------AgreementDetails-----"+response.toString());

		return response.toString();
	}
	
	@RequestMapping(value = "/getAllAgreementDetailsForGeneratePrograms", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	String getAllAgreementDetailsForGeneratePrograms(	@RequestBody JSONObject jsonData) throws Exception 
	{
		String dateFrom = null;
		String dateTo = null;
		String strdateFrom = null;
		String strdateTo = null;
		org.json.JSONObject response = new org.json.JSONObject();
	
		JSONObject formData = (JSONObject) jsonData.get("formData");
		JSONArray gridData = (JSONArray) jsonData.get("middleData");
		String season = (String) formData.get("season");
		Integer programNumber = (Integer) formData.get("programnumber");
		String modifyFlag = (String) formData.get("modifyFlag");
		List gridList=new ArrayList();
		for(int i=0;i<gridData.size();i++)
		{
	 		JSONObject jasonObj = (JSONObject) gridData.get(i);
	 		JSONArray vCode=(JSONArray)jasonObj.get("varietyOfCane");
	 		
	 		String vcCode=vCode.toString();
	 		String varietyCode=vcCode.substring(1, vcCode.length()-1);
			String month = (String) jasonObj.get("monthId");
			Integer year = (Integer) jasonObj.get("yearOfPlanting");
			Integer period = (Integer) jasonObj.get("periodId");
			int ptorrt =Integer.parseInt((String)jasonObj.get("typeOfExtent"));
			if(programNumber == null)
			{
				programNumber = 0;
			}
	 		if(period != null)
			{
				if (period == 1) 
				{
					dateFrom = "01" + "-" + month + "-" + year;
					dateTo = "15" + "-" + month + "-" + year;
					
					strdateFrom = year + "-" + month + "-" + "01";
					strdateTo = year + "-" + month + "-" + "15";
				} 
				else if (period == 2) 
				{
					dateFrom = "16" + "-" + month + "-" + year;
					strdateFrom = year + "-" + month + "-" + "16";
					
					if(month.equals("04") || month.equals("06") || month.equals("09") || month.equals("11"))
					{
						dateTo = "30" + "-" + month + "-" + year;
						strdateTo = year + "-" + month + "-" + "30";
					}
					else if(month.equals("02"))
					{
						if((year%4)==0)
						{
							dateTo = "29" + "-" + month + "-" + year;
							strdateTo = year + "-" + month + "-" + "29";
						}
						else
						{
							dateTo = "28" + "-" + month + "-" + year;
							strdateTo = year + "-" + month + "-" + "28";
						}
					}
					else
					{
						dateTo = "31" + "-" + month + "-" + year;	
						strdateTo = year + "-" + month + "-" + "31";
					}	
				}
				else 
				{
					dateFrom = "01" + "-" + month + "-" + year;
					strdateFrom = year + "-" + month + "-" + "01";
					
					if(month.equals("04") || month.equals("06") || month.equals("09") || month.equals("11"))
					{
					
					dateTo = "30" + "-" + month + "-" + year;
					strdateTo = year + "-" + month + "-" + "30";
					}
					else if(month.equals("02"))
					{
						if((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
						{
							dateTo = "29" + "-" + month + "-" + year;
							strdateTo = year + "-" + month + "-" + "29";
						}
						else
						{
							dateTo = "28" + "-" + month + "-" + year;
							strdateTo = year + "-" + month + "-" + "28";
						}
					}
					else
					{
						dateTo = "31" + "-" + month + "-" + year;
						strdateTo = year + "-" + month + "-" + "31";
					}
				}
			}
			List<ProgramDetailsBean> programDetailsBeans = null;
			programDetailsBeans = prepareListofGenerateProgramFromAgreementDetails(ahFuctionalService.getAgreementDetailsForGenerateProgram(season, varietyCode,strdateFrom, strdateTo, String.valueOf(programNumber),modifyFlag,ptorrt),modifyFlag);
			
			if(programDetailsBeans!=null)
			gridList.addAll(programDetailsBeans);
			
			}
			org.json.JSONArray programDetailsBeansJson = new org.json.JSONArray(gridList);
			response.put("gridData", programDetailsBeansJson);
			logger.info("------AgreementDetails-----"+response.toString());
			return response.toString();
	}
	private List<ProgramSummaryBean> prepareListForProgramSummaryBean(List programSummarys)
	{
		//ps.season,ps.monthid,ps.periodid,ps.programnumber,
		//ps.totalextent,ps.totalplant,ps.yearofplanting,ps.totalratoon,ps.typeofextent,pv.varietycode,pv.id
		List<ProgramSummaryBean> beans = null;
		if (programSummarys != null && !programSummarys.isEmpty())
		{
			beans = new ArrayList<ProgramSummaryBean>();
			ProgramSummaryBean bean = null;
			Map mp=null;
			String vcodes=null;
			for (int i=0;i<programSummarys.size();i++)
			{
				bean = new ProgramSummaryBean();
				mp=(HashMap)programSummarys.get(i);
				vcodes=(String)mp.get("varietycode");
				vcodes = "["+vcodes+"]";//+",";
				bean.setVarietyOfCane(vcodes);
				bean.setMonthId((Integer)mp.get("monthid"));
				bean.setYearOfPlanting((Integer)mp.get("yearofplanting"));
				bean.setPeriodId((Integer)mp.get("periodid"));
				bean.setTypeOfExtent((String)mp.get("typeofextent"));
				bean.setModifyFlag("Yes");
				//Added by DMurty on 13-07-2016
				bean.setSeason((String)mp.get("season"));
				bean.setProgramNumber((Integer)mp.get("programnumber"));
				beans.add(bean);
			}
		}
		return beans;
	}
	

	private List<ProgramDetailsBean> prepareListofGenerateProgramFromAgreementDetails(List<AgreementDetails> agreementDetails,String modifyFlag)
	{
		List<ProgramDetailsBean> beans = null;
		if (agreementDetails != null && !agreementDetails.isEmpty())
		{
			beans = new ArrayList<ProgramDetailsBean>();
			ProgramDetailsBean bean = null;
			for (AgreementDetails agreementDetail : agreementDetails)
			{
				bean = new ProgramDetailsBean();
			
				bean.setRyotName(agreementDetail.getRyotname());
				bean.setRyotCode(agreementDetail.getRyotcode());
				bean.setPlotNo(agreementDetail.getPlotnumber());
				bean.setAgreementNo(agreementDetail.getAgreementnumber());
				
				String plantOrRatoon = agreementDetail.getPlantorratoon();
				if (plantOrRatoon.equalsIgnoreCase("1"))
				{
					bean.setPlantAcres(agreementDetail.getExtentsize());
					bean.setRatoonAcres(0.00);
				}
				else
				{
					bean.setRatoonAcres(agreementDetail.getExtentsize());
					bean.setPlantAcres(0.00);
				}
				if(agreementDetail.getProgramnumber() != null)
				{
					bean.setProgramNumber(Integer.parseInt(agreementDetail.getProgramnumber()));
				}
		
				String agreementNum = agreementDetail.getAgreementnumber();
				List<AgreementSummary> agrmntSmry = ahFuctionalService.getAgreementSmryByAgreementNo(agreementDetail.getSeasonyear(),agreementNum);
				Byte status = agrmntSmry.get(0).getStatus();
				if(status==0)
				{
					beans.add(bean);
				}
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveGenaratePrograms", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveGenaratePrograms(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String strQry = null;
		Object Qryobj = null;
		try 
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject formData = (JSONObject) jsonData.get("formData");
			JSONArray middleData=(JSONArray)jsonData.get("middleData");

			 //ProgramSummaryBean programSummaryBean = new ObjectMapper().readValue(formData.toString(), ProgramSummaryBean.class);
			 HashMap fmd=new HashMap();
 
			 Integer programNumber=(Integer)formData.get("programNumber");
			 String season=(String)formData.get("season");
			 String modifyFlag=(String)formData.get("modifyFlag");	
			 Double totalExtent=((Integer)formData.get("totalExtent")).doubleValue();
			 Double totalPlant=((Integer)formData.get("totalPlant")).doubleValue();
			 Double totalRatoon=((Integer)formData.get("totalRatoon")).doubleValue();
			 fmd.put("programNumber",programNumber);
			 fmd.put("season",season);
			 fmd.put("totalPlant",totalPlant);
			 fmd.put("totalRatoon",totalRatoon);
			 fmd.put("modifyFlag",modifyFlag);
			for(int j=0;j<middleData.size();j++)
			{
				JSONObject obj=(JSONObject)middleData.get(j);
				Integer id=(Integer)obj.get("id");
				Integer periodId=(Integer)obj.get("periodId");
				Integer monthId=Integer.parseInt((String)obj.get("monthId"));
				Integer yearOfPlanting=(Integer)obj.get("yearOfPlanting");
				String typeOfExtent=(String)obj.get("typeOfExtent");
				String varietyOfCane=((JSONArray)obj.get("varietyOfCane")).toString();
				fmd.put("id", id);
				fmd.put("periodId",periodId);
				fmd.put("monthId",monthId);
				fmd.put("yearOfPlanting",yearOfPlanting);
				fmd.put("typeOfExtent",typeOfExtent);
				fmd.put("totalExtent",totalExtent);
				
			
				prepareModelForProgramSummary(fmd);
				prepareModelForProgramVariety(programNumber,season,varietyOfCane,modifyFlag,id);
				
			}
			for (int i = 0; i < gridData.size(); i++)
			{
				//gridData":["{\"ryotName\":\"NANIPALLI VEERABABU\",\"ryotCode\":\"10030461\",\"plantAcres\":2.6,
				//\"plotNo\":\"1\",\"ratoonAcres\":0,\"agreementNo\":\"193/16-17\",\"programNumber\":2}",
				JSONObject jasonObj = (JSONObject) gridData.get(i);
				String ryotCode = jasonObj.getString("ryotCode");
				String agreementNo = jasonObj.getString("agreementNo");
				Integer plotNo = jasonObj.getInt("plotNo");
				Integer program = jasonObj.getInt("programNumber");
				String strplotNo = Integer.toString(plotNo);
		
				if ("".equals(program) || "null".equals(program) || (program == null)) 
				{
					program = 0;
				}
				strQry = "UPDATE AgreementDetails set programnumber= "+program+" WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+agreementNo+"' and plotnumber='"+strplotNo+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				//ahFuctionalService.updateProgramNumberInAgreementDetails(ryotCode, agreementNo, plotNo, String.valueOf(program));
				}
					entityList=null;
					//ahFuctionalService.addProgramSummary(programSummary);
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
					return isInsertSuccess;
			} 
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
		}

	private void prepareModelForProgramSummary(HashMap programSummaryBean) 
	{
		Integer maxId = commonService.getMaxID("ProgramSummary");
		ProgramSummary programSummary = new ProgramSummary();
		String modifyFlag=(String)programSummaryBean.get("modifyFlag");
		if(modifyFlag.equalsIgnoreCase("Yes"))
		{
			programSummary.setId((Integer)programSummaryBean.get("periodId"));
		}
		else
		{
			programSummary.setId(maxId);
		}
		
		// programSummary.setProgramnumber(programSummaryBean.getProgramNumber().toString());
		programSummary.setSeason((String)programSummaryBean.get("season"));
		programSummary.setProgramnumber((Integer)programSummaryBean.get("programNumber"));
		programSummary.setPeriodid((Integer)programSummaryBean.get("periodId"));
		programSummary.setMonthid((Integer)programSummaryBean.get("monthId"));
		programSummary.setYearofplanting((Integer)programSummaryBean.get("yearOfPlanting"));
		programSummary.setTypeofextent((String)programSummaryBean.get("typeOfExtent"));
		programSummary.setVarietyofcane(0);
		programSummary.setTotalextent((Double)programSummaryBean.get("totalExtent"));
		programSummary.setTotalplant((Double)programSummaryBean.get("totalPlant"));
		programSummary.setTotalratoon((Double)programSummaryBean.get("totalRatoon"));
		
		//Modified by DMurty on 05-10-2016
		List ProgramList = new ArrayList();
		ProgramList = ahFuctionalService.getProgramDetailsByNo((Integer)programSummaryBean.get("programNumber"),maxId);
		
		ahFuctionalService.addProgramSummary(programSummary,ProgramList);
	}
	private void prepareModelForProgramVariety(Integer programNumber,String season,String varietycodes,String modifyFlag,Integer id) 
	{
		Integer maxId = commonService.getMaxID("ProgramVariety");
		ProgramVariety programVariety = new ProgramVariety();
		programVariety.setSeason(season);
		programVariety.setProgramnumber(programNumber);
		programVariety.setVarietycode(varietycodes.substring(1, varietycodes.length()-1));
		if(modifyFlag.equalsIgnoreCase("Yes"))
		{
			programVariety.setId(id);
		}
		else
		{
		programVariety.setId(maxId);
		}
		ahFuctionalService.addProgramVariety(programVariety);
	
	}
	// ---------Generate Programs--------------------------------------------------------------//

	// ----------------------------------------------ProgramDetails---------------------------------------------------//
	@RequestMapping(value = "/getAllProgramDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	String getAllProgramDetails(@RequestBody JSONObject jsonData)		throws Exception
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		Integer programnumber = (Integer) jsonData.get("programnumber");
		List<AgreementDetailsBean> agreementDetailsBeans = prepareListofAgreementDetailsBeans(ahFuctionalService.getAgreementDetailsForProgramDetails(season, String.valueOf(programnumber)));

		List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsForProgramDetails(season, String.valueOf(programnumber));
		ProgramSummaryBean programSummaryBean = prepareBeanForProgramSummary(ahFuctionalService.getGenerateProgramDetails(season, programnumber));

		org.json.JSONObject agreementSummaryBeanJson = new org.json.JSONObject(	programSummaryBean);
		org.json.JSONArray agreementDetailsBeansJson = new org.json.JSONArray(agreementDetails);

		response.put("formData", agreementSummaryBeanJson);
		response.put("gridData", agreementDetailsBeansJson);
		return response.toString();
	}

	private ProgramSummaryBean prepareBeanForProgramSummary(List<ProgramSummary> ProgramSummaries) 
	{
		ProgramSummaryBean bean = null;
		if (ProgramSummaries != null && !ProgramSummaries.isEmpty()) 
		{
			ProgramSummary programSummary = (ProgramSummary) ProgramSummaries.get(0);
			bean = new ProgramSummaryBean();
			bean.setMonthId(programSummary.getMonthid());
			bean.setPeriodId(programSummary.getPeriodid());
			//by naidu
			List<ProgramVariety> vcode=ahFuctionalService.getVarietyFromProgramVariety(programSummary.getProgramnumber(),programSummary.getSeason());
			String varietycodes="";
			if(vcode.size()>0 && vcode!=null)
			{
			varietycodes=((ProgramVariety)vcode.get(0)).getVarietycode();
			}
			bean.setVarietyOfCane(varietycodes);
			bean.setTypeOfExtent(programSummary.getTypeofextent());
			bean.setYearOfPlanting(programSummary.getYearofplanting());
		}
		return bean;
	}
	
	//Added by DMurty on 10-11-2016
	public Map returnAccountSummaryModel(Map accountMap)
	{
		AccountSummary accountSummary = new AccountSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		String accCode = (String) accountMap.get("ACCOUNT_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSummary.setRunningbalance(runbal);
		accountSummary.setBalancetype(strtype);
		accountSummary.setAccountcode(accCode);
		accountSummary.setSeason(ses);
		tempMap.put(key,accountSummary);
		
		return tempMap;
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountGroupSummaryModel(Map accountMap)
	{
		AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountGroupSummary.setAccountgroupcode(accountGroupCode);
		accountGroupSummary.setRunningbalance(runbal);
		accountGroupSummary.setBalancetype(strtype);
		accountGroupSummary.setSeason(ses);
		tempMap.put(key,accountGroupSummary);
		
		return tempMap;
	}
	//Added by DMurty on 10-11-2016
	public Map returnAccountSubGroupSummaryModel(Map accountMap)
	{
		AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
		double runbal = (Double) accountMap.get("RUN_BAL");
		double Amt = (Double) accountMap.get("AMT");
		String baltype = (String) accountMap.get("BAL_TYPE");
		String ses = (String) accountMap.get("SEASON");
		String TransactionType = (String) accountMap.get("TRANSACTION_TYPE");
		int accountGroupCode = (Integer) accountMap.get("ACCOUNT_GROUP_CODE");
		int accountsubgroupcode = (Integer) accountMap.get("ACCOUNT_SUB_GROUP_CODE");
		String key = (String) accountMap.get("KEY");

		String strtype = "";
		Map tempMap = new HashMap();
		
		if(TransactionType.equalsIgnoreCase(baltype))
		{
			runbal = runbal+Amt;
			strtype = baltype;
		}
		else
		{
			if(Amt>runbal)
			{
				runbal = Amt-runbal;
				strtype = TransactionType;
			}
			else if(runbal>Amt)
			{
				runbal = runbal-Amt;
				strtype = baltype;
			}
			else
			{
				runbal = 0.0;
				strtype = TransactionType;
			}
		}
		accountSubGroupSummary.setAccountsubgroupcode(accountsubgroupcode);
		accountSubGroupSummary.setAccountgroupcode(accountGroupCode);
		accountSubGroupSummary.setRunningbalance(runbal);
		accountSubGroupSummary.setBalancetype(strtype);
		accountSubGroupSummary.setSeason(ses);
		tempMap.put(key,accountSubGroupSummary);
		
		return tempMap;
	}
	
	// ----------------------------------------------End of ProgramDetails---------------------------------------------------//

	@RequestMapping(value = "/saveRyotAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	boolean saveRyotAdvances(@RequestBody JSONObject jsonData) throws Exception 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		List AccountsList = new ArrayList();
		
		String strQry = null;
		Object Qryobj = null;
		
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();
		
		try 
		{
			logger.info("========saveRyotAdvances()==========" );

			JSONObject formData = (JSONObject) jsonData.get("formData");
			logger.info("========formData==========" + formData);

			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			logger.info("========gridData==========" + gridData);

			RyotAdvanceBean ryotAdvanceBean = new ObjectMapper().readValue(formData.toString(), RyotAdvanceBean.class);

			String flag = ryotAdvanceBean.getModifyFlag();
			String season = ryotAdvanceBean.getSeason();
			String advDate = ryotAdvanceBean.getAdvanceDate();

			int TransactionCode = 0;
			if ("Yes".equalsIgnoreCase(flag)) 
			{
				TransactionCode = ryotAdvanceBean.getTransactionCode();
				// Reverting Account tables here
				commonService.RevertAccountTables(TransactionCode);
			} 
			else 
			{
				TransactionCode = commonService.GetMaxTransCode(season);
			}
		
			AdvanceDetails advanceDetails = prepareModelForAdvanceDetails(ryotAdvanceBean, TransactionCode);
			AdvanceSummary advanceSummary = prepareModelForAdvanceSummary(ryotAdvanceBean, TransactionCode);
			//Added by DMurty on 08-12-2016
			List AdvanceSummaryList = ahFuctionalService.getAdvanceSummaryList(ryotAdvanceBean.getRyotCode(),ryotAdvanceBean.getAdvanceType(),ryotAdvanceBean.getSeason());
			if(AdvanceSummaryList != null && AdvanceSummaryList.size()>0)
			{
				AdvanceSummary AdvanceSummary1 = (AdvanceSummary) AdvanceSummaryList.get(0);
				double Amt = advanceSummary.getAdvanceamount();

				double advanceAmt = AdvanceSummary1.getAdvanceamount();
				double pendingAmount = AdvanceSummary1.getPendingamount();
				double pendingpayable = AdvanceSummary1.getPendingpayable();
				double totalamount = AdvanceSummary1.getTotalamount();

				advanceAmt = Amt+advanceAmt;
				pendingAmount = Amt+pendingAmount;
				pendingpayable = Amt+pendingpayable;
				totalamount = Amt+totalamount;
				String qry = "Update AdvanceSummary set transactioncode="+TransactionCode+",advanceamount="+advanceAmt+",pendingamount="+pendingAmount+",pendingpayable="+pendingpayable+",totalamount="+totalamount+" where advancecode='"+ryotAdvanceBean.getAdvanceType()+"' and ryotcode='"+ryotAdvanceBean.getRyotCode()+"' and season='"+ryotAdvanceBean.getSeason()+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				entityList.add(advanceSummary);
			}
			
			int advCode = advanceSummary.getAdvancecode();
			
			AdvancePrincipleAmounts advanceprincipleamounts = prepareModelForAdvancePrincipleAmounts(ryotAdvanceBean);
			List advPrincipleList = new ArrayList();
			advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotAdvanceBean.getRyotCode(),ryotAdvanceBean.getAdvanceType(),ryotAdvanceBean.getSeason());
			if(advPrincipleList != null && advPrincipleList.size()>0)
			{
				AdvancePrincipleAmounts advanceprincipleamounts1 = (AdvancePrincipleAmounts) advPrincipleList.get(0);

				double principle = advanceprincipleamounts1.getPrinciple();
				logger.info("AdvancePrincipleAmounts()========principle==========" + principle);

				double advAmt = ryotAdvanceBean.getLoanAmount();
				double finalAdvAmt = principle+advAmt;
				logger.info("AdvancePrincipleAmounts()========finalAdvAmt==========" + finalAdvAmt);
				
				String qry = "Update AdvancePrincipleAmounts set principle="+finalAdvAmt+" where advancecode='"+ryotAdvanceBean.getAdvanceType()+"' and ryotcode='"+ryotAdvanceBean.getRyotCode()+"' and season='"+ryotAdvanceBean.getSeason()+"'";
				logger.info("saveRyotAdvances()========qry==========" + qry);
				Qryobj = qry;
		    	UpdateList.add(Qryobj);
			}
			else
			{
				entityList.add(advanceprincipleamounts);
			}
			
			entityList.add(advanceDetails);
			
			HttpSession session=request.getSession(false);
			Employees employ = (Employees) session.getAttribute(Constants.USER);
			String user = employ.getLoginid();
			
			double Amt = advanceSummary.getAdvanceamount();
			String loginuser = user;//advanceSummary.getLoginid();
			String AccountCode = advanceSummary.getRyotcode();
			int AccGrpCode = ahFuctionalService.GetAccGrpCode(advanceSummary.getRyotcode());
			String RyotCode = advanceSummary.getRyotcode();
			int advanceCode = advanceSummary.getAdvancecode();
			String stradvanceCode = Integer.toString(advanceCode);
			int onloadSeedlingStatus = ryotAdvanceBean.getOnloadSeedlingStatus();
			
			//byte isSeedlingAdvance = ryotAdvanceBean.getIsSeedlingAdvance();
			byte isSeedlingAdvance = ryotAdvanceBean.getIsItSeedling();
			int advcode = advanceDetails.getAdvancecode();
			List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcode);
			byte isSeedAdvance = companyAdvance.get(0).getIsseedlingadvance();
			// isSeedAdvance ------ Paramater from AdvanceMaster 
			if(isSeedAdvance == 0)
			{
				// isSeedlingAdvance ------ Paramater from Advances to ryot. Here we need to add rec in grid 
				if(isSeedlingAdvance == 0)
				{
					ahFuctionalService.deleteSeedlingTrayDetails(ryotAdvanceBean.getSeason(), ryotAdvanceBean.getRyotCode());
					for (int i = 0; i < gridData.size(); i++)
					{ 
						SeedlingTrayDetailsBean seedlingTrayBean = new ObjectMapper().readValue(gridData.get(i).toString(),SeedlingTrayDetailsBean.class);
						SeedlingTrayDetails seedlingTrayDetails = prepareModelForSeedlingTrayDetails(seedlingTrayBean, ryotAdvanceBean);
						entityList.add(seedlingTrayDetails);
					}
				}
				else
				{
					//We need to delete seed supplier summary 
				//	ahFuctionalService.deleteSeedSupplierDetails(ryotAdvanceBean.getSeason(), ryotAdvanceBean.getSeedSupplierCode());
					//07-04-2017 cumulated seedsupplier amount
					SeedSuppliersSummary seedSuppliersSummary = prepareModelForSeedSuppliersSummary(ryotAdvanceBean);
					entityList.add(seedSuppliersSummary);
	
					// setSeedsuppliercode
					Amt = advanceSummary.getAdvanceamount();
					loginuser = user;//advanceSummary.getLoginid();
					AccountCode = ryotAdvanceBean.getSeedSupplierCode();
					AccGrpCode = ahFuctionalService.GetAccGrpCode(ryotAdvanceBean.getSeedSupplierCode());
					String strAccGrpCode = Integer.toString(AccGrpCode);
					RyotCode = ryotAdvanceBean.getSeedSupplierCode();
					
					Map DtlsMap = new HashMap();
	
					DtlsMap.put("TRANSACTION_CODE", TransactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", Amt);
					DtlsMap.put("LOGIN_USER", loginuser);
					DtlsMap.put("ACCOUNT_CODE", AccountCode);
					DtlsMap.put("JOURNAL_MEMO", "Charges Towards Seed Supply");
					DtlsMap.put("TRANSACTION_TYPE", "Cr");
					DtlsMap.put("SEASON", ryotAdvanceBean.getSeason());
					DtlsMap.put("TRANSACTION_DATE", advDate);
					DtlsMap.put("GL_CODE", ryotAdvanceBean.getRyotCode());

					AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);
	
					String TransactionType = "Cr";
					AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
					
					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", AccountCode);
					boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
					if (advAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(AccountCode,accountSummary);
					}
					else
					{
						accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
						String straccCode = accountSummary.getAccountcode();
						if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", AccountCode);
							tmpMap.put("KEY", AccountCode);
							Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
						}
					}
					// Insert Rec in AccGrpDetails
					Map AccGrpDtlsMap = new HashMap();
	
					AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", "Charges Towards Seed Supply");
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					AccGrpDtlsMap.put("SEASON", ryotAdvanceBean.getSeason());
					AccGrpDtlsMap.put("TRANSACTION_DATE", advDate);
 
					AccountGroupDetails AccountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
					entityList.add(AccountGroupDetails);
	
					TransactionType = "Cr";
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (advAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
					}
					else
					{
						accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);
							Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}
					// Insert Rec in AccSubGrpDtls
					
					int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
					//Hence ryot is seed supplier we need to change this subgroup code to 
					AccSubGCode = 2;
					String strAccSubGrpCode = Integer.toString(AccSubGCode);

					Map AccSubGrpDtlsMap = new HashMap();
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Charges Towards Seed Supply");
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", ryotAdvanceBean.getSeason());
					AccSubGrpDtlsMap.put("TRANSACTION_DATE", advDate);
					//Added by DMurty on 18-11-2016 
					AccSubGrpDtlsMap.put("IS_SEED_SUPPLIER", "Yes");
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
					entityList.add(accountSubGroupDetails);
	
					TransactionType = "Cr";
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
					
					boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if(advAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
					}
					else
					{
						accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}

				AccountCode = ryotAdvanceBean.getRyotCode();
				if (AccountCode != null)
				{
					Amt = advanceSummary.getAdvanceamount();
					loginuser = user;//advanceSummary.getLoginid();
					AccountCode = advanceSummary.getRyotcode();
					AccGrpCode = ahFuctionalService.GetAccGrpCode(advanceSummary.getRyotcode());
					String strAccGrpCode = Integer.toString(AccGrpCode);

					RyotCode = advanceSummary.getRyotcode();
					advanceCode = advanceSummary.getAdvancecode();
					stradvanceCode = Integer.toString(advanceCode);

					String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
													
					// Insert rec in AccDtls
					int advanceCodeforJrnlMemo = advanceSummary.getAdvancecode();
					String advanceName = companyAdvance.get(0).getAdvance();
					String JrnlMemo = "NA";
					if(advanceName != null)
					{
						if(advanceName.contains("advance"))
						{
							JrnlMemo = "Charges towards "+advanceName;
						}
						else
						{
							JrnlMemo = "Charges towards "+advanceName+" advance";
						}
					}				
					Map DtlsMap = new HashMap();

					DtlsMap.put("TRANSACTION_CODE", TransactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", Amt);
					DtlsMap.put("LOGIN_USER", loginuser);
					DtlsMap.put("ACCOUNT_CODE", AccountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					DtlsMap.put("GL_CODE", GlCodeforAdvance);
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", advanceSummary.getSeason());
					DtlsMap.put("TRANSACTION_DATE", advDate);

					AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);

					// Insert Rec in AccSmry
					String TransactionType = "Dr";
					AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,advanceSummary.getSeason());
					
					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", AccountCode);
					boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
					if (advAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(AccountCode,accountSummary);
					}
					else
					{
						accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
						String straccCode = accountSummary.getAccountcode();
						if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", AccountCode);
							tmpMap.put("KEY", AccountCode);
							Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
						}
					}

					// Insert Rec in AccGrpDetails
					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					AccGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
					entityList.add(accountGroupDetails);

					TransactionType = "Dr";
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (advAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
					}
					else
					{
						accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);
							Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}
					
					// Insert Rec in AccSubGrpDtls
					int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
					String strAccSubGrpCode = Integer.toString(AccSubGCode);

					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccSubGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
					entityList.add(accountSubGroupDetails);

					TransactionType = "Dr";
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					
					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
					
					boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if(advAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
					}
					else
					{
						accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				if (stradvanceCode != null)
				{
					// get account code from company advance table
					String GlCode = companyAdvance.get(0).getGlCode(); // GlCode is nothing but AccountCode
					AccountCode = GlCode;
					AccGrpCode = ahFuctionalService.GetAccGrpCode(GlCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);
					loginuser = user;
					String RyotCodeforAdvance = advanceSummary.getRyotcode();
					
					// Insert rec in AccDtls
					int advanceCodeforJrnlMemo = advanceSummary.getAdvancecode();
					String advanceName = companyAdvance.get(0).getAdvance();
					String JrnlMemo = "NA";
					if(advanceName != null)
					{
						if(advanceName.contains("advance"))
						{
							JrnlMemo = "Charges towards "+advanceName;
						}
						else
						{
							JrnlMemo = "Charges towards "+advanceName+" advance";
						}
					}
					// Insert rec in AccDtls
					Map DtlsMap = new HashMap();

					DtlsMap.put("TRANSACTION_CODE", TransactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", Amt);
					DtlsMap.put("LOGIN_USER", loginuser);
					DtlsMap.put("ACCOUNT_CODE", GlCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					DtlsMap.put("GL_CODE", RyotCodeforAdvance);
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", advanceSummary.getSeason());
					DtlsMap.put("TRANSACTION_DATE", advDate);

					AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);

					String TransactionType = "Dr";
					AccountSummary accountSummary = prepareModelforAccSmry(Amt, GlCode,	TransactionType,advanceSummary.getSeason());
					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", AccountCode);
					boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
					if (advAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(AccountCode,accountSummary);
					}
					else
					{
						accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
						String straccCode = accountSummary.getAccountcode();
						if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", AccountCode);
							tmpMap.put("KEY", AccountCode);
							Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
						}
					}
					
					// Insert Rec in AccGrpDetails
					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccGrpDtlsMap.put("ACCOUNT_CODE", GlCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
					entityList.add(accountGroupDetails);

					TransactionType = "Dr";
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(GlCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (advAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
					}
					else
					{
						accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);
							Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}
					
					// Insert Rec in AccSubGrpDtls
					Map AccSubGrpDtlsMap = new HashMap();
					
					int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
					String strAccSubGrpCode = Integer.toString(AccSubGCode);
					
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", GlCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccSubGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
					entityList.add(accountSubGroupDetails);

					TransactionType = "Dr";
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(GlCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
					
					boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if(advAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
					}
					else
					{
						accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
			}
			else
			{
				AccountCode = ryotAdvanceBean.getRyotCode();
				if (AccountCode != null)
				{
					Amt = advanceSummary.getAdvanceamount();
					loginuser = user;//advanceSummary.getLoginid();
					AccountCode = advanceSummary.getRyotcode();
					AccGrpCode = ahFuctionalService.GetAccGrpCode(advanceSummary.getRyotcode());
					String strAccGrpCode = Integer.toString(AccGrpCode);

					RyotCode = advanceSummary.getRyotcode();
					advanceCode = advanceSummary.getAdvancecode();
					stradvanceCode = Integer.toString(advanceCode);
					String GlCodeforAdvance = companyAdvance.get(0).getGlCode();
													
					// Insert rec in AccDtls
					int advanceCodeforJrnlMemo = advanceSummary.getAdvancecode();
					String advanceName = companyAdvance.get(0).getAdvance();
					String JrnlMemo = "NA";
					if(advanceName != null)
					{
						if(advanceName.contains("advance"))
						{
							JrnlMemo = "Charges towards "+advanceName;
						}
						else
						{
							JrnlMemo = "Charges towards "+advanceName+" advance";
						}
					}				
					Map DtlsMap = new HashMap();
					DtlsMap.put("TRANSACTION_CODE", TransactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", Amt);
					DtlsMap.put("LOGIN_USER", loginuser);
					DtlsMap.put("ACCOUNT_CODE", AccountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					DtlsMap.put("GL_CODE", GlCodeforAdvance);
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", advanceSummary.getSeason());
					DtlsMap.put("TRANSACTION_DATE", advDate);

					AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);

					// Insert Rec in AccSmry
					String TransactionType = "Dr";
					AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,advanceSummary.getSeason());
					
					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", AccountCode);
					boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
					if (advAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(AccountCode,accountSummary);
					}
					else
					{
						accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
						String straccCode = accountSummary.getAccountcode();
						if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", AccountCode);
							tmpMap.put("KEY", AccountCode);
							Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
						}
					}
					

					// Insert Rec in AccGrpDetails
					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
					entityList.add(accountGroupDetails);

					TransactionType = "Dr";
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (advAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
					}
					else
					{
						accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);
							Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}

					// Insert Rec in AccSubGrpDtls
					Map AccSubGrpDtlsMap = new HashMap();
					int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
					String strAccSubGrpCode = Integer.toString(AccSubGCode);

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccSubGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
					entityList.add(accountSubGroupDetails);

					TransactionType = "Dr";
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
					
					boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if(advAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
					}
					else
					{
						accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}
				if (stradvanceCode != null)
				{
					// get account code from company advance table
					String GlCode = companyAdvance.get(0).getGlCode(); // GlCode is nothing but AccountCode
					AccountCode = GlCode;
					AccGrpCode = ahFuctionalService.GetAccGrpCode(GlCode);
					String strAccGrpCode = Integer.toString(AccGrpCode);

					String RyotCodeforAdvance = advanceSummary.getRyotcode();
					loginuser = user;
					// Insert rec in AccDtls
					int advanceCodeforJrnlMemo = advanceSummary.getAdvancecode();
					String advanceName = companyAdvance.get(0).getAdvance();
					String JrnlMemo = "NA";
					if(advanceName != null)
					{
						if(advanceName.contains("advance"))
						{
							JrnlMemo = "Charges towards "+advanceName;
						}
						else
						{
							JrnlMemo = "Charges towards "+advanceName+" advance";
						}
					}
					// Insert rec in AccDtls
					Map DtlsMap = new HashMap();

					DtlsMap.put("TRANSACTION_CODE", TransactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", Amt);
					DtlsMap.put("LOGIN_USER", loginuser);
					DtlsMap.put("ACCOUNT_CODE", GlCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					DtlsMap.put("GL_CODE", RyotCodeforAdvance);
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", advanceSummary.getSeason());
					DtlsMap.put("TRANSACTION_DATE", advDate);

					AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);

					String TransactionType = "Dr";
					AccountSummary accountSummary = prepareModelforAccSmry(Amt, GlCode,	TransactionType,advanceSummary.getSeason());
					Map AccSmry = new HashMap();
					AccSmry.put("ACC_CODE", AccountCode);
					boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
					if (advAccCodetrue == false)
					{
						AccountCodeList.add(AccSmry);
						AccountSummaryMap.put(AccountCode,accountSummary);
					}
					else
					{
						accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
						String straccCode = accountSummary.getAccountcode();
						if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
						{
							double runbal = accountSummary.getRunningbalance();
							String baltype = accountSummary.getBalancetype();
							String ses = accountSummary.getSeason();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_CODE", AccountCode);
							tmpMap.put("KEY", AccountCode);
							Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
							AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
						}
					}

					// Insert Rec in AccGrpDetails
					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccGrpDtlsMap.put("ACCOUNT_CODE", GlCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
					entityList.add(accountGroupDetails);

					TransactionType = "Dr";
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(GlCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccGrpSmry = new HashMap();
					AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
					boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
					if (advAccGrpCodetrue == false)
					{
						AccountGrpCodeList.add(AccGrpSmry);
						AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
					}
					else
					{
						accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
						int accountGrpCode = accountGroupSummary.getAccountgroupcode();
						if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
						{
							double runbal = accountGroupSummary.getRunningbalance();
							String baltype = accountGroupSummary.getBalancetype();
							String ses = accountGroupSummary.getSeason();
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("KEY", strAccGrpCode);
							Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
							AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
						}
					}
					
					// Insert Rec in AccSubGrpDtls
					Map AccSubGrpDtlsMap = new HashMap();
					
					int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
					String strAccSubGrpCode = Integer.toString(AccSubGCode);

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
					AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", GlCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", advanceSummary.getSeason());
					AccSubGrpDtlsMap.put("TRANSACTION_DATE", advDate);

					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
					entityList.add(accountSubGroupDetails);

					TransactionType = "Dr";
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(GlCode, Amt, AccGrpCode, TransactionType,advanceSummary.getSeason());
					Map AccSubGrpSmry = new HashMap();
					AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
					
					boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
					if(advAccSubGrpCodetrue == false)
					{
						AccountSubGrpCodeList.add(AccSubGrpSmry);
						AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
					}
					else
					{
						accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
						int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
						int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
						String ses = accountSubGroupSummary.getSeason();
						if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
						{
							double runbal = accountSubGroupSummary.getRunningbalance();
							String baltype = accountSubGroupSummary.getBalancetype();
							
							Map tmpMap = new HashMap();
							tmpMap.put("RUN_BAL", runbal);
							tmpMap.put("AMT", Amt);
							tmpMap.put("BAL_TYPE", baltype);
							tmpMap.put("SEASON", ses);
							tmpMap.put("TRANSACTION_TYPE", TransactionType);
							tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
							tmpMap.put("KEY", strAccSubGrpCode);
							Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
							AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
						}
					}
				}	
			}
			//Added by DMurty on 12-11-2016
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries.next();
			    Object value = entry.getValue();
			    AccountSummary accountSummary = (AccountSummary) value;
			    
			    String accountCode = accountSummary.getAccountcode();
			    String sesn = accountSummary.getSeason();
			    double runnbal = accountSummary.getRunningbalance();
			    String crOrDr = accountSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSummary);
			    }
			    //advanceLoansTablesList.add(value) ;
			}
			
			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries1.next();
			    Object value = entry.getValue();
			    
			    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
			    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
			    String sesn = accountGroupSummary.getSeason();
			    double runnbal = accountGroupSummary.getRunningbalance();
			    String crOrDr = accountGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountGroupSummary);
			    }
			    //advanceLoansTablesList.add(value) ;
			}
			
			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries2.next();
			    Object value = entry.getValue();
			    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
			   
			    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
			    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
			    String sesn = accountSubGroupSummary.getSeason();
			    double runnbal = accountSubGroupSummary.getRunningbalance();
			    String crOrDr = accountSubGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSubGroupSummary);
			    }
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private SeedSuppliersSummary prepareModelForSeedSuppliersSummary(RyotAdvanceBean ryotAdvanceBean) 
	{
		SeedSuppliersSummary seedSuppliersSummary = new SeedSuppliersSummary();

		seedSuppliersSummary.setLoginid(ryotAdvanceBean.getLoginId());
		seedSuppliersSummary.setAdvancepayable(ryotAdvanceBean.getLoanAmount());
		seedSuppliersSummary.setPaidamount(ryotAdvanceBean.getLoanAmount());
		seedSuppliersSummary.setPendingamount(ryotAdvanceBean.getLoanAmount());
		seedSuppliersSummary.setSeason(ryotAdvanceBean.getSeason());
		seedSuppliersSummary.setSeedsuppliercode(ryotAdvanceBean.getSeedSupplierCode());
		seedSuppliersSummary.setTotalextentsize(ryotAdvanceBean.getExtentSize());
		seedSuppliersSummary.setAdvancestatus((byte) 0);

		return seedSuppliersSummary;
	}

	private AdvanceDetails prepareModelForAdvanceDetails(RyotAdvanceBean ryotAdvanceBean, int TransactionCode)
	{
		AdvanceDetails advanceDetails = new AdvanceDetails();

		String modifyFlag = ryotAdvanceBean.getModifyFlag();
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			advanceDetails.setId(ryotAdvanceBean.getId());
		}
		advanceDetails.setRyotcode(ryotAdvanceBean.getRyotCode());
		advanceDetails.setSeason(ryotAdvanceBean.getSeason());
		advanceDetails.setAdvancecode(ryotAdvanceBean.getAdvanceType());
		advanceDetails.setAdvanceamount(ryotAdvanceBean.getLoanAmount());
		advanceDetails.setAgreements(ryotAdvanceBean.getAgreementNumber());
		advanceDetails.setExtentsize(ryotAdvanceBean.getExtentSize());
		//added by naidu on 02-03-2017
		advanceDetails.setPlantExtent(ryotAdvanceBean.getPlantExtent());
		advanceDetails.setRatoonExtent(ryotAdvanceBean.getRatoonExtent());
		advanceDetails.setAdvancecateogrycode(ryotAdvanceBean.getAdvanceCategoryCode());
		advanceDetails.setAdvancesubcategorycode(ryotAdvanceBean.getAdvanceSubCategoryCode());
		advanceDetails.setTotalamount(ryotAdvanceBean.getLoanAmount());
		advanceDetails.setIsitseedling(ryotAdvanceBean.getIsItSeedling());
		advanceDetails.setIsSeedlingAdvance(ryotAdvanceBean.getIsSeedlingAdvance());
		advanceDetails.setSeedsuppliercode(ryotAdvanceBean.getSeedSupplierCode());
		advanceDetails.setSeedingsquantity(ryotAdvanceBean.getSeedlingsQuantity());
		advanceDetails.setRyotpayable(ryotAdvanceBean.getLoanAmount());
		advanceDetails.setTransactioncode(TransactionCode);
		advanceDetails.setLoginId(ryotAdvanceBean.getLoginId());
		
		byte isSeedlingAdvance = ryotAdvanceBean.getIsSeedlingAdvance();
		int advcode = advanceDetails.getAdvancecode();
		List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcode);
		byte isSeedAdvance = companyAdvance.get(0).getIsseedlingadvance();
		if(ryotAdvanceBean.getIsSeedlingAdvance() == 0)
		{
			advanceDetails.setSeedsuppliercode(ryotAdvanceBean.getSeedSupplierCode());
			advanceDetails.setSeedingsquantity(ryotAdvanceBean.getSeedlingsQuantity());
			advanceDetails.setTotalamount(ryotAdvanceBean.getTotalCost());
		}
		else
		{
			advanceDetails.setSeedsuppliercode(ryotAdvanceBean.getSeedSupplierCode());
			advanceDetails.setSeedingsquantity(ryotAdvanceBean.getSeedlingsQuantity());
			advanceDetails.setTotalamount(ryotAdvanceBean.getTotalCost());
		}
		//Added by DMurty on 22-10-2016
		String advDate = ryotAdvanceBean.getAdvanceDate();
		advanceDetails.setAdvancedate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		
		return advanceDetails;
	}

	private AdvanceSummary prepareModelForAdvanceSummary(RyotAdvanceBean ryotAdvanceBean, int TransactionCode)
	{
		List<Ryot> ryot = ryotService.getRyotByRyotCode(ryotAdvanceBean.getRyotCode());

		AdvanceSummary advanceSummary = new AdvanceSummary();

		String modifyFlag = ryotAdvanceBean.getModifyFlag();
		
		advanceSummary.setRyotcode(ryotAdvanceBean.getRyotCode());
		
		advanceSummary.setVillagecode(ryot.get(0).getVillagecode());
		
		advanceSummary.setAdvanceamount(ryotAdvanceBean.getLoanAmount());
		advanceSummary.setAdvancecode(ryotAdvanceBean.getAdvanceType());
		advanceSummary.setSeason(ryotAdvanceBean.getSeason());
		advanceSummary.setPendingamount(ryotAdvanceBean.getLoanAmount());
		
		double intrestRate = 0;
		int advanceCode = ryotAdvanceBean.getAdvanceType();
		List<Double> IntrestList = new ArrayList<Double>();
		IntrestList = ahFuctionalService.getIntrestRatesByAmount(ryotAdvanceBean.getLoanAmount());
		if (IntrestList != null && IntrestList.size() > 0)
		{
			double myResult1 = IntrestList.get(0);
			intrestRate = myResult1;
		}
		
		advanceSummary.setInterestamount(intrestRate);
		advanceSummary.setRecoveredamount(0.00);
		advanceSummary.setTotalamount(ryotAdvanceBean.getLoanAmount());
		advanceSummary.setAdvancestatus((byte) 0);
		advanceSummary.setPaidamount(0.00);
		advanceSummary.setPendingpayable(ryotAdvanceBean.getLoanAmount());
		advanceSummary.setTransactioncode(TransactionCode);
		advanceSummary.setLoginid(ryotAdvanceBean.getLoginId());
		advanceSummary.setRyotcodeSeq(ryot.get(0).getId());
		
		if ("Yes".equalsIgnoreCase(modifyFlag))
		{
			int id = 0;
			List AdvanceList = ahFuctionalService.getAdvanceSummaryData(ryotAdvanceBean.getSeason(), ryotAdvanceBean.getRyotCode(), ryotAdvanceBean.getAdvanceCode());
			if(AdvanceList != null && AdvanceList.size()>0)
			{
				Map tempMap = new HashMap();
				tempMap = (Map) AdvanceList.get(0);
				id = (Integer) tempMap.get("id");
			}
			advanceSummary.setId(id);
		}
		return advanceSummary;
	}

	private AdvancePrincipleAmounts prepareModelForAdvancePrincipleAmounts(	RyotAdvanceBean ryotAdvanceBean)
	{
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AdvancePrincipleAmounts advanceprincipleamounts = new AdvancePrincipleAmounts();
		advanceprincipleamounts.setAdvancecode(ryotAdvanceBean.getAdvanceType());
		advanceprincipleamounts.setRyotcode(ryotAdvanceBean.getRyotCode());
		advanceprincipleamounts.setPrinciple(ryotAdvanceBean.getLoanAmount());
		advanceprincipleamounts.setSeason(ryotAdvanceBean.getSeason());
		String advDate = ryotAdvanceBean.getAdvanceDate();
		advanceprincipleamounts.setPrincipledate(DateUtils.getSqlDateFromString(advDate,Constants.GenericDateFormat.DATE_FORMAT));
		
		return advanceprincipleamounts;
	}

	private SeedlingTrayDetails prepareModelForSeedlingTrayDetails(SeedlingTrayDetailsBean seedlingTrayBean,RyotAdvanceBean ryotAdvanceBean) 
	{
		SeedlingTrayDetails seedlingTrayDetails = new SeedlingTrayDetails();

		seedlingTrayDetails.setNooftrays(seedlingTrayBean.getNoOfTrays());
		seedlingTrayDetails.setRyotcode(ryotAdvanceBean.getRyotCode());
		seedlingTrayDetails.setSeason(ryotAdvanceBean.getSeason());
		seedlingTrayDetails.setSuppliedseedlings(seedlingTrayBean.getSuppliedSeeds());
		seedlingTrayDetails.setTraytype(seedlingTrayBean.getTrayType());
		seedlingTrayDetails.setId(seedlingTrayBean.getId());
		seedlingTrayDetails.setVarietycode(seedlingTrayBean.getVarietyCode());
		return seedlingTrayDetails;
	}

	@RequestMapping(value = "/getRyotAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getRyotAdvances(@RequestBody String jsonData) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		List dropdownList = ahFuctionalService.getRyotAdvances(jsonData);
		// List AdvanceNamesList = new ArrayList();
		logger.info("========dropdownList==========" + dropdownList);
		List<String> AdvanceNamesList = new ArrayList<String>();
		try 
		{
			if (dropdownList != null && dropdownList.size() > 0) 
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();) 
				{
					jsonObj = new JSONObject();
					Integer myResult = (Integer) it.next();
					String AdvName = "NA";
					AdvanceNamesList = ahFuctionalService.getRyotAdvanceNames(myResult);
					if (AdvanceNamesList != null && AdvanceNamesList.size() > 0) 
					{
						String myResult1 = AdvanceNamesList.get(0);
						AdvName = myResult1;
					}

					jsonObj.put("advanceCode", myResult);
					jsonObj.put("advanceName", AdvName);

					jArray.add(jsonObj);
				}
			}
		} catch (Exception e) {
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}

	@RequestMapping(value = "/getRyotNamesByAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getRyotNamesByAdvances(@RequestBody JSONObject jsonData)	throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		Integer nadvancecode = (Integer) jsonData.get("advanceCode");

		List<String> RyotNamesList = new ArrayList<String>();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		try {
			List dropdownList = ahFuctionalService.getRyotNamesByAdvances(season, nadvancecode);
			if (dropdownList != null && dropdownList.size() > 0) 
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();)
				{
					jsonObj = new JSONObject();
					String myResult = (String) it.next();
					String ryotName = "";
					RyotNamesList = ahFuctionalService.getRyotName(myResult);
					if (RyotNamesList != null && RyotNamesList.size() > 0) 
					{
						String myResult1 = RyotNamesList.get(0);
						ryotName = myResult1;
					}
					jsonObj.put("ryotcode", myResult);
					jsonObj.put("ryotName", ryotName);
					jArray.add(jsonObj);
				}
			}
		} catch (Exception e) {
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}

	@RequestMapping(value = "/getAllRyotAdvanceDetails", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody String getAllRyotAdvanceDetails(@RequestBody JSONObject jsonData)	throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		String ryotcode = (String) jsonData.get("ryotcode");
		int nadvancecode = (Integer) jsonData.get("advanceCode");
		RyotAdvanceBean ryotAdvanceBean = prepareBeanForAdvanceDetails(ahFuctionalService.getAdvanceDetailsBySeason(season, ryotcode, nadvancecode));
		List<SeedlingTrayDetailsBean> prepareSeedlingTrayDetailsBeans = prepareSeedlingTrayDtlsBeans(ahFuctionalService	.getSeedlingTrayDetailsByRyotCodeSeason(season, ryotcode));

		org.json.JSONObject ryotAdvanceBeanJson = new org.json.JSONObject(ryotAdvanceBean);
		org.json.JSONArray prepareSeedlingTrayDetailsBeansJson = new org.json.JSONArray(prepareSeedlingTrayDetailsBeans);

		response.put("formData", ryotAdvanceBeanJson);
		response.put("gridData", prepareSeedlingTrayDetailsBeansJson);
		return response.toString();
	}

	private List<SeedlingTrayDetailsBean> prepareSeedlingTrayDtlsBeans(	List<SeedlingTrayDetails> seedlingTrayDetails)
	{
		List<SeedlingTrayDetailsBean> beans = null;
		if (seedlingTrayDetails != null && !seedlingTrayDetails.isEmpty())
		{
			beans = new ArrayList<SeedlingTrayDetailsBean>();
			SeedlingTrayDetailsBean bean = null;
			for (SeedlingTrayDetails seedlingTrayDetail : seedlingTrayDetails)
			{
				bean = new SeedlingTrayDetailsBean();
				bean.setId(seedlingTrayDetail.getId());
				bean.setNoOfTrays(seedlingTrayDetail.getNooftrays());
				bean.setSuppliedSeeds(seedlingTrayDetail.getSuppliedseedlings());
				bean.setTrayType(seedlingTrayDetail.getTraytype());
				bean.setVarietyCode(seedlingTrayDetail.getVarietycode());
				beans.add(bean);
			}
		}
		return beans;
	}

	private RyotAdvanceBean prepareBeanForAdvanceDetails(List<AdvanceDetails> advanceDetails) 
	{
		RyotAdvanceBean bean = null;
		if (advanceDetails != null && !advanceDetails.isEmpty())
		{
			bean = new RyotAdvanceBean();
			AdvanceDetails advanceDetail = (AdvanceDetails) advanceDetails.get(0);
			bean.setId(advanceDetail.getId());
			bean.setSeason(advanceDetail.getSeason());
			bean.setRyotCode(advanceDetail.getRyotcode());
			bean.setAdvanceCode(advanceDetail.getAdvancecode());
			bean.setLoanAmount(advanceDetail.getAdvanceamount());
			//bean.setVarietyCode(advanceDetail.getVarietycode());
			bean.setAgreementNumber(advanceDetail.getAgreements());
			bean.setExtentSize(advanceDetail.getExtentsize());
			bean.setAdvanceCategoryCode(advanceDetail.getAdvancecateogrycode());
			bean.setAdvanceSubCategoryCode(advanceDetail.getAdvancesubcategorycode());
			bean.setSeedSupplierCode(advanceDetail.getSeedsuppliercode());
			bean.setSeedlingsQuantity(advanceDetail.getSeedingsquantity());
			bean.setTotalCost(advanceDetail.getTotalamount());
			bean.setRyotPayable(advanceDetail.getRyotpayable());
			bean.setTransactionCode(advanceDetail.getTransactioncode());
			bean.setIsItSeedling(advanceDetail.getIsitseedling());
			String ryotName = "";
			List<String> RyotNamesList = new ArrayList<String>();
			RyotNamesList = ahFuctionalService.getRyotName(advanceDetail.getRyotcode());
			if (RyotNamesList != null && RyotNamesList.size() > 0) 
			{
				String myResult1 = RyotNamesList.get(0);
				ryotName = myResult1;
			}
			bean.setRyotName(ryotName);
			bean.setModifyFlag("Yes");
			bean.setAdvanceType(advanceDetail.getAdvancecode());
			bean.setLoginId(advanceDetail.getLoginId());
			bean.setIsSeedlingAdvance(advanceDetail.getIsSeedlingAdvance());
			//added by naidu
			double remadvamt =0.0;
			
			List<Double> AdvanceList = new ArrayList<Double>();
			AdvanceList = ahFuctionalService.getAdvanceAmtforValidation(advanceDetail.getAdvancecode());
			if (AdvanceList != null && AdvanceList.size() > 0) 
			{
				remadvamt = AdvanceList.get(0);
			}
			bean.setAvailableBalance(remadvamt-advanceDetail.getAdvanceamount());
			//Modified by DMurty on 22-10-2016
			Date advDate = advanceDetail.getAdvancedate();
			if(advDate != null)
			{
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String stradvDate = df.format(advDate);
				bean.setAdvanceDate(stradvDate);
			}
			else
			{
				Date date = new Date();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String stradvDate = df.format(date);
				bean.setAdvanceDate(stradvDate);
			}
			
			//AdvancePrincipleAmounts
			/*List PrincipleList = ahFuctionalService.getRyotAdvancePrincipleData(advanceDetail.getRyotcode(),advanceDetail.getAdvancecode(),advanceDetail.getSeason());
			if (PrincipleList != null && PrincipleList.size() > 0) 
			{
				Map AdvMap = new HashMap();
				AdvMap = (Map) PrincipleList.get(0);
				Date advDate = (Date) AdvMap.get("principledate");
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String stradvDate = df.format(advDate);
				bean.setAdvanceDate(stradvDate);
			}
			else
			{
				Date date = new Date();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String stradvDate = df.format(date);
				bean.setAdvanceDate(stradvDate);
			}*/
		}
		return bean;
	}

	@RequestMapping(value = "/getadvanceAmounttoValidate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray getadvanceAmounttoValidate(@RequestBody String jsonData)	throws Exception
	{
		logger.info("========getadvanceAmounttoValidate()==========");
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		jsonObj = new JSONObject();
		double Amt = 0;
		List<Double> AdvanceList = new ArrayList<Double>();
		AdvanceList = ahFuctionalService.getAdvanceAmtforValidation(Integer.parseInt(jsonData));
		if (AdvanceList != null && AdvanceList.size() > 0) 
		{
			double myResult1 = AdvanceList.get(0);
			Amt = myResult1;
		}
		jsonObj.put("maxAdvanceAmt", Amt);
		jArray.add(jsonObj);
		return jArray;
	}

	// This Ajax call is using for 2 screens. i.e for update ccs rating and avg
	// ccs rating.
	// We are getting data from sample card details
	@RequestMapping(value = "/getSampleCards", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	List getSampleCards(@RequestBody JSONObject jsonData) throws Exception {

		UpdateCcsBean updateCcsBean = null;
		org.json.JSONObject response = new org.json.JSONObject();

		String season = (String) jsonData.get("season");
		Integer program = (Integer) jsonData.get("programno");
		Integer samplecard = (Integer) jsonData.get("samplecard");
		String flag = (String) jsonData.get("Flag");
		String Page = (String) jsonData.get("pageName");
		// Yea for sample card
		// No for get Details
		logger.info("========season==========" + season);
		logger.info("========program==========" + program);
		logger.info("========samplecard==========" + samplecard);
		logger.info("========flag==========" + flag);
		logger.info("========Page==========" + Page);

		if ("Update".equalsIgnoreCase(Page)) // Update is for update lab ccs rating screen.
		{
			if ("No".equalsIgnoreCase(flag))
			{
				List<UpdateCcsBean> updateCcsBeans = prepareListofBeanForgetAllSampleCards(ahFuctionalService.listSampleCardsBySeasonandPrgrm(season, program, Page), Page);
				return updateCcsBeans;
			}
			else 
			{
				List<UpdateCcsBean> updateCcsBeans = prepareListofBeanForgetAllSampleCards(ahFuctionalService.listSampleCards(season, program,samplecard),Page);
				return updateCcsBeans;
			}
		} 
		else // This is for Average ccs rating screen.
		{
			List<UpdateCcsBean> updateCcsBeans = prepareListofBeanForgetAllSampleCards(	ahFuctionalService.listSampleCardsBySeasonandPrgrm(season,program, Page), Page);
			return updateCcsBeans;
		}
	}

	private List<UpdateCcsBean> prepareListofBeanForgetAllSampleCards(List<SampleCards> SampleCardss, String Page) 
	{
		List<UpdateCcsBean> beans = null;
		Double rating=0.0;
		if (SampleCardss != null && !SampleCardss.isEmpty()) 
		{
			beans = new ArrayList<UpdateCcsBean>();
			UpdateCcsBean bean = null;
			for (SampleCards sampleCards : SampleCardss) 
			{
				bean = new UpdateCcsBean();
				//added by naidu 24-11-2016 for getting agreement per plot wise
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(sampleCards.getAggrementno(),sampleCards.getPlotno(),sampleCards.getSeason());
		
				bean.setRyotCode(sampleCards.getRyotcode());

				String ryotName = "NA";
				List<String> RyotNamesList = new ArrayList<String>();
				RyotNamesList = ahFuctionalService.getRyotName(sampleCards.getRyotcode());
				if (RyotNamesList != null && RyotNamesList.size() > 0)
				{
					String myResult1 = RyotNamesList.get(0);
					ryotName = myResult1;
				}
				bean.setRyotName(ryotName);
				bean.setAgreementNumber(sampleCards.getAggrementno());
				bean.setPlotNumber(sampleCards.getPlotno());
				bean.setExtentSize(sampleCards.getExtentsize());
				String plntOrRtn = agreementDetails.get(0).getPlantorratoon();
				
				int nplntOrRtn = Integer.parseInt(plntOrRtn);
				
				String cropName = "";
				List<String> CropList = new ArrayList<String>();
				CropList = ahFuctionalService.getCropTypeByCode(nplntOrRtn);
				if (CropList != null && CropList.size() > 0) 
				{
					String myResult1 = CropList.get(0);
					cropName = myResult1;
				}
				bean.setPlantOrRatoon(cropName);

				String villageCode = "";
				int varietyCode = 0;
				List AgreementList = new ArrayList();
				villageCode = agreementDetails.get(0).getVillagecode();
				varietyCode = agreementDetails.get(0).getVarietycode();

				String villageName = "NA";
				List<String> VillageList = new ArrayList<String>();
				
				VillageList = ahFuctionalService.getVillageDetailsByCode(villageCode);
				if (VillageList != null && VillageList.size() > 0) 
				{
					String myResult1 = VillageList.get(0);
					villageName = myResult1;
				}

				String varietyName = "NA";
				List VarietyList = new ArrayList();
				VarietyList = ahFuctionalService.getVarietyDetailsByCodeNew(varietyCode);
				if (VarietyList != null && VarietyList.size() > 0) 
				{
					Map tempMap = new HashMap();
					tempMap = (Map) VarietyList.get(0);
					varietyName = (String) tempMap.get("variety");
				}
				
				bean.setVariety(varietyName);
				bean.setVillage(villageName);
				bean.setProgram(sampleCards.getProgramnumber());
				bean.setSeason(sampleCards.getSeason());
				//bean.setPlantOrRatoon(sampleCards.getPlantorratoon());
				bean.setSamplecard(sampleCards.getSamplecard());
				bean.setBrix(sampleCards.getBrix());
				bean.setPol(sampleCards.getPol());
				bean.setPurity(sampleCards.getPurity());
				bean.setCcsRating(sampleCards.getCcsrating());
				
				// send count for samplecards
				int programNo = sampleCards.getProgramnumber();
				String season = sampleCards.getSeason();
				int count = 0;
				List CountList = new ArrayList();
				CountList = ahFuctionalService.getCountofSampleCards(season,programNo);
				if (CountList != null && CountList.size() > 0) 
				{
					int countResult = (Integer)CountList.get(0);
					count = countResult;
				}
				bean.setSampleCardCount(count);

				if ("Update".equalsIgnoreCase(Page)) // Update is for update lab ccs rating screen.
				{
					bean.setAvgCcsRating(sampleCards.getAvgccsrating());
					//Added by Dmurty on 21-10-2016
					if(sampleCards.getSampledate() != null)
					{
						Date sampleDate = sampleCards.getSampledate();
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String strsampleDate = df.format(sampleDate);
						bean.setSampleDate(strsampleDate);
					}
					else
					{
						Date date = new Date();
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String strsampleDate = df.format(date);
						bean.setSampleDate(strsampleDate);
					}
				} 
				else 
				{
					double Avg = 0;
					String plotNo = sampleCards.getPlotno();
					programNo = sampleCards.getProgramnumber();
					List AvgList = ahFuctionalService.getCcsratingsByPlotno(plotNo, programNo, sampleCards.getAggrementno());
					if (AvgList != null && AvgList.size() > 0) 
					{
						if (!AvgList.contains(null)) // oldDate.equals(returndate.trim()))
						{
							Avg = (Double) AvgList.get(0);
						}
						//Modified by DMurty on 10-08-2016 
						bean.setAvgCcsRating(Avg);
					}

					List<AgreementSummary> agreementSummary = ahFuctionalService.getAgreementDetails(sampleCards.getAggrementno());
					double fAvgCcsRating = 0;
					
					int hasfamilygroup = agreementSummary.get(0).getHasfamilygroup();
					if(hasfamilygroup == 0)
					{
						if(agreementSummary.get(0).getFamilygroupcode() != null)
						{
							List FAvgList = ahFuctionalService.getFamilyAvgCcsratingsByPlotno(plotNo, programNo,agreementSummary.get(0).getFamilygroupcode());
							if (FAvgList != null && FAvgList.size() > 0)
							{
								if (!FAvgList.contains(null)) // oldDate.equals(returndate.trim()))
								{
									fAvgCcsRating = (Double) FAvgList.get(0);
								}
							}
							bean.setFgAvgCcsRating(fAvgCcsRating);
						}
						else
						{
							//bean.setFgAvgCcsRating(fAvgCcsRating);
							bean.setFgAvgCcsRating(Avg);
						}
					}
					else
					{
						//added by naidu 12-07-2016
						if(sampleCards.getFgavgccsrating() != null)
						{
							if(sampleCards.getFgavgccsrating() == 0)
							{
								bean.setFgAvgCcsRating(Avg)	;
							}
							else
							{
								bean.setFgAvgCcsRating(sampleCards.getFgavgccsrating());
							}
						}
						/*else
						{
							bean.setFgAvgCcsRating(Avg)	;
						}*/
					}
				}
				
				String agreementNum = sampleCards.getAggrementno();
				List<AgreementSummary> agrmntSmry = ahFuctionalService.getAgreementSmryByAgreementNo(sampleCards.getSeason(),agreementNum);
				Byte status = agrmntSmry.get(0).getStatus();
				if(status==0)
				{
					beans.add(bean);
				}
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveAverageCcsRatingsTemp", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	List saveAverageCcsRatingsTemp(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		String strQry = null;
		try 
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject formData = (JSONObject) jsonData.get("formData");

			String season = formData.getString("season");
			String flag = formData.getString("Flag");
			String Page = formData.getString("pageName");
			String program = formData.getString("programno");
			int nprogram = Integer.parseInt(program);
			
			//if(!("Average".equalsIgnoreCase(Page)))
		//	{
				ahFuctionalService.TruncateTemp("SampleCardsTemp");
		//	}
			
			for (int i = 0; i < gridData.size(); i++) 
			{
				logger.info("========Exception Record=========" + i);
				UpdateCcsBean updateCcsBean = new ObjectMapper().readValue(gridData.get(i).toString(), UpdateCcsBean.class);
				JSONObject jasonObj = (JSONObject) gridData.get(i);
				
				//Modified by DMurty on 10-09-2016
				SampleCardsTemp sampleCardsTemp = new SampleCardsTemp();
				
				//Added by DMurty on 11-08-2016
				String brix = "0";
				double ccs = 0.0;
				String pol = "0";
				String purity = "0";
				int sampleCard = updateCcsBean.getSamplecard();
				List SampleList = ahFuctionalService.getSampleCardDetails(sampleCard,season,"SampleCards");
				if(SampleList != null && SampleList.size()>0)
				{
					Map tempMap = new HashMap();
					tempMap = (Map) SampleList.get(0);
					
					brix = (String) tempMap.get("brix");
					ccs = (Double) tempMap.get("ccsrating");
					pol = (String) tempMap.get("pol");
					purity = (String) tempMap.get("purity");
				}
				
				sampleCardsTemp.setBrix(brix);
				sampleCardsTemp.setCcsrating(ccs);
				sampleCardsTemp.setPol(pol);
				sampleCardsTemp.setPurity(purity);

				sampleCardsTemp.setAggrementno(updateCcsBean.getAgreementNumber());
				sampleCardsTemp.setAvgccsrating(updateCcsBean.getAvgCcsRating());
				sampleCardsTemp.setExtentsize(updateCcsBean.getExtentSize());
				
				//Modified by DMurty on 10-08-2016
				int falmilyGrpCode = 0;
				List<AgreementSummary> agreementSummary = ahFuctionalService.getAgreementDetails(updateCcsBean.getAgreementNumber());
				if(agreementSummary.get(0).getFamilygroupcode() != null)
				{
					sampleCardsTemp.setFamilygroupcode(agreementSummary.get(0).getFamilygroupcode());
					falmilyGrpCode = agreementSummary.get(0).getFamilygroupcode();
				}
				else
				{
					sampleCardsTemp.setFamilygroupcode(0);
				}
				
				double fAvgCcsRating = 0.0;
				if(falmilyGrpCode != 0)
				{
					List FAvgList = ahFuctionalService.getFamilyAvgCcsratingsByPlotno(updateCcsBean.getPlotNumber(), updateCcsBean.getProgram(),agreementSummary.get(0).getFamilygroupcode());
					if (FAvgList != null && FAvgList.size() > 0)
					{
						if (!FAvgList.contains(null)) // oldDate.equals(returndate.trim()))
						{
							fAvgCcsRating = (Double) FAvgList.get(0);
						}
					}
				}
				
				
				if(fAvgCcsRating >0)
				{
					sampleCardsTemp.setFgavgccsrating(fAvgCcsRating);
				}
				else
				{
					//int falmilyGrpCode = agreementSummary.get(0).getFamilygroupcode();
					//if(falmilyGrpCode == 0)
					//{
						sampleCardsTemp.setFgavgccsrating(updateCcsBean.getAvgCcsRating());
					//}
				}
				//added by naidu on 24-11-2016 for plot wise extent type
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(updateCcsBean.getAgreementNumber(),updateCcsBean.getPlotNumber(),updateCcsBean.getSeason());
				sampleCardsTemp.setPlantorratoon((byte) Integer.parseInt(agreementDetails.get(0).getPlantorratoon()));
						
				sampleCardsTemp.setPlotno(updateCcsBean.getPlotNumber());
				sampleCardsTemp.setProgramnumber(updateCcsBean.getProgram());
				sampleCardsTemp.setRyotcode(updateCcsBean.getRyotCode());
				sampleCardsTemp.setSamplecard(updateCcsBean.getSamplecard());
				sampleCardsTemp.setSeason(updateCcsBean.getSeason());
				entityList.add(sampleCardsTemp);
				
				//SampleCardsTemp sampleCardsTemp = prepareModelForAverageCcsRating(updateCcsBean);
				//ahFuctionalService.addSampleCardTemp(sampleCardsTemp);
			}
			List<UpdateCcsBean> updateCcsBeans = null;
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			if(isInsertSuccess == true)
			{
				updateCcsBeans = prepareListofBeanForgetAllSampleCards(ahFuctionalService.listTempSampleCardsBySeasonandPrgrm(season,nprogram,Page),Page);
			}
			return updateCcsBeans;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}

	private SampleCardsTemp prepareModelForAverageCcsRating(UpdateCcsBean updateCcsBean) 
	{
		SampleCardsTemp sampleCardsTemp = new SampleCardsTemp();

		sampleCardsTemp.setAggrementno(updateCcsBean.getAgreementNumber());
		sampleCardsTemp.setAvgccsrating(updateCcsBean.getAvgCcsRating());
		sampleCardsTemp.setBrix(updateCcsBean.getBrix());
		sampleCardsTemp.setCcsrating(updateCcsBean.getCcsRating());
		sampleCardsTemp.setExtentsize(updateCcsBean.getExtentSize());
		
		//Modified by DMurty on 10-08-2016
		List<AgreementSummary> agreementSummary = ahFuctionalService.getAgreementDetails(updateCcsBean.getAgreementNumber());
		if(agreementSummary.get(0).getFamilygroupcode() != null)
		{
			sampleCardsTemp.setFamilygroupcode(agreementSummary.get(0).getFamilygroupcode());
		}
		else
		{
			sampleCardsTemp.setFamilygroupcode(0);
		}
		
		double fAvgCcsRating = 0.0;
		List FAvgList = ahFuctionalService.getFamilyAvgCcsratingsByPlotno(updateCcsBean.getPlotNumber(), updateCcsBean.getProgram(),agreementSummary.get(0).getFamilygroupcode());
		if (FAvgList != null && FAvgList.size() > 0)
		{
			if (!FAvgList.contains(null)) // oldDate.equals(returndate.trim()))
			{
				fAvgCcsRating = (Double) FAvgList.get(0);
			}
		}
		if(fAvgCcsRating >0)
		{
			sampleCardsTemp.setFgavgccsrating(fAvgCcsRating);
		}
		else
		{
			sampleCardsTemp.setFgavgccsrating(updateCcsBean.getAvgCcsRating());
		}
		
		//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(updateCcsBean.getSeason(), updateCcsBean.getAgreementNumber());
		List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(updateCcsBean.getAgreementNumber(),updateCcsBean.getPlotNumber(),updateCcsBean.getSeason());

		sampleCardsTemp.setPlantorratoon((byte) Integer.parseInt(agreementDetails.get(0).getPlantorratoon()));
				
		sampleCardsTemp.setPlotno(updateCcsBean.getPlotNumber());
		sampleCardsTemp.setPol(updateCcsBean.getPol());
		sampleCardsTemp.setProgramnumber(updateCcsBean.getProgram());
		sampleCardsTemp.setPurity(updateCcsBean.getPurity());
		sampleCardsTemp.setRyotcode(updateCcsBean.getRyotCode());
		sampleCardsTemp.setSamplecard(updateCcsBean.getSamplecard());
		sampleCardsTemp.setSeason(updateCcsBean.getSeason());

		return sampleCardsTemp;
	}

	@RequestMapping(value = "/saveUpdateAverageCcsRatings", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveUpdateAverageCcsRatings(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		String strQry = null;
		try 
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject formData = (JSONObject) jsonData.get("formData");

			String season = formData.getString("season");
			String flag = formData.getString("Flag");
			String Page = formData.getString("pageName");
			String nprogram = formData.getString("programno");

			for (int i = 0; i < gridData.size(); i++)
			{
				UpdateCcsBean updateCcsBean = new ObjectMapper().readValue(gridData.get(i).toString(), UpdateCcsBean.class);

				String ryotCode = updateCcsBean.getRyotCode();
				String agreementNo = updateCcsBean.getAgreementNumber();
				String plotNo = updateCcsBean.getPlotNumber();
				double fAvgCcsRating = updateCcsBean.getFgAvgCcsRating();
				double avgCcsRating = updateCcsBean.getAvgCcsRating();
				
				double ccsRating = 0.0;
				if(updateCcsBean.getCcsRating() != null)
				{
					ccsRating = updateCcsBean.getCcsRating();
				}
				
				int program = updateCcsBean.getProgram();

				/*Map TempMap = new HashMap();
				TempMap.put("RYOT_CODE", ryotCode);
				TempMap.put("AGREEMENT_NO", agreementNo);
				TempMap.put("PLOT_NO", plotNo);
				TempMap.put("F_CCS_RATING", fAvgCcsRating);
				TempMap.put("AVG_CCS_RATING", avgCcsRating);
				TempMap.put("CCS_RATING", ccsRating);
				TempMap.put("PROGRAM", program);
				TempMap.put("SEASON", season);*/
				
				strQry = "UPDATE SampleCards set avgccsrating = "+avgCcsRating+",fgavgccsrating="+fAvgCcsRating+" WHERE season ='"+season+"' and plotno='"+plotNo+"' and aggrementno='"+agreementNo+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				
				strQry = "UPDATE AgreementDetails set averageccsrating = "+avgCcsRating+",familygroupavgccsrating="+fAvgCcsRating+" WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				
				//ahFuctionalService.updateValuesforAvgCcsRating(TempMap);
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	@RequestMapping(value = "/getProgramnNumbersBySeason", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	JSONArray getProgramnNumbers(@RequestBody String jsonData) throws Exception
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		List dropdownList = ahFuctionalService.getProgramNumbersBySeason(jsonData);
		logger.info("========dropdownList==========" + dropdownList);
		try
		{
			if (dropdownList != null && dropdownList.size() > 0) 
			{
				for (Iterator it = dropdownList.iterator(); it.hasNext();) 
				{
					jsonObj = new JSONObject();
					Integer myResult = (Integer) it.next();
					jsonObj.put("programnumber", myResult);
					jArray.add(jsonObj);
				}
			}
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}

	@RequestMapping(value = "/saveLabCcsRating", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveLabCcsRating(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		String strQry = null;
		try
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");

			String brix = formData.getString("brix");
			String pol = formData.getString("pol");
			String purity = formData.getString("purity");
			double ccsRating = formData.getDouble("ccsRating");
			String season = formData.getString("season");
			String samplecard = formData.getString("samplecard");
			int nsamplecard = Integer.parseInt(samplecard);
			String program = formData.getString("program");
			int nprogram = Integer.parseInt(program);
			String sampledate=formData.getString("sampleDate");

			logger.info("========brix==========" + brix);
			logger.info("========pol==========" + pol);
			logger.info("========purity==========" + purity);
			logger.info("========ccsRating==========" + ccsRating);
			logger.info("========season==========" + season);
			logger.info("========samplecard==========" + samplecard);
			logger.info("========program==========" + program);
			
			strQry = "UPDATE SampleCards set brix= '"+brix+"',pol='"+pol+"',purity='"+purity+"',ccsrating="+ccsRating+",sampledate='"+DateUtils.getSqlDateFromString(sampledate,Constants.GenericDateFormat.DATE_FORMAT)+"' WHERE season ='"+season+"' and samplecard="+nsamplecard+" and programnumber="+program+"";
			Qryobj = strQry;
			UpdateList.add(Qryobj);
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			return isInsertSuccess;
			//ahFuctionalService.updateValuesforSampleCard(TempMap);
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	@RequestMapping(value = "/getAllAgrmntDetails", method = RequestMethod.POST)
	public @ResponseBody	List getAllAgrmntDetails(@RequestBody JSONObject jsonData) throws Exception 
	{
		SampleCardsBean sampleCardsBean = null;
		org.json.JSONObject response = new org.json.JSONObject();

		String season = (String) jsonData.get("season");
		Integer programno = (Integer) jsonData.get("programno");

		List<SampleCardsBean> sampleCardsBeans = prepareListofBeanForSampleCardDetails(ahFuctionalService.listAggrementsforSampleCards(season, programno));

		return sampleCardsBeans;
	}

	private List<SampleCardsBean> prepareListofBeanForSampleCardDetails(List<AgreementDetails> AgreementDetailss)
	{
		List<SampleCardsBean> beans = null;
		if (AgreementDetailss != null && !AgreementDetailss.isEmpty())
		{
			beans = new ArrayList<SampleCardsBean>();
			SampleCardsBean bean = null;
			for (AgreementDetails agreementDetails : AgreementDetailss)
			{
				bean = new SampleCardsBean();

				bean.setRyotCode(agreementDetails.getRyotcode());
				String ryotName = "NA";
				List<String> RyotNamesList = new ArrayList<String>();
				RyotNamesList = ahFuctionalService.getRyotName(agreementDetails
						.getRyotcode());
				if (RyotNamesList != null && RyotNamesList.size() > 0)
				{
					String myResult1 = RyotNamesList.get(0);
					ryotName = myResult1;
				}
				bean.setRyotName(ryotName);
				bean.setAgreementNumber(agreementDetails.getAgreementnumber());
				bean.setPlotNumber(agreementDetails.getPlotnumber());
				bean.setPlantOrRatooninAcres(agreementDetails.getExtentsize());

				int varietyCode = agreementDetails.getVarietycode();
				String varietyName = "NA";

				List<String> VarietyList = new ArrayList<String>();
				VarietyList = ahFuctionalService
						.getVarietyDetailsByCode(varietyCode);
				if (VarietyList != null && VarietyList.size() > 0) 
				{
					String myResult1 = VarietyList.get(0);
					varietyName = myResult1;
				}
				bean.setVariety(varietyName);
				//added by naidu because of misplacing cropdate by agreementdate
				bean.setDateOfPlantorRatoon(DateUtils.formatDate(agreementDetails.getCropdate(),Constants.GenericDateFormat.DATE_FORMAT));

				// need to send sample cards no based on PlantOrRatooninAcres
				double extentSize = agreementDetails.getExtentsize();
				int sampleCard = 0;
				List<Integer> SampleCardList = new ArrayList<Integer>();
				SampleCardList = ahFuctionalService.getSampleCardNo(extentSize);
				if (SampleCardList != null && SampleCardList.size() > 0)
				{
					int myResult1 = SampleCardList.get(0);
					sampleCard = myResult1;
				}
				String nsampleCard = Integer.toString(sampleCard);
				bean.setSampleCard(nsampleCard);
				bean.setSampleCardsId(agreementDetails.getSamplecards());
				bean.setSeason(agreementDetails.getSeasonyear());
				
				
				String agreementNum = agreementDetails.getAgreementnumber();
				List<AgreementSummary> agrmntSmry = ahFuctionalService.getAgreementSmryByAgreementNo(agreementDetails.getSeasonyear(),agreementNum);
				Byte status = agrmntSmry.get(0).getStatus();
				if(status==0)
				{
					beans.add(bean);
				}
				//beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveSampleCards", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray saveSampleCards(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		InetAddress ipAddr = InetAddress.getLocalHost(); 
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		String finalSampleCards = "";
		try
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject formData = (JSONObject) jsonData.get("formData");

			String season = formData.getString("season");
			String nprogram = formData.getString("programno");

			ahFuctionalService.deleteSampleCards(season, nprogram);
			for (int i = 0; i < gridData.size(); i++)
			{
				SampleCardsBean sampleCardsBean = new ObjectMapper().readValue(gridData.get(i).toString(), SampleCardsBean.class);
				
				int program = Integer.parseInt(nprogram);
				
				String sampleCardNos = sampleCardsBean.getSampleCardsId();
				//Added by Sahu on 01-08-2016 
				finalSampleCards += sampleCardNos+",";
				String ryotCode = sampleCardsBean.getRyotCode();
				String AgreementNo = sampleCardsBean.getAgreementNumber();
				season = sampleCardsBean.getSeason();
				String plotno = sampleCardsBean.getPlotNumber();
				
				String strQry = "UPDATE AgreementDetails set samplecards='"+sampleCardNos+"' WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+AgreementNo+"' and plotnumber='"+plotno+"'";
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				
				if (sampleCardNos.contains(","))
				{
					String totalsamplecards[] = sampleCardNos.split(",");
					for (int k = 0; k < totalsamplecards.length; k++)
					{
						SampleCards sampleCards = new SampleCards();

						String invnum = totalsamplecards[k];
						int ninvnum = Integer.parseInt(invnum);

						sampleCards.setRyotcode(sampleCardsBean.getRyotCode());
						sampleCards.setAggrementno(sampleCardsBean.getAgreementNumber());
						sampleCards.setExtentsize(sampleCardsBean.getPlantOrRatooninAcres());
						sampleCards.setPlotno(sampleCardsBean.getPlotNumber());
						sampleCards.setProgramnumber(program);
						sampleCards.setSeason(sampleCardsBean.getSeason());
						sampleCards.setSamplecard(ninvnum);
						
						//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, sampleCardsBean.getAgreementNumber());
						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(sampleCardsBean.getAgreementNumber(),sampleCardsBean.getPlotNumber(),sampleCardsBean.getSeason());

						String ptorrt = agreementDetails.get(0).getPlantorratoon();
						
						int nptorrt = Integer.parseInt(ptorrt);
						sampleCards.setPlantorratoon((byte)nptorrt);	
						
						entityList.add(sampleCards);
						//ahFuctionalService.addsampleCards(sampleCards);
					}
				} 
				else 
				{
					SampleCards sampleCards = new SampleCards();

					sampleCards.setRyotcode(sampleCardsBean.getRyotCode());
					sampleCards.setAggrementno(sampleCardsBean.getAgreementNumber());
					sampleCards.setExtentsize(sampleCardsBean.getPlantOrRatooninAcres());
					sampleCards.setPlotno(sampleCardsBean.getPlotNumber());
					sampleCards.setProgramnumber(program);
					sampleCards.setSeason(sampleCardsBean.getSeason());
					int ninvnum = Integer.parseInt(sampleCardNos);
					sampleCards.setSamplecard(ninvnum);

					//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, sampleCardsBean.getAgreementNumber());
					List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(sampleCardsBean.getAgreementNumber(),sampleCardsBean.getPlotNumber(),sampleCardsBean.getSeason());

					//sampleCards.setPlantorratoon((byte) Integer.parseInt(agreementDetails.get(0).getPlantorratoon()));				
					String ptorrt = agreementDetails.get(0).getPlantorratoon();
					int nptorrt = Integer.parseInt(ptorrt);
					sampleCards.setPlantorratoon((byte)nptorrt);	
					
					
					entityList.add(sampleCards);
					//ahFuctionalService.addsampleCards(sampleCards);
				}
				//isInsertSuccess = prepareModelForSampleCards(sampleCardsBean, nprogram);
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			//Modified by Sahu on 01-08-2016 
			if(isInsertSuccess == true)
			{
				String rptformat = "pdf";
				jsonObj.put("sampleCard", "string:"+finalSampleCards);
				jsonObj.put("status", "pdf");
				jsonObj.put("issuccess", "true");
				jsonObj.put("serverip", ipAddr.getHostAddress());
				jArray.add(jsonObj);
			}
			else
			{
				jsonObj.put("issuccess", "false");
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return jArray;
		}
	}

	private boolean prepareModelForSampleCards(	SampleCardsBean sampleCardsBean, String nprogram)
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		Object Qryobj = null;
		SampleCards sampleCards = new SampleCards();
		int program = Integer.parseInt(nprogram);

		String sampleCardNos = sampleCardsBean.getSampleCardsId();
		String ryotCode = sampleCardsBean.getRyotCode();
		String AgreementNo = sampleCardsBean.getAgreementNumber();
		String season = sampleCardsBean.getSeason();
		String plotno = sampleCardsBean.getPlotNumber();
		
		String strQry = "UPDATE AgreementDetails set samplecards='"+sampleCardNos+"' WHERE ryotcode ='"+ryotCode+"' and agreementnumber='"+AgreementNo+"' and plotnumber='"+plotno+"'";
		Qryobj = strQry;
		UpdateList.add(Qryobj);

		//ahFuctionalService.updateSampleCardInAgreementDetails(ryotCode,	AgreementNo, sampleCardNos, plotno);
		if (sampleCardNos.contains(","))
		{
			String totalsamplecards[] = sampleCardNos.split(",");
			for (int k = 0; k < totalsamplecards.length; k++)
			{
				String invnum = totalsamplecards[k];
				int ninvnum = Integer.parseInt(invnum);

				sampleCards.setRyotcode(sampleCardsBean.getRyotCode());
				sampleCards.setAggrementno(sampleCardsBean.getAgreementNumber());
				sampleCards.setExtentsize(sampleCardsBean.getPlantOrRatooninAcres());
				sampleCards.setPlotno(sampleCardsBean.getPlotNumber());
				sampleCards.setProgramnumber(program);
				sampleCards.setSeason(sampleCardsBean.getSeason());
				sampleCards.setSamplecard(ninvnum);
				
				//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, sampleCardsBean.getAgreementNumber());
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(sampleCardsBean.getAgreementNumber(),sampleCardsBean.getPlotNumber(),sampleCardsBean.getSeason());

				sampleCards.setPlantorratoon((byte) Integer.parseInt(agreementDetails.get(0).getPlantorratoon()));	
				
				entityList.add(sampleCards);
				//ahFuctionalService.addsampleCards(sampleCards);
			}
		} 
		else 
		{
			sampleCards.setRyotcode(sampleCardsBean.getRyotCode());
			sampleCards.setAggrementno(sampleCardsBean.getAgreementNumber());
			sampleCards.setExtentsize(sampleCardsBean.getPlantOrRatooninAcres());
			sampleCards.setPlotno(sampleCardsBean.getPlotNumber());
			sampleCards.setProgramnumber(program);
			sampleCards.setSeason(sampleCardsBean.getSeason());
			int ninvnum = Integer.parseInt(sampleCardNos);
			sampleCards.setSamplecard(ninvnum);

			//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, sampleCardsBean.getAgreementNumber());
			List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(sampleCardsBean.getAgreementNumber(),sampleCardsBean.getPlotNumber(),sampleCardsBean.getSeason());

			sampleCards.setPlantorratoon((byte) Integer.parseInt(agreementDetails.get(0).getPlantorratoon()));				
			entityList.add(sampleCards);
			//ahFuctionalService.addsampleCards(sampleCards);
		}
		isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
		return isInsertSuccess;
	}

	// --------------------------- Test Weighment ----------------------------------------------------------------------//
	@RequestMapping(value = "/saveWeighBridgeTestData", method = RequestMethod.POST)
	public @ResponseBody JSONArray saveWeighBridgeTestData(@RequestBody JSONObject jsonData) throws JsonParseException, JsonMappingException, Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		try 
		{
			WeighBridgeTestDataBean weighBridgeTestDataBean = new ObjectMapper().readValue(jsonData.toString(),WeighBridgeTestDataBean.class);
			WeighBridgeTestData weighBridgeTestData = prepareModelForWeighBridgeTestData(weighBridgeTestDataBean);
			entityList.add(weighBridgeTestData);
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			if(isInsertSuccess)
			{
				jsonObj.put("isInsertSuccess", isInsertSuccess);
				jsonObj.put("ryotname", "NA");
				jsonObj.put("serialnumber", "NA");
				jsonObj.put("ryotcode", "NA");
				jsonObj.put("landvilcode", "NA");
				jsonObj.put("landvillageName", "NA");
				jsonObj.put("shiftid", weighBridgeTestData.getShiftid());
				jsonObj.put("canereceiptdate", weighBridgeTestDataBean.getTestWeighmentDate());
				jsonObj.put("canereceipttime", weighBridgeTestDataBean.getTestWeighmentTime());
				jsonObj.put("weighbridgeslno", weighBridgeTestDataBean.getBridgeName());
				jsonObj.put("vehicletype", "NA");
				jsonObj.put("vehicleno", "NA");
				jsonObj.put("grossweight", weighBridgeTestData.getTotalweight());
				jsonObj.put("netwt", weighBridgeTestData.getTotalweight());
				jsonObj.put("lorrywt", weighBridgeTestData.getTotalweight());
				jsonObj.put("relativeName", "NA");
				jsonObj.put("villageName", "NA");
				jsonObj.put("varietyName", "NA");  
				jsonObj.put("circleName", "NA");  
				jsonObj.put("cropName", "NA"); 
				jsonObj.put("villagecode", "NA");
				jsonObj.put("circlecode", "NA");
				//Added by DMurty on 05-12-2016
				String login = null;
				String loginUser = loginController.getLoggedInUserName();
				List EmployeesList = commonService.getEmployeeDetails(loginUser);
				if(EmployeesList != null && EmployeesList.size()>0)
				{
					Employees emp = (Employees) EmployeesList.get(0);
					login = emp.getEmployeename();
				}
				jsonObj.put("login", login);
				
				jArray.add(jsonObj);
			}
			else
			{
				jsonObj.put("isInsertSuccess", "false");
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return jArray;
		}
	}

	private WeighBridgeTestData prepareModelForWeighBridgeTestData(	WeighBridgeTestDataBean weighBridgeTestDataBean)
	{
		WeighBridgeTestData weighBridgeTestDetails = new WeighBridgeTestData();
		
		weighBridgeTestDetails.setTestweighmentdate(DateUtils.getSqlDateFromString(weighBridgeTestDataBean.getTestWeighmentDate(),Constants.GenericDateFormat.DATE_FORMAT));
		weighBridgeTestDetails.setTestweighmenttime(java.sql.Time.valueOf(weighBridgeTestDataBean.getTestWeighmentTime()));
		weighBridgeTestDetails.setShiftid(weighBridgeTestDataBean.getShift());
		weighBridgeTestDetails.setWeighbridgeid(weighBridgeTestDataBean.getBridgeName());
		weighBridgeTestDetails.setOperatorid(weighBridgeTestDataBean.getOperatorName());
		weighBridgeTestDetails.setTotalweight(weighBridgeTestDataBean.getTotalWeight());

		return weighBridgeTestDetails;
	}

	// --------------------------- WeighBridge Test Weighment ----------------------------------------------------------------------//

	// ----------------------------------------------Update Loans---------------------------------------------------//
	@RequestMapping(value = "/getAllUpdateLoans", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	List<UpdateLoanDetailsBean> getAllUpdateLoans(	@RequestBody JSONObject jsonData) throws Exception 
	{
		org.json.JSONObject response = new org.json.JSONObject();
		String season = (String) jsonData.get("season");
		Integer branchcode = (Integer) jsonData.get("branchcode");		
	
		List<UpdateLoanDetailsBean> updateLoanDetailsBeans = prepareListofUpdateLoansBeans(ahFuctionalService.getLoanDetailsForUpdateLoan(season, branchcode));
		
		return updateLoanDetailsBeans;
	}

	private List<UpdateLoanDetailsBean> prepareListofUpdateLoansBeans(List<LoanDetails> loanDetails)
	{
		List<UpdateLoanDetailsBean> beans = null;
		if (loanDetails != null && !loanDetails.isEmpty())
		{
			beans = new ArrayList<UpdateLoanDetailsBean>();
			UpdateLoanDetailsBean bean = null;
			for (LoanDetails loanDetail : loanDetails) 
			{
				String accNum="NA";
				bean = new UpdateLoanDetailsBean();
				Ryot ryot = null;
				bean.setRyotCode(loanDetail.getRyotcode());
				List<Ryot> ryotList = ryotService.getRyotByRyotCode(loanDetail.getRyotcode());
				if (ryotList.size() > 0 && ryotList != null) 
				{
					ryot = (Ryot) ryotList.get(0);
					bean.setRyotName(ryot.getRyotname());
				}
				//bean.setLoanNumber(loanDetail.getLoannumber());
				
				if(loanDetail.getRecommendeddate()!=null)
				{
					//Modified by DMurty on 27-10-2016
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strrecommendedDate = df.format(loanDetail.getRecommendeddate());
					bean.setRecommendedDate(strrecommendedDate);
				}
				
				bean.setPaidAmount(loanDetail.getPrinciple());
				
				//bean.setReferenceNumber(loanDetail.getReferencenumber());
				//Bug Id--05038
				bean.setDisbursedAmount(loanDetail.getDisbursedamount());
				if(loanDetail.getDisburseddate() != null)
				{
					//Modified by DMurty on 05-11-2016
					Date disburseDate = loanDetail.getDisburseddate();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String strdisburseDate = df.format(disburseDate);
					bean.setDisbursedDate(strdisburseDate);
				}
				
				bean.setLoanNumber(loanDetail.getCompositePrKeyForLoans().getLoannumber());
				bean.setTransactioncode(loanDetail.getTransactioncode());
				
				if(loanDetail.getTransactioncode() > 0)
				{
					bean.setModifyFlag("Yes");
				}
				else
				{
					bean.setModifyFlag("No");
				}
				//Modified by DMurty on 21-02-2017
				List AccountNumberList = commonService.getAccountNumber(loanDetail.getRyotcode(),loanDetail.getCompositePrKeyForLoans().getSeason());
				if(AccountNumberList!=null)
				{
					Map accNumMap=(Map)AccountNumberList.get(0);
					accNum=(String)accNumMap.get("accountnumber");
				}
				//Modified by DMurty on 27-08-2016
				bean.setReferenceNumber(loanDetail.getReferencenumber());
				beans.add(bean);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveUpdateLoans", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean saveUpdateLoans(@RequestBody JSONObject jsonData) throws Exception
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		List AccountsList = new ArrayList();

		//Added by DMurty on 12-11-2016
		Map AccountSummaryMap = new HashMap();
		Map AccountGroupMap = new HashMap();
		Map AccountSubGroupMap = new HashMap();
		
		List AccountCodeList = new ArrayList();
		List AccountGrpCodeList = new ArrayList();
		List AccountSubGrpCodeList = new ArrayList();

		Object Qryobj = null;
		Map TempMap = null;
		try 
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			String season = formData.getString("season");
			Integer branchId = formData.getInt("branchcode");			
			//Modified by DMurty on 04-12-2016 for single row update and multiple rows update.
			String rowflag = (String) jsonData.get("flag");

			int TransactionCode = 0;
			int newTransactionCode = commonService.GetMaxTransCode(season);
			int finaltranscode=newTransactionCode;
			
			if("Single".equalsIgnoreCase(rowflag))
			{
				JSONObject gridData=(JSONObject) jsonData.get("griddata");
				
				String ryotCode=gridData.getString("ryotCode");
				String ryotName=gridData.getString("ryotName");
				String loanNumber=gridData.getString("loanNumber");
				String recommendedDate=gridData.getString("recommendedDate");
				double paidAmount=gridData.getDouble("paidAmount");
				String referenceNumber=gridData.getString("referenceNumber");
				String disbursedAmount=gridData.getString("disbursedAmount");
				String disbursedDate=gridData.getString("disbursedDate");
				String modifyFlag=gridData.getString("modifyFlag");
				int tcode = gridData.getInt("transactioncode");
				if(referenceNumber != null)
				{
					LoanPrincipleAmountsBean loanPrincipleAmountsBean = new ObjectMapper().readValue(gridData.toString(0).toString(),LoanPrincipleAmountsBean.class);

					LoanPrincipleAmounts loanPrincipleAmounts = prepareModelForLoanPrincipleAmounts(loanPrincipleAmountsBean, season,branchId);
					entityList.add(loanPrincipleAmounts);
					
					TempMap = new HashMap();
					TempMap.put("ACCOUNT_NUMBER", loanPrincipleAmountsBean.getReferenceNumber());
					TempMap.put("BRANCH_CODE", branchId);
					TempMap.put("RYOT_CODE", loanPrincipleAmountsBean.getRyotCode());
					int accType = 1;
					TempMap.put("ACC_TYPE", accType);

					RyotBankDetails ryotBankDetails = prepareModelForRyotBranch(TempMap);
					
					entityList.add(ryotBankDetails);
					
					String flag = loanPrincipleAmountsBean.getModifyFlag();
					if("Yes".equalsIgnoreCase(flag))
					{
						TransactionCode = loanPrincipleAmountsBean.getTransactioncode();
						finaltranscode = TransactionCode;
						
						logger.info("========finaltranscode=========="	+ finaltranscode);

						// Reverting Account tables here
						commonService.RevertAccountTables(finaltranscode);
					}
					else
					{
						finaltranscode = finaltranscode;
					}
					int loanNo = loanPrincipleAmountsBean.getLoanNumber();
					Date loanDate = DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT);
					String strQry = "UPDATE LoanDetails set disbursedamount= "+ loanPrincipleAmountsBean.getDisbursedAmount()+", disburseddate= '"+DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT)+"',referencenumber='"+loanPrincipleAmountsBean.getReferenceNumber()+"',paidamount=0,pendingamount="+loanPrincipleAmountsBean.getDisbursedAmount()+",transactioncode="+finaltranscode+" WHERE loannumber ="+loanNo+" and Season='"+season+"' and branchcode="+branchId+"";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
					
				
					strQry = "UPDATE LoanSummary set disbursedamount= "+loanPrincipleAmountsBean.getDisbursedAmount()+",totalamount="+loanPrincipleAmountsBean.getDisbursedAmount()+" WHERE loannumber ="+loanNo+" and season='"+season+"' and branchcode="+branchId+"";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
					
					double Amt = 0.0;
					String loginuser = "NA";
					String AccountCode = "";
					int AccGrpCode = 0;
					String RyotCode = loanPrincipleAmountsBean.getRyotCode();
					
					String glCode = "";
					List<Branch> branch = ahFuctionalService.getBranchDtls(branchId);
					glCode = branch.get(0).getGlcode().toString();
					String BankglCode = branch.get(0).getGlcode().toString();
					
					if (branchId != null)
					{
						if(branch != null && branch.size()>0)
						{
							
							Amt = loanPrincipleAmounts.getPrinciple();
							//added by sahadeva 27/10/2016
							loginuser = loginController.getLoggedInUserName();
							AccountCode = glCode;
							AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);

							Map DtlsMap = new HashMap();

							DtlsMap.put("TRANSACTION_CODE", finaltranscode);
							DtlsMap.put("TRANSACTION_AMOUNT", Amt);
							DtlsMap.put("LOGIN_USER", loginuser);
							DtlsMap.put("ACCOUNT_CODE", AccountCode);
							DtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							DtlsMap.put("TRANSACTION_TYPE", "Cr");
							DtlsMap.put("GL_CODE", RyotCode);
							//Added by DMurty on 18-10-2016
							DtlsMap.put("SEASON", season);
							
							AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
							entityList.add(accountDetails);
							
							// Insert Rec in AccSmry AccSmryList
							String TransactionType = "Cr";
							String strtype = "";
							AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
							
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", AccountCode);
							boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
							if (advAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(AccountCode,accountSummary);
							}
							else
							{
								accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
								String straccCode = accountSummary.getAccountcode();
								if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", AccountCode);
									tmpMap.put("KEY", AccountCode);
									Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
								}
							}
							
							// Insert Rec in AccGrpDetails
							Map AccGrpDtlsMap = new HashMap();

							AccGrpDtlsMap.put("TRANSACTION_CODE", finaltranscode);
							AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							AccGrpDtlsMap.put("LOGIN_USER", loginuser);
							AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
							AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
							//Added by DMurty on 18-10-2016
							AccGrpDtlsMap.put("SEASON", season);
							
							AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
							entityList.add(accountGroupDetails);

							TransactionType = "Cr";
							AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (advAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
							}
							else
							{
								accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
									Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
							
							// Insert Rec in AccSubGrpDtls
							Map AccSubGrpDtlsMap = new HashMap();
							
							int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
							String strAccSubGrpCode = Integer.toString(AccSubGCode);

							AccSubGrpDtlsMap.put("TRANSACTION_CODE", finaltranscode);
							AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
							AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
							AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
							//Added by DMurty on 18-10-2016
							AccSubGrpDtlsMap.put("SEASON", season);
							
							AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
							entityList.add(accountSubGroupDetails);

							TransactionType = "Cr";
							AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
							
							boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if(advAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
							}
							else
							{
								accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}
					}
					AccountCode = "RTL";			
					if (AccountCode != null)
					{
						Amt = loanPrincipleAmounts.getPrinciple();
						AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						RyotCode  = loanPrincipleAmountsBean.getRyotCode();
						loginuser = loginController.getLoggedInUserName();

						Map DtlsMap = new HashMap();
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Loan");
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						DtlsMap.put("GL_CODE", BankglCode);
						DtlsMap.put("SEASON", season);

						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
						
						String TransactionType = "Dr";
						String strtype = "";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
						
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();

						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccGrpDtlsMap.put("SEASON", season);

						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);

						TransactionType = "Dr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						
						// Insert Rec in AccSubGrpDtls
						Map AccSubGrpDtlsMap = new HashMap();

						int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
						String strAccSubGrpCode = Integer.toString(AccSubGCode);

						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO","Towards Loan");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccSubGrpDtlsMap.put("SEASON", season);

						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);

						TransactionType = "Dr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
			}
			else
			{
				JSONArray gridData = (JSONArray) jsonData.get("griddata");
				logger.info("========saveUpdateLoans()========== gridData.size()---"+gridData.size());
				for (int i = 0; i < gridData.size(); i++)
				{
					LoanPrincipleAmountsBean loanPrincipleAmountsBean = new ObjectMapper().readValue(gridData.getString(i).toString(),LoanPrincipleAmountsBean.class);
					ahFuctionalService.deleteLoanPrincipleAmounts(loanPrincipleAmountsBean.getRyotCode(),season,branchId,loanPrincipleAmountsBean.getLoanNumber());
					logger.info("========saveUpdateLoans()========== i value---"+i);
					if(loanPrincipleAmountsBean.getReferenceNumber() == null)
					{
						continue;
					}
					
					LoanPrincipleAmounts loanPrincipleAmounts = prepareModelForLoanPrincipleAmounts(loanPrincipleAmountsBean, season,branchId);
					entityList.add(loanPrincipleAmounts);
					
					TempMap = new HashMap();
					TempMap.put("ACCOUNT_NUMBER", loanPrincipleAmountsBean.getReferenceNumber());
					TempMap.put("BRANCH_CODE", branchId);
					TempMap.put("RYOT_CODE", loanPrincipleAmountsBean.getRyotCode());
					int accType = 1;
					TempMap.put("ACC_TYPE", accType);

					RyotBankDetails ryotBankDetails = prepareModelForRyotBranch(TempMap);
					
					
					entityList.add(ryotBankDetails);
					
					String flag = loanPrincipleAmountsBean.getModifyFlag();
					if("Yes".equalsIgnoreCase(flag))
					{
						TransactionCode = loanPrincipleAmountsBean.getTransactioncode();
						finaltranscode = TransactionCode;
						
						logger.info("========finaltranscode=========="	+ finaltranscode);

						// Reverting Account tables here
						commonService.RevertAccountTables(finaltranscode);
					}
					else
					{
						TransactionCode = newTransactionCode;
						
						if(i>0)
							finaltranscode++;
						else
							finaltranscode = TransactionCode;
					}
					int loanNo = loanPrincipleAmountsBean.getLoanNumber();
					Date loanDate = DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT);
					
					String strQry = "UPDATE LoanDetails set disbursedamount= "+ loanPrincipleAmountsBean.getDisbursedAmount()+", disburseddate= '"+DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT)+"',referencenumber='"+loanPrincipleAmountsBean.getReferenceNumber()+"',paidamount=0,pendingamount="+loanPrincipleAmountsBean.getDisbursedAmount()+",transactioncode="+finaltranscode+" WHERE loannumber ='"+loanNo+"' and Season='"+season+"' and branchcode="+branchId+"";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				
					strQry = "UPDATE LoanSummary set disbursedamount= "+loanPrincipleAmountsBean.getDisbursedAmount()+",totalamount="+loanPrincipleAmountsBean.getDisbursedAmount()+" WHERE loannumber ="+loanNo+" and season='"+season+"' and branchcode="+branchId+"";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
					
					double Amt = 0.0;
					String loginuser = "NA";
					String AccountCode = "";
					int AccGrpCode = 0;
					String RyotCode = loanPrincipleAmountsBean.getRyotCode();
					
					String glCode = "";
					List<Branch> branch = ahFuctionalService.getBranchDtls(branchId);
					glCode = branch.get(0).getGlcode().toString();
					String BankglCode = branch.get(0).getGlcode().toString();
					
					if (branchId != null)
					{
						if(branch != null && branch.size()>0)
						{
							
							Amt = loanPrincipleAmounts.getPrinciple();
							loginuser = loginController.getLoggedInUserName();
							AccountCode = glCode;
							AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);

							Map DtlsMap = new HashMap();

							DtlsMap.put("TRANSACTION_CODE", finaltranscode);
							DtlsMap.put("TRANSACTION_AMOUNT", Amt);
							DtlsMap.put("LOGIN_USER", loginuser);
							DtlsMap.put("ACCOUNT_CODE", AccountCode);
							DtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							DtlsMap.put("TRANSACTION_TYPE", "Cr");
							DtlsMap.put("GL_CODE", RyotCode);
							//Added by DMurty on 18-10-2016
							DtlsMap.put("SEASON", season);
							
							AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
							entityList.add(accountDetails);
							
							// Insert Rec in AccSmry AccSmryList
							String TransactionType = "Cr";
							String strtype = "";
							AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
							
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", AccountCode);
							boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
							if (advAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(AccountCode,accountSummary);
							}
							else
							{
								accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
								String straccCode = accountSummary.getAccountcode();
								if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", AccountCode);
									tmpMap.put("KEY", AccountCode);
									Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
								}
							}
							
							// Insert Rec in AccGrpDetails
							Map AccGrpDtlsMap = new HashMap();

							AccGrpDtlsMap.put("TRANSACTION_CODE", finaltranscode);
							AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							AccGrpDtlsMap.put("LOGIN_USER", loginuser);
							AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
							AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
							//Added by DMurty on 18-10-2016
							AccGrpDtlsMap.put("SEASON", season);
							
							AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
							entityList.add(accountGroupDetails);

							TransactionType = "Cr";
							AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
							boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (advAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
							}
							else
							{
								accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
									Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
							
							// Insert Rec in AccSubGrpDtls
							Map AccSubGrpDtlsMap = new HashMap();
							
							int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
							String strAccSubGrpCode = Integer.toString(AccSubGCode);

							AccSubGrpDtlsMap.put("TRANSACTION_CODE", finaltranscode);
							AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
							AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
							AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
							AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
							AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
							//Added by DMurty on 18-10-2016
							AccSubGrpDtlsMap.put("SEASON", season);
							
							AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
							entityList.add(accountSubGroupDetails);

							TransactionType = "Cr";
							AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
							Map AccSubGrpSmry = new HashMap();
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
							
							boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if(advAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
							}
							else
							{
								accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						}
					}
					AccountCode = "RTL";			
					if (AccountCode != null)
					{
						Amt = loanPrincipleAmounts.getPrinciple();
						AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
						String strAccGrpCode = Integer.toString(AccGrpCode);

						RyotCode  = loanPrincipleAmountsBean.getRyotCode();
						loginuser = loginController.getLoggedInUserName();

						Map DtlsMap = new HashMap();
						DtlsMap.put("TRANSACTION_CODE", TransactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", Amt);
						DtlsMap.put("LOGIN_USER", loginuser);
						DtlsMap.put("ACCOUNT_CODE", AccountCode);
						DtlsMap.put("JOURNAL_MEMO", "Towards Loan");
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						DtlsMap.put("GL_CODE", BankglCode);
						DtlsMap.put("SEASON", season);

						AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
						entityList.add(accountDetails);
						
						String TransactionType = "Dr";
						String strtype = "";
						AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
						
						Map AccSmry = new HashMap();
						AccSmry.put("ACC_CODE", AccountCode);
						boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
						if (advAccCodetrue == false)
						{
							AccountCodeList.add(AccSmry);
							AccountSummaryMap.put(AccountCode,accountSummary);
						}
						else
						{
							accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
							String straccCode = accountSummary.getAccountcode();
							if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
							{
								double runbal = accountSummary.getRunningbalance();
								String baltype = accountSummary.getBalancetype();
								String ses = accountSummary.getSeason();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_CODE", AccountCode);
								tmpMap.put("KEY", AccountCode);
								Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
								AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
							}
						}
						
						// Insert Rec in AccGrpDetails
						Map AccGrpDtlsMap = new HashMap();

						AccGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan");
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccGrpDtlsMap.put("SEASON", season);

						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
						entityList.add(accountGroupDetails);

						TransactionType = "Dr";
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccGrpSmry = new HashMap();
						AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
						boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
						if (advAccGrpCodetrue == false)
						{
							AccountGrpCodeList.add(AccGrpSmry);
							AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
						}
						else
						{
							accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
							int accountGrpCode = accountGroupSummary.getAccountgroupcode();
							if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
							{
								double runbal = accountGroupSummary.getRunningbalance();
								String baltype = accountGroupSummary.getBalancetype();
								String ses = accountGroupSummary.getSeason();
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("KEY", strAccGrpCode);
								Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
								AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
							}
						}
						
						
						// Insert Rec in AccSubGrpDtls
						Map AccSubGrpDtlsMap = new HashMap();

						int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
						String strAccSubGrpCode = Integer.toString(AccSubGCode);

						AccSubGrpDtlsMap.put("TRANSACTION_CODE", TransactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
						AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
						AccSubGrpDtlsMap.put("JOURNAL_MEMO","Towards Loan");
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						AccSubGrpDtlsMap.put("SEASON", season);

						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
						entityList.add(accountSubGroupDetails);

						TransactionType = "Dr";
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
						Map AccSubGrpSmry = new HashMap();
						AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
						
						boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
						if(advAccSubGrpCodetrue == false)
						{
							AccountSubGrpCodeList.add(AccSubGrpSmry);
							AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
						}
						else
						{
							accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
							int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
							int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
							String ses = accountSubGroupSummary.getSeason();
							if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
							{
								double runbal = accountSubGroupSummary.getRunningbalance();
								String baltype = accountSubGroupSummary.getBalancetype();
								
								Map tmpMap = new HashMap();
								tmpMap.put("RUN_BAL", runbal);
								tmpMap.put("AMT", Amt);
								tmpMap.put("BAL_TYPE", baltype);
								tmpMap.put("SEASON", ses);
								tmpMap.put("TRANSACTION_TYPE", TransactionType);
								tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
								tmpMap.put("KEY", strAccSubGrpCode);
								Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
								AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
							}
						}
					}
				}
			}
			
			//Added by DMurty on 12-11-2016
			Iterator entries = AccountSummaryMap.entrySet().iterator();
			while (entries.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries.next();
			    Object value = entry.getValue();
			    AccountSummary accountSummary = (AccountSummary) value;
			    
			    String accountCode = accountSummary.getAccountcode();
			    String sesn = accountSummary.getSeason();
			    double runnbal = accountSummary.getRunningbalance();
			    String crOrDr = accountSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSummary);
			    }
			}
			
			Iterator entries1 = AccountGroupMap.entrySet().iterator();
			while (entries1.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries1.next();
			    Object value = entry.getValue();
			    
			    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
			    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
			    String sesn = accountGroupSummary.getSeason();
			    double runnbal = accountGroupSummary.getRunningbalance();
			    String crOrDr = accountGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountGroupSummary);
			    }
			}
			
			Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
			while (entries2.hasNext())
			{
			    Map.Entry entry = (Map.Entry) entries2.next();
			    Object value = entry.getValue();
			    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
			   
			    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
			    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
			    String sesn = accountSubGroupSummary.getSeason();
			    double runnbal = accountSubGroupSummary.getRunningbalance();
			    String crOrDr = accountSubGroupSummary.getBalancetype();
			    
			    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
			    if(AccList != null && AccList.size()>0)
			    {
			    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
			    	Qryobj = qry;
			    	UpdateList.add(Qryobj);
			    }
			    else
			    {
			    	AccountsList.add(accountSubGroupSummary);
			    }
			}
			
			logger.info("SaveUpdateLoans()========entityList=========="+entityList);
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
			logger.info("========SaveUpdateLoans entityList=========="+entityList);
			return isInsertSuccess;
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}

	private RyotBankDetails prepareModelForRyotBranchDetails(LoanPrincipleAmountsBean loanPrincipleAmountsBean, Integer branchId)
	{
		RyotBankDetails ryotBankDetails = new RyotBankDetails();
		Branch branch = (Branch) commonService.getEntityById(branchId,Branch.class);
		// ryotBankDetails.setId(ryotBankDetailsBean.getId());
		ryotBankDetails.setAccountnumber(loanPrincipleAmountsBean.getReferenceNumber());
		ryotBankDetails.setAccounttype((byte) 0);
		ryotBankDetails.setIfsccode(branch.getIfsccode());
		ryotBankDetails.setRyotbankbranchcode(branchId);
		ryotBankDetails.setRyotcode(loanPrincipleAmountsBean.getRyotCode());

		return ryotBankDetails;
	}

	private LoanPrincipleAmounts prepareModelForLoanPrincipleAmounts(LoanPrincipleAmountsBean loanPrincipleAmountsBean, String season,Integer branchId)
	{
		Date date = new Date();
		LoanPrincipleAmounts loanPrincipleAmounts = new LoanPrincipleAmounts();

		// loanPrincipleAmounts.setId(loanPrincipleAmountsBean.getId());
		loanPrincipleAmounts.setSeason(season);
		loanPrincipleAmounts.setRyotcode(loanPrincipleAmountsBean.getRyotCode());
		loanPrincipleAmounts.setLoanaccountnumber(loanPrincipleAmountsBean.getReferenceNumber());
		loanPrincipleAmounts.setPrinciple(loanPrincipleAmountsBean.getDisbursedAmount());
		//Added by DMurty on 27-08-2016
		//Date loanDate = DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT);
		//Added by naidu on 07-11-2016 
		loanPrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(loanPrincipleAmountsBean.getDisbursedDate(),Constants.GenericDateFormat.DATE_FORMAT));
		loanPrincipleAmounts.setDateofupdate(date);
		
		loanPrincipleAmounts.setBranchcode(branchId);
		loanPrincipleAmounts.setLoannumber(loanPrincipleAmountsBean.getLoanNumber());
		
		return loanPrincipleAmounts;
	}

	// ----------------------------------------------End of Update Loans---------------------------------------------------//
	// ---------------------------------- Generate Ranking ----------------------------------------------------------------//
	@RequestMapping(value = "/getAgreementDetailsForRanking", method = RequestMethod.POST)
	public @ResponseBody	List getAgreementDetailsForRanking(@RequestBody JSONObject jsonData)throws Exception 
	{
		GenerateRankingBean generateRankingBean = null;
		org.json.JSONObject response = new org.json.JSONObject();

		String season = (String) jsonData.get("season");
		Integer programno = (Integer) jsonData.get("programno");
		String pageName = (String) jsonData.get("pageName");
		String fromCcsRating=(String)jsonData.get("fromCcsRating");
		String toCcsRating=(String)jsonData.get("toCcsRating");
		List<GenerateRankingBean> generateRankingBeans = prepareListofBeanForRanking(ahFuctionalService.listAggrementsforRanking(season, programno,	pageName,fromCcsRating,toCcsRating), pageName);

		return generateRankingBeans;
	}

	private List<GenerateRankingBean> prepareListofBeanForRanking(List<AgreementDetails> AgreementDetailss, String pageName)
	{
		List<GenerateRankingBean> beans = null;
		if (AgreementDetailss != null && !AgreementDetailss.isEmpty())
		{
			try{
			beans = new ArrayList<GenerateRankingBean>();
			GenerateRankingBean bean = null;
			for (AgreementDetails agreementDetails : AgreementDetailss) 
			{
				bean = new GenerateRankingBean();

				int zoneCode = agreementDetails.getZonecode();
				String zoneName = "NA";
				List<String> ZoneList = new ArrayList<String>();
				ZoneList = ahFuctionalService.getZone(zoneCode);
				if (ZoneList != null && ZoneList.size() > 0) 
				{
					String myResult1 = ZoneList.get(0);
					zoneName = myResult1;
				}
				bean.setZone(zoneName);

				int circleCode = agreementDetails.getCirclecode();
				String circleName = "NA";
				List<String> CircleList = new ArrayList<String>();
				CircleList = ahFuctionalService.getCircle(circleCode);
				if (CircleList != null && CircleList.size() > 0)
				{
					String myResult1 = CircleList.get(0);
					circleName = myResult1;
				}
				bean.setCircle(circleName);

				bean.setRyotCode(agreementDetails.getRyotcode());
				String ryotName = "NA";
				List<String> RyotNamesList = new ArrayList<String>();
				RyotNamesList = ahFuctionalService.getRyotName(agreementDetails.getRyotcode());
				if (RyotNamesList != null && RyotNamesList.size() > 0)
				{
					String myResult1 = RyotNamesList.get(0);
					ryotName = myResult1;
				}
				bean.setRyotName(ryotName);
				bean.setAgreementNumber(agreementDetails.getAgreementnumber());
				bean.setPlotNumber(agreementDetails.getPlotnumber());
				bean.setPlantOrRatoon(agreementDetails.getPlantorratoon());
				bean.setPlotSize(agreementDetails.getExtentsize());
				bean.setCcsRating(agreementDetails.getFamilygroupavgccsrating());
				if (agreementDetails.getRank() != null)
				{
					bean.setRank(agreementDetails.getRank());
				}

				if ("Permit".equalsIgnoreCase(pageName))
				{
					String pnum=null;
					String pnum1[]=null;
					
					if(agreementDetails.getPermitnumbers() != null)
					{
						//Added by DMurty on 25-08-2016
						//bean.setModifyFlag("Yes");
						pnum=agreementDetails.getPermitnumbers();
						if(pnum.contains(","))
						{
							pnum1=pnum.split(",");
							List pdate = ahFuctionalService.getPermitDate(Integer.parseInt(pnum1[0]));
							if(pdate != null && pdate.size() > 0)
							{
								
								Map tempMap = new HashMap();
								tempMap = (Map) pdate.get(0);
								Date perDate=(Date)tempMap.get("permitdate");
								bean.setPermitDate(DateUtils.formatDate(perDate,Constants.GenericDateFormat.DATE_FORMAT));
							}
						}
						else 
						{
							List pdate = ahFuctionalService.getPermitDate(Integer.parseInt(pnum));
							if(pdate != null && pdate.size() > 0)
							{
								Map tempMap = new HashMap();
								tempMap = (Map) pdate.get(0);
								Date perDate=(Date)tempMap.get("permitdate");
								bean.setPermitDate(DateUtils.formatDate(perDate,Constants.GenericDateFormat.DATE_FORMAT));
							}
						}
					}
					else
					{
						bean.setPermitDate(DateUtils.formatDate(new Date(),Constants.GenericDateFormat.DATE_FORMAT));
					}
					bean.setPermitNumber(pnum);
				} 
				else 
				{
					bean.setPermitNumber("null");
				}
				
				String agreementNum = agreementDetails.getAgreementnumber();
				List<AgreementSummary> agrmntSmry = ahFuctionalService.getAgreementSmryByAgreementNo(agreementDetails.getSeasonyear(),agreementNum);
				Byte status = agrmntSmry.get(0).getStatus();
				if(status==0)
				{
					beans.add(bean);
				}
				//beans.add(bean);
			}
			}
			catch(Exception e)
			{
				logger.info("prepareListofBeanForRanking()========error is "+ e);
			}
		}
		return beans;
	}

	@RequestMapping(value = "/saveGenerateRankingTemp", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	List saveGenerateRankingTemp(@RequestBody JSONObject jsonData)	throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		try
		{
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			JSONObject formData = (JSONObject) jsonData.get("formData");

			String season = formData.getString("season");
			String nprogram = formData.getString("programno");

			ahFuctionalService.TruncateTemp("GenerateRankingTemp");
			for (int i = 0; i < gridData.size(); i++)
			{
				GenerateRankingBean generateRankingBean = new ObjectMapper().readValue(gridData.get(i).toString(),GenerateRankingBean.class);
				GenerateRankingTemp generateRankingTemp = prepareModelForGenerateRankingTemp(generateRankingBean);
				entityList.add(generateRankingTemp);
				//ahFuctionalService.addRankingTemp(generateRankingTemp);
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
			List<GenerateRankingBean> generateRankingBeans = prepareListofBeansForRanking(ahFuctionalService.getRankingTempData());

			return generateRankingBeans;
		} 
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
	}

	private List<GenerateRankingBean> prepareListofBeansForRanking(	List<GenerateRankingTemp> generateRankingTemps) 
	{
		List<GenerateRankingBean> beans = null;
		if (generateRankingTemps != null && !generateRankingTemps.isEmpty())
		{
			beans = new ArrayList<GenerateRankingBean>();
			GenerateRankingBean bean = null;
			for (GenerateRankingTemp generateRankingTemp : generateRankingTemps) 
			{
				bean = new GenerateRankingBean();

				bean.setZone(generateRankingTemp.getZone());
				bean.setCircle(generateRankingTemp.getCircle());
				bean.setRyotCode(generateRankingTemp.getRyotcode());
				bean.setRyotName(generateRankingTemp.getRyotname());
				bean.setAgreementNumber(generateRankingTemp.getAgreementno());
				bean.setPlotNumber(generateRankingTemp.getPlotno());
				bean.setPlantOrRatoon(generateRankingTemp.getPlantorratoon());
				bean.setPlotSize(generateRankingTemp.getExtentsize());
				bean.setCcsRating(generateRankingTemp.getFamilygroupccsrating());
				bean.setRank(generateRankingTemp.getId());
				beans.add(bean);
			}
		}
		return beans;
	}

	private GenerateRankingTemp prepareModelForGenerateRankingTemp(GenerateRankingBean generateRankingBean) 
	{
		GenerateRankingTemp generateRankingTemp = new GenerateRankingTemp();

		generateRankingTemp.setZone(generateRankingBean.getZone());
		generateRankingTemp.setCircle(generateRankingBean.getCircle());
		generateRankingTemp.setRyotname(generateRankingBean.getRyotName());
		generateRankingTemp.setRyotcode(generateRankingBean.getRyotCode());
		generateRankingTemp.setAgreementno(generateRankingBean.getAgreementNumber());
		generateRankingTemp.setPlotno(generateRankingBean.getPlotNumber());
		generateRankingTemp.setPlantorratoon(generateRankingBean.getPlantOrRatoon());
		generateRankingTemp.setExtentsize(generateRankingBean.getPlotSize());
		generateRankingTemp.setFamilygroupccsrating(generateRankingBean.getCcsRating());
		generateRankingTemp.setRank(generateRankingBean.getRank());

		return generateRankingTemp;
	}

	// This function is using for Generate Ranking and Permit
	@RequestMapping(value = "/saveRanking", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean saveRanking(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException 
	{ 
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String Success = "";
		Object Qryobj = null;
		String strQry = null;
		try 
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			JSONArray gridData = (JSONArray) jsonData.get("gridData");
			logger.info("saveRanking()========formData=========="	+ formData);
			logger.info("saveRanking()========gridData=========="	+ gridData);

			String season = formData.getString("season");
			logger.info("saveRanking()========pageName========== season"+ season);

			String nprogram = formData.getString("programno");
			String pageName = formData.getString("pageName");
			logger.info("saveRanking()========pageName=========="	+ pageName);
			logger.info("saveRanking()========strQry=========="	+ strQry);
			//Modified by DMurty on 12-08-2016
			if("Permit".equalsIgnoreCase(pageName))
			{
				strQry = "DELETE FROM PermitDetails WHERE season = '"+season+"' and programno="+nprogram;
				logger.info("saveRanking()========strQry=========="	+ strQry);
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				//ahFuctionalService.deletePermits(season, nprogram);
			}
			for (int i = 0; i < gridData.size(); i++) 
			{
				GenerateRankingBean generateRankingBean = new ObjectMapper().readValue(gridData.get(i).toString(),GenerateRankingBean.class);
				String ryotCode = generateRankingBean.getRyotCode();
				String agreementNo = generateRankingBean.getAgreementNumber();
				String plotNo = generateRankingBean.getPlotNumber();
				double ccsRating = generateRankingBean.getCcsRating();
				double extentSize = generateRankingBean.getPlotSize();
				int rank = generateRankingBean.getRank();

				if ("Permit".equalsIgnoreCase(pageName))
				{
					//PermitDetails permitDetails = prepareModelForPermits(generateRankingBean, season, nprogram);
					String permitDate=null;
					String permitNos = generateRankingBean.getPermitNumber();
					if (permitNos.contains(",")) 
					{
						String totalpermits[] = permitNos.split(",");
						for (int k = 0; k < totalpermits.length; k++)
						{
							PermitDetails permitDetails = new PermitDetails();
							
							String perNo = totalpermits[k];
							ByteArrayOutputStream outData = QRCode.from(perNo).stream();//to store QR Code added By RamaKrishna on 26/08/2016
							logger.info("saveRanking()========outData=========="+ outData);

							int nperNo = Integer.parseInt(perNo);

							permitDetails.setSeason(season);

							CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
							compositePrKeyForPermitDetails.setPermitnumber(nperNo);

							permitDetails.setRank(generateRankingBean.getRank());
							permitDetails.setRyotcode(generateRankingBean.getRyotCode());
							permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
							permitDetails.setPlotno(generateRankingBean.getPlotNumber());

							//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,generateRankingBean.getAgreementNumber());
							List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

							String ptorrt = agreementDetails.get(0).getPlantorratoon();
							
							int nptorrt = Integer.parseInt(ptorrt);
							
							permitDetails.setPlantorratoon((byte) nptorrt);
							permitDetails.setExtentsize(generateRankingBean.getPlotSize());
							permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
							permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
							permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
							
							permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

							Date CropDate = agreementDetails.get(0).getCropdate();
							String strCropDate = null;
							
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							strCropDate = df.format(CropDate);
							//Commented by DMurty on 18-10-2016 for schedule permit
							//permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate, Constants.GenericDateFormat.DATE_FORMAT));
							permitDetails.setDeparturedt(new Date());
							permitDetails.setEntrancedt(new Date());
							permitDetails.setUnloadingdt(new Date());
							//added by sahadeva
							permitDetails.setValiditydate(new Date());
							//added by naidu on 14-07-2016
							permitDetails.setPermitdate(DateUtils.getSqlDateFromString(generateRankingBean.getPermitDate(),Constants.GenericDateFormat.DATE_FORMAT));
							
							permitDetails.setShiftandadmin("NA");
							compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
							permitDetails.setStatus((byte) 0);

							permitDetails.setQrcodedata(outData.toByteArray());//to store QR Code added By RamaKrishna on 8/26/2016
							
							permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
							permitDetails.setOldpermitnumber(0);

							entityList.add(permitDetails);
							//ahFuctionalService.addPermits(permitDetails);
						}
					} 
					else
					{
						PermitDetails permitDetails = new PermitDetails();
						
						String perNo = permitNos;
						int nperNo = Integer.parseInt(perNo);
						
						//Modified by DMurty on 02-11-2016 
						permitDetails.setSeason(season);
						CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
						compositePrKeyForPermitDetails.setPermitnumber(nperNo);
						
						//Added by DMurty on 02-11-2016
						ByteArrayOutputStream outData = QRCode.from(perNo).stream();//to store QR Code added By RamaKrishna on 26/08/2016
						logger.info("saveRanking()========outData=========="+ outData);
						permitDetails.setQrcodedata(outData.toByteArray());//to store QR Code added By RamaKrishna on 8/26/2016


						permitDetails.setRank(generateRankingBean.getRank());
						permitDetails.setRyotcode(generateRankingBean.getRyotCode());
						permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
						permitDetails.setPlotno(generateRankingBean.getPlotNumber());

						//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, generateRankingBean.getAgreementNumber());
						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

						String ptorrt = agreementDetails.get(0).getPlantorratoon();
						int nptorrt = Integer.parseInt(ptorrt);
						permitDetails.setPlantorratoon((byte) nptorrt);
						permitDetails.setExtentsize(generateRankingBean.getPlotSize());
						permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
						permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
						permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
						permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

						Date CropDate = agreementDetails.get(0).getCropdate();
						String strCropDate = null;
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						strCropDate = df.format(CropDate);
						
						//Commented by DMurty on 18-10-2016 for schedule permit
						//permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate,Constants.GenericDateFormat.DATE_FORMAT));
						
						permitDetails.setPermitdate(DateUtils.getSqlDateFromString(generateRankingBean.getPermitDate(),Constants.GenericDateFormat.DATE_FORMAT));
						
						permitDetails.setDeparturedt(new Date());
						permitDetails.setEntrancedt(new Date());
						permitDetails.setUnloadingdt(new Date());
						//added by sahadeva 11-11-2016
						permitDetails.setValiditydate(new Date());
						permitDetails.setShiftandadmin("NA");

						compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
						permitDetails.setStatus((byte) 0);
						permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
						permitDetails.setOldpermitnumber(0);

						entityList.add(permitDetails);
					}
				}
				logger.info("========ryotCode==========" + ryotCode);
				logger.info("========agreementNo==========" + agreementNo);
				logger.info("========plotNo==========" + plotNo);
				logger.info("========ccsRating==========" + ccsRating);
				logger.info("========extentSize==========" + extentSize);
				logger.info("========rank==========" + rank);
			
				int program = Integer.parseInt(nprogram);

				if("Permit".equalsIgnoreCase(pageName))
		        {
					//For Permit
					String permitNo = generateRankingBean.getPermitNumber();
					strQry = "UPDATE AgreementDetails set permitnumbers = '"+permitNo+"' WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"' and programnumber="+program+" and rank="+rank;
					logger.info("saveRanking()========strQry=========="	+ strQry);
					Qryobj = strQry;
					UpdateList.add(Qryobj);
		        }
				else
				{
					//For Ranking
					strQry = "UPDATE AgreementDetails set rank = "+rank+" WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"' and familygroupavgccsrating="+ccsRating+" and programnumber="+program;
					logger.info("saveRanking()========strQry=========="	+ strQry);
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
			}
			logger.info("saveRanking()========entityList=========="	+ entityList);
			logger.info("saveRanking()========UpdateList=========="	+ UpdateList);
			isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
			return isInsertSuccess;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
	}
	@RequestMapping(value = "/savePermits", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray savePermits(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String Success = "";
		Object Qryobj = null;
		String strQry = null;
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		try 
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			JSONArray gridData = (JSONArray) jsonData.get("gridData");

			String season = formData.getString("season");
			String nprogram = formData.getString("programno");
			String pageName = formData.getString("pageName");
			int programnum=Integer.valueOf(nprogram);
			//Modified by DMurty on 12-08-2016
			if("Permit".equalsIgnoreCase(pageName))
			{
				strQry = "DELETE FROM PermitDetails WHERE season = '"+season+"' and programno="+nprogram;
				Qryobj = strQry;
				UpdateList.add(Qryobj);
				//ahFuctionalService.deletePermits(season, nprogram);
			}
			for (int i = 0; i < gridData.size(); i++) 
			{
				GenerateRankingBean generateRankingBean = new ObjectMapper().readValue(gridData.get(i).toString(),GenerateRankingBean.class);
				String ryotCode = generateRankingBean.getRyotCode();
				String agreementNo = generateRankingBean.getAgreementNumber();
				String plotNo = generateRankingBean.getPlotNumber();
				double ccsRating = generateRankingBean.getCcsRating();
				double extentSize = generateRankingBean.getPlotSize();
				int rank = generateRankingBean.getRank();

				if ("Permit".equalsIgnoreCase(pageName))
				{
					//PermitDetails permitDetails = prepareModelForPermits(generateRankingBean, season, nprogram);
					String permitDate=null;
					String permitNos = generateRankingBean.getPermitNumber();
					if (permitNos.contains(",")) 
					{
						String totalpermits[] = permitNos.split(",");
						for (int k = 0; k < totalpermits.length; k++)
						{
							PermitDetails permitDetails = new PermitDetails();
							
							String perNo = totalpermits[k];
							ByteArrayOutputStream outData = QRCode.from(perNo).stream();//to store QR Code added By RamaKrishna on 26/08/2016
							
							int nperNo = Integer.parseInt(perNo);

							permitDetails.setSeason(season);

							CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
							compositePrKeyForPermitDetails.setPermitnumber(nperNo);

							permitDetails.setRank(generateRankingBean.getRank());
							permitDetails.setRyotcode(generateRankingBean.getRyotCode());
							permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
							permitDetails.setPlotno(generateRankingBean.getPlotNumber());

							//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,generateRankingBean.getAgreementNumber());
							List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

							String ptorrt = agreementDetails.get(0).getPlantorratoon();
							
							int nptorrt = Integer.parseInt(ptorrt);
							
							permitDetails.setPlantorratoon((byte) nptorrt);
							permitDetails.setExtentsize(generateRankingBean.getPlotSize());
							permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
							permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
							permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
							
							permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

							Date CropDate = agreementDetails.get(0).getCropdate();
							String strCropDate = null;
							
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							strCropDate = df.format(CropDate);
							//Commented by DMurty on 18-10-2016 for schedule permit
							//permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate, Constants.GenericDateFormat.DATE_FORMAT));
							permitDetails.setDeparturedt(new Date());
							permitDetails.setEntrancedt(new Date());
							permitDetails.setUnloadingdt(new Date());
							
							//added by naidu on 14-07-2016
							permitDetails.setPermitdate(DateUtils.getSqlDateFromString(generateRankingBean.getPermitDate(),Constants.GenericDateFormat.DATE_FORMAT));
							
							permitDetails.setShiftandadmin("NA");
							compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
							permitDetails.setStatus((byte) 0);

							permitDetails.setQrcodedata(outData.toByteArray());//to store QR Code added By RamaKrishna on 8/26/2016
							
							permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
							permitDetails.setOldpermitnumber(0);

							entityList.add(permitDetails);
							
							//added by sahadeva  25-10-2016
							List<Circle> circle=ahFuctionalService.getCircleByCirclecode(permitDetails.getCirclecode());
							List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetails.getZonecode());
							List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetails.getLandvilcode());
							List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetails.getVarietycode());
							List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
							List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(permitDetails.getRyotcode());
							//SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
							DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
							Date date = new Date();
							String cdate=(dateFormat.format(date));
							Calendar cal = Calendar.getInstance();
							String ctime=(dateFormat.format(cal.getTime()));
							jsonObj.put("issuccess", "true");
							jsonObj.put("ryotcode",permitDetails.getRyotcode());
							jsonObj.put("ryotname",ryot.get(0).getRyotname());
							jsonObj.put("fathername",ryot.get(0).getRelativename());
							jsonObj.put("circlecode",permitDetails.getCirclecode());
							jsonObj.put("landvilcode",permitDetails.getLandvilcode());
							//jsonObj.put("permitnumber",nperNo);
							jsonObj.put("permitnumber",permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
							jsonObj.put("programno",permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
							jsonObj.put("circle",circle.get(0).getCircle());
							jsonObj.put("zone",zone.get(0).getZone());
							jsonObj.put("variety",variety.get(0).getVariety());
							jsonObj.put("plantorratoon",crop.get(0).getCroptype());
							byte[] encoded = Base64.encodeBase64(permitDetails.getQrcodedata()); 
							jsonObj.put("qrcodedata",new String(encoded));
							jsonObj.put("currenttime",ctime);
							jsonObj.put("currentdate",cdate);
							
							jArray.add(jsonObj);
							//ahFuctionalService.addPermits(permitDetails);
						}
					} 
					else
					{
						PermitDetails permitDetails = new PermitDetails();
						
						String perNo = permitNos;
						int nperNo = Integer.parseInt(perNo);

						permitDetails.setSeason(generateRankingBean.getSeason());

						CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
						compositePrKeyForPermitDetails.setPermitnumber(nperNo);

						permitDetails.setRank(generateRankingBean.getRank());
						permitDetails.setRyotcode(generateRankingBean.getRyotCode());
						permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
						permitDetails.setPlotno(generateRankingBean.getPlotNumber());

						//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, generateRankingBean.getAgreementNumber());
						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

						String ptorrt = agreementDetails.get(0).getPlantorratoon();
						int nptorrt = Integer.parseInt(ptorrt);
						permitDetails.setPlantorratoon((byte) nptorrt);
						permitDetails.setExtentsize(generateRankingBean.getPlotSize());
						permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
						permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
						permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
						permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

						Date CropDate = agreementDetails.get(0).getCropdate();
						String strCropDate = null;
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						strCropDate = df.format(CropDate);
						//added by naidu 14-07-2016
						permitDetails.setPermitdate(DateUtils.getSqlDateFromString(generateRankingBean.getPermitDate(),Constants.GenericDateFormat.DATE_FORMAT));
						
						permitDetails.setDeparturedt(new Date());
						permitDetails.setEntrancedt(new Date());
						permitDetails.setUnloadingdt(new Date());
						permitDetails.setShiftandadmin("NA");
						permitDetails.setOldpermitnumber(0);

						compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
						permitDetails.setStatus((byte) 0);
						permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
						entityList.add(permitDetails);
						//added by sahadeva  25-10-2016
						List<Circle> circle=ahFuctionalService.getCircleByCirclecode(permitDetails.getCirclecode());
						List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetails.getZonecode());
						List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetails.getLandvilcode());
						List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetails.getVarietycode());
						List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
						List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(permitDetails.getRyotcode());
						//SimpleDateFormat ctime = new SimpleDateFormat("hh:mm:ss");
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date date = new Date();
						String cdate=(dateFormat.format(date));
						Calendar cal = Calendar.getInstance();
						String ctime=(dateFormat.format(cal.getTime()));
						jsonObj.put("issuccess", "true");
						jsonObj.put("ryotcode",permitDetails.getRyotcode());
						jsonObj.put("ryotname",ryot.get(0).getRyotname());
						jsonObj.put("fathername",ryot.get(0).getRelativename());
						jsonObj.put("circlecode",permitDetails.getCirclecode());
						jsonObj.put("landvilcode",permitDetails.getLandvilcode());
						//jsonObj.put("permitnumber",nperNo);
						jsonObj.put("permitnumber",permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
						jsonObj.put("programno",permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
						jsonObj.put("circle",circle.get(0).getCircle());
						jsonObj.put("zone",zone.get(0).getZone());
						jsonObj.put("variety",variety.get(0).getVariety());
						jsonObj.put("plantorratoon",crop.get(0).getCroptype());
						byte[] encoded = Base64.encodeBase64(permitDetails.getQrcodedata()); 
						jsonObj.put("qrcodedata",new String(encoded));
						jsonObj.put("currenttime",ctime);
						jsonObj.put("currentdate",cdate);
						
						jArray.add(jsonObj);
					}
				}
				int program = Integer.parseInt(nprogram);
				if("Permit".equalsIgnoreCase(pageName))
		        {
					//For Permit
					String permitNo = generateRankingBean.getPermitNumber();
					strQry = "UPDATE AgreementDetails set permitnumbers = '"+permitNo+"' WHERE seasonyear ='"+season+"' and plotnumber='"+plotNo+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"' and programnumber="+program+" and rank="+rank;
					Qryobj = strQry;
					UpdateList.add(Qryobj);
		        }
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
			if(isInsertSuccess == true)
			{
				jsonObj.put("issuccess", "true");
				jArray.add(jsonObj);
			}
			else
			{			
				jArray.clear();
				jsonObj.put("issuccess", "false");
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return jArray;
		}
	}
	
	@RequestMapping(value = "/saveReGeneratePermit", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody JSONArray saveReGeneratePermit(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException 
	{
		boolean isInsertSuccess  = false;
		List entityList = new ArrayList();
		List UpdateList = new ArrayList();
		String Success = "";
		Object Qryobj = null;
		String strQry = null;
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		try 
		{
			JSONObject formData = (JSONObject) jsonData.get("formData");
			int oldPermitNo = formData.getInt("permitNumber");
			int newPermitNo = formData.getInt("newpermitNumber");
			String agreementNo = formData.getString("agreementNo");
			String season = formData.getString("season");
			String plotNo = formData.getString("plotNo");
			String ryotCode = formData.getString("ryotCode");
			String harvestingDate = formData.getString("date");
			String generatedBy = formData.getString("GeneratedBy");
			String reason = formData.getString("reason");
			//Added by sahadeva   11/14/2016
			//Mofified by DMurty on 23-11-2016
			Integer a=1;
			DateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date hDate =(Date) dateformat.parse(harvestingDate);
			
			Date newDate =addDays(hDate, a);
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String strnewValidityDate = df.format(newDate);
			Date newValidityDate = DateUtils.getSqlDateFromString(strnewValidityDate,Constants.GenericDateFormat.DATE_FORMAT);
			//String strfinalDate = df2.format(newDate);
			

			ByteArrayOutputStream outData = QRCode.from(""+newPermitNo).stream();
			logger.info("---------------"+outData.size());
			
			//Update status to 2 to that permit no in permit details table for that permit no
			strQry = "UPDATE PermitDetails set status = 2 WHERE season ='"+season+"' and permitnumber="+oldPermitNo;
			Qryobj = strQry;
			UpdateList.add(Qryobj);
			
			//inser recode in permit details table with new permit no.
			List<PermitDetails> permiDetails = ahFuctionalService.getPermitDetailsByNumberSeasonAndPlot(oldPermitNo);
			
			int rank = permiDetails.get(0).getRank();
			double extentsize = permiDetails.get(0).getExtentsize();
			int programNo =  caneReceiptFunctionalService.getProgramNo(oldPermitNo);

			PermitDetails permitDetails = new PermitDetails();
			permitDetails.setSeason(season);

			CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
			compositePrKeyForPermitDetails.setPermitnumber(newPermitNo);

			permitDetails.setRank(rank);
			permitDetails.setRyotcode(ryotCode);
			permitDetails.setAgreementno(agreementNo);
			permitDetails.setPlotno(plotNo);
			permitDetails.setOldpermitnumber(oldPermitNo);
			
			//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,agreementNo);
			List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(agreementNo,plotNo,season);

			String ptorrt = agreementDetails.get(0).getPlantorratoon();
			int nptorrt = Integer.parseInt(ptorrt);
			
			permitDetails.setPlantorratoon((byte) nptorrt);
			permitDetails.setExtentsize(extentsize);
			permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
			permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
			permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
			
			permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

			Date CropDate = agreementDetails.get(0).getCropdate();
			String strCropDate = null;
			DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
			strCropDate = df1.format(CropDate);
			
			//permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate, Constants.GenericDateFormat.DATE_FORMAT));
			permitDetails.setDeparturedt(new Date());
			permitDetails.setEntrancedt(new Date());
			permitDetails.setUnloadingdt(new Date());
			//Modified by DMurty on 02-11-2016
			Date permitDate = new Date();
			String strpermitDate = df.format(permitDate);
			permitDetails.setPermitdate(DateUtils.getSqlDateFromString(strpermitDate,Constants.GenericDateFormat.DATE_FORMAT));
			
			permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(harvestingDate,Constants.GenericDateFormat.DATE_FORMAT));
			//Added by sahadeva 14/11/2016
			permitDetails.setValiditydate(newValidityDate);
			
			permitDetails.setShiftandadmin("NA");
			compositePrKeyForPermitDetails.setProgramno(programNo);
			permitDetails.setStatus((byte) 0);
			permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
			permitDetails.setRegeneratedby(generatedBy);
			permitDetails.setReason(reason);
			
			permitDetails.setQrcodedata(outData.toByteArray());
			entityList.add(permitDetails);
			
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			if(isInsertSuccess == true)
			{
				//Added by DMurty on 05-06-2016
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				String cdate=(dateFormat.format(date));
				Calendar cal = Calendar.getInstance();
				String ctime=(dateFormat.format(cal.getTime()));
				//Added by sahadeva 14/11/2016
				List<PermitDetails> permitDetails1 = ahFuctionalService.getValidityDate(newPermitNo);
				Date vdate=permitDetails1.get(0).getValiditydate();
				DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
				String strvDate = df2.format(vdate);
				//
				List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(permitDetails.getRyotcode());
				List<Circle> circle=ahFuctionalService.getCircleByCirclecode(permitDetails.getCirclecode());
				List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetails.getZonecode());
				List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetails.getLandvilcode());
				List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetails.getVarietycode());
				List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
				
				jsonObj.put("issuccess", "true");
				jsonObj.put("permitNumber", newPermitNo);
				jsonObj.put("ryotcode",permitDetails.getRyotcode());
				jsonObj.put("ryotname",ryot.get(0).getRyotname());
				jsonObj.put("fathername",ryot.get(0).getRelativename());
				jsonObj.put("circlecode",permitDetails.getCirclecode());
				jsonObj.put("landvilcode",permitDetails.getLandvilcode());
				jsonObj.put("permitnumber",permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
				jsonObj.put("programno",permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
				jsonObj.put("circle",circle.get(0).getCircle());
				jsonObj.put("zone",zone.get(0).getZone());
				jsonObj.put("variety",variety.get(0).getVariety());
				jsonObj.put("plantorratoon",crop.get(0).getCroptype());
				byte[] encoded = Base64.encodeBase64(permitDetails.getQrcodedata()); 
				jsonObj.put("qrcodedata",new String(encoded));
				jsonObj.put("currenttime",ctime);
				jsonObj.put("currentdate",cdate);
				jsonObj.put("currentdate",cdate);
				//Added by DMurty on 24-11-2016
				jsonObj.put("village",village.get(0).getVillage());
				
				jsonObj.put("rank",permitDetails1.get(0).getRank());
				Date harvestDate = permitDetails1.get(0).getHarvestdt();
				df2 = new SimpleDateFormat("dd-MM-yyyy");
				String strvharvestDate = df2.format(harvestDate);
				jsonObj.put("harvestDate",strvharvestDate);
				jsonObj.put("oldPermitNumber",permitDetails.getOldpermitnumber());


				jArray.add(jsonObj);
			}
			else
			{
				jsonObj.put("issuccess", "false");
				jArray.add(jsonObj);
			}
			return jArray;
		} 
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return jArray;
		}
	}

	@RequestMapping(value = "/getPermitRules", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	JSONArray getPermitRules(@RequestBody String jsonData) throws Exception 
	{
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = null;
		String strQuery = null;
		List<PermitRules> permitRules = ahFuctionalService.getPermitRules();
		try
		{
			jsonObj = new JSONObject();
			jsonObj.put("noOfTonsPeraAcre", permitRules.get(0).getNooftonsperacre());
			jsonObj.put("noOfTonsPerEachPermit", permitRules.get(0).getNooftonspereachpermit());
			jArray.add(jsonObj);
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return jArray;
	}

	private PermitDetails prepareModelForPermits(GenerateRankingBean generateRankingBean, String season,String nprogram)
	{
		PermitDetails permitDetails = new PermitDetails();

		String permitNos = generateRankingBean.getPermitNumber();
		if (permitNos.contains(",")) 
		{
			String totalpermits[] = permitNos.split(",");
			for (int k = 0; k < totalpermits.length; k++)
			{
				String perNo = totalpermits[k];
				int nperNo = Integer.parseInt(perNo);

				permitDetails.setSeason(season);

				CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
				compositePrKeyForPermitDetails.setPermitnumber(nperNo);

				permitDetails.setRank(generateRankingBean.getRank());
				permitDetails.setRyotcode(generateRankingBean.getRyotCode());
				permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
				permitDetails.setPlotno(generateRankingBean.getPlotNumber());

				//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,generateRankingBean.getAgreementNumber());
				List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

				String ptorrt = agreementDetails.get(0).getPlantorratoon();
				int nptorrt = Integer.parseInt(ptorrt);
				
				permitDetails.setPlantorratoon((byte) nptorrt);
				permitDetails.setExtentsize(generateRankingBean.getPlotSize());
				permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
				permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
				permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
				permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

				Date CropDate = agreementDetails.get(0).getCropdate();
				String strCropDate = null;
				
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				strCropDate = df.format(CropDate);
				
				permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate, Constants.GenericDateFormat.DATE_FORMAT));
				permitDetails.setDeparturedt(new Date());
				permitDetails.setEntrancedt(new Date());
				permitDetails.setUnloadingdt(new Date());

				permitDetails.setShiftandadmin("NA");
				compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
				permitDetails.setStatus((byte) 0);

				permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);

				ahFuctionalService.addPermits(permitDetails);
			}
		} 
		else
		{
			String perNo = permitNos;
			int nperNo = Integer.parseInt(perNo);

			permitDetails.setSeason(generateRankingBean.getSeason());

			CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
			compositePrKeyForPermitDetails.setPermitnumber(nperNo);

			permitDetails.setRank(generateRankingBean.getRank());
			permitDetails.setRyotcode(generateRankingBean.getRyotCode());
			permitDetails.setAgreementno(generateRankingBean.getAgreementNumber());
			permitDetails.setPlotno(generateRankingBean.getPlotNumber());

			//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season, generateRankingBean.getAgreementNumber());
			List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(generateRankingBean.getAgreementNumber(),generateRankingBean.getPlotNumber(),season);

			String ptorrt = agreementDetails.get(0).getPlantorratoon();
			int nptorrt = Integer.parseInt(ptorrt);
			permitDetails.setPlantorratoon((byte) nptorrt);
			permitDetails.setExtentsize(generateRankingBean.getPlotSize());
			permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
			permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
			permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
			permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());

			Date CropDate = agreementDetails.get(0).getCropdate();
			String strCropDate = null;
			
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			strCropDate = df.format(CropDate);
			
			permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(strCropDate,Constants.GenericDateFormat.DATE_FORMAT));
			permitDetails.setDeparturedt(new Date());
			permitDetails.setEntrancedt(new Date());
			permitDetails.setUnloadingdt(new Date());
			permitDetails.setShiftandadmin("NA");

			compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
			permitDetails.setStatus((byte) 0);
			permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);

			ahFuctionalService.addPermits(permitDetails);
		}
		return permitDetails;
	}
	
	@RequestMapping(value = "/isSeedlingAdvance", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	Byte isSeedlingAdvance(@RequestBody Integer jsonData) throws Exception
	{
		Byte isSeedlingAdvance= null;
		try
		{
			isSeedlingAdvance= (Byte)ahFuctionalService.getSeedlingAdvance(jsonData);
		}
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return isSeedlingAdvance;
	}
	
	@RequestMapping(value = "/getMaxLoanNo", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody
	Integer getMaxLoanNo(@RequestBody JSONObject jsonData) throws Exception
	{
		String season= jsonData.getString("season");
		int branchCode=jsonData.getInt("branchCode");
		Integer maxLoanNum= null;
		try
		{
			//maxLoanNum = commonService.getMaxLoanNo("LoanDetails","loannumber",jsonData);
			
			List ls=ahFuctionalService.getMaxLoanNoList(branchCode,season);			
			maxLoanNum=ls.size();
			maxLoanNum++;
		}
		catch (Exception e) 
		{
			logger.info(e.getCause(), e);
			return null;
		}
		return maxLoanNum;
	}
	
	
	//Migration for Circle Master
	@RequestMapping(value = "/getMigrateCircleData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getMigrateCircleData(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List CircleList = ahFuctionalService.getAgreementDataToMigrate("SssCircle");
		logger.info("========CircleList==========" + CircleList);

		try	
		{
			if(CircleList != null && CircleList.size()>0)
			{
				for (int i = 0; i < CircleList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) CircleList.get(i);
					logger.info("========tempMap==========" + tempMap);

					double clCode = (Double) tempMap.get("CI_CODE");
					String circleName = (String) tempMap.get("CI_NAME");
					String fieldAssistant = (String) tempMap.get("CI_FA");
					double zoneCode = (Double) tempMap.get("CI_ZONE");
					double ccCode = (Double) tempMap.get("CC_CODE");
					String strccCode = Double.toString(ccCode);
					strccCode = strccCode.substring(0, strccCode.length()-2);
					if(strccCode == "" || strccCode == null || strccCode == "null")
					{
						strccCode = "0";
					}
					Circle circle = new Circle();					
					
					int nclCode = (int) clCode;
					circle.setCirclecode(nclCode);
					
					int nccCode = (int) ccCode;
					circle.setVillagecode(strccCode);
					circle.setCircle(circleName);
					circle.setDescription("NA");
					
					int faId = 0;
					int foId = 0;

					List<FieldAssistant> fieldAsstant = ahFuctionalService.getFieldAssistantDetails(fieldAssistant);
					if(fieldAsstant != null && fieldAsstant.size()>0)
					{
						faId = fieldAsstant.get(0).getFieldassistantid();
						circle.setFieldassistantid(faId);
						
						foId = fieldAsstant.get(0).getFieldofficerid();
						circle.setFieldofficerid(foId);
					}
					else
					{
						circle.setFieldassistantid(0);
						circle.setFieldofficerid(0);
					}
					
					
					int cmId = 0;
					List<FieldOfficer> fieldOfficer = ahFuctionalService.getFieldOfficerDetails(foId);
					if(fieldAsstant != null && fieldOfficer.size()>0)
					{
						cmId = fieldOfficer.get(0).getCaneManagerId();
						circle.setCanemanagerid(cmId);
					}
					else
					{
						circle.setCanemanagerid(cmId);
					}
					circle.setStatus((byte) 0);
					//entityList.add(circle);
					circleService.addCircle(circle);
				}
				isInsertSuccess = true;
			}
			else
			{
				isInsertSuccess = true;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	
	//Migration for RegionCode in  AgreementDetails
	@RequestMapping(value = "/regionMigration", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean regionMigration(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		
		List UpdateList=new ArrayList();
		
		List ZoneCodeList = ahFuctionalService.getZonecodeFromAgreementDetails();
		logger.info("========ZoneCodeList==========" + ZoneCodeList);

		try	
		{
			if(ZoneCodeList != null && ZoneCodeList.size()>0)
			{
				for (int i = 0; i < ZoneCodeList.size(); i++)
				{
					logger.info("========i==========" + i);
					Map tempMap = new HashMap();
					tempMap = (Map) ZoneCodeList.get(i);
					logger.info("========tempMap==========" + tempMap);
					Integer zoneCode = (Integer) tempMap.get("zonecode");
					List<Zone> zone= ahFuctionalService.getRegionCode(zoneCode);
					if(zone != null && zone.size()>0)
					{
						Integer reCode = zone.get(0).getRegioncode();
						
						String	strQry = "UPDATE AgreementDetails set regioncode="+reCode+" WHERE zonecode ="+zoneCode+"";
					
						UpdateList.add(strQry);
					}
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	
	//Added by sahadeva 8/11/2016
	@RequestMapping(value = "/updateZonesMigration", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean updateZonesMigration(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		
		List UpdateList=new ArrayList();
		
		List CircleList = ahFuctionalService.getAgreementDataToMigrate("SssCircle");
		logger.info("========CircleList==========" + CircleList);

		try	
		{
			if(CircleList != null && CircleList.size()>0)
			{
				for (int i = 0; i < CircleList.size(); i++)
				{
					logger.info("========i==========" + i);
					Map tempMap = new HashMap();
					tempMap = (Map) CircleList.get(i);
					logger.info("========tempMap==========" + tempMap);
					
					Double zonecode = (Double) tempMap.get("CI_ZONE");
					Integer zoneCode=zonecode.intValue();
				
					Double circlecode = (Double) tempMap.get("CI_CODE");
					Integer circleCode=circlecode.intValue();
					
					List<Circle> circle= ahFuctionalService.getVillageCode(circleCode);
					String vCode=null;
					if(circle != null && circle.size()>0)
					{
						vCode=circle.get(0).getVillagecode();
					}
					List<Village> village= ahFuctionalService.getMandalCode(vCode);
					
					if(village != null && village.size()>0)
					{
						Integer mCode=village.get(0).getMandalcode();
						
						// update zoneCode to mCode in Mandal table
						String	strQry = "UPDATE Mandal set zonecode="+zoneCode+" WHERE mandalcode ="+mCode+"";
						UpdateList.add(strQry);
					}
					String strQry = "UPDATE AgreementDetails set zonecode="+zoneCode+" WHERE circlecode ="+circleCode+"";
					UpdateList.add(strQry);
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	//Added by sahadeva 11/11/2016
	@RequestMapping(value = "/AccountingClouser", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean accountingClouser(@RequestBody JSONObject jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		
		List UpdateList=new ArrayList();
		Object Qryobj = null;
		
		String season="16-17";
		String newSeason=NextSeasonYear(season);
		List AccountList = ahFuctionalService.getAccountCodesToMigrate(season);
		Double cr=0.0;
		Double dr=0.0;
		Double runBal=0.0;
		String balType=null;
		String accCode=null;
		int transactionCode = 0;
		String TransactionType =null;
		logger.info("========AccountList==========" + AccountList);
		try	
		{
			if(AccountList != null && AccountList.size()>0)
			{
				for (int i = 0; i < AccountList.size(); i++)
				{
					
					logger.info("========i==========" + i);
					Map tempMap = new HashMap();
					tempMap = (Map) AccountList.get(i);
					logger.info("========tempMap==========" + tempMap);
					 accCode = (String) tempMap.get("accountcode");
					List<AccountDetails> accountDetails1= ahFuctionalService.getMaxTransactionId(newSeason);
					if(accountDetails1 != null && accountDetails1.size()>0)
					{
						transactionCode=accountDetails1.get(0).getTransactioncode();
					}
					
					List AccountDetailsData = commonService.getAccountDetailsData(season,accCode);
					
					if(AccountDetailsData != null && AccountDetailsData.size()>0)
					{
						Map AccSumMap = new HashMap();
						AccSumMap = (Map) AccountDetailsData.get(0);
						cr = (Double) AccSumMap.get("cr");
						dr=(Double) AccSumMap.get("dr");
					}
				//	Double runBal=0.0;
				//	String balType=null;
					if(cr>dr)
					{
						runBal=cr-dr;
						balType="cr";
					}
					else if(dr>cr)
					{
						runBal=dr-cr;
						balType="dr";
					}
					else
					{
						runBal=0.0;
						balType="cr";
					}
					
					Map DtlsMap = new HashMap();
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT",runBal);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					DtlsMap.put("ACCOUNT_CODE", accCode);
					DtlsMap.put("JOURNAL_MEMO", "Towards Opening Balance");
					DtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType = balType;
					DtlsMap.put("SEASON", newSeason);
							
					AccountDetails accountDetails =  prepareModelForAccountDetails(DtlsMap);
					entityList.add(accountDetails);
					AccountSummary accountSummary = prepareModelforAccSmry(runBal,accCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						String strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+newSeason+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						entityList.add(accountSummary);
					}
					
				}
				
					List AccGroupList = ahFuctionalService.getAccountGroupCodesToMigrate(season);
					Integer accGroupCode=0;
					if(AccGroupList != null && AccGroupList.size()>0)
					{
						for (int j = 0; j < AccGroupList.size(); j++)
						{
							Map tempMap1 = new HashMap();
							tempMap1 = (Map) AccGroupList.get(j);
							logger.info("========tempMap1==========" + tempMap1);
							 accGroupCode = (Integer) tempMap1.get("accountgroupcode");
							List<AccountGroupDetails> accountGroupDetails1= ahFuctionalService.getMaxTransactionCode(newSeason);
							if(accountGroupDetails1 != null && accountGroupDetails1.size()>0)
							{
								transactionCode=accountGroupDetails1.get(0).getTransactioncode();
							}
							List AccountGroupData = commonService.getAccountGroupData(season,accGroupCode);
							if(AccountGroupData != null && AccountGroupData.size()>0)
							{
								Map AccSumMap = new HashMap();
								AccSumMap = (Map) AccountGroupData.get(0);
								cr = (Double) AccSumMap.get("cr");
								dr=(Double) AccSumMap.get("dr");
							}
							
							if(cr>dr)
							{
								runBal=cr-dr;
								balType="cr";
							}
							else if(dr>cr)
							{
								runBal=dr-cr;
								balType="dr";
							}
							else
							{
								runBal=0.0;
								balType="cr";
							}
							
							Map AccGrpDtlsMap = new HashMap();
							
							AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
							AccGrpDtlsMap.put("TRANSACTION_AMOUNT", runBal);
							//added by sahadeva 27/10/2016
							AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
							
							AccGrpDtlsMap.put("ACCOUNT_CODE", accCode);
							AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGroupCode);
							AccGrpDtlsMap.put("JOURNAL_MEMO","Towards Opening Balance");
							Date cdate=new Date();
							AccGrpDtlsMap.put("C_Date", cdate);
							//if(type==30)
							AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						//	else
						//	AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
							//Added by DMurty on 18-10-2016
							AccGrpDtlsMap.put("SEASON",newSeason);
							
							AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
							entityList.add(accountGroupDetails);
							AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accCode, runBal, accGroupCode, TransactionType,season);
							
							List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGroupCode,season);
							if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
							{
								double finalAmt = accountGroupSummary.getRunningbalance();
								String strCrDr = accountGroupSummary.getBalancetype();
								int AccGroupCode = accountGroupSummary.getAccountgroupcode();
								
								String strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGroupCode+" and season='"+newSeason+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							}
							else 
							{
								entityList.add(accountGroupSummary);
							}
							
							
						
					}
				
					List AccSubGroupList = ahFuctionalService.getAccSubGroupCodesToMigrate(season);
					
					if(AccSubGroupList != null && AccSubGroupList.size()>0)
					{
						for (int j = 0; j < AccSubGroupList.size(); j++)
						{
							Map tempMap1 = new HashMap();
							tempMap1 = (Map) AccSubGroupList.get(j);
							logger.info("========tempMap1==========" + tempMap1);
							Integer accSubGroupCode = (Integer) tempMap1.get("accountsubgroupcode");
							List<AccountSubGroupDetails> accountSubGroupDetails1= ahFuctionalService.getMaxTCodeForAccSub(newSeason);
							if(accountSubGroupDetails1 != null && accountSubGroupDetails1.size()>0)
							{
								transactionCode=accountSubGroupDetails1.get(0).getTransactioncode();
							}
							List AccSubGroupData = commonService.getAccSubGroupData(season,accSubGroupCode);
							if(AccSubGroupData != null && AccSubGroupData.size()>0)
							{
								Map AccSumMap = new HashMap();
								AccSumMap = (Map) AccSubGroupData.get(0);
								cr = (Double) AccSumMap.get("cr");
								dr=(Double) AccSumMap.get("dr");
							}
							
							if(cr>dr)
							{
								runBal=cr-dr;
								balType="cr";
							}
							else if(dr>cr)
							{
								runBal=dr-cr;
								balType="dr";
							}
							else
							{
								runBal=0.0;
								balType="cr";
							}
							
							Map AccSubGrpDtlsMap = new HashMap();

							AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
							AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", runBal);
							//added by sahadeva 27/10/2016
							AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
							AccSubGrpDtlsMap.put("ACCOUNT_CODE", accCode);
							AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGroupCode);
							AccSubGrpDtlsMap.put("JOURNAL_MEMO","Towards Opening Balance");
							Date cdate=new Date();
							AccSubGrpDtlsMap.put("C_Date", cdate);
							AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
							//Added by DMurty on 18-10-2016
							AccSubGrpDtlsMap.put("SEASON", newSeason);
							
							AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
							entityList.add(accountSubGroupDetails);
							
							AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accCode, runBal, accGroupCode, TransactionType,season);
							
							double finalAmt = accountSubGroupSummary.getRunningbalance();
							String strCrDr = accountSubGroupSummary.getBalancetype();
							int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
							int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

							List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGroupCode,accSubGroupCode,season);
							if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
							{
								 String strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGroupCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+newSeason+"'";
								Qryobj = strQry;
								UpdateList.add(Qryobj);
							} 
							else 
							{
								entityList.add(accountSubGroupSummary);
							}
							
						}
					}
				
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	
	//Added by sahadeva 18/11/2016
	@RequestMapping(value = "/ReconsileLoansAndAdvances", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean ReconsileLoansAndAdvances(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		String season="2016-2017";
		String TransactionType =null;
		String balType = null;
		double runBal = 0.0;
		try	
		{
			List AccountList = ahFuctionalService.getAccountCodesToMigrate(season);
			logger.info("========AccountList==========" + AccountList);
			if(AccountList != null && AccountList.size()>0)
			{
				for (int i = 0; i < AccountList.size(); i++)
				{
					logger.info("========i==========" + i);
					Map tempMap = new HashMap();
					tempMap = (Map) AccountList.get(i);
					logger.info("========tempMap==========" + tempMap);
					String accCode = (String) tempMap.get("accountcode");
					String AccountCode = accCode;
					List AccountDetailsData = commonService.getAccountDetailsData(season,accCode);
					if(AccountDetailsData != null && AccountDetailsData.size()>0)
					{
						Map AccSumMap = new HashMap();
						AccSumMap = (Map) AccountDetailsData.get(0);
						double cr = (Double) AccSumMap.get("cr");
						double dr=(Double) AccSumMap.get("dr");
						if(cr>dr)
						{
							runBal=cr-dr;
							balType="Cr";
						}
						else if(dr>cr)
						{
							runBal=dr-cr;
							balType="Dr";
						}
						else
						{
							runBal=0.0;
							balType="Cr";
						}
						TransactionType=balType;
						AccountSummary accountSummary = prepareModelforAccSmry(runBal,accCode, TransactionType,season);
						entityList.add(accountSummary);
					}
				}
			}
			List AccGroupList = ahFuctionalService.getAccountGroupCodesToMigrate(season);
			if(AccGroupList != null && AccGroupList.size()>0)
			{
				for (int j = 0; j < AccGroupList.size(); j++)
				{
					Map tempMap1 = new HashMap();
					tempMap1 = (Map) AccGroupList.get(j);
					logger.info("========tempMap1==========" + tempMap1);
					int accGroupCode = (Integer) tempMap1.get("accountgroupcode");
					
					List AccountGroupData = commonService.getAccountGroupData(season,accGroupCode);
					if(AccountGroupData != null && AccountGroupData.size()>0)
					{
						Map AccSumMap = new HashMap();
						AccSumMap = (Map) AccountGroupData.get(0);
						double cr = (Double) AccSumMap.get("cr");
						double dr=(Double) AccSumMap.get("dr");
						if(cr>dr)
						{
							runBal=cr-dr;
							balType="Cr";
						}
						else if(dr>cr)
						{
							runBal=dr-cr;
							balType="Dr";
						}
						else
						{
							runBal=0.0;
							balType="Cr";
						}
						TransactionType=balType;
						AccountGroupSummary accountGroupSummary = new AccountGroupSummary();
						accountGroupSummary.setAccountgroupcode(accGroupCode);
						accountGroupSummary.setBalancetype(balType);
						accountGroupSummary.setRunningbalance(runBal);
						accountGroupSummary.setSeason(season);
						entityList.add(accountGroupSummary);
					}
				}
			}
			List AccSubGroupList = ahFuctionalService.getAccSubGroupCodesToMigrate(season);
			if(AccSubGroupList != null && AccSubGroupList.size()>0)
			{
				for (int j = 0; j < AccSubGroupList.size(); j++)
				{
					Map tempMap1 = new HashMap();
					tempMap1 = (Map) AccSubGroupList.get(j);
					logger.info("========tempMap1==========" + tempMap1);
					int accSubGroupCode = (Integer) tempMap1.get("accountsubgroupcode");
					int accountgroupcode = (Integer) tempMap1.get("accountgroupcode");

					List AccSubGroupData = commonService.getAccSubGroupData(season,accSubGroupCode);
					if(AccSubGroupData != null && AccSubGroupData.size()>0)
					{
						Map AccSumMap = new HashMap();
						AccSumMap = (Map) AccSubGroupData.get(0);
						double cr = (Double) AccSumMap.get("cr");
						double dr=(Double) AccSumMap.get("dr");
						if(cr>dr)
						{
							runBal=cr-dr;
							balType="Cr";
						}
						else if(dr>cr)
						{
							runBal=dr-cr;
							balType="Dr";
						}
						else
						{
							runBal=0.0;
							balType="Cr";
						}
						TransactionType=balType;
						AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
						accountSubGroupSummary.setAccountgroupcode(accountgroupcode);
						accountSubGroupSummary.setAccountsubgroupcode(accSubGroupCode);
						accountSubGroupSummary.setBalancetype(balType);
						accountSubGroupSummary.setRunningbalance(runBal);
						accountSubGroupSummary.setSeason(season);
						
						entityList.add(accountSubGroupSummary);
					}
				}
			}
			isInsertSuccess = commonService.saveMultipleEntities(entityList);
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	

	
	
	
	
	
	@RequestMapping(value = "/getMigrateVillageData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getMigrateVillageData(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;
		List entityList = new ArrayList();
		List VillageList = ahFuctionalService.getAgreementDataToMigrate("SssVillage");
		logger.info("========VillageList==========" + VillageList);

		try	
		{
			if(VillageList != null && VillageList.size()>0)
			{
				for (int i = 0; i < VillageList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) VillageList.get(i);
					logger.info("========tempMap==========" + tempMap);

					String villageCode = (String) tempMap.get("TK_CODE");
					String villageName = (String) tempMap.get("TK_NAME");
					double distance = (Double) tempMap.get("TK_KM");
					double circleCode = (Double) tempMap.get("TK_CIRC");
					double otkCode = (Double) tempMap.get("OTK_CODE");
					double ccCode = (Double) tempMap.get("CC_CODE");
					double mandalCode = (Double) tempMap.get("M_NO");

					Village village = new Village();
					
					//int nvillageCode = Integer.parseInt(villageCode);
					//String strvillageCode = Integer.toString(nvillageCode);
					village.setVillagecode(villageCode);
					
					double ddistance = (double) distance;
					village.setDistance(ddistance);
					
					int nmandalCode = (int) mandalCode;
					List<Mandal> mandal = ahFuctionalService.getMandalDtls(nmandalCode);
					int mandalId = 0;
					if(mandal != null && mandal.size()>0)
					{
						mandalId = mandal.get(0).getId();
					}
					village.setMandalcode(mandalId);
					
					village.setStatus((byte) 0);
					village.setVillage(villageName);
					village.setDescription("NA");
					
					villageService.addVillage(village);
				}
				isInsertSuccess = true;
			}
			else
			{
				isInsertSuccess = true;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	@RequestMapping(value = "/getMigrateBankData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getMigrateBankData(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;	      
		try	
		{    
			List BankList = ahFuctionalService.getAgreementDataToMigrate("SssBank");
			logger.info("========BankList==========" + BankList);

			if(BankList != null && BankList.size()>0)
			{
				/*for(int j=0;j<9;j++)
				{
					Map tempMap1 = new HashMap();
					tempMap1 = (Map) BankList.get(j);
					String bankName = (String) tempMap1.get("BANK_NAME");
					String glCode = (String) tempMap1.get("GL_CODE");
					if(glCode == null || glCode == "null" || glCode == "")
					{
						glCode = "0";
					}
					double bCode = (Double) tempMap1.get("BANK_CODE");
					
					AccountMaster accountMaster = new AccountMaster();
					
					accountMaster.setAccountcode(glCode);
					accountMaster.setAccountname(bankName);
					accountMaster.setAccounttype(3);
					int AccGrpCode = companyAdvanceService.GetAccGrpCode("current asset");
					accountMaster.setAccountgroupcode(AccGrpCode);
					accountMaster.setAccountsubgroupcode(3);
					companyAdvanceService.addAccountMaster(accountMaster);
					Date date = new Date();
					
					CompanyAdvance companyAdvance = new CompanyAdvance();
					
					companyAdvance.setAdvancecode((int) bCode);
					companyAdvance.setAdvance(bankName);
					companyAdvance.setMaximumamount(100000.0);
					companyAdvance.setGlCode(glCode);
					companyAdvance.setAdvancemode((byte) 1);
					companyAdvance.setSubsidypart((byte) 1);
					companyAdvance.setAmount(0.0);
					companyAdvance.setAdvancepercent(0.0);
					companyAdvance.setAdvancefor(1);
					companyAdvance.setIsseedlingadvance((byte) 1);
					companyAdvance.setSeedlingcost(0.0);
					companyAdvance.setStatus((byte) 0);
					companyAdvance.setInterestapplicable((byte) 1);
					companyAdvance.setDescription("NA");
					companyAdvance.setIsamount((byte) 0);
					
					companyAdvance.setCalculationdate(date);
					companyAdvanceService.addCompanyAdvance(companyAdvance);
				}*/
				for (int i = 9; i <BankList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) BankList.get(i);
					logger.info("========tempMap==========" + tempMap);

					Bank bank = new Bank();
					Integer maxId = commonService.getMaxID("Bank");

					Branch branch=new Branch();
					String bankName = (String) tempMap.get("BANK_NAME");
					String glCode = (String) tempMap.get("GL_CODE");
					if(glCode==null)
					{
						glCode="0";
					}
					int BkCode=0;
					String bName[]=bankName.split(",");
					double bCode = (Double) tempMap.get("BANK_CODE");
					int bankCode = (int)bCode;
					/*List bankVerify = ahFuctionalService.getBankVerification(bName[0]);
					if(bankVerify == null || bankVerify.size() == 0)
					{
						bank.setBankname(bName[0]);
						bank.setBankcode(maxId);
						BkCode = maxId;
						bankService.addBank(bank);
						branch.setBankcode(maxId);
					}
					else
					{
						List bbcode = ahFuctionalService.getBankCode(bName[0]);
						Integer bbbcode=(Integer)((Map<Object,Object>)bbcode.get(0)).get("bankcode");
						branch.setBankcode(bbbcode);
						BkCode = bbbcode;
					}*/
					
					
					/*branch.setBranchcode(bankCode);

					branch.setBranchname(bName[0]+" "+bName[1]);
					branch.setCity(bName[1]);
					branch.setAddress("gajuwaka");
					branch.setContactperson("naidu");
					long contactnumber=888616661;
					long districtcode=0;
					long statecode=0;
					branch.setContactnumber(contactnumber);
					branch.setDistrictcode(districtcode);
					branch.setEmail("naidu@gmail.com");
				
					Integer maxIdForBranch = ahFuctionalService.maxIdForBranch(BkCode);
					//maxIdForBranch = maxIdForBranch+1;
					branch.setId(maxIdForBranch);
					
					branch.setIfsccode("1");
					branch.setMicrcode("1");
					branch.setStatecode(statecode);
					branch.setStatus("0");*/
				
					AccountMaster accountMaster = new AccountMaster();
					if(glCode.equals("0"))
					{
						accountMaster.setAccountcode(String.valueOf(bankCode));
						//branch.setGlcode(String.valueOf(bankCode));
						//branch.setGlcode((long) bankCode);

					}
					else
					{
						accountMaster.setAccountcode(glCode);
						//branch.setGlcode(glCode);
						//branch.setGlcode(Long.parseLong(glCode));
					}
					
					accountMaster.setAccountname(bName[0]+" "+bName[1]);
					accountMaster.setAccounttype(3);
					int AccGrpCode = companyAdvanceService.GetAccGrpCode("current asset");
					accountMaster.setAccountgroupcode(AccGrpCode);
					accountMaster.setAccountsubgroupcode(3);
					
					//bankService.addBranch(branch);
					ryotService.addAccountMaster(accountMaster);
				}
				isInsertSuccess = true;
			}
			else
			{
				isInsertSuccess = true;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}


	//Code to transfer data to Ryot,AccountMaster,AgreementDetails,AgreementSummary
	@RequestMapping(value = "/migrateAgreementData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody	boolean migrateAgreementData(@RequestBody String jsonData) throws Exception 
	{
		List AgreementList = new ArrayList();
		boolean isInserSuccess = false;
		AgreementList = ahFuctionalService.getAgreementDataToMigrate("SssAgeementsAndRyots");
		logger.info("========AgreementList==========" + AgreementList);

		try
		{
			if(AgreementList != null && AgreementList.size()>0)
			{
				for (int i = 0; i < AgreementList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) AgreementList.get(i);
					logger.info("========tempMap==========" + tempMap);

					double offerSlno = (Double) tempMap.get("OFFER_SLNO");
					//String date = (String) tempMap.get("OFFER_DT");
					
					Timestamp offerdate = (java.sql.Timestamp) tempMap.get("OFFER_DT");
					String  date = DateUtils.getStringFromTimestamp(offerdate,"dd-MM-yyyy");

					String ryotCode = (String) tempMap.get("RYOT_CODE");
					
					String mandalCode = ryotCode.substring(0, 2);
					//String villCode = ryotCode.substring(0, 4);
					String ryotSeqNo = ryotCode.substring(4,8);
					logger.info("========ryotSeqNo==========" + ryotSeqNo);

					int nryotSeqNo = Integer.parseInt(ryotSeqNo);
					logger.info("========nryotSeqNo==========" + nryotSeqNo);

					String villCode = ryotCode.substring(0, 4);
					logger.info("========villCode==========" + villCode);

					String salutation = (String) tempMap.get("TITLE_CODE");
					String ryotName = (String) tempMap.get("NAME");
					String fatherorMother = (String) tempMap.get("F_H_G_NAME");
					String landVillageCode = (String) tempMap.get("LVCOD");
					double circleCode = (Double) tempMap.get("CI_CODE");
					
					String variety1 = (String) tempMap.get("VARIETY_1");
					int plantOrRatoon1 = ((Double) tempMap.get("PL_OR_RT_1")).intValue();
					plantOrRatoon1 = plantOrRatoon1+1;
					
					//String plantOrRatoon1date = (String) tempMap.get("DT_PR_1");
					Timestamp plantOrRatoon1date1 = (java.sql.Timestamp) tempMap.get("DT_PR_1");
					String  plantOrRatoon1date = DateUtils.getStringFromTimestamp(plantOrRatoon1date1,"dd-MM-yyyy");
					
					double extent1 = (Double) tempMap.get("EXTENT_1");
					double agreedQty1 = (Double) tempMap.get("AGR_QTY_1");
					String survey1 = (String) tempMap.get("SUR_1");
					
					String variety2 = (String) tempMap.get("VARIETY_2");
					int plantOrRatoon2 = ((Double) tempMap.get("PL_OR_RT_2")).intValue();
					plantOrRatoon2 = plantOrRatoon2+1;

					//String plantOrRatoon2date = (String) tempMap.get("DT_PR_2");
					Timestamp plantOrRatoon2date1 = (java.sql.Timestamp) tempMap.get("DT_PR_2");
					String  plantOrRatoon2date = DateUtils.getStringFromTimestamp(plantOrRatoon2date1,"dd-MM-yyyy");

					double extent2 = (Double) tempMap.get("EXTENT_2");
					double agreedQty2 = (Double) tempMap.get("AGR_QTY_2");
					String survey2 = (String) tempMap.get("SUR_2");

					String variety3 = (String) tempMap.get("VARIETY_3");
					int plantOrRatoon3 = ((Double) tempMap.get("PL_OR_RT_3")).intValue();
					plantOrRatoon3 = plantOrRatoon3+1;

					//String plantOrRatoon3date = (String) tempMap.get("DT_PR_3");
					Timestamp plantOrRatoon3date1 = (java.sql.Timestamp) tempMap.get("DT_PR_3");
					String  plantOrRatoon3date = DateUtils.getStringFromTimestamp(plantOrRatoon3date1,"dd-MM-yyyy");

					double extent3 = (Double) tempMap.get("EXTENT_3");
					double agreedQty3 = (Double) tempMap.get("AGR_QTY_3");
					String survey3 = (String) tempMap.get("SUR_3");

					String variety4 = (String) tempMap.get("VARIETY_4");
					int plantOrRatoon4 = ((Double) tempMap.get("PL_OR_RT_4")).intValue();
					plantOrRatoon4 = plantOrRatoon4+1;

					//String plantOrRatoon4date = (String) tempMap.get("DT_PR_4");
					Timestamp plantOrRatoon4date1 = (java.sql.Timestamp) tempMap.get("DT_PR_4");
					String  plantOrRatoon4date = DateUtils.getStringFromTimestamp(plantOrRatoon4date1,"dd-MM-yyyy");
					
					double extent4 = (Double) tempMap.get("EXTENT_4");
					double agreedQty4 = (Double) tempMap.get("AGR_QTY_4");
					String survey4 = (String) tempMap.get("SUR_4");
					
					double manrpar = (Double) tempMap.get("MANRPAR");

					double bankCode = (Double) tempMap.get("BK_CODE");
					String bkAccNo = (String) tempMap.get("SB_AC_NO");

					double mNo = (Double) tempMap.get("M_NO");
					int nmandalCode = (int) mNo;

					String speedPar1 = (String) tempMap.get("SEEDPAR_1");
					String speedPar2 = (String) tempMap.get("SEEDPAR_2");
					String speedPar3 = (String) tempMap.get("SEEDPAR_3");
					String speedPar4 = (String) tempMap.get("SEEDPAR_4");

					int plot1 = ((Double) tempMap.get("PLOT_1")).intValue();
					int plot2 = ((Double) tempMap.get("PLOT_2")).intValue();
					int plot3 = ((Double) tempMap.get("PLOT_3")).intValue();
					int plot4 = ((Double) tempMap.get("PLOT_4")).intValue();

					//Ryot
					Ryot ryot = new Ryot();
					
					ryot.setRyotcode(ryotCode);
					//Integer maxId = commonService.getMaxID("Ryot");
					ryot.setRyotcodesequence(nryotSeqNo);
					ryot.setAadhaarNumber("123456789111");
					ryot.setAadhaarimagepath("NA");
					ryot.setPannumber("1234567890");
					
					int ncircleCode = (int) circleCode;
					List<Circle> circle = ahFuctionalService.getCircleDtls(ncircleCode);
				/*	int circleid=0;
					if(circle != null && circle.size()>0)
					{
						circleid=circle.get(0).getId();
						
					}
				*/	ryot.setCircleCode(ncircleCode);
					ryot.setVillagecode(villCode);

					List<Mandal> mandal = ahFuctionalService.getMandalDtls(nmandalCode);
					/*int mandalId = 0;
					if(mandal != null && mandal.size()>0)
					{
						mandalId = mandal.get(0).getId();
					}*/
					ryot.setMandalcode(nmandalCode);

					
					ryot.setCity("City");
					ryot.setAddress("Address");
					ryot.setRelation("FATHER");
					
					ryot.setRelativename(fatherorMother);
					ryot.setRyotphotopath("NA");
					ryot.setSalutation(salutation);
					
					ryot.setStatus((byte) 0);
					ryot.setMobilenumber("8886166614");
					ryot.setRyotname(ryotName);
					ryot.setRyottype((byte) 1);
					
					//Inserting Record here in ryot
					List<Ryot> ryot1 = ryotService.getRyotByRyotCodeNew(ryotCode);
					if(ryot1 == null || ryot1.size()==0)
					{
						ryotService.addRyot(ryot);
					}
					
					//AccountMaster
					AccountMaster accountMaster = new AccountMaster();
					
					accountMaster.setAccountcode(ryotCode);
					accountMaster.setAccountname(ryotName);
					
					int AccGrpCode = companyAdvanceService.GetAccGrpCode("Sundry Creditors");
					int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCode(AccGrpCode);

					accountMaster.setAccounttype(0);
					accountMaster.setAccountgroupcode(AccGrpCode);
					accountMaster.setAccountsubgroupcode(AccSubGrpCode);
					if(ryot1 == null || ryot1.size()==0)
					{
						ryotService.addAccountMaster(accountMaster);
					}
					
					//Inserting Record here in AgreementSummary
					AgreementSummary agreementSummary = new AgreementSummary();
					//Integer AgreementmaxId = commonService.getMaxColumnValue("AgreementSummary","agreementseqno","seasonyear","2016-2017");
					
					double offerNo = (Double) tempMap.get("OFFER_SLNO");
					logger.info("========offerNo==========" + offerNo);

					//int AgreementId = Integer.parseInt(AgreementId);
					int agreementId = (int) offerNo;
					int AgreementmaxId = agreementId;
					logger.info("========AgreementmaxId==========" + AgreementmaxId);

					agreementSummary.setAgreementseqno(AgreementmaxId);
					String strSeqNo = Integer.toString(AgreementmaxId);
					String strAgreementNo = strSeqNo+"/16-17";
					agreementSummary.setAgreementnumber(strAgreementNo);
					
					agreementSummary.setAgreementdate(DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT));
					
					int nbankCode = (int) bankCode;
					agreementSummary.setBranchcode(nbankCode);
					
					agreementSummary.setAccountnumber(bkAccNo);
					agreementSummary.setMandalcode(nmandalCode);
					agreementSummary.setCirclecode(ncircleCode);
					agreementSummary.setFamilygroupcode(0);
					agreementSummary.setSeasonyear("2016-2017");
					agreementSummary.setRyotcode(ryotCode);
					agreementSummary.setRemarks("NA");
					agreementSummary.setStatus((byte) 0);
					agreementSummary.setHasfamilygroup((byte) 1);
					//added by sahadeva 27/10/2016
					agreementSummary.setLoginId(loginController.getLoggedInUserName());
					agreementSummary.setSurityPerson("0");
					agreementSummary.setSurityPersonName("NA");
					agreementSummary.setSurityPersonaadhaarNumber("123456789111");
					
					ahFuctionalService.addAgreementSummary(agreementSummary,"No");
					 
					
					Map BankMap = new HashMap();
					BankMap.put("ACCOUNT_NUMBER", bkAccNo);
					BankMap.put("RYOT_CODE", ryotCode);
					BankMap.put("SEASON", "2016-2017");
					
					int branchId = 0;
					/*List<Branch> branch = ahFuctionalService.getBranchDtls(nbankCode);
					if(branch != null && branch.size()>0)
					{
						branchId = branch.get(0).getId();
					}*/
					BankMap.put("BRANCH_CODE", nbankCode);

					RyotBankDetails ryotBankDetails = prepareModelForRyotBranchNew(BankMap);
					ryotService.addRyotBankDetails(ryotBankDetails);
					
					AgreementDetails agreementDetails = new AgreementDetails();
			
					int caneManagerId=0;
					int fieldOfficerId=0;
					int fieldAssistantId=0;

					List CircleList = new ArrayList();
					CircleList = ahFuctionalService.getCircleDetailsNew(ncircleCode);
					if (CircleList != null && CircleList.size() > 0) 
					{
						Map CircleMap = new HashMap();
						CircleMap = (Map) CircleList.get(0);
						caneManagerId = (Integer) CircleMap.get("canemanagerid");
						fieldOfficerId = (Integer) CircleMap.get("fieldofficerid");
						fieldAssistantId = (Integer) CircleMap.get("fieldassistantid");
					}					

					//variety1
					int plotSeqNo = 0;

					if(variety1 != null)
					{
						agreementDetails.setAgreementnumber(strAgreementNo);
						
						plotSeqNo = ahFuctionalService.getMaxIDforPlotSeqNo("AgreementDetails",strAgreementNo);
						agreementDetails.setPlotseqno(plotSeqNo);
						
						agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setCirclecode(ncircleCode);
						agreementDetails.setLandtype(0);
						
						agreementDetails.setLandvillagecode(landVillageCode);
						String strplot1 = Integer.toString(plot1);
						agreementDetails.setPlotnumber(strplot1);
						
						double dextent1 = (double) extent1;
						agreementDetails.setExtentsize(dextent1);
						
						
						double dagreedQty1 = (double) agreedQty1;
						agreementDetails.setAgreedqty(dagreedQty1);
						
						//plantOrRatoon1
						String strplantOrRatoon1 = Integer.toString(plantOrRatoon1);
						agreementDetails.setPlantorratoon(strplantOrRatoon1);
						
						agreementDetails.setCropdate(DateUtils.getSqlDateFromString(plantOrRatoon1date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setRyotcode(ryotCode);
						agreementDetails.setRyotname(ryotName);
						
						agreementDetails.setRelativename(fatherorMother);
						agreementDetails.setRelation("NA");
						agreementDetails.setRyotsalutation(salutation);
						agreementDetails.setSuretyryotcode("NA");
						agreementDetails.setSurveynumber(survey1);
						agreementDetails.setMandalcode(nmandalCode);
						
						//variety1
						int variety1Code = 0;
						List<Integer> VarietyList = new ArrayList<Integer>();
						VarietyList = ahFuctionalService.getVarietyDetailsByNameNew(variety1);
						if (VarietyList != null && VarietyList.size() > 0) 
						{
							int myResult1 = VarietyList.get(0);
							variety1Code = myResult1;
						}
						else
						{
							variety1Code=9;//hard coded
						}
						agreementDetails.setVarietycode(variety1Code);
						
					
						agreementDetails.setVillagecode(villCode);
						
						//get it from mandal
						int zoneCode = 0;
						List<Integer> ZoneList = new ArrayList<Integer>();
						ZoneList = ahFuctionalService.getZoneCodeByMandalNew(nmandalCode);
						if (ZoneList != null && ZoneList.size() > 0) 
						{
							int myResult1 = ZoneList.get(0);
							zoneCode = myResult1;
						}
						agreementDetails.setZonecode(zoneCode);
						agreementDetails.setSeasonyear("2016-2017");
						
						
						agreementDetails.setCanemanagerid(caneManagerId);
						agreementDetails.setFieldofficerid(fieldOfficerId);
						agreementDetails.setFieldassistantid(fieldAssistantId);						
						agreementDetails.setProgressiveqty(0.0);
						if(ryot1 == null || ryot1.size()==0)
						{
							agreementDetails.setRyotseqno(nryotSeqNo);
						}
						else
						{
							agreementDetails.setRyotseqno(ryot1.get(0).getRyotcodesequence());
						}
						
						agreementDetails.setCommercialOrSeed((byte) 0);
						//Added by DMurty on 07-09-2016
						agreementDetails.setPlotdistance(0.0);
						agreementDetails.setAddress("NA");
						
						ahFuctionalService.addAgreementDetails(agreementDetails);
					}
					
					if(variety2 != null)
					{
						agreementDetails.setAgreementnumber(strAgreementNo);
						
						plotSeqNo = ahFuctionalService.getMaxIDforPlotSeqNo("AgreementDetails",strAgreementNo);
						agreementDetails.setPlotseqno(plotSeqNo);
											
						agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setCirclecode(ncircleCode);
						agreementDetails.setLandtype(0);
						
						agreementDetails.setLandvillagecode(landVillageCode);
						
						
						String strplot2 = Integer.toString(plot2);
						agreementDetails.setPlotnumber(strplot2);
						
						double dextent2 = (double) extent2;
						agreementDetails.setExtentsize(dextent2);
						
						
						double dagreedQty2 = (double) agreedQty2;
						agreementDetails.setAgreedqty(dagreedQty2);
						
						//plantOrRatoon1
						String strplantOrRatoon2 = Integer.toString(plantOrRatoon2);
						agreementDetails.setPlantorratoon(strplantOrRatoon2);
						
						agreementDetails.setCropdate(DateUtils.getSqlDateFromString(plantOrRatoon2date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setRyotcode(ryotCode);
						agreementDetails.setRyotname(ryotName);
						
						agreementDetails.setRelativename(fatherorMother);
						agreementDetails.setRelation("NA");
						agreementDetails.setRyotsalutation(salutation);
						agreementDetails.setSuretyryotcode("NA");
						agreementDetails.setSurveynumber(survey2);
						agreementDetails.setMandalcode(nmandalCode);
						
						int variety2Code = 0;
						List<Integer> VarietyList = new ArrayList<Integer>();
						VarietyList = ahFuctionalService.getVarietyDetailsByNameNew(variety2);
						if (VarietyList != null && VarietyList.size() > 0) 
						{
							int myResult1 = VarietyList.get(0);
							variety2Code = myResult1;
						}
						else
						{
							variety2Code=9;
						}
						agreementDetails.setVarietycode(variety2Code);
					
						agreementDetails.setVillagecode(villCode);
						
						//get it from mandal
						int zoneCode = 0;
						List<Integer> ZoneList = new ArrayList<Integer>();
						ZoneList = ahFuctionalService.getZoneCodeByMandalNew(nmandalCode);
						if (ZoneList != null && ZoneList.size() > 0) 
						{
							int myResult1 = ZoneList.get(0);
							zoneCode = myResult1;
						}
						agreementDetails.setZonecode(zoneCode);
						agreementDetails.setSeasonyear("2016-2017");
						
						agreementDetails.setCanemanagerid(caneManagerId);
						agreementDetails.setFieldofficerid(fieldOfficerId);
						agreementDetails.setFieldassistantid(fieldAssistantId);					
						agreementDetails.setProgressiveqty(0.0);
						if(ryot1 == null || ryot1.size()==0)
						{
							agreementDetails.setRyotseqno(nryotSeqNo);
						}
						else
						{
							agreementDetails.setRyotseqno(ryot1.get(0).getRyotcodesequence());
						}				
						agreementDetails.setCommercialOrSeed((byte) 0);
						//Added by DMurty on 07-09-2016
						agreementDetails.setPlotdistance(0.0);
						agreementDetails.setAddress("NA");

						ahFuctionalService.addAgreementDetails(agreementDetails);
					}
					
					if(variety3 != null)
					{

						agreementDetails.setAgreementnumber(strAgreementNo);

						plotSeqNo = ahFuctionalService.getMaxIDforPlotSeqNo("AgreementDetails",strAgreementNo);
						agreementDetails.setPlotseqno(plotSeqNo);
						
						agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setCirclecode(ncircleCode);
						agreementDetails.setLandtype(0);
						
						agreementDetails.setLandvillagecode(landVillageCode);
						String strplot3 = Integer.toString(plot3);
						agreementDetails.setPlotnumber(strplot3);
						
						double dextent3 = (double) extent3;
						agreementDetails.setExtentsize(dextent3);
						
						
						double dagreedQty3 = (double) agreedQty3;
						agreementDetails.setAgreedqty(dagreedQty3);
						
						//plantOrRatoon1
						String strplantOrRatoon3 = Integer.toString(plantOrRatoon3);
						agreementDetails.setPlantorratoon(strplantOrRatoon3);
						
						agreementDetails.setCropdate(DateUtils.getSqlDateFromString(plantOrRatoon3date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setRyotcode(ryotCode);
						agreementDetails.setRyotname(ryotName);
						
						agreementDetails.setRelativename(fatherorMother);
						agreementDetails.setRelation("NA");
						agreementDetails.setRyotsalutation(salutation);
						agreementDetails.setSuretyryotcode("NA");
						agreementDetails.setSurveynumber(survey3);
						agreementDetails.setMandalcode(nmandalCode);
						
						int variety3Code = 0;
						List<Integer> VarietyList = new ArrayList<Integer>();
						VarietyList = ahFuctionalService.getVarietyDetailsByNameNew(variety3);
						if (VarietyList != null && VarietyList.size() > 0) 
						{
							int myResult1 = VarietyList.get(0);
							variety3Code = myResult1;
						}
						else
						{
							variety3Code=9;
						}
						agreementDetails.setVarietycode(variety3Code);
						agreementDetails.setVillagecode(villCode);
						
						//get it from mandal
						int zoneCode = 0;
						List<Integer> ZoneList = new ArrayList<Integer>();
						ZoneList = ahFuctionalService.getZoneCodeByMandalNew(nmandalCode);
						if (ZoneList != null && ZoneList.size() > 0) 
						{
							int myResult1 = ZoneList.get(0);
							zoneCode = myResult1;
						}
						agreementDetails.setZonecode(zoneCode);
						agreementDetails.setSeasonyear("2016-2017");
						
						agreementDetails.setCanemanagerid(caneManagerId);
						agreementDetails.setFieldofficerid(fieldOfficerId);
						agreementDetails.setFieldassistantid(fieldAssistantId);					
						agreementDetails.setProgressiveqty(0.0);
						if(ryot1 == null || ryot1.size()==0)
						{
							agreementDetails.setRyotseqno(nryotSeqNo);
						}
						else
						{
							agreementDetails.setRyotseqno(ryot1.get(0).getRyotcodesequence());
						}	
						agreementDetails.setCommercialOrSeed((byte) 0);
						//Added by DMurty on 07-09-2016
						agreementDetails.setPlotdistance(0.0);
						agreementDetails.setAddress("NA");

						ahFuctionalService.addAgreementDetails(agreementDetails);
					}
					
					if(variety4 != null)
					{
						agreementDetails.setAgreementnumber(strAgreementNo);

						plotSeqNo = ahFuctionalService.getMaxIDforPlotSeqNo("AgreementDetails",strAgreementNo);
						agreementDetails.setPlotseqno(plotSeqNo);
						
						agreementDetails.setAgreementdate(DateUtils.getSqlDateFromString(date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setCirclecode(ncircleCode);
						agreementDetails.setLandtype(0);
						
						agreementDetails.setLandvillagecode(landVillageCode);
						String strplot4 = Integer.toString(plot4);
						agreementDetails.setPlotnumber(strplot4);
						
						double dextent4 = (double) extent4;
						agreementDetails.setExtentsize(dextent4);
						
						
						double dagreedQty4 = (double) agreedQty4;
						agreementDetails.setAgreedqty(dagreedQty4);
						
						//plantOrRatoon1
						String strplantOrRatoon4 = Integer.toString(plantOrRatoon4);
						agreementDetails.setPlantorratoon(strplantOrRatoon4);
						
						agreementDetails.setCropdate(DateUtils.getSqlDateFromString(plantOrRatoon4date,Constants.GenericDateFormat.DATE_FORMAT));

						agreementDetails.setRyotcode(ryotCode);
						agreementDetails.setRyotname(ryotName);
						
						agreementDetails.setRelativename(fatherorMother);
						agreementDetails.setRelation("NA");
						agreementDetails.setRyotsalutation(salutation);
						agreementDetails.setSuretyryotcode("NA");
						agreementDetails.setSurveynumber(survey4);
						agreementDetails.setMandalcode(nmandalCode);
						
						int variety4Code = 0;
						List<Integer> VarietyList = new ArrayList<Integer>();
						VarietyList = ahFuctionalService.getVarietyDetailsByNameNew(variety4);
						if (VarietyList != null && VarietyList.size() > 0) 
						{
							int myResult1 = VarietyList.get(0);
							variety4Code = myResult1;
						}
						else
						{
							variety4Code=9;
						}
						agreementDetails.setVarietycode(variety4Code);
						agreementDetails.setVillagecode(villCode);
						
						//get it from mandal
						int zoneCode = 0;
						List<Integer> ZoneList = new ArrayList<Integer>();
						ZoneList = ahFuctionalService.getZoneCodeByMandalNew(nmandalCode);
						if (ZoneList != null && ZoneList.size() > 0) 
						{
							int myResult1 = ZoneList.get(0);
							zoneCode = myResult1;
						}
						agreementDetails.setZonecode(zoneCode);
						agreementDetails.setSeasonyear("2016-2017");
						
						agreementDetails.setCanemanagerid(caneManagerId);
						agreementDetails.setFieldofficerid(fieldOfficerId);
						agreementDetails.setFieldassistantid(fieldAssistantId);					
						agreementDetails.setProgressiveqty(0.0);
						
						if(ryot1 == null || ryot1.size()==0)
						{
							agreementDetails.setRyotseqno(nryotSeqNo);
						}
						else
						{
							agreementDetails.setRyotseqno(ryot1.get(0).getRyotcodesequence());
						}
						agreementDetails.setCommercialOrSeed((byte) 0);
						//Added by DMurty on 07-09-2016
						agreementDetails.setPlotdistance(0.0);
						agreementDetails.setAddress("NA");
						
						ahFuctionalService.addAgreementDetails(agreementDetails);
					}
				}	
				isInserSuccess = true;
			}
			else
			{
				isInserSuccess = true;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInserSuccess;
		}
		return isInserSuccess;
	}
	
	@RequestMapping(value = "/getMigrateLoanData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean getMigrateLoanData(@RequestBody String jsonData) throws Exception 
	{
		boolean isInsertSuccess = false;	  
		List LoanList = new ArrayList();
		boolean isInserSuccess = false;
		LoanList = ahFuctionalService.getAgreementDataToMigrate("SssLoans");
		logger.info("========LoanList==========" + LoanList);
		try	
		{
			if(LoanList != null && LoanList.size()>0)
			{
				for (int i = 0; i < LoanList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) LoanList.get(i);
					logger.info("========tempMap==========" + tempMap);
					
					double lplantextent = (Double) tempMap.get("LI_PEXTE");
					double lratoonextent = (Double) tempMap.get("LI_REXTE");
					double lAmt = (Double) tempMap.get("LI_PAMT");
					double liNo = (Double) tempMap.get("LI_NO");
					int nliNo = (int) liNo;
					double lbkCode = (Double) tempMap.get("LI_BK_COD");
					int bankCode = (int) lbkCode;
					String ryotCode = (String) tempMap.get("LI_RYOT");
					String lrefNo = (String) tempMap.get("LI_REF_NO");
					//String strLirefNo = Double.toString(lrefNo);
					
					Date loanDate = null;
					String ldate = (String) tempMap.get("LI_DATE");
					if(ldate != null)
					{
						loanDate = DateUtils.getSqlDateFromString(ldate,Constants.GenericDateFormat.DATE_FORMAT);
					}
					
					
					
					LoanSummary loanSummary = new LoanSummary();
					
					loanSummary.setSeason("2016-2017");
					loanSummary.setRyotcode(ryotCode);
					loanSummary.setPrinciple(lAmt);
					
					loanSummary.setLoanstatus(0);
					loanSummary.setPaidamount(0.00);
					loanSummary.setTotalamount(lAmt);
					loanSummary.setDisbursedamount(lAmt);
					loanSummary.setPendingamount(lAmt);
					loanSummary.setInterestamount(0.0);
					//GetMax Loan no
					
					//int maxLoanNum = ahFuctionalService.getMaxLoanNo("LoanDetails","loannumber",bankCode);
					/*List ls=ahFuctionalService.getMaxLoanNoList(bankCode);
					
					int maxLoanNum=ls.size();
					maxLoanNum++;*/
					loanSummary.setLoannumber(nliNo);
					loanSummary.setBranchcode(bankCode);

					ahFuctionalService.addLoanSummary(loanSummary);

					
					LoanDetails loanDetails = new LoanDetails();
					
					String agNumber = "";
					List AgreementList = ahFuctionalService.getAgreementsByRyot(ryotCode,"2016-2017");
					if(AgreementList != null && AgreementList.size()>0)
					{
						for (int j = 0; j < AgreementList.size(); j++)
						{
							Map AgreementMap = new HashMap();
							AgreementMap = (Map) AgreementList.get(j);
							String AggNo = (String) AgreementMap.get("agreementnumber");
							agNumber += AggNo+",";
						}
						//agNumber = agNumber.substring(0, agNumber.length()-1);
					}
					
					String AgNoforCricle = "";
					String surveyNo = "";
					Timestamp AggDate = null;
					if(AgreementList != null && AgreementList.size()>0)
					{
						Map AgreementMap = new HashMap();
						AgreementMap = (Map) AgreementList.get(0);
						AgNoforCricle = (String) AgreementMap.get("agreementnumber");
						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByCode(AgNoforCricle,"2016-2017");
						if(agreementDetails != null && agreementDetails.size()>0)
						{
							surveyNo = agreementDetails.get(0).getSurveynumber();
						}
						AggDate = (java.sql.Timestamp) AgreementMap.get("agreementdate");
					}	

					int circleCode = 0;
					List<Integer> CodeList = new ArrayList<Integer>();
					CodeList = ahFuctionalService.getCircleCodeByAgreementNo(AgNoforCricle);
					if (CodeList != null && CodeList.size() > 0) 
					{
						int myResult1 = CodeList.get(0);
						circleCode = myResult1;
					}
				
					int caneManagerId = 0;
					int fieldOfficerId = 0;
					int fieldAssistantId = 0;
					List CircleList = new ArrayList();
					CircleList = ahFuctionalService.getCircleDetailsNew(circleCode);
					if (CircleList != null && CircleList.size() > 0) 
					{
						Map CircleMap = new HashMap();
						CircleMap = (Map) CircleList.get(0);
						caneManagerId = (Integer) CircleMap.get("canemanagerid");
						fieldOfficerId = (Integer) CircleMap.get("fieldofficerid");
						fieldAssistantId = (Integer) CircleMap.get("fieldassistantid");
					}	
				
					loanDetails.setAgreementnumber(agNumber);
					//loanDetails.setBranchcode(bankCode);
					//loanDetails.setLoannumber(maxLoanNum);
					loanDetails.setSuveynumber(surveyNo);
					
					//Modified by DMurty on 02-06-2016 for migration for loan data
					CompositePrKeyForLoans compositePrKeyForLoans = new CompositePrKeyForLoans();
					compositePrKeyForLoans.setBranchcode(bankCode);
					compositePrKeyForLoans.setLoannumber(nliNo);
					//Modified by DMurty on 21-02-2017
					compositePrKeyForLoans.setSeason("2016-2017");
					
					loanDetails.setCompositePrKeyForLoans(compositePrKeyForLoans);
					
					String  strAggDate = DateUtils.getStringFromTimestamp(AggDate,"dd-MM-yyyy");
					loanDetails.setRecommendeddate(DateUtils.getSqlDateFromString(strAggDate,Constants.GenericDateFormat.DATE_FORMAT));
					
					String strlratoonextent = Double.toString(lratoonextent);
					loanDetails.setRatoonextent(strlratoonextent);
					
					loanDetails.setRyotcode(ryotCode);
					
					String strlplantextent = Double.toString(lplantextent);

					loanDetails.setPlantextent(strlplantextent);
					
					loanDetails.setPrinciple(lAmt);
					
					loanDetails.setLoanstatus(0);
					loanDetails.setPaidamount(0.00);
					loanDetails.setTotalamount(lAmt);
					
					String strmaxLoanNum = Integer.toString(nliNo);
					
					String addedLoan = strmaxLoanNum+"-"+ryotCode;
					loanDetails.setAddedloan(addedLoan);

					loanDetails.setCanemanagerid(caneManagerId);
					loanDetails.setFieldofficerid(fieldOfficerId);
					loanDetails.setFieldassistantid(fieldAssistantId);
					loanDetails.setDisbursedamount(lAmt);
					loanDetails.setDisburseddate(loanDate);
					loanDetails.setInterestamount(0.0);
					loanDetails.setInterstrate(0.0);
					//added by sahadeva 27/10/2016
					loanDetails.setLoginid(loginController.getLoggedInUserName());
					loanDetails.setPendingamount(lAmt);
					loanDetails.setTransactioncode(0);
					loanDetails.setReferencenumber(lrefNo);
					
					ahFuctionalService.addLoanDetailsNew(loanDetails);
				}
				isInsertSuccess = true;	  
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInsertSuccess;
		}
		return isInsertSuccess;
	}
	
	//Code to transfer data to Ryot,AccountMaster,AgreementDetails,AgreementSummary
	@RequestMapping(value = "/migrateOldRyotData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean migrateOldRyotData(@RequestBody String jsonData) throws Exception 
	{
		List AgreementList = new ArrayList();
		boolean isInserSuccess = false;
		AgreementList = ahFuctionalService.getAgreementDataToMigrate("SssAgeementsAndRyots");		
		try
		{
			if(AgreementList != null && AgreementList.size()>0)
			{
				for (int i = 0; i < AgreementList.size(); i++)
				{
					logger.info("========i==========" + i);

					Map tempMap = new HashMap();
					tempMap = (Map) AgreementList.get(i);
					logger.info("========tempMap==========" + tempMap);
	
					String ryotCode = (String) tempMap.get("RYOT_CODE");
					String ryotSeqNo = ryotCode.substring(4,8);
					int nryotSeqNo = Integer.parseInt(ryotSeqNo);
					logger.info("========nryotSeqNo==========" + nryotSeqNo);
					logger.info("========ryotSeqNo==========" + ryotSeqNo);

					String salutation = (String) tempMap.get("TITLE_CODE");
					String ryotName = (String) tempMap.get("NAME");
					String fatherorMother = (String) tempMap.get("F_H_G_NAME");
					String landVillageCode = (String) tempMap.get("LVCOD");
					double circleCode = (Double) tempMap.get("CI_CODE");
				
					double bankCode = (Double) tempMap.get("BK_CODE");
					String bkAccNo = (String) tempMap.get("SB_AC_NO");

					double mNo = (Double) tempMap.get("M_NO");
					//int nmandalCode = (int) mNo;

					//ryotCode
					String mandalCode = ryotCode.substring(0, 2);
					int nmandalCode = Integer.parseInt(mandalCode);
					String villCode = ryotCode.substring(0, 4);
					logger.info("========villCode==========" + villCode);

					List CircleList = new ArrayList();

					int caneManagerId = 0;
					int fieldOfficerId = 0;
					int fieldAssistantId = 0;
					int circlecode = 0;
					CircleList = ahFuctionalService.getCircleDetailsByVillageCode(villCode);
					if (CircleList != null && CircleList.size() > 0) 
					{
						Map CircleMap = new HashMap();
						CircleMap = (Map) CircleList.get(0);
						caneManagerId = (Integer) CircleMap.get("canemanagerid");
						fieldOfficerId = (Integer) CircleMap.get("fieldofficerid");
						fieldAssistantId = (Integer) CircleMap.get("fieldassistantid");
						circlecode = (Integer) CircleMap.get("circlecode");
					}	
					else
					{
						//villCode
						int ncircleCode = (int) circleCode;
						CircleList = ahFuctionalService.getCircleDetailsNew(ncircleCode);
						if (CircleList != null && CircleList.size() > 0) 
						{
							Map CircleMap = new HashMap();
							CircleMap = (Map) CircleList.get(0);
							caneManagerId = (Integer) CircleMap.get("canemanagerid");
							fieldOfficerId = (Integer) CircleMap.get("fieldofficerid");
							fieldAssistantId = (Integer) CircleMap.get("fieldassistantid");
							circlecode = (Integer) CircleMap.get("circlecode");
						}
					}
					//Ryot
					Ryot ryot = new Ryot();
					
					ryot.setRyotcode(ryotCode);
					//Integer maxId = commonService.getMaxID("Ryot");
					ryot.setRyotcodesequence(nryotSeqNo);
					ryot.setAadhaarNumber("12345");
					ryot.setAadhaarimagepath("NA");
					ryot.setPannumber("12345");
					ryot.setCircleCode(circlecode);
					
					ryot.setVillagecode(villCode);

					/*List<Mandal> mandal = ahFuctionalService.getMandalDtls(nmandalCode);
					int mandalId = 0;
					if(mandal != null && mandal.size()>0)
					{
						mandalId = mandal.get(0).getId();
					}*/
					ryot.setMandalcode(nmandalCode);
					ryot.setCity("City");
					ryot.setAddress("NA");
					ryot.setRelation("FATHER");
					
					ryot.setRelativename(fatherorMother);
					ryot.setRyotphotopath("NA");
					ryot.setSalutation(salutation);
					
					ryot.setStatus((byte) 1);
					ryot.setMobilenumber("8886166614");
					ryot.setRyotname(ryotName);
					//Added by DMurty on 07-09-2016
					ryot.setRyottype((byte) 1);
					//Inserting Record here in ryot
					List<Ryot> ryot1 = ryotService.getRyotByRyotCodeNew(ryotCode);
					if(ryot1 == null || ryot1.size()==0)
					{
						ryotService.addRyot(ryot);
					}
					
					//AccountMaster
					AccountMaster accountMaster = new AccountMaster();
					
					accountMaster.setAccountcode(ryotCode);
					accountMaster.setAccountname(ryotName);
					
					int AccGrpCode = companyAdvanceService.GetAccGrpCode("Sundry Creditors");
					int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCode(AccGrpCode);

					accountMaster.setAccounttype(0);
					accountMaster.setAccountgroupcode(AccGrpCode);
					accountMaster.setAccountsubgroupcode(AccSubGrpCode);
					if(ryot1 == null || ryot1.size()==0)
					{
						ryotService.addAccountMaster(accountMaster);
					}
					
					
					int nbankCode = (int) bankCode;
					Map BankMap = new HashMap();
					BankMap.put("ACCOUNT_NUMBER", bkAccNo);
					BankMap.put("RYOT_CODE", ryotCode);
					BankMap.put("SEASON", "2016-2017");
					
					/*int branchId = 0;
					List<Branch> branch = ahFuctionalService.getBranchDtls(nbankCode);
					if(branch != null && branch.size()>0)
					{
						branchId = branch.get(0).getId();
					}*/
					
					BankMap.put("BRANCH_CODE", nbankCode);

					RyotBankDetails ryotBankDetails = prepareModelForRyotBranchNew(BankMap);
					if(ryot1 == null || ryot1.size()==0)
					{
						ryotService.addRyotBankDetails(ryotBankDetails);	
					}
				}
				isInserSuccess = true;
			}
			else
			{
				isInserSuccess = true;
			}
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
			return isInserSuccess;
		}
		return isInserSuccess;
	}
	//Added by DMurty on 26-07-2016
	@RequestMapping(value = "/updateRyotNames", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean updateRyotNames(@RequestBody String jsonData) throws Exception 
	{
		List RyotList = new ArrayList();
		List UpdateList = new ArrayList();
		List entityList = new ArrayList();

		boolean isInsertSuccess = false;
		String strQry = null;
		Object Qryobj = null;
		
		RyotList = ahFuctionalService.updateRyotNames("AccountMaster");
		if(RyotList != null && RyotList.size()>0)
		{
			for (int i = 0; i < RyotList.size(); i++)
			{
				logger.info("========i==========" + i);
				Map tempMap = new HashMap();
				tempMap = (Map) RyotList.get(i);
				logger.info("========tempMap==========" + tempMap);

				String ryotCode = (String) tempMap.get("accountcode");
				String ryotName = "";
				List RyotNamesList = ahFuctionalService.getRyotNameNew(ryotCode);
				if (RyotNamesList != null && RyotNamesList.size() > 0) 
				{
					Map nameMap = new HashMap();
					nameMap = (Map) RyotNamesList.get(0);
					ryotName = (String) nameMap.get("ryotname");
					
					strQry = "UPDATE AccountMaster set accountname='"+ryotName+"' WHERE accountcode ='"+ryotCode+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}	
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList);
		}
		return isInsertSuccess;
	}
	
	@RequestMapping(value = "/printCaneWeighment", method = RequestMethod.POST)
	public @ResponseBody
	JSONArray printCaneWeighment(@RequestBody JSONObject jsonData) throws Exception 
	{
		int serailNumber=(Integer)jsonData.get("serailNumber");
		String season=(String)jsonData.get("season");
		int lorryStatus=(Integer)jsonData.get("lorryStatus");
		logger.info("printCaneWeighment()========jsonData==========" + jsonData);

		JSONArray jArray = new JSONArray();
		JSONObject jsonObj =null;
		jsonObj = new JSONObject();
		List CaneWeighmentList=commonService.getPrintCaneWeighment(serailNumber,season);
		logger.info("printCaneWeighment()========CaneWeighmentList==========" + CaneWeighmentList);
		if(CaneWeighmentList != null && CaneWeighmentList.size()>0)
		{
			for (int j = 0; j < CaneWeighmentList.size(); j++)
			{
				Map CaneWeightmentMap = new HashMap();
				CaneWeightmentMap = (Map) CaneWeighmentList.get(j);
				String rName = (String) CaneWeightmentMap.get("ryotname");
				Integer slNo = (Integer) CaneWeightmentMap.get("serialnumber");
				Integer actualserialno = (Integer) CaneWeightmentMap.get("actualserialnumber");

				String rcode = (String) CaneWeightmentMap.get("ryotcode");
				List<Ryot> relativename= ahFuctionalService.getRelativeNameByRyotCode(rcode);
				 String relativeName = null;
				 if(relativename != null && relativename.size()>0)
				 {
					 relativeName = relativename.get(0).getRelativename();
				 }
				
				String lvlgcode = (String) CaneWeightmentMap.get("landvilcode");
				Integer shift = (Integer) CaneWeightmentMap.get("shiftid");

				Date canereceiptdate = null;//(Date) CaneWeightmentMap.get("canereceiptdate"); 
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String strcanereceiptdate = null;//df.format(canereceiptdate);
				
				//String date[]=canereceiptdate.toString().split(" ");
				if(actualserialno == 0)
				{
					//This is for lorry in time
					Timestamp canereceipttime = (Timestamp) CaneWeightmentMap.get("canereceipttime");
					//As per the old software, they are showing shift date.
					Timestamp systemstartdate = (Timestamp) CaneWeightmentMap.get("canereceiptdate");
					String recDate = null;
					Date date = new Date(systemstartdate.getTime());
					df = new SimpleDateFormat("dd-MM-yyyy");
					recDate = df.format(date);

					String time[]=canereceipttime.toString().split(" ");
					canereceiptdate = (Date) CaneWeightmentMap.get("canereceiptdate");
					strcanereceiptdate = df.format(canereceiptdate);
					//time[1] = time[1].substring(0,time[1].length()-2);
					time[1] = time[1].substring(0, 8);
					jsonObj.put("canereceipttime", time[1]);
					jsonObj.put("systemdate", recDate);
				}
				else
				{
					Timestamp canereceipttime = (Timestamp) CaneWeightmentMap.get("canereceiptendtime");
					Timestamp systemdate = null;//(Timestamp) CaneWeightmentMap.get("systemenddate");
					String recDate = null;
					//This is for reprint weighment.
					if(lorryStatus == 0)
					{
						systemdate = (Timestamp) CaneWeightmentMap.get("canereceiptdate");
						Date date = new Date(systemdate.getTime());
						df = new SimpleDateFormat("dd-MM-yyyy");
						recDate = df.format(date);
					}
					else
					{
						systemdate = (Timestamp) CaneWeightmentMap.get("canereceiptenddate");
						Date date = new Date(systemdate.getTime());
						df = new SimpleDateFormat("dd-MM-yyyy");
						recDate = df.format(date);
					}
					canereceiptdate = (Date) CaneWeightmentMap.get("canereceiptenddate");
					strcanereceiptdate = df.format(canereceiptdate);
					String time[]=canereceipttime.toString().split(" ");
					//time[1] = time[1].substring(0,time[1].length()-2);
					time[1] = time[1].substring(0, 8);
					jsonObj.put("canereceipttime", time[1]);
					jsonObj.put("systemdate", recDate);
				}
				
				Integer weighbridgeslno = (Integer) CaneWeightmentMap.get("weighbridgeslno");
				String weighBridge = null;
				if(lorryStatus == 0)
				{
					weighBridge = "FIRST";
				}
				else
				{
					if(actualserialno == 0)
					{
						weighBridge = "FIRST";
					}
					else 
					{
						weighBridge = "SECOND";
					}
				}
				
				Integer vehicletype = (Integer) CaneWeightmentMap.get("vehicletype");
				String vehicleno = (String) CaneWeightmentMap.get("vehicleno");
				
				Double grossweight = (Double) CaneWeightmentMap.get("lorryandcanewt");
				Double netwt = (Double) CaneWeightmentMap.get("netwt");
				Double lorrywt = (Double) CaneWeightmentMap.get("lorrywt");
	
				String villagecode = (String) CaneWeightmentMap.get("villagecode");
				Integer permitNo = (Integer) CaneWeightmentMap.get("permitnumber");

				List<Village> villagename= ahFuctionalService.getVillageNameByvillageCode(villagecode);
				 String villageName = null;
				 if(villagename != null && villagename.size()>0)
				 {
					 villageName = villagename.get(0).getVillage();
				 }
				Integer varietycode = (Integer) CaneWeightmentMap.get("varietycode");
				String strvarietycode = Integer.toString(varietycode);
				
				List<Variety> variety= ahFuctionalService.getVarietyNameByvarietyCode(varietycode);
				 String varietyName = null;
				 if(variety != null && variety.size()>0)
				 {
					 varietyName = variety.get(0).getVariety();
				 }
				 //varietyName = strvarietycode+" "+varietyName;
					 
				Integer circlecode = (Integer) CaneWeightmentMap.get("circlecode");
				List<Circle> circle= ahFuctionalService.getCircleNameBycircleCode(circlecode);
				 String circleName = null;
				 if(circle != null && circle.size()>0)
				 {
					 circleName = circle.get(0).getCircle();
				 }
				 else circleName="NA";
				Byte plantorratoon=(Byte) CaneWeightmentMap.get("plantorratoon");
				Integer ptorrt=plantorratoon.intValue();
				List<CropTypes> crop= ahFuctionalService.getCropType(ptorrt);
				 String cropName = null;
				 if(crop != null && crop.size()>0)
				 {
					 cropName = crop.get(0).getCroptype();
				 }
				 
				List<Village> landvillage= ahFuctionalService.getVillageNameByvillageCode(lvlgcode);
				 String landvillageName = null;
				 if(landvillage != null && landvillage.size()>0)
				 {
					 landvillageName = landvillage.get(0).getVillage();
				 }

				 String vehicleType="Big Lorry";
				 if(vehicletype == 0)
				 {
					 vehicleType="Tractor";
				 }
				 else if(vehicletype == 1)
				 {
					 vehicleType="Small Lorry";
				 }
				 
				 double grossWt = (Double) CaneWeightmentMap.get("grossweight");
				 double netWt = (Double) CaneWeightmentMap.get("netwt");

				 double bindingMaterial = grossWt-netWt;
				 String strbindingMaterial = HMSDecFormatter.format(bindingMaterial);
				 //String strgrossWt = HMSDecFormatter.format(grossWt);
				 //String strnetWt = HMSDecFormatter.format(netWt);

				 int ucCode = 0;
				 if((Integer) CaneWeightmentMap.get("uccode") != null)
				 {
					 ucCode = (Integer) CaneWeightmentMap.get("uccode");
				 }
				 
				jsonObj.put("ryotname", rName);
				jsonObj.put("serialnumber", slNo);
				jsonObj.put("ryotcode", rcode);
				jsonObj.put("landvilcode", lvlgcode);
				jsonObj.put("landvillageName", landvillageName);
				
				//Modified by DMurty on 02-12-2016
				if(actualserialno == 0)
				{
					String shiftName = null;
					if(shift == 1)
					{
						shiftName = "A";
					}
					else if(shift == 2)
					{
						shiftName = "B";
					}
					else
					{
						shiftName = "C";
					}
					jsonObj.put("shiftid", shiftName);
					
					String login = null;
					String loginUser = (String) CaneWeightmentMap.get("loginid");
					List EmployeesList = commonService.getEmployeeDetails(loginUser);
					if(EmployeesList != null && EmployeesList.size()>0)
					{
						Employees emp = (Employees) EmployeesList.get(0);
						login = emp.getEmployeename();
					}
					jsonObj.put("login", login);
				}
				else
				{
					Integer shift1 = (Integer) CaneWeightmentMap.get("shiftid1");
					String shiftName = null;
					if(shift1 == 1)
					{
						shiftName = "A";
					}
					else if(shift1 == 2)
					{
						shiftName = "B";
					}
					else
					{
						shiftName = "C";
					}
					jsonObj.put("shiftid", shiftName);
					
					String login = null;
					String loginUser = (String) CaneWeightmentMap.get("shiftname");
					if(loginUser != null)
					{
						List EmployeesList = commonService.getEmployeeDetails(loginUser);
						if(EmployeesList != null && EmployeesList.size()>0)
						{
							Employees emp = (Employees) EmployeesList.get(0);
							login = emp.getEmployeename();
						}
						jsonObj.put("login", login);
					}
					else
					{
						List EmployeesList = commonService.getEmployeeDetails(loginUser);
						if(EmployeesList != null && EmployeesList.size()>0)
						{
							Employees emp = (Employees) EmployeesList.get(0);
							login = emp.getEmployeename();
						}
						jsonObj.put("login", login);
					}
				}
				
				jsonObj.put("canereceiptdate", strcanereceiptdate);
				jsonObj.put("weighbridgeslno", weighbridgeslno);
				jsonObj.put("weighBridge", weighBridge);
				jsonObj.put("vehicletype", vehicleType);
				jsonObj.put("vehicleno", vehicleno);
				jsonObj.put("grossweight", grossweight);
				jsonObj.put("netwt", netwt);
				jsonObj.put("lorrywt", lorrywt);
				jsonObj.put("relativeName", relativeName);
				jsonObj.put("villageName", villageName);
				jsonObj.put("varietyName", varietyName);  
				jsonObj.put("circleName", circleName);  
				jsonObj.put("cropName", cropName); 
				jsonObj.put("villagecode", villagecode);
				jsonObj.put("circlecode", circlecode);
				jsonObj.put("circlecode", circlecode);
				jsonObj.put("actualSlNo", actualserialno);
				jsonObj.put("permitNo", permitNo);
				jsonObj.put("grossWt", grossWt);
				jsonObj.put("bindingMaterial", strbindingMaterial);
				jsonObj.put("netWt", netWt);
				jsonObj.put("ucCode", ucCode);
				
				jArray.add(jsonObj);
			}
		}
		logger.info("printCaneWeighment()========jArray==========" + jArray);
		return jArray;
	}
	
	
//------------------------------------written by naidu-----------------------------------	
	//to transfer the data of 15-16
	@RequestMapping(value = "/transportingMigration", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean transportingMigration() throws Exception 
	{
		boolean IsInsertSuccess = false;
		try
		{
			//for cane weighment details
			TransferCaneWtDtls("2015-2016");
			//Transport Subsidy
			TransferTSData("2015-2016");
			//For Advances
			TransferAdvances("2015-2016");
			//For Loans and SB AC payment
			TransferSBAC("2015-2016");
			//For ACP payment
			TransferACPData("2015-2016");
			//For CP Subsidy
			TransferCPData("2015-2016");
			
			IsInsertSuccess = true;
			
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		return IsInsertSuccess;
	}
	
	//to transfer the data of 13-14
	@RequestMapping(value = "/Migrate1314Accounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean Migrate1314Accounts() throws Exception 
	{
		boolean IsInsertSuccess = false;
		try
		{
			//for cane weighment details
			TransferCaneWtDtls("2013-2014");
			//Transport Subsidy
			TransferTSData("2013-2014");
			//For Advances
			TransferAdvances("2013-2014");
			//For Loans and SB AC payment
			TransferSBAC("2013-2014");
			//For Bank Charges
			TransferBankCharges("2013-2014");
			//For Opening Balances
			TransferOBData("2013-2014");
			//For Opening Balances
			TransferOBPayData("2013-2014");			

			
			IsInsertSuccess = true;
			
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		return IsInsertSuccess;
	}	
	
	//to transfer the data of 14-15
	@RequestMapping(value = "/Migrate1415Accounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean Migrate1415Accounts() throws Exception 
	{
		boolean IsInsertSuccess = false;
		try
		{
			//for cane weighment details
			TransferCaneWtDtls("2014-2015");
			//Transport Subsidy
			TransferTSData("2014-2015");
			//For Advances
			TransferAdvances("2014-2015");
			//For Loans and SB AC payment
			TransferSBAC("2014-2015");
		
			IsInsertSuccess = true;
			
		}
		catch (Exception e)
		{
			logger.info(e.getCause(), e);
		}
		return IsInsertSuccess;
	}	
	
	private boolean TransferCaneWtDtls(String season) throws Exception
	{		
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean isInsertSuccess=false;
		List trm=ahFuctionalService.getAllCaneReceiptData(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;

		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
					
					mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
					
					type=(Integer)mp.get("acctype");
					JrnlMemo =(String)mp.get("description");
					if(type==3)
					{
						for(int k=1;k<=4;k++)
						{
							nqty=(Double)mp.get("c_supqty");
							JrnlMemo =(String)mp.get("description");
						
						  if(k==1)
						  {
						  frprate=(Double)mp.get("frprate");  
						  finalAmtToPost=frprate*nqty;
						  JrnlMemo=JrnlMemo+"-"+nqty+" @Frp "+frprate;
						  }
					  else if(k==2)
					  {
						  
						  hsrate=(Double)mp.get("hsrate"); 
					  finalAmtToPost=hsrate*nqty;
					  JrnlMemo=JrnlMemo+"-"+nqty+" @Hs "+hsrate;
					  }
					  else if(k==3)
					  {
						  JrnlMemo="To CDC Charges";
					  finalAmtToPost=4*nqty;
					  JrnlMemo=JrnlMemo+"-"+nqty;
					  }
					  else if(k==4)
					  {
						  JrnlMemo="To UL Charges";
						  finalAmtToPost=11*nqty;
						  JrnlMemo=JrnlMemo + "-" + nqty + "tons";
					  }
						
					 
					accountCode =(String)mp.get("ryotcode");	
					rtname=ahFuctionalService.getRyotRecordName(accountCode);
					Session session=hibernatedao.getSessionFactory().openSession();
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rttname=accountCode;				
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					
					session.flush();
					session.close();
					cdate=(String)mp.get("cdate");
					//Integer advcodes=(Integer)mp.get("advcodes");
						
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					DtlsMap.put("LOGIN_USER",loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					/*if(type==20)
					{
					JrnlMemo=ahFuctionalService.getAdvName(advcodes);
					if(JrnlMemo==null ||JrnlMemo.equalsIgnoreCase("null") || JrnlMemo.equals("") )
						JrnlMemo="NA";
					}*/
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					if(k==1 || k==2)
					{
						DtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType = "Cr";
					}
					else if(k==3 || k==4)
					{
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						TransactionType = "Dr";
					}
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON",season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
						/*
								if(type==3 || type==30)
								TransactionType = "Cr";
								else
								TransactionType="Dr";
					*/			
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
					
					Map AccGrpDtlsMap = new HashMap();
					
					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);

					if(k==1 || k==2)
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else if(k==3 || k==4)
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON",season);
					
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					/*if(type==3 || type==30)
					TransactionType = "Cr";
					else
					TransactionType = "Dr";*/
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();
					
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					/*if(type==20)
					{
					JrnlMemo=ahFuctionalService.getAdvName(advcodes);
					if(JrnlMemo==null)
						JrnlMemo="NA";
					}*/
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					if(k==1 || k==2)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else if(k==3 || k==4)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON",season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					/*if(type==3 || type==30)
					TransactionType = "Cr";
					else
					TransactionType = "Dr";*/
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
					
					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{					
						trmentityList.add(accountSubGroupSummary);
					}
						
							isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
						} //end of for()
					} //end of if()
					else
					{					
						finalAmtToPost=(Double)mp.get("amount");	
						accountCode =(String)mp.get("ryotcode");	
						rtname=ahFuctionalService.getRyotRecordName(accountCode);
						Session session=hibernatedao.getSessionFactory().openSession();
						if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
						{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rttname=accountCode;
						
						}
							verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
						if(!verifyAccount)
						{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						 session.save(accountMaster);
						}
						 session.flush();
						 session.close();
						cdate=(String)mp.get("cdate");
						Map DtlsMap = new HashMap();
						
						DtlsMap.put("TRANSACTION_CODE", transactionCode);
						DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						//added by sahadeva 27/10/2016
						DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						DtlsMap.put("ACCOUNT_CODE", accountCode);
						DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
						DtlsMap.put("C_Date", cdate);
						if(type==30)
						{
						DtlsMap.put("TRANSACTION_TYPE", "Cr");
						TransactionType = "Cr";
						}
						else
						{
						DtlsMap.put("TRANSACTION_TYPE", "Dr");
						TransactionType="Dr";
						}
						//Added by DMurty on 18-10-2016
						DtlsMap.put("SEASON",season);
						
						AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
						trmentityList.add(accountDetails);
						AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
						List BalList = ahFuctionalService.BalDetails(accountCode,season);
						if (BalList != null && BalList.size() > 0) 
						{
							double finalAmt = accountSummary.getRunningbalance();
							String strCrDr = accountSummary.getBalancetype();
							String AccCode = accountSummary.getAccountcode();
							
							strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						} 
						else 
						{
							trmentityList.add(accountSummary);
						}
						// Insert Rec in Acc Grp Details
						int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
						
						Map AccGrpDtlsMap = new HashMap();
						
						AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
						AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						//added by sahadeva 27/10/2016
						AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
						AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
						AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
						AccGrpDtlsMap.put("C_Date", cdate);
						if(type==30)
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						else
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
						//Added by DMurty on 18-10-2016
						AccGrpDtlsMap.put("SEASON",season);
						
						AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
						trmentityList.add(accountGroupDetails);
						AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
						
						List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
						if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
						{
							double finalAmt = accountGroupSummary.getRunningbalance();
							String strCrDr = accountGroupSummary.getBalancetype();
							int AccGroupCode = accountGroupSummary.getAccountgroupcode();
							
							strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						}
						else 
						{
							trmentityList.add(accountGroupSummary);
						}
						
						Map AccSubGrpDtlsMap = new HashMap();
						
						AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
						AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
						//added by sahadeva 27/10/2016
						AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
						
						AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
						AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
						/*if(type==20)
						{
						JrnlMemo=ahFuctionalService.getAdvName(advcodes);
						if(JrnlMemo==null)
							JrnlMemo="NA";
						}*/
						AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
						AccSubGrpDtlsMap.put("C_Date", cdate);
						if(type==30)
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
						else
						AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
						//Added by DMurty on 18-10-2016
						AccSubGrpDtlsMap.put("SEASON",season);
						
						AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
						trmentityList.add(accountSubGroupDetails);
						/*if(type==3 || type==30)
						TransactionType = "Cr";
						else
						TransactionType = "Dr";*/
						AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
						
						double finalAmt = accountSubGroupSummary.getRunningbalance();
						String strCrDr = accountSubGroupSummary.getBalancetype();
						int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
						int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
						
						List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
						if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
						{
							strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
							Qryobj = strQry;
							UpdateList.add(Qryobj);
						} 
						else 
						{  
							trmentityList.add(accountSubGroupSummary);
						}
						isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
					} //end of else
				} //end of for()	
			} //end of if()
			return isInsertSuccess;
		} //end of try()
		catch(Exception e)
		{
			return isInsertSuccess;
		}
	}
	
	private boolean TransferTSData(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getTransportSubsidyData(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		//String season=null;
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
					mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			  		accountCode =(String)mp.get("c_ryotcode");	
					rtname=ahFuctionalService.getRyotRecordName(accountCode);
					Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rtname=accountCode;
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					
					session.flush();
					session.close();
					
					nqty=(Double)mp.get("c_supqty"); 
					finalAmtToPost =(Double)mp.get("c_rec");
					JrnlMemo =(String)mp.get("description");
					JrnlMemo = JrnlMemo + " : " + nqty + " tons";
					type=(Integer)mp.get("transactiontype");
					cdate=(String)mp.get("cdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType = "Cr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
				
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				} //end of for()
			} //end of if()

		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For transferring Advances data
	//---------------this is for tempadd & tempbank & tempcaneamount------------------------------------------
	private boolean TransferAdvances(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getAdvances(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("lm_ryot");	
					rtname=ahFuctionalService.getRyotRecordName(accountCode);
					Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rtname=accountCode;
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					nAdvCode = (Integer)mp.get("lm_bk_cod"); 
					strAdvance = ahFuctionalService.getAdvName(nAdvCode);
					
					finalAmtToPost =(Double)mp.get("lm_amt_rec");
					JrnlMemo =(String)mp.get("description");
					JrnlMemo = JrnlMemo + strAdvance;
					
					cdate=(String)mp.get("lm_date");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON",season);
							
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON",season);
				
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					if(type==3 || type==30)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON",season);
				
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For SB and Loan account
	private boolean TransferSBAC(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getSBandLoanPayments(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("cdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					if(type==3 || type==30)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For Additional Cane Payment Details
	private boolean TransferACPData(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getACPDetails();
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("cdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
							
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For cane payments which were as subsidy
	private boolean TransferCPData(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getCPDetails();
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("cdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
							
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 28/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//To transfer additional Bank Charges
	private boolean TransferBankCharges(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.TransferBankCharges(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("bcdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					DtlsMap.put("LOGIN_USER", "makella");
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccGrpDtlsMap.put("LOGIN_USER", "makella");
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccSubGrpDtlsMap.put("LOGIN_USER", "makella");
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					if(type==3 || type==30)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For Opening Balance Details
	private boolean TransferOBData(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getOBData(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";		

		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + "TransferOBData==" +(Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("obdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					DtlsMap.put("LOGIN_USER", "makella");
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType="Cr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
							
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccGrpDtlsMap.put("LOGIN_USER", "makella");
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccSubGrpDtlsMap.put("LOGIN_USER", "makella");
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");	
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}
	
	//For Final payments
		private boolean TransferOBPayData(String season) throws Exception
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean IsSuccess = false;
		List trm=ahFuctionalService.getFinalPayDetails(season);
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		String TransactionType=null;
		int nAdvCode = 0;
		String strAdvance = "";
		
		try
		{
			int transactionCode=0;
			if(trm.size()>0 && trm!=null)
			{			
				for(int i=0;i<trm.size();i++)
				{
					trmentityList.clear();
					UpdateList.clear();
			
			 		mp=(HashMap)trm.get(i);
					
					logger.info("========tempMap==========" + "TransferOBPayData==" + (Integer)mp.get("id"));
					transactionCode = commonService.GetMaxTransCode(season);
					Double finalAmtToPost=0.0;
			 		accountCode =(String)mp.get("ryotcode");
			 		rttname = (String)mp.get("ryotname");

			 		Session session=hibernatedao.getSessionFactory().openSession();
					
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
					}
					
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					
					finalAmtToPost =(Double)mp.get("debit");
					JrnlMemo =(String)mp.get("description");
					
					cdate=(String)mp.get("obPdate");
					TransactionType=null;
					Map DtlsMap = new HashMap();
					
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					DtlsMap.put("LOGIN_USER", "makella");
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType="Dr";
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);

					Map AccGrpDtlsMap = new HashMap();

					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccGrpDtlsMap.put("LOGIN_USER", "makella");
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccSubGrpDtlsMap.put("LOGIN_USER", "makella");
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);

					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					if(type==3 || type==30)
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					else
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();

					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSubGroupSummary);
					}
					IsSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				}
			}
		} //end of try()
		catch(Exception e)
		{
			logger.info(e.getCause(), e);
			return IsSuccess;
		}
		return IsSuccess;
	}	
	
	//end of transfer bank charges
	private AccountMaster prepareModelForAccountMaster(String ryotname,String ryotcode)
	{
		AccountMaster accountMaster = new AccountMaster();
		
		accountMaster.setAccountcode(ryotcode);
		//Modified by DMurty on 26-07-2016
		//accountMaster.setAccountname("Sundry Creditors");
		accountMaster.setAccountname(ryotname);
		int AccGrpCode = companyAdvanceService.GetAccGrpCode("Sundry Creditors");
		int AccSubGrpCode = companyAdvanceService.GetAccSubGrpCode(AccGrpCode);

		accountMaster.setAccounttype(0);
		accountMaster.setAccountgroupcode(AccGrpCode);
		accountMaster.setAccountsubgroupcode(AccSubGrpCode); 
		
		return accountMaster;
	}

	private AccountDetails prepareModelForAccountDetailsForTransport(Map DtlsMap) 
	{

		int TransactionCode = (Integer) DtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) DtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) DtlsMap.get("LOGIN_USER");
		String AccountCode = (String) DtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) DtlsMap.get("JOURNAL_MEMO");
		String TransType = (String) DtlsMap.get("TRANSACTION_TYPE");
		String GlCode = (String) DtlsMap.get("GL_CODE");
		String cdate = (String) DtlsMap.get("C_Date");
		if ("".equals(GlCode) || "null".equals(GlCode) || (GlCode == null))
		{
			GlCode = "NA";
		}
		//Added by DMurty on 18-10-2016
		String season = (String) DtlsMap.get("SEASON");
				
		
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setTransactioncode(TransactionCode);
		if(cdate== null)
		{
			accountDetails.setTransactiondate(date);
			accountDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		}
		else
		{
		String ccdate[]=cdate.split("/");
		cdate=ccdate[1]+"-"+ccdate[0]+"-"+ccdate[2];
		Date fromdate=DateUtils.getSqlDateFromString(cdate,Constants.GenericDateFormat.DATE_FORMAT);
		accountDetails.setTransactiondate(fromdate);
		accountDetails.setTransactiontime(new Timestamp(fromdate.getTime()));
		}
		accountDetails.setAccountcode(AccountCode);
		accountDetails.setJournalmemo(journalMemo);
		if ("Dr".equalsIgnoreCase(TransType))
		{
			accountDetails.setDr(Amt);
			accountDetails.setCr(0.0);
		} 
		else
		{
			accountDetails.setDr(0.0);
			accountDetails.setCr(Amt);
		}
		accountDetails.setRunningbalance(Amt);
		//added by sahadeva 28/10/2016
		accountDetails.setLoginuser(loginController.getLoggedInUserName());
		
		accountDetails.setIsapplyforledger((byte) 0);
		accountDetails.setGlcode(GlCode);
		accountDetails.setStatus((byte) 1);
		accountDetails.setSeason(season);
		return accountDetails;
	}
	private AccountGroupDetails prepareModelForAccGrpDetailsForTransport(Map AccGrpDtlsMap)
	{
		int TransactionCode = (Integer) AccGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccGrpDtlsMap.get("TRANSACTION_TYPE");
		String cdate=(String)AccGrpDtlsMap.get("C_Date");
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		AccountGroupDetails accountGroupDetails = new AccountGroupDetails();
		accountGroupDetails.setTransactioncode(TransactionCode);
		//Added by DMurty on 18-10-2016
		String season = (String) AccGrpDtlsMap.get("SEASON");
		if(cdate== null)
		{
			accountGroupDetails.setTransactiondate(date);
			accountGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		}
		else
		{
			String ccdate[]=cdate.split("/");
			cdate=ccdate[1]+"-"+ccdate[0]+"-"+ccdate[2];
			Date fromdate=DateUtils.getSqlDateFromString(cdate,Constants.GenericDateFormat.DATE_FORMAT);
			accountGroupDetails.setTransactiondate(fromdate);
			accountGroupDetails.setTransactiontime(new Timestamp(fromdate.getTime()));
		}
		
		accountGroupDetails.setAccountgroupcode(AccGrpCode);
		accountGroupDetails.setJournalmemo(journalMemo);

		if ("Dr".equalsIgnoreCase(TransType)) 
		{
			accountGroupDetails.setDr(Amt);
			accountGroupDetails.setCr(0.0);
		}
		else
		{
			accountGroupDetails.setDr(0.0);
			accountGroupDetails.setCr(Amt);
		}

		accountGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 27/10/2016
		accountGroupDetails.setLoginuser(loginController.getLoggedInUserName());
		accountGroupDetails.setIsapplyforledger((byte) 1);
		accountGroupDetails.setStatus((byte) 1);
		accountGroupDetails.setSeason(season);

		return accountGroupDetails;
	}
	private AccountSubGroupDetails prepareModelForAccSubGrpDtlsForTransport(Map AccSubGrpDtlsMap)
	{
		int TransactionCode = (Integer) AccSubGrpDtlsMap.get("TRANSACTION_CODE");
		double Amt = (Double) AccSubGrpDtlsMap.get("TRANSACTION_AMOUNT");
		String loginuser = (String) AccSubGrpDtlsMap.get("LOGIN_USER");
		String AccountCode = (String) AccSubGrpDtlsMap.get("ACCOUNT_CODE");
		String journalMemo = (String) AccSubGrpDtlsMap.get("JOURNAL_MEMO");
		int AccGrpCode = (Integer) AccSubGrpDtlsMap.get("ACCOUNT_GROUP_CODE");
		String TransType = (String) AccSubGrpDtlsMap.get("TRANSACTION_TYPE");
		String cdate=(String) AccSubGrpDtlsMap.get("C_Date");
		Date date = new Date();
		java.util.Date datendtime = new java.util.Date();
		int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
		String season = (String) AccSubGrpDtlsMap.get("SEASON");
		AccountSubGroupDetails accountSubGroupDetails = new AccountSubGroupDetails();

		accountSubGroupDetails.setTransactioncode(TransactionCode);
		if(cdate == null)
		{
			accountSubGroupDetails.setTransactiondate(date);
			accountSubGroupDetails.setTransactiontime(new Timestamp(datendtime.getTime()));
		}
		else
		{
			String ccdate[]=cdate.split("/");
			cdate=ccdate[1]+"-"+ccdate[0]+"-"+ccdate[2];	
			Date fromdate=DateUtils.getSqlDateFromString(cdate,Constants.GenericDateFormat.DATE_FORMAT);
			accountSubGroupDetails.setTransactiondate(fromdate);
			accountSubGroupDetails.setTransactiontime(new Timestamp(fromdate.getTime()));
		}
		
		accountSubGroupDetails.setAccountgroupcode(AccGrpCode);
		accountSubGroupDetails.setAccountsubgroupcode(AccSubGrpCode);
		accountSubGroupDetails.setJournalmemo(journalMemo);
		if ("Dr".equalsIgnoreCase(TransType)) 
		{
			accountSubGroupDetails.setDr(Amt);
			accountSubGroupDetails.setCr(0.0);
		} 
		else
		{
			accountSubGroupDetails.setDr(0.0);
			accountSubGroupDetails.setCr(Amt);
		}
		
		accountSubGroupDetails.setRunningbalance(Amt);
		//added by sahadeva 27/10/2016
		accountSubGroupDetails.setLoginuser(loginController.getLoggedInUserName());
		
		accountSubGroupDetails.setIsapplyforledger((byte) 1);
		accountSubGroupDetails.setStatus((byte) 1);
		accountSubGroupDetails.setSeason(season);
		
		return accountSubGroupDetails;
	}
	
	@RequestMapping(value = "/generatePermitPrint", method = RequestMethod.POST)
	public @ResponseBody List generatePermitPrint(@RequestBody JSONObject jsonData) throws Exception 
	{		
		Integer pNum=(Integer)jsonData.getInt("permitNumber");

		List dataList=ahFuctionalService.generatePermitPrint(pNum);
		return dataList;
	}
	
	//----------------------------------------AdvancesToAccounts -----------------------------------//
	//this function is to create an account entry into Accounts tables for each advance
	@RequestMapping(value = "/AdvancesToAccounts", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public @ResponseBody boolean AdvancesToAccounts() throws Exception 
	{
		List UpdateList = new ArrayList();
		List trmentityList = new ArrayList();
		
		String strQry=null;
		String Qryobj=null;
		String rtname=null;
		boolean verifyAccount=false;
		boolean isInsertSuccess=false;
		List ssAdvanceList=ahFuctionalService.getAllAdvanceData();
		Map mp=null;
		Integer type=0;
		String JrnlMemo=null;
		Double nqty=0.0;
		Double frprate=0.0;
		Double hsrate=0.0;
		String accountCode=null;
		String rttname=null;
		String cdate=null;
		Double finalAmtToPost=0.0;
		String TransactionType=null;
		String advType=null;
		Integer advcode=0;
		String seedsupcode=null;
		Double quantity=0.0;
		String season="2016-2017";
		String strAdvCode = null;

		try
		{
			int transactionCode=0;
			if(ssAdvanceList.size()>0 && ssAdvanceList!=null)
			{					
				for(int i=0;i<ssAdvanceList.size();i++)
				{
					UpdateList.clear();
					trmentityList.clear();
					
					mp=(HashMap)ssAdvanceList.get(i);
					
					logger.info("========tempMap==========" + (Integer)mp.get("id"));
					transactionCode = (Integer)mp.get("transactioncode");
					advcode=(Integer)mp.get("advancecode");
					List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advcode);
					strAdvCode = companyAdvance.get(0).getGlCode();
					
					accountCode =(String)mp.get("ryotcode");	
					finalAmtToPost=(Double)mp.get("advanceamount");				  
					seedsupcode=(String)mp.get("seedsuppliercode");
					//quantity=(Integer)mp.get("seedingsquantity");
					  
					advType = ahFuctionalService.getAdvName(advcode);
					JrnlMemo = advType;
					  
					rtname=ahFuctionalService.getRyotRecordName(accountCode);
					Session session=hibernatedao.getSessionFactory().openSession();
					if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					{
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rttname=accountCode;
						continue;
					}
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					session.flush();
					session.close();
					Date tcdate=(Date)mp.get("advancedate");
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
					cdate = df.format(tcdate);
					
					//Entries for Ryot account				
					Map DtlsMap = new HashMap();
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					DtlsMap.put("GL_CODE", strAdvCode);
	
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
					Map AccGrpDtlsMap = new HashMap();
						
					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");

					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();
	
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
	
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{  
						trmentityList.add(accountSubGroupSummary);
					}					
					
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
						
					//For respective Advance
					UpdateList.clear();
					trmentityList.clear();
		
				//	session.flush();
				//	session.close();
				
					//Entries for Advance account				
					DtlsMap.clear();
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					DtlsMap.put("ACCOUNT_CODE", strAdvCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					DtlsMap.put("GL_CODE", accountCode);
		
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					accountSummary = prepareModelforAccSmry(finalAmtToPost,strAdvCode, TransactionType,season);
					BalList.clear();
					BalList = ahFuctionalService.BalDetails(strAdvCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						finalAmt = accountSummary.getRunningbalance();
						strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details

					accGrpCode = ahFuctionalService.GetAccGrpCode(strAdvCode);
					AccGrpDtlsMap.clear();
						
					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					
					
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccGrpDtlsMap.put("ACCOUNT_CODE", strAdvCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					accountGroupSummary = prepareModelForAccGrpSmry(strAdvCode, finalAmtToPost, accGrpCode, TransactionType,season);
					AccountGroupSummaryList.clear();
					AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						finalAmt = accountGroupSummary.getRunningbalance();
						strCrDr = accountGroupSummary.getBalancetype();
						AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					AccSubGrpDtlsMap.clear();
		
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", strAdvCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
		
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					accountSubGroupSummary = prepareModelforAccSubGrpSmry(strAdvCode, finalAmtToPost, accGrpCode, TransactionType,season);
					finalAmt = accountSubGroupSummary.getRunningbalance();
					strCrDr = accountSubGroupSummary.getBalancetype();
					AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
					AccountSubGroupSummaryList.clear();
					AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{  
						trmentityList.add(accountSubGroupSummary);
					}					
					
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
					
					//for seed supplier  code
					if(seedsupcode==null ||seedsupcode.equals("") || seedsupcode.equalsIgnoreCase("null"))
					{
						continue;
					}
					
					UpdateList.clear();
					trmentityList.clear();
		
					//session.flush();
					//session.close();
				
					//Entries for Seed Supplier account
					//we need to move this code for posting of seed supplier
					/*DtlsMap.clear();
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					DtlsMap.put("LOGIN_USER", "makella");
					DtlsMap.put("ACCOUNT_CODE", seedsupcode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					DtlsMap.put("GL_CODE", accountCode);
		
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					accountSummary = prepareModelforAccSmry(finalAmtToPost,seedsupcode, TransactionType,season);
					BalList = ahFuctionalService.BalDetails(seedsupcode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						finalAmt = accountSummary.getRunningbalance();
						strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+seedsupcode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					accGrpCode = ahFuctionalService.GetAccGrpCode(seedsupcode);
					AccGrpDtlsMap.clear();
						
					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccGrpDtlsMap.put("LOGIN_USER", "makella");
					AccGrpDtlsMap.put("ACCOUNT_CODE", seedsupcode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					accountGroupSummary = prepareModelForAccGrpSmry(seedsupcode, finalAmtToPost, accGrpCode, TransactionType,season);
					AccountGroupSummaryList.clear();
					AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						finalAmt = accountGroupSummary.getRunningbalance();
						strCrDr = accountGroupSummary.getBalancetype();
						AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					AccSubGrpDtlsMap.clear();
		
					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					AccSubGrpDtlsMap.put("LOGIN_USER", "makella");
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", seedsupcode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
		
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					accountSubGroupSummary = prepareModelforAccSubGrpSmry(seedsupcode, finalAmtToPost, accGrpCode, TransactionType,season);
					finalAmt = accountSubGroupSummary.getRunningbalance();
					strCrDr = accountSubGroupSummary.getBalancetype();
					AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
					AccountSubGroupSummaryList.clear();
					AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{  
						trmentityList.add(accountSubGroupSummary);
					}					
					
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);					
*/					
				
				} //end of for loop
			}
			return isInsertSuccess;
		}
		catch(Exception e)
		{
			logger.info("========SeedAndSeedMigration==========" +e);
			return isInsertSuccess;
		}
	}
	
	//-------------------------------------end of function ---------------------------------//
	
	//---------------------------------------------------SeedAndSeedAdvance Migration------------------------------------
	
		@RequestMapping(value = "/SeedAndSeedAdvanceMigration", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody boolean SeedAndSeedAdvanceMigration() throws Exception 
		{
			List UpdateList = new ArrayList();
			List trmentityList = new ArrayList();
			String strQry=null;
			String Qryobj=null;
			String rtname=null;
			boolean verifyAccount=false;
			boolean isInsertSuccess=false;
			List ssAdvanceList=ahFuctionalService.getAllSeedAndSeedAdvanceMgData();
			Map mp=null;
			Integer type=0;
			String JrnlMemo=null;
			Double nqty=0.0;
			Double frprate=0.0;
			Double hsrate=0.0;
			String accountCode=null;
			String rttname=null;
			String cdate=null;
			Double finalAmtToPost=0.0;
			String TransactionType=null;
			String advType=null;
			Integer advcode=0;
			String seedsupcode=null;
			Double quantity=0.0;
			String season=null;

			try
			{
				int transactionCode=0;
				if(ssAdvanceList.size()>0 && ssAdvanceList!=null)
				{
						
					for(int i=0;i<ssAdvanceList.size();i++)
					{
						UpdateList.clear();
						trmentityList.clear();
						
					  mp=(HashMap)ssAdvanceList.get(i);
					
					  logger.info("========tempMap==========" + (Integer)mp.get("id"));
					  transactionCode = commonService.GetMaxTransCode(season);
					  JrnlMemo =(String)mp.get("SeedorSeedlingAdvance");
					  accountCode =(String)mp.get("CODE");	
					  finalAmtToPost=(Double)mp.get("AMOUNT");
					  advType=(String)mp.get("SeedorSeedlingAdvance");
					  seedsupcode=(String)mp.get("SeedSupplierCode");
					  quantity=(Double)mp.get("Quantity");
					  advcode=ahFuctionalService.getRyotAdvCode(advType);
					  rtname=ahFuctionalService.getRyotRecordName(accountCode);
					  Session session=hibernatedao.getSessionFactory().openSession();
					  if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
					  {
						NewRyots newryot=new NewRyots();
						newryot.setRyotcode(accountCode);
						session.save(newryot);
						rttname=accountCode;
						continue;
					  }
					verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
					if(!verifyAccount)
					{
						AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
						session.save(accountMaster);
					}
					 session.flush();
					 session.close();
					 cdate=(String)mp.get("AdvanceDate");
						//Integer advcodes=(Integer)mp.get("advcodes");
					
					Map DtlsMap = new HashMap();
					DtlsMap.put("TRANSACTION_CODE", transactionCode);
					DtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					DtlsMap.put("ACCOUNT_CODE", accountCode);
					DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					DtlsMap.put("C_Date", cdate);
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					/*if(quantity==null)
					{
					DtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					}
					else
					{
					DtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType = "Cr";
					}*/
					//Added by DMurty on 18-10-2016
					DtlsMap.put("SEASON", season);
					
					AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
					trmentityList.add(accountDetails);
					AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
					List BalList = ahFuctionalService.BalDetails(accountCode,season);
					if (BalList != null && BalList.size() > 0) 
					{
						double finalAmt = accountSummary.getRunningbalance();
						String strCrDr = accountSummary.getBalancetype();
						String AccCode = accountSummary.getAccountcode();
						
						strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{
						trmentityList.add(accountSummary);
					}
					// Insert Rec in Acc Grp Details
					int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
					Map AccGrpDtlsMap = new HashMap();
						
					AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
					AccGrpDtlsMap.put("C_Date", cdate);
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					/*if(quantity==null)
					{
					AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
					TransactionType = "Dr";
					}
					else
					{
						AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
					TransactionType = "Cr";
					}*/
					//Added by DMurty on 18-10-2016
					AccGrpDtlsMap.put("SEASON", season);
					
					AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
					trmentityList.add(accountGroupDetails);
					AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					
					List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
					if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
					{
						double finalAmt = accountGroupSummary.getRunningbalance();
						String strCrDr = accountGroupSummary.getBalancetype();
						int AccGroupCode = accountGroupSummary.getAccountgroupcode();
						
						strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					}
					else 
					{
						trmentityList.add(accountGroupSummary);
					}
					
					Map AccSubGrpDtlsMap = new HashMap();

					AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
					AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
					//added by sahadeva 27/10/2016
					AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
					
					AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
					AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
					AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
					AccSubGrpDtlsMap.put("C_Date", cdate);
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
					/*if(quantity==null)
					{
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
					TransactionType = "Dr";
					}
					else
					{
					AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");	
					TransactionType = "Cr";
					}*/
					//Added by DMurty on 18-10-2016
					AccSubGrpDtlsMap.put("SEASON", season);
					
					AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
					trmentityList.add(accountSubGroupDetails);
					AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
					double finalAmt = accountSubGroupSummary.getRunningbalance();
					String strCrDr = accountSubGroupSummary.getBalancetype();
					int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
					int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
					List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
					if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
					{
						strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
						Qryobj = strQry;
						UpdateList.add(Qryobj);
					} 
					else 
					{  
						trmentityList.add(accountSubGroupSummary);
					}
						
					AdvancePrincipleAmounts advancePrincipleAmounts = prepareModelOfAPAmountsForAdvanceMigration(DtlsMap,advcode);
					AdvanceSummary advanceSummary = prepareModelOfAdvSummaryForAdvanceMigration(DtlsMap, advcode,transactionCode);
					AdvanceDetails advanceDetails = prepareModelOfAdvDetailsForAdvanceMigration(DtlsMap, advcode,transactionCode,seedsupcode,advType,quantity);
					trmentityList.add(advanceSummary);
					trmentityList.add(advanceDetails);
					trmentityList.add(advancePrincipleAmounts);
					if(quantity==null)
					{
						SeedSuppliersSummary ssuppliersSummary=prepareModelOfSeedSuppliersSummary(DtlsMap,seedsupcode);
						if(ssuppliersSummary!=null)
						trmentityList.add(ssuppliersSummary);
						boolean result=accountingDataPostingForCredit(finalAmtToPost,accountCode,JrnlMemo,cdate,rtname);
						logger.info("========SeedSupplier posting result==========" +result);					
					}
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
					}

				}
				return isInsertSuccess;
			}
			catch(Exception e)
			{
				logger.info("========SeedAndSeedMigration==========" +e);
				return isInsertSuccess;
			}
		}
		private SeedSuppliersSummary prepareModelOfSeedSuppliersSummary(Map DtlsMap,String seedsupcode)
		{
			SeedSuppliersSummary ssuppliersSummary=null;
			List  sspSummaryList=ahFuctionalService.getSeedSupplierDetails("2016-2017",seedsupcode);
			if(sspSummaryList!=null && sspSummaryList.size()>0)
			{
				ssuppliersSummary=(SeedSuppliersSummary)sspSummaryList.get(0);
				Double advpayable=ssuppliersSummary.getAdvancepayable();
				Double pendingamt=ssuppliersSummary.getPendingamount();
				if(pendingamt==null)
					pendingamt=0.0;
				if(advpayable==null)
					advpayable=0.0;
				advpayable=advpayable+(Double)DtlsMap.get("TRANSACTION_AMOUNT");
				ssuppliersSummary.setAdvancepayable(advpayable);
				Double extsize=ahFuctionalService.GetSupplierExtentSize(seedsupcode);
				ssuppliersSummary.setTotalextentsize(extsize);
			ssuppliersSummary.setPendingamount(pendingamt+(Double)DtlsMap.get("TRANSACTION_AMOUNT"));
			}
			
			return ssuppliersSummary;
			
		}
		private AdvancePrincipleAmounts prepareModelOfAPAmountsForAdvanceMigration(Map DtlsMap,Integer advcode)
		{
			AdvancePrincipleAmounts advancePrincipleAmounts =new AdvancePrincipleAmounts();
			advancePrincipleAmounts.setAdvancecode(advcode);
			advancePrincipleAmounts.setRyotcode((String)DtlsMap.get("ACCOUNT_CODE"));
			advancePrincipleAmounts.setPrinciple((Double)DtlsMap.get("TRANSACTION_AMOUNT"));
			advancePrincipleAmounts.setSeason("2016-2017");
			String ccdate[]=((String)DtlsMap.get("C_Date")).split("/");
			String cdate=ccdate[1]+"-"+ccdate[0]+"-"+ccdate[2];
			advancePrincipleAmounts.setPrincipledate(DateUtils.getSqlDateFromString(cdate,Constants.GenericDateFormat.DATE_FORMAT));
			return advancePrincipleAmounts;
		}
		private AdvanceSummary prepareModelOfAdvSummaryForAdvanceMigration(Map DtlsMap,Integer advcode,Integer transactionCode)
		{
			AdvanceSummary advSummary=new AdvanceSummary();
			Double advAmount=(Double)DtlsMap.get("TRANSACTION_AMOUNT");
			advSummary.setAdvanceamount(advAmount);
			advSummary.setAdvancecode(advcode);
			advSummary.setAdvancestatus((byte)0);
			//added by sahadeva 27/10/2016
			advSummary.setLoginid(loginController.getLoggedInUserName());
			
			advSummary.setPaidamount(0.0);
			advSummary.setPendingamount(advAmount);
			advSummary.setPendingpayable(advAmount);
			advSummary.setRecoveredamount(0.0);
			advSummary.setRyotcode((String)DtlsMap.get("ACCOUNT_CODE"));
			advSummary.setSeason("2016-2017");
			advSummary.setTotalamount(advAmount);
			advSummary.setTransactioncode(transactionCode);
			
			advSummary.setVillagecode(ahFuctionalService.GetVillageCodeForRyot((String)DtlsMap.get("ACCOUNT_CODE")));
		return 	advSummary;
		}
		private AdvanceDetails prepareModelOfAdvDetailsForAdvanceMigration(Map DtlsMap,Integer advcode,Integer transactionCode,String seedryotcode,String advType,Double quantity)
		{
			AdvanceDetails advDetails=new AdvanceDetails();
			Double advAmount=(Double)DtlsMap.get("TRANSACTION_AMOUNT");
			advDetails.setAdvanceamount(advAmount);
			advDetails.setAdvancecode(advcode);
			Integer advcatCode=ahFuctionalService.GetAdvCategoryCodeForAdvcode(advcode);
			advDetails.setAdvancecateogrycode(advcatCode);
			Integer advSubcatCode=ahFuctionalService.GetAdvSubCategoryCodeForAdvcode(advcode,advcatCode);
			advDetails.setAdvancesubcategorycode(advSubcatCode);
			String ryotcode=(String)DtlsMap.get("ACCOUNT_CODE");
			List agrList=ahFuctionalService.getAgreementsAndExtentSizes(ryotcode);
			Map mp3=null;
			String agreements="";
			Double extsize=0.0;
			if(agrList.size()>0 && agrList!=null)
			{
				for(int i=0;i<agrList.size();i++)
				{
					mp3=(HashMap)agrList.get(i);
					agreements=agreements+(String)mp3.get("agreementnumber")+",";
					extsize=extsize+(Double)mp3.get("extentsize");
				}
			}
			advDetails.setAgreements(agreements);
			advDetails.setExtentsize(extsize);
			if(advType.equalsIgnoreCase("Seedling Advance"))
			{
				advDetails.setIsitseedling((byte)0);
				advDetails.setIsSeedlingAdvance((byte)0);
				advDetails.setSeedingsquantity(quantity.intValue());
			}
			else
			{
				advDetails.setIsitseedling((byte)1);
				advDetails.setIsSeedlingAdvance((byte)1);
			}
			//added by sahadeva 27/10/2016
			advDetails.setLoginId(loginController.getLoggedInUserName());
			
			advDetails.setRyotcode(ryotcode);
			advDetails.setRyotpayable(advAmount);
			advDetails.setSeason("2016-2017");
			
			advDetails.setSeedsuppliercode(seedryotcode);
			advDetails.setTotalamount(advAmount);
			advDetails.setTransactioncode(transactionCode);
			return 	advDetails;
		}
		private boolean accountingDataPostingForCredit(Double finalAmtToPost,String accountCode,String JrnlMemo,String cdate,String rtname)
		{		
			boolean isInsertSuccess=false;
			boolean verifyAccount=false;
			String strQry=null;
			String Qryobj=null;
			String season=null;
		    Integer transactionCode = commonService.GetMaxTransCode(season);
		    List trmentityList=new ArrayList();
		    List UpdateList=new ArrayList();

		    try
		    {
			Session session=hibernatedao.getSessionFactory().openSession();
			  if(rtname==null ||rtname.equals("") || rtname.equalsIgnoreCase("null"))
			  {
				NewRyots newryot=new NewRyots();
				newryot.setRyotcode(accountCode);
				session.save(newryot);
			}
			  else
			  {
				verifyAccount=ahFuctionalService.validateAccountMaster(accountCode);
				if(!verifyAccount)
				{
					AccountMaster accountMaster = prepareModelForAccountMaster(rtname,accountCode);
					session.save(accountMaster);
				}
			    Map DtlsMap = new HashMap();
				DtlsMap.put("TRANSACTION_CODE", transactionCode);
				DtlsMap.put("TRANSACTION_AMOUNT",finalAmtToPost);
				//added by sahadeva 27/10/2016
				DtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
				
				DtlsMap.put("ACCOUNT_CODE", accountCode);
				DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
				DtlsMap.put("C_Date", cdate);
				DtlsMap.put("TRANSACTION_TYPE", "Cr");
				String TransactionType = "Cr";
				//Added by DMurty on 18-10-2016
				DtlsMap.put("SEASON", season);
						
				AccountDetails accountDetails =  prepareModelForAccountDetailsForTransport(DtlsMap);
				trmentityList.add(accountDetails);
				AccountSummary accountSummary = prepareModelforAccSmry(finalAmtToPost,accountCode, TransactionType,season);
				List BalList = ahFuctionalService.BalDetails(accountCode,season);
				if (BalList != null && BalList.size() > 0) 
				{
					double finalAmt = accountSummary.getRunningbalance();
					String strCrDr = accountSummary.getBalancetype();
					String AccCode = accountSummary.getAccountcode();
					
					strQry = "UPDATE AccountSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountcode ='"+AccCode+"' and season='"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				} 
				else 
				{
					trmentityList.add(accountSummary);
				}
				// Insert Rec in Acc Grp Details
				int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
				Map AccGrpDtlsMap = new HashMap();
					
				AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
				AccGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
				//added by sahadeva 28/10/2016
				AccGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
				
				AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
				AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
				AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
				AccGrpDtlsMap.put("C_Date", cdate);
				AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
				//Added by DMurty on 18-10-2016
				AccGrpDtlsMap.put("SEASON", season);
				
				AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetailsForTransport(AccGrpDtlsMap);
				trmentityList.add(accountGroupDetails);
				AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
				
				List AccountGroupSummaryList = ahFuctionalService.AccGrpBalDetails(accGrpCode,season);
				if (AccountGroupSummaryList != null && AccountGroupSummaryList.size() > 0)
				{
					double finalAmt = accountGroupSummary.getRunningbalance();
					String strCrDr = accountGroupSummary.getBalancetype();
					int AccGroupCode = accountGroupSummary.getAccountgroupcode();
					
					strQry = "UPDATE AccountGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and season='"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
				else 
				{
					trmentityList.add(accountGroupSummary);
				}
				
				Map AccSubGrpDtlsMap = new HashMap();
		
				AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
				AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", finalAmtToPost);
				//added by sahadeva 27/10/2016
				AccSubGrpDtlsMap.put("LOGIN_USER", loginController.getLoggedInUserName());
				
				AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
				AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
				AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
				AccSubGrpDtlsMap.put("C_Date", cdate);
				AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");	
				//Added by DMurty on 18-10-2016
				AccSubGrpDtlsMap.put("SEASON", season);
				
				AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtlsForTransport(AccSubGrpDtlsMap);
				trmentityList.add(accountSubGroupDetails);
				AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(accountCode, finalAmtToPost, accGrpCode, TransactionType,season);
				double finalAmt = accountSubGroupSummary.getRunningbalance();
				String strCrDr = accountSubGroupSummary.getBalancetype();
				int AccGroupCode = accountSubGroupSummary.getAccountgroupcode();
				int AccSubGrpCode = accountSubGroupSummary.getAccountsubgroupcode();
				List AccountSubGroupSummaryList = ahFuctionalService.SubGrpBalDetails(accGrpCode,AccSubGrpCode,season);
				if (AccountSubGroupSummaryList != null && AccountSubGroupSummaryList.size() > 0) 
				{
					strQry = "UPDATE AccountSubGroupSummary set runningbalance="+finalAmt+",balancetype='"+strCrDr+"' WHERE accountgroupcode ="+accGrpCode+" and accountsubgroupcode="+AccSubGrpCode+" and season='"+season+"'";
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				} 
				else 
				{  
					trmentityList.add(accountSubGroupSummary);
				}	
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreens(trmentityList,UpdateList);
				
			  }
			  	 session.flush();
				 session.close();
		    }
		    catch(Exception e)
		    {
		    	logger.info("========SeedAndSeedMigration==========" +e);	
		    }
			return isInsertSuccess;
		}
		//Added by DMurty on 02-11-2016
		@RequestMapping(value = "/validateHarvestDateForPermit", method = RequestMethod.POST)
		public @ResponseBody boolean validateHarvestDateForPermit(@RequestBody JSONObject jsonData)throws Exception 
		{
			int permitNo = (Integer) jsonData.get("permitNo");
			boolean hasHarvestingDate = ahFuctionalService.validateHarvestDateForPermit(permitNo);
			return hasHarvestingDate;
		}
		
		//Migration by naidu For Dateupdation in loandetails
		@RequestMapping(value = "/getMigrateLoandetialsForDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody boolean getMigrateLoandetialsForDate(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess = false;
			List entityList = new ArrayList();
			List LoanList = ahFuctionalService.getAgreementDataToMigrate("LoanDetails1");
			logger.info("========LoanList==========" + LoanList);
			LoanDetails loandetails=null;
			try	
			{
				if(LoanList != null && LoanList.size()>0)
				{
					for (int i = 0; i < LoanList.size(); i++)
					{
						logger.info("========i==========" + i);

						Map tempMap = new HashMap();
						tempMap = (Map) LoanList.get(i);
						logger.info("========tempMap==========" + tempMap);
						Integer brhcode= (Integer) tempMap.get("branchcode");//int
						Integer lnumber = (Integer) tempMap.get("loannumber");
						String season = (String) tempMap.get("Season");//char
						String adloan = (String) tempMap.get("addedloan");
						String agrnum = (String) tempMap.get("agreementnumber");
						Integer cmid = (Integer) tempMap.get("canemanagerid");//int
						Double damount = (Double) tempMap.get("disbursedamount");//float
						String ddate = (String) tempMap.get("disburseddate");//char
						Integer fasid = (Integer) tempMap.get("fieldassistantid");//int
						Integer foffid = (Integer) tempMap.get("fieldofficerid");
						Double intramount = (Double) tempMap.get("interestamount");//float
						Integer lstatus = (Integer) tempMap.get("loanstatus");//int
						String logid = (String) tempMap.get("loginid");//char
						Double paidamount = (Double) tempMap.get("paidamount");//f
						Double pamount = (Double) tempMap.get("pendingamount");//float
						String pextent = (String) tempMap.get("plantextent");//char                  look at here
						Double principle = (Double) tempMap.get("principle");//float
						String rextent = (String) tempMap.get("ratoonextent");//ch
						Date recomdate = (Date) tempMap.get("recommendeddate");//datetime
						String refnumber = (String) tempMap.get("referencenumber");//ch
						String ryotcode = (String) tempMap.get("ryotcode");//char
						String surnum = (String) tempMap.get("suveynumber");//ch
						Double ttamount = (Double) tempMap.get("totalamount");//float
						Integer tcode = (Integer) tempMap.get("transactioncode");//int
						loandetails=new LoanDetails();
						loandetails.setAddedloan(adloan);
						loandetails.setAgreementnumber(agrnum);
						loandetails.setCanemanagerid(cmid);
						CompositePrKeyForLoans compositePrKeyForLoans = new CompositePrKeyForLoans();
						compositePrKeyForLoans.setBranchcode(brhcode);
						compositePrKeyForLoans.setLoannumber(lnumber);
						//Modified by DMurty on 21-02-2017
						compositePrKeyForLoans.setSeason(season);
						loandetails.setCompositePrKeyForLoans(compositePrKeyForLoans);
						loandetails.setDisbursedamount(damount);
						loandetails.setDisburseddate(DateUtils.getSqlDateFromString(ddate,Constants.GenericDateFormat.DATE_FORMAT));
						loandetails.setFieldassistantid(fasid);
						loandetails.setFieldofficerid(foffid);
						loandetails.setInterestamount(intramount);
						loandetails.setInterstrate(0.0);
						loandetails.setLoanstatus(lstatus);
						loandetails.setLoginid(logid);
						loandetails.setPaidamount(paidamount);
						loandetails.setPendingamount(pamount);
						loandetails.setPlantextent(pextent);
						loandetails.setPrinciple(principle);
						loandetails.setRatoonextent(rextent);
						loandetails.setReferencenumber(refnumber);
						loandetails.setRecommendeddate(recomdate);
						loandetails.setRyotcode(ryotcode);
						//loandetails.setSeason(season);
						loandetails.setSuveynumber(surnum);
						loandetails.setTotalamount(ttamount);
						loandetails.setTransactioncode(tcode);
						entityList.add(loandetails);
					}
					isInsertSuccess = commonService.saveMultipleEntities(entityList);
				}
				else
				{
					isInsertSuccess = true;
				}
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
			return isInsertSuccess;
		}
		//Added by DMurty on 08-11-2016
		@RequestMapping(value = "/getMigrateLoanPrinciplesForDate", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody boolean getMigrateLoanPrinciplesForDate(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess = false;
			List entityList = new ArrayList();
			List LoanPrincipleList = ahFuctionalService.getAgreementDataToMigrate("LoanPrincipleAmounts1");
			logger.info("========getMigrateLoanPrinciplesForDate==========" + LoanPrincipleList);
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

			try	
			{
				if(LoanPrincipleList != null && LoanPrincipleList.size()>0)
				{
					for (int i = 0; i < LoanPrincipleList.size(); i++)
					{
						logger.info("========i==========" + i);

						Map tempMap = new HashMap();
						tempMap = (Map) LoanPrincipleList.get(i);
						
						Date dateofupdate = (Date) tempMap.get("dateofupdate");
						String strdateofupdate = df.format(dateofupdate);
						String loanaccountnumber = (String) tempMap.get("loanaccountnumber");
						Double principle = (Double) tempMap.get("principle");//float
						String ryotcode = (String) tempMap.get("ryotcode");
						String season= (String) tempMap.get("season");//int
						Integer branchcode= (Integer) tempMap.get("branchcode");//int
						Integer lnumber = (Integer) tempMap.get("loannumber");
						Date principleDt = null;
						List loanDetailsList = ahFuctionalService.getDisbursedDate(branchcode,lnumber);
						if(loanDetailsList != null && loanDetailsList.size()>0)
						{
							Map loanMap = new HashMap();
							loanMap = (Map) loanDetailsList.get(0);
							principleDt = (Date) loanMap.get("disburseddate");
						}
						
						LoanPrincipleAmounts loanPrincipleAmounts = new LoanPrincipleAmounts();
						
						loanPrincipleAmounts.setBranchcode(branchcode);
						loanPrincipleAmounts.setDateofupdate(DateUtils.getSqlDateFromString(strdateofupdate,Constants.GenericDateFormat.DATE_FORMAT));
						loanPrincipleAmounts.setLoanaccountnumber(loanaccountnumber);
						loanPrincipleAmounts.setLoannumber(lnumber);
						loanPrincipleAmounts.setPrinciple(principle);
						loanPrincipleAmounts.setPrincipledate(principleDt);
						loanPrincipleAmounts.setRyotcode(ryotcode);
						loanPrincipleAmounts.setSeason(season);
						entityList.add(loanPrincipleAmounts);
					}
					isInsertSuccess = commonService.saveMultipleEntities(entityList);
				}
			}
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
				return isInsertSuccess;
			}
			return isInsertSuccess;
		}
		//Added  by sahadeva   22/11/2016
		@RequestMapping(value = "/saveSpecialPermit", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody JSONArray saveSpecialPermit(@RequestBody JSONObject jsonData)throws JsonParseException, JsonMappingException, IOException
		{ 	
			JSONArray jArray = new JSONArray();
			JSONObject jsonObj =null;
			jsonObj = new JSONObject();
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			String Success = "";
			Object Qryobj = null;
			String strQry = null;
			String strPermitNos="0";
			try 
			{
				JSONObject formData = (JSONObject) jsonData.get("formData");
				logger.info("saveSpecialPermit()========formData=========="	+ formData);
				//JSONObject gridData = (JSONObject) jsonData.get("gridData");
				JSONArray gridData = (JSONArray) jsonData.get("gridData");
				logger.info("saveSpecialPermit()========gridData=========="	+ gridData);

				String season = formData.getString("season");
				String nprogram = formData.getString("programno");
				String harvestDate = formData.getString("harvestingDate");
				String permitDate = formData.getString("permitDate");
				String agreementNo = formData.getString("agreementAddNumber");
				String ryotCode = formData.getString("ryotCode");
				String ryotName = formData.getString("ryotName");
				String remarks = formData.getString("remarks");

				Integer a=1;
				DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
				Date hDate =(Date) df2.parse(harvestDate);
				Date newDate =addDays(hDate, a); 
				int program = Integer.parseInt(nprogram);
				Integer Rank  = commonService.GetMaxRank(season,program);
				double ccsRating = 0.0;
				int rank = Rank;
				String pNo=null;
				
				for (int i = 0; i < gridData.size(); i++)
				{
					SpecialPermitBean specialPermitBean = new ObjectMapper().readValue(gridData.get(i).toString(),SpecialPermitBean.class);
					  
					int plotNum = specialPermitBean.getPlotNumber();
					String strplotNum = Integer.toString(plotNum);
					String permitNos = specialPermitBean.getPermitNumber();
					List<AgreementDetails> agreementDetails1 = ahFuctionalService.getAgreementDetailsBySeasonByPlot(season, agreementNo,strplotNum);
					if(agreementDetails1.get(0).getRank() != null)
					{
						Rank = agreementDetails1.get(0).getRank();
					}
					
					double extentSize = specialPermitBean.getExtentSize();
					if (permitNos.contains(",")) 
					{
						String totalpermits[] = permitNos.split(",");
						//modified by umanath 29-11-2016
						if(i==0)
						{
						strPermitNos=permitNos;
						}
						else
						{
							strPermitNos=strPermitNos+","+permitNos;
						}

						for (int k = 0; k < totalpermits.length; k++)
						{
							PermitDetails permitDetails = new PermitDetails();
							
							String perNo = totalpermits[k];
							ByteArrayOutputStream outData = QRCode.from(perNo).stream();//to store QR Code added By RamaKrishna on 26/08/2016
							logger.info("saveSpecialPermit()========outData=========="+ outData);

							int nperNo = Integer.parseInt(perNo);

							permitDetails.setSeason(season);

							CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
							compositePrKeyForPermitDetails.setPermitnumber(nperNo);

							permitDetails.setRank(Rank);
							permitDetails.setRyotcode(ryotCode);
							permitDetails.setAgreementno(agreementNo);

							//List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,agreementNo);
							List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(agreementNo,specialPermitBean.getPlotNumber().toString(),season);

							String ptorrt = agreementDetails.get(0).getPlantorratoon();
							
							int nptorrt = Integer.parseInt(ptorrt);
							permitDetails.setPlantorratoon((byte) nptorrt);
							permitDetails.setExtentsize(extentSize);
							permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
							permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
							permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
							permitDetails.setPlotno(agreementDetails.get(0).getPlotnumber());
							permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());
							permitDetails.setOldpermitnumber(0);

							Date CropDate = agreementDetails.get(0).getCropdate();
							String strCropDate = null;
							
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							strCropDate = df.format(CropDate);
							permitDetails.setDeparturedt(new Date());
							permitDetails.setEntrancedt(new Date());
							permitDetails.setUnloadingdt(new Date());
							permitDetails.setValiditydate(newDate);
							
							permitDetails.setPermitdate(DateUtils.getSqlDateFromString(permitDate,Constants.GenericDateFormat.DATE_FORMAT));
							permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(harvestDate,Constants.GenericDateFormat.DATE_FORMAT));
							permitDetails.setShiftandadmin("NA");
							compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
							permitDetails.setStatus((byte) 0);

							permitDetails.setQrcodedata(outData.toByteArray());//to store QR Code added By RamaKrishna on 8/26/2016
							
							permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
							permitDetails.setReason(remarks);
							
							entityList.add(permitDetails);
						}
					} 
					else
					{
						PermitDetails permitDetails = new PermitDetails();
						//modified by umanath 29-11-2016 
						if(i==0)
						{
							strPermitNos=permitNos;
						}
						else
						{
							strPermitNos=strPermitNos+","+permitNos;
						}
						String perNo = permitNos;
						int nperNo = Integer.parseInt(perNo);
						
						permitDetails.setSeason(season);
						CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
						compositePrKeyForPermitDetails.setPermitnumber(nperNo);
						
						ByteArrayOutputStream outData = QRCode.from(perNo).stream();//to store QR Code added By RamaKrishna on 26/08/2016
						logger.info("saveRanking()========outData=========="+ outData);
						permitDetails.setQrcodedata(outData.toByteArray());//to store QR Code added By RamaKrishna on 8/26/2016

						permitDetails.setRank(Rank);
						permitDetails.setRyotcode(ryotCode);
						permitDetails.setAgreementno(agreementNo);

						List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsByNumberSeasonAndPlot(agreementNo,specialPermitBean.getPlotNumber().toString(),season);

						String ptorrt = agreementDetails.get(0).getPlantorratoon();
						int nptorrt = Integer.parseInt(ptorrt);
						permitDetails.setPlantorratoon((byte) nptorrt);
						permitDetails.setExtentsize(extentSize);
						permitDetails.setVarietycode(agreementDetails.get(0).getVarietycode());
						permitDetails.setZonecode(agreementDetails.get(0).getZonecode());
						permitDetails.setCirclecode(agreementDetails.get(0).getCirclecode());
						permitDetails.setLandvilcode(agreementDetails.get(0).getLandvillagecode());
						
						permitDetails.setPlotno(agreementDetails.get(0).getPlotnumber());
						Date CropDate = agreementDetails.get(0).getCropdate();
						String strCropDate = null;
						
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						strCropDate = df.format(CropDate);
						
						permitDetails.setPermitdate(DateUtils.getSqlDateFromString(permitDate,Constants.GenericDateFormat.DATE_FORMAT));
						permitDetails.setHarvestdt(DateUtils.getSqlDateFromString(harvestDate,Constants.GenericDateFormat.DATE_FORMAT));
						permitDetails.setDeparturedt(new Date());
						permitDetails.setEntrancedt(new Date());
						permitDetails.setUnloadingdt(new Date());
						
						permitDetails.setValiditydate(newDate);
						permitDetails.setShiftandadmin("NA");

						compositePrKeyForPermitDetails.setProgramno(Integer.parseInt(nprogram));
						permitDetails.setStatus((byte) 0);
						permitDetails.setCompositePrKeyForPermitDetails(compositePrKeyForPermitDetails);
						permitDetails.setReason(remarks);
						permitDetails.setOldpermitnumber(0);

						entityList.add(permitDetails);
					}
					
					List AggrementDetailsList = ahFuctionalService.getAgreementDetailsByaggrementno( season, agreementNo, plotNum, ryotCode);
					if(AggrementDetailsList != null && AggrementDetailsList.size()>0)
					{
						Map permitMap = new HashMap();
						permitMap = (Map) AggrementDetailsList.get(0);
						String agreementPermitNo = (String) permitMap.get("permitnumbers");
						if (("".equals(agreementPermitNo)) ||("null".equals(agreementPermitNo)) || agreementPermitNo == null)
						{
							permitNos=permitNos;
						}
						else
						{
							permitNos=agreementPermitNo+','+permitNos;
						}
					}
					else
					{
						permitNos=permitNos;
					}
					strQry = "UPDATE AgreementDetails set permitnumbers = '"+permitNos+"',programnumber="+program+",rank="+Rank+",samplecards='NA' WHERE seasonyear ='"+season+"' and plotnumber='"+plotNum+"' and agreementnumber='"+agreementNo+"' and ryotcode='"+ryotCode+"'";
					logger.info("saveRanking()========strQry=========="	+ strQry);
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
				logger.info("saveSpecialPermit()========entityList=========="	+ entityList);
				isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
				logger.info("saveSpecialPermit()========isInsertSuccess=========="+isInsertSuccess);
				
				List permitList = new ArrayList();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				String cdate=(dateFormat.format(date));
				Calendar cal = Calendar.getInstance();
				String ctime=(dateFormat.format(cal.getTime()));
				int circleCode = ahFuctionalService.getCircleCodeFromAgreementDetailsNew(agreementNo);
				List<Circle> circle=ahFuctionalService.getCircleByCirclecode(circleCode);
				String circlename=circle.get(0).getCircle();
				String PermitNos[] = strPermitNos.split(",");
				for (int k = 0; k < PermitNos.length; k++)
				{
					String permitnumber = PermitNos[k];
					Map TempMap = new HashMap();
													
					Map permitMap = new HashMap();
					permitMap.put("PERMIT_NO",permitnumber);
					permitMap.put("PROGRAM_NO", nprogram);
					permitMap.put("HARVEST_DATE", harvestDate);
					permitMap.put("RYOT_CODE", ryotCode);
					permitMap.put("RYOT_NAME", ryotName);
					permitMap.put("CIRCLE_CODE", circleCode);
					permitMap.put("CIRCLE_NAME", circlename);
					permitMap.put("AGREEMET_NO", agreementNo);
					permitList.add(permitMap);
				}
				if(isInsertSuccess == true)
				{
					logger.info("saveSpecialPermit()========permitList=========="+permitList);
					if(permitList != null && permitList.size()>0)
					{
						for(int i=0;i<permitList.size();i++)
						{
							Map permitMap1 = new HashMap();
							permitMap1 = (Map) permitList.get(i);
							
							String permitNo1 = (String) permitMap1.get("PERMIT_NO");
							String programNo1 = (String) permitMap1.get("PROGRAM_NO");
							String harvestDate1 = (String) permitMap1.get("HARVEST_DATE");
							String ryotCode1 = (String) permitMap1.get("RYOT_CODE");
							String ryotName1 = (String) permitMap1.get("RYOT_NAME");
							Integer circleCode1 = (Integer) permitMap1.get("CIRCLE_CODE");
							String agreementNumber = (String) permitMap1.get("AGREEMET_NO");
							String circleName = (String) permitMap1.get("CIRCLE_NAME");
							Integer permitNo=Integer.parseInt(permitNo1);
							Integer programNo=Integer.parseInt(programNo1);
							List<AgreementDetails> agreementDetails = ahFuctionalService.getAgreementDetailsBySeason(season,agreementNumber);
							String ptorrt = agreementDetails.get(0).getPlantorratoon();
							int nptorrt = Integer.parseInt(ptorrt);
							List<PermitDetails> permitDetails1 = ahFuctionalService.getValidityDate(permitNo);
							Date vdate=permitDetails1.get(0).getValiditydate();
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							String strvDate = df.format(vdate);
							//
							List<Ryot>ryot=ahFuctionalService.getRyotNameByRyotCodeNew(ryotCode);
							
							CompositePrKeyForPermitDetails compositePrKeyForPermitDetails = new CompositePrKeyForPermitDetails();
							compositePrKeyForPermitDetails.setPermitnumber(permitNo);
							compositePrKeyForPermitDetails.setProgramno(programNo);
							
							PermitDetails pd=new PermitDetails();
							PermitDetails permitDetails=(PermitDetails) hibernatedao.get(PermitDetails.class, compositePrKeyForPermitDetails);
							
							List<Zone> zone=ahFuctionalService.getZoneByZonecode(permitDetails.getZonecode());
							List<Village> village=ahFuctionalService.getVillageBylandVilCode(permitDetails.getLandvilcode());
							List<Variety> variety=ahFuctionalService.getVarietyByVarietyCode(permitDetails.getVarietycode());
							List<CropTypes> crop=ahFuctionalService.getPtOrRt(nptorrt);
							
							jsonObj.put("issuccess", "true");
							jsonObj.put("ryotcode",ryotCode);
							jsonObj.put("ryotname",ryot.get(0).getRyotname());
							jsonObj.put("fathername",ryot.get(0).getRelativename());
							jsonObj.put("circlecode",circleCode);
							jsonObj.put("landvilcode",permitDetails.getLandvilcode());
							jsonObj.put("permitnumber",permitDetails.getCompositePrKeyForPermitDetails().getPermitnumber());
							jsonObj.put("programno",permitDetails.getCompositePrKeyForPermitDetails().getProgramno());
							jsonObj.put("circle",circleName);
							jsonObj.put("zone",zone.get(0).getZone());
							jsonObj.put("variety",variety.get(0).getVariety());
							jsonObj.put("plantorratoon",crop.get(0).getCroptype());
							byte[] encoded = Base64.encodeBase64(permitDetails.getQrcodedata()); 
							jsonObj.put("qrcodedata",new String(encoded));
							jsonObj.put("currenttime",ctime);
							jsonObj.put("currentdate",cdate);
							jsonObj.put("village",village);
							jsonObj.put("rank",permitDetails.getRank());
							jsonObj.put("harvestDate",harvestDate);
							jsonObj.put("validityDate",strvDate);
							jArray.add(jsonObj);
						}
					}
				}
				else
				{
					jsonObj.put("issuccess", "false");
					jArray.add(jsonObj);
				}
			} 
			catch (Exception e)
			{
				logger.info(e.getCause(), e);
			}
			logger.info("saveSpecialPermit()========jArray=========="+jArray);
			return jArray;
		}
		
		
			/////////////////////Controllerr///////////////////////////////////////////////////////////////////////////

			@RequestMapping(value = "/GetReprintReceiptDetails", method = RequestMethod.POST)
			public @ResponseBody JSONArray GetReprintReceiptDetails(@RequestBody JSONObject jsonData) throws Exception 
			{
				JSONArray jArray = new JSONArray();
				JSONObject jsonObj =null;
				jsonObj = new JSONObject();
				org.json.JSONObject response = new org.json.JSONObject();
				String Dfromdate=(String)jsonData.get("fromDate");
				String Dtodate=(String)jsonData.get("toDate");
				Integer Shift=(Integer)jsonData.get("shift");
				Integer lorryStatus=(Integer)jsonData.get("lorryStatus");
				
				Date fromDate=DateUtils.getSqlDateFromString(Dfromdate,Constants.GenericDateFormat.DATE_FORMAT);
				String Frmdate= new SimpleDateFormat("yyyy-MM-dd").format(fromDate);
				Date toDate=DateUtils.getSqlDateFromString(Dtodate,Constants.GenericDateFormat.DATE_FORMAT);
				String toDdate= new SimpleDateFormat("yyyy-MM-dd").format(toDate);
				
				//Modified by DMurty on 29-11-2016
				List DateList = commonService.listGetReceiptDetails(Frmdate,toDdate,Shift,lorryStatus);
				logger.info("GetReprintReceiptDetails() ========DateList=========="+DateList);
				if(DateList != null && DateList.size()>0)
				{
					for (int j = 0; j < DateList.size(); j++)
					{
						
						Map dateMap = new HashMap();
						dateMap = (Map) DateList.get(j);
						double netwt = (Double) dateMap.get("netwt");
						Integer Slno = (Integer) dateMap.get("serialnumber");
						Integer permitno = (Integer) dateMap.get("permitnumber");
						String ryotcode = (String) dateMap.get("ryotcode");
						String ryotname = (String) dateMap.get("ryotname");
						jsonObj.put("netwt", netwt);
						jsonObj.put("serialnumber", Slno);
						jsonObj.put("permitnumber", permitno);
						jsonObj.put("ryotcode", ryotcode);
						jsonObj.put("ryotname", ryotname);
						jArray.add(jsonObj);
					}
				}
				logger.info("GetReprintReceiptDetails() ========jArray=========="+jArray);
				return jArray;
			}
			
			@RequestMapping(value = "/migrateAdvancesData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody boolean migrateAdvancesData(@RequestBody String jsonData) throws Exception 
			{
				boolean isInsertSuccess  = false;
				List entityList = new ArrayList();
				List UpdateList = new ArrayList();
				List AccountsList = new ArrayList();
				
				String strQry = null;
				Object Qryobj = null;
				
				Map AccountSummaryMap = new HashMap();
				Map AccountGroupMap = new HashMap();
				Map AccountSubGroupMap = new HashMap();
				
				List AccountCodeList = new ArrayList();
				List AccountGrpCodeList = new ArrayList();
				List AccountSubGrpCodeList = new ArrayList();
				
				HttpSession session=request.getSession(false);
				Employees employ = (Employees) session.getAttribute(Constants.USER);
				String user = employ.getLoginid();
				
				List AdvanceList = ahFuctionalService.getAgreementDataToMigrate("AdvanceDetails");
				logger.info("========migrateAdvancesData==========" + AdvanceList);
				try	
				{
					if(AdvanceList != null && AdvanceList.size()>0)
					{
						for (int i = 0; i < AdvanceList.size(); i++)
						{
							logger.info("========i==========" + i);

							Map tempMap = new HashMap();
							tempMap = (Map) AdvanceList.get(i);
							
							Date strtestDate = (Date) tempMap.get("advancedate");
							String ryotcode = (String) tempMap.get("ryotcode");
							String seedsuppliercode = (String) tempMap.get("seedsuppliercode");
							String season= (String) tempMap.get("season");//int
							Double advanceAmt = (Double) tempMap.get("advanceamount");//float
							double Amt = advanceAmt;
							int transaction = (Integer) tempMap.get("transactioncode");//int
							int advanceCode = (Integer) tempMap.get("advancecode");//int //GET GL CODE FROM cOMPANYaDVANCE
							double totalextentsize = (Double) tempMap.get("extentsize");
							
							List advPrincipleList = new ArrayList();
							advPrincipleList = ahFuctionalService.getAdvancePrinciples(ryotcode,advanceCode,season);
							if(advPrincipleList != null && advPrincipleList.size()>0)
							{
								AdvancePrincipleAmounts advanceprincipleamounts = new AdvancePrincipleAmounts();
								advanceprincipleamounts = (AdvancePrincipleAmounts) advPrincipleList.get(0);
								
								double principle = advanceprincipleamounts.getPrinciple();
								advanceAmt = advanceAmt+principle;
								
								advanceprincipleamounts.setPrinciple(advanceAmt);
								advanceprincipleamounts.setId(advanceprincipleamounts.getId());
								ahFuctionalService.addAdvancePrincipleAmounts(advanceprincipleamounts);
							}
							else
							{
								AdvancePrincipleAmounts advanceprincipleamounts = new AdvancePrincipleAmounts();
								
								advanceprincipleamounts.setAdvancecode(advanceCode);
								advanceprincipleamounts.setPrinciple(advanceAmt);
								advanceprincipleamounts.setPrincipledate(strtestDate);
								advanceprincipleamounts.setRyotcode(ryotcode);
								advanceprincipleamounts.setSeason(season);
								
								ahFuctionalService.addAdvancePrincipleAmounts(advanceprincipleamounts);
							}
							
							/*DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							String dateofupdate = df.format(strtestDate);
							
							List<CompanyAdvance> companyAdvance = ahFuctionalService.getAdvanceDateilsByAdvanceCode(advanceCode);
							String GlCode = companyAdvance.get(0).getGlCode();*/ // GlCode is nothing but AccountCode
							
							//Ryot Dr
							/*//Amt = advanceAmt;
							String loginuser = user;
							String AccountCode = ryotcode;
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
							
							if(AccountCode != null)
							{
								Map DtlsMap = new HashMap();
								
								DtlsMap.put("TRANSACTION_CODE", transaction);
								DtlsMap.put("TRANSACTION_AMOUNT", Amt);
								DtlsMap.put("LOGIN_USER", loginuser);
								DtlsMap.put("ACCOUNT_CODE", AccountCode);
								DtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								DtlsMap.put("TRANSACTION_TYPE", "Dr");
								DtlsMap.put("SEASON", season);
								DtlsMap.put("TRANSACTION_DATE", dateofupdate);
								DtlsMap.put("GL_CODE", GlCode);

								AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
								entityList.add(accountDetails);
				
								String TransactionType = "Dr";
								AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", AccountCode);
								boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
								if (advAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(AccountCode,accountSummary);
								}
								else
								{
									accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", AccountCode);
										tmpMap.put("KEY", AccountCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
									}
								}
								// Insert Rec in AccGrpDetails
								Map AccGrpDtlsMap = new HashMap();
				
								AccGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccGrpDtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccGrpDtlsMap.put("SEASON", season);
								AccGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
			 
								AccountGroupDetails AccountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
								entityList.add(AccountGroupDetails);
				
								TransactionType = "Dr";
								AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (advAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
								}
								else
								{
									accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								// Insert Rec in AccSubGrpDtls
								
								int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
								String strAccSubGrpCode = Integer.toString(AccSubGCode);

								Map AccSubGrpDtlsMap = new HashMap();
								AccSubGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccSubGrpDtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccSubGrpDtlsMap.put("SEASON", season);
								AccSubGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
								 
								AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
								entityList.add(accountSubGroupDetails);
								
								TransactionType = "Dr"; 
								AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
								
								boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(advAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
								}
								else
								{
									accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							
							//update for advance ---- Dr
							AccountCode = GlCode;
							AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
							strAccGrpCode = Integer.toString(AccGrpCode);
							if(AccountCode != null)
							{
								Map DtlsMap = new HashMap();
								
								DtlsMap.put("TRANSACTION_CODE", transaction);
								DtlsMap.put("TRANSACTION_AMOUNT", Amt);
								DtlsMap.put("LOGIN_USER", loginuser);
								DtlsMap.put("ACCOUNT_CODE", AccountCode);
								DtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								DtlsMap.put("TRANSACTION_TYPE", "Dr");
								DtlsMap.put("SEASON", season);
								DtlsMap.put("TRANSACTION_DATE", dateofupdate);
								DtlsMap.put("GL_CODE", ryotcode);

								AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
								entityList.add(accountDetails);
				
								String TransactionType = "Dr";
								AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", AccountCode);
								boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
								if (advAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(AccountCode,accountSummary);
								}
								else
								{
									accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", AccountCode);
										tmpMap.put("KEY", AccountCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
									}
								}
								// Insert Rec in AccGrpDetails
								Map AccGrpDtlsMap = new HashMap();
				
								AccGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccGrpDtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccGrpDtlsMap.put("SEASON", season);
								AccGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
			 
								AccountGroupDetails AccountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
								entityList.add(AccountGroupDetails);
				
								TransactionType = "Dr";
								AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (advAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
								}
								else
								{
									accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								// Insert Rec in AccSubGrpDtls
								
								int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
								String strAccSubGrpCode = Integer.toString(AccSubGCode);

								Map AccSubGrpDtlsMap = new HashMap();
								AccSubGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccSubGrpDtlsMap.put("JOURNAL_MEMO", companyAdvance.get(0).getAdvance());
								AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccSubGrpDtlsMap.put("SEASON", season);
								AccSubGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
								
								AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
								entityList.add(accountSubGroupDetails);
				
								TransactionType = "Dr";
								AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
								
								boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(advAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
								}
								else
								{
									accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}
							//update for seed supplier------Cr
							if(seedsuppliercode != null)
							{
								SeedSuppliersSummary seedSuppliersSummary = new SeedSuppliersSummary();
								
								seedSuppliersSummary.setAdvancepayable(Amt);
								seedSuppliersSummary.setAdvancestatus((byte) 0);
								seedSuppliersSummary.setLoginid(user);
								seedSuppliersSummary.setPaidamount(0.0);
								seedSuppliersSummary.setPendingamount(Amt);
								seedSuppliersSummary.setSeason(season);
								seedSuppliersSummary.setSeedsuppliercode(seedsuppliercode);
								seedSuppliersSummary.setTotalextentsize(totalextentsize);
								ahFuctionalService.addSeedSuppliersSummary(seedSuppliersSummary);
								
								
								AccountCode = seedsuppliercode;
								AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
								strAccGrpCode = Integer.toString(AccGrpCode);

								Map DtlsMap = new HashMap();
								
								DtlsMap.put("TRANSACTION_CODE", transaction);
								DtlsMap.put("TRANSACTION_AMOUNT", Amt);
								DtlsMap.put("LOGIN_USER", loginuser);
								DtlsMap.put("ACCOUNT_CODE", AccountCode);
								DtlsMap.put("JOURNAL_MEMO", "Charges towards Seed Supply");
								DtlsMap.put("TRANSACTION_TYPE", "Cr");
								DtlsMap.put("SEASON", season);
								DtlsMap.put("TRANSACTION_DATE", dateofupdate);
								DtlsMap.put("GL_CODE", ryotcode);

								AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
								entityList.add(accountDetails);
				
								String TransactionType = "Cr";
								AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", AccountCode);
								boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
								if (advAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(AccountCode,accountSummary);
								}
								else
								{
									accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", AccountCode);
										tmpMap.put("KEY", AccountCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
									}
								}
								// Insert Rec in AccGrpDetails
								Map AccGrpDtlsMap = new HashMap();
				
								AccGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccGrpDtlsMap.put("JOURNAL_MEMO", "Charges towards Seed Supply");
								AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
								AccGrpDtlsMap.put("SEASON", season);
								AccGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
			 
								AccountGroupDetails AccountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
								entityList.add(AccountGroupDetails);
				
								TransactionType = "Cr";
								AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (advAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
								}
								else
								{
									accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								// Insert Rec in AccSubGrpDtls
								int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
								//Hence ryot is seed supplier we need to change this subgroup code to 
								AccSubGCode = 2;
								String strAccSubGrpCode = Integer.toString(AccSubGCode);

								Map AccSubGrpDtlsMap = new HashMap();
								AccSubGrpDtlsMap.put("TRANSACTION_CODE", transaction);
								AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Charges towards Seed Supply");
								AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
								AccSubGrpDtlsMap.put("SEASON", season);
								AccSubGrpDtlsMap.put("TRANSACTION_DATE", dateofupdate);
								AccSubGrpDtlsMap.put("IS_SEED_SUPPLIER", "Yes");
								
								AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
								entityList.add(accountSubGroupDetails);
				
								TransactionType = "Cr"; 
								AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmryForSeedSupplier(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
								
								boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(advAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
								}
								else
								{
									accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}*/
						}
						isInsertSuccess = true;
						/*Iterator entries = AccountSummaryMap.entrySet().iterator();
						while (entries.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries.next();
						    Object value = entry.getValue();
						    AccountSummary accountSummary = (AccountSummary) value;
						    
						    String accountCode = accountSummary.getAccountcode();
						    String sesn = accountSummary.getSeason();
						    double runnbal = accountSummary.getRunningbalance();
						    String crOrDr = accountSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountSummary);
						    }
						}
						
						Iterator entries1 = AccountGroupMap.entrySet().iterator();
						while (entries1.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries1.next();
						    Object value = entry.getValue();
						    
						    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
						    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
						    String sesn = accountGroupSummary.getSeason();
						    double runnbal = accountGroupSummary.getRunningbalance();
						    String crOrDr = accountGroupSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountGroupSummary);
						    }
						}
						
						Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
						while (entries2.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries2.next();
						    Object value = entry.getValue();
						    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
						   
						    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
						    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
						    String sesn = accountSubGroupSummary.getSeason();
						    double runnbal = accountSubGroupSummary.getRunningbalance();
						    String crOrDr = accountSubGroupSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountSubGroupSummary);
						    }
						}*/
						//isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
						logger.info("========migrateAdvancesData========== Advance Migration Completed " + isInsertSuccess);
					}
				}
				catch (Exception e)
				{
					logger.info(e.getCause(), e);
					return isInsertSuccess;
				}
				return isInsertSuccess;
			}
			
			private AccountSubGroupSummary prepareModelforAccSubGrpSmryForSeedSupplier(String AccountCode, Double Amt, int AccGrpCode,	String TransactionType,String season)
			{
				Double RunnBal = 0.0;
				String TransType = TransactionType;
				AccountSubGroupSummary accountSubGroupSummary = new AccountSubGroupSummary();
				String AccCode = AccountCode;
				double TotalAmt = Amt;
				//Here AccSubGroupCode for SeedSuppliers is 2
				int AccSubGroupCode = 2;
				List BalList = commonService.SubGrpBalanceDetails(AccGrpCode,AccSubGroupCode, TotalAmt, TransactionType,season);
				if (BalList != null && BalList.size() > 0) 
				{
					Map TempMap = new HashMap();
					TempMap = (Map) BalList.get(0);
					RunnBal = (Double) TempMap.get("RUNNING_BAL");
					TransType = (String) TempMap.get("TRANS_TYPE");
				}

				accountSubGroupSummary.setAccountgroupcode(AccGrpCode);
				accountSubGroupSummary.setAccountsubgroupcode(AccSubGroupCode);
				accountSubGroupSummary.setRunningbalance(RunnBal);
				accountSubGroupSummary.setBalancetype(TransType);
				accountSubGroupSummary.setSeason(season);
				
				return accountSubGroupSummary;
			}
			
			@RequestMapping(value = "/migrateLoanData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody boolean migrateLoanData(@RequestBody String jsonData) throws Exception 
			{
				boolean isInsertSuccess  = false;
				List entityList = new ArrayList();
				List UpdateList = new ArrayList();
				List AccountsList = new ArrayList();
				
				String strQry = null;
				Object Qryobj = null;
				
				Map AccountSummaryMap = new HashMap();
				Map AccountGroupMap = new HashMap();
				Map AccountSubGroupMap = new HashMap();
				
				List AccountCodeList = new ArrayList();
				List AccountGrpCodeList = new ArrayList();
				List AccountSubGrpCodeList = new ArrayList();
				
				HttpSession session=request.getSession(false);
				Employees employ = (Employees) session.getAttribute(Constants.USER);
				String user = employ.getLoginid();
				
				List LoanList = ahFuctionalService.getAgreementDataToMigrate("LoanDetails");
				logger.info("========migrateLoanData==========" + LoanList);
				try	
				{
					if(LoanList != null && LoanList.size()>0)
					{
						for(int i=0;i<LoanList.size();i++)
						{
							Map tempMap = new HashMap();
							tempMap = (Map) LoanList.get(i);
							logger.info("========migrateLoanData========== i val" + i);
							logger.info("========migrateLoanData========== tempMap" + tempMap);

							int branchcode = (Integer) tempMap.get("branchcode");
							String branchId = Integer.toString(branchcode);
							int loannumber = (Integer) tempMap.get("loannumber");
							String season = (String) tempMap.get("Season");
							Date disburseddate = (Date) tempMap.get("disburseddate");
							if(disburseddate==null ||disburseddate.equals(""))
							{
								continue;
							}
							double disbursedamount = (Double) tempMap.get("disbursedamount");
							String referencenumber = (String) tempMap.get("referencenumber");
							String ryotcode = (String) tempMap.get("ryotcode");
							int transactionCode = (Integer) tempMap.get("transactioncode");
							
							List loanPrincipleList = new ArrayList();
							loanPrincipleList = ahFuctionalService.getLoanPrinciples(ryotcode,loannumber,season);
							logger.info("========migrateLoanData========== loanPrincipleList" + loanPrincipleList);
							if(loanPrincipleList == null || loanPrincipleList.size()==0)
							{
								if(disburseddate != null )
								{
									LoanPrincipleAmounts loanPrincipleAmounts = new LoanPrincipleAmounts();
									loanPrincipleAmounts.setBranchcode(branchcode);
									loanPrincipleAmounts.setLoannumber(loannumber);
									loanPrincipleAmounts.setPrinciple(disbursedamount);
									loanPrincipleAmounts.setPrincipledate(disburseddate);
									loanPrincipleAmounts.setRyotcode(ryotcode);
									loanPrincipleAmounts.setSeason(season);
									loanPrincipleAmounts.setLoanaccountnumber(referencenumber);
									loanPrincipleAmounts.setDateofupdate(disburseddate);
									ahFuctionalService.addloanPrincipleAmounts(loanPrincipleAmounts);
								}
							}
							
							/*String glCode = "";
							List<Branch> branch = ahFuctionalService.getBranchDtls(branchcode);
							glCode = branch.get(0).getGlcode().toString();
							String BankglCode = branch.get(0).getGlcode().toString();
							
							double Amt = disbursedamount;
							String loginuser = loginController.getLoggedInUserName();
							String AccountCode = glCode;
							int AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
							String strAccGrpCode = Integer.toString(AccGrpCode);
							if (branchId != null)
							{
								if(branch != null && branch.size()>0)
								{
									Map DtlsMap = new HashMap();

									DtlsMap.put("TRANSACTION_CODE", transactionCode);
									DtlsMap.put("TRANSACTION_AMOUNT", Amt);
									DtlsMap.put("LOGIN_USER", loginuser);
									DtlsMap.put("ACCOUNT_CODE", AccountCode);
									DtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
									DtlsMap.put("TRANSACTION_TYPE", "Cr");
									DtlsMap.put("GL_CODE", ryotcode);
									DtlsMap.put("SEASON", season);
									
									AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
									entityList.add(accountDetails);
									
									// Insert Rec in AccSmry AccSmryList
									String TransactionType = "Cr";
									String strtype = "";
									AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
									
									Map AccSmry = new HashMap();
									AccSmry.put("ACC_CODE", AccountCode);
									boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
									if (advAccCodetrue == false)
									{
										AccountCodeList.add(AccSmry);
										AccountSummaryMap.put(AccountCode,accountSummary);
									}
									else
									{
										accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
										String straccCode = accountSummary.getAccountcode();
										if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
										{
											double runbal = accountSummary.getRunningbalance();
											String baltype = accountSummary.getBalancetype();
											String ses = accountSummary.getSeason();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_CODE", AccountCode);
											tmpMap.put("KEY", AccountCode);
											Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
											AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
										}
									}
									
									// Insert Rec in AccGrpDetails
									Map AccGrpDtlsMap = new HashMap();

									AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
									AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
									AccGrpDtlsMap.put("LOGIN_USER", loginuser);
									AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
									AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
									AccGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
									AccGrpDtlsMap.put("SEASON", season);
									
									AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
									entityList.add(accountGroupDetails);

									TransactionType = "Cr";
									AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
									Map AccGrpSmry = new HashMap();
									AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
									boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
									if (advAccGrpCodetrue == false)
									{
										AccountGrpCodeList.add(AccGrpSmry);
										AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
									}
									else
									{
										accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
										int accountGrpCode = accountGroupSummary.getAccountgroupcode();
										if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
										{
											double runbal = accountGroupSummary.getRunningbalance();
											String baltype = accountGroupSummary.getBalancetype();
											String ses = accountGroupSummary.getSeason();
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("KEY", strAccGrpCode);
											Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
											AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
										}
									}
									
									// Insert Rec in AccSubGrpDtls
									Map AccSubGrpDtlsMap = new HashMap();
									
									int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
									String strAccSubGrpCode = Integer.toString(AccSubGCode);

									AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
									AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
									AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
									AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
									AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
									AccSubGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan To Ryot");
									AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Cr");
									//Added by DMurty on 18-10-2016
									AccSubGrpDtlsMap.put("SEASON", season);
									
									AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
									entityList.add(accountSubGroupDetails);

									TransactionType = "Cr";
									AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
									Map AccSubGrpSmry = new HashMap();
									AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
									
									boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
									if(advAccSubGrpCodetrue == false)
									{
										AccountSubGrpCodeList.add(AccSubGrpSmry);
										AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
									}
									else
									{
										accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
										int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
										int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
										String ses = accountSubGroupSummary.getSeason();
										if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
										{
											double runbal = accountSubGroupSummary.getRunningbalance();
											String baltype = accountSubGroupSummary.getBalancetype();
											
											Map tmpMap = new HashMap();
											tmpMap.put("RUN_BAL", runbal);
											tmpMap.put("AMT", Amt);
											tmpMap.put("BAL_TYPE", baltype);
											tmpMap.put("SEASON", ses);
											tmpMap.put("TRANSACTION_TYPE", TransactionType);
											tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
											tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
											tmpMap.put("KEY", strAccSubGrpCode);
											Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
											AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
										}
									}
								}
							}
							AccountCode = "RTL";			
							if (AccountCode != null)
							{
								AccGrpCode = ahFuctionalService.GetAccGrpCode(AccountCode);
								strAccGrpCode = Integer.toString(AccGrpCode);

								Map DtlsMap = new HashMap();
								DtlsMap.put("TRANSACTION_CODE", transactionCode);
								DtlsMap.put("TRANSACTION_AMOUNT", Amt);
								DtlsMap.put("LOGIN_USER", loginuser);
								DtlsMap.put("ACCOUNT_CODE", AccountCode);
								DtlsMap.put("JOURNAL_MEMO", "Towards Loan");
								DtlsMap.put("TRANSACTION_TYPE", "Dr");
								DtlsMap.put("GL_CODE", BankglCode);
								DtlsMap.put("SEASON", season);

								AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
								entityList.add(accountDetails);
								
								String TransactionType = "Dr";
								String strtype = "";
								AccountSummary accountSummary = prepareModelforAccSmry(Amt,	AccountCode, TransactionType,season);
								
								Map AccSmry = new HashMap();
								AccSmry.put("ACC_CODE", AccountCode);
								boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
								if (advAccCodetrue == false)
								{
									AccountCodeList.add(AccSmry);
									AccountSummaryMap.put(AccountCode,accountSummary);
								}
								else
								{
									accountSummary = (AccountSummary) AccountSummaryMap.get(AccountCode);
									String straccCode = accountSummary.getAccountcode();
									if(straccCode.equalsIgnoreCase(AccountCode) && season.equals(accountSummary.getSeason()))
									{
										double runbal = accountSummary.getRunningbalance();
										String baltype = accountSummary.getBalancetype();
										String ses = accountSummary.getSeason();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_CODE", AccountCode);
										tmpMap.put("KEY", AccountCode);
										Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
										AccountSummaryMap.put(AccountCode,finalAccSmryMap.get(AccountCode));
									}
								}
								
								// Insert Rec in AccGrpDetails
								Map AccGrpDtlsMap = new HashMap();

								AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
								AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccGrpDtlsMap.put("JOURNAL_MEMO", "Towards Loan");
								AccGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccGrpDtlsMap.put("SEASON", season);

								AccountGroupDetails accountGroupDetails = prepareModelForAccGrpDetails(AccGrpDtlsMap);
								entityList.add(accountGroupDetails);

								TransactionType = "Dr";
								AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccGrpSmry = new HashMap();
								AccGrpSmry.put("ACC_GRP_CODE", AccGrpCode);
								boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
								if (advAccGrpCodetrue == false)
								{
									AccountGrpCodeList.add(AccGrpSmry);
									AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
								}
								else
								{
									accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
									int accountGrpCode = accountGroupSummary.getAccountgroupcode();
									if(AccGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
									{
										double runbal = accountGroupSummary.getRunningbalance();
										String baltype = accountGroupSummary.getBalancetype();
										String ses = accountGroupSummary.getSeason();
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("KEY", strAccGrpCode);
										Map finalAccGrpSmryMap = returnAccountGroupSummaryModel(tmpMap);
										AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
									}
								}
								
								
								// Insert Rec in AccSubGrpDtls
								Map AccSubGrpDtlsMap = new HashMap();

								int AccSubGCode = ahFuctionalService.GetAccSubGrpCode(AccountCode);
								String strAccSubGrpCode = Integer.toString(AccSubGCode);

								AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
								AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
								AccSubGrpDtlsMap.put("LOGIN_USER", loginuser);
								AccSubGrpDtlsMap.put("ACCOUNT_CODE", AccountCode);
								AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
								AccSubGrpDtlsMap.put("JOURNAL_MEMO","Towards Loan");
								AccSubGrpDtlsMap.put("TRANSACTION_TYPE", "Dr");
								AccSubGrpDtlsMap.put("SEASON", season);

								AccountSubGroupDetails accountSubGroupDetails = prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
								entityList.add(accountSubGroupDetails);

								TransactionType = "Dr";
								AccountSubGroupSummary accountSubGroupSummary = prepareModelforAccSubGrpSmry(AccountCode, Amt, AccGrpCode, TransactionType,season);
								Map AccSubGrpSmry = new HashMap();
								AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGCode);
								
								boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
								if(advAccSubGrpCodetrue == false)
								{
									AccountSubGrpCodeList.add(AccSubGrpSmry);
									AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
								}
								else
								{
									accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
									int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
									int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
									String ses = accountSubGroupSummary.getSeason();
									if(AccGrpCode == accgrpcode && AccSubGCode==accsubgrpcode && season.equalsIgnoreCase(ses))
									{
										double runbal = accountSubGroupSummary.getRunningbalance();
										String baltype = accountSubGroupSummary.getBalancetype();
										
										Map tmpMap = new HashMap();
										tmpMap.put("RUN_BAL", runbal);
										tmpMap.put("AMT", Amt);
										tmpMap.put("BAL_TYPE", baltype);
										tmpMap.put("SEASON", ses);
										tmpMap.put("TRANSACTION_TYPE", TransactionType);
										tmpMap.put("ACCOUNT_GROUP_CODE", AccGrpCode);
										tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGCode);
										tmpMap.put("KEY", strAccSubGrpCode);
										Map finalAccSubGrpSmryMap = returnAccountSubGroupSummaryModel(tmpMap);
										AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
									}
								}
							}*/
						}
						isInsertSuccess = true;
						/*Iterator entries = AccountSummaryMap.entrySet().iterator();
						while (entries.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries.next();
						    Object value = entry.getValue();
						    AccountSummary accountSummary = (AccountSummary) value;
						    
						    String accountCode = accountSummary.getAccountcode();
						    String sesn = accountSummary.getSeason();
						    double runnbal = accountSummary.getRunningbalance();
						    String crOrDr = accountSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode+"' and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountSummary);
						    }
						}
						
						Iterator entries1 = AccountGroupMap.entrySet().iterator();
						while (entries1.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries1.next();
						    Object value = entry.getValue();
						    
						    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
						    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
						    String sesn = accountGroupSummary.getSeason();
						    double runnbal = accountGroupSummary.getRunningbalance();
						    String crOrDr = accountGroupSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountGroupSummary);
						    }
						}
						
						Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
						while (entries2.hasNext())
						{
						    Map.Entry entry = (Map.Entry) entries2.next();
						    Object value = entry.getValue();
						    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
						   
						    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
						    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
						    String sesn = accountSubGroupSummary.getSeason();
						    double runnbal = accountSubGroupSummary.getRunningbalance();
						    String crOrDr = accountSubGroupSummary.getBalancetype();
						    
						    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
						    if(AccList != null && AccList.size()>0)
						    {
						    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
						    	Qryobj = qry;
						    	UpdateList.add(Qryobj);
						    }
						    else
						    {
						    	AccountsList.add(accountSubGroupSummary);
						    }
						}*/
						//isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
						logger.info("========migrateLoanData========== Loans Migration Completed " + isInsertSuccess);
					}
				}
				catch (Exception e)
				{
					logger.info(e.getCause(), e);
					return isInsertSuccess;
				}
				return isInsertSuccess;
			}
			
			//Added by DMurty on 06-12-2016
			@RequestMapping(value = "/getPermitDetailsByRyot", method = RequestMethod.POST)
			public @ResponseBody JSONArray getPermitDetailsByRyot(@RequestBody JSONObject jsonData) throws Exception 
			{
				logger.info("getPermitDetailsByRyot() ========jsonData=========="+jsonData);
				JSONArray jArray = new JSONArray();
				JSONObject jsonObj =null;
				jsonObj = new JSONObject();
				org.json.JSONObject response = new org.json.JSONObject();
				String ryotCode=(String)jsonData.get("ryotcode");
				String season=(String)jsonData.get("season");
				
				List<PermitDetails> permiDetails = ahFuctionalService.getPermits(season,ryotCode);
				if(permiDetails != null)
				{
					for (PermitDetails permitDetail : permiDetails)
					{
						Date harvestDt = permitDetail.getHarvestdt();
						String strharvestDt = null;
						if(harvestDt != null)
						{
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							strharvestDt = df.format(harvestDt);
						}
						jsonObj.put("harvestDt", strharvestDt);
						
						String permitStatus="Open";

						int permitno = permitDetail.getCompositePrKeyForPermitDetails().getPermitnumber();
						List weighmentList = ahFuctionalService.CheckWeighmentList(permitno);
						if(weighmentList != null && weighmentList.size()>0)
						{
							permitStatus = "Closed";
						}
						
						byte status = permitDetail.getStatus();
						if(status == 1)
						{
							permitStatus = "Closed";
						}
						else if(status == 2)
						{
							permitStatus = "Cancelled";
						}
						int oldpermitnumber = 0;
						if(permitDetail.getOldpermitnumber() != null)
						{
							oldpermitnumber = permitDetail.getOldpermitnumber();
						}
						
						jsonObj.put("permitnumber", permitno);
						jsonObj.put("status", permitStatus);
						
						jsonObj.put("oldpermitnumber", oldpermitnumber);
						jsonObj.put("rank", permitDetail.getRank());
						jsonObj.put("programNo", permitDetail.getCompositePrKeyForPermitDetails().getProgramno());

						jArray.add(jsonObj);
					}
				}
				logger.info("getPermitDetailsByRyot() ========jArray=========="+jArray);
				return jArray;
			}
			@RequestMapping(value = "/migrateAdvancesSummaryData", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody boolean migrateAdvancesSummaryData(@RequestBody String jsonData) throws Exception 
			{
				boolean isInsertSuccess  = false;
				List entityList = new ArrayList();
				List UpdateList = new ArrayList();
				List AccountsList = new ArrayList();
				
				String strQry = null;
				Object Qryobj = null;
				
				List AdvanceList = ahFuctionalService.getAgreementDataToMigrate("AdvanceSummary1");
				logger.info("========migrateAdvancesSummaryData==========" + AdvanceList);
				try	
				{
					if(AdvanceList != null && AdvanceList.size()>0)
					{
						for (int i = 0; i < AdvanceList.size(); i++)
						{
							logger.info("========i==========" + i);

							Map tempMap = new HashMap();
							tempMap = (Map) AdvanceList.get(i);
						
							Double advanceAmt = (Double) tempMap.get("advanceamount");
							int advanceCode = (Integer) tempMap.get("advancecode");
							byte advanceStatus = (Byte) tempMap.get("advancestatus");
							double interestamount = (Double) tempMap.get("interestamount");

							String loginid= (String) tempMap.get("loginid");
							double paidamount = (Double) tempMap.get("paidamount");
							double pendingamount = (Double) tempMap.get("pendingamount");
							double pendingpayable = (Double) tempMap.get("pendingpayable");
							double recoveredamount = (Double) tempMap.get("recoveredamount");
							String ryotcode = (String) tempMap.get("ryotcode");
							int ryotcodeSeq = (Integer) tempMap.get("ryotcodeSeq");
							String season= (String) tempMap.get("season");
							double totalamount = (Double) tempMap.get("totalamount");
							int transactioncode = (Integer) tempMap.get("transactioncode");
							String villagecode = (String) tempMap.get("villagecode");
							
							List advSmryList = new ArrayList();
							advSmryList = ahFuctionalService.getAdvanceSummary(ryotcode,advanceCode,season);
							if(advSmryList != null && advSmryList.size()>0)
							{
								AdvanceSummary advanceSummary = (AdvanceSummary) advSmryList.get(0);
								
								double finalAdvance = advanceSummary.getAdvanceamount();
								finalAdvance = finalAdvance+advanceAmt;
								advanceSummary.setAdvanceamount(finalAdvance);
								
								double finalinterestamount = advanceSummary.getInterestamount();
								finalinterestamount = finalinterestamount+interestamount;
								advanceSummary.setInterestamount(finalinterestamount);

								double finalPaid = advanceSummary.getPaidamount();
								finalPaid = finalPaid+paidamount;
								advanceSummary.setPaidamount(finalPaid);
								
								double finalPending = advanceSummary.getPendingamount();
								finalPending = finalPending+pendingamount;
								advanceSummary.setPendingamount(finalPending);

								double finalPendingPayable = advanceSummary.getPendingpayable();
								finalPendingPayable = finalPendingPayable+pendingpayable;
								advanceSummary.setPendingpayable(finalPendingPayable);

								double finalRecoveredAmt = advanceSummary.getRecoveredamount();
								finalRecoveredAmt = finalRecoveredAmt+recoveredamount;
								advanceSummary.setRecoveredamount(finalRecoveredAmt);

								
								double finalTotalAmt = advanceSummary.getTotalamount();
								finalTotalAmt = finalTotalAmt+totalamount;
								advanceSummary.setTotalamount(finalTotalAmt);
								
								ahFuctionalService.addAdvanceSummary(advanceSummary);
							}
							else
							{
								AdvanceSummary advanceSummary = new AdvanceSummary();
								
								advanceSummary.setAdvanceamount(advanceAmt);
								advanceSummary.setAdvancecode(advanceCode);
								advanceSummary.setAdvancestatus(advanceStatus);
								advanceSummary.setInterestamount(interestamount);
								advanceSummary.setLoginid(loginid);
								advanceSummary.setPaidamount(paidamount);
								advanceSummary.setPendingamount(pendingamount);
								advanceSummary.setPendingpayable(pendingpayable);
								advanceSummary.setRecoveredamount(recoveredamount);
								advanceSummary.setRyotcode(ryotcode);
								advanceSummary.setRyotcodeSeq(ryotcodeSeq);
								advanceSummary.setSeason(season);
								advanceSummary.setTotalamount(totalamount);
								advanceSummary.setTransactioncode(transactioncode);
								advanceSummary.setVillagecode(villagecode);
								
								ahFuctionalService.addAdvanceSummary(advanceSummary);
							}
						}
						isInsertSuccess = true;
						logger.info("========migrateAdvancesSummaryData========== Advance Summary Migration Completed " + isInsertSuccess);
					}
				}
				catch (Exception e)
				{
					logger.info(e.getCause(), e);
					return isInsertSuccess;
				}
				return isInsertSuccess;
			}
			//Added by DMurty on 02-01-2017
			@RequestMapping(value = "/migrateMobileNumbers", method = RequestMethod.POST)
			public @ResponseBody
			boolean migrateMobileNumbers(@RequestBody String jsonData) throws Exception 
			{
				boolean isInsertSuccess  = false;
				List entityList = new ArrayList();
				List UpdateList = new ArrayList();
				List AccountsList = new ArrayList();
				
				String strQry = null;
				Object Qryobj = null;
				
				List DataList = new ArrayList();
				HashMap hm= null;
				List mNoList = ahFuctionalService.getAgreementDataToMigrate("RyotMobileNumbers");
				if(mNoList != null && mNoList.size()>0)
				{
					for (int j = 0; j < mNoList.size(); j++)
					{
						hm= new HashMap();
						Map VMap=(Map)mNoList.get(j);
						String ryotCode = (String) VMap.get("ryotcode");
						String mobileno = (String) VMap.get("mobileno");

						strQry = "Update Ryot set mobilenumber='"+mobileno+"' where ryotcode='"+ryotCode+"'";
						Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	

				}
				return isInsertSuccess;
			}
			
			@RequestMapping(value = "/migrateRyotCategory", method = RequestMethod.POST)
			public @ResponseBody
			boolean migrateRyotCategory(@RequestBody String jsonData) throws Exception 
			{
				boolean isInsertSuccess  = false;
				List entityList = new ArrayList();
				List UpdateList = new ArrayList();
				List AccountsList = new ArrayList();
				
				String strQry = null;
				Object Qryobj = null;
				
				HashMap hm= null;
				List categoryList = ahFuctionalService.getAgreementDataToMigrate("RyotCategory");
				if(categoryList != null && categoryList.size()>0)
				{
					for (int j = 0; j < categoryList.size(); j++)
					{
						hm= new HashMap();
						Map VMap=(Map)categoryList.get(j);
						String ryotCode = (String) VMap.get("ryotcode");
						String harvestingstatus = (String) VMap.get("harvestingstatus");
						String iscommon = (String) VMap.get("iscommon");
						byte status = 0;
						if("COMPLETED".equalsIgnoreCase(harvestingstatus))
						{
							status = 1;
						}
						byte category = 0;
						if("Y".equalsIgnoreCase(iscommon))
						{
							category = 0;
						}
						else
						{
							category = 1;
						}

						strQry = "Update Ryot set isweighmentcompleted="+status+",ryotcategory="+category+" where ryotcode='"+ryotCode+"'";
						Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
				}
				return isInsertSuccess;
			}
			
			@RequestMapping(value = "/CanePurchargeTaxAccountMigration", method = RequestMethod.POST, headers = { "Content-type=application/json" })
			public @ResponseBody boolean CanePurchargeTaxAccountMigration() throws Exception 
			{
				List UpdateList = new ArrayList();
				List trmentityList = new ArrayList();
				Map AccountSummaryMap = new HashMap();
				Map AccountGroupMap = new HashMap();
				Map AccountSubGroupMap = new HashMap();
				List AccountCodeList = new ArrayList();
				List AccountGrpCodeList = new ArrayList();
				List AccountSubGrpCodeList = new ArrayList();
				List AccountsList = new ArrayList();
				JSONObject jsonObj =null;
				jsonObj = new JSONObject();
				boolean  isInsertTrue=false;
				Object Qryobj = null;
				Map mp=null;
				Double Amt=0.0;
				Double finalAmtToPost=0.0;
				Double quantity=0.0;
				Double purchaseTax=0.0;
				String season="2016-2017";
				try
				{
					List <CaneAcctRules> caneAcctRules=caneReceiptFunctionalService.getTransprtAlwnceFromCaneAcctRules(season,"2016-12-17");
					if(caneAcctRules != null && caneAcctRules.size()>0)
					{
						int calcid = caneAcctRules.get(0).getCalcid();
						List <CaneAcctAmounts> purcaneAcctAmounts=caneReceiptFunctionalService.getCaneAcctAmounts(season,calcid,"Purchase Tax / ICP");
						purchaseTax = purcaneAcctAmounts.get(0).getAmount();
						List weighmentDetailsList=caneReceiptFunctionalService.getWeighmentDetailsForAccMigration(season);
					
					int transactionCode=0;
					if(weighmentDetailsList.size()>0 && weighmentDetailsList!=null)
					{
						WeighmentDetails ws=null;
						for(int i=0;i<weighmentDetailsList.size();i++)
						{
							 ws=(WeighmentDetails)weighmentDetailsList.get(i);
							  logger.info("========tempMap========== i val" + i);
							  transactionCode=ws.getTransactioncode();
							  
							  double wt = ws.getNetwt();
							  String strpurchaseTax = HMSDecFormatter.format(purchaseTax);
							  String JrnlMemo ="Towards Purchase Tax of "+wt+ " tons @ Rs."+strpurchaseTax+"";
							  String accountCode ="CANEPURTAX";	
							  String transType="Cr";
							  finalAmtToPost=purchaseTax*ws.getNetwt();
							  Amt=finalAmtToPost;
							  //Added by DMurty on 25-02-2017
							  Amt = agricultureHarvestingController.round(Amt);
							  String accCode=accountCode;
							  String ryotcode=ws.getRyotcode();
							  logger.info("========ryotcode========== Gl Code" + ryotcode);
							 String cdate=DateUtils.formatDate(ws.getCanereceiptenddate(),Constants.GenericDateFormat.DATE_FORMAT);
							Map DtlsMap = new HashMap();
							DtlsMap.put("TRANSACTION_CODE", transactionCode);
							DtlsMap.put("TRANSACTION_AMOUNT", Amt);
							DtlsMap.put("LOGIN_USER", ws.getLoginid());
							DtlsMap.put("ACCOUNT_CODE", accountCode);
							DtlsMap.put("JOURNAL_MEMO", JrnlMemo);
							DtlsMap.put("TRANSACTION_DATE", cdate);
							DtlsMap.put("TRANSACTION_TYPE", transType);
							DtlsMap.put("SEASON", season);
							DtlsMap.put("GL_CODE", ryotcode);
							
							logger.info("========DtlsMap==========" + DtlsMap);
							String TransactionType = transType;
							AccountDetails accountDetails = prepareModelForAccountDetails(DtlsMap);
							trmentityList.add(accountDetails);
							AccountSummary accountSummary = prepareModelforAccSmry(Amt,accountCode, TransactionType,season);
							Map AccSmry = new HashMap();
							AccSmry.put("ACC_CODE", accountCode);
							boolean advAccCodetrue = AccountCodeList.contains(AccSmry);
							if (advAccCodetrue == false)
							{
								AccountCodeList.add(AccSmry);
								AccountSummaryMap.put(accountCode,accountSummary);
							}
							else
							{
								accountSummary = (AccountSummary) AccountSummaryMap.get(accountCode);
								String straccCode = accountSummary.getAccountcode();
								if(straccCode.equalsIgnoreCase(accCode) && season.equals(accountSummary.getSeason()))
								{
									double runbal = accountSummary.getRunningbalance();
									String baltype = accountSummary.getBalancetype();
									String ses = accountSummary.getSeason();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_CODE", accCode);
									tmpMap.put("KEY", accCode);
									Map finalAccSmryMap = returnAccountSummaryModel(tmpMap);
									AccountSummaryMap.put(accCode,finalAccSmryMap.get(accCode));
								}
							}
							
							// Insert Rec in Acc Grp Details
							
							int accGrpCode = ahFuctionalService.GetAccGrpCode(accountCode);
							String strAccGrpCode = Integer.toString(accGrpCode);
							Map AccGrpDtlsMap = new HashMap();
							AccGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
							AccGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							//added by sahadeva 28/10/2016
							AccGrpDtlsMap.put("LOGIN_USER", ws.getLoginid());
							AccGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
							AccGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
							AccGrpDtlsMap.put("JOURNAL_MEMO", JrnlMemo);
							AccGrpDtlsMap.put("TRANSACTION_TYPE", transType);
							AccGrpDtlsMap.put("TRANSACTION_DATE", cdate);
							//Added by DMurty on 18-10-2016
							AccGrpDtlsMap.put("SEASON",season);
							logger.info("========AccGrpDtlsMap==========" + AccGrpDtlsMap);
	
							AccountGroupDetails accountGroupDetails =prepareModelForAccGrpDetails(AccGrpDtlsMap);
							trmentityList.add(accountGroupDetails);
							
							AccountGroupSummary accountGroupSummary = prepareModelForAccGrpSmry(accountCode, Amt, accGrpCode, TransactionType, season);
							
							Map AccGrpSmry = new HashMap();
							AccGrpSmry.put("ACC_GRP_CODE", accGrpCode);
							boolean advAccGrpCodetrue = AccountGrpCodeList.contains(AccGrpSmry);
							if (advAccGrpCodetrue == false)
							{
								AccountGrpCodeList.add(AccGrpSmry);
								AccountGroupMap.put(strAccGrpCode,accountGroupSummary);
							}
							else
							{
								accountGroupSummary = (AccountGroupSummary) AccountGroupMap.get(strAccGrpCode);
								int accountGrpCode = accountGroupSummary.getAccountgroupcode();
								if(accGrpCode == accountGrpCode && season.equals(accountGroupSummary.getSeason()))
								{
									double runbal = accountGroupSummary.getRunningbalance();
									String baltype = accountGroupSummary.getBalancetype();
									String ses = accountGroupSummary.getSeason();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
									tmpMap.put("KEY", strAccGrpCode);
									
									Map finalAccGrpSmryMap =returnAccountGroupSummaryModel(tmpMap);
									AccountGroupMap.put(strAccGrpCode, finalAccGrpSmryMap.get(strAccGrpCode));
								}
							}
							
							Map AccSubGrpDtlsMap = new HashMap();

							AccSubGrpDtlsMap.put("TRANSACTION_CODE", transactionCode);
							AccSubGrpDtlsMap.put("TRANSACTION_AMOUNT", Amt);
							AccSubGrpDtlsMap.put("LOGIN_USER", ws.getLoginid());
							
							AccSubGrpDtlsMap.put("ACCOUNT_CODE", accountCode);
							AccSubGrpDtlsMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
							AccSubGrpDtlsMap.put("JOURNAL_MEMO",JrnlMemo);
							AccSubGrpDtlsMap.put("TRANSACTION_TYPE", transType);
							AccSubGrpDtlsMap.put("SEASON", season);
							AccSubGrpDtlsMap.put("TRANSACTION_DATE", cdate);
							logger.info("========AccSubGrpDtlsMap==========" + AccSubGrpDtlsMap);

							int AccSubGrpCode = ahFuctionalService.GetAccSubGrpCode(accCode);
							String strAccSubGrpCode = Integer.toString(AccSubGrpCode);

							
							AccountSubGroupDetails accountSubGroupDetails =prepareModelForAccSubGrpDtls(AccSubGrpDtlsMap);
							trmentityList.add(accountSubGroupDetails);
							
							TransactionType = transType; 
							AccountSubGroupSummary accountSubGroupSummary =prepareModelforAccSubGrpSmry(accountCode, Amt, accGrpCode, TransactionType,season);
							
							Map AccSubGrpSmry = new HashMap();
							//AccSubGrpSmry.put("ACC_GRP_CODE", accGrpCode);
							AccSubGrpSmry.put("ACC_SUB_GRP_CODE", AccSubGrpCode);
							
							boolean advAccSubGrpCodetrue = AccountSubGrpCodeList.contains(AccSubGrpSmry);
							if(advAccSubGrpCodetrue == false)
							{
								AccountSubGrpCodeList.add(AccSubGrpSmry);
								AccountSubGroupMap.put(strAccSubGrpCode,accountSubGroupSummary);
							}
							else
							{
								accountSubGroupSummary = (AccountSubGroupSummary) AccountSubGroupMap.get(strAccSubGrpCode);
								int accgrpcode = accountSubGroupSummary.getAccountgroupcode();
								int accsubgrpcode = accountSubGroupSummary.getAccountsubgroupcode();
								String ses = accountSubGroupSummary.getSeason();
								if(accGrpCode == accgrpcode && AccSubGrpCode==accsubgrpcode && season.equalsIgnoreCase(ses))
								{
									double runbal = accountSubGroupSummary.getRunningbalance();
									String baltype = accountSubGroupSummary.getBalancetype();
									
									Map tmpMap = new HashMap();
									tmpMap.put("RUN_BAL", runbal);
									tmpMap.put("AMT", Amt);
									tmpMap.put("BAL_TYPE", baltype);
									tmpMap.put("SEASON", ses);
									tmpMap.put("TRANSACTION_TYPE", TransactionType);
									tmpMap.put("ACCOUNT_GROUP_CODE", accGrpCode);
									tmpMap.put("ACCOUNT_SUB_GROUP_CODE", AccSubGrpCode);
									tmpMap.put("KEY", strAccSubGrpCode);
									Map finalAccSubGrpSmryMap =returnAccountSubGroupSummaryModel(tmpMap);
									AccountSubGroupMap.put(strAccSubGrpCode,finalAccSubGrpSmryMap.get(strAccSubGrpCode));
								}
							}
						
						}
					}
					logger.info("========After Accounting Postings==========");

					//Added by DMurty on 12-11-2016
					Iterator entries = AccountSummaryMap.entrySet().iterator();
					while (entries.hasNext())
					{
					    Map.Entry entry = (Map.Entry) entries.next();
					    Object value = entry.getValue();
					    AccountSummary accountSummary = (AccountSummary) value;
					    
					    String accountCode1 = accountSummary.getAccountcode();
					    String sesn = accountSummary.getSeason();
					    double runnbal = accountSummary.getRunningbalance();
					    String crOrDr = accountSummary.getBalancetype();
					    
					    List AccList = ahFuctionalService.getAccDetailsFromSummary(accountCode1,sesn);
					    if(AccList != null && AccList.size()>0)
					    {
					    	String qry = "Update AccountSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountcode='"+accountCode1+"' and season='"+sesn+"'";
					    	Qryobj = qry;
					    	UpdateList.add(Qryobj);
					    }
					    else
					    {
					    	AccountsList.add(accountSummary);
					    }
					}
					
					Iterator entries1 = AccountGroupMap.entrySet().iterator();
					while (entries1.hasNext())
					{
					    Map.Entry entry = (Map.Entry) entries1.next();
					    Object value = entry.getValue();
					    
					    AccountGroupSummary accountGroupSummary = (AccountGroupSummary) value;
					    int accountGroupCode = accountGroupSummary.getAccountgroupcode();
					    String sesn = accountGroupSummary.getSeason();
					    double runnbal = accountGroupSummary.getRunningbalance();
					    String crOrDr = accountGroupSummary.getBalancetype();
					    
					    List AccList = ahFuctionalService.getAccGrpDetailsFromSummary(accountGroupCode,sesn);
					    if(AccList != null && AccList.size()>0)
					    {
					    	String qry = "Update AccountGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and season='"+sesn+"'";
					    	Qryobj = qry;
					    	UpdateList.add(Qryobj);
					    }
					    else
					    {
					    	AccountsList.add(accountGroupSummary);
					    }
					}
					
					Iterator entries2 = AccountSubGroupMap.entrySet().iterator();
					while (entries2.hasNext())
					{
					    Map.Entry entry = (Map.Entry) entries2.next();
					    Object value = entry.getValue();
					    AccountSubGroupSummary accountSubGroupSummary = (AccountSubGroupSummary) value;
					   
					    int accountGroupCode = accountSubGroupSummary.getAccountgroupcode();
					    int accountSubGroupCode = accountSubGroupSummary.getAccountsubgroupcode();
					    String sesn = accountSubGroupSummary.getSeason();
					    double runnbal = accountSubGroupSummary.getRunningbalance();
					    String crOrDr = accountSubGroupSummary.getBalancetype();
					    
					    List AccList = ahFuctionalService.getAccSubGrpDtls(accountGroupCode,accountSubGroupCode,sesn);
					    if(AccList != null && AccList.size()>0)
					    {
					    	String qry = "Update AccountSubGroupSummary set runningbalance="+runnbal+",balancetype='"+crOrDr+"' where accountgroupcode='"+accountGroupCode+"' and accountsubgroupcode="+accountSubGroupCode+" and season='"+sesn+"'";
					    	Qryobj = qry;
					    	UpdateList.add(Qryobj);
					    }
					    else
					    {
					    	AccountsList.add(accountSubGroupSummary);
					    }
					}
					logger.info("========After Accounting Postings=========="+AccountsList);
				
					logger.info("========caneWeighmentTablesList==========" + trmentityList);
					logger.info("========UpdateList==========" + UpdateList);
					isInsertTrue = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(trmentityList,UpdateList,AccountsList);
					logger.info("========isInsertTrue==========" + isInsertTrue);
				}
				return isInsertTrue;
			}
			catch(Exception e)
			{
				logger.info("========SeedAndSeedMigration==========" +e);
				return isInsertTrue;
			}
		}
			
		@RequestMapping(value = "/DedsCaStatusMigration", method = RequestMethod.POST)
		public @ResponseBody
		boolean DedsCaStatusMigration(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List CaneAccountsList = new ArrayList();
			List RyotList = new ArrayList();
			
			String strQry = null;
			Object Qryobj = null;
			Date FromDt = null;
			Date ToDt = null;
			
			CaneAccountsList = ahFuctionalService.getCompletedCaneAccounts("CaneAccountSmry");
			if(CaneAccountsList != null && CaneAccountsList.size()>0)
			{
				for (int j = 0; j < CaneAccountsList.size(); j++)
				{
					
					Map VMap=(Map)CaneAccountsList.get(j);
					int nCaneActSlNo = (Integer) VMap.get("caneacslno");
					FromDt = (Date)VMap.get("datefrom");
					ToDt = (Date)VMap.get("dateto");
					RyotList = ahFuctionalService.getRyotList(nCaneActSlNo,"CaneValueSplitup");
					if (RyotList != null && RyotList.size() > 0)
					{
						for (int k=0; k<RyotList.size(); k++)
						{
							Map hm=(Map)RyotList.get(k);			
							String strRyotCode = (String) hm.get("ryotcode");

							strQry = "Update CDCDetailsByRotAndDate set castatus=1 where ryotcode='"+strRyotCode+"' and receiptdate between  '"+FromDt+"' and '"+ToDt+"'";
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);
					    	
							strQry = "Update HarvChgDtlsByRyotContAndDate set castatus=1 where ryotcode='"+strRyotCode+"' and receiptdate between  '"+FromDt+"' and '"+ToDt+"'";
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);
					    	
							strQry = "Update TranspChgSmryByRyotContAndDate set castatus=1 where ryotcode='"+strRyotCode+"' and receiptdate between  '"+FromDt+"' and '"+ToDt+"'";
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);
					    	
							strQry = "Update UCDetailsByRotAndDate set castatus=1 where ryotcode='"+strRyotCode+"' and receiptdate between  '"+FromDt+"' and '"+ToDt+"'";
							Qryobj = strQry;
					    	UpdateList.add(Qryobj);

						}						
					}	
				}
				//isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
				isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
			}
			return isInsertSuccess;
		}
		@RequestMapping(value = "/updateHarvesterCode", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateHarvesterCode(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			HashMap hm= null;
			int code=4;
			List DedList = ahFuctionalService.getHarvestingOrTpDetails(code);
			if(DedList != null && DedList.size()>0)
			{
				for (int j = 0; j < DedList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)DedList.get(j);
					String ryotCode = (String) VMap.get("ryotcode");
					String season = (String) VMap.get("season");
					int harvestCode = ahFuctionalService.ContCode(ryotCode,season,code);
					
					strQry = "Update DedDetails set harvestcontcode="+harvestCode+" where ryotcode='"+ryotCode+"' and season='"+season+"' and DedCode="+code;
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		
		@RequestMapping(value = "/updateTransporterCode", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateTransporterCode(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			HashMap hm= null;
			int code=3;
			List DedList = ahFuctionalService.getHarvestingOrTpDetails(code);
			if(DedList != null && DedList.size()>0)
			{
				for (int j = 0; j < DedList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)DedList.get(j);
					String ryotCode = (String) VMap.get("ryotcode");
					String season = (String) VMap.get("season");
					int tpCode = ahFuctionalService.ContCode(ryotCode,season,code);
					
					strQry = "Update DedDetails set harvestcontcode="+tpCode+" where ryotcode='"+ryotCode+"' and season='"+season+"' and DedCode="+code;
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		//Added by DMurty on 13-02-2017
		@RequestMapping(value = "/updateSeedSupplierAccounts", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateSeedSupplierAccounts(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season=(String)jsonData.get("dropdownname");
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			HashMap hm= null;
			List AccList = new ArrayList();

			List DedList = ahFuctionalService.getSeedSupplierAccounts(season);
			if(DedList != null && DedList.size()>0)
			{
				for (int j = 0; j < DedList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)DedList.get(j);
					String ryotCode = (String) VMap.get("ryotcode");
					int transactionCode = (Integer) VMap.get("transactioncode");
					String ses = (String) VMap.get("season");
					int id = (Integer) VMap.get("id");

					Map AccMap = new HashMap();
					AccMap.put("ACCOUNT_CODE", ryotCode);
					
					boolean isAcctrue = AccList.contains(AccMap);
					if(isAcctrue == false)
					{
						AccList.add(AccMap);
					}
					
					strQry = "Update AccountDetails set cr=0,runningbalance=0 where accountcode='"+ryotCode+"' and season='"+season+"' and glcode='"+ryotCode+"' and transactioncode="+transactionCode;
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
			    	
			    	strQry = "Update AccountGroupDetails set cr=0,runningbalance=0 where season='"+season+"' and transactioncode="+transactionCode+" and accountgroupcode=2 and cr>0";
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
			    	
			    	strQry = "Update AccountSubGroupDetails set cr=0,runningbalance=0 where season='"+season+"' and transactioncode="+transactionCode+" and accountgroupcode=2 and accountsubgroupcode=2 and cr>0";
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
				if(isInsertSuccess == true)
				{
					UpdateList.clear();
					//AccountSummary
					if(AccList != null && AccList.size()>0)
					{
						for (int i = 0; i < AccList.size(); i++)
						{
							Map AccMap = new HashMap();
							AccMap=(Map)AccList.get(i);
							
							String accCode=(String)AccMap.get("ACCOUNT_CODE");
							double cr = 0.0;
							double dr = 0.0;
							List BalList = ahFuctionalService.getRunningBalForAccount(accCode,season);
							if(BalList != null && BalList.size()>0)
							{
								Map AccSmryMap = new HashMap();
								AccSmryMap=(Map)BalList.get(0);
								
								cr=(Double)AccSmryMap.get("cr");
								dr=(Double)AccSmryMap.get("dr");
								double runBal = 0.0;
								String balType = "";
								if(cr>dr)
								{
									runBal = cr-dr;
									balType = "Cr";
								}
								else
								{
									runBal = dr-cr;
									balType = "Dr";
								}
								
						    	strQry = "Update AccountSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountcode='"+accCode+"' and season='"+season+"'";
						    	Qryobj = strQry;
						    	UpdateList.add(Qryobj);
							}
						}
					}
					
					//AccountGroupSummary
					int accountgroupcode=2;
					double cr = 0.0;
					double dr = 0.0;
					List AccGrpList = ahFuctionalService.getRunningBalForAccountGrp(accountgroupcode,season);
					if(AccGrpList != null && AccGrpList.size()>0)
					{
						Map AccGrpSmryMap = new HashMap();
						AccGrpSmryMap=(Map)AccGrpList.get(0);
						
						cr=(Double)AccGrpSmryMap.get("cr");
						dr=(Double)AccGrpSmryMap.get("dr");
						double runBal = 0.0;
						String balType = "";
						if(cr>dr)
						{
							runBal = cr-dr;
							balType = "Cr";
						}
						else
						{
							runBal = dr-cr;
							balType = "Dr";
						}
				    	strQry = "Update AccountGroupSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountgroupcode="+accountgroupcode+" and season='"+season+"'";
				    	Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					
					//AccountSubGroupSummary
					int accountsubgroupcode=2;
					cr = 0.0;
					dr = 0.0;
					List AccSubGrpList = ahFuctionalService.getRunningBalForAccountSubGrp(accountgroupcode,season);
					if(AccSubGrpList != null && AccSubGrpList.size()>0)
					{
						Map AccSubGrpSmryMap = new HashMap();
						AccSubGrpSmryMap=(Map)AccSubGrpList.get(0);
						
						cr=(Double)AccSubGrpSmryMap.get("cr");
						dr=(Double)AccSubGrpSmryMap.get("dr");
						double runBal = 0.0;
						String balType = "";
						if(cr>dr)
						{
							runBal = cr-dr;
							balType = "Cr";
						}
						else
						{
							runBal = dr-cr;
							balType = "Dr";
						}
				    	strQry = "Update AccountSubGroupSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountsubgroupcode="+accountsubgroupcode+" and season='"+season+"'";
				    	Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
				}
			}
			else
			{
				isInsertSuccess = true;
			}
			return isInsertSuccess;
		}
		@RequestMapping(value = "/updateWeighmentAccounts", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateWeighmentAccounts(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			
			List WeighmentList = ahFuctionalService.getWeighmentDetailsData();
			if(WeighmentList != null && WeighmentList.size()>0)
			{
				for (int j = 0; j < WeighmentList.size(); j++)
				{
					logger.info("========updateWeighmentAccounts()==========" +j);

					//Map hm= new HashMap();
					Map WMap=(Map)WeighmentList.get(j);
					String ryotCode = (String) WMap.get("ryotcode");
					logger.info("==================ryotCode" +ryotCode);

					String season = (String) WMap.get("season");
					Integer tCode =(Integer) WMap.get("transactioncode");
					Double netwt =(Double) WMap.get("netwt");
					Double Amt=netwt*2332;
					Amt = Double.parseDouble(new DecimalFormat("##.##").format(Amt));

					List GetAccInfo = ahFuctionalService.GetAccInfo(ryotCode,season,tCode,"FRP");
					if(GetAccInfo != null && GetAccInfo.size()>0)
					{ 
						Map GMap=(Map)GetAccInfo.get(0);
						Integer id =(Integer) GMap.get("id");
						String strQry1 = "Update AccountDetails set cr="+Amt+",runningbalance="+Amt+" where id="+id;
						Object Qryobj = strQry1;
						UpdateList.add(Qryobj);
					}
					List GetAccInfo1 = ahFuctionalService.GetAccInfo("FRP",season,tCode,ryotCode);
					if(GetAccInfo1 != null && GetAccInfo1.size()>0)
					{ 
						Map GMap=(Map)GetAccInfo1.get(0);
						Integer id =(Integer) GMap.get("id");
						String strQry2 = "Update AccountDetails set dr="+Amt+",runningbalance="+Amt+" where id="+id;
						Object Qryobj = strQry2;
						UpdateList.add(Qryobj);
					}
					
					Amt=netwt*258;
					Amt =Double.parseDouble(new DecimalFormat("##.##").format(Amt));
					List GetAccInfo2 = ahFuctionalService.GetAccInfo(ryotCode,season,tCode,"ACP");
					if(GetAccInfo2 != null && GetAccInfo2.size()>0)
					{ 
						Map GMap=(Map)GetAccInfo2.get(0);
						Integer id =(Integer) GMap.get("id");
						String strQry3 = "Update AccountDetails set cr="+Amt+",runningbalance="+Amt+" where id="+id;
						Object Qryobj = strQry3;
						UpdateList.add(Qryobj);
					}
					List GetAccInfo3 = ahFuctionalService.GetAccInfo("ACP",season,tCode,ryotCode);
					if(GetAccInfo3 != null && GetAccInfo3.size()>0)
					{ 
						Map GMap=(Map)GetAccInfo3.get(0);
						Integer id =(Integer) GMap.get("id");
						String strQry4 = "Update AccountDetails set dr="+Amt+",runningbalance="+Amt+" where id="+id;
						Object Qryobj = strQry4;
						UpdateList.add(Qryobj);
					}
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		
		//added by sahu on 15-02-2017 For weighmentdate in accounting tables

		@RequestMapping(value = "/updateWeighmentCaneReceiptDates", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateWeighmentCaneReceiptDates(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			
			List WeighmentList = ahFuctionalService.getWeighmentDatesData();
			if(WeighmentList != null && WeighmentList.size()>0)
			{
				for (int j = 0; j < WeighmentList.size(); j++)
				{
					logger.info("========updateWeighmentCaneReceiptDates()==========" +j);

					//Map hm= new HashMap();
					Map WMap=(Map)WeighmentList.get(j);
					
					String season = (String) WMap.get("season");
					Integer tCode =(Integer) WMap.get("transactioncode");
					Date caneRecDate =(Date) WMap.get("canereceiptenddate");
					
					String strQry = "Update AccountDetails set transactiondate='"+caneRecDate+"' where transactioncode="+tCode+" and season='"+season+"' ";
					Object Qryobj = strQry;
					UpdateList.add(Qryobj);
					String strQry1 = "Update AccountGroupDetails set transactiondate='"+caneRecDate+"' where transactioncode="+tCode+" and season='"+season+"' ";
					Object Qryobj1 = strQry1;
					UpdateList.add(Qryobj1);
					String strQry2 = "Update AccountSubGroupDetails set transactiondate='"+caneRecDate+"' where transactioncode="+tCode+" and season='"+season+"' ";
					Object Qryobj2 = strQry2;
					UpdateList.add(Qryobj2);
				
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		
		//Added by DMurty on 16-02-2017
		@RequestMapping(value = "/updateLoanPrinciplesForAccountedRyots", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateLoanPrinciplesForAccountedRyots(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			HashMap hm= null;
			int code=3;
			List DedList = ahFuctionalService.updateLoanPrinciplesForAccountedRyots();
			if(DedList != null && DedList.size()>0)
			{
				for (int j = 0; j < DedList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)DedList.get(j);
					
					String ryotCode = (String) VMap.get("ryotcode");
					String season = (String) VMap.get("season");
					int bankBranch=(Integer)VMap.get("branchcode");

					
					int loanno=(Integer)VMap.get("loanno");
					double intrestAmt = (Double)VMap.get("interestamount");
					double loanPending = (Double) VMap.get("pendingamt");	//From Cane Accounting//5000
					double loanAmt =  (Double) VMap.get("advanceamount"); //Loan Amount
					double paidAmt = loanPending;	//We need to Assume pending is paid
					
					double prevAdvisedamt = 0.0;
					double finalAdvised = prevAdvisedamt+paidAmt;
					double additionalInt = 0.0;
					double finalIntrest = intrestAmt+additionalInt;			//7500+add
					
					
					//this needs to be deducted from principle
					double loanPayable = paidAmt-finalIntrest;			//5000-7500=-2500 = principle left
					double finalPrinciple = loanAmt-loanPayable;			//100000-(-2500)=102500 // this is same as adjustedPrincple + addn.int
					
					double intryotpayable = finalIntrest;// once delayed intrest is calculated. This will be final int-delayed int. Present Delayed int is 0.
					
					
					List loanSummaryList=caneAccountingFunctionalService.getLoanSummaryByBankAndLoanNo(season,ryotCode,loanno,bankBranch);
					if(loanSummaryList!=null)
					{		
						LoanSummary loanSummary=(LoanSummary)loanSummaryList.get(0);
						
						double intrestAmount = loanSummary.getInterestamount();
						intrestAmount = intrestAmount+finalIntrest;
						loanSummary.setInterestamount(intrestAmount);
						double disbursed = loanSummary.getDisbursedamount();
						double totalAmt = disbursed+finalIntrest;
						loanSummary.setTotalamount(totalAmt);
						
						double fpaid = loanSummary.getPaidamount()+paidAmt;
						loanSummary.setPaidamount(fpaid);
						
						loanSummary.setPendingamount(finalPrinciple);
						loanSummary.setRyotcode(loanSummary.getRyotcode());
						entityList.add(loanSummary);						
						
						int branchCode = loanSummary.getBranchcode();

						Date date = new Date();
						DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String stradvDate = df.format(date);
						Date principleDate = DateUtils.getSqlDateFromString(stradvDate,Constants.GenericDateFormat.DATE_FORMAT);
						
						String qry = "Update LoanPrincipleAmounts set principle="+finalPrinciple+",principledate='"+principleDate+"' where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
						logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
						Qryobj = qry;
						UpdateList.add(Qryobj);
						
						//Updating Record in Loan Details table.
						qry = "Update LoanDetails set interestamount="+intrestAmount+",pendingamount="+finalPrinciple+",recoverydate='"+principleDate+"',paidamount="+fpaid+" where ryotcode='"+ryotCode+"' and season='"+season+"' and loannumber="+loanSummary.getLoannumber()+" and branchcode="+branchCode;
						logger.info("======== Update AdvancePrinciple Amt qry=========="+qry);
						Qryobj = qry;
						UpdateList.add(Qryobj);
					}
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		
		//Added by DMurty on 22-02-2017
		@RequestMapping(value = "/updateOldLoans", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateOldLoans(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			
			List loanList = ahFuctionalService.getOldLoans();
			if(loanList != null && loanList.size()>0)
			{
				for (int j = 0; j < loanList.size(); j++)
				{
					logger.info("========updateOldLoans()==========" +j);

					Map WMap=(Map)loanList.get(j);
					logger.info("========updateOldLoans()==========WMap" +WMap);

					String season = (String) WMap.get("season");
					int loanNumber =(Integer) WMap.get("loannumber");
					int branchcode =(Integer) WMap.get("branchcode");
					double principle = (Double) WMap.get("Principle");
					double disbursedamount = (Double) WMap.get("disbursedamount");
					
					double interestamount = 0.0;
					if((Double) WMap.get("interestamount") != null)
					{
						interestamount = (Double) WMap.get("interestamount");
					}
					
					double paidamount = 0.0;
					if((Double) WMap.get("paidamount") != null)
					{
						paidamount = (Double) WMap.get("paidamount");
					}
					
					double pendingamount = 0.0;
					if((Double) WMap.get("pendingamount") != null)
					{
						pendingamount = (Double) WMap.get("pendingamount");
					}
					
					String ryotcode = (String) WMap.get("ryotcode");
					logger.info("========updateOldLoans()==========ryotcode" +ryotcode);

					List loanDetailsList = ahFuctionalService.checkLoanDetails(loanNumber,branchcode,season);
					if(loanDetailsList != null && loanDetailsList.size()>0)
					{
						continue;
					}
					else
					{
						
						LoanDetails ld = new LoanDetails();
						
						CompositePrKeyForLoans compositePrKeyForLoans = new CompositePrKeyForLoans();
						compositePrKeyForLoans.setBranchcode(branchcode);
						compositePrKeyForLoans.setLoannumber(loanNumber);
						compositePrKeyForLoans.setSeason(season);
						ld.setCompositePrKeyForLoans(compositePrKeyForLoans);
						ld.setRyotcode(ryotcode);
						String strLoanNo = Integer.toString(loanNumber);
						String addedLoan = strLoanNo+"-"+ryotcode;
						ld.setAddedloan(addedLoan);
						
						ld.setInterestamount(interestamount);
						ld.setPaidamount(paidamount);
						ld.setPendingamount(pendingamount);
						ld.setPrinciple(principle);
						
						List oldLoanList = ahFuctionalService.getOldLoanDetails(loanNumber,branchcode,season);
						if(oldLoanList != null && oldLoanList.size()>0)
						{
							Map tempMap=(Map)oldLoanList.get(0);
							
							String agreementnumber = (String) tempMap.get("agreementnumber");
							int caneManagrId = (Integer) tempMap.get("canemanagerid");
							Date disburseddate = (Date) tempMap.get("disburseddate");
							double disamt = 0.0;
							if((Double) tempMap.get("disbursedamount") != null)
							{
								disamt = (Double) tempMap.get("disbursedamount");
							}
							int fieldassistantid = (Integer) tempMap.get("fieldassistantid");
							int fieldofficerid = (Integer) tempMap.get("fieldofficerid");
							int loanstatus = (Integer) tempMap.get("loanstatus");
							String loginid = (String) tempMap.get("loginid");
							String plantextent = (String) tempMap.get("plantextent");
							String ratoonextent = (String) tempMap.get("ratoonextent");
							Date recommendeddate = (Date) tempMap.get("recommendeddate");
							Date recoverydate = (Date) tempMap.get("recoverydate");
							String referencenumber = (String) tempMap.get("referencenumber");
							String suveynumber = (String) tempMap.get("suveynumber");
							double totalamount = (Double) tempMap.get("totalamount");
							int transactioncode = (Integer) tempMap.get("transactioncode");
							double interstrate = (Double) tempMap.get("interstrate");

							
							ld.setAgreementnumber(agreementnumber);
							ld.setCanemanagerid(caneManagrId);
							ld.setDisburseddate(disburseddate);
							ld.setDisbursedamount(disamt);
							ld.setFieldassistantid(fieldassistantid);
							ld.setFieldofficerid(fieldofficerid);
							ld.setInterstrate(interstrate);

							ld.setLoanstatus(loanstatus);
							ld.setLoginid(loginid);
							ld.setPlantextent(plantextent);
							ld.setRatoonextent(ratoonextent);
							ld.setRecommendeddate(recommendeddate);
							ld.setRecoverydate(recoverydate);
							ld.setReferencenumber(referencenumber);
							ld.setSuveynumber(suveynumber);
							ld.setTotalamount(totalamount);
							ld.setTransactioncode(transactioncode);
							ld.setBatchno(0);
							
							entityList.add(ld);
						}
					}
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		//Added by DMurty on 24-02-2017
		@RequestMapping(value = "/loanReconsilation", method = RequestMethod.POST)
		public @ResponseBody
		boolean loanReconsilation(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season=(String)jsonData.get("dropdownname");
			String suppliedOrNot=(String)jsonData.get("suppliedOrNot");
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			List loanDetails = ahFuctionalService.loanReconsilation(suppliedOrNot);
			if(loanDetails != null && loanDetails.size()>0)
			{
				ahFuctionalService.TruncateTemp("LoanReconsilation");
				for (int j = 0; j < loanDetails.size(); j++)
				{
					logger.info("========loanReconsilation()==========" +j);
					Map WMap=(Map)loanDetails.get(j);
					
					String ryotcode = (String) WMap.get("ryotcode");

					LoanReconsilation lr = new LoanReconsilation();
					
					List<Ryot> ryot = ryotService.getRyotByRyotCode(ryotcode);
					lr.setRyotname(ryot.get(0).getRyotname());
					lr.setRyotcode(ryotcode);
					
					List amtDetails = ahFuctionalService.getRyotSupplyAndPayDetails(ryotcode,season);
					if(amtDetails != null && amtDetails.size()>0)
					{
						Map tempMap=(Map)amtDetails.get(0);
						
						double suppqty = (Double) tempMap.get("qty");
						double totalamt = (Double) tempMap.get("val");
						double cdcamt = (Double) tempMap.get("cdc");
						double ulamt = (Double) tempMap.get("ul");
						double harvesting = (Double) tempMap.get("har");
						double transport = (Double) tempMap.get("tp");
						double otherded = (Double) tempMap.get("od");
						double advamt = (Double) tempMap.get("advance");
						double loanrecovered = (Double) tempMap.get("loan");
						double sbac = (Double) tempMap.get("sb");

						lr.setSuppqty(suppqty);
						lr.setTotalamt(totalamt);
						lr.setCdcamt(cdcamt);
						lr.setUlamt(ulamt);
						lr.setHarvesting(harvesting);
						lr.setTransport(transport);
						lr.setAdvamt(advamt);
						lr.setLoanrecovered(loanrecovered);
						lr.setSbac(sbac);
						
						Double loantaken = ahFuctionalService.getTotalLoanAmt(season,ryotcode);
						lr.setLoantaken(loantaken);
						
						entityList.add(lr);
					}
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			return isInsertSuccess;
		}
		//Added by sahadeva on 24-02-2017
		@RequestMapping(value = "/caneSupplyAndPaymentStatementarray", method = RequestMethod.POST, headers = { "Content-type=application/json" })
		public @ResponseBody
		JSONArray caneSupplyAndPaymentStatementarray(@RequestBody String  jsonData) throws Exception
		{	
    		JSONArray jArray = new JSONArray();
    		JSONObject jsonObj = null;
    		jsonObj = new JSONObject();
    		String season=jsonData;
    		String ryotcode=null;
    		Double balancedue=0.0;
    		
    		String sql=" select distinct ryotcode from CaneValueSplitup where season='"+season+"'  order by ryotcode ";
      		List ls=hibernatedao.findBySqlCriteria(sql);
      		
      		String sql8=" select distinct ryotcode from AdvancePrincipleAmounts where season='"+season+"' and principle >0 order by ryotcode ";
      		List AdvList=hibernatedao.findBySqlCriteria(sql8);
      	//	List AdvList1 = new ArrayList();
      		if(AdvList.size()>0 && AdvList!=null)
      		{	
      			for (int k = 0; k <AdvList.size(); k++) 
      			{
      				Map tempMap = (Map) AdvList.get(k);
      				boolean flag = ls.contains(tempMap);
      				if(flag == false)
      				{
      					ls.add(tempMap);
      				}
      			}
      		}
      		//ls.addAll(AdvList1);
      		String sql9=" select distinct ryotcode from LoanPrincipleAmounts where season='"+season+"' and principle >0 order by ryotcode ";
      		List LoanList=hibernatedao.findBySqlCriteria(sql9);
      	//	List LoanList1 = new ArrayList();
      		if(LoanList.size()>0 && LoanList!=null)
      		{	
      			for (int k = 0; k <LoanList.size(); k++) 
      			{
      				Map tempMap = (Map) LoanList.get(k);
      				boolean flag = ls.contains(tempMap);
      				if(flag == false)
      				{
      					ls.add(tempMap);
      				}
      			}
      		}
      	//	ls.addAll(LoanList1);
      		if(ls.size()>0 && ls!=null)
      		{	
      			for (int k = 0; k <ls.size(); k++) 
      			{
      				logger.info("------k---------------"+ k);
      				double tfr = 0.0;
      				double sbpaid = 0.0;
      				ryotcode=(String)((Map<Object,Object>)ls.get(k)).get("ryotcode");
     				logger.info("------k---------------ryotcode"+ ryotcode);
     				
     				String sql10=" select sum(canesupplied) canesupplied,sum(totalvalue) totalvalue from CaneValueSplitup where season='"+season+"' and ryotcode='"+ryotcode+"' ";
     	      		List ls10=hibernatedao.findBySqlCriteria(sql10);
     	      		if(ls10!=null && ls10.size()>0 )
      		  		{
     	      			Double canesupplied=(Double)((Map<Object,Object>)ls10.get(0)).get("canesupplied");
     	      			if(canesupplied !=null)
     	      			{
     	      				jsonObj.put("Canesupply", canesupplied);
     	      			}
     	      			else
     	      			{
     	      				jsonObj.put("Canesupply", 0.0);
     	      			}
     	      			Double totalvalue=(Double)((Map<Object,Object>)ls10.get(0)).get("totalvalue");
     	      			if(totalvalue !=null)
     	      			{
     	      				jsonObj.put("Canevalue", totalvalue);
     	      			}
     	      			else
     	      			{
     	      				jsonObj.put("Canevalue", 0.0);
     	      			}
      		  		} 
     	      		String sql13=" select ryotname from ryot where ryotcode='"+ryotcode+"'  ";
     	      		List ls13=hibernatedao.findBySqlCriteria(sql13);
     	      		if(ls13.size()>0 && ls13!=null)
      		  		{
     	      			String ryotname=(String)((Map<Object,Object>)ls13.get(0)).get("ryotname");
     	      			jsonObj.put("Ryotname", ryotname);
      		  		}
      				jsonObj.put("Ryotcode", ryotcode);
      				
      				String	sql1=" select sum(cdcamt) cdc,sum(ulamt) ul,sum(transport) transport,sum(harvesting) harvesting,sum(otherded) otherded,sum(sbac) sbac,sum(paidamt) paidamt from CanAccSBDetails where ryotcode='"+ryotcode+"' and season='"+season+"'";
      		  		List ls1=hibernatedao.findBySqlCriteria(sql1);
      		  		Double sbpaidamt=0.0;
      		  		Double transport=0.0;
      		  		if(ls1.size()>0 && ls1!=null)
      		  		{
      		  			Double cdc=(Double)((Map<Object,Object>)ls1.get(0)).get("cdc");
      		  			if(cdc !=null)
      		  			{
      		  				jsonObj.put("cdc", cdc);
      		  			}
      		  			else{
      		  			jsonObj.put("cdc", 0.0);
      		  			}
      		  			Double ul=(Double)((Map<Object,Object>)ls1.get(0)).get("ul");
      		  			if(ul !=null)
      		  			{
      		  				jsonObj.put("ul", ul);
      		  			}
      		  			else{
      		  			jsonObj.put("ul", 0.0);
      		  			}
      		  			transport=(Double)((Map<Object,Object>)ls1.get(0)).get("transport");
      		  			if(transport !=null)
      		  			{
      		  				jsonObj.put("transport", transport);
      		  			}
      		  			else{
      		  				jsonObj.put("transport", 0.0);
      		  			}
    		  			Double harvesting=(Double)((Map<Object,Object>)ls1.get(0)).get("harvesting");
    		  			if(harvesting !=null)
      		  			{
    		  				jsonObj.put("harvesting", harvesting);
      		  			}
    		  			else{
    		  				jsonObj.put("harvesting", 0.0);
    		  			}
    		  			Double otherded=(Double)((Map<Object,Object>)ls1.get(0)).get("otherded");
    		  			if(otherded !=null)
      		  			{
    		  				jsonObj.put("otherded", otherded);
      		  			}else
      		  			{
      		  				jsonObj.put("otherded", 0.0);
      		  			}
      		  			Double sbacTransfer=(Double)((Map<Object,Object>)ls1.get(0)).get("sbac");
	      		  		if(sbacTransfer !=null)
	  		  			{
	      		  			jsonObj.put("sbacTransfer", sbacTransfer);
	      		  			tfr += sbacTransfer;
	  		  			}else
	  		  			{
	  		  				jsonObj.put("sbacTransfer", 0.0);
	  		  			}
      		  			sbpaidamt=(Double)((Map<Object,Object>)ls1.get(0)).get("paidamt");
	      		  		if(sbpaidamt !=null)
	  		  			{
	      		  			jsonObj.put("sbpaidamt", sbpaidamt);
	      		  			sbpaid +=sbpaidamt;
	  		  			}else{
	  		  				jsonObj.put("sbpaidamt", 0.0);
	  		  			}
      		  		}
      		  		String	sql2=" SELECT sum(disbursedamount) disbursedamount  FROM LoanDetails where ryotcode='"+ryotcode+"' and season='"+season+"' and disburseddate  IS NOT NULL  ";
    		  		List ls2=hibernatedao.findBySqlCriteria(sql2);
    		  		if(ls2.size()>0 && ls2!=null)
    		  		{
    		  			Double disbursedamount=(Double)((Map<Object,Object>)ls2.get(0)).get("disbursedamount");
    		  			if(disbursedamount !=null)
    		  			{
    		  				jsonObj.put("Lprinciple", disbursedamount);	
    		  			}
    		  			else
    		  				jsonObj.put("Lprinciple", 0.0);	
    		  			
    		  		}
    		  		String	sql3="  select sum(finalinterest) finalinterest, sum(paidamt) paidamt, sum(advisedamt) advisedamt from CanAccLoansDetails where advloan=1 and ryotcode='"+ryotcode+"' and season='"+season+"' ";
    		  		List ls3=hibernatedao.findBySqlCriteria(sql3);
    		  		Double advisedamt=0.0;
    		  		Double paidamt=0.0;
    		  		if(ls3.size()>0 && ls3!=null)
    		  		{
    		  			Double finalinterest=(Double)((Map<Object,Object>)ls3.get(0)).get("finalinterest");
    		  			if(finalinterest!=null)
    		  			{
    		  				jsonObj.put("finalinterest", finalinterest);				
    		  			}
    		  			else
        		  		{
        		  			Date principledate = null;
        		  			int branchcode = 0;
        		  			int loanno=0;
        		  			Double Principle=0.0;
        		  			double months=0.00;
        		  			String	sql14="select branchcode,loannumber,principle,principledate from LoanPrincipleAmounts where ryotcode='"+ryotcode+"' and season='"+season+"'";
            		  		List ls14=hibernatedao.findBySqlCriteria(sql14);
            		  		if(ls14.size()>0 && ls14!=null)
            		  		{
            		  			Principle=(Double)((Map<Object,Object>)ls14.get(0)).get("principle");
            		  			principledate=(Date)((Map<Object,Object>)ls14.get(0)).get("principledate");
            		  			branchcode=(Integer)((Map<Object,Object>)ls14.get(0)).get("branchcode");
            		  			loanno=(Integer)((Map<Object,Object>)ls14.get(0)).get("loannumber");
            		  			double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotcode,season,branchcode,loanno);
            		  			long earliertime=principledate.getTime();
            		  			Calendar cal = Calendar.getInstance();
          	  					
          	  					Long latertime= cal.getTimeInMillis();
          	  					long days=DateUtils.dateDifference(latertime, earliertime);
          	  					days = Math.abs(days);
          	  					Long ldays = new Long(days);
          	  					double dDays = ldays.doubleValue();
          	  					if(previousIntrest == 0)
          	  					{
          	  						dDays = dDays+1;
          	  					}
          	  					months=dDays/30;
          	  					months = Math.round(months * 100.0) / 100.0;							
          	  					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(Principle,branchcode,season);
          	  					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

          	  					if(interestRate==null)
          	  						interestRate=0.00;
        			
          	  					if(Principle==null)
          	  						Principle=0.00;
        			
          	  					double intrate = interestRate/100;
        					
          	  					double intrestPerDay = 	Principle*intrate/365;
          	  					double intrestAmt = intrestPerDay*dDays;
          	  					intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;
          	  					
          	  				jsonObj.put("finalinterest", intrestAmt);	
        		  			//jsonObj.put("paidamt", "0.0");
        		  			//jsonObj.put("advisedamt", "0.0");
            		  		}
            		  		else
            		  		{
            		  			jsonObj.put("finalinterest", 0.0);
            		  		}
        		  			//Get principledate,branchcode,loanno,Principle Amount from LoanPrincipleAmounts table
        		  			//List != null and size>0
        		  			//get all the parameters above what we mentioned and calculate intrest
        		  	  		//List loanList=caneAccountingFunctionalService.getLoanDetails(season1[1],ryotcode);
        		  		}
    		  			paidamt=(Double)((Map<Object,Object>)ls3.get(0)).get("paidamt");
    		  			if(paidamt!=null)
    		  			{
    		  				jsonObj.put("paidamt", paidamt);
    		  				tfr += paidamt;
    		  			}
    		  			else
        		  		{
        		  			jsonObj.put("paidamt", 0.0);
        		  		}
    		  			advisedamt=(Double)((Map<Object,Object>)ls3.get(0)).get("advisedamt");
    		  			if(advisedamt!=null)
    		  			{
    		  				jsonObj.put("advisedamt", advisedamt);
    		  				sbpaid +=advisedamt;
    		  			}
    		  			else
        		  		{
        		  			jsonObj.put("advisedamt", 0.0);
        		  		}
    		  		}
    		  		else
    		  		{
    		  			Date principledate = null;
    		  			int branchcode = 0;
    		  			int loanno=0;
    		  			Double Principle=0.0;
    		  			double months=0.00;
    		  			String	sql14="select branchcode,loannumber,principle,principledate from LoanPrincipleAmounts where advloan=0 and ryotcode='"+ryotcode+"' and season='"+season+"'";
        		  		List ls14=hibernatedao.findBySqlCriteria(sql14);
        		  		if(ls14.size()>0 && ls14!=null)
        		  		{
        		  			Principle=(Double)((Map<Object,Object>)ls14.get(0)).get("principle");
        		  			principledate=(Date)((Map<Object,Object>)ls14.get(0)).get("principledate");
        		  			branchcode=(Integer)((Map<Object,Object>)ls14.get(0)).get("branchcode");
        		  			loanno=(Integer)((Map<Object,Object>)ls14.get(0)).get("loannumber");
        		  			double previousIntrest = caneAccountingFunctionalService.getPreviousIntrestAmt(ryotcode,season,branchcode,loanno);
        		  			long earliertime=principledate.getTime();
        		  			Calendar cal = Calendar.getInstance();
      	  					
      	  					Long latertime= cal.getTimeInMillis();
      	  					long days=DateUtils.dateDifference(latertime, earliertime);
      	  					days = Math.abs(days);
      	  					Long ldays = new Long(days);
      	  					double dDays = ldays.doubleValue();
      	  					if(previousIntrest == 0)
      	  					{
      	  						dDays = dDays+1;
      	  					}
      	  					months=dDays/30;
      	  					months = Math.round(months * 100.0) / 100.0;							
      	  					Double interestRate=(Double)caneAccountingFunctionalService.getInterestForLoan(Principle,branchcode,season);
      	  					logger.info("getInterestForLoan()------ Loan Intrest "+interestRate);

      	  					if(interestRate==null)
      	  						interestRate=0.00;
    			
      	  					if(Principle==null)
      	  						Principle=0.00;
    			
      	  					double intrate = interestRate/100;
    					
      	  					double intrestPerDay = 	Principle*intrate/365;
      	  					double intrestAmt = intrestPerDay*dDays;
      	  					intrestAmt = Math.round(intrestAmt * 100.0) / 100.0;
      	  					
      	  				jsonObj.put("finalinterest", intrestAmt);	
    		  			jsonObj.put("paidamt", "0.0");
    		  			jsonObj.put("advisedamt", "0.0");
        		  		}
    		  			//Get principledate,branchcode,loanno,Principle Amount from LoanPrincipleAmounts table
    		  			//List != null and size>0
    		  			//get all the parameters above what we mentioned and calculate intrest
    		  	  		//List loanList=caneAccountingFunctionalService.getLoanDetails(season1[1],ryotcode);
    		  			
    		  		}
    		  		String	sql4="select isnull( sum(paidamt),0) advance from CanAccLoansDetails where advloan=0 and ryotcode='"+ryotcode+"' and season='"+season+"'";
    		  		List ls4=hibernatedao.findBySqlCriteria(sql4);
    		  		if(ls4.size()>0 && ls4!=null)
    		  		{
    		  			Double advance=(Double)((Map<Object,Object>)ls4.get(0)).get("advance");
    		  			jsonObj.put("advance", advance);
    		  			if(advance==0.0)
    		  			{
    		  				String	sql14="select  sum(principle) advance from AdvancePrincipleAmounts where  ryotcode='"+ryotcode+"' and season='"+season+"'";
            		  		List ls14=hibernatedao.findBySqlCriteria(sql14);
            		  		if(ls14.size()>0 && ls14.get(0)!=null)
            		  		{
            		  			Double advance1=(Double)((Map<Object,Object>)ls14.get(0)).get("advance");
            		  			jsonObj.put("advance", advance1);
            		  			
            		  		}
            		  		else
            		  		{
            		  			jsonObj.put("advance", 0.0);
            		  		}
    		  				
    		  			}
    		  		}
    		  		else
    		  		{
    		  			String	sql14="select  sum(principle) advance from AdvancePrincipleAmounts where  ryotcode='"+ryotcode+"' and season='"+season+"'";
        		  		List ls14=hibernatedao.findBySqlCriteria(sql14);
        		  		if(ls14.size()>0 && ls14!=null)
        		  		{
        		  			Double advance=(Double)((Map<Object,Object>)ls14.get(0)).get("advance");
        		  			jsonObj.put("advance", advance);
        		  			
        		  		}
    		  			
    		  		}
    		  		String	sql5=" SELECT distinct c.circle,z.zone,v.village FROM AgreementDetails w ,circle c,zone z,village v where  w.ryotcode='"+ryotcode+"' and seasonyear='"+season+"' and w.circlecode=c.circlecode and w.zonecode=z.zonecode and w.villagecode=v.villagecode ";
    		  		List ls5=hibernatedao.findBySqlCriteria(sql5);
    		  		if(ls5.size()>0 && ls5!=null)
    		  		{
    		  			String circle=(String)((Map<Object,Object>)ls5.get(0)).get("circle");
    		  			String zone=(String)((Map<Object,Object>)ls5.get(0)).get("zone");
    		  			String village=(String)((Map<Object,Object>)ls5.get(0)).get("village");
    		  			jsonObj.put("zone", zone);
          				jsonObj.put("circle", circle);
          				jsonObj.put("village", village);
    		  		}
    		  		
    		  		List sourceList=new ArrayList();
    		  		String	sql6=" select distinct  paymentsource from CanAccLoansDetails where advloan=1 and ryotcode='"+ryotcode+"' and season='"+season+"' and paidamt>0 ";
    		  		List ls6=hibernatedao.findBySqlCriteria(sql6);
    		  		if(ls6!=null && ls6.size()>0 )
    		  		{
    		  			for(int j=0;j<ls6.size();j++)
    		  			{
    		  				String paymentsource=(String)((Map<Object,Object>)ls6.get(j)).get("paymentsource");
    		  				Map tempMap=new HashMap();
    		  				if(paymentsource!=null)
    		  				{
    		  					tempMap.put("Source", paymentsource);
    		  					sourceList.add(tempMap);
    		  				}
    		  			}
    		  		}
    		  		String	sql7=" select distinct paymentsource from CanAccSBDetails where  ryotcode='"+ryotcode+"' and season='"+season+"' and paidamt>0 ";
    		  		List ls7=hibernatedao.findBySqlCriteria(sql7);
    		  		if( ls7!=null && ls7.size()>0 )
    		  		{
    		  			for(int j=0;j<ls7.size();j++)
    		  			{
    		  				String paymentsource=(String)((Map<Object,Object>)ls7.get(j)).get("paymentsource");
    		  				Map tempMap=new HashMap();
    		  				if(paymentsource!=null)
    		  				{
    		  					tempMap.put("Source", paymentsource);
    		  					boolean isAcctrue = sourceList.contains(tempMap);
    							if(isAcctrue == false)
    							{
    								sourceList.add(tempMap);
    							}
    		  				}					
    		  			}
    		  		}
    		  		String paySource = "";
    		  		if(sourceList != null && sourceList.size()>0)
    		  		{
    		  			for(int j=0;j<sourceList.size();j++)
    		  			{
    		  				String paymentsource=(String)((Map<Object,Object>)sourceList.get(j)).get("Source");
    		  				paySource += paymentsource+",";
    		  			}
    		  			paySource = paySource.substring(0, paySource.length()-1);
    		  		}
      				jsonObj.put("Payamantresourcebank", paySource);

    		  		
    		  		balancedue=(tfr)-(sbpaid);
    		  		if(balancedue>0)
    		  		{
          				jsonObj.put("balancedue", balancedue);
    		  		}
    		  		else
    		  		{
      				jsonObj.put("balancedue", "0.0");
    		  		}
    				jArray.add(jsonObj);
      			}
      		}
      	return jArray;		
	}

		//Added by DMurty on 16-02-2017
		@RequestMapping(value = "/migratePincodeInVillageMaster", method = RequestMethod.POST)
		public @ResponseBody
		boolean migratePincodeInVillageMaster(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List UpdateList = new ArrayList();
			List entityList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			try
			{
			List villagePincodeList = ahFuctionalService.getPincodeDataFromTemp();
			if(villagePincodeList != null && villagePincodeList.size()>0)
			{
				for (int j = 0; j < villagePincodeList.size(); j++)
				{
					Map VMap=(Map)villagePincodeList.get(j);
					
					String villageCode = (String) VMap.get("villagecode");
					String pincode = (String) VMap.get("pincode");
					strQry = "Update Village set pincode="+pincode+" where villagecode='"+villageCode+"'";
					logger.info("======== Update Village Amt qry=========="+strQry);
					Qryobj = strQry;
					UpdateList.add(Qryobj);
				}
			}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);	
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return isInsertSuccess;
			}
			return isInsertSuccess;
		}
		//Added by naidu Migration For already added SeedSupplier Accounts.
		//Added by DMurty on 16-02-2017
		/*@RequestMapping(value = "/migrateAccountsForSeedSupplierPerSeason", method = RequestMethod.POST)
		public @ResponseBody
		boolean migrateAccountsForSeedSupplierPerSeason(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List UpdateList = new ArrayList();
			List UpdateList1 = new ArrayList();
			List entityList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry7 = null;
			Object Qryobj7 = null;
			AccountDetails accd=null;
			int transactionCode=0;
			boolean isInsertSuccess2  = false;
			boolean isInsertSuccess1  = false;
			try
			{
			List seedSupAccList = ahFuctionalService.getAccountDetailsForSeedSuuplier("2017-2018","SEEDPUR");
			if(seedSupAccList != null && seedSupAccList.size()>0)
			{ 
				for (int j = 0; j < seedSupAccList.size(); j++)
				{
				transactionCode = commonService.GetMaxTransCode("2016-2017");
				
				accd=(AccountDetails)seedSupAccList.get(j);
				String strQry = "Update AccountGroupDetails set season='2016-2017',transactioncode="+transactionCode+" where season='"+accd.getSeason()+"' and transactioncode="+accd.getTransactioncode()+"";
				logger.info("======== Update AccountGroupDetails Amt qry=========="+strQry);
				
				Object Qryobj = strQry;
				UpdateList1.add(Qryobj);
				String strQry1 = "Update AccountSubGroupDetails set season='2016-2017',transactioncode="+transactionCode+"  where season='"+accd.getSeason()+"' and transactioncode="+accd.getTransactioncode()+"";
				logger.info("======== Update AccountSubGroupDetails Amt qry=========="+strQry1);
				Object Qryobj1 = strQry1;
				UpdateList1.add(Qryobj1);
				String strQry2 = "Update AccountDetails set season='2016-2017',transactioncode="+transactionCode+"  where season='"+accd.getSeason()+"' and transactioncode="+accd.getTransactioncode()+"";
				logger.info("======== Update AccountDetails Amt qry=========="+strQry2);
				Object Qryobj2 = strQry2;
				UpdateList1.add(Qryobj2);
				}
			}
				
				
				isInsertSuccess1 = commonService.saveMultipleEntitiesForFunctionalScreens(entityList,UpdateList1);
				
				if(isInsertSuccess1)
				{
					
					List seedSupAccList11 = ahFuctionalService.getDistinctAccountsForSeedSuuplier("2016-2017","SEEDPUR");	
					if(seedSupAccList11 != null && seedSupAccList11.size()>0)
					{ 
						for (int j = 0; j < seedSupAccList11.size(); j++)
						{
							String acccode=(String)((Map<Object,Object>)seedSupAccList11.get(j)).get("accountcode");
							
							List llseedpur1=ahFuctionalService.getRunningBalForAccount(acccode,"2016-2017");
							if(llseedpur1.size()>0 && llseedpur1 != null && (Double)((Map<Object,Object>)llseedpur1.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur1.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur1.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur1.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								String qry11 =null;
								List checkAccsmry=ahFuctionalService.getRecordinAccSmry(acccode,"2016-2017");
								if(checkAccsmry.size()>0 &&  checkAccsmry!=null && !checkAccsmry.isEmpty())
								{
								qry11 = "Update AccountSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountcode='"+acccode+"' and season='2016-2017'";
								}
								else
								{
									qry11 = "insert  into AccountSummary(accountcode,balancetype,runningbalance,season) values('"+acccode+"','"+balType+"',"+runBal+",'2016-2017')";
								}
								Object Qryobj11 = qry11;
								UpdateList.add(Qryobj11);
							}
							int AccGroupCode1=0;
							int AccSubGroupCode1=0;
							List CodeList = commonService.getAccCodes(acccode);
							if (CodeList != null && CodeList.size() > 0)
							{
								Object[] myResult = (Object[]) CodeList.get(0);
								AccGroupCode1 = (Integer) myResult[0];
								AccSubGroupCode1 = (Integer) myResult[1];
							}
							
							List llseedpur2=ahFuctionalService.getRunningBalForAccountGrp(AccGroupCode1,"2016-2017"); 
							if(llseedpur2.size()>0 && llseedpur2 != null && (Double)((Map<Object,Object>)llseedpur2.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur2.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur2.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur2.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								List checkAccGrpsmry=ahFuctionalService.getAccGrpDetailsFromSummary(AccGroupCode1,"2016-2017");
								String qry12 ="";
								if(checkAccGrpsmry.size()>0 && checkAccGrpsmry!=null && (Integer)((Map<Object,Object>)checkAccGrpsmry.get(0)).get("accountgroupcode")!=null  && !checkAccGrpsmry.isEmpty())
								{
								qry12 = "Update AccountGroupSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountgroupcode='" + AccGroupCode1 + "' and season='2016-2017'";
								}
								else
								{
									qry12 = "insert  into AccountGroupSummary(accountgroupcode,balancetype,runningbalance,season) values("+AccGroupCode1+",'"+balType+"',"+runBal+",'2016-2017')";	
								}
								Object Qryobj12 = qry12;
								UpdateList.add(Qryobj12);
							}
							List llseedpur3=ahFuctionalService.getRunningBalForAccountSubGrp(AccSubGroupCode1,"2016-2017");
							if(llseedpur3.size()>0 && llseedpur3 != null && (Double)((Map<Object,Object>)llseedpur3.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur3.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur3.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur3.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								List checkAccSubGrpsmry=ahFuctionalService.getAccSubGrpDtls(AccGroupCode1,AccSubGroupCode1,"2016-2017");
								String qry13 ="";
								if(checkAccSubGrpsmry.size()>0 && checkAccSubGrpsmry!=null && (String)((Map<Object,Object>)checkAccSubGrpsmry.get(0)).get("season")!=null  && !checkAccSubGrpsmry.isEmpty())
								{
								qry13 = "Update AccountSubGroupSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountgroupcode='" + AccGroupCode1 + "' and accountsubgroupcode="
						        + AccSubGroupCode1 + " and season='2016-2017'";
								}
								else
								{
								qry13 = "insert  into AccountSubGroupSummary(accountgroupcode,accountsubgroupcode,balancetype,runningbalance,season) values("+AccGroupCode1+","+AccSubGroupCode1+",'"+balType+"',"+runBal+",'2016-2017')";
								}
								Object Qryobj13 = qry13;
								UpdateList.add(Qryobj13);
							}
						}
						
				}//For Season 16-17Summary Posting
						
						
				List checkAccsmry=ahFuctionalService.getRecordinAccSmry("SEEDPUR","2017-2018");
				if(checkAccsmry.size()>0 && checkAccsmry != null && !checkAccsmry.isEmpty())
				{
					AccountSummary ass=(AccountSummary)checkAccsmry.get(0);
					int AccGroupCode1=0;
					int AccSubGroupCode1=0;
					List CodeList = commonService.getAccCodes(ass.getAccountcode());
					if (CodeList != null && CodeList.size() > 0)
					{
						Object[] myResult = (Object[]) CodeList.get(0);
						AccGroupCode1 = (Integer) myResult[0];
						AccSubGroupCode1 = (Integer) myResult[1];
					}
					String strQry1 = "Update AccountSummary set season='2016-2017' where season='"+ass.getSeason()+"' and accountcode='"+ass.getAccountcode()+"'";
					logger.info("======== Update AccountSummary Amt qry=========="+strQry1);
					Object Qryobj1 = strQry1;
					UpdateList1.add(Qryobj1);
					String strQry2 = "Update AccountGroupSummary set season='2016-2017'  where season='"+ass.getSeason()+"' and accountgroupcode="+AccGroupCode1+"";
					logger.info("======== Update AccountGroupSummary Amt qry=========="+strQry2);
					Object Qryobj2 = strQry2;
					UpdateList1.add(Qryobj2);
					String strQry3 = "Update AccountSubGroupSummary set season='2016-2017' where season='"+ass.getSeason()+"' and accountgroupcode="+ AccGroupCode1 +" and accountsubgroupcode="+ AccSubGroupCode1 +"";
					logger.info("======== Update AccountSubGroupSummary Amt qry=========="+strQry3);
					Object Qryobj3 = strQry3;
					UpdateList1.add(Qryobj3);
				
				}
				
					//For 2017-2018 Season Accounting reverting for posting supplier into 2016-2017 Season
					

					List seedSupAccList12 = ahFuctionalService.getDistinctAccountsForSeedSuuplier("2016-2017","SEEDPUR");	
					if(seedSupAccList12 != null && seedSupAccList12.size()>0)
					{ 
						for (int j = 0; j < seedSupAccList12.size(); j++)
						{
							String acccode=(String)((Map<Object,Object>)seedSupAccList12.get(j)).get("accountcode");
							
							List llseedpur1=ahFuctionalService.getRunningBalForAccount(acccode,"2017-2018");
							if(llseedpur1.size()>0 && llseedpur1 != null && (Double)((Map<Object,Object>)llseedpur1.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur1.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur1.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur1.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								String qry11 = "Update AccountSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountcode='"+acccode+"' and season='2017-2018'";
								Object Qryobj11 = qry11;
								UpdateList.add(Qryobj11);
							}
							else
							{
								String qry11 = "Update AccountSummary set runningbalance=0.0,balancetype='Cr' where accountcode='"+acccode+"' and season='2017-2018'";
								Object Qryobj11 = qry11;
								UpdateList.add(Qryobj11);
							}
							int AccGroupCode1=0;
							int AccSubGroupCode1=0;
							List CodeList = commonService.getAccCodes(acccode);
							if (CodeList != null && CodeList.size() > 0)
							{
								Object[] myResult = (Object[]) CodeList.get(0);
								AccGroupCode1 = (Integer) myResult[0];
								AccSubGroupCode1 = (Integer) myResult[1];
							}
							
							List llseedpur2=ahFuctionalService.getRunningBalForAccountGrp(AccGroupCode1,"2017-2018"); 
							if(llseedpur2.size()>0 && llseedpur2 != null && (Double)((Map<Object,Object>)llseedpur2.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur2.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur2.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur2.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								String qry12 = "Update AccountGroupSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountgroupcode='" + AccGroupCode1 + "' and season='2017-2018'";
						
								Object Qryobj12 = qry12;
								UpdateList.add(Qryobj12);
							}
							else
							{
								String qry12 = "Update AccountGroupSummary set runningbalance=0.0,balancetype='Cr' where accountgroupcode='" + AccGroupCode1 + "' and season='2017-2018'";
								Object Qryobj12 = qry12;
								UpdateList.add(Qryobj12);
							}
							
							List llseedpur3=ahFuctionalService.getRunningBalForAccountSubGrp(AccSubGroupCode1,"2017-2018");
							if(llseedpur3.size()>0 && llseedpur3 != null && (Double)((Map<Object,Object>)llseedpur3.get(0)).get("cr")!=null && (Double)((Map<Object,Object>)llseedpur3.get(0)).get("dr")!=null)
							{
								double prCr=(Double)((Map<Object,Object>)llseedpur3.get(0)).get("cr");
								double prDr=(Double)((Map<Object,Object>)llseedpur3.get(0)).get("dr");
								String balType="";
								double runBal=0.0;
								if(prCr>prDr)
								{
									balType = "Cr";
									runBal = prCr-prDr;
								}
								else
								{
									runBal = prDr-prCr;
									balType = "Dr";
								}
								String qry13 = "Update AccountSubGroupSummary set runningbalance=" + runBal + ",balancetype='" + balType
						        + "' where accountgroupcode='" + AccGroupCode1 + "' and accountsubgroupcode="
						        + AccSubGroupCode1 + " and season='2017-2018'";
								Object Qryobj13 = qry13;
								UpdateList.add(Qryobj13);
							}
							else
							{
								String qry13 = "Update AccountSubGroupSummary set runningbalance=0.0,balancetype='Cr' where accountgroupcode='" + AccGroupCode1 + "' and accountsubgroupcode="+ AccSubGroupCode1 + " and season='2017-2018'";
								Object Qryobj13 = qry13;
								UpdateList.add(Qryobj13);
							}
						}
						
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
			}
				
			if(isInsertSuccess==true && isInsertSuccess1==true)
			{
				isInsertSuccess2=true;
				return isInsertSuccess2;
			}
			else
				return isInsertSuccess2;
					
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return isInsertSuccess2;
			}
		}*/
		@RequestMapping(value = "/migrateBatchesForSeedSupplierPerSeason", method = RequestMethod.POST)
		public @ResponseBody
		boolean migrateBatchesForSeedSupplierPerSeason(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List UpdateList = new ArrayList();
			List entityList = new ArrayList();
			try
			{
				Temp_BatchServiceMaster bsm=null;
				BatchServicesMaster bsm1=null;
			List seedSupAccList = ahFuctionalService.getBatchServicemasterDataForMig("2017-2018");
			if(seedSupAccList != null && seedSupAccList.size()>0)
			{ 
				for (int i = 0; i < seedSupAccList.size(); i++)
				{
					bsm=(Temp_BatchServiceMaster)seedSupAccList.get(i);
					bsm1=new BatchServicesMaster();
					String strseedseqcount = Integer.toString(bsm.getId());
					if(strseedseqcount.length() == 2)
					{
						strseedseqcount = "0"+strseedseqcount;
					}
					else if(strseedseqcount.length() == 1)
					{
						strseedseqcount = "00"+strseedseqcount;
					}
					else 
					{
						strseedseqcount = strseedseqcount;
					}
					bsm1.setBatchseries("S1718"+strseedseqcount);
					bsm1.setIsbatchprepared(bsm.getIsbatchprepared());
					bsm1.setLandvillagecode(bsm.getLandvillagecode());
					bsm1.setRunningyear(bsm.getRunningyear());
					bsm1.setSeason(bsm.getSeason());
					bsm1.setSeedsuppliercode(bsm.getSeedsuppliercode());
					bsm1.setSeedType(bsm.getSeedType());
					bsm1.setSeqno(bsm.getId());
					bsm1.setVariety(bsm.getVariety());
					bsm1.setVarietycode(bsm.getVarietycode());
					entityList.add(bsm1);
					
				}
				
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
			return isInsertSuccess;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return isInsertSuccess;
			}
		}
		@RequestMapping(value = "/migrateBatchesInSeedSourceRecords", method = RequestMethod.POST)
		public @ResponseBody
		boolean migrateBatchesInSeedSourceRecords(@RequestBody String jsonData) throws Exception 
		{
			boolean isInsertSuccess  = false;
			List UpdateList = new ArrayList();
			List entityList = new ArrayList();
			try
			{
				Temp_BatchServiceMaster bsm=null;
				BatchServicesMaster bsm1=null;
				SeedSource ss1=null;
				Map hmp=null;
			List seedSupAccList = ahFuctionalService.getSuppliersFromBatchSeries("2017-2018");
			if(seedSupAccList != null && seedSupAccList.size()>0)
			{ 
				for (int i = 0; i < seedSupAccList.size(); i++)
				{
					String supcode=(String)((Map<Object,Object>)seedSupAccList.get(i)).get("seedsuppliercode");
					List seedSupBatch = ahFuctionalService.getBatchForSupplier(supcode);
					if(seedSupBatch != null && seedSupBatch.size()>0)
					{ 
						hmp=new HashMap();
						for (int j = 0; j < seedSupBatch.size(); j++)
						{
							String bseries=(String)((Map<Object,Object>)seedSupBatch.get(j)).get("batchseries");
							hmp.put(j, bseries);
						}
					}
					
					List seedSupBatchForSourceOfSeed = ahFuctionalService.listSourceSeedsForBatch(supcode,"2017-2018");
					if(seedSupBatchForSourceOfSeed != null && seedSupBatchForSourceOfSeed.size()>0)
					{ 
						for (int k = 0; k < seedSupBatchForSourceOfSeed.size(); k++)
						{
							ss1=(SeedSource)seedSupBatchForSourceOfSeed.get(k);
							if(hmp.size()==seedSupBatchForSourceOfSeed.size())
								ss1.setBatchno((String)hmp.get(k));
							else
								ss1.setBatchno("");
							entityList.add(ss1);
					
						}
					}
					
				}
				
			}
			isInsertSuccess = commonService.saveMultipleEntitiesForScreensUpdateFirst(entityList,UpdateList);
			return isInsertSuccess;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return isInsertSuccess;
			}
		}
		//Naidu on 26-04-2017
		//ReUpdation Cr amt FOr Supplier where supllier and consumer has same 
		//Added by DMurty on 13-02-2017
		@RequestMapping(value = "/updateSeedSupplierAccountsToCr", method = RequestMethod.POST)
		public @ResponseBody
		boolean updateSeedSupplierAccountsToCr(@RequestBody JSONObject jsonData) throws Exception 
		{
			String season=(String)jsonData.get("dropdownname");
			boolean isInsertSuccess  = false;
			List entityList = new ArrayList();
			List UpdateList = new ArrayList();
			List AccountsList = new ArrayList();
			String strQry = null;
			Object Qryobj = null;
			HashMap hm= null;
			List AccList = new ArrayList();

			List DedList = ahFuctionalService.getSeedSupplierAccounts(season);
			if(DedList != null && DedList.size()>0)
			{
				for (int j = 0; j < DedList.size(); j++)
				{
					hm= new HashMap();
					Map VMap=(Map)DedList.get(j);
					String ryotCode = (String) VMap.get("ryotcode");
					int transactionCode = (Integer) VMap.get("transactioncode");
					String ses = (String) VMap.get("season");
					int id = (Integer) VMap.get("id");
					double advanceamount = (Double) VMap.get("advanceamount");
					

					Map AccMap = new HashMap();
					AccMap.put("ACCOUNT_CODE", ryotCode);
					
					boolean isAcctrue = AccList.contains(AccMap);
					if(isAcctrue == false)
					{
						AccList.add(AccMap);
					}
					
					strQry = "Update AccountDetails set cr="+advanceamount+",runningbalance="+advanceamount+" where accountcode='"+ryotCode+"' and season='"+season+"' and glcode='"+ryotCode+"' and transactioncode="+transactionCode;
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
			    	
			    	strQry = "Update AccountGroupDetails set cr="+advanceamount+",runningbalance="+advanceamount+" where season='"+season+"' and transactioncode="+transactionCode+" and accountgroupcode=2";
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
			    	
			    	strQry = "Update AccountSubGroupDetails set cr="+advanceamount+",runningbalance="+advanceamount+" where season='"+season+"' and transactioncode="+transactionCode+" and accountgroupcode=2 and accountsubgroupcode=2";
					Qryobj = strQry;
			    	UpdateList.add(Qryobj);
				}
				isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
				if(isInsertSuccess == true)
				{
					UpdateList.clear();
					//AccountSummary
					if(AccList != null && AccList.size()>0)
					{
						for (int i = 0; i < AccList.size(); i++)
						{
							Map AccMap = new HashMap();
							AccMap=(Map)AccList.get(i);
							
							String accCode=(String)AccMap.get("ACCOUNT_CODE");
							double cr = 0.0;
							double dr = 0.0;
							List BalList = ahFuctionalService.getRunningBalForAccount(accCode,season);
							if(BalList != null && BalList.size()>0)
							{
								Map AccSmryMap = new HashMap();
								AccSmryMap=(Map)BalList.get(0);
								
								cr=(Double)AccSmryMap.get("cr");
								dr=(Double)AccSmryMap.get("dr");
								double runBal = 0.0;
								String balType = "";
								if(cr>dr)
								{
									runBal = cr-dr;
									balType = "Cr";
								}
								else
								{
									runBal = dr-cr;
									balType = "Dr";
								}
								
						    	strQry = "Update AccountSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountcode='"+accCode+"' and season='"+season+"'";
						    	Qryobj = strQry;
						    	UpdateList.add(Qryobj);
							}
						}
					}
					
					//AccountGroupSummary
					int accountgroupcode=2;
					double cr = 0.0;
					double dr = 0.0;
					List AccGrpList = ahFuctionalService.getRunningBalForAccountGrp(accountgroupcode,season);
					if(AccGrpList != null && AccGrpList.size()>0)
					{
						Map AccGrpSmryMap = new HashMap();
						AccGrpSmryMap=(Map)AccGrpList.get(0);
						
						cr=(Double)AccGrpSmryMap.get("cr");
						dr=(Double)AccGrpSmryMap.get("dr");
						double runBal = 0.0;
						String balType = "";
						if(cr>dr)
						{
							runBal = cr-dr;
							balType = "Cr";
						}
						else
						{
							runBal = dr-cr;
							balType = "Dr";
						}
				    	strQry = "Update AccountGroupSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountgroupcode="+accountgroupcode+" and season='"+season+"'";
				    	Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					
					//AccountSubGroupSummary
					int accountsubgroupcode=2;
					cr = 0.0;
					dr = 0.0;
					List AccSubGrpList = ahFuctionalService.getRunningBalForAccountSubGrp(accountgroupcode,season);
					if(AccSubGrpList != null && AccSubGrpList.size()>0)
					{
						Map AccSubGrpSmryMap = new HashMap();
						AccSubGrpSmryMap=(Map)AccSubGrpList.get(0);
						
						cr=(Double)AccSubGrpSmryMap.get("cr");
						dr=(Double)AccSubGrpSmryMap.get("dr");
						double runBal = 0.0;
						String balType = "";
						if(cr>dr)
						{
							runBal = cr-dr;
							balType = "Cr";
						}
						else
						{
							runBal = dr-cr;
							balType = "Dr";
						}
				    	strQry = "Update AccountSubGroupSummary set runningbalance="+runBal+",balancetype='"+balType+"' where accountsubgroupcode="+accountsubgroupcode+" and season='"+season+"'";
				    	Qryobj = strQry;
				    	UpdateList.add(Qryobj);
					}
					isInsertSuccess = commonService.saveMultipleEntitiesForFunctionalScreensForAccounting(entityList,UpdateList,AccountsList);
				}
			}
			else
			{
				isInsertSuccess = true;
			}
			return isInsertSuccess;
		}
}
