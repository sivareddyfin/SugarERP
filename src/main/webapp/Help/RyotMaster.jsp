<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-30T11:33:34.158000000"/>
	<meta name="changed" content="2016-06-02T15:41:51.342000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
	</style>
</head>
<body lang="en-US" dir="ltr">
<ul>
	<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	</p>
	<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	</p>
	<ul>
		<ul>
			<ul>
				<ul>
					<ul>
						<ul>
							<ul>
								<ul>
									<h2 class="western"><font color="#0066cc"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>RYOT
									MASTER</b></u></font></font></font></h2>
								</ul>
							</ul>
						</ul>
					</ul>
				</ul>
			</ul>
		</ul>
		<h2 class="western" style="font-weight: normal"></h2>
	</ul>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Ryot
	Master screen is used to add the new ryots by giving the required
	information.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here
	there is a provision to modify already added ryot records.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-006a-24c9-7f0a-a7c94bc02fe7"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">The
	Ryot Code have 8 digits &ndash; Mandal Code(2) + Village Code (2) +
	Ryot Sequence number.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">By
	default ryot code is 00000001.</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">
	After selecting the Mandal and village </span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">the
	code will be generate.</span></span></span></span></font></span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="background: transparent">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-style: normal"><span style="font-weight: normal">Here
	Ryot Sequence number </span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">will
	</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">be
	</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">added</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">
	by 1 </span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">to
	the previous ryot number</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">.</span></span></span></span></font></span><span style="font-weight: normal">
	</span><span style="font-weight: normal">	</span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">By
	default Village and circle fields will not be able to select.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-weight: normal">After
	selecting Mandal, Village and circle fields </span><span style="font-weight: normal">can
	be selected</span><span style="font-weight: normal">.</span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">To
	get Mandal names, village names and circle names in drop down have
	to add names in mandal, village and circle master screens.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here,
	there is a provision to set the status of the ryot as
	Active/Inactive.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Action
	Button is used to add the multiple bank branch for that particular
	ryot.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Can
	add ryot image and aadhar card image by clicking the Select ryot
	image and select aadhar image buttons.</font></font></p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>