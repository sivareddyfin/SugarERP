<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T12:50:43.378000000"/>
	<meta name="changed" content="2016-06-02T15:45:04.446000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">					<font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>SETUP
MASTER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0ada-6b89-2fce-0846f85f3a70"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen is useful to define different system level parameters.</span></span></span></span></font></span>
	</font></font>
	</p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-8cb8a7b7-0adb-2f6b-39bd-d7f783bf0e73"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Different
	parameters will be set in this screen and these values will be used
	in different screen as decision making parameters.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0af3-f2fb-cbf2-77c71c085558"></a><a name="docs-internal-guid-6fef71c0-0af4-2cd4-8160-14a00cc37136"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Lab
	Results Mandatory</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">
	&ndash; </span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	will decide whether soil needs to undergo lab tests or not.</span></span></span></span></font></span>
	</font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Audit
	Trail required &ndash; If yes, asks add/modify records need to be
	tracked or not in audit trail.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><span style="background: transparent">        
	                           <font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">-
	If no, add/modify records options will be hide.</span></font></font></font></p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b47-e373-f7ab-dd1ccf7dbf76"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Modify
	record need to be tracked &ndash; If yes, have to same data need to
	be tracked While updating the record, shall the system needs to
	detail even if the old data is not changed </span></font></font></font>
	</p>
	<li/>
<p style="font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><span style="background: transparent">        
	                                               <font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">-
	If no, same data need to be tracked option will be hide.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b49-5516-f5ab-8743667bedc4"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Deletion
	of records needs to be tracked by Audit Trail.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b49-9929-77ca-02ee07c87167"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Transport
	Subsidy need to posted at the time of Cane Weighment.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0b4a-8e77-e7c9-2ac512ab092f"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Additional
	Cane Price need to be posted at the time of Cane Weighment.</span></font></font></font></p>
</ul>
<p><br/>
<br/>
<br/>

</p>
</body>
</html>