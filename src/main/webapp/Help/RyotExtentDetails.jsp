<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-30T16:48:03.672000000"/>
	<meta name="changed" content="2016-06-02T15:33:05.622000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">                    
            <font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>EXTENT
DETAILS FOR SOIL TEST</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	</p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0166-043e-b981-a572894d215b"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">This
	screen is useful to update the Ryot Extent Details.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Based
	on this extent details soil test will be conducted..</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Based
	on the selected ryot names, ryot code and village will be loaded
	from the respective screens.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">Action
	Button is used to add the multiple </span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">extent
	</span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">details
	of that</span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">
	particular </span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">ryot</span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">.</span></span></span></font></span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">There
	is a provision to add/modify/delete the extent details.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Once
	the soil test results is updated, then there is no chance to modify
	the added extent details.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here
	plot number will start with 1 and incremented by 1.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Variety
	name will be selected from the drop down which are added in the
	variety master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Extent
	size and survey number of the selected ryot have to give manually.</font></font></p>
</ul>
</body>
</html>