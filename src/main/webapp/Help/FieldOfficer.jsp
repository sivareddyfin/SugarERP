<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-31T23:29:20.647000000"/>
	<meta name="changed" content="2016-06-02T15:35:03.269000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h1 { margin-bottom: 0.08in }
		h1.western { font-family: "Liberation Sans", sans-serif; font-size: 18pt }
		h1.cjk { font-family: "Microsoft YaHei"; font-size: 18pt }
		h1.ctl { font-family: "Mangal"; font-size: 18pt }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h1 class="western" align="center" style="line-height: 100%"><font color="#3399ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u>Field
Officer Master</u></font></font></font></h1>
<p align="center" style="line-height: 100%"><br/>
<br/>

</p>
<ul>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Field
	officer id starts with 1 and are added by 1 which is done by the
	system only.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Cane
	managers comes from the cane manager master.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Field
	officers reports to cane manager, so link is given in this screen.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">The
	names in the Field officer drop down comes from the User Master
	screen. By selecting the field officer in the drop down, it means
	that particular user is the field officer of the company.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added field officers are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">By
	clicking on the action button we can modify the given values and
	also make any officer inactive when he is transferred /expired.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">And
	the inactive officer can be seen in only this screen, he does not
	appear else where i.e.,  in Field assistant screen only active field
	officers are displayed.</font></font></font></p>
</ul>
</body>
</html>