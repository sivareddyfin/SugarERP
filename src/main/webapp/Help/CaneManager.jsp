<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-30T14:54:44.474000000"/>
	<meta name="changed" content="2016-06-02T15:36:19.894000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h3.western { font-family: "Liberation Sans", sans-serif; font-size: 14pt }
		h3.cjk { font-family: "Microsoft YaHei"; font-size: 14pt }
		h3.ctl { font-family: "Mangal"; font-size: 14pt }
		h5.western { font-family: "Liberation Sans", sans-serif; font-size: 11pt }
		h5.cjk { font-family: "Microsoft YaHei"; font-size: 11pt }
		h5.ctl { font-family: "Mangal"; font-size: 11pt }
		a:link { so-language: zxx }
	</style>
	<script type="text/javascript">
		function onload()
		{
			//alert(1);
		}
	</script>
</head>
<body lang="en-US" dir="ltr" onLoad="onload();">
<h3 class="western"></h3>
<h5 class="western" style="text-decoration: none">                   
                            <font color="#0066cc"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>GENERATE
AGREEMENT</b></u></font></font></font></h5>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">This
	screen is used to do the agreements with ryot for a particular
	season.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here
	seasons will be displayed in drop down which are added in the season
	master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Agreement
	number is starting with 1 and followed by season i.e.,1/2014-2015.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here
	provision is there to modify the agreement details, by selecting
	season and added agreement.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Agreement
	date is server date.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">To
	select ryot name in drop down, have to add ryot names in ryot master
	screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">After
	selecting ryot name from drop down, the details like salutation,
	ryot code, F/H/G name, circle name and mandal name will get from the
	ryot master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">In
	branch field, branch names will be loaded which are added in the
	bank master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-weight: normal">Here,
	there is a provision to set the status of the </span><span style="font-weight: normal">agreement</span><span style="font-weight: normal">
	as Active/Inactive.</span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Has
	family group radio button gives the information whether the ryot has
	family group or not by selecting Yes/No.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Family
	group names will be loaded from the family group master.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here,
	there is a provision to select surety ryot name.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Action
	Button is used to add the multiple plot details of that particular
	ryot for that agreement.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Here
	plot number will start with 1 and incremented by 1.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Variety
	name will be selected from the drop down which are added in the
	variety master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Plant
	/Ratoon types will select based on the crop type.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">In
	setup master, if &ldquo;lab results mandatory&rdquo; is Yes then
	survey number dialog box will open to select the survey number when
	we click on survey number field.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">If
	No, It will take the input from the user.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Have
	to select land type in the drop down which are added in the land
	type master.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Extent
	image button is used to upload the image of the extent.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">After
	advances and loans are given to ryot then there is no provision to
	modify the agreement details.</font></font></p>
	<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	</p>
</ul>
</body>
</html>