<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T11:04:58.566000000"/>
	<meta name="changed" content="2016-06-02T15:31:54.358000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">				<font size="4" style="font-size: 14pt">	</font><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>DEPARTMENT
MASTER</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-b50e65ce-0a7b-cfb5-ecbe-cc944a27b358"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">This
	screen is useful to add a new department or modify the added
	department.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Department
	id will start with 1 and incremented by 1 by the system
	automatically.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-072d0272-0a7e-b945-e7c5-cdafdd9a8a71"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">All
	added departments will displayed in the grid and should have access
	to modify the values. </span></font></font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Provision
	is there to set the status as Active/Inactive.</span></font></font></font></p>
	<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	</p>
	<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%; text-decoration: none">
	</p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFMAAAAXCAYAAAB3e3N6AAAACXBIWXMAAA7OAAAOnAFG0vohAAAAUElEQVR4nO3QQQkAMAzAwD3q33Jn4mAwcgpCZndPjHkd8JNmQs2Emgk1E2om1EyomVAzoWZCzYSaCTUTaibUTKiZUDOhZkLNhJoJNRNqJnQBINEDWjMlOZ8AAAAASUVORK5CYII=" align="left"/>
<br/>

</p>
</body>
</html>