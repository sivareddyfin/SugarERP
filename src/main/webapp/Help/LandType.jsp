<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-31T22:01:48.776000000"/>
	<meta name="changed" content="2016-06-02T15:37:13.264000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h1 { margin-bottom: 0.08in }
		h1.western { font-family: "Liberation Sans", sans-serif; font-size: 18pt }
		h1.cjk { font-family: "Microsoft YaHei"; font-size: 18pt }
		h1.ctl { font-family: "Mangal"; font-size: 18pt }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h1 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>Masters</b></u></font></font></font></h1>
<p align="left" style="font-weight: normal"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">These
screens are related to master data. These are one time recorded.
These data is used in the transaction screens. All the data once
given cannot be deleted but it can be made inactive.</font></font></font></p>
<p align="left" style="font-weight: normal"><br/>
<br/>

</p>
<h1 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>Land
Type</b></u></font></font></font></h1>
<ul>
	<li/>
<p align="left"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="font-weight: normal">Land
	ID </span></font><font color="#000000"><span style="font-weight: normal">starts
	with 1 </span></font><font color="#000000"><span style="font-weight: normal">and
	is added by 1 </span></font><font color="#000000"><span style="font-weight: normal">done
	by the system </span></font><font color="#000000"><span style="font-weight: normal">on</span></font><font color="#000000"><span style="font-weight: normal">ly</span></font><font color="#000000"><span style="font-weight: normal">.</span></font></font></font></p>
	<li/>
<p align="left"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="font-weight: normal">L</span></font><font color="#000000"><span style="font-weight: normal">and
	can be different types like own or lease land by the ryot. These are
	recorded in this screen and is used in generate agreement.</span></font></font></font></p>
	<li/>
<p align="left" style="font-weight: normal"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added types are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left" style="font-weight: normal"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">When
	we click on the Action button we can modify/change the given values.</font></font></font></p>
</ul>
<p align="left" style="font-weight: normal"><br/>
<br/>

</p>
<p align="left"><br/>
<br/>

</p>
</body>
</html>