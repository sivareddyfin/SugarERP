<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-02T14:51:52.122000000"/>
	<meta name="changed" content="2016-06-02T16:10:45.424000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>PERMIT
RULES</b></u></font></font></font></h2>
<ul>
	<li/>
<p align="left" style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-10b3-0b5a-a22b-b6064366f2b6"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><span style="font-variant: normal"><span style="text-decoration: none"><font face="Calibri"><font size="2" style="font-size: 10pt"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen defines the rules to generate the permits for harvesting
	process.</span></span></span></font></font></span></span></font></font></font></p>
	<p align="left"></p>
</ul>
<p align="left" style="text-decoration: none"><br/>
<br/>

</p>
<p align="left" style="text-decoration: none"><br/>
<br/>

</p>
<p align="center" style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>