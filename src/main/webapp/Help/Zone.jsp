<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-01T15:31:10.226000000"/>
	<meta name="changed" content="2016-06-02T15:54:10.190000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%">                    
                                        <font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><font color="#0066ff"><font size="4" style="font-size: 14pt"><u><b>ZONE
MASTER</b></u></font></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Zone
	id starts with 1 and are added by 1 which is done by the system
	only.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Names
	of the Zones are defined.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Field
	officer names and region names will be loaded from the respective
	master screens.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added Zones are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">By
	clicking on the action button we can modify the given values and
	also make any Zones inactive when not in use.</font></font></font></p>
	<li/>
<p align="left" style="line-height: 100%"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-style: normal"><span style="text-decoration: none">And
	the inactive </span></span><span style="font-style: normal"><span style="text-decoration: none">Zones</span></span><span style="font-style: normal"><span style="text-decoration: none">
	can be seen in only this screen, </span></span><span style="font-style: normal"><span style="text-decoration: none">it</span></span><span style="font-style: normal"><span style="text-decoration: none">
	does not appear else where.</span></span></font></font></font></p>
</ul>
</body>
</html>