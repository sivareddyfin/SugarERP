<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-06-02T09:27:35.037000000"/>
	<meta name="changed" content="2016-06-02T15:26:44.984000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western" align="center"><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u><b>COMPANY
ADVANCE MASTER</b></u></font></font></font></h2>
<p align="left"><br/>
<br/>

</p>
<ul>
	<li/>
<p align="left" style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none"><a name="docs-internal-guid-6fef71c0-0f92-9839-0915-2076c82b8a95"></a>
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">This
	screen gives information about company advance details which will be
	arranged to the ryot by the Company. </span></font></font></font>
	</p>
	<li/>
<p align="left" style="font-style: normal; font-weight: normal; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Advance
	code starts with 1 and are added by 1 which is done by the system
	only.</font></font></font></p>
	<li/>
<p align="left" style="font-style: normal; font-weight: normal; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">Names
	of the advances are defined.</font></font></font></p>
	<li/>
<p align="left" style="text-decoration: none"><font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">All
	the added advances are displayed in the grid.</font></font></font></p>
	<li/>
<p align="left"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="font-style: normal"><span style="text-decoration: none"><span style="font-weight: normal">By
	clicking on the action button we can modify the given values </span></span></span></font><font color="#000000"><span style="font-style: normal"><span style="text-decoration: none"><span style="font-weight: normal">if
	necessary.</span></span></span></font></font></font></p>
	<li/>
<p align="left"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="font-style: normal"><span style="text-decoration: none"><span style="font-weight: normal">P</span></span></span></font><font color="#000000"><span style="font-style: normal"><span style="text-decoration: none"><span style="font-weight: normal">rovision
	is there to set the status as Active/Inactive.</span></span></span></font></font></font></p>
</ul>
</body>
</html>