<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-30T16:31:35.785000000"/>
	<meta name="changed" content="2016-06-02T15:38:49.009000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
		h2.cjk { font-family: "SimSun" }
		h2.ctl { font-family: "Mangal" }
		a:link { so-language: zxx }
	</style>
</head>
<body lang="en-US" dir="ltr">
<h2 class="western">                                <b><font color="#0066ff"><font face="Calibri, sans-serif"><font size="4" style="font-size: 14pt"><u>PREPARE
TIE UP LOANS</u></font></font></font></b></h2>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-6fef71c0-0159-5527-a625-4633ebb7a596"></a>
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">This
	screen is used to feed the loan details given to the ryot for the
	given season</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><b><span style="background: transparent">.</span></b></span></span></font></span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-weight: normal; line-height: 100%">
	<font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">Here
	seasons will be displayed </span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">in
	</span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">drop
	down</span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="background: transparent">
	which are added in the season master screen.</span></span></span></font></span></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Ryot
	code will be displayed based on the selected ryot names.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">By
	default Loan number starts with 1 and incremented by 1.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Branch
	code details will be loaded from the bank master screen.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Provision
	is there to select the agreement number.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Based
	on the selected agreement number, survey number and crop extent will
	be loaded.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Have
	provision to check the added loans by selecting the season and loan
	number.</span></font></font></font></p>
	<li/>
<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="background: transparent">Get
	details button will display the already added loan details.</span></font></font></font></p>
	<p style="margin-bottom: 0in; font-variant: normal; font-style: normal; font-weight: normal; line-height: 100%; text-decoration: none">
	</p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
</body>
</html>