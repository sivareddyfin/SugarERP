<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-30T13:12:39.193000000"/>
	<meta name="changed" content="2016-06-02T15:46:50.142000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">	<span style="text-decoration: none">		</span><font size="4" style="font-size: 14pt"><span style="text-decoration: none">	</span></font><font color="#0066ff"><font size="4" style="font-size: 14pt"><u><b>UPDATE
SOIL TEST RESULTS</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Update soil
	test results screen is used to update the test results of the soil
	for the particular plot of the ryot.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">This screen
	shows that whether the plot is suitable for cultivation or not.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Based on this
	result agreement will be done to ryot. </font></font>
	</p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Ryot code
	will be displayed based on the selected ryot name.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Ryot names
	will get from ryot master screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Here extent
	details like variety, extent, survey number will be loaded from the
	extent details for soil test screen.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">In cultivate
	field, have to select Yes/Yes with enrichment/No based on the soil
	test result.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">Recommendation
	field will open the dialog box with Ryot code, ryot name and survey
	number details.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%; text-decoration: none">
	<font color="#000000"><font face="Calibri, sans-serif">And have to
	give the details of recommendations and then Save in recommendation
	dialog box.</font></font></p>
</ul>
</body>
</html>