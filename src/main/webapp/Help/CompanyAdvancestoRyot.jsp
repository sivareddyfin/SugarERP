<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.1.0.3 (Windows)"/>
	<meta name="created" content="2016-05-31T11:10:31.657000000"/>
	<meta name="changed" content="2016-06-02T15:21:04.622000000"/>
	<style type="text/css">
		@page { margin: 0.79in }
		p { margin-bottom: 0.1in; line-height: 120% }
	</style>
</head>
<body lang="en-US" dir="ltr">
<p style="margin-bottom: 0in; line-height: 100%"><a name="docs-internal-guid-baf28d49-055c-7309-ddd7-66cfbad8199d"></a>
                                                         <font color="#0066ff"><u>
</u></font><font color="#0066ff"><font face="Calibri, sans-serif"><font size="3" style="font-size: 13pt"><u><b>ADVANCES
TO RYOT</b></u></font></font></font></p>
<p style="margin-bottom: 0in; line-height: 100%"><br/>

</p>
<ul>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt"><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">Using
	this screen we can give d</span></span></span></span></font></span><span style="font-variant: normal"><font color="#000000"><span style="text-decoration: none"><span style="font-style: normal"><span style="font-weight: normal"><span style="background: transparent">ifferent
	advances to ryot</span></span></span></span></font></span>.</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif"><font size="3" style="font-size: 12pt">There
	is a provision to modify the added advances</font></font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">By
	default added advances and ryot name drop downs will not support to
	select.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">After
	selecting the season, can select the added advances and ryot names
	in the drop down.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Based
	on the selected ryot, ryot code will be displayed.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Here,
	there is a provision to select the advance type which are given in
	the company advance master screen.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Here
	given amount should not be greater than the amount which is given in
	the company advance master for that particular selected advance
	type. </font>
	</p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Here,
	variety names will be loaded from the variety master.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Provision
	is there to select whether the advance is seedling advance or not by
	selecting Yes/No.</font></p>
	<li/>
<p style="margin-bottom: 0in; line-height: 100%"><font face="Calibri, sans-serif">Tray
	types will be loaded from the seedling tray master.</font></p>
	<p style="margin-bottom: 0in; line-height: 100%"></p>
</ul>
</body>
</html>