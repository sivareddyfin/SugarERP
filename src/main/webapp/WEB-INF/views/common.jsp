<% String contextPath = request.getContextPath(); %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<style type="text/css" title="currentStyle" media="screen">
		@import "<%=contextPath%>/angular/vendors/bower_components/animate.css/animate.min.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css";
		@import "<%=contextPath%>/angular/vendors/bower_components/chosen/chosen.min.css";
		@import "<%=contextPath%>/angular/vendors/summernote/dist/summernote.css";
		@import "<%=contextPath%>/angular/css/app.min.1.css";
		@import "<%=contextPath%>/angular/css/app.min.2.css";	
		

		</style>
		        <!-- Core -->
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Angular -->
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-resource/angular-resource.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-animate/angular-animate.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-resource/angular-resource.min.js"></script>

        
        <!-- Angular Modules -->
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-loading-bar/src/loading-bar.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

        <!-- Common Vendors -->
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/angular-nouislider/src/nouislider.min.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/ng-table/dist/ng-table.min.js"></script>
		<script type="text/javascript" src="<%=contextPath%>/angular/vendors/bower_components/chosen/chosen.jquery.min.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
                
        <!-- App level -->
        <script type="text/javascript" src="<%=contextPath%>/angular/js/app.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/config.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/controllers/main.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/services.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/templates.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/controllers/ui-bootstrap.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/controllers/table.js"></script>


        <!-- Template Modules -->
        <script type="text/javascript" src="<%=contextPath%>/angular/js/modules/template.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/modules/ui.js"></script>
        <script type="text/javascript" src="<%=contextPath%>/angular/js/modules/form.js"></script>
</script>
</body>
</html>