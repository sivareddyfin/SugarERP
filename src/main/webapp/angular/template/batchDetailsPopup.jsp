<div id="batchPopupScreen">
<form name="GridPopup" novalidate>
	<div class="card popupbox_ratoon" id="gridpopup_box">
					   				<div class="row">
										<div class="col-sm-4">
											<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="productCodepopup">Product Code</label>
														
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="productCode" data-ng-model="productCode" placeholder="Product Code" tabindex="1"  id="productCodepopup" with-floating-label readonly  />
													</div>
													
													
													
															  </div>
														  </div>
										
										</div>
										<div class="col-sm-4"><input type="hidden" ng-model="productId" name="productId" /></div>
										<div class="col-sm-4"><input type="hidden"  ng-model="popupId"/></div>
									</div>						  
						  	  					
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th> &nbsp;&nbsp;Select</th>
												<th>Batch ID</th>	
												<th>Batch</th>
												<th>Expiry Date</th>
												<th>Cost Price</th>
												<th>MRP</th>
												<th>Stock</th>
												<th>Mfr. Date</th>
												
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="batchDetails in productBatchData">
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label class="checkbox checkbox-inline m-r-20" style="margin-top:-10px;" >
												<input type="checkbox"  class="checkbox" name="batchFlag{{$index}}"  ng-model="batchDetails.batchFlag" ng-click="batchDatapush(batchDetails.batchFlag,batchDetails.batch,batchDetails.expiryDate,batchDetails.mrp,$index,batchDetails.batchSeqNo,batchDetails.costPrice,batchDetails.batchId,batchDetails.mfrDate,batchDetails.stock,batchDetails.stockInDate);"    ><i class="input-helper" ></i></label></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.batchId" name="batchId{{$index}}" /></td>
											<td><input type="text" class="form-control1"  readonly  ng-model="batchDetails.batch" name="batch{{$index}}"/></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.expiryDate" name="expiryDate{{$index}}" data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$" maxlength='7' /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.costPrice" name="costPrice{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mrp" name="mrp{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.stock" name="stock{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mfrDate" name="mfrDate{{$index}}" /></td>
											<td style="display:none" ><input type="text" class="form-control1" readonly  ng-model="batchDetails.stockInDate" name="stockInDate{{$index}}" /></td>
											
											
											
											
											
											
											</tr>
											
											
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 	
							 
							 <div class="row">
					   	<div class="col-sm-4">
							<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="newbatch">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="newabatch" data-ng-model="newBatch.batch" placeholder="Batch" tabindex="1"  id="newbatch" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="expdt">Batch</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="expdt" data-ng-model="newBatch.expdt" placeholder="Expiry Date" tabindex="1"  id="expdt" data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$" maxlength='7' with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
						
						<div class="col-sm-4">
							<div class="input-group">
											
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="MRP">MRP</label>
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="MRP" data-ng-model="newBatch.mrp" placeholder="MRP" tabindex="1"  id="MRP" with-floating-label  />
													</div>
													
													
															  </div>
														  </div>
						</div>
					   
					   </div>
					   <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseBatchPopup(newBatch.batch,newBatch.expdt,newBatch.mrp,productId);">Load</button>
									</div>
								</div>						 	
							 </div>						 							 
				        </div>
</form>
</div>					 