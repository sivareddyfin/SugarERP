
<div class="sidebar-inner c-overflow">    
    <div class="profile-menu">
        <a href="" toggle-submenu>
            <div class="profile-pic">
                <img src="img/profile-pics/1.jpg" alt="">
            </div>

            <div class="profile-info">
                makella
                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>

        <ul class="main-menu">
            <li>
                <a data-ui-sref="pages.profile.profile-about" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
            <li>
                <a href="/SugarERP" id="logout"><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>
        </ul>
    </div>

    <ul class="main-menu">
        
       <!-- <li data-ui-sref-active="active">
            <a data-ui-sref="home" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Home</a>
        </li>
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('headers') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-view-compact"></i> Headers</a>

            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="headers.textual-menu" data-ng-click="mactrl.sidebarStat($event)">Textual menu</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="headers.image-logo" data-ng-click="mactrl.sidebarStat($event)">Image logo</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="headers.mainmenu-on-top" data-ng-click="mactrl.sidebarStat($event)">Mainmenu on top</a></li>
            </ul>
        </li>
        <li data-ui-sref-active="active">
            <a data-ui-sref="typography" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-format-underlined"></i> Typography</a>
        </li>
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('widgets') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-widgets"></i> Widgets</a>

            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="widgets.widget-templates" data-ng-click="mactrl.sidebarStat($event)">Templates</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="widgets.widgets" data-ng-click="mactrl.sidebarStat($event)">Widgets</a></li>
            </ul>
        </li>
        
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('tables') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-view-list"></i> Tables</a>

            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="tables.tables" data-ng-click="mactrl.sidebarStat($event)">Tables</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="tables.data-table" data-ng-click="mactrl.sidebarStat($event)">Data Tables</a></li>
            </ul>
        </li>
        
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('form') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-collection-text"></i> Forms</a>

            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="form.basic-form-elements" data-ng-click="mactrl.sidebarStat($event)">Basic Form Elements</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="form.form-components" data-ng-click="mactrl.sidebarStat($event)">Form Components</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="form.form-examples" data-ng-click="mactrl.sidebarStat($event)">Form Examples</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="form.form-validations" data-ng-click="mactrl.sidebarStat($event)">Form Validation</a></li>
            </ul>
        </li>

        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('user-interface') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-swap-alt"></i>User Interface</a>
            <ul>
                <li><a  data-ui-sref="user-interface.ui-bootstrap" data-ng-click="mactrl.sidebarStat($event)">UI Bootstrap</a></li>
                <li><a data-ui-sref="user-interface.colors" data-ng-click="mactrl.sidebarStat($event)">Colors</a></li>
                <li><a  data-ui-sref="user-interface.animations" data-ng-click="mactrl.sidebarStat($event)">Animations</a></li>
                <li><a  data-ui-sref="user-interface.box-shadow" data-ng-click="mactrl.sidebarStat($event)">Box Shadow</a></li>
                <li><a  data-ui-sref="user-interface.buttons" data-ng-click="mactrl.sidebarStat($event)">Buttons</a></li>
                <li><a  data-ui-sref="user-interface.icons" data-ng-click="mactrl.sidebarStat($event)">Icons</a></li>
                <li><a  data-ui-sref="user-interface.alerts" data-ng-click="mactrl.sidebarStat($event)">Alerts</a></li>
                <li><a  data-ui-sref="user-interface.preloaders" data-ng-click="mactrl.sidebarStat($event)">Preloaders</a></li>
                <li><a  data-ui-sref="user-interface.notifications-dialogs" data-ng-click="mactrl.sidebarStat($event)">Notifications & Dialogs</a></li>
                <li><a  data-ui-sref="user-interface.media" data-ng-click="mactrl.sidebarStat($event)">Media</a></li>
                <li><a  data-ui-sref="user-interface.other-components" data-ng-click="mactrl.sidebarStat($event)">Others</a></li>
            </ul>
        </li>
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('charts') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-trending-up"></i>Charts</a>
            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="charts.flot-charts" data-ng-click="mactrl.sidebarStat($event)">Flot Charts</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="charts.other-charts" data-ng-click="mactrl.sidebarStat($event)">Other Charts</a></li>
            </ul>
        </li>

        <li data-ui-sref-active="active">
            <a data-ui-sref="calendar" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-calendar"></i> Calendar</a>
        </li>
        
        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('photo-gallery') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-image"></i>Photo Gallery</a>
            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="photo-gallery.photos" data-ng-click="mactrl.sidebarStat($event)">Default</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="photo-gallery.timeline" data-ng-click="mactrl.sidebarStat($event)">Timeline</a></li>
            </ul>
        </li>

        
        <li data-ui-sref-active="active">
            <a data-ui-sref="generic-classes" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-layers"></i> Generic Classes</a>
        </li>

        <li class="sub-menu" data-ng-class="{ 'active toggled': mactrl.$state.includes('pages') }">
            <a href="" toggle-submenu><i class="zmdi zmdi-collection-item"></i> Sample Pages</a>
            <ul>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.profile.profile-about" data-ng-click="mactrl.sidebarStat($event)">Profile</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.listview" data-ng-click="mactrl.sidebarStat($event)">List View</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.messages" data-ng-click="mactrl.sidebarStat($event)">Messages</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.pricing-table" data-ng-click="mactrl.sidebarStat($event)">Pricing Table</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.contacts" data-ng-click="mactrl.sidebarStat($event)">Contacts</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.invoice" data-ng-click="mactrl.sidebarStat($event)">Invoice</a></li>
                <li><a data-ui-sref-active="active" data-ui-sref="pages.wall" data-ng-click="mactrl.sidebarStat($event)">Wall</a></li>
                <li><a href="login.html">Login and Sign Up</a></li>
                <li><a href="lockscreen.html">Lockscreen</a></li>
                <li><a href="404.html">Error 404</a></li>
            </ul>
        </li>-->
        <li class="sub-menu">
            <a href="" toggle-submenu><i class="zmdi zmdi-menu"></i> Administration</a>
            <ul>				
                <li class="sub-menu" style="margin-left:-20px;">
                    <a href="" toggle-submenu><i class="zmdi zmdi-account-circle"></i> Admin Data</a>
                    <ul style="margin-left:25px;">
                        <li class="sub-menu">
							<a href="" toggle-submenu><i class="zmdi zmdi-lock-open"></i> Security</a>
							<ul style="background-color: #f7f7f7;padding-left:12px;">
								<li style="padding:3px;">
									<a data-ui-sref="ScreenMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Screen Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="RoleMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Role Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="DepartmentMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Department Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="LoginUserMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Login User Master</a>
								</li>								
							</ul>	
						</li>
						<li class="sub-menu">
							<a href="" toggle-submenu><i class="zmdi zmdi-lock-open"></i> Master Data</a>
							<ul style="background-color: #f7f7f7;padding-left:12px;">
								<li style="padding:3px;">
									<a data-ui-sref="CompanyMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Master</a>
								</li>							
								<li style="padding:3px;">
									<a data-ui-sref="CaneManager" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Manager Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="FieldOfficer" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Field Officer Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="FieldAssistant" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Field Assistant Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="Zone" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Zone Master</a>
								</li>								
								<li style="padding:3px;">
									<a data-ui-sref="Circle" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Circle Master</a>
								</li>																
								<li style="padding:3px;">
									<a data-ui-sref="Region" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Region Master</a>
								</li>																
								<li style="padding:3px;">
									<a data-ui-sref="Mandal" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Mandal Master</a>
								</li>																
								<li style="padding:3px;">
									<a data-ui-sref="Village" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Village Master</a>
								</li>																
								<li style="padding:3px;">
									<a data-ui-sref="Bank" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Bank Master</a>
								</li>																								
								<li style="padding:3px;">
									<a data-ui-sref="Ryot" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Ryot Master</a>
								</li>																								
								<li style="padding:3px;">
									<a data-ui-sref="Advance" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Advance Master</a>
								</li>								
								<li style="padding:3px;">
									<a data-ui-sref="AdvanceOrder" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Order of Advances</a>
								</li>																
								<li style="padding:3px;">
									<a data-ui-sref="CompanyAdvanceIntrest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Advance Intrest Rate</a>
								</li>																																
								<li style="padding:3px;">
									<a data-ui-sref="BankIntrestOnLoans" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Bank Intrest on Loans</a>
								</li>																																								
								<li style="padding:3px;">
									<a data-ui-sref="Deductions" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Deduction Master</a>
								</li>																																
								<li style="padding:3px;">
									<a data-ui-sref="DeductionsOrder" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Order of Deductions</a>
								</li>																																
								<li style="padding:3px;">
									<a data-ui-sref="SampleRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Sample Rules</a>
								</li>																																								
								<li style="padding:3px;">
									<a data-ui-sref="PermitRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Permit Rules</a>
								</li>																																								
								<li style="padding:3px;">
									<a data-ui-sref="RyotExtentDetails" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Ryot Extent Details</a>
								</li>																																								
								<li style="padding:3px;">
									<a data-ui-sref="soilTestResults" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Soil Test Results</a>
								</li>																																								
								<li style="padding:3px;">
									<a data-ui-sref="OfferMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Offer Master</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="PrepareLoan" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Prepare Loans</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="RyotLoans" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Search Ryot Loans</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="SearchRyotLoansbySurvey" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Search Ryot Loans by Survey No</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="Designation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Designation Master</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="FarmerType" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Farmer Type Master</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="Variety" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Variety Master</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="UpdateAdvances" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Company Advances</a>
								</li>																																																
								<li style="padding:3px;">
									<a data-ui-sref="UpdateLoans" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Loan Details</a>
								</li>																																																								
								<li style="padding:3px;">
									<a data-ui-sref="UpdateLoanIntrest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Loan Intrest</a>
								</li>																																																								
								<li style="padding:3px;">
									<a data-ui-sref="UpdateCompanyAdvanceIntrest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Company Advance Intrest</a>
								</li>																																																								
								<li style="padding:3px;">
									<a data-ui-sref="SetupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Setup Master</a>
								</li>																																																								
								<li style="padding:3px;">
									<a data-ui-sref="UpdateEstimatedQty" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Estimated Quantity</a>
								</li>																																																																
								<li style="padding:3px;">
									<a data-ui-sref="GeneratePrograms" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Programs</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="ProgramDetails" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Program Details</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="GenerateSampleCards" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Sample Cards</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="LabCCSRatings" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Lab CCS Ratings</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="AvgCCSRatings" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Average CCS Ratings</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="GenerateRanking" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Ranking</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="GeneratePermit" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Generate Permits</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="AdvanceCategory" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Advance Category</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="AdvanceSubCategory" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Advance Subcategory</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="SeedlingTray" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Seedling Tray Master</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="CompanyAdvances" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Company Advances</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="TrayReturns" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Seedling Tray Returns</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="ScreenFields" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Screen Fields Master</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="RyotType" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Ryot Type</a>
								</li>																																																																								
								<li style="padding:3px;">
									<a data-ui-sref="TieupLoanInterest" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Tie Up Loan Interets Master</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="CaneAccountingRules" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Accounting Rules - Season</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="SeedAccountingRate" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Seed Accounting Rate Details</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="PostSeasonPaymentRate" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Post Season Payment Rate Master</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="OtherDeductions" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Other Deductions</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="SeedadvanceCalculation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Perform Seed Advance Calculations</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="TestWeighment" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Test Weighment</a>
								</li>																																																																								
								<li style="padding:3px;">
								  <a data-ui-sref="CaneWeighment" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Cane Weighment</a>
								</li>																																																																								
								
								
								
								<li style="padding:3px;">
									<a data-ui-sref="Sample" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Sample</a>
								</li>																																																								
								
							</ul>	
						</li>		
						<li class="sub-menu">
            <a href="" toggle-submenu><i class="zmdi zmdi-lock-open"></i> Cane Receipts</a>
            <ul style="background-color: #f7f7f7;padding-left:12px;">				
                <li class="sub-menu">
								<li style="padding:3px;">
									<a data-ui-sref="ShiftMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Shift Master</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="Weight Bridge master" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Weight Bridge Master</a>
								</li>	
								<li style="padding:3px;">
									<a data-ui-sref="AccountGroupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Account Group Master</a>
								</li>	
								
								<li style="padding:3px;">
									<a data-ui-sref="HarvestContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Harvest Contract Master</a>
								</li>	
									<li style="padding:3px;">
									<a data-ui-sref="SubGroupMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Sub Group Master</a>
								</li>									
								<li style="padding:3px;">
									<a data-ui-sref="TransportContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Transport Contractor Master</a>
								</li>									
									<li style="padding:3px;">
									<a data-ui-sref="UpdateHarvestCharges" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Harvest Charges</a>
								</li>
								<li style="padding:3px;">
									<a data-ui-sref="UpdateTransportCharges" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Update Transport Charges</a>
								</li>								
								<li style="padding:3px;">
									<a data-ui-sref="UnloadingContractorMaster" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Unloading Contractor Master</a>
								</li>	
								<li style="padding:3px;">
									<a data-ui-sref="PerformCaneAccountingCalculation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Perform Cane Accounting Calculation</a>
								</li>								
								<li style="padding:3px;">
									<a data-ui-sref="PostSeasonCalculation" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> PostSeason Calculation</a>
								</li>																																																														
								<li style="padding:3px;">
									<a data-ui-sref="Sample" data-ng-click="mactrl.sidebarStat($event)"><i class="zmdi zmdi-home"></i> Sample</a>
								</li>																																																								
								
							</ul>	
						</li>						
                    </ul>
                </li>				
                    </ul>
                </li>								
            </ul>
        </li>
        <!--<li>
            <a href="https://wrapbootstrap.com/theme/material-admin-responsive-angularjs-WB011H985"><i class="zmdi zmdi-money"></i> Buy this template</a>
        </li>-->
    </ul>
</div>
