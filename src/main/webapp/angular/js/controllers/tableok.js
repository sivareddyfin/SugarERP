/*materialAdmin
    .controller('tableCtrl', function($filter, $sce, ngTableParams, tableService) {
        //var data = tableService.data;
		var data = sessionStorage.getItem('data1');
		if (data) { data = JSON.parse(data);}*/
		//alert(data.length);
        //Basic Example
        /*this.tableBasic = new ngTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function ($defer, params) {
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        })*/
        
        //Sorting
       /* this.tableSorting = new ngTableParams({
            page: 1,            // show first page
            count: 10,           // count per page
            sorting: {
                name: 'asc'     // initial sorting
            }
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
    
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        })*/
        
        //Filtering
        /*this.tableFilter = new ngTableParams({
            page: 1,            // show first page
            count: 10
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.filter() ? $filter('filter')(data, params.filter()) : data;
				
                this.id = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                this.name = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                this.email = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                this.username = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                this.contact = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(this.id, this.name, this.email, this.username, this.contact);
            }
        })*/
        
        //Editable
       /* this.tableEdit = new ngTableParams({
            page: 1,            // show first page
            count: 20           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
	   
    })*/
	
materialAdmin
    .controller('addrows', function($scope, $compile) {
																		
		/*for(var $i=0;$i<10;$i++)
    	{
		    var $el = $('<tr><td><input type="text" class="editBox" value=""/></td>' +
        					'<td><input type="text" class="editBox" value=""/></td>' +
					        '<td>' +'<span>' +'<button id="createHost" class="btn btn-mini btn-success" data-ng-click="create()"><b>Create</b></button>' +
					        '</span>' +'</td></tr>').appendTo('#newTransaction');
        }
		
    	$compile($el)($scope);      
	    $scope.create = function()
		{
        	alert('1');
	    }*/
		
		

})

materialAdmin
    .controller('formsubmit', function($scope) {
									   
									   
	$scope.users = [];				   
	$scope.submitForm=function()
	{
			
			if($scope.datalist.id)
			{
				$scope.users.push({ 'id':$scope.datalist.id, 'name': $scope.datalist.name, 'age':$scope.datalist.age, 'contactnumber':$scope.datalist.contactnumber, 'address':$scope.datalist.address  });
				$scope.datalist.id = "";
				$scope.datalist.name = "";
				$scope.datalist.age = "";
				$scope.datalist.contactnumber = "";
				$scope.datalist.address = "";
			}
			
			
	}

	$scope.resetForm=function()
	{
			$scope.datalist = {};
	}
	
	$scope.update = function(id,sno)
	{
		$scope.datalist.id = sno.id;
		$scope.datalist.name = sno.name;
		$scope.datalist.age = sno.age;
		$scope.datalist.contactnumber = sno.contactnumber;
		$scope.datalist.address = sno.address;
		
	}
			
})


materialAdmin.directive('ngCompare', function () {
    return {
        require: 'ngModel',
        link: function (scope, currentEl, attrs, ctrl) {
            var comparefield = document.getElementsByName(attrs.ngCompare)[0]; //getting first element
            compareEl = angular.element(comparefield);

            //current field key up
            currentEl.on('keyup', function () {
                if (compareEl.val() != "") {
                    var isMatch = currentEl.val() === compareEl.val();
                    ctrl.$setValidity('compare', isMatch);
                    scope.$digest();
                }
            });

            //Element to compare field key up
            compareEl.on('keyup', function () {
                if (currentEl.val() != "") {
                    var isMatch = currentEl.val() === compareEl.val();
                    ctrl.$setValidity('compare', isMatch);
                    scope.$digest();
                }
            });
        }
    }
});

materialAdmin.controller('validation', function ($scope) {

    $scope.countryList = [
    { CountryId: 'India', Name: 'India' },
    { CountryId: 'USA', Name: 'USA' }
    ];

    $scope.cityList = [];

    $scope.$watch('user.country', function (newVal, oldVal) {

        if (newVal == 'India')
			//for(var i=0;i<4;i++)
			//{
	            $scope.cityList = [
                                	{ CountryId: 'India', CityId: 'Texas', Name: 'Texas' },
									{ CountryId: 'India', CityId: 'NewYork', Name: 'NewYork' }];
			//}
        else if (newVal == 'USA')
            $scope.cityList = [
                               { CountryId: 'USA', CityId: 'Texas', Name: 'Texas' },
                               { CountryId: 'USA', CityId: 'NewYork', Name: 'NewYork' }];
        else
            $scope.cityList = [];
    });

    // function to submit the form after all validation has occurred			
    $scope.submitForm = function (user) {
		//var str = document.getElementsByName("name")[0].value;	
		//var str = $scope.user.city;
		//$scope.focusElement = "chk";
		//alert(JSON.stringify(user));
		var str = $scope.user.terms;
		
        // Set the 'submitted' flag to true
        $scope.submitted = true;
		//alert($scope.userForm.$valid);
        if ($scope.userForm.$valid) {
			//alert('No Errors');
			
		/*if (window.XMLHttpRequest)  
		{
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();  
		}
		else  
		{
			// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");  
		}
		xmlhttp.onreadystatechange=function()  
		{
				alert(xmlhttp.status);
			if (xmlhttp.readyState==4 && xmlhttp.status==200) 
			{ 
				//document.getElementById("dispstutf").innerHTML=xmlhttp.responseText; 
				alert(xmlhttp.responseText);
			} 
		}
		xmlhttp.open("GET","gethint.php?str="+str,true);
		xmlhttp.send(); 			
			*/
            //alert("Form is valid!");
			//alert(document.getElementByname('name').value);	
        }
        else {
            alert("Please correct errors!");
        }
    };
});
	