materialAdmin
//==========================================
			 //Seedling Nursery
		//==========================================
		
		
		//=============CategoryMaster================================
		.controller('CategoryMasterController', function($scope,$http)
		{
			$scope.loadCategoryStatus = function()
			{
				$scope.AddedCategory={'status':'0'};
			};
			
			$scope.loadgetAllCategory = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/url.html",
				data: {}
				}).success(function(data, status) 
				{
				$scope.CategoryData=data;											
				});
			};
			$scope.AddCategoryMasterSubmit = function(AddedCategory,form)
			{
				if($scope.CategoryMasterForm.$valid) 
				{
					//alert(JSON.stringify(AddedCategory));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/url.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedCategory),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Category Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedCategory = angular.copy($scope.master);
									$scope.loadCategoryStatus();
									form.$setPristine(true);			    								
									
									//---------get all Category--------									
									var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/url.html",
										data: {}
									   }).success(function(data, status) 
										{
											$scope.CategoryData=data;
										});  	   
									//-------load Status after Submit----
									//$scope.loadCategoryStatus = function()
//									{
//										$scope.AddedCategory={'status':'0'};
//									};
									
								  //-----------end--------									
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
				
			};
			
			
			$scope.UpdateCategory = function(category,id)
			{					
					delete category["$edit"];
					
					$scope.isEdit = true;
					
					$scope.Addsubmitted = true;
					
					if($scope.EditCategoryMasterFrom.$valid)
					{
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/url.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(category),

						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Category Updated Successfully!', "success");
														
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/url.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.CategoryData=data;											
							 			});  	   
							//---------End--------
						 		}
							    else
								 {
									  sweetAlert("Oops...", "Something went wrong!", "error");
								 }
					 		}
						});			
				}
			//-----------end----------			
			};
			
			  $scope.reset = function(form)
		   {			  
				$scope.AddedCategory = angular.copy($scope.master);
				
			    form.$setPristine(true);
				$scope.loadCategoryStatus();
		   };
			
			
		})

							   
	
		.controller('MachineMasterController', function($scope,$http)
		{
			$scope.loadModifyFlag = function()
			{
				$scope.AddedMachine={'modifyFlag':"No",'status':"0"};
				
			};
			$scope.loadMachines = function()
		{		
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllMachines.html",
					data: {}
			   }).success(function(data, status) 
			   {
					$scope.machineData = data; 		//-------for load the data in to added departments grid---------		
			   });
  	   };
				$scope.machineSubmit = function(AddedMachine,form)
			{
				if($scope.MachineForm.$valid) 
				{
					//alert(JSON.stringify(AddedMachine));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveMachine.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedMachine),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Machine Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedMachine = angular.copy($scope.master);
									
									form.$setPristine(true);			    								
									
									//---------get all machines--------									
									
									
									var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getAllMachines.html",
											data: {}
									   }).success(function(data, status) 
									   {
											$scope.machineData = data; 		//-------for load the data in to added departments grid---------		
									   });
  	  
								  	$scope.AddedMachine={'modifyFlag':"No",'status':"0"};								
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
				
			};
			
			
			$scope.UpdateMachine = function(Group,id)
			{					
					delete Group["$edit"];
					
					$scope.isEdit = true;
					
					//$scope.Addsubmitted = true;
					
					if($scope.EditAccountSubGroupMasterFrom.$valid)
					{
						//alert(angular.toJson(Group));
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/saveMachine.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(Group),

						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	//alert(response);
								if(response==true)
								{
									swal("Success!", 'Machine Updated Successfully!', "success");
														
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllMachines.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.machineData=data;											
							 			});  	   
							//---------End--------
						 		}
							    else
								 {
									  sweetAlert("Oops...", "Something went wrong!", "error");
								 }
					 		}
						});			
				}
			//-----------end----------			
			};
			
			
			  $scope.reset = function(form)
		   {			  
		   $scope.AddedMachine={'modifyFlag':"No",'status':"0"};
				//$scope.AddedMachine = angular.copy($scope.master);
//				$scope.AddedMachine={'modifyFlag':"No",'status':"0"};
//				form.$setPristine(true);
				
		   };
		
		})
		
		.controller('kindMasterController', function($scope,$http)
		{
			var DropDownLoad = new Object();
			DropDownLoad.tablename = "CompanyAdvance";
			DropDownLoad.columnName = "advancecode";
			DropDownLoad.columnName1 = "advance";
			var jsonStringDropDown= JSON.stringify(DropDownLoad);
			
			$scope.loadAdvanceNames = function()
			{
				//$scope.advanceNames=[];
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadAddedDropDownsForMasters.html",
							data: jsonStringDropDown,
						 }).success(function(data, status) 
						 {
							 //alert(JSON.stringify(data));
							$scope.advanceNames=data;
						 }); 
			};
			$scope.loadKindStatus = function()
			{
			 $scope.AddedKind={'status':'0','modifyFlag':'No','roundingTo':'1'};
			};
			
			$scope.getallKinds = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllKindType.html",
				data: {}
				}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
				$scope.kindData=data;											
				});
			};


			$scope.AddKindMasterSubmit = function(AddedKind,form)
			{
				if($scope.kindForm.$valid) 
				{
					//alert(JSON.stringify(AddedKind));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    		url:"/SugarERP/saveKindType.html",
						    processData:true,
						    type:'POST',
						    contentType:'Application/json',
						    data:JSON.stringify(AddedKind),
							
							beforeSend: function(xhr) 
							{
					            xhr.setRequestHeader("Accept", "application/json");
					            xhr.setRequestHeader("Content-Type", "application/json");
			    		    },
						    success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Kind Added Successfully!', "success");
									$('.btn-hide').attr('disabled',false);
									$scope.AddedKind = angular.copy($scope.master);
									$scope.loadKindStatus();
									form.$setPristine(true);			    								
									
									//---------get all kinds--------									
								var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllKindType.html",
									data: {}
									}).success(function(data, status) 
									{
										//alert(JSON.stringify(data));
									$scope.kindData=data;											
									});				
								}
							   else
							    {
								   sweetAlert("Oops...", "Something went wrong!", "error");
								   $('.btn-hide').attr('disabled',false);									
								}
							}
						});					
				}
			   else
			    {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}					
				}
				
			};
			
			
			$scope.updateKindMaster = function(kind,id)
			{					
					delete kind["$edit"];
					
					$scope.isEdit = true;
					
					//$scope.Addsubmitted = true;
					//alert(id);
				//	alert(angular.toJson(kind));
				//	alert($scope.kindMasterEdit.$valid);
					if($scope.kindMasterEdit.$valid)
					{
						$scope.isEdit = false;
			  			$.ajax({
			   			url:"/SugarERP/saveKindType.html",
				    	processData:true,
				    	type:'POST',
				    	contentType:'Application/json',
				    	data:angular.toJson(kind),

						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
			            	xhr.setRequestHeader("Content-Type", "application/json");
						},
							success:function(response) 
							{	
								if(response==true)
								{
									swal("Success!", 'Kind Updated Successfully!', "success");
														
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAllKindType.html",
									data: {}
									}).success(function(data, status) 
									{
										//alert(JSON.stringify(data));
									$scope.kindData=data;											
									});  	   
							//---------End--------
						 		}
							    else
								 {
									  sweetAlert("Oops...", "Something went wrong!", "error");
								 }
					 		}
						});			
				}
			//-----------end----------			
			};
			
			  $scope.reset = function(form)
		   {			  
				$scope.AddedKind = angular.copy($scope.master);
				
			    form.$setPristine(true);
				$scope.loadKindStatus();
		   };
			
			
		})

		
		.controller('BudRemovedController', function($scope,$http)
		{
			$scope.RemovedBudCane={'modifyFlag':'No'};
			$scope.loadAddedDates = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getEstimateDates.html",
				data: {}
				}).success(function(data, status) 
				{
					$scope.addedDateNames= data;
				//	alert(JSON.stringify(data));
				//$scope.kindData= ;											
				});
			};
             $scope.master={};
			 $scope.modifyDate = function(addedDates)
			{
				
				//alert(addedDates);
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllBudRemovedCane.html",
					data: addedDates,
			   }).success(function(data, status) 
			   {		//alert(JSON.stringify(data));
					$scope.RemovedBudCane=data[0];
	 		   }); 
			};
			$scope.budRemovedCaneSubmit = function(RemovedBudCane,form)
		    {
		
			if($scope.BudRemovedCane.$valid) 
			if($scope.BudRemovedCane.$valid) 
			{	
			//alert(JSON.stringify(RemovedBudCane));
				$('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveBudRemovedCane.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(RemovedBudCane),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success!", 'Bud Removed Cane Added Successfully!', "success");  //-------For showing success message-------------							
							$('.btn-hide').attr('disabled',false);
							$scope.RemovedBudCane = angular.copy($scope.master);
									//$scope.loadKindStatus();
									form.$setPristine(true);
									$scope.$apply();
							//$scope.RemovedBudCane = angular.copy($scope.master);
							//form.$setPristine(true);
							//$scope.reset(form);
							//$scope.RemovedBudCane = angular.copy($scope.master);
							//form.$setPristine(true);
							}
						 else
						  {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);							  
						  }
  				    }
		         });				 	
			  }	
			 else
			  {
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
			  }
		  };
		    $scope.reset = function(form)
		   {			  
				$scope.RemovedBudCane = angular.copy($scope.master);
				form.$setPristine(true);			    											   
		   };
		})

		


		
		.controller('SeedlingEstimateController',function($scope,$http)
		{
	
			$scope.master = {};
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname="Season";
			var StringDropDown= JSON.stringify(DropDownLoad);
			
			var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "Employees";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
			
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Month";
			var jsonStringDropDownMonth= JSON.stringify(DropDownLoad);
			
			var LoadDateofPlanting = new Object();
			LoadDateofPlanting.dropdownname = "Period";
			var StringDatePlanting= JSON.stringify(LoadDateofPlanting);
			
			
			$scope.loadSeason=function()
		   {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
					$scope.SeasonData=data;
					$scope.AddedSeedlingEstimateQty = {'modifyFlag':'No'};
			   }); 
			};
		$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
					$scope.FieldOffNames=data;
	 		   });  	   
		};
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		$scope.loadmonthNames = function()
				{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringDropDownMonth,
					}).success(function(data, status) 
					{				
						$scope.monthNames = data;
						
					});  	   					
		};
		$scope.year = [ { yearId: 2014, year: 2014 }, { yearId: 2015, year: 2015 }, { yearId: 2016, year: 2016 }, { yearId: 2017, year: 2017 }, { yearId: 2018, year: 2018 }, { yearId: 2019, year: 2019 }, { yearId: 2020, year: 2020 }];
		
		$scope.loadPeriodNames = function()
		{
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDatePlanting,
				}).success(function(data, status) 
				{
					
					$scope.periodNames = data;
				});						
		};
			
		$scope.seedlingEstimateSubmit = function(AddedSeedlingEstimateQty,form)
		  {
			
			  var AddedSeedlingEstimateQtys = JSON.stringify(AddedSeedlingEstimateQty);   /*------------for getting data from form-----------*/
				//alert($scope.SeedlingEstimate.$valid);
			  if($scope.SeedlingEstimate.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/saveSeedlingEstimate.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedSeedlingEstimateQtys,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success!", 'Seedling Estimate done Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.AddedSeedlingEstimateQty = angular.copy($scope.master);
							form.$setPristine(true);
								
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllSeedingEstimate.html",
							data: {}
							}).success(function(data, status) 
							{
								$scope.EstimateData=data;
												
							}); 
														 //--------------end---------
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };
		  
		    $scope.reset = function(form)
		   {			  
				$scope.AddedSeedlingEstimateQty = angular.copy($scope.master);
				
			    form.$setPristine(true);			    											   
		   };
		   
		   $scope.getAllSeedlingEstimate=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllSeedingEstimate.html",
				data: {}
				}).success(function(data, status) 
				{
					$scope.EstimateData=data;
											
				}); 
				
			};
			
			
			$scope.updateSeedlingEstimate = function(Estimate,id)
		  {		  	  			  
			  delete Estimate["$edit"];
			  var UpdatedData = angular.toJson(Estimate);
			  //alert(UpdatedData);
			  $.ajax({
				    url:"/SugarERP/saveSeedlingEstimate.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						
						if(response==true)
						{
			  				swal("Success!", 'Seedling Estimate done Successfully!', "success");  //-------For showing success message-------------		
							//-------Load All Added Seedling Estimates-------------						
								var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/getAllSeedingEstimate.html",
								data: {}
								}).success(function(data, status) 
								{
									$scope.EstimateData=data;
															
								});
							//---------end---------
						}
					   else
					    {
							 sweetAlert("Oops...", "Something went wrong!", "error");
						}
					}
				});			  
		  };
		
		
			
		})
		.controller('SourceOfSeedController', function($scope,$http)
		{
			$scope.master = {};
			var DropDownLoad = new Object();
			DropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(DropDownLoad);
			var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
				
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var CircleDropDown = new Object();
			CircleDropDown.dropdownname = "Circle";
			var jsonCircleDropDown= JSON.stringify(CircleDropDown);
			
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			var obj = new Object();
			var addedSource = new Object();
			$scope.AddSource = [];
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			$scope.updateSSValidation = function(status,changeStatus)
			{
				
				if(status==0)
				{
					
					$scope.changeStatus = false;
					$scope.changeStatus2 = true;
					
				}
				else
				{
					$scope.changeStatus = true;
					$scope.changeStatus2 = false;
					
					//$scope.changeStatus = false;
				}
			};
			
			
			$scope.loadRyotCircleVillage = function(seedSrCode,season)
			{
				var seasonSplit = season.split("-");
					var fromYear = Number(seasonSplit[0])-Number(1);
					var toYear = Number(seasonSplit[1])-Number(1);
					var seasonyear = fromYear+"-"+toYear;
					
				var supAdr = new Object();
				supAdr.season = seasonyear;
				supAdr.supplierCode = seedSrCode;
				alert(supAdr);
				var httpRequest = $http({
				method: 'POST',				
				url : "/SugarERP/getCircleVillage.html",
				data: JSON.stringify(supAdr),
				}).success(function(data, status) 
				{
				$scope.AddSource.villageCode = data[0].villagecode;
				$scope.AddSource.circleCode=data[0].circlecode;
				}); 
			}
			
			$scope.getdisabledStatus = function(seedSrCode)
			{
				if(seedSrCode!=null)
				{
					$scope.disabled=true;
				}
				else
				{
					$scope.disabled=false;
				}
			};
			$scope.getotherRyotenableStatus = function(seedSrCode)
			{
				if(seedSrCode!=null)
				{
					$scope.isDisabled=true;
				}
				else
				{
					$scope.isDisabled=false;
				}
			};
			//added on 24-2-2017
			$scope.getAgreementNumberByConsumerCode = function(season,ryotcode)
			{
				
				var consumerCodeObj = new Object();
				consumerCodeObj.season = season;
				consumerCodeObj.ryotcode = ryotcode;
			//	alert(JSON.stringify(consumerCodeObj));
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getaggrementDetailsForConsumer.html",
							data: JSON.stringify(consumerCodeObj),
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
								$scope.agreementnumberNames = data;
							
						}); 
			};

			$scope.physicalConditionNames = [{ id: 'Good', pc: 'Good' }, { id: 'Bad', pc: 'Bad' }, { id: 'Normal', pc: 'Normal' }];
			
			
		$scope.getAddedSorceDropDown = function(batchno)
			{
                 var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getseedsourceChangeSummary.html",
							data: batchno,
					   }).success(function(data, status) 
						{
								//alert(JSON.stringify(data));
								$scope.batchDates = data;
								//$scope.AddSource.ryotName = data[0].Otherryotname;

						}); 
			}
				
				$scope.loadRyotCodeDropDown = function()
				{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							$scope.ryotCode = data;
							
							//$scope.AddSource = {'purpose':'0','seedType':'0','bm':'10','modifyFlag':'No','acreage':'1','updateType':'0','netWeight':'0','caneWeight':'0','lorryWeight':'0','finalWeight':'0','seedCostPerTon':'2725.00','modifyFlag1':'No'};
						$scope.AddSource = {'purpose':'0','seedType':'0','bm':'10','modifyFlag':'No','acreage':'1','updateType':'0','seedCostPerTon':'2725.00','modifyFlag1':'No','transactionCode':'0','prvConsumer':''};
							var seedType = 0;
							$scope.loadbatchNo(seedType);
							$scope.changeStatus = true;
							$scope.changeStatus2 = false;
							
							
							
							
						});  	   
				};
				
				
				
				$scope.decimalpoint = function(b)
			{
				if($scope.AddSource[b]!=null)
				{
				var decmalPoint = $scope.AddSource[b];				
				var getdecimal = parseFloat(Math.round(decmalPoint * 100) / 100).toFixed(3);
				$scope.AddSource[b] = getdecimal;
				}
			};
			
			$scope.decimalpointRupees = function(b)
			{
				if($scope.AddSource[b]!=null)
				{
					var decmalPointRupees = $scope.AddSource[b];				
				var getdecimal = parseFloat(Math.round(decmalPointRupees * 100) / 100).toFixed(2);
				$scope.AddSource[b] = getdecimal;	
				}
				
			};
				
				$scope.getOtherRyot = function()
				{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getOtherRyots.html",
							data: {},
					   }).success(function(data, status) 
						{
								
							$scope.otherRyotNames = data;
						});  	 
					
				};

				$scope.getOtherRyotName = function(otherryotcode,changdpStatus)
				{
					
					if(otherryotcode!=null)
					{
						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getOtherRyotsnames.html",
							data: otherryotcode,
					   }).success(function(data, status) 
						{
							$("#otherRyotVillage").show();
							$scope.changdpStatus = false;
							$scope.otherRyotVillageMandatory=true;
								
								$scope.AddSource.ryotName = data[0].Otherryotname;
								$scope.AddSource.otherRyotVillage = data[0].village;

						});
					}
					else
					{
						$("#otherRyotVillage").hide();
						$scope.changdpStatus = true;
						$scope.otherRyotVillageMandatory=false;
						$scope.AddSource.ryotName = "";
						$scope.AddSource.otherRyotVillage="";
					}
					  
				
				};

				$scope.updateTotalCostByAcreage = function(noofacre,costperacre,acreage)
				{
					if(acreage=='0')
					{
						if(costperacre==null || costperacre=="")
						{
							costperacre = 0;
						}
						if(noofacre==null || noofacre=="")
						{
							noofacre = 0;
						}
						if(noofacre!=null || costperacre!=null)
						{
							var totalCost = Number(noofacre)*Number(costperacre);
						$scope.AddSource.totalCost = totalCost;
							
						}
						
					}
					else
					{
						
					}
				};
				
			$scope.loadRyotDet = function(RyotCode)
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/GetRyotNameForCode.html",
			data: RyotCode,
			}).success(function(data, status) 
			{
				
				$scope.AddSource.ryotName = data[0].ryotName;
			});
			
			
		};
		$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
		};
		$scope.loadCircleNames = function()
		{			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCircleDropDown,
			   }).success(function(data, status) 
				{
					$scope.circleNames=data;
					
				}); 
		};
		
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		
	$scope.loadbatchNo = function(seedType,season)
	{
		var batchObj = new Object();
		batchObj.seedType = seedType;
		batchObj.season  = season;
		
		if(seedType==null || seedType=="")
		{
			seedType="0";
		}
		if(seedType!=null && seedType!='' && season!=null && season!='' )
		{
			//alert(JSON.stringify(batchObj));
			var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/GetMaxSourceOfSeed.html",
							data: JSON.stringify(batchObj),
					   }).success(function(data, status) 
						{							
							$scope.AddSource.batchNo=data[0].batchNo;
						});
		}
		
	};
	
	$scope.updateTotalCost = function(seedcostperTon,finalWeight)
	{
		if(seedcostperTon!=null && finalWeight!=null )
		{
			var updateTotalCost = Number(seedcostperTon)*Number(finalWeight);
	$scope.AddSource.totalCost = parseFloat(updateTotalCost*100/100).toFixed(2);
		}
	
	
	};
	
	$scope.getAllWeightmentDetails = function(batchNo)
	{
		
			var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getweighmentDetailsSeedSource.html",
							data: batchNo,
					   }).success(function(data, status) 
						{	
						
						if(data.length<1)
						{
							alert("No Data Found");
						}
						else
						{
							var tc=0;
							tc = parseFloat(tc);
							$scope.weightmentData = data;
						
							for(var i=0;i<data.length;i++)
							{ 
							$scope.weightmentData[i].totalcost = data[i].totalcost.toFixed(2);
							$scope.weightmentData[i].finalweight = data[i].finalweight.toFixed(3);
							
							tc = tc + parseFloat($scope.weightmentData[i].totalcost);
							// alert(data[i].totalcost.toFixed(2));
							// tc = tc + data[i].totalcost.toFixed(2);
							//totalcost = totalcost+$scope.weightmentData[i].totalcost;
							//alert(data[i].totalcost.toFixed(2));
							}
							
							
						$scope.GridTotal = tc.toFixed(2);
							 $('#gridpopup_box').fadeIn("slow");
						
						}
					
					
							 
							
						});
		
		 
		 
	};
	$scope.CloseDialogRatoon = function()
		{
			$('#gridpopup_box').fadeOut("slow");
		};
		
		$(document).keyup(function(e) {
     if (e.keyCode == 27) {
		
		 $('#gridpopup_box').fadeOut("slow");
		 
		
    }
});
		
	$scope.getAllSeedSources = function()
	{
		var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getYearSourceofSeeds.html",
							data: {},
					   }).success(function(data, status) 
						{	
						//alert(JSON.stringify(data));
							$scope.addedYearNames=data;
							
						});
	};
	$scope.getAddedBatchNo = function(addedYear,seedSrCode,otherRyot)
	{
		var addedbatchObj = new Object();
		addedbatchObj.addedYear = addedYear;
		addedbatchObj.seedSrCode = seedSrCode;
		addedbatchObj.otherRyot = otherRyot;
		if( seedSrCode!='' && seedSrCode!=null || otherRyot!='' && otherRyot!=null)
		{
			
		
		var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getbatchSourceofSeeds.html",
							data: JSON.stringify(addedbatchObj),
					   }).success(function(data, status) 
						{	
						
							$scope.addedBatchNames=data;
							$scope.AddSource.addedYear = addedYear;
							
						});
			
		}
		
		
	};
	
	$scope.getAddedSeedSource = function(addedYear,addedBatch,dateid)
	{
		addedSource.addedYear =addedYear;
		addedSource.addedBatch = addedBatch;
		addedSource.dateid =dateid;

		//alert(JSON.stringify(addedSource));
		var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllSeedSources.html",
							data: JSON.stringify(addedSource),
					   }).success(function(data, status) 
						{	
							//alert(JSON.stringify(data));
								$scope.AddSource=data[0];
								/*  ADDED BY NAIDU
								if(data[0].purpose==1)
								{
						
								var consumerSplit = data[0].consumerCode.split(",");
								
							  var consumerCodeNew = [];
							  for(var s=0;s<consumerSplit.length-1;s++)
							  {
									consumerCodeNew.push(consumerSplit[s]);							
							  }/
						
							  $scope.AddSource.consumerCode = consumerCodeNew;
								}
								*/
								//alert(data[0].circleCode+"  "+data[0].aggrementNo);
								 $scope.AddSource.consumerCode =data[0].consumerCode;
								$scope.AddSource.circleCode=data[0].circleCode;
								$scope.AddSource.seedSrCode=data[0].seedSrCode;	
								$scope.getAgreementNumberByConsumerCode(data[0].season,data[0].consumerCode)
								$scope.AddSource.aggrementNo=data[0].aggrementNo;
								$scope.AddSource.prvConsumer=data[0].consumerCode;
								$scope.loadRyotDet($scope.AddSource.seedSrCode);
								$scope.AddSource.dates =dateid;
								/*var httpRequest = $http({
								method: 'POST',
								url : "/SugarERP/GetRyotNameForCode.html",
								data: data[0].seedSrCode,
								}).success(function(data, status) 
								{
									
									$scope.AddSource.ryotName = data[0].ryotName;
								});*/
							//alert(JSON.stringify(data[0]));
							

							
							
						});
		
		
	};
	   


	   $scope.getBacthNo=function(batchno,addedYear){
		var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getseedsourceLoadSummary.html",
							data: batchno,
					   }).success(function(data, status) 
						{	
						
							//alert("batchno"+JSON.stringify(data[0].SeedType));

						if(data[0].SeedType=='S'){

														$scope.AddSource = {'purpose':'0','seedType':'0','bm':'10','modifyFlag':'No','acreage':'1','updateType':'0','netWeight':'0','caneWeight':'0','lorryWeight':'0','finalWeight':'0','seedCostPerTon':'2725','modifyFlag1':'Yes'};
							}else if(data[0].SeedType=='V'){
														$scope.AddSource = {'purpose':'0','seedType':'1','bm':'10','modifyFlag':'No','acreage':'1','updateType':'0','netWeight':'0','caneWeight':'0','lorryWeight':'0','finalWeight':'0','seedCostPerTon':'2725','modifyFlag1':'Yes'};

							}else if(data[0].SeedType=='F'){
														$scope.AddSource = {'purpose':'0','seedType':'2','bm':'10','modifyFlag':'No','acreage':'1','updateType':'0','netWeight':'0','caneWeight':'0','lorryWeight':'0','finalWeight':'0','seedCostPerTon':'2725','modifyFlag1':'Yes'};
							}

						$scope.AddSource.addedYear=addedYear;
						$scope.AddSource.batchNo=data[0].batchseries;
						$scope.AddSource.villageCode=data[0].landvillagecode;
						$scope.AddSource.season=data[0].season;
						$scope.AddSource.seedSrCode=data[0].seedsuppliercode;
						$scope.loadRyotDet($scope.AddSource.seedSrCode);
						$scope.AddSource.varietyCode=data[0].varietycode;
						$scope.AddSource.harvestingCharges = data[0].harvestingCharges;
						$scope.AddSource.transCharges = data[0].transCharges;
						$scope.AddSource.certDate = data[0].certDate;
						$scope.AddSource.circleCode = data[0].circleCode;
						$scope.AddSource.otherRyotVillage = data[0].otherRyotVillage;
						//$scope.AddSource.date = data[0].date;
						$scope.AddSource.avgBudsPerCane = data[0].avgBudsPerCane;
						$scope.AddSource.ageOfCrop = data[0].ageOfCrop;
						$scope.AddSource.physicalCondition = data[0].physicalCondition;
						$scope.AddSource.seedCertifiedBy = data[0].seedCertifiedBy;
						//code added on 24-2-2017
						$scope.AddSource.consumerCode = data[0].consumercode;
						$scope.getAgreementNumberByConsumerCode(data[0].season,data[0].seedsuppliercode);
						$scope.AddSource.aggrementNo = data[0].agreementno;
						$scope.AddSource.purpose = data[0].purpose;

						var fileloc = data[0].fileLocation;
						//alert(fileloc);
						$('#blah').attr('src', "../"+fileloc);
			
								
								});
	   
	   };
		
		
		$scope.SourceofSeedSubmit = function(AddSource,form)
		{
		//	alert("entered");
			//04-03-2017 naidu
			/*
			var consumerString = "";
			if($scope.AddSource.purpose==1)
			{
			 var consumerCode = $scope.AddSource['consumerCode'];
					
					for(var s=0;s<consumerCode.length;s++)
					{
						  consumerString +=consumerCode[s]+",";
					}	
					delete AddSource['consumerCode'];
					//var sourceseed=new Object();
					//sourceseed.formData = AddSource;
					//sourceseed.consumerCode = consumerString;
					
			}
			*/
				obj = JSON.stringify(AddSource);
				alert(JSON.stringify(obj));
			  if($scope.SourceofSeedForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
				cusImage.append("formData", obj);
				//cusImage.append("consumerCode", consumerString);
				
				$.ajax({
					   url         : "/SugarERP/saveSeedSource.html",
					   data        : cusImage,
					   contentType : false,
					   processData : false,
					   type        : 'POST'
				   }).success(function(data, status){   
				   alert(JSON.stringify(data));
				    if(data=="true")
						{
							swal("Success!", 'Source of Seed done successfully!', "success");  //-------For showing success message-------------			
							$('.btn-hide').attr('disabled',false);
							
							//$scope.loadbatchNo(seedType);
							//$scope.AddSource = angular.copy($scope.master);
							//$scope.AddSource = {};
							//form.$setPristine(true);
							
						//	$scope.reset(form);
							//alert("form cleared");

							$scope.AddSource = angular.copy($scope.master);
							form.$setPristine(true);
							$scope.loadRyotCodeDropDown();

							$scope.getAllSeedSources();
							$scope.AddSource.addedYear={};
							$scope.addedBatch={};
							 setTimeout(function(){ location.reload(); }, 1000);
							//

														 //--------------end---------
						 }
						else if(data=="duplicateBatch")
						 {
						   sweetAlert("Oops...", "Some one try to insert same Batch wait and update Again!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
						 else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				   
				   
				  // alert('data');
				   });
				
				

			/*  $http.post("/SugarERP/saveSeedSource.html", cusImage, {
						        withCredentials: true,
					        	 headers: {'Content-Type': undefined },
						       	 transformRequest: angular.identity
							  }).success(function(data, status) 
						 	   { 								  // alert(data);

							   if(data==true)
						{
							swal("Success!", 'Source of Seed done successfully!', "success");  //-------For showing success message-------------			
							$('.btn-hide').attr('disabled',false);
							
							//$scope.loadbatchNo(seedType);
							//$scope.AddSource = angular.copy($scope.master);
							//$scope.AddSource = {};
							//form.$setPristine(true);
							
						//	$scope.reset(form);
							//alert("form cleared");

							$scope.AddSource = angular.copy($scope.master);
							form.$setPristine(true);
							$scope.loadRyotCodeDropDown();

							$scope.getAllSeedSources();
							$scope.AddSource.addedYear={};
							$scope.addedBatch={};
							 setTimeout(function(){ location.reload(); }, 1000);
							//

														 //--------------end---------
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
							   })*/
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}
		};
		 $scope.reset = function(form)
		   {	
		   
		  $('.btn-hide').attr('disabled',false);
				$scope.AddSource = angular.copy($scope.master);
				form.$setPristine(true);
				$scope.loadRyotCodeDropDown();
				$scope.addedYear ="";
				$scope.addedBatch ={};
				
		   };
		   $scope.getNetWeight = function(caneWeight,lorryWeight)
		   {
			  
			   if(lorryWeight!=null && caneWeight!=null)
			   {
				   var netWeight = Number(caneWeight)-Number(lorryWeight);
					   $scope.AddSource.netWeight = parseFloat(Math.round(netWeight * 100) / 100).toFixed(3);
			   }
			   else
			   {
				  $scope.AddSource.netWeight = 0; 
			   }
					 
			};
					   
		   $scope.getBm = function(netWeight,bm)
		   {
			
			if(netWeight!=null && bm!=null)
			{
				var netWeightbm = Number(netWeight)*(bm/100);
			  
			   Number(netWeight)-Number(netWeightbm);
			   var finalWeight = Number(netWeight)-Number(netWeightbm);
			   $scope.AddSource.finalWeight = parseFloat((finalWeight)*100/100).toFixed(3);
			   
			   if($scope.AddSource.seedCostPerTon!=null || $scope.AddSource.seedCostPerTon)
							{
								if($scope.AddSource.finalWeight!=null || $scope.AddSource.finalWeight)
								{
									var updateTotalCost = Number($scope.AddSource.seedCostPerTon)*Number($scope.AddSource.finalWeight);
							$scope.AddSource.totalCost = parseFloat(updateTotalCost*100/100).toFixed(2);
								}
							}	
			}
			   
		   };
		   
		    $scope.getBm1 = function()
		   {
			   var netWeight=$scope.AddSource.netWeight;
			   var bm=$scope.AddSource.bm;
			alert('netwt'+netWeight+' bm '+bm);
			if(netWeight!=null && bm!=null)
			{
				var netWeightbm = Number(netWeight)*(bm/100);
			  
			   Number(netWeight)-Number(netWeightbm);
			   var finalWeight = Number(netWeight)-Number(netWeightbm);
			   $scope.AddSource.finalWeight = parseFloat((finalWeight)*100/100).toFixed(3);
			   
			   if($scope.AddSource.seedCostPerTon!=null || $scope.AddSource.seedCostPerTon)
							{
								if($scope.AddSource.finalWeight!=null || $scope.AddSource.finalWeight)
								{
									var updateTotalCost = Number($scope.AddSource.seedCostPerTon)*Number($scope.AddSource.finalWeight);
							$scope.AddSource.totalCost = parseFloat(updateTotalCost*100/100).toFixed(2);
								}
							}	
			}
			   
		   };
		   
				///image code////
				var cusImage = new FormData();
			$scope.uploadUserFile = function(files) 
				{
					//$scope.dummySignature="1";
					$('#dummySignature').trigger('blur');
					cusImage.delete("userphoto");
		    		cusImage.append("userphoto", files[0]);
				};				
		
				
		})
		.controller('DetrashingController', function($scope,$http)
		 {
			
			 
			 var obj = new Object();

 			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
		//	alert(jsonStringSeasonDropDown);

			  var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);//
//alert(jsonCaneWeightSeasonString);
	 	var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			$scope.master = {};
		
	 
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
					//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddedDetrashing = {'labourSpplr':'0','modifyFlag':'No','gridCount':1,'manCost':'300.00','womenCost':'200.00'};
			}); 		
		};
		
		$scope.getotalCost = function(id,Wttrash)
			{
			
				 $scope.data[id-1].wtWithoutTrash= Wttrash;
				
					  var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount = parseFloat($scope.data[s].wtWithoutTrash)+parseFloat(totalAmount);
					  }
					 
//					  $scope.AddedDetrashing.wtWithoutTrash = parseFloat(totalAmount);
					  $scope.AddedDetrashing.totalCaneWt = totalAmount.toFixed(3);
				
				
			
			};

			
			$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedDetrashing.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };


        $scope.getAddedDetrashingList=function(season,date,shift)
			 {
			var detrashingListobj = new Object();
			detrashingListobj.season=season;
			detrashingListobj.shift=shift;
			detrashingListobj.detrashingDate=date;
              
			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  //alert(JSON.stringify(detrashingListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllDetrashingDetails.html",
					data: JSON.stringify(detrashingListobj),
					  }).success(function(data, status) 
					{
						alert(JSON.stringify(data));
						$scope.AddedDetrashing=data.DetachSummary;
						$scope.AddedDetrashing.totalCaneWt = data.DetachSummary.totalCaneWt.toFixed(3);
						$scope.data= data.DetachDetails;
						$scope.getLabourContractorDetails();

					});
				 }
				
					

			 };

		$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			
			
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
		};
		$scope.getaddRow  = function(addRowValues)
		{
			
			if(addRowValues.trim()!= "" || addRowValues.trim()!=null )
			{
				$("#getAddRowGrid").show();
			}
			if(addRowValues.trim()=="")
			{
				
				$("#getAddRowGrid").hide();
			}
		};
		
		$index=0;
			$scope.data=[];
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				//alert($index);
				
			};
		
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
			
			//$scope.data[id-1].wtWithoutTrash= Wttrash;
				
					  

           var totalAmount = 0;
			for(var k=0;k<$index;k++)
			{
				//alert($scope.data[k].totalCost);
			     totalAmount = parseFloat($scope.data[k].wtWithoutTrash)+parseFloat(totalAmount);
				 
			}
				//  alert(totalAmount);
				$scope.AddedDetrashing.wtWithoutTrash = totalAmount.toFixed(3);
			 $scope.AddedDetrashing.totalCaneWt = totalAmount.toFixed(3);
			
			
			
			
  	    };
			
			
		
			$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				//alert(batchNumber);
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
						//	alert(JSON.stringify(data));
							var g= gridCount-Number(1);
							$scope.data[g].landVillageCode = data[0].landVillageCode;
							$scope.data[g].seedSupr = data[0].seedSupr;
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
							$scope.data[g].ryotName = data[0].ryotName;
							$scope.data[g].lvName = data[0].LVName;
						});
					}
				}); 
			 };


				$scope.clearTotalforDetrashing = function(val)
				{
					$scope.AddedDetrashing.totalContractAmt="";
				}
			 $scope.getGrandTotalforDetrashing = function(menTotal,womenTotal,totalTons,labourSpplr)
			 {
				 
				 if(labourSpplr==0)
				 {
					 $scope.AddedDetrashing.totalContractAmt="";
					  if(womenTotal==null || womenTotal=="")
				 	  {
					 	womenTotal=0;
				 	  }
					   if(menTotal==null || menTotal=="")
				 	  {
					 	menTotal=0;
				 	  }
					 
					var grandTotal = Number(menTotal)+Number(womenTotal);
				
				$scope.AddedDetrashing.totalLabourCost = grandTotal.toFixed(2); 
				 }
				 else
				 {
					 $scope.AddedDetrashing.totalContractAmt="";  
					  if(totalTons==null || totalTons=="")
				   {
					 totalTons=0;
				   } 
				   var grandTotal = Number(totalTons);
				   $scope.AddedDetrashing.totalContractAmt = grandTotal.toFixed(2); 
				  
				 }
				 
				
				
				
			 };
			 
			 $scope.getContractTotalCost = function(costPerTon,totalTons)
			 {
				 if(costPerTon==null || costPerTon=="")
				 {
					 costPerTon =0;
				 }
				 if(totalTons==null || totalTons=="")
				 {
					 totalTons = 0;
				 }
				var totalLabourCost = Number(costPerTon)*Number(totalTons);
				
				$scope.AddedDetrashing.totalLabourCost = totalLabourCost.toFixed(2);
				$scope.AddedDetrashing.totalContractAmt = totalLabourCost.toFixed(2);
			 };
			

			$scope.DetrashSubmit = function(AddedDetrashing,form)
			 {
			
				if($scope.DetrashingForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (batchData) 
					  {
						  

	            		 gridData.push(angular.toJson(batchData));														 
				      });
					  obj.formData = angular.toJson(AddedDetrashing);
					  obj.gridData = gridData;
				      
				     // delete obj["$index"];
					  alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveDetrashing.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Detrashing Done Successfully!', "success");
							//	$scope.reset(form);
								
							$scope.reset(form);
									
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };
		 $scope.reset = function(form)
		 {
				$scope.AddedDetrashing = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadShift();
				  $index=1;
					$scope.data = [{'id':$index}]


		 };
		 $scope.LoadWoManTotal =function(people,cost)
			{
			 if(people==null || people=='')
				{
					people = 0;
				}
				if(cost==null || cost=='')
				{
					cost = 0;
				}
			var total=Number(people)*Number(cost);
				$scope.AddedDetrashing.womenTotal=total.toFixed(2);
			};

			$scope.LoadManTotal =function(people,cost)
			{
				if(people==null || people=='')
				{
					people = 0;
				}
				if(cost==null || cost=='')
				{
					cost = 0;
				}
			var total=Number(people)*Number(cost);
				$scope.AddedDetrashing.menTotal=total.toFixed(2);
			};
			
			$scope.deductTrash =function(id,weightwithtrash)
			{
			var total=Number(weightwithtrash)-(Number(weightwithtrash)*(10/100));
			$scope.data[id-1].wtWithoutTrash=parseFloat(Math.round(total*1000)/1000).toFixed(3);
			$scope.getotalCost(id,$scope.data[id-1].wtWithoutTrash);
				//alert($scope.batchData.wtWithoutTrash);
			};
			
	
			
		 })

		 .controller('GradingController', function($scope,$http)
		{
			$index=0;
			
			$scope.data=[];
			
			var obj = new Object();
			
			 var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
	 
	 	var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			$scope.master = {};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				$scope.WeighBridgeNames=data;
				$scope.AddedGrading = {'labourSpplr':'0','modifyFlag':'No','manCost':'300.00','womenCost':'200.00'};
			}); 		
		};
		
		$scope.getGermination = function(transferToTrayFilling,totalBuds,id)
		{
			var germination  = Number(transferToTrayFilling/totalBuds)*100;
			if( transferToTrayFilling==null || transferToTrayFilling=='')
			{
				transferToTrayFilling =1;
			}
			if(totalBuds==null || totalBuds=="")
			{
				totalBuds=1;
			}
			if(transferToTrayFilling!='' && transferToTrayFilling!=null && totalBuds!='' && totalBuds!=null  )
			{
					var germinationperc  = Math.round(germination*100)/100;
					 $scope.data[id-1].germinationPerc = germinationperc;
			}
		};
		
		$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedGrading.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };


		
		$scope.getdisposedtoRejection = function(totalNofBuds,transfertoTrayFilling,sentBack,id)
		{
			if(totalNofBuds==null || totalNofBuds=='')
			{
				totalNofBuds =0;
			}
			if(transfertoTrayFilling==null || transfertoTrayFilling=='')
			{
				transfertoTrayFilling =0;
			}
			if(sentBack==null || sentBack=='')
			{
				sentBack =0;
			}
			var getdisposedtoRejection = Number(totalNofBuds)-Number(transfertoTrayFilling)-Number(sentBack);
			
			$scope.data[id-1].disposedToRejection = getdisposedtoRejection;
		};
			
			$scope.loadseasonNames = function()
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
				}); 
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
			};
			$scope.loadVillageNames = function()
			{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
			};
			
			$scope.LoadWoManTotal = function(people,cost)
			{
				
				if(people==null || people=="")
				{
					people=0;
				}
				if(cost==null || cost=="")
				{
					cost=0;
				}
					var total=Number(people)*Number(cost);
				$scope.AddedGrading.womenTotal=total.toFixed(2);
			};

			$scope.LoadManTotal =function(people,cost)
			{
				if(people==null || people=="")
				{
					people=0;
				}
				if(cost==null || cost=="")
				{
					cost=0;
				}
				
			var total=Number(people)*Number(cost);
				$scope.AddedGrading.menTotal=total.toFixed(2);
			};
			$scope.getGrandTotalforGrading = function(menTotal,womenTotal)
			{
				if(menTotal==null || menTotal=="")
				{
					menTotal =0;
				}
				if(womenTotal==null || womenTotal=="")
				{
					womenTotal =0;
				}
				
				
				var total = Number(menTotal)+Number(womenTotal);
				$scope.AddedGrading.totalLabourCost = total.toFixed(2);
			};
			$scope.getContractTotalCost= function(costPerTon,totalTons)
			{
				if(costPerTon==null || costPerTon=="")
				{
					costPerTon =0;
				}
				if(totalTons==null || totalTons=="")
				{
					totalTons =0;
				}
				var totalTon = Number(costPerTon)*Number(totalTons);
				
				$scope.AddedGrading.totalContractAmt = totalTon.toFixed(2);
			};
			
			
			$scope.GradingSubmit = function(AddedGrading,form)
			 {
			
			
				if($scope.gradingForm.$valid) 
				{	
					gridData = [];	
					  $scope.data.forEach(function (batchData) 
					  {
						  

	            		 gridData.push(angular.toJson(batchData));														 
				      });
					  obj.formData = angular.toJson(AddedGrading);
					  obj.gridData = gridData;
					   
				      
				     
					//  alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveSpourtGrading.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Grading Done Successfully!', "success");
								
								$scope.AddedGrading = angular.copy($scope.master);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true); 
								$scope.loadShift();
								$index=1;
								$scope.data = [{'id':$index}]
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };

		 $scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
					//alert(JSON.stringify(data));
						var g= gridCount-Number(1);
							$scope.data[g].landVillageCode = data[0].landVillageCode;
							$scope.data[g].seedSupr = data[0].seedSupr;
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
							$scope.data[g].ryotName = data[0].ryotName;
							$scope.data[g].lvName = data[0].LVName;
						});
					}
				}); 
			 };
			 
			
			 $scope.getAddedGradingList=function(season,date,shift)
			 {
			var gradingListobj = new Object();
			gradingListobj.season=season;
			gradingListobj.shift=shift;
			gradingListobj.gradingDate=date;
              
			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  //alert(JSON.stringify(gradingListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllSpourtGradingDetails.html",
					data: JSON.stringify(gradingListobj),
					  }).success(function(data, status) 
					{//alert(JSON.stringify(data));
						$scope.AddedGrading=data.GradingSummary;
						$scope.data= data.GradingDetails;
						$scope.getLabourContractorDetails();
					});
				 }
				
					

			 };




			 $scope.reset = function(form)
				 {
						$scope.AddedGrading = angular.copy($scope.master);
						$('.btn-hide').attr('disabled',false);
						form.$setPristine(true); 
						 $scope.loadShift();
						  $index=1;
							$scope.data = [{'id':$index}]


				 };
		
		
		})



.controller('greenHouseController', function($scope,$http)
		 {
			 
			$index=0;
			
			$scope.data=[];
			$scope.master = {};
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
			
			var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};

			$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedHardening.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };

			$scope.getTotalSeedlingsIn = function(noOfTraysIn,cavities,id)
			{
				
				var totalSeedlingsIn = Number(noOfTraysIn)*Number(cavities);
				$scope.data[id-1].totalSeedlingsIn = totalSeedlingsIn;
				
				
				 		var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsIn;
					  }
					  $scope.AddedHardening.grandTotalIn = Number(parseFloat(totalAmount));
				
				
				
				
			};
			
				$scope.getTotalSeedlingsOut = function(noOfTraysOut,cavities,id)
			{
				
				var totalSeedlingsOut = Number(noOfTraysOut)*Number(cavities);
				$scope.data[id-1].totalSeedlingsOut = totalSeedlingsOut;
				
					var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsOut;
					  }
					  $scope.AddedHardening.grandTotalOut = Number(parseFloat(totalAmount));
			};
			
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				
				$scope.WeighBridgeNames=data;
				
			}); 		
		};
		
		$scope.getLabourGrandTotal = function(man,woman)
			{
				var labourTotal=0;
				
				if(man==null || man=="")
				{
					man=0;
				}
				if(woman==null || woman=="")
				{
					woman=0;
				}
				
				labourTotal = Number(man)+Number(woman);
				$scope.AddedHardening.totalLabourCost = labourTotal.toFixed(2);
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
				
				 var totalAmountIn = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountIn += $scope.data[k].totalSeedlingsIn;
			}
			
			 var totalAmountOut = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountOut += $scope.data[k].totalSeedlingsOut;
			}
				
			$scope.AddedHardening.grandTotalIn = totalAmountIn;
			$scope.AddedHardening.grandTotalOut = totalAmountOut;
			
  	    	};
			
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
			};
			
			
			$scope.getMenTotal  = function(man,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedHardening.manCost = cost;
				}
				if(man==null || man=="")
				{
					man=0;
					//$scope.AddedHardening.manCost = cost;
				}
				var totalMan = Number(man) * Number(cost);
				$scope.AddedHardening.menTotal = totalMan.toFixed(2);
			};
			$scope.getWomanTotal  = function(woman,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				if(woman==null || woman=="")
				{
					woman=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				
				var totalWoman = Number(woman) * Number(cost);
				$scope.AddedHardening.womenTotal = totalWoman.toFixed(2);
			};
			
			
			$scope.getContractTotal = function(cost,Totaltons)
			{
				
				if(Totaltons==null || Totaltons=="")
				{
					Totaltons =0;
				}
				if(cost==null || cost=="")
				{
					cost =0;
				}
				grandContractTotal = Number(cost)*Number(Totaltons);
				$scope.AddedHardening.totalContractAmt = grandContractTotal.toFixed(2);
				
				
			};
			
		$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchDetailsNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchSeriesNoDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
								
							var g= gridCount-Number(1);
							
							$scope.data[g].variety = data[0].variety;

							$scope.data[g].varietyName = data[0].varietyName;
						});
					}
				}); 
			 };
			 
			 
			$scope.loadVillageNames = function()
			{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
			};
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
					$scope.AddedHardening = {'labourSpplr':'0','modifyFlag':'No','manCost':'300.00','womenCost':'200.00'};
					
				}); 
			};
			
			var obj = new Object();
			
				$scope.HardeningSubmit = function(AddedHardening,form)
				{
					
					if($scope.hardeningForm.$valid)
					{
						  gridData = [];	
									  $scope.data.forEach(function (batchData) 
									  {
										  
				
										 gridData.push(angular.toJson(batchData));														 
									  });
									  obj.formData = angular.toJson(AddedHardening);
									  obj.gridData = gridData;
									  
									
								//	 alert(JSON.stringify(obj));
									$('.btn-hide').attr('disabled',true);
									$.ajax({
										url:"/SugarERP/saveGreenHouseDetails.html",
										processData:true,
										type:'POST',
										contentType:'Application/json',
										data:JSON.stringify(obj),
										beforeSend: function(xhr) 
										{
											xhr.setRequestHeader("Accept", "application/json");
											xhr.setRequestHeader("Content-Type", "application/json");
										},
										success:function(response) 
										{
											if(response==true)
											{
												swal("Success", 'Green House Details Added Successfully!', "success");
											//	$scope.reset(form);
													$scope.AddedHardening = angular.copy($scope.master);
													$('.btn-hide').attr('disabled',false);
													form.$setPristine(true); 
													$scope.loadseasonNames();
													$index=1;
													$scope.data = [{'id':$index}]
										
													
												
											}
										   else
											{
												sweetAlert("Oops...", "Something went wrong!", "error");
											   $('.btn-hide').attr('disabled',false);
													
											}
										}
									});	
					}
					else
								{
										var field = null, firstError = null;
										for (field in form) 
										{
											if (field[0] != '$') 
											{
												if (firstError === null && !form[field].$valid) 
												{
													firstError = form[field].$name;
												}
												if (form[field].$pristine) 
												{
													form[field].$dirty = true;
												}
											}	
										}
								}
				};
				
				
			$scope.getHardeningList = function(season,date,shift)
			{
				
				var hardeningListobj = new Object();
				hardeningListobj.season=season;
				hardeningListobj.shift=shift;
				hardeningListobj.summaryDate=date;
            

			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				 //  alert(JSON.stringify(hardeningListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllGreenHouseDetails.html",
					data: JSON.stringify(hardeningListobj),
					  }).success(function(data, status) 
					  {
						//alert(JSON.stringify(data.GreenHouseSummary));
						$scope.AddedHardening=data.GreenHouseSummary;
						$scope.data= data.GreenHouseDetails;
						$scope.getLabourContractorDetails();
					 });
				 }
				
				
			};
			
			  	$scope.reset = function(form)
		 		{
					$scope.AddedHardening = angular.copy($scope.master);
					$('.btn-hide').attr('disabled',false);
					form.$setPristine(true); 
					$scope.loadseasonNames();
					$index=1;
					$scope.data = [{'id':$index}]
		 		};
			
		 })
		
		
		.controller('plantUpdateController',function($scope,$http){
					$scope.master = {};			
					var obj = new Object();
					
 			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "Employees";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
		 
		 
		 var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			
			
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
		 $index=0;
		$scope.data=[];
			$scope.addFormField = function() 
			{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						 //alert(data.serverdate);
				$index++;
		    	$scope.data.push({'id':$index,'dateofPlanting':data.serverdate})
				
					 })
			};
				$scope.PlantUpdateSubmit = function(plantupdate,form)
				{
					//alert($scope.plantingForm.$valid);
					if($scope.plantingForm.$valid)
					{
						  gridData = [];	
									  $scope.data.forEach(function (plantData) 
									  {
										  
				
										 gridData.push(angular.toJson(plantData));														 
									  });
									  obj.season = plantupdate.season;
									   obj.fieldMan = plantupdate.fieldMan;
									    obj.dateOfEntry = plantupdate.dateOfEntry;
									  obj.gridData = gridData;
									  
									 // delete obj["$index"];
									 //alert(JSON.stringify(obj));
									
									$.ajax({
										url:"/SugarERP/SavePlantingDetails.html",
										processData:true,
										type:'POST',
										contentType:'Application/json',
										data:JSON.stringify(obj),
										beforeSend: function(xhr) 
										{
											xhr.setRequestHeader("Accept", "application/json");
											xhr.setRequestHeader("Content-Type", "application/json");
										},
										success:function(response) 
										{
											if(response==true)
											{
												swal("Success", 'Planting Details Updated!', "success");
											//	$scope.reset(form);
												
													//$scope.AddedHardening = angular.copy($scope.master);
												//$('.btn-hide').attr('disabled',false);
												//form.$setPristine(true); 
												//$scope.loadseasonNames();
												//$index=1;
												//$scope.data = [{'id':$index}]													
												
											}
										   else
											{
												sweetAlert("Oops...", "Something went wrong!", "error");
											   $('.btn-hide').attr('disabled',false);
													
											}
										}
									});	
					}
					else
								{
										var field = null, firstError = null;
										for (field in form) 
										{
											if (field[0] != '$') 
											{
												if (firstError === null && !form[field].$valid) 
												{
													firstError = form[field].$name;
												}
												if (form[field].$pristine) 
												{
													form[field].$dirty = true;
												}
											}	
										}
								}
				};
							
						$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};			
				
				$scope.loadusername=function(){
					
					 var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						var serverdate=data.serverdate;
					
					
					
					
					var loginobj=new Object();
					var username = sessionStorage.getItem('username');
					loginobj.loginId=username;
					//alert(JSON.stringify(loginobj));
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getLoginNameFromEmployeeId.html",
					data: loginobj,
			   }).success(function(data, status) 
			   
			   {
				  //$scope.loadserverdate();  
				  // alert(JSON.stringify(data[0].fieldOfficer));
				  $scope.plantupdate={'fieldOfficer':data[0].fieldOfficer,'dateOfEntry':serverdate};
				 
			   })
			    })
				};
				
				$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
			 
					$scope.FieldOffNames=data;
	 		   });  	   
		};
		
		
			
			
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
			
			
			
			$scope.loadRyotCodeDropDown = function()
				{
					//alert(ExtentRyotCode);
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							//alert("alertnotcoming");
							//alert(JSON.stringify(data));	
							$scope.ryotCodeData = data;
						});  	   
				};
				
				
				$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		
		$scope.ShowDatePicker = function()

{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		 $scope.reset = function(form)
		 {
				$scope.plantupdate = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadusername();
				
				  $index=1;
					$scope.data = [{'id':$index}]


		 };
		 

				
								
		})
		
		
	//====================Authorization===========================
	.controller('authorisationController', function($scope,$http,$rootScope,$state)
		{
			$index=0;
			$scope.authorizedData=[];
			var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "FieldOfficer";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
			
		
		
 			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
		
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);

				var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
			
			
		var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";
			
		 
		 var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.seasonNames=data;
				}); 
			};
			
		//Added by sahadeva 20-02-2017
			$scope.loadStoreCode = function()
		{  
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStoreAuthorization.html",
				data: {},
			   }).success(function(data, status) 
				{
				$scope.storeCodeData = data;
					//alert(JSON.stringify(data));
					
				});
		};

			$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 //  alert(JSON.stringify(data));
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};
	
			$scope.getfofaseedlingQty =function(agreement,season)
				{
						var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = agreement;
						
						 if(season!='' && season!=null && agreement!='' && agreement!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getExtraColumnAgrExtent.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									//27-02-2017
									$scope.authorized.noOfAcr = data.noOfAcr;
									//01-03-2017 
									$scope.authorized.individualExtent =data.individualExtent;
									});
									
									
							}
							
				};
			
			
			$scope.getaggrementDetails = function(season,ryotcode)
			{
				
			var agrobj=new Object();
			agrobj.season=season;
			agrobj.ryotcode=ryotcode;
					var httpRequest = $http
					({
					method: 'POST',
					url : "/SugarERP/getaggrementDetailsauth.html",
					data: JSON.stringify(agrobj),
					}).success(function(data, status) 
					{
						
					
					$scope.agreementnumberNames = data;
					
					$scope.authorized.agreementnumber = data[0].agreementnumber;
					
					
					// extra code
					
					
					var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = data[0].agreementnumber;
						 if(season!='' && season!=null && data[0].agreementnumber!='' && data[0].agreementnumber!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getExtraColumnAgrExtent.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									alert("data of Extent"+JSON.stringify(data));
									//27-02-2017
									$scope.authorized.noOfAcr = data.noOfAcr;
									//01-03-2017
									$scope.authorized.individualExtent =data.individualExtent;
									});
									
							}
					
					
						
					});
			};
			
			
			
		 
		$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
			 
					$scope.FieldOffNames=data;
	 		   });  	   
		};
		
		 
			$scope.addFormField = function()
			{
				$index++;
				
					$scope.authorizedData.push({'id':$index})
			};
		
			
			$scope.removeRow = function(name)
			{	
			
				var Index = -1;		
				var comArr = eval( $scope.authorizedData );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.authorizedData.splice( Index, 1 );
				$index = comArr.length;
				
				for(var s=0;s<$index;s++)
				{
					$scope.authorizedData[s].id = Number(s)+Number(1);
				}
				
				var grandTotalCost = 0;
					
						for($i=0;$i<$index;$i++)
						{
							grandTotalCost = grandTotalCost+$scope.authorizedData[$i].totalCost;
						}
						
						$scope.authorized.grandTotal = grandTotalCost.toFixed(2);
			
  	    	};
		
		
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		
		
		$scope.loadKindType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllKindTypeDetails.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.kindType=data;
					});

			};
			
			
		$scope.loadusername=function(){
			
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
		var username = sessionStorage.getItem('username');
		$scope.authorized={'authorisedBy':username,'authorisationDate':data.serverdate};
		
					 })
		
		}
		
		$scope.loadRyotCodeDropDown = function()
				{
					//alert(ExtentRyotCode);
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
												
							$scope.ryotCodeData = data;
						
						});  	   
				};
				
				
				
				$scope.onloadFunction=function(){
			
			var ryotCodeData=$rootScope.arraydefault;
			var seasonData = $rootScope.arraydefault1;
			
			$scope.ryotCodeData = ryotCodeData;
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.seasonNames=data;
					$scope.authorized.season = seasonData;
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
						//alert(JSON.stringify(data));
								
							$scope.ryotCodeData = data;
							$scope.authorized.ryotCode = ryotCodeData;
							$scope.getIndentnumbers(seasonData,ryotCodeData);
							$scope.loadRyotDet(ryotCodeData);
							$scope.getaggrementDetails(seasonData,ryotCodeData);
							$scope.loadIndents(seasonData,ryotCodeData,authorized.fieldOfficer);
						
						});
				});
			
				
			
			
		};
		
		
		$scope.getIndentnumbers = function(season,ryotcode)
				{
					if( ryotcode!=null){
					var fieldofficerobj=new Object();
					fieldofficerobj.Season=season;
					fieldofficerobj.RyotCode=ryotcode;
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAuthorisedIndentsforAuthorisation.html",
							data: JSON.stringify(fieldofficerobj),
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							
							$scope.indentNoNames=data;
						});  	
					
					}
				};
				
				$scope.CloseDialogRatoon = function()
		{
			$('#gridpopup_box').fadeOut("slow");
		};
		
		$(document).keyup(function(e) {
     if (e.keyCode == 27) {
		
		 $('#gridpopup_box').fadeOut("slow");
		 
		
    }
});
				
				$scope.getpopup=function(indentno,ryotCode){
				 $('#gridpopup_box').fadeIn("slow");
				var indentobj=new Object();
				indentobj.Indentno=indentno;
			
				indentobj.season = $scope.authorized.season;
				indentobj.agreementnumber  = $scope.authorized.agreementnumber;
	
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAuthorisedIndentsforAuthorisePopUp.html",
							data:JSON.stringify(indentobj),
					   }).success(function(data, status) 
						{
							
							
							$scope.IndentNumberData=data;
						
								$scope.authorized.fieldOfficer = data[0].foCode;
									$scope.authorized.faCode= data[0].faCode;
									$scope.authorized.storeCode = data[0].storeCode;
								
							
						}); 
			 
		}
		
		var indentno="";
		$scope.loadPopup=function(indentno,ryotCode){
		indentno=indentno;
		 $scope.getpopup(indentno,ryotCode);
		
			
		}
		
		$scope.loadRyotDet = function(RyotCode)
					{	
		
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllRyots.html",
						data: RyotCode,
						}).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							$scope.authorized.ryotName = data.ryot.ryotName;
							
							$scope.authorized.phoneNo = data.ryot.mobileNumber;
							
						});
					};
					
					
					var IndentObj=new Object();
					var IndentArray=new Array();
					var results = new Array();
					
					$scope.optionToggled=function(checkbox,indentno,kind,quantity,indentNumbers,totalcost,costofeachitem,allowed,indent,pending){
					
						if(checkbox==true){
							IndentObj=indentno;
							
							
												
							if(IndentArray.includes(indentno))
							{
							}
							else
							{
								IndentArray=IndentArray+IndentObj+',';
							}
						$scope.authorized.indentNumbers=IndentArray.replace(/,\s*$/, "");
						var quantity1 = 0;
						if(allowed>quantity)
						{
							quantity1 = quantity;
						}
						else
						{
							quantity1 = allowed;
							pending = quantity-quantity1;
							totalcost = quantity1*costofeachitem;
						}
						$scope.authorizedData.push({kind:kind,quantity:quantity1,indentNumbers:indentNumbers,totalCost:totalcost,costofEachItem:costofeachitem,allowed:allowed,indent:quantity,pending:pending})
						
						
						var grandTotalCost = 0;
					
						for($i=0;$i<$scope.authorizedData.length;$i++)
						{
							grandTotalCost = grandTotalCost+$scope.authorizedData[$i].totalCost;
						}
						
						$scope.authorized.grandTotal = grandTotalCost.toFixed(2);
						
						
						}else if(checkbox==false){
							
							
							}
						}
						
						
						
						$scope.updatePendingAmtGrid = function(quantity,indent,allowed,id)
						{
							//$scope.authorizedData[id].pending
							if(quantity<=indent && quantity<=allowed)
							{
								var pending = Number(indent)-Number(quantity);
								$scope.authorizedData[id].pending = pending;
							}
							else
							{
								 sweetAlert("Oops...", "Please check the quantity and try again!", "error");
								 $scope.authorizedData[id].quantity ="";
							}
							
						};
						$scope.updategridTotalCost = function(quantity,costofEachItem,id)
						{
							var totalCost = Number(quantity)*Number(costofEachItem);
							$scope.authorizedData[id].totalCost = totalCost;
							
							var grandTotalCost = 0;
					
						for($i=0;$i<$scope.authorizedData.length;$i++)
						{
							grandTotalCost = grandTotalCost+$scope.authorizedData[$i].totalCost;
						}
						$scope.authorized.grandTotal = grandTotalCost.toFixed(2);
						};
						
					$scope.authorisationSubmit=function(authroized,form){
						
						var authroizedObj=new Object();
						
						if($scope.authorisationForm.$valid) 
					{
						delete authroized["indentNo"];
					
					gridData = [];	
									  $scope.authorizedData.forEach(function (plantData) 
									  {
										  
				
										 gridData.push(angular.toJson(plantData));														 
									  });
								
					authroizedObj.formData=angular.toJson(authroized);
					authroizedObj.gridData=gridData;
					//alert(JSON.stringify(authroizedObj));
					 $('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/SaveAuthorisationForm.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(authroizedObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							//alert("response"+response);
							//alert($scope.authorized.season);
							
							
							if(response==false)
							{
								 sweetAlert("Oops...", "Something went wrong!", "error");
								
								
							}
						   else
						    {
								//$('#authprint').show();
								
								var newobject=new Object();	
													newobject.AuthFerNo=response;
													newobject.Season=$scope.authorized.season;
											var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getAuthorisePrintDetails.html",
											data:JSON.stringify(newobject),
										   }).success(function(data, status) 
											{
												//alert("array"+JSON.stringify(data));
												
												$('.btn-hide').attr('disabled',false);
												 if($rootScope.arraydefault==undefined)
												 {
													 $scope.items = data.formData;
												 $scope.itemsGrid=data.gridData;
												  $scope.itemsTotal=data.Total;
													 
													swal({
													 title: "Authorisation Fertilizer Updated Successfully!",
													 text: " Do You want to take a print out?",
													 type: "success",
													 showCancelButton: true,
													 confirmButtonColor: "#2298f2",
													 confirmButtonText: "Print",
													 closeOnConfirm: false}, 
													 function(){
														
														var prtContent = document.getElementById("authprint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
														 
													
													
													});
													
												}
												else
												{
													$scope.items = data.formData;
												 $scope.itemsGrid=data.gridData;
												  $scope.itemsTotal=data.Total;
													
													swal("Success!", 'Authorisation Fertilizer Updated Successfully!', "success");
													$rootScope.arraydefault='';
													$rootScope.arraydefault1='';
													$state.go('seedling.transaction.Indents');
												}
										}); 
												
											form.$setPristine(true); 
											$scope.loadusername();
											$scope.authorizedData=[];
											$scope.indentNoNames=[];
				
											$("#hidetable").hide();
									
							 
								
							}
						}
					});
				}
				
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
						}	
				};	
				
		
				
				
					
					
					$scope.reset = function(form)
		 {
				$scope.authorized = angular.copy($scope.master);
				 $('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadusername();
				$scope.authorizedData=[];
				  	$("#hidetable").hide();


		 };
			
		})
		
		
		
	.controller('authorisationAcknowledgementController', function($scope,$http)
		{
			$index=0;
			$scope.authorizedData=[];
			var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "FieldOfficer";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
			
		
		
 			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
		
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);

				var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
			
			
		var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";
			
		 
		 var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			
			$scope.loadseasonNames = function()
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
				}); 
			};
			
	
			$scope.loadStoreCode = function()
		{  
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStoreAuthorization.html",
				data: {},
			   }).success(function(data, status) 
				{
				$scope.storeCodeData = data;
				
					
				});
		};

			$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 //  alert(JSON.stringify(data));
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};
		
		
		$scope.getaggrementDetails = function(season,ryotcode)
			{
				
			var agrobj=new Object();
			agrobj.season=season;
			agrobj.ryotcode=ryotcode;
			//alert(JSON.stringify(agrobj));
					var httpRequest = $http
					({
					method: 'POST',
					url : "/SugarERP/getaggrementDetailsauth.html",
					data: JSON.stringify(agrobj),
					}).success(function(data, status) 
					{
						
					
					$scope.agreementnumberNames = data;
					
					$scope.AddedAcknowledgementForm.agreementnumber = data[0].agreementnumber;
					var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = data[0].agreementnumber;
							getfofaseedlingQtyObj.Screen='auth';
						 if(season!='' && season!=null && data[0].agreementnumber!='' && data[0].agreementnumber!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getAllAgreementDetailsfordispatch.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
								
									$scope.AddedAcknowledgementForm.noOfAcr = data.noOfAcr.toFixed(2);
							
									});
									
									
							}
					
					
						
					});
			};
			
			
				
				
			
		
		$scope.getauthorisationAckData = function(season,authorisationNo)
		{
			if(season!='' && season!=null && authorisationNo!='' && authorisationNo!=null)
			{
				var authAck = new Object();
				authAck.season = season;
				authAck.authorisationSeqNo = authorisationNo;
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllAuthorizationDetails.html",
				data: JSON.stringify(authAck),
			   }).success(function(data, status) 
			   {
				   
				 $scope.AddedAcknowledgementForm = data.AuthorisationFormSummary;
				 $scope.AddedAcknowledgementForm.fieldOfficer = parseInt(data.AuthorisationFormSummary.fieldOfficer);
				 $scope.getaggrementDetails(data.AuthorisationFormSummary.season,data.AuthorisationFormSummary.ryotCode);
				 
				 $scope.AddedAcknowledgementForm.authorisationNo = authorisationNo;
				 $scope.AddedAcknowledgementForm.totalCost = data.AuthorisationFormSummary.grandTotal.toFixed(2);
				 $scope.authorizedData = data.AuthorisationFormDetails;
				})
			}
		};
			
		$scope.validTakenQty = function(quantity,taken,id)
		{
			if(quantity<taken)
			{
				sweetAlert("Oops...", "Taken should be equal or less than the Quantity!", "error");
				$scope.authorizedData[id].taken = "";
				$("#taken"+id).focus();
				return false;
			}
			else
			{
				
			}
		};
		
		$scope.updateConsumerAmt = function(taken,costofEachItem,quantity,id)
		{
			if(taken<=quantity)
			{
				var consumerAmt = Number(taken)*Number(costofEachItem);
				$scope.authorizedData[id].consumerAmt = consumerAmt;
				var ryotQty = Number(quantity)-Number(taken);
				var ryotAmt = Number(ryotQty)*Number(costofEachItem);
				$scope.authorizedData[id].ryotAmt = ryotAmt;
				
				
				var grandRyotTotalCost = 0;
					
						
						  var totalAmount = 0;
						  var totalConsumerAmount =0;
			for(var k=0;k<$scope.authorizedData.length;k++)
			{
				
				if($scope.authorizedData[k].ryotAmt!=undefined)
				{
				
					 totalAmount = parseFloat($scope.authorizedData[k].ryotAmt)+parseFloat(totalAmount);
					 totalConsumerAmount = parseFloat($scope.authorizedData[k].consumerAmt)+parseFloat(totalConsumerAmount);

				}
				else
				{
				
					$scope.authorizedData[k].ryotAmt = 0;
					$scope.authorizedData[k].consumerAmt = 0;
				}
			    
			}
				
				$scope.AddedAcknowledgementForm.grandRyotTotal = totalAmount.toFixed(2);
				$scope.AddedAcknowledgementForm.grandConsumerTotal = totalConsumerAmount.toFixed(2);
			 
				
				
				
			}
		};

		$scope.updateRyotTotal = function(totalCost,supplierAmt)
			{
			var ryotAmt = Number(totalCost)-Number(supplierAmt);
			$scope.AddedAcknowledgementForm.grandRyotTotal = ryotAmt.toFixed(2);
			};
			
			
			
			
			
			
		 
		$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
			 
					$scope.FieldOffNames=data;
	 		   });  	   
		};
		
		 
			$scope.addFormField = function()
			{
				$index++;
				
					$scope.authorizedData.push({'id':$index})
			};
		
			
			$scope.removeRow = function(name)
			{	
			
				var Index = -1;		
				var comArr = eval( $scope.authorizedData );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.authorizedData.splice( Index, 1 );
				$index = comArr.length;
				
				for(var s=0;s<$index;s++)
				{
					$scope.authorizedData[s].id = Number(s)+Number(1);
				}
				
				var grandTotalCost = 0;
					
						for($i=0;$i<$index;$i++)
						{
							grandTotalCost = grandTotalCost+$scope.authorizedData[$i].totalCost;
						}
						
						$scope.authorized.grandTotal = grandTotalCost.toFixed(2);
			
  	    	};
		
		
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		
		
		$scope.loadKindType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllKindTypeDetails.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.kindType=data;
					});

			};
			
			
		$scope.loadusername=function(){
			
			var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
		var username = sessionStorage.getItem('username');
		$scope.authorized={'authorisedBy':username,'authorisationDate':data.serverdate};
		
					 })
		
		}
		
		$scope.loadRyotCodeDropDown = function()
				{
					//alert(ExtentRyotCode);
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
												
							$scope.ryotCodeData = data;
						
						});  	   
				};
			$scope.authorisationAckSubmit=function(AddedAcknowledgementForm,form){
						
						var authroizedObj=new Object();
						
						if($scope.AckForm.$valid) 
					{
						//delete AddedAcknowledgementForm["indentNo"];
					
					gridData = [];	
									  $scope.authorizedData.forEach(function (plantData) 
									  {
										  
				
										 gridData.push(angular.toJson(plantData));														 
									  });
								
					authroizedObj.formData=angular.toJson(AddedAcknowledgementForm);
					authroizedObj.gridData=gridData;
					//alert(JSON.stringify(authroizedObj));
					 $('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/SaveAuthorisationAcknowledgement.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(authroizedObj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							
							
							
							if(response==false)
							{
								 sweetAlert("Oops...", "Something went wrong!", "error");
								
								
							}
						   else
						    {
								
								
								swal("Success", 'Authorisation Acknowledgement Done Successfully!', "success"); 
								$scope.AddedAcknowledgementForm = angular.copy($scope.master);
								 $('.btn-hide').attr('disabled',false);
								form.$setPristine(true); 
								 $scope.loadusername();
								$scope.authorizedData=[];
									$("#hidetable").hide();
						
							}
						}
					});
				}
				
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
						}	
				};	
				
		
				
				
					
					
					$scope.reset = function(form)
		 {
				$scope.AddedAcknowledgementForm = angular.copy($scope.master);
				 $('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadusername();
				$scope.authorizedData=[];
				  	$("#hidetable").hide();


		 };
			
		})	
		
		



//====================Acknowledgement===========================

.controller('AcknowledgementController',function($scope,$http,$rootScope)
		{
			
		
		
				
				
			
		
					var d = new Date();
					var dd = d.getDate();
					var m = d.getMonth();
					var mm = m+1;
					var yy = d.getFullYear();
					var todayDate  = dd+"-"+mm+"-"+yy;
					
					
					
					$scope.onloadFunction=function(){
			var onloadObj= new Object();
			onloadObj.season = $rootScope.season;
			onloadObj.dispatchNo = $rootScope.dispatchNo;
			var season=$rootScope.season;
			var dispatchNo=$rootScope.dispatchNo;
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDispatchDetailsforAcK.html",
				data: JSON.stringify(onloadObj),
			   }).success(function(data, status) 
				{
						
						$("#hidetable").show();
						$scope.AddedAcknowledgementForm = data.DispatchAckSummary;
						$scope.data = data.DispatchAckDetails;
						$scope.AddedAcknowledgementForm.dateOfAcknowledge =todayDate;
					
					
					
				})
		};

			var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
				
			
			
			
			
			
				var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "FieldOfficer";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
			
			var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
		var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
				var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);



			var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};


			
			
			$scope.loadseasonNames = function()
			{
				
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
					$scope.AddedAcknowledgementForm = {'soilWaterAnalyis':'0','landSurvey':'0','agreementSign':'0','dateOfAcknowledge':todayDate};
				}); 
			};
			
			$scope.loadDetails = function(season,dispatchNo)
			{
				var ackDetailsObj  = new Object();
				ackDetailsObj.season = season;
				ackDetailsObj.dispatchNo = dispatchNo;
				if(dispatchNo!='' && dispatchNo!=null && season!='' && season!=null )
				{
					
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/validateDispatchAck.html",
						data: JSON.stringify(ackDetailsObj),
					   }).success(function(data, status) 
					  {
						  if(data)
						  {
							sweetAlert("Oops...", "This Dispatch Number already Acknowledged !", "error");
						
						  }
						  else
						  {
							  
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDispatchDetailsforAcK.html",
				data: JSON.stringify(ackDetailsObj),
				  }).success(function(data, status) 
					{
						$("#hidetable").show();
						$scope.AddedAcknowledgementForm = data.DispatchAckSummary;
						$scope.data = data.DispatchAckDetails;
						$scope.AddedAcknowledgementForm.dateOfAcknowledge =todayDate;
						
					}); 
						  }
						  
					  });
					
					
				
				}
				
			};

			//-------------Load Field Assistants-------
		$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};	
				
				
				$scope.loadRyotCodeDropDown = function()
				{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							
							$scope.ryotCode = data;
						});  	   
				};

				$scope.loadVarietyNames = function()
				{
							
							var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: VarietyNameString,
							   }).success(function(data, status) 
								{					
								
							//alert(JSON.stringify(data));
									$scope.varietyNames = data;
								});  	   					
				};
				$scope.loadFieldOfficerDropdown = function()
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: FieldOffDropDown,
			  		 }).success(function(data, status) 
			   			{	
							$scope.FieldOffNames=data;
	 		   			});  	   
					};
			
			
			var obj = new Object();
			$scope.AckSubmit = function(AddedAcknowledgementForm,form)
			 {
			
				if($scope.AddedAcknowledgementForm.landSurvey=="0" && $scope.AddedAcknowledgementForm.agreementSign=="0" && $scope.AddedAcknowledgementForm.soilWaterAnalyis=="0")
				 {
					if($scope.Ackform.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (dispatchData) 
					  {
						 gridData.push(angular.toJson(dispatchData));														 
				      });
					  obj.formData = angular.toJson(AddedAcknowledgementForm);
					  obj.gridData = gridData;
				     
				    $('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/SaveSeedlingDispatchAckSummary.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Acknowledgement Done Successfully!', "success");
							//	$scope.reset(form);
								
							$scope.AddedAcknowledgementForm = angular.copy($scope.master);
							form.$setPristine(true);
							$scope.loadseasonNames();
								$scope.data=[];
							$('.btn-hide').attr('disabled',false);	
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}
				 }
				 else
				 {
					
					sweetAlert("Oops...", "Select LandSurvey,Soil and Water Analysis andAgreement Signed Yes to proceed further !", "error");
				 }
								

			 };

			 $scope.reset= function(form)
			 {
				$scope.AddedAcknowledgementForm = angular.copy($scope.master);
				form.$setPristine(true);
				$scope.loadseasonNames();
				$scope.data=[];
				$('.btn-hide').attr('disabled',false);
			 }

			

		})

	
		.controller('CaneShiftingController', function($scope,$http)
		{
			 
			 var obj = new Object();

 			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
		//	alert(jsonStringSeasonDropDown);

			  var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);//
//alert(jsonCaneWeightSeasonString);
	 	var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			$scope.master = {};
		
	 
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
					//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddedDetrashing = {'labourSpplr':'0','modifyFlag':'No','gridCount':1,'manCost':'300.00','womenCost':'200.00'};
			}); 		
		};

		$scope.getLabourContractorDetails = function()
			 {
			if($scope.AddedDetrashing.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };
		

			$scope.LoadWoManTotal =function(people,cost)
			{
				if(people==null || people=='')
				{
					people = 0;
				}
				if(cost==null || cost=='')
				{
					cost = 0;
				}
			var total=Number(people)*Number(cost);
				$scope.AddedDetrashing.womenTotal=total.toFixed(2);
			};

			$scope.LoadManTotal =function(people,cost)
			{
				if(people==null || people=='')
				{
					people = 0;
				}
				if(cost==null || cost=='')
				{
					cost = 0;
				}
			var total=Number(people)*Number(cost);
			
				$scope.AddedDetrashing.menTotal= total.toFixed(2);
				
			};



        $scope.getAddedDetrashingList=function(season,date,shift)
			 {
			var detrashingListobj = new Object();
			detrashingListobj.season=season;
			detrashingListobj.shift=shift;
			detrashingListobj.caneShiftingDate=date;
              
			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  //alert(JSON.stringify(detrashingListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllCaneShiftingDetails.html",
					data: JSON.stringify(detrashingListobj),
					  }).success(function(data, status) 
					{
						$scope.AddedDetrashing=data.DetachSummary;
						$scope.data= data.DetachDetails;
						$scope.getLabourContractorDetails();
						
					});
				 }
				
					

			 };

		$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			
			
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
		};
		$scope.getaddRow  = function(addRowValues)
		{
			
			if(addRowValues.trim()!= "" || addRowValues.trim()!=null )
			{
				$("#getAddRowGrid").show();
			}
			if(addRowValues.trim()=="")
			{
				
				$("#getAddRowGrid").hide();
			}
		};
		
		
		$scope.getotalCost = function(id,caneWt)
			{
			
				 
				 
				  $scope.data[id-1].caneWt= caneWt;
				
					 var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount = parseFloat($scope.data[s].caneWt)+parseFloat(totalAmount);
					  }
					
					  
					
					  $scope.AddedDetrashing.totalCaneWt = totalAmount.toFixed(3);
				
				
			
			};
		
		$index=0;
			$scope.data=[];
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				//alert($index);
				
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
           
		
			
			var totalAmount = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmount = parseFloat($scope.data[k].caneWt)+parseFloat(totalAmount);
			}
			 $scope.AddedDetrashing.totalCaneWt = totalAmount.toFixed(3);
			
  	    };
		
		 $scope.getContractTotalCost = function(costPerTon,totalTons)
			 {
				 if(costPerTon==null || costPerTon=="")
				 {
					 costPerTon =0;
				 }
				 if(totalTons==null || totalTons=="")
				 {
					 totalTons = 0;
				 }
				var totalLabourCost = Number(costPerTon)*Number(totalTons);
				
				//$scope.AddedDetrashing.totalLabourCost = totalLabourCost.toFixed(2);
				$scope.AddedDetrashing.totalContractAmt = totalLabourCost.toFixed(2);
			 };
		
			$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
					
							var g= gridCount-Number(1);
							$scope.data[g].landVillageCode = data[0].landVillageCode;
							$scope.data[g].seedSupr = data[0].seedSupr;
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
							$scope.data[g].ryotName = data[0].ryotName;
							$scope.data[g].lvName = data[0].LVName;
						});
					}
				}); 
			 };


				$scope.clearTotalforDetrashing = function(val)
				{
					$scope.AddedDetrashing.totalContractAmt="";
				}
			 $scope.getGrandTotalforDetrashing = function(menTotal,womenTotal,totalTons,labourSpplr)
			 {
				 
				 if(labourSpplr==0)
				 {
					 $scope.AddedDetrashing.totalContractAmt="";
					  if(womenTotal==null || womenTotal=="")
				 	  {
					 	womenTotal=0;
				 	  }
					   if(menTotal==null || menTotal=="")
				 	  {
					 	menTotal=0;
				 	  }
					 
					 
					var grandTotal = Number(menTotal)+Number(womenTotal);
				
				$scope.AddedDetrashing.totalLabourCost = grandTotal.toFixed(2); 
				 }
				 else
				 {
					 $scope.AddedDetrashing.totalContractAmt="";  
					  if(totalTons==null || totalTons=="")
				   {
					 totalTons=0;
				   } 
				   var grandTotal = Number(totalTons);
				   $scope.AddedDetrashing.totalContractAmt = grandTotal.toFixed(2); 
				  
				 }
				 
				
				
				
			 };
			

			$scope.CaneShiftSubmit = function(AddedDetrashing,form)
			 {
			
				if($scope.DetrashingForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (batchData) 
					  {
						  

	            		 gridData.push(angular.toJson(batchData));														 
				      });
					  obj.formData = angular.toJson(AddedDetrashing);
					  obj.gridData = gridData;
				      
				     // delete obj["$index"];
					 // alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveCaneShifting.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Cane Transferred Successfully!', "success");
							//	$scope.reset(form);
								
							$scope.reset(form);
									
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };
		 $scope.reset = function(form)
		 {
				$scope.AddedDetrashing = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadShift();
				  $index=1;
					$scope.data = [{'id':$index}]


		 };
		 
			
		})
		.controller('BudCuttingController', function($scope,$http)
		{
			$scope.master = {};
			var obj = new Object();
		var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			 var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
	 
	 	var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);

			var MachineDropDown = new Object();
			MachineDropDown.dropdownname = "Machine";
			var jsonMachineDropDown= JSON.stringify(MachineDropDown);
			 $scope.loadMachine = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonMachineDropDown,
			}).success(function(data, status)
			{
					//alert(JSON.stringify(data));
					$scope.machine=data;
			}); 		
		};
		$scope.getLabourContractorDetails = function()
			{
				if($scope.AddedBudCutting.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
			};
		 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status)
			{
					//alert(JSON.stringify(data));
				$scope.WeighBridgeNames=data;
				$scope.AddedBudCutting = {'labourSpplr':'0','modifyFlag':'No','machineOrMen':1,'manCost':'300.00','womanCost':'200.00'};
			}); 		
		};
		$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				}); 
			};
			
		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
		};
		$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
				 
                 var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							var g= gridCount-Number(1);
							$scope.data[g].landVillageCode = data[0].landVillageCode;
							$scope.data[g].seedSupr = data[0].seedSupr;
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
							$scope.data[g].ryotName = data[0].ryotName;
							$scope.data[g].lvName = data[0].LVName;
						});
					}
				}); 
			 };
			 $scope.loadmenTotal = function(company,men,cost,mantotal)
			 {
				 if(company==0)
				 {
					 if(cost==null || cost=="")
					 {
						 cost=0;
						 //$scope.AddedBudCutting.manCost = cost;
					 }
					var totalMen =  Number(men)*Number(cost);
					$scope.AddedBudCutting.menTotal = totalMen.toFixed(2);
				 }
				 
			 };
			 $scope.loadWomenTotal = function(company,women,cost)
			 {
				 if(company==0)
				 {
					 if(cost==null || cost=="")
					 {
						 cost=0;
						 //$scope.AddedBudCutting.womanCost = cost;
					 }
					var totalWomen =  Number(women)*Number(cost);
					$scope.AddedBudCutting.womenTotal = totalWomen.toFixed(2);
				 }
				 
			 };
			 $scope.getGrandTotal = function(company,men,women)
			 {
				 if(company==0)
				 {
					 if(women==null || women=="")
					 {
						 women=0;
						//$scope.AddedBudCutting.womenTotal = women;
					 }
					 if(men==null || men=="")
					 {
						 men=0;
						//$scope.AddedBudCutting.womenTotal = women;
					 }
					 var grandTotal = Number(men)+Number(women);
					 $scope.AddedBudCutting.totalLabourCost = grandTotal.toFixed(2);
				 }
			 };
			 $scope.loadContractTotal = function(company,costperton,totalTons)
			 {
				 if(company==1)
				 {
					 if(costperton==null || costperton=="")
					 {
						 costperton =0;
						
					 }
					 if(totalTons==null || totalTons=="")
					 {
						 totalTons =0;
					 }
					 
					var contractGrandTotal =  Number(costperton)*Number(totalTons);
					$scope.AddedBudCutting.totalNoOfBuds = contractGrandTotal.toFixed(2);
					$scope.AddedBudCutting.totalContractAmt = contractGrandTotal.toFixed(2);
				 }
			 };
			 
			 
			 
			 $scope.getTotalBuds = function(budsPerKG,noOfTubs,tubWt,id)
			 {
				 if(budsPerKG==null || budsPerKG=="")
				 {
					 budsPerKG = 1;
				 }
				  if(noOfTubs==null || noOfTubs=="")
				 {
					 noOfTubs = 1;
				 }
				  if(tubWt==null || tubWt=="")
				 {
					 tubWt = 1;
				 }
				 
				var totalBuds = Number(budsPerKG)*Number(noOfTubs)*Number(tubWt);
				$scope.data[id-1].totalBuds = totalBuds;
				
				  var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount = parseFloat($scope.data[s].totalBuds)+parseFloat(totalAmount);
					  }
					  
					  //$scope.AddedDetrashing.totalBuds = Number(parseFloat(totalAmount));
					  $scope.AddedBudCutting.budGrandTotal = totalAmount.toFixed(2);
				
				
			 };
			
			$index=0;
			$scope.data=[];
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
				
				 var totalAmount = 0;
			for(var k=0;k<$index;k++)
			{
				//alert($scope.data[k].totalCost);
			     totalAmount = parseFloat($scope.data[k].totalBuds)+parseFloat(totalAmount);
				 
			}
				  
			$scope.AddedBudCutting.budGrandTotal = totalAmount.toFixed(2);
			// $scope.AddedDetrashing.totalCaneWt = parseFloat(totalAmount);
			
  	    	};

			 $scope.getAddedBudCuttingList=function(season,date,shift)
			 {
			var budCuttinggListobj = new Object();
			budCuttinggListobj.season=season;
			budCuttinggListobj.shift=shift;
			budCuttinggListobj.caneShiftingDate=date;
              
			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  //alert(JSON.stringify(detrashingListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllBudCuttingDetails.html",
					data: JSON.stringify(budCuttinggListobj),
					  }).success(function(data, status) 
					{
						$scope.AddedBudCutting=data.DetachSummary;
						$scope.data= data.DetachDetails;
						$scope.getLabourContractorDetails();
					});
				 }
				
					

			 };
			
			
			$scope.BudCuttingSubmit = function(AddedBudCutting,form)
			 {
				
				if($scope.BudCuttingForm.$valid) 
				{	
				
					  gridData = [];	
					  $scope.data.forEach(function(batchData) 
					  {
						  

	            		 gridData.push(angular.toJson(batchData));														 
				      });
					
					  obj.formData = angular.toJson(AddedBudCutting);
					   
					  obj.gridData = gridData;
				      
				  // alert(JSON.stringify(obj));
					 
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveBudCutting.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'BudCutting Done Successfully!', "success");
								//alert("before clear");
								$scope.reset(form);
								//alert("after clear");
							
									
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
	 };

	  $scope.reset = function(form)
		 {
				$scope.AddedBudCutting = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadShift();
				  $index=1;
					$scope.data = [{'id':$index}]


		 };
	
		
		})
		
		.controller('ChemicalTreatmentController', function($scope,$http)
		{
			$index=0;
			$indexBud=0;
			$scope.data=[];
			$scope.datagrid=[];
			
			var obj = new Object();
			
			 var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
	 
	 	var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
	 
	 		$scope.master = {};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				$scope.WeighBridgeNames=data;
				$scope.AddedChemicalTreatment = {'labourSpplr':'0','modifyFlag':'No','manCost':'300.00','womanCost':'200.00'};
			}); 		
		};

		$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedChemicalTreatment.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };


		$scope.loadVarietyNames = function()
		{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
		};
		
		$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
					
							var g= gridCount-Number(1);
							$scope.datagrid[g].landVillageCode = data[0].landVillageCode;
							$scope.datagrid[g].seedSupr = data[0].seedSupr;
							$scope.datagrid[g].variety = data[0].variety;
							$scope.datagrid[g].varietyName = data[0].varietyName;
							$scope.datagrid[g].ryotName = data[0].ryotName;
							$scope.datagrid[g].lvName = data[0].LVName;
						});
					}
				}); 
			 };


			$scope.LoadManTotal = function(men,cost)
			 {
				if(men==null || men=='')
						{
							men=0;
						}
						if(cost==null || cost=='')
						{
							cost=0;
						}
					var totalMen =  Number(men)*Number(cost);
					
					$scope.AddedChemicalTreatment.menTotal = totalMen.toFixed(2);
				
				 
			 };
		
			
			 $scope.LoadWoManTotal = function(women,cost)
			 {
				if(women==null || women=='')
						{
							women=0;
						}
						if(cost==null || cost=='')
						{
							cost=0;
						}
					var totalWoman =  Number(women)*Number(cost);
					
					
					$scope.AddedChemicalTreatment.womenTotal = totalWoman.toFixed(2);;
				 
			 };
			 $scope.getContractTotalCost = function(costPerTon,totalTons)
			 {
				 if(costPerTon==null || costPerTon=="")
				 {
					 costPerTon =1;
				 }
				 if(totalTons==null || totalTons=="")
				 {
					 totalTons = 1;
				 }
				var totalLabourCost = Number(costPerTon)*Number(totalTons);
				
				
				$scope.AddedChemicalTreatment.totalContractAmt = totalLabourCost.toFixed(2);
			 };
			 
			
	 
		
		$scope.loadseasonNames = function()
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
				}); 
			};
			
			
			
		$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
		};
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};
			
			$scope.addFormFieldBud = function() 
			{
				$indexBud++;
		    	$scope.datagrid.push({'id':$indexBud})
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			$scope.removeRowBud = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.datagrid.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.datagrid[s].id = Number(s)+Number(1);
				}
			
  	    	};

			 $scope.getChemicalTreatmentList=function(season,date,shift)
			 {
			var budCuttinggListobj = new Object();
			budCuttinggListobj.season=season;
			budCuttinggListobj.shift=shift;
			budCuttinggListobj.chemicaltreatmentDate=date;
             // alert(JSON.stringify(budCuttinggListobj));

			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  // alert(JSON.stringify(budCuttinggListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllChemicalTreatments.html",
					data: JSON.stringify(budCuttinggListobj),
					  }).success(function(data, status) 
					{
						$scope.AddedChemicalTreatment=data.TreatmentSummary;
						$scope.data= data.InvTreatmentDetails;
						$scope.datagrid= data.TreatmentDetails;

			

					});
				 }
				
					

			 };
			



			
			$scope.SaveChemicalTreatment = function(AddedChemicalTreatment,form)
			 {
			
			
				if($scope.chemicalTreatmentForm.$valid) 
				{	
					  middleData = [];	
					  $scope.data.forEach(function (chemicalTreatmentData) 
					  {
						  

	            		 middleData.push(angular.toJson(chemicalTreatmentData));														 
				      });
					  
					  
					 
					 gridData = [];	
					  $scope.datagrid.forEach(function (batchData) 
					  {
						  

	            		 gridData.push(angular.toJson(batchData));														 
				      });
					  obj.formData = angular.toJson(AddedChemicalTreatment);
					  obj.gridData = gridData;
					   obj.middleData = middleData;
				      
				     
					 // alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveChemicalTreatment.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Chemical Treatment Done Successfully!', "success");
								$scope.AddedChemicalTreatment = angular.copy($scope.master);
							$('.btn-hide').attr('disabled',false);
							form.$setPristine(true); 
							 $scope.loadShift();
							  $index=1;
								$scope.data = [{'id':$index}]

								 $indexBud=1;
								$scope.datagrid = [{'id':$indexBud}];
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };
		   $scope.reset = function(form)
		 {
				$scope.AddedChemicalTreatment = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true); 
				 $scope.loadShift();
				  $index=1;
					$scope.data = [{'id':$index}]

					 $indexBud=1;
					$scope.datagrid = [{'id':$indexBud}];
		 };
		 
		 
		  $scope.getGrandTotalforCT = function(menTotal,womenTotal,totalTons,labourSpplr)
			 {
				 
				 if(labourSpplr==0)
				 {
					 $scope.AddedChemicalTreatment.totalContractAmt="";
					  if(womenTotal==null || womenTotal=="")
				 	  {
					 	womenTotal=0;
				 	  }
					    if(menTotal==null || menTotal=="")
				 	  {
					 	menTotal=0;
				 	  }
					 
					var grandTotal = Number(menTotal)+Number(womenTotal);
				$scope.AddedChemicalTreatment.totalLabourCost = grandTotal.toFixed(2);
			
				 }
				 else
				 {
					 $scope.AddedChemicalTreatment.totalContractAmt="";  
					  if(totalTons==null || totalTons=="")
				   {
					 totalTons=0;
				   } 
				   var grandTotal = Number(totalTons);
				   $scope.AddedChemicalTreatment.totalContractAmt = grandTotal.toFixed(2); 
				  
				 }
				 
				
				
				
			 };
			 
		
		
		})
		
		.controller('hardeningController', function($scope,$http)
		{
			$index=0;
			
			$scope.data=[];
			$scope.master = {};
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
			
			
			
			var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};
			
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				
				$scope.WeighBridgeNames=data;
				
			}); 		
		};
		
		$scope.getTotalSeedlingsIn = function(noOfTraysIn,cavities,id)
			{
				if(noOfTraysIn==null || noOfTraysIn=="")
				{
					noOfTraysIn =1;
				}
				if(cavities==null || cavities=="")
				{
					cavities =1;
				}
				
				var totalSeedlingsIn = Number(noOfTraysIn)*Number(cavities);
				$scope.data[id-1].totalSeedlingsIn = totalSeedlingsIn;
				
				var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsIn;
					  }
					  $scope.AddedHardening.grandTotalIn = Number(parseFloat(totalAmount));
			};
			
				$scope.getTotalSeedlingsOut = function(noOfTraysOut,cavities,id)
			{
				
					if(noOfTraysOut==null || noOfTraysOut=="")
					{
						noOfTraysOut =1;
					}
					if(cavities==null || cavities=="")
					{
						cavities =1;
					}
				
				var totalSeedlingsOut = Number(noOfTraysOut)*Number(cavities);
				
				$scope.data[id-1].totalSeedlingsOut = totalSeedlingsOut;
				
				var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsOut;
					  }
					  $scope.AddedHardening.grandTotalOut = Number(parseFloat(totalAmount));
			};
		
		$scope.getLabourGrandTotal = function(man,woman)
			{
				var labourTotal=0;
				if(man==null || man=="")
				{
					man=0;
				}
				if(woman==null || woman=="")
				{
					woman=0;
				}
				
				labourTotal = Number(man)+Number(woman);
				$scope.AddedHardening.totalLabourCost = labourTotal.toFixed(2);
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
				
				 var totalAmountIn = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountIn += $scope.data[k].totalSeedlingsIn;
			}
			
			 var totalAmountOut = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountOut += $scope.data[k].totalSeedlingsOut;
			}
				
			$scope.AddedHardening.grandTotalIn = totalAmountIn;
			$scope.AddedHardening.grandTotalOut = totalAmountOut;
			
  	    	};
			
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
			};
			
			
			$scope.getMenTotal  = function(man,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedHardening.manCost = cost;
				}
				if(man==null || man=="")
				{
					man=0;
					//$scope.AddedHardening.manCost = cost;
				}
				var totalMan = Number(man) * Number(cost);
				$scope.AddedHardening.menTotal = totalMan.toFixed(2);
			};
			$scope.getWomanTotal  = function(woman,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				if(woman==null || woman=="")
				{
					woman=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				var totalWoman = Number(woman) * Number(cost);
				$scope.AddedHardening.womenTotal = totalWoman.toFixed(2);
			};
			
			
			$scope.getContractTotal = function(cost,Totaltons)
			{
				if(cost==null || cost=="")
				{
					cost=0;
				}
				if(Totaltons==null || Totaltons=="")
				{
					Totaltons=0;
				}
				
				var grandContractTotal = 0;
				grandContractTotal = Number(cost)*Number(Totaltons);
				$scope.AddedHardening.totalContractAmt = grandContractTotal.toFixed(2);
				$scope.AddedHardening.grandTotalOut = grandContractTotal.toFixed(2);
				
			};


			$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedHardening.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };
			
		$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchDetailsNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchSeriesNoDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
					
							var g= gridCount-Number(1);
							
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
						});
					}
				}); 
			 };
			 
			 
			$scope.loadVillageNames = function()
			{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
			};
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
					$scope.AddedHardening = {'labourSpplr':'0','modifyFlag':'No','manCost':'300.00','womenCost':'200.00'};
					
				}); 
			};
			
			var obj = new Object();
			
				$scope.HardeningSubmit = function(AddedHardening,form)
				{
					//alert($scope.hardeningForm.$valid);
					if($scope.hardeningForm.$valid)
					{
						  gridData = [];	
									  $scope.data.forEach(function (batchData) 
									  {
										  
				
										 gridData.push(angular.toJson(batchData));														 
									  });
									  obj.formData = angular.toJson(AddedHardening);
									  obj.gridData = gridData;
									  
									 // delete obj["$index"];
									 // alert(JSON.stringify(obj));
									$('.btn-hide').attr('disabled',true);
									$.ajax({
										url:"/SugarERP/SaveHardeningDetails.html",
										processData:true,
										type:'POST',
										contentType:'Application/json',
										data:JSON.stringify(obj),
										beforeSend: function(xhr) 
										{
											xhr.setRequestHeader("Accept", "application/json");
											xhr.setRequestHeader("Content-Type", "application/json");
										},
										success:function(response) 
										{
											if(response==true)
											{
												swal("Success", 'Hardening Done Successfully!', "success");
											//	$scope.reset(form);
												
													$scope.AddedHardening = angular.copy($scope.master);
												$('.btn-hide').attr('disabled',false);
												form.$setPristine(true); 
												$scope.loadseasonNames();
												$index=1;
												$scope.data = [{'id':$index}]													
												
											}
										   else
											{
												sweetAlert("Oops...", "Something went wrong!", "error");
											   $('.btn-hide').attr('disabled',false);
													
											}
										}
									});	
					}
					else
								{
										var field = null, firstError = null;
										for (field in form) 
										{
											if (field[0] != '$') 
											{
												if (firstError === null && !form[field].$valid) 
												{
													firstError = form[field].$name;
												}
												if (form[field].$pristine) 
												{
													form[field].$dirty = true;
												}
											}	
										}
								}
				};
				
				
			$scope.getHardeningList = function(season,date,shift)
			{
				
				var hardeningListobj = new Object();
				hardeningListobj.season=season;
				hardeningListobj.shift=shift;
				hardeningListobj.summaryDate=date;
            

			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  // alert(JSON.stringify(hardeningListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllHardenningDetails.html",
					data: JSON.stringify(hardeningListobj),
					  }).success(function(data, status) 
					  {
						 
						$scope.AddedHardening=data.HardenningSummary;
						$scope.data= data.HardenningDetails;
						$scope.getLabourContractorDetails();
					 });
				 }
				
				
			};

				$scope.reset = function(form)
		 		{
					$scope.AddedHardening = angular.copy($scope.master);
					$('.btn-hide').attr('disabled',false);
					form.$setPristine(true); 
					$scope.loadseasonNames();
					$index=1;
					$scope.data = [{'id':$index}]
		 		};
		
		
		})
		
		.controller('trimmingController', function($scope,$http)
		{
			$index=0;
			
			$scope.data=[];
			$scope.master = {};
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
			var CaneWeightSeasonObj = new Object();
	 		CaneWeightSeasonObj.dropdownname = "Shift";
	 		var jsonCaneWeightSeasonString= JSON.stringify(CaneWeightSeasonObj);
			
			var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};
			
			$scope.getTotalSeedlingsIn = function(noOfTraysIn,cavities,id)
			{
				if(noOfTraysIn==null || noOfTraysIn=="")
				{
					noOfTraysIn = 1;
					
				}
				
				if(cavities==null || cavities=="")
				{
					cavities = 1;
					
				}
				
				var totalSeedlingsIn = Number(noOfTraysIn)*Number(cavities);
				$scope.data[id-1].totalSeedlingsIn = totalSeedlingsIn;
				var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsIn;
					  }
					  $scope.AddedHardening.grandTotalIn = Number(parseFloat(totalAmount));
				
				
			};
			
				$scope.getTotalSeedlingsOut = function(noOfTraysOut,cavities,id)
			{
				if(noOfTraysOut==null || noOfTraysOut=="")
				{
					noOfTraysOut = 1;
					
				}
				
				if(cavities==null || cavities=="")
				{
					cavities = 1;
					
				}
				
				var totalSeedlingsOut = Number(noOfTraysOut)*Number(cavities);
				$scope.data[id-1].totalSeedlingsOut = totalSeedlingsOut;
				
				var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.data[s].totalSeedlingsOut;
					  }
					  $scope.AddedHardening.grandTotalOut = Number(parseFloat(totalAmount));
			};
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			 $scope.loadShift = function()
			{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonCaneWeightSeasonString,
			}).success(function(data, status) 
			{
				
				$scope.WeighBridgeNames=data;
				
			}); 		
		};

		$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedHardening.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };

		
		$scope.getLabourGrandTotal = function(man,woman)
			{
				var labourTotal=0;
				if(man==null || man=="")
				{
					man = 0;
				}
				if(woman==null || woman=="")
				{
					woman = 0;
				}
				labourTotal = Number(man)+Number(woman);
				$scope.AddedHardening.totalLabourCost = labourTotal.toFixed(2);
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
				
				var totalAmountIn = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountIn += $scope.data[k].totalSeedlingsIn;
			}
			
			 var totalAmountOut = 0;
			for(var k=0;k<$index;k++)
			{
			     totalAmountOut += $scope.data[k].totalSeedlingsOut;
			}
				
			$scope.AddedHardening.grandTotalIn = totalAmountIn;
			$scope.AddedHardening.grandTotalOut = totalAmountOut;
			
  	    	};
			
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{							
							$scope.varietyNames = data;
						});  	   					
			};
			
			
			$scope.getMenTotal  = function(man,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					
				}
				if(man==null || man=="")
				{
					man=0;
					
				}
				var totalMan = Number(man) * Number(cost);
				$scope.AddedHardening.menTotal = totalMan.toFixed(2);
			};
			$scope.getWomanTotal  = function(woman,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				if(woman==null || woman=="")
				{
					woman=0;
					//$scope.AddedHardening.womenCost = cost;
				}
				var totalWoman = Number(woman) * Number(cost);
				$scope.AddedHardening.womenTotal = totalWoman.toFixed(2);
			};
			
			
			$scope.getContractTotal = function(cost,Totaltons)
			{
				if(cost==null || cost=="")
				{
					cost =0;
				}
				if(Totaltons==null || Totaltons=="")
				{
					Totaltons =0;
				}
				//var grandContractTotal = 1;
				grandContractTotal = Number(cost)*Number(Totaltons);
				$scope.AddedHardening.totalContractAmt = grandContractTotal.toFixed(2);
				
				
			};
			
		$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchDetailsNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchSeriesNoDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
					
							var g= gridCount-Number(1);
							
							$scope.data[g].variety = data[0].variety;
							$scope.data[g].varietyName = data[0].varietyName;
						});
					}
				}); 
			 };
			 
			 
			$scope.loadVillageNames = function()
			{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
					
					$scope.villageNames=data;
				}); 
			};
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					
					$scope.seasonNames=data;
					$scope.AddedHardening = {'labourSpplr':'0','modifyFlag':'No','manCost':'300.00','womenCost':'200.00'};
					
				}); 
			};
			
			var obj = new Object();
			
				$scope.HardeningSubmit = function(AddedHardening,form)
				{
					//alert($scope.hardeningForm.$valid);
					if($scope.hardeningForm.$valid)
					{
						  gridData = [];	
									  $scope.data.forEach(function (batchData) 
									  {
										  
				
										 gridData.push(angular.toJson(batchData));														 
									  });
									  obj.formData = angular.toJson(AddedHardening);
									  obj.gridData = gridData;
									  
									 // delete obj["$index"];
									 // alert(JSON.stringify(obj));
									$('.btn-hide').attr('disabled',true);
									$.ajax({
										url:"/SugarERP/saveTrimmingDetails.html",
										processData:true,
										type:'POST',
										contentType:'Application/json',
										data:JSON.stringify(obj),
										beforeSend: function(xhr) 
										{
											xhr.setRequestHeader("Accept", "application/json");
											xhr.setRequestHeader("Content-Type", "application/json");
										},
										success:function(response) 
										{
											if(response==true)
											{
												swal("Success", 'Trimming Done Successfully!', "success");
											//	$scope.reset(form);
													$scope.AddedHardening = angular.copy($scope.master);
													$('.btn-hide').attr('disabled',false);
													form.$setPristine(true); 
													$scope.loadseasonNames();
													$index=1;
													$scope.data = [{'id':$index}]
										
													
												
											}
										   else
											{
												sweetAlert("Oops...", "Something went wrong!", "error");
											   $('.btn-hide').attr('disabled',false);
													
											}
										}
									});	
					}
					else
								{
										var field = null, firstError = null;
										for (field in form) 
										{
											if (field[0] != '$') 
											{
												if (firstError === null && !form[field].$valid) 
												{
													firstError = form[field].$name;
												}
												if (form[field].$pristine) 
												{
													form[field].$dirty = true;
												}
											}	
										}
								}
				};
				
				
			$scope.getHardeningList = function(season,date,shift)
			{
				
				var hardeningListobj = new Object();
				hardeningListobj.season=season;
				hardeningListobj.shift=shift;
				hardeningListobj.summaryDate=date;
            

			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				 //  alert(JSON.stringify(hardeningListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllTrimmingDetails.html",
					data: JSON.stringify(hardeningListobj),
					  }).success(function(data, status) 
					  {
						$scope.AddedHardening=data.TrimmingSummary;
						$scope.data= data.TrimmingDetails;
						$scope.getLabourContractorDetails();
					 });
				 }
				
				
			};
			
			  	$scope.reset = function(form)
		 		{
					$scope.AddedHardening = angular.copy($scope.master);
					$('.btn-hide').attr('disabled',false);
					form.$setPristine(true); 
					$scope.loadseasonNames();
					$index=1;
					$scope.data = [{'id':$index}]
		 		};
			
		
		
		})
		
		.controller('trayfillingController', function($scope,$http)
		{
			$index=0;
			$indexBud=0;
			$scope.data=[];
			$scope.datagrid=[];
			var obj = new Object();
			$scope.master = {};

			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);

			var ShiftDropDownLoad = new Object();
			ShiftDropDownLoad.dropdownname = "Shift";
			var jsonStringShiftDropDown= JSON.stringify(ShiftDropDownLoad);
			
			var DropDownLoadKind = new Object();
			DropDownLoadKind.dropdownname ="KindType";
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			$scope.loadBatchDetailsPopup = function()
					{
						
							var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getBatchMasterData.html",
											data: {},
									   }).success(function(data, status) 
										{	
										//alert(JSON.stringify(data));
										if(data.length<1)
										{
											alert("No Data Found");
										}
										else
										{
											
											
											$scope.batchdetailsData = data;
											
										
											 $('#gridpopup_box').fadeIn("slow");
										
										}
									           //$('#gridpopup_box').fadeIn("slow");
									
											 
											
										});
						
						 
						 
					};
					$scope.CloseDialogBatch = function()
						{
							$('.popupbox_ratoon').fadeOut("slow");
						};
						
						$(document).keyup(function(e) {
					 if (e.keyCode == 27) {
						
						 $('#gridpopup_box').fadeOut("slow");
						 
						
					}
				});
			
			
			
			
			
			
			
			
			$scope.updateTotalSeedlings = function(totalTrays,noOfCavitiesPerTray,id)
			{
				var totalSeedlings = Number(totalTrays)* Number(noOfCavitiesPerTray);
				$scope.datagrid[id-1].totalNoOfSeedlingss = totalSeedlings;
				
				var totalAmount = 0;
					  for(var s=0;s<id;s++)
					  {
						  totalAmount += $scope.datagrid[s].totalNoOfSeedlingss;
					  }
					  
					  //$scope.AddedDetrashing.totalBuds = Number(parseFloat(totalAmount));
					  $scope.AddedTrayFilling.totalNoOfSeedlings = Number(parseFloat(totalAmount));
				 //$scope.data[id-1].wtWithoutTrash=Number(parseFloat(Wttrash));
				
					
					  //$scope.AddedDetrashing.totalCaneWt = Number(parseFloat(totalAmount));
				
				
			}

			$scope.getLabourContractorDetails = function()
			 {
				if($scope.AddedTrayFilling.labourSpplr =='1')
				 {
					//alert($scope.AddedDetrashing.labourSpplr);

						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getLabourContratorDetails.html",
							data: {},
					   }).success(function(data, status) 
						{							
							$scope.lcNames = data;
							
						}); 
				 }
				 else
				 {
					$scope.lcNames = "";
				 }
				
			 };
			
			$scope.hideTrayFields = function(company)
			{
				if(company==1)
				{
					$scope.AddedTrayFilling.machineOrMen=5;
					//$("#hidelabourcharges").hide();
					//$("#totalLabourCost").hide();
					//$("#totalContractAmt").show();
				}
				else
				{
					$scope.AddedTrayFilling.machineOrMen=1;
					//$("#hidelabourcharges").show();
					//$("#totalContractAmt").hide();
					//$("#totalLabourCost").show();
				}
			};
			$scope.getMenTotal  = function(man,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedTrayFilling.manCost = cost;
				}
				if(man==null || man=="")
				{
					man=0;
				//	$scope.AddedTrayFilling.manCost = cost;
				}
				var totalMan = Number(man) * Number(cost);
				$scope.AddedTrayFilling.menTotal = totalMan.toFixed(2);
			};
			$scope.getWomenTotal  = function(woman,cost)
			{
				
				if(cost==null || cost=="")
				{
					cost=0;
					//$scope.AddedTrayFilling.womenCost = cost;
				}
				if(woman==null || woman=="")
				{
					woman=0;
					//$scope.AddedTrayFilling.womenCost = cost;
				}
				var totalWoman = Number(woman) * Number(cost);
				$scope.AddedTrayFilling.womenTotal = totalWoman.toFixed(2);
			};
			
		

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};


				$scope.loadBatchDetails = function(batchNumber,gridCount)
			 {
				
				 var trayFillingDate = $scope.AddedTrayFilling.date+"-";
				 if(trayFillingDate!=null)
				 {
				 var exptrayDate  = trayFillingDate.split("-");
				 var yydate  = exptrayDate[2].substring(2);
				 var trayDate = yydate+exptrayDate[1]+exptrayDate[0];
				  
				 }
				 
			     var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchNumbers.html",
					data: batchNumber,
					}).success(function(data, status) 
					{	
						
						$scope.addedbatchSeries=data;
						
						
						if(batchNumber.length>3)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchNumbersDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
							
						var conap  ="";
							var g= gridCount-Number(1);
							//$scope.datagrid[g].landVillageCode = data[0].landVillageCode;
							//$scope.datagrid[g].seedSupr = data[0].seedSupr;
							$scope.datagrid[g].variety = data[0].variety;
							$scope.datagrid[g].varietyName = data[0].varietyName;
							
							var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getServerDate.html",
						data: {},
				     }).success(function(data, status) 
					 {
						
						var datecon = data.serverdate+"-";
						
						var datesplit = datecon.split("-");
						var con1 = datesplit[1];
						
						var con2 = datesplit[0];
						
						 conap = con1+con2;
						  if(trayFillingDate!=null)
						 {
								$scope.datagrid[g].batchNo = batchNumber+"-"+trayDate;
						 }
						var seedlingType = batchNumber.charAt(0);
						$scope.datagrid[g].seedlingType = seedlingType;
							//alert(conap);
					
				 	 });	
						
						});
					}
				}); 
			 };
			 
			 $scope.clearBatchgrid = function()
			 {
				if($scope.AddedTrayFilling.modifyFlag=="No")
				{
					 if($scope.datagrid!=null)
					 {
						 $indexBud=1;
						$scope.datagrid = [{'id':$indexBud}];
					
					 }
				}
				
				
				
				
			 };

			
			$scope.getLabourGrandTotal = function(man,woman)
			{

				var labourTotal=0;
				if(man==null || man=="")
				{
					man = 0;
				}
				if(woman==null || woman=="")
				{
					woman = 0;
				}
				
				labourTotal = Number(man)+Number(woman);
				$scope.AddedTrayFilling.totalLabourCost = labourTotal.toFixed(2);
			};
			$scope.getContractTotal = function(cost,Totaltons)
			{
				if(cost==null || cost =="")
				{
					cost = 0;
				}
				if(Totaltons==null || Totaltons =="")
				{
					Totaltons = 0;
				}
				var grandContractTotal = 0;
				grandContractTotal = Number(cost)*Number(Totaltons);
				$scope.AddedTrayFilling.totalCost = grandContractTotal.toFixed(2);
				$scope.AddedTrayFilling.totalContractAmt = grandContractTotal.toFixed(2);
				
			};
			
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
					$scope.AddedTrayFilling = {'labourSpplr':'0','machineOrMen':'1','modifyFlag':'No','manCost':'300.00','womenCost':'200.00'};
				}); 
			};
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{					
						
						
							$scope.varietyNames = data;
						});  	   					
				};

				$scope.ShiftDropDownLoad = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: jsonStringShiftDropDown,
					   }).success(function(data, status) 
						{					
						
						
							$scope.WeighBridgeNames = data;
						});  	   					
				};

				$scope.displayTime = function(id,name)
				{
				
				$('.time').timepicker({ 'scrollDefault': 'now', format: 'HH:mm:ss'});
				var StoreseassionId = sessionStorage.setItem('sessionId',id);
			
				};
				
				$scope.CalculateShiftHrs = function()			
				{
//				
				var sessionId = sessionStorage.getItem('sessionId');
//			
				if(sessionId!='')
				{
				    var startTime = $('#from'+ sessionId).val();
					
				    var endTime = $('#to'+ sessionId).val();
					if(startTime!='' && endTime!='')
					{
	        	        var startTimeArray = startTime.split(":");
    	        	    var startInputHrs = parseInt(startTimeArray[0]);
        	        	var startInputMins = parseInt(startTimeArray[1]);

						var endTimeArray = endTime.split(":");
						var endInputHrs = parseInt(endTimeArray[0]);
			        	var endInputMins = parseInt(endTimeArray[1]);

					    var startMin = startInputHrs*60 + startInputMins;
   						var endMin = endInputHrs*60 + endInputMins;

					    var result;
	
					   if (endMin < startMin) 
					   {
					       var minutesPerDay = 24*60; 
				    	   result = minutesPerDay - startMin;  // Minutes till midnight
					       result += endMin; // Minutes in the next day
					   } 
					  else 
					   {
				    	  result = endMin - startMin;
					   }
	
					   var minutesElapsed = result % 60;
					   var hoursElapsed = (result - minutesElapsed) / 60;

					
					  
					  if(endTime.length>3)
					 {
						  $scope.datagrid[sessionId-1].totalTime = hoursElapsed +':'+ (minutesElapsed < 10 ? '0'+minutesElapsed : minutesElapsed)+":"+"00";
					}
					else
					{

						$scope.datagrid[sessionId-1].totalTime="";
					}
					  				  
					  
					}
			   }
			};
			
			$scope.addFormFieldBud = function() 
			{
				$indexBud++;
		    	$scope.datagrid.push({'id':$indexBud})
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			$scope.removeRowBud = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.datagrid.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.datagrid[s].id = Number(s)+Number(1);
				}
				
				
				 var totalAmount = 0;
			for(var k=0;k<$index;k++)
			{
				//alert($scope.data[k].totalCost);
			     totalAmount += $scope.datagrid[k].totalNoOfSeedlingss;
				 
			}
				  
			$scope.AddedTrayFilling.totalNoOfSeedlings = parseFloat(totalAmount);
			
  	    	};
			
			$obj = new Object();
			$scope.SaveTrayFilling = function(AddedTrayFilling,form)
			 {
				//alert($scope.trayfillingform.$valid);
			
				if($scope.trayfillingform.$valid) 
				{	
					  middleData = [];
					  
					  $scope.data.forEach(function (trayfillingdata) 
					  {
						  

	            		 middleData.push(angular.toJson(trayfillingdata));														 
				      });
					  
					  //alert(JSON.stringify(obj.middleData));
					  // alert(JSON.stringify(obj.formData));
					 
					 gridData = [];	
					  $scope.datagrid.forEach(function (BudData) 
					  {
						  //alert("entered");

	            		 gridData.push(angular.toJson(BudData));														 
				      });
					  obj.formData = angular.toJson(AddedTrayFilling);
					  obj.gridData = gridData;
					   obj.middleData = middleData;
				      
				     
					 // alert(JSON.stringify(obj));
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/saveTrayFilling.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Tray Filling Done Successfully!', "success");
									$scope.AddedTrayFilling = angular.copy($scope.master);
											$('.btn-hide').attr('disabled',false);
											form.$setPristine(true); 
											$scope.loadseasonNames();
											$index=1;
											$indexBud=1;
											$scope.data = [{'id':$index}]
											$scope.datagrid = [{'id':$indexBud}];
		
									
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}				
		 };
			$scope.reset= function(form)
			{
				$scope.AddedTrayFilling = angular.copy($scope.master);
											$('.btn-hide').attr('disabled',false);
											form.$setPristine(true); 
											$scope.loadseasonNames();
											$index=1;
											$indexBud=1;
											$scope.data = [{'id':$index}]
											$scope.datagrid = [{'id':$indexBud}];
										
			};
				$scope.getTrayfillingList = function(season,date,shift)
			{
				
				var trayFillingListobj = new Object();
			trayFillingListobj.season=season;
			trayFillingListobj.shift=shift;
			trayFillingListobj.trayFillingDate=date;
             // alert(JSON.stringify(trayFillingListobj));

			  if(season!='' && season!=null && date!='' && date!=null && shift!='' && shift!=null )
				 {
				  // alert(JSON.stringify(trayFillingListobj));
				 
				  var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllTrayfilling.html",
					data: JSON.stringify(trayFillingListobj),
					  }).success(function(data, status) 
					{	//alert(JSON.stringify(data));
						$scope.AddedTrayFilling=data.TrayfillingSummary;
						//$scope.data= data.InvTreatmentDetails;
						$scope.datagrid= data.TrayfillingDetails;
						$scope.getLabourContractorDetails();

					});
				 }
				
				
			};
		
		
		})
		
	.controller('seedlingIndentController', function($scope,$http,$rootScope,$state)
		{
			
			//$scope.AddedSeedlingIndent.indentNo = indent;
			//$scope.AddedSeedlingIndent.indentNo = indent;
			var VillageDropDown = new Object();
			var ryotSeasonObj = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
			
			$scope.master = {};
				var DropDownLoadSeason = new Object();
			DropDownLoadSeason.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(DropDownLoadSeason);

		var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";
		
			var fieldDropDown = new Object();
		 fieldDropDown.dropdownname = "FieldAssistant";
		 var StringDropDown= JSON.stringify(fieldDropDown);
		
			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			
			var FieldOffDownLoad = new Object();
		FieldOffDownLoad.dropdownname = "FieldOfficer";
		var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
		
		///Code added on 20-2-2017
					var d = new Date();
					var dd = d.getDate();
					var m = d.getMonth();
					var mm = m+1;
					var yy = d.getFullYear();
					var todayDate  = dd+"-"+mm+"-"+yy;
					
			$scope.loadDate = function()
			{
				$scope.AddedSeedlingIndent ={'indentedDate':todayDate,'purpose':'1'};
			};
			
			$scope.duplicateProductCode=function(kind,index)
			{
			
			//var productCode=productCode;
			var kind=kind;
			var count=0;
			 angular.forEach($scope.data, function(value,index) 
				{
					//var sum = (value.productCode);
					var kindsum = (value.kind);
					//alert(batch+"batch");
					//alert(batchsum+"batchsum");
			 			if(kind==kindsum)
						{
			   				count++;
			   			}
				})
			//alert("count"+count);
				if(count>1)
				 {
					swal("Duplicate Entry", "Kind already Exists  :)", "error");
					$scope.data[index].kind="";
			 	}
				else
				{
					
			 	}
			
			
			};
		
		$scope.onloadFunction=function(){
			var indent=$rootScope.indentNo;
			//alert("Front End"+indent);
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllModifySeedlingIndentDetails.html",
				data: indent,
			   }).success(function(data, status) 
				{
				//alert(JSON.stringify(data));
				$scope.getZonebyFieldOfficer(data.KindIndentSummary.foCode);
				
				$scope.getaggrementDetails(data.KindIndentSummary.season,data.KindIndentSummary.ryotCode);
				$scope.AddedSeedlingIndent = data.KindIndentSummary;
				
				$scope.data = data.KindIndentDetails;
				if(data.KindIndentSummary.storeCode==null)
					$scope.AddedSeedlingIndent.purpose='1';
				else
				$scope.AddedSeedlingIndent.purpose='0';
				}); 
			
		};
		
		
		//$scope.AddedSeedlingIndent = {'purpose':'1'};
		$scope.loadStoreCode = function()
		{  
		//alert("entered");
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStoreAuthorization.html",
				data: {},
			   }).success(function(data, status) 
				{
				$scope.storeCodeData = data;
					//alert(JSON.stringify(data));
					
				});
		};
			
			$scope.loadVillageNames = function()
		{	
	
		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNamesForVillage.html",
				data: jsonVillageDropDown,
			   }).success(function(data, status) 
				{
				
					$scope.villageNames=data;
				}); 
		};
		
		$scope.loadFieldOfficerDropdown = function()
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: FieldOffDropDown,
			  		 }).success(function(data, status) 
			   			{	
										//alert(JSON.stringify(data));

							$scope.FieldOffNames=data;
	 		   			});  	   
					};

		$scope.getZonebyFieldOfficer = function(fieldOfficer)
			{
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/GetZonesForIndent.html",
				data: fieldOfficer,
			   }).success(function(data, status) 
				{
				
					$scope.zoneNames=data;
				});
			};

			$scope.getIndentByzone = function(zone)
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/url.html",
				data: zone,
			   }).success(function(data, status) 
				{
				
					$scope.AddedSeedlingIndent.indentNo=data;
				});

			};
		
		$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{					
						
						//alert(JSON.stringify(data));
							$scope.varietyNames = data;
						});  	   					
				};
		$scope.ShowDatePicker = function()

{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};

		
		$scope.loadseasonNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
			  }).success(function(data, status) 
			  {
				
				$scope.seasonNames=data;
			  });
		};	
		/*$scope.getseasonRyots = function(seasoncode)
		{
			
			ryotSeasonObj.season = seasoncode;
		
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSeedlingAdvanceRyots.html",
					data: JSON.stringify(ryotSeasonObj),
					}).success(function(data, status) 
					{	 
					
						$scope.ryotCodeData=data;
					}); 	
		};
		*/
		$scope.loadRyotCodeDropDown = function()
				{
					//alert(ExtentRyotCode);
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							//alert("alertnotcoming");
							//alert(JSON.stringify(data));	
							$scope.ryotCodeData = data;
						});  	   
				};
				
		$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 //  alert(JSON.stringify(data));
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};	

		$scope.getIndentMax = function(season,zone)
		{
			var indentObj = new Object();
			indentObj.season = season;
			indentObj.zone = zone;
		if(season!='' && season!=null &&   zone!='' && zone!=null )
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetMaxKindIndent.html",
					data: JSON.stringify(indentObj),
					}).success(function(data, status) 
					{	 
						//alert(JSON.stringify(data));
					
						//$scope.ryotCodeData=data;
						$scope.AddedSeedlingIndent.indentNo = data[0].indentNo;
					});
			}
						//alert(JSON.stringify(indentObj));
					 	
		};

		$scope.loadRyotDet = function(RyotCode)
					{	
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllRyots.html",
						data: RyotCode,
						}).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							$scope.AddedSeedlingIndent.ryotName = data.ryot.ryotName;
							$scope.AddedSeedlingIndent.village = data.ryot.villageCode;
							$scope.AddedSeedlingIndent.fatherName = data.ryot.relativeName;
							$scope.AddedSeedlingIndent.aadhaarNumber = data.ryot.aadhaarNumber;
							$scope.AddedSeedlingIndent.phoneNumber = data.ryot.mobileNumber;
							
						});
					};
			
			$scope.loadKindType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllKindTypeDetails.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.kindType=data;
					});

			};

			$scope.getuomdetails= function(kind,id)
			{
				var kindTypeobj=new Object();
				kindTypeobj.kindType=kind;
				//alert(JSON.stringify(kindTypeobj));
				var httpRequest =$http
				({
					method:'POST',
						url:"/SugarERP/getAllKindDetailsByType.html",
						data:JSON.stringify(kindTypeobj),
				}).success(function(data,status)
				{
						//alert(JSON.stringify(data));
						//alert(data[0].uom);
						var g= id-Number(1);
						$scope.data[g].uom = data[0].uom; 
						//$scope.batchData[0].uom=data[0].uom;
				});
			};
			
			$scope.getfofaseedlingQty =function(agreement,season)
				{
						var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = agreement;
						
						 if(season!='' && season!=null && agreement!='' && agreement!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getExtraColumnAgrExtent.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									//01-03-2017
									$scope.AddedSeedlingIndent.individualExtent =data.individualExtent;
										$scope.AddedSeedlingIndent.noOfAcr = data.noOfAcr;
									});
									
									
							}
							
				};
			
			
			$scope.getaggrementDetails = function(season,ryotcode)
			{
				if(season!='' || season=="" && ryotcode!='' || ryotcode!=null)
				{
				var agrobj=new Object();
				agrobj.season=season;
				agrobj.ryotcode=ryotcode;
						var httpRequest = $http
						({
						method: 'POST',
						url : "/SugarERP/getaggrementDetailsauth.html",
						data: JSON.stringify(agrobj),
						}).success(function(data, status) 
						{
						$scope.agreementnumberNames = data;
						$scope.AddedSeedlingIndent.agreementNo = data[0].agreementnumber;
						var getfofaseedlingQtyObj = new Object();
							getfofaseedlingQtyObj.season = season;
							getfofaseedlingQtyObj.agreementnumber = data[0].agreementnumber;
							 if(season!='' && season!=null && data[0].agreementnumber!='' && data[0].agreementnumber!=null)
								{
								var httpRequest = $http
									({
									method: 'POST',
									url : "/SugarERP/getExtraColumnAgrExtent.html",
									data: JSON.stringify(getfofaseedlingQtyObj),
									}).success(function(data, status) 
									{
										//01-03-2017
									$scope.AddedSeedlingIndent.individualExtent =data.individualExtent;
										$scope.AddedSeedlingIndent.noOfAcr = data.noOfAcr;
								
										});
									}
							});
					}
				};
			
			$index=0;
			$scope.data=[];
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index,'status':'0'})
			};
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};

			$scope.seedlingIndentFormSubmit = function(AddedSeedlingIndent,form)
				{
				var obj= new Object();
					//alert($scope.seedlingIndentForm.$valid);
					if($scope.seedlingIndentForm.$valid)
					{
						  gridData = [];	
									  $scope.data.forEach(function (batchData) 
									  {
										  
				
										 gridData.push(angular.toJson(batchData));														 
									  });
									  obj.formData = angular.toJson(AddedSeedlingIndent);
									  obj.gridData = gridData;
									  
									 // delete obj["$index"];
								//	  alert(JSON.stringify(obj));
									$('.btn-hide').attr('disabled',true);
									$.ajax({
										url:"/SugarERP/SaveSeedlingIndentSummary.html",
										processData:true,
										type:'POST',
										contentType:'Application/json',
										data:JSON.stringify(obj),
										beforeSend: function(xhr) 
										{
											xhr.setRequestHeader("Accept", "application/json");
											xhr.setRequestHeader("Content-Type", "application/json");
										},
										success:function(response) 
										{
											if(response==true)
											{
												
												
												if($rootScope.indentNo==undefined)
												{
												
												swal("Success", 'Common Indent Details Added Successfully!', "success");
													$scope.AddedSeedlingIndent = angular.copy($scope.master);
													$('.btn-hide').attr('disabled',false);
													form.$setPristine(true);
													$index=1;
													$scope.data = [{'id':$index}]
													$scope.$apply();
														$scope.loadDate();
												}
												else
												{
													swal("Success", 'Common Indent Details Added Successfully!', "success");
													$rootScope.indentNo='';
													$scope.AddedSeedlingIndent = angular.copy($scope.master);
													$('.btn-hide').attr('disabled',false);
													form.$setPristine(true);
													$index=1;
													$scope.data = [{'id':$index}]
													$scope.$apply();
													
													
													$state.go('seedling.transaction.Indents');
												}
											
																		
											}
										   else
											{
												sweetAlert("Oops...", "Something went wrong!", "error");
											   $('.btn-hide').attr('disabled',false);
													
											}
										}
									});	
					}
					else
								{
										var field = null, firstError = null;
										for (field in form) 
										{
											if (field[0] != '$') 
											{
												if (firstError === null && !form[field].$valid) 
												{
													firstError = form[field].$name;
												}
												if (form[field].$pristine) 
												{
													form[field].$dirty = true;
												}
											}	
										}
								}
				};
				$scope.reset = function(form)
				{

				$scope.AddedSeedlingIndent = angular.copy($scope.master);
				$('.btn-hide').attr('disabled',false);
				form.$setPristine(true);
				$scope.loadDate();
				//$scope.loadseasonNames();
				$index=1;
				$scope.data = [{'id':$index}]
				};
		})
		
		
			.controller('IndentController',function($scope,$http,$rootScope)
		{
			$index=0;
			$scope.data=[];
			
			var obj = new Object();
			$scope.master = {};

			var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			
			
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);

		var FieldOffDownLoad = new Object();
		FieldOffDownLoad.dropdownname = "Employees";
		var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
		
		var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
				
				
			};
			
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
				//	$scope.AddedIndent = {'status':'0'};
				}); 
			};
			$scope.loadVarietyNames = function()
			{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: VarietyNameString,
					   }).success(function(data, status) 
						{					
						
						
							$scope.varietyNames = data;
						});  	   					
				};
				$scope.GetLoginUserStatus = function()
				{
						var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/GetLoginUserStatus.html",
							data: {},
					   }).success(function(data, status) 
						{		
							$scope.AddedIndent = {'status':data};
							//$scope.AddedIndent.status = parseInt(data);
								
								
							
								var indentUserStatus = $scope.AddedIndent.status;
								//alert("indentUserStatus"+indentUserStatus);
								var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/GetOnloadIndentsstatus.html",
										data: indentUserStatus,
								   }).success(function(data, status) 
								   {		
								 
										//alert(JSON.stringify(data));
										$scope.data=data;
								   }); 
								   
			
		
							
						});  	   					
				};
				
				
				
			$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				   //alert(JSON.stringify(data));
					$scope.AssistantNames = data; 	
				//added by naidu22-02-2017					//-------for load the data in to added departments grid---------
					 data.push({"fieldassistant":"ALL","id":0});
				});
		};
		$scope.loadFieldOfficerDropdown = function()
		{
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: FieldOffDropDown,
			   }).success(function(data, status) 
			   {		
			 
					$scope.FieldOffNames=data;
	 		   });  	   
		};
		
		
		$scope.ShowDatePicker = function()

{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		$scope.authorizationIndent  = function(season,ryotCode)
		{
			
			$rootScope.arraydefault= ryotCode;
			$rootScope.arraydefault1= season;
		};
		 
		 
			
		$scope.modifyIndent = function(indent)
		{
			
			$rootScope.indentNo=indent;
			if(indent==null || indent=="")
			{
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/url.html",
					data: indent,
			   }).success(function(data, status) 
			   {		
			 
					//alert(JSON.stringify(data));
	 		   });  
			}
		};
		//03-03-2017
		$scope.rejectIndent = function(season,ryotCode,indent,form)
		{
			var indentData=new Object();
			indentData.season=season;
			indentData.ryotCode=ryotCode;
			indentData.indent=indent;
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/indentReject.html",
					data: indentData,
			   }).success(function(data, status) 
			   {		
		
					if(data==true)
							{
								swal("Success", 'Indent Rejected  Successfully!', "success");
								
								$scope.GetLoginUserStatus();
								$scope.loadseasonNames();
							//	$scope.reset(form);
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
							  
							}
							// form.$setPristine(true);
								//$scope.$apply();
								 $route.reload();
	 		   });  	   
		};
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		$scope.fieldOff = function(field)
		{
			//alert("yes");
			//alert(field);
		};
		$scope.loadIndents = function(AddedIndent,form)
		{
			var indentLoad = JSON.stringify(AddedIndent);
			//alert(indentLoad);
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/GetIndentsUserListsDetails.html",
					data: indentLoad,
			   }).success(function(data, status) 
			   {		
			 
					//alert(JSON.stringify(data));
					$scope.data=data;
	 		   }); 
			
		};
		
		
		})
		
		.controller('DispatchFormController',function($scope,$http)
		{
			
				var d = new Date();
				var dd = d.getDate();
    			var mm = d.getMonth();
				var yy = d.getFullYear();
				var mm = d.getMonth()+1;
				var todayDate = dd+"-"+mm+"-"+yy;
				
			
			var LoadRyotDropDown = new Object();
			LoadRyotDropDown.dropdownname = "ExtentDetails";
			var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
				
			var VillageDropDown = new Object();
			VillageDropDown.dropdownname = "Village";
			var jsonVillageDropDown= JSON.stringify(VillageDropDown);
			
			var CircleDropDown = new Object();
			CircleDropDown.dropdownname = "Circle";
			var jsonCircleDropDown= JSON.stringify(CircleDropDown);
			
				var FieldOffDownLoad = new Object();
			FieldOffDownLoad.dropdownname = "FieldOfficer";
			var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
			
		var fieldDropDown = new Object();
		fieldDropDown.dropdownname = "FieldAssistant";
		var StringDropDown= JSON.stringify(fieldDropDown);
		
		var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
				var LoadVarietyName = new Object();
			LoadVarietyName.dropdownname = "Variety";
			var VarietyNameString= JSON.stringify(LoadVarietyName);
			
			var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "SeedlingTray";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);

		var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
					
					//$scope.trayTypeData = [{id:'1',trayType:""}];

			};
			$scope.getTransportContratorDetails = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTransportContratorDetails.html",
					data: {},
					}).success(function(data, status) 
					{
						$scope.lcNames = data;
					});
					
					

			};
	$scope.getfofaseedlingQty =function(agreement,season)
				{
						var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = agreement;
						 if(season!='' && season!=null && agreement!='' && agreement!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getAllAgreementDetailsfordispatch.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									$scope.FieldOffNames = data.FO;
									$scope.FieldAssistantData=data.FA;
									//added by naidu for sum of extent
									

									//$scope.AddedDispatchForm.foCode = data.FO[0].id;
									//$scope.AddedDispatchForm.faCode= data.FA[0].id;
									$scope.AddedDispatchForm.agreementSign = '0';
								
									
									
									var indentObj = new Object();
					indentObj.season = $scope.AddedDispatchForm.season;
					indentObj.RyotCode = $scope.AddedDispatchForm.ryotcode;
					indentObj.FieldOfficer = $scope.AddedDispatchForm.foCode;
					  if($scope.AddedDispatchForm.ryotcode!='' && $scope.AddedDispatchForm.ryotcode!=null && $scope.AddedDispatchForm.foCode!='' && $scope.AddedDispatchForm.foCode!=null && $scope.AddedDispatchForm.season!='' && $scope.AddedDispatchForm.season!=null )
				
						{
								
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getAuthorisedIndentsforDispatch.html",
									data: JSON.stringify(indentObj),
									  }).success(function(data, status) 
									{
										//alert(JSON.stringify(data));
										$scope.IndentData = data;
										//$scope.seasonNames=data;
										//$scope.AddDispatchForm = {'dispatchNo':'1'};
									});
						}
									
									
									
									});
									
									
							}
							
				};

			
			
	$scope.getaggrementDetails = function(season,ryotcode,agrType)
			{
		
		var agrobj=new Object();
		agrobj.season=season;
		agrobj.ryotcode=ryotcode;
		if(agrType==1){
					var httpRequest = $http
					({
					method: 'POST',
					
					url : "/SugarERP/getaggrementDetails.html",
					data: JSON.stringify(agrobj),
					}).success(function(data, status) 
					{
						
					
					$scope.agreementnumberNames = data;
					//24-03-2017
					//$scope.AddedDispatchForm.agreementnumber = data[0].agreementnumber;
					
					
					// extra code
					
					
					var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = data[0].agreementnumber;
						 if(season!='' && season!=null && data[0].agreementnumber!='' && data[0].agreementnumber!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getAllAgreementDetailsfordispatch.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									
									//$scope.FieldOffNames = data.FO;
									//$scope.FieldAssistantData=data.FA;
								
									$scope.AddedDispatchForm.seedlingQty = data.SeedlingQty;
								
									//$scope.AddedDispatchForm.foCode = data.FO[0].id;
									//$scope.AddedDispatchForm.faCode= data.FA[0].id;
								
									$scope.AddedDispatchForm.agreementSign = '0';
								
									
									
									});
									
									
							}
					
					
					});
	}
					else
					{
					var httpRequest = $http
					({
					method: 'POST',
					
					url : "/SugarERP/getaggrementDetailsauth.html",
					data: JSON.stringify(agrobj),
					}).success(function(data, status) 
					{
						
					
					$scope.agreementnumberNames = data;
					
					$scope.AddedDispatchForm.agreementnumber = data[0].agreementnumber;
					
					
					// extra code
					
					
					var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = data[0].agreementnumber;
						getfofaseedlingQtyObj.Screen ="auth";
						
						 if(season!='' && season!=null && data[0].agreementnumber!='' && data[0].agreementnumber!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getAllAgreementDetailsfordispatch.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									
									$scope.AddedDispatchForm.seedlingQty = data.SeedlingQty;
								
									$scope.AddedDispatchForm.agreementSign = '0';
								
									
									
									});
									
									
							}
					
					
					
					
					
					
					
					
					
						
					});	
					}
			};
			
		$scope.loadKindType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllKindTypeDetails.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						$scope.kindType=data;
					});

			};
						
			$scope.getSquantityForAgreement = function(season,agrnum,agrType)
			{
				
					var getfofaseedlingQtyObj = new Object();
						getfofaseedlingQtyObj.season = season;
						getfofaseedlingQtyObj.agreementnumber = agrnum;
						alert(season+""+agrnum+""+agrType);
						if(agrType==0)
						{
						getfofaseedlingQtyObj.Screen ="auth";
						}
						
						 if(season!='' && season!=null && agrnum!='' && agrnum!=null)
							{
							var httpRequest = $http
								({
								method: 'POST',
								url : "/SugarERP/getAllAgreementDetailsfordispatch.html",
								data: JSON.stringify(getfofaseedlingQtyObj),
								}).success(function(data, status) 
								{
									
									$scope.AddedDispatchForm.seedlingQty = data.SeedlingQty;
								
									$scope.AddedDispatchForm.agreementSign = '0';
								
									
									
									});
									
									
							}
			};
			
			

			$scope.loadIndents = function(season,ryotcode)
				{
					
					var indentObj = new Object();
					indentObj.Season = season;
					indentObj.RyotCode = ryotcode;
					  if(ryotcode!='' && ryotcode!=null && season!='' && season!=null )
				
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAuthorisedIndentsforDispatch.html",
						data: JSON.stringify(indentObj),
						  }).success(function(data, status) 
						{
							
							$scope.IndentData = data;
							//$scope.seasonNames=data;
							//$scope.AddDispatchForm = {'dispatchNo':'1'};
						});
					}
					
				};


			$scope.loadseasonNames = function()
			{
				//$scope.seasonNames=[];
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
					$scope.AddedDispatchForm = {'soilWaterAnalyis':'1','landSurvey':'1','agreementSign':'1','dateOfDispatch':todayDate,'agrType':'0'};
				}); 
			};
		
		//-------------Load Field Assistants-------
		$scope.loadFieldAssistants = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: StringDropDown,
			   }).success(function(data, status) 
			   {
				 
					$scope.FieldAssistantData = data; 		//-------for load the data in to added departments grid---------
				});
		};	
				
				
				$scope.loadRyotCodeDropDown = function()
				{
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: ExtentRyotCode,
					   }).success(function(data, status) 
						{
							
							$scope.ryotCode = data;
						});  	   
				};
				var villagecode="0";
				$scope.loadRyotDet = function(RyotCode)
					{	
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllRyots.html",
						data: RyotCode,
						}).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							$scope.AddedDispatchForm.ryotName = data.ryot.ryotName;
							villagecode = data.ryot.villageCode;
							$scope.AddedDispatchForm.ryotPhoneNumber = data.ryot.mobileNumber;
							
						});
					};


				
				$index=0;
			$scope.data=[];
			
			
			
			
		$scope.loadVarietyNames = function()
		{
							
							var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: VarietyNameString,
							   }).success(function(data, status) 
								{					
								
							//alert(JSON.stringify(data));
									$scope.varietyNames = data;
								});  	   					
		};

						$scope.loadGetMaxDispatchNo = function(season)
						{
							var loadMaxObj = new Object();
							loadMaxObj.season = season;
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/GetMaxDispatchNo.html",
							data: JSON.stringify(loadMaxObj),
					 }).success(function(data, status) 
					{
					$scope.AddedDispatchForm.dispatchNo = data[0].DispatchNo;
					$scope.$apply();
					}); 
					};


					$scope.loadIndentPopup=function(indentno){
				 $('#gridpopup_box').fadeIn("slow");
				var indentobj=new Object();
				indentobj.Indentno=indentno;
				$scope.AddedDispatchForm.indentNumbers="";
				
						var httpRequest = $http({
										method: 'POST',
										url : "/SugarERP/getAuthorisedIndentsforDispatchPopUp.html",
										data:JSON.stringify(indentobj),
								   }).success(function(data, status) 
									{
										
										//alert(JSON.stringify(data[0].foCode));
										//alert(JSON.stringify(data[0].faCode));

										$scope.AddedDispatchForm.foCode = data[0].foCode;
									$scope.AddedDispatchForm.faCode= data[0].faCode
										$scope.IndentNumberData=data;
										$scope.AddedDispatchForm.indentNumbers=data[0].IndentNo
									}); 
						 
					}
					
					var indentno="";
					$scope.loadPopup=function(indentno,ryotCode){
					indentno=indentno;
					 $scope.getpopup(indentno,ryotCode);
					
						
					}
					
					
					$scope.CloseDialogRatoon=function(){
						$('.popupbox_ratoon').fadeOut("slow");
						
						}


				
					var IndentObj=new Object();
					var IndentArray=new Array();
					var results = new Array();
					
					var indNo = [];
					
					$scope.optionToggled = function(checkbox,indentno,kind,quantity,variety,totalcost,costofeachitem,indentNumbers)
					{
						
					var ind = 1;
					   indNo.push(indentNumbers);			          
					     
					      
						if(checkbox==true){
							IndentObj=indentno;
							if(IndentArray.includes(indentno))
							{
							}
							else
							{
								IndentArray=IndentArray+IndentObj+',';
							}
							
							
						$scope.AddedDispatchForm.indentNumbers=IndentArray.replace(/,\s*$/, "");
						$scope.data.push({kind:kind,seedlingsVal:quantity,noOfSeedlings:quantity,indentNumbers:indentno,variety:variety,totalCost:totalcost,costOfEachItem:costofeachitem})
						
				
				if(indentNumbers == undefined || indentNumbers == "")
				{
					
					$scope.data=[];
					$scope.data=[{id:ind,kind:kind,seedlingsVal:quantity,noOfSeedlings:quantity,indentNumbers:indentno,variety:variety,totalCost:totalcost,costOfEachItem:costofeachitem}]
					$('.popupbox_ratoon').fadeOut("slow");
				}
				
				 else
				 {
					
					swal({
                             title: "Warning!",	
                            text: "do you want to reload the indent",
                           
  							 showCancelButton: true,
  							 confirmButtonColor: "#DD6B55",
  							 confirmButtonText: "Yes",
 							 closeOnConfirm: false,
							 closeOnCancel: false
						 },
						
							 function(isConfirm)
							 {
								 
								   if (isConfirm) 
								   {
									  swal.close();
									 
									   $index = 0;
									$scope.data=[];
									$scope.data=[{id:ind,kind:kind,seedlingsVal:quantity,noOfSeedlings:quantity,indentNumbers:indentno,variety:variety,totalCost:totalcost,costOfEachItem:costofeachitem}]
									 $('.popupbox_ratoon').fadeOut("slow");
									
								   } 
								   else 
								   {
									 swal.close();
										
								   }
                           
							 })
					
				 }
				
					
						
						}else if(checkbox==false)
						{
							  $scope.removeRow(name);
							 $scope.AddedDispatchForm.indentNumbers = null;
							 $index=1;
							}
						};
						

					$scope.loadFieldOfficerDropdown = function()
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/loadDropDownNames.html",
						data: FieldOffDropDown,
			  		 }).success(function(data, status) 
			   			{	
							$scope.FieldOffNames=data;
	 		   			});  	   
					};
					
					
			$scope.loadBatchDetails = function(batchNumber,gcount,variety)
			 {
				 alert('hai');
				var bobj = new Object();
				bobj.variety=variety;
				bobj.batch=batchNumber;
				alert('hai'+JSON.stringify(bobj));
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllBatchDetailsNumbersForDispatch.html",
					data: JSON.stringify(bobj),
					}).success(function(data, status) 
					{	
						alert(JSON.stringify(data));
						$scope.addedbatchSeries=data;
						
						
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAllBatchSeriesNoDetails.html",
						data: batchNumber,
					   }).success(function(data, status) 
						{
							alert(JSON.stringify(data));
						$scope.data[gcount].pendingQty=data[0].pendingQty;
						$scope.data[gcount].noOfSeedlings=0;
						});
				
				});
			 };
				
				$scope.getdispatchTotalCost = function(noOfSeedlings,costOfEachItem,id,batch,batchNo)
				{
					var seedling = $scope.data[id].seedlingsVal;
					var total=0;var seed = parseInt(noOfSeedlings);
					for(var i=0;i<$scope.data.length;i++)
					{
						 total +=  parseInt($scope.data[i].noOfSeedlings);
					}
			
					if(total > seedling)
					{
					sweetAlert("Oops...", "Exceeds seedling Indent Rised Quantity!", "error");
					for(var i=0;i<$scope.data.length;i++)
				      {			
						$scope.data[i].noOfSeedlings=0;
						$scope.data[i].totalCost=0;
				      }
					   					  
					}
					else
					{
						if(batch < noOfSeedlings)
					{
					
						swal("Exceeds seedling for this Batch Pending Quantity");
						for(var i=0;i<$scope.data.length;i++)
				      {			
						$scope.data[i].noOfSeedlings=0;
						$scope.data[i].totalCost=0;
				      }
					}
					else
					{
						var totalCost = Number(noOfSeedlings)*Number(costOfEachItem);
						$scope.data[id].totalCost = totalCost;
					}	
					}
				
					if(noOfSeedlings>seedlingsVal)
					{
						$("#validationSeedling"+id).show();
						$('.btn-hide').attr('disabled',true);
					}
					else
					{
						$("#validationSeedling"+id).hide();
						 $('.btn-hide').attr('disabled',false);
					}
					
					var costOfItem = $scope.dispatchData.costOfEachItem;
					
					var totalCost = Number(noOfSeedlings)*Number(costOfEachItem);
					$scope.data[id].totalCost = totalCost;
				

				 
				};
			$scope.loadTrayCost = function(trayType,ryotCode,id,tray)
			{
				
				var TrayCostObj = new Object();
				TrayCostObj.trayType = trayType;
				TrayCostObj.ryotCode = ryotCode;
				
			
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayTypeamount.html",
					data: JSON.stringify(TrayCostObj),
					}).success(function(data, status) 
					{
					
						$scope.data[id].trayCost = data[0].TrayCost;
						
						var totalCost = tray*$scope.data[id].trayCost;
						$scope.data[id].trayTotalCost = totalCost.toFixed(2);

					});
			};

			var obj = new Object();
			$scope.AddedDispatchSubmit = function(AddedDispatchForm,form)
			 {
			
				if($scope.AddedDispatchForm.landSurvey=="0" && $scope.AddedDispatchForm.agreementSign=="0" && $scope.AddedDispatchForm.soilWaterAnalyis=="0")
				 {
					if($scope.DispatchForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (dispatchData) 
					  {
						 gridData.push(angular.toJson(dispatchData));														 
				      });
					  obj.formData = angular.toJson(AddedDispatchForm);
					  obj.gridData = gridData;
					 
					  var gridSeedlingQty = 0;
					  gridSeedlingQty = parseFloat(gridSeedlingQty);
					  for(var i=0;i<obj.gridData.length;i++)
					  {
						  gridSeedlingQty =parseFloat(gridSeedlingQty)+ parseFloat($scope.data[i].noOfSeedlings);
						 
					  }
					  
					  var formSeedlingQty = $scope.AddedDispatchForm.seedlingQty;
					
						  $('.btn-hide').attr('disabled',true);
						  $.ajax({//SaveDispatchSummary
				    	url:"/SugarERP/SaveDispatchSummary.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								
								var DispatchNo = $scope.AddedDispatchForm.dispatchNo;
								var disobj=new Object();
								disobj.DispatchNo=DispatchNo;
								disobj.agrType=$scope.AddedDispatchForm.agrType;
								var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAcknowledgePrintDetails.html",
						data: disobj,
			  		 }).success(function(data, status) 
			   			{	
							
							$scope.dispatchPrnt = data.formData[0];
							$scope.dispatchPrnt.dispatchNo = DispatchNo;
							$scope.gridDetails = data.gridData;
							
							swal({
										   	 title: "Dispatch Done Successfully!",
										   	 text: " Do You want to take a print out?",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Print",
   											 closeOnConfirm: false}, 
											 
								function(){	
											var prtContent = document.getElementById("authprint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											$("#authprint").hide();
											$scope.AddedDispatchForm = angular.copy($scope.master);
											form.$setPristine(true);
											$scope.loadseasonNames();
											$scope.data=[];
											$('.btn-hide').attr('disabled',false);	 
											
											
										});
							
							
							
							
	 		   			});
								
								
								
								
								
								
//							
//								
//							
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
						  
					/*	  
					  }
					  else
					  {
						 sweetAlert("Oops...", "Check Seedling Quantity and try again !", "error");
						 $('.btn-hide').attr('disabled',false); 
					  }
				     */
				   // $('.btn-hide').attr('disabled',true);
					
				}	
				else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}
				 }
				 else
				 {
					
					sweetAlert("Oops...", "Select LandSurvey,Soil and Water Analysis andAgreement Signed Yes to proceed further !", "error");
				 }
								
			 };

				 $scope.reset = function(form)
				{
						$scope.AddedDispatchForm = angular.copy($scope.master);
					
					form.$setPristine(true);
					$scope.loadseasonNames();
					$('.btn-hide').attr('disabled',false);	
				};
				$indexId = 0;
				$index=1;
			
			var id1=1;
			$scope.duplicateField=function(indentNo,vriety,seedlings,seedlingsNo,cost,ttalCost,id)
			{
	
				
									swal({
										   	 title: "Warning!",	
										   	 text: "Duplicate Indent is added",
										// type: "warning",
											 showCancelButton: true,
											 confirmButtonColor: "#DD6B55",
   											 confirmButtonText: "Go",
   											 closeOnConfirm: true}, 
									   function(){
											
										});
										
									id++;
										
				$scope.data.push({'id':id,'indentNumbers':indentNo,'variety':vriety,'seedlingsVal':seedlings,'noOfSeedlings':seedlingsNo,'costOfEachItem':cost,'totalCost':ttalCost})
			
				var total=0;
				var seed =  seedlingsNo;
				for(var i=0;i<$scope.data.length;i++)
				{			
		
					 total +=  parseInt($scope.data[i].noOfSeedlings);
				}
			
				if(total == seed)
				{
					
				}
				else
				{
					 for(var i=0;i<$scope.data.length;i++)
				{			
		
					 $scope.data[i+1].noOfSeedlings = null;
				}
				
					 
				}
	
			   				   
			};
			
			
		
		
		
			$scope.removeRow = function(name)
			{	
			
			
			
				var Index = -1;		
				var comArr = eval( $scope.data );
				
				for( var i = 0; i < comArr.length; i++ ) 
				{
				
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
			
		})


		.controller('SubCategoryController',function($scope,$http)
		{
		})
		
		
		
		.controller('reprintcontroller', function($scope,$http)
		{
			
			$scope.ajaxCallcome = function()
			{
				
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAcknowledgePrintDetails.html",
						data: {},
			  		 }).success(function(data, status) 
			   			{	
							//$scope.FieldOffNames=data;
	 		   			});
			};
			
			
			
		})
		.controller('stockIssueController', function($scope,$http)
		{
			$index=0;
			
			$scope.data=[];
			
			
			$scope.addFormField = function() 
			{
				$index++;
		    	$scope.data.push({'id':$index})
			};
			
			
		
			$scope.removeRow = function(name)
			{				
				var Index = -1;		
				var comArr = eval( $scope.data );
				for( var i = 0; i < comArr.length; i++ ) 
				{
					if( comArr[i].id === name ) 
					{
						Index = i;
						break;
					}				
				}
				$scope.data.splice( Index, 1 );
				$index = comArr.length;
				for(var s=0;s<$index;s++)
				{
					$scope.data[s].id = Number(s)+Number(1);
				}
			
  	    	};
		})
		
		.controller('printDispatchController', function($scope,$http,$rootScope)
			{
						
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);


			$scope.loadseasonNames = function()
						{
							//$scope.seasonNames=[];
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: jsonStringSeasonDropDown,
							  }).success(function(data, status) 
							{
								$scope.seasonNames=data;
								//$scope.AddedDispatchForm = {'soilWaterAnalyis':'1','landSurvey':'1','agreementSign':'1','dateOfDispatch':todayDate};
							}); 
						};
							
			$scope.printDispatchForm = function(DispatchNo)
				{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAcknowledgePrintDetails.html",
						data: DispatchNo,
			  		 }).success(function(data, status) 
			   			{	
							
							$scope.dispatchPrnt = data.formData[0];
							$scope.dispatchPrnt.dispatchNo = DispatchNo;
							$scope.gridDetails = data.gridData;
	 		   			});
					
				};
				$scope.getDispatchDetails = function(season,dateFrom,dateTo)
				{
					
					var fromDate = dateFrom+"-";
					fromDateSpl = fromDate.split("-");
					var fromdate = fromDateSpl[2]+"-"+fromDateSpl[1]+"-"+fromDateSpl[0];
					
					var toDate = dateTo+"-";
					toDateSpl = toDate.split("-");
					var todate = toDateSpl[2]+"-"+toDateSpl[1]+"-"+toDateSpl[0];
					
					dispatchDetObj = new Object();
					dispatchDetObj.season  = season;
					dispatchDetObj.dateFrom  = fromdate;
					dispatchDetObj.dateTo  = todate;
								
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getDispatchNoDetailsForPrint.html",
						data: JSON.stringify(dispatchDetObj),
			  		 }).success(function(data, status) 
			   			{	
						$("#showTable").show();
						//alert(JSON.stringify(data));
							$scope.dispatchData = data;
							
	 		   			});
					
				};
				$scope.dispatchSMS = function(dispatchNo,id)
				
				{
					var season = $scope.AddedDispatchForm.season;
					var dateFrom = $scope.AddedDispatchForm.dateFrom;
					var dateTo = $scope.AddedDispatchForm.dateTo;
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/GetSMSStatus.html",
						data: dispatchNo,
			  		 }).success(function(data, status) 
			   			{	
						if(data==true)
						{
								swal("Success", 'SMS sent Successfully!', "success");
								$("#smsButton"+id).hide();
								
							$scope.getDispatchDetails(season,dateFrom,dateTo);
							  
						}
						else
						{
							 sweetAlert("Oops...", "SMS sending failed!", "error");
							 $("#smsButton"+id).show();
						}
							
	 		   			});
				};
				
				
				$scope.dispatchAck = function(season,dispatchNo)
				{
					$rootScope.season = season;
					$rootScope.dispatchNo = dispatchNo;
				};

				$scope.generatePrintDispatch = function(DispatchNo,agrType)
				{
					
					var disobj=new Object();
					disobj.DispatchNo=DispatchNo;
					disobj.agrType=agrType;
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAcknowledgePrintDetails.html",
						data: disobj,
			  		 }).success(function(data, status) 
			   			{	
							
							$scope.dispatchPrnt = data.formData[0];
							$scope.dispatchPrnt.dispatchNo = DispatchNo;
							$scope.gridDetails = data.gridData;


							//var prtContent = document.getElementById("authprint");
//											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
//											WinPrint.document.write(prtContent.innerHTML);
//											WinPrint.document.close();
//											WinPrint.focus();
//											WinPrint.print();
//											WinPrint.close();

swal({
										   	title: "Do You want to take a print out!",
										   	 text: " ",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Go",
   											 closeOnConfirm: false}, 
											 function(){
												
											var prtContent = document.getElementById("authprint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											$scope.dispatchPrnt = {}
							$scope.dispatchPrnt.dispatchNo = "";
							$scope.gridDetails = [];
										$("#authprint").hide();		 
											
											
										});

	 		   			});
					
					
					

				};
				


				


				

			})
		
		
		
		
		.controller('printAuthorisationController', function($scope,$http)
			{
						
			var SeasonDropDownLoad = new Object();
			SeasonDropDownLoad.dropdownname = "Season";
			var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);


			$scope.loadseasonNames = function()
						{
							//$scope.seasonNames=[];
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/loadDropDownNames.html",
							data: jsonStringSeasonDropDown,
							  }).success(function(data, status) 
							{
								$scope.seasonNames=data;
								//$scope.AddedDispatchForm = {'soilWaterAnalyis':'1','landSurvey':'1','agreementSign':'1','dateOfDispatch':todayDate};
							}); 
						};
							
			$scope.printDispatchForm = function(DispatchNo)
				{
				var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAcknowledgePrintDetails.html",
						data: DispatchNo,
			  		 }).success(function(data, status) 
			   			{	
							
							$scope.dispatchPrnt = data.formData[0];
							$scope.dispatchPrnt.dispatchNo = DispatchNo;
							$scope.gridDetails = data.gridData;
	 		   			});
					
				};
				$scope.getDispatchDetails = function(season,dateFrom,dateTo)
				{
					
					var fromDate = dateFrom+"-";
					fromDateSpl = fromDate.split("-");
					var fromdate = fromDateSpl[2]+"-"+fromDateSpl[1]+"-"+fromDateSpl[0];
				
					var toDate = dateTo+"-";
					toDateSpl = toDate.split("-");
					var todate = toDateSpl[2]+"-"+toDateSpl[1]+"-"+toDateSpl[0];
				
					dispatchDetObj = new Object();
					dispatchDetObj.season  = season;
					dispatchDetObj.dateFrom  = fromdate;
					dispatchDetObj.dateTo  = todate;
					//04-03-2017
					var username = sessionStorage.getItem('username');
					dispatchDetObj.loginId=username;
					
					var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAuthorisationDetailsForPrint.html",
						data: JSON.stringify(dispatchDetObj),
			  		 }).success(function(data, status) 
			   			{	
						$("#showTable").show();
						
							$scope.dispatchData = data;
							
							
	 		   			});
					
				};
/*

$scope.dispatchData=[];
$scope.$apply();
*/
				$scope.generatePrintDispatch = function(DispatchNo)
				{
				
											var newobject=new Object();	
											newobject.AuthFerNo=DispatchNo;
											newobject.Season=$scope.AddedDispatchForm.season;
											var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/getAuthorisePrintDetails.html",
											data:JSON.stringify(newobject),
										   }).success(function(data, status) 
											{
											//	alert("array"+JSON.stringify(data));
												
												$scope.items = data.formData;
												 $scope.itemsGrid=data.gridData;
												  $scope.itemsTotal=data.Total;
											$scope.storeLocation=data.formData[0].storeCity;
											$scope.storeCode=data.formData[0].storeCode;
												//alert($scope.authdate);
												//$scope.storeLocation=data.formData[0].authSeqNo;
												var printFlagupdation=new Object();
												printFlagupdation.authSeqNo=data.formData[0].authSeqNo;
												printFlagupdation.season=data.formData[0].season;
												var x=0;
												swal({
										   	title: "Do You want to take a print out!",
										   	 text: " ",
											 type: "success",
											 showCancelButton: true,
											 confirmButtonColor: "#2298f2",
   											 confirmButtonText: "Go",
   											 closeOnConfirm: false}, 
											 function(res){
												 x++;
												 if(res==true && x==1)
												 {
													 
													 //alert(JSON.stringify(printFlagupdation));
													 var httpRequest = $http({
											method: 'POST',
											url : "/SugarERP/updatePrintFlag.html",
											data:JSON.stringify(printFlagupdation),
										   }).success(function(data, status) 
											{
												var prtContent = document.getElementById("authprint");
											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
											WinPrint.document.write(prtContent.innerHTML);
											WinPrint.document.close();
											WinPrint.focus();
											WinPrint.print();
											WinPrint.close();
											
											});
												 }
												 else{
													 var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													 WinPrint.focus();
													 WinPrint.close();
												 }
												 
												 
												
											
											WinPrint.close();
											$scope.itemgrid = [];
											$scope.items = [];
											$scope.itemsTotal = [];
										$("#authprint").hide();		 
											
											
										});
												
												
												
												//var prtContent = document.getElementById("authprint");
//											var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
//											WinPrint.document.write(prtContent.innerHTML);
//											WinPrint.document.close();
//											WinPrint.focus();
//											WinPrint.print();
//											WinPrint.close();
												
												
											});
					
					//var httpRequest = $http({
//						method: 'POST',
//						url : "/SugarERP/getAcknowledgePrintDetails.html",
//						data: DispatchNo,
//			  		 }).success(function(data, status) 
//			   			{	
//						
//						
//													 
//						//alert(JSON.stringify(data));
//							
//							//$scope.dispatchPrnt = data.formData[0];
////							$scope.dispatchPrnt.dispatchNo = DispatchNo;
////							$scope.gridDetails = data.gridData;
//
//
//							
//	 		   			});
					
					
					

				};
				


				


				

			})
		.controller('SeedlingTrayRND', function($scope,$http)
	 {
		 
		var maxid = new Object();
		maxid.tablename = "SeedlingTray";
		var jsonTable= JSON.stringify(maxid);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "SeedlingTray";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		var VillageDropDownLoad = new Object();
		VillageDropDownLoad.dropdownname = "Village";
		var jsonvillageDropDownLoad= JSON.stringify(VillageDropDownLoad);
		
		
		var ZoneDropDownLoad = new Object();
		ZoneDropDownLoad.dropdownname = "Zone";
		var jsonStringDropDownZone= JSON.stringify(ZoneDropDownLoad);
		
		
		var fieldDropDown = new Object();
			fieldDropDown.dropdownname = "Circle";
			var CircleStringDropDown= JSON.stringify(fieldDropDown);
			
			 	var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";
		
		var DropDownLoadSeason = new Object();
		DropDownLoadSeason.dropdownname = "Season";
		var jsonStringSeasonDropDown= JSON.stringify(DropDownLoadSeason);
		
		$scope.loadZoneNames = function()
			{
				var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: jsonStringDropDownZone,
							 }).success(function(data, status) 
							 {
								$scope.ZoneData=data;
								//alert(JSON.stringify(data));
							 });  	   
			};
			$scope.updateotherTrays = function(damagedTray,ryotDamagedTray,id)
			{
				
				if(ryotDamagedTray<=damagedTray)
				{
					var remainingTrays = Number(damagedTray)-Number(ryotDamagedTray);
					$scope.data[id-1].damagedTrayTravelling = remainingTrays;
				}
				else
				{
					 sweetAlert("Oops...", "Ryot Damaged Trays cannot be greater than Damaged tray!", "error");
							  $scope.data[id-1].damagedTrayTravelling = "";
					
				}
				
			};
			
			$scope.loadseasonNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
			  }).success(function(data, status) 
			  {
				$scope.seasonNames=data;
			  });
		};
		
		$scope.updateDamagedTray = function(noofTraysReturn,traysinGoodCondition,id)
		{
			if(noofTraysReturn==null || noofTraysReturn=='')
			{
				noofTraysReturn =0;
			}
			if(traysinGoodCondition==null || traysinGoodCondition=='')
			{
				traysinGoodCondition =0;
			}
			var damageTray75 = Math.round(noofTraysReturn)*(100/100);
			var damageTray25 = Math.round(noofTraysReturn)*(0/100);
			//var damageT75  =  Math.round(damageTray75*100)/100;
			//var damageT25  =  Math.round(damageTray75*100)/100;
			
			$scope.data[id-1].traysinGoodCondition =  damageTray75.toFixed(2);
			
			$scope.data[id-1].damagedTray =   damageTray25.toFixed(2);
		};
		
		$scope.getseasonRyots = function(seasoncode)
				{
					var ryotSeasonObj = new Object();
					ryotSeasonObj.season = seasoncode;
					
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getSeedlingAdvanceRyots.html",
					data: JSON.stringify(ryotSeasonObj),
					}).success(function(data, status) 
					{	 
					
						$scope.ryotCodeData=data;
					}); 	
				
			};
		
			
			$scope.loadVillageNames = function()
		{		
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonvillageDropDownLoad,
			   }).success(function(data, status) 
			   {
					$scope.VillagesNamesData = data; 
					//alert(JSON.stringify(data));
					//-------for load the data in to added departments grid---------
				});
		};
		
		$scope.loadCircleNames = function()
			  {
				$scope.circleNames=[];
		 		var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: CircleStringDropDown,
				   }).success(function(data, status) 
				   {
						$scope.circleNames=data;
						//alert(JSON.stringify(data));
			  	   });  	   
			  };
			  
			 

			
			$scope.loadTrayType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getTrayType.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						
						$scope.TrayTypeNames = data;
						//alert(JSON.stringify(data));
						//alert(JSON.stringify($scope.TrayTypeNames ));
					});
			};
			
			$scope.ShowDatePicker = function()

		{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		 $scope.data = [];

		
		$scope.master = {};
		 $index=0;
		  var gridData = [];
		 var obj = new Object();
		$scope.addFormField = function() 
		{
			
			$index++;
		    $scope.data.push({'id':$index})
		};
		
		//----------Load Added Seedling Trays---
		
	
		 
		//----------Save Seedling Trays-----
		$scope.AddSeedlingsSubmit = function(AddedSeedlingTray,form)
		{
			//$scope.Addsubmitted=true;
			

		if($scope.SeedlingTrayMasterForm.$valid) 
				{	
					  gridData = [];	
					  $scope.data.forEach(function (SeedlingData) 
					  {
	            		 gridData.push(angular.toJson(SeedlingData));														 
				      });
					  obj.gridData = gridData;
				      obj.formData = angular.toJson(AddedSeedlingTray);
				      delete obj["$index"];
					 // alert(JSON.stringify(obj));
					
					$('.btn-hide').attr('disabled',true);
					$.ajax({
				    	url:"/SugarERP/SaveTrayUpdate.html",
					    processData:true,
					    type:'POST',
					    contentType:'Application/json',
					    data:JSON.stringify(obj),
						beforeSend: function(xhr) 
						{
			            	xhr.setRequestHeader("Accept", "application/json");
				            xhr.setRequestHeader("Content-Type", "application/json");
				        },
					    success:function(response) 
						{
							if(response==true)
							{
								swal("Success", 'Seedling Tray Added Successfully!', "success");	
								$('.btn-hide').attr('disabled',false);
								$scope.AddedSeedlingTray = angular.copy($scope.master);
								form.$setPristine(true);
								$index=1;
								$scope.data = [{'id':$index}];
								$scope.$apply();
								
							}
						   else
						    {
							   sweetAlert("Oops...", "Something went wrong!", "error");
							   $('.btn-hide').attr('disabled',false);
								
							}
						}
					});	
				}	
				else
				{
					var field = null, firstError = null;
					for (field in form) 
					{
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					}
				}
		};
		
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		//---------Reset Form----
		   $scope.reset = function(form)
		   {			  
				$scope.AddedSeedlingTray = angular.copy($scope.master);
				$index=1;
				$scope.seedlingTray={};
				$scope.data = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };		

		$scope.setValidation = function(id)
		  	{
			  var prvId="";
			  
			  if(id>1)
			  {
				  prvId = Number(id) - Number(1);
				 
				  var currentVal =  $('#from'+ id).val();
				  
				  var PrevVal =  $('#to'+prvId).val();
				   
				  var SameVal =  $('#to'+id).val();
			
				  if(currentVal<PrevVal || currentVal==SameVal || currentVal==PrevVal) 
				  { 
				  	$('#error'+ id).show();
					$('#saveButton').attr("disabled", true);
				  } 
				 else 
				  { 
				  	$('#error'+ id).hide();
					$('#errorto'+ PrvId).hide();
					$('#saveButton').attr("disabled", false);
				  }
			  }			  
		  };
		  $scope.setoValidation = function(id)
		  {
			  	  
			  	  var nextId = Number(id) + Number(1);	
				  var currentVal =  $('#from'+ id).val();
				  var nextVal =  $('#from'+nextId).val();
				  var SameVal =  $('#to'+id).val();			  	
				 
					 if(Number(SameVal)== Number(currentVal))
					 {
						 	$('#errorto'+ id).show();
						 	$('#saveButton').attr("disabled", true);
					 }
					 else
					 {
						 $('#errorto'+ id).hide();
						 	$('#saveButton').attr("disabled", false);
					 }
				 				  
				  
				  if(id<=$index)
				  {
					  if(SameVal==currentVal || SameVal>nextVal)
					  {
						
					  	$('#error'+ nextId).show();
						$('#saveButton').attr("disabled", true);					  
					  }
					 else
					  {
						 
					  	$('#error'+ id).hide();
						$('#saveButton').attr("disabled", false);					  					  
					  }
				  }
		  };
	 })

	 .controller('productMasterController', function($scope,$http)
	 {
	 })
		//========================================
				//Seedling weighment Report
		//========================================
	    .controller('SeedlingWeighmentController',function($scope,$http)
		{
			$scope.loadRyotCodeDropDown = function()
				{
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getSeedRyotsForReport.html",
							data: {},
					   }).success(function(data, status) 
						{
							$scope.seedSupplierNames = data;
							data.push({"seedSupplier":0,"seedSupplier":"All"});
						});  	   
				};
				
				$scope.generateSeedlingWeighmentDetails = function(fromDate,toDate,seedSupplier)
				{
					var frmDate  = fromDate+"-";
					var tDate  = toDate+"-";
					var fromDate = frmDate.split("-");
					var spfDate =  fromDate[2]+"-"+fromDate[1]+"-"+fromDate[0];
					
					var toDate = toDate.split("-");
					var sptDate =  toDate[2]+"-"+toDate[1]+"-"+toDate[0];
					
					var weighmentObj = new Object();
					weighmentObj.dateFrom = spfDate;
					weighmentObj.dateTo = sptDate;
					weighmentObj.seedsupplier = seedSupplier;
					
					
					
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getweighmentDetailsSeedSourceReport.html",
							data: JSON.stringify(weighmentObj),
					   }).success(function(data, status) 
						{
							$("#dispSeedlingWeighmentDetails").show(1500);
							//alert(JSON.stringify(data));
							$scope.weighmentData = data;
						});
					
				};
				
				
		})
		
		//========================================
				//Seedling Indent Report
		//========================================
	    .controller('SeedlingIndentReportController',function($scope,$http)
		{
			var DropDownLoad=new Object();
		DropDownLoad.dropdownname="Circle";
		var DropDownSeason= JSON.stringify(DropDownLoad);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname = "Zone";
		var jsonStringzoneDropDown= JSON.stringify(DropDownLoad);
		
		var varietyObj = new Object();
		varietyObj.dropdownname = "Variety";
		var jsonVarietyString= JSON.stringify(varietyObj);
		
		var DropDownLoadKind = new Object();
		DropDownLoadKind.dropdownname ="KindType";
			
			$scope.loadCircleNames=function()
			{
					
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: DropDownSeason,
				 }).success(function(data, status) 
				 {
				
					 $scope.CircleNames=data;
					 data.push({"id":0,"circle":"All"});
						
				 }); 
			};
			
			$scope.loadZoneNames = function()
			{
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: jsonStringzoneDropDown,
					}).success(function(data, status) 
					{ 
					
						data.push({"id":0,"zone":"All"});
						$scope.ZoneData=data;
						
					}); 
			};
			
			$scope.loadVarietyNames = function()
		{
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonVarietyString,
			   }).success(function(data, status) 
				{
					 data.push({"id":0,"variety":"All"});//alert(JSON.stringify(data));
					$scope.varietyNames=data;
				}); 
		};
		
		

	$scope.loadKindType = function()
			{
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getAllKindTypeDetails.html",
					data: JSON.stringify(DropDownLoadKind),
					}).success(function(data, status) 
					{
						
						$scope.kindData=data;
					});

			};

									
				
				$scope.generateSeedlingindentDetails = function(circle,zone,variety,KindType)
				{
					
					
					var weighmentObj = new Object();
					weighmentObj.Circle = circle;
					weighmentObj.Zone = zone;
					weighmentObj.Variety = variety;
					weighmentObj.KindType = KindType;
					
					//alert(JSON.stringify(weighmentObj));
					
					var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getIndentDetailsForReport.html",
							data: JSON.stringify(weighmentObj),
					   }).success(function(data, status) 
						{
							$("#dispSeedlingWeighmentDetails").show(1500);
						//	alert(JSON.stringify(data));
							$scope.weighmentData = data;
						});
					
				};
				
				
		})
		
			.controller('labourContractorController', function($scope,$http)
	{
		$scope.loadStatus = function()
		{
				$scope.AddedLabourContractor = {'status':'0','modifyFlag':'No'};
		};
		
		$scope.labourContractorSubmit = function(AddedLabourContractor,form)
			{
				
				
				  if($scope.labourContractorForm.$valid)
				  {
					  
				
					  
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					  
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
		
		$scope.reset = function(form)
		   {			  
				$scope.AddedLabourContractor = angular.copy($scope.master);
				$scope.loadStatus();
			    form.$setPristine(true);			    											   
		   };
	})
	
	
	.controller('companyTransportContractorController', function($scope,$http)
	{
		$scope.loadStatus = function()
		{
				$scope.AddedcompanyTransportContractor = {'status':'0'};
		};
		
		$scope.companyTransportContractorSubmit = function(AddedcompanyTransportContractor,form)
			{
				
				
				  if($scope.companyTransportContractorForm.$valid)
				  {
					  
				
					  
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					 
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
		
		$scope.reset = function(form)
		   {			  
				$scope.AddedcompanyTransportContractor = angular.copy($scope.master);
				$scope.loadStatus();
			    form.$setPristine(true);			    											   
		   };
	})
	

		.controller('authorisationStatusReportController' , function($scope,$http)
		{
			
			$scope.master = {};
			
			$scope.loadData=function(AddedauthorisationStatusReport){
					
		
			
			
			var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAuthorisationStatusReport.html",
							data: JSON.stringify(AddedauthorisationStatusReport),
							beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        }
				    
					   }).success(function(data, status) 
						{
							
							$scope.authorisationStatusReport=data;
						});
			};
		})
		
