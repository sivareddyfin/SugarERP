materialAdmin
//===================Store=========================

.controller('productMasterController', function($scope,$http,$rootScope,$state)
	 {
		 
		 $scope.master={};
		 	
		$scope.AddedProduct={'status':'0','modifyFlag':"No"};
		
		
		$scope.onloadFunction=function(){
			
			$("#productCode").focus();
			var productCode=$rootScope.gridProduct;
			
			
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductsDetailsByCode.html",
				data: productCode,
			   }).success(function(data, status) 
				{
					var stckMethod = data[0].stockMethod;
					$scope.loadSubGroup(data[0].productGroup);
					$scope.AddedProduct = data[0];
				
					$scope.AddedProduct.stockMethod = stckMethod.toString();
					
					
					
				}); 
			
		};
		
		
		 $scope.loadproductCode = function(Subgroupcode)
		{	
		
		if(Subgroupcode!=null){
			var jsonString= JSON.stringify(Subgroupcode);
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxProductbyid.html",
					data: Subgroupcode,
			   }).success(function(data, status) 
				{
					
					$scope.AddedProduct.productCode=data[0].productCode;
											
		        });	
		}	else{
			
			$scope.AddedProduct.productCode="";
		}			
	    };
		
		
		
		$scope.loaduom=function(){
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getUoms.html",
					data: {},
			   }).success(function(data, status) 
				{
					
					$scope.uoms=data;
											
		        });	 
			
			
		}
		
		$scope.loadStock=function(){
			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getStockMethods.html",
					data: {},
			   }).success(function(data, status) 
				{
					
					$scope.stockMethodCodes=data;
					
											
		        });	 
			
		}
		
		
		$scope.productSubmit = function(AddedProduct,form)
		  {
			  
			  var AddedProduct = JSON.stringify(AddedProduct);   /*------------for getting data from form-----------*/

			  if($scope.productForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
			    $.ajax({
				    url:"/SugarERP/SaveProducts.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:AddedProduct,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							if($rootScope.gridProduct==undefined)
							{
								swal("Sucess!", 'Product Added Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
								$scope.AddedProduct = angular.copy($scope.master);
								//$scope.loadproductCode();
								 //$scope.loadproductCode();
								 
								$scope.AddedProduct={'status':'0','modifyFlag':"No"};
								form.$setPristine(true);
								 $scope.$apply();
							}
							else
							{
								swal("Sucess!", 'Product Added Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
								$scope.AddedProduct = angular.copy($scope.master);
								//$scope.loadproductCode();
								 //$scope.loadproductCode();
								 
								$scope.AddedProduct={'status':'0','modifyFlag':"No"};
								form.$setPristine(true);
								 $scope.$apply();
								 $rootScope.gridProduct=undefined;

								 $state.go('store.transaction.ProductList');
							}

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };
		
		
			$scope.loadproducts=function()
			{
				
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getGroups.html",
							data: {},
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							$scope.addedProductCodes=data;
						})

			};
			
			$scope.loadSubGroup=function(GroupCode){
				//alert(GroupCode);
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getSubGroupsByGrp.html",
							data: GroupCode,
					   }).success(function(data, status) 
						{
							//alert(JSON.stringify(data));
							$scope.addedProducsubGroupCodes=data;
						})
				
				
			}
		
		   $scope.reset = function(form)
		   {			  
				$scope.AddedProduct = angular.copy($scope.master);
				$scope.loadproductCode();
			    form.$setPristine(true);			    											   
		   };
	 })
	 
	 
	
	.controller('productGroupMasterController', function($scope,$http)
	{
			$scope.loadStatus = function()
			{
				$("#productGroup").focus();
				$scope.Addedproductgroup = {'status':'0','modifyFlag':'No'};
			};
			$scope.getAllProductGroups = function()
			{
				
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllProductGroups.html",
							data: {},
					   }).success(function(data, status) 
						{
							
							$scope.productData = data;
							
						});
				
			};
			$scope. getMAXProductGroup = function()
			{
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/ getMAXProductGroup.html",
							data: {},
					   }).success(function(data, status) 
						{
							
							$scope.Addedproductgroup = {'groupCode':data[0].MAXID,'status':'0','modifyFlag':'No'};
							
						});
			};
			
			$scope.validateDupProductName = function()
		  {
			//alert(JSON.stringify($scope.AddedRyot));
				for(var d =0;d<$scope.productData.length;d++)
				{
					 //alert($scope.SeasonData);
					var firstString = $scope.Addedproductgroup.group.toUpperCase();
					
					var secondString = $scope.productData[d].group.toUpperCase();
					
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicateproductGroupName").show();
							return false;
						}					
					   else
					    {
							$(".duplicateproductGroupName").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  $scope.validateDupGroupCode = function()
		  {
			//alert(JSON.stringify($scope.AddedRyot));
				for(var d =0;d<$scope.productData.length;d++)
				{
					 //alert($scope.SeasonData);
					var firstString = $scope.Addedproductgroup.groupCode.toUpperCase();
					
					var secondString = $scope.productData[d].productGroupCode.toUpperCase();
					
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicateproductGroup").show();
							return false;
						}					
					   else
					    {
							$(".duplicateproductGroup").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  
		  
		  	$scope.validateDuplicate = function(desigName,id)
		  {
			 
			
			 for(var i =0;i<JSON.stringify($scope.productData.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				
				var str2 = $scope.productData[i].group.toUpperCase();
				
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };
		  
		  
			
			$scope.productSubgroupSubmit = function(Addedproductgroup,form)
			{
				
				  if($scope.productGroupMaster.$valid)
				  {
					//  alert(JSON.stringify(Addedproductgroup));
					    $('.btn-hide').attr('disabled',true);
						
						$.ajax({
				    url:"/SugarERP/SaveProductGroup.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(Addedproductgroup),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success!", 'Product Group Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.Addedproductgroup = angular.copy($scope.master);
							form.$setPristine(true);
							$scope.loadStatus();
							$scope.getAllProductGroups();
							$scope.getMAXProductGroup();
							

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
						
						
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			
			 $scope.UpdateProductGroup = function(AddedProductGroup,id)
		  {		
		  	  			  
			 		  delete AddedProductGroup["$edit"];
			 		 var UpdatedData = angular.toJson(AddedProductGroup);
			 		 $scope.isEdit = true;
					$scope.submitted = true;
					if($scope.updateproductForm.$valid)
					{
						$scope.isEdit = false;
						$('.btn-hide').attr('disabled',true);
			  	$.ajax({
				    url:"/SugarERP/SaveProductGroup.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
						if(response==true)
						{
			  				swal("Success", 'Updated Successfully!', "success");
							$('.btn-hide').attr('disabled',false);	
							//$('.btn-hide').attr('disabled',false);

						//---------get all productGroups--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/url.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.productData=data;
											$scope.loadStatus();
											$scope.getAllProductGroups();
											$scope.getMAXProductGroup();
											$('.btn-hide').attr('disabled',false);		
							 			});  	   
							//---------End--------
						 	}
						    else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
					 	  }
						});			
				}
			//-----------end----------			
			};
			
			
				$scope.reset = function(form)
		   {	
		  	 $('.btn-hide').attr('disabled',false);
				$scope.Addedproductgroup = angular.copy($scope.master);
				$scope.loadStatus();
				$scope.getAllProductGroups();
				$scope.getMAXProductGroup();
				
			    form.$setPristine(true);
				$scope.$apply();
		   };
	})
	
	
	.controller('productsubGroupMasterController', function($scope,$http)
	{
			$scope.loadStatus = function()
			{
				$("#groupCode").focus();
				$scope.AddedproductSubgroup= {'status':'0','modifyFlag':'No'};
			};
			$scope.loadproducts=function()
			{
				
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getGroups.html",
							data: {},
					   }).success(function(data, status) 
						{
							
							$scope.addedProductCodes=data;
						})

			};
			$scope.getProductsDetailsBygroupCode = function(groupCode)
			{
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getMaxSubGroupsbyGroupid.html",
							data: groupCode,
					   }).success(function(data, status) 
						{
							
							
							$scope.AddedproductSubgroup.group = data[0].GroupName;
							$scope.AddedproductSubgroup.subGroupCode = data[0].SubGroupId;
						})
			};
			
			
			
			
				$scope.getAllProductSubGroups = function()
			{
				var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllProductSubGroups.html",
					data: {},
			   }).success(function(data, status) 
				{
					$("#hidegrid").show();
					$scope.productData = data;
					
		        });	
			};
			
			
			 $scope.UpdateProductGroup = function(AddedProductGroup,id)
		  {		
		  	  			  
			 		  delete AddedProductGroup["$edit"];
			 		 var UpdatedData = angular.toJson(AddedProductGroup);
			 		 $scope.isEdit = true;
					$scope.submitted = true;
					if($scope.updateProductSubGroupform.$valid)
					{
						$scope.isEdit = false;
						
			  	$.ajax({
				    url:"/SugarERP/SaveProductSubGroup.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
						if(response==true)
						{
			  				swal("Success", 'Updated Successfully!', "success");
							$('.btn-hide').attr('disabled',false);	
							

							   
							//---------End--------
						 	}
						    else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
					 	  }
						});			
				}
			//-----------end----------			
			};
			
			
			$scope.getMaxSubGroupsbyGroupName = function(groupCode,id)
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxSubGroupsbyGroupid.html",
					data: groupCode,
			   }).success(function(data, status) 
				{
					
				$scope.productData[id].group = data[0].GroupName;
				});	
			};
			
			
			$scope.productSubgroupSubmit = function(AddedproductSubgroup,form)
			{
				
				
				  if($scope.productsubGroupMaster.$valid)
				  {
					 
					  $('.btn-hide').attr('disabled',true);
						
					$.ajax({
				    url:"/SugarERP/SaveProductSubGroup.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(AddedproductSubgroup),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							$("#hidegrid").hide();
							$scope.reset(form);
							$scope.$apply();
							swal("Success!", 'Product Subgroup Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							
							
							
							
							

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
					  
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					  
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			$scope.reset = function(form)
		   {	
		  
				$scope.AddedproductSubgroup = angular.copy($scope.master);

			    form.$setPristine(true);
				$scope.loadStatus();
				
		   };
	})
	
	
	.controller('uomMasterController', function($scope,$http)
	{
			$scope.loadStatus = function()
			{
				$("#uom").focus();
				$scope.AddedUOM = {'status':'0','modifyFlag':'No'};
			};
			$scope.getAllUOM = function()
			{
				
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllUOM.html",
							data: {},
					   }).success(function(data, status) 
						{
							
							$scope.uomData = data;
							
						});
				
			};
			$scope.getMAXUOM = function()
			{
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getMAXUOM.html",
							data: {},
					   }).success(function(data, status) 
						{
							
							$scope.AddedUOM = {'uomCode':data[0].MAXID,'status':'0','modifyFlag':'No'};
							
						});
			};
			
				$scope.validateDup = function()
		  {
			//alert(JSON.stringify($scope.AddedRyot));
				for(var d =0;d<$scope.uomData.length;d++)
				{
					 //alert($scope.SeasonData);
					var firstString = $scope.AddedUOM.uom.toUpperCase();
					
					var secondString = $scope.uomData[d].uom.toUpperCase();
					
					if(firstString!=null)
					{
						if(firstString==secondString)
						{
							$('.btn-hide').attr('disabled',true);						
							$(".duplicate").show();
							return false;
						}					
					   else
					    {
							$(".duplicate").hide();
							$('.btn-hide').attr('disabled',false);						
						}
					}
				}
				
		  };
		  
		  	$scope.validateDuplicate = function(desigName,id)
		  {
			 
			
			 for(var i =0;i<JSON.stringify($scope.uomData.length);i++)
			 {
				var str1 = desigName.toUpperCase();
				
				var str2 = $scope.uomData[i].uom.toUpperCase();
				
				if(id!=i)
				{
				 if(str1 == str2)
				 {
					$('.btn-hideg').attr('disabled',true);						
							$(".duplicate"+ id).show();
							return false;
				 }
				 else
				 {
					 $(".duplicate"+ id).hide();
					 $('.btn-hideg').attr('disabled',false);	
				 }
				}
			 }
		  };
		  
			
			$scope.UOMSubmit = function(AddedUOM,form)
			{
				
				  if($scope.uomMaster.$valid)
				  {
					  //alert(JSON.stringify(AddedUOM));
					    $('.btn-hide').attr('disabled',true);
						
						$.ajax({
				    url:"/SugarERP/SaveUom.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(AddedUOM),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
					
						if(response==true)
						{
							swal("Success!", 'UOM Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.AddedUOM = angular.copy($scope.master);
							$scope.loadStatus();
							form.$setPristine(true);
							$scope.getMAXUOM();
							
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllUOM.html",
							data: {},
						   }).success(function(data, status) 
							{
								$scope.uomData = data;
								$('.btn-hide').attr('disabled',false);		
								
							});

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
						
						
					
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			 $scope.updateUom = function(addedUom,id)
		  {		
		  	  			  
			 		  delete addedUom["$edit"];
			 		 var UpdatedData = angular.toJson(addedUom);
			 		 $scope.isEdit = true;
					$scope.submitted = true;
					if($scope.UpdateUOMForm.$valid)
					{
						$scope.isEdit = false;
						$('.btn-hide').attr('disabled',true);
			  	$.ajax({
				    url:"/SugarERP/SaveUom.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:UpdatedData,
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{			  
						if(response==true)
						{
			  				swal("Success", 'Updated Successfully!', "success");
							$('.btn-hide').attr('disabled',false);	
							//$('.btn-hide').attr('disabled',false);

						//---------get all Seasons--------									
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/url.html",
									data: {}
							 			}).success(function(data, status) 
							 			{
											$scope.uomData=data;
											$('.btn-hide').attr('disabled',false);		
							 			});  	   
							//---------End--------
						 	}
						    else
							{
								sweetAlert("Oops...", "Something went wrong!", "error");
							}
					 	  }
						});			
				}
			//-----------end----------			
			};
			
			
			
				$scope.reset = function(form)
		   {			  
				$('.btn-hide').attr('disabled',false);		
				$scope.AddedUOM = angular.copy($scope.master);
				$scope.loadStatus();
				$scope.getMAXUOM();
			    form.$setPristine(true);
				
		   };
	})
	
	 .controller('otherRyotMaster', function($scope,$http)
	 {
		 
		 var DropDownLoad = new Object();
		DropDownLoad.tablename = "Bank";
		DropDownLoad.columnName = "bankcode";
		DropDownLoad.columnName1 = "bankname";
		var jsonStringDropDown= JSON.stringify(DropDownLoad);
		
		var DropDownLoad = new Object();
		DropDownLoad.dropdownname="Branch";
		var StringDropDown= JSON.stringify(DropDownLoad);
			
		
		$scope.loadStatus = function()
		{
			$scope.Addedryots = {'status':'0'};
		};
		
			 $scope.loadBanks = function()
		 {
			 
			 var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadAddedDropDownsForMasters.html",
					data: jsonStringDropDown,
				   }).success(function(data, status) 
					{
						//alert(JSON.stringify(data));
						//alert(JSON.stringify(data));
						$scope.banknames=data;							
					});											 
		 };
		 
		 $scope.loadBranchNames=function()
		    {			
			var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/loadDropDownNames.html",
					data: StringDropDown,
			   }).success(function(data, status) 
			   {
				  
					$scope.branchNames=data;
			   }); 
		};
		
		
		$scope.otherRyotSubmit = function(Addedryots,form)
			{
				
				  if($scope.otherRyotMaster.$valid)
				  {
					    $('.btn-hide').attr('disabled',true);
						
						$.ajax({
				    url:"/SugarERP/url.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(Addedryots),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							swal("Success", 'Ryot Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							$scope.Addedryots = angular.copy($scope.master);
							$scope.loadStatus();
							form.$setPristine(true);

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);							 
						 }
				    }
		         });
						
						
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
				$scope.reset = function(form)
		   {			  
				$scope.Addedryots = angular.copy($scope.master);
				$scope.loadStatus();
			    form.$setPristine(true);			    											   
		   };
		   
	 })
	 
	 //====================Transactions
	 
	.controller('OpeningStockController', function($scope,$http)
	{
		
		 $scope.data = [];
		 $scope.master = {};
		 $index=0;
		 
		$scope.addFormField = function() 
		{
		
		$index++;
		$scope.data.push({'id':$index});
		
		
		};
			
		
		$scope.loadProductCode = function(productCode,id)
		{
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductCodes.html",
				data: productCode,
				}).success(function(data, status) 
				{
					$scope.addedProductCode=data;
				if(productCode.length>=2)
			{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getProductDetailsByCodes.html",
					data: productCode,
				   }).success(function(data, status) 
					{
						
						$scope.data[id].productName = data[0].ProductName;
						$scope.data[id].uom = data[0].Uom;
						$scope.data[id].uomName = data[0].Uomname;
					});
			}
				});
			
		}
			
		$scope.getbatchPopup = function(productcode,id,deptId)
		{
			//alert("department"+deptId);
			//alert('hello');
			if(productcode==undefined)
			{
				swal("Product Code not given", "Product Code is required :)", "error");
				$("#prodcode"+id).focus();
				return false;
			}
		
			else if(productcode!=undefined)
			{
				$scope.productCode = productcode;
				$scope.productId = id;
				$scope.productBatchData = [];
			
				var stockObj = new Object();
				stockObj.productCode = productcode;
				
				stockObj.deptId = parseInt(deptId);
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductBatches.html",
				data: JSON.stringify(stockObj),
			}).success(function(data, status) 
		   {	
				$('#gridpopup_box').fadeIn("slow");
				$scope.productBatchData = data;
				$scope.screenName ="OpeningStock";
		   });
			}
		};

		
		$scope.batchDatapush = function(checkbox,batch,expiryDate,mrp,id,batchSeqNo,costPrice,batchId,mfrDate,stock,stockInDate)
		{
			var gridLength = $scope.data.length;
			if(checkbox==true)
			{
				var rowId = $scope.productId;
				$scope.popupId = id;
				 $('#gridpopup_box').fadeOut("slow");
				 
				 $scope.data[rowId].batch = batch;
				 $scope.data[rowId].expDate = expiryDate;
				 $scope.data[rowId].mrp = mrp;
				 $scope.data[rowId].batchFlag = 'Old';
				
				 $scope.data[rowId].batchSeqNo = batchSeqNo;
				 $scope.data[rowId].cp = costPrice;
				 $scope.data[rowId].batchId = batchId;
				 $scope.data[rowId].mfrDate = mfrDate; 
				 $scope.data[rowId].modifyFlag = 'Yes';
			}
			else if(checkbox==false)
			{
							
							
			}
						
		};
		
		$scope.CloseBatchPopup = function(batch,expdt,mrp,rowId)
		{
				
		
			 $('#gridpopup_box').fadeOut("slow");
				
			
			$scope.data[rowId].batch = batch;
			$scope.data[rowId].expDate = expdt;
			$scope.data[rowId].mrp = mrp;
			$scope.data[rowId].batchFlag = 'New';
			$scope.data[rowId].batchSeqNo = parseInt(0);
			//$scope.data[rowId].modifyFlag = 'No';
			
			
		};
			
			
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
		
		};
		
		$scope.ShowDatePicker = function()
		{
			$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					//alert(JSON.stringify(data));
					$scope.DepartmentsData=data;
				}); 
			};
			
			$scope.OpeningStockSubmit = function(OpeningStock,form)
			{
				if($scope.OpeningStockForm.$valid) 
				{
					//alert('hello');
					$('.btn-hide').attr('disabled',true);
					gridData = [];
					$scope.data.forEach(function (openingstockData) 
					{
						gridData.push(angular.toJson(openingstockData));														 
				    });
					
					var obj = new Object();
					$scope.OpeningStock.finYr='';
					
					obj.formData = angular.toJson(OpeningStock);
					obj.gridData = gridData;
					
					$.ajax({
				    url:"/SugarERP/saveOpeningStock.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							
							$('.btn-hide').attr('disabled',false);

							swal("Sucess!", 'Opening Stock Updated Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							
							$scope.OpeningStock = angular.copy($scope.master);
							$index=1;
							$scope.data = [{'id':$index}];
							
							form.$setPristine(true);
							$('.btn-hide').attr('disabled',false);
							form.$setPristine(true);
							$scope.$apply();
							
						 }
						 else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						 }
				    }
		         });
					
				}
				else
				{
					var field = null, firstError = null;
					for (field in form) 
					{
						
						if (field[0] != '$') 
						{
							if (firstError === null && !form[field].$valid) 
							{
								firstError = form[field].$name;
							}
							if (form[field].$pristine) 
							{
								form[field].$dirty = true;
							}
						}	
					} 
				}
			};
			$scope.resetForm = function(form)
			{
		 			$('.btn-hide').attr('disabled',false);
							
							$scope.OpeningStock = angular.copy($scope.master);
							$index=1;
							$scope.data = [{'id':$index}];
							
							form.$setPristine(true);
							$('.btn-hide').attr('disabled',false);
								
							
			};
	})

	 
	  .controller('stockSupplyController', function($scope,$http,$rootScope,$state)
	 {
		  $scope.data = [];
		 $scope.master = {};
		 $index=0;
		 
		  var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		  //alert(mstring.length);
		  var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 

		$scope.onloadFunction=function(){
			var stockInNo=$rootScope.arraydefault;
			//$rootScope.arraydefault=null;
			if(stockInNo==undefined)
			{
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxStockInid.html",
				data:'',
				//data: {}
				}).success(function(data, status) 
				{
					$scope.AddedStock={'stockInNo':data[0].productCode,'entryDate':todayDate};
				}); 
			}
			else
			{
				
				$scope.AddedStock={'stockInNo':stockInNo,'entryDate':todayDate};
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockInDetails.html",
				data: stockInNo,
			   }).success(function(data, status) 
				{
					
					var id = $index-1;
					$scope.AddedStock = data.StockInSummaryBean;
					$scope.data = data.StockInDetailsBean;
				var pcode = $scope.data[id].productCode;
				
				
				for(var i=0;i<$scope.data.length;i++)
					{
						
						var pcode = data.StockInDetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
					
				});
			}



			//$scope.AddedStock.stockInNo = stockInNo;
			 
			
		};



		  $scope.data = [];
		 $scope.master = {};
		 $index=0;
		 
		 
		 var obj = new Object();
		 
		
		 
		 	$scope.addFormField = function() 
			{
			
			$index++;
		    $scope.data.push({'id':$index});
			
			
			};
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.getbatchPopup = function(productcode,id,deptId)
		{
			//alert('deptId-----'+deptId);
			if(productcode==undefined)
			{
				swal("Product Code not given", "Product Code is required :)", "error");
	  			$("#prodcode"+id).focus();
				return false;
			}
			
			else if(productcode!=undefined)
			{
				
				$scope.productCode = productcode;
				$scope.productId = id;
				$scope.productBatchData = [];
		
				
				var stockObj = new Object();
				stockObj.productCode = productcode;
				stockObj.deptId = parseInt(deptId);
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductBatches.html",
				data: JSON.stringify(stockObj),
			}).success(function(data, status) 
		   {
				$('#gridpopup_box').fadeIn("slow");
				$("#batchPopupScreen").show();
				$scope.productBatchData = data;
		   });
				
	
				
			
			}
		};
		

		$scope.batchDatapush = function(checkbox,batch,expiryDate,mrp,id,batchSeqNo,costPrice,batchId,mfrDate,stock,stockInDate)
		{
			//var gridLength = $scope.data.length;
			if(checkbox==true)
			{
				//alert(expiryDate);
				var rowId = $scope.productId;
				$scope.popupId = id;
				 $('#gridpopup_box').fadeOut("slow");
				 
				 $scope.data[rowId].batch = batch;
				 $scope.data[rowId].expDt = expiryDate;
				 $scope.data[rowId].mrp = mrp;
				 $scope.data[rowId].batchFlag = 'Old';
				 
			
				 $scope.data[rowId].batchSeqNo = batchSeqNo;
				 $scope.data[rowId].cp = costPrice;
				 $scope.data[rowId].batchId = batchId;
				// $scope.data[rowId].mfrDate = mfrDate; 
			
			}
			else if(checkbox==false)
			{
							
							
			}
						
		};
		
		$scope.CloseBatchPopup = function(batch,expdt,mrp,rowId)
		{
				
		
			 $('#gridpopup_box').fadeOut("slow");
				$("#batchPopupScreen").hide();
			
			$scope.data[rowId].batch = batch;
			$scope.data[rowId].expDt = expdt;
			$scope.data[rowId].mrp = mrp;
			$scope.data[rowId].batchFlag = 'New';
			$scope.data[rowId].batchSeqNo = parseInt(0);
			//$scope.data[rowId].modifyFlag = 'No';
			
			
		};
		
	
	$(document).keyup(function(e) {
     if (e.keyCode == 27) {
		
		 $('#gridpopup_box').fadeOut("slow");
		//  $scope.stockIssueData=array;
		 
		
    }
});
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					$scope.DepartmentsData=data;						
				}); 
				
			};
			
			$scope.loadProductCode = function(productCode,gridCount)
			 {
				
				  var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						
									var g= gridCount;
									$scope.data[g].productName = data[0].ProductName;
									$scope.data[g].uom = data[0].Uom;
									$scope.data[g].uomName = data[0].Uomname;
						 
						});
					}
				}); 
				
					
				 };
				 
				 $scope.stockInSubmit = function(AddedStock,form)
		  {
			  
			  if($scope.stockinForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
				 gridData = [];
				 $scope.data.forEach(function (stockinData) 
					  {
						gridData.push(angular.toJson(stockinData));														 
				      });
					  obj.formData = angular.toJson(AddedStock);
					  obj.gridData = gridData;
					  
					
				
			    $.ajax({
				    url:"/SugarERP/SaveStockSupply.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							//alert("rootscope"+$rootScope.arraydefault);
							if($rootScope.arraydefault==undefined)
							{
								$('.btn-hide').attr('disabled',false);
	
								swal("Sucess!", 'Stock In Added Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
								
								$scope.AddedStock = angular.copy($scope.master);
								$index=1;
								$scope.data = [{'id':$index}];
								$scope.onloadFunction();
								form.$setPristine(true);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true);
								$scope.$apply();
							}
							else
							{
								//alert($rootScope.arraydefault);
								$('.btn-hide').attr('disabled',false);
	
								swal("Sucess!", 'Stock In Added Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
								
								$scope.AddedStock = angular.copy($scope.master);
								$index=1;
								$scope.data = [{'id':$index}];
								$scope.onloadFunction();
								form.$setPristine(true);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true);
								$scope.$apply();
								$rootScope.arraydefault=undefined;
								$state.go('store.transaction.StockSupplyDetails');
								
							}
						

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						   
						   
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };
			$scope.duplicateProductCode=function(batch,index)
			{
			
			//var productCode=productCode;
			var batch=batch;
			var count=0;
			
			 angular.forEach($scope.data, function(value,index) 
				{
			
					//var sum = (value.productCode);
					var batchsum = (value.batch);
					//alert(batch+"batch");
					//alert(batchsum+"batchsum");
			 			if(batch==batchsum)
						{
			   				count++;
			   			}
			
				})
			//alert("count"+count);
				if(count>1)
				 {
					swal("Duplicate Entry", "Batch already Exist for given batch :)", "error");
					$scope.data[index].batch="";
			 	}
				else
				{
					
			 	}
			
			
			};
			$scope.getMRP = function(mrp,quantity,id)
			{
				//alert($scope.popupId);
				var popupId = $scope.popupId;
				gridMrp = $scope.productBatchData[popupId].mrp;
				var mrpValue  = gridMrp*quantity;
				$scope.data[id].mrp =mrpValue.toFixed(2);
				
				
			};

		
			$scope.getCP = function(cp,quantity,id)
			{
				var cpValue  = cp*quantity;
				$scope.data[id].cp =cpValue;
				
			};

			 
			
		
		
		  
		    $scope.reset = function(form)
		   {			  
				$scope.AddedStock = angular.copy($scope.master);
				$index=1;
				$scope.data = [{'id':$index}]
			    form.$setPristine(true);
				$scope.onloadFunction();
				$scope.getMaxStockInid();
				$('.btn-hide').attr('disabled',false);
		   };
		
		
		 
	 })
	  
	  .controller('StockRequestController',function($scope,$http,$rootScope,$state)
	 {
		 var obj = new Object();
		$scope.stockRequestData=[];
		$scope.master = {};
			$index=0;
			$scope.addFormField=function()
			{
				$index++;
				$scope.stockRequestData.push({'id':$index,'consumption':'0'});
			};
			
		
		$scope.getMaxStockReqid = function(deptID)
			{
			var deptID = $scope.AddedStockRequest.deptID;
				var username =  sessionStorage.getItem('username');
				
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxStockReqid.html",
					data:deptID,
			   }).success(function(data, status) 
				{
					
				$scope.AddedStockRequest.stockReqNo =data[0].StockReqid;
				
				});	
			};
			
			
			
			$scope.duplicateProductCode=function(productCode,index,name)
			{
			
			//var productCode=productCode;
			var productCode=productCode;
			var count=0;
			 angular.forEach($scope.stockRequestData, function(value,index) 
				{
					//var sum = (value.productCode);
					var productCodesum = (value.productCode);
					//alert(batch+"batch");
					//alert(batchsum+"batchsum");
			 			if(productCode==productCodesum)
						{
			   				count++;
			   			}
				})
			//alert("count"+count);
				if(count>1)
				 {
					swal("Duplicate Entry", "ProductCode already Exists  :)", "error");
					$scope.removeRow(name);
					$scope.stockRequestData[index].productCode="";
					$scope.stockRequestData[index].productName="";
					$scope.stockRequestData[index].uom="";
					$scope.stockRequestData[index].uomName="";
					$scope.stockRequestData[index].deptStock="";
					$scope.stockRequestData[index].csStock="";
					
					
			 	}
				else
				{
					
			 	}
			return false;
			
			};
			
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		
		
		
		$scope.onloadFunction=function()
		{
			var stockReqNo=$rootScope.updateStockReqNo;
			
			if(stockReqNo==undefined || stockReqNo==null || stockReqNo=='')
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getUserDeptStatus.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.AddedStockRequest = {'deptID':data[0].Department,'authorisedUser':data[0].Username,'modifyFlag':'No','status':data[0].Status};
					$scope.getMaxStockReqid(data[0].Department);
				});
					
				});	 
			}
			else
			{
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIndentDetails.html",
				data: stockReqNo,
				}).success(function(data, status) 
				{
					$scope.AddedStockRequest = data.SummaryBean;
					$scope.stockRequestData = data.DetailsBean;
					
					for(var i=0;i<$scope.stockRequestData.length;i++)
					{
						
						var pcode = data.DetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
						
				}); 
				
			}

			 
			
		};
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
					
				}); 
				
			};
			
			$scope.loadProductCode = function(productCode,id)
			{
				
				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						
						$scope.stockRequestData[id].productName = data[0].ProductName;
						$scope.stockRequestData[id].uom = data[0].Uom;
						$scope.stockRequestData[id].uomName = data[0].Uomname;

						var deptId = $scope.AddedStockRequest.deptID;
						//alert('deptId-------'+deptId);
						var stockObj = new Object();
						stockObj.productCode = productCode;
						stockObj.deptId = parseInt(deptId);
						stockObj.finYear = '16-17';
				
						//Getting SubStore Stock
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getStockForDepartment.html",
						data: stockObj,
						}).success(function(data, status) 
						{
							$scope.stockRequestData[id].deptStock = data;
						}); 


						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getCentranStoreId.html",
						data: stockObj,
						}).success(function(data, status) 
						{
							var centralStoreId = data;
							var mainStoreStockObj = new Object();
							mainStoreStockObj.productCode = productCode;
							mainStoreStockObj.deptId = parseInt(centralStoreId);
							mainStoreStockObj.finYear = '16-17';
							
							//Getting MainStore Stock
							var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getStockForDepartment.html",
							data: mainStoreStockObj,
							}).success(function(data, status) 
							{
								$scope.stockRequestData[id].csStock = data;
							}); 
						});

							
					
							
					
											
						 
							
							
							
						});
					}
				}); 
				

			};
			
			
		 $scope.reset = function(form)
		   {			  
				$scope.AddedStockRequest = angular.copy($scope.master);
				$index=1;
				$scope.AddedStockRequest={};
				$scope.stockRequestData = [{'id':$index,'consumption':'0'}]				
			    form.$setPristine(true);
				$scope.onloadFunction();
				$scope.$apply();
		   };
		   
		   $scope.stockIndentReqSubmit = function(AddedStockRequest,form)
		  {
			  
			  if($scope.StockRequestForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
				 gridData = [];
				 $scope.stockRequestData.forEach(function (stockData) 
					  {
						gridData.push(angular.toJson(stockData));
						
				      });
					  obj.formData = angular.toJson(AddedStockRequest);
					  obj.gridData = gridData;
					
					
				
			    $.ajax({
				    url:"/SugarERP/SaveStockIndent.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						//alert(response);
						if(response==true)
						{
						
							if($rootScope.updateStockReqNo==undefined)
							{
								$('.btn-hide').attr('disabled',false);
								var reqNoObj = new Object();
								reqNoObj.RequestNo = $scope.AddedStockRequest.stockReqNo;
	                             // alert(JSON.stringify(reqNoObj));
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getRequestDetailsForPrint.html",
									data: JSON.stringify(reqNoObj),
								   }).success(function(data, status) 
									{
										
										$scope.stockRequestprint = data;
										$scope.stockReqNo = data[0].RequestNo;
										$scope.reqDate = data[0].ReqDate;
										
										
										swal({
													 title: "Stock Request Added Successfully!",
													 text: " Do You want to take a print out?",
													 type: "success",
													 showCancelButton: true,
													 confirmButtonColor: "#2298f2",
													 confirmButtonText: "Print",
													 closeOnConfirm: true}, 
													 function(){
														
														var prtContent = document.getElementById("stockRequestPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
														 
													
													
													});
										
										
									});  
	
								
								//swal("Sucess!", 'Stock Request Added Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
//								
								 $scope.reset(form);
								
							}
							else
							{
								$('.btn-hide').attr('disabled',false);

							swal("Sucess!", 'Stock Request Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							//alert($rootScope.arraydefault);
							// $scope.reset(form);
							 $rootScope.updateStockReqNo='';
							//$scope.onloadFunction();
							$state.go('store.transaction.StockRequests');
							}
						

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						   
						   
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };
		
	 })
	 
	  .controller('StockRequestsController',function($scope,$http,$rootScope)
	 {
		 $("#fromDate").focus();
		$scope.stockRequestData=[];
		$scope.master = {};
			$index=0;
			
		
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
					data.push({"deptCode":0,"department":"ALL"});

				}); 
				
			};
			
			$scope.userLoginDept = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getUserDeptStatus.html",
				data: {}
				}).success(function(data, status) 
				{
					
					
					$scope.AddedStockRequest = {'userDept':data[0].Department};
					
					
				});
					
				});	
			};	
			
			$scope.getStockRequestDetails = function(fromDate,toDate,department)
			{
				
				var fromDate = fromDate+"-";
				var spltFromDate = fromDate.split("-");
				var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
				
				var toDate = toDate+"-";
				var spltToDate = toDate.split("-");
				var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
				
				
				var stockRequestDetailsObj = new Object();
				stockRequestDetailsObj.dateFrom = dateFrom;
				stockRequestDetailsObj.dateTo = dateTo;
				stockRequestDetailsObj.department = department;
				
				//alert(JSON.stringify(stockRequestDetailsObj));
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStockIndentDetails.html",
				data: JSON.stringify(stockRequestDetailsObj),
				}).success(function(data, status) 
				{
					$("#hidetable").fadeIn(2000);
					//alert(JSON.stringify(data));
					$scope.stockRequestData=data;
				}); 
				
			};
			
		 
		 $scope.stockRequestSubmit=function(AddedStockRequest,form)
			{
				
				  if($scope.StockRequestForm.$valid)
				  {
				   
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			
			$scope.printStockReqDetails = function(stockReqNo)
			{
				var reqNoObj = new Object();
								reqNoObj.RequestNo = stockReqNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getRequestDetailsForPrint.html",
									data: JSON.stringify(reqNoObj),
								   }).success(function(data, status) 
									{
										
										$scope.stockRequestprint = data;
										$scope.stockReqNo = data[0].RequestNo;
										$scope.reqDate = data[0].ReqDate;
										
											 setTimeout(function(){ 
																 
																 
													var prtContent = document.getElementById("stockRequestPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 			 
																 
																 }, 500);
									
														
														 
													
														
														
										
										});
			};
			
			$scope.UpdateStockRequest = function(stockReqNo,index)
			{
			
				var index = this.$index;
				$rootScope.updateStockReqNo= stockReqNo;
				
			};

			$scope.updateToStockIssue = function(stockReqNo,index)
			{
				var index = this.$index;
				$rootScope.stockReqNotoIssue= stockReqNo;
				
			};
		 
		
	 })
	  
	   .controller('StockIssuesController',function($scope,$http,$rootScope)
	 {
		 $("#fromDate").focus();
		$scope.stockRequestData=[];
		$scope.master = {};
			$index=0;
			
			
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		
		
			
		$scope.getAllStockIssueDetails = function(stockIssueNo,index)
		{
			var index = this.$index;
				$rootScope.stockIssueNoArray= stockIssueNo;
		
		};
		$scope.getUpdatedGRNfromStockIssueDetails = function(grnStockIssueNo,index)
		{
			var index = this.$index;
				$rootScope.grnStockIssueNo= grnStockIssueNo;
		};
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
			
			$scope.getStockRequestDetails = function(fromDate,toDate,department)
			{
				
				var fromDate = fromDate+"-";
				var spltFromDate = fromDate.split("-");
				var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
				
				var toDate = toDate+"-";
				var spltToDate = toDate.split("-");
				var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
				
				
				var stockRequestDetailsObj = new Object();
				stockRequestDetailsObj.dateFrom = dateFrom;
				stockRequestDetailsObj.dateTo = dateTo;
				stockRequestDetailsObj.department = department;
				
				
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStockIssueDetails.html",
				data: JSON.stringify(stockRequestDetailsObj),
				}).success(function(data, status) 
				{
					$("#hidetable").fadeIn(2000);
				
					$scope.stockIssuesData=data;
				}); 
				
			};
			
		 
		 $scope.stockIssuesSubmit=function(AddedStockRequest,form)
			{
				
				  if($scope.StockIssuesForm.$valid)
				  {
				   
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			$scope.UpdateStockRequest = function(stockReqNo,index)
			{
			
				var index = this.$index;
				$rootScope.updateStockReqNo= stockReqNo;
				
			};

			$scope.updateToStockIssue = function(stockReqNo,index)
			{
				var index = this.$index;
				$rootScope.stockReqNotoIssue= stockReqNo;
				
				
			};
			
			
			$scope.printSTADetails = function(stockIssueNo)
			{
				var issueNoObj = new Object();
								issueNoObj.IssueNo = stockIssueNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getIssueDetailsForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										
										$scope.stockTransferPrint = data;
									    $scope.issueDate = data[0].issueDate;
									    $scope.indentno = data[0].indentno;
									    $scope.ReqDate = data[0].ReqDate;
//										
											 setTimeout(function(){ 
																 
																 
													var prtContent = document.getElementById("stockTransferPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 			 
																 
																 }, 500);
									});
			};
			
			
			$scope.printDeliveryNote = function(stockIssueNo)
			{
				var issueNoObj = new Object();
								issueNoObj.IssueNo = stockIssueNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getDeliveryNoteForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										var quantityTotal = 0;
										
										$scope.Delivery = data.formData[0];
										$scope.deliveryNoteData = data.gridData;
										for(var g=0;g<data.gridData.length;g++)
										{
											quantityTotal = quantityTotal+parseInt(data.gridData[g].quantity);
										}
										
										$scope.quantityTotal = quantityTotal.toFixed(3);
										//$scope.stockTransferPrint = data;
//									    $scope.issueDate = data[0].issueDate;
//									    $scope.indentno = data[0].indentno;
//									    $scope.ReqDate = data[0].ReqDate;
//										
											 setTimeout(function(){ 
																 
																 
													var prtContent = document.getElementById("deliveryNotePrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 			 
																 
																 }, 500);
									});
			};
			
			
		 
		
	 })
	   
	   .controller('grnsController',function($scope,$http,$rootScope)
	 {
		 $("#fromDate").focus();
		$scope.stockRequestData=[];
		$scope.master = {};
			$index=0;
			
			$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
		 
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
			
			$scope.getStockRequestDetails = function(fromDate,toDate,department)
			{
				
				var fromDate = fromDate+"-";
				var spltFromDate = fromDate.split("-");
				var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
				
				var toDate = toDate+"-";
				var spltToDate = toDate.split("-");
				var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
				
				
				var grnsObj = new Object();
				grnsObj.dateFrom = dateFrom;
				grnsObj.dateTo = dateTo;
				grnsObj.department = department;
				
				
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getGrnDetails.html",
				data: JSON.stringify(grnsObj),
				}).success(function(data, status) 
				{
					$("#hidetable").fadeIn(2000);
					
					$scope.data=data;
				}); 
				
			};
			
			
			$scope.grnsSubmit=function(AddedGRN,form)
			{
				
				  if($scope.GRNForm.$valid)
				  {
				   
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			$scope.getUpdatedGRNfromGRNS = function(rootgrnNo)
			{
				
				var index = this.$index;
				$rootScope.rootgrnNo= rootgrnNo;
			
			};
			
			$scope.printGRNDetails = function(grnno)
			{
				var grnObj = new Object();
								grnObj.GRNNo = grnno;
	                          
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getGRNDetailsForPrint.html",
									data: JSON.stringify(grnObj),
								   }).success(function(data, status) 
									{
										
										$scope.grnPrint = data;
										$scope.grnNo = data[0].grnNo;
										$scope.grndate = data[0].grndate;
										
											 setTimeout(function(){ 
																 
																 
													var prtContent = document.getElementById("grnPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 			 
																 
																 }, 500);
									
														
														 
													
														
														
										
										});
			};
			
		 
		 
		 
		
	 })
	 
	  	
	 
	   .controller('StoreStockIssueController',function($scope,$http,$rootScope,$state)
	 {
		 $("#issueNo").focus();
		$scope.stockIssueData=[];
		$scope.master = {};
			$index=0;
			var obj = new Object();
			
			$scope.loadProductCode = function(productCode,id)
			{
				

  				var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						//	alert(JSON.stringify(data));
							
							$scope.stockIssueData[id].productName = data[0].ProductName;
							$scope.stockIssueData[id].uom = data[0].Uom;
							$scope.stockIssueData[id].uomName = data[0].Uomname;
						});
					}
				}); 
						
			};
			
			
			
			  var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
			  var yy = d.getFullYear();
			 var todayDate  = dd+"-"+mm+"-"+yy;
			
			$scope.onloadFunction=function(){
			var stockReqNotoIssue=$rootScope.stockReqNotoIssue;
			
			var stockIssueNoArray = $rootScope.stockIssueNoArray
			if($rootScope.stockReqNotoIssue!=undefined)
			{
				$scope.readonlyStatus=true;
			}
			else
			{
				$scope.readonlyStatus=false;
			}
			if($rootScope.stockIssueNoArray!=undefined)
			{
				$scope.readonlyStatus=true;
			}
			else
			{
				$scope.readonlyStatus=false;
			}
			
			if(stockIssueNoArray!=undefined)
			{
				var stockIssueNumber = stockIssueNoArray.toString();
				//alert("datasending"+stockIssueNumber);
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIssueDetails.html",
				data:stockIssueNumber,
				
				}).success(function(data, status) 
				{
					$("#stockIssueTable").show();
					
					$scope.AddedStockIssue = data.IssueSummaryBean;
					$scope.stockIssueData = data.IssueDetailsBean;
					if(data.IssueDetailsBean.length>1)
					{
						$scope.onloadgridLength="multiple";
					}
					else
					{
						$scope.onloadgridLength="single";
					}
					$("#hideAddBatch").hide();
					for(var i=0;i<$scope.stockIssueData.length;i++)
					{
						
						var pcode = data.IssueDetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}

					//$scope.$apply();
										//alert(JSON.stringify($scope.stockIssueData));

					
				}); 
				
				
			}
			else
			{
				
			}
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getMaxStockIssueid.html",
				data:'',
				
				}).success(function(data, status) 
				{
					
					$scope.AddedStockIssue={'stockIssueNo':data[0].StockIssueid,'issueDate':todayDate,'stockReqNo':stockReqNotoIssue,'modifFlag':'No'};
					
				}); 
			
			
				
				
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIndentDetails.html",
				data: stockReqNotoIssue,
			   }).success(function(data, status) 
				{
					
					
					$("#stockIssueTable").show();
					
					$scope.AddedStockIssue.deptId = data.SummaryBean.deptID;
					$scope.AddedStockIssue.modifFlag = "No";
						if(data.DetailsBean.length>1)
						{
							$scope.onloadgridLength = "multiple";
						}
						else
						{
							$scope.onloadgridLength = "single";
						}
					$scope.stockIssueData = data.DetailsBean;
					$("#hideAddBatch").hide();
				for(var i=0;i<$scope.stockIssueData.length;i++)
					{
						
						var pcode = data.DetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
					
						
					
					
					
				});
			
			 
			
		};
		$scope.getStockIssueSummaryDetails = function(stockReqNotoIssue)
		{
			if($scope.AddedStockIssue.modifFlag=="No")
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIndentDetails.html",
				data: stockReqNotoIssue,
			   }).success(function(data, status) 
				{
					
					
					$("#stockIssueTable").show();
				
					$scope.AddedStockIssue.deptId = data.SummaryBean.deptID;

					$scope.stockIssueData = data.DetailsBean;
					if(data.DetailsBean.length>1)
					{
						$scope.onloadgridLength ="multiple";
					}
					else
					{
						$scope.onloadgridLength ="single";
					}
					
				for(var i=0;i<$scope.stockIssueData.length;i++)
					{
						
						var pcode = data.DetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
					
						
					
					
					
				});
			}
			
			
			
			
		};
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockIssueData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockIssueData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockIssueData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.getbatchPopup = function(productcode,productName,uom,uomname,pendingQty,id,issuedQty,deptId)
		{
		
			if(productcode!=undefined)
			{
			$('#gridpopup_box').fadeIn("slow");
			
			$scope.productCode = productcode;
			$scope.productName = productName;
			$scope.uom = uom;
			$scope.uomName = uomname;
			$scope.pendingQty = pendingQty;
			$scope.issuedQty = issuedQty;
			$scope.popupId = id;

			var stockObj = new Object();
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getCentranStoreId.html",
			data: stockObj,
			}).success(function(data, status) 
			{
					//alert('data---------'+data);
					deptId = data;


					//centralStoreId
					//AddedStockIssue.
					$scope.AddedStockIssue.centralStoreId = parseInt(data);

					var stockIssueObj = new Object();
					stockIssueObj.productCode = productcode;
					stockIssueObj.deptId = parseInt(deptId);
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getProductBatches.html",
					data: JSON.stringify(stockIssueObj),
				}).success(function(data, status) 
			   {
					//	alert(JSON.stringify(data));
					$('#gridpopup_box').fadeIn("slow");
					$("#addNewbatch").hide();
					$("#batchPopupScreen").show();
					$("#addBatchPopup").hide();
					$scope.productBatchData = data;
			   });
			}); 

			}
			else if(productcode==undefined)
			{
				swal("Product Code not given", "Product Code is required :)", "error");
	  			$("#prodcode"+id).focus();
				return false;
			}
			
			
		};
			
			
			$scope.validateIssueQtyByBatchQty = function(issuedQty,batchQty,id)
			{
				
				if(parseInt(issuedQty)>parseInt(batchQty))
				{
					
					sweetAlert("Oops...", "Issued Quantity cannot be greater than Batch Quantity!", "error");
					$scope.stockIssueData[id].issuedQty="";
				}
			};
			var totalBalance =0;
			
			$scope.getBalancedIssuedQtyTtoal = function(pendingQty,issuedQty,id,productCode)
			{
				//alert("PendingQty"+pendingQty);
				//alert("IssuedQty"+issuedQty);
				
				var tot = 0;
				
				for(var i=0;i<$scope.stockIssueData.length;i++)
				{
					if($scope.stockIssueData[i].issuedQty!=undefined && $scope.stockIssueData[i].productCode==productCode)
					{
						tot = tot+parseInt($scope.stockIssueData[i].issuedQty);
					}
					
				}
			//totalBalance = parseInt(totalBalance)+parseInt(issuedQty); 
			//alert("totalBalance"+totalBalance);
			
			
			//alert("pendingQty"+pendingQty);
				if(tot>pendingQty)
				{
					
					
					sweetAlert("Oops...", "Issued Quantity cannot be greater than Pending Quantity!", "error");
					$scope.stockIssueData[id].issuedQty="";
				}
				//if(total==pendingQty)
//				{
//					
//				}
//				else if(total>pendingQty)
//				{
//					
//				}
				
			};
			
		var arraytt = new Array(); var batchIssueQty = 0;
	

		$scope.batchDatapush = function(checkbox,batch,expiryDate,mrp,id,batchSeqNo,cp,batchId,mfgDate,qty,stockInDate)
		{
		
		
		
		if(checkbox==true)
			{
			
				if($scope.stockIssueData.length=='1')
				{
					var rowId = $scope.popupId;
					
			
					$scope.productCode;
					$scope.productName;
					$scope.uom;
					$scope.uomName;
					$scope.pendingQty;
					$scope.issuedQty;
					 var objPush = new Object();
					objPush.productCode =$scope.productCode; 
					objPush.productName =$scope.productName;
					objPush.uom =$scope.uom;
					objPush.uomName = $scope.uomName;
					objPush.pendingQty =$scope.pendingQty;
					objPush.issuedQty =$scope.issuedQty; 
					objPush.batchId = batchId;
					objPush.batch = batch;
					objPush.expDt = expiryDate;
					objPush.mrp = mrp;
					objPush.batchQty = qty;
					objPush.batchSeqNo = batchSeqNo;
					
					batchIssueQty = parseInt(batchIssueQty)+parseInt(qty); 
					arraytt.push(objPush);
				$scope.stockIssueData = [];
					$scope.stockIssueData = arraytt;
					
					 
				}
				else
				{
					
					/*var rowId = $scope.popupId;
				$scope.stockIssueData[rowId].batchId = batchId;
				$scope.stockIssueData[rowId].batch = batch;
				 $scope.stockIssueData[rowId].mrp = mrp;
				$scope.stockIssueData[rowId].issuedQty = qty;
				$scope.stockIssueData[rowId].batchSeqNo = batchSeqNo;
				 $('#gridpopup_box').fadeOut("slow");
				$scope.loadStockIssue();
				//$('#gridpopup_box').fadeOut("slow");
				//$scope.stockIssueData[rowId].batchQty = qty;*/
				 
				// alert('batchId------'+batchId);
				
					
					
				}
			}
			else if(checkbox==false)
			{
							
					batchIssueQty = parseInt(batchIssueQty)-parseInt(qty); 		
			}
						
		};
		
		$(document).keyup(function(e) {
     if (e.keyCode == 27) {
		
		 $('#gridpopup_box').fadeOut("slow");
//$scope.stockIssueData=arraytt;
		 
		
    }
});


		

		
	
			$scope.CloseBatchPopup = function()
			{
			
			if($scope.onloadgridLength=="single")
			{
				
				 $('#gridpopup_box').fadeOut("slow");
			}
			else
			{
			
			if($scope.stockIssueData.length>1)
			{
					var inputElems = document.getElementsByTagName("input");
					
		        count = 0;
		
		for (var i=0; i<inputElems.length; i++) 
		{
		if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) 
		{
			
	
		
					if(count==0)
					{
					var rowId = $scope.popupId;
						//$('#gridpopup_box').fadeOut("slow");
				$scope.stockIssueData[rowId].batchId =  $scope.productBatchData[count].batchId;
				$scope.stockIssueData[rowId].batch =  $scope.productBatchData[count].batch;
				 $scope.stockIssueData[rowId].mrp =  $scope.productBatchData[count].mrp;
				$scope.stockIssueData[rowId].batchQty = $scope.productBatchData[count].stock;
				$scope.stockIssueData[rowId].batchSeqNo = $scope.productBatchData[count].batchSeqNo;


					}
					else
					{
					$scope.productCode;
					$scope.productName;
					$scope.uom;
					$scope.uomName;
					$scope.pendingQty;
					$scope.issuedQty;
					 var objPush = new Object();
					objPush.productCode =$scope.productCode; 
					objPush.productName =$scope.productName;
					objPush.uom =$scope.uom;
					objPush.uomName = $scope.uomName;
					objPush.pendingQty =$scope.pendingQty;
					objPush.issuedQty =$scope.issuedQty; 
					objPush.batchId = $scope.productBatchData[count].batchId;
					objPush.batch = $scope.productBatchData[count].batch;
					objPush.expDt =  $scope.productBatchData[count].expiryDate;
					objPush.mrp =  $scope.productBatchData[count].mrp;
					objPush.batchQty = $scope.productBatchData[count].stock;
					objPush.batchSeqNo = $scope.productBatchData[count].batchSeqNo;
					
					$scope.stockIssueData.push(objPush);
				
					}
					count++;
					
					 $('#gridpopup_box').fadeOut("slow");
		
				}
			}	
			}
			
			if($scope.stockIssueData.length==1)
			{
				 $('#gridpopup_box').fadeOut("slow");
			}
			}
			
			
			
			
			
			
			
		};
		
		$scope.loadStockIssueRequestDetails=function(AddedStockIssue,form)
			{
				
				  if($scope.StoreStockIssueForm.$valid)
				  {
				   
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			
			$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
			
			$scope.updatevalidIssueQtyBypendingQty = function(pendingQty,issuedQty,id)
			{
				if(issuedQty>pendingQty)
				{
					sweetAlert("Oops...", "Issued Quantity cannot be greater than pending Quantity!", "error");
					$scope.stockIssueData[id].issuedQty = "";
				}
				else
				{
					
				}
			};
			
			$scope.storeStockIssueSubmit = function(AddedStockIssue,form)
		  {
			  
			  if($scope.StoreStockIssueForm.$valid) 
			  {	
			    $('.btn-hide').attr('disabled',true);
				 gridData = [];
				delete AddedStockIssue['modifyFlag'];
				 $scope.stockIssueData.forEach(function (stockIssue) 
					  {
						  delete stockIssue['purpose'];
						  delete stockIssue['csStock'];
						  delete stockIssue['deptStock'];
						  delete stockIssue['stockReqNo'];
						  delete stockIssue['mfgDate'];
						  delete stockIssue['expDt'];
						  delete stockIssue['consumption'];

						gridData.push(angular.toJson(stockIssue));														 
				      });
					  obj.formData = angular.toJson(AddedStockIssue);
					  obj.gridData = gridData;
					  
					
				
			    $.ajax({
				    url:"/SugarERP/SaveStockIssue.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						var stkIssueNo = $scope.AddedStockIssue.stockIssueNo;
						if(response==true)
						{
							
							if($rootScope.stockReqNotoIssue==undefined && $rootScope.stockIssueNoArray==undefined)
							{
								
								$('.btn-hide').attr('disabled',false);
	
								

								swal({
									  title: "Are you sure?",
									  text: "Do you want to take print!",
									  type: "success",
									  showCancelButton: true,
									  confirmButtonClass: "btn-success",
									  confirmButtonText: "Delivery Note!",
									  cancelButtonText: "Stock Issue!",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm) {
									  if (isConfirm) {
										  
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                          
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getDeliveryNoteForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										var quantityTotal = 0;
										
										$scope.Delivery = data.formData[0];
										$scope.deliveryNoteData = data.gridData;
										for(var g=0;g<data.gridData.length;g++)
										{
											quantityTotal = quantityTotal+parseInt(data.gridData[g].quantity);
										}
										
										$scope.quantityTotal = quantityTotal.toFixed(3);
										
										
											setTimeout(function(){
																 
																 
													var prtContent = document.getElementById("deliveryNotePrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
													
													 }, 1000);
																 
																
									});		 
								  } else {
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getIssueDetailsForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										
										 
															 
										$scope.stockTransferPrint = data;
									    $scope.issueDate = data[0].issueDate;
									    $scope.indentno = data[0].indentno;
									    $scope.ReqDate = data[0].ReqDate;
//										
									setTimeout(function(){ 
														
										var prtContent = document.getElementById("stockTransferPrint");
										var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
										WinPrint.document.write(prtContent.innerHTML);
										WinPrint.document.close();
										WinPrint.focus();
										WinPrint.print();
										WinPrint.close(); 		 
															 
															 }, 1000);
										
										
										
													 
																 
															
									});
										
									  }
									});
								
								$scope.AddedStockIssue = angular.copy($scope.master);
								$index=1;
								$scope.stockIssueData = [{'id':$index}];
								$scope.onloadFunction();
								form.$setPristine(true);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true);
								$scope.$apply();
							}
							
							 if($rootScope.stockReqNotoIssue!=undefined && $rootScope.stockIssueNoArray==undefined )
							{
								
								
								
								swal({
									  title: "Are you sure?",
									  text: "Do you want to take print!",
									  type: "success",
									  showCancelButton: true,
									  confirmButtonClass: "btn-success",
									  confirmButtonText: "Delivery Note!",
									  cancelButtonText: "Stock Issue!",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm) {
									  if (isConfirm) {
										  
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                          
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getDeliveryNoteForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
									  
														 
												var quantityTotal = 0;
										
										$scope.Delivery = data.formData[0];
										$scope.deliveryNoteData = data.gridData;
										for(var g=0;g<data.gridData.length;g++)
										{
											quantityTotal = quantityTotal+parseInt(data.gridData[g].quantity);
										}
										
										$scope.quantityTotal = quantityTotal.toFixed(3);
										
										
											setTimeout(function(){
																 
																 
													var prtContent = document.getElementById("deliveryNotePrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
													
													 }, 1000);
																 
																
									});		 
								  } else {
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getIssueDetailsForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										
										 
															 
										$scope.stockTransferPrint = data;
									    $scope.issueDate = data[0].issueDate;
									    $scope.indentno = data[0].indentno;
									    $scope.ReqDate = data[0].ReqDate;
//										
									setTimeout(function(){ 
														
										var prtContent = document.getElementById("stockTransferPrint");
										var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
										WinPrint.document.write(prtContent.innerHTML);
										WinPrint.document.close();
										WinPrint.focus();
										WinPrint.print();
										WinPrint.close(); 		 
															 
															 }, 1000);
										
										
										
													 
																 
															
									});
										
									  }
									});
								
	
								
								$rootScope.stockReqNotoIssue=undefined;
								setTimeout(function(){
								$state.go('store.transaction.StockRequests'); }, 3000);
								
							}
							if($rootScope.stockReqNotoIssue==undefined && $rootScope.stockIssueNoArray!=undefined)
							{
								

swal({
									  title: "Are you sure?",
									  text: "Do you want to take print!",
									  type: "success",
									  showCancelButton: true,
									  confirmButtonClass: "btn-success",
									  confirmButtonText: "Delivery Note!",
									  cancelButtonText: "Stock Issue!",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm) {
									  if (isConfirm) {
										  
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                          
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getDeliveryNoteForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
									  
														 
												var quantityTotal = 0;
										
										$scope.Delivery = data.formData[0];
										$scope.deliveryNoteData = data.gridData;
										for(var g=0;g<data.gridData.length;g++)
										{
											quantityTotal = quantityTotal+parseInt(data.gridData[g].quantity);
										}
										
										$scope.quantityTotal = quantityTotal.toFixed(3);
										
										
											setTimeout(function(){
																 
																 
													var prtContent = document.getElementById("deliveryNotePrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
													
													 }, 1000);
																 
																
									});		 
								  } else {
										  
										  var issueNoObj = new Object();
								issueNoObj.IssueNo = stkIssueNo;
	                           
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getIssueDetailsForPrint.html",
									data: JSON.stringify(issueNoObj),
								   }).success(function(data, status) 
									{
										
										 
															 
										$scope.stockTransferPrint = data;
									    $scope.issueDate = data[0].issueDate;
									    $scope.indentno = data[0].indentno;
									    $scope.ReqDate = data[0].ReqDate;
//										
									setTimeout(function(){ 
														
										var prtContent = document.getElementById("stockTransferPrint");
										var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
										WinPrint.document.write(prtContent.innerHTML);
										WinPrint.document.close();
										WinPrint.focus();
										WinPrint.print();
										WinPrint.close(); 		 
															 
															 }, 1000);
										
										
										
													 
																 
															
									});
										
									  }
									});
								
	
								
								$rootScope.stockReqNotoIssue=undefined;
								setTimeout(function(){
								$state.go('store.transaction.StockIssues'); }, 3000);



								
							}
						
							
						

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						   
						   
						 }
				    }
		         });
			  }
			  else
				{
						var field = null, firstError = null;
						for (field in form) 
						{
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						}
				}			  
		  };

		
		
	 })

	.controller('deliveryChallanController', function($scope,$http,$rootScope)
	{
		$scope.getdiscType = function()
		{
			$scope.deliveryChallan={'discType':'0','discountPercent':'0','totalCost':'0','discountAmount':'0','netAmount':'0'};
		}
		
		$("#dcnumber").focus();
		 var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		 var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 $scope.deliveryChallan = {'dcdate':todayDate};
		
		$scope.getAllDepartments=function()
		{
			//alert('getAllDepartments');
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.DepartmentsData=data;
			}); 
		};
		
		$scope.getMaxDcNo=function(department)
		{
			var deptId = parseInt(department);
			
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxDcNo.html",
			data: deptId,
			}).success(function(data, status) 
			{
				$scope.deliveryChallan.dcnumber = data[0].dcNum;
				$scope.deliveryChallan.dcSeqNo = data[0].dcSeqNo;
				$scope.deliveryChallan.saleInvoiceNo = data[0].saleInvoiceNo;
			}); 
		};
			
			
		$scope.onloadFunction=function()
		{
			var rSdcNum = $rootScope.sdcno;//= sdcno;
			
			$scope.deliveryChallan.dcnumber=$rootScope.sdcno;

			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDcs.html",
			data: rSdcNum,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$("#hidetable").fadeIn(2000);
				
				$scope.deliveryChallan=data.DcSummaryBean;
				$scope.data = data.DcBeanArray;
			}); 
			
		};
		
		$scope.loadProductCode = function(productCode,id)
		{
			var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						//	alert(JSON.stringify(data));
							
							$scope.data[id].productName = data[0].ProductName;
							$scope.data[id].uom = data[0].Uom;
							$scope.data[id].uomName = data[0].Uomname;
						});
					}
				});
		};
		
		$scope.getbatchPopup = function(productcode,id,deptId)
		{
			//alert("department"+deptId);
			//alert('hello');
			if(productcode==undefined)
			{
				swal("Product Code not given", "Product Code is required :)", "error");
				$("#prodcode"+id).focus();
				return false;
			}
		
			else if(productcode!=undefined)
			{
				$scope.productCode = productcode;
				$scope.productId = id;
				$scope.productBatchData = [];
			
				var stockObj = new Object();
				stockObj.productCode = productcode;
				stockObj.deptId = parseInt(deptId);
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductBatches.html",
				data: JSON.stringify(stockObj),
			}).success(function(data, status) 
		   {	
				$('#gridpopup_box').fadeIn("slow");
				$scope.productBatchData = data;
		   });
			}
		};
		//Modified by DMurty on 27-03-2017
		$scope.batchDatapush = function(checkbox,batch,expiryDate,mrp,id,batchSeqNo,costPrice,batchId,mfrDate,stock,stockInDate)
		{
			if(checkbox==true)
			{
				var rowId = $scope.productId;
				$scope.popupId = id;
				 $('#gridpopup_box').fadeOut("slow");
				 
				 $scope.data[rowId].batch = batch;
				 $scope.data[rowId].expDt = expiryDate;
				 $scope.data[rowId].mrp = mrp;
				 $scope.data[rowId].batchFlag = 'Old';
				 $scope.data[rowId].batchSeqNo = batchSeqNo;
				 $scope.data[rowId].batchId = batchId;
				 $scope.data[rowId].stockInDate = stockInDate;
				 $scope.data[rowId].cp = costPrice;


			}
			else if(checkbox==false)
			{
							
							
			}
						
		};
		
		$scope.updateTotal = function(quantity,mrp,discountAmount,id)
		{
			var total = parseFloat(quantity)*parseFloat(mrp);
			$scope.data[id].totalCost = total;
			
			var totalAmount = 0;
			if(discountAmount == '' || discountAmount == null)
			{
				discountAmount = 0;
				$scope.data[id].discountAmount = discountAmount;
			}
				
			var netAmount = parseFloat(total)-parseFloat(discountAmount);
			if(netAmount == '' || netAmount == null)
			{	
				netAmount = 0;
				$scope.data[id].netAmount = netAmount;
			}
			$scope.data[id].netAmount = netAmount;

			var net = 0;
			var totalDisc = 0;
			//alert($scope.data.length);
		    for(var s=0;s<$scope.data.length;s++)
		    {
				if($scope.data[s].totalCost>0)
					totalAmount += parseFloat($scope.data[s].totalCost);//+parseFloat(total);
				
				//if($scope.data[s].netAmount>0)
				totalDisc = totalDisc + parseFloat($scope.data[s].discountAmount);
				net += parseFloat($scope.data[s].netAmount);//+parseFloat(total);
		    }
			
			
			
			//alert('totalAmount-----'+totalAmount);	
			$scope.deliveryChallan.totalCost = totalAmount;
			$scope.deliveryChallan.netAmount = net;	

			var totalCst = $scope.deliveryChallan.totalCost;
			if(totalDisc>0)
			{
				var discPer = (totalDisc/totalCst)*100;
				$scope.deliveryChallan.discountPercent = parseFloat(discPer).toFixed(2);	
			}
		};
		
		
		
		
		$scope.updateNetAmount = function(quantity,mrp,discountAmount,id)
		{
			var total = parseFloat(quantity)*parseFloat(mrp);
			
			if(discountAmount == '' || discountAmount == null)
			{
				discountAmount = 0;
				$scope.data[id].discountAmount = discountAmount;
			}
						
			var netAmount = parseFloat(total)-parseFloat(discountAmount);
			$scope.data[id].netAmount = netAmount;
			
			var totalDisc = 0;
			var net =0;
			//alert($scope.data.length);
		    for(var s=0;s<$scope.data.length;s++)
		    {	
				totalDisc = totalDisc + parseFloat($scope.data[s].discountAmount);
				var gridNet = $scope.data[s].netAmount;
				if(gridNet == '' || gridNet == null)
				{
					gridNet = 0;
				}				
				net = net + parseFloat(gridNet);
				
				var totalCst = $scope.deliveryChallan.totalCost;
				if(totalDisc>0)
				{
					var discPer = (totalDisc/totalCst)*100;
					$scope.deliveryChallan.discountPercent = parseFloat(discPer).toFixed(2);	
				}
		    }
			
			$scope.deliveryChallan.discountAmount = totalDisc;
			$scope.deliveryChallan.netAmount = net;			
			
			if(discountAmount>total)
			{
				sweetAlert("Oops...", "Discount Amount can not be greater than Total Cost!", "error");
				$scope.data[id].discountAmount = 0;	
				$scope.data[id].netAmount = net;					
			}
			
		}
		
		
		
		
		//formsubmit
		$scope.deliveryChallanSubmit = function(deliveryChallan,form)
		{
			
			if($scope.deliveryChallanForm.$valid) 
			{
				$('.btn-hide').attr('disabled',true);
				gridData = [];
				
				$scope.data.forEach(function (stockinData) 
				{
					gridData.push(angular.toJson(stockinData));														 
				});
				
				//alert(JSON.stringify(gridData));
				var obj = new Object();
				obj.formData = angular.toJson(deliveryChallan);
				obj.gridData = gridData;
				
				$.ajax({
				    url:"/SugarERP/saveDelivaryChalan.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						//alert(response);
						if(response != null)
						{
							$('.btn-hide').attr('disabled',false);

							swal("Sucess!", 'Delivary Chalan Added Successfully!', "success");  //-------For showing success message-------------		
							$('.btn-hide').attr('disabled',false);
							
							$scope.deliveryChallan = angular.copy($scope.master);
							$index=1;
							$scope.deliveryChallanData = [{'id':$index}]				
							form.$setPristine(true);
							$scope.$apply();							
						}
						else
						{
							sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						}
					}
				});
			}
			else
			{
				
				var field = null, firstError = null;
				for (field in form) 
				{
					
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				} 
			}
		}
	
	
		
		 $scope.data = [];
		 $scope.master = {};
		 $index=0;
		 
		 	$scope.addFormField = function() 
			{
			
			$index++;
		    $scope.data.push({'id':$index});
			
			
			};
			
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.ShowDatePicker = function()

{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		
		$scope.loadGRNDetails = function()
		{
			
			 $('#gridpopup_box').fadeIn("slow");
		};
		$scope.closeGRNDetails = function()
		{
			 $('#gridpopup_box').fadeOut("slow");
		};
		 $scope.reset = function(form)
		   {			  
				$scope.deliveryChallan = angular.copy($scope.master);
				$index=1;
				//$scope.deliveryChallan={};
				$scope.deliveryChallanData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
		   
		
	})
	
	 .controller('StockconsumptionsController',function($scope,$http,$rootScope)
	 {
		//alert('212');
		$scope.loadDepartments=function()
		{
			//alert('Hello');
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.DepartmentsData=data;
			}); 
		};
		
		$scope.updateToConsumption = function(consumptionNo)
		{
			var index = this.$index;
			
			$rootScope.consumptionNo= consumptionNo;
			
		};
		
		
		$scope.loadConsumptionDetails = function(fromDate,toDate,department)
		{
			var fromDate = fromDate+"-";
			var spltFromDate = fromDate.split("-");
			var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
			
			var toDate = toDate+"-";
			var spltToDate = toDate.split("-");
			var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
			
			
			var dcObj = new Object();
			dcObj.fromDate = dateFrom;
			dcObj.dateTo = dateTo;
			dcObj.department = parseInt(department);
			
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getConsumptionDetails.html",
			data: JSON.stringify(dcObj),
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$("#hidetable").fadeIn(2000);
				
				$scope.StockconsumptionsData=data;
			}); 
		};
	
		$scope.StockconsumptionsData=[];
		$scope.master = {};
			$index=0;
			$scope.loadStatus = function()
			{
				$scope.AddedStockconsumptions = {'status':'0','modifyFlag':'No'};
			};
			$scope.addFormField=function()
			{
				$index++;
				$scope.StockconsumptionsData.push({'id':$index});
			}; 
		    $scope.ShowDatePicker = function()
			{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
			};
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.StockconsumptionsData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.StockconsumptionsData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.StockconsumptionsData[s].id = Number(s)+Number(1);
			}
			
  	    };
			$scope.StockconsumptionsSubmit = function(AddedStockconsumptions,form)
			{
				
				  if($scope.StockconsumptionsForm.$valid)
				  {
				           
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
			};
			$scope.reset = function(form)
		   {			  
				$scope.AddedStockconsumptions = angular.copy($scope.master);
				$index=1;
				$scope.AddedStockconsumptions={};
				$scope.StockconsumptionsData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
		   $scope.loadStatus = function()
			{
				$scope.AddedproductSubgroup= {'status':'0','modifyFlag':'No'};
			};	
	 })
	 
	 
	 
	.controller('consumptionController', function($scope,$http,$rootScope)
	 {
		 $("#consumptionNo").focus();
		 
		 //Added by DMurty on 27-03-2017
		 var d = new Date();
		 var dd = d.getDate();
		 var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		 var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 $scope.AddedConsumption = {'consumptionDate':todayDate};
		 
		$scope.getAllDepartments=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.DepartmentsData=data;
			}); 
		};
		
		$scope.getAllOldConsumptions=function()
		{
			
			/*var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getOldConsumptions.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.consumptionData=data;
			}); */
		};
		
		$scope.onloadFunction=function()
		{
			var rConNo = $rootScope.consumptionNo;
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getConsumptionDetailsByNo.html",
			data: rConNo,
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data.ConsumptionDetailsBeanArray));
				$("#hidetable").fadeIn(2000);
				
				$scope.AddedConsumption=data.ConsumptionSummaryBean;
				$scope.data = data.ConsumptionDetailsBeanArray;
			}); 
		}
	
		//Modify for Added Con. Change
		$scope.getConsumptionDetailsByNo=function(consumptionNo)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getConsumptionDetailsByNo.html",
			data: consumptionNo,
			}).success(function(data, status) 
			{		
				//alert(JSON.stringify(data));			
				$scope.AddedConsumption=data.ConsumptionSummaryBean;
				$scope.data = data.ConsumptionDetailsBeanArray;
			}); 
		};
		
		
		
		$scope.getMaxConsumptionNo=function(department)
		{
			//alert(department);
			var deptId = parseInt(department);
			
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getMaxConsumptionNo.html",
			data: deptId,
			}).success(function(data, status) 
			{
				$scope.AddedConsumption.consumptionNo = data[0].consumptionNo;
				$scope.AddedConsumption.conSeqNo = data[0].conSeqNo;
			}); 
		};
		 
		 
		 $scope.loadProductCode = function(productCode,id)
		{
			var httpRequest = $http
			 ({
				method: 'POST',
				url : "/SugarERP/getProductCodes.html",
				data: productCode,
				}).success(function(data, status) 
				{	
					$scope.addedProductCode=data;
					if(productCode.length>1)
					{	
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
					{
						$scope.data[id].productName = data[0].ProductName;
						$scope.data[id].uom = data[0].Uom;
						$scope.data[id].uomName = data[0].Uomname;
					});
				}
			});
		};
		 
		$scope.getbatchPopup = function(productcode,id,deptId)
		{
			if(productcode==undefined)
			{
				swal("Product Code not given", "Product Code is required :)", "error");
				$("#prodcode"+id).focus();
				return false;
			}
		
			else if(productcode!=undefined)
			{
				$scope.productCode = productcode;
				$scope.productId = id;
				$scope.productBatchData = [];
			
				var stockObj = new Object();
				stockObj.productCode = productcode;
				stockObj.deptId = parseInt(deptId);
			
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getProductBatches.html",
				data: JSON.stringify(stockObj),
			}).success(function(data, status) 
		   {	
				$('#gridpopup_box').fadeIn("slow");
				$scope.productBatchData = data;
		   });
			}
		};
		
		$scope.batchDatapush = function(checkbox,batch,expiryDate,mrp,id,batchSeqNo,costPrice,batchId,mfrDate,stock,stockInDate)
		{
			if(checkbox==true)
			{
				var rowId = $scope.productId;
				$scope.popupId = id;
				 $('#gridpopup_box').fadeOut("slow");
				 
				 $scope.data[rowId].batch = batch;
				 $scope.data[rowId].expDt = expiryDate;
				 $scope.data[rowId].mrp = mrp;
				 $scope.data[rowId].batchFlag = 'Old';
				 $scope.data[rowId].batchSeqNo = batchSeqNo;
				 $scope.data[rowId].batchId = batchId;
				 $scope.data[rowId].stockInDate = stockInDate;
				 $scope.data[rowId].cp = costPrice;
			}
			else if(checkbox==false)
			{
							
							
			}
						
		};
		 
		 
		 
		$index=0;
		$scope.data=[];
		$scope.addFormField=function()
		{
			$index++;
			$scope.data.push({'id':$index});
		};
		
			
		$scope.consumptionSubmit = function(AddedConsumption,form)
		{
			//alert('hello---');
		  if($scope.consumptionForm.$valid)
		  {
			$('.btn-hide').attr('disabled',true);
			gridData = [];
		
			$scope.data.forEach(function (stockinData) 
			{
				gridData.push(angular.toJson(stockinData));														 
			});
		
			var obj = new Object();
			
			//oldConsumptions
			delete AddedConsumption['oldConsumptions'];

			obj.formData = angular.toJson(AddedConsumption);
			obj.gridData = gridData;
			  
			$.ajax({
				url:"/SugarERP/saveStockConsumption.html",
				processData:true,
				type:'POST',
				contentType:'Application/json',
				data:JSON.stringify(obj),
				beforeSend: function(xhr) 
				{
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success:function(response) 
				{
					//alert(response);
					if(response != null)
					{
						$('.btn-hide').attr('disabled',false);

						swal("Sucess!", 'Consumption Added Successfully!', "success");  
						$('.btn-hide').attr('disabled',false);
						
						$scope.AddedConsumption = angular.copy($scope.master);
						$index=1;
						$scope.data = [{'id':$index}]				
						form.$setPristine(true);
						$scope.$apply();							
					}
					else
					{
						sweetAlert("Oops...", "Something went wrong!", "error");
					   $('.btn-hide').attr('disabled',false);
					}
				}
			});
				
			  
		  }
		  else
		  {
			  alert('else');
			var field = null, firstError = null;
				for (field in form) 
				{
					
					if (field[0] != '$') 
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}	
				} 
		  }
		};
	 })
	.controller('SaleDeliveryChallans',function($scope,$http,$rootScope)
	 {
		 $("#fromDate").focus();
		 
		 $scope.getAllDepartments=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.DepartmentsData=data;
			}); 
		};
		
		$scope.getSaleDcs = function(fromDate,toDate,department)
		{
			var fromDate = fromDate+"-";
			var spltFromDate = fromDate.split("-");
			var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
			
			var toDate = toDate+"-";
			var spltToDate = toDate.split("-");
			var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
			
			
			var dcObj = new Object();
			dcObj.dateFrom = dateFrom;
			dcObj.dateTo = dateTo;
			dcObj.department = department;
			
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getSDCDetails.html",
			data: JSON.stringify(dcObj),
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$("#hidetable").fadeIn(2000);
				
				$scope.sdcData=data;
			}); 
		};
		
		$scope.updateToDc = function(sdcno,index)
		{
			var index = this.$index;
			$rootScope.sdcno= sdcno;
			
		};
			
		/*$scope.getAllDcs = function(sdcno)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDcs.html",
			data: sdcno,
			}).success(function(data, status) 
			{
				alert(JSON.stringify(data));
				$("#hidetable").fadeIn(2000);
				
				//$scope.AddedSalesDc=data;
			}); 
			
			
		}*/
		
		
		
		$scope.stockRequestData=[];
		$scope.master = {};
		$index=0;
		$scope.addFormField=function()
		{
			$index++;
			$scope.stockRequestData.push({'id':$index});
		};
		
		$scope.ShowDatePicker = function()
		{
			$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
		$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		 $scope.reset = function(form)
		   {			  
				$scope.AddedStockRequest = angular.copy($scope.master);
				$index=1;
				$scope.AddedStockRequest={};
				$scope.stockRequestData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
		
	 })
	

	.controller('StockSupplyDetailsController',function($scope,$http,$rootScope)
	 {
		$("#fromDate").focus();
		$scope.stockSupplyData=[];
		$scope.master = {};
		$index=0;
			
			
			
			
			$scope.getStockSupplyDetails = function(fromDate,toDate,department)
			{
				var fromDate = fromDate+"-";
				var spltFromDate = fromDate.split("-");
				var dateFrom = spltFromDate[2]+"-"+spltFromDate[1]+"-"+spltFromDate[0];
				
				var toDate = toDate+"-";
				var spltToDate = toDate.split("-");
				var dateTo = spltToDate[2]+"-"+spltToDate[1]+"-"+spltToDate[0];
				
				
				var stockSupplyDetailsObj = new Object();
				stockSupplyDetailsObj.dateFrom = dateFrom;
				stockSupplyDetailsObj.dateTo = dateTo;
				stockSupplyDetailsObj.department = department;
				
				
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getStockSupplyDetails.html",
				data: JSON.stringify(stockSupplyDetailsObj),
				}).success(function(data, status) 
				{
					$("#hidetable").fadeIn(1000);
					//alert(JSON.stringify(data));
					$scope.stockSupplyData=data;
				}); 
				
			};
			
			$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
			
		$scope.removeRow = function(name)
	    {				
			var Index = -1;		
			var comArr = eval( $scope.stockSupplyData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockSupplyData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockSupplyData[s].id = Number(s)+Number(1);
			}			
  	    };
			$scope.StockSupplySubmit=function(AddedStockSupply,form)
			{
				
				  if($scope.StockSupplyDetailsForm.$valid)
				  {
				   
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};

			
			$scope.UpdateStockInNyyo = function(stockInNo,index)
			{
			
				var index = this.$index;
				$rootScope.arraydefault= stockInNo;
			};
				
	 })
	 
	 .controller('ClosingStockReport',function($scope,$http)
	 {
		$scope.StockData = [];
		$scope.loadDepartments=function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				$scope.DepartmentsData=data;
			}); 
		};	
	
		$scope.getClosingStock=function(deptId)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getClosingStockByDepartment.html",
			data: deptId,
			}).success(function(data, status) 
			{
				$("#hideTable").fadeIn(2000);
				$scope.closingStockData=data;
				$scope.StockData=data;
			}); 
		}
		
		//export to excel
		$scope.exportData = function()
		{
			alasql('SELECT productGroup as ProductGroup,productSubGroup as ProductSubGroup,productCode as ProductCode,productName as ProductName,batch as Batch,expDt as ExpiryDate,currentStock as CurrentStock,mrp as MRP,mrpValue as MRPValue,cp as CP,cpValue as CpValue INTO XLSX("ClosingStockStatement.xlsx",{headers:true}) FROM ?',[$scope.StockData]);
		};
	
	  	
	 })
	 
	.controller('ProductLedger',function($scope,$http)
	 {	
		$("#fromDate").focus();
		
		$scope.StockData = [];
		var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		 var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 $scope.productLedger = {'fromDate':todayDate,'toDate':todayDate};
		 
		 
		 
		$scope.loadDepartments=function()
		{
			//alert('Hello');
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getAllDepartments.html",
			data: {}
			}).success(function(data, status) 
			{
				//alert(JSON.stringify(data));
				$scope.DepartmentsData=data;
			}); 
		};
		
		$scope.loadProductCode = function(productCode)
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getProductCodes.html",
			data: productCode,
			}).success(function(data, status) 
			{
				$scope.addedProductCode=data;
			});
			
		};
		
		$scope.getProductLedger=function(fromDate,toDate,deptId,productCode)
		{
			var obj = new Object();
			obj.fromdate = fromDate;
			obj.todate = toDate;
			obj.deptId = parseInt(deptId);
			obj.productCode = productCode;

			//alert(JSON.stringify(obj));
			
			
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/getProductLedger.html",
			data: obj,
			}).success(function(data, status) 
			{
				$("#hideTable").fadeIn(2000);
				//alert(JSON.stringify(data));
				$scope.productLedgerData=data.gridData;
				$scope.pLedgerReport=data.productData; 					
			});
		};
		
		$scope.exportData = function()
		{
			alasql('SELECT date as Date,recOrIssTo as Received or Issued to,delNoteOrVochNo as Delivery Note Or Vouvher No,batch as Batch,batch as Batch,expDt as ExpiryDate,qtyReceived as Qty. Received,qtyIssued as Qty. Issued,stockBalance as Stock Balance INTO XLSX("ClosingStockStatement.xlsx",{headers:true}) FROM ?',[$scope.StockData]);
		};
	 })





	 .controller('DispatchBhuShaktiController',function($scope,$http)
	 {					
		$index =0;
		$scope.data=[];
		var SeasonDropDownLoad = new Object();
		SeasonDropDownLoad.dropdownname = "Season";
		var jsonStringSeasonDropDown= JSON.stringify(SeasonDropDownLoad);
			
		var LoadRyotDropDown = new Object();
		LoadRyotDropDown.dropdownname = "ExtentDetails";
		var ExtentRyotCode= JSON.stringify(LoadRyotDropDown);
		
		var FieldOffDownLoad = new Object();
		FieldOffDownLoad.dropdownname = "FieldOfficer";
		var FieldOffDropDown= JSON.stringify(FieldOffDownLoad);
		
		var ZoneDropDownLoad = new Object();
		ZoneDropDownLoad.dropdownname = "Zone";
		var jsonStringDropDownZone= JSON.stringify(ZoneDropDownLoad);


		  var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		 
		  var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 
		 $scope.addGrid = function() 
			{
			
			$index++;
		    $scope.data.push({'id':$index});
			
			
			};
		
			$scope.loadseasonNames = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: jsonStringSeasonDropDown,
				  }).success(function(data, status) 
				{
					$scope.seasonNames=data;
					$scope.AddedBhuShakti = {'date':todayDate};
				}); 
			};
		
		$scope.loadRyotCodeDropDown = function()
		{
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/loadDropDownNames.html",
			data: ExtentRyotCode,
			 }).success(function(data, status) 
			{
			
				$scope.ryotCodeData = data;					
			});  	   
		};
		
		$scope.updateRyotNamebyCode = function(RyotCode)
			{	
			var httpRequest = $http({
			method: 'POST',
			url : "/SugarERP/GetRyotNameForCode.html",
			data: RyotCode,
			}).success(function(data, status) 
			{
				
				$scope.AddedBhuShakti.ryotName = data[0].ryotName;
			});
			
			
		};
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
			
			$scope.loadFieldOfficerDropdown = function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/loadDropDownNames.html",
				data: FieldOffDropDown,
			  	}).success(function(data, status) 
			   	{	
					$scope.FieldOffNames=data;
	 		   	});  	   
			};
			
			$scope.loadIndents = function(season,ryotcode)
				{
					
					var indentObj = new Object();
					indentObj.Season = season;
					indentObj.RyotCode = ryotcode;
					  if(ryotcode!='' && ryotcode!=null && season!='' && season!=null )
				
					{
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getAuthorisedIndentsforDispatch.html",
						data: JSON.stringify(indentObj),
						  }).success(function(data, status) 
						{
							
							$scope.IndentData = data;
							
						});
					}
					
				};
				
			$scope.loadZoneNames = function()
			{
				var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/loadDropDownNames.html",
									data: jsonStringDropDownZone,
							 }).success(function(data, status) 
							 {
								$scope.ZoneData=data;
								
							 });  	   
			};

			$scope.loadProductCode = function(productCode,gridCount)
			 {
				
				  var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						
									var g= gridCount;
									$scope.data[g].productName = data[0].ProductName;
									$scope.data[g].uom = data[0].Uom;
									$scope.data[g].uomName = data[0].Uomname;
						 
						});
					}
				}); 
				
					
				 };	
	 })
	 
	 .controller('ProductListController',function($scope,$http,$rootScope)
				{
					
					$scope.ProductListData=[];
		            $scope.master = {};
		         	
					$scope.ProductListSubmit = function(AddedProductList,form)
					{
				
				  if($scope.ProductListForm.$valid)
				  {
				           
				  
						
					 
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			$scope.loadproducts=function(){
				
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getGroups.html",
							data: {},
					   }).success(function(data, status) 
						{
						$scope.productGroups=data;
						data.push({"GroupCode":0,"Group":"ALL"});
						})
				
				
			};
			
			
			$scope.AddedProductLists=function(GroupCode){
				var groupCode = GroupCode;
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getSubGroupsByGrp.html",
							data: GroupCode,
					   }).success(function(data, status) 
						{
						
							if(groupCode=="0")
							{
								$scope.productSubGroups=data;
								data.push({"subGroupCode":0,"subGroup":"ALL"});
								
								
							}
							else
							{
								$scope.productSubGroups=data;
							}
							//alert(JSON.stringify(data));
							
						})
				
				
			};
			
			$scope.getAllProductData= function(productGroup,productSubGroup)
			{
				
				if(productSubGroup==null || productSubGroup=='')
				{
					productSubGroup = 0;
				}
				
			var productSubGroup = productSubGroup.toString();
				var httpRequest = $http({
							method: 'POST',
							url : "/SugarERP/getAllProducts.html",
							data: productSubGroup,
					   }).success(function(data, status) 
						{
							
							
							$scope.data = data;
						})
				
				
			};
			$scope.UpdateProductList = function(productCode,id)
			{
				//alert(JSON.stringify(angular.toJson(productList)));
				$rootScope.gridProduct= productCode;
			};
				
			
			
			$scope.reset = function(form)
		   {			  
				$scope.AddedProductList = angular.copy($scope.master);
				$index=1;
				$scope.AddedProductList={};
				$scope.ProductListData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
		   
		   $scope.loadDepartments=function(){
			
				
				$scope.productGroups=[{'id':1,'productGroup':'ALL'},{'id':2,'productGroup':'ALL MUET' }];
				$scope.productSubGroups=[{'id':1,'productSubGroup':'ALL'},{'id':2,'productSubGroup':'ALL MUET' }];
				
				};
				})
	 
	 
	 
	 
	  .controller('SaleInvoice',function($scope,$http)
	 {
		$scope.stockRequestData=[];
		$scope.master = {};
			$index=0;
			$scope.addFormField=function()
			{
				$index++;
				$scope.stockRequestData.push({'id':$index});
			};
			
			$scope.ShowDatePicker = function()
			{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
			};
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.stockRequestData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.stockRequestData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.stockRequestData[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		$scope.SaleInvoiceSubmit = function(AddedSaleInvoice,form)
			{
				
				  if($scope.SaleInvoiceForm.$valid)
				  {
				           
				  
						
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
		 $scope.reset = function(form)
		   {			  
				$scope.AddedStockRequest = angular.copy($scope.master);
				$index=1;
				$scope.AddedStockRequest={};
				$scope.stockRequestData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
		
	 })
	 
	 .controller('SaleInvoices',function($scope,$http)
	 {
		
		$scope.SaleInvoicesData=[];
		$scope.master = {};
			$index=0;
			$scope.loadStatus = function()
			{
				$scope.AddedSaleInvoices = {'status':'0','modifyFlag':'No'};
			};
			$scope.addFormField=function()
			{
				$index++;
				$scope.SaleInvoicesData.push({'id':$index});
			}; 
			$scope.ShowDatePicker = function()
			{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
			};
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.SaleInvoicesData );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.SaleInvoicesData.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.SaleInvoicesData[s].id = Number(s)+Number(1);
			}
			
  	    };
		$scope.SaleInvoicesSubmit = function(AddedSaleInvoices,form)
			{
				
				  if($scope.SaleInvoicesForm.$valid)
				  {
				           
				  
						
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			$scope.reset = function(form)
		   {			  
				$scope.AddedSaleInvoices = angular.copy($scope.master);
				$index=1;
				$scope.AddedSaleInvoices={};
				$scope.SaleInvoicesData = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
			
			
			$scope.loadDepartments=function(){
			
				
				$scope.departments=[{'id':1,'department':'ALL'},{'id':2,'department':'ALL MUET' }]
				
				};
	 })
	  .controller('GRNController',function($scope,$http,$rootScope,$state)
	 {
		  var d = new Date();
		  var dd = d.getDate();
		  var m = d.getMonth();
		
		  var mm = m+1;
		  ddstring = dd.toString();
		  var mstring = mm.toString();
		    if(ddstring.length==1)
		  {
			dd = "0"+dd;  
		  }
		  
		  if(mstring.length==1)
		  {
			  
			  mm = "0"+mm;
		  }
		
		 
		  var yy = d.getFullYear();
		 var todayDate  = dd+"-"+mm+"-"+yy;
		 
		 var obj = new Object();
		$scope.data=[];
		$scope.master = {};
			$index=0;
			$scope.addFormField=function()
			{
				$index++;
				$scope.data.push({'id':$index});
			}; 
		   	$scope.ShowDatePicker = function()
		{
	     		$(".datepicker").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy', beforeShow: function() { setTimeout(function(){ $('.ui-datepicker').css('z-index', 999999); }, 0); } });
		};
			$scope.removeRow = function(name)
		{				
			var Index = -1;		
			var comArr = eval( $scope.data );
			for( var i = 0; i < comArr.length; i++ ) 
			{
				if( comArr[i].id === name ) 
				{
					Index = i;
					break;
				}				
			}
			$scope.data.splice( Index, 1 );
			$index = comArr.length;
			for(var s=0;s<$index;s++)
			{
				$scope.data[s].id = Number(s)+Number(1);
			}
			
  	    };
		
		
		
		$scope.loadProductCode = function(productCode,id)
			{
			var httpRequest = $http
				 ({
					method: 'POST',
					url : "/SugarERP/getProductCodes.html",
					data: productCode,
					}).success(function(data, status) 
					{	
						
						$scope.addedProductCode=data;
						
						if(productCode.length>1)
						{
							
						var httpRequest = $http({
						method: 'POST',
						url : "/SugarERP/getProductDetailsByCodes.html",
						data: productCode,
					   }).success(function(data, status) 
						{
						//	alert(JSON.stringify(data));
							
							$scope.data[id].productName = data[0].ProductName;
							$scope.data[id].uom = data[0].Uom;
							$scope.data[id].uomName = data[0].Uomname;
						});
					}
				});
			};


			
		
		var username =  sessionStorage.getItem('username');
		$scope.onloadFunction = function()
		{
			
			var grnStockIssueNo=$rootScope.grnStockIssueNo;
			var rootgrnNo = $rootScope.rootgrnNo;
			
			if(grnStockIssueNo==undefined && rootgrnNo==undefined)
			{
				
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIssueDetails.html",
				data:grnStockIssueNo,
				
				}).success(function(data, status) 
				{
					
					
					$("#hideTable").show();
					$scope.AddedGRN = data.IssueSummaryBean;
					$scope.data = data.IssueDetailsBean;

					for(var i=0;i<$scope.data.length;i++)
					{
						
						var pcode = data.IssueDetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
					
				}); 
			}
			else if(grnStockIssueNo!=undefined && rootgrnNo==undefined)
			{
				
				
			
			var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllStockIssueDetails.html",
				data: grnStockIssueNo,
			   }).success(function(data, status) 
				{
					
					$("#hideTable").show();

					$scope.AddedGRN = data.IssueSummaryBean;
					
					
					
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getUserDeptStatus.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.AddedGRN = {'deptId':data[0].Department,'loginId':data[0].Username,'modifyFlag':'No','status':data[0].Status,'grnDate':todayDate,'stockIssueNo':$rootScope.grnStockIssueNo,'modifyFlag':'No'};
					$scope.getMaxStockReqid(data[0].Department);
					
				});
					
					$scope.data = data.IssueDetailsBean;
					

					for(var i=0;i<$scope.data.length;i++)
					{
						
						var pcode = data.IssueDetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
					
					
				});
			}
			else if(grnStockIssueNo==undefined && rootgrnNo!=undefined)
			{
				//alert("last else");
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllGrnDetails.html",
				data: rootgrnNo,
			   }).success(function(data, status) 
				{
					
					//alert(JSON.stringify(data));
					$("#hideTable").show();
					$scope.checkValue=true;
					$scope.AddedGRN = data.GrnSummaryBean;
					$scope.data = data.GrnDetailsBean;
					for(var i=0;i<$scope.data.length;i++)
					{
						
						var pcode = data.GrnDetailsBean[i].productCode;
						$scope.loadProductCode(pcode,i);
						
					}
				});
			}
			
			
		};
		
		
		
			
			$scope.getuserLoginId = function()
			{
				
				if($rootScope.grnStockIssueNo==undefined)
				{
					$scope.checkValue=false;
					
					var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getUserDeptStatus.html",
				data: {}
				}).success(function(data, status) 
				{
					
					
					$scope.AddedGRN = {'deptId':data[0].Department,'loginId':data[0].Username,'modifyFlag':'No','grnDate':todayDate,'modifyFlag':'No'};
					
					$scope.getMaxStockReqid(data[0].Department);
				});
				}
				else
				{
					$scope.checkValue=true;
				}
			};


			$scope.getMaxStockReqid = function(deptID)
			{
			var deptID = $scope.AddedGRN.deptId;
		
				var username =  sessionStorage.getItem('username');
				if($rootScope.grnStockIssueNo==undefined)
				{
					
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxGrnid.html",
					data:deptID,
			   }).success(function(data, status) 
				{
						
					$scope.AddedGRN.grnNo = data[0].MaxId;
					
				
				
				});
				}
				else
				{
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxStockReqid.html",
					data:deptID,
			   }).success(function(data, status) 
				{
					if($rootScope.grnStockIssueNo!=undefined)
					{
						$scope.AddedGRN.stockIssueNo = $rootScope.grnStockIssueNo;

						var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getMaxGrnid.html",
					data:deptID,
			   }).success(function(data, status) 
				{
						
					$scope.AddedGRN.grnNo = data[0].MaxId;
					
				
				
				});
					}
					
				
				
				});
				

				}
					
			};
			
			$scope.getGRNSummaryDetails = function(stockIssueNo)
			{
				if($rootScope.grnStockIssueNo==undefined)
				{
					
					var httpRequest = $http({
					method: 'POST',
					url : "/SugarERP/getAllStockIssueDetails.html",
					data:stockIssueNo,
					
					}).success(function(data, status) 
					{
						$("#hideTable").show();
						
						
						$scope.data = data.IssueDetailsBean;
	
						for(var i=0;i<$scope.data.length;i++)
						{
							var pcode = data.IssueDetailsBean[i].productCode;
							$scope.loadProductCode(pcode,i);
						}
	
					});
				}
				else
				{
					
				}
			};

		
		$scope.updateRejectedQty = function(issued,accepted,id)
		{
			if(parseInt(accepted)>parseInt(issued))
			{
				sweetAlert("Oops","Accepted Qty cannot be greater than Issued Qty ", "error");
				$scope.data[id].acceptedQty="";
			}
			if(accepted<=issued)
			{
				var rejected = Number(issued)-Number(accepted);
			$scope.data[id].rejectedQty = rejected;
			}
			
		};
		
		$scope.getAllDepartments=function()
			{
				var httpRequest = $http({
				method: 'POST',
				url : "/SugarERP/getAllDepartments.html",
				data: {}
				}).success(function(data, status) 
				{
					
					$scope.DepartmentsData=data;
				}); 
				
			};
		 
		 
		 
		 $scope.GRNSubmit = function(AddedGRN,form)
			{
				
				  if($scope.GrnForm.$valid)
				  {
				    delete AddedGRN["status"];      
				  $('.btn-hide').attr('disabled',true);
				 gridData = [];
				 $scope.data.forEach(function (grnData) 
					  {
						delete grnData['status'];
						delete grnData['batchQty'];
						delete grnData['pendingQty'];
						gridData.push(angular.toJson(grnData));														 
				      });
					  obj.formData = angular.toJson(AddedGRN);
					  obj.gridData = gridData;
					  
					
				
			    $.ajax({
				    url:"/SugarERP/SaveGrnDetails.html",
				    processData:true,
				    type:'POST',
				    contentType:'Application/json',
				    data:JSON.stringify(obj),
					beforeSend: function(xhr) 
					{
			            xhr.setRequestHeader("Accept", "application/json");
			            xhr.setRequestHeader("Content-Type", "application/json");
			        },
				    success:function(response) 
					{
						if(response==true)
						{
							if($rootScope.grnStockIssueNo==undefined && $rootScope.rootgrnNo==undefined)
							{
								
								//swal("Sucess!", 'GRN updated Successfully!', "success");  //-------For showing success message-------------		
								
								
								$('.btn-hide').attr('disabled',false);
								var grnNoObj = new Object();
								grnNoObj.GRNNo = $scope.AddedGRN.grnNo;
	                             // alert(JSON.stringify(reqNoObj));
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getGRNDetailsForPrint.html",
									data: JSON.stringify(grnNoObj),
								   }).success(function(data, status) 
									{
										//alert(data[0].grnNo);
										//alert(data[0].grndate);
										//alert(JSON.stringify(data[0].ReqDate));
										$scope.grnPrint = data;
										$scope.grnNo = data[0].grnNo;
										$scope.grndate = data[0].grndate;
										//$scope.AddedStockRequest.reqDate = data[0].ReqDate;
										
										
										swal({
													 title: "GRN updated Successfully!",
													 text: " Do You want to take a print out?",
													 type: "success",
													 showCancelButton: true,
													 confirmButtonColor: "#2298f2",
													 confirmButtonText: "Print",
													 closeOnConfirm: true}, 
													 function(){
														
														var prtContent = document.getElementById("grnPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
														 
													
													
													});
										
										
									}); 
								
								
								$scope.AddedGRN = angular.copy($scope.master);
								$scope.getuserLoginId();
								$index=1;
								$scope.data = [{'id':$index}]				
								form.$setPristine(true);
								$scope.$apply();
								
							}
							else if($rootScope.grnStockIssueNo!=undefined && $rootScope.rootgrnNo==undefined)
							{
								//alert("else if");
								//swal("Sucess!", 'GRN updated Successfully!', "success");  //-------For showing success message-------------		
								//$('.btn-hide').attr('disabled',false);
								
								$('.btn-hide').attr('disabled',false);
								var grnNoObj = new Object();
								grnNoObj.GRNNo = $scope.AddedGRN.grnNo;
	                             // alert(JSON.stringify(reqNoObj));
									var httpRequest = $http({
									method: 'POST',
									url : "/SugarERP/getGRNDetailsForPrint.html",
									data: JSON.stringify(grnNoObj),
								   }).success(function(data, status) 
									{
										
										//alert(JSON.stringify(data[0].ReqDate));
										$scope.grnPrint = data;
										$scope.grnNo = data[0].grnNo;
										$scope.grndate = data[0].grndate;
										//$scope.AddedStockRequest.reqDate = data[0].ReqDate;
										
										
										swal({
													 title: "GRN updated Successfully!",
													 text: " Do You want to take a print out?",
													 type: "success",
													 showCancelButton: true,
													 confirmButtonColor: "#2298f2",
													 confirmButtonText: "Print",
													 closeOnConfirm: true}, 
													 function(){
														
														var prtContent = document.getElementById("grnPrint");
													var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
													WinPrint.document.write(prtContent.innerHTML);
													WinPrint.document.close();
													WinPrint.focus();
													WinPrint.print();
													WinPrint.close(); 
														 
													
													
													});
										
										
									});
								
								$scope.AddedGRN = angular.copy($scope.master);
								$index=1;
								$scope.data = [{'id':$index}];
								form.$setPristine(true);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true);
								$rootScope.grnStockIssueNo="";
								$scope.$apply();
								setTimeout(function(){  $state.go('store.transaction.StockIssues'); }, 2000);
								
							}
							else
							{
								//alert("else");
								swal("Sucess!", 'GRN updated Successfully!', "success");  //-------For showing success message-------------		
								$('.btn-hide').attr('disabled',false);
								
								$scope.AddedGRN = angular.copy($scope.master);
								$index=1;
								$scope.data = [{'id':$index}];
								form.$setPristine(true);
								$('.btn-hide').attr('disabled',false);
								form.$setPristine(true);
								$rootScope.grnStockIssueNo="";
								$scope.$apply();
								 $state.go('store.transaction.GRNs');
							}
							
						

							
						 }
						else
						 {
						   sweetAlert("Oops...", "Something went wrong!", "error");
						   $('.btn-hide').attr('disabled',false);
						   
						   
						 }
				    }
		         });
						
					  //alert(JSON.stringify(AddedproductSubgroup));
				  }
				  else
				  {
					var field = null, firstError = null;
						for (field in form) 
						{
							
							if (field[0] != '$') 
							{
								if (firstError === null && !form[field].$valid) 
								{
									firstError = form[field].$name;
								}
								if (form[field].$pristine) 
								{
									form[field].$dirty = true;
								}
							}	
						} 
				  }
				
				
			};
			$scope.reset = function(form)
		   {			  
				$scope.AddedGRN = angular.copy($scope.master);
				$scope.getuserLoginId();
				$index=1;
				
				$scope.data = [{'id':$index}]				
			    form.$setPristine(true);
				$scope.$apply();
		   };
	 })
	
	 
	  .controller('StockrequestprintController',function($scope,$http)
	 {
			
		 $scope.updatePrintData=function(){
			 $("#stockRequestPrint").show();
		    $scope.stockRequuest= [{"itemName":"item1","quantity":"200","noOfAcres":"10.000","remarks":"remarks1"},{"itemName":"item2","quantity":"200","noOfAcres":"10.000","remarks":"remarks2"},{"itemName":"item3","quantity":"200","noOfAcres":"10.000","remarks":"remarks3"}]
			
					
			
		 };
		 
		 $scope.printStockReq = function()
		 {
			 var prtContent = document.getElementById("stockRequestPrint");
					var WinPrint = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
		 };
	 })
	 
	 
	 .controller('StocktransferprintController',function($scope,$http)
	 {
		
             $scope.updateTransferPrintData=function(){
			 $("#stockTransferPrint").show();
		    $scope.stockTransfer= [{"itemName":"item1","quantity":"200"},{"itemName":"item2","quantity":"200"},{"itemName":"item3","quantity":"200"}]
			
					
			
		 };
		$scope.printStockTransfer = function()
		 {
			 var prtContent = document.getElementById("stockTransferPrint");
					var WinPrint = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
		 };
	 })
	 
	 .controller('SalesDeliveryChallanPrintController',function($scope,$http)
	 {
		
       $scope.updatePrintitemData=function()
	   {
			 $("#itemsfromZonalOffice").show();
		    $scope.itemsfromZonalOffice= [{"itemName":"Bhoo shakti 50kgs Bags","noOfAcres":"10.000","noOfBags":"50","remarks":"remarks1"},{"itemName":"Bhoo shakti 30kgs Bags","noOfAcres":"10.000", "noOfBags":"30", "remarks":"remarks2"},{"itemName":"Bhoo shakti 40kgs Bags","noOfAcres":"10.000","noOfBags":"40","remarks":"remarks3"}]
		};
		 $scope.printitemsfromZonalOffice = function()
		 {
			 var prtContent = document.getElementById("itemsfromZonalOffice");
					var WinPrint = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
		 };


	 })
	 
	  .controller('DeliveryChallanPrintController',function($scope,$http)
	{	
	    $scope.printDeliveryChallan = function()
		 {
			 var prtContent = document.getElementById("deliveryChallanPrint");
					var WinPrint = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
		 };
	})
	 
	    .controller('GRNPrintController',function($scope,$http)
	{										   
			$scope.updateGRNPrintData=function(){
			 $("#grnPrint").show();
		    $scope.grnPrint= [{"itemName":"Bhooshakthi 30kg Bags","quantity":"200","noOfAcres":"","remarks":"good"},{"itemName":"Bhooshakthi 40kg Bags","quantity":"400","noOfAcres":"",  "remarks":"good"},{"itemName":"Bhooshakthi 50kg Bags","quantity":"600","noOfAcres":"","remarks":"good"}]
			
			
					
			
		 };	
		 $scope.printGRN = function()
		 {
			 var prtContent = document.getElementById("grnPrint");
					var WinPrint = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
					WinPrint.document.write(prtContent.innerHTML);
					WinPrint.document.close();
					WinPrint.focus();
					WinPrint.print();
					WinPrint.close();
		 };
	 })