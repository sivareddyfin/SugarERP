	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="DetrashingController" ng-init="loadShift();loadVarietyNames();addFormField();loadVillageNames();loadseasonNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Detrashing</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="DetrashingForm" novalidate  ng-submit='DetrashSubmit(AddedDetrashing,DetrashingForm);'>
							  <input type="hidden" name="modifyFlag" ng-model="AddedDetrashing.modifyFlag" />
							   
							  <div class="row">
							 
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : DetrashingForm.season.$invalid && (DetrashingForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedDetrashing.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="getAddedDetrashingList(AddedDetrashing.season,AddedDetrashing.detrashingDate,AddedDetrashing.shift)">
															<option value="">Season</option>
						        			            </select>	
				        		        	        </div>
													<p ng-show="DetrashingForm.season.$error.required && (DetrashingForm.season.$dirty || submitted)" class="help-block">Season</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : DetrashingForm.detrashingDate.$invalid && (DetrashingForm.detrashingDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="detrashingDate" ng-model="AddedDetrashing.detrashingDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-blur="getAddedDetrashingList(AddedDetrashing.season,AddedDetrashing.detrashingDate,AddedDetrashing.shift)"/>	
				        		        	        </div>
													<p ng-show="DetrashingForm.detrashingDate.$error.required && (DetrashingForm.detrashingDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							  <div class="row">
							  	<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : DetrashingForm.shift.$invalid && (DetrashingForm.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift" ng-required="true"  ng-model="AddedDetrashing.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames | orderBy:'-shiftName':true" ng-change="getAddedDetrashingList(AddedDetrashing.season,AddedDetrashing.detrashingDate,AddedDetrashing.shift)">
															<option value="">Shift</option>
															
														</select>
				                			        </div>
													<p ng-show="DetrashingForm.shift.$error.required && (DetrashingForm.shift.$dirty || submitted)" class="help-block">Select Shift</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								
								
							  </div>
							  <div class="row">
							 <div class="col-sm-4">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
		<input type="radio" name="labourSpplr" value="0" ng-model="AddedDetrashing.labourSpplr" ng-click="getLabourContractorDetails();">
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
		<input type="radio" name="labourSpplr" value="1"  ng-model="AddedDetrashing.labourSpplr" ng-click="getLabourContractorDetails();" >
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>
										</div>


							<div class='col-sm-4'>
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
												<select chosen class="w-100" name='lc'  ng-model="AddedDetrashing.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							</div>
									
								
										
							 </div>
							  <div ng-show="AddedDetrashing.labourSpplr=='0'">						
							  <div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
									</div>
								</div>
								
							 	<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Men</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.noOfMen.$invalid && (DetrashingForm.noOfMen.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="noOfMen">No. of People</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfMen" ng-model="AddedDetrashing.noOfMen" data-ng-pattern="/^[0-9\s]*$/" ng-keyup="LoadManTotal(AddedDetrashing.noOfMen,AddedDetrashing.manCost);getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" tabindex="3" id="noOfMen" with-floating-label  />
												</div>
												
												<p ng-show="DetrashingForm.noOfMen.$error.pattern && (DetrashingForm.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>	
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.manCost.$invalid && (DetrashingForm.manCost.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="manCost">Man Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="manCost" ng-model="AddedDetrashing.manCost" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  ng-keyup="LoadManTotal(AddedDetrashing.noOfMen,AddedDetrashing.manCost);getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" tabindex="4"  id="manCost" with-floating-label />
												</div>
												<p ng-show="DetrashingForm.manCost.$error.pattern && (DetrashingForm.manCost.$dirty || submitted)" class="help-block">Invalid</p>	
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												   <label for="menTotal">Man Total</label>
														<input type="text" class="form-control" placeholder="Total" name="menTotal" readonly ng-model="AddedDetrashing.menTotal" tabindex="5" ng-keyup="getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);"   ng-change="getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" id="menTotal" with-floating-label />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.noOfWomen.$invalid && (DetrashingForm.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="noOfWomen">No. of Woman</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfWomen" data-ng-pattern="/^[0-9\s]*$/"   ng-model="AddedDetrashing.noOfWomen" ng-keyup="LoadWoManTotal(AddedDetrashing.noOfWomen,AddedDetrashing.womenCost);getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);"  tabindex="6" id="noOfWomen" with-floating-label />
												</div>
												
											<p ng-show="DetrashingForm.noOfWomen.$error.pattern && (DetrashingForm.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>		
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.womenCost.$invalid && (DetrashingForm.womenCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="womenCost">Woman Cost</label>
														<input type="text" class="form-control" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  placeholder="Cost" name="womenCost" ng-model="AddedDetrashing.womenCost" ng-keyup="LoadWoManTotal(AddedDetrashing.noOfWomen,AddedDetrashing.womenCost);getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" tabindex="7" id="womenCost" with-floating-label />
												</div>
												
												<p ng-show="DetrashingForm.womenCost.$error.pattern && (DetrashingForm.womenCost.$dirty || submitted)" class="help-block">Invalid</p>		
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="womenTotal">Woman Total</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" readonly ng-model="AddedDetrashing.womenTotal" tabindex="8" ng-keyup="getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" ng-change="getGrandTotalforDetrashing(AddedDetrashing.menTotal,AddedDetrashing.womenTotal,AddedDetrashing.totalLabourCost,AddedDetrashing.labourSpplr);" id="womenTotal" with-floating-label />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								
								<div class="row">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalLabourCost"></label>
														<input type="text" class="form-control" placeholder="Grand Total" ng-model="AddedDetrashing.totalLabourCost" tabindex="12" readonly id="totalLabourCost" with-floating-label />
												</div>
												</div>
												</div>
									</div>
									
								</div>
								
								
								
								
								</div>
							  <div  ng-show="AddedDetrashing.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.costPerTon.$invalid && (DetrashingForm.costPerTon.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="costPerTon">Cost Per Ton</label>
							<input type="number" placeholder="Cost Per Ton" ng-model="AddedDetrashing.costPerTon" class="form-control" name="costPerTon" tabindex="9" ng-keyup="getContractTotalCost(AddedDetrashing.costPerTon,AddedDetrashing.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="costPerTon" with-floating-label />
							</div>
							
							<p ng-show="DetrashingForm.costPerTon.$error.pattern && (DetrashingForm.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : DetrashingForm.totalTons.$invalid && (DetrashingForm.totalTons.$dirty || submitted)}">				
							<div class="fg-line">
							<label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons" ng-model="AddedDetrashing.totalTons" class="form-control" name="totalTons" tabindex="10" ng-keyup="getContractTotalCost(AddedDetrashing.costPerTon,AddedDetrashing.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" id="totalTons" with-floating-label   />
							</div>
							<p ng-show="DetrashingForm.totalTons.$error.pattern && (DetrashingForm.totalTons.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							
							
							</div>
							</div>
									<div class="col-sm-3">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalContractAmt"></label>
														<input type="text" class="form-control" placeholder="Grand Total" ng-model="AddedDetrashing.totalContractAmt" tabindex="12" readonly id="totalContractAmt" with-floating-label />
												</div>
												</div>
												</div>
							</div>
								</div>
								
							
								
								</div>	
							  					 					 	
							  <div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Batch Series</th>
							                <th>Variety</th>
		                    				<th>Supplier</th>
											<th>Land Village</th>
											<th>Weight in tons with trash</th>
											<th>Weight in tons without trash</th>
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data">
											<td>
											
											
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											
											<td style="display:none;">
											<input type="hidden" ng-model="batchData.id" name="batchid{{batchData.id}}" />
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.batchSeries{{batchData.id}}.$invalid && (DetrashingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
											<input list="stateList" placeholder="Batch Series" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{batchData.id}}" placeholder ="Batch No" ng-model="batchData.batchSeries" ng-change="loadBatchDetails(batchData.batchSeries,batchData.id);">
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
       						    </select>    
   									</datalist>

<p ng-show="DetrashingForm.batchSeries{{batchData.id}}.$error.required && (DetrashingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.batchSeries{{batchData.id}}.$error.pattern && (DetrashingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
										
</div>	


						</td>
						<td style='display:none;'>
						
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.variety{{batchData.id}}.$invalid && (DetrashingForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="variety{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.variety' name="variety" readonly>
													
<p ng-show="DetrashingForm.variety{{batchData.id}}.$error.required && (DetrashingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.variety{{batchData.id}}.$error.pattern && (DetrashingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.seedSupr{{batchData.id}}.$invalid && (DetrashingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="seedSupr{{batchData.id}}" ng-model="batchData.seedSupr"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="DetrashingForm.seedSupr{{batchData.id}}.$error.required && (DetrashingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.seedSupr{{batchData.id}}.$error.pattern && (DetrashingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.landVillageCode{{batchData.id}}.$invalid && (DetrashingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="landVillageCode{{batchData.id}}" placeholder="Land Village" data-ng-model='batchData.landVillageCode' name="landVillageCode" readonly  />
<p ng-show="DetrashingForm.landVillageCode{{batchData.id}}.$error.required && (DetrashingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.landVillageCode{{batchData.id}}.$error.pattern && (DetrashingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>

											<td>
						
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.varietyName{{batchData.id}}.$invalid && (DetrashingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="varietyName{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.varietyName' name="varietyName" readonly>
													
<p ng-show="DetrashingForm.varietyName{{batchData.id}}.$error.required && (DetrashingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.varietyName{{batchData.id}}.$error.pattern && (DetrashingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.ryotName{{batchData.id}}.$invalid && (DetrashingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="ryotName{{batchData.id}}" ng-model="batchData.ryotName"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="DetrashingForm.ryotName{{batchData.id}}.$error.required && (DetrashingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.ryotName{{batchData.id}}.$error.pattern && (DetrashingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.lvName{{batchData.id}}.$invalid && (DetrashingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="LVName{{batchData.id}}" placeholder="Land Village" data-ng-model='batchData.lvName' name="lvName" readonly  />
<p ng-show="DetrashingForm.lvName{{batchData.id}}.$error.required && (DetrashingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.LVName{{batchData.id}}.$error.pattern && (DetrashingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.wtWithTrash{{batchData.id}}.$invalid && (DetrashingForm.wtWithTrash{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Weight in tons with trash." maxlength='10' name="wtWithTrash{{batchData.id}}" ng-model="batchData.wtWithTrash" data-ng-required='true'  ng-keyup="deductTrash(batchData.id,batchData.wtWithTrash);" ata-ng-pattern="/^[0-9.\s]*$/"/>
<p ng-show="DetrashingForm.wtWithTrash{{batchData.id}}.$error.required && (DetrashingForm.wtWithTrash{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.wtWithTrash{{batchData.id}}.$error.pattern && (DetrashingForm.wtWithTrash{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : DetrashingForm.wtWithoutTrash{{batchData.id}}.$invalid && (DetrashingForm.wtWithoutTrash{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Weight in tons witout trash" maxlength='10' name="wtWithoutTrash{{batchData.id}}" ng-model="batchData.wtWithoutTrash" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getotalCost(batchData.id,batchData.wtWithoutTrash);"/>
<p ng-show="DetrashingForm.wtWithoutTrash{{batchData.id}}.$error.required && (DetrashingForm.wtWithoutTrash{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="DetrashingForm.wtWithoutTrash{{batchData.id}}.$error.pattern && (DetrashingForm.wtWithoutTrash{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											
											</tr>
											<tr>
											
												<td colspan="6"></td><td align="right"><input type="text" class="form-control1" style="width:200px;" ng-model="AddedDetrashing.totalCaneWt" name="totalCaneWt" placeholder="Total Cane Weight in Tons" readonly /></td>
												
												
											</tr>
											
											
											
									</tbody>
							 	</table>
							</div>
							  <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(DetrashingForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
