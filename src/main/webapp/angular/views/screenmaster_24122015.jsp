	<%@ include file="DisableCache.jsp" %>
	<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
	<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
	<%@ taglib uri="/components.hms.com" prefix="hc" %>
	<%@ page import="com.hms.vo.*, java.util.Map"%>	
<%
	ApplicationSession applSession=null;
	String userName="";
	//String storeNameId="";
	if(session.getAttribute("ApplicationSession")!=null)
	{
		applSession = (ApplicationSession)session.getAttribute("ApplicationSession");
		userName = applSession.getUserName();
		//storeNameId=applSession.getStoreNameID();
	}
	else
	{
		System.out.println("Application session is null");
	}
%>	

	<style type="text/css">
		.btnok
		{
			width:50px;
			height:20px;
			color:#FFFFFF;
			font-weight:bold;
			border:none;
			background-color:#2196F3;
		}
	</style>		
	<script type="text/javascript">		
	  /*-----get Values from session------*/
		var LoginUser = '<%=userName %>';
	  /*-----get Values from session end------*/
	  /*-----Validations added by Suresh on 24-12-2015------*/	
		$(document).ready(function()
		{		
			var asd='1';
			//var name[name] = ['Jani','Hege','Kai'];
			//var country[country] =  ['India','Pakistan','Amp']; 
			var data1 =new Array();
			var data =new Array();
			var name = new Array();
			var country = new Array();
			
			name['name'] = ['Jani','Hege','Kai'];
			country['country'] =  ['India','Pakistan','Amp']; 
			
			for(var i=0;i<3;i++)
			{
				data1 += '{name:'+"'"+name['name'][i]+"'"+","+'country:'+"'"+country['country'][i]+"'}"+",";
				//alert(data1);
			}
			var data ="["+data1+"]"; 
			alert(data);
				var data = [{name:'Jani',country:'Norway'},{name:'Hege',country:'Sweden'},{name:'Kai',country:'Denmark'}];
				//alert(data);
				sessionStorage.setItem('data1',JSON.stringify(data));

			//swal("Good job!", "Screen Added Successfully", "success");
			$('.validate').keyup(function()
			{
				var textvalue = $(this).val();
			    var regex = /[^a-z A-Z]/;
			    if(regex.test(textvalue)) { $(this).val(textvalue.replace(regex, '')); }
			});
		});
		
		$(document).ready(function()
		{			
			$('.checkbox').click(function()
			{
				if($('input[name=posbltychk]:checked').length==0) 
				{ 
					$('.validate_checkbox').addClass('has-warning'); 
					$('#Possibiliteserr').show();
				} 
			   else 
			    { 
					$('.validate_checkbox').removeClass('has-warning'); 
					$('#Possibiliteserr').hide();
				}
			});
		});		
		/*------Success alert------*/
		//swal("Good job!", "Screen Added Successfully", "success");
		//$('.confirm').click(function()
		//{
			//location.reload();
		//});
		/*-----------form submit function---------*/
		var Updateflag = 'false';		
		function formSubmit()
		{
			var checked_status="";
			var checked_status_all = "";
          	if($('input[name=posbltychk]:checked').length==0) 
			{ 
				$('.validate_checkbox').addClass('has-warning'); 
				$('#Possibiliteserr').show();
			} 
		   else 
		    { 
				$('.validate_checkbox').removeClass('has-warning'); 
				$('#Possibiliteserr').hide();
			}
			
			var input_screenname = $('#input_screenname').val();		
			var input_description = $('#input_description').val(); 
			
			$('input:checkbox.checkbox').each(function () 
			{
		       var sThisVal = (this.checked ? $(this).val() : "off");
			   if(sThisVal=='on') { checked_status = 'Yes'; } else { checked_status = 'No'; }
			   checked_status_all += checked_status+",";
			});			
			
			var check_status_split= checked_status_all.split(",");
			
			alert(document.forms[0]["SCREENS:SCREEN_NAME"].value);
			document.forms[0]["SCREENS:SCREEN_NAME"].value = input_screenname;	
			document.forms[0]["SCREENS:DESCRIPTION"].value = input_description;		
			document.forms[0]["SCREENS:ADD_STATUS"].value = check_status_split[0];
			document.forms[0]["SCREENS:MODIFY_STATUS"].value = check_status_split[1];
			document.forms[0]["SCREENS:VIEW_STATUS"].value = check_status_split[2];
			document.forms[0]["SCREENS:DELETE_STATUS"].value = check_status_split[3];
			document.forms[0]["SCREENS:FLAG"].value =Updateflag;
			document.forms[0]["SCREENS:TABLENAME"].value = "hms_admin_screens";
			document.forms[0]["SCREENS:ADMINTYPE"].value = "Screens Master";
			document.forms[0]["SCREENS:PageName"].value = "ScreensMaster";
			
alert(document.forms[0]["SCREENS:SCREEN_NAME"].value);
alert(document.forms[0]["SCREENS:ADD_STATUS"].value);
alert(document.forms[0]["SCREENS:MODIFY_STATUS"].value);
alert(document.forms[0]["SCREENS:VIEW_STATUS"].value);
alert(document.forms[0]["SCREENS:DELETE_STATUS"].value);

			swal("Good job!", "Screen Added Successfully", "success");
			return true;
		}
		function editsuccess()
		{
			alert(1);
		}
	</script>
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.html'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">  
        	<div class="container" data-ng-controller="tableCtrl as tctrl">
    			<div class="block-header"><h2>Screen Master</h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <f:view> 
					 <hc:hmsForm id="SCREENS">
					 <!-------body start------>
					    <br />
            			<div class="row">
			                <div class="col-sm-12">
			                    <div class="input-group">
								       									
            			            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        	<div class="fg-line nulvalidate">
											<label class="control-label has-warning" for="inputWarning1" style="display:none;" id="screenname">Screen Name is Required</label>
            				          		<input type="text" class="form-control validate" placeholder="Screen Name" autofocus onkeypress="" onblur="if(this.value=='') { $('.nulvalidate').addClass('has-warning'); $('#screenname').show(); } else { $('#screenname').hide(); $('.nulvalidate').removeClass('has-warning'); }" id="input_screenname">									  
			                        	</div>										
			                    </div>			                    
			                  </div>
						 </div>
						 <br />
						 <div class="row"> 
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        <div class="fg-line">
            				          <input type="text" class="form-control" placeholder="Description(Optional)" id="input_description">
			                        </div>
			                    </div>			                    
			                </div>
            			 </div>
						 <br />
						 <div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">									
									<div class="checkbox">	
										<p class="validate_checkbox"><label class="control-label" id="Possibiliteserr" style="display:none;">Possibilities are Required</label></p>																																			
										<div class="fg-line">
											<label class="checkbox checkbox-inline m-r-20"><b>Possibilities :</b></label>
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="posbltychk"><i class="input-helper"></i>Add
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="posbltychk"><i class="input-helper"></i>Modify
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox"name="posbltychk"><i class="input-helper"></i>View
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="posbltychk"><i class="input-helper"></i>Delete
								            </label>																					
										</div>
                			       	</div>
								</div>			                    
			                </div>
						 </div>
						 <br />
						 <div class="row">            			
							<div class="input-group">
								<div class="fg-line">
									<a href="#" class="btn btn-primary btn-sm m-t-10">
										<h:commandButton type="submit" value='Add' actionListener="#{formBean.businessActionHandler.processAction}" action="#{formBean.doAction}" onclick="return formSubmit()" styleClass='btnok' id="HMSAdminProcessUpdate"/>
									</a>
								</div>
							</div>						 	
						 </div>
			        </div>
					<!------Form End--->
<hc:hmsJavaScriptComponent id="javascript"/>    
<h:inputHidden id="SCREEN_NAME" value="#{formBean.fieldsMap.SCREEN_NAME}"/>
<h:inputHidden id="DESCRIPTION" value="#{formBean.fieldsMap.DESCRIPTION}"/>
<h:inputHidden id="TABLENAME" value="#{formBean.fieldsMap.TABLENAME}"/>
<h:inputHidden id="ADMINTYPE" value="#{formBean.fieldsMap.ADMINTYPE}"/>
<h:inputHidden id="PageName" value="#{formBean.fieldsMap.PageName}" />
<h:inputHidden id="FLAG" value="#{formBean.fieldsMap.FLAG}" />
<h:inputHidden id="ID" value="#{formBean.fieldsMap.ID}" />
<h:inputHidden id="ADD_STATUS" value="#{formBean.fieldsMap.ADD_STATUS}" />
<h:inputHidden id="MODIFY_STATUS" value="#{formBean.fieldsMap.MODIFY_STATUS}" />
<h:inputHidden id="VIEW_STATUS" value="#{formBean.fieldsMap.VIEW_STATUS}" />
<h:inputHidden id="DELETE_STATUS" value="#{formBean.fieldsMap.DELETE_STATUS}" />					
					
					</hc:hmsForm>
					</f:view> 
					<!------Form End--->
			    </div>
				<!----------Table Design-------->
					 		
						<div class="card">
					        <div class="card-body card-padding">
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Screen Name'">
                		    					<span ng-if="!w.$edit">{{w.name}}</span>
					    		                <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.name" /></div>
					            		      </td>
		                    				  <td data-title="'Description'">
							                     <span ng-if="!w.$edit">{{w.country}}</span>
							                     <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.country" /></div>
							                  </td>
							                  <td data-title="'Add'">
							                      <span ng-if="!w.$edit">Yes</span>
        		            					  <div ng-if="w.$edit">
												  	<label class="checkbox" style="margin-top:-20px;">
													  	<input type="checkbox" value="option1" class="checkbox"><i class="input-helper"></i>
													</label>
												  </div>
							                  </td>
							                  <td data-title="'Modify'">
                    							   <span ng-if="!w.$edit">Yes</span>
					            		           <div ng-if="w.$edit">
													  	<label class="checkbox" style="margin-top:-20px;">
														  	<input type="checkbox" value="option1"><i class="input-helper"></i>
														</label>
													</div>
							                  </td>
					    		              <td data-title="'View'">
                    							   <span ng-if="!w.$edit">Yes</span>
					                    		   <div ng-if="w.$edit">
											  			<label class="checkbox" style="margin-top:-20px;">
														  	<input type="checkbox" value="option1" checked="checked"><i class="input-helper"></i>
														</label>
												    </div>
							                  </td>					
							                  <td data-title="'Edit'">
                    							    <span ng-if="!w.$edit">Yes</span>
					            		            <div ng-if="w.$edit">
													  	<label class="checkbox" style="margin-top:-20px;" >
														  	<input type="checkbox" value="option1"><i class="input-helper"></i>
														</label>
													</div>
					            		      </td>			
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>				
					
					
				
				<!----------Table Design-------->
			</div>
	    </section>  
	</section> 

	<footer id="footer" data-ng-include="'template/footer.html'"></footer>
