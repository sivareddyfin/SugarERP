	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	<script>
	  $(function() 
	  {
	     $( ".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy',
			  yearRange: "-100:+0"
	     });
	  });
    </script>


	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="LoginUserMaster" ng-init="loadLoginUserId();loaddepartmentNames();loadDesignationNames();loadUserNames();loadRolesNames();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Login User Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="LoginUserMasterForm" ng-submit="LoginUserSubmit(AddedLoginUser,LoginUserMasterForm);" novalidate id='loginUser'>	
					  		<input type="hidden" name="screenName" ng-model="AddedLoginUser.screenName" />				
    	        			<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100 select2"  name="employeeID" ng-change="loadUsersOnChange(AddedLoginUser.employeeID);" data-ng-model="AddedLoginUser.employeeID" ng-options="employeeID.id as employeeID.employeename for employeeID in employeeNames | orderBy:'-employeename':true">
												<option value="">Added Users</option>
						                        
				        		            </select>
                	            		  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>								
    	        			 </div><br />  
							<div class="row">
				                  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								           <div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.employeeName.$invalid && (LoginUserMasterForm.employeeName.$dirty || submitted)}">
			        	                <div class="fg-line">
										<label for="loginusername">Login User Name</label>
            															<input type="text" class="form-control autofocus" placeholder='Login User Name' autofocus name="employeeName"  maxlength="30" data-ng-model="AddedLoginUser.employeeName" tabindex="1" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" id="loginusername" with-floating-label  ng-blur="spacebtw('employeeName');" />
			                	        	</div>
											<p data-ng-show="LoginUserMasterForm.employeeName.$error.pattern  && (LoginUserMasterForm.employeeName.$dirty || submitted)" class="help-block">Enter valid Login User Name</p>					
			
						<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedLoginUser.employeeName!=null">Login User  Name Already Exist</p>			
			                    	</div>
									</div>			                    
				                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.designationCode.$invalid && (LoginUserMasterForm.designationCode.$dirty || submitted)}">			
			        	                <div class="fg-line">
            					          <select chosen class="w-100" name="designationCode"  data-ng-required='true' data-ng-model="AddedLoginUser.designationCode" ng-options="designationCode.id as designationCode.designation for designationCode in designationNames | orderBy:'-designation':true" tabindex="2">
												<option value="">Select Designation</option>
											</select>
			                	        </div>
										<p ng-show="LoginUserMasterForm.designationCode.$error.required && (LoginUserMasterForm.designationCode.$dirty || submitted)" class="help-block">Select Designation</p>																									
										</div>
			                    	</div>			                    
				                  </div>	
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.deptCode.$invalid && (LoginUserMasterForm.deptCode.$dirty || submitted)}">			
										<div class="fg-line">
                				    		<select chosen class="w-100" name="deptCode"  data-ng-required='true' data-ng-model="AddedLoginUser.deptCode" ng-options="deptCode.id as deptCode.department for deptCode in departmentNames  | orderBy:'-department':true" tabindex="3">
												<option value="">Select Department</option>
											</select>
			                	        </div>
										<p ng-show="LoginUserMasterForm.deptCode.$error.required && (LoginUserMasterForm.deptCode.$dirty || submitted)" class="help-block">Select Department</p>																									
										</div>
			                    	</div>			                    
				                  </div>							
    	        			 </div>
							<div class="row">
				                  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										 	<div  class="form-group floating-label-wrapper" ng-class="{ 'has-error' : LoginUserMasterForm.loginID.$invalid && (LoginUserMasterForm.loginID.$dirty || submitted)}">
			        	                		<div class="fg-line">
												<label for="loginid">Login ID</label>
											<input type="hidden"   name="employeeID"  ng-value="" data-ng-model="AddedLoginUser.employeeID"/>
            					          <input type="text" class="form-control" placeholder='Login ID' name="loginID" data-ng-model="AddedLoginUser.loginID" maxlength="20" tabindex="4" data-ng-required='true' id="loginid" with-floating-label  ng-blur="duplicateId(AddedLoginUser.loginID);spacebtw('loginID');" />
										             </div>
													 <p ng-show="LoginUserMasterForm.loginID.$error.required && (LoginUserMasterForm.loginID.$dirty || submitted)" class="help-block">Login id is required</p>									
													 <p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedLoginUser.loginID!=null">Login Id Already Exist.</p>																	
												 </div>
											</div>			                    
				                 		</div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.dateOfBirth.$invalid && (LoginUserMasterForm.dateOfBirth.$dirty || submitted)}">
			        	                <div class="fg-line">
											<label for="dateofbirth">Date of Birth</label>
            					         							<input type="text" class="form-control datepicker" placeholder='Date of Birth' name="dateOfBirth" data-ng-model="AddedLoginUser.dateOfBirth" tabindex="5" ng-required="true" data-input-mask="{mask: '00-00-0000'}"  id="dateofbirth" with-floating-label  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"/>
			                	        </div>
										  <p data-ng-show="LoginUserMasterForm.dateOfBirth.$error.required && (LoginUserMasterForm.dateOfBirth.$dirty || submitted)" class="help-block">Date of Birth is required</p>	
										  <p data-ng-show="LoginUserMasterForm.dateOfBirth.$error.pattern && (LoginUserMasterForm.dateOfBirth.$dirty || submitted)" class="help-block">Enter Valid Date of Birth</p>										  
										
										</div>
			                    	</div>			                    
				                  </div>	
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										 	<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.emailID.$invalid && (LoginUserMasterForm.emailID.$dirty || submitted)}">
			        	               			 <div class="fg-line">
												 <label for="email">E-mail</label>
											<input type="email" class="form-control" placeholder='E-mail' name="emailID" maxlength="25" data-ng-model="AddedLoginUser.emailID" tabindex="6" data-ng-required='true' id="email" with-floating-label  />
			                	       		 </div>
											<p data-ng-show="LoginUserMasterForm.emailID.$error.required && (LoginUserMasterForm.emailID.$dirty || submitted)" class="help-block">E-mail is required</p>
<p data-ng-show="LoginUserMasterForm.emailID.$error.email && (LoginUserMasterForm.emailID.$dirty || submitted)" class="help-block">Invalid E-mail</p>												
														 
										</div>
			                    	</div>			                    
				                  </div>							
    	        			 </div>  
							<div class="row">
				                  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.gender.$invalid && (LoginUserMasterForm.gender.$dirty || submitted)}">			
			        	                <div class="fg-line">
            					          <select chosen class="w-100" name="gender" data-ng-required='true' data-ng-model="AddedLoginUser.gender" tabindex="7" ng-options="Gender.id as Gender.gender for Gender in genderData  | orderBy:'-gender':true">									
<option value="">Select Gender</option>

										  </select>
										  
			                	        </div>
										<p data-ng-show="LoginUserMasterForm.gender.$error.required && (LoginUserMasterForm.gender.$dirty || submitted)" class="help-block">Select Gender</p>	
										</div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.maritalStatus.$invalid && (LoginUserMasterForm.maritalStatus.$dirty || submitted)}">	
			        	                <div class="fg-line">
            			
						<select chosen class="w-100"  data-ng-required='true' data-ng-model='AddedLoginUser.maritalStatus' name="maritalStatus" ng-options="maritalStatus.maritalStatusId as maritalStatus.maritalStatus for maritalStatus in maritalStatus  | orderBy:'-marialStatus':true" tabindex="8">
															<option value="">Select Marital Status</option>
				        					            </select> 
														
						
								       
			                	        </div>
										<p data-ng-show="LoginUserMasterForm.maritalStatus.$error.required && (LoginUserMasterForm.maritalStatus.$dirty || submitted)" class="help-block">Select Marital Status</p>	
										</div>
			                    	</div>			                    
				                  </div>	
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.phoneNo.$invalid && (LoginUserMasterForm.phoneNo.$dirty || submitted)}">
			        	                <div class="fg-line">
										<label for="phonenumber">Phone Number</label>
											<input type="text" class="form-control" placeholder='Phone Number(Optional)' name="phoneNo" data-ng-model="AddedLoginUser.phoneNo" maxlength="12"  data-ng-minlength="10" data-ng-pattern="/^[0-9\s]*$/" tabindex="9" id="phonenumber" with-floating-label  ng-blur="spacebtw('phoneNo');" />
			                	        </div>
										<!--<p data-ng-show="LoginUserMasterForm.phoneNo.$error.required && (LoginUserMasterForm.phoneNo.$dirty || submitted)" class="help-block">Phone Number(H) is required</p>-->
										<p data-ng-show="LoginUserMasterForm.phoneNo.$error.pattern && (LoginUserMasterForm.phoneNo.$dirty || submitted)" class="help-block">Enter valid Phone Number(H)</p>	
										<span ng-show="LoginUserMasterForm.phoneNo.$error.minlength " class="help-block" ng-if="!LoginUserMasterForm.phoneNo.$error.pattern" >Phone number is too short	</span>	
												 
										</div>
									</div>			                    
				                  </div>							
    	        			 </div>   
							<div class="row">
										  <div class="col-sm-4">
							<div class="input-group">
									 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
		<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.mobileNo.$invalid && (LoginUserMasterForm.mobileNo.$dirty || submitted)}">
									<div class="fg-line">
		<label for="mobilenumber">Mobile Number</label>	
		<input type="text" class="form-control" placeholder='Mobile Number' maxlength="11" name="mobileNo" data-ng-model="AddedLoginUser.mobileNo"  data-ng-required='true'  data-ng-minlength="10"  data-ng-pattern="/^[7890]/"  tabindex="10" id="mobilenumber" with-floating-label  ng-blur="spacebtw('mobileNo');" />
										 </div>
		
		<p data-ng-show="LoginUserMasterForm.mobileNo.$error.pattern && (LoginUserMasterForm.mobileNo.$dirty || submitted)" class="help-block"  >Enter Valid Mobile Number starting with 7,8,9 series</p>	
		<span ng-show="LoginUserMasterForm.mobileNo.$error.minlength " class="help-block" ng-if="!LoginUserMasterForm.mobileNo.$error.pattern" >Mobile number is too short	</span>	
		 
		
		
								</div>
</div>	                     
                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : LoginUserMasterForm.phoneNo2.$invalid && (LoginUserMasterForm.phoneNo2.$dirty || submitted)}">
			        	                <div class="fg-line">
										<label for="additionalcontacts">Additional Contacts</label>
														<input type="text" class="form-control" placeholder='Additional Contact Number(Optional)' maxlength="10"  name="phoneNo2" data-ng-model="AddedLoginUser.phoneNo2" tabindex="11"  data-ng-minlength="10" data-ng-pattern="/^[0-9\s]*$/"  id="additionalcontacts" with-floating-label  ng-blur="spacebtw('phoneNo2');" />
			                	       		 </div>										
							<p data-ng-show="LoginUserMasterForm.phoneNo2.$error.pattern && (LoginUserMasterForm.phoneNo2.$dirty || submitted)" class="help-block">Enter valid Additional Contacts</p>	
							<p data-ng-show="LoginUserMasterForm.phoneNo2.$error.minlength && (LoginUserMasterForm.phoneNo2.$dirty || submitted)" class="help-block"> Additional Contacts are Too Short</p>			
										</div>
			                    	</div>			                    
				                  </div>	
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
										<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.roleID.$invalid && (LoginUserMasterForm.roleID.$dirty || submitted)}">			
										<div class="fg-line">
                				    		<select chosen class="w-100" name="roleID"  data-ng-required='true' data-ng-model="AddedLoginUser.roleID"  ng-options="roleID.id as roleID.role for roleID in RoleNames  | orderBy:'-role':true" tabindex="12">
												<option value="">Select Role</option>
											</select>
			                	        </div>
										<p ng-show="LoginUserMasterForm.roleID.$error.required && (LoginUserMasterForm.roleID.$dirty || submitted)" class="help-block">Select Role</p>																									
										</div>
										
										<!--<div  class="form-group"  ng-class="{ 'has-error' : LoginUserMasterForm.roleid.$invalid && (LoginUserMasterForm.roleid.$dirty || submitted)}">
			        	                <div class="fg-line">
										<div class="select">
											<select chosen class="w-100"  name="roleid" tabindex="1" data-ng-model="AddedLoginUser.roleid"  ng-options="roleid.id as roleid.role for roleid in RoleNames | orderBy:'-role':true"  />
												<option value="">Select Role</option>
												</select>
						                       </div> 
											   </div>
											  
						<p data-ng-show="LoginUserMasterForm.roleid.$error.required && (LoginUserMasterForm.roleid.$dirty || submitted)" class="help-block">Select Role</p>
				        		            
											</div>-->
								         </div>
			                    	</div>			                    
				                  							
    	        			 </div> 
							<div class="row">
							<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : LoginUserMasterForm.empStatus.$invalid && (LoginUserMasterForm.empStatus.$dirty || submitted)}">	
			        	                <div class="fg-line">
            			
						<select chosen class="w-100"  data-ng-required='true' data-ng-model='AddedLoginUser.empStatus' name="empStatus" ng-options="empStatus.empStatusId as empStatus.empStatus for empStatus in empStatus  | orderBy:'empStatus':true" tabindex="8">
															<option value="">Select Authentication Status</option>
				        					            </select> 
														
						
								       
			                	        </div>
										<p data-ng-show="LoginUserMasterForm.empStatus.$error.required && (LoginUserMasterForm.empStatus.$dirty || submitted)" class="help-block">Select Authentication Status</p>	
										</div>
			                    	</div>			                    
				                  </div>

								<div class="col-sm-4">
				                    <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" data-ng-model="AddedLoginUser.status" tabindex="13">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1" data-ng-model="AddedLoginUser.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>
								   
								  <div class="row" align="center">
								  <div class="col-sm-12">
								  	<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(LoginUserMasterForm,AddedLoginUser.employeeID)" ng-if="AddedLoginUser.employeeID==null">Reset</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(LoginUserMasterForm,AddedLoginUser.employeeID)" ng-if="AddedLoginUser.employeeID!=null">Reset</button>
									</div>
								</div>
								  </div>
								  </div>								  
							</div>
							</form>     
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
