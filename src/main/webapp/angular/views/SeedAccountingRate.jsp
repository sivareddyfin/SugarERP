	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
</style>

	<script type="text/javascript">	
		$('#autofocus').focus();	
		var data = [{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'}];
		sessionStorage.setItem('data1',JSON.stringify(data));		
	</script>

	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seed Accounting Rate Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 		<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='SB A/c' maxlength="10" id="autofocus" />
			                	        </div>
			                    	</div>			                    
				                  </div>
    	        			 </div><br />
							<div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Loan Recovery' maxlength="10" />
			                	        </div>
			                    	</div>			                    
				                  </div>
    	        			 </div><br /> 
							<div class="row">
				                <div class="col-sm-12" align="center">
									<a href="#/admin/admindata/masterdata/SeedAccountingRate" class="btn btn-primary btn-sm m-t-10">Add</a>
									<a href="#/admin/admindata/masterdata/SeedAccountingRate" class="btn btn-primary btn-sm m-t-10">Reset</a>
				                </div>
    	        			 </div>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
														
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
