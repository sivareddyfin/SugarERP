		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	

	<!--<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>-->
	<section id="main" class='bannerok'>    
<!--	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>-->

    	<section id="content" data-ng-controller="productMasterController" ng-init="loadproducts();loaduom();loadStock();onloadFunction();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Product Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
					 
					 <div class="row">
							<div class="col-sm-6">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100" autofocus  data-ng-model='AddedProduct.productGroup' name="productGroup"   ng-options="productGroup.GroupCode as productGroup.Group for productGroup in addedProductCodes" ng-change="loadSubGroup(AddedProduct.productGroup);">
															<option value="">Added Product Group</option>
						        			            </select>
		                	            			  </div>
					<!--<p ng-show="productForm.uom.$error.required && (productForm.uom.$dirty || Addsubmitted)" class="help-block">Select UOM</p>-->		 
											  </div>
			                    	      </div>
										</div>
							<div class="col-sm-6">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"    data-ng-model='AddedProduct.productSubGroup' name="productSubGroup" ng-options="productSubGroup.subGroupCode as productSubGroup.subGroup for productSubGroup in addedProducsubGroupCodes"  ng-change="loadproductCode(AddedProduct.productSubGroup);">
															<option value="">Product Subgroup</option>
						        			            </select>
		                	            			  </div>
					
											  </div>
			                    	      </div>
										</div>			
						</div>
						<br />
 					 	<form name="productForm"  novalidate ng-submit="productSubmit(AddedProduct,productForm);">
						<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productForm.productCode.$invalid && (productForm.productCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="productCode">Product Code</label>
    	        								         <input type="text" class="form-control " placeholder="Product Code" readonly    maxlength="25" tabindex="1" name="productCode" data-ng-model='AddedProduct.productCode' data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" id="productCode" with-floating-label  ng-blur="spacebtw('productCode');" />														 
				                				    </div>
						<p ng-show="productForm.productCode.$error.required && (productForm.productCode.$dirty || Addsubmitted)" class="help-block">Product Code is required</p>
						<p ng-show="productForm.productCode.$error.pattern  && (productForm.productCode.$dirty || Addsubmitted)" class="help-block">Enter Valid Product Code</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productForm.stockMethod.$invalid && (productForm.stockMethod.$dirty || Addsubmitted)}">										
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100" data-ng-required='true'  tabindex="7"  data-ng-model='AddedProduct.stockMethod' name="stockMethod" ng-options="stockMethod.methodCode as stockMethod.method for stockMethod in stockMethodCodes">
															<option value="">Stock Method Code</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="productForm.stockMethod.$error.required && (productForm.stockMethod.$dirty || Addsubmitted)" class="help-block">Select StockMethod</p> 
											  </div>
			                    	      </div>
									</div>
									</div>
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productForm.productName.$invalid && (productForm.productName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="productName">Product Name</label>
    	        								         <input type="text" class="form-control" placeholder="Product Name"  maxlength="25" tabindex="2" name="productName" data-ng-model='AddedProduct.productName' id="productName" with-floating-label   ng-blur="spacebtw('productName');" ng-required='true' />														 
				                				    </div>
								<p ng-show="productForm.productName.$error.required && (productForm.productName.$dirty || Addsubmitted)" class="help-block">ProductName is required</p>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productForm.description.$invalid && (productForm.description.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="description">Description</label>
									<input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="22" name="description" data-ng-model='AddedProduct.description'    id="description" with-floating-label   ng-blur="spacebtw('serviceTaxRegnNo');"   />	
													
    	            						    		
		                	            			  </div>
					
					<p ng-show="productForm.description.$error.pattern && (productForm.description.$dirty || Addsubmitted)" class="help-block">Valid description is required</p>	
								 		 
											  </div>
			                    	      </div>
										</div>
									</div>
									
									
									
								</div>
								<div class="col-sm-4">
								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : productForm.uom.$invalid && (productForm.uom.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="7" data-ng-required='true' data-ng-model='AddedProduct.uom' name="uom" ng-options="uom.UomCode as uom.Uom for uom in uoms">
															<option value="">UOM</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="productForm.uom.$error.required && (productForm.uom.$dirty || Addsubmitted)" class="help-block">Select UOM</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
			                        <br />
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedProduct.status"  >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="AddedProduct.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>
							
							
							
							
</div>
								</div>
							</div>	
							
								
														 				
												
					
					
					
					
					
						
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="reset" class="btn btn-primary" ng-click='reset(productForm);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
				
				</form>								 
				   			
				        </div>
			    	</div>
							</div>
					
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
