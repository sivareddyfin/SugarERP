	<script type="text/javascript">
		$('.autofocus').focus();		
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="DepartmentMaster" ng-init="loadDepartments();getAllDepartments();">    
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Department Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
						<form name="DepartmentForm" data-ng-submit="AddDepartments(Departments,DepartmentForm);" novalidate>
						<input type="hidden" name="screenName" ng-model="Departments.screenName" />
					<!--------Form Start----->
    	        		<div class="row">
				          <div class="col-sm-6">
						  	<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  		<div class="form-group floating-label-wrapper">
				        	            			<div class="fg-line">
														<label for="departmentcode">Department Code</label>
    	        					      <input type="text" class="form-control" placeholder="Department Code" data-ng-model='Departments.deptCode' maxlength="25" name="deptCode" readonly id="departmentcode" with-floating-label />
				                	   		 </div>
										</div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div class="form-group floating-label-wrapper">
										<!--<div  class="form-group"  ng-class="{ 'has-error' : DepartmentForm.description.$invalid && (DepartmentForm.description.$dirty || Addsubmitted)}">-->	
				        	            <div class="fg-line">
										<label for="description">Description</label>
    	        					                    <input type="text" class="form-control" placeholder="Description" data-ng-model='Departments.description' maxlength="50"  name="description" tabindex="2"  id="description" with-floating-label ng-blur="spacebtw('description');" />
				                	    </div>
										 <!--<p data-ng-show="DepartmentForm.description.$error.required && (DepartmentForm.description.$dirty || Addsubmitted)" class="help-block">Description is required</p>-->
										<!--</div>-->
										</div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group floating-label-wrapper">
                  <!--<div  class="form-group"  ng-class="{ 'has-error' : DepartmentForm.location.$invalid && (DepartmentForm.location.$dirty || Addsubmitted)}">-->																					
				        	            <div class="fg-line">
										<label for="deptlocation">Dept.Location</label>
    	        	<input type="text" class="form-control" placeholder="Dept.Location" data-ng-model='Departments.location' maxlength="20" name="location"  tabindex="4" id="deptlocation" with-floating-label ng-blur="spacebtw('location');" />
				                	      </div>
							
			 <!--<p data-ng-show="DepartmentForm.location.$error.required && (DepartmentForm.location.$dirty || Addsubmitted)" class="help-block">Dept. Location Name is required</p>-->																								
											<!--</div>-->										
										</div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : DepartmentForm.extension.$invalid && (DepartmentForm.extension.$dirty || Addsubmitted)}">																															
				        	            <div class="fg-line">
									<label for="extension">Extension</label>
    	        					      <input type="text" class="form-control" placeholder="Extension" data-ng-model='Departments.extension' maxlength="10" name="extension" data-ng-required='true' tabindex="6" data-ng-pattern="/^[0-9\s]*$/" id="extension" with-floating-label ng-blur="spacebtw('extension');" />
				                	    	</div>
			 <p data-ng-show="DepartmentForm.extension.$error.pattern  && (DepartmentForm.extension.$dirty || Addsubmitted)" class="help-block">Enter a valid Extension</p>					
			 <p data-ng-show="DepartmentForm.extension.$error.required && (DepartmentForm.extension.$dirty || Addsubmitted)" class="help-block">Extension is required</p>																																		
										
											</div>										
										<!--</div>-->
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
				                    <div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" data-ng-model='Departments.status'>
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1" data-ng-model='Departments.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>							 
								</div>
						  </div>
				          <div class="col-sm-6">
						  	<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : DepartmentForm.department.$invalid && (DepartmentForm.department.$dirty || Addsubmitted)}">
				        	            <div class="fg-line">
										<label for="departmentname">Department Name</label>
										
    	        					                					<input type="text" class="form-control autofocus" placeholder="Department Name" data-ng-model='Departments.department' maxlength="25"  name="department" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" autofocus  tabindex="1" id="departmentname" with-floating-label ng-blur="spacebtw('department');validateDup();" />
				                	    </div>
					 <p data-ng-show="DepartmentForm.department.$error.pattern  && (DepartmentForm.department.$dirty || Addsubmitted)" class="help-block">Enter a valid Name.</p>					
					<p data-ng-show="DepartmentForm.department.$error.required && (DepartmentForm.department.$dirty || Addsubmitted)" class="help-block">Department Name is required</p>		
					<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="Departments.department!=null">Department Name Already Exist.</p>												 
									  </div>
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : DepartmentForm.deptShortcut.$invalid && (DepartmentForm.deptShortcut.$dirty || Addsubmitted)}">				
				        	            <div class="fg-line">
										<label for="departmentshortcut">Department Shortcut</label>
										
								 <input type="text" class="form-control" placeholder="Department Shortcut" data-ng-model='Departments.deptShortcut' maxlength="15" name="deptShortcut" data-ng-required='true'  tabindex="3" id="departmentshortcut" with-floating-label ng-blur="spacebtw('deptShortcut');" />
				                	    </div>
							
		<p data-ng-show="DepartmentForm.deptShortcut.$error.required && (DepartmentForm.deptShortcut.$dirty || Addsubmitted)" class="help-block">Dept. ShortCut is required</p>																								
											</div>										
										<!--</div>-->
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
<div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : DepartmentForm.contactPerson.$invalid && (DepartmentForm.contactPerson.$dirty || Addsubmitted)}">													
				        	            <div class="fg-line">
											<label for="contactperson">Contact Person</label>
    	        					      <input type="text" class="form-control" placeholder="Contact Person" data-ng-model='Departments.contactPerson' maxlength="25" name="contactPerson" data-ng-required='true' tabindex="5" data-ng-pattern="/^[a-z A-Z\s]*$/" id="contactperson" with-floating-label  ng-blur="spacebtw('contactPerson');" />
				                	    </div>
		<p data-ng-show="DepartmentForm.contactPerson.$error.pattern  && (DepartmentForm.contactPerson.$dirty || Addsubmitted)" class="help-block">Enter a valid Contact Person</p>					
		<p data-ng-show="DepartmentForm.contactPerson.$error.required && (DepartmentForm.contactPerson.$dirty || Addsubmitted)" class="help-block">Contact Person is required</p>																																		
											</div>										
									  <!--</div>-->
			                    	</div>
								</div>	
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <!--<div class="form-group">-->
          <div  class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : DepartmentForm.directNumber.$invalid && (DepartmentForm.directNumber.$dirty || Addsubmitted)}">																						
				        	            <div class="fg-line">
										<label for="directnumber">Direct No</label>
    	        					      <input type="text" class="form-control" placeholder="Direct No." data-ng-model='Departments.directNumber' maxlength="10" name="directNumber"  tabindex="7" data-ng-pattern="/^[0-9\s]*$/" id="directnumber" with-floating-label ng-blur="spacebtw('directNumber');" />
				                	    </div>
		<p data-ng-show="DepartmentForm.directNumber.$error.pattern  && (DepartmentForm.directNumber.$dirty || Addsubmitted)" class="help-block">Enter a valid Direct No.</p>					
		<!--<p data-ng-show="DepartmentForm.directNumber.$error.required && (DepartmentForm.directNumber.$dirty || Addsubmitted)" class="help-block">Direct No. is required</p>-->																																												
											</div>										
										<!--</div>-->
			                    	</div>
								</div>	
							</div>
							<br /><br /><br />
							<input type="hidden" name="modifyFlag" data-ng-model= "Departments.modifyFlag">
							<div class="row">
							<div class="col-sm-12" align="left">
								<div class="input-group">
									<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-hide">save</button>									
									<button type="reset" class="btn btn-primary" ng-click="reset(DepartmentForm)">Reset</button>									
									</div>
								</div>
							</div>
						</div>
						  </div>						  
						</div>
						
						
															
					 <!-------body start------>					    	        			
						  </form>												 	 							 
				        </div>
			    	</div>
					<!---------Table Design Start--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Departments</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">					
								  <form name="UpdateDepartmentForm" novalidate>
							        <table ng-table="DepartmentMaster.tableEdit" class="table table-striped table-vmiddle">	
									
												<thead>
											        <tr>
											           <th style="width:5%"><span>Action</span></th>
													   <th style="width:10%"><span>Dept. Name</span></th>
													   <th style="width:15%"><span>Description</span></th>
													   <th style="width:15%"><span>Dept. Shortcut</span></th>
													   <th style="width:15%"><span>Dept. Location</span></th>
													   <th style="width:15%"><span>Contact Person</span></th>
													   <th style="width:10%"><span>Extension</span></th>
													   <th style="width:10%"><span>Direct No.</span></th>
													   <th style="width:5%"><span>Status</span></th>
											        </tr>
											      </thead>
											      <tbody>
												  
												  								
								        <tr ng-repeat="Department in DepartmentsData"  ng-class="{ 'active': Department.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Department.$edit" ng-click="Department.$edit = true;Department.modifyFlag='Yes';Department.screenName='Department Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Department.$edit" data-ng-click='updateDepartment(Department,$index);Department.$edit = isEdit;'><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Department.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="Department.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Department.$edit">{{Department.department}}</span>
					    		                <div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.department{{$index}}.$invalid && (UpdateDepartmentForm.department{{$index}}.$dirty || submitted)}">																																		
<input class="form-control" type="text" data-ng-model="Department.department" maxlength="25" placeholder='Department Name' name="department{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/"  ng-blur="spacebtwgrid('department',$index);validateDuplicate(Department.department,$index);"/>
<p data-ng-show="UpdateDepartmentForm.department{{$index}}.$error.pattern  && (UpdateDepartmentForm.department{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>					
<p data-ng-show="UpdateDepartmentForm.department{{$index}}.$error.required && (UpdateDepartmentForm.department{{$index}}.$dirty || submitted)" class="help-block">Required</p>	
<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Department.department!=null">Department Name Already Exist.</p>																																																									
													</div>													
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!Department.$edit">{{Department.description}}</span>
							                     <div ng-if="Department.$edit">
												 <div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.description{{$index}}.$invalid && (UpdateDepartmentForm.description{{$index}}.$dirty || submitted)}">
								 <input class="form-control" type="text" data-ng-model="Department.description" maxlength="15" placeholder='Description' name="description{{$index}}"  ng-blur="spacebtwgrid('description',$index)"/>
								 <!--<p data-ng-show="UpdateDepartmentForm.description{{$index}}.$error.required && (UpdateDepartmentForm.description{{$index}}.$dirty || submitted)" class="help-block">Description is Required</p>-->																																										
								 				</div>
												</div>
							                  </td>
							                  <td>
							                      <span ng-if="!Department.$edit">{{Department.deptShortcut}}</span>
												  <div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.deptShortcut{{$index}}.$invalid && (UpdateDepartmentForm.deptShortcut{{$index}}.$dirty || submitted)}">																																														
												  	<input class="form-control" type="text" data-ng-model="Department.deptShortcut" maxlength="15" placeholder='Dept. Shortcut' name="deptShortcut{{$index}}" data-ng-required='true' ng-blur="spacebtwgrid('deptShortcut',$index)"/>
<!--<p data-ng-show="UpdateDepartmentForm.deptShortcut{{$index}}.$error.pattern  && (UpdateDepartmentForm.deptShortcut{{$index}}.$dirty || submitted)" class="help-block">Enter valid Department Shortcut</p>-->					
<p data-ng-show="UpdateDepartmentForm.deptShortcut{{$index}}.$error.required && (UpdateDepartmentForm.deptShortcut{{$index}}.$dirty || submitted)" class="help-block">Required</p>																																																									
														</div>																										
												   </div>							            							      
											  </td>
							                  <td>
                    							   <span ng-if="!Department.$edit">{{Department.location}}</span>
												   <div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.location{{$index}}.$invalid && (UpdateDepartmentForm.location{{$index}}.$dirty || submitted)}">									   					<input class="form-control" type="text" data-ng-model="Department.location" maxlength="20" placeholder='Dept. Location' name="location{{$index}}"  ng-blur="spacebtwgrid('location',$index)"/>
<!--<p data-ng-show="UpdateDepartmentForm.location{{$index}}.$error.required && (UpdateDepartmentForm.location{{$index}}.$dirty || submitted)" class="help-block">Dept.Location is Required</p>-->																																																									
														</div>
												   </div>	
							                  </td>
					    		              <td>
                    							   <span ng-if="!Department.$edit">{{Department.contactPerson}}</span>
												   <div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.contactPerson{{$index}}.$invalid && (UpdateDepartmentForm.contactPerson{{$index}}.$dirty || submitted)}">												
		<input class="form-control" type="text" data-ng-model="Department.contactPerson"  maxlength="25" placeholder='Contact Person' name="contactPerson{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('contactPerson',$index)"/>
<p data-ng-show="UpdateDepartmentForm.contactPerson{{$index}}.$error.pattern  && (UpdateDepartmentForm.contactPerson{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>					
<p data-ng-show="UpdateDepartmentForm.contactPerson{{$index}}.$error.required && (UpdateDepartmentForm.contactPerson{{$index}}.$dirty || submitted)" class="help-block">Required</p>																																																											
</div>													   
												   </div>	
							                  </td>					
							                  <td>
                    							    <span ng-if="!Department.$edit">{{Department.extension}}</span>
													<div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.extension{{$index}}.$invalid && (UpdateDepartmentForm.extension{{$index}}.$dirty || submitted)}">													
		<input class="form-control" type="text" data-ng-model="Department.extension" maxlength="10" name="extension{{$index}}" data-ng-required='true' placeholder='Extension' data-ng-pattern="/^[0-9\s]*$/" ng-blur="spacebtwgrid('extension',$index)"/>
<p data-ng-show="UpdateDepartmentForm.extension{{$index}}.$error.pattern  && (UpdateDepartmentForm.extension{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>					
<p data-ng-show="UpdateDepartmentForm.extension{{$index}}.$error.required && (UpdateDepartmentForm.extension{{$index}}.$dirty || submitted)" class="help-block">Required</p>																																																													
														</div>													
													</div>	
					            		      </td>
											  <td>
                    							    <span ng-if="!Department.$edit">{{Department.directNumber}}</span>
													<div ng-if="Department.$edit">
<div  class="form-group"  ng-class="{ 'has-error' : UpdateDepartmentForm.directNumber{{$index}}.$invalid && (UpdateDepartmentForm.directNumber{{$index}}.$dirty || submitted)}">													
	<input class="form-control" type="text" data-ng-model="Department.directNumber" name="directNumber{{$index}}"  placeholder='Direct No.' data-ng-pattern="/^[0-9\s]*$/" maxlength="10" size="4" ng-blur="spacebtwgrid('directNumber',$index)"/>
<p data-ng-show="UpdateDepartmentForm.directNumber{{$index}}.$error.pattern  && (UpdateDepartmentForm.directNumber{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>					
<!--<p data-ng-show="UpdateDepartmentForm.directNumber{{$index}}.$error.required && (UpdateDepartmentForm.directNumber{{$index}}.$dirty || submitted)" class="help-block">Direct No is Required</p>-->																																																														
													</div>	
													</div>	
					            		      </td>											  			
											  <td>
							                     		 <span ng-if="!Department.$edit">
												<span ng-if="Department.status=='0'">Active</span>
												<span ng-if="Department.status=='1'">Inactive</span>
															</span>
        		            					  <div ng-if="Department.$edit">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status{{$index}}" value="0" data-ng-model='Department.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="status{{$index}}" value="1" data-ng-model='Department.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>						 							 												  
												  </div>
							                  </td>																						  
							             </tr>
									 </tbody>
								   </table>							 
								</form>
							</div>
						</section>
					 </div>
				</div>
			</div>
					<!---------Table Design End----------->


				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
