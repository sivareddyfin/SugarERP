		<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="seedlingIndentController" ng-init="addFormField();loadVillageNames();loadseasonNames();loadKindType();loadFieldAssistants();onloadFunction();loadRyotCodeDropDown();loadVarietyNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Common Indent</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	
						<form name="seedlingIndentForm"  novalidate >
							  
							  <div class="row">
							 
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : seedlingIndentForm.season.$invalid && (seedlingIndentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select  chosen class="w-100" ng-model="AddedSeedlingIndent.season" ng-options="season.season as season.season for season in seasonNames" ng-change="getIndentMax(AddedSeedlingIndent.season,AddedSeedlingIndent.fieldMan);"><option value="">Season</option></select>
															
				        		        	        </div>
													<p ng-show="seedlingIndentForm.season.$error.required && (seedlingIndentForm.season.$dirty || submitted)" class="help-block">Season is Required.</p>													
												</div>
					                    	</div>
										</div>
										<div class='col-sm-6'>
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.fieldMan.$invalid && (seedlingIndentForm.fieldMan.$dirty || submitted)}">
						        	                <div class="fg-line">
													<select  chosen class="w-100" name="village" ng-model="AddedSeedlingIndent.fieldMan" tabindex="14" ng-required="true" ng-options="fieldMan.id as fieldMan.fieldassistant for fieldMan in FieldAssistantData | orderBy:'fieldassistant':true" ng-change="getIndentMax(AddedSeedlingIndent.season,AddedSeedlingIndent.fieldMan);">
													<option value="">Field Assistant</option>
												</select>
													</div>
													<p ng-show="seedlingIndentForm.fieldMan.$error.required && (seedlingIndentForm.fieldMan.$dirty || submitted)" class="help-block">Select Field Assistant</p>
													</div>
													
													</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : seedlingIndentForm.indentNo.$invalid && (seedlingIndentForm.indentNo.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<input type="text" class="form-control" ng-model="AddedSeedlingIndent.indentNo" name="indentNo" placeholder="Indent No." readonly />
				                			        </div>
													<p ng-show="seedlingIndentForm.indentNo.$error.required && (seedlingIndentForm.indentNo.$dirty || submitted)" class="help-block">Indent No. is required</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							  <div class="row">
							  	<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.ryotCode.$invalid && (seedlingIndentForm.ryotCode.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="ryotCode" data-ng-model='AddedSeedlingIndent.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-ryotCode':true" ng-change="loadRyotDet(AddedSeedlingIndent.ryotCode);">
															<option value="">Ryot Code</option>
														</select>
				                			        </div>
													<p ng-show="seedlingIndentForm.ryotCode.$error.required && (seedlingIndentForm.ryotCode.$dirty || submitted)" class="help-block">Select RyotCode</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.ryotName.$invalid && (seedlingIndentForm.ryotName.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<input type="text" class="form-control" placeholder="Ryot Name" name="ryotName" data-ng-model='AddedSeedlingIndent.ryotName' name="ryotName" readonly>
															
				                			        </div>
													<p ng-show="seedlingIndentForm.ryotCode.$error.required && (seedlingIndentForm.ryotCode.$dirty || submitted)" class="help-block">RyotName is required</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							
							
							  
							  
							 <div class="row">
							 <div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.village.$invalid && (seedlingIndentForm.village.$dirty || submitted)}">
						        	                <div class="fg-line">
													
													<select  chosen class="w-100" name="village" ng-model="AddedSeedlingIndent.village" ng-options="village.id as village.village for village in villageNames"><option value="">Village</option></select>
													</div>
													<!--<p ng-show="seedlingIndentForm.village.$error.required && (seedlingIndentForm.village.$dirty || submitted)" class="help-block">Village is required</p>-->
													</div>
													
													</div>
									</div>
								
								</div>			
										</div>
										<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.fatherName.$invalid && (seedlingIndentForm.fatherName.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Father" ng-model="AddedSeedlingIndent.fatherName" readonly />
													</div>
													<p ng-show="seedlingIndentForm.fatherName.$error.required && (seedlingIndentForm.fatherName.$dirty || submitted)" class="help-block">Father Name is required</p>
													</div>
													
													</div>
									</div>
								
								</div>			
										</div>
							</div>
							<div class="row">
							 <div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.aadhaarNumber.$invalid && (seedlingIndentForm.aadhaarNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Aadhaar Number" ng-model="AddedSeedlingIndent.aadhaarNumber" readonly />
													</div>
													<p ng-show="seedlingIndentForm.aadhaarNumber.$error.required && (seedlingIndentForm.aadhaarNumber.$dirty || submitted)" class="help-block">Aadhaar Number is required</p>
													</div>
													
													</div>
									</div>
								
								</div>
								
											
										</div>
										<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.phoneNumber.$invalid && (seedlingIndentForm.phoneNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Phone Number" ng-model="AddedSeedlingIndent.phoneNumber" readonly />
													</div>
													<p ng-show="seedlingIndentForm.phoneNumber.$error.required && (seedlingIndentForm.phoneNumber.$dirty || submitted)" class="help-block">Phone Number is required</p>
													</div>
													
													</div>
									</div>

									<div class="col-sm-12">
								
										
											
										</div>
							</div>			
									
							<div class="row">
							
							</div>		
										
							 </div>
							
							  	
									
													 					 	
							 	
								<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Kind</th>
											<th>Date of Delivery</th>
											<th>Quantity</th>
							                <th>UOM</th>
		                    				<th>Village</th>
											<th>Vareity</th>
											
										
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td style="display:none;">
											<input type ="hidden" ng-model="batchData.id" name="batchID{{batchData.id}}"
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.kind{{batchData.id}}.$invalid && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<select class="form-control1" name="kind{{batchData.id}}" data-ng-model='batchData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType" ng-change ="getuomdetails(batchData.kind,batchData.id)" >
														<option value="" > Kind Type</option>	
														</select>
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.required && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.pattern && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td style="border:none;"><div class="form-group"><input type="text" class="form-control1 datepicker" ng-model="batchData.batchDate" name="batchDate{{batchData.id}}" placeholder="Date" ng-mouseover="ShowDatePicker();" /></div></td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.quantity{{batchData.id}}.$invalid && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Quantity"  name="quantity{{batchData.id}}" ng-model="batchData.quantity" data-ng-required='true' data-ng-pattern="/^[0-9.\s]*$/"/>
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.required && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.pattern && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.uom{{batchData.id}}.$invalid && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="UOM"  name="uom{{batchData.id}}" ng-model="batchData.uom" data-ng-required='true' readonly />
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.required && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.pattern && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.landVillageCode{{batchData.id}}.$invalid && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<select class="form-control1" name="village" ng-model="batchData.landVillageCode" tabindex="14" ng-required="true" ng-options="landVillageCode.id as landVillageCode.village for landVillageCode in villageNames | orderBy:'-village':true">
													<option value="">Village Name</option>
												</select>
<p ng-show="seedlingIndentForm.landVillageCode{{batchData.id}}.$error.required && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.variety{{batchData.id}}.$invalid && (seedlingIndentForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<select class="form-control1" name="village" ng-model="batchData.variety" tabindex="14" ng-required="true" ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true">
													<option value="">Variety</option>
												</select>
<p ng-show="seedlingIndentForm.variety{{batchData.id}}.$error.required && (seedlingIndentForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											
											
											</tr>
									</tbody>
							 	</table>
							</div>
								
										
							  
							
							
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide " ng-click="seedlingIndentFormSubmit(AddedSeedlingIndent,seedlingIndentForm);">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(seedlingIndentForm)">Reset</button>
										<br />
										<br />
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
