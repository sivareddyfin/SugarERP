	<script type="text/javascript">	
		$('#autofocus').focus();	
	</script>		

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GenerateAgreement" ng-init="addAgreemntRows();loadSeasonNames();loadRyotNames();loadBankNames();loadVarietyNames();loadLandType();loadCropType();loadCircleNames();loadFamilyNames();loadMandalNames();loadAgreementNumber();loadVillageNames();loadRyotExtentNames();loadSetupDetails();">       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Agreement</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="GenerateAgreementForm" novalidate ng-submit="GenerateAgreement(AddAgreement);">
					  	 <div class="row">
							 	<div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
										<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.seasonYear.$invalid && (GenerateAgreementForm.seasonYear.$dirty || submitted)}">																					
				        	                <div class="fg-line">
											  	<select chosen class="w-100" ng-options="seasonCode.season as seasonCode.season for seasonCode in seasonNames | orderBy:'-season':true" name="seasonYear" ng-model="AddAgreement.seasonYear" ng-required="true" ng-change="setAgreementNumber(AddAgreement.seasonYear);">
													<option value="">Select Season</option>
												</select>
				                	        </div>
				<p ng-show="GenerateAgreementForm.seasonYear.$error.required && (GenerateAgreementForm.seasonYear.$dirty || submitted)" class="help-block">Season required.</p>
										</div>
										</div>
			                    	</div>			                    
				                  </div>
							 </div>							 				  
						 <div class="row">
							 	<div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
				        	                <div class="fg-line">
											  	<select chosen class="w-100">
													<option value="0">Added Agreements</option>
												</select>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>
							 </div>							 				  
					  	 <div class="row">
						 	<div class="col-sm-6">																
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control" placeholder="Agreement Number"   maxlength="15" tabindex="1" readonly name="agreementNumber" ng-model="AddAgreement.agreementNumber"/>
												  <input type="hidden" name="tempAgreement" ng-model="tempAgreement" />
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.ryotCode.$invalid && (GenerateAgreementForm.ryotCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select chosen class="w-100"  tabindex="2" name="ryotCode" ng-model="Agreement.ryotCode" ng-options="ryotCode.id as ryotCode.ryotname for ryotCode in ryotNames| orderBy:'-ryotname':true"  ng-required="true" ng-change="loadTyotDetails(Agreement.ryotCode);">
														<option value="">Select Ryot</option>
													</select>
				                	    	    </div>
				<p ng-show="GenerateAgreementForm.ryotCode.$error.required && (GenerateAgreementForm.ryotCode.$dirty || submitted)" class="help-block">Select Ryot.</p>												
				</div>												
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control" placeholder="Ryot Name"  readonly maxlength="25" name="ryotName" ng-model="AddAgreement.ryotName"/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
												  	<select class="w-100" chosen name="circleCode" ng-model="AddAgreement.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in circleNames" ng-disabled="dropDisabled"><option value="">Circle Name</option></select>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.branchCode.$invalid && (GenerateAgreementForm.branchCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select chosen class="w-100"  tabindex="3" name="branchCode" ng-model="branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in bankNames | orderBy:'-bankname':true"  ng-required="true">
														<option value="">Select Branch</option>
													</select>
												  </div>
<p ng-show="GenerateAgreementForm.branchCode.$error.required && (GenerateAgreementForm.branchCode.$dirty || submitted)" class="help-block">Select Branch.</p>													
</div>												  
			                	    	    </div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				           	 	<span class="input-group-addon">Has FamilyGroup :</span>
				        	                <div class="fg-line" style="margin-top:5px;">
    	        					          <label class="radio radio-inline m-r-20">
									            <input type="radio" name="hasFamilyGroup" value="0" ng-model="AddAgreement.hasFamilyGroup">
				    			            	<i class="input-helper"></i>Yes
									          </label>
				 				              <label class="radio radio-inline m-r-20">
				        	    			    <input type="radio" name="hasFamilyGroup" value="1" ng-model="AddAgreement.hasFamilyGroup">
			    					            <i class="input-helper"></i>No
					  		    	          </label>
			                	    	    </div>
				                    	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.suretyRyotCode.$invalid && (GenerateAgreementForm.suretyRyotCode.$dirty || submitted)}">											
					        	                <div class="fg-line">    	    	    					          
													<select  chosen multiple data-placeholder="Select Surety Ryot" class="w-100" tabindex="6" name="suretyRyotCode" ng-model="AddAgreement.suretyRyotCode" ng-options="suretyRyotCode.id as suretyRyotCode.ryotname for suretyRyotCode in ryotNames | orderBy:'-ryotname':true"  ng-required="true"></select>												
					                	        </div>
<p ng-show="GenerateAgreementForm.suretyRyotCode.$error.required && (GenerateAgreementForm.suretyRyotCode.$dirty || submitted)" class="help-block">Select Surety Ryot.</p>												
</div>												
											</div>
			    		               	</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				            	<span class="input-group-addon">Status :</span>
				        	                <div class="fg-line" style="margin-top:5px;">
    	        					          <label class="radio radio-inline m-r-20">
									            <input type="radio" name="status" value="0" ng-model="AddAgreement.status">
				    			            	<i class="input-helper"></i>Active
									          </label>
				 				              <label class="radio radio-inline m-r-20">
				        	    			    <input type="radio" name="status" value="1" ng-model="AddAgreement.status">
			    					            <i class="input-helper"></i>Inactive
					  		    	          </label>
			                	    	    </div>
				                    	</div>
									</div>
								</div>
								
							</div>
							
							
							<div class="col-sm-6">								
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.agreementDate.$invalid && (GenerateAgreementForm.agreementDate.$dirty || submitted)}">											
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control datepicker" placeholder="Agreement Date" maxlength="10" name="agreementDate" ng-model="AddAgreement.agreementDate" data-input-mask="{mask: '00-00-0000'}"  ng-required="true" id="autofocus" autofocus ng-mouseover="ShowDatePicker();"/>
					                	        </div>
<p ng-show="GenerateAgreementForm.agreementDate.$error.required && (GenerateAgreementForm.agreementDate.$dirty || submitted)" class="help-block">Agreement Date Required.</p>												
</div>												
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control" placeholder="Salutation" readonly maxlength="5" name="salutation" ng-model="AddAgreement.salutation"/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control" placeholder="F/H/G Name" readonly maxlength="25" name="relativeName" ng-model="AddAgreement.relativeName"/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
												  <select class="w-100" chosen name="mandalCode" ng-model="AddAgreement.mandalCode" ng-options="mandalCode.id as mandalCode.mandal for mandalCode in mandalNames | orderBy:'-mandal':true" ng-disabled="dropDisabled"><option value="">Mandal Name</option></select>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.accountNumber.$invalid && (GenerateAgreementForm.accountNumber.$dirty || submitted)}">											
					        	                <div class="fg-line">
    		        					          <input type="text" class="form-control" placeholder="Ryot SB A/C No."  tabindex="4" name="accountNumber" ng-model="AddAgreement.accountNumber" maxlength="20"  ng-required="true" data-ng-pattern="/^[0-9]+$/"/>
					                	        </div>
<p ng-show="GenerateAgreementForm.accountNumber.$error.required && (GenerateAgreementForm.accountNumber.$dirty || submitted)" class="help-block">Account Number Required.</p>												
<p ng-show="GenerateAgreementForm.accountNumber.$error.pattern && (GenerateAgreementForm.accountNumber.$dirty || submitted)" class="help-block">Enter Valid Account Number.</p>												
</div>												
											</div>
				                    	</div>										
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.familyGroupCode.$invalid && (GenerateAgreementForm.familyGroupCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select chosen class="w-100"  tabindex="5" name="familyGroupCode" ng-model="AddAgreement.familyGroupCode"  ng-required="true" ng-options="familyGroupCode.id as familyGroupCode.groupname for familyGroupCode in FamilyNames | orderBy:'-groupname':true"><option value="">Select Family Group</option></select>
												  </div>
<p ng-show="GenerateAgreementForm.familyGroupCode.$error.required && (GenerateAgreementForm.familyGroupCode.$dirty || submitted)" class="help-block">Select Family Group.</p>													
</div>												  
			                		        </div>
			                    		</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
												  	<input type="text" class="form-control" placeholder="Remarks"  tabindex="7" name="remarks" ng-model="AddAgreement.remarks"/>
													<input type="hidden"  name="villageCode" ng-model="AddAgreement.villageCode"/>
													<input type="hidden"  tabindex="7" name="relation" ng-model="AddAgreement.relation"/>
												  </div>
			                		        </div>
			                    		</div>										
									</div>
								</div>								
							</div>
						 </div>	
						 <hr />				  	
						 <div  class="table-responsive">	 
							<table class="table table-striped">
							 	<thead>
								   <tr>
										<th>Action</th>
                		    			<th>Plot No</th>
							            <th>Variety</th>
		                    			<th>Plant/Ratoon</th>
										<th>Date of Planting</th>											
										<th>Agrd. Qty(in tons)</th>
										<th>Extent(Acers)</th>											
										<th>Survey No</th>
										<th>Land Vil.Code</th>
										<th>Land Type</th>
										<th>Extent Image</th>
            						</tr>											
								</thead>
								<tbody>
								    <tr ng-repeat="agreementData in data">
										<td>
											<input type="hidden" name="setupStatus" ng-model="setupStatus" />
											<button type="button" class="btn btn-primary" ng-click="addAgreemntRows();" ng-if="agreementData.plotSeqNo=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
											<button type="button" class="btn btn-primary" ng-click="removeAgreemntRows(agreementData.plotSeqNo);" ng-if="agreementData.plotSeqNo!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>											
										</td>
                		    			<td style="width:100px;">
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plotNumber.$invalid && (GenerateAgreementForm.plotNumber.$dirty || submitted)}">
												<input type="text" class="form-control1" placeholder='Plot Number' maxlength="10"  tabindex="7" name="plotNumber" ng-model="agreementData.plotNumber" data-ng-pattern="/^[0-9]+$/"  ng-required="true"/>							
											<p ng-show="GenerateAgreementForm.plotNumber.$error.required && (GenerateAgreementForm.plotNumber.$dirty || submitted)" class="help-block">Required.</p>		
											<p ng-show="GenerateAgreementForm.plotNumber.$error.pattern && (GenerateAgreementForm.plotNumber.$dirty || submitted)" class="help-block">Invalid.</p>											
											</div>		
										</td>
							            <td style="width:100px;">
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.varietyCode.$invalid && (GenerateAgreementForm.varietyCode.$dirty || submitted)}">										
												<select class="form-control1" tabindex="8" name="varietyCode" ng-model="agreementData.varietyCode" ng-options="varietyCode.id as varietyCode.variety for varietyCode in varietyNames | orderBy:'-variety':true"  ng-required="true">
													<option value="">Variety</option>
												</select>
												<p ng-show="GenerateAgreementForm.varietyCode.$error.required && (GenerateAgreementForm.varietyCode.$dirty || submitted)" class="help-block">Required.</p>													
											</div>
										</td>
		                    			<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plantorRatoon.$invalid && (GenerateAgreementForm.plantorRatoon.$dirty || submitted)}">										
												<select class="form-control1" tabindex="9" name="plantorRatoon" ng-model="agreementData.plantorRatoon" ng-options="plantorRatoon.id as plantorRatoon.croptype for plantorRatoon in cropTypeNames | orderBy:'-croptype':true"  ng-required="true"><option value="">Crop Type</option></select>
												<p ng-show="GenerateAgreementForm.plantorRatoon.$error.required && (GenerateAgreementForm.plantorRatoon.$dirty || submitted)" class="help-block">Required.</p>
											</div>
										</td>
										<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.cropDate.$invalid && (GenerateAgreementForm.cropDate.$dirty || submitted)}">										
												<input type="text" class="form-control1 datepicker" placeholder='Crop Date' maxlength="10" tabindex="10" name="cropDate" ng-model="agreementData.cropDate" data-input-mask="{mask: '00-00-0000'}" ng-mouseover="ShowDatePicker();"  ng-required="true"/>
												<p ng-show="GenerateAgreementForm.cropDate.$error.required && (GenerateAgreementForm.cropDate.$dirty || submitted)" class="help-block">Required.</p>												
											</div>
										</td>
										<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.agreedQty.$invalid && (GenerateAgreementForm.agreedQty.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Agrd. Qty' maxlength="10" tabindex="11" name="agreedQty" ng-model="agreementData.agreedQty" data-ng-pattern="/^[0-9]+$/"  ng-required="true"/>
												<p ng-show="GenerateAgreementForm.agreedQty.$error.required && (GenerateAgreementForm.agreedQty.$dirty || submitted)" class="help-block">Required.</p>	
												<p ng-show="GenerateAgreementForm.agreedQty.$error.pattern && (GenerateAgreementForm.agreedQty.$dirty || submitted)" class="help-block">Invalid.</p>																						
</div>											
										</td>
										<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.extentSize.$invalid && (GenerateAgreementForm.extentSize.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Extent(Acers)' maxlength="10" tabindex="12" name="extentSize" ng-model="agreementData.extentSize" data-ng-pattern="/^[0-9]+$/"  ng-required="true"/>
												<p ng-show="GenerateAgreementForm.extentSize.$error.required && (GenerateAgreementForm.extentSize.$dirty || submitted)" class="help-block">Required.</p>							
												<p ng-show="GenerateAgreementForm.extentSize.$error.pattern && (GenerateAgreementForm.extentSize.$dirty || submitted)" class="help-block">Invalid.</p>																		
											</div>												
										</td>
										<!--<td><a href="#/harvesting/transaction/OfferMaster" ng-click="showSurveyDialog();">Survey No.</a></td>-->										
										<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.surveyNumber.$invalid && (GenerateAgreementForm.surveyNumber.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Survey No' maxlength="10" tabindex="13" name="surveyNumber" ng-model="agreementData.surveyNumber" data-ng-pattern="/^[0-9]+$/" ng-required="true" ng-click="showSurveyDialog(agreementData.plotSeqNo);" ng-focus="showSurveyDialog(agreementData.plotSeqNo);" ng-if="setupStatus=='0'" readonly/>
												<input type="text" class="form-control1" placeholder='Survey No' maxlength="10" tabindex="13" name="surveyNumber" ng-model="agreementData.surveyNumber" data-ng-pattern="/^[0-9]+$/" ng-required="true" ng-click="showSurveyDialog(agreementData.plotSeqNo);" ng-focus="showSurveyDialog(agreementData.plotSeqNo);" ng-if="setupStatus=='1'"/>
												
												<p ng-show="GenerateAgreementForm.surveyNumber.$error.required && (GenerateAgreementForm.surveyNumber.$dirty || submitted)" class="help-block">Required.</p>		
												<p ng-show="GenerateAgreementForm.surveyNumber.$error.pattern && (GenerateAgreementForm.surveyNumber.$dirty || submitted)" class="help-block">Invalid.</p>																						
											</div>
										</td>
                		    			<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.landVillageCode.$invalid && (GenerateAgreementForm.landVillageCode.$dirty || submitted)}">										
												<select class="form-control1" name="landVillageCode" ng-model="agreementData.landVillageCode" tabindex="14" ng-required="true" ng-options="landVillageCode.id as landVillageCode.village for landVillageCode in villageNames | orderBy:'-village':true">
													<option value="">Village</option>
												</select>
												<p ng-show="GenerateAgreementForm.landVillageCode.$error.required && (GenerateAgreementForm.landVillageCode.$dirty || submitted)" class="help-block">Required.</p>
											</div>
										</td>
										<td style="width:120px;">
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.landTypeCode.$invalid && (GenerateAgreementForm.landTypeCode.$dirty || submitted)}">											
												<select class="form-control1" tabindex="15" name="landTypeCode" ng-model="agreementData.landTypeCode" ng-options="landTypeCode.id as landTypeCode.landtype for landTypeCode in landTypeNames" ng-required="true"><option value="">Land Type</option></select>
												<p ng-show="GenerateAgreementForm.landTypeCode.$error.required && (GenerateAgreementForm.landTypeCode.$dirty || submitted)" class="help-block">Required.</p>
											</div>
										</td>
										<td>
											<div class="fileinput fileinput-new" data-provides="fileinput">
								               <span class="btn btn-primary btn-file m-r-10">
								               		<span class="fileinput-new">Select</span>
									                <span class="fileinput-exists">Change</span>
									                  <input type="file" name="..." tabindex="16">
				  		                       </span>
								               <span class="fileinput-filename"></span>
										       <a href="#/harvesting/transaction/OfferMaster" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
								           </div>
										</td>											
            						</tr>											
								</tbody>											
							 </table>
						</div>
						<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10">Save</button>
									<button type="submit" class="btn btn-primary btn-sm m-t-10">Reset</button>
								</div>
							</div>						 	
					   </div>
						<input type="hidden" name="seySurveyNumber" ng-model="seySurveyNumber" />
					   
					</form>					  							 		 							 
					<!------------------form end---------->							 
					   <!----------Popup box-------->
					<form name="agreementpopup" novalidate>
					   <div class="card" id="popup_box">						  
						  	  <div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : agreementpopup.ryotCodePop.$invalid && (agreementpopup.ryotCodePop.$dirty || Popsubmitted)}">										
			        	                	<div class="fg-line">										
												<select class="form-control1" name="ryotCodePop" ng-model="ryotCodePop" ng-options="ryotCodePop.id as ryotCodePop.ryotname for ryotCodePop in ryotNamesExtent| orderBy:'-ryotname':true"  ng-required="true" ng-change="loadAddedExtentDetails(ryotCodePop);">
													<option value="">Select Ryot</option>
												</select>
			         	       	        	</div>
											<p ng-show="agreementpopup.ryotCodePop.$error.required && (agreementpopup.ryotCodePop.$dirty || Popsubmitted)" class="help-block">Select Ryot Code.</p>								</div>			
			                    		</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder='Ryot Name' readonly name="ryotNamePop" ng-model="ryotNamePop"/>
			                	        </div>
			                    	</div>			                    
				                  </div>															
							</div><br />							
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
                		    					<th>Select</th>											
							                	<th>Plot No.</th>
												<th>Survey No.</th>
			                    				<th>Test Date</th>
												<input type="hidden" name="PlotNumberPop" ng-model="PlotNumberPop" />
												
        	    							</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="surveyData in PopupExtentData">
												<td>
													<div class="checkbox">
										                <label>														
										                    <input type="radio" value="" name="checkSurvey{{$index}}" ng-model="surveyData.checkSurvey" ng-click="setSurveyNumber(surveyData.surveyNo,surveyData.plotSlNumber,PlotNumberPop);">
															
									    	                <i class="input-helper"></i>
														</label>
									            	</div>
												</td>	
												<td>{{surveyData.plotSlNumber}}</td>
												<td>{{surveyData.surveyNo}}</td>
												<td>{{surveyData.testDate}}</td>
											</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="getSurveyNumber();">Get Survey Number</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialog();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>		
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<!----------end----------------------->										
					
				</div> 
	    </section>   
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
