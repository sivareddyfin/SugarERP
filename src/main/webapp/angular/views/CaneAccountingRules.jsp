	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneAccountingRules" ng-init="loadSeasonNames();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Accounting Rule Season</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="CaneAccountingFilterForm" novalidate>
					 <div class="row">
						 <div class="col-sm-3">					 
						 	<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>									
									<div class="fg-line">
										<div class="form-group" >
											<select chosen class="w-100"   data-ng-required='true' data-ng-model='AddedCaneAccountFilter.season' name="season" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true" ng-change="AddedDatesBySeason();">
												<option value="">Select Season</option>
							        		</select>										
											<!-- <p ng-show="CaneAccountingFilterForm.season.$error.required && (CaneAccountingFilterForm.season.$dirty || Filtersubmitted)" class="help-block">Season required.</p>-->
										</div>        		           		                				                
				                    </div>			                    
				                </div>
						 </div>
					</div>
					<div class="col-sm-3">
					 	<div class="col-sm-12">
							<div class="input-group">
        				        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : date.$invalid && (date.$dirty || Filtersubmitted)}">							                  
									<div class="form-group">
										<div class="fg-line">
											<select chosen class="w-100"  data-ng-model='AddedCaneAccountFilter.addedDate' name="calcruledate" ng-options="calcruledate.calcruledate as calcruledate.calcruledate for calcruledate in calcruledate | orderBy:'-calcruledate':true">
												<option value="">Select Added Date</option>
							        		</select>										
										</div>
									</div>														 
				                </div>
							</div>
                    	</div>
				</div>
				<div class="col-sm-3">
					<div class="col-sm-12">
						<div class="input-group">
		            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group" ng-class="{ 'has-error' : date.$invalid && (date.$dirty || Filtersubmitted)}">							        	            
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingFilterForm.date.$invalid && (CaneAccountingFilterForm.date.$dirty || Filtersubmitted)}">
									<div class="fg-line">
										<label for="date">Date</label>
    	        						<input type="text"  class="form-control input_date" placeholder="Date"  maxlength="25"  name="date" data-ng-model='AddedCaneAccountFilter.date' data-ng-required='true' data-input-mask="{mask: '00-00-0000'}" id="date" with-floating-label />
									</div>
									<p ng-show="CaneAccountingFilterForm.date.$error.required && (CaneAccountingFilterForm.date.$dirty || Filtersubmitted)" class="help-block">Date is required.</p>																																				
								</div>														 
				            </div>
						</div>
                  	</div>
					<input type="hidden" ng-model="AddedCaneAccount.modifyFlag"> 
				</div>			 
				<div class="col-sm-2">
				 	<div class="col-sm-12">					
					 	<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="loadCaneAccountDataByFilter(AddedCaneAccountFilter,CaneAccountingFilterForm);" >Load Previous Season Details</button>
					 </div>
				</div>
			</div>
		 </form>
		<hr />
		<form name="CaneAccountingRuleSeasonForm" ng-submit='CaneAccountingSubmit(AddedCaneAccount,CaneAccountingRuleSeasonForm);' novalidate>
			<div class="table-responsive">
				<table style="width:100%;">
					<tr>
						<th  style="text-align:center;">Is Purchase Tax</th>
						<th colspan="3" style="text-align:center;">Subsidy Details</th>
						<th style="text-align:center;">Is Accrued but not due</th>
					</tr>
					<tr>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" name="isFrpTax" ng-model="AddedCaneAccount.isFrpTax" ng-true-value="'1'" ng-false-value="'0'"><i class="input-helper"></i>
								</label>
							</div>
						</td>
						<td>
							<div class="input-group floating-label-wrapper" style="width:95%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.frp.$invalid && (CaneAccountingRuleSeasonForm.frp.$dirty || Addsubmitted)}">
									<div class="fg-line">
										<label for="frps">FRP</label>
										<input type="text" class="form-control" placeholder="FRP" id="frps" with-floating-label  maxlength="10" name="frp" data-ng-model='AddedCaneAccount.frp'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('frp');" ng-keyup="getTotalFRP();" ng-required="true"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.frp.$error.required && (CaneAccountingRuleSeasonForm.frp.$dirty || Filtersubmitted)" class="help-block">FRP is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.frp.$error.pattern && (CaneAccountingRuleSeasonForm.frp.$dirty || Filtersubmitted)" class="help-block">Enter Valid FRP.</p>	
								</div>
							</div>
						</td>
						<td>
							<div class="input-group floating-label-wrapper" style="width:95%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.frpSubsidy.$invalid && (CaneAccountingRuleSeasonForm.frpSubsidy.$dirty || Addsubmitted)}">
									<div class="fg-line">	
										<label for="frpsub">FRP Subsidy</label>					
										<input type="text" class="form-control" placeholder="FRP Subsidy" id="frpsub" with-floating-label maxlength="10" name="frpSubsidy" data-ng-model='AddedCaneAccount.frpSubsidy'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('frpSubsidy');" ng-keyup="getTotalFRP();"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.frpSubsidy.$error.required && (CaneAccountingRuleSeasonForm.frpSubsidy.$dirty || Filtersubmitted)" class="help-block">FRP Subsidy is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.frpSubsidy.$error.pattern && (CaneAccountingRuleSeasonForm.frpSubsidy.$dirty || Filtersubmitted)" class="help-block">Enter Valid FRP Subsidy.</p>										
								</div>
							</div>
						</td>
						<td>
							<div class="input-group floating-label-wrapper" style="width:95%;">
								<div class="form-group">
									<div class="fg-line">	
										<label for="netFrp">Net FRP</label>						
										<input type="text" class="form-control" placeholder="Net FRP" id="netFrp" with-floating-label maxlength="10" name="netFrp" data-ng-model='AddedCaneAccount.netFrp'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('netFrp');" readonly/>
									</div>
								</div>
							</div>
						</td>						
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="getSeaonCanePrice(AddedCaneAccount.isFrpSeaon,AddedCaneAccount.netFrp);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-true-value="'1'" ng-false-value="'0'" name="isFrpSeaon" ng-model="AddedCaneAccount.isFrpSeaon"><i class="input-helper"></i>
									
								</label>
								<input type="hidden" name="tempVal" ng-model="tempVal" />
							</div>
						</td>
					</tr>
					
					<tr>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" name="isHarTax" ng-model="AddedCaneAccount.isHarTax" ng-true-value="'1'" ng-false-value="'0'"><i class="input-helper"></i>
								</label>
							</div>
						</td>
						<td colspan="3">
							<div class="input-group floating-label-wrapper" style="width:100%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.harvestingSub.$invalid && (CaneAccountingRuleSeasonForm.harvestingSub.$dirty || Addsubmitted)}">
									<div class="fg-line">		
										<label for="harSub">Harvesting Subsidy</label>						
										<input type="text" class="form-control" placeholder="Harvesting Subsidy" id="harSub" with-floating-label maxlength="10" name="harvestingSub" data-ng-model='AddedCaneAccount.harvestingSub'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('harvestingSub');" ng-keyup="getTotalPrice(AddedCaneAccount.isHarvestingSeaon,AddedCaneAccount.harvestingSub);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-required="true"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.harvestingSub.$error.required && (CaneAccountingRuleSeasonForm.harvestingSub.$dirty || Filtersubmitted)" class="help-block">Harvesting Subsidy is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.harvestingSub.$error.pattern && (CaneAccountingRuleSeasonForm.harvestingSub.$dirty || Filtersubmitted)" class="help-block">Enter Valid Harvesting Subsidy</p>										
									
								</div>
							</div>
						</td>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="getSeaonCanePrice(AddedCaneAccount.isHarvestingSeaon,AddedCaneAccount.harvestingSub);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-true-value="'1'" ng-false-value="'0'" name="isHarvestingSeaon" ng-model="AddedCaneAccount.isHarvestingSeaon"><i class="input-helper"></i>
								</label>
							</div>
						</td>
					</tr>
					
					<tr>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" name="isIcpTax" ng-model="AddedCaneAccount.isIcpTax" ng-true-value="'1'" ng-false-value="'0'"><i class="input-helper" ng-click="removePurchaseTax(AddedCaneAccount.isIcpTax,AddedCaneAccount.isIcpSeaon,AddedCaneAccount.purchaseTax);" ng-disabled='beforefield'></i>
								</label>
							</div>
						</td>
						<td colspan="3">
							<div class="input-group floating-label-wrapper" style="width:100%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.purchaseTax.$invalid && (CaneAccountingRuleSeasonForm.purchaseTax.$dirty || Addsubmitted)}">
									<div class="fg-line">	
										<label for="icp">Purchase Tax / ICP</label>					
										<input type="text" class="form-control" placeholder="Purchase Tax / ICP" id="icp" with-floating-label maxlength="10" name="purchaseTax" data-ng-model='AddedCaneAccount.purchaseTax'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('purchaseTax'); " ng-keyup="getTotalPrice(AddedCaneAccount.isIcpSeaon,AddedCaneAccount.purchaseTax);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-required="true"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.purchaseTax.$error.required && (CaneAccountingRuleSeasonForm.purchaseTax.$dirty || Filtersubmitted)" class="help-block">Purchase Tax / ICP is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.purchaseTax.$error.pattern && (CaneAccountingRuleSeasonForm.purchaseTax.$dirty || Filtersubmitted)" class="help-block">Enter Valid Purchase Tax / ICP</p>										
									
								</div>
							</div>
						</td>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="getSeaonCanePrice(AddedCaneAccount.isIcpSeaon,AddedCaneAccount.purchaseTax);" ng-true-value="'1'" ng-false-value="'0'" name="isIcpSeaon" ng-model="AddedCaneAccount.isIcpSeaon" ng-disabled="righptaxStatus"><i class="input-helper"></i>
								</label>
							</div>
						</td>
					</tr>
					
					<tr>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" name="isAdditionalTax" ng-model="AddedCaneAccount.isAdditionalTax" ng-true-value="'1'" ng-false-value="'0'"><i class="input-helper"></i>
								</label>
							</div>
						</td>
						<td colspan="3">
							<div class="input-group floating-label-wrapper" style="width:100%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.additionalPrice.$invalid && (CaneAccountingRuleSeasonForm.additionalPrice.$dirty || Addsubmitted)}">
									<div class="fg-line">		
										<label for="addtnlCane">Additional Cane Price</label>					
										<input type="text" class="form-control" placeholder="Additional Cane Price" id="addtnlCane" with-floating-label maxlength="10" name="additionalPrice" data-ng-model='AddedCaneAccount.additionalPrice'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('additionalPrice');" ng-keyup="getTotalPrice(AddedCaneAccount.isAdditionalSeaon,AddedCaneAccount.additionalPrice);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-required="true"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.additionalPrice.$error.required && (CaneAccountingRuleSeasonForm.additionalPrice.$dirty || Filtersubmitted)" class="help-block">Additional Cane Price is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.additionalPrice.$error.pattern && (CaneAccountingRuleSeasonForm.additionalPrice.$dirty || Filtersubmitted)" class="help-block">Enter Valid Additional Cane Price</p>										
									
								</div>
							</div>
						</td>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="getSeaonCanePrice(AddedCaneAccount.isAdditionalSeaon,AddedCaneAccount.additionalPrice);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-true-value="'1'" ng-false-value="'0'" name="isAdditionalSeaon" ng-model="AddedCaneAccount.isAdditionalSeaon"><i class="input-helper"></i>
								</label>
							</div>
						</td>
					</tr>
					
					<tr>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" name="isTransportTax" ng-model="AddedCaneAccount.isTransportTax" ng-true-value="'1'" ng-false-value="'0'"><i class="input-helper"></i>
								</label>
							</div>
						</td>
						<td colspan="3">
							<div class="input-group floating-label-wrapper" style="width:100%;">
								<div class="form-group" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.tansportSub.$invalid && (CaneAccountingRuleSeasonForm.tansportSub.$dirty || Addsubmitted)}">
									<div class="fg-line">	
										<label for="transSub">Transport Subsidy</label>					
										<input type="text" class="form-control" placeholder="Transport Subsidy" id="transSub" with-floating-label maxlength="10" name="tansportSub" data-ng-model='AddedCaneAccount.tansportSub'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('tansportSub');" ng-keyup="getTotalPrice(AddedCaneAccount.isTransportSeaon,AddedCaneAccount.tansportSub);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-required="true"/>
									</div>
									<p ng-show="CaneAccountingRuleSeasonForm.tansportSub.$error.required && (CaneAccountingRuleSeasonForm.tansportSub.$dirty || Filtersubmitted)" class="help-block">Transport Subsidy is required.</p>
									<p ng-show="CaneAccountingRuleSeasonForm.tansportSub.$error.pattern && (CaneAccountingRuleSeasonForm.tansportSub.$dirty || Filtersubmitted)" class="help-block">Enter Valid Transport Subsidy.</p>	
								</div>							
							</div>
						</td>
						<td style="text-align:center;">
							<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="getSeaonCanePrice(AddedCaneAccount.isTransportSeaon,AddedCaneAccount.tansportSub);removePurchaseTax1(AddedCaneAccount.isIcpTax,AddedCaneAccount.purchaseTax,AddedCaneAccount.isIcpSeaon);" ng-true-value="'1'" ng-false-value="'0'" name="isTransportSeaon" ng-model="AddedCaneAccount.isTransportSeaon"><i class="input-helper"></i>
								</label>
							</div>
						</td>
					</tr>
					
				</table>
			</div><p></p>
			<div class="row">
				<div class="col-sm-4">
					<div class="input-group">
		            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group floating-label-wrapper">
							<div class="fg-line">
								<label for="totalPrice">Total Price</label>	
								<input type="text" class="form-control" placeholder="Total Price" id="totalPrice" with-floating-label maxlength="10" name="totalPrice" data-ng-model='AddedCaneAccount.totalPrice'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('totalPrice');" readonly/>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="input-group">
		            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group floating-label-wrapper">
							<div class="fg-line">
								<label for="seasonPrice">Accrued But Not Due</label>	
								<input type="text" class="form-control" placeholder="Accrued But Not Due" id="seasonPrice" with-floating-label maxlength="10" name="seasonPrice" data-ng-model='AddedCaneAccount.seasonPrice'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('seasonPrice');"/>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="input-group">
		            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group floating-label-wrapper">
							<div class="fg-line">
								<label for="postSeason">Accrued and Due</label>
								<input type="text" class="form-control" placeholder="Accrued and Due" id="postSeason" with-floating-label maxlength="10" name="postSeason" data-ng-model='AddedCaneAccount.postSeason'   ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpoint('postSeason');"/>
							</div>
						</div>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="input-group" style="margin-top:10px;">
						<span class="input-group-addon" title="Accrued but not Due Amt. Division Mode">Division Mode :</span>
					    <div class="fg-line" style="margin-top:5px;">
							<label class="radio radio-inline m-r-20">
								<input type="radio" name="isPercentOrAmt" value="0"    data-ng-model='AddedCaneAccount.isPercentOrAmt'  ng-click="clearValues();">
			    				<i class="input-helper"></i>Percentage
							</label>
				 			<label class="radio radio-inline m-r-20">
				            	<input type="radio" name="isPercentOrAmt" value="1"  data-ng-model='AddedCaneAccount.isPercentOrAmt' ng-click="clearValues();">
					    		<i class="input-helper"></i>Amount
							</label>						 							 
						</div>
					</div>
				</div>				
				<div class="col-sm-4">
					<div ng-if="AddedCaneAccount.isPercentOrAmt == '0'">										
						<div class="input-group">
		            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.percentForDed.$invalid && (CaneAccountingRuleSeasonForm.percentForDed.$dirty || Addsubmitted)}">												
					        	<div class="fg-line">
									<label for="percen">Percentage For Deductions</label>
    	            				<input type="text" class="form-control" placeholder="Percentage For Deductions"  maxlength="10" name="percentForDed" data-ng-model='AddedCaneAccount.percentForDed' ng-keyup="CheckLoanWithPercent(AddedCaneAccount.isPercentOrAmt);"   ng-pattern="/^[0-9.\s]*$/" tabindex="11" id="percen" with-floating-label style="text-align:right;" ng-required="true"/>
		                	    </div>
								<p ng-show="CaneAccountingRuleSeasonForm.percentForDed.$error.required && (CaneAccountingRuleSeasonForm.percentForDed.$dirty || Addsubmitted)" class="help-block">Percentage for Deductions is required</p>
								<p ng-show="CaneAccountingRuleSeasonForm.percentForDed.$error.pattern && (CaneAccountingRuleSeasonForm.percentForDed.$dirty || Addsubmitted)" class="help-block">Valid Percentage is required</p>		 
							</div>
			            </div>
					</div>
					<div ng-if="AddedCaneAccount.isPercentOrAmt == '1'">																			
						<div class="input-group">
		            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.amtForDed.$invalid && (CaneAccountingRuleSeasonForm.amtForDed.$dirty || Addsubmitted)}" >												
					        	<div class="fg-line">
									<label for="amount">Amount For Deductions</label>
    	            				<input type="text" class="form-control" placeholder="Amount For Deductions"  maxlength="10" name="amtForDed" data-ng-model='AddedCaneAccount.amtForDed'  ng-pattern="/^[0-9.\s]*$/" tabindex="8" id="amount" with-floating-label style="text-align:right;" ng-keyup="chkAmountForLoanDeductions(AddedCaneAccount.isPercentOrAmt);";  ng-blur="decimalpoint('amtForDed');"   ng-required="true"/>
		                	    </div>
								<p ng-show="CaneAccountingRuleSeasonForm.amtForDed.$error.required && (CaneAccountingRuleSeasonForm.amtForDed.$dirty || Addsubmitted)" class="help-block">Amount for Deductions is required</p>
								<p ng-show="CaneAccountingRuleSeasonForm.amtForDed.$error.pattern && (CaneAccountingRuleSeasonForm.amtForDed.$dirty || Addsubmitted)" class="help-block">Valid Amount for Deductions is required</p>		 
							</div>
			           </div>
					</div>
				</div>
				<div class="col-sm-4">
				<div ng-if="AddedCaneAccount.isPercentOrAmt == '0'">
					<div class="input-group">
		            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.loanRecovery.$invalid && (CaneAccountingRuleSeasonForm.loanRecovery.$dirty || Addsubmitted)}">												
					        <div class="fg-line">
								<label for="loan">Loan in Percentage</label>
    	            			<input type="text" class="form-control" placeholder="Loan Recovery"  maxlength="10" name="loanRecovery" data-ng-model='AddedCaneAccount.loanRecovery'  data-ng-required="true" ng-pattern="/^[0-9]+(\.-[0-9]{1,2})?$/" tabindex="12" id="loan" with-floating-label ng-required="true"/>
		                	</div>
							<p ng-show="CaneAccountingRuleSeasonForm.loanRecovery.$error.required && (CaneAccountingRuleSeasonForm.loanRecovery.$dirty || Addsubmitted)" class="help-block">Loan Recovery is required</p>		 
							<p ng-show="CaneAccountingRuleSeasonForm.loanRecovery.$error.pattern && (CaneAccountingRuleSeasonForm.loanRecovery.$dirty || Addsubmitted)" class="help-block">Enter Valid Loan Recovery Amount</p>		 
						</div>
			      </div>
				  </div>
				 <div ng-if="AddedCaneAccount.isPercentOrAmt == '1'">																			
						<div class="input-group">
		            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.amtForLoans.$invalid && (CaneAccountingRuleSeasonForm.amtForLoans.$dirty || Addsubmitted)}" >												
					        	<div class="fg-line">
									<label for="loanInAmount">Loan in Amount</label>
    	            				<input type="text" class="form-control" placeholder="Loan in Amount"  maxlength="10" name="amtForLoans" data-ng-model='AddedCaneAccount.amtForLoans'  ng-pattern="/^[0-9.\s]*$/" tabindex="8" id="loanInAmount" with-floating-label style="text-align:right;" ng-keyup="chkAmountForLoanDeductions(AddedCaneAccount.isPercentOrAmt);";  ng-blur="decimalpoint('amtForLoans');"   ng-required="true"/>
		                	    </div>
								<p ng-show="CaneAccountingRuleSeasonForm.amtForLoans.$error.required && (CaneAccountingRuleSeasonForm.amtForLoans.$dirty || Addsubmitted)" class="help-block">Amount for Loans is required</p>
								<p ng-show="CaneAccountingRuleSeasonForm.amtForLoans.$error.pattern && (CaneAccountingRuleSeasonForm.amtForLoans.$dirty || Addsubmitted)" class="help-block">Valid Amount for Loans is required</p>		 
							</div>
			           </div>
					</div> 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="input-group" style="margin-top:10px;">
						<span class="input-group-addon" title="Transport Subsidy">TS :</span>
					    <div class="fg-line" style="margin-top:5px;">
							<label class="radio radio-inline m-r-20">
								<input type="radio" name="isTAApplicable" value="0"    data-ng-model='AddedCaneAccount.isTAApplicable'>
				    			<i class="input-helper"></i>Yes
							</label>
				 			<label class="radio radio-inline m-r-20">
				            	<input type="radio" name="isTAApplicable" value="1"   data-ng-model='AddedCaneAccount.isTAApplicable'>
					    		<i class="input-helper"></i>No
							</label>						 							 
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div ng-if="AddedCaneAccount.isTAApplicable=='0'">
						<div class="input-group">
			            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.noOfKms.$invalid && (CaneAccountingRuleSeasonForm.noOfKms.$dirty || Addsubmitted)}">												
						        <div class="fg-line">
									<label for="kms">No. of Kms</label>
    	            				<input type="text" class="form-control" placeholder="No. of Kms"  maxlength="10" name="noOfKms" data-ng-model='AddedCaneAccount.noOfKms'  ng-pattern="/^[0-9]+(\.-[0-9]{1,2})?$/" tabindex="13" id="kms" with-floating-label ng-required="true"/>
		                		</div>
								<p ng-show="CaneAccountingRuleSeasonForm.noOfKms.$error.required && (CaneAccountingRuleSeasonForm.noOfKms.$dirty || Addsubmitted)" class="help-block">No.of Kms required </p>
								<p ng-show="CaneAccountingRuleSeasonForm.noOfKms.$error.pattern && (CaneAccountingRuleSeasonForm.noOfKms.$dirty || Addsubmitted)" class="help-block">Valid no.of Kms required </p>		 
							</div>
				         </div>
					</div>
				</div>
				<div class="col-sm-4">
					<div ng-if="AddedCaneAccount.isTAApplicable=='0'">
						<div class="input-group">
		            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$invalid && (CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$dirty || Addsubmitted)}">												
					        	<div class="fg-line">
									<label for="allow">Subsidy/Ton/Km</label>
    	            				<input type="text" class="form-control" placeholder="Subsidy/Ton/Km"  maxlength="10" name="allowancePerKmPerTon" data-ng-model='AddedCaneAccount.allowancePerKmPerTon'   ng-pattern="/^[0-9]+(\.-[0-9]{1,2})?$/" tabindex="14" id="allow" with-floating-label ng-required="true"/>
		                	    </div>
								<p ng-show="CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$error.required && (CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$dirty || Addsubmitted)" class="help-block">Subsidy/Ton/Km is required</p>
								<p ng-show="CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$error.pattern && (CaneAccountingRuleSeasonForm.allowancePerKmPerTon.$dirty || Addsubmitted)" class="help-block">Valid Subsidy/Ton/Km is required</p>		 
							</div>
			             </div>
					</div>
				</div>
			</div>
 <input type="hidden" ng-model="AddedCaneAccount.calcid1" />
<input type="hidden" ng-model="AddedCaneAccount.calcid2" />
<input type="hidden" ng-model="AddedCaneAccount.calcid3" />
<input type="hidden" ng-model="AddedCaneAccount.calcid4" />
<input type="hidden" ng-model="AddedCaneAccount.calcid5" />
			   
			
			<br />					
			<div class="row" align="center">
				<div class="col-sm-12">
					<div class="input-group">
						<div class="fg-line">
							<button type="submit" class="btn btn-primary btn-hide">Save</button>
							<button type="reset" class="btn btn-primary" ng-click="reset(CaneAccountingRuleSeasonForm);">Reset</button>
						</div>
					</div>
				</div>							
			</div>
		</form>
		
		<!------------------form end---------->							 
		<!-------from-end--------------------->		 						           			
	</div>
  </div>
</div>
</section> 
</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
