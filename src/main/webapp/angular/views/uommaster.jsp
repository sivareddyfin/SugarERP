		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	

	<!--<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>-->
	<section id="main" class='bannerok'>    
<!--	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>-->

    	<section id="content" data-ng-controller="uomMasterController" data-ng-init="loadStatus();getAllUOM();getMAXUOM();" >      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>UOM  Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="uomMaster" novalidate ng-submit="UOMSubmit(AddedUOM,uomMaster);">
					<input type="hidden" ng-model='AddedUOM.modifyFlag' name="modifyFlag" />
					<div class="row">
						<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : uomMaster.uomCode.$invalid && (uomMaster.uomCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="uomCode">UOM Code</label>
															 <input type="text" class="form-control autofocus" placeholder="UOM Code"  autofocus  maxlength="10" tabindex="1" name="uomCode" data-ng-model='AddedUOM.uomCode' data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" id="uomCode" with-floating-label ng-disabled='true'/>														 
				                				    </div>
						<p ng-show="uomMaster.uomCode.$error.required && (uomMaster.uomCode.$dirty || Addsubmitted)" class="help-block">UOM Code is required</p>
						<p ng-show="uomMaster.uomCode.$error.pattern  && (uomMaster.uomCode.$dirty || Addsubmitted)" class="help-block">Enter Valid UOM Code</p>													
												</div>
					                    	</div>
											</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : uomMaster.description.$invalid && (uomMaster.description.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="description">Description</label>
															 <input type="text" class="form-control autofocus" placeholder="Description"  autofocus  maxlength="50" tabindex="3" name="description" data-ng-model='AddedUOM.description'   id="description" with-floating-label />														 
				                				    </div>
						<p ng-show="uomMaster.description.$error.required && (uomMaster.description.$dirty || Addsubmitted)" class="help-block">Description is required</p>
																
												</div>
					                    	</div>
											</div>
						</div>
						</div>
						<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : uomMaster.uom.$invalid && (uomMaster.uom.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="uom">UOM</label>
															 <input type="text" class="form-control autofocus" placeholder="UOM"  autofocus  maxlength="25" tabindex="2" name="uom" data-ng-model='AddedUOM.uom' data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="uom" with-floating-label ng-blur="validateDup();" />														 
				                				    </div>
						<p ng-show="uomMaster.uom.$error.required && (uomMaster.uom.$dirty || Addsubmitted)" class="help-block">UOM is required</p>
						<p ng-show="uomMaster.uom.$error.pattern  && (uomMaster.uom.$dirty || Addsubmitted)" class="help-block">Enter Valid UOM</p>
						<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedUOM.uom!=null">UOM Already Exist.</p>													
												</div>
					                    	</div>
											</div>
						</div>
						
						
						<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
			                        <br />
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedUOM.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="AddedUOM.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>
							
							
							
							
</div>
							
						</div>
						
						
					</div>
					<br />
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="reset" class="btn btn-primary" ng-click='reset(uomMaster);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
					</form>
						
 					 									 
				   			
				        </div>
						
			    	</div>
					
					
				<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added UOM</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">		
											  <form name="UpdateUOMForm" novalidate>
												<table ng-table="RyotTypeMaster.tableEdit" class="table table-striped table-vmiddle" >
												
												<thead>
													<tr>
														<th><span>Action</span></th>
														<th><span>UOM Code</span></th>
														<th><span>UOM</span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
													<tr ng-repeat="addedUom in uomData"  ng-class="{ 'active': addedUom.$edit }">
														<td>
														   <button type="button" class="btn btn-default" ng-if="!addedUom.$edit" ng-click="addedUom.$edit = true;addedUom.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="addedUom.$edit" ng-click="updateUom(addedUom,$index);addedUom.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="addedUom.modifyFlag"  />	
														  
														 </td>
														 <td>
															<span ng-if="!addedUom.$edit">{{addedUom.uomCode}}</span>
															<div ng-if="addedUom.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="addedUom.uomCode" maxlength="25" placeholder='UOM Code' readonly/>																</div>
															</div>
														  </td>											 
														 <td>
															<span ng-if="!addedUom.$edit">{{addedUom.uom}}</span>
															<div ng-if="addedUom.$edit">
			<div class="form-group" ng-class="{ 'has-error' : UpdateUOMForm.uom{{$index}}.$invalid && (UpdateUOMForm.uom{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="addedUom.uom" maxlength="25" placeholder='UOM' name="uom{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" ng-blur="validateDuplicate(addedUom.uom,$index);" >
					<p ng-show="UpdateUOMForm.uom{{$index}}.$error.required && (UpdateUOMForm.uom{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="UpdateUOMForm.uom{{$index}}.$error.pattern  && (UpdateUOMForm.uom{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="addedUom.uom!=null">UOM Type Already Exist.</p>
																</div>													
																
															</div>
														  </td>
														  <td>
															 <span ng-if="!addedUom.$edit">{{addedUom.description}}</span>
															 <div ng-if="addedUom.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="addedUom.description" maxlength="50" placeholder='Description' ng-blur="spacebtwgrid('description',$index)"/>																</div>
															 </div>
														  </td>
														  <td>
															  <span ng-if="!addedUom.$edit">
																<span ng-if="addedUom.status=='0'">Active</span>
																<span ng-if="addedUom.status=='1'">Inactive</span>
															  </span>
															  <div ng-if="addedUom.$edit">	
																<div class="form-group">
																  <label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model='addedUom.status'>
																	<i class="input-helper"></i>Active
																  </label>
																  <label class="radio radio-inline m-r-20">
																		<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model='addedUom.status'>
																		<i class="input-helper"></i>Inactive
																  </label>
																 </div>
															  </div>
														  </td>
													 </tr>
													 </tbody>
													 </table>	
													</form>	
									 	 </div>
								 	</section>					 
							  	</div>
							</div>
						</div>
					<!----------end----------------------->	
					
					
							</div>
					
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
