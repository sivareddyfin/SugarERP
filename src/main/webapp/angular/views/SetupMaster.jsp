
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SetupMaster" ng-init="loadAddedSetupDetails();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Setup Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
    	        		<form name="SetupMasterForm" ng-submit="SaveSetupMaster(AddSetup);">
							<div class="table-responsive">
								<table>
									<tr>
										<td><span class="input-group-addon"><b>Lab Results Mandatory</b></span></td>
										<td>: </td>
										<td></td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="lrm" value="0" ng-model="AddSetup.lrmVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="lrm" value="1" ng-model="AddSetup.lrmVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="lrmConfig" ng-model="AddSetup.lrmConfig"/>
											<input type="hidden" name="lrmName" ng-model="AddSetup.lrmName"/>
											<input type="hidden" name="lrmDesc" ng-model="AddSetup.lrmDesc"/>
											<input type="hidden" name="lrmId" ng-model="AddSetup.lrmId"/>
										</td>
									</tr>
									<tr>
										<td><span class="input-group-addon"><b>Audit Trail Required</b></span></td>
										<td>: </td>
										<td></td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="atr" value="0" ng-model='AddSetup.atrVal'>
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="atr" value="1" ng-model='AddSetup.atrVal'>
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="atrConfig" ng-model="AddSetup.atrConfig"/>
											<input type="hidden" name="atrName" ng-model="AddSetup.atrName"/>
											<input type="hidden" name="atrDesc" ng-model="AddSetup.atrDesc"/>
											<input type="hidden" name="atrId" ng-model="AddSetup.atrId"/>
										</td>										
									</tr>
									<tr ng-if="AddSetup.atrVal=='0'">
										<td><span class="input-group-addon"><b>Add records need to be tracked</b></span></td>
										<td>: </td>
										<td></td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="arnt" value="0" ng-model="AddSetup.arntVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="arnt" value="1" ng-model="AddSetup.arntVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="arntConfig" ng-model="AddSetup.arntConfig"/>
											<input type="hidden" name="arntName" ng-model="AddSetup.arntName"/>
											<input type="hidden" name="arntDesc" ng-model="AddSetup.arntDesc"/>
											<input type="hidden" name="arntId" ng-model="AddSetup.arntId"/>
										</td>										
										
									</tr>
									<tr ng-if="AddSetup.atrVal == '0'">
										<td><span class="input-group-addon"><b>Modify records need to be tracked</b> </span></td>
										<td>: </td>
										<td></td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="mrnt" value="0" ng-model='AddSetup.mrntVal'>
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="mrnt" value="1" ng-model='AddSetup.mrntVal'>
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="mrntConfig" ng-model="AddSetup.mrntConfig"/>
											<input type="hidden" name="mrntName" ng-model="AddSetup.mrntName"/>
											<input type="hidden" name="mrntDesc" ng-model="AddSetup.mrntDesc"/>
											<input type="hidden" name="mrntId" ng-model="AddSetup.mrntId"/>
										</td>																				
									</tr>
									<tr ng-if="AddSetup.mrntVal == '0'">
										<td><span class="input-group-addon"><b>Same Data also need to be tracked</b></span></td>
										<td>: </td>
										<td> </td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="sdnt" value="0" ng-model="AddSetup.sdntVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="sdnt" value="1" ng-model="AddSetup.sdntVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="sdntConfig" ng-model="AddSetup.sdntConfig"/>
											<input type="hidden" name="sdntName" ng-model="AddSetup.sdntName"/>
											<input type="hidden" name="sdntDesc" ng-model="AddSetup.sdntDesc"/>
											<input type="hidden" name="sdntId" ng-model="AddSetup.sdntId"/>
										</td>										
									</tr>
									<tr>
										<td><span class="input-group-addon"><b>Delete Data need to be tracked</b></span></td>
										<td>: </td>
										<td style="color:#FFFFFF;">dasdasd</td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="ddnt" value="0" ng-model="AddSetup.ddntVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="ddnt" value="1" ng-model="AddSetup.ddntVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="ddntConfig" ng-model="AddSetup.ddntConfig"/>
											<input type="hidden" name="ddntName" ng-model="AddSetup.ddntName"/>
											<input type="hidden" name="ddntDesc" ng-model="AddSetup.ddntDesc"/>
											<input type="hidden" name="ddntId" ng-model="AddSetup.ddntId"/>
										</td>										
									</tr>									
									<tr>
										<td><span class="input-group-addon"><b>Transport Data need to be posted</b></span></td>
										<td>: </td>
										<td style="color:#FFFFFF;">dasdasd</td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="tdnp" value="0" ng-model="AddSetup.tdnpVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="tdnp" value="1" ng-model="AddSetup.tdnpVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="tdnpConfig" ng-model="AddSetup.tdnpConfig"/>
											<input type="hidden" name="tdnpName" ng-model="AddSetup.tdnpName"/>
											<input type="hidden" name="tdnpDesc" ng-model="AddSetup.tdnpDesc"/>
											<input type="hidden" name="tdnpId" ng-model="AddSetup.tdnpId"/>
										</td>										
									</tr>
									<tr>
										<td><span class="input-group-addon"><b>Additional cane price need to be posted</b></span></td>
										<td>: </td>
										<td style="color:#FFFFFF;">dasdasd</td>
										<td>
											<div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="acpnp" value="0" ng-model="AddSetup.acpnpVal">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="acpnp" value="1" ng-model="AddSetup.acpnpVal">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
										</td>
										<td>
											<input type="hidden" name="acpnpConfig" ng-model="AddSetup.acpnpConfig"/>
											<input type="hidden" name="acpnpName" ng-model="AddSetup.acpnpName"/>
											<input type="hidden" name="acpnpDesc" ng-model="AddSetup.acpnpDesc"/>
											<input type="hidden" name="acpnpId" ng-model="AddSetup.acpnpId"/>
										</td>										
									</tr>
								</table>
							</div> 						   
							<div class="row" align="center">            			
							   <div class="input-group">
								  <div class="fg-line">
									  <button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
								  </div>
							   </div>						 	
							</div>							 							 
						 </form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
