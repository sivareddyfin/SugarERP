
	<script type="text/javascript">		
		$('#autofocus').focus();				
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="changePasswordController" ng-init="loginUsername();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Change Password</b></h2></div>
			    <div class="card">
			        <div  class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					    <form name="changepasswordForm" novalidate ng-submit="addedChangePasswordSubmit(AddedChange,changepasswordForm);">
							 
							 <div class="row">
							 	<div class="col-sm-6">									
								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : changepasswordForm.username.$invalid && (changepasswordForm.username.$dirty || Addsubmitted)}">
					        	                <div class="fg-line">
												<label for="Username">Username</label>
    		        					          <input type="text" placeholder='Username' class="form-control" maxlength="10" id="Username" with-floating-label name="username" ng-model="AddedChange.username" data-ng-required="true" readonly   />
			                	    		    </div>
						<p ng-show="changepasswordForm.username.$error.required && (changepasswordForm.username.$dirty || Addsubmitted)" class="help-block">Username is Required.</p>												
											</div>
				                    	 </div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : changepasswordForm.password.$invalid && (changepasswordForm.password.$dirty || submitted)}">	
			        	                		<div class="fg-line">
            					          			<label for="password">Password</label>
    		        					          <input type="text" placeholder='Password' class="form-control" maxlength="10" id="password" with-floating-label name="password" ng-model="AddedChange.password" data-ng-required="true" data-ng-minlength="10"   />
												
			                	        		</div>
	<p ng-show="changepasswordForm.password.$error.required && (changepasswordForm.password.$dirty || submitted)" class="help-block">Password is required</p>
	<p ng-show="changepasswordForm.password.$error.minlength && (changepasswordForm.password.$dirty || submitted)" class="help-block">Password is too short</p>												
											  </div>
			                    			</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											
											
											<div class="form-group"  ng-class="{ 'has-error' : changepasswordForm.confirmPassword.$invalid && (changepasswordForm.confirmPassword.$dirty || submitted)}">	
			        	                		<div class="fg-line">
            					          			<label for="retypepassword">Retype Password</label>
    		        					          <input type="text" placeholder='Retype Password' class="form-control" maxlength="10" id="retypepassword" with-floating-label name="confirmPassword" ng-model="AddedChange.confirmPassword" data-ng-compare="password" data-ng-required='true' />
												
			                	        		</div>
	<p ng-show="changepasswordForm.confirmPassword.$error.required && (changepasswordForm.confirmPassword.$dirty || submitted)" class="help-block">Your retype password is required</p>
    <p ng-show="changepasswordForm.confirmPassword.$error.compare  && (changepasswordForm.confirmPassword.$dirty || submitted)" class="help-block">Re type password did not match</p>											
											  </div>
			                    			</div>
										</div>
									</div>
									
								</div>
							
							 </div><br />
							
							 
							
							
							 
							 
							 
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<!--<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(changepasswordForm)">Reset</button>-->
									</div>
								</div>						 	
							 </div>	
						  </form>						 							 
						  
						  
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
