	<script type="text/javascript">		
		$('#autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="fieldAssistantMaster" ng-init='loadFieldAssistantId();loadFieldOfficerNames();loadAllFieldAssistants();loadFieldAssistantsNames();'>      
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Field Assistant Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
					 			<form name="FieldAssistantMaster" ng-submit="fieldoassistantfficersubmit(AddedFieldAssistant,FieldAssistantMaster)"  novalidate>
								<input type="hidden" name="screenName" ng-model="AddedFieldAssistant.screenName" />
							 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group floating-label-wrapper">
			                        	<div class="fg-line nulvalidate">
											<label for="fieldAsID">Field Assistant ID</label>
											<input type="text" class="form-control" placeholder="Field Assistant ID" maxlength="25"  name="fieldassistantid"  data-ng-model="AddedFieldAssistant.fieldAssistantId" readonly id="fieldAsID" with-floating-label> 
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" ng-class="{ 'has-error' : FieldAssistantMaster.fieldAssistant.$invalid && (FieldAssistantMaster.fieldAssistant.$dirty || Addsubmitted)}">
			                        	<div class="fg-line nulvalidate">										
								<select chosen class="w-100" name="fieldAssistant"    data-ng-required="true" data-ng-model="AddedFieldAssistant.employeeId" ng-options="fieldAssistant.id as fieldAssistant.employeename for fieldAssistant in AssistantNames  | orderBy:'-employeename':true" tabindex="2"  ng-change="selectFieldOfficer(AddedFieldAssistant.employeeId,AddedFieldAssistant.fieldOfficerId);">
									<option value=''>Select Field Assistant Name</option>
								</select>										
			                        	</div>
										<p ng-show="FieldAssistantMaster.fieldAssistant.$error.required && (FieldAssistantMaster.fieldAssistant.$dirty || Addsubmitted)" class="help-block"> Select Field Assistant Name</p>
										<div id="errorMsg" style="display:none; color:#FF0000;">Record Already Exist</div>
										</div>
			                
			                    </div>			                    
			                </div>
							</div>

<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedFieldAssistant.status" ng-init="AddedFieldAssistant.status='Active'"   >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"   data-ng-model='AddedFieldAssistant.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                        	</div>
										
										</div>

			                
			                    </div>			                    
			                </div>
				
							
</div>

							</div>
							
							<div class="col-sm-6">
							<div class="row">						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : FieldAssistantMaster.fieldOfficer.$invalid  && (FieldAssistantMaster.fieldOfficer.$dirty || Addsubmitted)}">
							<div class="fg-line">
							
								<select chosen class="w-100" name="fieldOfficer"    data-ng-required="true" data-ng-model="AddedFieldAssistant.fieldOfficerId" ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in officerNames | orderBy:'-fieldOfficer':true" id="autofocus" auyofocus tabindex="1"  ng-change="selectFieldOfficer(AddedFieldAssistant.employeeId,AddedFieldAssistant.fieldOfficerId);"> 
									<option value=''>Select Field Officer Name</option>
								</select>
							</div>
        		            <p ng-show="FieldAssistantMaster.fieldOfficer.$error.required  && (FieldAssistantMaster.fieldOfficer.$dirty || Addsubmitted)" class="help-block">Select Field Officer.</p>
		                </div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        	<div class="fg-line">
										<label for="description">Description</label>
									
											<input type="text" class="form-control" placeholder="Description" maxlength="50" name="description"  data-ng-model="AddedFieldAssistant.description"       tabindex="3"  ng-blur="spacebtw('description')" id="description" with-floating-label>
			                        	</div>
			                    </div>			                    
			                </div>
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedFieldAssistant.modifyFlag"  />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(FieldAssistantMaster)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Field Assistants</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
										<form name="FieldAssistantMasterEdit" novalidate>					
											<table ng-table="fieldAssistantMaster.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
											<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Field Assistant ID</span></th>
																<th><span>Field Assistant Name</span></th>
																<th><span>Field Officer Name</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
												<tbody>
												<tr ng-repeat="FieldAssistant in FieldAssistantData"  ng-class="{ 'active': FieldAssistant.$edit }">
													<td>
													   <button type="button" class="btn btn-default" ng-if="!FieldAssistant.$edit" ng-click="FieldAssistant.$edit = true;FieldAssistant.modifyFlag='Yes';;FieldAssistant.screenName='Field Assistant Master';"><i class="zmdi zmdi-edit"></i></button>
													   <button type="submit" class="btn btn-success" ng-if="FieldAssistant.$edit" ng-click="fieldassistantupdate(FieldAssistant,$index);FieldAssistant.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
													   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="FieldAssistant.modifyFlag"  />
													   <input type="hidden" name="screenName{{$index}}" ng-model="FieldAssistant.screenName" />
													 </td>
													 <td>
														<span ng-if="!FieldAssistant.$edit">{{FieldAssistant.fieldAssistantId}}</span>
														<div ng-if="FieldAssistant.$edit">
															<div class="form-group">
																<input class="form-control" type="text" ng-model="FieldAssistant.fieldAssistantId" placeholder='Field Assistant ID' maxlength="4" readonly/>
															</div>
														</div>
													  </td>
													  <td>
														 <span ng-if="!FieldAssistant.$edit">{{FieldAssistant.fieldAssistant}}</span>
														 <div ng-if="FieldAssistant.$edit">	
															<div class="form-group">												
		<select class="form-control1" name="fieldassistantnameedit{{$index}}"    data-ng-model="FieldAssistant.employeeId" ng-options="fieldAssistant.id as fieldAssistant.employeename for fieldAssistant in AssistantNames  | orderBy:'-employeename':true">
													<option value="">{{FieldAssistant.fieldAssistant}}</option>
										</select>																																						 													</div>
														 </div>
													  </td>
													  <td>
														  <span ng-if="!FieldAssistant.$edit" ng-repeat="names in officerNames">
															<span ng-if="names.id==FieldAssistant.fieldOfficerId">{{names.fieldOfficer}}</span>
														  </span> 
														  <div class="form-group" ng-class="{ 'has-error' : FieldAssistantMasterEdit.fieldofficernameedit.$invalid && (FieldAssistantMasterEdit.fieldofficernameedit.$dirty || submitted)}">
														  <div ng-if="FieldAssistant.$edit">
														  
		<select  class="form-control1" name="fieldOfficer"    data-ng-required="true" data-ng-model="FieldAssistant.fieldOfficerId" ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in officerNames | orderBy:'-fieldOfficer':true">
		</select>
													
															<p ng-show="FieldAssistantMasterEdit.fieldofficernameedit.$error.required && (FieldAssistantMasterEdit.fieldofficernameedit.$dirty || submitted)" class="help-block"> Field Officer Name is required</p>
													</div>
														  </div>
													  </td>
													  <td>
														   <span ng-if="!FieldAssistant.$edit">{{FieldAssistant.description}}</span>
														   <div ng-if="FieldAssistant.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="FieldAssistant.description" placeholder='Description' maxlength="50" ng-model="FieldAssistant.description" ng-blur="spacebtwgrid('description',$index)"/>
															   </div>
															 </div>
													  </td>
													  
													  <td>
														  <span ng-if="!FieldAssistant.$edit">
															<span ng-if="FieldAssistant.status=='0'">Active</span>
															<span ng-if="FieldAssistant.status=='1'">Inactive</span>
														  </span>
														  <div ng-if="FieldAssistant.$edit">
															<div class="form-group">
															<label class="radio radio-inline m-r-20">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model="FieldAssistant.status">
																<i class="input-helper"></i>Active
															</label>
															<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
																<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="FieldAssistant.status">
																<i class="input-helper"></i>Inactive
															</label>				
															</div>		 							 												  
														  </div>
													  </td>
													  
																																			
												 </tr>
												 </tbody>
											 </table>
												 </form>	
									</div>
								</section?>			 						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
