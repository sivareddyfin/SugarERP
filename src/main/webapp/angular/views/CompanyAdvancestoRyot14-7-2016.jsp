
	<script type="text/javascript">		
		$('#autofocus').focus();				
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="CompanyAdvancestoRyot" ng-init="addCompanyAdvanceRows();loadSeasonDropDown();loadCategoryDropDown();loadAdvanceTypeDropDown();loadAdvanceSubCategoryDropDown();loadTrayTypeDropDown();loadSeedSuppliersDropDown();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Company Advances to Ryots</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					    <form name="AdvancestoRyot" novalidate ng-submit="AddedAdvancestoRyot(AddAdvancestoRyot,AdvancestoRyot);">
							 <div class="row">
								<div class="col-sm-4">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.season.$invalid && (AdvancestoRyot.season.$dirty || Addsubmitted)}">
						        	      <div class="fg-line">
    	    		    					<div class="select">
												<select chosen class="w-100" name="season" ng-model="AddAdvancestoRyot.season" ng-options="season.season as season.season for season in seasonNames" data-ng-required="true" ng-change="getAddedAdvanceNames(AddAdvancestoRyot.season); getRyotNames(AddAdvancestoRyot.season);getseasonRyots(AddAdvancestoRyot.season);loadAgreementsDropDown(AddAdvancestoRyot.season);loadVarietyDropDown(AddAdvancestoRyot.season);">
													<option value="">Select Season</option>
												</select>
											</div>
					    	             </div>
							<p ng-show="AdvancestoRyot.season.$error.required && (AdvancestoRyot.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>										
									   </div>
					               </div>
									
								</div>
								<div class="col-sm-4">
									<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
						        	        <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" name="advanceCode" ng-model="AddAdvancestoRyot.advanceCode" ng-options="advancecode.advanceCode as advancecode.advanceName for advancecode in addedAdvanceNames | orderBy:'-advanceName':true" ng-change="loadRyotNames(AddAdvancestoRyot.advanceCode,AddAdvancestoRyot.season);">
														<option value="">Select Added Advances</option>
													</select>													  
												</div>
			    		            		</div>
										</div>
			            		    </div>									
								</div>
								<div class="col-sm-4">
									<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.ryotCode.$invalid && (AdvancestoRyot.ryotCode.$dirty || Addsubmitted)}">
						        	      <div class="fg-line">
    	    		    					<div class="select">
											   <select chosen class="w-100" name="ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodeData | orderBy:'-ryotcode':false" ng-model="AddAdvancestoRyot.ryotCode" ng-change="setRyotCode(AddAdvancestoRyot.ryotCode,AddAdvancestoRyot.advanceCode,AddAdvancestoRyot.season);companyadvdet(AddAdvancestoRyot.ryotCode);getRyotName(AddAdvancestoRyot.ryotCode);" data-ng-required="true">
													<option value="">Select Ryot Code</option>
											   </select>
											</div>
					    	             </div>
						 <p ng-show="AdvancestoRyot.ryotCode.$error.required && (AdvancestoRyot.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>
									   </div>
					        	    </div>
								</div>
							 </div>
							 <div class="row">
							 	<div class="col-sm-6">									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
														<label for="RyotCode">Ryot Name</label>
														<input type="text" class="form-control" placeholder="Ryot Name" readonly id="RyotCode" with-floating-label name="ryotName" ng-model="AddAdvancestoRyot.ryotName"/>
													</div>
												</div>
											</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
											 		<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.loanAmount.$invalid && (AdvancestoRyot.loanAmount.$dirty || Addsubmitted)}">
						        	                	<div class="fg-line">
														  <label for="Amount">Amount</label>  		
    		    		    					          <input type="number" class="form-control" placeholder='Amount' maxlength="10" id="Amount" with-floating-label name="loanAmount" ng-model="AddAdvancestoRyot.loanAmount" data-ng-required="true" data-ng-pattern="/^[0-9.\s]*$/" ng-blur="decimalpoint('loanAmount');" max="{{maxAdvanceAmt}}" style="text-align:right;" ng-keyup="setAmountValidation(AddAdvancestoRyot.loanAmount);"/>
					    		            	        </div>
							<p ng-show="AdvancestoRyot.loanAmount.$error.required && (AdvancestoRyot.loanAmount.$dirty || Addsubmitted)" class="help-block">Amount is Required.</p>																											
							<p ng-show="AdvancestoRyot.loanAmount.$error.pattern && (AdvancestoRyot.loanAmount.$dirty || Addsubmitted)" class="help-block">Enter Valid Amount.</p>
							<p ng-show="AdvancestoRyot.loanAmount.$error.max && (AdvancestoRyot.loanAmount.$dirty || Addsubmitted)" class="help-block">You are not permitted to give loan Rs {{difAmount}}/- to ryot, please check with administration and try again.</p>																						
													</div>							
												  </div>
			            		        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : AdvancestoRyot.extentSize.$invalid && (AdvancestoRyot.extentSize.$dirty || Addsubmitted)}">
						        	                <div class="fg-line">
														<label for="ExtentSize">Extent Size</label>
														<input type="text" placeholder='Extent Size' class="form-control" maxlength="10" id="ExtentSize" with-floating-label name="extentSize" ng-model="AddAdvancestoRyot.extentSize" data-ng-required="true" data-ng-pattern="/^[0-9.\s]*$/" ng-blur="decimalpoint('extentSize');"/>
				    		            	        </div>
					<p ng-show="AdvancestoRyot.extentSize.$error.required && (AdvancestoRyot.extentSize.$dirty || Addsubmitted)" class="help-block">Extent Size is Required.</p>
					<p ng-show="AdvancestoRyot.extentSize.$error.pattern && (AdvancestoRyot.extentSize.$dirty || Addsubmitted)" class="help-block">Enter Valid Extent Size.</p>																		
												</div>
			            		        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.advanceSubCategoryCode.$invalid && (AdvancestoRyot.advanceSubCategoryCode.$dirty || Addsubmitted)}">
					        	                <div class="fg-line">
    		        					          <div class="select">
												  	<select chosen class="w-100" name="advanceSubCategoryCode" ng-model="AddAdvancestoRyot.advanceSubCategoryCode" ng-options="advanceSubCategoryCode.id as advanceSubCategoryCode.name for advanceSubCategoryCode in advanceSubCatNames  | orderBy:'-subcategory':true" data-ng-required="false">
														<option value="">Select Sub Category</option>
													</select>
												  </div>
			                	    		    </div>
						<p ng-show="AdvancestoRyot.advanceSubCategoryCode.$error.required && (AdvancestoRyot.advanceSubCategoryCode.$dirty || Addsubmitted)" class="help-block">Sub Category is Required.</p>												
											</div>
				                    	 </div>
										</div>
									</div>
									<div class="row" ng-if="AddAdvancestoRyot.isItSeedling=='1'">
										<div class="col-sm-12" ng-if="isSeedlingAdvance!='0'">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.seedSupplierCode.$invalid && (AdvancestoRyot.seedSupplierCode.$dirty || submitted)}">	
			        	                		<div class="fg-line">
            					          			<div class="select">
										  				
														<select chosen class="w-100" name="seedSupplierCode" ng-options="seedSupplierCode.id as seedSupplierCode.ryotname for seedSupplierCode in supplierNames | orderBy:'-ryotname':true" ng-model="AddAdvancestoRyot.seedSupplierCode" ng-change="setRyotCode(AddAdvancestoRyot.ryotCode);" data-ng-required="true">
														
													<option value="">Select Seed Supplier</option>
											   </select>
										  			</div>
			                	        		</div>
	<p ng-show="AdvancestoRyot.seedSupplierCode.$error.required && (AdvancestoRyot.seedSupplierCode.$dirty || submitted)" class="help-block">Select Seed Supplier</p>												
											  </div>
			                    			</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.advanceType.$invalid && (AdvancestoRyot.advanceType.$dirty || Addsubmitted)}">												
						        	                <div class="fg-line">
    	    		    					          <div class="select">
													  	<select chosen class="w-100" name="advanceType" ng-model="AddAdvancestoRyot.advanceType" ng-options="advanceType.id as advanceType.advance for advanceType in advanceTypeNames | orderBy:'-advance':true" data-ng-required="true" ng-change="getAdvanceAmount(AddAdvancestoRyot.advanceType);getSeedlingAdvanceStatus(AddAdvancestoRyot.advanceType);">
															<option value="">Select Advance Type</option>
														</select>
													  </div>
			    		            		        </div>
<p ng-show="AdvancestoRyot.advanceType.$error.required && (AdvancestoRyot.advanceType.$dirty || Addsubmitted)" class="help-block">Advance Type is Required.</p>													
												  </div>
												</div>
			            		        	</div>
										</div>
									</div>									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
													<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.agreementNumber.$invalid && (AdvancestoRyot.agreementNumber.$dirty || Addsubmitted)}">												
												  		<div class="fg-line">
								        	                <div class="select">
													  			<select chosen class="w-100" name="agreementNumber" ng-model="AddAdvancestoRyot.agreementNumber" ng-options="agreementNumber.agreementnumber as agreementNumber.agreementnumber for agreementNumber in agreementsNames" data-ng-required="true">
																	<option value="">Select Agreements</option>
																</select>
													  		</div>
													   </div>
		<p ng-show="AdvancestoRyot.agreementNumber.$error.required && (AdvancestoRyot.agreementNumber.$dirty || Addsubmitted)" class="help-block">Agreement Number is Required.</p>													
													</div>
												</div>
			            		        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
											<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.advanceCategoryCode.$invalid && (AdvancestoRyot.advanceCategoryCode.$dirty || Addsubmitted)}">										
				        	                	<div class="fg-line">
	    	        					          <div class="select">
												  	<select chosen class="w-100" name="advanceCategoryCode" ng-model="AddAdvancestoRyot.advanceCategoryCode" ng-options="advanceCategoryCode.id as advanceCategoryCode.name for advanceCategoryCode in categoryNames  | orderBy:'-name':true" data-ng-required="false">
														<option value="">Select Category</option>
													</select>
												  </div>
			        	        	    	    </div>
		<p ng-show="AdvancestoRyot.advanceCategoryCode.$error.required && (AdvancestoRyot.advanceCategoryCode.$dirty || Addsubmitted)" class="help-block">Category is Required.</p>																									
											</div>
										</div>
			                    	</div>
										</div>
									</div>
									<div class="row" ng-if="isSeedlingAdvance!='0'">
										<div class="col-sm-12">
											<div class="input-group">
            				           		  <span class="input-group-addon">Is this Seedling Advance</span> 
				        	                  <div class="fg-line">											
												<div class="fg-line" style="margin-top:5px;">
		            					          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="isItSeedling" value="0" ng-model='AddAdvancestoRyot.isItSeedling'>
			    					            	<i class="input-helper"></i>Yes
										          </label>
				 			    		          <label class="radio radio-inline m-r-20">
				            					    <input type="radio" name="isItSeedling" value="1" ng-model='AddAdvancestoRyot.isItSeedling'>
					    				            <i class="input-helper"></i>No
							  		              </label>
			    	            	           </div>
			                	            </div>
				                    	  </div>
										</div>
									</div>
																		
								</div>
							 </div><br />
							 <div class="row" ng-if="AddAdvancestoRyot.isItSeedling=='0'">
							 	<div class="col-sm-4" ng-if="isSeedlingAdvance!='0'">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>												
												<div class="from-group floating-label-wrapper" ng-class="{ 'has-error' : AdvancestoRyot.seedlingsQuantity.$invalid && (AdvancestoRyot.seedlingsQuantity.$dirty || Addsubmittedok)}">
						        	                <div class="fg-line">
														<label for="NoofSeedling">Total No.of Seedlings</label>
														
														<input type="text" placeholder='Total No.of Seedlings' class="form-control" maxlength="10" id="NoofSeedling" with-floating-label name="seedlingsQuantity" ng-model="AddAdvancestoRyot.seedlingsQuantity" data-ng-required="true"  data-ng-pattern="/^[0-9.\s]*$/" />
				    		            	        </div>
	<p ng-show="AdvancestoRyot.seedlingsQuantity.$error.required && (AdvancestoRyot.seedlingsQuantity.$dirty || Addsubmittedok)" class="help-block">Seedlings Quantity is Required.</p>									
	<p ng-show="AdvancestoRyot.seedlingsQuantity.$error.pattern && (AdvancestoRyot.seedlingsQuantity.$dirty || Addsubmittedok)" class="help-block">Enter Valid Seedlings Quantity.</p>														
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"  ng-if="isSeedlingAdvance!='0'">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : AdvancestoRyot.totalCost.$invalid && (AdvancestoRyot.totalCost.$dirty || Addsubmittedok)}">												
						        	                <div class="fg-line">
														<label for="TotalCost">Total Cost</label>
														<input type="text" placeholder='Total Cost' class="form-control" maxlength="10" id="TotalCost" with-floating-label name="totalCost" ng-model="AddAdvancestoRyot.totalCost" data-ng-required="true"  data-ng-pattern="/^[0-9.\s]*$/"  ng-blur="decimalpoint('totalCost');" style="text-align:right"/>
				    		            	        </div>
<p ng-show="AdvancestoRyot.totalCost.$error.required && (AdvancestoRyot.totalCost.$dirty || Addsubmittedok)" class="help-block">Total Cost is Required.</p>
<p ng-show="AdvancestoRyot.totalCost.$error.pattern && (AdvancestoRyot.totalCost.$dirty || Addsubmittedok)" class="help-block">Enter Valid Total Cost.</p>													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>								
								<div class="col-sm-4"  ng-if="isSeedlingAdvance!='0'">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="from-group floating-label-wrapper" ng-class="{ 'has-error' : AdvancestoRyot.ryotPayable.$invalid && (AdvancestoRyot.ryotPayable.$dirty || Addsubmittedok)}">
						        	                <div class="fg-line">
														<label for="RyotPayable">Ryot Payable</label>
														<input type="text" placeholder='Ryot Payable In Cash' class="form-control"  maxlength="10" id="RyotPayable" with-floating-label name="ryotPayable" ng-model="AddAdvancestoRyot.ryotPayable"  data-ng-pattern="/^[0-9.\s]*$/" ng-blur="decimalpoint('ryotPayable');" style="text-align:right;"/>
				    		            	        </div>

<p ng-show="AdvancestoRyot.ryotPayable.$error.pattern && (AdvancestoRyot.ryotPayable.$dirty || Addsubmittedok)" class="help-block">Enter Valid Ryot Payable.</p>													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
							 </div>
							 <input type="hidden" name="modifyFlag" ng-model="AddAdvancestoRyot.modifyFlag">
							 <input type="hidden" name="transactionCode" ng-model="AddAdvancestoRyot.transactionCode">
							 <input type="hidden" name="loginId" ng-model="AddAdvancestoRyot.loginId">
							 <input type="hidden" name="onloadSeedlingStatus" ng-model="AddAdvancestoRyot.onloadSeedlingStatus">
							 <input type="hidden" name="isSeedlingAdvance" ng-model="AddAdvancestoRyot.isSeedlingAdvance" />
							 <hr />
							 
							 <div class="table-responsive" ng-if="AddAdvancestoRyot.isItSeedling=='0'">
							 	<table class="table table-stripped advanceyes" ng-if="isSeedlingAdvance!='0'">
									<thead>
										<tr>
											<th>Action</th>
											<th>Variety</th>
											<th>Supplied Seedlings</th>
											<th>Tray Type</th>
											<th>No.of Trays</th>									
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="SeedlingGridData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addCompanyAdvanceRows();" ng-if="SeedlingGridData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeCompanyAdvanceRows(SeedlingGridData.id);" ng-if="SeedlingGridData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>																							
											</td>
											<td>
												<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.varietyCode{{$index}}.$invalid && (AdvancestoRyot.varietyCode{{$index}}.$dirty || Addsubmitted)}">
													<select class="form-control1" name="varietyCode{{$index}}" ng-model="SeedlingGridData.varietyCode" ng-options="varietyCode.varietycode as varietyCode.variety for varietyCode in varietyNames  | orderBy:'-variety':true" data-ng-required="true">
														<option value="">Select Variety</option>
													</select>
													<p ng-show="AdvancestoRyot.varietyCode{{$index}}.$error.required && (AdvancestoRyot.varietyCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required.</p>
												</div>
											</td>
											<td>
												<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.suppliedSeeds{{$index}}.$invalid && (AdvancestoRyot.suppliedSeeds{{$index}}.$dirty || Addsubmittedok)}">												
													<input type="text" placeholder='Supplied Seedlings' class="form-control1"  maxlength="5" name="suppliedSeeds{{$index}}" ng-model="SeedlingGridData.suppliedSeeds" ng-required="true" data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="AdvancestoRyot.suppliedSeeds{{$index}}.$error.required && (AdvancestoRyot.suppliedSeeds{{$index}}.$dirty || Addsubmittedok)" class="help-block">Required.</p>
<p ng-show="AdvancestoRyot.suppliedSeeds{{$index}}.$error.pattern && (AdvancestoRyot.suppliedSeeds{{$index}}.$dirty || Addsubmittedok)" class="help-block">Invalid.</p>
																										
												</div>
											</td>
											<td>
												<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.trayType{{$index}}.$invalid && (AdvancestoRyot.trayType{{$index}}.$dirty || Addsubmittedok)}">											
												<select class="form-control1" name="trayType{{$index}}" ng-model="SeedlingGridData.trayType" ng-options="trayType.id as trayType.tray for trayType in trayTypeNames  | orderBy:'-trayname':true" data-ng-required="true"><option value="">Tray Type</option></select>
<p ng-show="AdvancestoRyot.trayType{{$index}}.$error.required && (AdvancestoRyot.trayType{{$index}}.$dirty || Addsubmittedok)" class="help-block">Required.</p>												
												</div>
											</td>
											<td>
												<div class="form-group" ng-class="{ 'has-error' : AdvancestoRyot.noOfTrays{{$index}}.$invalid && (AdvancestoRyot.noOfTrays{{$index}}.$dirty || Addsubmittedok)}">											
													<input type="text" placeholder='No.of Trays' class="form-control1"  maxlength="5" name="noOfTrays{{$index}}" ng-model="SeedlingGridData.noOfTrays" data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="AdvancestoRyot.noOfTrays{{$index}}.$error.required && (AdvancestoRyot.noOfTrays{{$index}}.$dirty || Addsubmittedok)" class="help-block">Required.</p>
<p ng-show="AdvancestoRyot.noOfTrays{{$index}}.$error.pattern && (AdvancestoRyot.noOfTrays{{$index}}.$dirty || Addsubmittedok)" class="help-block">Invalid.</p>														
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							 </div>
							 
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(AdvancestoRyot)">Reset</button>
									</div>
								</div>						 	
							 </div>	
						  </form>						 							 
						  
						  
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
