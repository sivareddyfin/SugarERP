	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="StoreStockIssueController" ng-init="getAllDepartments();onloadFunction();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Store Stock Issue</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StoreStockIssueForm" novalidate ng-submit="storeStockIssueSubmit(AddedStockIssue,StoreStockIssueForm);" >
<input type="hidden" ng-model="onloadgridLength" />
						<input type='hidden' data-ng-model="AddedStockIssue.modifFlag">
						 <div class="row">
							 <div class="col-sm-2">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StoreStockIssueForm.issueNo.$invalid && (StoreStockIssueForm.issueNo.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="issueNo">Issue No.</label>
										<input type="text" class="form-control  " data-ng-required='true'  name="issueNo" data-ng-model="AddedStockIssue.stockIssueNo" placeholder="Issue No." tabindex="1"  maxlength="15" id="issueNo" with-floating-label readonly  />
									</div>
								<p ng-show="StoreStockIssueForm.issueNo.$error.required && (StoreStockIssueForm.issueNo.$dirty || Addsubmitted)" class="help-block">Issue No. is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StoreStockIssueForm.issueDate.$invalid && (StoreStockIssueForm.issueDate.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="issueDate">Issue Date</label>
													<input type="text" class="form-control date" tabindex="2" name="issueDate" ng-required='true' data-ng-model="AddedStockIssue.issueDate" placeholder="Issue Date" maxlength="10" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/" id="issueDate" with-floating-label readonly>
												</div>
												<p ng-show="StoreStockIssueForm.issueDate.$error.required && (StoreStockIssueForm.issueDate.$dirty || Addsubmitted)" class="help-block">Issue Date required</p>
												<p ng-show="StoreStockIssueForm.issueDate.$error.pattern && (StoreStockIssueForm.issueDate.$dirty || Addsubmitted)" class="help-block">Enter Valid Issue Date</p>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.deptId.$invalid && (StoreStockIssueForm.deptId.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100"  tabindex="3" data-ng-required='true' data-ng-model='AddedStockIssue.deptId' name="deptId" ng-options="deptId.deptCode as deptId.department for deptId in DepartmentsData" ng-required='true'>
																		<option value="">Department</option>
																	</select>	
												</div>
												<p ng-show="StoreStockIssueForm.deptId.$error.required && (StoreStockIssueForm.deptId.$dirty || Addsubmitted)" class="help-block">Select Department</p>
												
											 </div>
										</div>
									</div>
								</div>											
							</div>
							
							<div class="col-sm-2">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : StoreStockIssueForm.stockRequest.$invalid && (StoreStockIssueForm.stockRequest.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="stockRequest">Stock Request</label>
									
									
										<input type="text" class="form-control  "   name="stockRequest" data-ng-model="AddedStockIssue.stockReqNo" placeholder="Stock Request" tabindex="4"  maxlength="20" id="stockRequest" with-floating-label ng-readonly="readonlyStatus" ng-blur="getStockIssueSummaryDetails(AddedStockIssue.stockReqNo);" />
									</div>
								<p ng-show="StoreStockIssueForm.stockRequest.$error.required && (StoreStockIssueForm.stockRequest.$dirty || Addsubmitted)" class="help-block">Stock Request is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<input type='hidden'  name="AddedStockIssue" ng-model="AddedStockIssue.centralStoreId" >

							
							
							
								
						 </div>
							<br>
							
							<div id="stockIssueTable" style="display:none;">
							<table style="width:100%;"  style="border-collapse:collapse; ">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Pending Qty.</th>
											<th>Issue Qty.</th>
											<th>Open Batch</th>
											<th>&nbsp;&nbsp; Batch</th>
											
											<th>Batch Qty</th>
											
											<th>MRP</th>
										</tr>
										<tr class="styletr" style="height:70px;" ng-repeat='stockIssue in stockIssueData'>


									
									
										<td >
										
										<button type="button" style="display:none;"  id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='repeatDuplicateRow($index,stockIssue.productCode,stockIssue.productName,stockIssue.uom,stockIssue.uomName,stockIssue.pendingQty,stockIssue.issuedQty,stockIssue.batchId,stockIssue.batchQty,stockIssue.mrp);'><i class="glyphicon glyphicon-repeat-sign" style="display:none;"  ></i></button>
										
<button type="button" id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='removeRow(stockIssue.id);' ng-if="stockIssue.id != '0'"><i class="glyphicon glyphicon-remove-sign"></i></button>
												</td>

											


											<td>
												<input type='hidden'  name="batchFlag{{$index}}" ng-model="stockIssue.batchFlag" >
												<input type='hidden'  name="batchSeqNo{{$index}}" ng-model="stockIssue.batchSeqNo" >
												<input type='hidden'  name="batchId{{$index}}" ng-model="stockIssue.batchId" >



											<br />
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.productCode{{$index}}.$invalid && (StoreStockIssueForm.productCode{{$index}}.$dirty || submitted)}">
											
											<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="stockIssue.productCode" ng-change="loadProductCode(stockIssue.productCode,$index);getproductIdforPopup(stockIssue.productCode,$index);" style="height:40px;" ng-required='true' >
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
       						    </select>    
   									</datalist>
											
											
											
											
											<p ng-show="StoreStockIssueForm.productCode{{$index}}.$error.required && (StoreStockIssueForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											<p ng-show="StoreStockIssueForm.productCode{{$index}}.$error.pattern && (StoreStockIssueForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.productName{{$index}}.$invalid && (StoreStockIssueForm.productName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="productName{{$index}}"  ng-model="stockIssue.productName" data-ng-required='true' placeholder="Product Name"  style="height:40px;" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" maxlength="30" readonly />
												<p ng-show="StoreStockIssueForm.productName{{$index}}.$error.required && (StoreStockIssueForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="StoreStockIssueForm.productName{{$index}}.$error.pattern && (StoreStockIssueForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											<td style="display:none;">
											<br />
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.uom{{$index}}.$invalid && (StoreStockIssueForm.uom{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uom{{$index}}"  ng-model="stockIssue.uom" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="StoreStockIssueForm.uom{{$index}}.$error.required && (StoreStockIssueForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="StoreStockIssueForm.uom{{$index}}.$error.pattern && (StoreStockIssueForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.uomName{{$index}}.$invalid && (StoreStockIssueForm.uomName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uomName{{$index}}"  ng-model="stockIssue.uomName" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="StoreStockIssueForm.uomName{{$index}}.$error.required && (StoreStockIssueForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="StoreStockIssueForm.uomName{{$index}}.$error.pattern && (StoreStockIssueForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												
												<td><br />	<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.pendingQty{{$index}}.$invalid && (StoreStockIssueForm.pendingQty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="pendingQty{{$index}}"  ng-model="stockIssue.pendingQty"  placeholder="Pending Qty" style="height:40px;" id="pendingQty{{$index}}" maxlength="10"  data-ng-required='true' data-input-mask="{mask: '00/0000'}" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" readonly />
											
											
											
											<p ng-show="StoreStockIssueForm.pendingQty{{$index}}.$error.required && (StoreStockIssueForm.pendingQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="StoreStockIssueForm.pendingQty{{$index}}.$error.pattern && (StoreStockIssueForm.pendingQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
												</div></td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.issueQty{{$index}}.$invalid && (StoreStockIssueForm.issueQty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="issueQty{{$index}}" id="issueQty{{$index}}"  ng-model="stockIssue.issuedQty"  placeholder="Issue Qty" ng-required='true'  style="height:40px;" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" maxlength="10" ng-blur="updatevalidIssueQtyBypendingQty(stockIssue.pendingQty,stockIssue.issuedQty,$index);" ng-keyup="validateIssueQtyByBatchQty(stockIssue.issuedQty,stockIssue.batchQty,$index);getBalancedIssuedQtyTtoal(stockIssue.pendingQty,stockIssue.issuedQty,$index,stockIssue.productCode);" />
											<p ng-show="StoreStockIssueForm.issueQty{{$index}}.$error.required && (StoreStockIssueForm.issueQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="StoreStockIssueForm.issueQty{{$index}}.$error.pattern && (StoreStockIssueForm.issueQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
												
												
												<td> <button type="button" class="btn btn-default btn-sm" style="height:38px;" ng-click="getbatchPopup(stockIssue.productCode,stockIssue.productName,stockIssue.uom,stockIssue.uomName,stockIssue.pendingQty,$index,stockIssue.issuedQty,AddedStockIssue.deptId);">
          <span class="glyphicon glyphicon-open"></span> Open
        </button></td>
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.batch{{$index}}.$invalid && (StoreStockIssueForm.batch{{$index}}.$dirty || submitted)}">
											<input type="text" class="form-control1" name="batch{{$index}}"   ng-model="stockIssue.batch" placeholder="Batch" ng-required='true' style="height:40px;" maxlength="10" ng-blur="duplicateProductCode(stockIssue.batch,$index);" />
											<p ng-show="StoreStockIssueForm.batch{{$index}}.$error.required && (StoreStockIssueForm.batch{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.batchQty{{$index}}.$invalid && (StoreStockIssueForm.batchQty{{$index}}.$dirty || submitted)}">
											<input type="text" class="form-control1" name="batchQty{{$index}}"   ng-model="stockIssue.batchQty" placeholder="Batch Qty" ng-required='true' style="height:40px;" maxlength="10" />
											<p ng-show="StoreStockIssueForm.batchQty{{$index}}.$error.required && (StoreStockIssueForm.batchQty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : StoreStockIssueForm.mrp{{$index}}.$invalid && (StoreStockIssueForm.mrp{{$index}}.$dirty || submitted)}">		<input type="text" class="form-control1" id name="mrp{{$index}}"  ng-model="stockIssue.mrp"  placeholder="MRP" ng-required='true'  style="height:40px;" readonly maxlength="12"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="getMRPid{{$index}}" ng-blur= "getMRP(stockIssue.mrp,stockIssue.quantity,$index);" /><p ng-show="StoreStockIssueForm.mrp{{$index}}.$error.required && (StoreStockIssueForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	<p ng-show="StoreStockIssueForm.mrp{{$index}}.$error.pattern && (StoreStockIssueForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											
										</tr>
										
										
														
									</thead>
							</table>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(StoreStockIssueForm)">Reset</button>
									</div>
								</div>						 	
							 </div>
							</div>
						 					
											
						</form>
									 							 
						
					<!------------------form end---------->	
						

		<!--<form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">
					   				<div class="row">
										<div class="col-sm-4">
											<div class="input-group">
											&nbsp;&nbsp;&nbsp;&nbsp;
																<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
																 <div class="form-group floating-label-wrapper">
													<div class="fg-line">
													<label for="productCodepopup">Product Code</label>
														
														<input type="text" class="form-control "  autofocus data-ng-required='true'  name="productCode" data-ng-model="productCode" placeholder="Product Code" tabindex="1"  id="productCodepopup" with-floating-label readonly  />
														
														<input type="hidden" name="productName" data-ng-model="productName" />
														<input type="hidden" name="uom" data-ng-model="uom" />
														<input type="hidden" name="uomName" data-ng-model="uomName" />
															<input type="hidden" name="issuedQty" data-ng-model="issuedQty" />
															
													</div>
													
													
													
															  </div>
														  </div>
										
										</div>
										
										
										
										
										
										
										<div class="col-sm-4"><input type="hidden" ng-model="productId" name="productId{{$index}}" /></div>
										<div class="col-sm-4"><input type="text"  ng-model="popupId"/></div>
										
										<button  class="btn btn-primary btn-sm m-t-10" ng-click="loadStockIssue();">Load </button>
									</div>						  
						  	  					
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
												<th> &nbsp;&nbsp;Select</th>
												<th>Batch ID</th>	
												<th>Batch</th>
												<th>Expiry Date</th>
												<th>MRP</th>
												<th>Quantity</th>
												
												<th>Mfr. Date</th>
												
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="batchDetails in productBatchData">
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label class="checkbox checkbox-inline m-r-20" style="margin-top:-10px;" >
												<input type="checkbox"  class="checkbox" name="batchFlag{{$index}}"  ng-model="batchDetails.batchFlag" ng-click="batchDatapush(batchDetails.batchFlag,batchDetails.batch,batchDetails.expiryDate,batchDetails.mrp,batchDetails.quantity,batchDetails.mfgDate,$index,batchDetails.batchId);"    ><i class="input-helper" ></i></label></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.batchId" name="batchId{{$index}}" /></td>
											<td><input type="text" class="form-control1"  readonly  ng-model="batchDetails.batch" name="batch{{$index}}"/></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.expiryDate" name="expiryDate{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mrp" name="mrp{{$index}}" /></td>
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.quantity" name="quantity{{$index}}" /></td>
											
											<td><input type="text" class="form-control1" readonly  ng-model="batchDetails.mfgDate" name="mfrDate{{$index}}" /></td>
											
											</tr>
											
											
										</tbody>	
								 </table>
							</div>	  
						</div>
						
						</div>
									 							 
				        </div>
			    	</div>
					   </div>
					   <br />
					   
					   				
					 </form>-->

					<footer data-ng-include="'template/batchDetailsPopup.jsp'"  style='font-weight:bold;'></footer>
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
