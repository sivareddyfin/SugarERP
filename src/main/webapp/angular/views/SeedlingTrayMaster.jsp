	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeedlingTrayMaster" ng-init="loadTrayCode();loadAddedSeedlingTrays();addFormField(); loadSeedlingTrayNames();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Tray Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>		
					 <div class="row">
								<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="fg-line">

										<select chosen class="w-100"   data-ng-required='true' data-ng-model='seedlingTray' name="seedlingTray" ng-options="seedlingTray.id as seedlingTray.tray for seedlingTray in seedlingTrayNames | orderBy:'-seedlingTray':true" ng-change="loadAddedSeedlingTrays();" >
										<option value="">Select Added Seedling Trays</option>
						        		</select>
							</div>        		           
		                	
			                
			                    </div>			                    
			                </div>
							</div><br>
							<br />			
					 	<form name="SeedlingTrayMasterForm" novalidate ng-submit="AddSeedlings(AddSeedlig,SeedlingTrayMasterForm);">
							<input type="hidden" name="screenName" ng-model="AddSeedlig.screenName" />
							<input type="hidden" name="modifyFlag" ng-model="AddSeedlig.modifyFlag" />
							<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="trayCode">Tray Code</label>
            							          <input type="text" class="form-control" placeholder="Tray Code" maxlength="10" readonly ng-model='AddSeedlig.trayCode' id="trayCode" with-floating-label>
			            		    	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
	            					            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.trayName.$invalid && (SeedlingTrayMasterForm.trayName.$dirty || Addsubmitted)}">												
					       	        	        <div class="fg-line">
												<label for="trayName">Tray Name</label>
            								          <input type="text" class="form-control" placeholder="Tray Name"   maxlength="25" data-ng-model="AddSeedlig.trayName" name="trayName" data-ng-required='true' ng-blur="spacebtw('trayName');" id="trayName" with-floating-label >
			        		       	        	</div>
<p ng-show="SeedlingTrayMasterForm.trayName.$error.required && (SeedlingTrayMasterForm.trayName.$dirty || Addsubmitted)" class="help-block">Tray Name is required</p>												
</div>												
				                	    	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
		            				            <span class="input-group-addon">Tray Type :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
            							          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="inlineRadioOptions" value="0" checked="checked" ng-model="AddSeedlig.trayType">
			    			    		        	<i class="input-helper"></i>Disposable
								        		  </label>
						 			              <label class="radio radio-inline m-r-20" style="margin-left:-10px;">
						            			    <input type="radio" name="inlineRadioOptions" value="1" ng-model="AddSeedlig.trayType">
			    						            <i class="input-helper"></i>Non-Disposable
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.lifeOfTray.$invalid && (SeedlingTrayMasterForm.lifeOfTray.$dirty || Addsubmitted)}" ng-if="AddSeedlig.trayType!='0'">																								
					        	                <div class="fg-line">
												<label for="lit">Life of Tray(Times)</label>
            							          <input type="text" class="form-control" placeholder="Life of Tray(Times)" maxlength="2" data-ng-model="AddSeedlig.lifeOfTray" ng-readonly="AddSeedlig.trayType=='0'" name="lifeOfTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-blur="spacebtw('lifeOfTray');"  id="lit" with-floating-label>										
			            		    	        </div>
<p ng-show="SeedlingTrayMasterForm.lifeOfTray.$error.required && (SeedlingTrayMasterForm.lifeOfTray.$dirty || Addsubmitted)" class="help-block">Life of Tray is required</p>
<p ng-show="SeedlingTrayMasterForm.lifeOfTray.$error.pattern && (SeedlingTrayMasterForm.lifeOfTray.$dirty || Addsubmitted)" class="help-block">Enter Valid Life of Tray</p>																										
</div>												
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.lifeOfTray.$invalid && (SeedlingTrayMasterForm.lifeOfTray.$dirty || Addsubmitted)}" ng-if="AddSeedlig.trayType=='0'">																								
					        	                <div class="fg-line">
												<label for="lit1">Life of Tray(Times)</label>
            							          <input type="text" class="form-control" placeholder="Life of Tray(Times)" maxlength="2" data-ng-model="AddSeedlig.lifeOfTray" ng-readonly="AddSeedlig.trayType=='0'" name="lifeOfTray" ng-blur="spacebtw('lifeOfTray');"  id="lit1" with-floating-label >										
			            		    	        </div>
</div>												

			                    			</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
    		        				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.noOfSeedlingPerTray.$invalid && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)}" ng-if="AddSeedlig.trayType!='0'">																																				
					        	                <div class="fg-line">
												<label for="seeddling">No.Of Seedlings Per Tray</label>
            							          <input type="text" class="form-control disposble" placeholder="No.Of Seedlings Per Tray" maxlength="2" ng-model="AddSeedlig.noOfSeedlingPerTray" ng-readonly="AddSeedlig.trayType=='0'" name="noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-blur="spacebtw('noOfSeedlingPerTray');" id="seeddling" with-floating-label>
					                	        </div>
<p ng-show="SeedlingTrayMasterForm.noOfSeedlingPerTray.$error.required && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)" class="help-block">Seedlings Per Tray is required</p>																																						
<p ng-show="SeedlingTrayMasterForm.noOfSeedlingPerTray.$error.pattern && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)" class="help-block">Enter Valid Seedlings Per Tray</p>																																						
</div>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.noOfSeedlingPerTray.$invalid && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)}" ng-if="AddSeedlig.trayType=='0'">																																				
					        	                <div class="fg-line">
												<label for="noOfSeedlingPerTray">No.of Cavities/tray</label>
            							          <input type="text" class="form-control disposble" placeholder="No.of Cavities/tray" maxlength="2" ng-model="AddSeedlig.noOfSeedlingPerTray" ng-readonly="AddSeedlig.trayType=='1'" name="noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-blur="spacebtw('noOfSeedlingPerTray')"  id="noOfSeedlingPerTray" with-floating-label>
												   </div>
<p ng-show="SeedlingTrayMasterForm.noOfSeedlingPerTray.$error.required && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)" class="help-block">Seedlings Per Tray is required</p>												  
<p ng-show="SeedlingTrayMasterForm.noOfSeedlingPerTray.$error.pattern && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)" class="help-block">Enter Valid Seedlings Per Tray</p>												  
					                	       
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.noOfSeedlingPerTray.$invalid && (SeedlingTrayMasterForm.noOfSeedlingPerTray.$dirty || Addsubmitted)}" ng-if="AddSeedlig.trayType=='0'">	
</div>												
					                    	</div>
										</div>
									</div>
								</div>								
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>												
					        	                <div class="fg-line">
												<label for="description">Description</label>
        		    					          <input type="text" class="form-control" placeholder="Description"  maxlength="50" ng-model="AddSeedlig.description" ng-blur="spacebtw('description');" id="description" with-floating-label>
			    		            	        </div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" checked="checked" ng-model="AddSeedlig.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            			    <input type="radio" name="status" value="1" ng-model="AddSeedlig.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>
										</div>
									</div>
								</div>
								
								
							</div>
							
							<br />
							<br />
							
							
							<div class="row" >
							  
							  <div class="col-md-12">		
						 	 <div class="table-responsive"  align="center">
							 <table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th><span>Action</span></th>
												<th width="150"><span>From Distance</span></th>
												<th width="150"><span>To Distance</span></th>
												<th width="150"><span>Amount(Rs)</span></th>
												
											</tr>
										</thead>										
										<tbody>
											<tr ng-repeat='SeedlingData in data'>
												<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="SeedlingData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="SeedlingData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<!--<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="SeedlingData.branchCode"/>-->
												</td>
												<!--<td>
		<input type="text" class="form-control1" placeholder="From" name="id" readonly data-ng-model="SeedlingData.id" style="margin-top:-10px;"/>
												</td>-->
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="number" class="form-control1" placeholder="Distance" id="from{{SeedlingData.id}}" name="distanceFrom{{SeedlingData.id}}" data-ng-model="SeedlingData.distanceFrom" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" step="0.01"   max="{{SeedlingData.distanceTo}}" ng-keyup="setValidation(SeedlingData.id)"/>
	<p ng-show="SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.distanceFrom{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
	<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{SeedlingData.id}}">From Distance Must Be Greater Than Previous To Distance</p>
</div>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$dirty || submitted)}">																																																											
<input type="number" class="form-control1" placeholder="Distance" name="distanceTo{{SeedlingData.id}}" step="0.01" min="{{SeedlingData.distanceFrom}}" id="to{{SeedlingData.id}}"  data-ng-model="SeedlingData.distanceTo" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-keyup="setoValidation(SeedlingData.id)"/>
	<p ng-show="SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
	<p ng-show="SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$error.min  && (SeedlingTrayMasterForm.distanceTo{{SeedlingData.id}}.$dirty || submitted)" class="help-block">To Distance must be greater than From Distance</p>
	<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{SeedlingData.id}}">Invalid</p>		
	<!--<p class="help-block duplicate{{SeedlingData.id}}" style="color:#FF0000; display:none;" ng-if="SeedlingData.branchName!=null">Branch Name Already Exist.</p>-->																																																				 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Amount(Rs)" name="trayCharge{{SeedlingData.id}}" ng-model="SeedlingData.trayCharge" data-ng-required="true"   ng-blur="decimalpointgrid('trayCharge',$index)" />
	<p ng-show="SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	
													
	<!--<p ng-show="SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>-->																																																																
</div>												 
												</td>
												
												
											</tr>

										</tbody>
									</table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							
							
							
							
													
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SeedlingTrayMasterForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>			 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!---------Table Design Start--------->
					<!--<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Seedling Trays</b></h2></div>-->
					<!--<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
								  	<form name="UpdateSeedlingTrays" novalidate>					
							        <table ng-table="SeedlingTrayMaster.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Tray Name</span></th>
													<th><span>Tray Type</span></th>
													<th><span>Life of Tray</span></th>
													<th><span>Seedlings Per Tray</span></th>
													<th><span>Description</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
										
								        <tr ng-repeat="UpdateTrays in AddedSeedlingTraysData"  ng-class="{ 'active': UpdateTrays.$edit }">
                		    				<td>
											
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateTrays.$edit" ng-click="UpdateTrays.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="UpdateTrays.$edit" ng-click="UpdateAddedSeedlingTrays(UpdateTrays);UpdateTrays.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateTrays.$edit">{{UpdateTrays.trayName}}</span>
					    		                <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.trayName.$invalid && (UpdateSeedlingTrays.trayName.$dirty || submitted)}">																																																
													<input class="form-control1" type="text" ng-model="UpdateTrays.trayName" maxlength="10" placeholder='Tray Name' name="trayName" data-ng-required='true'  ng-blur="spacebtwgrid('trayName',$index)"/>
<p ng-show="UpdateSeedlingTrays.trayName.$error.required && (UpdateSeedlingTrays.trayName.$dirty || submitted)" class="help-block">is required</p>													
</div>													
												</div>
					            		      </td>
							                  <td>
							                      <span ng-if="!UpdateTrays.$edit">
												  	<span ng-if="UpdateTrays.trayType=='0'">Disposable </span>	  	
													<span ng-if="UpdateTrays.trayType=='1'">Non-Disposable </span>	  	
												  </span>
        		            					  <div ng-if="UpdateTrays.$edit">
												  	<div class="form-group">
													
													 <select class="form-control1" name="trayType" data-ng-model='UpdateTrays.trayType'>
													 	<option value="0" ng-selected="UpdateTrays.trayType=='0'">Disposable</option>
														<option value="1" ng-selected="UpdateTrays.trayType=='1'">Non-Disposable</option>
													 </select> 	
													 </div>
												  </div>
							                  </td>
							                  <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.lifeOfTray}}</span>
					            		           <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.lifeOfTray{{$index}}.$invalid && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType=='0'" ng-init="UpdateTrays.lifeOfTray==''">												   													  	
													  <input type="text" class="form-control1" placeholder="Life of Tray(Times)" ng-readonly="UpdateTrays.trayType=='0'" maxlength="2" data-ng-model="UpdateTrays.lifeOfTray" name="lifeOfTray{{$index}}" >
													  </div>													 
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.lifeOfTray{{$index}}.$invalid && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType!='0'">												   													  	
													  <input type="text" class="form-control1" placeholder="Life of Tray(Times)"  maxlength="2" data-ng-model="UpdateTrays.lifeOfTray" name="lifeOfTray{{$index}}" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.lifeOfTray{{$index}}.$error.required && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.lifeOfTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.lifeOfTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>  													
													  
</div>													  

													</div>
							                  </td>
					    		              <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.noOfSeedlingPerTray}}</span>
					                    		   <div ng-if="UpdateTrays.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$invalid && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType=='0'">												   
													  <input type="text" class="form-control1" placeholder="No.of Seedlings per tray"  maxlength="2" ng-readonly="UpdateTrays.trayType=='1'" name="noOfSeedlingPerTray{{$index}}" data-ng-model="UpdateTrays.noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.required && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
</div>
<div class="form-group" ng-class="{ 'has-error' : UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$invalid && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)}" ng-if="UpdateTrays.trayType!='0'">												   
													  <input type="text" class="form-control1" placeholder="No.of Seedlings per tray"  maxlength="2" name="noOfSeedlingPerTray{{$index}}" data-ng-model="UpdateTrays.noOfSeedlingPerTray" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/">
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.required && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">is required</p>													
<p ng-show="UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$error.pattern && (UpdateSeedlingTrays.noOfSeedlingPerTray{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>  
													  
</div>													  
												    </div>
							                  </td>
											  <td>
                    							   <span ng-if="!UpdateTrays.$edit">{{UpdateTrays.description}}</span>
					                    		   <div ng-if="UpdateTrays.$edit">
												   	<div class="form-group">
													  <input type="text" class="form-control1" placeholder="Description" maxlength="50" name="description{{$index}}" data-ng-model="UpdateTrays.description"  ng-blur="spacebtwgrid('description',$index)">
													  </div>
												    </div>
							                  </td>
											  <td>
                    							   <span ng-if="!UpdateTrays.$edit">
												   	  <span ng-if="UpdateTrays.status=='0'">Active</span>
													  <span ng-if="UpdateTrays.status=='1'">Inactive</span>
												   </span>
					                    		   <div ng-if="UpdateTrays.$edit">
												   	<div class="form-group">
													   <select class="form-control1" name="status{{$index}}" data-ng-model='UpdateTrays.status'>
													 		<option value="0" ng-selected="UpdateTrays.status=='0'">Active</option>
															<option value="1" ng-selected="UpdateTrays.status=='1'">Inactive</option>
													   </select>
													   </div>
												    </div>
							                  </td>														  			
							             </tr>
										 </tbody>
								         </table>		
									 </form>
								</div>
							</section>					 
							     </div>
							</div>
						</div>-->
					<!---------Table Design End----------->
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
