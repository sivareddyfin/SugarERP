	<script type="text/javascript">	
		$('.autofocus').focus();	
	  	  	  
    </script>
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
		<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="CompanyAdvanceMaster" ng-init="loadCompanyAdvanceCode();addCompanyAdvanceAmount(); loadAdvanceNames();addCompanySeedlingAdvanceAmount();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Company Advance Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>


						<div class="row">
								<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="fg-line">

										<select chosen class="w-100"   data-ng-required='true' data-ng-model='advance' name="advance" ng-options="advance.id as advance.advance for advance in advanceNames | orderBy:'-advance':true" ng-change="loadAddedAdvancessonChange(advance);">
										<option value="">Select Advances</option>
						        		</select>
							</div>        		           
		                	
			                
			                    </div>			                    
			                </div>
							</div><br>
							

					 		<form name="AdvanceMasterForm" ng-submit="CompanyAdvanceSubmit(AddedCompanyAdvance,AdvanceMasterForm)" novalidate>
					         <input type="hidden" name="modifyFlag" ng-model="AddedCompanyAdvance.modifyFlag" />
							 <input type="hidden" name="screenName" ng-model="AddedCompanyAdvance.screenName" />
							<div class="row">							
								<div class="col-sm-6">
										<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="advanceCode">Advance Code</label>
        		    					          <input type="text" class="form-control autofocus" placeholder="Advance Code" maxlength="10" tabindex="1" readonly name="advanceCode" data-ng-model='AddedCompanyAdvance.advanceCode' ng-blur="spacebtw('advanceCode');" id="advanceCode" with-floating-label   >
			    		            	        </div>
			                    			</div>
										</div>
									</div><br>
										
										<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.maximumAmount.$invalid && (AdvanceMasterForm.maximumAmount.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="maxamt">Maximum Amount</label>
    	        								         <input type="text" class="form-control" placeholder="Maximum Amount" tabindex="3"  maxlength="25"  name="maximumAmount" data-ng-model='AddedCompanyAdvance.maximumAmount' data-ng-required='true' ng-blur="spacebtw('maximumAmount'); decimalpoint('maximumAmount');" id="maxamt" with-floating-label style="text-align:right" data-ng-pattern="/^[0-9.\d]*$/"/>														 
				                				    </div>
								<p ng-show="AdvanceMasterForm.maximumAmount.$error.required && (AdvanceMasterForm.maximumAmount.$dirty || Addsubmitted)" class="help-block">Maximum Amount is required</p>
								<p ng-show="AdvanceMasterForm.maximumAmount.$error.pattern && (AdvanceMasterForm.maximumAmount.$dirty || Addsubmitted)" class="help-block">Enter Valid Maximum Amount</p>							
																					
												</div>
					                    	</div>
										</div>
									</div>
										<div class="row">
											<div class="col-sm-12">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Advance Mode</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="advanceMode" value="0"   data-ng-model="AddedCompanyAdvance.advanceMode" >
			    			            					<i class="input-helper"></i>Returnable
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="advanceMode" value="1"   data-ng-model='AddedCompanyAdvance.advanceMode'>
			    				            			<i class="input-helper"></i>Subsidy
					  		              				</label>
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
			                    	<div class="input-group">
										<div class="form-group" >
			                        		<div class="fg-line nulvalidate">
									<br />
									 			 <label class="radio radio-inline m-r-20"><b>Subsidy in % or Amount</b></label>	
            					          		<label class="radio radio-inline m-r-20">
								           			 <input type="radio" name="isAmount" value="0" data-ng-model="AddedCompanyAdvance.isAmount" ng-disabled="AddedCompanyAdvance.subsidyPart=='0'" ng-change='ChangePlaceholder(AddedCompanyAdvance.isAmount);'>
			    			            			<i class="input-helper"></i>Percentage(%)
								        	 	 </label>
				 			              		<label class="radio radio-inline m-r-20">
				            			  		  <input type="radio" name="isAmount" value="1"   data-ng-model='AddedCompanyAdvance.isAmount'  ng-disabled="AddedCompanyAdvance.subsidyPart=='0'" ng-change='ChangePlaceholder(AddedCompanyAdvance.isAmount);'>
			    				           		 <i class="input-helper"></i>Amount (Rs)
					  		             		 </label>
									
			                        		</div>
										</div>
			                   	 	</div>			                    
			                	</div>
							</div>
							<div class="row">
									<div class="col-sm-12">
			                    		<div class="input-group">
											<div class="form-group" >
			                        			<div class="fg-line nulvalidate">
									<br />
													<label class="radio radio-inline m-r-20"><b>Advance Applied for</b></label>	
            					          			<label class="radio radio-inline m-r-20">
								            			<input type="radio" name="advanceFor" value="0" checked="checked"  data-ng-model="AddedCompanyAdvance.advanceFor" >
			    			            				<i class="input-helper"></i>Extent
								         			 </label>
				 			             			 <label class="radio radio-inline m-r-20">
				            			   				 <input type="radio" name="advanceFor" value="1"   data-ng-model='AddedCompanyAdvance.advanceFor'>
			    				            			<i class="input-helper"></i>Agreement
					  		             			 </label>
										  			<label class="radio radio-inline m-r-20">
				            			   				 <input type="radio" name="advanceFor" value="2"   data-ng-model='AddedCompanyAdvance.advanceFor'>
			    				           			 <i class="input-helper"></i>Ryot
					  		              			</label>
			                        			</div>
											</div>
			                   		 	</div>			                    
			                		</div>
								</div>
								
								
								
								
								<div class="row">
									<div class="col-sm-12">
			                    		<div class="input-group">
										<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0"   data-ng-model="AddedCompanyAdvance.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"   data-ng-model='AddedCompanyAdvance.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              				</label>
			                        				</div>
												</div>
			                    			</div>			                    
			                			</div>
									</div>
									<div class="row">
											<div class="col-sm-12">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Is Kind or Amount</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="isSeedlingAdvance" value="0"   data-ng-model="AddedCompanyAdvance.isSeedlingAdvance" ng-change="seedlingValid(AddedCompanyAdvance.isSeedlingAdvance);" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="isSeedlingAdvance" value="1"   data-ng-model='AddedCompanyAdvance.isSeedlingAdvance' ng-change="seedlingValid(AddedCompanyAdvance.isSeedlingAdvance);" >
			    				            			<i class="input-helper"></i>No
					  		              				</label>
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
							</div>
									
								</div>
								<input type="hidden" ng-model="AddedCompanyAdvance.modifyFlag" />
								<input type="hidden" ng-model="AddedCompanyAdvance.onloadIsKindorAmt" />
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.advance.$invalid && (AdvanceMasterForm.advance.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="advance">Calculate Interest Till</label>
    	        								         <input type="text" class="form-control" placeholder="Advance"  maxlength="25" tabindex="2" name="advance" data-ng-model='AddedCompanyAdvance.advance' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.-\s]*$/" ng-blur="spacebtw('advance');validateDup(AddedCompanyAdvance.advanceCode);"  id="advance" with-floating-label>														 
				                				    </div>
								<p ng-show="AdvanceMasterForm.advance.$error.required && (AdvanceMasterForm.advance.$dirty || Addsubmitted)" class="help-block">Advance Name is required</p>
								<p ng-show="AdvanceMasterForm.advance.$error.pattern  && (AdvanceMasterForm.advance.$dirty || Addsubmitted)" class="help-block">Enter Valid Advance Name</p>											
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedCompanyAdvance.advance!=null">Advance Name Already Exist.</p>		
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.glCode.$invalid && (AdvanceMasterForm.glCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="glcode">GL Code</label>
    	        								         <input type="text" class="form-control" placeholder="Gl Code"  maxlength="25" tabindex="4" name="glCode" data-ng-model='AddedCompanyAdvance.glCode' data-ng-required='true' ng-blur="spacebtw('glCode');"  id="glcode" with-floating-label >														 
				                				    </div>
								<p ng-show="AdvanceMasterForm.glCode.$error.required && (AdvanceMasterForm.glCode.$dirty || Addsubmitted)" class="help-block">Gl Code is required</p>
								<!--<p ng-show="AdvanceMasterForm.glCode.$error.pattern  && (AdvanceMasterForm.glCode.$dirty || Addsubmitted)" class="help-block">Enter Valid Gl Code.</p>!-->													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
													<br />
									  					<label class="radio radio-inline m-r-20"><b>Subsidy Part</b></label>	
            					        				<label class="radio radio-inline m-r-20">
								           	 				<input type="radio" name="subsidyPart" value="0" checked="checked"  data-ng-model="AddedCompanyAdvance.subsidyPart" ng-disabled="AddedCompanyAdvance.advanceMode=='0'">
			    			            					<i class="input-helper"></i>Full
								          				</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    				<input type="radio" name="subsidyPart" value="1"   data-ng-model='AddedCompanyAdvance.subsidyPart' ng-disabled="AddedCompanyAdvance.advanceMode=='0'">
			    				            			<i class="input-helper"></i>Partial
					  		              			</label>
			                        			</div>
											</div>
			                   	 		</div>			                    
			                		</div>
							   	</div>
								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.percent.$invalid && (AdvanceMasterForm.percent.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="percent">{{placeholder}}</label>
    	        								         <input type="text" class="form-control" placeholder="{{placeholder}}"  maxlength="25" tabindex="5" name="percent" data-ng-model='AddedCompanyAdvance.percent'  data-ng-pattern="/^[0-9.]+(\.[0-9]{1,2})?$/" ng-disabled="AddedCompanyAdvance.subsidyPart=='0'" ng-blur="spacebtw('percent');"  id="percent" with-floating-label style="text-align:right;">														    	        							         
				                				    </div>
								<p ng-show="AdvanceMasterForm.percent.$error.required && (AdvanceMasterForm.percent.$dirty || Addsubmitted)" class="help-block"> required</p>
								<p ng-show="AdvanceMasterForm.percent.$error.pattern  && (AdvanceMasterForm.percent.$dirty || Addsubmitted)" class="help-block">Enter Valid Percent</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<!--<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.seedlingCost.$invalid && (AdvanceMasterForm.seedlingCost.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="ces">Percent</label>
    	        								         <input type="text" class="form-control" placeholder="Cost of Each Seedling"  maxlength="25" tabindex="6" name="seedlingCost" data-ng-model='AddedCompanyAdvance.seedlingCost' data-ng-required='requiredstatus' ng-disabled="AddedCompanyAdvance.isSeedlingAdvance=='1'" ng-blur="spacebtw('seedlingCost'); decimalpoint('seedlingCost'); " id="ces" with-floating-label style="text-align:right">														 
														 
				                				    </div>
								<p ng-show="AdvanceMasterForm.seedlingCost.$error.required && (AdvanceMasterForm.seedlingCost.$dirty || Addsubmitted)" class="help-block">Seedling Cost is required.</p>
								<p ng-show="AdvanceMasterForm.seedlingCost.$error.pattern  && (AdvanceMasterForm.seedlingCost.$dirty || Addsubmitted)" class="help-block">Enter Valid Seedling Cost.</p>													
												</div>												
					                    	</div>
										</div>
									</div>-->
									<br />
									<div class="row">
										<div class="col-sm-12">
			                   			 <div class="input-group">
											<div class="form-group" >
			                        			<div class="fg-line nulvalidate">
													<br />
									  				<label class="radio radio-inline m-r-20"><b>Is Interest Applicable</b></label>	
            					          			<label class="radio radio-inline m-r-20">
								            			<input type="radio" name="interestApplicable" value="0"   data-ng-model="AddedCompanyAdvance.interestApplicable" ng-change="tillDateHide(AddedCompanyAdvance.interestApplicable);">
			    			            				<i class="input-helper"></i>Yes
								          			</label>
				 			              			<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="interestApplicable" value="1"   data-ng-model='AddedCompanyAdvance.interestApplicable' ng-change="tillDateHide(AddedCompanyAdvance.interestApplicable);">
			    				            			<i class="input-helper"></i>No
					  		              			</label>
			                        			</div>
											</div>
			                    		</div>			                    
			                		</div>
								</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.description.$invalid && (AdvanceMasterForm.description.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="description">Description</label>
    	        								         <input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="8" name="description" data-ng-model='AddedCompanyAdvance.description' ng-blur="spacebtw('description');"  id="description" with-floating-label>														 
				                				    </div>
																		
												</div>
					                    	</div>
										</div>
									</div>									
								</div>
							</div><br /><hr />
															
								<div class="row" >
							  <div class="col-md-1"></div>
							  <div class="col-md-10">		
						 	 <div class="table-responsive"  align="center">
							   
							   <table class="table table-striped table-vmiddle">
							 	   <thead>
								      <tr>
                		    			 <th>Add</th>
							             <th>From Amount</th>
		                    			 <th>To Amount</th>
										 <th>% of Interest</th>
            						  </tr>											
									</thead>
									<tbody>
										<tr ng-repeat="CompanyAdvancesData in data">
											<td>
<button type="button" class="btn btn-primary" ng-click='addCompanyAdvanceAmount();' ng-if="CompanyAdvancesData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
<button type="button" class="btn btn-primary" ng-click='removeRow(CompanyAdvancesData.id);' ng-if="CompanyAdvancesData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>									
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$invalid && (AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$dirty || submitted)}">																																																																						
<input type="number" class="form-control1 autofocus" placeholder="From Amount" maxlength="10" id="from{{CompanyAdvancesData.id}}"
 name="fromAmount{{CompanyAdvancesData.id}}" data-ng-model='CompanyAdvancesData.fromAmount' data-ng-required='interestvalid'
 ng-pattern="/^[0-9.\s]*$/" step="0.01" autofocus max="{{CompanyAdvancesData.toAmount}}" ng-keyup="setValidation(CompanyAdvancesData.id)" 
 ng-disabled="AddedCompanyAdvance.interestApplicable=='1'" style="text-align:right;" ng-blur="decimalpointgrid('fromAmount',$index)"/>
		
		
	<p ng-show="AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$error.required && (AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$error.pattern  && (AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
	<p ng-show="AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$error.max  && (AdvanceMasterForm.fromAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{CompanyAdvancesData.id}}" ng-if="CompanyAdvancesData.fromAmount">FromAmount Must Be Greater Than Previous ToAmount</p>
</div>																					

											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$invalid && (AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$dirty || submitted)}">																																																																																	
	<input type="number" class="form-control1" placeholder="To Amount" maxlength="10"  name="toAmount{{CompanyAdvancesData.id}}" data-ng-model='CompanyAdvancesData.toAmount' data-ng-required='interestvalid' ng-pattern="/^[0-9.\s]*$/" step="0.01" min="{{CompanyAdvancesData.fromAmount}}" id="to{{CompanyAdvancesData.id}}" ng-keyup="setoValidation(CompanyAdvancesData.id)" ng-disabled="AddedCompanyAdvance.interestApplicable=='1'" style="text-align:right;" ng-blur="decimalpointgrid('toAmount',$index)"/>
	<p ng-show="AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$error.required && (AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$error.pattern  && (AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block" ng-if="CompanyAdvancesData.toAmount">Invalid</p>																																																																		
<p ng-show="AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$error.min  && (AdvanceMasterForm.toAmount{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">To Amount must be greater than From Amount</p>												
<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{CompanyAdvancesData.id}}" ng-if="CompanyAdvancesData.toAmount">Invalid</p>


</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$invalid && (AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$dirty || submitted)}">																																																																																												
	<input type="text" class="form-control1"  placeholder="% of Interest" maxlength="10"  name="interestRate{{CompanyAdvancesData.id}}" data-ng-model='CompanyAdvancesData.interestRate' data-ng-required='interestvalid' ng-pattern="/^[0-9.\s]*$/" ng-disabled="AddedCompanyAdvance.interestApplicable=='1'" style="text-align:right;" ng-blur="decimalpointgrid('interestRate',$index)"/>
	<p ng-show="AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$error.required && (AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$error.pattern  && (AdvanceMasterForm.interestRate{{CompanyAdvancesData.id}}.$dirty || submitted)" class="help-block" >Invalid</p>																																																																		
												
</div>												
											</td>
										</tr>
									</tbody>		
							 </table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div><br />
						
									<div ng-if="AddedCompanyAdvance.isSeedlingAdvance== 0">				
										<div class="row" >
							  <div class="col-md-1"></div>
							  <div class="col-md-10">		
						 	 <div class="table-responsive"  align="center">
							   
							   <table class="table table-striped table-vmiddle">
							 	   <thead>
								      <tr>
                		    			 <th>Add</th>
							             <th>From Distance</th>
		                    			 <th>To Distance</th>
										 <th>Cost of Each Unit</th>
            						  </tr>											
									</thead>
									<tbody>
										<tr ng-repeat="CompanyAdvancesSeedlingData in Seedlingdata">
											<td>
									<button type="button" class="btn btn-primary" ng-click='addCompanySeedlingAdvanceAmount();' ng-if="CompanyAdvancesSeedlingData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
									<button type="button" class="btn btn-primary" ng-click='removeSeedlingRow(CompanyAdvancesSeedlingData.id);' ng-if="CompanyAdvancesSeedlingData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>									
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$invalid && (AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)}">																																																																						
<input type="number" class="form-control1 autofocus" placeholder="From Distance" maxlength="10" id="fromDist{{CompanyAdvancesSeedlingData.id}}" 
name="fromDistance{{CompanyAdvancesSeedlingData.id}}" data-ng-model='CompanyAdvancesSeedlingData.fromDistance' 
data-ng-required='SeedlingAmtRow' ng-pattern="/^[0-9.\s]*$/" step="0.01" autofocus max="{{CompanyAdvancesSeedlingData.toAmount}}" 
ng-keyup="setSeedValidation(CompanyAdvancesSeedlingData.id)"  style="text-align:right;" 
ng-blur="decimalpointseed('fromDistance',$index)"/>
		
		
	<p ng-show="AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$error.required && (AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$error.pattern  && (AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
	<p ng-show="AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$error.max  && (AdvanceMasterForm.fromDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
<p class="help-block errormessage" style="color:#FF0000; display:none;" id="errormessage{{CompanyAdvancesSeedlingData.id}}" ng-if="CompanyAdvancesSeedlingData.fromDistance">FromDistance Must Be Greater Than Previous ToDistance</p>

</div>																					

											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$invalid && (AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)}">																																																																																	
<input type="number" class="form-control1" placeholder="To Distance" maxlength="10"  name="toDistance{{CompanyAdvancesSeedlingData.id}}" data-ng-model='CompanyAdvancesSeedlingData.toDistance' 
data-ng-required='SeedlingAmtRow' ng-pattern="/^[0-9.\s]*$/" step="0.01" min="{{CompanyAdvancesSeedlingData.fromDistance}}"
id="toDist{{CompanyAdvancesSeedlingData.id}}" ng-keyup="setSeedtoValidation(CompanyAdvancesSeedlingData.id)"  style="text-align:right;" 
ng-blur="decimalpointseed('toDistance',$index)"/>
<p ng-show="AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$error.required && (AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
<p ng-show="AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$error.pattern  && (AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block" ng-if="CompanyAdvancesSeedlingData.toDistance">Invalid</p>																																																																		
<p ng-show="AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$error.min  && (AdvanceMasterForm.toDistance{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">To Distance must be greater than From Distance</p>												

</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$invalid && (AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)}">																																																																																												
	<input type="text" class="form-control1"  placeholder="Cost of Each Unit" maxlength="10"  name="costOfEachUnit{{CompanyAdvancesSeedlingData.id}}" data-ng-model='CompanyAdvancesSeedlingData.costOfEachUnit' data-ng-required='SeedlingAmtRow' ng-pattern="/^[0-9.\s]*$/" style="text-align:right;" ng-blur="decimalpointSeedgrid('costOfEachUnit',$indexSeed)"/>
	<p ng-show="AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$error.required && (AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$error.pattern  && (AdvanceMasterForm.costOfEachUnit{{CompanyAdvancesSeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																		
												
</div>												
											</td>
										</tr>
									</tbody>		
							 </table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
									</div>
								<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(AdvanceMasterForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>
								
							
							</form>							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
