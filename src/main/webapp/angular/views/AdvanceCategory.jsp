	<script type="text/javascript">		
		$('#autofocus').focus();	

	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="AdvanceCategory" ng-init="loadAdvanceCategoryCode();loadAllDetails();loadCompanyAdvances();">      
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Advance Category Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
					 			<form name="AdvanceCategoryForm" ng-submit="advanceCategorysubmit(AddedAdvanceCategory,AdvanceCategoryForm)"  novalidate>
								<input type="hidden" name="screenName" ng-model="AddedAdvanceCategory.screenName" />
							 <div class="row">
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	<div class="fg-line">
										<label for="advanceCode">Advance Code</label>
											<input type="text" class="form-control" placeholder="Category Code" maxlength="25" name="advanceCategoryCode"  data-ng-model="AddedAdvanceCategory.advanceCategoryCode" tabindex="1"  readonly="readonly"  id="advanceCode" with-floating-label> 
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryForm.advanceCategoryName.$invalid && (AdvanceCategoryForm.advanceCategoryName.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="Category">Category</label>
											<input type="text" class="form-control autofocus" placeholder="Category" maxlength="25"   name="advanceCategoryName"  data-ng-model="AddedAdvanceCategory.advanceCategoryName"   data-ng-required="true"     tabindex="3"  ng-blur="spacebtw('advanceCategoryName');validateDup();"  id="Category" with-floating-label> 
			                        	</div>
										<p ng-show="AdvanceCategoryForm.advanceCategoryName.$error.required && (AdvanceCategoryForm.advanceCategoryName.$dirty || Addsubmitted)" class="help-block"> Category is required</p>
										<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAdvanceCategory.advanceCategoryName!=null">Category Name Already Exist.</p>
										<!--<p ng-show="AdvanceCategoryForm.advanceCategoryCode.$error.required && (AdvanceCategoryForm.advanceCategoryCode.$dirty || submitted)" class="help-block"> Valid Field Assistant Name is required.</p>-->
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>

<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0"   data-ng-model="AddedAdvanceCategory.status"    >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"   data-ng-model='AddedAdvanceCategory.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
									
			                        	</div>
										
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>

							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryForm.advanceCode.$invalid  && (AdvanceCategoryForm.advanceCode.$dirty || Addsubmitted)}">
							<div class="fg-line">
							
								<select chosen class="w-100" name="advanceCode"    data-ng-required="true" data-ng-model="AddedAdvanceCategory.advanceCode" tabindex="2" ng-options="advanceCode.id as advanceCode.advance for advanceCode in GetAdvancesData | orderBy:'-advance':true" id="autofocus">
									<option value="">Select Advances</option>
								</select>
							</div>
        		            <p ng-show="AdvanceCategoryForm.advanceCode.$error.required  && (AdvanceCategoryForm.advanceCode.$dirty ||  Addsubmitted)" class="help-block">Select Advance</p>
		                </div>

			                
			                    </div>			                    
			                </div>
							
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryForm.description.$invalid && (AdvanceCategoryForm.description.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
									<label for="Description">Description</label>
											<input type="text" class="form-control" placeholder="Description" maxlength="50"   name="description"  data-ng-model="AddedAdvanceCategory.description"       tabindex="4" ng-blur="spacebtw('description')" id="Description" with-floating-label >
			                        	</div>
										<p ng-show="AdvanceCategoryForm.description.$error.required && (AdvanceCategoryForm.description.$dirty || Addsubmitted)" class="help-block"> Description is required</p>
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							    
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedAdvanceCategory.modifyFlag"  />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(AdvanceCategoryForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Categories</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
											<form name="AdvanceCategoryEdit" >					
							        <table ng-table="AdvanceCategory.tableEdit" class="table table-striped table-vmiddle" >
											<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Category ID</span></th>
																<th><span>Advance</span></th>
																<th><span>Category</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
														<tbody>
								        <tr ng-repeat="AdvCategory in AdvanceCategoryData"  ng-class="{ 'active': AdvCategory.$edit }">
                		    				<td >
					    		               <button type="button" class="btn btn-default" ng-if="!AdvCategory.$edit" ng-click="AdvCategory.$edit = true;AdvCategory.modifyFlag='Yes';AdvCategory.screenName='Advance Category Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="AdvCategory.$edit" ng-click="advanceCategoryUpdate(AdvCategory,$index);AdvCategory.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AdvCategory.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="AdvCategory.screenName" />
		                    				 </td>
							                 <td>
							                     <span ng-if="!AdvCategory.$edit">{{AdvCategory.advanceCategoryCode}}</span>
												  <div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryEdit.advanceCategoryCode.$invalid && (AdvanceCategoryEdit.advanceCategoryCode.$dirty || submitted)}">
							                     <div ng-if="AdvCategory.$edit">
												 	<input class="form-control" type="text" ng-model="AdvCategory.advanceCategoryCode" placeholder='Advance Name' maxlength="25" name="advanceCategoryCode{{$index}}" ng-blur="spacebtwgrid('advanceCategoryCode',$index)" >
													<p ng-show="AdvanceCategoryEdit.advanceCode.$error.required && (AdvanceCategoryEdit.advanceCode.$dirty || submitted)" class="help-block"> Field Assistant Name is required.</p>


												 </div>
												 
												 </div>
							                  </td>
											  <td>
							                      <span ng-if="!AdvCategory.$edit" ng-repeat='AdvanceNames in GetAdvancesData'>
												  	<span ng-if="AdvCategory.advanceCode==AdvanceNames.id">{{AdvanceNames.advance}}</span>
												  </span> 
												  <div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryEdit.advanceCode.$invalid && (AdvanceCategoryEdit.advanceCode.$dirty || submitted)}">
        		            					  <div ng-if="AdvCategory.$edit">												  
												  	<select class="form-control1" tabindex="2" data-ng-required='true' data-ng-model='AdvCategory.advanceCode' name="advanceCode{{$index}}" ng-options="advanceCode.id as advanceCode.advance for advanceCode in GetAdvancesData | orderBy:'-advance':true">
													<option value="">{{AdvCategory.advanceCode}}</option>
        										     </select>
											
													<p ng-show="AdvanceCategoryEdit.advanceCode.$error.required && (AdvanceCategoryEdit.advanceCode.$dirty || submitted)" class="help-block"> Select Advance Name.</p>
											</div>
												  </div>
							                  </td>
		                    				  <td >
							                     <span ng-if="!AdvCategory.$edit">{{AdvCategory.advanceCategoryName}}</span>
												  <div class="form-group" ng-class="{ 'has-error' : AdvanceCategoryEdit.advanceCategoryName.$invalid && (AdvanceCategoryEdit.advanceCategoryName.$dirty || submitted)}">
							                     <div ng-if="AdvCategory.$edit">
												 	<input class="form-control" type="text" ng-model="AdvCategory.advanceCategoryName" placeholder='Advance Name' maxlength="25" name="advanceCategoryName{{$index}}" ng-blur="spacebtwgrid('advanceCategoryName',$index);validateDuplicate(AdvCategory.advanceCategoryName,$index);" >
													<p ng-show="AdvanceCategoryEdit.advanceCategoryName.$error.required && (AdvanceCategoryEdit.advanceCategoryName.$dirty || submitted)" class="help-block"> Field Assistant Name is required.</p>
													<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AdvCategory.advanceCategoryName!=null">Category Name Already Exist.</p>


												 </div>
												 
												 </div>
							                  </td>
							                  
							                  <td>
                    							   <span ng-if="!AdvCategory.$edit">{{AdvCategory.description}}</span>

					            		           <div ng-if="AdvCategory.$edit">
												   	 <div class="form-group">
													   	<input class="form-control" type="text" ng-model="AdvCategory.description" placeholder='Description' maxlength="50" name="description{{$index}}" ng-blur="spacebtwgrid('description',$index)"/>  
													 </div>
												   </div>
							                  </td>
											  
											  <td>
							                      <span ng-if="!AdvCategory.$edit">
												  <span ng-if="AdvCategory.status=='0'">Active</span>
													<span ng-if="AdvCategory.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="AdvCategory.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model="AdvCategory.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="AdvCategory.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
											  
													    		              										  			
							             </tr>
										 </tbody>
								         </table>
										 </form>
										 </div>
										 </section>							 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
