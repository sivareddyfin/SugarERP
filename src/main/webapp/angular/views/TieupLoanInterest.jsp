	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
	  $(function() 
	  {
	     $(".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy',
			   minDate: 0
	     });		 		 		 
	  });
    </script>	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="TieupLoanInterestMaster" ng-init="loadTieupLoanBanks();loadTieupLoanSeasons();addTielUpField();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Tie Up Loan Interest Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					   <form name="TieupLoanInterestMasterForm" novalidate ng-submit='AddedTieupLoans(AddTieupLoan,TieupLoanInterestMasterForm);'>	
					   <!--<form name="TieupLoanFilterForm" novalidate>-->	
					 		<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.season.$invalid && (TieupLoanInterestMasterForm.season.$dirty || Filtersubmitted)}">										
			        	                <div class="fg-line">
										  	<select chosen class="w-100" name="season" ng-model="AddTieupLoan.season" ng-options='season.season as season.season for season in GetLoanSeasonDropdownData' data-ng-required='true' ng-change="seasonOnChange();">
												<option value="">Select Season</option>
											</select>
			                	        </div>
<p ng-show="TieupLoanInterestMasterForm.season.$error.required && (TieupLoanInterestMasterForm.season.$dirty || Filtersubmitted)" class="help-block">Season required.</p>																						
</div>										
			                    	</div>			                    
				                  </div>
								  <input type="hidden"  name="modifyFlag" data-ng-model="AddTieupLoan.modifyFlag"  />
				                <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.branchCode.$invalid && (TieupLoanInterestMasterForm.branchCode.$dirty || Filtersubmitted)}">										
			        	                <div class="fg-line">
										  	<select chosen class="w-100" name="branchCode" ng-model="AddTieupLoan.branchCode" ng-options="bankData.id as bankData.branchname for bankData in GetLoanBankDropdownData | orderBy:'-branchname':true" data-ng-required='true' ng-change="seasonOnChange();">
												<option value="">Select Branch</option>
											</select>
			                	        </div>
<p ng-show="TieupLoanInterestMasterForm.branchCode.$error.required && (TieupLoanInterestMasterForm.branchCode.$dirty || Filtersubmitted)" class="help-block">Branch required.</p>																																
</div>										
			                    	</div>			                    
				                  </div>
								<div class="col-sm-2">
				                    <div class="input-group">
			        	                <div class="fg-line">
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="loadTieupLoansByFilters(AddTieupLoan);">Get rates from Prev. Season</button>
			                	        </div>
			                    	</div>			                    
				                  </div> 
								  <div class="col-sm-1"></div> 								  
							</div><br><hr />
						
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.payableDate.$invalid && (TieupLoanInterestMasterForm.payableDate.$dirty || submitted)}">																				
			        	                <div class="fg-line"> 
										<label for="payableDate">Calculate Interest Till</label>           					          
											<input type='text' class='form-control input_date'  placeholder='Calculate Interest Till' ng-model='AddTieupLoan.payableDate' name="payableDate"  autofocus  data-input-mask="{mask: '00-00-0000'}" id="payableDate" with-floating-label >
			                	        </div>
<p ng-show="TieupLoanInterestMasterForm.payableDate.$error.required && (TieupLoanInterestMasterForm.payableDate.$dirty || submitted)" class="help-block">Calculate Interest Till required.</p>																																										
</div>										
			                    	</div>			                    									
								  </div>
							</div><br />
							
					 		<div class="row">
							  <div class="col-md-1"></div>
							  <div class="col-md-10">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle">
							 			<thead>
								        	<tr>
                		    					<th>Actions</th>
								                <th>From Amount</th>
			                    				<th>To Amount</th>
												<th>% of Interest</th>
            								</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="TieupData in data">
												<td>
													<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addTielUpField();" ng-if="TieupData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
													<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(TieupData.id);" ng-if="TieupData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>
													
												</td>  
												<td>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$invalid && (TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$dirty || submitted)}">												
													<input type="number" class="form-control1" placeholder="Starting Range of Amount" name="fromAmount{{TieupData.id}}" ng-model='TieupData.fromAmount' data-ng-required='true' max="{{TieupData.toAmount}}" ng-keyup="setTieupFromValidation(TieupData.id)" id="from{{TieupData.id}}" ng-blur="hideButton(TieupData.id);decimalpointgrid('fromAmount',$index);" style="text-align:right;"/>
<p ng-show="TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$error.required && (TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$dirty || submitted)" class="help-block">Required.</p>																																																							
<p ng-show="TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$error.max && (TieupLoanInterestMasterForm.fromAmount{{TieupData.id}}.$dirty || submitted)" class="help-block">Invalid.</p>		
<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{TieupData.id}}">Invalid</p>											
</div>													
												</td>  
												<td>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$invalid && (TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$dirty || submitted)}">												
													<input type="number" class="form-control1" placeholder="Ending Amount"  name="toAmount{{TieupData.id}}" ng-model='TieupData.toAmount' data-ng-required='true' min="{{TieupData.fromAmount}}" ng-keyup="setTieupToValidation(TieupData.id)" id="to{{TieupData.id}}" style="text-align:right;" ng-blur="decimalpointgrid('toAmount',$index);"/>
<p ng-show="TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$error.required && (TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$dirty || submitted)" class="help-block">Required.</p>
<p ng-show="TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$error.min && (TieupLoanInterestMasterForm.toAmount{{TieupData.id}}.$dirty || submitted)" class="help-block">Invalid.</p>
<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{TieupData.id}}">Invalid</p>																																																																				
</div>													
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : TieupLoanInterestMasterForm.interestRate{{TieupData.id}}.$invalid && (TieupLoanInterestMasterForm.interestRate{{TieupData.id}}.$dirty || submitted)}">												
													<input type="text" class="form-control1"  placeholder="Rate of Interest"  name="interestRate{{TieupData.id}}" ng-model='TieupData.interestRate' data-ng-required='true' style="text-align:right;" ng-blur="decimalpointgrid('interestRate',$index);"/>
<p ng-show="TieupLoanInterestMasterForm.interestRate{{TieupData.id}}.$error.required && (TieupLoanInterestMasterForm.interestRate{{TieupData.id}}.$dirty || submitted)" class="help-block">Required.</p>													
</div>													
												</td>
											</tr>
										</tbody>		
								   </table>
							    </div>	  
						     </div>
						     <div class="col-md-1"></div>
						 </div>
						 <div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-hide">Save</button>
									<button type="button" class="btn btn-primary" ng-click="reset(TieupLoanInterestMasterForm);">Reset</button>								
									</div>
							</div>						 	
						 </div>									 
					</form>		 
							 
							 
							 	 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
