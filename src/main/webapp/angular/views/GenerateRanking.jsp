	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GenerateRanking" ng-init="loadRankingSeason();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Ranking</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
						<form name="GenerateRankingFilterForm" ng-submit="loadRankingDetails(RankingFilter,GenerateRankingFilterForm);" novalidate>
							<input type="hidden" name="pageName" ng-model="RankingFilter.pageName"/>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GenerateRankingFilterForm.season.$invalid && (GenerateRankingFilterForm.season.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
        	        						    		<select chosen class="w-100" name="season" ng-model="RankingFilter.season" ng-options="season.season as season.season for season in GenerateRankingeasonNames | orderBy:'-season':true" ng-change="loadRankingProgramBySeason(RankingFilter.season);" ng-required="true">
															<option value="">Select Season</option>
							             		        </select>
		            	    	            		  </div>
					    	            	        </div>
													<p ng-show="GenerateRankingFilterForm.season.$error.required && (GenerateRankingFilterForm.season.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>
												</div>
			    		                	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GenerateRankingFilterForm.programno.$invalid && (GenerateRankingFilterForm.programno.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
        	        						    		<select chosen class="w-100" name="programno" ng-model="RankingFilter.programno" ng-options="programno.programnumber as programno.programnumber for programno in GenerateRankingProgramNames | orderBy:'-programnumber':true"  ng-required="true">
															<option value="">Select Program</option>
							             		        </select>
		            	    	            		  </div>
					    	            	        </div>
													<p ng-show="GenerateRankingFilterForm.programno.$error.required && (GenerateRankingFilterForm.programno.$dirty || Filtersubmitted)" class="help-block">Program is Required.</p>
												</div>
			    		                	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
			        	                		<div class="fg-line">
													<button type="submit" class="btn btn-primary btn-sm m-t-10">Get Details</button>
					                	        </div>
			    		                	</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<hr />							
						<!-------------------------------------table--------------->	
						<form name="GenerateRankingGrid" ng-submit="SaveGenerateRankings(GenerateRankingGrid);" novalidate>						 
							<div  class="table-responsive">
							   <section class="asdok">
								  <div class="container1">	
									  <table class="table table-striped">
										 <thead>
											<tr>
                					    		<th><span>Zone</span></th>
										        <th><span>Circle</span></th>
												<th><span>Ryot Code</span></th>
												<th><span>Name</span></th>											
												<th><span>Agreement No.</span></th>
												<th><span>Plot No.</span></th>
												<th><span>Plant/Ratoon</span></th>
												<th><span>Plot Size</span></th>
												<th><span>CCS Rating</span></th>
												<th><span>Rank</span></th>
            								</tr>											
										</thead>
										<tbody>
										    <tr ng-repeat="RankingData in GenerateRankingData">		                			    		
                					    		<td>
													{{RankingData.zone}}
													<input type="hidden" name="zone" ng-model="RankingData.zone" />
												</td>
							    			    <td>
													{{RankingData.circle}}
													<input type="hidden" name="circle" ng-model="RankingData.circle" />
												</td>
												<td>
													{{RankingData.ryotCode}}
													<input type="hidden" name="ryotCode" ng-model="RankingData.ryotCode" />
												</td>
												<td>
													{{RankingData.ryotName}}
													<input type="hidden" name="ryotName" ng-model="RankingData.ryotName" />
												</td>											
												<td>
													{{RankingData.agreementNumber}}
													<input type="hidden" name="agreementNumber" ng-model="RankingData.agreementNumber" />
												</td>
												<td>
													{{RankingData.plotNumber}}
													<input type="hidden" name="plotNumber" ng-model="RankingData.plotNumber" />
												</td>
												<td>
													{{RankingData.plantOrRatoon}}
													<input type="hidden" name="plantOrRatoon" ng-model="RankingData.plantOrRatoon" />
												</td>
												<td>
													{{RankingData.plotSize}}
													<input type="hidden" name="plotSize" ng-model="RankingData.plotSize" />
												</td>
												<td>
													{{RankingData.ccsRating}}
													<input type="hidden" name="ccsRating" ng-model="RankingData.ccsRating" />
												</td>
												<td style="width:200px;">												
													<div class="input-group input-group-unstyled">
														<div class="form-group" ng-class="{ 'has-error' : GenerateRankingGrid.rank{{$index}}.$invalid && (GenerateRankingGrid.rank{{$index}}.$dirty || submitted)}">
	                   										<input type="text" placeholder='Rank' maxlength="5" class="form-control1" name="rank{{$index}}" ng-model="RankingData.rank" readonly ng-required="true" tooltip-placement="bottom"  uib-tooltip="{{RankingData.rank}}" tooltip-trigger="mouseenter" tooltip-enable="RankingData.rank"/>	                   							
															<p ng-show="GenerateRankingGrid.rank{{$index}}.$error.required && (GenerateRankingGrid.rank{{$index}}.$dirty || submitted)" class="help-block">Program is Required.</p>
															
														</div>
											        </div>
												</td>
        			    					</tr>																		
										</tbody>		
									</table>
						 		</div>
							 </section>
						   </div>
						   <div class="row" align="center">            			
							 <div class="input-group">
								<div class="fg-line">
									<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="AssignRankings();">Generate Ranks</button>
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
								</div>
							</div>						 	
						 </div>
					</form>
					<!-------------------------------------table End--------------->							  
				 </div>
			  </div>
		  </div>
	  </section> 
  </section>

  <footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
