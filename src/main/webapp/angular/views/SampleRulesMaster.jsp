	<script type="text/javascript">	
		$('#autofocus').focus();	
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SampleRulesMaster" ng-init="addSampleCard();LoadAddedSamples();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Sample Rules Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					   <form name="AddSampleRulesForm" novalidate ng-submit='AddSamples(AddSampleRulesForm);'>
					 	<div class="row">
						  <div class="col-md-1"></div>
						  <div class="col-md-10">		
						 	 <div class="table-responsive">
							    <table class="table table-striped table-vmiddle">
							 	   <thead>
								      <tr>
                		    			 <th>Add Extent Size</th>
							             <th>From Acres</th>
		                    			 <th>To Acres</th>
										 <th>No.of Sample Cards</th>
            						  </tr>											
									</thead>
									<tbody>
										<tr ng-repeat="SampleRulesData in data">
											<td>
									<button type="button" class="btn btn-primary" ng-click='addSampleCard();' ng-if="SampleRulesData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
									<button type="button" class="btn btn-primary" ng-click='removeRow(SampleRulesData.id);' ng-if="SampleRulesData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>									
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$invalid && (AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$dirty || submitted)}">																																																																						
		<input type="number" class="form-control1 autofocus" placeholder="From Acres" maxlength="5" id="from{{SampleRulesData.id}}" name="sizeFrom{{SampleRulesData.id}}" data-ng-model='SampleRulesData.sizeFrom' data-ng-required='true' ng-pattern="/^[0-9]\d*(\.\d+)?$/" autofocus max="{{SampleRulesData.sizeTo}}" ng-keyup="setValidation(SampleRulesData.id)"/>
		
		
	<p ng-show="AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$error.required && (AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$error.pattern  && (AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
	<p ng-show="AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$error.max  && (AddSampleRulesForm.sizeFrom{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																			
<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{SampleRulesData.id}}">Invalid</p>
</div>																					

											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$invalid && (AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$dirty || submitted)}">																																																																																	
	<input type="number" class="form-control1" placeholder="To Acres" maxlength="5"  name="sizeTo{{SampleRulesData.id}}" data-ng-model='SampleRulesData.sizeTo' data-ng-required='true' ng-pattern="/^[0-9]\d*(\.\d+)?$/"  min="{{SampleRulesData.sizeFrom}}" id="to{{SampleRulesData.id}}" ng-keyup="setoValidation(SampleRulesData.id)"/>
	<p ng-show="AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$error.required && (AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$error.pattern  && (AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																		
<p ng-show="AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$error.min  && (AddSampleRulesForm.sizeTo{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>												
<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{SampleRulesData.id}}">Invalid</p>


</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$invalid && (AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$dirty || submitted)}">																																																																																												
	<input type="text" class="form-control1"  placeholder="No.of Sample Cards" maxlength="3"  name="noOfSampleCards{{SampleRulesData.id}}" data-ng-model='SampleRulesData.noOfSampleCards' data-ng-required='true' ng-pattern="/^[1-9]\d*(\.\d+)?$/"/>
	<p ng-show="AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$error.required && (AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$error.pattern  && (AddSampleRulesForm.noOfSampleCards{{SampleRulesData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																		
												
</div>												
											</td>
										</tr>
									</tbody>		
							 </table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(AddSampleRulesForm)">Reset</button>
									</div>
								</div>						 	
							 </div>		
						</form>					 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
								
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
