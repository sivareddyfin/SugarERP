	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		
    	<section id="content" data-ng-controller="AccountGroupMaster" data-ng-init="loadAccountGroupCode(); loadAllAccountGroup();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Account Group Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <!--{{mactrl.currentSkin}}-->
					 	 <form name="AccountGroupMasterForm" ng-submit='AddAcountGroupSubmit(AddedAccountGroup,AccountGroupMasterForm);' novalidate>
					 		<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="accountGroupCode">Group Code</label> 
        		    					          <input type="text" class="form-control" placeholder="Group Code"  maxlength="10" readonly name="accountGroupCode" data-ng-model='AddedAccountGroup.accountGroupCode' id="accountGroupCode" with-floating-label>
			    		            	        </div>
			                    			</div>
										</div>
									</div><br />
																		
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="fg-line">
													<label for="description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="3" name="description" data-ng-model='AddedAccountGroup.description' ng-blur="spacebtw('description');"  id="description" with-floating-label>
			                	        			</div>
			                    				</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AccountGroupMasterForm.accountGroup.$invalid && (AccountGroupMasterForm.accountGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="accountGroup">Group Name</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Group Name"  maxlength="25" tabindex="2" name="accountGroup" data-ng-model='AddedAccountGroup.accountGroup' data-ng-required='true' ng-blur="spacebtw('accountGroup');validateDup();" data-ng-pattern="/^[a-z A-Z\s]*$/" id="accountGroup" with-floating-label>														 
				                				    </div>
								<p ng-show="AccountGroupMasterForm.accountGroup.$error.required && (AccountGroupMasterForm.accountGroup.$dirty || Addsubmitted)" class="help-block"> Group Name is required</p>
								<p ng-show="AccountGroupMasterForm.accountGroup.$error.pattern  && (AccountGroupMasterForm.accountGroup.$dirty || Addsubmitted)" class="help-block"> Valid Group  Name is required</p>					
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedAccountGroup.accountGroup!=null">Group Name Already Exist.</p>									
												</div>
					                    	</div>
										</div>
									</div>								
																											
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  data-ng-model='AddedAccountGroup.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"  data-ng-model='AddedAccountGroup.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>																											
								</div>
							</div><br />		
							<input type="hidden"  name="modifyFlag" data-ng-model="AddedAccountGroup.modifyFlag"  />			
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(AccountGroupMasterForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Account Groups</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
								<form name="EditAccountGroupMasterFrom" novalidate>					
							        <table ng-table="AccountGroupMaster.tableEdit" class="table table-striped table-vmiddle">
									<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Group Code</span></th>
													<th><span>Group Name</span></th>
													<th><span>Description</span></th>
													<th><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
										
								        <tr ng-repeat="Group in AccountGroupData"  ng-class="{ 'active': Group.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Group.$edit" ng-click="Group.$edit = true;Group.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Group.$edit" ng-click="UpdateAccountGroup(Group,$index);Group.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Group.modifyFlag"  />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Group.$edit">{{Group.accountGroupCode}}</span>
					    		                <div ng-if="Group.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="Group.accountGroupCode" placeholder='Group Code' maxlength="10" name="accountGroupCode{{$index}}" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!Group.$edit">{{Group.accountGroup}}</span>
							                     <div ng-if="Group.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditAccountGroupMasterFrom.accountGroup{{$index}}.$invalid && (EditAccountGroupMasterFrom.accountGroup{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="Group.accountGroup" placeholder='Group Name' maxlength="25" name="accountGroup{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('accountGroup',$index);validateDuplicate(Group.accountGroup,$index);"/>
			<p ng-show="EditAccountGroupMasterFrom.accountGroup{{$index}}.$error.required && (EditAccountGroupMasterFrom.accountGroup{{$index}}.$dirty || submitted)" class="help-block">Account Group Name is required</p>
			<p ng-show="EditAccountGroupMasterFrom.accountGroup{{$index}}.$error.pattern  && (EditAccountGroupMasterFrom.accountGroup{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Account Group Name.</p>	
			<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Group.accountGroup!=null">Group Name Already Exist.</p>												

</div>
												 </div>
							                  </td>
							                  
											  
											  <td>
							                      <span ng-if="!Group.$edit">{{Group.description}}</span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												  	 <input class="form-control" type="text" ng-model="Group.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
													 </div>
												  </div>
							                  </td>
											  <td style="width:235px;">
							                     
												  <span ng-if="!Group.$edit">
												  		<span ng-if="Group.status=='0'">Active</span>
														<span ng-if="Group.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  data-ng-model='Group.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='Group.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>											  
							             </tr>
										 </tbody>
								         </table>	
									</form>
									</div>
									</section>						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
