		
	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>
	<section id="main" class='bannerok'>    
    	<section id="content" data-ng-controller="productGroupMasterController" data-ng-init="loadStatus();getMAXProductGroup();getAllProductGroups();" >      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Product Group  Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">  						
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="productGroupMaster" novalidate ng-submit="productSubgroupSubmit(Addedproductgroup,productGroupMaster);">
					<input type="hidden" ng-model="Addedproductgroup.modifyFlag" name="modifyFlag"  />
					<div class="row">
						<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productGroupMaster.productGroup.$invalid && (productGroupMaster.productGroup.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="productGroup">Product Group</label>
															 <input type="text" class="form-control autofocus " autofocus placeholder="Product Group"    maxlength="25" tabindex="1" name="productGroup" data-ng-model='Addedproductgroup.group' data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" id="productGroup" with-floating-label ng-blur="validateDupProductName();" ng-keyup='validateDupProductName();' />														 
				                				    </div>
						<p ng-show="productGroupMaster.productGroup.$error.required && (productGroupMaster.productGroup.$dirty || Addsubmitted)" class="help-block">Product Group Name is required</p>
						<p ng-show="productGroupMaster.productGroup.$error.pattern  && (productGroupMaster.productGroup.$dirty || Addsubmitted)" class="help-block">Enter Valid Product Groups Name</p>
						<p class="help-block duplicateproductGroupName{{$index}}" style="color:#FF0000; display:none;" ng-if="Addedproductgroup.group!=null">Product Group Already Exist.</p>													
												</div>
					                    	</div>
											</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productGroupMaster.description.$invalid && (productGroupMaster.description.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="description">Description</label>
															 <input type="text" class="form-control autofocus" placeholder="Description"  autofocus  maxlength="25" tabindex="3" name="description" data-ng-model='Addedproductgroup.description'   id="description" with-floating-label />														 
				                				    </div>
						<p ng-show="productGroupMaster.description.$error.required && (productGroupMaster.description.$dirty || Addsubmitted)" class="help-block">Description is required</p>
																
												</div>
					                    	</div>
											</div>
						</div>
						</div>
						<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-12">
							<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : productGroupMaster.groupCode.$invalid && (productGroupMaster.groupCode.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="groupCode">Group Code</label>
															 <input type="text" class="form-control autofocus" placeholder="Group Code"  autofocus  maxlength="10" tabindex="2" name="groupCode" data-ng-model='Addedproductgroup.groupCode' data-ng-required='true' ng-keyup="validateDupGroupCode();"   data-ng-pattern="/^[0-9\s]*$/" id="groupCode" with-floating-label >														 
				                				    </div>
						<p ng-show="productGroupMaster.groupCode.$error.required && (productGroupMaster.groupCode.$dirty || Addsubmitted)" class="help-block">Group Code is required</p>
						<p ng-show="productGroupMaster.groupCode.$error.pattern  && (productGroupMaster.groupCode.$dirty || Addsubmitted)" class="help-block">Enter Valid Group Code</p>	
						<p class="help-block duplicateproductGroup" style="color:#FF0000; display:none;" ng-if="Addedproductgroup.groupCode!=null">Group Code Already Exist.</p>		
												</div>
					                    	</div>
											</div>
						</div>
						
						<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
			                        <br />
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="Addedproductgroup.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="Addedproductgroup.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>
							
							
							
							
</div>
							
						</div>
						
						
					</div>
					<br />
					<div class="row" align="center">            			
										<div class="input-group">
											<div class="fg-line">
												<button type="submit" class="btn btn-primary btn-hide">Save</button>									
												<button type="button" class="btn btn-primary" ng-click='reset(productGroupMaster);'>Reset</button>
												</div>
											</div>						 	
							 		</div>
					
					</form>
						
 					 									 
				   			
				        </div>
						
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Product Group</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">		
											  <form name="updateproductForm" novalidate>
												<table ng-table="RyotTypeMaster.tableEdit" class="table table-striped table-vmiddle" >
												
												<thead>
													<tr>
														<th><span>Action</span></th>
														<th><span>Product Group</span></th>
														<th><span>Group Code</span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
			
																						
													<tr ng-repeat="AddedProductGroup in productData"  ng-class="{ 'active': AddedProductGroup.$edit }">
														<td>
														   <button type="button" class="btn btn-default" ng-if="!AddedProductGroup.$edit" ng-click="AddedProductGroup.$edit = true;AddedProductGroup.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="AddedProductGroup.$edit" ng-click="UpdateProductGroup(AddedProductGroup,$index);AddedProductGroup.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AddedProductGroup.modifyFlag"  />	
														   
														 </td>
														 <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.group}}</span>
															<div ng-if="AddedProductGroup.$edit">
																<div class="form-group" ng-class="{ 'has-error' : updateproductForm.productGroup{{$index}}.$invalid && (updateproductForm.productGroup{{$index}}.$dirty || submitted)}">
																	<input class="form-control" data-ng-required='true' type="text" ng-model="AddedProductGroup.group" name="productGroup{{$index}}" maxlength="25" placeholder='Product Group' ng-blur="validateDuplicate(AddedProductGroup.group,$index);" data-ng-required='true' ng-keyup="validateDuplicate(AddedProductGroup.group,$index);"/>
													<p ng-show="updateproductForm.productGroup{{$index}}.$error.required && (updateproductForm.productGroup{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="updateproductForm.productGroup{{$index}}.$error.pattern  && (updateproductForm.productGroup{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedProductGroup.group!=null">ProductGroup Already Exist.</p>				
																																	
																																	</div>
															</div>
														  </td>											 
														 <td>
															<span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.productGroupCode}}</span>
															<div ng-if="AddedProductGroup.$edit">
			<div class="form-group" ng-class="{ 'has-error' : updateproductForm.groupCode{{$index}}.$invalid && (updateproductForm.groupCode{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="AddedProductGroup.productGroupCode" maxlength="25" placeholder='Product GroupCode' name="groupCode{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  readonly ng-blur=""/ >
					<p ng-show="updateproductForm.groupCode{{$index}}.$error.required && (updateproductForm.groupCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="updateproductForm.groupCode{{$index}}.$error.pattern  && (updateproductForm.groupCode{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<!--<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="AddedProductGroup.groupCode!=null">Land Type Already Exist.</p>-->
																</div>													
																
															</div>
														  </td>
														  <td>
															 <span ng-if="!AddedProductGroup.$edit">{{AddedProductGroup.description}}</span>
															 <div ng-if="AddedProductGroup.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="AddedProductGroup.description" maxlength="50" placeholder='Description'/>																</div>
															 </div>
														  </td>
														  <td>
															  <span ng-if="!AddedProductGroup.$edit">
																<span ng-if="AddedProductGroup.status=='0'">Active</span>
																<span ng-if="AddedProductGroup.status=='1'">Inactive</span>
															  </span>
															  <div ng-if="AddedProductGroup.$edit">	
																<div class="form-group">
																  <label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model='AddedProductGroup.status'>
																	<i class="input-helper"></i>Active
																  </label>
																  <label class="radio radio-inline m-r-20">
																		<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model='AddedProductGroup.status'>
																		<i class="input-helper"></i>Inactive
																  </label>
																 </div>
															  </div>
														  </td>
													 </tr>
													 </tbody>
													 </table>	
													</form>	
									 	 </div>
								 	</section>					 
							  	</div>
							</div>
						</div>
					<!----------end----------------------->										
					
							</div>
					
							
							
							
															
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
