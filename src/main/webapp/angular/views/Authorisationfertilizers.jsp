	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	
	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.textbox
		{
			width:100px;	
		}
		.spacing-table {
  
   
    border-collapse: separate;
    border-spacing: 0 5px; /* this is the ultimate fix */
}
.spacing-table th {
    text-align: left;
    
}
.spacing-table td {
    border-width: 1px 0;
  
   
    
}
.spacing-table td:first-child {
    border-left-width: 3px;
    border-radius: 5px 0 0 5px;
}
.spacing-table td:last-child {
    border-right-width: 3px;
    border-radius: 0 5px 5px 0;
}
	</style>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
<section id="content" data-ng-controller="authorisationController" ng-init="loadFieldOfficerDropdown();loadRyotCodeDropDown();loadusername();loadVarietyNames();loadKindType();loadseasonNames();loadFieldAssistants();loadStoreCode();onloadFunction();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Authorisation for Fertilizers </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->				
						<form name="authorisationForm"  novalidate ng-submit="authorisationSubmit(authorized,authorisationForm);">
							<!--  <input type="hidden" name="modifyFlag" ng-model="plantupdate.modifyFlag" />-->
							  <div class="row">
							  <div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.season.$invalid && (authorisationForm.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='authorized.season'  name="season" ng-options="season.season as season.season for season in seasonNames" data-ng-required="true ">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="authorisationForm.season.$error.required && (authorisationForm.season.$dirty || submitted)" class="help-block">Select Season</p>													
													</div>
															
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.ryotCode.$invalid && (authorisationForm.ryotCode.$dirty || submitted)}">
																<div class="fg-line">
				<select chosen class="w-100" data-ng-required="true"  name="ryotCode" data-ng-model='authorized.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-id':true"  ng-change="loadRyotDet(authorized.ryotCode);getaggrementDetails(authorized.season,authorized.ryotCode);getIndentnumbers(authorized.season,authorized.ryotCode);">
															<option value="">Ryot Code</option>
														</select >
																</div>
															<p ng-show="authorisationForm.ryotCode.$error.required && (authorisationForm.ryotCode.$dirty || submitted)" class="help-block">RyotCode is required</p>											
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.agreementnumber.$invalid && (authorisationForm.agreementnumber.$dirty || submitted)}">
																<div class="fg-line">
				<select chosen class="w-100" data-ng-required="true"   name='agreementnumber' ng-model="authorized.agreementnumber" ng-options="agreementnumber.agreementnumber as agreementnumber.agreementnumber for agreementnumber in agreementnumberNames" ng-change="getfofaseedlingQty(authorized.agreementnumber,authorized.season);"  >
															<option value="">Agreement Number</option>
														</select>
																</div>
																<p ng-show="authorisationForm.agreementnumber.$error.required && (authorisationForm.agreementnumber.$dirty || submitted)" class="help-block">Agreement Number is required</p>						
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
							  	<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.fieldOfficer.$invalid && (authorisationForm.fieldOfficer.$dirty || submitted)}">
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="1"  name="fieldOfficer" data-ng-required="true" data-ng-model='authorized.fieldOfficer' ng-options="fieldOfficer.id as fieldOfficer.fieldOfficer for fieldOfficer in FieldOffNames | orderBy:'fieldOfficer':true">
																	<option value="">Field Officer</option>																	</select>	
																</div>
																<p ng-show="authorisationForm.fieldOfficer.$error.required && (authorisationForm.fieldOfficer.$dirty || submitted)" class="help-block">Field Officer is required</p>	
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								
								
							</div>
								<div class="row">
								<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : authorisationForm.faCode.$invalid && (authorisationForm.faCode.$dirty || submitted)}">
												<div class="fg-line">
														 <select chosen class="w-100" name='faCode' ng-model="authorized.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true">
														<option value="">Field Assistant</option>
														
														</select>
				    		            	        </div>
														
				    		            	        
					<p ng-show="authorisationForm.faCode.$error.required && (authorisationForm.faCode.$dirty || submitted)" class="help-block">Field Assistant is required.</p>
																						
												</div>
			            		        	</div>
										</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  >
																<div class="fg-line">
			
														<select  chosen class="w-100"  placeholder='IndentNo' id="indent"   name="indentNo" ng-model="authorized.indentNo"   ng-options="indentNo.IndentNo as indentNo.IndentNo for indentNo in indentNoNames"  ng-change="loadPopup(authorized.indentNo,authorized.ryotCode);"   >
														<option value="">Indent Number</option>
														</select>	
																</div>
															
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
							  	<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
														<label for="date">Date Of Entry</label>
														<input type="text" class="form-control" placeholder='Date Of Entry' id="date" with-floating-label  name="authorisationDate" ng-model="authorized.authorisationDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true"  />	
				        		        	        </div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>	
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.ryotName.$invalid && (authorisationForm.ryotName.$dirty || submitted)}">
																<div class="fg-line">
																<label for="ryotname">RyotName</label>
														<input type="text" class="form-control" placeholder='RyotName' id="ryotname" with-floating-label  name="ryotname" ng-model="authorized.ryotName" readonly="readonly"    />	
														
				        		        	        </div>
																<p ng-show="authorisationForm.ryotName.$error.required && (authorisationForm.ryotName.$dirty || submitted)" class="help-block">Ryot Name is required.</p>	
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								
							</div>		 
							<div class="row">
							  		<div class="col-sm-4">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper" >
																<div class="fg-line">
																<label for="Indents">Indents</label>
														<input type="text" class="form-control" placeholder='Indents' id="Indents" with-floating-label  name="indentNumbers" ng-model="authorized.indentNumbers" ng-required="true"   />	
														
				        		        	        </div>
															<!--<p ng-show="authorisationForm.indentNumbers.$error.required && (authorisationForm.indentNumbers.$dirty || submitted)" class="help-block">Indent Numbers required</p>				-->									
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper">
																<div class="fg-line">
				<label for="authorized">Authorized</label>
														<input type="text" class="form-control" placeholder='Authorized' id="authorized" with-floating-label  name="authorisedBy" ng-model="authorized.authorisedBy"   readonly  />	
																</div>
																
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.phoneNo.$invalid && (authorisationForm.phoneNo.$dirty || submitted)}">
																<div class="fg-line">
																<label for="phonenum">Phone Number</label>
														<input type="text" class="form-control" placeholder='Phone Number' id="phonenum" with-floating-label  name="phoneNo" ng-model="authorized.phoneNo" readonly="readonly"   readonly  data-ng-required="true" />	
														
				        		        	        </div>
																<p ng-show="authorisationForm.phoneNo.$error.required && (authorisationForm.phoneNo.$dirty || submitted)" class="help-block">Phone Number is required</p>		
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
								<div class="col-sm-2">
									<div class="row">
										 <div class="col-sm-12">
														<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : authorisationForm.noOfAcr.$invalid && (authorisationForm.noOfAcr.$dirty || submitted)}">
																<div class="fg-line">
																<label for="noOfAcr">No. of Acres</label>
														<input type="text" class="form-control" placeholder='No. of Acres' id="noOfAcr" with-floating-label  name="noOfAcr" ng-model="authorized.noOfAcr" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  readonly data-ng-required="true" />	
														
				        		        	        </div>
																<p ng-show="authorisationForm.noOfAcr.$error.required && (authorisationForm.noOfAcr.$dirty || submitted)" class="help-block">No.of Acres is required</p>		
															</div>
														</div>
													</div>
										</div>
							  		
									
									
									
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : authorisationForm.Sauthorization.$invalid && (authorisationForm.Sauthorization.$dirty || submitted)}">
										<div class="fg-line">
										 <select chosen class="w-100" name="Sauthorization" data-ng-model='authorized.storeCode' data-ng-required='true' ng-options="Sauthorization.storeCode as Sauthorization.city for Sauthorization in storeCodeData  | orderBy:'-storeCode':true" >
										   <option value="">Store Code</option>
				        				 </select>
				    		            </div>										 
										<p ng-show="authorisationForm.Sauthorization.$error.required && (authorisationForm.Sauthorization.$dirty || Addsubmitted)" class="help-block">Select Store Code</p>										
									</div>
								</div>
										
								</div>
							<div class="col-sm-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : authorisationForm.individualExtent.$invalid && (authorisationForm.individualExtent.$dirty || submitted)}">
										<div class="fg-line">
										<input type="text" class="form-control" placeholder='individualExtent Acres' id="individualExtent" with-floating-label  name="individualExtent" ng-model="authorized.individualExtent"  readonly data-ng-required="true" />	
				    		            </div>										 
										<p ng-show="authorisationForm.individualExtent.$error.required && (authorisationForm.individualExtent.$dirty || submitted)" class="help-block">Select Store Code</p>										
									</div>
								</div>
										
								</div>
							</div>
							
							
							
							  <br/>
							
							 
							 
							   							
							 	 					 	
							 		
									
									
										
								
							
							
							 
							
								<div class="table-responsive" ng-if="authorizedData!=0" id="hidetable">
							 	<table class="table">
							 		<thead>
								        <tr>
											<th>Action</th>
											
							              
		                    				
											<th>Indent NO</th>
											<th>Item Type</th>
											<th>Qty</th>
											
											<th>Allowed</th>
											<th>Indent</th>
											
											<th>Pending Indent</th>
											<th>Cost Of Each Item</th>
											<th>Total Cost</th>
											
											
            							</tr>											
									</thead>
									
									<tbody>
										<tr ng-repeat="plantData in authorizedData">
											<td>
											
											
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow($index,plantData.indentNumbers);" ><i class="glyphicon glyphicon-remove-sign"></i></button>
												<input  type="hidden" ng-model="plantData.id" name="plantData{{$index}}" />												
											</td>


											
								<td>
									<input type="text" class="form-control1"  placeholder=""  name="indentNumbers{{$index}}" ng-model="plantData.indentNumbers" readonly   />
									</td>
									<td>
									<select class="form-control1" name="kind{{$index}}" data-ng-model='plantData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
														<option value="" > Kind Type</option>	
														</select>									
												

											
											</td>     
											
											<td>
										<input type="text" class="form-control1"  placeholder=""  name="quantity{$index}}" ng-model="plantData.quantity" ng-keyup="updatePendingAmtGrid(plantData.quantity,plantData.indent,plantData.allowed,$index);updategridTotalCost(plantData.quantity,plantData.costofEachItem,$index);"  />
										</td>
											<td>
										<input type="text" class="form-control1"  placeholder=""  name="allowed{{$index}}" ng-model="plantData.allowed" readonly  />
										</td>
											<td>
											<input type="text" class="form-control1"  placeholder=""  name="indent{{$index}}" ng-model="plantData.indent" readonly  />
											</td>
											
											<td>
											<input type="text" class="form-control1"  placeholder=""  name="pending{{$index}}" ng-model="plantData.pending"   readonly /></td>
											<td ><input type="text" class="form-control1 " ng-model="plantData.costofEachItem" name="costofEachItem{{$index}}" placeholder="" ng-keyup="updategridTotalCost(plantData.quantity,plantData.costofEachItem,$index);"  /></td>
											<td>
											<input type="text" class="form-control1"  placeholder=""  name="totalCost{{$index}}" ng-model="plantData.totalCost" readonly   />
											</td>
											
											</tr>
									</tbody>
							 	</table>
							</div>
								<div class="row" align="right" ng-if="authorizedData!=0">            			
								<div class="input-group">
									<div class="fg-line">
									<input type="text" class="form-control"  placeholder="Grand Total"  name="grandTotal" ng-model="authorized.grandTotal" ng-disabled="true" style="color:#000000;"    />
									</div>
									</div>
									</div>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Authorise</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(authorisationForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>				 	
					 		 
													 
					<!------------------form end---------->							 
					<!-------from-end--------------------->	
					<form name="GridPopup" novalidate>
					   <div class="card popupbox_ratoon" id="gridpopup_box">						  
						  	  						
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
											<th></th>	
												<th>Indent</th>
													
                                                <th >Date of Indent</th>
									
										
                                                <th>Date of Delivery </th>
											
                                                <th>Kind</th>
														
                                                <th>Variety </th>
														
												<th>Quantity</th>
											</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="gridData in IndentNumberData" >
											
                                                <td>
                                                  <div class="checkbox">
<label class="checkbox checkbox-inline m-r-20">
<input type="checkbox" class="checkbox" name="hrvFlag"  ng-model="gridData.hrvFlag" ng-click="optionToggled(gridData.hrvFlag,gridData.IndentNo,gridData.kind,gridData.quantity,gridData.IndentNo,gridData.totalCost,gridData.costOfEachItem,gridData.allowed,gridData.indent,gridData.pending)"><i class="input-helper"></i>
</label>
</div>
                                                </td>
                                                <td>
                                                   <input type="text"  class="form-control1" name="IndentNo{{$index}}" ng-model="gridData.IndentNo"  readonly="readonly" />
                                                </td>
                                                <td>
                                                 <input type="text"  class="form-control1" name="dateofindent{{$index}}" ng-model="gridData.dateofindent" readonly="readonly"  />
                                                </td>
                                                <td>
                                               <input type="text"  class="form-control1"  name="dateofdeliver{{$index}}" ng-model="gridData.dateofdeliver"  data-input-mask="{mask: '00-00-0000'}" readonly />
                                                </td>
                                                <td>
												
											<select class="form-control1" name="kind{{$index}}" data-ng-model='gridData.kind' name="kind" ng-options ="kind.id as kind.KindType for kind in kindType"   readonly="readonly">
														<option value="" > Kind Type</option>	
														</select>
                                               
												</td>
                                                <td>
                                                  <select class="form-control1" name="variety{{$index}}" ng-model="gridData.variety"  ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true" readonly="readonly">
													<option value="">Variety</option>
												</select>
                                                </td>
                                                <td colspan="2">
												  <input type="text"  class="form-control1"  name="quantity{{$index}}" ng-model="gridData.quantity" readonly/>
                                                </td>
                                               
												
                                            </tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Load Indents</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>
					
					 	 
					 
					 
					 
					 
					 
					 
					 	 						           			
				        </div>
			    	</div>
					
					
				</div>
			<!--
							<div id="authprint" style="display:none;">
					<div ng-repeat="item in items"  class="row"> 
					<table style="width:100%;position:absolute;top:11.7%" border="0">
						<tr><td style="float:left;"><div style="margin-left:100px;"></div></td><td style="float:right;"><div style="margin-right:70px;text-align:right;font-size:15px"><b>{{item.authSeqNo}}</b></div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">THE RETAILE STORES MANAGER,</div></td><td style="float:right;"><div style="margin-right:50px;text-align:right;font-size:15px"><b>{{item.AuthDate}}</b></div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">{{item.storeCity}},</div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">EAST GODAVARI,</div></td></tr>
						<tr><td style="float:left;"><div style="margin-left:10px;">STORE CODE : {{item.storeCode}}.</div></td></tr>
						</table>
						<table style="width:100%;position:absolute;top:42.5%" border="0"  style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
										<tr><td ><div style="margin-left:160px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.RyotCode}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:160px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.RyotName}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:160px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.village}}</b></div></td></tr>	
										<tr><td ><div style="margin-left:160px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"><b>{{item.mandal}}</b></div></td></tr>	
						</table>			
																
						
					</div>
					
								<div style="position:absolute;top:60%"> 
					<table style="width:100%;" border="0" style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
						
													<tr ng-repeat="itemgrid in itemsGrid" >
													<td ><div style="margin-left:228px;font-size:15px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{itemgrid.Fertiliser}}</div></td>
													<td ><div style="margin-left:140px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{itemgrid.NoOfBags}}</div></td>

													</tr>	
													
					</table>
					
					</div>
				
					<div   ng-repeat="itemTotal in itemsTotal"> 
					<table style="width:100%;position:absolute;top:75.5%" border="0" style="font-family:Georgia, Helvetica, sans-serif;font-weight:bold;color:#0000FF;">
						
													
													<tr><td style="float:left;"><div style="margin-left:100px;"></div></td><td style="float:right;"><div style="margin-right:325px;text-align:right;font-size:15px"><b>{{itemTotal.totalBags}}</b></div></td></tr>
													
									
										
					</table>
					
					</div>
					
				</div>
				-->
	    </section> 
	</section>
	
	
	
	
	

	<!--<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>-->
