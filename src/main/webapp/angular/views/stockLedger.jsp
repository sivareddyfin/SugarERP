<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="SeedlingEstimateController" ng-init="loadSeasonNames();loadCircleNames();loadFormData();">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Ledger</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="SeedlingEstimate">
					 		<div class="row">
										
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control date" placeholder="From Date" />
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.circle.$invalid && (SeedlingEstimate.circle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control date" placeholder="To Date" />
		                	            			  </div>
					<p ng-show="SeedlingEstimate.circle.$error.required && (SeedlingEstimate.circle.$dirty || Filtersubmitted)" class="help-block">Select Circle</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true'>
															<option value="">Select Store Names</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.circle.$invalid && (SeedlingEstimate.circle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control" placeholder="Item Name" />
		                	            			  </div>
					<p ng-show="SeedlingEstimate.circle.$error.required && (SeedlingEstimate.circle.$dirty || Filtersubmitted)" class="help-block">Select Circle</p>		 
											  </div>
			                    	      </div>
										</div>										
							</div>
							
							<div class="row">
							<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Stock Type </span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
							<input type="radio" name="permitType" checked="checked"  >
			    					    				<i class="input-helper">Serviceable</i>
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				        <input type="radio" name="permitType" value="1"   >
					    								<i class="input-helper"></i>Non-Serviceable
							  		 				</label>
													<button type="button" class="btn btn-primary btn-hide" ng-click="">Get Details</button>						 							 
												</div>
											</div>
											
											
										</div>
										
							</div>
							
							
							
					</form>		
							
							<br />
											 		
						 
							
							<!----------table grid design--------->
					
											
					        
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
