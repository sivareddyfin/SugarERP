	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RyotTypeMaster" ng-init="loadRyotTypeId();loadAllRyotTypes();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Land Type Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 <form name="RyotTypeForm" data-ng-submit='AddRyotType(AddRyot,RyotTypeForm);' novalidate>
					 <input type="hidden" name="screenName" ng-model="AddRyot.screenName" />
					 	<div class="row">
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	                <div class="fg-line">
											 <label for="landid">Land ID</label>
												<input type="text" class="form-control" placeholder="Land ID" readonly data-ng-model='AddRyot.landTypeId' name="RyotID" id="landid" with-floating-label />
				                	        </div>
			    	                	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="Description">Description</label>
											<input type="text" class="form-control" placeholder="Description" maxlength="50" ng-model='AddRyot.description' name="Description"  tabindex="2" ng-blur="spacebtw('description');" id="Description" with-floating-label />
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									  <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : RyotTypeForm.RyotType.$invalid && (RyotTypeForm.RyotType.$dirty || Addsubmitted)}">
			        	                <div class="fg-line">
										 <label for="LandType">Land Type</label>
											<input type="text" class="form-control autofocus" placeholder="Land Type"  maxlength="25" data-ng-model='AddRyot.landType' name="RyotType" data-ng-required='true' tabindex="1" data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" ng-blur="spacebtw('landType');validateDup();" id="LandType" with-floating-label/>
			                	        </div>
								<p ng-show="RyotTypeForm.RyotType.$error.required && (RyotTypeForm.RyotType.$dirty || Addsubmitted)" class="help-block">Land Type is required</p>
								<p ng-show="RyotTypeForm.RyotType.$error.pattern  && (RyotTypeForm.RyotType.$dirty || Addsubmitted)" class="help-block">Enter a valid Land Type.</p>
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddRyot.landType!=null">Land Type Name Already Exist.</p>
									  </div>
			                    	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" ng-model='AddRyot.status' name="status" tabindex="3"> 
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1" ng-model='AddRyot.status' name="Status" tabindex="4"> 
											
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden"  name="modifyFlag" data-ng-model="AddRyot.modifyFlag"  />
						<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(RyotTypeForm)">Reset</button>
									</div>
								</div>						 	
							 </div>
					 </form>  
						  
						  
						  
						  
						
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Land Types</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">		
											  <form name="UpdateLandTypeForm" novalidate>
												<table ng-table="RyotTypeMaster.tableEdit" class="table table-striped table-vmiddle" >
												
												<thead>
													<tr>
														<th><span>Action</span></th>
														<th><span>Land ID</span></th>
														<th><span>Land Type</span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
			
																						
													<tr ng-repeat="EditAddedRyots in AddedRyot"  ng-class="{ 'active': EditAddedRyots.$edit }">
														<td>
														   <button type="button" class="btn btn-default" ng-if="!EditAddedRyots.$edit" ng-click="EditAddedRyots.$edit = true;EditAddedRyots.modifyFlag='Yes';EditAddedRyots.screenName='Land Type Master';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="EditAddedRyots.$edit" ng-click="UpdateRyotTypes(EditAddedRyots,$index);EditAddedRyots.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="EditAddedRyots.modifyFlag"  />	
														   <input type="hidden" name="screenName{{$index}}" ng-model="EditAddedRyots.screenName" />
														 </td>
														 <td>
															<span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.landTypeId}}</span>
															<div ng-if="EditAddedRyots.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="EditAddedRyots.landTypeId" maxlength="25" placeholder='Land Type ID' readonly/>																</div>
															</div>
														  </td>											 
														 <td>
															<span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.landType}}</span>
															<div ng-if="EditAddedRyots.$edit">
			<div class="form-group" ng-class="{ 'has-error' : UpdateLandTypeForm.landType{{$index}}.$invalid && (UpdateLandTypeForm.landType{{$index}}.$dirty || submitted)}">												
					<input class="form-control" type="text" data-ng-model="EditAddedRyots.landType" maxlength="25" placeholder='Land Type' name="landType{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  ng-blur="spacebtwgrid('landType',$index);validateDuplicate(EditAddedRyots.landType,$index);"/ >
					<p ng-show="UpdateLandTypeForm.landType{{$index}}.$error.required && (UpdateLandTypeForm.landType{{$index}}.$dirty || submitted)" class="help-block">Required.</p>					<p ng-show="UpdateLandTypeForm.landType{{$index}}.$error.pattern  && (UpdateLandTypeForm.landType{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>		
					<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="EditAddedRyots.landType!=null">Land Type Already Exist.</p>
																</div>													
																
															</div>
														  </td>
														  <td>
															 <span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.description}}</span>
															 <div ng-if="EditAddedRyots.$edit">
																<div class="form-group">
																	<input class="form-control" type="text" ng-model="EditAddedRyots.description" maxlength="50" placeholder='Description' ng-blur="spacebtwgrid('description',$index)"/>																</div>
															 </div>
														  </td>
														  <td>
															  <span ng-if="!EditAddedRyots.$edit">
																<span ng-if="EditAddedRyots.status=='0'">Active</span>
																<span ng-if="EditAddedRyots.status=='1'">Inactive</span>
															  </span>
															  <div ng-if="EditAddedRyots.$edit">	
																<div class="form-group">
																  <label class="radio radio-inline m-r-20">
																	<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" ng-model='EditAddedRyots.status'>
																	<i class="input-helper"></i>Active
																  </label>
																  <label class="radio radio-inline m-r-20">
																		<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model='EditAddedRyots.status'>
																		<i class="input-helper"></i>Inactive
																  </label>
																 </div>
															  </div>
														  </td>
													 </tr>
													 </tbody>
													 </table>	
													</form>	
									 	 </div>
								 	</section>					 
							  	</div>
							</div>
						</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
