<style type="text/css">
/*@media print {*/
    .footer {page-break-after: always;}
/*}*/

/*.row:nth-child(3n){
page-break-after: always;
color:#FF0000;
}*/

#start {
/*  margin-bottom:8px;*/
}

.col1 {
  /*margin-bottom: 4px;*/
}

.col-inside {
  border: solid 1px #000000;

}
.col1:first-child {
  padding: 0 5px 5px 0;

}
.col1:nth-child(2) {
  padding: 0 5px 5px;
}

table.test
{
    border-collapse: separate;
    border-spacing: 2px;
}

table.test td
{
     
    padding: 0 0 0 10px;
  /*  border: 1px  #ccc;*/
}
</style>
	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SchedulePermits" ng-init="loadPermitSeason();loadCircleNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Schedule Permits</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	<form name="SchedulePermitForm"  ng-submit="saveSchedulepermitData(ScheduleFilter,SchedulePermitForm);" novalidate>
							
							<div class="row">
								
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.season.$invalid && (SchedulePermitForm.season.$dirty || Filtersubmitted)}">	
						        	                <div class="fg-line">
        		    						          <div class="select">
                							    		<select chosen class="w-100" name="season" ng-model="ScheduleFilter.season" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true" ng-change="loadProgramBySeason(ScheduleFilter.season);loadPermitMaxNumber(ScheduleFilter.season)" ng-required="true">
															<option value="">Season</option>
						        			            </select>
        		        	            			  </div>
			    		            	    	    </div>
													<p ng-show="SchedulePermitForm.season.$error.required && (SchedulePermitForm.season.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.programNumber.$invalid && (SchedulePermitForm.programNumber.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
        	        						    		<select chosen class="w-100" name="programNumber" ng-model="ScheduleFilter.programNumber" ng-options="programNumber.programnumber as programNumber.programnumber for programNumber in ScheduleProgramNumber | orderBy:'-programnumber':true" ng-required="true">
															<option value="">Program</option>
							        		            </select>
        		    	    	            		  </div>
			    			            	        </div>
													<p ng-show="SchedulePermitForm.programNumber.$error.required && (SchedulePermitForm.programNumber.$dirty || Filtersubmitted)" class="help-block">Program is Required.</p>
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.fromCcsRating.$invalid && (SchedulePermitForm.fromCcsRating.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
													<label for="fromRank">From Rank</label>
														<input type="text" class="form-control" placeholder="From Rank" maxlength="50" name="fromRank" data-ng-model='ScheduleFilter.fromRank' data-ng-required='true' data-ng-pattern="/^[0-9]{1,7}(\.[0-9]+)?$/" id="fromRank" with-floating-label>
														
													</div>
													<p ng-show="SchedulePermitForm.fromRank.$error.required && (SchedulePermitForm.fromRank.$dirty || submitted)" class="help-block">From Rank is required.</p>												
													<p ng-show="SchedulePermitForm.fromRank.$error.pattern  && (SchedulePermitForm.fromRank.$dirty || submitted)" class="help-block">Enter a From Rank</p>
													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.toRank.$invalid && (SchedulePermitForm.toRank.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
														<label for="toRank">To Rank</label>
														<input type="text" class="form-control" placeholder="To Rank" maxlength="50" name="toRank" data-ng-model='ScheduleFilter.toRank' data-ng-required='true' data-ng-pattern="/^[0-9]{1,7}(\.[0-9]+)?$/" id="toRank" with-floating-label> 
														
													</div>
													<p ng-show="SchedulePermitForm.toRank.$error.required && (SchedulePermitForm.toRank.$dirty || submitted)" class="help-block">To Rank is required.</p>												
													<p ng-show="SchedulePermitForm.toRank.$error.pattern  && (SchedulePermitForm.toRank.$dirty || submitted)" class="help-block">Enter a To Rank</p>
													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.circle.$invalid && (SchedulePermitForm.circle.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
													  <select chosen class="w-100" name="circle1"   data-ng-model='ScheduleFilter.circle' ng-options="circle.id as circle.circle for circle in CircleNames">
														
													  <option value="">Circle</option>
													   <option value="0">All</option>
													  
													  </select>
													  </div>
													  
													 </div>
													<p ng-show="SchedulePermitForm.circle.$error.pattern  && (SchedulePermitForm.circle.$dirty || submitted)" class="help-block">Select Circle</p> 
												</div>
											</div>		  
										</div>
									</div>
							</div>
							</div>
						
							<div class="row">
							<div class="col-sm-3">
							<div class="input-group">
            			            
			                       
            				          <div class="fg-line" style="margin-top:5px;">
										 
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0"   data-ng-model="ScheduleFilter.permitDateFlag">
			    			            	<i class="input-helper"></i>UnSchedule
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="ScheduleFilter.permitDateFlag">
			    				            <i class="input-helper"></i>All
					  		              </label>
			                	        </div>
			                
			                    </div>
								
								</div>
								<div class="col-sm-2">
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10 " ng-click="getScheduleDetails(ScheduleFilter.season,ScheduleFilter.programNumber,ScheduleFilter.fromRank,ScheduleFilter.toRank,ScheduleFilter.circle,ScheduleFilter.permitDateFlag,ScheduleFilter.harvestDate);">Get Details</button>
										
									</div>
								</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SchedulePermitForm.hrvDate.$invalid && (SchedulePermitForm.hrvDate.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
													<label for="harvestingDate">Harvesting Date</label>
														<input type="text" class="form-control date" placeholder="Harvesting Date" name="hrvDate" data-ng-model='ScheduleFilter.hrvDate' data-ng-required='true' id="harvestingDate" with-floating-label>
														
													</div>
													<p ng-show="SchedulePermitForm.hrvDate.$error.required && (SchedulePermitForm.hrvDate.$dirty || submitted)" class="help-block">Harvesting Date is required.</p>												
											
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								
								<div class="col-sm-2">
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="appendHarvestDate(ScheduleFilter.hrvDate);">Apply</button>
										
									</div>
								</div>
								</div>
							</div>	
								
						<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" >&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Select all<i class="input-helper"></i>
								</label>
							</div>
								
						<div class="table-responsive">
				
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Select</span></th>
															<th><span>Rank</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
															<th><span>Agreement</span></th>
															<th><span>Circle</span></th>
															<th><span>Permit</span></th>
															<th><span>HarvestDate</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
										<tr ng-repeat="ScheduleData in SchedulepermitData">
										
													<td>
														<div class="checkbox">
															<label class="checkbox checkbox-inline m-r-20">
																<input type="checkbox" class="checkbox" name="hrvFlag"  ng-model="ScheduleData.hrvFlag" ng-change="optionToggled()"><i class="input-helper"></i>
															</label>
														</div>  
													</td>	
												
													<td>
												
												<input type="text" class="form-control1" name="rank{{$index}}" ng-model="ScheduleData.rank" readonly/>
											</td>
							    	    	<td>
												
												
												<!--<select  class="form-control1"  name="ryotCode{{$index}}" ng-model="ScheduleData.ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCode | orderBy:'-id':true" ng-required='true' readonly>
														<option value="">Ryot Code</option>
													</select>-->
													
													
													<input type="text" class="form-control1" readonly name="ryotCode{{$index}}" ng-model="ScheduleData.ryotCode" />
											</td>		
														
											<td>
											
												<input type="text" class="form-control1" name="ryotName{{$index}}" ng-model="ScheduleData.ryotName" readonly  />
											</td>
											
											<td>
												
												<input type="text" name="agreementNumber{{$index}}" class="form-control1" ng-model="ScheduleData.agreementNumber" readonly/>
											</td>											
										<!--	<td>
												
											<select class="form-control1" ng-options="circle.id as circle.circle for circle in CircleNames" ng-model="ScheduleData.circle">
											
											<option value="">Circle</option>
											
											</select>
												
											</td>
											-->
											<td>
												
												<input type="text" name="circle{{$index}}" class="form-control1" ng-model="ScheduleData.circle" readonly  />
											</td>
											<td>	
												<input type="text" name="permitNumber{{$index}}" class="form-control1" ng-model="ScheduleData.permitNumber" readonly/>
											</td>
											<td>
												
												<input type="text" name="harvestDate{{index}}" class="form-control1" ng-model="ScheduleData.harvestDate" readonly/>
											</td>
											
														</tr>											
													</tbody>
												</table>	
												<br />
												<div align="center" id="loadingImg" style="display:none;"><img src="img/images/loading.gif" style="height:50px;" /></div>								  												
										</div>
									</section>
																				
									</div>
								<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									<br />
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(SchedulePermitForm);">Reset</button>
									</div>
								</div>						 	
							 </div>	
						<!-------------------------------------table--------------->
						</form>
						
							<div id="dispGeneratePrint" style="display:none;">
					
					<div ng-repeat="item in itemData"  class="row">
							
	        		<div class="col-xs-12 col1" >
			
			<div class="col-inside" style=" border: solid 1px #000000;">
				
				<table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
					<tr style="text-align:center; font-size:14px;"><td><strong>SRI SARVARAYA SUGARS LTD., CHELLURU</strong></td></tr>
					<tr style="text-align:center; font-size:12px;"><td><strong>CANE HARVESTING PERMIT</strong></td></tr>					
				</table>
				<hr style="border: solid 1px #333333;"/>
				<div class="row" style="font-family:Georgia, Helvetica, sans-serif; ">
				<div class="col-xs-4">
				<table width="" align="left" style="font-size:12px;">
				<tr  style="text-align:left" ><td><strong>&nbsp;&nbsp;PERMIT NO.</strong></td>
				<td>:</td>
				<td style="font-family:Calibry, Helvetica, sans-serif;"> &nbsp;&nbsp;{{item.permitnumber}}</td>
				</tr>
				<tr><td> <strong>&nbsp;&nbsp;HARVESTING DATE</strong></td>
				<td>:</td>
				<td style="font-family:Calibry, Helvetica, sans-serif;">&nbsp;&nbsp;{{item.harvestDate}}</td>
				</tr>
				<tr><td > <strong>&nbsp;&nbsp;CIRCLE</strong></td>
				<td>:</td>
				<td> &nbsp; {{item.circle}}</td>
					
					</tr>	
									
					
				</table>
				</div>
				<div class="col-xs-4"  valign='top'>
				<table width="" align="center" style="font-size:12px;">
				<tr  style="text-align:left"><td  ><strong>&nbsp;&nbsp;RYOT CODE</strong></td>
					<td>:</td>
					<td style="font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"> &nbsp;&nbsp;{{item.ryotcode}}</td>
					</tr>
					<tr><td > <strong>&nbsp;&nbsp;VILLAGE</strong></td>
					<td>:</td>
					<td>&nbsp;&nbsp;  {{item.village[0].village}}</td>
				</tr>
				<tr><td > <strong>&nbsp;&nbsp;FATHER</strong></td>
				<td>:</td>
				<td>&nbsp;&nbsp; {{item.fathername}}</td>
				</tr>
				<tr ><td ><strong>&nbsp;&nbsp;RYOT</strong></td>
				<td>:</td>
				<td> &nbsp;&nbsp; {{item.ryotname}}</td>
				</tr>
				
				
				
				</table>
				</div>
			
				<div class="col-xs-4"  rowspan="4"   valign='top' >
				
				
				
				
				<img alt="Embedded Image" data-ng-src="data:image/png;base64,{{item.qrcodedata}}"  align="right" height="60"  style="margin-top:-75px; margin-right:10px;" />
				
				
				</div>
				</div>
                	
                	
                	
                	<div align="center">
						
                    <table border="1" style="width:100%; border-collapse:collapse; margin-top:1%; font-family:Georgia, Helvetica, sans-serif; ">
                    	<tr>
                        	<td style="width:25%;border-left:none;">Plant/Ratoon</td>
                            <td style="width:25%;"> &nbsp;{{item.plantorratoon}}</td>
                            <td style="width:20%;">Admn.No.</td>
                            <td style="width:30%;">&nbsp;  </td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Variety</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.variety}}</td>
                            <td style="width:25%;">Time</td>
                            <td style="width:25%;">&nbsp;</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Circle Code</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.circlecode}}</td>
                            <td style="width:25%;">Shift</td>
                            <td style="width:25%;"><input type="checkbox" >A <input type="checkbox" > B <input type="checkbox" > C</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Land Village Code</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.landvilcode}}</td>
                            <td style="width:25%;">Vehicle Regn.</td>
                            <td style="width:25%;">&nbsp;</td>                            
                        </tr>
                    	<tr>
                        	<td style="width:25%;border-left:none;">Program & Rank No.</td>
                            <td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{item.programno}} & {{item.rank}} </td>
                            <td style="width:25%;">Vehicle No.</td>
                            <td  style="width:25%;">&nbsp;</td>                            
                        </tr>
                        <tr>
                        	<td style="width:25%;border-left:none;">Vehicle Type</td>
                            <td colspan="3"  style="" ><input type="checkbox" >Tractor <input type="checkbox" >Small Lorry <input type="checkbox" >Big Lorry</td>
                        </tr>
                        
                    </table>
                    </div>
					
				<table width="100%"  class="test" style="font-size:14px;font-weight:bold;font-family:Georgia, Helvetica, sans-serif; ">
				<tr  style="text-align:left; width:25%;"><td></td>
					<td style="text-align:left; width:25%;"></td>
					<td  style="text-align:right; width:25%;"></td>
					<td  style="text-align:right; width:25%;"><img src="img/images/Cane Manager Signature.png" style="width:40%; height:30px; margin-right:10px;" /></td>			
					
					</tr>
					<tr  style="text-align:left; width:25%;"><td>En.Clerk</td>
					<td style="text-align:left; width:25%;">W.B.Operator</td>
					<td  style="text-align:right; width:25%;">Agrl.Officer</td>
					<td  style="text-align:right; width:25%;">AGM(CANE)</td>			
					
					</tr>
					
					
														
				</table>
				</div>
			</div>
			
		
			
			
	<div  style="page-break-after: always;" ng-if="$index%1=='0'"></div>	
		
	    </div>
				
				</div>
							<button type="button" ng-click="printGeneratePermit();" style="display:none;"  >Print</button>
				</div>
			 </div>
		</div>
	 </section> 
</section>

<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
