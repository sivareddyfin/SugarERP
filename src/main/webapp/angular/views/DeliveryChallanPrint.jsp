<script type="text/javascript">
    $(".autofocus").focus();
</script>

</script>

<style>
 table,th,td{
     text-align:center;
	 
	 }
	
	div{
	   color:#0099CC;
	   text-align:left;
	   }
	   .numberCol
	   {
	      font-family:Calibry, Helvetica, sans-serif;
	   }
	   ul
{
list-style-type: none;
}
#asdf {
  font-family: "Gothic 57";
  Gothic 57 Normal.ttf) format("truetype");
}
h1 { font-family: "Gothic 57", sans-serif } 
	
</style>

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
	<section id="content" data-ng-controller="DeliveryChallanPrintController" >       
        	<div class="container">				
    			<div class="block-header"><h2><b>Delivery Challan Print</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding">
				  
				  <!--------Form Start----->
					 <!-------body start------>
					 
					 
					  <div id="deliveryChallanPrint">
					      <table width="100%"  style="font-family:Georgia, Helvetica, sans-serif; border-collapse:collapse;">
						  <tr style="text-align:center; font-size:16px;"><td style="text-align:center;"><h1 style="color:#0099CC;font-family: 'Gothic 57', sans-serif;">Sri Sarvaraya Sugars Limited</h1></td></tr><br/>
					<tr style="text-align:center; font-size:16px;"><td style="text-align:center;color:#0099CC; "><strong>DELIVERY CHALLAN</strong></td></tr>
							</table>
					   <div style="border:5px solid #0099CC;border-radius:5px; margin:5px; text-align:center; padding:5px;">
					   <%--<div style="border:5px ;border-radius:5px; margin:5px; text-align:center; padding:5px;">--%>   
					      <%--<div  style ="font-family:Georgia, Helvetica, sans-serif; border-radius:5px ; border: 1px  ; margin:5px;">--%>
					
			                  <div>
					             <table border="1" style="width:100%;  border:#0099CC; border-collapse:collapse;">
								    <tr>
									   <td rowspan="3" style="width:50%;"><ul><li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">SRI SARVARAYA SUGARS LTD.</li>
									   
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Chelluru- 533261</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Rayavaram Mandal</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">East Godavari District</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Andhra Pradesh</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">CIN: UO1115TN1956PLC003435</li>
									   
									   </ul></td> <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left; width:25%;color:#0099CC;">&nbsp;Delivery Challan No. :</td> <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;0063/2016-17</td>
									</tr>
									<tr>
									   <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;width:30%;color:#0099CC;">&nbsp;Delivery Challan Date. :</td> <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;22-03-2017</td>
									</tr>
									<tr>
									    <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;width:20%;color:#0099CC;">&nbsp;Buyer's Order No.</td> <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;MCPL/001</td>
									</tr>
									<tr>
									   <td rowspan="3"><ul><li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Dispatch & Invoice to :</li>
									   
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">M/s. Meenalshi Commodities Private Limited</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Regd Office D.No. 1-3-190A/1, R.R. NAGAR,</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Vidhyadharapuram</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">Vijayawada- 520012</li>
									   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;list-style-type: none;">TIN No: <span style="font-weight:bold;color:#0099CC;">37379013712</span></li>
									   
									   </ul></td> <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;Buyer's Order No</td> <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;21-03-2017</td>
									</tr>
									<tr>
									   <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left; text-align:left;color:#0099CC;">&nbsp;Dispatch Through</td> <td style="text-align:left;color:#0099CC;">&nbsp;Lorry</td>
									</tr>
									<tr>
									    <td  style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;color:#0099CC;">&nbsp;Vehicle No  :</td> <td style=" font-family:Calibry, Helvetica, sans-serif;text-align:left;color:#0099CC;">&nbsp;AP31TT5886</td>
									</tr>
								 
								 </table>
								 <table border="1" style="width:100%;  border:#0099CC; border-collapse:collapse;color:#0099CC;">
								    <tr>
									  <th>S.No</th>
									  <th>Description</th>
									  <th>UOM</th>
									  <th>Quantity</th>
									</tr>
									<tr>
									  <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:center;"> 1</td>
									   <td>
									     <ul>
										   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;list-style-type: none;"> STEAM COAL</li>
										   <br />
										   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;list-style-type: none;">Gross Weight :   26.690 M.Tons</li>
										   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;list-style-type: none;">Tare Weight  :   8.560  M.Tons</li>
										   <li style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;list-style-type: none;">Net Weight   :   18.130 M.Tons</li>
										   <br />
										 </ul>
									   </td>
									   <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:left;">M.Tons</td>
									   <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:right;">18.130</td>
									</tr>
									
									<tr>
									 <td> </td>
									   <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:center;">
									      Total
									   </td>
									   <td></td>
									   <td style=" font-family:Calibry, Helvetica, sans-serif; text-align:right;">18.130</td> 
									</tr>
									
								 </table>
								 
								 
								 
								 <table width="100%" border="1" style="width:100%;border:#0099CC; border-collapse:collapse; color:#0099CC;" >
						          <tr >
								  <td style="width:50%; height:50px; border 1px: #333333;">Received the goods in good condition</td><td style="width:50%; height:50px;border 1px:#333333;">For SARVARAYA  SUGARS LIMITED</td>
								  </tr>
								   <tr>
								  <td style="width:50%; height:50px; border 1px:#333333; ">Signature of Zonal Officer</td><td style="width:50%; height:50px;border 1px:#333333;">AUTHORIZED SIGNATORY</td>
								  </tr>
						          
					               </table>
									 
					          </div>
						   </div>	
					  </div>
					 </div>
					
					<div style="text-align:center;"><button class="btn btn-primary btn-sm m-t-10" ng-click="printDeliveryChallan();" >Print</button></div>  	
					</div>
					</div>
					</div>
				</section>
		</section>			     