	<script type="text/javascript">		
		$('#autofocus').focus();				
	</script>
	<script>
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
				 
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
	
	function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah1').attr('src', e.target.result);
				 
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp1").change(function(){
        readURL1(this);
    });
	
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="RyotMaster" ng-init='addFormField();loadMandalDropDown();loadBankBranchesDropDown();loadRyotCode();loadseasonNames();'>      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Ryot Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
						<!--<div class="row">
								<div class="col-sm-12">
					                <div class="input-group">
        	    				       <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	           <div class="fg-line">
            						      <div class="select">
	                				  		<select chosen class="w-100" ng-options="AddedRyot.id as AddedRyot.ryotname for AddedRyot in AddedRyotsData | orderBy:'-ryotname':true" name="AddedRyot" ng-model='AddedRyot' ng-change="UpdateRyotData(AddedRyot);">
												<option value="">Added Ryots</option>
				        		    	     </select>
                	            		  </div>
			        	        	    </div>
			            	       </div>			                    
				            	 </div>
							</div>-->
							
							<div class="row">
								<div class="col-sm-12">
					                <div class="input-group">
        	    				       <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	           <div class="fg-line">
            						      <div class="select">
										  <label for="loadryot">Enter Ryot Code to load Added Ryot Details</label>
										  <input list="testList"   class="form-control" type=""  placeholder="Enter Ryot Code to load Added Ryot Details" ng-model="AddedRyot" ng-blur="loadAddedRyotDropdowns(AddedRyot)" id="loadryot" with-floating-label/>
    										<datalist id="testList">
        										<option ng-repeat="ryotname in AddedRyotsData" value="{{AddedRyot.ryotname}}">
    										</datalist>
  
                	            		  </div>
			        	        	    </div>
			            	       </div>			                    
				            	 </div>
							</div>
							<br />					 
					    <form name="RyotMasterForm" novalidate ng-submit='AddedRyots(AddRyot,RyotMasterForm);' enctype="multipart/form-data">
							<!--<input type="hidden"  name="modifyFlag" data-ng-model="AddRyot.modifyFlag"  />-->
							<input type="hidden"  name="modifyFlag" data-ng-model="modifyFlag"  />
							
							<input type="hidden" name="screenName" ng-model="AddRyot.screenName" />
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						        	            <div class="fg-line">
												<label for="a">Ryot Code</label>
    	        							       <input type="text" class="form-control" placeholder="Ryot Code"  maxlength="20" name="ryotCode" ng-model='AddRyot.ryotCode' ng-blur="spacebtw('ryotCode');ryotCodeValidation(AddRyot.ryotCode);"  id="a" with-floating-label readonly>
												   <input type="hidden" ng-model='TempRyotCode'/>
												   <input type="hidden" ng-model='AddRyot.ryotCodeSequence' name="ryotCodeSequence"/>
				        		        	    </div>
												<p class="help-block ryotDup" style="color:#FF0000; display:none;"> The Given Ryot Code is already exist </p>
				                		    </div>
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.relation.$invalid && (RyotMasterForm.relation.$dirty || submitted)}">
						        	            <div class="fg-line">
    	        							       <div class="select">
	                						    		<select chosen class="w-100" name="relation" ng-model='AddRyot.relation' data-ng-required='true' ng-options="relation.relationId as relation.Name for relation in relation | orderBy:'-Name':true" ng-change="changeRelationName();" tabindex="2">
															<option value="">Select Relation</option>
				        		    			        </select>
		                	            			 	 </div>
													  </div>
							<p ng-show="RyotMasterForm.relation.$error.required && (RyotMasterForm.relation.$dirty || submitted)" class="help-block">Select Relation</p>  
						                	    </div>
						                    </div>
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.villageCode.$invalid && (RyotMasterForm.villageCode.$dirty || submitted)}">												
						        	                <div class="fg-line">		    	            				    		
												   	 <select chosen class="w-100" name="villageCode" ng-model='AddRyot.villageCode' data-ng-required='true' ng-options="villageCode.id as villageCode.village for villageCode in VillageData | orderBy:'-village':true" tabindex="5" ng-disabled="vmDropDownDisabled" ng-change='setNewRyotCode(AddRyot.villageCode);setCircleDropDown(AddRyot.villageCode);setRyotCodeonvillageChange(AddRyot.villageCode);'>
														<option value="">Select Village</option>
					        		    	        </select>		
																								
					        	        	        </div>
			<p ng-show="RyotMasterForm.villageCode.$error.required && (RyotMasterForm.villageCode.$dirty || submitted)" class="help-block">Select Village </p>													
												</div>													
				            	        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
	        	    				            <span class="input-group-addon"><i class="zmdi zmdi-my-location"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.city.$invalid && (RyotMasterForm.city.$dirty || submitted)}">																																																
					        	                <div class="fg-line">
												<label for="city">City</label>
        	    						          <input type="text" class="form-control" placeholder="City" maxlength="20" name="city" ng-model='AddRyot.city' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" tabindex="9" ng-blur="spacebtw('city')" id="city" with-floating-label>
				        	        	        </div>
							<p ng-show="RyotMasterForm.city.$error.required && (RyotMasterForm.city.$dirty || submitted)" class="help-block"> City is required</p>																																														
							<p ng-show="RyotMasterForm.city.$error.pattern && (RyotMasterForm.city.$dirty || submitted)" class="help-block"> Enter Valid City</p>																																																			
									</div>
			    	        	        	</div>
										</div>
									</div>
									<div class="row">
									    <div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
			        	                		<div class="fg-line" style="margin-top:5px;">
												  <label class="radio radio-inline m-r-20"><b>Status:</b></label>	
        		    					          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="inlineRadioOptions" value="0" name="status" data-ng-model='AddRyot.status'> 
			    					            	<i class="input-helper"></i>Active
										          </label>
				 			            		  <label class="radio radio-inline m-r-20">
						            			    <input type="radio" name="inlineRadioOptions" value="1" name="status" data-ng-model='AddRyot.status'>   
					    				            <i class="input-helper"></i>Inactive
							  		              </label>
			            		    	        </div>
			                    			</div>
										</div>
									</div>
									
								</div>
								<!------------>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.salutation.$invalid && (RyotMasterForm.salutation.$dirty || submitted)}">												
						        	            	<div class="fg-line">
	                						    		<select chosen class="w-100" name="salutation" ng-model='AddRyot.salutation' data-ng-required='true' ng-options="salutation.Id as salutation.Name for salutation in Salutation">
															<option value="">Salutation</option>
						        		    	        </select>
				        		        	    	</div>
							<p ng-show="RyotMasterForm.salutation.$error.required && (RyotMasterForm.salutation.$dirty || submitted)" class="help-block"> Select Salutation</p>																												
										</div>
				                		    </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
							<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.relativeName.$invalid && (RyotMasterForm.relativeName.$dirty || submitted)}">																								
						        	            <div class="fg-line"> 
													
 	 <input type="text" class="form-control" placeholder="{{RelationName}} Name" maxlength="50" name="relativeName" data-ng-model='AddRyot.relativeName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" tabindex="3" ng-blur="spacebtw('relativeName')">
						                	    </div>
					<p ng-show="RyotMasterForm.relativeName.$error.required && (RyotMasterForm.relativeName.$dirty || submitted)" class="help-block"> Relation Name is required</p>																																								
					<p ng-show="RyotMasterForm.relativeName.$error.pattern && (RyotMasterForm.relativeName.$dirty || submitted)" class="help-block"> Enter Valid Relation Name</p>																																								
							</div>												
				        		            </div>
										</div>
									</div>									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		        	    				        <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.mobileNumber.$invalid && (RyotMasterForm.mobileNumber.$dirty || submitted)}">
						        	            <div class="fg-line">
												<label for="mobile">Mobile Number</label>
            						<input type="text" class="form-control" placeholder="Mobile Number" maxlength="11" name="mobileNumber" ng-model='AddRyot.mobileNumber' data-ng-required='true' data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-minlength='10' tabindex="7" ng-blur="spacebtw('mobileNumber')" id="mobile" with-floating-label >
			        			        	    </div>
					<p ng-show="RyotMasterForm.mobileNumber.$error.required && (RyotMasterForm.mobileNumber.$dirty || submitted)" class="help-block"> Mobile Number is required</p>
					<p ng-show="RyotMasterForm.mobileNumber.$error.pattern && (RyotMasterForm.mobileNumber.$dirty || submitted)" class="help-block"> Enter Valid Mobile Number starting with 7,8,9 series</p>	
					<p ng-show="RyotMasterForm.mobileNumber.$error.minlength && (RyotMasterForm.mobileNumber.$dirty || submitted)" class="help-block"> Too Short</p>										
									</div>
					            	        </div>
										</div>
									</div>	
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            <span class="input-group-addon"><i class="zmdi zmdi-my-location"></i></span>
							<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.aadhaarNumber.$invalid && (RyotMasterForm.aadhaarNumber.$dirty || submitted)}">											
					        	            <div class="fg-line">
											<label for="aadhar">Aadhar Number</label>
    	        		<input type="text" class="form-control" placeholder="Aadhar Number" maxlength="20" name="aadhaarNumber" data-ng-model='AddRyot.aadhaarNumber' data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-minlength='12' tabindex="10" data-input-mask="{mask: '0000 0000 0000'}" ng-blur="spacebtw('aadhaarNumber')" id="aadhar" with-floating-label >
				        	        	    </div>
				<p ng-show="RyotMasterForm.aadhaarNumber.$error.required && (RyotMasterForm.aadhaarNumber.$dirty || submitted)" class="help-block"> Aadhar Number is required</p>
				<p ng-show="RyotMasterForm.aadhaarNumber.$error.pattern && (RyotMasterForm.aadhaarNumber.$dirty || submitted)" class="help-block"> Enter Valid Aadhar Number</p>																
				<p ng-show="RyotMasterForm.aadhaarNumber.$error.minlength && (RyotMasterForm.aadhaarNumber.$dirty || submitted)" class="help-block"> Too Short</p>																					
							</div>
				            	        </div>	
										</div>
										
									    <div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
			        	                		<div class="fg-line" style="margin-top:5px;">
												  <label class="radio radio-inline m-r-20"><b>Ryot Type:</b></label>	
        		    					          <label class="radio radio-inline m-r-20">
										            <input type="radio"  value="0" name="ryotType" data-ng-model='AddRyot.ryotType'> 
			    					            	<i class="input-helper"></i>Company
										          </label>
				 			            		  <label class="radio radio-inline m-r-20">
						            			    <input type="radio"  value="1" name="ryotType" data-ng-model='AddRyot.ryotType'>   
					    				            <i class="input-helper"></i>Outside
							  		              </label>
			            		    	        </div>
			                    			</div>
										</div>
									
									</div>																																			
								</div>
								<!--------------->
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.ryotName.$invalid && (RyotMasterForm.ryotName.$dirty || submitted)}">																																																																								
						        	            <div class="fg-line">
												
												<label for="RyotName">Ryot Name</label>
    			        			<input type="text" class="form-control autofocus" placeholder="Ryot Name" maxlength="40" autofocus name="ryotName" data-ng-model='AddRyot.ryotName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" tabindex="1" ng-blur="spacebtw('ryotName')"  id="RyotName" with-floating-label >
						                	    </div>
					<p ng-show="RyotMasterForm.ryotName.$error.required && (RyotMasterForm.ryotName.$dirty || submitted)" class="help-block"> Ryot Name is required</p>
					<p ng-show="RyotMasterForm.ryotName.$error.pattern && (RyotMasterForm.ryotName.$dirty || submitted)" class="help-block"> Enter Valid Ryot Name</p>																																																																											
									</div>
				        		            </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.mandalCode.$invalid && (RyotMasterForm.mandalCode.$dirty || submitted)}">		
						        	            <div class="fg-line">
												   	 <select chosen class="w-100" name="mandalCode" ng-model='AddRyot.mandalCode' data-ng-required='true' ng-options="mandalCode.id as mandalCode.mandal for mandalCode in MandalData | orderBy:'-mandal':true" tabindex="4" ng-disabled="AddedRyot!=null || AddedRyot!=''" ng-change='setNewRyotCode(AddRyot.villageCode);setVillageDropDown(AddRyot.mandalCode);'>
														<option value="">Select Mandal</option>
					        		    	        </select>													
						                	    </div>
							<p ng-show="RyotMasterForm.mandalCode.$error.required && (RyotMasterForm.mandalCode.$dirty || submitted)" class="help-block"> Select Mandal</p>		
										</div>
					    	                  </div>
											</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		        	    				        <span class="input-group-addon"><i class="zmdi zmdi-pin"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.address.$invalid && (RyotMasterForm.address.$dirty || submitted)}">																								
						        	            <div class="fg-line">
												<label for="add">Address</label>
            								        <input type="text" class="form-control" placeholder="Address"  maxlength="50" name="address" ng-model='AddRyot.address' data-ng-required='true' tabindex="8" ng-blur="spacebtw('address')" id="add" with-floating-label >
			        			        	    </div>
									<p ng-show="RyotMasterForm.address.$error.required && (RyotMasterForm.address.$dirty || submitted)" class="help-block"> Address is required</p>																									
								</div>												
			            	    		    </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-my-location"></i></span>
								<!--<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.panNumber.$invalid && (RyotMasterForm.panNumber.$dirty || submitted)}">-->												
					        	                <div class="fg-line">
													<label for="c">PAN Number</label>
    	        						          <input type="text" class="form-control" placeholder="PAN Number" maxlength="10"  name="panNumber" ng-model="AddRyot.panNumber"  data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"  tabindex="11" ng-blur="spacebtw('panNumber')"  id="c" with-floating-label>
				        	        	        </div>
							<!--<p ng-show="RyotMasterForm.panNumber.$error.required && (RyotMasterForm.panNumber.$dirty || submitted)" class="help-block"> PAN Number is required</p>-->																									
							<p ng-show="RyotMasterForm.panNumber.$error.pattern && (RyotMasterForm.panNumber.$dirty || submitted)" class="help-block"> Enter Valid PAN Number</p>																												
							<!--<p ng-show="RyotMasterForm.panNumber.$error.minlength && (RyotMasterForm.panNumber.$dirty || submitted)" class="help-block"> Too Short</p>-->																												
												<!--</div>-->												
				            	        	</div>
										</div>
									</div>
									<div class="row" ng-hide="true">
									    <div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.suretyRyotCode.$invalid && (RyotMasterForm.suretyRyotCode.$dirty || submitted)}">																																																												
					        	                <div class="fg-line">
<select  chosen multiple data-placeholder="Select Surety Ryot" class="w-100" name="suretyRyotCode" data-ng-model='AddRyot.suretyRyotCode' ng-options="suretyryot.id as suretyryot.ryotname for suretyryot in AddedRyotsData | orderBy:'-ryotname':true" tabindex="12" >
                    </select>																										
													
				        	        	        </div>
									</div>												
				            	        	</div>
										</div>
									</div>
									
								</div>
								
	        	        		<input type="hidden" class="form-control" name="circleCode" data-ng-model='AddRyot.circleCode'>
								<input type="hidden" data-ng-model='AddRyot.aadhaarImagePath' name="aadhaarImagePath"/>
								<input type="hidden" data-ng-model='AddRyot.ryotPhotoPath' name="ryotPhotoPath" />
								<!------------->
							</div><br /><hr />
							
							<!-----------Ryot Bank Details---------->							
							 
							 <div class="table-responsive">
							 	<table class="table table-stripped">
									<thead>
										<tr>
											<th>Action</th>
											<th>Season</th>
											<th>Branch Code</th>
											<th>IFSC Code</th>
											<th>Account No.</th>
											<th>Account Type</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat='BranchData in data'>
											<td>
	<button type="button" id="right_All_1" class="btn btn-primary add" ng-click="addFormField();" ng-if="BranchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>											
	<button type="button" id="right_All_1" class="btn btn-primary add" ng-click="removeRow(BranchData.id);"  ng-if="BranchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>													
											</td>
											<td>
												<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.season{{BranchData.id}}.$invalid && (RyotMasterForm.season{{BranchData.id}}.$dirty || submitted)}">	
							                	     <select class="form-control1" name="season{{BranchData.id}}" ng-model='BranchData.season' data-ng-required='true' ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true">
														<option value="">Select Season</option>
						        	     	        </select>
													<p ng-show="RyotMasterForm.season{{BranchData.id}}.$error.required && (RyotMasterForm.season{{BranchData.id}}.$dirty || submitted)" class="help-block"> Required</p>	
												</div>	
</td>										<td>
<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.ryotBankBranchCode{{BranchData.id}}.$invalid && (RyotMasterForm.ryotBankBranchCode{{BranchData.id}}.$dirty || submitted)}">											
	                						    <select class="form-control1" name="ryotBankBranchCode{{BranchData.id}}" ng-model='BranchData.ryotBankBranchCode'  ng-options="branchCode.id as branchCode.branchname for branchCode in BranchDropData | orderBy:'-branchname':true" ng-change='setifscCode(BranchData.id,BranchData.ryotBankBranchCode);'>
													<option value="">Select Branch Code</option>
				        		    	        </select>
<p ng-show="RyotMasterForm.ryotBankBranchCode{{BranchData.id}}.$error.required && (RyotMasterForm.ryotBankBranchCode{{BranchData.id}}.$dirty || submitted)" class="help-block"> Required</p>												
</div>												
											</td>
											<td>
												<input type="text" class="form-control1" placeholder="IFSC Code" maxlength="20"  name="ifscCode{{BranchData.id}}" data-ng-model="BranchData.ifscCode" readonly/>
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.accountNumber{{BranchData.id}}.$invalid && (RyotMasterForm.accountNumber{{BranchData.id}}.$dirty || submitted)}">																						
												<input type="text" class="form-control1" placeholder="Account Number" maxlength="25" name="accountNumber{{BranchData.id}}" data-ng-model='BranchData.accountNumber'  data-ng-pattern="/^[0-9\s]*$/" data-ng-minlength="7" />
<p ng-show="RyotMasterForm.accountNumber{{BranchData.id}}.$error.required && (RyotMasterForm.accountNumber{{BranchData.id}}.$dirty || submitted)" class="help-block"> Required</p>
<p ng-show="RyotMasterForm.accountNumber{{BranchData.id}}.$error.pattern && (RyotMasterForm.accountNumber{{BranchData.id}}.$dirty || submitted)" class="help-block"> Invalid</p>																																																
<p ng-show="RyotMasterForm.accountNumber{{BranchData.id}}.$error.minlength && (RyotMasterForm.accountNumber{{BranchData.id}}.$dirty || submitted)" class="help-block"> Too short</p>																																																
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : RyotMasterForm.accountType{{BranchData.id}}.$invalid && (RyotMasterForm.accountType{{BranchData.id}}.$dirty || submitted)}">																																	
												<select class="form-control1" name="accountType{{BranchData.id}}" ng-model='BranchData.accountType' data-ng-required='true'>
													<option value="" ng-selected="BranchData.accountType=='Select Accoount Type'">Select Account Type</option>
													<option value="0" ng-selected="BranchData.accountType=='0'">Savings A/C</option>
													<option value="1" ng-selected="BranchData.accountType=='1'">Loan A/C</option>
				        		    	        </select>
<p ng-show="RyotMasterForm.accountType{{BranchData.id}}.$error.required && (RyotMasterForm.accountType{{BranchData.id}}.$dirty || submitted)" class="help-block"> Required</p>												
</div>												
												<input type="hidden" name="id" ng-model='BranchData.id' />
											</td>
											
										</tr>
									</tbody>
								</table>
							 </div>	
								
							<!----------Bank Details End------------>
								
								<div class="row"> 							   						
								   <div class="col-sm-6" align="right">
<div class="form-group">																												   
									 <div class="fileinput fileinput-new" data-provides="fileinput">
						                <div class="fileinput-preview thumbnail" data-trigger="fileinput">	
											<!--<img src="{{AddRyot.ryotPhotoPath}}" alt="{{AddRyot.ryotPhotoPath}}" ng-if="AddRyot.ryotPhotoPath!='0'">										
											{{AddRyot.ryotPhotoPath}}-->
											<img id="blah" src="{{AddRyot.ryotPhotoPath}}" alt="{{AddRyot.ryotPhotoPath}}"   ng-if="AddRyot.ryotPhotoPath!='0'"   />
										</div>
						               <div>
                					    <span class="btn btn-info btn-file">
		                        			<span class="fileinput-new" id="freshryotimage"><i class="glyphicon glyphicon-picture"></i> Select Ryot image</span>
			        		                <span class="fileinput-new" id="ryotchangeimage" style="display:none;">Change Ryot image</span>
    			            		    	 
												<!--<input type="file"  name="file" onchange="angular.element(this).scope().uploadRyotImage(this.files)" />-->
												
												 <input type='file' id="imgInp"  name="file" onchange="angular.element(this).scope().uploadRyotImage(this.files)" />
        
												
					                	</span>
        					           
            	    				</div>
<p class="help-block" style="color:#FF0000; display:none;" id="image"> Ryot Image is required</p>
</div>												
									
					            	</div>							 							 
								   </div>
								   <div class="col-sm-6">
<div class="form-group">																														   
									 <div class="fileinput fileinput-new" data-provides="fileinput">
						                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
											<!--<img src="{{AddRyot.aadhaarImagePath}}" alt="{{AddRyot.aadhaarImagePath}}" ng-if="AddRyot.aadhaarImagePath!='0'">-->
											 <img id="blah1"  src="{{AddRyot.aadhaarImagePath}}" alt="{{AddRyot.aadhaarImagePath}}" src="#" ng-if="AddRyot.aadhaarImagePath!='0'" />
											
										</div>
						               <div>
                					    <span class="btn btn-info btn-file">
		                        			<span class="fileinput-new"><i class="glyphicon glyphicon-picture"></i>Select Aadhar Image</span>
			        		                <span class="fileinput-exists">Change Aadhar Image</span>
    			            		    	   
												
												 <input type='file' id="imgInp1" onchange="angular.element(this).scope().uploadAadharFile(this.files)"/>
												
					                	</span>
        					     
            	    				</div>
					            	</div>							 							 
<p class="help-block" style="color:#FF0000; display:none;" id="Aadhar"> Aadhar is required</p>																								
</div>												
									
								   </div>
								</div>
								
       
								
								<!-- <input type='file' id="imgInp" /><br><br>
        <img id="blah" src="#" alt="your image" style='width:25%;height:25%;'  data-ng-model='AddRyot.ryotPhotoPath' name="ryotPhotoPath" />-->
		
		
		
							 <div class="row" align="center">            			
							 	<div class="col-sm-12">
									<div class="input-group">
										<div class="fg-line">
											<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
											<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(RyotMasterForm,AddedRyot)">Reset</button>
											
										</div>
									</div>						 	
								</div>
							 </div>
					   </form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
				</div>
	    </section> 
	</section>
	
	
	
	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
