	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GeneratePermits" ng-init="loadPermitSeason();getPermitRules();getNewPermitRules();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Permits</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	<form name="GeneratePermitFilterForm"  novalidate>
							<input type="hidden" name="pageName" ng-model="PermitFilter.pageName"/>
							<input type="hidden" name="permitMaxNumber" ng-model="PermitFilter.maxNumber" />
							<div class="row">
								
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GeneratePermitFilterForm.season.$invalid && (GeneratePermitFilterForm.season.$dirty || Filtersubmitted)}">	
						        	                <div class="fg-line">
        		    						          <div class="select">
                							    		<select chosen class="w-100" name="season" ng-model="PermitFilter.season" ng-options="season.season as season.season for season in GeneratePermiteasonNames | orderBy:'-season':true" ng-change="loadProgramBySeason(PermitFilter.season);loadPermitMaxNumber(PermitFilter.season)" ng-required="true">
															<option value="">Season</option>
						        			            </select>
        		        	            			  </div>
			    		            	    	    </div>
													<p ng-show="GeneratePermitFilterForm.season.$error.required && (GeneratePermitFilterForm.season.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GeneratePermitFilterForm.programno.$invalid && (GeneratePermitFilterForm.programno.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
        	        						    		<select chosen class="w-100" name="programno" ng-model="PermitFilter.programno" ng-options="programno.programnumber as programno.programnumber for programno in GeneratePermitProgramNames | orderBy:'-programnumber':true" ng-required="true">
															<option value="">Program Number</option>
							        		            </select>
        		    	    	            		  </div>
			    			            	        </div>
													<p ng-show="GeneratePermitFilterForm.programno.$error.required && (GeneratePermitFilterForm.programno.$dirty || Filtersubmitted)" class="help-block">Program is Required.</p>
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GeneratePermitFilterForm.fromCcsRating.$invalid && (GeneratePermitFilterForm.fromCcsRating.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
														<input type="text" class="form-control" placeholder="From avgCCSRating" maxlength="50" name="fromCcsRating" data-ng-model='PermitFilter.fromCcsRating' data-ng-required='true' data-ng-pattern="/^[0-9]{1,7}(\.[0-9]+)?$/">
														
													</div>
													<p ng-show="GeneratePermitFilterForm.fromCcsRating.$error.required && (GeneratePermitFilterForm.fromCcsRating.$dirty || submitted)" class="help-block">Avg CcsRating is required.</p>												
													<p ng-show="GeneratePermitFilterForm.fromCcsRating.$error.pattern  && (GeneratePermitFilterForm.fromCcsRating.$dirty || submitted)" class="help-block">Enter a valid CcsRating.</p>
													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : GeneratePermitFilterForm.toCcsRating.$invalid && (GeneratePermitFilterForm.toCcsRating.$dirty || Filtersubmitted)}">
						        	                <div class="fg-line">
														<input type="text" class="form-control" placeholder="To avgCCSRating" maxlength="50" name="toCcsRating" data-ng-model='PermitFilter.toCcsRating' data-ng-required='true' data-ng-pattern="/^[0-9]{1,7}(\.[0-9]+)?$/">
														
													</div>
													<p ng-show="GeneratePermitFilterForm.toCcsRating.$error.required && (GeneratePermitFilterForm.toCcsRating.$dirty || submitted)" class="help-block">Avg CcsRating is required.</p>												
													<p ng-show="GeneratePermitFilterForm.toCcsRating.$error.pattern  && (GeneratePermitFilterForm.toCcsRating.$dirty || submitted)" class="help-block">Enter a valid CcsRating.</p>
													
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
			        	                		<div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10"  ng-click="loadPermitDetails(PermitFilter.season,PermitFilter.programno,PermitFilter.pageName,PermitFilter.fromCcsRating,PermitFilter.toCcsRating);">Submit</button>
					                	        </div>
			    		                	</div>
										</div>
									</div>
							</div>
							</div>
						
							
						</form>
						<input type="hidden" name="noOfTonsPeraAcre" ng-model="PermitRules.noOfTonsPeraAcre" />
						<input type="hidden" name="noOfTonsPerEachPermit" ng-model="PermitRules.noOfTonsPerEachPermit" />
						<input type="hidden" name="permitType" ng-model="permitType" />
						<input type="hidden" name="noofpermitperacre" ng-model="noofpermitperacre"  />
						<hr />							
						<!-------------------------------------table--------------->
						<form name="GeneratePermitGridForm" ng-submit="SavePermitNumbers(GeneratePermitGridForm);" novalidate>
							<!--<div  class="table-responsive">				
							   <section class="asdok">
								  <div class="container1">													 
								 	<table class="table table-striped">
									 	<thead>
										  <tr>
            		   			    		 <th><span>Rank</span></th>
									         <th><span>Ryot Code</span></th>
											 <th><span>Ryot Name</span></th>
											 <th><span>Agrmeent<br />No.</span></th>
											 <th><span>Permit<br />Date</span></th>
											 <th><span>Plot No.</span></th>
											 <th><span>Plant<br />Ratoon</span></th>											
											 <th><span>Extent Size</span></th>
											 <th><span>Zone</span></th>
											 <th><span>Circle</span></th>
											 <th><span>Permit Number</span></th>
            							 </tr>											
									   </thead>
									   <tbody>
										 <tr ng-repeat="PermtiData in GeneratePermitData">
                		    				<td>
												{{PermtiData.rank}}
												<input type="hidden" name="rank" ng-model="PermtiData.rank" />
											</td>
							    	    	<td>
												{{PermtiData.ryotCode}}
												<input type="hidden" name="ryotCode" ng-model="PermtiData.ryotCode" />
											</td>
											<td>
												{{PermtiData.ryotName}}
												<input type="hidden" name="ryotName" ng-model="PermtiData.ryotName" />
											</td>
											<td>
												{{PermtiData.agreementNumber}}
												<input type="hidden" name="agreementNumber" ng-model="PermtiData.agreementNumber" />
											</td>
											<td>
											<input type="text" class="form-control1 datepicker autofocus" name="permitDate" ng-model="PermtiData.permitDate" ng-mouseover="ShowDatePicker();" data-input-mask="{mask: '00-00-0000'}"/>
											</td>
											<td>
												{{PermtiData.plotNumber}}
												<input type="hidden" name="plotNumber" ng-model="PermtiData.plotNumber" />
											</td>
											<td>
												{{PermtiData.plantOrRatoon}}
												<input type="hidden" name="plantOrRatoon" ng-model="PermtiData.plantOrRatoon" />
											</td>											
											<td>
												{{PermtiData.plotSize}}
												<input type="hidden" name="plotSize" ng-model="PermtiData.plotSize" />
											</td>
											<td>
												{{PermtiData.zone}}
												<input type="hidden" name="zone" ng-model="PermtiData.zone" />
											</td>
											<td>
												{{PermtiData.circle}}
												<input type="hidden" name="circle" ng-model="PermtiData.circle" />
											</td>
											<td style="width:50%;">
												<div class="input-group input-group-unstyled">
													<div class="form-group" ng-class="{ 'has-error' : GeneratePermitGridForm.permitNumber{{$index}}.$invalid && (GeneratePermitGridForm.permitNumber{{$index}}.$dirty || submitted)}">
	                   									<input type="text" class="form-control1" placeholder='Permit Number' name="permitNumber{{$index}}" ng-model="PermtiData.permitNumber" ng-required="true" readonly/ tooltip-placement="bottom"  uib-tooltip="{{PermtiData.permitNumber}}" tooltip-trigger="mouseenter" tooltip-enable="PermtiData.permitNumber">
														<p ng-show="GeneratePermitGridForm.permitNumber{{$index}}.$error.required && (GeneratePermitGridForm.permitNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
													</div>
								    	    	</div>												
											</td>
	    	        					</tr>								
									</tbody>		
								</table>
							</div>
						  </section>
						</div>-->
						<tabset justified="true">
			                <tab  heading="Ryot Info">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Rank</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
															<th><span>Agreement No.</span></th>
															<th><span>Zone</span></th>
															<th><span>Circle</span></th>
															
															
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="PermtiData in GeneratePermitData">
															<td>
												{{PermtiData.rank}}
												<input type="hidden" name="rank" ng-model="PermtiData.rank" />
											</td>
							    	    	<td>
												{{PermtiData.ryotCode}}
												<input type="hidden" name="ryotCode" ng-model="PermtiData.ryotCode" />
											</td>
											<td>
												{{PermtiData.ryotName}}
												<input type="hidden" name="ryotName" ng-model="PermtiData.ryotName" />
											</td>
											<td>
												{{PermtiData.agreementNumber}}
												<input type="hidden" name="agreementNumber" ng-model="PermtiData.agreementNumber" />
											</td>
												
											<td>
												{{PermtiData.zone}}
												<input type="hidden" name="zone" ng-model="PermtiData.zone" />
											</td>
											<td>
												{{PermtiData.circle}}
												<input type="hidden" name="circle" ng-model="PermtiData.circle" />
											</td>
											
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
							<tab  heading="Ryot Permit Information">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															
															<th><span>Rank</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Plot</span></th>
															
															<th><span>Plant/Ratoon</span></th>
															<th><span>Extent Size</span></th>
															<th><span>Permit Date</span></th>
															<th><span>Permit</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="PermtiData in GeneratePermitData">
															
													<td>
												{{PermtiData.rank}}
												<input type="hidden" name="rank" ng-model="PermtiData.rank" />
											</td>
							    	    	<td>
												{{PermtiData.ryotCode}}
												<input type="hidden" name="ryotCode" ng-model="PermtiData.ryotCode" />
											</td>		
														
											<td>
												{{PermtiData.plotNumber}}
												<input type="hidden" name="plotNumber" ng-model="PermtiData.plotNumber" />
											</td>
											
											<td>
												{{PermtiData.plantOrRatoon}}
												<input type="hidden" name="plantOrRatoon" ng-model="PermtiData.plantOrRatoon" />
											</td>											
											<td>
												{{PermtiData.plotSize}}
												<input type="hidden" name="plotSize" ng-model="PermtiData.plotSize" />
											</td>
											<td>
											<input type="text" class="form-control1 datepicker autofocus" name="permitDate" ng-model="PermtiData.permitDate" ng-mouseover="ShowDatePicker();" data-input-mask="{mask: '00-00-0000'}"/>
											</td>
											
											<td>
												<div class="input-group input-group-unstyled">
													<div class="form-group" ng-class="{ 'has-error' : GeneratePermitGridForm.permitNumber{{$index}}.$invalid && (GeneratePermitGridForm.permitNumber{{$index}}.$dirty || submitted)}">
	                   									<input type="text" class="form-control1" placeholder='Permit Number' name="permitNumber{{$index}}" ng-model="PermtiData.permitNumber" ng-required="true" readonly/ tooltip-placement="bottom"  uib-tooltip="{{PermtiData.permitNumber}}" tooltip-trigger="mouseenter" tooltip-enable="PermtiData.permitNumber">
														<p ng-show="GeneratePermitGridForm.permitNumber{{$index}}.$error.required && (GeneratePermitGridForm.permitNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
													</div>
								    	    	</div>												
											</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
							</tabset>
						<div class="row" align="center">
							<div class="col-sm-12">
					            <div class="input-group">
				        	        <div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="GeneratePermits(PermitFilter.maxNumber);">Generate Permit Numbers</button>
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
			            	    	</div>
				               </div>			                    
					       </div>
						</div>
				    </form>
				</div>
			 </div>
		</div>
	 </section> 
</section>

<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
