	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
</style>

	<script type="text/javascript">		
		var data = [{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'}];
		sessionStorage.setItem('data1',JSON.stringify(data));		
	</script>
	<script type="text/javascript">
	var rowCount = 0;
	function addMoreRows(frm) 
	{
		rowCount ++;
		$('#tab_logic').append('<tr id="addr'+rowCount+'"></tr>');
		$('#addr'+rowCount).html('<td width="100px;"><button type="button" id="right_All_1" class="btn btn-primary" onclick="removeRow('+rowCount+');"><i class="glyphicon glyphicon-remove-sign"></i></button></td><td width="150px;"><input type="text" class="form-control1" placeholder="Plot No"/></td><td width="150px;"><input type="text" class="form-control1" placeholder="Variety"/></td><td width="200px;"><input type="text" class="form-control1" placeholder="Plant/Ratoon"/></td><td width="200px;"><input type="text" class="form-control1" placeholder="Extent"/></td><td width="100px;"><input type="text" class="form-control1" placeholder="Avg Qty" /></td><td width="150px;"><input type="text" class="form-control1"  placeholder="Survey No"/></td>');
	}

	function removeRow(removeNum) 
	{	
		jQuery('#addr'+removeNum).remove();
	}	
	
	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Perform Seed Accounting Calculation</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>					 		
							 	<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
							                 <td data-title="'Seed Supplr Code'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='Seed Supplier Ryot Code' readonly id="input_ryotcodegrid"/></span>
					            		      </td>
											 <td data-title="'Name'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='Ryot Name' readonly id="input_ryotnamegrid"/></span>
					            		      </td>
											 <td data-title="'Extent Size'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='Extent Size' id="input_caneqtygrid"/></span>
					            		      </td>
											   
											  <td data-title="'Amount'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='Amount' id="input_valuegrid"/></span>
					            		      </td>
											
											
											  <td data-title="'Loans'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='Loans' id="input_adddeducgrid"/></span>
					            		      </td>
											  <td data-title="'To SB A/C'">
                		    					<span ng-if="!w.$edit"><input type="text" class="form-control1" placeholder='To SB A/C' id="input_netpayablegrid"/></span>
					            		      </td>
							             </tr>
								         </table>							 
							     </div>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/SeedadvanceCalculation" class="btn btn-primary btn-sm m-t-10">Perform Calculations</a>
										<a href="#/admin/admindata/masterdata/SeedadvanceCalculation" class="btn btn-primary btn-sm m-t-10">Update</a>
									</div>
								</div>						 	
							 </div> 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
