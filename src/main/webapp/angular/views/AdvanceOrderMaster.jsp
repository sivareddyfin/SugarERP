	<style type="text/css">
		.border
		{
			border-bottom: solid 1px  #CCCCCC;
			width:100%;
			font-weight:bold;
		}		
	</style>	
	<script type="text/javascript">		
		$('#autofocus').focus();								
	</script>
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>
	<script type="text/javascript">
            
		function moveUp()
        {
            var ddl = document.getElementById('contentlist');
            //var size = ddl.length;
            //var index = ddl.selectedIndex;
            var selectedItems = new Array();
			var temp = {innerHTML:null, value:null};
            for(var i = 0; i < ddl.length; i++)
                if(ddl.options[i].selected)             
                    selectedItems.push(i);

            if(selectedItems.length > 0)    
                if(selectedItems[0] != 0)
                    for(var i = 0; i < selectedItems.length; i++)
                    {
						temp.innerHTML = ddl.options[selectedItems[i]].innerHTML;
						temp.value = ddl.options[selectedItems[i]].value;
                        ddl.options[selectedItems[i]].innerHTML = ddl.options[selectedItems[i] - 1].innerHTML;
                        ddl.options[selectedItems[i]].value = ddl.options[selectedItems[i] - 1].value;
                        ddl.options[selectedItems[i] - 1].innerHTML = temp.innerHTML; 
                        ddl.options[selectedItems[i] - 1].value = temp.value; 
                        ddl.options[selectedItems[i] - 1].selected = true;
                        ddl.options[selectedItems[i]].selected = false;
                    }
        }

        function moveDown()
        {
            var ddl = document.getElementById('contentlist');
            //var size = ddl.length;
            //var index = ddl.selectedIndex;
            var selectedItems = new Array();
			var temp = {innerHTML:null, value:null};
            for(var i = 0; i < ddl.length; i++)
                if(ddl.options[i].selected)             
                    selectedItems.push(i);

            if(selectedItems.length > 0)    
                if(selectedItems[selectedItems.length - 1] != ddl.length - 1)
                    for(var i = selectedItems.length - 1; i >= 0; i--)
                    {
                        temp.innerHTML = ddl.options[selectedItems[i]].innerHTML;
                        temp.value = ddl.options[selectedItems[i]].value;
                        ddl.options[selectedItems[i]].innerHTML = ddl.options[selectedItems[i] + 1].innerHTML;
                        ddl.options[selectedItems[i]].value = ddl.options[selectedItems[i] + 1].value;
                        ddl.options[selectedItems[i] + 1].innerHTML = temp.innerHTML; 
                        ddl.options[selectedItems[i] + 1].value = temp.value; 
                        ddl.options[selectedItems[i] + 1].selected = true;
                        ddl.options[selectedItems[i]].selected = false;
                    }
        }
			
        </script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="orderAdvanceMaster" ng-init="loadseasonNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Order Advances</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="orderAdvanceMaster" ng-submit="SaveOrderAdvances(AddOrderAdvances,orderAdvanceMaster);"  novalidate>		
						  
						  <div class="row" style="margin-left:30%;">
						      <div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : orderAdvanceMaster.season.$invalid && (orderAdvanceMaster.season.$dirty || Addsubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddOrderAdvances.season" data-ng-required="true"       ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true"  >
										<option value="">Select Season</option>

                    							</select>
                	            	   </div>
                	        </div>
							<p ng-show="orderAdvanceMaster.season.$error.required && (orderAdvanceMaster.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div> 
						  
						  
						  
							   <!--<div class="col-sm-4">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddOrderAdvances.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true">
                        									<option value="">Select Season</option>

                   									 			</select>
													 		</div>
                	        							</div>
													</div>
												</div>
		                             		</div>
				                    	</div>	
									</div>-->
							
							
								<!--<div class="col-sm-4">
 									<div class="input-group">
            	           				 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
        	              						 <div class="fg-line">
						                              <select chosen class="w-100" name="season" data-ng-model="AddOrderAdvances.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true">
                                					<option value="">Select Season</option>

                    								</select>
												</div>
                    						</div>	
										</div>-->
							
							
									<!--<div class="col-sm-4">
										 <div class="input-group">
            	                              <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												  <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : orderAdvanceMaster.inputDate.$invalid && (orderAdvanceMaster.inputDate.$dirty || Addsubmitted)}">
        	                                         <div class="fg-line">
													 <label for="effe">Effective From Date</label>
<input type="text" class="form-control input_date autofocus" placeholder='Effective From Date' maxlength="10" ng-model="AddOrderAdvances.inputDate" autofocus name="inputDate" data-input-mask="{mask: '00-00-0000'}"  id="effe" with-floating-label data-ng-required="true" />
                	                        		</div>
													<p ng-show="orderAdvanceMaster.inputDate.$error.required && (orderAdvanceMaster.inputDate.$dirty || Addsubmitted)" class="help-block">Effective From Date is required</p>													
												</div>
                    	                  	</div>	
										 </div>
										 -->





														<div class="col-sm-6">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="getAdvancesOrder(AddOrderAdvances);">Get Advances</button>
															</div>
														</div>
													<br>

							
					 		<div class="row">            			
								<div class="col-md-3"></div>
								<div class="col-md-6">  								      										 		
									<select name="from[]" id="contentlist" class="form-control1" size="8" multiple="multiple" autofocus ng-model="AddOrderAdvances.OrderAdvance">
										<option ng-repeat="advCode in AddOrderAdvancesData" value="{{advCode.advanceCode}}-{{advCode.advanceName}}-{{advCode.id}}" class="border">{{$index+1}} - {{advCode.advanceName}}</option>
									</select>
								</div>
								<div class="col-md-1" style="vertical-align:middle;">
									<div style=" margin-top:50px;">            			
										<button type="button" id="right_All_1" class="btn btn-primary" onClick="moveUp(); return false;"><i class="glyphicon glyphicon-chevron-up"></i></button><br /><p></p>
										<button type="button" id="right_All_1" class="btn btn-primary" onClick="moveDown(); return false;"><i class="glyphicon glyphicon-chevron-down"></i></button>															
									</div>
								</div>
							</div>
							
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">																		
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(orderAdvanceMaster)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
														
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
