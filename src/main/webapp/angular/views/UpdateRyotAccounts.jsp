	<script type="text/javascript">
		$('#date').focus();
	</script>
	<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateRyotAccounts" ng-init="loadUpdateRyotVillageNames();loadServerDate();loadUpdateRyotSeasonNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> Update Ryot Accounts </b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	<form name="UpdateRyotAccountFilterForm" ng-submit="getUpdateRyotAccountDetails(UpdateFilterData);" novalidate>	
					 		<div class="row">
								<div class="col-md-1"></div>
							  	<div class="col-md-3">
									<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div  class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : UpdateRyotAccountFilterForm.season.$invalid && (UpdateRyotAccountFilterForm.season.$dirty || FilterSubmitted)}">
											  	<div class="fg-line">
													<select chosen class="w-100" name="season" ng-options="season.season as season.season for season in UpdateSeasonNames | orderBy:'-season':true" ng-model="UpdateFilterData.season" ng-required="true">
														<option value="">Select Season</option>
													</select>
												</div>
												<p data-ng-show="UpdateRyotAccountFilterForm.season.$error.required && (UpdateRyotAccountFilterForm.season.$dirty || FilterSubmitted)" class="help-block">Season is required</p>
											</div>
									</div>
								</div>	
								<div class="col-md-3">
									<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div  class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : UpdateRyotAccountFilterForm.UpdateFilterVillage.$invalid && (UpdateRyotAccountFilterForm.UpdateFilterVillage.$dirty || FilterSubmitted)}">
											  	<div class="fg-line">
													<select chosen class="w-100" name="UpdateFilterVillage" ng-options="UpdateFilterVillage.id as UpdateFilterVillage.village for UpdateFilterVillage in UpdateVillageNames | orderBy:'-village':true" ng-model="UpdateFilterData.village" ng-required="true">
														<option value="">Select Village</option>
													</select>
												</div>
												<p data-ng-show="UpdateRyotAccountFilterForm.UpdateFilterVillage.$error.required && (UpdateRyotAccountFilterForm.UpdateFilterVillage.$dirty || FilterSubmitted)" class="help-block">Village is required</p>
											</div>
									</div>
								</div>									
							    <div class="col-md-3">
							   	 	<div class="input-group">
								 		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>	
										<div  class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : UpdateRyotAccountFilterForm.AccountingDate.$invalid && (UpdateRyotAccountFilterForm.AccountingDate.$dirty || FilterSubmitted)}">
										   	<div class="fg-line">
												<label for="date">Accounting Date</label>
												<input type="text" name="AccountingDate" ng-model="UpdateFilterData.caneaccdate" class="form-control datepicker"  ng-required="true" placeholder="Acccounting Date" id="date" with-floating-label autofocus data-input-mask="{mask: '00-00-0000'}"/>
											</div>
											<p data-ng-show="UpdateRyotAccountFilterForm.AccountingDate.$error.required && (UpdateRyotAccountFilterForm.AccountingDate.$dirty || FilterSubmitted)" class="help-block">Date is required</p>
										</div>
									  </div>
							   </div>						
							   <div class="col-md-2">
							   	  <button type="submit" class="btn btn-primary btn-sm m-t-10">Get Details</button>     
							   </div>	   
					        </div>
						 </form>
						 <hr />							 					
						<!------------------------->
						<tabset justified="true">
			                <tab  heading="Company Advances">
								<div class="table-responsive">
								  <form name="CompanyAdvanceSubmit" novalidate ng-submit="saveCompanyAdvances();">						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Sl. No.</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
															<th><span>Adv Code</span></th>
															<th><span>Advance</span></th>
															<th><span>Payable</span></th>
															<th><span>Paid Amount</span></th>
															<th><span>Pending Amount</span></th>
														</tr>
													</thead>
													<tbody>
														<tr ng-repeat="AdvanceData in AdvanceTab">
															<td>{{$index+1}}</td>
															<td>{{AdvanceData.ryotcode}}</td>
															<td>{{AdvanceData.ryotname}}</td>
															<td>{{AdvanceData.advancecode}}</td>
															<td>1123</td>
															<td>{{AdvanceData.payableamount}}</td>
															<td>
																<div  class="form-group"  ng-class="{ 'has-error' : CompanyAdvanceSubmit.paidamount{{$index}}.$invalid && (CompanyAdvanceSubmit.paidamount{{$index}}.$dirty || AdvanceSubmitted)}">
																	<input type="number" name="paidamount{{$index}}" ng-model="AdvanceData.paidamount" ng-keyup="calculatePendingAmount(AdvanceData.paidamount,$index,'AdvanceTab');" style="text-align:right;" ng-required="true" max="{{AdvanceData.payableamount}}"/>
																	<p data-ng-show="CompanyAdvanceSubmit.paidamount{{$index}}.$error.required && (CompanyAdvanceSubmit.paidamount{{$index}}.$dirty || AdvanceSubmitted)" class="help-block">required</p>
																	<p data-ng-show="CompanyAdvanceSubmit.paidamount{{$index}}.$error.max && (CompanyAdvanceSubmit.paidamount{{$index}}.$dirty || AdvanceSubmitted)" class="help-block">Invalid</p>
																</div>
															</td>
															<td>
																<input type="text" name="Pendingamount{{$index}}" ng-model="AdvanceData.pendingamount" readonly style="text-align:right;"/>
															</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
									<div align="center"><button type="submit" class="btn btn-primary btn-sm m-t-10">Update</button></div>											
									</form>
								</div>
    			            </tab>
                			<tab  heading="Loans">
								<div class="table-responsive">
									<form name="CompanyLoanSubmit" novalidate ng-submit="saveCompanyLoans();">
									<section class="asdok">
										<div class="container1">																			
							                  <table class="table table-striped" style="width:100%;">								                  
												 <thead>
													<tr>
														<th><span>Sl. No.</span></th>
														<th><span>Ryot Code</span></th>
														<th><span>Bank Code</span></th>
														<th><span>Loan Amount Payable</span></th>
														<th><span>Amount Paid</span></th>
														<th><span>Pending Amount</span></th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="LoanData in LoansTab">
														<td>{{$index+1}}</td>
														<td>{{LoanData.ryotcode}}</td>
														<td>1</td>
														<td>{{LoanData.payableamount}}</td>
														<td>
															<div  class="form-group"  ng-class="{ 'has-error' : CompanyLoanSubmit.paidamount{{$index}}.$invalid && (CompanyLoanSubmit.paidamount{{$index}}.$dirty || LoanSubmitted)}">
																<input type="number" name="paidamount{{$index}}" ng-model="LoanData.paidamount" ng-keyup="calculatePendingAmount(LoanData.paidamount,$index,'LoansTab');" style="text-align:right;" ng-required="true" max="{{LoanData.payableamount}}"/>
																	<p data-ng-show="CompanyLoanSubmit.paidamount{{$index}}.$error.required && (CompanyLoanSubmit.paidamount{{$index}}.$dirty || LoanSubmitted)" class="help-block">required</p>
																	<p data-ng-show="CompanyLoanSubmit.paidamount{{$index}}.$error.max && (CompanyLoanSubmit.paidamount{{$index}}.$dirty || LoanSubmitted)" class="help-block">Invalid</p>
																
															</div>
														</td>
														<td>
															<input type="text" name="pendingamount{{$index}}" ng-model="LoanData.pendingamount" style="text-align:right;"/>
														</td>
													</tr>										
												</tbody>
											</table>											
									</div>
								 </section>
								 <div align="center"><button type="submit" class="btn btn-primary btn-sm m-t-10">Update</button></div>										
								 </form>
							  </div>
            			    </tab>
			                <tab  heading="Harvesting Charges">
            			      <div class="table-responsive">
							    <form name="HarvestingChargesSubmit" novalidate ng-submit="saveHarvestingCharges();">
								<section class="asdok">
									<div class="container1">															  									 
						                  <table class="table table-striped" style="width:100%;">								                  
											 <thead>
												<tr>
													<th><span>Sl. No.</span></th>
													<th><span>Ryot Code</span></th>
													<th><span>Harvesting Cont. Code</span></th>
													<th><span>Charges Payable</span></th>
													<th><span>Paid</span></th>
													<th><span>Pending Amount</span></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="HarvestingData in HarvesterTab">
													<td>{{$index+1}}</td>
													<td>{{HarvestingData.ryotcode}}</td>
													<td>{{HarvestingData.harvestcontcode}}</td>
													<td>{{HarvestingData.pendingpayable}}</td>
													<td>
														<div  class="form-group"  ng-class="{ 'has-error' : HarvestingChargesSubmit.paidamount{{$index}}.$invalid && (HarvestingChargesSubmit.paidamount{{$index}}.$dirty || HarvestingSubmitted)}">													
															<input type="number" name="paidamount{{$index}}" ng-model="HarvestingData.paidamount"  ng-keyup="calculatePendingAmount(HarvestingData.paidamount,$index,'HarvesterTab');" style="text-align:right;" ng-required="true" max="{{HarvestingData.pendingpayable}}"/>
															<p data-ng-show="HarvestingChargesSubmit.paidamount{{$index}}.$error.required && (HarvestingChargesSubmit.paidamount{{$index}}.$dirty || HarvestingSubmitted)" class="help-block">required</p>
															<p data-ng-show="HarvestingChargesSubmit.paidamount{{$index}}.$error.max && (HarvestingChargesSubmit.paidamount{{$index}}.$dirty || HarvestingSubmitted)" class="help-block">Invalid</p>															
														</div>
													</td>
													<td>
														<input type="text" name="pendingamount{{$index}}" ng-model="HarvestingData.pendingamount" style="text-align:right;"/>
													</td>
												</tr>										
											</tbody>
										</table>										
								  </div>
								</section>
								<div align="center"><button type="submit" class="btn btn-primary btn-sm m-t-10">Update</button></div>									
								</form>
							  </div>
			                </tab>
            			    <tab  heading="Transport Charges">
				              <div class="table-responsive">
							  <form name="TransportingChargesSubmit" novalidate ng-submit="saveTransportingCharges();">
								<section class="asdok">
								   <div class="container1">															  							  								  
					                  	<table class="table table-striped" style="width:100%;">								                  
											 <thead>
												<tr>
													<th><span>Sl. No.</span></th>
													<th><span>Ryot Code</span></th>
													<th><span>Transporting Cont. Code</span></th>
													<th><span>Charges Payable</span></th>
													<th><span>Paid</span></th>
													<th><span>Pending Amount</span></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="TransporterData in TransporterTab">
													<td>{{$index+1}}</td>
													<td>{{TransporterData.ryotcode}}</td>											
													<td>{{TransporterData.transportcontcode}}</td>
													<td>{{TransporterData.pendingpayable}}</td>
													<td>
														<div  class="form-group"  ng-class="{ 'has-error' : TransportingChargesSubmit.paidamount{{$index}}.$invalid && (TransportingChargesSubmit.paidamount{{$index}}.$dirty || TransportingSubmitted)}">													
															<input type="number" name="paidamount{{$index}}" ng-model="TransporterData.paidamount" ng-keyup="calculatePendingAmount(TransporterData.paidamount,$index,'TransporterTab');" style="text-align:right;" ng-required="true" max="{{TransporterData.pendingpayable}}"/>
															<p data-ng-show="TransportingChargesSubmit.paidamount{{$index}}.$error.required && (TransportingChargesSubmit.paidamount{{$index}}.$dirty || TransportingSubmitted)" class="help-block">required</p>
															<p data-ng-show="TransportingChargesSubmit.paidamount{{$index}}.$error.max && (TransportingChargesSubmit.paidamount{{$index}}.$dirty || TransportingSubmitted)" class="help-block">Invalid</p>																														
														</div>
													</td>
													<td>
														<input type="text" name="pendingamount{{$index}}" ng-model="TransporterData.pendingamount" style="text-align:right;"/>
													</td>
												</tr>										
											</tbody>
										</table>										
								   </div>
								</section>
								<div align="center"><button type="submit" class="btn btn-primary btn-sm m-t-10">Update</button></div>									 
								</form>
							  </div>
			                </tab>
            			    <tab  heading="SB A/C">
				              <div class="table-responsive">
							  <form name="SBAccountSubmit" novalidate ng-submit="saveRyotSBACDetails();">																			
								<section class="asdok">
									<div class="container1">											
						                  <table class="table table-striped" style="width:100%;">								                  
											 <thead>
												<tr>
													<th><span>Sl. No.</span></th>
													<th><span>Is It ICICI</span></th>
													<th><span>Ryot Code</span></th>
													<th><span>SB Code</span></th>
													<th><span>Bank</span></th>
													<th><span>Payable</span></th>
													<th><span>Paid</span></th>
													<th><span>Pending Amount</span></th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="BankData in BankAcTab">
													<td>{{$index+1}}</td>
													<td>1</td>
													<td>{{BankData.ryotcode}}</td>
													<td>{{BankData.sbcode}}</td>
													<td>1</td>
													<td>1</td>
													<td>
														<div  class="form-group"  ng-class="{ 'has-error' : SBAccountSubmit.paidamount{{$index}}.$invalid && (SBAccountSubmit.paidamount{{$index}}.$dirty || SBSubmitted)}">														
															<input type="number" name="paidamount{{$index}}" ng-model="BankData.paidamount" ng-keyup="calculatePendingAmount(BankData.paidamount,$index,'BankAcTab');" style="text-align:right;" ng-required="true"/>
															<p data-ng-show="SBAccountSubmit.paidamount{{$index}}.$error.required && (SBAccountSubmit.paidamount{{$index}}.$dirty || SBSubmitted)" class="help-block">required</p>
															<p data-ng-show="SBAccountSubmit.paidamount{{$index}}.$error.max && (SBAccountSubmit.paidamount{{$index}}.$dirty || SBSubmitted)" class="help-block">Invalid</p>																														
															
														</div>
													</td>
													<td>
														<input type="text" name="pendingamount{{$index}}" ng-model="BankData.pendingamount" style="text-align:right;"/>
													</td>
												</tr>										
											</tbody>
										</table>										
								 </div>
							   </section>	
							   <div align="center"><button type="submit" class="btn btn-primary btn-sm m-t-10">Update</button></div>									
							   </form>
							 </div>
            			    </tab>				
			            </tabset>            									
						<!---------------------------->
						</div>						
					<!----------end----------------------->															
				</div>																												
			</div>		
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
