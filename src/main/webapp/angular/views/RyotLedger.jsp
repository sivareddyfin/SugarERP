	<script>
	  $(function() 
	  {
	     $( ".datepickerFrom" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	     $( ".datepickerTo" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		

	 
	
	  });
    </script>
	<style>
	tr:nth-child(even) {background: #CCC}
tr:nth-child(odd) {background: #FFF}
	</style>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">
        	<div class="container" data-ng-controller="RyotLedger" ng-init="loadTransportSeason();">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> General Ledger</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					     <form name="ryotLedgerForm" novalidate ng-submit="getRyotLedger(ryotLedger,ryotLedgerForm);">
					 		<div class="row">
								<div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : ryotLedgerForm.season.$invalid && (ryotLedgerForm.season.$dirty || FilterSubmitted)}">
			        	               			 <div class="fg-line">
            					         			 <div class="select">
										  	<select chosen class="w-100" name="season" ng-model="ryotLedger.season" ng-options="season.season as season.season for season in getLedgerSeasons | orderBy:'-season':true"  data-ng-required="true" ng-change="getAgreedRyots(ryotLedger.season);">
												<option value="">Season</option>
											</select>
										  </div>
			                	        </div>
												<p data-ng-show="ryotLedgerForm.season.$error.required && (ryotLedgerForm.season.$dirty || FilterSubmitted)" class="help-block">Select Season</p>
										</div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-4">
				                    
											<label for="ryotcodeorname">Account Name or Account Code</label>
											<input list="stateList" placeholder="Account Name or Account Code" id="ryotcodeorname" class="form-control"  name="ryotcode" ng-model = "ryotLedger.ryotcode" with-floating-label ng-change="loadRyotDatainSession(ryotLedger.ryotcode);">
    								<datalist id="stateList">
        							<select class="form-control">
           						 <option ng-repeat="ryot in accName" value="{{ryot.accName}}"></option>
       						    </select>    
   									</datalist>
											
										
									
									
										                    
				                  </div>
								
								
									
									<div class="col-sm-4">
								 <button type="submit" class="btn btn-primary btn-sm m-t-10" ng-click="generateLedger(ryotLedger,ryotLedgerForm);">Generate Ledger</button>
								 															
								</div>   								  
    	        			 </div>
							 <p></p>
							 <!--<div class="row">
							 	<div class="col-sm-12" align="center">
								 <button type="submit" class="btn btn-primary btn-sm m-t-10" ng-click="generateLedger(ryotLedger,ryotLedgerForm);">Generate Ledger</button>
								 																
								</div>								
							 </div>-->
						</form>
						<div class="table-responsive" id="dispLedger" style="display:none;">
						<hr />
						
								 
						
						  <table  border="0" style="width:100%; border-collapse:collapse;">
						  <thead>
							        <tr style="height:40px; border:;" >
										<th colspan="6" style="text-align:center; border:none;">SRI SARVARAYA SUGARS LTD<br>CHELLURU</th>
            						</tr>
										
								</thead>
								<thead>
								 <tr style="height:10px;border:none;" ng-repeat="ryotLedger in ryotLedgers" >
										<th colspan="2" style="text-align:Left;border:none;"><b>Ryot Code &nbsp; :{{ryotLedger.ryotCode}}</b></th>
										
										<tr>
							        <tr style="height:10px;border:none;" ng-repeat="ryotLedger in ryotLedgers">
										
										<th colspan="1" style="text-align:Left;border:none;"><b>Ryot Name :&nbsp;{{ryotLedger.ryotname}} </b></th>																			
										<th colspan="1" style="text-align:Left;border:none;"><b>&nbsp </b></th>  
										<th colspan="1" style="text-align:Left;border:none;"><b>Netwt(M.Tons):{{ryotLedger.supplyQty}} </b></th> 
            						</tr>
								</thead>
							<!--	<thead> 
									 <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">									
										<th colspan="1" style="text-align:Left;border:none;"><b><u>S/o   :{{ryotLedger.relativeName}} </u></b></th>																				
            						</tr>	
								</thead> 
									
									 <thead>
							       <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">
									
								
            						</tr>											
								</thead>-->
								<thead>
							        <tr style="height:20px;border-collapse:collapse;" ng-repeat="ryotLedger in ryotLedgers">
									<th colspan="2" style="text-align:Left;border:none;"><b>S/o &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{ryotLedger.relativeName}} </b></th>	
										<th colspan="2" style="text-align:Left;border:none;"><b>Tieup Loan  :{{ryotLedger.advAmount}} </b></th>
            						</tr>
								</thead> 
								<thead>
							        <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">									
									<th colspan="2" style="text-align:Left;border:none;"><b>Mandal  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:{{ryotLedger.mandal}} </b></th>	
										<th colspan="2" style="text-align:Left;border:none;"><b>Transport Allowance :{{ryotLedger.tpAllowance}}  </b></th>
            						</tr>
								</thead> 
								<thead>
							        <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">									
									<th colspan="1" style="text-align:Left;border:none;"><b>Village &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :{{ryotLedger.village}} </b></th>
            						</tr>
								</thead> 
								
								<thead>
							        <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">									
									<th colspan="1" style="text-align:Left;border:none;"><b>Pincode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :{{ryotLedger.pincode}}</b></th>
            						</tr>
								</thead> 
								<thead>
							        <tr style="height:20px;border:none;" ng-repeat="ryotLedger in ryotLedgers">									
									<th colspan="1" style="text-align:Left;border:none;"><b><u style='margin-left:275px;'>Statement As On :{{currentDate}}  </u></b></th>
            						</tr>
								</thead> 
									</table>										
							<table  border="1" style="width:100%;">
						 		 <thead>
								
							        <tr style="height:40px;">
										<th style="width:15%;"><b>Date</b></th>
                		   				<th style="width:45%;"><b>Description</b></th>
										<th  style="text-align:right; width:12%;" ><b>Debit(Rs)</b></th>
						                <th style="text-align:right; width:12%;"><b>Credit(Rs)</b></th>										
		                   				<th style="text-align:right; width:16%;" ><b>Running Bal.</b></th>
            						</tr>											
								</thead>	
								<tbody>
									<tr ng-repeat="ledgerData in loadLedgerData" style="height:30px;">
										<input type="hidden" ng-model="ledgerData.runbalance" />
										<td>{{ledgerData.transactiondate}}</td>
										
                		   				<td style="font-size:10px;">{{ledgerData.journalmemo}}</td>
							            <td style="text-align:right;">{{ledgerData.dr}}</td>
										<td style="text-align:right;">{{ledgerData.cr}}</td>
		                    			<td style="text-align:right;">{{ledgerData.Runbalance}}( {{ledgerData.transType}} )</td>
            						</tr>
									<tr style=" color:#000000;">
									<td colspan="2"  style="text-align:right" ><b>TOTAL</b></td>
									<td style="text-align:right">{{debitTotal}}</td>
										<td style="text-align:right">{{creditTotal}}</td>
										<td style="text-align:right"></td>
										
									</tr>
								</tbody>		
							 </table>
							 
						 <div id="printButton" style='text-align:center;'><button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="ryotLedgerPrint();">Print</button></div>
							 
							</div> 
						  </div>							 						           			
				        </div>
			    	</div>
				</div>
	    </section>
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
