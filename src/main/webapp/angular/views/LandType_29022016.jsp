	<script type="text/javascript">	
		$('#autofocus').focus();	
	</script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="LandTypeMaster" ng-init="loadRyotTypeId();loadAllRyotTypes();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Land Type Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 <form name="RyotTypeForm" data-ng-submit='AddRyotType(AddRyot);' novalidate>
					 	<div class="row">
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	                <div class="fg-line">
												<input type="text" class="form-control" placeholder="Land ID" readonly data-ng-model='AddRyot.ryotTypeId' name="RyotID"/>
				                	        </div>
			    	                	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Description" maxlength="50" ng-model='AddRyot.description' name="Description"  tabindex="2"/>
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					<div class="form-group" ng-class="{ 'has-error' : RyotTypeForm.RyotType.$invalid && (RyotTypeForm.RyotType.$dirty || Addsubmitted)}">
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Land Type" id="autofocus" maxlength="25" data-ng-model='AddRyot.ryotType' name="RyotType" data-ng-required='true' tabindex="1" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
			                	        </div>
								<p ng-show="RyotTypeForm.RyotType.$error.required && (RyotTypeForm.RyotType.$dirty || Addsubmitted)" class="help-block">Ryot Type is required.</p>
								<p ng-show="RyotTypeForm.RyotType.$error.pattern  && (RyotTypeForm.RyotType.$dirty || Addsubmitted)" class="help-block">Enter a valid Name.</p>
									  </div>
			                    	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="Active" ng-model='AddRyot.status' name="status" tabindex="3"> 
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="Inactive" ng-model='AddRyot.status' name="Status" tabindex="4"> 
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="submit" class="btn btn-primary">Reset</button>
									</div>
								</div>						 	
							 </div>
					 </form>  
						  
						  
						  
						  
						
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Ryot Types</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">		
								  <form name="UpdateLandTypeMaster" novalidate>				
							        <table class="table table-striped table-vmiddle" ng-table="RyotTypeMaster.tableEdit">
										
								        <tr ng-repeat="EditAddedRyots in AddedRyot"  ng-class="{ 'active': EditAddedRyots.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!EditAddedRyots.$edit" ng-click="EditAddedRyots.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="EditAddedRyots.$edit" ng-click="UpdateRyotTypes(EditAddedRyots,$index);EditAddedRyots.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Land ID'">
                		    					<span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.ryotTypeId}}</span>
					    		                <div ng-if="EditAddedRyots.$edit">
													<input class="form-control" type="text" ng-model="EditAddedRyots.ryotTypeId" maxlength="25" placeholder='Ryot ID' readonly/>
												</div>
					            		      </td>											 
							                 <td data-title="'Land Type'">
                		    					<span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.ryotType}}</span>
					    		                <div ng-if="EditAddedRyots.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateLandTypeMaster.ryotType{{$index}}.$invalid && (UpdateLandTypeMaster.ryotType{{$index}}.$dirty || submitted)}">												
<input class="form-control" type="text" ng-model="EditAddedRyots.ryotType" maxlength="25" placeholder='Ryot Type' name="ryotType{{$index}}"/>
	<p ng-show="UpdateLandTypeMaster.ryotType{{$index}}.$error.required && (UpdateLandTypeMaster.ryotType{{$index}}.$dirty || submitted)" class="help-block">Ryot Type is required.</p>
	<p ng-show="UpdateLandTypeMaster.ryotType{{$index}}.$error.pattern  && (UpdateLandTypeMaster.ryotType{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Name.</p>

</div>													
												</div>
					            		      </td>
		                    				  <td data-title="'Description'">
							                     <span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.description}}</span>
							                     <div ng-if="EditAddedRyots.$edit"><input class="form-control" type="email" ng-model="EditAddedRyots.description" maxlength="50" placeholder='Description'/></div>
							                  </td>
											  <td data-title="'Status'">
							                      <span ng-if="!EditAddedRyots.$edit">{{EditAddedRyots.status}}</span>
        		            					  <div ng-if="EditAddedRyots.$edit">	
			            					          <label class="radio radio-inline m-r-20">
								            				<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="Active" checked="checked" ng-model='EditAddedRyots.status'>
			    			            					<i class="input-helper"></i>Active
													  </label>
				 			              			  <label class="radio radio-inline m-r-20">
															<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="Inactive" ng-model='EditAddedRyots.status'>
			    				                            <i class="input-helper"></i>Inactive
					  		                          </label>
											      </div>
										      </td>
							             </tr>
								         </table>
									  </form>							 
							     </div>
							</div>
						</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
