	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>
	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="PrepareLoanMaster" data-ng-init="loadseasonNames();loadBranchNames();loadServerDate();">
        	<div class="container" >				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Prepare Tie Up Loans</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 <form name="PrepareLoanForm" ng-submit='PrepareLoanSubmit(AddedPrepareLoan,PrepareLoanForm);'  novalidate>
					 		<div class="row">
								<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.season.$invalid && (PrepareLoanForm.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedPrepareLoan.season' name="season" ng-options="season.season as season.season for season in seasonNames" ng-change="loadAgreements(AddedPrepareLoan.season);getseasonRyots(AddedPrepareLoan.season);loadAddedSeasonloan(AddedPrepareLoan.season);">
															<option value="">Select Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.season.$error.required && (PrepareLoanForm.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.branchCode.$invalid && (PrepareLoanForm.branchCode.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedPrepareLoan.branchCode' name="branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in branchNames | orderBy:'-branchname':true" ng-required="true" ng-change="loadPrepareLoanCode(AddedPrepareLoan.season,AddedPrepareLoan.branchCode);">
															<option value="">Select Branch</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.branchCode.$error.required && (PrepareLoanForm.branchCode.$dirty || Filtersubmitted)" class="help-block">Select Branch</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.addedLoan.$invalid && (PrepareLoanForm.addedLoan.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<!--<select chosen class="w-100"  tabindex="2"  data-ng-model='AddedPrepareLoan.addedLoan' name="addedLoan" ng-options="addedLoan.loannumber as addedLoan.loannumber for addedLoan in loanNames" data-ng-required="true">
															<option value="">Added Loans</option>
															
						        			            </select>-->
														<select chosen class="w-100"  tabindex="2"  data-ng-model='AddedPrepareLoan.addedLoan' name="addedLoan" ng-options="addedLoan.loanNumber as addedLoan.loanNumber for addedLoan in loanNames | orderBy:'-loanNumber':true" data-ng-required="true">
															<option value="">Added Loans</option>
	
    								    	             </select>														
		                	            			  </div>
					<p ng-show="PrepareLoanForm.addedLoan.$error.required && (PrepareLoanForm.addedLoan.$dirty || Filtersubmitted)" class="help-block">Select Added Loans</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<button type="button" class="btn btn-primary" ng-click="getAllPrepareTieup(AddedPrepareLoan.addedLoan,AddedPrepareLoan.season,PrepareLoanForm,AddedPrepareLoan.branchCode);">Get Details</button>
										</div>
									</div>
								</div>
							</div>								
							<hr />
							<div class="row">
								<div class="col-sm-4">
									
									
									<div class="row">									
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.ryotCode.$invalid && (PrepareLoanForm.ryotCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<!--<select chosen class="w-100"  tabindex="3" data-ng-required='true' data-ng-model='AddedPrepareLoan.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.ryotname for ryotCode in ryotCodeData | orderBy:'-ryotname':true" ng-change="getRyotCode();">
															<option value="">Select Ryot Name</option>
						        			            </select>-->
														<select chosen class="w-100"  tabindex="3" data-ng-required='true' data-ng-model='AddedPrepareLoan.ryotCode' name="ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodeData | orderBy:'-ryotcode':true" ng-change="getRyotCode(AddedPrepareLoan.ryotCode);getRyotName(AddedPrepareLoan.ryotCode,AddedPrepareLoan.season); getagreement(AddedPrepareLoan.ryotCode,AddedPrepareLoan.season);">
															<option value="">Select Ryot Code</option>
								        	             </select>														
		                	            			  </div>
					<p ng-show="PrepareLoanForm.ryotCode.$error.required && (PrepareLoanForm.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>		 
											  </div>
			                    	      </div>
										</div>	
										
									</div>
									
									<div class="row">
										 <div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.loanNumber.$invalid && (PrepareLoanForm.loanNumber.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="exampleInputEmail1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Loan Number"  maxlength="10" name="loanNumber" data-ng-model='AddedPrepareLoan.loanNumber'    ng-pattern="/^[0-9]+(\.-[0-9]{1,2})?$/" tabindex="5" data-ng-required="true"  id="exampleInputEmail1" with-floating-label>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.loanNumber.$error.required && (PrepareLoanForm.loanNumber.$dirty || Addsubmitted)" class="help-block">Loan Number is required</p>
					<p ng-show="PrepareLoanForm.loanNumber.$error.pattern && (PrepareLoanForm.loanNumber.$dirty || Addsubmitted)" class="help-block">Valid Loan Number is required</p>											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.plantExtent.$invalid && (PrepareLoanForm.plantExtent.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													 <label for="exampleInputEmail1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Plant Extent(Acres)"  name="plantExtent" data-ng-model='AddedPrepareLoan.plantExtent'    tabindex="9" ng-required="true" id="exampleInputEmail1" with-floating-label readonly>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.plantExtent.$error.required && (PrepareLoanForm.plantExtent.$dirty || Addsubmitted)" class="help-block">Plant Extent is required</p>
					
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : recommendedDate.$invalid && (recommendedDate.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.recommendedDate.$invalid && (PrepareLoanForm.recommendedDate.$dirty || Addsubmitted)}">
    	        								         <input type="text"  class="form-control input_date" placeholder="Date"  maxlength="25"  name="recommendedDate" data-ng-model='AddedPrepareLoan.recommendedDate' ng-readonly="true" data-ng-required='true' tabindex="12"  data-input-mask="{mask: '00-00-0000'}">
														 <p ng-show="PrepareLoanForm.recommendedDate.$error.required && (PrepareLoanForm.recommendedDate.$dirty || Addsubmitted)" class="help-block">Date required.</p>
																																				
</div>			</div>											 
				                				    </div>
								
																					
												</div>
					                    	</div>
									</div>
									
									
								</div>	
								<div class="col-sm-4">
									<div class="row">		
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<!--<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.ryotName.$invalid && (PrepareLoanForm.ryotName.$dirty || Addsubmitted)}">-->				
					        	                	<div class="fg-line">
													 <label for="exampleInputPassword1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Ryot Name"  autofocus  maxlength="50" name="ryotName" data-ng-model='AddedPrepareLoan.ryotName'   tabindex="4" id="exampleInputPassword1" with-floating-label readonly="readonly"> 
		                	            			  </div>
					<!--<p ng-show="PrepareLoanForm.ryotName.$error.required && (PrepareLoanForm.ryotName.$dirty || Addsubmitted)" class="help-block">Ryot Name is required</p>
					<p ng-show="PrepareLoanForm.ryotName.$error.pattern && (PrepareLoanForm.ryotName.$dirty || Addsubmitted)" class="help-block">Valid Ryot Name is required</p>-->
											  <!--</div>-->
			                    	      </div>
										</div>	
									</div>
									<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.agreementNumber.$invalid && (PrepareLoanForm.agreementNumber.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen multiple class="w-100"  tabindex="7" data-ng-required='true' data-ng-model='AddedPrepareLoan.agreementNumber' name="agreementNumber" ng-options="agreementNumber.agreementnumber as agreementNumber.agreementnumber for agreementNumber in agreementNames" ng-change="getSurveuNumbers(AddedPrepareLoan.agreementNumber);" data-placeholder="Select Agreements">
															<option value="" >Select Agreements</option>
														
						        			            </select>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.agreementNumber.$error.required && (PrepareLoanForm.agreementNumber.$dirty || Addsubmitted)" class="help-block">Select Agreement Number</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<!--<input type="hidden"  name="addedLoan" data-ng-model='AddedPrepareLoan.addedLoan'>-->
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.ratoonExtent.$invalid && (PrepareLoanForm.ratoonExtent.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="exampleInputEmail1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Ratoon Extent(Acres)"   name="ratoonExtent" data-ng-model='AddedPrepareLoan.ratoonExtent' tabindex="10" ng-required="true" id="exampleInputEmail1" with-floating-label readonly>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.ratoonExtent.$error.required && (PrepareLoanForm.ratoonExtent.$dirty || Addsubmitted)" class="help-block">Ratoon Extent is required</p>
					
											  </div>
			                    	      </div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">								
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.branchCode.$invalid && (PrepareLoanForm.branchCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="6" data-ng-required='true' data-ng-model='AddedPrepareLoan.branchCode' name="branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in branchNames | orderBy:'-branchname':true" ng-change="setMaxLoanNumber(AddedPrepareLoan.branchCode,AddedPrepareLoan.season);">
															<option value="">Select Branch Code</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.branchCode.$error.required && (PrepareLoanForm.branchCode.$dirty || Addsubmitted)" class="help-block">Select Branch Code</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : PrepareLoanForm.surveyNumber.$invalid && (PrepareLoanForm.surveyNumber.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													 <label for="exampleInputEmail1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Survey Number"  name="surveyNumber" data-ng-model='AddedPrepareLoan.surveyNumber' tabindex="8" data-ng-required="true" id="exampleInputEmail1" with-floating-label readonly>
		                	            			  </div>
					<p ng-show="PrepareLoanForm.surveyNumber.$error.required && (PrepareLoanForm.surveyNumber.$dirty || Addsubmitted)" class="help-block"> Survey Number is required</p>
					<p ng-show="PrepareLoanForm.surveyNumber.$error.pattern && (PrepareLoanForm.surveyNumber.$dirty || Addsubmitted)" class="help-block">Valid Survey Number is required</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
									<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PrepareLoanForm.recommendedLoan.$invalid && (PrepareLoanForm.recommendedLoan.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
													<label for="exampleInputEmail1"></label>
    	            						    		<input type="text" class="form-control" placeholder="Recommended Loan"  maxlength="10" name="recommendedLoan" data-ng-model='AddedPrepareLoan.recommendedLoan' data-ng-pattern="/^[0-9\d]*$/" ng-blur="getLoanAmount()" tabindex="11" data-ng-required="true" id="exampleInputEmail1" with-floating-label>
		                	            			  </div>
						<p ng-show="PrepareLoanForm.recommendedLoan.$error.required && (PrepareLoanForm.recommendedLoan.$dirty || Addsubmitted)" class="help-block">Recommended Loan is required</p>
						<p ng-show="PrepareLoanForm.recommendedLoan.$error.pattern && (PrepareLoanForm.recommendedLoan.$dirty || Addsubmitted)" class="help-block">Recommended Loan in numbers is required</p>
											  </div>
			                    	      </div>
										</div>
									</div>			
								</div>
							</div>
								<br />
								<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(PrepareLoanForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>			
							 <input type="hidden" name="hiddenLoanNo" ng-model="hiddenLoanNo" />
							 <input type="hidden" name="screenName" ng-model="AddedPrepareLoan.screenName" />
						</form>
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
