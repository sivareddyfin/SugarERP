	<script type="text/javascript">		
		$('.autofocus').focus();			
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="MandalMaster" ng-init='loadMaxMandalId();loadZoneNames();loadAddedMandals();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Mandal Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="MandalMasterForm" ng-submit='SubmitMandals(AddMandal,MandalMasterForm);' novalidate>
						<input type="hidden" name="screenName" ng-model="AddMandal.screenName" />
							<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="mandalCode">Mandal Code</label>
            							          <input type="text" class="form-control" placeholder="Mandal Code"  maxlength="20" readonly data-ng-model='AddMandal.mandalCode' id="mandalCode" with-floating-label>
			            		    	        </div>
			                    			</div>		
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : MandalMasterForm.zoneCode.$invalid && (MandalMasterForm.zoneCode.$dirty || Addsubmitted)}">		
					        	                <div class="fg-line">
        		        				    		<select chosen class="w-100" name="zoneCode" data-ng-model='AddMandal.zoneCode' data-ng-required='true' ng-options="ZoneCode.id as ZoneCode.zone for ZoneCode in ZoneData  | orderBy:'-zone':true">
														<option value="">Select Zone</option>
				        				            </select>
				        	        	        </div>
										<p ng-show="MandalMasterForm.zoneCode.$error.required && (MandalMasterForm.zoneCode.$dirty || Addsubmitted)" class="help-block">Select Zone</p>
											  </div>
				            	        	</div>		
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" data-ng-model='AddMandal.status'>
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1" data-ng-model='AddMandal.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>		
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : MandalMasterForm.name.$invalid && (MandalMasterForm.name.$dirty || Addsubmitted)}">												
				        	                	<div class="fg-line">
												<label for="mandalname">Mandal Name</label>
													<input type="text" class="form-control autofocus" placeholder="Mandal Name" maxlength="25" name="name" data-ng-model='AddMandal.mandal' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtw('mandal');validateDup();" id="mandalname" with-floating-label>
			        	        	        	</div>
								<p ng-show="MandalMasterForm.name.$error.required && (MandalMasterForm.name.$dirty || Addsubmitted)" class="help-block">Mandal is required</p>
								<p ng-show="MandalMasterForm.name.$error.pattern  && (MandalMasterForm.name.$dirty || Addsubmitted)" class="help-block">Enter a valid Mandal</p>	
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddMandal.mandal!=null">Mandal Name Already Exist.</p>													
												
											</div>
			            	        	  </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	                	<div class="fg-line">
													
												<label for="description">Description</label>
													<input type="text" placeholder='Description(Optional)' name='description' class="form-control" maxlength="50" data-ng-model='AddMandal.description' ng-blur="spacebtw('description');" id="description" with-floating-label/>
			        	        	        	</div>
			            	        		</div>
										</div>
									</div><br />
									<input type="hidden"  name="modifyFlag" data-ng-model="AddMandal.modifyFlag"  />
									<div class="row">
										<div class="col-sm-12" align="left">
											<div class="input-group">
				        	                	<div class="fg-line">
													<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
													<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(MandalMasterForm)">Reset</button>
			        	        	        	</div>
			            	        		</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					 
					 
					 									 		
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
										
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Mandals</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
									<div class="container1">		
								  <form name="UpdateMandalMasterForm" novalidate>					
							        <table ng-table="MandalMaster.tableEdit" class="table table-striped table-vmiddle">
									<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Mandal Code</span></th>
																<th><span>Mandal Name</span></th>
																<th><span>Description</span></th>
																<th><span>Zone Name</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
										<tbody>
								        <tr ng-repeat="UpdateMandal in AddedMandals"  ng-class="{ 'active': UpdateMandal.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateMandal.$edit" ng-click="UpdateMandal.$edit = true;UpdateMandal.modifyFlag='Yes';UpdateMandal.screenName='Mandal Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success btn-hideg" ng-if="UpdateMandal.$edit" ng-click="UpdateAddedMandals(UpdateMandal,$index);UpdateMandal.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="UpdateMandal.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="UpdateMandal.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateMandal.$edit">{{UpdateMandal.mandalCode}}</span>
					    		                <div ng-if="UpdateMandal.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="UpdateMandal.mandalCode" placeholder='Mandal Code' maxlength="20" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!UpdateMandal.$edit">{{UpdateMandal.mandal}}</span>
							                     <div ng-if="UpdateMandal.$edit">
									<div class="form-group" ng-class="{ 'has-error' : UpdateMandalMasterForm.name{{$index}}.$invalid && (UpdateMandalMasterForm.name{{$index}}.$dirty || submitted)}">																				
											<input class="form-control" type="text" ng-model="UpdateMandal.mandal" placeholder='Mandal Name' maxlength="25" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" name="name{{$index}}" ng-blur="spacebtwgrid('mandal',$index);validateDuplicate(UpdateMandal.mandal,$index);"/>
						<p ng-show="UpdateMandalMasterForm.name{{$index}}.$error.required && (UpdateMandalMasterForm.name{{$index}}.$dirty || submitted)" class="help-block">Mandal is required.</p>
						<p ng-show="UpdateMandalMasterForm.name{{$index}}.$error.pattern  && (UpdateMandalMasterForm.name{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Mandal.</p>						
						<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="UpdateMandal.mandal!=null">Mandal Name Already Exist.</p>							
																					
									</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!UpdateMandal.$edit">{{UpdateMandal.description}}</span>
        		            					  <div ng-if="UpdateMandal.$edit">
												  	<div class="form-group">
													  	<input class="form-control" type="text" ng-model="UpdateMandal.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
													</div>
												  </div>
							                  </td>
							                  <td>
							                      <span ng-if="!UpdateMandal.$edit" ng-repeat='ZoneName in ZoneData'>
												  	<span ng-if="ZoneName.id==UpdateMandal.zoneCode">{{ZoneName.zone}}</span>
												  </span>
        		            					  <div ng-if="UpdateMandal.$edit">
												  	<div class="form-group" ng-class="{ 'has-error' : UpdateMandalMasterForm.zoneCode{{$index}}.$invalid && (UpdateMandalMasterForm.zoneCode{{$index}}.$dirty || submitted)}">
	        		        				    		<select class='form-control1' name="zoneCode" data-ng-model='UpdateMandal.zoneCode' data-ng-required='true' ng-options="ZoneCode.id as ZoneCode.zone for ZoneCode in ZoneData  | orderBy:'-zone':true" ng-required="true">
															<option value="">Select Zone</option>
				        				            	</select>
														<p ng-show="UpdateMandalMasterForm.zoneCode{{$index}}.$error.required && (UpdateMandalMasterForm.zoneCode{{$index}}.$dirty || submitted)" class="help-block">Zone is required.</p>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!UpdateMandal.$edit">
												  	<span ng-if="UpdateMandal.status=='0'">Active</span>
													<span ng-if="UpdateMandal.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="UpdateMandal.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" data-ng-model='UpdateMandal.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='UpdateMandal.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
							             </tr>
										 </tbody>
								         </table>							 
									</form>
									</div>
									</section>
							     </div>
							</div>
						</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
