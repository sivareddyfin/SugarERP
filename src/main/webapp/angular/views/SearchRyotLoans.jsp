

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	   <section id="content" data-ng-controller="SearchRyotLoanController" ng-init="loadryotCode();loadSeasonNames();">  
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Search Ryot Loans</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="SearchRyotLoan" ng-submit="SearchLoan(AddedSearchLoan)">
 								<div class="row">
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : SearchRyotLoan.season.$invalid && (SearchRyotLoan.season.$dirty || Filtersubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddedSearchLoan.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" ng-required='true'>
<option value="">Select Season</option>

                    </select>
                	            	   </div>
                	        </div>
							<p ng-show="SearchRyotLoan.season.$error.required && (SearchRyotLoan.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-3">

											<div class="row">
													<div class="col-sm-12">
														<div class="input-group">
            	             									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group" ng-class="{ 'has-error' : SearchRyotLoan.ryotName.$invalid && (SearchRyotLoan.ryotName.$dirty || Filtersubmitted)}">
        	               						 <div class="fg-line">
            	          							 <div class="select">
                	     	 						<select chosen class="w-100" name="ryotName" data-ng-model="AddedSearchLoan.ryotName" ng-options="ryotName.id as ryotName.id for ryotName in ryotNames | orderBy:'-id':true" ng-required='true' ng-change="getRyotCode(AddedSearchLoan.ryotName);">
<option value="">Select Ryot Code</option>

                    						</select>
                	            	   </div>
                	        </div>
							<p ng-show="SearchRyotLoan.ryotName.$error.required && (SearchRyotLoan.ryotName.$dirty || Filtersubmitted)" class="help-block">Select Ryot Code</p>
								</div>
                    	</div>
				</div>

			</div>
	</div>
			<div class="col-sm-3">

				<div class="row">
					<div class="col-sm-12">
								<div class="input-group">
            	             <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper" ng-class="{ 'has-error' : SearchRyotLoan.ryotCode.$invalid && (SearchRyotLoan.ryotCode.$dirty || Filtersubmitted)}">	
        	               						 <div class="fg-line">
												 <label for="ryotname">Ryot Name</label>
            	          
                	     	 <input type="text" class="form-control" placeholder="Ryot Name" name="ryotCode" data-ng-model="AddedSearchLoan.ryotCode" readonly="readonly"  id="ryotname" with-floating-label />
                	            	  
                	        </div>
					<p ng-show="SearchRyotLoan.ryotCode.$error.required && (SearchRyotLoan.ryotCode.$dirty || Filtersubmitted)" class="help-block">Ryot Name is required</p>
								</div>
                    	</div>
					</div>
				</div>
			</div>
								<div class="col-sm-3">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getRyotDetails();">Get Details</button>

													</div>
												</div>
  										</div>
									</div>
 								</form>
 							</div>

					
					
					<div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
										<form name="SearchRyotLoanForm" novalidate >				
							        <table ng-table="SearchRyotLoanController.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>LOAN SL NO.</span></th>
													<th><span>LOAN A/C NO.</span></th>
													<th><span>LOAN RECOMMENDED  <br />DATE</span></th>
													<th><span>FIELD OFFICER</span></th>
													<th><span>DISBURSED AMOUNT</span></th>
													<th><span>DATE OF DISBURSEMENT</span></th>
													
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="SearchRyot in SearRyotLoanData"  ng-class="{ 'active': SearchRyot.$edit }">
                		    				<td>
                		    					{{SearchRyot.loanNumber}}
					    		               
					            		      </td>
		                    				  
							                  <td>
							                     {{SearchRyot.referenceNumber}}
        		            					 
							                  </td>
											  <td>
							                      {{SearchRyot.recommendedDate}}
        		            					 
							                  </td>
											  <td>
							                     {{SearchRyot.fieldOfficer}}
        		            					 
												 
							                  </td>
											  <td>
							                      {{SearchRyot.disbursedAmount}}
        		            					
							                  </td>
											   <td>
							                     {{SearchRyot.disbursedDate}}
        		            					
							                  </td>
											   
											   									  
							           </tr>
									</tbody>
								 </table>
										 </form>	
										 </div>
										</section> 
																 
							     </div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
				 </div>
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
