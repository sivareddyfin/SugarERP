	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeedlingTrayRND" ng-init="loadTrayCode();loadAddedSeedlingTrays();addFormField(); loadZoneNames();loadVillageNames();loadCircleNames();loadTrayType();loadseasonNames();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Tray</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="SeedlingTrayMasterForm" novalidate ng-submit="AddSeedlingsSubmit(AddedSeedlingTray,SeedlingTrayMasterForm);">	
					 <div class="row">
						 <div class="col-sm-6">
									<div class="col-sm-12">
										<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.season.$invalid && (SeedlingTrayMasterForm.season.$dirty || submitted)}">												
															<div class="fg-line">
																<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingTray.season' name="season" ng-options="season.season as season.season for season in seasonNames" ng-change="getseasonRyots(AddedSeedlingTray.season);">
																	<option value="">Select Season</option>
																</select>
															  </div>
							<p ng-show="SeedlingTrayMasterForm.season.$error.required && (SeedlingTrayMasterForm.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>		 
													  </div>
												  </div>			                    
								</div>
						</div>	
						 <div class="col-sm-6">
										<div class="col-sm-12">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.ryotCode.$invalid && (SeedlingTrayMasterForm.ryotCode.$dirty || submitted)}">												
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="3" data-ng-required='true'  data-ng-model='AddedSeedlingTray.ryotCode' name="ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodeData  | orderBy:'-ryotCode':true">
																<option value="">Select Ryot Code</option>
															 </select>															
														</div>
			<p ng-show="SeedlingTrayMasterForm.ryotCode.$error.required && (SeedlingTrayMasterForm.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Ryot Code</p>		 
												  </div>
											  </div>
											</div>			                    
									  </div>	
					 </div>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group " ng-class="{ 'has-error' : SeedlingTrayMasterForm.zone.$invalid && (SeedlingTrayMasterForm.zone.$dirty || submitted)}">												
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="3" data-ng-required='true'  data-ng-model='AddedSeedlingTray.zone' name="zone" ng-options="zone.id as zone.zone for zone in ZoneData  | orderBy:'-zone':true" >
																<option value="">Zone</option>
															 </select>															
														</div>
			<p ng-show="SeedlingTrayMasterForm.zone.$error.required && (SeedlingTrayMasterForm.zone.$dirty || Addsubmitted)" class="help-block">Select Zone</p>		 
												  </div>
											  </div>
							</div>
							</div>
							<div class="row">
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SeedlingTrayMasterForm.vehicle.$invalid && (SeedlingTrayMasterForm.vehicle.$dirty || submitted)}">												
														<div class="fg-line">
														<label for="Vehicle">Vehicle</label>
															<input type="text" class="form-control" name="vehicle" ng-model="AddedSeedlingTray.vehicle" placeholder="Vehicle" data-ng-required="true" id="Vehicle" with-floating-label />														
														</div>
			<p ng-show="SeedlingTrayMasterForm.vehicle.$error.required && (SeedlingTrayMasterForm.vehicle.$dirty || Addsubmitted)" class="help-block">Vehicle. is required</p>		 
												  </div>
											  </div>
							</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="row">
							
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.village.$invalid && (SeedlingTrayMasterForm.village.$dirty || submitted)}">												
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="3" data-ng-required='true'  data-ng-model='AddedSeedlingTray.village' name="village" ng-options="village.id as village.village for village in VillagesNamesData  | orderBy:'-village':true">
																<option value="">Village</option>
															 </select>															
														</div>
			<p ng-show="SeedlingTrayMasterForm.village.$error.required && (SeedlingTrayMasterForm.village.$dirty || Addsubmitted)" class="help-block">Select Village</p>		 
												  </div>
											  </div>
							</div>
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SeedlingTrayMasterForm.driverNo.$invalid && (SeedlingTrayMasterForm.driverNo.$dirty || submitted)}">												
														<div class="fg-line">
														<label for="driverNo">Driver No.</label>
															<input type="text" class="form-control" name="driverNo" ng-model="AddedSeedlingTray.driverNo" placeholder="Driver No." data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/" maxlength="10" id="driverNo" with-floating-label />														
														</div>
			<p ng-show="SeedlingTrayMasterForm.driverNo.$error.required && (SeedlingTrayMasterForm.driverNo.$dirty || Addsubmitted)" class="help-block">Driver No. is required</p>
			<p ng-show="SeedlingTrayMasterForm.driverNo.$error.pattern && (SeedlingTrayMasterForm.driverNo.$dirty || Addsubmitted)" class="help-block">Valid Driver No. is required</p>		 
												  </div>
											  </div>
							</div>
							
							</div>
							
						</div>
						<div class="col-sm-4">
							<div class="row">
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.circle.$invalid && (SeedlingTrayMasterForm.circle.$dirty || submitted)}">												
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="3" data-ng-required='true'  data-ng-model='AddedSeedlingTray.circle' name="circle" ng-options="circle.id as circle.circle for circle in circleNames  | orderBy:'-circle':true" ng-change="loadSeedlingTrayFilter()">
																<option value="">Circle</option>
															 </select>															
														</div>
			<p ng-show="SeedlingTrayMasterForm.ryotCode.$error.required && (SeedlingTrayMasterForm.ryotCode.$dirty || Addsubmitted)" class="help-block">Select Circle</p>		 
												  </div>
											  </div>
							</div>
							</div>
							<div class="row">
							<div class="col-sm-12">
								<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : SeedlingTrayMasterForm.testedBy.$invalid && (SeedlingTrayMasterForm.testedBy.$dirty || submitted)}">												
														<div class="fg-line">
														<label for="testedBy">Verified By</label>
															<input type="text" class="form-control" name="testedBy" ng-model="AddedSeedlingTray.testedBy" placeholder="Verified By" data-ng-required="true" id="testedBy" with-floating-label />														
														</div>
			<p ng-show="SeedlingTrayMasterForm.testedBy.$error.required && (SeedlingTrayMasterForm.testedBy.$dirty || Addsubmitted)" class="help-block">Tested By is required</p>		 
												  </div>
											  </div>
							</div>
							</div>
						</div>
					</div>
							
							
						<br />
						
							
							
							<div class="row" >
							  
							  <div class="col-md-12">		
						 	 <div class="table-responsive"  align="center">
							 <table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th><span>Action</span></th>
												<th width="150"><span>Tray Type</span></th>
												<th width="150"><span>No. of Trays <br /> Return</span></th>
												<th width="150"><span>Trays in <br /> good condition (%)</span></th>
												<th width="150"><span>Damaged Tray (%)</span></th>
												<!--<th width="150"><span>Ryot Damaged <br /> trays</span></th>
												<th width="150"><span>Remaining Trays</span></th>-->
												<th width="150"><span>Tray taken date </span></th>
												
											</tr>
											<br />
										</thead>										
										<tbody>
											<tr ng-repeat='SeedlingData in data'>
												<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="SeedlingData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="SeedlingData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(SeedlingData.id);' ng-if="SeedlingData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<!--<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="SeedlingData.branchCode"/>-->
												</td>
												<!--<td>
		<input type="text" class="form-control1" placeholder="From" name="id" readonly data-ng-model="SeedlingData.id" style="margin-top:-10px;"/>
												</td>-->
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.trayType{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.trayType{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<select placeholder="Tray Type" class="form-control1"  name="trayType{{SeedlingData.id}}" data-ng-model="SeedlingData.trayType" data-ng-required='true' ng-options="trayType.id as trayType.TrayType for trayType in TrayTypeNames" >
	<option value="">Tray Type</option>
</select>
	<p ng-show="SeedlingTrayMasterForm.trayType{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.trayType{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	
</div>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$dirty || submitted)}">																																																											
<input type="text" class="form-control1" placeholder="No. of Trays return" name="noofTraysReturn{{SeedlingData.id}}"  data-ng-model="SeedlingData.noofTraysReturn" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-keyup="updateDamagedTray(SeedlingData.noofTraysReturn,SeedlingData.traysinGoodCondition,SeedlingData.id);" />
	<p ng-show="SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.noofTraysReturn{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
	
		
	<!--<p class="help-block duplicate{{SeedlingData.id}}" style="color:#FF0000; display:none;" ng-if="SeedlingData.branchName!=null">Branch Name Already Exist.</p>-->																																																				 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Tray in Good Condition" name="traysinGoodCondition{{SeedlingData.id}}" ng-model="SeedlingData.traysinGoodCondition" data-ng-required="true" data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="updateDamagedTray(SeedlingData.noofTraysReturn,SeedlingData.traysinGoodCondition,SeedlingData.id);" readonly    />
	<p ng-show="SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	<p ng-show="SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.traysinGoodCondition{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
													
	<!--<p ng-show="SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>-->																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Damaged Tray" name="damagedTray{{SeedlingData.id}}" ng-model="SeedlingData.damagedTray" data-ng-required="true" data-ng-pattern="/^[0-9.\s]*$/" readonly    />
	<p ng-show="SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	<p ng-show="SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.damagedTray{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
													
	<!--<p ng-show="SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>-->																																																																
</div>												 
												</td>
											<!--	<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Ryot Damaged Tray" name="ryotDamagedTray{{SeedlingData.id}}" ng-model="SeedlingData.ryotDamagedTray" data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/" ng-keyup="updateotherTrays(SeedlingData.damagedTray,SeedlingData.ryotDamagedTray,SeedlingData.id);"     />
	<p ng-show="SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	<p ng-show="SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.ryotDamagedTray{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
													
																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="Damaged Tray" name="damagedTrayTravelling{{SeedlingData.id}}" ng-model="SeedlingData.damagedTrayTravelling" data-ng-required="true" data-ng-pattern="/^[0-9\s]*$/" readonly     />
	<p ng-show="SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	<p ng-show="SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.damagedTrayTravelling{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>
													
																																																																
</div>												 
												</td>-->
												<td>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayMasterForm.trayDateTaken{{SeedlingData.id}}.$invalid && (SeedlingTrayMasterForm.trayDateTaken{{SeedlingData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1 datepicker" placeholder="Tray taken date" name="trayDateTaken{{SeedlingData.id}}" ng-model="SeedlingData.trayDateTaken" data-ng-required="true" ng-mouseover="ShowDatePicker();" readonly="readonly"  />
	<p ng-show="SeedlingTrayMasterForm.trayDateTaken{{SeedlingData.id}}.$error.required && (SeedlingTrayMasterForm.trayDateTaken{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Required</p>
	
													
	<!--<p ng-show="SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$error.pattern  && (SeedlingTrayMasterForm.trayCharge{{SeedlingData.id}}.$dirty || submitted)" class="help-block">Invalid</p>-->																																																																
</div>												 
												</td>
												
												
											</tr>

										</tbody>
									</table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							
							
							
							
													
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SeedlingTrayMasterForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>			 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
