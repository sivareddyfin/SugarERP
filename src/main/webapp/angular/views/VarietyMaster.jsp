	<script type="text/javascript">	
		$('#autofocus').focus();				
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="varietyMaster" ng-init='loadVarietyId();loadAllVarieties()'>   
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Variety Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					  	<form name="VarietyMaster" ng-submit="varietymastersubmit(AddedVarietyMaster,VarietyMaster)"  novalidate>
						<input type="hidden" name="screenName" ng-model="AddedVarietyMaster.screenName" />
							 <div class="row">							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" >
			                        	<div class="fg-line">
											<label for="varietyCode">Variety Code</label>
											<input type="text" class="form-control" placeholder="Variety Code" name="varietyCode"  data-ng-model="AddedVarietyMaster.varietyCode"         maxlength="25" readonly="readonly" id="varietyCode" with-floating-label  >  
			                        	</div>
										
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        	<div class="fg-line">
										<label for="Description">Description</label>
											<input type="text" class="form-control" placeholder="Description" maxlength="50"  name="description"  data-ng-model="AddedVarietyMaster.description"       tabindex="2" ng-blur="spacebtw('description');" id="Description" with-floating-label   >
			                        	</div>

			                
			                    </div>			                    
			                </div>
							   
							 
							   
							
							
							
							
</div>



							</div>
							
							
							
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									
									<div class="form-group" ng-class="{ 'has-error' : VarietyMaster.name.$invalid && (VarietyMaster.name.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
										<label for="varietyName">Variety Name</label>
											<input type="text" class="form-control autofocus" placeholder="Variety Name" maxlength="25"   name="name"  data-ng-model="AddedVarietyMaster.variety"   ng-required='true'  tabindex="1"  data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" ng-blur="spacebtw('variety');validateDup();"  id="varietyName" with-floating-label > 
			                        	</div>
										<p ng-show="VarietyMaster.name.$error.required && (VarietyMaster.name.$dirty || Addsubmitted)" class="help-block"> Variety Name is required</p>
										<p ng-show="VarietyMaster.name.$error.pattern  && (VarietyMaster.name.$dirty || Addsubmitted)" class="help-block"> Valid Variety Name is required</p>
										<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedVarietyMaster.variety!=null">Variety Name Already Exist.</p>
										
										
										</div>
			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" >
			                        	<div class="fg-line nulvalidate">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedVarietyMaster.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"   data-ng-model='AddedVarietyMaster.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
									
								
			                        	</div>
										
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							   
							    
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedVarietyMaster.modifyFlag"  />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(VarietyMaster)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
							
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Varieties</b></h2></div>
					<div class="card">						
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 	
								<form name="varietymasteredit" novalidate>					
							        <table ng-table="varietyMaster.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Variety Code</span></th>
																<th><span>Variety Name</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
														<tbody>
								        <tr ng-repeat="variety in varietyData"  ng-class="{ 'active': variety.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!variety.$edit" ng-click="variety.$edit = true;variety.modifyFlag='Yes';variety.screenName='Variety Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="variety.$edit" ng-click="updateVarietyMaster(variety,$index);variety.$edit = isEdit"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="variety.modifyFlag"  />
											   <input type="hidden" name="screenName" ng-model="variety.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!variety.$edit">{{variety.varietyCode}}</span>
					    		                <div ng-if="variety.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="variety.varietyCode" placeholder='Variety Code' maxlength="10" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!variety.$edit">{{variety.variety}}</span>
												 <div class="form-group" ng-class="{ 'has-error' : varietymasteredit.varietynameedit{{index}}.$invalid && (varietymasteredit.varietynameedit{{index}}.$dirty || submitted)}">
							                     <div ng-if="variety.$edit">
												 	<input class="form-control" type="text" ng-model="variety.variety" placeholder='Variety Name' maxlength="25" name="varietynameedit{{index}}" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" ng-required='true' ng-blur="spacebtwgrid('variety',$index);validateDuplicate(variety.variety,$index);"/>
													<p ng-show="varietymasteredit.varietynameedit{{index}}.$error.required && (varietymasteredit.varietynameedit{{index}}.$dirty || submitted)" class="help-block"> Variety Name is required.</p>
													<p ng-show="varietymasteredit.varietynameedit{{index}}.$error.pattern && (varietymasteredit.varietynameedit{{index}}.$dirty || submitted)" class="help-block"> Valid Variety Name is required.</p>
													<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="variety.variety!=null">Variety Name Already Exist.</p>
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!variety.$edit">{{variety.description}}</span>
        		            					  <div ng-if="variety.$edit">
												  	<div class="form-group">
													  	<input class="form-control" type="text" ng-model="variety.description" placeholder='Description' maxlength="50" name="description" ng-blur="spacebtwgrid('description',$index)"/>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!variety.$edit">
												  	<span ng-if="variety.status=='0'">Active</span>
													<span ng-if="variety.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="variety.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  ng-model="variety.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="variety.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
							             </tr>
										 </tbody>
										 
								         </table>
										 
										 </form>
										 </div>
										 </section>							 
							     </div>
							</div>
						</div>										
					</div>
					
				
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
