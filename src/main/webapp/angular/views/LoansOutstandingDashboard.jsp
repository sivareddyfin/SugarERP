<style type="text/css">

/*.tableCss tr:nth-child(even) {background: #CCC}
.tableCss tr:nth-child(odd) {background: #FFF}*/
.tableCss tr:nth-child(2n+0) {background:black;color:#FFFFFF; opacity:0.5;}
.tableCss tr:nth-child(odd) {background: #FFFFFF; color:#000000;}
.tableCss tr:nth-child(1) {background: #2298f2;color:#FFFFFF;}


.tableCss
{
 font-weight:normal;
 font-family:"Helvetica";
 height:50%;
 float:right;	
}
</style>
<script>
//$(document).ready(function(){

var renderTable = function (chart, containerId) {
		
            // After the chart is rendered we export the data as CSV, parse it and then create a markup
            // equivalent to a table by parsing the exported CSV.

            var data = chart.getDataAsCSV(),
                rows,
                row,
                i,
               length,
                tableBody = '',
				tableBody1 = '',
                tableHeader = '';
				tableHeader1 = '';
     
            // Get all the rows by splitting data with '\n' seperator
            rows = data.replace(/"/g, '').split('\n');
     
            // Retrieve the data from the rows and compute body string from the data rows
            for (i = 1, length = rows.length; i < length; i++) {
                //row = rows[0].split(',');
				
				//tableHeader = '<th>' + row[0] + '</th><th>' + row[1] + '</th>'
               //tableBody += '<td>' + row[0] + '</td>' ;
				//tableBody1 += '<td>' + row[1] + '</td>' ;
				row = rows[i].split(',');
				//alert(tableBody1);
				//alert(tableHeader);
				//tableBody += '<td>' + row[0] + '</td><td>'+ row[1] +'</td>' ;
				tableBody += '<tr><td>' + row[0] + '</td><td>'+ row[1] +'</td></tr>' ;
            }
     			
				//alert(tableBody1);
           // Compute header string from first row
            row = rows[0].split(',');
			
            tableHeader = '<th>' + row[0] + '</th><th>'+ row[1] +'</th>' ;
			
			//tableBody1 += '<tr>'+ tableBody +'</tr>';
			
			//tableHeader1 = '<tr>' + tableBody + '</tr>' ;
     
            // Create the table string and append it to the table container
          // document.getElementById(containerId).innerHTML = '<table width ="100%;" border="1" style="border-collapse:collapse;"><tbody><tr>'+tableHeader+tableBody+'</tr><tr>'+tableHeader1+tableBody1+'</tr></tbody</table>';
		   document.getElementById(containerId).innerHTML = '<table width ="100%;" border="1" style="border-collapse:collapse;" class="tableCss"><tbody><tr>'+tableHeader+tableBody+'</tr></tbody</table>';
     
       };
	   
	   var ajaxLink = "/SugarERP/bankDetailsChart.html";
	   loadChart(ajaxLink);
	 function loadChart(ajaxLink)
	 
	 {
	   $.getJSON(ajaxLink, function(json) 
		{
			
			 FusionCharts.ready(function () 
			 {
           
		   var caption = "Current Outstanding Loans";
			var chartData = {
						"chart": {
							"caption": caption,
							"showborder": "0",
							"exportenabled": "1",
							"exportatclient": "0",
							"exporthandler": "http://export.api3.fusioncharts.com",
							"html5exporthandler": "http://export.api3.fusioncharts.com",								
							"xAxisNameFont": "Helvetica",
							"xAxisNameFontSize": "12",
							"xAxisNameFontBold":"1",
							"yAxisNameFontBold":"1",
							"yAxisNameFontSize": "12",
							"yAxisNameFont": "Helvetica",
							"yFormatNumberScale":"1",
							"numberScaleValue": "1000,1000,1000",
							"numberScaleUnit": "K,M,B",
							"bgColor": "#ffffff",
							"decimals":'1',
							"baseFont":"Helvetica",		
							"baseFontSize":"12",
							"captionFontSize":"18",
							"showlabels": "1",
							"showlegend": "1",
							"legendBgColor": "#ffffff",
							"legendBorderColor":"#000000",
							"legendBorder":"1",
							"legendBorderAlpha": "1",
							"legendShadow": "1",
							"legendValue":"1",
							"legendPosition":"bottom",
							"showCanvasBorder": "0",
							"enablemultislicing": "0",
							"slicingdistance": "20",
							"showpercentvalues": "1",
							"showpercentintooltip": "1",
							"showvalues":"1",
							"plottooltext": "$label  : $datavalue $percentvalue "
						},
					"data":  json['data']
					};
					
 		 var revenueChart = new FusionCharts({
		
        type: 'pie2d',
	    renderAt: 'container',
        width: '100%',
        height: '400',
	        dataFormat: 'json',
	        dataSource: chartData,
			events: {
					dataPlotClick: function (event,dataObj) 
					{	
						//alert(JSON.stringify(dataObj));
								
						//sessionStorage.setItem("count","0");
//						sessionStorage.setItem("firstData0",JSON.stringify(json['data']));
//						sessionStorage.setItem("firstTableData0",JSON.stringify(json['tableData']));
//						var ajaxDrillVal = sessionStorage.getItem("ajaxLinkDrill");			
//						var cstatus = sessionStorage.getItem("cStatus");
						sessionStorage.setItem("code",json['data'][dataObj.dataIndex].value);
					    sessionStorage.setItem("category",dataObj.categoryLabel);	
//						sessionStorage.setItem("year",json['data'][dataObj.dataIndex].year);																
						drillDown(json['data'][dataObj.dataIndex].value,dataObj.categoryLabel);
						//alert(json['data'][dataObj.dataIndex].value);
						//alert(json['data'][dataObj.dataIndex].foid);
//						sessionStorage.setItem("dillStatus1",'1');
					 }
				 }

	    });

		 revenueChart.addEventListener('renderComplete', function (e, a) {
            // In the renderComplete event create the table, even when the data is updated by calling setChartData method, this event will be fired and the updated data will reflect in the table too, since renderTable will be called again.
//            // Creating the table on 'render' event will not update the table everytime, it will only update when render() method is called.
            renderTable(revenueChart, 'table-container');
			});
		revenueChart.render();
//		

// 
// 
// 
 })
 
		})
}
		
		function drillDown(value,category)
		
		{
	
		//var bankid = bankid.toString(); 
		//alert(category);
			 $.getJSON("/SugarERP/branchDetailsChart.html?bankid="+value, function(json) 
			{
			//alert(JSON.stringify(json));
			 FusionCharts.ready(function () 
			 {
			 	var caption2 = category;
				var chartData2 = {
									"chart": {
										"caption": caption2,
										"showborder": "0",
										"exportenabled": "1",
										"exportatclient": "0",
										"exporthandler": "http://export.api3.fusioncharts.com",
										"html5exporthandler": "http://export.api3.fusioncharts.com",								
										"xAxisNameFont": "Helvetica",
										"xAxisNameFontSize": "12",
										"xAxisNameFontBold":"1",
										"yAxisNameFontBold":"1",
										"yAxisNameFontSize": "12",
										"yAxisNameFont": "Helvetica",
										"yFormatNumberScale":"1",
										"numberScaleValue": "1000,1000,1000",
										"numberScaleUnit": "K,M,B",
										"bgColor": "#ffffff",
										"decimals":'1',
										"baseFont":"Helvetica",		
										"baseFontSize":"12",
										"captionFontSize":"18",
										"showlabels": "1",
										"showlegend": "1",
										"legendBgColor": "#ffffff",
										"legendBorderColor":"#000000",
										"legendBorder":"1",
										"legendBorderAlpha": "1",
										"legendShadow": "1",
										"legendValue":"1",
										"legendPosition":"bottom",
										"showCanvasBorder": "0",
										"enablemultislicing": "0",
										"slicingdistance": "20",
										"showpercentvalues": "1",
										"showpercentintooltip": "1",
										"showvalues":"1",
										"plottooltext": "$label  : $datavalue $percentvalue "
									},
								"data": json['data']
							};
		 var revenueChart = new FusionCharts({
		
        type: 'pie2d',
	    renderAt: 'container',
        width: '100%',
        height: '400',
	        dataFormat: 'json',
	        dataSource: chartData2,
			events: {
					dataPlotClick: function (event,dataObj) 
					{	
						
					 }
				 }

	    });
		 revenueChart.addEventListener('renderComplete', function (e, a) {
            // In the renderComplete event create the table, even when the data is updated by calling setChartData method, this event will be fired and the updated data will reflect in the table too, since renderTable will be called again.
//            // Creating the table on 'render' event will not update the table everytime, it will only update when render() method is called.
            renderTable(revenueChart, 'table-container');
			});
		revenueChart.render();
		
			$("#goback1").show();
			 
			 })
		})
			//alert(fieldofficerId);
		}
		
		$(document).ready(function()
		{
			$(".goback").click(function()
			{
			loadChart(ajaxLink);
					$(".goback").hide();
			//	FusionCharts.ready(function () 
//			 {
//           
//				   var caption = "Current Outstanding Loans";
//					var chartData = {
//								"chart": {
//									"caption": caption,
//									"showborder": "0",
//									"exportenabled": "1",
//									"exportatclient": "0",
//									"exporthandler": "http://export.api3.fusioncharts.com",
//									"html5exporthandler": "http://export.api3.fusioncharts.com",								
//									"xAxisNameFont": "Helvetica",
//									"xAxisNameFontSize": "12",
//									"xAxisNameFontBold":"1",
//									"yAxisNameFontBold":"1",
//									"yAxisNameFontSize": "12",
//									"yAxisNameFont": "Helvetica",
//									"yFormatNumberScale":"1",
//									"numberScaleValue": "1000,1000,1000",
//									"numberScaleUnit": "K,M,B",
//									"bgColor": "#ffffff",
//									"decimals":'1',
//									"baseFont":"Helvetica",		
//									"baseFontSize":"12",
//									"captionFontSize":"18",
//									"showlabels": "1",
//									"showlegend": "1",
//									"legendBgColor": "#ffffff",
//									"legendBorderColor":"#000000",
//									"legendBorder":"1",
//									"legendBorderAlpha": "1",
//									"legendShadow": "1",
//									"legendValue":"1",
//									"legendPosition":"bottom",
//									"showCanvasBorder": "0",
//									"enablemultislicing": "0",
//									"slicingdistance": "20",
//									"showpercentvalues": "1",
//									"showpercentintooltip": "1",
//									"showvalues":"1",
//									"plottooltext": "$label  : $datavalue $percentvalue "
//								},
//							"data":  json['data']
//							};
//						alert(JSON.stringify(json['data']));	
//				 var revenueChart = new FusionCharts({
//				
//				type: 'pie2d',
//				renderAt: 'container',
//				width: '100%',
//				height: '400',
//					dataFormat: 'json',
//					dataSource: chartData,
//					events: {
//							dataPlotClick: function (event,dataObj) 
//							{	
//								//alert(JSON.stringify(dataObj));
//										
//								//sessionStorage.setItem("count","0");
//		//						sessionStorage.setItem("firstData0",JSON.stringify(json['data']));
//		//						sessionStorage.setItem("firstTableData0",JSON.stringify(json['tableData']));
//		//						var ajaxDrillVal = sessionStorage.getItem("ajaxLinkDrill");			
//		//						var cstatus = sessionStorage.getItem("cStatus");
//								sessionStorage.setItem("code",json['data'][dataObj.dataIndex].bankid);
//		//					sessionStorage.setItem("category",dataObj.categoryLabel);	
//		//						sessionStorage.setItem("year",json['data'][dataObj.dataIndex].year);																
//								drillDown(json['data'][dataObj.dataIndex].bankid);
//								//alert(json['data'][dataObj.dataIndex].foid);
//		//						sessionStorage.setItem("dillStatus1",'1');
//							 }
//						 }
//		
//				});
//		
//				 revenueChart.addEventListener('renderComplete', function (e, a) {
//					// In the renderComplete event create the table, even when the data is updated by calling setChartData method, this event will be fired and the updated data will reflect in the table too, since renderTable will be called again.
//		//            // Creating the table on 'render' event will not update the table everytime, it will only update when render() method is called.
//					renderTable(revenueChart, 'table-container');
//					});
//				revenueChart.render();
		//		
		
		// 
		// 
		// 
		 //})
					
			});
			
		});

					
</script>

 </head>
 <body>
 <br>
 
 
<div class="row">
  	<div class="col-sm-9">
	<div style="float: right; margin-right:20px;"><p><input type="button" id="goback1" class="goback" value="< Go Back" style=" border:none; display:none;"/></p></div>
    	 <div id='container'></div>
	</div>
	<div class="col-sm-3">
		 <div id="table-container"  id='exportpdf'>The table corresponding to the chart will render here </div>
	</div>
</div>
 


