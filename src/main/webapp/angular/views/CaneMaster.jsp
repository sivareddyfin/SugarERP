	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneManagerMaster" ng-init='loadCaneManagerNames();loadAllCaneManagers();loadCaneManagerId()'>     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Manager Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
    	        			  
							
							 	<form name="CaneMaster" ng-submit="canesubmit(AddedCane,CaneMaster)" novalidate>
								<input type="hidden" name="screenName" ng-model="AddedCane.screenName" />
							 <div class="row">
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
							<div class="input-group">								       									
            			            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
							<div class="fg-line">
								<label for="caneManagerid">Cane Manager ID</label>
                    			 <input type="text" class="form-control" placeholder="Cane Manager ID"   maxlength="5" name='caneManagerId'  data-ng-model="AddedCane.caneManagerId" id="caneManagerid" with-floating-label readonly/>
							</div>
        		            
		              								
			                    </div>			                    
			                  </div>
							</div>
							<div class="row">
							
						
							    <div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
									<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneMaster.description.$invalid && (CaneMaster.description.$dirty || submitted)}">
									<br />
			                        	<div class="fg-line">
										<label for="description">Description</label>	
											<input type="text" class="form-control" placeholder="Description" maxlength="50" autofocus  name="description"  data-ng-model="AddedCane.description" tabindex="1" ng-blur="spacebtw('description')" id="description" with-floating-label  >
			                        	</div>
										<p ng-show="CaneMaster.description.$error.required && (CaneMaster.description.$dirty || submitted)" class="help-block"> Description is required.</p>
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							
							
							
</div>
							</div>
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
								 <div class="input-group">	
		                 <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
        		        <div class="form-group" ng-class="{ 'has-error' : CaneMaster.canename.$invalid  && (CaneMaster.canename.$dirty || submitted)}">
							<div class="fg-line">
							<div class="select">
								<select  chosen class="w-100" name="canename"    data-ng-required="true" data-ng-model="AddedCane.employeeId" ng-options="employeeId.id as employeeId.employeename for employeeId in mgrNames  | orderBy:'-employeename':true">
								<option value=''>Select Cane Manager Name</option>
								</select>
								</div>
							</div>
        		            <p ng-show="CaneMaster.canename.$error.required  && (CaneMaster.canename.$dirty || submitted)" class="help-block">Select Cane Manager Name.</p>
		                </div>
				</div>
										                    
			                  </div>
							</div>
							<div class="row">
							
						
							   
							    <div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><br /><i class="zmdi zmdi-account"></i></span>
			                        <br />
            				          <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="0" checked="checked"  data-ng-model="AddedCane.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="1"  data-ng-model="AddedCane.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                
			                    </div>			                    
			                </div>
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedCane.modifyFlag"  />
							 <br />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(CaneMaster)">Reset</button>
									
										
									</div>
								</div>						 	
							 </div>	
							 </form>	
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!----------table grid----------------->
					<div class="block-header" style="margin-bottom:0px; positiontatic; margin-top:-30px; padding:8px;"><h2><b>Added Cane Managers</b></h2></div>
						<div class="card">	
        					<div class="card-body card-padding">	
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">	
										<form name="CaneMasterEdit">

        <table   ng-table="CaneManagerMaster.tableEdit"  class="table table-striped table-vmiddle" >
			
												<thead>
													<tr>
														<th><span>ACTIONS</span></th>
														<th><span>CANE MANAGER ID</span></th>
														<th><span>CANE MANAGER</span></th>
														<th><span>DESCRIPTION</span></th>
														<th><span>STATUS</span></th>
														</tr>
												</thead>
											<tbody>

        <tr ng-repeat="Cane in CaneData"  ng-class="{ 'active': Cane.$edit }">
                	     	 <td data-title="'Actions'">
    	                <button type="button" class="btn btn-default" ng-if="!Cane.$edit" ng-click="Cane.$edit = true;Cane.modifyFlag='Yes';Cane.screenName='CANE MANAGER MASTER';"><i class="zmdi zmdi-edit"></i></button>
            	        <button type="submit" class="btn btn-success" ng-if="Cane.$edit" data-ng-click='updateCaneMaster(Cane,$index);'><i class="zmdi zmdi-check"></i></button>
						<input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Cane.modifyFlag"  />
						<input type="hidden" name="screenName{{$index}}" ng-model="Cane.screenName" />
                    	  </td>
                <td>
                	     	 <span ng-if="!Cane.$edit">{{Cane.caneManagerId}}</span>
    	                 <div ng-if="Cane.$edit">
						 	<div class="form-group">
								<input class="form-control" type="text" data-ng-model="Cane.caneManagerId" maxlength="25" placeholder='Cane Manager ID' readonly/>
							</div>
						</div>
            	       </td>
                    	   <td>
                     <span ng-if="!Cane.$edit">{{Cane.caneManagerName}}</span>
                     <div ng-if="Cane.$edit">
					 	<div class="form-group">
							<select class='form-control1' ng-model="Cane.employeeId"  ng-options="employeeId.id as employeeId.employeename for employeeId in mgrNames  | orderBy:'-employeename':true">	</select>
						</div>
					</div>
                  </td>
                  <td data-title="'Description'">
                      <span ng-if="!Cane.$edit">{{Cane.description}}</span>
					  <div ng-if="Cane.$edit">
					  	<div class="form-group">
						<input class="form-control" type="text" ng-model="Cane.description" maxlength="50" placeholder='Description' ng-blur="spacebtwgrid('description',$index)"/>
						  
						</div>
				      </div>	             	       
  						</td>
 						 <td>
                      <span ng-if="!Cane.$edit">
					  	<span ng-if="Cane.status=='0'">Active</span>
						<span ng-if="Cane.status=='1'">Inactive</span>
					  </span>
  <div ng-if="Cane.$edit">
  	 <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions{{$index}}" value="0" checked="checked"  data-ng-model="Cane.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions{{$index}}" value="1"  data-ng-model="Cane.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
   </div>	             	       
  </td>
                 	     	               	   	
             </tr>
         </table>
		 </tbody>
 </form>
 										</div>
									 </section>
    							 </div>
								</div>
							</div>	
<!----------table grid end------------->

				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



