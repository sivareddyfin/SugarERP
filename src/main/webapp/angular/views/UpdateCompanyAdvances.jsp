	<style type="text/css">

		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
	.input-group.input-group-unstyled input.form-control1 
	{
	    -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
	}
	.input-group-unstyled .input-group-addon 
	{
    	border-radius: 4px;
	    border: 0px;
    	background-color: transparent;
	}		
	</style>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Company Advances</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="row">
				                <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_updzone">
												<option value="0">Select Zone</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_updcircle">
												<option value="0">Select Circle</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-2">
								  	<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/UpdateAdvances" class="btn btn-primary btn-sm m-t-10">Get Details</a>
									</div>
								</div>
								  </div>
    	        			 </div>
							 <hr />	
							 <div  class="table-responsive">
							 <table class="table table-striped">
							 		<thead>
								        <tr>
                		    				<th>Ryot Code</th>
							                <th>Name</th>
		                    				<th>Village</th>
											<th>Ratoon Ext</th>
											<th>Plant Ext</th>
											<th>Amount</th>
            							</tr>											
									</thead>
									<tbody>
										<tr>
											<td>123</td>  
											<td>Axisbank</td>  
											<td>02-01-2016</td>
											<td>1000</td>
											<td>123</td>
											<td style="width:200px;">
												<div class="input-group input-group-unstyled">
                   									<input type="text" class="form-control1" size="5" placeholder='Amount'/>
								                    <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign"></i>
								                    </span>
								                </div>
											</td>
										</tr>
										<tr>
											<td>123</td>  
											<td>Axisbank</td>  
											<td>02-01-2016</td>
											<td>1000</td>
											<td>123</td>
											<td style="width:200px;">
												<div class="input-group input-group-unstyled">
                   									<input type="text" class="form-control1" size="5" placeholder='Amount'/>
								                    <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign"></i>
								                    </span>
								                </div>
											</td>
										</tr>
										<tr>
											<td>123</td>  
											<td>Axisbank</td>  
											<td>02-01-2016</td>
											<td>1000</td>
											<td>123</td>
											<td style="width:200px;">
												<div class="input-group input-group-unstyled">
                   									<input type="text" class="form-control1" size="5" placeholder='Amount'/>
								                    <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign"></i>
								                    </span>
								                </div>
											</td>
										</tr>
										<tr>
											<td>123</td>  
											<td>Axisbank</td>  
											<td>02-01-2016</td>
											<td>1000</td>
											<td>123</td>
											<td style="width:200px;">
												<div class="input-group input-group-unstyled">
                   									<input type="text" class="form-control1" size="5" placeholder='Amount'/>
								                    <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign"></i>
								                    </span>
								                </div>												
											</td>
										</tr>
									</tbody>		
							 </table>
						   </div>
							 	 						           			
				        </div>
			    	</div>
					
													
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
