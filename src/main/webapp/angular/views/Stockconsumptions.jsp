


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		<section id="content" data-ng-controller="StockconsumptionsController" ng-init="addFormField(); loadStatus();loadDepartments();">
		  <div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Consumptions</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StockconsumptionsForm" novalidate ng-submit="StockconsumptionsSubmit(AddedStockconsumptions,StockconsumptionsForm)" >
                            <input type="hidden" ng-model="Addedproductgroup.modifyFlag" name="modifyFlag"  />
						 <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{'has-error':StockconsumptionsForm.fromDate.$invalid && (StockconsumptionsForm.fromDate.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date" autofocus tabindex="1" name="fromDate " ng-required='true' data-ng-model="AddedStockconsumptions.fromDate" placeholder="From Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}" data-ng-required="true" id="fromDate" with-floating-label/>
									</div>
								<p ng-show="StockconsumptionsForm.fromDate.$error.required && (StockconsumptionsForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{'has-error':StockconsumptionsForm.to.$invalid && (StockconsumptionsForm.to.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<label for="to">To Date</label>
										<input type="text" class="form-control date" tabindex="2" name="to " ng-required='true' data-ng-model="AddedStockconsumptions.to" placeholder="To Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}" data-ng-required="true" id="to" with-floating-label/>
									</div>
							<p ng-show="StockconsumptionsForm.to.$error.required && (StockconsumptionsForm.to.$dirty || Addsubmitted)" class="help-block">To Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group " ng-class="{'has-error':StockconsumptionsForm.department.$invalid && (StockconsumptionsForm.department.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									  <select chosen class="w-100 form-control " tabindex="3"   data-ng-model='AddedStockconsumptions.department' name="department"  placeholder="select Department"  ng-options="department.department as department.department for department in departments"  >
														
						        			            </select>
									 					
									</div>
								<p ng-show="StockconsumptionsForm.department.$error.required && (StockconsumptionsForm.department.$dirty || Addsubmitted)" class="help-block">Department is required</p>		
								<p ng-show="StockconsumptionsForm.department.$error.pattern && (StockconsumptionsForm.department.$dirty || Addsubmitted)" class="help-block"> Valid Department is required</p>	 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							</div>
							
							 
							 
							 
							<br/>
							<div class="table-responsive">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
							
								<tr class="styletr" style="font-weight:bold;">
									<th>Options</th>
									<th>StockConsumptionNo</th>
									<th>Date</th>
									<th>Department</th>
									
								</tr>
								<tr class="styletr"  ng-repeat="Stockconsumptions in StockconsumptionsData" ng-class="{ 'active': AddedStockconsumptions.$edit }" >
								   
									<td>
														   <button type="button" class="btn btn-default" ng-if="!AddedStockconsumptions.$edit" ng-click="AddedStockconsumptions.$edit = true; AddedStockconsumptions.modifyFlag='Yes';"><i class="zmdi zmdi-edit"></i></button>
														   <button type="submit" class="btn btn-success btn-hideg" ng-if="AddedStockconsumptions.$edit" ng-click="UpdateStockconsumptions(AddedStockconsumptions,$index);AddedStockconsumptions.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
														   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="AddedStockconsumptions.modifyFlag"  />	
														   
														 </td>
										
									<td >
										<div class="form-group" ng-class="{'has-error':StockconsumptionsForm.stockConsumptionNo{{$index}}.$invalid && (StockconsumptionsForm.stockConsumptionNo{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="stock Consumption No" maxlength="10" name="stockConsumptionNo{{$index}}" ng-model="Stockconsumptions.stockConsumptionNo" ng-required="true"  readonly="readonly">							
										<p ng-show="StockconsumptionsForm.stockConsumptionNo{{$index}}.$error.required && (StockconsumptionsForm.stockConsumptionNo{{$index}}.$dirty || submitted)" class="help-block"> Stock Consumption No Required</p>
										
										</div>
									</td>	
									
										
									<td >
										<div class="form-group" ng-class="{'has-error':StockconsumptionsForm.date{{$index}}.$invalid && (StockconsumptionsForm.date{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Date" maxlength="10" name="date{{$index}}" ng-model="Stockconsumptions.date" ng-required="true"  readonly="readonly">							
										<p ng-show="StockconsumptionsForm.date{{$index}}.$error.required && (StockconsumptionsForm.date{{$index}}.$dirty || submitted)" class="help-block"> Date Required</p>
										
										</div>
									</td>	
									
										
									<td >
										<div class="form-group" ng-class="{'has-error':StockconsumptionsForm.department{{$index}}.$invalid && (StockconsumptionsForm.department{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Department" maxlength="20" name="department{{$index}}" ng-model="Stockconsumptions.department" ng-required="true"  readonly="readonly">							
										<p ng-show="StockconsumptionsForm.department{{$index}}.$error.required && (StockconsumptionsForm.department{{$index}}.$dirty || submitted)" class="help-block"> Department Required</p>
										
										</div>
									</td>	
									
										
								
										
							</tr></table>
						</div>
						
						
						<br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(StockconsumptionsForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	
						
						 
							 
						</form>
						
						
						<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>	 	
								
							
		