	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
$(document).ready(function(){

$( ".date" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
    $("#fromDate").datepicker({
        numberOfMonths: 1,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
        onSelect: function(selected) {
          $("#toDate").datepicker("option","minDate", selected)
		  
        }
    });

    $("#toDate").datepicker({
        numberOfMonths: 1,
		changeMonth: true,
		 changeYear: true,
		dateFormat: 'dd-mm-yy',
        onSelect: function(selected) {
           $("#fromDate").datepicker("option","maxDate", selected)
        }
    }); 
});
</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="printDispatchController" ng-init="loadMode();loadseasonNames();">       
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>REPRINT DISPATCH</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
    	        			  
							
							 	<form name="printDispatch" method="post">
								
							 <div class="row">
							<div class="col-sm-12">
							<div class="row">
							
							<div class="col-sm-3">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : printDispatch.season.$invalid && (printDispatch.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedDispatchForm.season'  name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="printDispatch.season.$error.required && (printDispatch.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
											</div>
							
							  
							  
							  	<div class="col-sm-2">
								
											  	<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : printDispatch.dateFrom.$invalid && (printDispatch.dateFrom.$dirty || filterSubmitted)}">
													  	<div class="fg-line">
														
											<input type="text" placeholder='From Date'  class="form-control date" name="dateFrom" ng-model="AddedDispatchForm.dateFrom"  id="fromDate"/>
														</div>
														<p ng-show="printDispatch.dateFrom.$error.required && (printDispatch.dateFrom.$dirty || submitted)" class="help-block">From Date is Required</p>
													</div>
												</div>
											  </div>
											 									
								<div class="col-sm-2">
											   	  <div class="input-group">
												  	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>	
													<div class="form-group" ng-class="{ 'has-error' : printDispatch.dateTo.$invalid && (printDispatch.dateTo.$dirty || filterSubmitted)}">
													   	<div class="fg-line">
														
															<input type="text" placeholder='To Date' id="toDate" class="form-control date" name="dateTo" ng-model="AddedDispatchForm.dateTo" ng-required="true"  />	
														</div>
														<p ng-show="printDispatch.dateTo.$error.required && (printDispatch.dateTo.$dirty || submitted)" class="help-block">To Date is Required</p>
													</div>
												  </div>
											   </div>
							 
							<div class="col-sm-2"><button type="button" class="btn btn-primary" ng-click='getDispatchDetails(AddedDispatchForm.season,AddedDispatchForm.dateFrom,AddedDispatchForm.dateTo);'>Get Details</button>
							</div> 
							  
							</div>
							
							</div>
							
							
							 </div>
							 
							 
							<!--<div class="row">
							
						<div class="col-sm-3">
							<div class="input-group">								       									
            			            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
							<div class="fg-line">
								<label for="caneManagerid">Dispatch Number</label>
                    			 <input type="text" class="form-control" placeholder="Dispatch Number"    name='DispatchNo'  ng-model="AddedDispatchForm.DispatchNo" data-ng-required="true" ng-blur="printDispatchForm(AddedDispatchForm.DispatchNo)" id="caneManagerid" with-floating-label />
							</div>
        		            
		              								
			                    </div>			                    
			                  </div>
							<div class="col-sm-3"><button type="button" class="btn btn-primary" ng-click='generatePrintDispatch(AddedDispatchForm.DispatchNo);'>Generate Print</button>
							</div>
							</div>-->
							
							<br />
							 	<div class="table-responsive" id="showTable" style="display:none;">
								
										<section class="asdok">
										<div class="container1">
								<table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
														
															<th><span>Update Delivery </span></th>
																<th><span>SNO</span></th>
															<th><span>Dispatch No.</span></th>
															<th><span>Ryot Code</span></th>
															<th><span>Ryot Name</span></th>
														
															
															
															
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
													
															<tr ng-repeat="dispatchDetails in dispatchData">
															
											<td style="display:none;">
												
												<input type="hidden" name="smsStatus" ng-model="dispatchDetails.smsStatus" />
												<input type="hidden" name="acknowledgeStatus" ng-model="dispatchDetails.acknowledgeStatus" />
											</td>
										<td align="right">
													<!--<button type="button" class="btn btn-danger" ng-if="dispatchDetails.smsStatus!='Yes'" ng-click="dispatchSMS(dispatchDetails.dispatchNo);">SMS</button>
													<button type="button" class="btn btn-success" ng-if="dispatchDetails.smsStatus=='Yes'" ng-click="dispatchSMS(dispatchDetails.dispatchNo);" >SMS</button>-->
													
													
													 
					            		       <button type="submit" class="btn btn-success btn-hideg" id="smsButton{{$index}}" ng-if="dispatchDetails.smsStatus=='Yes'"ng-click="dispatchSMS(dispatchDetails.dispatchNo,$index);"><i class="zmdi zmdi-check"></i></button>
								              											
											</td>
											<td>
												{{dispatchDetails.id}}
												<input type="hidden" name="sno" ng-model="dispatchDetails.id" />
											</td>
											
							    	    	<td>
												{{dispatchDetails.dispatchNo}}
												<input type="hidden" name="dispatchNo" ng-model="dispatchDetails.dispatchNo" />
											</td>
											<td>
												{{dispatchDetails.ryotCode}}
												<input type="hidden" name="ryotCode" ng-model="dispatchDetails.ryotCode" />
											</td>
											<td>
												{{dispatchDetails.ryotName}}
												<input type="hidden" name="ryotName" ng-model="dispatchDetails.ryotName" />
											</td>
											
											<td>
											<button type="button" class="btn btn-primary" ng-click='generatePrintDispatch(dispatchDetails.dispatchNo,dispatchDetails.agrType);'>Print</button>
											</td>
											<td>
												<input type="hidden" name="agrType" ng-model="dispatchDetails.agrType" />
											</td>
											
											<td>
												
												
											<a  href="#/seedling/transaction/Acknowledgement"  ng-click="dispatchAck(AddedDispatchForm.season,dispatchDetails.dispatchNo);" ng-hide="dispatchDetails.acknowledgeStatus!='Yes'"    >Acknowledgement</a>
									
										
										
										<!--<button type="button" class="btn btn-primary"  ng-hide="dispatchDetails.acknowledgeStatus!='Yes'">Print</button>-->
											</td>
												
																						
														</tr>											
													</tbody>
												</table>
												</div>
									</section>
								</div>
								
								
							 </form>
							 
							 
							 	
							 
												
												
													
							<form name='dispatchPrintForm'>
						  		<div id="authprint" style="display:none;">
									<table style='width:100%;position:absolute;top:11.3%;' border='0' >
									
					<tr>
					<td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;">{{dispatchPrnt.RyotName}}</div><!--<input type='hidden' name='RyotName' ng-model='dispatchPrnt.RyotName'>{{dispatchPrnt.RyotName}}</div>--></td>
				<td style="float:right;" colspan='2'><div style="font-size:18px;"><input type='hidden' name='dispatchNo' ng-model='dispatchPrnt.dispatchNo'><span style="margin-right:160px;">{{dispatchPrnt.dispatchNo}}</span></div></td></tr>
<tr><td  style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='RyotCode' ng-model='dispatchPrnt.RyotCode'>{{dispatchPrnt.RyotCode}}</div></td>
<td style="float:right;" colspan='2'><div style="text-align:right;font-size:18px;"><span style="margin-right:160px;">{{dispatchPrnt.DispatchDate}}</span></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Village' ng-model='dispatchPrnt.Village'>
{{dispatchPrnt.Village}}</div></td>
<td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:18px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Circle' ng-model='dispatchPrnt.Circle'>{{dispatchPrnt.Circle}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:14px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:100px;font-size:18px;"><input type='hidden' name='Zone' ng-model='dispatchPrnt.Zone'>{{dispatchPrnt.Zone}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:18px;"></div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:214px;font-size:14px;"><br /><input type='hidden' name='dispatchNo' ng-model='dispatchPrnt.dispatchNo'>{{dispatchPrnt.IndentNo}}</div></td><td style="float:left;" colspan='2'><div style="margin-left:50px;text-align:left;font-size:14px;"><br />{{dispatchPrnt.IndentDate}}</div></td></tr>
<tr><td style="float:left;" colspan='2'><div style="margin-left:120px;font-size:14px;"><input type='hidden' name='Vehical' ng-model='dispatchPrnt.Vehical'>{{dispatchPrnt.Vehical}}</div></td><td style="float:right;" colspan='2'><div style="margin-right:80px;text-align:right;font-size:14px;"></div></td></tr>

									</table>
						
									<div>
									
												
									
					
									<div style='position:absolute;top:42%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr ng-repeat="printGrid in gridDetails">
										<td ><div style="margin-left:30px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.varietyName}}</div></td>
<td ><div style="margin-left:150px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofTrays}}</div></td>
<td ><div style="margin-left:200px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofSeedlings}}</div></td>
<td ><div style="margin-left:165px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.TrayType}}</div></td>

</tr>	
									</table>			

					
										<table border='0' style='width:100%;font-size:5px;' >
										<tr><td align='right'><p  style='margin-right:60px;height:4px;'><br></p></td></tr>
										<tr><td align='right'><p style='margin-right:60px;height:4px;'></p></td></tr>
										</table>
									<br>
									</div>
												

					
									<div style='position:absolute;top:85%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr ng-repeat="printGrid in gridDetails">
										<td ><div style="margin-left:30px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.varietyName}}</div></td><td ><div style="margin-left:150px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter; ">{{printGrid.NoofTrays}}</div></td>
<td ><div style="margin-left:200px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.NoofSeedlings}}</div></td>
<td ><div style="margin-left:130px;font-size:14px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{printGrid.TrayType}}</div></td>
</tr>	
									</table>																				
									
								</div>
								
	</div>
				<!--below grid-->
							

									<div style='position:absolute;top:98%'> 
										<table border="0"  style="font-family:Georgia, Helvetica, sans-serif;">
										<tr>
										<td ><div style="margin-left:30px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;height:30px;">{{dispatchPrnt.dispatchNo}}</div></td>
<td ><div style="margin-left:150px;font-size:13px;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;height:30px;">{{dispatchPrnt.DispatchDate}}</div></td>
</tr>	
									</table>			

					
										<table border='0' style='width:100%;font-size:5px;' >
										<tr><td align='right'><p  style='margin-right:60px;height:4px;'><br></p></td></tr>
										<tr><td align='right'><p style='margin-right:60px;height:4px;'></p></td></tr>
										</table>
									<br>
									</div>

<!--end grid----->

								</div>
					 
					 			 					

		 
						  </form>
								</div>						 	
							 </div>
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	
					<!----------table grid----------------->
					
							
<!----------table grid end------------->

				
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



