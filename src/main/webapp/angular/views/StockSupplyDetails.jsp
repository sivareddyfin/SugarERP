	<!-----	<style>

.styletr {border-bottom: 1px solid grey;}


</style>-->


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="StockSupplyDetailsController" ng-init="loadStatus();getAllDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Supply Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="StockSupplyDetailsForm" novalidate ng-submit="StockSupplySubmit(AddedStockSupply,StockSupplyDetailsForm)" >

						
						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{'has-error':StockSupplyDetailsForm.fromDate.$error.required && (StockSupplyDetailsForm.fromDate.$dirty || Addsubmitted)}">																							
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date "  data-ng-required='true'  name="fromDate" data-ng-model="AddedStockSupply.fromDate" placeholder="From Date" tabindex="1"  maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="fromDate" with-floating-label/>
									</div>
									<p ng-show="StockSupplyDetailsForm.fromDate.$error.required && (StockSupplyDetailsForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 	 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{'has-error':StockSupplyDetailsForm.todate.$error.required && (StockSupplyDetailsForm.todate.$dirty || Addsubmitted)}">																							
											 	<div class="fg-line">
												<label for="todate">To Date</label>
													<input type="text" class="form-control date" tabindex="2" name="todate" ng-required='true' data-ng-model="AddedStockSupply.todate" placeholder="Todate" maxlength="10"data-input-mask="{mask: '00-00-0000'}" id="todate" with-floating-label/>
												</div>
												<p ng-show="StockSupplyDetailsForm.todate.$error.required && (StockSupplyDetailsForm.todate.$dirty || Addsubmitted)" class="help-block">To Date is required</p>		 	 
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper" ng-class="{'has-error':StockSupplyDetailsForm.addedDepartment.$error.required && (StockSupplyDetailsForm.addedDepartment.$dirty || Addsubmitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100" name="addedDepartment" tabindex="3"   data-ng-model="AddedStockSupply.addedDepartment" ng-options="addedDepartment.deptCode as addedDepartment.department for addedDepartment  in DepartmentsData" data-ng-required='true'>
														<option value=''>Department</option>
													 </select>
												
												</div>
											<p ng-show="StockSupplyDetailsForm.addedDepartment.$error.required && (StockSupplyDetailsForm.addedDepartment.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 		
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"></span>
											 	<div class="fg-line">
												
												 <button type="submit"   name="getData"  class="btn btn-primary btn-sm m-t-10" ng-click="getStockSupplyDetails(AddedStockSupply.fromDate,AddedStockSupply.todate,AddedStockSupply.addedDepartment);">Get Details</button>	
												</div>																							 
										</div>
									</div>
								</div>									
							</div>
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive' id="hidetable" style="display:none;">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;" ng-class="{ 'active': AddedStockSupply.$edit }">
									<th>&nbsp;&nbsp; &nbsp;&nbsp; Action</th>
									<th>Stock In No</th>
									<th>Date </th>
									<th>Items</th>
									<th>Department</th>
									
									
								</tr>
								<tr class="styletr"   ng-repeat="stockSupply in stockSupplyData">
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
									
									<a  href="#/store/transaction/stockIn"  >
										 <img src="../icons/edit.png" style="height:30px;" ng-click="UpdateStockInNyyo(stockSupply.stockInNo);" />
										</a>
									</td>
									
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Stock In No"  name="stockInNo{{$index}}" ng-model="stockSupply.stockInNo" readonly>																					
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Date"  name="date{{$index}}" ng-model="stockSupply.stockDate" readonly>																					
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Items"  name="items{{$index}}" ng-model="stockSupply.Items" readonly>																					
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Department"  name="department{{$index}}" ng-model="stockSupply.departmentName" readonly>																					
									</div>
									</td>
									
									
									
									
									
									
									
									
								</tr>
							</table>
							
							</div>
							
						 <br>
						 					
						<!-----	<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(StockSupplyDetailsForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	---->			
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
