<script type="text/javascript">
    $(".autofocus").focus();
</script>

</script>

<style>
 table,th,td{
     text-align:center;
	 
	 }
	
	div{
	   color:#0099CC;
	   }
	   .numberCol
	   {
	      font-family:Calibry, Helvetica, sans-serif;
	   }
	#asdf {
  font-family: "Gothic 57";
  Gothic 57 Normal.ttf) format("truetype");
}
h1 { font-family: "Gothic 57", sans-serif } 
</style>

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
	<section id="content" data-ng-controller="SalesDeliveryChallanPrintController"  ng-init='updatePrintitemData();'>       
        	<div class="container">				
    			<div class="block-header"><h2><b>Sales Delivery Challan</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding">
				  
				  <!--------Form Start----->
					 <!-------body start------>
					 
					 <div  id="itemsfromZonalOffice">
						  <div style="border:5px solid #0099CC;border-radius:5px; margin:5px; text-align:center; padding:5px;">   
						<div  style="font-family:Georgia, Helvetica, sans-serif; border-radius:5px ;  border-color:  #0099CC; border: 1px  #0099CC solid; margin:5px;">
						
						<div>
						
						   <table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
						<tr style="text-align:center; font-size:16px;"><td style="text-align:center;"><h1 style="color:#0099CC;font-family: 'Gothic 57', sans-serif;">Sri Sarvaraya Sugars Limited</h1></td></tr><br/>
						<tr style="text-align:center; font-size:14px;"><td style="color:#0099CC"><strong>CHELLURU</strong></td></tr><br/>
						<tr style="text-align:center; font-size:14px;"><td style="color:#0099CC"><strong>SALES DELIVERY CHALLAN</strong></td></tr><br/>
						
						</table>
						<br>
						
						
				
						 <table width="100%">
							   <tr><td style="text-align:left; color:#0099CC; font-family:Calibry, Helvetica, sans-serif;"  >&nbsp;&nbsp;To  </td><td></td><td style="text-align:right;color:#0099CC;" >Date :  <span  class="numberCol">&nbsp;&nbsp;27-02-2017 </span></td></tr>
								<tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ><td style="text-align:left;" colspan="3" >&nbsp;&nbsp;  The Sr. General Manager,</td></tr>
								<tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ><td style="text-align:left;" colspan="3" >&nbsp;&nbsp;  Sri Sarvaraya Sugars Ltd., Chelluru.</td></tr><br/><br/>
								<tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ><td style="text-align:left;" colspan="3" ><br /></td></tr>
								
							   <tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ><td style="text-align:left; padding-left: 80px;" colspan="3" >Today, I have received the following items from the zonal office   </td></tr>
							 </table>
						 
					
						 <br/>
					
						<table style="width:100%; border-collapse:collapse;" border="1"  bordercolor="#0099CC"  >
						<thead>
					   <tr style="color:#0099CC" >
					   
						  <th style="font-size:14px;">S.No</th>  <th style="font-size:14px;" > Name of the Input </th>  <th style="font-size:14px;">No. of Acres</th>   <th style="font-size:14px;">No. of Bags/kgs</th>   <th style="font-size:14px;">Remarks (if any)</th> 
						</tr></thead>
						<tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ng-repeat="stockTransferPrintdata in itemsfromZonalOffice">
						<td class="numberCol" style=" text-align:center;font-family:Calibry, Helvetica, sans-serif;">{{$index+1}}</td>
						<td style="text-align:center;" style=" font-family:Calibry, Helvetica, sans-serif;">{{stockTransferPrintdata.itemName}} </td> 
						<td style="text-align:center;" class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif;">{{stockTransferPrintdata.noOfAcres}}</td>
						<td style="text-align:center;" class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif;">{{stockTransferPrintdata.noOfBags}}</td>
						<td style="text-align:center;"  style=" font-family:Calibry, Helvetica, sans-serif;">{{stockTransferPrintdata.remarks}}</td>
						</tr>
						
					</table>  
					
						<br>
						
						<table width="100%">
						  <tr><td></td><td></td><td style="text-align:center; width:50%;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;"> <br />Signature of the Farmer</td>
						  <tr><td style="text-align:left;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" colspan="3" >&nbsp;&nbsp;  Farmer Name: </td></tr>
						   <tr><td style="text-align:left;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" colspan="3" >&nbsp;&nbsp; Farmer Code: </td></tr>
						    <tr><td style="text-align:left;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" colspan="3" >&nbsp;&nbsp; Village: </td></tr>
							 <tr><td style="text-align:left;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" colspan="3" >&nbsp;&nbsp; Circle: </td></tr>
							  <tr><td style="text-align:left;color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" colspan="3" >&nbsp;&nbsp; Zone: </td></tr>
						    
						   
					     </table>	
						
					
						</div>
						</div>
						</div>
					</div>
					 <div style="text-align:center;"><button class="btn btn-primary btn-sm m-t-10" ng-click="printitemsfromZonalOffice();" >Print</button></div>
			         
					 
					 </div>
					
				</div>
			</div>
		</section>			 
	</section>	