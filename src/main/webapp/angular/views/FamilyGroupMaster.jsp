	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="FamilyGroupMaster" data-ng-init='loadFamilyGroupCode();loadAddedFamilyGroups();loadZoneNames();loadCircleNames();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Family Group Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="FamilyGroupMasterForm" ng-submit='AddFamilyGroupSubmit(AddedFamilyGroup,FamilyGroupMasterForm);' novalidate>
					 		<div class="row">
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div  class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												<label for="familyGroupCode">Family Group Code</label>
        		    					          <input type="text" class="form-control" placeholder="Family Group Code"  maxlength="10" readonly name="familyGroupCode" data-ng-model='AddedFamilyGroup.familyGroupCode' id="familyGroupCode" with-floating-label />
			    		            	        </div>
												</div>
			                    			</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div  class="form-group floating-label-wrapper">
			        	                			<div class="fg-line">
													<label for="Description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="4" name="description" data-ng-model='AddedFamilyGroup.description' ng-blur="spacebtw('description')" id="Description" with-floating-label>
			                	        				</div>
													</div>
			                    				</div>
										</div>
									</div>
									
									
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : FamilyGroupMasterForm.zoneCode.$invalid && (FamilyGroupMasterForm.zoneCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedFamilyGroup.zoneCode' name="zoneCode" ng-options="zoneCode.id as zoneCode.zone for zoneCode in zoneNames | orderBy:'-zone':true">
															<option value="">Select Zone</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="FamilyGroupMasterForm.zoneCode.$error.required && (FamilyGroupMasterForm.zoneCode.$dirty || Addsubmitted)" class="help-block">Select Zone</p>		 
											  </div>
			                    	      </div>
										</div>
									</div>
																		
									<!--<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                			<div class="fg-line">
													<label for="Description">Description</label>
            					          				<input type="text" class="form-control" placeholder="Description"  maxlength="50" tabindex="4" name="description" data-ng-model='AddedFamilyGroup.description' ng-blur="spacebtw('description')" id="Description" with-floating-label>
			                	        			</div>
			                    				</div>
										</div>
									</div>-->
									
									<!--<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  tabindex="5"  data-ng-model='AddedFamilyGroup.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1" tabindex="6"  data-ng-model='AddedFamilyGroup.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>-->	
									
																		
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : FamilyGroupMasterForm.familyGroupName.$invalid && (FamilyGroupMasterForm.familyGroupName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="fgroupname">Family Group Name</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Family Group Name"  maxlength="25" tabindex="1" name="familyGroupName" data-ng-model='AddedFamilyGroup.familyGroupName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtw('familyGroupName');validateDup();"  id="fgroupname" with-floating-label>														 
				                				    </div>
								<p ng-show="FamilyGroupMasterForm.familyGroupName.$error.required && (FamilyGroupMasterForm.familyGroupName.$dirty || Addsubmitted)" class="help-block">Family Group Name is required.</p>
								<p ng-show="FamilyGroupMasterForm.familyGroupName.$error.pattern  && (FamilyGroupMasterForm.familyGroupName.$dirty || Addsubmitted)" class="help-block">Enter Valid Family Group Name.</p>		
								<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedFamilyGroup.familyGroupName!=null">Family Group Name Already Exist.</p>												
												</div>
					                    	</div>
										</div>
									</div>
									
									
									<!--<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
							        	            <div class="fg-line">
													<label for="famem">Family Members</label>
    	        								 <input type="text" class="form-control" placeholder="Family Members"  maxlength="500" id="famem" with-floating-label />														 
				                				    </div>
								<p ng-show="FamilyGroupMasterForm.familymembers.$error.required && (FamilyGroupMasterForm.familymembers.$dirty || Addsubmitted)" class="help-block">Family Members Are required</p>
								<p ng-show="FamilyGroupMasterForm.familymembers.$error.pattern  && (FamilyGroupMasterForm.familymembers.$dirty || Addsubmitted)" class="help-block">Enter Valid Family Members</p>	
																				
												</div>
					                    	</div>
										</div>
									</div>-->
									
									
									
									
																	
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : FamilyGroupMasterForm.circleCode.$invalid && (FamilyGroupMasterForm.circleCode.$dirty || Addsubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100" tabindex="3" name="circleCode" data-ng-required='true' data-ng-model='AddedFamilyGroup.circleCode' ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-circle':true">
															<option value="">Select Circle</option>
				        					            </select>
					                	        	</div>
								<p ng-show="FamilyGroupMasterForm.circleCode.$error.required && (FamilyGroupMasterForm.circleCode.$dirty || Addsubmitted)" class="help-block">Select Circle</p>																									
												</div>							
					                    	</div>
										</div>
									</div>																		
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  tabindex="5"  data-ng-model='AddedFamilyGroup.status'>
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1" tabindex="6"  data-ng-model='AddedFamilyGroup.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>																											
								</div>
							</div><br />		
							<input type="hidden"  name="modifyFlag" data-ng-model="AddedFamilyGroup.modifyFlag"  />				
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(FamilyGroupMasterForm)">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Family Groups</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">		
										<form name="EditFamilyGroupMasterFrom" novalidate>					
							        <table ng-table="FamilyGroupMaster.tableEdit" class="table table-striped table-vmiddle">
											<thead>
												<tr>
													<th style="width:10%"><span>Action</span></th>
													<th style="width:15%"><span>Family Group Code</span></th>
													<th style="width:15%"><span>Family Group Name</span></th>
													<th style="width:10%"><span>Zone</span></th>
													<th style="width:10%"><span>Circle</span></th>
													<th style="width:15%"><span>Description</span></th>
													<th style="width:18%"><span>Family Group Members</span></th>
													<th style="width:7%"><span>Status</span></th>
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="FamilyGroup in FamilyGroupData"  ng-class="{ 'active': FamilyGroup.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!FamilyGroup.$edit" ng-click="FamilyGroup.$edit = true;FamilyGroup.modifyFlag='yes'"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="FamilyGroup.$edit" ng-click="UpdateFamilyGroup(FamilyGroup,$index);FamilyGroup.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="FamilyGroup.modifyFlag"  />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!FamilyGroup.$edit">{{FamilyGroup.familyGroupCode}}</span>
					    		                <div ng-if="FamilyGroup.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="FamilyGroup.familyGroupCode" placeholder='familyGroup Code' maxlength="10" name="familyGroupCode{{$index}}" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!FamilyGroup.$edit">{{FamilyGroup.familyGroupName}}</span>
							                     <div ng-if="FamilyGroup.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$invalid && (EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="FamilyGroup.familyGroupName" placeholder='FamilyGroupName' maxlength="25" name="familyGroupName{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('familyGroupName',$index);validateDuplicate(FamilyGroup.familyGroupName,$index);"/>
			<p ng-show="EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$error.required && (EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$dirty || submitted)" class="help-block">Family Group Name is required.</p>
			<p ng-show="EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$error.pattern  && (EditFamilyGroupMasterFrom.familyGroupName{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Family Group Name.</p>	
			<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="FamilyGroup.familyGroupName!=null">FamilyGroup Name Already Exist.</p>												

</div>
												 </div>
							                  </td>
							                  <td>
							                      <!--<span ng-if="!FamilyGroup.$edit">{{FamilyGroup.zone}}</span>-->
												  <span ng-if="!FamilyGroup.$edit" ng-repeat='familyzone in zoneNames'>
												  	<span ng-if="familyzone.id==FamilyGroup.zoneCode">{{familyzone.zone}}</span>
												  </span>
        		            					  <div ng-if="FamilyGroup.$edit">
												  	<div class="form-group">
												  	<select class="form-control1"  ng-model='FamilyGroup.zoneCode' ng-options="zoneCode.id as zoneCode.zone for zoneCode in zoneNames | orderBy:'-zone':true"></select>
													</div>
												  </div>
							                  </td>
											  <td>
							                       <span ng-if="!FamilyGroup.$edit" ng-repeat='familycircle in circleNames'>
												  	<span ng-if="familycircle.id==FamilyGroup.circleCode">{{familycircle.circle}}</span>
												  </span>
        		            					  <div ng-if="FamilyGroup.$edit">
												  	<div class="form-group">
												  	<select class="form-control1" ng-model='FamilyGroup.circleCode' ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-circle':true"></select>
													</div>
												  </div>
							                  </td>
											  <td>
							                      <span ng-if="!FamilyGroup.$edit">{{FamilyGroup.description}}</span>
        		            					  <div ng-if="FamilyGroup.$edit">
												  	<div class="form-group">
												  	 <input class="form-control" type="text" ng-model="FamilyGroup.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
													 </div>
												  </div>
							                  </td>
											  
											 <td>
                		    					<span ng-if="!FamilyGroup.$edit">{{FamilyGroup.familyGroupMembers}}</span>
					    		                <div ng-if="FamilyGroup.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="FamilyGroup.familyGroupMembers" placeholder='family Group Members' maxlength="1000" name="familyGroupMembers{{$index}}" readonly />
													</div>
												</div>
					            		      </td> 
											  
											  
											  
											  <td style="width:235px;">
							                     
												  <span ng-if="!FamilyGroup.$edit">
												  		<span ng-if="FamilyGroup.status=='0'">Active</span>
														<span ng-if="FamilyGroup.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="FamilyGroup.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0" checked="checked" data-ng-model='FamilyGroup.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" data-ng-model='FamilyGroup.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>											  
							             </tr>
										 </tbody>
								         </table>	
									</form>
									</div>
									</section>						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
