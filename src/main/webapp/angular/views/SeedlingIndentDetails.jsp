<script>
	  $(function() 
	  {
	     $(".datepicker" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SeedlingIndentReportController" ng-init="loadZoneNames();loadKindType();loadVarietyNames();loadCircleNames();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Indent Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>					
					<form name="SeedlingIndentform" novalidate ng-submit=''>
						<div class="row">
							
							
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingIndentform.circle.$invalid && (SeedlingIndentform.circle.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="circle"  data-ng-model="Weighment.circle" ng-options="circle.id as circle.circle for circle in CircleNames  | orderBy:'-id':true" tabindex="3" data-ng-required='true'>
													<option value=''>Circle</option>
												</select>	
											</div>
											<p ng-show="SeedlingIndentform.circle.$error.required && (SeedlingIndentform.circle.$dirty || submitted)" class='help-block'>Select Circle</p>					
										</div>
			                    </div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingIndentform.zone.$invalid && (SeedlingIndentform.zone.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="zone"  data-ng-model="Weighment.zone" ng-options="zone.id as zone.zone for zone in ZoneData  | orderBy:'-id':true" tabindex="3" data-ng-required='true'>
													<option value=''>Zone</option>
												</select>	
											</div>
											<p ng-show="SeedlingIndentform.zone.$error.required && (SeedlingIndentform.zone.$dirty || submitted)" class='help-block'>Select Zone</p>					
										</div>
			                    </div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingIndentform.variety.$invalid && (SeedlingIndentform.circle.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="variety"  data-ng-model="Weighment.variety" ng-options="variety.id as variety.variety for variety in varietyNames  | orderBy:'-id':true" tabindex="3" data-ng-required='true'>
													<option value=''>Variety</option>
												</select>	
											</div>
											<p ng-show="SeedlingIndentform.variety.$error.required && (SeedlingIndentform.variety.$dirty || submitted)" class='help-block'>Select Variety</p>					
										</div>
			                    </div>
							</div>
							
							<div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper" ng-class="{'has-error': SeedlingIndentform.KindType.$invalid && (SeedlingIndentform.KindType.$dirty || submitted)}">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="variety"  data-ng-model="Weighment.KindType" ng-options="KindType.id as KindType.KindType for KindType in kindData  | orderBy:'-id':true" tabindex="3" data-ng-required='true'>
													<option value=''>Kind</option>
												</select>	
											</div>
											<p ng-show="SeedlingIndentform.KindType.$error.required && (SeedlingIndentform.KindType.$dirty || submitted)" class='help-block'>Select Kind</p>					
										</div>
			                    </div>
							</div>
							
							
							
						</div>
						<div class="row">
							<div class="col-sm-3"></div>
							<div class="col-sm-2"></div>
							<div class="col-sm-3">
								<div class="input-group">
            				         <button type="button"  class="btn btn-primary" ng-click="generateSeedlingindentDetails(Weighment.circle,Weighment.zone,Weighment.variety,Weighment.KindType);">Get Details</button>
									    
			                    </div>
							</div>
						</div>
							</form>
							
					
					<div id="dispSeedlingWeighmentDetails" style="display:none;">
					<h4 align="center">Indent Details</h4>	
					<table  class="table table-striped table-vmiddle"  style="border-collapse:collapse; width:100%;">
							<tr>
							<th>SNO</th>
							<th>Indent No.</th>
							<th>Summary Date</th>
							<th>Quantity </th>
							<th>Delivery Date</th>
							<th>Zone</th>
							<th>Variety</th>
							<th>Circle</th>

					
							</tr>
							<tr ng-repeat="weighment in weighmentData">
								<td>{{$index+1}}</td>
								<td>{{weighment.indentNo}}</td>
								<td>{{weighment.SummaryDate}}</td>
								<td>{{weighment.pendingQuantity}}</td>
								<td>{{weighment.batchDate}}</td>
								<td>{{weighment.zone}}</td>
								<td>{{weighment.variety}}</td>
								<td>{{weighment.Circle}}</td>

							</tr>
							</table>
		      		</div>
			 			
				</div>		 							 
					<!------------------form end---------->	
					
					
											 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



