	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneLedgerController" ng-init="loadSeason(); addFormField();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Ledger</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					
					<!--<form name="CaneLedgerForm">
					<div class="row">
					
					<div class="col-sm-2">
					</div>
						<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : CaneLedgerForm.season.$invalid && (CaneLedgerForm.season.$dirty || Filtersubmitted)}">
											  <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" name="season" ng-model="AddedSeason.season" ng-options="season.season as season.season for season in seasons   | orderBy:'-season':true" data-ng-required="true" ng-change="getSeasonRyotCode(AddedSeason.season);">
														<option value="">Select Season</option>
													</select>
												</div>
											 </div>
								<p ng-show="CaneLedgerForm.season.$error.required && (CaneLedgerForm.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>										
										   </div>
									   </div>
										
									</div>
						<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : CaneLedgerForm.season.$invalid && (CaneLedgerForm.season.$dirty || Filtersubmitted)}">
											  <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" name="ryotCode" ng-model="AddedSeason.ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodes   | orderBy:'-ryotCode':true" data-ng-required="true">
														<option value="">RYOT CODE</option>
													</select>
												</div>
											 </div>
								<p ng-show="CaneLedgerForm.season.$error.required && (CaneLedgerForm.season.$dirty || Addsubmitted)" class="help-block">Select RyotCode</p>										
										   </div>
									   </div>
										
									</div>
									
									<div class="col-sm-3">
									<button type="button" class="btn btn-primary" ng-click="loadCaneLedgerData(AddedSeason.season,AddedSeason.ryotCode);">Load Details</button>
									</div>			
					</div>
					
					</form>-->
		      
		
			<form name="CaneLedgerFormData" ng-submit="CaneLedgerFormSubmit(AddCaneLedger,CaneLedgerFormData);">
		
					<div class="row">
							
							<div class="col-sm-2">
							</div>
								<div class="col-sm-3">
												<div class="input-group">
												  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 
													  <div class="fg-line">
														<div class="select">
															<select chosen class="w-100" name="season" ng-model="AddCaneLedger.season" ng-options="season.season as season.season for season in seasons   | orderBy:'-season':true" data-ng-required="true" ng-change="getSeasonRyotCode(AddCaneLedger.season);">
																<option value="">Select Season</option>
															</select>
														</div>
													 </div>
										
											   </div>
												
											</div>
								<div class="col-sm-3">
												<div class="input-group">
												  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												 
													  <div class="fg-line">
														<div class="select">
															<select chosen class="w-100" name="ryotCode" ng-model="AddCaneLedger.ryotCode" ng-options="ryotCode.ryotcode as ryotCode.ryotcode for ryotCode in ryotCodes   | orderBy:'-ryotCode':true" data-ng-required="true">
																<option value="">RYOT CODE</option>
															</select>
														</div>
													 </div>
										
											   </div>
												
											</div>
											
											<div class="col-sm-3">
											<button type="submit" class="btn btn-primary">Load Details</button>
											</div>			
							</div>
			
			
			
			
			
			
			<br><br><br>
			<div id="1234" style="display:;">
			<div id="dispCaneLedger" style="display:none;">
			
			<div class="row">
				<div class="col-sm-4">
				<b>RYOT CODE :  {{AddCaneLedger.ryotcode}} </b>
				<input type="hidden" data-ng-model="AddCaneLedger.ryotcode" name="ryotcode" />
				
				</div>
				
				<div class="col-sm-4">
			<b>	SB A/C : {{AddCaneLedger.accountNum}} </b> 
					
				</div>
				
				<div class="col-sm-4">
				<b> LN. No.  : {{AddCaneLedger.loanNumber}} </b>
					
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">
				
				<b> RYOTNAME : {{AddCaneLedger.ryotname}} </b>
					
				</div>
				
				<div class="col-sm-6">
			<b> 	BANK CODE : {{AddCaneLedger.branchCode}} &nbsp;&nbsp;&nbsp;   {{AddCaneLedger.branch}}</b>
						
				</div>
				
				
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">
				<b> FATHER NAME : {{AddCaneLedger.relativeName}} </b>
					
				</div>
				
				<div class="col-sm-6">
			<b>REF NO: 	{{AddCaneLedger.referenceNumber}} </b>
				
					
				</div>
				
				
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">
					<b>VILLAGE NAME : {{AddCaneLedger.village}} </b>
					
				</div>
				
				<div class="col-sm-6">
					<b>PRINCIPLE (Rs) : {{AddCaneLedger.principle}} </b>
					
				</div>
				
				
			</div>
			<br>
				<div class="row">
				<div class="col-sm-3">
				<b>PLANT (Acres) : {{AddCaneLedger.plant}} </b>
					
				</div>
				
				<div class="col-sm-3">
				<b>RATOON (Acres) : {{AddCaneLedger.ratoon}} </b>
				
				</div>
				
				<div class="col-sm-3">
				<b>AGREED QTY(tons): {{AddCaneLedger.agreedqty}} </b>
					
				</div>
				
				<div class="col-sm-3">
				<b>INTEREST AMOUNT (Rs): {{AddCaneLedger.interestAmount}} /-</b>
					   
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-sm-3">
				<b>CANE PRICE(Rs):{{AddCaneLedger.canePrice}} /- </b>
					
				</div>
				
				<div class="col-sm-3">
				<b></b>
				
				</div>
				
				<div class="col-sm-3">
				<b></b>
					
				</div>
				
				<div class="col-sm-3">
				<b></b>
					   
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3">
				<b>O/B  </b>
					
				</div>
				
				<div class="col-sm-3">
				<b>SUPPLIED QTY(Tons): {{AddCaneLedger.progressiveqty}}  </b>
				
				</div>
				
				<div class="col-sm-3">
				<b>PROG.AMT(Rs) :{{AddCaneLedger.AmountTotalpayable}} /- </b>
					
				</div>
				
				<div class="col-sm-3">
				<b></b>
					   
				</div>
			</div>
			
			
			<br>
			<div class="table responsive">
			<table style="width:100%;">
			<tr>
				
				<td>DATE </td>
				<td>RECT.NO  </td>
				<td>SHIFT Id </td>
				<td> SUP.QTY(Tons) </td>
				<td> AMOUNT PAYABLE(Rs)</td>
			
			</tr>
			
				<tr ng-repeat="summary in data">
			<td style="display:none;">
				<button type="button" class="btn btn-primary" ng-click="addFormField();" ng-if="summary.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
				<button type="button" class="btn btn-primary" ng-if="summary.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>	
			
					<input type="hidden"  data-ng-model="summary.id" />
					</td>
					<td>{{summary.crDate}}<input type="hidden" class="form-control" data-ng-model="summary.crDate" name="summaryDate{{$index}}"  /></td>
				  <td>{{summary.rectNo}}<input type="hidden" class="form-control" data-ng-model="summary.rectNo" name="reactno{{$index}}"  /></td>
				<td>{{summary.shiftId}}<input type="hidden" class="form-control"  data-ng-model="summary.shiftId" name="shift{{$index}}"  /></td>
				 <td>{{summary.supQty}}<input type="hidden" class="form-control"  data-ng-model="summary.supQty" name="supqty{{$index}}"  /></td>
				<td>{{summary.amountPayable}}/-<input type="hidden" class="form-control"  data-ng-model="summary.amountPayable" name="amountPayable{{$index}}"  /></td>
				
				
				
				</tr>
			<tr>
			<br>
			<td>TOTAL(Rs):</td><td></td><td></td><td><b>{{AddCaneLedger.supQtyTotal}}</b></td><td><b>{{AddCaneLedger.AmountTotalpayable}}/-</b></td>
			</tr>
			</table>
			</div>
			<br>
			
			
			<div class="row">
				<div class="col-sm-4">
				
					<b>UNLOAD CHARGES (Rs)   : {{AddCaneLedger.udcAmount}}  </b>

				</div>
				<div class="col-sm-4">
				
					<b>CDC (Rs)   :  {{AddCaneLedger.cdcAmount}}    </b>

				</div>
				<div class="col-sm-4">
				
					<b> L.REC (Rs) : {{AddCaneLedger.paidAmount}} /-     </b>

				</div>
			</div>
			
			
			<br>
			
			<div class="row">
				<div class="col-sm-3">
				<b>	TRANSPORT (Rs) :{{AddCaneLedger.transport}}/- </b>
				</div>
				
				<div class="col-sm-3">
				
				</div>
				
				<div class="col-sm-3">
					<b> OTHER DEDUCTIONS: </b>
				</div>
				
				<div class="col-sm-3">
					  
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3">
					<b>HARVEST :    
					{{AddCaneLedger.harvAmountPay}}/- </b>
 					
				</div>
				
				<div class="col-sm-3">
				
				</div>
				
				<div class="col-sm-3">
					 
				</div>
				
				<div class="col-sm-3">
					   <b>   AMOUNT  PAYABLE (Rs) : {{AddCaneLedger.amtPay}}/- </b>

				</div>
				
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">
				
				<b>	ADVANCE AMOUNT (Rs)  :{{AddCaneLedger.advanceAmount}}/-  </b>

				</div>
				<div class="col-sm-6">
				
				<b>	LOAN AMOUNT (Rs) :  {{AddCaneLedger.pendingAmount}}/- </b>

				</div>
			</div>
			
		<!--<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>-->
		</form>
		</div>
		
 			</div>	
    					 
							 
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>
	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



