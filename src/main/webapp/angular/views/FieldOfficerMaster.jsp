	<script type="text/javascript">		
		$('#autofocus').focus();	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="OfficerMaster"   ng-init='loadFieldOfficerId();loadCaneManagerNames();loadAllFieldOfficers();loadFieldOfficerDropdown();'>     
        	<div class="container" >		
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Field Officer Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 
					 <form name="FieldOfficerMaster" ng-submit="fieldofficersubmit(AddedFieldOfficer,FieldOfficerMaster)"  novalidate>
					 <input type="hidden" name="screenName" ng-model="AddedFieldOfficer.screenName" />
							 <div class="row">
							<div class="col-sm-6">
							<div class="row">
							
							  
							  <div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group floating-label-wrapper">
			                        	<div class="fg-line nulvalidate">
											<label for="fieldOfficerID">Field Officer ID</label>
											<input type="text" class="form-control" placeholder="Field Officer ID" maxlength="5"   name="fieldofficerid"  data-ng-model="AddedFieldOfficer.fieldOfficerId"   readonly id="fieldOfficerID" with-floating-label>  
			                        	</div> 
										
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							  
							  
							</div><div class="row">
							
						
							    <div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : FieldOfficerMaster.employeeId.$invalid && (FieldOfficerMaster.employeeId.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">										
<select chosen class='w-100'  name="employeeId"  data-ng-model="AddedFieldOfficer.employeeId"  data-ng-required="true"  tabindex="2" ng-options="employeeId.id as employeeId.employeename for employeeId in FieldOffNames  | orderBy:'-employeename':true">
												<option value="">Select Field Officer Name</option>
											</select>
			                        	</div>
										<p ng-show="FieldOfficerMaster.employeeId.$error.required && (FieldOfficerMaster.employeeId.$dirty || Addsubmitted)" class="help-block">Select Field Officer Name</p>																				
										</div>

			                
			                    </div>			                    
			                </div>
							
							
							
</div>

<div class="col-sm-12">
			                    <div class="input-group">
            			            
									<div class="form-group" >
			                        	<div class="fg-line">
									<br />
									
									  <label class="radio radio-inline m-r-20"><b>Status</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" checked="checked"  data-ng-model="AddedFieldOfficer.status" >
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"   data-ng-model='AddedFieldOfficer.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
									
									
									
										
			                        	</div>
										
										
										
										</div>

			                
			                    </div>			                    
			                </div>

							
							</div>
							<div class="col-sm-6">
							<div class="row">
							<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : FieldOfficerMaster.caneManagerId.$invalid && (FieldOfficerMaster.caneManagerId.$dirty || Addsubmitted)}">
			                        	<div class="fg-line">
									
											<select chosen class="w-100" name="caneManagerId"    data-ng-required="true" data-ng-model="AddedFieldOfficer.caneManagerId" ng-options="caneManagerId.id as caneManagerId.canemanagername for caneManagerId in mgrNames  | orderBy:'-canemanagername':true" id="autofocus" tabindex="1" autofocus>
<option value=''>Select Cane Manager Name</option>
								</select>
			                        	</div>
										<p ng-show="FieldOfficerMaster.caneManagerId.$error.required && (FieldOfficerMaster.caneManagerId.$dirty || Addsubmitted)" class="help-block">Select Cane Manager Name</p>
										
										
										</div>

			                
			                    </div>			                    
			                </div>
							</div>
							<div class="row">
						
						<div class="col-sm-12">
			                    <div class="input-group">
            			            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			                        	<div class="fg-line">
										<label for="description">Description</label>
									
											<input type="text" class="form-control" placeholder="Description" maxlength="50" name="desc"  data-ng-model="AddedFieldOfficer.description"       tabindex="3" ng-blur="spacebtw('description')"  id="description" with-floating-label>
			                        	</div>
										

			                
			                    </div>			                    
			                </div>
							   
							    
							
							
							
							
</div>
							</div>
							
							 </div>
							 
							 <br />
							 
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddedFieldOfficer.modifyFlag"  />	
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
									     <button type="submit" class="btn btn-primary btn-hide">Save</button>
										 <button type="reset" class="btn btn-primary" ng-click="reset(FieldOfficerMaster)">Reset</button>
									</div>
								</div>						 	
							 </div>	
							 </form>										 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					<!-----------Table Griid-------------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Field Officers</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">		
								<form name="FieldoOfiicerMasterEdit" novalidate>				
							        <table  ng-table="OfficerMaster.tableEdit"  class="table table-striped table-vmiddle" >
										<thead>
													<tr>
														<th><span>Action</th>
														<th><span>FIeld Officer ID</span></th>
														<th><span>Field Officer Name</span></th>
														<th><span>Cane Manager Name</span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
										
								        <tr ng-repeat="Officer in OfficerData"  ng-class="{ 'active': Officer.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!Officer.$edit" ng-click="Officer.$edit = true;Officer.modifyFlag='Yes';;Officer.screenName='Field Officer Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success" ng-if="Officer.$edit" ng-click="filedofficerupdate(Officer,$index);Officer.$edit = isEdit" ><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Officer.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="Officer.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Officer.$edit">{{Officer.fieldOfficerId}}</span>
					    		                <div ng-if="Officer.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="Officer.fieldOfficerId" maxlength="25"  readonly placeholder='Field Officer ID' />
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!Officer.$edit">{{Officer.fieldOfficer}}</span>
							                     <div ng-if="Officer.$edit">
												 <div class="form-group" ng-class="{ 'has-error' : FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$invalid && (FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$dirty || submitted)}">												 	
<select class="form-control1" type="text" ng-model="Officer.employeeId"  data-ng-required="true"  ng-options="fieldofficername.id as fieldofficername.employeename for fieldofficername in FieldOffNames  | orderBy:'-employeename':true" name="fieldofficernameedit{{$index}}"></select>													
													
													<p ng-show="FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$error.required && (FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$dirty || submitted)" class="help-block"> Field Officer Name is required</p>
														<p ng-show="FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$error.pattern && (FieldoOfiicerMasterEdit.fieldofficernameedit{{$index}}.$dirty || submitted)" class="help-block">Enter Valid Field Officer Name</p>
													
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!Officer.$edit" ng-repeat='Canemanagers in mgrNames'>
												  	<span ng-if="Canemanagers.id==Officer.caneManagerId">{{Canemanagers.canemanagername}}</span>
												  </span>
        		            					  <div ng-if="Officer.$edit">
												  <div class="form-group" ng-class="{ 'has-error' : FieldoOfiicerMasterEdit.canenameedit.$invalid && (FieldoOfiicerMasterEdit.canenameedit.$dirty || submitted)}">
												  	<select class="form-control1" id="input_canemanagername"  ng-model="Officer.caneManagerId" name="canenameedit" ng-options="caneManagerName.id as caneManagerName.canemanagername for caneManagerName in mgrNames  | orderBy:'-canemanagername':true">
													</select>
											<p ng-show="FieldoOfiicerMasterEdit.canenameedit.$error.required && (FieldoOfiicerMasterEdit.canenameedit.$dirty || submitted)" class="help-block"> Cane Manager Name is required</p>
											</div>
												  </div>
							                  </td>
							                  <td data-title="'Description'">
                    							   <span ng-if="!Officer.$edit">{{Officer.description}}</span>
												   <div ng-if="Officer.$edit">
												   		<div class="form-group">
														   	<input class="form-control" type="text" ng-model="Officer.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
														</div>
												   </div>
							                  </td>
											  <td >
							                      <span ng-if="!Officer.$edit">
												  	<span ng-if="Officer.status=='0'">Active</span>
													<span ng-if="Officer.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Officer.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status{{$index}}" value="0" ng-model="Officer.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status{{$index}}" value="1" ng-model="Officer.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>					    		              										  			
							             </tr>
										 </tbody>
								         </table>	
										 
										 </form>
										 </div>
										 </section>
										 						 
							     </div>
							</div>
						</div>					
					<!-----------Table Grid End----------->
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
