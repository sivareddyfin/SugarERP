	<script type="text/javascript">		
		$('#autofocus').focus();		
	</script>
	
	<script>
	  $(function() 
	  {
	    $(".datepicker").datepicker({dateFormat: 'dd-mm-yy'});
		 
      });
    </script>	


	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	   <section id="main" class='bannerok'>    
	       <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	          <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateHarvestCharges" ng-init="loadSeasonNames();loadCircleNames();loadHarvestNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Harvest Charges</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					  <form name="UpdateHarvestChargesForm" ng-submit="UpdateHarvestSubmit(AddedCharges,UpdateHarvestChargesForm)" novalidate>
					        <div class="row">
							  <div class="col-sm-4">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : UpdateHarvestChargesForm.season.$invalid && (UpdateHarvestChargesForm.season.$dirty || filterSubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="AddedCharges.season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" ng-required='true'>
                        <option value="">Select Season</option>

                    </select>
                	            	   </div>
                	        </div>
				<p ng-show="UpdateHarvestChargesForm.season.$error.required && (UpdateHarvestChargesForm.season.$dirty || filterSubmitted)" class="help-block">Select Season</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-4">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : UpdateHarvestChargesForm.circleCode.$invalid && (UpdateHarvestChargesForm.circleCode.$dirty || filterSubmitted)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="circleCode" data-ng-model="AddedCharges.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-circle':true" ng-required='true'>
                                         <option value="">Select Circle</option>

                    </select>
                	            	   </div>
                	        </div>
			<p ng-show="UpdateHarvestChargesForm.circleCode.$error.required && (UpdateHarvestChargesForm.circleCode.$dirty || filterSubmitted)" class="help-block">Select Circle</p>
							</div>
                    	</div>
					</div>
				</div>	
			</div>
								<div class="col-sm-4">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getCaneSuppliedRyotDetails(UpdateHarvestChargesForm);">Load Cane Supplied Ryot Details</button>

													</div>
												</div>
  											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
													   <div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
														<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : UpdateHarvestChargesForm.effectiveDate.$invalid && (UpdateHarvestChargesForm.effectiveDate.$dirty || submitted)}">
			        	                					<div class="fg-line">
															<label for="fromdate">From Date</label>
            					                                  <input type="text" class="form-control datepicker" placeholder="From Date"  maxlength="25" name="effectiveDate" data-ng-model="AddedCharges.effectiveDate" data-input-mask="{mask: '00-00-0000'}" id="fromdate" with-floating-label data-ng-required="true" />
			                	        				</div>
														<p ng-show="UpdateHarvestChargesForm.effectiveDate.$error.required && (UpdateHarvestChargesForm.effectiveDate.$dirty || submitted)" class="help-block">From Date is required</p>
								
															</div>
			                    						</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
													   <div class="input-group" style="margin-top:10px;">
            				           					  <span class="input-group-addon">Harvesting Method:</span>
			        	                					<div class="fg-line" style="margin-top:5px;">
            					                               <label class="radio radio-inline m-r-20">
								                                <input type="radio" name="harvestingMode" value="0" data-ng-model="AddedCharges.harvestingMode">
			    			            	                       <i class="input-helper"></i>Manual
								                               </label>
				 			                                       <label class="radio radio-inline m-r-20">
				            			                        <input type="radio" name="harvestingMode" value="1" data-ng-model="AddedCharges.harvestingMode">
			    				                                 <i class="input-helper"></i>Mechanical
					  		                                    </label>
			                	        			        </div>
										 		        </div>
													</div>
												</div>
											</div>
										</div>
											
												<!--<div class="row" align="center">
													  <div class="col-sm-12">
													    <div class="input-group">
															<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getRyotDetails();">Load  Ryot Details</button>

															</div>
														</div>
													</div>
												</div>-->
											
											<hr />
											
											
											<div  class="table-responsive">
							     <section class="asdok">
									<div class="container1">
									<!--<form name="editUpdateHarvestForm" novalidate >-->	
							             <table ng-table="UpdateHarvestCharges.tableEdit" class="table table-striped">
							 	<thead>
									<tr>
										<th><span>Ryot Code</span></th>
										<th><span>Ryot Name</span></th>
										<th><span>Harvesting Contr</span></th>
										<th><span>Sl.No</span></th>
										<th><span>Rate Per Ton /Acre</span></th>
										
										
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="Updateharvest in UpdateharvestData"  ng-class="{ 'active': Updateharvest.$edit }">
									 
									 <td>
                		    			{{Updateharvest.ryotCode}}
										<input type="hidden" name="ryotCode" ng-model="Updateharvest.ryotCode" />										
					    		        </td>
										 <td>
                		    			{{Updateharvest.ryotName}}
										<input type="hidden" name="ryotName" ng-model="Updateharvest.ryotName" />
					    		        </td>
										 <td>
                		    			<div class="form-group" ng-class="{ 'has-error' : UpdateHarvestChargesForm.harvestContractorCode{{$index}}.$invalid && (UpdateHarvestChargesForm.harvestContractorCode{{$index}}.$dirty || submitted)}">																																																																																																																						 													<select class="form-control1" name="harvestContractorCode{{$index}}" ng-model="Updateharvest.harvestContractorCode" ng-options="harvestContractorCode.id as harvestContractorCode.harvester for harvestContractorCode in harvestNames | orderBy:'-harvester':true" >
														<option value="">Select Harvester</option>
													</select>
													
	                     <p ng-show="UpdateHarvestChargesForm.harvestContractorCode{{$index}}.$error.required && (UpdateHarvestChargesForm.harvestContractorCode{{$index}}.$dirty || submitted)" class="help-block">Select</p>												
	<!--<p ng-show="CompanyForm.district{{CompanyData.id}}.$error.pattern  && (CompanyForm.district{{CompanyData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>-->																																																																

											</div>
					    		        </td>
										 <td>
										 <div class="form-group" ng-class="{ 'has-error' : UpdateHarvestChargesForm.slNo{{$index}}.$invalid && (UpdateHarvestChargesForm.slNo{{$index}}.$dirty || submitted)}">
               <input type="text" class="form-control1" name="slNo{{$index}}" data-ng-model="Updateharvest.slNo"  data-ng-pattern="/^[0-9\s]*$/" placeholder="Serial Number">
		<p ng-show="UpdateHarvestChargesForm.slNo{{$index}}.$error.required && (UpdateHarvestChargesForm.slNo{{$index}}.$dirty || submitted)" class="help-block">Required</p>
		<p ng-show="UpdateHarvestChargesForm.slNo{{$index}}.$error.pattern  && (UpdateHarvestChargesForm.slNo{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
											</div>
					    		        </td>
										 <td>
                		    		
										<div class="form-group" ng-class="{ 'has-error' : UpdateHarvestChargesForm.rate{{$index}}.$invalid && (UpdateHarvestChargesForm.rate{{$index}}.$dirty || submitted)}">
            <input type="text" class="form-control1" name="rate{{$index}}" data-ng-model="Updateharvest.rate"   data-ng-pattern="/^[0-9\s]*$/" placeholder="Harvesting Rate"/>
		                   <p ng-show="UpdateHarvestChargesForm.rate{{$index}}.$error.required && (UpdateHarvestChargesForm.rate{{$index}}.$dirty || submitted)" class="help-block">Required</p>
		                     <p ng-show="UpdateHarvestChargesForm.rate{{$index}}.$error.pattern  && (UpdateHarvestChargesForm.rate{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
											</div>
					    		        </td>
										
																			
												</tr>
											</tbody>
						     			</table>
							 		</div>
							 	</section>
							 </div>
							 
							<div class="row" align="center">
							 	<div class="col-sm-12">
				                    <div class="input-group">            				            
			        	                <div class="fg-line">
										             <button type="submit" class="btn btn-primary btn-hide">Save</button>
									                  <button type="reset" class="btn btn-primary" ng-click="reset(UpdateHarvestChargesForm)">Reset</button>
													   
												
			                	        </div>
			                    	</div>			                    
				                  </div>
							 </div>
							 <br />
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->	
					      </form>	 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>														 