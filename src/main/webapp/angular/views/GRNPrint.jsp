<script type="text/javascript">
    $(".autofocus").focus();
</script>

</script>

<style>
 table,th,td{
     text-align:center;
	 
	 }
	
	div{
	color:#0099CC;
	}
	   .numberCol
	   {
	      font-family:Calibry, Helvetica, sans-serif;
	   }
	
</style>

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		<section id="content" data-ng-controller="GRNPrintController"  ng-init='updateGRNPrintData(); '>       
        	<div class="container">				
    			<div class="block-header"><h2><b>GOODS RECEIPT NOTE</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding">
				  
				  <!--------Form Start----->
					 <!-------body start------>
					 
					 <div  id="grnPrint">
					    
						<table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
					<tr style="text-align:center; font-size:16px;"><td style="text-align:center;"><h1 style="color:#0099CC;font-family: 'Gothic 57', sans-serif;">Sri Sarvaraya Sugars Limited</h1></td></tr><br/>
					<tr style="text-align:center; font-size:14px;"><td><strong style="color:#0099CC;">Chelluru</strong></td></tr>
					<tr style="text-align:center; font-size:14px;"><td><strong style="color:#0099CC;">GOODS RECEIPT NOTE</strong></td></tr><br/>
					
			</table>
			   <div class="row" style="font-family:Georgia, Helvetica, sans-serif; border:1px solid #0099CC; border-radius:5px ; margin-bottom:2px;">
			<div class="col-sm-12" style="margin-top:-35px;" >
			
			            <table width="100%"  style="font-family:Georgia, Helvetica, sans-serif; color:#0099CC" >
					 <tr><td style="text-align:left; "  >&nbsp;&nbsp;To  </td><td></td><td style="text-align:right; font-family:Calibry, Helvetica, sans-serif;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No :  <span  class="numberCol" style=" font-size:13px;">&nbsp;&nbsp;0100002/16-17 </span></td></tr>
					  <tr><td style="text-align:left; "  >&nbsp;&nbsp;The Sr.General Manager,  </td><td></td><td style="text-align:right; font-family:Calibry, Helvetica, sans-serif;" >Date :  <span  class="numberCol" style=" font-size:13px;">&nbsp;&nbsp;27-02-2017 </span></td></tr>
					  <tr><td style="text-align:left;" colspan="3" >&nbsp;&nbsp;  Sri Sarvaraya Sugars Ltd., Chelluru.</td></tr><br/><br/>
					  
					 <tr><td style="text-align:left;" colspan="3">
           <br/>					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><u>Through &nbsp;&nbsp;&nbsp;&nbsp; (AGM &nbsp;&nbsp; : &nbsp;&nbsp;Cane)</u></span> </td></tr><br>
					   <tr><td style="text-align:left;" colspan="3">  <br/>&nbsp;&nbsp; Today we have received the following items from our factory by Vehicle No:  <span  class="numberCol">&nbsp;&nbsp; </span></td></tr>
			</table>
			
			<br>
			            <table style="width:100%; border-collapse:collapse; color:#0099CC" border="1" bordercolor="#0099CC" >
						    <thead>
							<tr>
							   <th style="font-size:14px;">S.No</th>  <th style="font-size:14px;"> Name of the Input  </th>  <th style="font-size:14px;">Quantity(Nos)  </th> <th style="font-size:14px;">For the No. of Acres </th><th style="font-size:14px;">Remarks(if any)  </th></tr></thead>
							     <tr  ng-repeat="grnPrintdata in grnPrint">
								 <td class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif; text-align:center; font-size:13px;">{{$index+1}}</td>
					              <td style="text-align:center;  font-family:Calibry, Helvetica, sans-serif; font-size:13px;">{{grnPrintdata.itemName}} </td>
								    <td style="text-align:center; font-family:Calibry, Helvetica, sans-serif;  font-size:13px;">{{grnPrintdata.quantity}}</td> 
								  <td style="text-align:center; font-family:Calibry, Helvetica, sans-serif;  font-size:13px;">{{grnPrintdata.noOfAcres}}</td> 
					              <td style="text-align:center;  font-size:13px;">{{grnPrintdata.remarks}}</td></tr>
						 </table>
						 <br> 
				
			</div>
			</div>
			
			 
			<div class="row" style="font-family:Georgia, Helvetica, sans-serif; border:1px solid #0099CC; border-radius:5px ; ">
			<div class="col-sm-12">
			     <div class="row" >
					<div class="col-xs-12">	
					     <table width="100%">
						     <tr style="color:#0099CC">
						  <td style="text-align:left;">
						  <br />	
						   <p></p>
							 <span> <strong>Field Assistant </strong></span>
						</td>
						<td></td>
						  <td style="text-align:center;">
						  <br />	
						   <p></p>
							 <span> <strong>Zonal Officer </strong></span>
						</td>
						 </tr>
						 </table>
					
					
					</div>
				</div>
			</div>
		</div>			
					 </div>
					 <div style="text-align:center;"><button class="btn btn-primary btn-sm m-t-10" ng-click="printGRN();" >Print</button></div>
				</div>
			</div>		 
		</div>
	</section>
	</section>		