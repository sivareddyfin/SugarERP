<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		.textbox
		{
			width:100px;	
		}
		.spacing-table {
  
   
    border-collapse: separate;
    border-spacing: 0 5px; /* this is the ultimate fix */
}
.spacing-table th {
    text-align: left;
    
}
.spacing-table td {
    border-width: 1px 0;
  
   
    
}
.spacing-table td:first-child {
    border-left-width: 3px;
    border-radius: 5px 0 0 5px;
}
.spacing-table td:last-child {
    border-right-width: 3px;
    border-radius: 0 5px 5px 0;
}

	</style>
	<script>
	  $(function() 
	  {
	     $( ".input_date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 		 		 
	  });
    </script>		
	

<script>
    $(function() {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>




<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
<section id="main" class='bannerok'>
    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    <section id="content" data-ng-controller="SeedAccountingController" ng-init="loadPostSeasonNames();loadVillageNames();loadSeedAccountingDate();loadSeedAccountedDates();">
        <div class="container">
		 <div class="row">
		 <div class="col-sm-3">
            <div  class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;">
                <h2><b> Seed Accounting Calculation </b></h2>
            </div>
			</div>
							<div class="col-sm-2" style="display:none;" id="buttonshow12">
				                    <div class="input-group">
            				         <div  class="form-group floating-label-wrapper"  >
			        	                <div class="fg-line">
										<label for="Rate">Rate</label>
            						<input type="text" class="form-control autofocus" placeholder='Rate' autofocus name="Rate"   data-ng-model="caneaccounting.Rate" tabindex="1"  id="Rate" with-floating-label  ng-blur="spacebtw('employeeName');" />
			                	        	</div>
											
			                    	</div>
									</div>			                    
				                  </div>
				<div style="display:none;" id="buttonshow1">
						<button type="button"   class="btn btn-primary"	ng-disabled="" ng-click="LoadRate(caneaccounting.Rate);">Apply Rate</button></div>				  
           </div>    
                    <!--------Form Start----->
                    <!-------body start------>

                    <form name="SeedAccountingForm" novalidate id="form1">
                        <div class="card">
                            <div class="card-body card-padding">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                                    <!--<div class="form-group">-->
                                                    <div class="form-group" ng-class="{ 'has-error' : SeedAccountingForm.season.$invalid && (SeedAccountingForm.season.$dirty || submitted)}">
                                                        <div class="fg-line">


                                                            <select chosen class="w-100" name="season" data-ng-model="caneaccounting.season" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true" data-ng-required='true'>
                                                                <option value="">Season</option>
                                                            </select>
                                                        </div>

                                                        <p data-ng-show="SeedAccountingForm.season.$error.required && (SeedAccountingForm.season.$dirty || submitted)" class="help-block">Select Season</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                                    <!--<div class="form-group">-->
                                                    <div class="form-group">
                                                        <div class="fg-line">




                                                            <input type="text" class="form-control datepicker" placeholder='SeedAccounting Date' name="seedAccDate" ng-model="caneaccounting.seedAccDate" data-input-mask="{mask: '00-00-0000'}" readonly />

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                                                    <div class="form-group floating-label-wrapper">
                                                        <div class="fg-line nulvalidate">
                                                            <label for="SeedAccountingSlNo"></label>
                                                            <input type="text" class="form-control" placeholder="SeedAccountingSlNo" name="seedActSlno" data-ng-model="caneaccounting.seedActSlno" readonly id="Seedslno">

                                                        </div>



                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                                    <!--<div class="form-group">-->
                                                    <div class="form-group">
                                                        <div class="fg-line">


                                                            <select chosen class="w-100" name="AccountingDate" data-ng-model="caneaccounting.AccountingDate" ng-options="AccountingDate.seedAccDate as AccountingDate.seedAccDate for AccountingDate in seedAccDates | orderBy:'-seedAccDates':true">
                                                                <option value="">SeedAccountedDates</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p></p>
								
								<div class="row">
										<div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedAccountingForm.villageCode.$invalid && (SeedAccountingForm.villageCode.$dirty || Addsubmitted)}">																																																											
					        	                <div class="fg-line">
                						    		<select chosen class="w-100" name="villageCode" data-ng-model='caneaccounting.villageCode' data-ng-required='true' ng-options="villageCode.id as villageCode.village for villageCode in VillagesNamesData | orderBy:'-village':true">
														<option value="">Select Village</option>
				        		        		    </select>
					                	        </div>
	<p ng-show="SeedAccountingForm.villageCode.$error.required && (SeedAccountingForm.villageCode.$dirty || Addsubmitted)" class="help-block">Select Village</p>																								
</div>												
					                    	</div>		
										</div>
									</div>
									
  
  </div>



                                <div class="row" align="center">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="fg-line">

                                                <button type="button" class="btn btn-primary" ng-click="getSeedAccountingDetails(SeedAccountingForm);">Load Details</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div >




                        <!------table data start--------->


                    </form>
					
					<form name="SeedAccountingCalculation"  style=" width:95%; margin-left:-1%;   margin-top:-1%;" id="popup_box">
                    
                            <div style="height:100%; background-color:#FFFFFF;" >
                                <div style="height:400px; overflow-y:auto;">
                                


                                        <table style="width:100%; position:relative;"  class="spacing-table"  cellpadding="5"  cellspacing="10"  ng-repeat="headerData in SeedACCHeaderData" >

                                            <tr style="background-color: #0099FF;font-weight:bold;">
                                                <th  colspan="1" style=" color:#FFFFFF;">Ryot Code</th>
													
                                                <th   colspan="1" style=" color:#FFFFFF;">Ryot Name</th>
										 <th  colspan="1" style=" color:#FFFFFF;">Rate</th>
													
                                                <th   colspan="1" style=" color:#FFFFFF;">Total seed</th>
										
                                                <th  colspan="1" style=" color:#FFFFFF;">Accrued & Due Amount </th>
											
                                                <th  colspan="2"  style=" color:#FFFFFF;">Paying Amount</th>
														
                                                <th  colspan="2"  style=" color:#FFFFFF;">Advances &  Loans </th>
														
												<th  colspan="2" style=" color:#FFFFFF;">SB AC/ Amount</th>
                                               	
												

                                            </tr>
											
											<tr>
											
											<td colspan="1">
										
											
                                                   {{headerData.seedSupplierCode}}  <input type="hidden" name="seedSupplierCode{{$index}}" ng-model="headerData.seedSupplierCode" readonly />
                                             </td>
											<td colspan="1">
                                                   {{headerData.seedSupplierName}}  <input type="hidden" name="seedSupplierName{{$index}}" ng-model="headerData.seedSupplierName" readonly />
                                                </td>
												<td colspan="1">
                                           
                                            <input type="text" class="textbox" name="seedRate{{$index}}" ng-model="headerData.seedRate" ng-blur="changeTotalamount(headerData.seedRate,headerData.extentSize,$index);" />
                                        </td>
										<td colspan="1">
                                           
                                            <input type="text" class="textbox" name="extentSize{{$index}}" ng-model="headerData.extentSize" ng-blur="changeTotalamount(headerData.seedRate,headerData.extentSize,$index);" />
                                        </td>
											<td  colspan="1">
											
                                                   {{headerData.totalAmount}} <input type="hidden"  name="totalAmount{{$index}}" ng-model="headerData.totalAmount" readonly />
                                                </td>
												 <td colspan="2" >
                                            <input type="text" class="textbox" name="payingAmount{{$index}}" ng-model="headerData.payingAmount" ng-blur="ChangeSBAcc(headerData.payingAmount,$index)" />
                                        </td>
											<td colspan="2">
											
                                                   {{headerData.loansAndAdvancesPaid}} <input type="hidden" name="loansAndAdvancesPaid{{$index}}" ng-model="headerData.loansAndAdvancesPaid"  />
                                                </td>
											<td colspan="2">
											
                                                   {{headerData.paidToSBAC}} <input type="hidden" name="paidToSBAC{{$index}}" ng-model="headerData.paidToSBAC" readonly />
                                                </td>
										
											</tr>
											
                                            
											
                                            <tr style="background-color: #c0c5ce;font-weight:bold;">
                                               
												<th><span>Advance<br/> Code</span></th>
												<th><span>Advance</span></th>
                                                <th><span>Principle</span></th>
												 <th><span>Advance<br/>Date</span></th>
                                                <th><span>Repayment Date</span></th>
                                                <th colspan="1"><span>Interest Rate</span></th>
												<th colspan="1"><span>Interest Amount</span></th>
												<th colspan="2"><span>Net Payable</span></th>
												<th colspan="2"><span>Paid</span></th>

                                            </tr>
                                            <tr ng-repeat="gridData in headerData.advanceAndLoans" >
											
                                                <td>
                                                  {{gridData.advanceCode}}   <input type="hidden" name="advanceCode{{$index}}" ng-model="gridData.advanceCode" readonly />
                                                </td>
                                                <td>
                                                   {{gridData.advance}}  <input type="hidden" name="advance{{$index}}" ng-model="gridData.advance" readonly />
                                                </td>
                                                <td>
                                                   {{gridData.principle}}  <input type="hidden" name="principle{{$index}}" ng-model="gridData.principle" readonly />
                                                </td>
                                                <td>
                                                 {{gridData.issuingDate}}   <input type="text" class="hidden" name="issuingDate{{$index}}" ng-model="gridData.issuingDate"  readonly />
                                                </td>
                                                                                        <td>

                                            <div>
                                                <input type="text" class="form-control datepicker autofocus" name="repaymentDate{{headerData.seedActSlno}}{{$index}}" ng-model="gridData.repaymentDate" data-input-mask="{mask: '00-00-0000'}" ng-blur="ChangeDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" />
                                               
                                            </div>
                                        </td>
                                                  <td colspan="1">
                                            {{gridData.interestRate}}
                                            <input type="hidden" name="interestRate{{$index}}" ng-model="gridData.interestRate" readonly />
                                        </td>
                                                <td colspan="2">
                                            {{gridData.interestAmount}}
                                            <input type="hidden" name="interestAmount{{headerData.seedActSlno}}{{$index}}" ng-blur="ChangeDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" ng-model="gridData.interestAmount" readonly />
                                        </td>
                                                <td colspan="2">
                                            {{gridData.totalPayable}}
                                            <input type="hidden" name="totalPayable{{headerData.seedActSlno}}{{$index}}" ng-blur="ChangeDate(gridData.principle,gridData.interestRate,gridData.issuingDate,gridData.repaymentDate,headerData.seedActSlno,$index);" ng-model="gridData.totalPayable" readonly />
                                        </td>
                                               <td colspan="2">
                                            <input type="text" class="textbox" name="paidAmount{{$index}}" ng-model="gridData.paidAmount" />
                                        </td>
												
                                            </tr>

						
					<!--	<div class="row" align="center">
                                   
                                        <div class="input-group">
                                            <div class="fg-line">
                                              
                                            <button type="button"   class="btn btn-primary"	 ng-click="getSeedAccountingDetails(SeedAccountingForm);">Update</button>
                                            
											</div>
                                        </div>
                                   
									
                                </div>-->



                                        </table>
                                  
                                </div>
								<div style="text-align:center;display:none; background-color:#FFFFFF;" id="buttonshow">
						<button type="button"   class="btn btn-primary"	ng-disabled="" ng-click="SeedAccountingSave();">Update</button></div>
                            </div>
                       
                        <p></p>
                        <!--<button class="btn btn-primary previous" id="Third" ng-click="setNullArray();"><i class="zmdi zmdi-arrow-back"></i></button>-->
                        <%--   <button class="btn btn-primary next third" id="third" ng-disabled="ThirdTabDeduction.$invalid" ng-click="thirdTabDeductionsSave();"><i class="zmdi zmdi-arrow-forward"></i></button>					
					   <button type="button" class="btn btn-primary third" ng-click="thirdTabDeductionsSave();" ng-disabled="ThirdTabDeduction.$invalid">Update</button>--%>
                            <!--</div>	-->
                            <!--</div>		-->
                    </form>


                    <!----------end----------------------->

               
           </div> 
        </div>

        </div>

    </section>
</section>
<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>