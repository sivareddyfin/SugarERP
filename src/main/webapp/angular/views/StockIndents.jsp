	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });
		  $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GradingController" ng-init="addFormField();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Indents</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="gradingForm" novalidate ng-submit="SaveCaneWeighment(AddCaneWeight,gradingForm);">
							  <input type="hidden" name="shiftTime" ng-model="AddCaneWeight.shiftTime" />
							  <div class="row">
							 
								<div class="col-sm-6">
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.caneWeighmentDate.$invalid && (gradingForm.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="fromdate">From Date</label>
														<input type="text" class="form-control date" placeholder='From Date' id="fromdate" with-floating-label  name="caneWeighmentDate" ng-model="" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-change="getShiftId();"/>	
				        		        	        </div>
													<p ng-show="gradingForm.caneWeighmentDate.$error.required && (gradingForm.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.caneWeighmentDate.$invalid && (gradingForm.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100"><option value="">Select Departments</option></select>	
				        		        	        </div>
													<p ng-show="gradingForm.caneWeighmentDate.$error.required && (gradingForm.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.caneWeighmentDate.$invalid && (gradingForm.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="todate">To Date</label>
														<input type="text" class="form-control date" readonly="readonly" placeholder='To Date' id="todate" with-floating-label  name="caneWeighmentDate" ng-model="" tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-change="getShiftId();"/>	
				        		        	        </div>
													<p ng-show="gradingForm.caneWeighmentDate.$error.required && (gradingForm.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : gradingForm.caneWeighmentDate.$invalid && (gradingForm.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100"><option value="">Select Status</option></select>	
				        		        	        </div>
													<p ng-show="gradingForm.caneWeighmentDate.$error.required && (gradingForm.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								
							  </div>
						
							<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Actions</th>
											<th>Stock Indent No</th>
							                <th>Date</th>
		                    				<th>Items</th>
											<th>Department Name</th>
											<th>Indent Status</th>
											
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="chemicalTreatmentData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="chemicalTreatmentData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(chemicalTreatmentData.id);" ng-if="chemicalTreatmentData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.surety{{chemicalTreatmentData.id}}.$invalid && (gradingForm.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Stock Indent No." maxlength='8' name="surety{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.surety" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" />
<p ng-show="gradingForm.surety{{chemicalTreatmentData.id}}.$error.required && (gradingForm.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.surety{{chemicalTreatmentData.id}}.$error.pattern && (gradingForm.surety{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.contactNo{{chemicalTreatmentData.id}}.$invalid && (gradingForm.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1 " placeholder="Date" maxlength='10' name="contactNo{{chemicalTreatmentData.id}}" data-ng-model='chemicalTreatmentData.contactNo' data-ng-required='true' ng-minlength='10' data-ng-pattern='/^[789]\d{9,10}$/'/>
<p ng-show="gradingForm.contactNo{{chemicalTreatmentData.id}}.$error.required && (gradingForm.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.contactNo{{chemicalTreatmentData.id}}.$error.pattern && (gradingForm.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>												
<p ng-show="gradingForm.contactNo{{chemicalTreatmentData.id}}.$error.minlength && (gradingForm.contactNo{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																								
</div>												
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Items" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Department Name" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$invalid && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Indent Status" maxlength='14' name="aadhaarNumber{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}"  data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.required && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.pattern && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$error.minlength && (gradingForm.aadhaarNumber{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											
										
											
											
											</tr>
									</tbody>
							 	</table>
							</div>		
							  
							
								
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(gradingForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
