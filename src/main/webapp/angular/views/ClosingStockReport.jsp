	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="ClosingStockReport" >    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Closing Stock Report</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="ClosingStockReportForm" novalidate >

						
						 <div class="row">
							 
							

							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" >
											 	<div class="fg-line">
													 <select chosen class="w-100" name="department" tabindex="3"   data-ng-model="AddedDepartment.department"  tabindex="1">
														<option value=''>Department</option>
													 </select>	
												</div>
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"></span>
											 	<div class="fg-line">
												
												 <button type="Button"   name="getData"  class="btn btn-primary btn-sm m-t-10" ng-click="">GetData</button>	
												</div>																							 
										</div>
									</div>
								</div>									
							</div>
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive'>
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
							<thead>
								<tr class="styletr" style="font-weight:bold;" >
									<th>Product Group</th>
									<th>Product SubGroup</th>
									<th>Products Data </th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>Current Stock</th>
									<th>MRP</th>
									<th>MRP Value</th>
									<th>Cost Price</th>
									<th>Value</th>
									
								</tr>
							</thead>
							<tbody>
							<!--	<tr ng-repeat="dept in closingStockData"> -->
								<tr >	
									
								<!--	<td> {{dept.department}}</td> -->
									<td> 1</td>
									<td> 1</td>
									<td>1 </td>
									<td>1 </td>
									<td>Testing </td>
									<td>Empty</td>
									<td>50 </td>
									<td>50</td>
									<td>50 </td>
									<td>50</td>
							
									
								</tr>
								
								</tbody>
							</table>
							
							</div>
							
						 <br>
						 	<!--				
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(ClosingStockReportForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	-->			
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
