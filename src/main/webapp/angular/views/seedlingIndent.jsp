		<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
		<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="seedlingIndentController" ng-init="loadStoreCode();addFormField();loadVillageNames();loadseasonNames();loadKindType();loadFieldAssistants();onloadFunction();loadRyotCodeDropDown();loadVarietyNames();loadFieldOfficerDropdown();loadDate();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Common Indent</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	
						<form name="seedlingIndentForm"  novalidate >
							  
							  <div class="row">
							 
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : seedlingIndentForm.season.$invalid && (seedlingIndentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select  chosen class="w-100" ng-model="AddedSeedlingIndent.season" name="season" data-ng-required="true"  ng-options="season.season as season.season for season in seasonNames" ng-change="getaggrementDetails(AddedSeedlingIndent.season,AddedSeedlingIndent.ryotCode);"><option value="">Season</option></select>
															
				        		        	        </div>
													<p ng-show="seedlingIndentForm.season.$error.required && (seedlingIndentForm.season.$dirty || submitted)" class="help-block">Season is required.</p>													
												</div>
					                    	</div>
										</div>


										<div class="col-sm-6">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.fieldMan.$invalid && (seedlingIndentForm.fieldMan.$dirty || submitted)}">
						        	                <div class="fg-line">
													<select  chosen class="w-100" name="fieldMan" ng-model="AddedSeedlingIndent.fieldMan" tabindex="14" ng-required="true" ng-options="fieldMan.id as fieldMan.fieldassistant for fieldMan in FieldAssistantData | orderBy:'fieldassistant':true" >
													<option value="">Field Assistant</option>
												</select>
													</div>
													<p ng-show="seedlingIndentForm.fieldMan.$error.required && (seedlingIndentForm.fieldMan.$dirty || submitted)" class="help-block">Select Field Assistant</p>
													</div>
													
													</div>
										</div>


									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										
										<div class="col-sm-6">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.foCode.$invalid && (seedlingIndentForm.foCode.$dirty || submitted)}">
										 <div class="fg-line">
														 <select chosen class="w-100" name="foCode" data-ng-model='AddedSeedlingIndent.foCode' data-ng-required='true' ng-options="foCode.id as foCode.fieldOfficer for foCode in FieldOffNames  | orderBy:'-fieldOfficer':true" ng-change='getZonebyFieldOfficer(AddedSeedlingIndent.foCode);'>
														<option value="">Field Officer</option>
				        				            </select>
				    		            	        </div>
										 
											 
								<p ng-show="seedlingIndentForm.foCode.$error.required && (seedlingIndentForm.foCode.$dirty || Addsubmitted)" class="help-block">Select Field Officer</p>										
										   </div>
									   </div>
										
									</div>
									
										<div class='col-sm-6'>
												<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : seedlingIndentForm.zone.$invalid && (seedlingIndentForm.zone.$dirty || submitted)}">
						        	                <div class="fg-line">
													<select  chosen class="w-100" name='zone' ng-model="AddedSeedlingIndent.zone" data-ng-required="true"  ng-options="zone.id as zone.zone for zone in zoneNames"  ng-change="getIndentMax(AddedSeedlingIndent.season,AddedSeedlingIndent.zone);">
													<option value="">Zone</option>
												</select>
													</div>
														<p ng-show="seedlingIndentForm.zone.$error.required && (seedlingIndentForm.zone.$dirty || submitted)" class="help-block">Zone is required.</p>								
													</div>
													
													</div>
										</div>


										


									</div>
								</div>
							  </div>
							
							  <div class="row">
							  	<div class="col-sm-6">
									<div class="row">
									<div class="col-sm-6">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : seedlingIndentForm.indentNo.$invalid && (seedlingIndentForm.indentNo.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<input type="text" class="form-control autofocus" data-ng-required="true" ng-model="AddedSeedlingIndent.indentNo" name="indentNo" placeholder="Indent No." readonly tabindex="1" />
				                			        </div>
													<p ng-show="seedlingIndentForm.indentNo.$error.required && (seedlingIndentForm.indentNo.$dirty || submitted)" class="help-block">Indent No. is required</p>													
												</div>
					                    	</div>
									</div>
									
										<div class="col-sm-6">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.ryotCode.$invalid && (seedlingIndentForm.ryotCode.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="ryotCode" data-ng-model='AddedSeedlingIndent.ryotCode' name="ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeData | orderBy:'-id':true" ng-change="loadRyotDet(AddedSeedlingIndent.ryotCode);getaggrementDetails(AddedSeedlingIndent.season,AddedSeedlingIndent.ryotCode);" data-ng-required="true">
															<option value="">Ryot Code</option>
														</select>
				                			        </div>
													<p ng-show="seedlingIndentForm.ryotCode.$error.required && (seedlingIndentForm.ryotCode.$dirty || submitted)" class="help-block">Select RyotCode</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.ryotName.$invalid && (seedlingIndentForm.ryotName.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<input type="text" class="form-control" placeholder="Ryot Name" name="ryotName" data-ng-model='AddedSeedlingIndent.ryotName' name="ryotName" readonly tabindex="2">
															
				                			        </div>
													<p ng-show="seedlingIndentForm.ryotName.$error.required && (seedlingIndentForm.ryotName.$dirty || submitted)" class="help-block">RyotName is required</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							
							
							  
							  
							 <div class="row">
							 <div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.village.$invalid && (seedlingIndentForm.village.$dirty || submitted)}">
						        	                <div class="fg-line">
													
													<select  chosen class="w-100" name="village" ng-model="AddedSeedlingIndent.village" ng-options="village.id as village.village for village in villageNames" ng-required='true'><option value="">Village</option></select>
													</div>
													<p ng-show="seedlingIndentForm.village.$error.required && (seedlingIndentForm.village.$dirty || submitted)" class="help-block">Village is required</p>
													</div>
													
													</div>
									</div>
								
								</div>			
										</div>
										<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.fatherName.$invalid && (seedlingIndentForm.fatherName.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Father" ng-model="AddedSeedlingIndent.fatherName" readonly tabindex="3" />
													</div>
													<p ng-show="seedlingIndentForm.fatherName.$error.required && (seedlingIndentForm.fatherName.$dirty || submitted)" class="help-block">Father Name is required</p>
													</div>
													
													</div>
									</div>
								
								</div>			
										</div>
							</div>
							<div class="row">
							 <div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.aadhaarNumber.$invalid && (seedlingIndentForm.aadhaarNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Aadhaar Number" ng-model="AddedSeedlingIndent.aadhaarNumber" readonly tabindex="4" />
													</div>
													<p ng-show="seedlingIndentForm.aadhaarNumber.$error.required && (seedlingIndentForm.aadhaarNumber.$dirty || submitted)" class="help-block">Aadhaar Number is required</p>
													</div>
													
													</div>
									</div>
								
								</div>
								<div class="row">
									<div class="col-sm-12">
									
									</div>
								
								</div>
											
										</div>
										<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : seedlingIndentForm.phoneNumber.$invalid && (seedlingIndentForm.phoneNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Phone Number" ng-model="AddedSeedlingIndent.phoneNumber" readonly tabindex="5" />
													</div>
													<p ng-show="seedlingIndentForm.phoneNumber.$error.required && (seedlingIndentForm.phoneNumber.$dirty || submitted)" class="help-block">Phone Number is required</p>
													</div>
													
													</div>
									</div>
								
								</div>	
											
										</div>
							</div>			
									
							<div class="row">
								<div class="col-sm-2">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
												<label for="fromDate">Indented Date</label>
													<input type="text" class="form-control date" tabindex="6" name="fromDate " ng-required='true' data-ng-model="AddedSeedlingIndent.indentedDate" placeholder="Indented Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}" readonly id="fromDate" with-floating-label />
												</div>
											 </div>
										</div>
									</div>
								</div>															
								</div>
								
								
								  <div class="col-sm-4">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />
									 					 <label class="radio radio-inline m-r-20"><b>Is Authorization</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="purpose" value="0"   data-ng-model="AddedSeedlingIndent.purpose" >
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="purpose" value="1"   data-ng-model='AddedSeedlingIndent.purpose'>
			    				            			<i class="input-helper"></i>NO
					  		              				</label>
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
								
								<div class="row">
								<div class="col-sm-2">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
												
												<select chosen class="w-100" ng-model="AddedSeedlingIndent.agreementNo" name="agreementNo" ng-options="agreementNo.agreementnumber as agreementNo.agreementnumber for agreementNo in agreementnumberNames"  ng-change="getfofaseedlingQty(AddedSeedlingIndent.agreementNo,AddedSeedlingIndent.season);"  >
													<option value="">Agreement Number</option>
												</select>
												
												
												</div>
											 </div>
										</div>
									</div>
								</div>															
								</div>
								<div class="col-sm-2">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
												<label for="noOfAcr">No. of Acres</label>
												<input type="text" class="form-control" placeholder="No. of Acres" ng-model="AddedSeedlingIndent.noOfAcr" name="noOfAcr" readonly  id="noOfAcr" with-floating-label  />
												
												
												</div>
											 </div>
										</div>
									</div>
								</div>															
								</div>
									<div class="col-sm-2">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
												<label for="individualExtent">Individual Extent Acre</label>
												<input type="text" class="form-control" placeholder="Individual Extent Acre" ng-model="AddedSeedlingIndent.individualExtent" name="individualExtent" readonly  id="individualExtent" with-floating-label  />
												
												
												</div>
											 </div>
										</div>
									</div>
								</div>															
								</div>
								
							</div>
							
							
							<div class="row">
							<div ng-hide="AddedSeedlingIndent.purpose=='1'">
								<div class="col-sm-5">
								<div class="input-group">
									<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.Sauthorization.$invalid && (seedlingIndentForm.Sauthorization.$dirty || submitted)}">
										<div class="fg-line">
										 <select chosen class="w-100" name="Sauthorization" data-ng-model='AddedSeedlingIndent.storeCode' ng-options="Sauthorization.storeCode as Sauthorization.city for Sauthorization in storeCodeData  | orderBy:'-storeCode':true" >
										   <option value="">Store Code</option>
				        				 </select>
				    		            </div>										 
										<p ng-show="seedlingIndentForm.Sauthorization.$error.required && (seedlingIndentForm.Sauthorization.$dirty || Addsubmitted)" class="help-block">Select Authorization</p>										
									</div>
								</div>
										
								</div>
									  </div>
								
								
							</div>		
										
							 </div>
							
							  	
									
													 					 	
							 	
								<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Kind</th>
											<th>Date of Delivery</th>
											<th>Quantity</th>
							                <th>UOM</th>
		                    				<th>Village</th>
											<th>Vareity</th>
											
										
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data"  ng-if="batchData.status!='3'">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"  ></i></button>
																								
											</td>
											<td style="display:none;">
											<input type ="hidden" ng-model="batchData.id" name="batchID{{batchData.id}}" />
											</td>
											<td  style="display:none;"><input type="hidden" ng-model="batchData.status" name="status{{batchData.id}}" /></td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.kind{{batchData.id}}.$invalid && (seedlingIndentForm.kind{{batchData.id}}.$dirty || submitted)}">
																																																																																																																							
												<select class="form-control1" name="kind{{batchData.id}}" data-ng-model='batchData.kind' ng-options ="kind.id as kind.KindType for kind in kindType" ng-change ="getuomdetails(batchData.kind,batchData.id);duplicateProductCode(batchData.kind,$index);" ng-required="true">
														<option value="" > Kind Type</option>	
														</select>
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.required && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.pattern && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td style="border:none;"><div class="form-group"><input type="text" class="form-control1 datepicker" ng-model="batchData.batchDate" name="batchDate{{batchData.id}}" placeholder="Date" ng-mouseover="ShowDatePicker();" tabindex="6" /></div></td>    
											<td ng-if="batchData.status!='3'">
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.quantity{{batchData.id}}.$invalid && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Quantity"  name="quantity{{batchData.id}}" ng-model="batchData.quantity" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" maxlength="10" tabindex="7"  />
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.required && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.pattern && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td ng-if="batchData.status=='3'">
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.quantity{{batchData.id}}.$invalid && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Quantity"  name="quantity{{batchData.id}}" ng-model="batchData.quantity" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" maxlength="10" tabindex="7" style="background-color:#FFFF00;" ng-readonly="batchData.status=='3'"  />
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.required && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.pattern && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.uom{{batchData.id}}.$invalid && (seedlingIndentForm.uom{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="UOM"  name="uom{{batchData.id}}" ng-model="batchData.uom" data-ng-required='true' readonly tabindex="8" />
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.required && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.pattern && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.landVillageCode{{batchData.id}}.$invalid && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<select class="form-control1" name="landVillageCode{{batchData.id}}" ng-model="batchData.landVillageCode" tabindex="14" ng-required="true" ng-options="landVillageCode.id as landVillageCode.village for landVillageCode in villageNames | orderBy:'-village':true">
													<option value="">Village Name</option>
												</select>
<p ng-show="seedlingIndentForm.landVillageCode{{batchData.id}}.$error.required && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.variety{{batchData.id}}.$invalid && (seedlingIndentForm.variety{{batchData.id}}.$dirty || submitted)}">																																																																																																																																		
												<select class="form-control1" name="variety{{batchData.id}}" ng-model="batchData.variety" tabindex="14" ng-required="true" ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true">
													<option value="">Variety</option>
												</select>
<p ng-show="seedlingIndentForm.variety{{batchData.id}}.$error.required && (seedlingIndentForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											
											
											</tr>
										<tr ng-repeat="batchData in data" ng-if="batchData.status=='3'" style="background-color: #99CCCC;">
											<td>
												<!--<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>-->
												<!--<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"  ></i></button>-->
																								
											</td>
											<td style="display:none;">
											<input type ="hidden" ng-model="batchData.id" name="batchID{{batchData.id}}" />
											</td>
											<td  style="display:none;"><input type="hidden" ng-model="batchData.status" name="status{{batchData.id}}" /></td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.kind{{batchData.id}}.$invalid && (seedlingIndentForm.kind{{batchData.id}}.$dirty || submitted)}">
																																																																																																																							
												<select class="form-control1" name="kind{{batchData.id}}" data-ng-model='batchData.kind' ng-options ="kind.id as kind.KindType for kind in kindType" ng-change ="getuomdetails(batchData.kind,batchData.id)" ng-required="true">
														<option value="" > Kind Type</option>	
														</select>
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.required && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.kind{{batchData.id}}.$error.pattern && (seedlingIndentForm.kind{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td style="border:none;"><div class="form-group"><input type="text" class="form-control1 datepicker" ng-model="batchData.batchDate" name="batchDate{{batchData.id}}" placeholder="Date" ng-mouseover="ShowDatePicker();" tabindex="6" /></div></td>    
											<td ng-if="batchData.status!='3'">
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.quantity{{batchData.id}}.$invalid && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Quantity"  name="quantity{{batchData.id}}" ng-model="batchData.quantity" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" maxlength="10" tabindex="7"  />
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.required && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.pattern && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td ng-if="batchData.status=='3'">
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.quantity{{batchData.id}}.$invalid && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Quantity"  name="quantity{{batchData.id}}" ng-model="batchData.quantity" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" maxlength="10" tabindex="7" style="background-color:#99CCCC;" ng-readonly="batchData.status=='3'"  />
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.required && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.quantity{{batchData.id}}.$error.pattern && (seedlingIndentForm.quantity{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.uom{{batchData.id}}.$invalid && (seedlingIndentForm.uom{{batchData.id}}.$dirty || submitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="UOM"  name="uom{{batchData.id}}" ng-model="batchData.uom" data-ng-required='true' readonly tabindex="8" />
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.required && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="seedlingIndentForm.uom{{batchData.id}}.$error.pattern && (seedlingIndentForm.uom{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												

												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.landVillageCode{{batchData.id}}.$invalid && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<select class="form-control1" name="village{{batchData.id}}" ng-model="batchData.landVillageCode" tabindex="14" ng-required="true" ng-options="landVillageCode.id as landVillageCode.village for landVillageCode in villageNames | orderBy:'-village':true">
													<option value="">Village Name</option>
												</select>
<p ng-show="seedlingIndentForm.landVillageCode{{batchData.id}}.$error.required && (seedlingIndentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : seedlingIndentForm.variety{{batchData.id}}.$invalid && (seedlingIndentForm.variety{{batchData.id}}.$dirty || submitted)}">																																																																																																																																		
												<select class="form-control1" name="variety{{batchData.id}}" ng-model="batchData.variety" tabindex="14" ng-required="true" ng-options="variety.id as variety.variety for variety in varietyNames | orderBy:'-variety':true">
													<option value="">Variety</option>
												</select>
<p ng-show="seedlingIndentForm.variety{{batchData.id}}.$error.required && (seedlingIndentForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											
											
											</tr>	
									</tbody>
							 	</table>
							</div>
								
										
							  
							
							
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide " ng-click="seedlingIndentFormSubmit(AddedSeedlingIndent,seedlingIndentForm);">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(seedlingIndentForm)">Reset</button>
										<br />
										<br />
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
