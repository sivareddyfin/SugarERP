	<style type="text/css">
/*@media print {*/
    .footer {page-break-after: always;}
/*}*/

/*.row:nth-child(3n){
page-break-after: always;
color:#FF0000;
}*/

#start {
/*  margin-bottom:8px;*/
}

.col1 {
  /*margin-bottom: 4px;*/
}

.col-inside {
  border: solid 1px #000000;

}
.col1:first-child {
  padding: 0 5px 5px 0;

}
.col1:nth-child(2) {
  padding: 0 5px 5px;
}

table.test
{
    border-collapse: separate;
    border-spacing: 2px;
}

table.test td
{
     
    padding: 0 0 0 10px;
  /*  border: 1px  #ccc;*/
}
</style><script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 //$('.date').focus();	 		 
		 //$('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>			

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="specialPermitController" ng-init="loadSeasonNames();loadRyotNames();loadAgreementNumber();loadRyotExtentNames();loadServerDate();getNewPermitRules();getPermitRules();">       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Additional Permit</b></h2></div>
			    <div class="card"> 
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="specialPermitForm" novalidate>
					  <input type='hidden' ng-model="AddAgreement.maxNumber">
					
						<input type="hidden" name="permitType" ng-model="permitType" />
						<input type="hidden" name="noofpermitperacre" ng-model="noofpermitperacre"  />
					  	 <div class="row">
							 <div class="col-sm-6">
									<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
											<div class="form-group" ng-class="{ 'has-error' : specialPermitForm.seasonYear.$invalid && (specialPermitForm.seasonYear.$dirty || submitted)}">																					
												<div class="fg-line">
													<select chosen class="w-100" ng-options="seasonCode.season as seasonCode.season for seasonCode in seasonNames | orderBy:'-season':true" name="seasonYear" ng-model="AddAgreement.seasonYear" ng-required="true"  ng-change='setAddAgreement(AddAgreement.seasonYear);loadProgramBySeason(AddAgreement.seasonYear);loadPermitMaxNumber(AddAgreement.seasonYear)' >
														<option value="">Select Season</option>
													</select>
												</div>
					<p ng-show="specialPermitForm.seasonYear.$error.required && (specialPermitForm.seasonYear.$dirty || submitted)" class="help-block">Season is required.</p>
											</div>
											</div>
										</div>	
										</div>
										</div>	
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						<div class="form-group" ng-class="{ 'has-error' : specialPermitForm.ryotCode.$invalid && (specialPermitForm.ryotCode.$dirty || submitted)}">											
													<div class="fg-line">
														<select chosen class="w-100"  tabindex="2" name="ryotCode" ng-model="AddAgreement.ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotNames | orderBy:'-id':true"  ng-required="true" ng-change="loadTyotDetails(AddAgreement.ryotCode);loadRyotDet(AddAgreement.ryotCode)">
															<option value="">Select Ryot Code</option>
														</select>
													</div>
					<p ng-show="specialPermitForm.ryotCode.$error.required && (specialPermitForm.ryotCode.$dirty || submitted)" class="help-block">Select Ryot Code</p>												
					</div>												
												</div>
											</div>
										</div>
										
									</div>
									
									<div class="row">
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
													  <label for="permitDate">Permit Date</label>
													  
													  <input type="text" class="form-control" placeholder="Permit Date"  readonly maxlength="50" name="permitDate" ng-model="AddAgreement.permitDate" id="permitDate" with-floating-label/>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
													  <label for="harvestingDate">Harvesting Date</label>
													  <input type="text" class="form-control date" placeholder="Harvesting Date"  readonly maxlength="50" name="harvestingDate" ng-model="AddAgreement.harvestingDate" id="harvestingDate" with-floating-label/>
													</div>
												</div>
											</div>
										</div>
										
									</div>	                    
				                  </div>
						     <div class="col-sm-6">																
									<div class="row">
										<div class="col-sm-6">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
				        	                <div class="fg-line">
											  	<select chosen class="w-100" name="agreementAddNumber" ng-model="AddAgreement.agreementAddNumber" ng-options="agreementAddNumber.agreementnumber as agreementAddNumber.agreementnumber for agreementAddNumber in AddedAgreements" ng-change="getAllAddedDetails(AddAgreement.seasonYear,AddAgreement.agreementAddNumber);">
													<option value="">Agreements</option>
												</select>
				                	        </div>
										</div>
			                    	</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : specialPermitForm.programno.$invalid && (specialPermitForm.programno.$dirty || AddSubmitted)}">
						        	                <div class="fg-line">
    	    		    					          <div class="select">
        	        						    		<select chosen class="w-100" name="programno" ng-model="AddAgreement.programno" ng-options="programno.programnumber as programno.programnumber for programno in GeneratePermitProgramNames | orderBy:'-programnumber':true" ng-required="true">
															<option value="">Program Number</option>
							        		            </select>
        		    	    	            		  </div>
			    			            	        </div>
													<p ng-show="specialPermitForm.programno.$error.required && (specialPermitForm.programno.$dirty || AddSubmitted)" class="help-block">Program is Required.</p>
												</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
													  <label for="RyotName">Ryot Name</label>
													  <input type="text" class="form-control" placeholder="Ryot Name"  readonly maxlength="50" name="ryotName" ng-model="AddAgreement.ryotName" id="RyotName" with-floating-label/>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									
									
									<div class="row">
										
										<div class="col-sm-12">
											<div class="input-group">
												<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : specialPermitForm.remarks.$invalid && (specialPermitForm.remarks.$dirty || AddSubmitted)}">
												<div class="form-group floating-label-wrapper">
													<div class="fg-line">
													  <label for="remarks">Remarks</label>
													  <input type="text" class="form-control" placeholder="Remarks" name="remarks" ng-model="AddAgreement.remarks" id="remarks" with-floating-label/ data-ng-required="true">
													</div>
												</div>
												<p ng-show="specialPermitForm.remarks.$error.required && (specialPermitForm.remarks.$dirty || AddSubmitted)" class="help-block">Remarks is Required.</p>
												</div>
											</div>
										</div>
									</div>
								</div> 
						</div>							 				  
					  		
							
							<div class="row">
										<div class="col-sm-4">
										
											<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Permit Selection :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" data-ng-model='AddAgreement.permitSelection' ng-change="changePermitSelection(AddAgreement.maxNumber,AddAgreement.permitSelection);">
			    			            	<i class="input-helper"></i>Single
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"  data-ng-model='AddAgreement.permitSelection'  ng-change="changePermitSelection(AddAgreement.maxNumber,AddAgreement.permitSelection);">
			    				            <i class="input-helper"></i>All
					  		              </label>
			                	        </div>
			                    	</div>
										</div>
									</div>
							
						 <hr />	
						<div align='center'> <button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="generateSpecialPermit(AddAgreement.maxNumber,AddAgreement.permitSelection);">Generate Permit Numbers</button></div>
						 		<tabset justified="true">
			                
							<tab  heading="">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															
															<th><span>Remove</span></th>
															<th><span>Plot</span></th>
															<th><span>Extent Size</span></th>
															<th><span>Permit</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="PermtiData in SpecialPermitData">
														
											
												<td>
												
												<!--<button id="shwButton"  type="button" id="showButton"  id="right_All_1" class="btn btn-primary" ng-click="removeRow($index);" ><i class="glyphicon glyphicon-remove-sign"></i></button>-->
												
												
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow($index,AddAgreement.maxNumber,AddAgreement.permitSelection);" ><i class="glyphicon glyphicon-remove-sign"></i></button>	
												</td>	
							    	    			
														
											<td>
												{{PermtiData.plotNumber}}
												<input type="hidden" name="plotNumber{{$index}}" ng-model="PermtiData.plotNumber" />
											</td>
											
																						
											<td>
												{{PermtiData.extentSize}}
												<input type="hidden" name="extentSize{{$index}}" ng-model="PermtiData.extentSize" />
											</td>
											
											
											<td>
											{{PermtiData.permitNumber}}
												<div class="input-group input-group-unstyled">
													<div class="form-group" ng-class="{ 'has-error' : specialPermitForm.permitNumber{{$index}}.$invalid && (specialPermitForm.permitNumber{{$index}}.$dirty || submitted)}">
	                   									<input type="hidden" class="form-control1" placeholder='Permit Number' name="permitNumber{{PermtiData.id}}" ng-model="PermtiData.permitNumber" ng-required="true" readonly/ tooltip-placement="bottom"  uib-tooltip="{{PermtiData.permitNumber}}" tooltip-trigger="mouseenter" tooltip-enable="PermtiData.permitNumber" style="width:100%;">
														<p ng-show="specialPermitForm.permitNumber{{$index}}.$error.required && (specialPermitForm.permitNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
													</div>
								    	    	</div>												
											</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
							</tabset>	  	
						 
						<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
								
									<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="specialPermitFormSubmit(AddAgreement.seasonYear,AddAgreement.ryotCode,AddAgreement.permitDate,AddAgreement.agreementAddNumber,AddAgreement.programno,AddAgreement.ryotName,AddAgreement.harvestingDate,AddAgreement.remarks,specialPermitForm);">Save</button>
									
								</div>
							</div>						 	
					   </div>
						
					    
					</form>
					
							<div id="dispGeneratePrint" style="display:none;">
									
									<div ng-repeat="item in itemData"  class="row">
											
									<div class="col-xs-12 col1" >
							
							<div class="col-inside" style=" border: solid 1px #000000;">
								
								<table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
									<tr style="text-align:center; font-size:14px;"><td><strong>SRI SARVARAYA SUGARS LTD., CHELLURU</strong></td></tr>
									<tr style="text-align:center; font-size:12px;"><td><strong>CANE HARVESTING PERMIT</strong></td></tr>					
								</table>
								<hr style="border: solid 1px #333333;"/>
								<div class="row" style="font-family:Georgia, Helvetica, sans-serif; ">
								<div class="col-xs-4">
								<table width="" align="left" style="font-size:12px;">
								<tr  style="text-align:left" ><td><strong>&nbsp;&nbsp;PERMIT NO.</strong></td>
								<td>:</td>
								<td style="font-family:Calibry, Helvetica, sans-serif;"> &nbsp;&nbsp;{{item.permitnumber}}</td>
								</tr>
								<tr><td> <strong>&nbsp;&nbsp;HARVESTING DATE</strong></td>
								<td>:</td>
								<td style="font-family:Calibry, Helvetica, sans-serif;">&nbsp;&nbsp;{{item.harvestDate}}</td>
								</tr>
								<tr><td > <strong>&nbsp;&nbsp;CIRCLE</strong></td>
								<td>:</td>
								<td> &nbsp; {{item.circle}}</td>
									
									</tr>	
													
									
								</table>
								</div>
								<div class="col-xs-4"  valign='top'>
								<table width="" align="center" style="font-size:12px;">
								<tr  style="text-align:left"><td  ><strong>&nbsp;&nbsp;RYOT CODE</strong></td>
									<td>:</td>
									<td style="font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;"> &nbsp;&nbsp;{{item.ryotcode}}</td>
									</tr>
									<tr><td > <strong>&nbsp;&nbsp;VILLAGE</strong></td>
									<td>:</td>
									<td>&nbsp;&nbsp;  {{item.village[0].village}}</td>
								</tr>
								<tr><td > <strong>&nbsp;&nbsp;FATHER</strong></td>
								<td>:</td>
								<td>&nbsp;&nbsp; {{item.fathername}}</td>
								</tr>
								<tr ><td ><strong>&nbsp;&nbsp;RYOT</strong></td>
								<td>:</td>
								<td> &nbsp;&nbsp; {{item.ryotname}}</td>
								</tr>
								
								
								
								</table>
								</div>
							
								<div class="col-xs-4"  rowspan="4"   valign='top' >
								
								
								
								
								<img alt="Embedded Image" data-ng-src="data:image/png;base64,{{item.qrcodedata}}"  align="right" height="60"  style="margin-top:-75px; margin-right:10px;" />
								
								
								</div>
								</div>
									
									
									
									<div align="center">
										
									<table border="1" style="width:100%; border-collapse:collapse; margin-top:1%; font-family:Georgia, Helvetica, sans-serif; ">
										<tr>
											<td style="width:25%;border-left:none;">Plant/Ratoon</td>
											<td style="width:25%;"> &nbsp;{{item.plantorratoon}}</td>
											<td style="width:20%;">Admn.No.</td>
											<td style="width:30%;">&nbsp;  </td>                            
										</tr>
										<tr>
											<td style="width:25%;border-left:none;">Variety</td>
											<td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.variety}}</td>
											<td style="width:25%;">Time</td>
											<td style="width:25%;">&nbsp;</td>                            
										</tr>
										<tr>
											<td style="width:25%;border-left:none;">Circle Code</td>
											<td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.circlecode}}</td>
											<td style="width:25%;">Shift</td>
											<td style="width:25%;"><input type="checkbox" >A <input type="checkbox" > B <input type="checkbox" > C</td>                            
										</tr>
										<tr>
											<td style="width:25%;border-left:none;">Land Village Code</td>
											<td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">&nbsp;{{item.landvilcode}}</td>
											<td style="width:25%;">Vehicle Regn.</td>
											<td style="width:25%;">&nbsp;</td>                            
										</tr>
										<tr>
											<td style="width:25%;border-left:none;">Program & Rank No.</td>
											<td style="width:25%;font-family:Calibry, Helvetica, sans-serif; font-weight:lighter;">{{item.programno}} & {{item.rank}} </td>
											<td style="width:25%;">Vehicle No.</td>
											<td  style="width:25%;">&nbsp;</td>                            
										</tr>
										<tr>
											<td style="width:25%;border-left:none;">Vehicle Type</td>
											<td colspan="3"  style="" ><input type="checkbox" >Tractor <input type="checkbox" >Small Lorry <input type="checkbox" >Big Lorry</td>
										</tr>
										
									</table>
									</div>
									
								<table width="100%"  class="test" style="font-size:14px;font-weight:bold;font-family:Georgia, Helvetica, sans-serif; ">
								<tr  style="text-align:left; width:25%;"><td></td>
									<td style="text-align:left; width:25%;"></td>
									<td  style="text-align:right; width:25%;"></td>
									<td  style="text-align:right; width:25%;"><img src="img/images/Cane Manager Signature.png" style="width:40%; height:30px; margin-right:50px;" /></td>			
									
									</tr>
									<tr  style="text-align:left; width:25%;"><td>En.Clerk</td>
									<td style="text-align:left; width:25%;">W.B.Operator</td>
									<td  style="text-align:right; width:25%;">Agrl.Officer</td>
									<td  style="text-align:right; width:25%;">CANE MANAGER</td>			
									
									</tr>
									
									
																		
								</table>
								</div>
							</div>
							
						
							
							
					<div  style="page-break-after: always;" ng-if="$index%1=='0'"></div>	
						
						</div>
								
								</div>
							<button type="button" ng-click="printGeneratePermit();" style="display:none;"  >Print</button>
										  							 		 							 
						 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<!----------end----------------------->										
					
				</div> 
	    </section>   
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
