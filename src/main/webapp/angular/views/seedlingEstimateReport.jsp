

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="SeedlingEstimateController" ng-init="loadSeasonNames();loadCircleNames();loadFormData();">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Estimate Report</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="SeedlingEstimate">
					 		<div class="row">
										
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.season' name="season" ng-options="season.season as season.season for season in SeasonData">
															<option value="">Select Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										  <div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.circle.$invalid && (SeedlingEstimate.circle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.circle' name="circle" ng-options="circle.id as circle.circle for circle in circleNames">
															<option value="">Select Field Officer</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.circle.$error.required && (SeedlingEstimate.circle.$dirty || Filtersubmitted)" class="help-block">Select Circle</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.season.$invalid && (SeedlingEstimate.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.season' name="season" ng-options="season.season as season.season for season in SeasonData">
															<option value="">Select Variety</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.season.$error.required && (SeedlingEstimate.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : SeedlingEstimate.circle.$invalid && (SeedlingEstimate.circle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedSeedlingEstimateQty.circle' name="circle" ng-options="circle.id as circle.circle for circle in circleNames">
															<option value="">Delivered By</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="SeedlingEstimate.circle.$error.required && (SeedlingEstimate.circle.$dirty || Filtersubmitted)" class="help-block">Select Circle</p>		 
											  </div>
			                    	      </div>
										</div>										
							</div>
							
							
							
							
					</form>		
							
							<br /><hr />	
											 		
						 
							<input type="hidden"  data-ng-model="flag" />
							<!----------table grid design--------->
					
											
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
										<form name="SeedlingEstimateForm" novalidate >				
							        <table ng-table="SeedlingEstimate.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>Sl. No</span></th>
												
													<th><span>Field Officer</span></th>
													<th><span>Variety</span></th>
														<th><span>Delivered By</span></th>
													
												</tr>
												<br>
											</thead>
										<tbody>
								        <tr ng-repeat="UpdateEstimate in EstimateQtyData"  ng-class="{ 'active': UpdateEstimate.$edit }">
                		    				<td>
                		    					{{UpdateEstimate.ryotCode}}
					            		      </td>
		                    				  
							                  <td>
							                     {{UpdateEstimate.ryotName}}
							                  </td>
											  <td>
							                     {{UpdateEstimate.fghName}}
							                  </td>
											  <td>
							                     {{UpdateEstimate.agreementNumber}}
							                  </td>
											  
											  											  
							             </tr>
										 </tbody>
								         </table>
										 </form>	
										 </div>
										</section>  
										<div align="center"><button type="button" class="btn btn-primary btn-hide" ng-click="saveEstimatedQtyDetails(SeedlingEstimateForm);">Save</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary btn-hide" ng-click="saveEstimatedQtyDetails(SeedlingEstimateForm);">Reset</button></div>						 
							     </div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
