	


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SaleInvoice" ng-init="addFormField();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Sale Invoice</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="SaleInvoiceForm" novalidate  ng-submit=" SaleInvoiceSubmit(AddedSaleInvoice,SaleInvoiceForm)">
						   <input type="hidden" ng-model="AddedSaleInvoice.modifyFlag" name="modifyFlag"  />


						 <div class="row">
							 <div class="col-sm-4">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.salesInvNo.$invalid && (SaleInvoiceForm.salesInvNo.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control " autofocus data-ng-required='true'  name="salesInvNo" data-ng-model="AddedSaleInvoice.salesInvNo" placeholder="Sales Inv No " tabindex="1"  maxlength="15" readonly="readonly" />
									</div>
								<p ng-show="SaleInvoiceForm.salesInvNo.$error.required && (SaleInvoiceForm.salesInvNo.$dirty || Addsubmitted)" class="help-block">Sales Invoice No is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
													<input type="text" class="form-control date" tabindex="2" name="invoiceDate " ng-required='true' data-ng-model="AddedSaleInvoice.invoiceDate" placeholder="Invoice Date " maxlength="10" data-input-mask="{mask: '00-00-0000'}"/>
												</div>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.dcNo.$invalid && (SaleInvoiceForm.dcNo.$dirty || submitted)}">
											 	<div class="fg-line">
													 <input type="text" class="form-control " tabindex="3" name="dcNo" ng-required='true' data-ng-model="AddedSaleInvoice.dcNo" placeholder="DC No." maxlength="10" readonly="readonly"/>	
												</div>
												<p ng-show="SaleInvoiceForm.dcNo.$error.required && (SaleInvoiceForm.dcNo.$dirty || Addsubmitted)" class="help-block">Delivery Challan No. is required</p>		 
											 </div>
										</div>
									</div>
								</div>															
							</div>
							
						 </div>
						 
						 
						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.customerName.$invalid && (SaleInvoiceForm.customerName.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="customerName" data-ng-model="AddedSaleInvoice.customerName" placeholder="Customer Name " tabindex="4"  maxlength="20" readonly="readonly" />
									</div>
								<p ng-show="SaleInvoiceForm.customerName.$error.required && (SaleInvoiceForm.customerName.$dirty || Addsubmitted)" class="help-block">Customer Name is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.address.$invalid && (SaleInvoiceForm.address.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="address" data-ng-model="AddedSaleInvoice.address" placeholder="Address " tabindex="5"  maxlength="50" readonly="readonly" />
									</div>
								<p ng-show="SaleInvoiceForm.address.$error.required && (SaleInvoiceForm.address.$dirty || Addsubmitted)" class="help-block"Address is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.contactNumber.$invalid && (SaleInvoiceForm.contactNumber.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control autofocus" data-ng-required='true'  name="contactNumber" data-ng-model="AddedSaleInvoice.contactNumber" placeholder="ContactNumber" tabindex="6"  maxlength="15" readonly="readonly" />
									</div>
								<p ng-show="SaleInvoiceForm.contactNumber.$error.required && (SaleInvoiceForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Contact Number is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : SaleInvoiceForm.department.$invalid && (SaleInvoiceForm.department.$dirty || submitted)}">												
									<div class="fg-line">
										<input type="text" class="form-control " data-ng-required='true'  name="department" data-ng-model="AddedSaleInvoice.department" placeholder="Department " tabindex="7"  maxlength="15" readonly="readonly" />
									</div>
								<p ng-show="SaleInvoiceForm.department.$error.required && (SaleInvoiceForm.department.$dirty || Addsubmitted)" class="help-block">Sales Invoice No is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div></div>
						
						 
						 <br />
							<div class="table-responsive">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>Action</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>UOM</th>
									<th>Batch</th>
									<th>Exp Date</th>
									<th>MRP</th>
									<th>Stockindate</th>
									<th>QTY</th>
									<th>Total Cost </th>
									
								</tr>
								<tr class="styletr"   ng-repeat="stockData in stockRequestData" >
									<td><button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();'ng-if="stockData.id=='1'" ><i class="glyphicon glyphicon-plus-sign"></i>
										<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(stockData.id);'ng-if="stockData.id!='1'" ><i class="glyphicon glyphicon-remove-sign"></i>
										</td>
									
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.productCode{{$index}}.$invalid && (SaleInvoiceForm.productCode{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Code" maxlength="20" name="productCode{{$index}}" ng-model="stockData.productCode" ng-required="true" >							
										<p ng-show="SaleInvoiceForm.productCode{{$index}}.$error.required && (SaleInvoiceForm.productCode{{$index}}.$dirty || submitted)" class="help-block">Product Code Required</p>
																		
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.productName{{$index}}.$invalid && (SaleInvoiceForm.productName{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Product Name" maxlength="20" name="productName{{$index}}" ng-model="stockData.productName" ng-required="true"  ng-pattern="/^[a-zA-Z0-9?=.*!@#$%^&*_\-\s]+$/">							
										<p ng-show="SaleInvoiceForm.productName{{$index}}.$error.required && (SaleInvoiceForm.productName{{$index}}.$dirty || submitted)" class="help-block"> Product Name Required</p>	
										<p ng-show="SaleInvoiceForm.productName{{$index}}.$error.pattern && (SaleInvoiceForm.productName{{$index}}.$dirty || submitted)" class="help-block">Valid Product Name Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.uom{{$index}}.$invalid && (SaleInvoiceForm.uom{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="UOM" maxlength="10" name="uom{{$index}}" ng-model="stockData.uom" ng-required="true" ng-pattern="^[a-zA-Z0-9]+$">							
										<p ng-show="SaleInvoiceForm.uom{{$index}}.$error.required && (SaleInvoiceForm.uom{{$index}}.$dirty || submitted)" class="help-block"> UOM Required</p>							
										<p ng-show="SaleInvoiceForm.productName{{$index}}.$error.pattern && (SaleInvoiceForm.productName{{$index}}.$dirty || submitted)" class="help-block">Valid UOM Required</p>		
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.batch{{$index}}.$invalid && (SaleInvoiceForm.batch{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Batch" maxlength="10" name="batch{{$index}}" ng-model="stockData.batch" ng-pattern="/^[a-zA-Z0-9?=.*!@#$%^&*_\-\s]+$/">							
										<p ng-show="SaleInvoiceForm.batch{{$index}}.$error.pattern && (SaleInvoiceForm.batch{{$index}}.$dirty || submitted)" class="help-block">Valid Batch Required</p>									
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.expDate{{$index}}.$invalid && (SaleInvoiceForm.expDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Expiry Date" maxlength="10" name="expDate{{$index}}" ng-model="stockData.expDate" >							
																			
										</div>
									</td>
									
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.mrp{{$index}}.$invalid && (SaleInvoiceForm.mrp{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="MRP" maxlength="10" name="mrp{{$index}}" ng-model="mrp.rejectedQty"  >							
																		
										</div>
									</td>
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.stockInDate{{$index}}.$invalid && (SaleInvoiceForm.stockInDate{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="StockInDate" maxlength="10" name="stockInDate{{$index}}" ng-model="stockData.stockInDate">							
										
									</td>
									
									
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.qty{{$index}}.$invalid && (SaleInvoiceForm.qty{{$index}}.$dirty || submitted)}">
		     <input type="text" class="form-control1" placeholder="QTY" maxlength="110" name="qty{{$index}}" ng-model="stockData.qty" ng-required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" >							
										<p ng-show="SaleInvoiceForm.qty{{$index}}.$error.required && (SaleInvoiceForm.qty{{$index}}.$dirty || submitted)" class="help-block">Required</p>						
										<p ng-show ="SaleInvoiceForm.qty{{$index}}.$error.pattern && (SaleInvoiceForm.qty{{$index}}.$dirty||Addsubmitted) " class="help-block">    Valid QTY is Required</p>			
										</div>
									</td>
									
									
									<td >
										<div class="form-group" ng-class="{'has-error':SaleInvoiceForm.totalCost{{$index}}.$invalid && (SaleInvoiceForm.totalCost{{$index}}.$dirty || submitted)}">
										<input type="text" class="form-control1" placeholder="Total Cost" maxlength="110" name="totalCost{{$index}}" ng-model="stockData.totalCost" ng-required="true" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/">							
										<p ng-show="SaleInvoiceForm.totalCost{{$index}}.$error.required && (SaleInvoiceForm.totalCost{{$index}}.$dirty || submitted)" class="help-block">Required</p>				
										<p ng-show ="SaleInvoiceForm.totalCost{{$index}}.$error.pattern && (SaleInvoiceForm.totalCost{{$index}}.$dirty||Addsubmitted) " class="help-block">    Valid Cost is Required</p>					
										</div>
									</td>
									
									
								</tr>
								
								<tr class="styletr">
			<td colspan="9"></td>
			<td> 
			<div class="form-group" ng-class="{'has-error': SaleInvoiceForm.totalamount{{$index}}.$(invalid) && (SaleInvoiceForm.totalamount{{$index}}.$dirty || submitted)}"> 
			 <input type="text" class="form-control1" name="totalamount{{$index}}"  ng-model="stockData.totalamount" placeholder="Total Amount"  size="10"  maxlength="10" readonly="readonly" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-required="true" /> 
			 <p ng-show ="SaleInvoiceForm.totalamount{{$index}}.$error.required && (SaleInvoiceForm.totalamount{{$index}}.$dirty||Addsubmitted)" class="help-block">   Total Cost Required </p>
			   <p ng-show ="SaleInvoiceForm.totalamount{{$index}}.$error.pattern && (SaleInvoiceForm.totalamount{{$index}}.$dirty||Addsubmitted)" class="help-block">    Valid Cost is Required</p> 
			 </div></td>
			</tr>
							</table>
							
							</div>
							
						 <br>
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SaleInvoiceForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>
						
						
						
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
