	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="SaleDeliveryChallans" ng-init="addFormField();getAllDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Sale Delivery Challans</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="saleDeliveryChallanForm" novalidate >





						
						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : saleDeliveryChallanForm.fromDate.$invalid && (saleDeliveryChallanForm.fromDate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date "  data-ng-required='true'  name="fromDate" data-ng-model="AddedSalesDc.fromDate"  placeholder="From Date" tabindex="1"  maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="fromDate" with-floating-label />
									</div>
								<p ng-show="saleDeliveryChallanForm.fromDate.$error.required && (saleDeliveryChallanForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group">
											 	<div class="fg-line">
												<label for="toDate">To Date</label>
													<input type="text" class="form-control date" tabindex="5" name="todate" ng-required='true' data-ng-model="AddedSalesDc.todate" placeholder="Todate" maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="toDate" with-floating-label/>
												</div>
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							 
							 
							

							
							
								<div class="row">
									 <div class="col-sm-3">
									<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group "  ng-class="{ 'has-error' : saleDeliveryChallanForm.department.$invalid && (saleDeliveryChallanForm.department.$dirty || submitted)}">
										<div class="fg-line">
											<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedSalesDc.department' name="department" ng-options="department.deptCode as department.department for department in DepartmentsData">
																<option value="">Department</option>
															</select>
										</div>
										<p ng-show="saleDeliveryChallanForm.department.$error.required && (saleDeliveryChallanForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>	 
												  </div>
												  
												  </div>
								</div>
								
								<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton" ng-click='getSaleDcs(AddedSalesDc.fromDate,AddedSalesDc.todate,AddedSalesDc.department);'>Get Details</button>
											 	
												
											 
										</div>
									</div>
								</div>
											  </div>			                    
							
								
							
							</div>
								
								
								
								
								
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive' id='hidetable' style="display:none">
							<table  class="table table-stripped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>Action</th>
									<th>DC No.</th>
									<th>DC Date </th>
									<th>DC Items</th>
									<th>Department</th>
									<th>View</th>
									<th>Modify</th>
									<th>Sale Invoice</th>
									
								</tr>
								
								<tr class="styletr"   ng-repeat="stockData in sdcData">
									<td><button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();'ng-if="stockData.id=='1'" ><i class="glyphicon glyphicon-plus-sign"></i>
										<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(stockData.id);'ng-if="stockData.id!='1'" ><i class="glyphicon glyphicon-remove-sign"></i>
										</td>
									
									<td>
									<div class="form-group" ng-class="{'has-error':saleDeliveryChallanForm.sdcno{{$index}}.$invalid && (saleDeliveryChallanForm.sdcno{{$index}}.$dirty || submitted)}">
									<input type="text" readonly class="form-control1" placeholder="Dc No." maxlength="15" name="sdcno{{$index}}" ng-model="stockData.sdcno" ng-required="true">

									<p ng-show="saleDeliveryChallanForm.sdcno{{$index}}.$error.required && (saleDeliveryChallanForm.sdcno{{$index}}.$dirty || submitted)" class="help-block">Required</p>

									</div>
									</td>
									
									<td>
									<div class="form-group" ng-class="{'has-error':saleDeliveryChallanForm.sdcdate{{$index}}.$invalid && (saleDeliveryChallanForm.sdcdate{{$index}}.$dirty || submitted)}">
									<input type="text" readonly class="form-control1 datepicker" placeholder="Date of Issue" maxlength="10" name="sdcdate{{$index}}" ng-model="stockData.sdcdate" ng-required="true" ng-click="ShowDatePicker();">
									
									<p ng-show="saleDeliveryChallanForm.sdcdate{{$index}}.$error.required && (saleDeliveryChallanForm.sdcdate{{$index}}.$dirty || submitted)" class="help-block">Required</p>
									
									</div>
									</td>
									<td>
									<div class="form-group" ng-class="{'has-error':saleDeliveryChallanForm.dcItems{{$index}}.$invalid && (saleDeliveryChallanForm.dcItems{{$index}}.$dirty || submitted)}">
									<input type="text" readonly class="form-control1" placeholder="Issued Items" maxlength="20" name="dcItems{{$index}}" ng-model="stockData.dcItems" ng-required="true">									
									<p ng-show="saleDeliveryChallanForm.dcItems{{$index}}.$error.required && (saleDeliveryChallanForm.dcItems{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
									</div>
									</td>
									<td>
									<div class="form-group" ng-class="{'has-error':saleDeliveryChallanForm.departmentName{{$index}}.$invalid && (saleDeliveryChallanForm.departmentName{{$index}}.$dirty || submitted)}">
									<input type="text" readonly class="form-control1" placeholder="Department" maxlength="30" name="departmentName{{$index}}" ng-model="stockData.departmentName" ng-required="true">									
									<p ng-show="saleDeliveryChallanForm.departmentName{{$index}}.$error.required && (saleDeliveryChallanForm.departmentName{{$index}}.$dirty || submitted)" class="help-block">Required</p>									
									</div>
									</td>
									
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										
										<a  href="#/store/transaction/deliveryChallan"  >
											<span style="font-size:20px; " class="glyphicon glyphicon-eye-open" title="View" ng-click="updateToDc(stockData.sdcno);"></span>
											
										</a>
									</td>
									
									
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										<a  href="#/store/transaction/deliveryChallan"  >
										 <img src="../icons/edit.png" style="height:30px;" ng-click="updateToDc(stockData.sdcno);" />
										</a>
									</td>
									
									
									<td> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
										<a  href="#/store/transaction/deliveryChallan"  >
										 <img src="../icons/edit.png" style="height:30px;" ng-click="" />
										</a>
									</td>
									
									
									
								</tr>
								
								
							</table>
							
							</div>
							
									
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
