	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
</style>

	<script type="text/javascript">	
		$('#autofocus').focus();	
		//var data = [{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'},{did:'SBI',dname:'SBI1',ddesc:'VSEZ'}];
		//sessionStorage.setItem('data1',JSON.stringify(data));		
	</script>
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Post Season Payment Rate Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>					 		 					 		
					 		 <div class="row">
							 	<div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<div class="select">
												<select chosen class="w-100">
													<option value="0">Select Season</option>
												</select>
											</div>											
			                	        </div>
			                    	</div>			                    
				                  </div>
							 </div><br />
							 <div class="row">
							 	<div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Additional Cane Price" id="autofocus" maxlength="5" readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
							 </div><br />
							 <div class="row">							 					                		
								  <div class="col-sm-4">
				                    <div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Transport Allowances :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptionstrans" value="Yes" checked="checked" class="input_radiotrans">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptionstrans" value="No"  class="input_radiotrans">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>													  
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control hidetext" placeholder="No.of kms" id="input_noofkms" maxlength="3"/>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-4">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control hidetext" placeholder="Allowance/Ton/Km" id="input_allowance" maxlength="5"/>
			                	        </div>
			                    	</div>			                    
				                  </div>	
								  
    	        			 </div><br />
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/PostSeasonPaymentRate" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/PostSeasonPaymentRate" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!--<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Post Season Payment Rates</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Season'">
                		    					<span ng-if="!w.$edit">Season</span>
					    		                <div ng-if="w.$edit">
													<select class="form-control1">
														<option value="0">Select Season</option>
													</select>
												</div>
					            		      </td>
											 
							                 <td data-title="'Additional Price'">
                		    					<span ng-if="!w.$edit">1000</span>
					    		                <div ng-if="w.$edit"><input class="form-control1" type="text" ng-model="w.name" placeholder='Additional Price' maxlength="5"/></div>
					            		      </td>
		                    				  <td data-title="'Transport Allowances'">
							                     <span ng-if="!w.$edit">Yes</span>
							                     <div ng-if="w.$edit">
												 	<div class="fg-line" style="margin-top:5px;">
            					          				<label class="radio radio-inline m-r-20">
								            				<input type="radio" name="inlineRadioOptionsgridtrans" value="Yes" checked="checked" class="input_radiotransgrid">
			    			            					<i class="input-helper"></i>Yes
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			   					 <input type="radio" name="inlineRadioOptionsgridtrans" value="No"  class="input_radiotransgrid">
			    				            				 <i class="input-helper"></i>No
					  		             				 </label>
			                	        			</div>
												 </div>
							                  </td>
							                  <td data-title="'No.of Kms'">
							                      <span ng-if="!w.$edit">Yes</span>
        		            					  <div ng-if="w.$edit">
												  	<input class="form-control1 hidetextgrid" type="email" ng-model="w.country" placeholder='No.of Kms' id="input_kmsgrid" maxlength="3"/>
												  </div>
							                  </td>
							                  <td data-title="'Allowances'">
							                      <span ng-if="!w.$edit">Yes</span>
        		            					  <div ng-if="w.$edit">
												  	<input class="form-control1 hidetextgrid" type="email" ng-model="w.country" placeholder='Allowances' id='input_allwegrd' maxlength="5"/>
												  </div>
							                  </td>
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>	-->					
														
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
