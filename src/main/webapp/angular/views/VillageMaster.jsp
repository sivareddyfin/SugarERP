	<script type="text/javascript">		
		$('.autofocus').focus();			
	</script>
	<!--script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>-->
	
	
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="VillageMaster" ng-init='loadMandals();LoadAddedVillages();getInitialize();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Village Master</b></h2></div>
			    <div class="card">
				
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					  
					  <form name="VillageMasterForm" ng-submit='AddVillages(AddVillage,VillageMasterForm);' novalidate>
					  <input type="hidden" name="screenName" ng-model="AddVillage.screenName" />
					  <input type="text" name="start" id="start"  name="start" data-ng-model='AddVillage.start' style="display:none;"/>
										
					  	<div class="row">
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                	<div class="fg-line">
											<label for="villageCode">Village Code</label>
											
											
											
            					          		<input type="text" class="form-control" placeholder="Village Code" maxlength="4" readonly data-ng-model='AddVillage.villageCode' id="villageCode" with-floating-label>
				                	        </div>
				                    	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group" ng-class="{ 'has-error' : VillageMasterForm.mandalCode.$invalid && (VillageMasterForm.mandalCode.$dirty || Addsubmitted)}">										
			        	                <div class="fg-line">
                				    		<select chosen class="w-100" data-ng-required='true' data-ng-model='AddVillage.mandalCode' name="mandalCode" ng-options="mandalCode.id as mandalCode.mandal for mandalCode in MandalData  | orderBy:'-mandal':true" ng-change="setNewVillageCode(AddVillage.mandalCode);">
												<option value="">Select Mandal</option>
				        		            </select>											
			                	        </div>
						<p ng-show="VillageMasterForm.mandalCode.$error.required && (VillageMasterForm.mandalCode.$dirty || Addsubmitted)" class="help-block">Select Mandal</p>	
									</div>
			                    	</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : VillageMasterForm.villageName.$invalid && (VillageMasterForm.villageName.$dirty || Addsubmitted)}">											
				        	                <div class="fg-line">
											<label for="villageName">Village Name</label>
    	        					          <input type="text" class="form-control autofocus" placeholder="Village Name" maxlength="25" data-ng-required='true' data-ng-model='AddVillage.villageName' name="villageName" data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtw('villageName');validateDup();calcRoute();" id="villageName" with-floating-label>
				                	        </div>
					<p ng-show="VillageMasterForm.villageName.$error.required && (VillageMasterForm.villageName.$dirty || Addsubmitted)" class="help-block">Village is required</p>												
					<p ng-show="VillageMasterForm.villageName.$error.pattern  && (VillageMasterForm.villageName.$dirty || Addsubmitted)" class="help-block">Enter a valid Village.</p>
					<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddVillage.villageName!=null">Village Name Already Exist.</p>		
										</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="Description(Optional)">Description(Optional)</label>
            					          <input type="text" class="form-control" placeholder="Description(Optional)" maxlength="50" data-ng-model='AddVillage.description' ng-blur="spacebtw('description');" id="Description(Optional)" with-floating-label>
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-6">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : VillageMasterForm.distance.$invalid && (VillageMasterForm.distance.$dirty || Addsubmitted)}">																						
				        	                <div class="fg-line">
											<label for="distance">Distance</label>
    	        					          <input type="text" class="form-control" placeholder="Distance (Km)" maxlength="9" data-ng-model='AddVillage.distance' name="distance"   ng-blur="spacebtw('distance');" id="distance" with-floating-label data-ng-pattern="/^[0-9.\s]*$/" >
				                	        </div>
					<p ng-show="VillageMasterForm.distance.$error.required && (VillageMasterForm.distance.$dirty || Addsubmitted)" class="help-block">Distance is required</p>
					<p ng-show="VillageMasterForm.distance.$error.pattern && (VillageMasterForm.distance.$dirty || Addsubmitted)" class="help-block">Enter Valid Distance.</p>												
																																		
				                    	</div>
									</div>
								</div>
								<div class="col-sm-6">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group" ng-class="{ 'has-error' : VillageMasterForm.pincode.$invalid && (VillageMasterForm.pincode.$dirty || Addsubmitted)}">																						
				        	                <div class="fg-line">
											<label for="pincode">pincode</label>
    	        					          <input type="text" class="form-control" placeholder="pincode" maxlength="9" data-ng-model='AddVillage.pincode' name="pincode"   ng-blur="spacebtw('pincode');" id="pincode" with-floating-label data-ng-pattern="/^[0-9.\s]*$/" >
				                	        </div>
					<p ng-show="VillageMasterForm.pincode.$error.required && (VillageMasterForm.pincode.$dirty || Addsubmitted)" class="help-block">pincode is required</p>
					<p ng-show="VillageMasterForm.pincode.$error.pattern && (VillageMasterForm.pincode.$dirty || Addsubmitted)" class="help-block">Enter Valid pincode.</p>												
																																		
				                    	</div>
									</div>
								</div>
								<!--<input type="button" value="Calculate Route" ng-click="calcRoute();"/>-->
								<div id="map_canvas" style="display:"></div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" checked="checked" data-ng-model='AddVillage.status'>
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1"  data-ng-model='AddVillage.status'>
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>
									</div>
								</div>
							</div>							
						</div>
						<input type="hidden"  name="modifyFlag" data-ng-model="AddVillage.modifyFlag"  />
						<div class="row">								  
							  <div class="col-sm-12" align="center">
				               <div class="input-group">
								  <div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(VillageMasterForm)">Reset</button>
								  </div>
								</div>			                    
				              </div>
						      <div class="col-sm-4"></div>								
    	        		</div>						
					  </form>
					 
				        </div>
			    	</div>
					</div>
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Villages</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">								
								  <form name="UpdateVillageMaster" novalidate>					
							        <table ng-table="VillageMaster.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
											<thead>
													<tr>
														<th><span>ACTIONS</span></th>
														<th><span>Village Code</span></th>
														<th><span>Village Name</span></th>
														<th><span>pincode</span></th>
														<th><span>Distance</span></th>
														<th><span>Mandal</span></th>
														<th><span>Description</span></th>
														<th><span>Status</span></th>
														</tr>
												</thead>
											<tbody>
								        <tr ng-repeat="UpdateVillage in AddedVillages"  ng-class="{ 'active': UpdateVillage.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateVillage.$edit" ng-click="UpdateVillage.$edit = true;UpdateVillage.modifyFlag='Yes';UpdateVillage.screenName='Village Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="UpdateVillage.$edit" ng-click="UpdateAddedVillages(UpdateVillage,$index);UpdateVillage.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="UpdateVillage.modifyFlag"  />
											   <input type="hidden" name="screenName{{$index}}" ng-model="UpdateVillage.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateVillage.$edit">{{UpdateVillage.villageCode}}</span>
					    		                <div ng-if="UpdateVillage.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="UpdateVillage.villageCode" placeholder='Village Code' maxlength="4" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!UpdateVillage.$edit">{{UpdateVillage.villageName}}</span>
							                     <div ng-if="UpdateVillage.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateVillageMaster.villageName{{$index}}.$invalid && (UpdateVillageMaster.villageName{{$index}}.$dirty || submitted)}">																							
												 	<input class="form-control" type="text" data-ng-model="UpdateVillage.villageName" placeholder='Village Name' maxlength="25" name="villageName{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtwgrid('villageName',$index);validateDuplicate(UpdateVillage.villageName,$index);"/>
<p ng-show="UpdateVillageMaster.villageName{{$index}}.$error.required && (UpdateVillageMaster.villageName{{$index}}.$dirty || submitted)" class="help-block">Village is required</p>												
<p ng-show="UpdateVillageMaster.villageName{{$index}}.$error.pattern  && (UpdateVillageMaster.villageName{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Village.</p>
<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="UpdateVillage.villageName!=null">Village Name Already Exist.</p>
</div>				                               																																										
												 </div>
							                  </td>
											  <td>
							                      <span ng-if="!UpdateVillage.$edit">{{UpdateVillage.pincode}}</span>
        		            					  <div ng-if="UpdateVillage.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateVillageMaster.pincode{{$index}}.$invalid && (UpdateVillageMaster.pincode{{$index}}.$dirty || submitted)}">																																	
												  	<input class="form-control" type="text" data-ng-model="UpdateVillage.pincode" placeholder='pincode' maxlength="9" name='pincode{{$index}}' data-ng-required='true' data-ng-pattern="/^[0-9.\s]*$/"/>
<p ng-show="UpdateVillageMaster.pincode{{$index}}.$error.required && (UpdateVillageMaster.pincode{{$index}}.$dirty || submitted)" class="help-block">Distance is required</p>	
<p ng-show="UpdateVillageMaster.pincode{{$index}}.$error.pattern && (UpdateVillageMaster.pincode{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>												
													
</div>													
												  </div>
							                  </td>
							                  <td>
							                      <span ng-if="!UpdateVillage.$edit">{{UpdateVillage.distance}}</span>
        		            					  <div ng-if="UpdateVillage.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateVillageMaster.distance{{$index}}.$invalid && (UpdateVillageMaster.distance{{$index}}.$dirty || submitted)}">																																	
												  	<input class="form-control" type="text" data-ng-model="UpdateVillage.distance" placeholder='Distance(Km)' maxlength="9" name='distance{{$index}}' data-ng-required='true' data-ng-pattern="/^[0-9.\s]*$/"/>
<p ng-show="UpdateVillageMaster.distance{{$index}}.$error.required && (UpdateVillageMaster.distance{{$index}}.$dirty || submitted)" class="help-block">Distance is required</p>	
<p ng-show="UpdateVillageMaster.distance{{$index}}.$error.pattern && (UpdateVillageMaster.distance{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>												
													
</div>													
												  </div>
							                  </td>
							                  <td style="width:180px;">
							                     <span ng-if="!UpdateVillage.$edit">{{UpdateVillage.mandal}}</span>
        		            					  <div ng-if="UpdateVillage.$edit">
												<div class="form-group">																																										  														<select class="form-control1"  data-ng-model='UpdateVillage.mandalCode' name="mandalCode" ng-options="mandalCode.id as mandalCode.mandal for mandalCode in MandalData  | orderBy:'-mandal':true" ng-change="setNewVillageCodeUpdate(UpdateVillage.mandalCode,$index);">
						        		            </select>
													
			<!--<p ng-show="UpdateVillageMaster.mandalCode{{$index}}.$error.required && (UpdateVillageMaster.mandalCode{{$index}}.$dirty || submitted)" class="help-block">Select Mandal</p>-->																									
														</div>													
												  </div>
							                  </td>											  
											  <td>
                		    					<span ng-if="!UpdateVillage.$edit">{{UpdateVillage.description}}</span>
					    		                <div ng-if="UpdateVillage.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="UpdateVillage.description" placeholder='Description' maxlength="50" ng-blur="spacebtwgrid('description',$index)"/>
													</div>
												</div>
					            		      </td>
											  <td>
							                      <span ng-if="!UpdateVillage.$edit">
												  	<span ng-if="UpdateVillage.status=='0'">Active</span>
													<span ng-if="UpdateVillage.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="UpdateVillage.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status{{$index}}" value="0"  ng-model="UpdateVillage.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status{{$index}}" value="1" ng-model="UpdateVillage.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>											  
							                  </td>
							             </tr>
										 </tbody>
								         </table>	
									 </form>
								</div>
								</section>			
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
