<script type="text/javascript">
    $(".autofocus").focus();
</script>

</script>

<style>
 table,th,td{
     text-align:center;
	 
	 }
	
	div{
	   color:#0099CC;
	   }
	   .numberCol
	   {
	      font-family:Calibry, Helvetica, sans-serif;
	   }
	#asdf {
  font-family: "Gothic 57";
  Gothic 57 Normal.ttf) format("truetype");
}
h1 { font-family: "Gothic 57", sans-serif } 
</style>

<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
	<section id="content" data-ng-controller="StocktransferprintController"  ng-init='updateTransferPrintData();'>       
        	<div class="container">				
    			<div class="block-header"><h2><b>Stock Transfer Print</b></h2></div>
			    <div class="card">
				 
			      <div class="card-body card-padding">
				  
				  <!--------Form Start----->
					 <!-------body start------>
					 
					 <div  id="stockTransferPrint">
					<!--<div id="stockTransferPrint" style="border:5px solid #0099CC;border-radius:5px; margin:5px; text-align:center; padding:5px;">-->
					  <div style="border:5px solid #0099CC;border-radius:5px; margin:5px; text-align:center; padding:5px;">   
					<div  style="font-family:Georgia, Helvetica, sans-serif; border-radius:5px ;  border-color:  #0099CC; border: 1px  #0099CC solid; margin:5px;">
					
			        <div>
					
					   <table width="100%"  style="font-family:Georgia, Helvetica, sans-serif;">
					<tr style="text-align:center; font-size:16px;"><td style="text-align:center; color:#0099CC"><h1 style="color:#0099CC;font-family: 'Gothic 57', sans-serif;">Sri Sarvaraya Sugars Limited</h1></td></tr><br/>
					<tr style="text-align:center; font-size:14px;"><td style="color:#0099CC"><strong>CHELLURU</strong></td></tr><br/>
					<tr style="text-align:center; font-size:14px;"><td style="color:#0099CC"><strong>STOCK TRANSFER ADVICE</strong></td></tr><br/>
					
			        </table>
					<br>
					
					
			
			         <table width="100%" style="color:#0099CC;"  border="0">
					 <tr><td style="text-align:left;"  >&nbsp;&nbsp;Delivery Challan No &nbsp;&nbsp;: <span  class="numberCol"style="font-size:13px;">&nbsp;&nbsp;</span> </td></tr>
					 <tr><td style="text-align:left;" colspan="3" >&nbsp;&nbsp;Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span  class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif; font-size:13px;">&nbsp;&nbsp;21-3-2017 </span>  </td></tr>
					 <br /><br />
					 <tr><td style="text-align:left;" colspan="3" >&nbsp;&nbsp;Sir, </td></tr>
					
					<tr><td colspan="3" style="text-align:left;font-size:11px; ">&nbsp;&nbsp;&nbsp;&nbsp; We are today transferring the following items as required by you , vide your Indent No.	&nbsp;&nbsp;dated <span  class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif;">&nbsp;&nbsp;21-3-2017 </span> &nbsp;&nbsp; to our Zonal Office at</td></tr>
					 
					 </table>
				     
				
				     <br/>
				
					<table style="width:100%; border-collapse:collapse;" border="1"  bordercolor="#0099CC"  >
					<thead>
				   <tr style="color:#0099CC" >
				   
				      <th style="font-size:14px;">S.No</th>  <th style="font-size:14px;"> Description of the item  </th>  <th style="font-size:14px;">Quantity</th> 
					</tr></thead>
				    <tr style="color:#0099CC; font-family:Calibry, Helvetica, sans-serif;" ng-repeat="stockTransferPrintdata in stockTransfer">
					<td class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif; text-align:center; font-size:13px;">{{$index+1}}</td>
					<td style="text-align:center;" style=" font-family:Calibry, Helvetica, sans-serif; font-size:13px;">{{stockTransferPrintdata.itemName}} </td> 
					<td style="text-align:center;" class="numberCol" style=" font-family:Calibry, Helvetica, sans-serif; font-size:13px;">{{stockTransferPrintdata.quantity}}</td>
				    </tr>
					
				</table>  
				
				    <br>
					
					<table width="100%" border="0" style="color:#0099CC; " >
						<tr><td style="text-align:left; width:100%;" colspan="3">Please acknowledge the receipt of  above items and take these into your stock.</td></tr>
					    <tr><td style="text-align:left; width:50%;">Thanking you</td><td></td><td></td></tr>
						<tr><td style="text-align:left; width:50%;">Yours Sincerely</td><td></td><td></td></tr>
						<tr><td style="text-align:left; width:50%;"> <br />Stores in-charge</td><td></td><td style="text-align:center; width:100%;"><br />Acknowledged by</td></tr>
					</table>	
					
				
			        </div>
					</div>
					</div>
					</div>
					 <div style="text-align:center;"><button class="btn btn-primary btn-sm m-t-10" ng-click="printStockTransfer();" >Print</button></div>
			         
					 
					 </div>
					
				</div>
			</div>
		</section>			 
	</section>	