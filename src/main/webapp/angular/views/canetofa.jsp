	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" ></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneToFinancialAccounting" ng-init="loadBranchNames();loadSeason();">     
        	<div class="container" >
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane To FinancialAccounting</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
				
					<!--------Form Start----->
					 <!-------body start------>	
					 <form method="POST" >
					 
					<div class="row">
						<div class="col-sm-12">
						   <div class="row">
						     <div class="col-sm-3">
                    			<div class="input-group">
            	             			<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										

                                      <div class="form-group">
                        	             <div class="fg-line">	
                                         <select chosen class="w-100" name="season"  data-ng-model="AddedSeason.season" ng-options="season.season as season.season for season in seasons  | orderBy:'-season':true" tabindex="1" ng-change="loadAccountingDates(AddedSeason.season);">
                                      <option value=''>Select Season</option>
                                     </select>	
                        	            </div>
                                     
						            </div>
						        </div>
						  </div>
						  
						  <div class="col-sm-3">
								<div class="input-group">
            				          <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									    <div  class="form-group  floating-label-wrapper">
				        	            	<div class="fg-line">
												<select chosen class="w-100" name="ftDate" ng-model="AddedSeason.ftDate" ng-options="ftDate.id as ftDate.foCode for ftDate in ftDates"  >
													<option value=''>Dates</option>
												</select>	
											</div>				
										</div>
			                    </div>
							</div>
						  
						  
						 
						  
						  <div class="col-sm-2">
                    			<div class="input-group">
            	             			
										

                                      <div class="form-group">
                        	             	
                                        <button type="button"  class="btn btn-primary btn-sm m-t-10" ng-click="getcanetoFa(AddedSeason.season,AddedSeason.ftDate,AddedSeason.branchCode);">Get Details</button>
											
                        	            
                                     
						            </div>
						        </div>
						  </div>
						  <div class="col-sm-3">
						  <div class="input-group">
								
                        	             	
                                        <button type='button' class='btn btn-primary' ng-click='exportcanetoFaexcel();'>Export Data</button>
											
                        	            
                                     
						            
						        </div>
								</div>
						  
						  
						  
						  
						  
						  
						  
						  
						  
						  
						
                
                    </div>
					
					</div>
					
					       
				    
				
						
			  
			  <br /> <br /> <br /><br />
			  
			  
			             
			              <!--<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit"  class="btn btn-primary btn-sm m-t-10" ng-click="asd();">Generate Report</button>
										
									</div>
								</div>
							</div>-->
							
							<div id='hidecanetofatable' style='display:none;'>
							<table style="width:100%;" border='1'  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;background-color:#454545;color:white;text-align:center;height:30px;">
											<th style='text-align:center;'>TCOMP</th>
											<th style='text-align:center;'>TDB</th>
							                <th style='text-align:center;'>TDOC</th>
											<th style='text-align:center;'>TSERIAL</th>
											<th style='text-align:center;'>TDT</th>
											<th style='text-align:center;'>TDES</th>
											<th style='text-align:center;'>TGEN</th>
											<th style='text-align:center;'>TSUB_CODE</th>
											<th style='text-align:center;'>TPARTY</th>
											<th style='text-align:center;'>TAMOUNT</th>
											<th style='text-align:center;'>TDC</th>
											<th style='text-align:center;'>TQTY</th>
											<th style='text-align:center;'>TUNIT_PRIC</th>
											<th style='text-align:center;'>TADB</th>
											<th style='text-align:center;'>TCON</th>
											<th style='text-align:center;'>TPROD</th>
											<th style='text-align:center;'>TREF_NO</th>
											
										
											
            							</tr>
										<tr class="styletr"  ng-repeat="canetofaData in canetofArray" style="height:50px;">
										
										
										<td style='text-align:center;'>{{canetofaData.TCOMP}}</td>
										<td style='text-align:center;'>{{canetofaData.TDB}}</td>
										<td style='text-align:center;'>{{canetofaData.TDOC}}</td>
										<td style='text-align:center;'>{{canetofaData.TSERIAL}}</td>
										<td style='text-align:center;'>{{canetofaData.TDT}}</td>
										<td style='text-align:left;'>{{canetofaData.TDES}}</td>
										
										<td style='text-align:center;'>{{canetofaData.TGEN}}</td>
										<td style='text-align:center;'>{{canetofaData.TSUB_CODE}}</td>
										<td style='text-align:center;'>{{canetofaData.TPARTY}}</td>
										<td style='text-align:center;'>{{canetofaData.TAMOUNT}}</td>
										<td style='text-align:center;'>{{canetofaData.TDC}}</td>
										<td style='text-align:center;'>{{canetofaData.TQTY}}</td>
										<td style='text-align:center;'>{{canetofaData.TUNIT_PRIC}}</td>
										<td style='text-align:center;'>{{canetofaData.TADB}}</td>
										<td style='text-align:center;'>{{canetofaData.TCON}}</td>
										<td style='text-align:center;'>{{canetofaData.TPROD}}</td>
										<td style='text-align:center;'>{{canetofaData.TREF_NO}}</td>
										
										
										
										
										</tr>
										
										
														
									</thead>
							</table>
							
							
							
							
							</div>
							
							
											</form>
										</div>
									</div>
								</div>
							</section>
						</section>
							
						
				</div>		 							 
					<!------------------form end---------->							 

			</div>
		</div>
	 </section> 
   </section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>



