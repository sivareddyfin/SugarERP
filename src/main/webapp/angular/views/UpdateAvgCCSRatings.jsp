	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateAvgCCSRating" ng-init="loadAvgCCSSeason();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Average CCS Ratings</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
						<form name="AvgCCSFilterForm" ng-submit="AvgCSSFilterSubmit(AvgCCSfilter,AvgCCSFilterForm);" novalidate>
							<div class="row">
								<div class="col-sm-2"></div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AvgCCSFilterForm.season.$invalid && (AvgCCSFilterForm.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
	        		    					          <div class="select">
													  	<select chosen class="w-100" name="season" ng-model="AvgCCSfilter.season" ng-options="season.season as season.season for season in AvgCCSSeasonNames | orderBy:'-season':true" ng-change="loadAvgCCSProgram(AvgCCSfilter.season);" ng-required="true">
															<option value="">Select Season</option>
														</select>
													  </div>
			    			            	        </div>
													<p ng-show="AvgCCSFilterForm.season.$error.required && (AvgCCSFilterForm.season.$dirty || Filtersubmitted)" class="help-block">Season is Required.</p>													
												 </div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="cols-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : AvgCCSFilterForm.program.$invalid && (AvgCCSFilterForm.program.$dirty || Filtersubmitted)}">	
						        	                <div class="fg-line">
        		    						          <div class="select">
													  	<select chosen class="w-100" name="program" ng-model="AvgCCSfilter.program" ng-options="program.programnumber as program.programnumber for program in AvgCCSProgramNames | orderBy:'-programnumber':true" data-ng-required="true">
															<option value="">Select Program</option>
														</select>
													  </div>
			    		            	    	    </div>
													<p ng-show="AvgCCSFilterForm.program.$error.required && (AvgCCSFilterForm.program.$dirty || Filtersubmitted)" class="help-block">Program is Required.</p>	
												</div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
			        	                		<div class="fg-line">
													<button type="submit" class="btn btn-primary btn-sm m-t-10">Get Details</button>
					                	        </div>
			    		                	</div>
										</div>
									</div>
								</div>
							</div>
						</form>					 		
						<hr />
						<form name="UpdateAvgCCSRatingForm" novalidate ng-submit="SaveCCSRatings(UpdateAvgCCSRatingForm);">	 							
							<div class="table-responsive">
							   <section class="asdok">
								  <div class="container1">								
									<table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th><span>Sample<br />Card</span></th>
												<th><span>Ryot Name</span></th>
												<th><span>Ryot Code</span></th>
												<th><span>Agreement<br />No.</span></th>
												<th><span>Plot No.</span></th>
												<th><span>Land Village</span></th>
												<th><span>Variety</span></th>
												<th><span>Plant<br />Ratoon</span></th>
												<th><span>CCS Rating</span></th>
												<th><span>Avg. CCS</span></th>
												<th><span>Avg CCS By FG</span></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="AvgSampleCard in AvgCCSSampleCards" style="font-size:12px;">
												<td>
													<div class="form-group">
														{{AvgSampleCard.samplecard}}<input type="hidden" name="samplecard" ng-model="AvgSampleCard.samplecard" />
													</div>
												</td>
												<td>
													<div class="form-group">													
														{{AvgSampleCard.ryotName}}<input type="hidden" name="ryotName" ng-model="AvgSampleCard.ryotName" />
													</div>
												</td>
												<td>
													<div class="from-group">
														{{AvgSampleCard.ryotCode}}<input type="hidden" name="ryotCode" ng-model="AvgSampleCard.ryotCode" />
													</div>
												</td>
												<td>
													<div class="form-group">													
														{{AvgSampleCard.agreementNumber}}<input type="hidden" name="agreementNumber" ng-model="AvgSampleCard.agreementNumber" />
													</div>
												</td>
												<td>
													<div class="form-group">
														{{AvgSampleCard.plotNumber}}<input type="hidden" name="plotNumber" ng-model="AvgSampleCard.plotNumber" />
													</div>
												</td>
												<td>
													<div class="form-group">
														{{AvgSampleCard.village}}<input type="hidden" name="village" ng-model="AvgSampleCard.village" />
													</div>
												</td>
												<td>
													<div class="form-group">
														{{AvgSampleCard.variety}}<input type="hidden" name="variety" ng-model="AvgSampleCard.variety" />
													</div>
												</td>
												<td>
													<div class="form-group">
														{{AvgSampleCard.plantOrRatoon}}<input type="hidden" name="plantOrRatoon" ng-model="AvgSampleCard.plantOrRatoon" />
													</div>
												</td>
												<td>
													<div class="form-group">
													<input type="text" class="form-control1" placeholder='CCS Rating' name="ccsRating" ng-model="AvgSampleCard.ccsRating" readonly uib-tooltip="{{AvgSampleCard.ccsRating}}" tooltip-trigger="mouseenter" tooltip-enable="AvgSampleCard.ccsRating" tooltip-placement="bottom"/>									
													</div>
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : UpdateAvgCCSRatingForm.avgCcsRating{{$index}}.$invalid && (UpdateAvgCCSRatingForm.avgCcsRating{{$index}}.$dirty || Mainsubmitted)}">
														<input type="text" class="form-control1" placeholder='Avg.CCS Rating' name="avgCcsRating{{$index}}" ng-model="AvgSampleCard.avgCcsRating" readonly ng-required="true" ng-if="display=='0'" uib-tooltip="{{AvgSampleCard.avgCcsRating}}" tooltip-trigger="mouseenter" tooltip-enable="AvgSampleCard.avgCcsRating" tooltip-placement="bottom"/>
														<input type="text" class="form-control1" placeholder='Avg.CCS Rating' name="avgCcsRating" ng-model="AvgSampleCard.avgCcsRatingok" readonly ng-required="true" ng-if="display=='1'" uib-tooltip="{{AvgSampleCard.avgCcsRatingok}}" tooltip-trigger="mouseenter" tooltip-enable="AvgSampleCard.avgCcsRatingok" tooltip-placement="bottom"/>
														<p ng-show="UpdateAvgCCSRatingForm.avgCcsRating{{$index}}.$error.required && (UpdateAvgCCSRatingForm.avgCcsRating{{$index}}.$dirty || Mainsubmitted)" class="help-block">Required.</p>
													</div>
												</td>
												<td>
													<div class="form-group" ng-class="{ 'has-error' : UpdateAvgCCSRatingForm.fgAvgCcsRating{{$index}}.$invalid && (UpdateAvgCCSRatingForm.fgAvgCcsRating{{$index}}.$dirty || Mainsubmitted)}">
														<input type="text" class="form-control1" placeholder='family Group' name="fgAvgCcsRating{{$index}}" ng-model="AvgSampleCard.fgAvgCcsRating" readonly ng-required="true" style="width:50px;" uib-tooltip="{{AvgSampleCard.fgAvgCcsRating}}" tooltip-trigger="mouseenter" tooltip-enable="AvgSampleCard.fgAvgCcsRating" tooltip-placement="bottom"/>
														<p ng-show="UpdateAvgCCSRatingForm.fgAvgCcsRating{{$index}}.$error.required && (UpdateAvgCCSRatingForm.fgAvgCcsRating{{$index}}.$dirty || Mainsubmitted)" class="help-block">Required.</p>
													</div>
												</td>
											</tr>										
										</tbody>
									</table>
								</div>
							  </section>
							</div>
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<!--<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CalculateAvgCCS();">Calculate Avg</button>-->
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CalculateAvgbyFg();">Calculate Avg by Family Group</button>
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									</div>
								</div>						 	
							 </div>					
        			    </form>      			
				  </div>
			    </div>
			</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
