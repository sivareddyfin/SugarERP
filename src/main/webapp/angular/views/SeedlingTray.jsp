	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>


	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="SeedlingTrayController">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Seedling Tray</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					
						<form name="SeedlingTrayForm" novalidate>	
							<input type="hidden"  name="modifyFlag" data-ng-model="AddTransporter.modifyFlag"  />						
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						        	                <div class="fg-line">
													<label for="transporterCode">Village</label>
														<input type="text" class="form-control" placeholder="Village" maxlength="10" name="transporterCode" ng-model="AddTransporter.transporterCode"  id="transporterCode" with-floating-label/>
				            		    	        </div>
			                    			</div>
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.contactNumber.$invalid && (SeedlingTrayForm.contactNumber.$dirty || Addsubmitted)}">												
						        	                <div class="fg-line">
													<label for="contactNumber">Trays taken Date</label>
														<input type="text" class="form-control" placeholder="Trays taken Date" maxlength="10" tabindex="3" name="contactNumber" ng-model="AddTransporter.contactNumber" data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-required='true' data-ng-minlength="10"  id="contactNumber" with-floating-label/>
				            		    	        </div>
<p ng-show="SeedlingTrayForm.contactNumber.$error.required && (SeedlingTrayForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Contact Number is required</p>																									
<p ng-show="SeedlingTrayForm.contactNumber.$error.pattern && (SeedlingTrayForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Contact Number starting with 7,8,9 series</p>																									
<p ng-show="SeedlingTrayForm.contactNumber.$error.minlength && (SeedlingTrayForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																									
</div>													
			                    			</div>						                    			                    											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																								
					        	                <div class="fg-line">
												<label for="ifsc">Sent Back</label>
													<input type="text" class="form-control" placeholder="Sent Back"  maxlength="10" tabindex="6" name="ifscCode" ng-model="AddTransporter.ifscCode"   id="ifsc" with-floating-label/>
			    		            	        </div>
<p></p>												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																								
					        	                <div class="fg-line">
												<label for="sentbackvno">Sent Back by Vehicle No.</label>
													<input type="text" class="form-control" placeholder="Sent Back by Vehicle No."  maxlength="10" tabindex="6" name="sentbackvno" ng-model="AddTransporter.ifscCode"   id="sentbackvno" with-floating-label/>
			    		            	        </div>
<p></p>												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																								
					        	                <div class="fg-line">
												<label for="trayscheckby">Trays Check By</label>
													<input type="text" class="form-control" placeholder="Trays Check By"  maxlength="10" tabindex="6" name="sentbackvno" ng-model="AddTransporter.ifscCode"   id="trayscheckby" with-floating-label/>
			    		            	        </div>
<p></p>												
</div>												
			            		        	</div>											
										</div>
									</div>
									
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				          		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.address.$invalid && (SeedlingTrayForm.address.$dirty || Addsubmitted)}">																																																																																				
					        	                <div class="fg-line">
												<label for="circle">Circle</label>
													<input type="text" class="form-control" placeholder="Circle" maxlength="50" tabindex="2" name="address" ng-model="AddTransporter.address" data-ng-required='true'  ng-blur="spacebtw('address');" id="circle" with-floating-label/>
					                	        </div>
<p ng-show="SeedlingTrayForm.address.$error.required && (SeedlingTrayForm.address.$dirty || Addsubmitted)" class="help-block">Address is required</p>												
</div>												
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																																																																																																
			    		    	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="Actual No.of Trays taken" />
			                			        </div>
</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.aadhaarNumber.$invalid && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
												
													<input type="text" class="form-control" placeholder="Good Trays" />
			    		            	        </div>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.required && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number is required</p>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.pattern && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.minlength && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.aadhaarNumber.$invalid && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
												
													<input type="text" class="form-control" placeholder="Driver No." />
			    		            	        </div>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.required && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number is required</p>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.pattern && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.minlength && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				          		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.address.$invalid && (SeedlingTrayForm.address.$dirty || Addsubmitted)}">																																																																																				
					        	                <div class="fg-line">
												<label for="address">Zone</label>
													<input type="text" class="form-control" placeholder="Zone" maxlength="50" tabindex="2" name="address" ng-model="AddTransporter.address" data-ng-required='true'  ng-blur="spacebtw('address');" id="address" with-floating-label/>
					                	        </div>
<p ng-show="SeedlingTrayForm.address.$error.required && (SeedlingTrayForm.address.$dirty || Addsubmitted)" class="help-block">Address is required</p>												
</div>												
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																																																																																																
			    		    	                <div class="fg-line">
													<input type="text" class="form-control" placeholder="No. of Trays Sent Back" />
			                			        </div>
</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.aadhaarNumber.$invalid && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
												
													<input type="text" class="form-control" placeholder="Balance" />
			    		            	        </div>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.required && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number is required</p>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.pattern && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.minlength && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : SeedlingTrayForm.aadhaarNumber.$invalid && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
												
													<input type="text" class="form-control" placeholder="Damage Tray" />
			    		            	        </div>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.required && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number is required</p>
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.pattern && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="SeedlingTrayForm.aadhaarNumber.$error.minlength && (SeedlingTrayForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>									
								</div>
							</div><br />

							
														 
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(SeedlingTrayForm,AddedTransporters);">Reset</button>
									</div>
								</div>						 	
							</div>																	 							 
						</form>	 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
									
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
