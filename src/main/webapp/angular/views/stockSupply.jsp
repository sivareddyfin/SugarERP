	<style>
.styletr {border-bottom: 1px solid grey;}


</style>

	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
	
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		 // $( "#date" ).datepicker({
//    		  changeMonth: true,
//		      changeYear: true,
//			  dateFormat: 'dd-mm-yy'
//	     });
		  $( "#stockDate" ).datepicker({
		  
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy',
			   maxDate: 0
			   
			 
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="stockSupplyController" ng-init="loadAddedStocks();addFormField();getAllDepartments();loadServerDate();onloadFunction();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Stock Supply</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="stockinForm" novalidate ng-submit="stockInSubmit(AddedStock,stockinForm);">	
						 <div class="row">
							 <div class="col-sm-6">
							 <div class="row">
								<div class="col-sm-12">
											<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															 <div class="form-group floating-label-wrapper">
											 	<div class="fg-line">
												<label for="stockInNo">Stock No.</label>
													<input type="text" class="form-control "  autofocus data-ng-required='true'  name="stockInNo" data-ng-model="AddedStock.stockInNo" placeholder="Stock No." tabindex="1"  id="stockInNo" with-floating-label readonly  />
												</div>
												
												
														  </div>
													  </div>			                    
									</div>
							</div>	
							</div>	
							
							<div class="col-sm-6">
							 <div class="row">
								<div class="col-sm-12">
											<div class="input-group">
															<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group "  ng-class="{ 'has-error' : stockinForm.department.$invalid && (stockinForm.department.$dirty || submitted)}">
											 	<div class="fg-line">
													<select chosen class="w-100"  tabindex="2" data-ng-required='true' data-ng-model='AddedStock.department' name="department" ng-options="department.deptCode as department.department for department in DepartmentsData">
																		<option value="">Department</option>
																	</select>
												</div>
												<p ng-show="stockinForm.department.$error.required && (stockinForm.department.$dirty || Addsubmitted)" class="help-block">Select Department</p>	 
														  </div>
													  </div>			                    
									</div>
							</div>	
							</div>		
								
						 </div>
						 
						 <div class="row">
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : stockinForm.stockDate.$invalid && (stockinForm.stockDate.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="stockDate">Stock Date</label>
													<input type="text" class="form-control"  name="stockDate" ng-required='true' data-ng-model="AddedStock.stockDate" placeholder="Stock Date" tabindex="4" maxlength="10"  data-input-mask="{mask: '00-00-0000'}" id="stockDate" with-floating-label data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/" readonly />
												</div>
												
												<p ng-show="stockinForm.stockDate.$error.required && (stockinForm.stockDate.$dirty || Addsubmitted)" class="help-block">Select Stock Date</p>
												<p ng-show="stockinForm.stockDate.$error.pattern && (stockinForm.stockDate.$dirty || Addsubmitted)" class="help-block">Valid Stock Date is required</p>
											 </div>
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : stockinForm.entryDate.$invalid && (stockinForm.entryDate.$dirty || submitted)}">
											 	<div class="fg-line">
												<label for="entryDate">Entry Date</label>
													<input type="text" class="form-control" data-ng-required='true'  name="entryDate" data-ng-model="AddedStock.entryDate" placeholder="Entry Date" tabindex="5" maxlength="10" data-input-mask="{mask: '00-00-0000'}" id="entryDate" with-floating-label   readonly />
												</div>
												<p ng-show="stockinForm.entryDate.$error.required && (stockinForm.entryDate.$dirty || Addsubmitted)" class="help-block">Select Entry Date</p>
												
												
											 </div>
										</div>
									</div>
								</div>
							
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : stockinForm.productionDepartment.$invalid && (stockinForm.productionDepartment.$dirty || submitted)}">												
																<div class="fg-line">
																	<select chosen class="w-100"  tabindex="6" data-ng-required='true' data-ng-model='AddedStock.productionDepartment' name="productionDepartment" ng-options="productionDepartment.deptCode as productionDepartment.department for productionDepartment in DepartmentsData">
																		<option value=""> Production Department</option>
																	</select>
																  </div>
								<p ng-show="stockinForm.productionDepartment.$error.required && (stockinForm.productionDepartment.$dirty || Addsubmitted)" class="help-block">Select Production Department</p>	
												
											 </div>
										</div>
									</div>
								</div>
							
							</div>
						 </div>
						 
						 
						 <br />
						 <div>
							<table style="width:100%;"  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Action</th>
											<th>Product Code</th>
											<th>Product Name</th>
							                <th>UOM</th>
											<th>Open Batch</th>
											<th>&nbsp;&nbsp; Batch</th>
											<th>Exp Date.</th>
											<th>Quantity</th>
											<th>MRP</th>
											<th>CP</th>
											
											
										
											
            							</tr>
										<tr class="styletr" style="height:70px;" ng-repeat='stockinData in data'>
										<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="stockinData.id == '1'" style="height:45px;"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" style="height:45px;" ng-click='removeRow(stockinData.id);' ng-if="stockinData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
												</td>
											<td>
											<input type='hidden'  name="batchFlag{{$index}}" ng-model="stockinData.batchFlag" >
											<input type='hidden'  name="batchSeqNo{{$index}}" ng-model="stockinData.batchSeqNo" >
											<input type='hidden'  name="batchId{{$index}}" ng-model="stockinData.batchId" >
											
																					
											
											
											<br />
											<div class="form-group" ng-class="{ 'has-error' : stockinForm.productCode{{$index}}.$invalid && (stockinForm.productCode{{$index}}.$dirty || submitted)}">
											
											<input list="stateList" placeholder="Product Code" id="prodcode{{$index}}"  class="form-control1"  name="productCode{{$index}}" placeholder ="Batch No" ng-model="stockinData.productCode" ng-change="loadProductCode(stockinData.productCode,$index);getproductIdforPopup(stockinData.productCode,$index);" style="height:40px;">
    								
											<datalist id="stateList">
												<select class="form-control1">
												<option ng-repeat="batch in addedProductCode" value="{{batch.ProductCode}}"></option>
												</select>    
											</datalist>
											
											
											
											
											<p ng-show="stockinForm.productCode{{$index}}.$error.required && (stockinForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											<p ng-show="stockinForm.productCode{{$index}}.$error.pattern && (stockinForm.productCode{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : stockinForm.productName{{$index}}.$invalid && (stockinForm.productName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="productName{{$index}}"  ng-model="stockinData.productName" data-ng-required='true' placeholder="Product Name"  style="height:40px;" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" maxlength="30" readonly />
												<p ng-show="stockinForm.productName{{$index}}.$error.required && (stockinForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="stockinForm.productName{{$index}}.$error.pattern && (stockinForm.productName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
											<td style="display:none;">
											<br />
											<div class="form-group" ng-class="{ 'has-error' : stockinForm.uom{{$index}}.$invalid && (stockinForm.uom{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uom{{$index}}"  ng-model="stockinData.uom" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="10" readonly/>
												<p ng-show="stockinForm.uom{{$index}}.$error.required && (stockinForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="stockinForm.uom{{$index}}.$error.pattern && (stockinForm.uom{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												<td>
											<br />
											<div class="form-group" ng-class="{ 'has-error' : stockinForm.uomName{{$index}}.$invalid && (stockinForm.uomName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" name="uomName{{$index}}"  ng-model="stockinData.uomName" placeholder="UOM" data-ng-required='true'   style="height:40px;"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" maxlength="20" readonly/>
												<p ng-show="stockinForm.uomName{{$index}}.$error.required && (stockinForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="stockinForm.uomName{{$index}}.$error.pattern && (stockinForm.uomName{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												</div>
												</td>
												
												<td> <button type="button" class="btn btn-default btn-sm" style="height:38px;" ng-click="getbatchPopup(stockinData.productCode,$index,AddedStock.department);">
          <span class="glyphicon glyphicon-open"></span> Open
        </button></td>
											<td>
											
											<br />	
											<div class="form-group" ng-class="{ 'has-error' : stockinForm.batch{{$index}}.$invalid && (stockinForm.batch{{$index}}.$dirty || submitted)}">
											<input type="text" class="form-control1" name="batch{{$index}}"   ng-model="stockinData.batch" placeholder="Batch" ng-required='true' style="height:40px;" maxlength="10" ng-blur="duplicateProductCode(stockinData.batch,$index);" readonly />
											<p ng-show="stockinForm.batch{{$index}}.$error.required && (stockinForm.batch{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
											</div>
											</td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : stockinForm.expDate{{$index}}.$invalid && (stockinForm.expDate{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1 datepicker" name="expDate{{$index}}"  ng-model="stockinData.expDt"  placeholder="Exp Date" style="height:40px;" maxlength="10"  data-ng-required='true' data-input-mask="{mask: '00/0000'}" data-ng-pattern="^(0[1-9]|(1[0-2]+)+)\/(20(0[2-9]|([1-2][0-9])|30))$" readonly />
											
											
											
											<p ng-show="stockinForm.expDate{{$index}}.$error.required && (stockinForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="stockinForm.expDate{{$index}}.$error.pattern && (stockinForm.expDate{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>
												</div></td>
											
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : stockinForm.qty{{$index}}.$invalid && (stockinForm.qty{{$index}}.$dirty || submitted)}">	<input type="text" class="form-control1" name="qty{{$index}}"  ng-model="stockinData.quantity"  placeholder="Qty" ng-required='true'  style="height:40px;" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" maxlength="10"  />
											<p ng-show="stockinForm.qty{{$index}}.$error.required && (stockinForm.qty{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>	
												<p ng-show="stockinForm.qty{{$index}}.$error.pattern && (stockinForm.qty{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div>
											</td>
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : stockinForm.mrp{{$index}}.$invalid && (stockinForm.mrp{{$index}}.$dirty || submitted)}">		<input type="text" class="form-control1" id name="mrp{{$index}}"  ng-model="stockinData.mrp"  placeholder="MRP" ng-required='true'  style="height:40px;" maxlength="12"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="getMRPid{{$index}}" /><p ng-show="stockinForm.mrp{{$index}}.$error.required && (stockinForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block" >Required</p>	<p ng-show="stockinForm.mrp{{$index}}.$error.pattern && (stockinForm.mrp{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
											</div></td>
											<td><br />	<div class="form-group" ng-class="{ 'has-error' : stockinForm.cp{{$index}}.$invalid && (stockinForm.cp{{$index}}.$dirty || submitted)}">		<input type="text" class="form-control1" name="cp{{$index}}"  ng-model="stockinData.cp" placeholder="CP" style="height:40px;" maxlength="12"   ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" /><p ng-show="stockinForm.cp{{$index}}.$error.required && (stockinForm.cp{{$index}}.$dirty || Addsubmitted)" class="help-block">Required</p>
											<p ng-show="stockinForm.cp{{$index}}.$error.pattern && (stockinForm.cp{{$index}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>	
												
											</div></td>
											
										</tr>
										
										
														
									</thead>
							</table>
							</div>
						 
						 
						 
						 					
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(stockinForm)">Reset</button>
									</div>
								</div>						 	
							 </div>				
						</form>
								
								
					 <footer data-ng-include="'template/batchDetailsPopup.jsp'"  style='font-weight:bold;'></footer>	
								 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
