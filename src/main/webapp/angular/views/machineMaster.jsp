	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="MachineMasterController" ng-init="loadModifyFlag();loadMachines();">      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Machine Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 	 <form name="MachineForm" novalidate ng-submit="machineSubmit(AddedMachine,MachineForm)">
						 <input type="hidden" ng-model="AddedMachine.modifyFlag" name="modifyFlag">
					 		<div class="row">
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : MachineForm.machineId.$invalid && (MachineForm.machineId.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="accountSubGroup">Machine Code</label>
    	        								         <input type="text" class="form-control autofocus" placeholder="Machine Code"  maxlength="10"  name="machineId" data-ng-model='AddedMachine.machineCode' data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" tabindex="1"  id="accountSubGroup" with-floating-label>														 
				                				    </div>
								<p ng-show="MachineForm.machineId.$error.required && (MachineForm.machineId.$dirty || Addsubmitted)" class="help-block"> Machine Code is required</p>
								<p ng-show="MachineForm.machineId.$error.pattern && (MachineForm.machineId.$dirty || Addsubmitted)" class="help-block"> Machine Code is Invalid</p>
										
											</div>
					                    	</div>
										</div>
									</div>
								
								
										
										
									
																		
																		
								</div>
								<div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : MachineForm.machineName.$invalid && (MachineForm.machineName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
														<label for="MFRShortCut">Machine Number</label>
    	        								         <input type="text" class="form-control " placeholder="Machine Number"  maxlength="25"  name="machineName" data-ng-model='AddedMachine.machineNumber' data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/"  id="MFRShortCut" with-floating-label tabindex="2">														 
				                				    </div>
								<p ng-show="MachineForm.machineName.$error.required && (MachineForm.machineName.$dirty || Addsubmitted)" class="help-block"> Machine Number is required</p>
								<p ng-show="MachineForm.machineName.$error.pattern && (MachineForm.machineName.$dirty || Addsubmitted)" class="help-block"> Machine Number is Invalid</p>
																		
												</div>
					                    	</div>
										</div>
									</div>
								
								
										
										
									
																		
																		
								</div>
								
							</div>
							
							<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="status" value="0"  data-ng-model='AddedMachine.status' tabindex="3">
			    					    				<i class="input-helper"></i>Active
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				            							<input type="radio" name="status" value="1"  data-ng-model='AddedMachine.status'>
					    								<i class="input-helper"></i>Inactive
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>
							<br />	
							<div class="row" align="center">
								<div class="col-sm-12">
									<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-hide" tabindex="7">Save</button>
										<button type="reset" class="btn btn-primary" ng-click="reset(MachineForm);">Reset</button>
									</div>
								</div>
								</div>							
							</div>
						</form>
							 						
							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Machines</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1">	
										<form name="EditAccountSubGroupMasterFrom" novalidate>					
							        <table ng-table="MachineForm.tableEdit" class="table table-striped table-vmiddle">
											<thead>
												<tr>
													<th><span>Action</span></th>
													<th><span>Machine Code</span></th>
													<th><span>Machine Number</span></th>
													<th><span>Status</span></th>
													
													
												</tr>
											</thead>
										<tbody>
								        <tr ng-repeat="Group in machineData"  ng-class="{ 'active': Group.$edit }">
                		    				<td>
					    		               <button type="button" class="btn btn-default" ng-if="!Group.$edit" ng-click="Group.$edit = true;Group.modifyFlag='Yes'"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Group.$edit" ng-click="UpdateMachine(Group,$index);Group.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="Group.modifyFlag"  />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Group.$edit">{{Group.machineCode}}</span>
					    		                <div ng-if="Group.$edit">
													<div class="form-group">
													<input class="form-control" type="text" ng-model="Group.machineCode" placeholder='Machine Code' maxlength="10" name="machineCode{{$index}}" />
													</div>
												</div>
					            		      </td>
		                    				  
											  <td>
							                     <span ng-if="!Group.$edit">{{Group.machineNumber}}</span>
							                     <div ng-if="Group.$edit">
<div class="form-group" ng-class="{ 'has-error' : EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$invalid && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || Addsubmitted)}">												
<input class="form-control" type="text" ng-model="Group.machineNumber" placeholder='Machine Number'  name="accountSubGroup{{$index}}" data-ng-required='true' data-ng-pattern="/^[a-z A-Z 0-9\s]*$/" />
			<p ng-show="EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$error.required && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || submitted)" class="help-block"> Machine Number is required</p>
			<p ng-show="EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$error.pattern  && (EditAccountSubGroupMasterFrom.accountSubGroup{{$index}}.$dirty || submitted)" class="help-block">Enter Valid Machine Number</p>
			

</div>
												 </div>
							                  </td>

											   <td>
							                      <span ng-if="!Group.$edit">
												  	<span ng-if="Group.status=='0'">Active</span>
													<span ng-if="Group.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="Group.$edit">
												  	<div class="form-group">
												  	<label class="radio radio-inline m-r-20">
														<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="0"  ng-model="Group.status">
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-10px;">
				            							<input type="radio" name="inlineRadioOptionsgrid{{$index}}" value="1" ng-model="Group.status">
			    										<i class="input-helper"></i>Inactive
							  		 				</label>				
													</div>		 							 												  
												  </div>
							                  </td>
							                  
											  
											  
											  											  
							             </tr>
								         </table>	
									</form>
									</div>
									</section>						 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
