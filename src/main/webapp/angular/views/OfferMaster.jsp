	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>		

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>     
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="GenerateAgreement" ng-init="addAgreemntRows();loadSeasonNames();loadRyotNames();loadBankNames();loadVarietyNames();loadLandType();loadCropType();loadCircleNames();loadFamilyNames();loadMandalNames();loadAgreementNumber();loadVillageNames();loadRyotExtentNames();loadSetupDetails();loadServerDate();loadSuretyRyotNames();getScreenPreveliges();getInitialize();loadStartingFrom();getAgreementNumbersForSeason();loadFieldOfficerDropdown();loadFieldAssistants();loadZoneNames();">       
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Agreement</b></h2></div>
			    <div class="card"> 
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					  <form name="GenerateAgreementForm" novalidate ng-submit="GenerateAgreement(AddAgreement,GenerateAgreementForm);" enctype="multipart/form-data">
					  	 <div class="row">
							 	<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
										<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.seasonYear.$invalid && (GenerateAgreementForm.seasonYear.$dirty || submitted)}">																					
				        	                <div class="fg-line">
											  	<select chosen class="w-100" ng-options="seasonCode.season as seasonCode.season for seasonCode in seasonNames | orderBy:'-season':true" name="seasonYear" ng-model="AddAgreement.seasonYear" ng-required="true" ng-change='setAddAgreement(AddAgreement.seasonYear,AddAgreement.typeOfAgreement);loadAgreementSeqNumberBySeason(AddAgreement.seasonYear);'>
													<option value="">Select Season</option>
												</select>
				                	        </div>
				<p ng-show="GenerateAgreementForm.seasonYear.$error.required && (GenerateAgreementForm.seasonYear.$dirty || submitted)" class="help-block">Season is required.</p>
										</div>
										</div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
				        	                <div class="fg-line">
											  	<select chosen class="w-100" name="agreementAddNumber" ng-model="AddAgreement.agreementAddNumber" ng-options="agreementAddNumber.agreementnumber as agreementAddNumber.agreementnumber for agreementAddNumber in AddedAgreements" ng-change="getAllAddedDetails(AddAgreement.agreementAddNumber,AddAgreement.seasonYear);">
													<option value="">Added Agreements</option>
												</select>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>  
							 </div>							 				  
					  	 <div class="row">
						 	<div class="col-sm-6">																
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												  <label for="AgreementNumber">Agreement Number</label>  	
    		        					          <input type="text" class="form-control" placeholder="Agreement Number"   maxlength="15" tabindex="1" readonly name="agreementNumber" ng-model="AddAgreement.agreementNumber" id="AgreementNumber" with-floating-label/>
												  <input type="hidden" name="tempAgreement" ng-model="tempAgreement" />
					                	        </div>
												<input type="hidden" ng-model="AddAgreement.modifyFlag" />
												<input type="hidden" ng-model="AddAgreement.screenName" />
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.ryotCode.$invalid && (GenerateAgreementForm.ryotCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select chosen class="w-100"  tabindex="2" name="ryotCode" ng-model="AddAgreement.ryotCode" ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotNames | orderBy:'-id':true"  ng-required="true" ng-change="loadTyotDetails(AddAgreement.ryotCode);loadRyotDet(AddAgreement.ryotCode)">
														<option value="">Select Ryot Code</option>
													</select>
				                	    	    </div>
				<p ng-show="GenerateAgreementForm.ryotCode.$error.required && (GenerateAgreementForm.ryotCode.$dirty || submitted)" class="help-block">Select Ryot Code</p>												
				</div>												
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												  <label for="RyotName">Ryot Name</label>
    		        					          <input type="text" class="form-control" placeholder="Ryot Name"  readonly maxlength="50" name="ryotName" ng-model="AddAgreement.ryotName" id="RyotName" with-floating-label/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.circleCode.$invalid && (GenerateAgreementForm.circleCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select class="w-100" chosen name="circleCode" ng-model="AddAgreement.circleCode" ng-options="circleCode.id as circleCode.circle for circleCode in circleNames | orderBy:'-cirlce':true"  ng-required="true" ng-change="loadZoneData(AddAgreement.circleCode)";>
													<option value="">Select Circle</option>
													</select>													
					                	        </div>
												<p ng-show="GenerateAgreementForm.circleCode.$error.required && (GenerateAgreementForm.circleCode.$dirty || submitted)" class="help-block">Circle is Required.</p>													
											</div>
											</div>  
				                    	</div>
									</div>
								</div>
								
								
									<div class='row'><div class="col-sm-12">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.foCode.$invalid && (GenerateAgreementForm.foCode.$dirty || Addsubmitted)}">
										 <div class="fg-line">
														 <select chosen class="w-100" name="foCode" data-ng-model='AddAgreement.foCode' data-ng-required='true' ng-options="foCode.id as foCode.fieldOfficer for foCode in FieldOffNames  | orderBy:'-fieldOfficer':true">
														<option value="">Field Officer</option>
				        				            </select>
				    		            	        </div>
										 
											 
								<p ng-show="GenerateAgreementForm.foCode.$error.required && (GenerateAgreementForm.foCode.$dirty || Addsubmitted)" class="help-block">Select Field Officer</p>										
										   </div>
									   </div>
										
									</div></div>
									
									<div class='row'><div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateAgreementForm.faCode.$invalid && (GenerateAgreementForm.faCode.$dirty || Addsubmitted)}">
												<div class="fg-line">
														 <select chosen class="w-100" name='faCode' ng-model="AddAgreement.faCode" ng-options="faCode.id as faCode.fieldassistant for faCode in FieldAssistantData" data-ng-required="true">
														<option value="">Field Assistant</option>
														
														</select>
				    		            	        </div>
														
				    		            	        
					<p ng-show="GenerateAgreementForm.faCode.$error.required && (GenerateAgreementForm.faCode.$dirty || Addsubmitted)" class="help-block">Field Assistant is Required.</p>
																						
												</div>
			            		        	</div>
										</div></div>
								
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.branchCode.$invalid && (GenerateAgreementForm.branchCode.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  	<select chosen class="w-100"  tabindex="3" name="branchCode" ng-model="AddAgreement.branchCode" ng-options="branchCode.id as branchCode.branchname for branchCode in bankNames | orderBy:'-branchname':true"  ng-required="true">
														<option value="">Select Branch</option>
													</select>
												  </div>
<p ng-show="GenerateAgreementForm.branchCode.$error.required && (GenerateAgreementForm.branchCode.$dirty || submitted)" class="help-block">Branch is Required.</p>													
</div>												  
			                	    	    </div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				           	 	<span class="input-group-addon">Has FamilyGroup :</span>
				        	                <div class="fg-line" style="margin-top:5px;">
    	        					          <label class="radio radio-inline m-r-20">
									            <input type="radio" name="hasFamilyGroup" value="0" ng-model="AddAgreement.hasFamilyGroup" ng-click="changeFamilyGroupStatus(AddAgreement.hasFamilyGroup);">
				    			            	<i class="input-helper"></i>Yes
									          </label>
				 				              <label class="radio radio-inline m-r-20">
				        	    			    <input type="radio" name="hasFamilyGroup" value="1" ng-model="AddAgreement.hasFamilyGroup" ng-click="changeFamilyGroupStatus(AddAgreement.hasFamilyGroup);">
			    					            <i class="input-helper"></i>No
					  		    	          </label>
			                	    	    </div>
				                    	</div>
									</div>
								</div><br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.suretyRyotCode.$invalid && (GenerateAgreementForm.suretyRyotCode.$dirty || submitted)}">											
					        	                <div class="fg-line">    	    	    					          
													<select  chosen  class="w-100" data-placeholder="Select Surety Ryots" tabindex="6" name="suretyRyotCode" ng-model="AddAgreement.suretyRyotCode" ng-options="suretyRyotCode.id as suretyRyotCode.id for suretyRyotCode in suretyRyotNames"      ng-change="setSuretyRequired(AddAgreement.suretyRyotCode);"><option value="">Select Surety Ryot</option></select>												
					                	        </div>
<p ng-show="GenerateAgreementForm.suretyRyotCode.$error.required && (GenerateAgreementForm.suretyRyotCode.$dirty || submitted)" class="help-block">Surety Ryot is Required.</p>												
</div>												
											</div>
			    		               	</div>
										
									</div>
								</div>
								<div id="map_canvas" style="display:"></div>
								
								
								
							</div>
							
							
							<div class="col-sm-6">								
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateAgreementForm.agreementDate.$invalid && (GenerateAgreementForm.agreementDate.$dirty || submitted)}">											
					        	                <div class="fg-line">
												   <label for="AgreementDate">Agreement Date</label>
    		        					          <input type="text" class="form-control datepicker autofocus" placeholder="Agreement Date" maxlength="10" name="agreementDate" ng-model="AddAgreement.agreementDate" data-input-mask="{mask: '00-00-0000'}"  ng-required="true" id="AgreementDate" autofocus ng-mouseover="ShowDatePicker();" with-floating-label  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"/>
					                	        </div>
<p ng-show="GenerateAgreementForm.agreementDate.$error.required && (GenerateAgreementForm.agreementDate.$dirty || submitted)" class="help-block">Agreement Date is Required.</p>	
<p ng-show="GenerateAgreementForm.agreementDate.$error.pattern && (GenerateAgreementForm.agreementDate.$dirty || submitted)" class="help-block">Enter Valid Agreement Date</p>	
</div>												
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												  <label for="Salutation">Salutation</label>
    		        					          <input type="text" class="form-control" placeholder="Salutation" readonly maxlength="5" name="salutation" ng-model="AddAgreement.salutation" id="Salutation"  with-floating-label/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
												  <label for="Fname">F/H/G Name</label>
    		        					          <input type="text" class="form-control" placeholder="F/H/G Name" readonly maxlength="50" name="relativeName" ng-model="AddAgreement.relativeName" id="Fname" with-floating-label/>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
												  <select class="w-100" chosen name="mandalCode" ng-model="AddAgreement.mandalCode" ng-options="mandalCode.id as mandalCode.mandal for mandalCode in mandalNames | orderBy:'-mandal':true" ng-disabled="AddAgreement.ryotCode!=null"><option value="">Mandal Name</option></select>
					                	        </div>
											</div>
				                    	</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GenerateAgreementForm.accountNumber.$invalid && (GenerateAgreementForm.accountNumber.$dirty || submitted)}">											
					        	                <div class="fg-line">
												  <label for="RyotAccountNumber">Ryot SB A/C No</label>
    		        					          <input type="text" class="form-control" placeholder="Ryot SB A/C No."  tabindex="4" name="accountNumber" ng-model="AddAgreement.accountNumber" maxlength="16"  ng-required="true" data-ng-pattern="/^[0-9]+$/" id="RyotAccountNumber" with-floating-label ng-blur="spacebtw('accountNumber');" data-ng-minlength="7" />
					                	        </div>
<p ng-show="GenerateAgreementForm.accountNumber.$error.required && (GenerateAgreementForm.accountNumber.$dirty || submitted)" class="help-block">Account Number is Required.</p>												
<p ng-show="GenerateAgreementForm.accountNumber.$error.pattern && (GenerateAgreementForm.accountNumber.$dirty || submitted)" class="help-block">Enter Valid Account Number.</p>
<p ng-show="GenerateAgreementForm.accountNumber.$error.minlength && (GenerateAgreementForm.accountNumber.$dirty || submitted)" class="help-block">Account Number is Too Short.</p>												
</div>												
				                    	</div>										
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.familyGroupCode.$invalid && (GenerateAgreementForm.familyGroupCode.$dirty || submitted)}">											
					        	                <div class="fg-line">													
												  	<select chosen class="w-100"  tabindex="5" name="familyGroupCode" ng-model="AddAgreement.familyGroupCode"  ng-required="familyGroupValid" ng-options="familyGroupCode.id as familyGroupCode.groupname for familyGroupCode in FamilyNames | orderBy:'-groupname':true" ng-disabled="AddAgreement.hasFamilyGroup=='1'"><option value="">Select Family Group</option></select>
												  	<!--<select chosen class="w-100"  tabindex="5" name="familyGroupCode" ng-model="AddAgreement.familyGroupCode"  ng-options="familyGroupCode.id as familyGroupCode.groupname for familyGroupCode in FamilyNames | orderBy:'-groupname':true" ng-disabled="AddAgreement.hasFamilyGroup=='1'"><option value="">Select Family Group</option></select>-->													
												  </div>
<p ng-show="GenerateAgreementForm.familyGroupCode.$error.required && (GenerateAgreementForm.familyGroupCode.$dirty || submitted)" class="help-block">Family Group is Required.</p>													
</div>												  
			                		        </div>
			                    		</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.surityPersonName.$invalid && (GenerateAgreementForm.surityPersonName.$dirty || submitted)}">																																																																																																												
					        	                <div class="fg-line">
													<label for="sPerson">Surety Person Name</label>
													<input type="text" class="form-control" placeholder="Surety Person Name" maxlength="25" tabindex="8" name="surityPersonName" ng-model="AddAgreement.surityPersonName"  data-ng-pattern="/^[a-zA-Z.\s]*$/"  ng-blur="spacebtw('surityPersonName');" id="sPerson"   with-floating-label  ng-required="{{suretyStatus}}" ng-disabled="AddAgreement.suretyRyotCode!='0'"/>
			    		            	        </div>
<p ng-show="GenerateAgreementForm.surityPersonName.$error.required && (GenerateAgreementForm.surityPersonName.$dirty || submitted)" class="help-block">Surety Person Name Required</p>
<p ng-show="GenerateAgreementForm.surityPersonName.$error.pattern && (GenerateAgreementForm.surityPersonName.$dirty || submitted)" class="help-block">Enter Valid Surety Person Name</p>																																											
												</div>												
											</div>
			    		               	</div>
										
									</div>
									<div class="col-sm-6">
										<div class="input-group">
	            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.surityPersonaadhaarNumber.$invalid && (GenerateAgreementForm.surityPersonaadhaarNumber.$dirty || submitted)}">																																																																																																												
					        	                <div class="fg-line">
													<label for="aadhaar">Aadhar No.</label>
													<input type="text" class="form-control" placeholder="Aadhar No." maxlength="14" tabindex="8" name="surityPersonaadhaarNumber" ng-model="AddAgreement.surityPersonaadhaarNumber"  ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}" data-ng-pattern="/^[0-9\s]*$/"  ng-blur="spacebtw('surityPersonaadhaarNumber');" id="aadhaar"   with-floating-label ng-required="{{suretyStatus}}" ng-disabled="AddAgreement.suretyRyotCode!='0'"/>
			    		            	        </div>
<p ng-show="GenerateAgreementForm.surityPersonaadhaarNumber.$error.required && (GenerateAgreementForm.surityPersonaadhaarNumber.$dirty || submitted)" class="help-block">Aadhar Number Required</p>
<p ng-show="GenerateAgreementForm.surityPersonaadhaarNumber.$error.pattern && (GenerateAgreementForm.surityPersonaadhaarNumber.$dirty || submitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="GenerateAgreementForm.surityPersonaadhaarNumber.$error.minlength && (GenerateAgreementForm.surityPersonaadhaarNumber.$dirty || submitted)" class="help-block">Too Short</p>								
												</div>												
											</div>
			    		               	</div>
										
									</div>
								</div>
								<div class="row">
									<div class="col-sm-7">
										<div class="input-group" style="margin-top:10px;">
            				            	<span class="input-group-addon">Status :</span>
				        	                <div class="fg-line" style="margin-top:5px;">
    	        					          <label class="radio radio-inline m-r-20">
									            <input type="radio" name="status" value="0" ng-model="AddAgreement.status">
				    			            	<i class="input-helper"></i>Active
									          </label>
				 				              <label class="radio radio-inline m-r-20">
				        	    			    <input type="radio" name="status" value="1" ng-model="AddAgreement.status">
			    					            <i class="input-helper"></i>Inactive
					  		    	          </label>
			                	    	    </div>
				                    	</div>
									</div>
									
										<input type="hidden" name="zoneCode" ng-model="AddAgreement.zoneCode" />
										
										<div class='col-sm-5'>
											
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group  floating-label-wrapper">
					        	                <div class="fg-line">
												  <input type="text" class="form-control" placeholder="Zone Name"   maxlength="50" name="ryotName" readonly ng-model="AddAgreement.zoneName" id="zoneName" with-floating-label/>
					                	        </div>
											</div>
				                    	</div>
									
										</div>
									
								</div>
								<br />
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper">
					        	                <div class="fg-line">
													<label for="Remarks">Remarks</label>
												  	<input type="text" class="form-control" placeholder="Remarks"  tabindex="7" name="remarks" ng-model="AddAgreement.remarks" id="Remarks" with-floating-label ng-blur="spacebtw('remarks');"/>
													<input type="hidden"  name="villageCode" ng-model="AddAgreement.villageCode"/>
													<input type="hidden"  tabindex="7" name="relation" ng-model="AddAgreement.relation"/>
													<input type="hidden"  tabindex="7" name="agreementSeqNo" ng-model="AddAgreement.agreementSeqNo"/>
												  </div>
			                		        </div>
			                    		</div>										
									</div>
								</div>
																								
							</div>							
						 </div>
						 
						 <div class="row">
									<div class="col-sm-12">
										<div class="input-group" style="margin-top:10px;">
            				            	<span class="input-group-addon">Type of Agreement :</span>
				        	                <div class="fg-line" style="margin-top:5px;">
    	        					          <label class="radio radio-inline m-r-20">
									            <input type="radio" name="typeOfAgreement" value="0" ng-model="AddAgreement.typeOfAgreement" ng-change="updateAgmtNobytypeOfAgrmt(AddAgreement.seasonYear,AddAgreement.typeOfAgreement);setAddAgreement(AddAgreement.seasonYear,AddAgreement.typeOfAgreement);">
				    			            	<i class="input-helper"></i>Seedling
									          </label>
				 				              <label class="radio radio-inline m-r-20">
				        	    			    <input type="radio" name="typeOfAgreement" value="1" ng-model="AddAgreement.typeOfAgreement" ng-change="updateAgmtNobytypeOfAgrmt(AddAgreement.seasonYear,AddAgreement.typeOfAgreement);setAddAgreement(AddAgreement.seasonYear,AddAgreement.typeOfAgreement);">
			    					            <i class="input-helper"></i>Cane Supply
					  		    	          </label>
			                	    	    </div>
				                    	</div>
									</div>
								</div>	
						 <hr />	
						 
						 <tabset justified="true">
			                <tab  heading="Planting">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															
															<th><span>Action</span></th>
															<th><span>Plot no</span></th>
															<th><span>variety</span></th>
															
															<th><span>Plant/Ratoon </span></th>
															<th><span>Date of planting</span></th>
															<th><span>Agreed Quantity<br />(Tons)</span></th>
															<th><span>Extent (Acres)</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="agreementData in data" id="idok{{$index}}">
															
													<td>
											<input type="hidden" name="setupStatus" ng-model="setupStatus" />
											<input type="hidden" ng-model="agreementData.screenName" />
											<button type="button" class="btn btn-primary" ng-click="addAgreemntRows();" ng-if="agreementData.plotSeqNo=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
											<button type="button" class="btn btn-primary" ng-click="removeAgreemntRows(agreementData.plotSeqNo);" ng-if="agreementData.plotSeqNo!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>	
											</td>
							    	    	<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plotNumber{{$index}}.$invalid && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" placeholder='Plot Number' maxlength="10"  tabindex="7" name="plotNumber{{$index}}" ng-model="agreementData.plotNumber" data-ng-pattern="/^[0-9.]+$/"  ng-required="true" min="agreementData.plotNumber"  uib-tooltip="{{agreementData.plotNumber}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.plotNumber" tooltip-placement="bottom"/>							
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.required && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>		
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.pattern && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.min && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Invalidsdf.</p>																						
											</div>	
											</td>		
														
											<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.varietyCode{{$index}}.$invalid && (GenerateAgreementForm.varietyCode{{$index}}.$dirty || submitted)}">										
												<select class="form-control1" tabindex="8" name="varietyCode{{$index}}" ng-model="agreementData.varietyCode" ng-options="varietyCode.id as varietyCode.variety for varietyCode in varietyNames | orderBy:'-variety':true"  ng-required="true" uib-tooltip="{{agreementData.varietyCodeTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.varietyCode" tooltip-placement="bottom" ng-change="setVarietyTooltip(agreementData.varietyCode,$index);">
													<option value="">Variety</option>
												</select>
												<p ng-show="GenerateAgreementForm.varietyCode{{$index}}.$error.required && (GenerateAgreementForm.varietyCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>													
											</div>
											</td>
											
											<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plantorRatoon{{$index}}.$invalid && (GenerateAgreementForm.plantorRatoon{{$index}}.$dirty || submitted)}">										
												<select class="form-control1" tabindex="9" name="plantorRatoon{{$index}}" ng-model="agreementData.plantorRatoon" ng-options="plantorRatoon.id as plantorRatoon.croptype for plantorRatoon in cropTypeNames | orderBy:'-croptype':true"  ng-required="true" ng-change="showRatoonDialog(AddAgreement.seasonYear,AddAgreement.ryotCode,agreementData.plantorRatoon,$index);setplantorRatoonTooltip(agreementData.plantorRatoon,$index);" uib-tooltip="{{agreementData.plantorRatoonTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.plantorRatoon" tooltip-placement="bottom">
												<option value="">Crop Type</option></select>
												<p ng-show="GenerateAgreementForm.plantorRatoon{{$index}}.$error.required && (GenerateAgreementForm.plantorRatoon{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
											</div>
											</td>											
											<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.cropDate{{$index}}.$invalid && (GenerateAgreementForm.cropDate{{$index}}.$dirty || submitted)}">										
												<input type="text" class="form-control1 datepicker" placeholder='Crop Date' maxlength="10" tabindex="10" name="cropDate{{$index}}" ng-model="agreementData.cropDate" data-input-mask="{mask: '00-00-0000'}" ng-mouseover="ShowDatePicker();"  ng-required="true"  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  uib-tooltip="{{agreementData.cropDate}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.cropDate" tooltip-placement="bottom"/>
												<p ng-show="GenerateAgreementForm.cropDate{{$index}}.$error.required && (GenerateAgreementForm.cropDate{{$index}}.$dirty || submitted)" class="help-block">Required.</p>									
												<p ng-show="GenerateAgreementForm.cropDate{{$index}}.$error.pattern && (GenerateAgreementForm.cropDate{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
											</div>
											</td>
											<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.agreedQty{{$index}}.$invalid && (GenerateAgreementForm.agreedQty{{$index}}.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Agrd. Qty' maxlength="10" tabindex="11" name="agreedQty{{$index}}" ng-model="agreementData.agreedQty" data-ng-pattern="/^[0-9]+$/"  ng-required="true" uib-tooltip="{{agreementData.agreedQty}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.agreedQty" tooltip-placement="bottom"/>
												<p ng-show="GenerateAgreementForm.agreedQty{{$index}}.$error.required && (GenerateAgreementForm.agreedQty{{$index}}.$dirty || submitted)" class="help-block">Required.</p>	
												<p ng-show="GenerateAgreementForm.agreedQty{{$index}}.$error.pattern && (GenerateAgreementForm.agreedQty{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>																						
</div>	
											</td>
											
											<td>
													<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.extentSize{{$index}}.$invalid && (GenerateAgreementForm.extentSize{{$index}}.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Extent(Acers)' maxlength="10" tabindex="12" name="extentSize{{$index}}" ng-model="agreementData.extentSize" data-ng-pattern="/^[0-9.]+$/"  ng-required="true" uib-tooltip="{{agreementData.extentSize}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.extentSize" tooltip-placement="bottom"/>
												<p ng-show="GenerateAgreementForm.extentSize{{$index}}.$error.required && (GenerateAgreementForm.extentSize{{$index}}.$dirty || submitted)" class="help-block">Required.</p>							
												<p ng-show="GenerateAgreementForm.extentSize{{$index}}.$error.pattern && (GenerateAgreementForm.extentSize{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>																		
											</div>												
											</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
							<tab  heading="Plot Identity">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Action</span></th>
															<th><span>Plot no</span></th>
															<th><span>Plot <br />Indentity</span></th>
															
															<th><span>Land  <br />Villlage code</span></th>
															<!--<th><span>Start</span></th>-->
															<th><span>Address</span></th>
															<th><span>Distance</span></th>
															<th><span>Land type</span></th>
															
															<th><span>Survey<br /> number</span></th>
															<th><span>Extent Image</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat="agreementData in data" id="idok{{$index}}">
															
													<td>
											<input type="hidden" name="setupStatus" ng-model="setupStatus" />
											<input type="hidden" ng-model="agreementData.screenName" />
											<button type="button" class="btn btn-primary" ng-click="addAgreemntRows();" ng-if="agreementData.plotSeqNo=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
											<button type="button" class="btn btn-primary" ng-click="removeAgreemntRows(agreementData.plotSeqNo);" ng-if="agreementData.plotSeqNo!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>	
											</td>
							    	    	<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plotNumber{{$index}}.$invalid && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control1" placeholder='Plot Number' maxlength="10"  tabindex="7" name="plotNumber{{$index}}" ng-model="agreementData.plotNumber" data-ng-pattern="/^[0-9.]+$/"  ng-required="true" min="agreementData.plotNumber"  uib-tooltip="{{agreementData.plotNumber}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.plotNumber" tooltip-placement="bottom"/>							
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.required && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>		
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.pattern && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>
											<p ng-show="GenerateAgreementForm.plotNumber{{$index}}.$error.min && (GenerateAgreementForm.plotNumber{{$index}}.$dirty || submitted)" class="help-block">Invalidsdf.</p>																						
											</div>	
											</td>		
														
											<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.plotIdentity{{$index}}.$invalid && (GenerateAgreementForm.plotIdentity{{$index}}.$dirty || submitted)}">
												<select class="form-control1" tabindex="8" name="plotIdentity{{$index}}" ng-model="agreementData.plotIdentity" ng-options="plotIdentity.id as plotIdentity.plotIdentity for plotIdentity in plotIdentity | orderBy:'-plotIdentify':true"  ng-required="true" uib-tooltip="{{agreementData.plotIdentityTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.plotIdentity" tooltip-placement="bottom">
													<option value="">Plot Identify</option>
												</select>							
											<p ng-show="GenerateAgreementForm.plotIdentity{{$index}}.$error.required && (GenerateAgreementForm.plotIdentity{{$index}}.$dirty || submitted)" class="help-block">Required.</p>		
											<p ng-show="GenerateAgreementForm.plotIdentity{{$index}}.$error.pattern && (GenerateAgreementForm.plotIdentity{{$index}}.$dirty || submitted)" class="help-block">Invalid.</p>
											<p ng-show="GenerateAgreementForm.plotIdentity{{$index}}.$error.min && (GenerateAgreementForm.plotIdentity{{$index}}.$dirty || submitted)" class="help-block">Invalidsdf.</p>																						
											</div>
											</td>
																					
											<td>
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.landVillageCode{{$index}}.$invalid && (GenerateAgreementForm.landVillageCode{{$index}}.$dirty || submitted)}">										
												<select class="form-control1" name="landVillageCode{{$index}}" ng-model="agreementData.landVillageCode" tabindex="14" ng-required="true" ng-options="landVillageCode.id as landVillageCode.village for landVillageCode in villageNames | orderBy:'-village':true" uib-tooltip="{{agreementData.landVillageCodeTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.landVillageCode" tooltip-placement="bottom" ng-change="setLandVillageCodeTooltip(agreementData.landVillageCode,$index);">
													<option value="">Village</option>
												</select>
												<p ng-show="GenerateAgreementForm.landVillageCode{{$index}}.$error.required && (GenerateAgreementForm.landVillageCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
											</div>	
											
											
											<!--<td><input type="text" class="form-control1" id="villageName{{$index}}" placeholder="Address" data-ng-model="agreementData.address" name="address{{$index}}" ng-blur="calcRoute(agreementData.start,agreementData.plotDistance,$index);" /></td>-->
											<td><input type="text" class="form-control1" id="villageName{{$index}}" placeholder="Address" data-ng-model="agreementData.address" name="address{{$index}}" /></td>
											</td>
											<td><input type="text" class="form-control1" id="distance" placeholder="Distance in Km" data-ng-model="agreementData.plotDistance" name=" plotDistance{{$index}}" style="" /></td>
											<td>
											<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.landTypeCode{{$index}}.$invalid && (GenerateAgreementForm.landTypeCode{{$index}}.$dirty || submitted)}">											
												<select class="form-control1" tabindex="15" name="landTypeCode{{$index}}" ng-model="agreementData.landTypeCode" ng-options="landTypeCode.id as landTypeCode.landtype for landTypeCode in landTypeNames  | orderBy:'-landtype':true" ng-required="true" uib-tooltip="{{agreementData.landTypeCodeTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.landTypeCode" tooltip-placement="bottom" ng-change="setLandTypeCodeTooltip(agreementData.landTypeCode,$index);">
												<option value="">Land Type</option></select>
												<p ng-show="GenerateAgreementForm.landTypeCode{{$index}}.$error.required && (GenerateAgreementForm.landTypeCode{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
											</div>
											
												
											</td>
											<td>
													
												<div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.surveyNumber{{$index}}.$invalid && (GenerateAgreementForm.surveyNumber{{$index}}.$dirty || submitted)}">										
												<input type="text" class="form-control1" placeholder='Survey No' maxlength="10" tabindex="13" name="surveyNumber{{$index}}" ng-model="agreementData.surveyNumber"  ng-click="showSurveyDialog(agreementData.plotSeqNo,agreementData.plantorRatoon);" ng-focus="showSurveyDialog(agreementData.plotSeqNo,agreementData.plantorRatoon);" ng-if="setupStatus=='0'" readonly uib-tooltip="{{agreementData.surveyNumber}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.surveyNumber" tooltip-placement="bottom"/>
												<input type="text" class="form-control1" placeholder='Survey No' maxlength="10" tabindex="13" name="surveyNumber{{$index}}" ng-model="agreementData.surveyNumber"  ng-required="true" ng-if="setupStatus=='1'" uib-tooltip="{{agreementData.surveyNumber}}" tooltip-trigger="mouseenter" tooltip-enable="agreementData.surveyNumber" tooltip-placement="bottom"/>
												
												<p ng-show="GenerateAgreementForm.surveyNumber{{$index}}.$error.required && (GenerateAgreementForm.surveyNumber{{$index}}.$dirty || submitted)" class="help-block">Required.</p>																																				
											</div>	
													
													
																									
											</td>
											
											
											
											
											
											
											<td>
											
											<div class="fileinput fileinput-new" data-provides="fileinput">
								               <span class="btn btn-primary btn-file m-r-10">
								               		<span class="fileinput-new">Select</span>
									                <span class="fileinput-exists">Change</span>
									                  <input type="file" name="..." tabindex="16" onchange="angular.element(this).scope().uploadExtentImage(this.files,this.id);" id="{{$index}}">	
				  		                       </span>													   									   
										               <span class="fileinput-filename">{{agreementData.extentImage}}</span>
												       <a href="#/harvesting/transaction/OfferMaster" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>													
											   
													  <div class="form-group" ng-class="{ 'has-error' : GenerateAgreementForm.extentImage{{$index}}.$invalid && (GenerateAgreementForm.extentImage{{$index}}.$dirty || submitted)}">
													    <input type="hidden" name="extentImage{{$index}}" ng-model="agreementData.extentImage" ng-required="true" />
														<p ng-show="GenerateAgreementForm.extentImage{{$index}}.$error.required && (GenerateAgreementForm.extentImage{{$index}}.$dirty || submitted)" class="help-block">Required.</p>
													  </div>											   
								           </div>
											</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
							</tabset>			  	
						 
						<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(GenerateAgreementForm)">Reset</button>
								</div>
							</div>						 	
					   </div>
						<input type="hidden" name="seySurveyNumber" ng-model="seySurveyNumber" />
					    
					</form>					  							 		 							 
					<!------------------form end------------------------------>							 
					<!----------Popup box on surveynumber for plant----------->
					<form name="agreementpopup" novalidate>
					   <div class="card popupbox" id="popup_box">						  
						  	  <div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : agreementpopup.ryotCodePop.$invalid && (agreementpopup.ryotCodePop.$dirty || Popsubmitted)}">										
			        	                	<div class="fg-line">										
												<select class="form-control1" name="ryotCodePop" ng-model="ryotCodePop" ng-options="ryotCodePop.id as ryotCodePop.ryotname for ryotCodePop in ryotNamesExtent| orderBy:'-ryotname':true"  ng-required="true" ng-change="loadAddedExtentDetails(ryotCodePop);">
													<option value="">Select Ryot Name</option>
												</select>
			         	       	        	</div>
											<p ng-show="agreementpopup.ryotCodePop.$error.required && (agreementpopup.ryotCodePop.$dirty || Popsubmitted)" class="help-block">Select Ryot Code.</p>								</div>			
			                    		</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper">
				        	                <div class="fg-line">
												<label for="RyotCodePOP">Ryot SB A/C No</label>
												<input type="text" class="form-control" placeholder='Ryot Code' readonly name="ryotNamePop" ng-model="ryotNamePop" id="RyotCodePOP" with-floating-label/>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>															
							</div><br />							
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
                		    					<th>Select</th>											
							                	<th>Plot No.</th>
												<th>Survey No.</th>
			                    				<th>Test Date</th>
												<input type="hidden" name="PlotNumberPop" ng-model="PlotNumberPop" />
												
        	    							</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="surveyData in PopupExtentData">
												<td>
													<div class="checkbox">
										                <label>														
										                    <input type="radio" value="" name="checkSurvey{{$index}}" ng-model="surveyData.checkSurvey" ng-click="setSurveyNumber(surveyData.surveyNo,surveyData.plotSlNumber,PlotNumberPop);">
															
									    	                <i class="input-helper"></i>
														</label>
									            	</div>
												</td>	
												<td>{{surveyData.plotSlNumber}}</td>
												<td>{{surveyData.surveyNo}}</td>
												<td>{{surveyData.testDate}}</td>
											</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="getSurveyNumber();">Get Survey Number</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialog();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form>	
					 
					<!-----------Popup box on croptype dropdown on change----->
					<form name="RatoonPopup" novalidate>
					   <div class="card popupbox_ratoon" id="popup_box">						  
						  	  <div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : RatoonPopup.ryotCodeRatoon.$invalid && (RatoonPopup.ryotCodeRatoon.$dirty || Popsubmitted)}">										
			        	                	<div class="fg-line">										
												<select class="form-control1" name="ryotCodeRatoon" ng-model="ryotCodeRatoon" ng-options="ryotCodeRatoon.id as ryotCodeRatoon.ryotname for ryotCodeRatoon in ryotNamesExtent| orderBy:'-ryotname':true"  ng-required="true" ng-change="loadAddedRatoonDetails(ryotCodeRatoon);">
													<option value="">Select Ryot Name</option>
												</select>
			         	       	        	</div>
											<p ng-show="RatoonPopup.ryotCodeRatoon.$error.required && (RatoonPopup.ryotCodeRatoon.$dirty || Popsubmitted)" class="help-block">Select Ryot Code.</p>								</div>			
			                    		</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group floating-label-wrapper">
				        	                <div class="fg-line">
												<label for="RyotCodeRatoon">Ryot Code</label>
												<input type="text" class="form-control" placeholder='Ryot Code' readonly name="ryotNameRatoon" ng-model="ryotNameRatoon" id="RyotCodeRatoon" with-floating-label/>
				                	        </div>
										</div>
			                    	</div>			                    
				                  </div>															
							</div><br />							
					 		<div class="row">
							  <div class="col-md-12">		
						 		  <div class="table-responsive">
							 		<table class="table table-striped table-vmiddle" id="tab_logic">
							 			<thead>
								        	<tr style="background-color:#FFFFFF;">
                		    					<th>Select</th>											
							                	<th>Agreement No.</th>
												<th>Plot No.</th>
												<th>Survey No.</th>
			                    				<th>Start Date</th>
												<th>End Date</th>
												<th>Mean Date</th>
												
												<input type="hidden" name="PlotNumberRatoon" ng-model="PlotNumberRatoon" />
												<input type="hidden" name="currentIndex" ng-model="currentIndex" />
												
												
        	    							</tr>											
										</thead>
										<tbody>
											<tr ng-repeat="RatoonData in RatoonExtentData">
												<td>
													<div class="checkbox">
										                <label>														
										                    <input type="radio" value="" name="checkSurvey" ng-model="RatoonData.checkSurvey" ng-click="getRatoonDetails(RatoonData.meanDate);">
															
									    	                <i class="input-helper"></i>
														</label>
									            	</div>
												</td>	
												<td>{{RatoonData.agreementNo}}</td>
												<td>{{RatoonData.plotNo}}</td>
												<td>{{RatoonData.surveyNo}}</td>
												<td>{{RatoonData.startDate}}</td>
												<td>{{RatoonData.endDate}}</td>
												<td>{{RatoonData.meanDate}}</td>												
											</tr>
										</tbody>	
								 </table>
							</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Get Planting Date</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CloseDialogRatoon();">Close Dialog</button>
									</div>
								</div>						 	
							 </div>							 							 
				        </div>
			    	</div>
					   </div>				
					 </form> 	
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					
					<!----------end----------------------->										
					
				</div> 
	    </section>   
	</section> 

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
