	<style>
.styletr {border-bottom: 1px solid grey;}


</style>


	<script type="text/javascript">	
		$('.autofocus').focus();


		
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 
		 
	  });
	 
    </script>
	
	
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="grnsController" ng-init="getAllDepartments();">    
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>GRN's</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					 	<form name="GRNForm" novalidate ng-submit="grnsSubmit(AddedGRN,GRNForm);">


						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GRNForm.fromDate.$invalid && (GRNForm.fromDate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="fromDate">From Date</label>
										<input type="text" class="form-control date " data-ng-required='true'  name="fromDate" data-ng-model="AddedGRN.fromDate" placeholder="From Date" tabindex="1"  maxlength="15" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  id="fromDate" with-floating-label/>
									</div>
								<p ng-show="GRNForm.fromDate.$error.required && (GRNForm.fromDate.$dirty || Addsubmitted)" class="help-block">From Date is required</p>		 
								<p ng-show="GRNForm.fromDate.$error.pattern && (GRNForm.fromDate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										
											 
											 <div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : GRNForm.todate.$invalid && (GRNForm.todate.$dirty || submitted)}">												
									<div class="fg-line">
									<label for="todate">To Date</label>
										<input type="text" class="form-control date " data-ng-required='true'  name="todate" data-ng-model="AddedGRN.todate" placeholder="To Date" tabindex="1"  maxlength="15" data-input-mask="{mask: '00-00-0000'}" data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"  id="todate" with-floating-label/>
									</div>
								<p ng-show="GRNForm.todate.$error.required && (GRNForm.todate.$dirty || Addsubmitted)" class="help-block">To Date is required</p>		 
								<p ng-show="GRNForm.todate.$error.pattern && (GRNForm.todate.$dirty || Addsubmitted)" class="help-block">Valid Date is required</p>		 
								</div>
										</div>
									</div>
								</div>
								
							
							</div>
							 

							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											 <div class="form-group" ng-class="{ 'has-error' : GRNForm.addDepartment.$invalid && (GRNForm.addDepartment.$dirty || submitted)}">
											 	<div class="fg-line">
													 <select chosen class="w-100" name="addDepartment" tabindex="3"   data-ng-model="AddedGRN.addDepartment" ng-options="addedDepartment.deptCode as addedDepartment.department for addedDepartment  in DepartmentsData" ng-required='true'>
														<option value=''>Department</option>
													 </select>	
												</div>
												<p ng-show="GRNForm.addDepartment.$error.required && (GRNForm.addDepartment.$dirty || Addsubmitted)" class="help-block">Select Department</p>		 
												
											 </div>
										</div>
									</div>
								</div>
								
							
							</div>
							
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											 <button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton" ng-click='getStockRequestDetails(AddedGRN.fromDate,AddedGRN.todate,AddedGRN.addDepartment);'>Get Details</button>
											 	
												
											 
										</div>
									</div>
								</div>
								
							
							</div>
							
								
						 </div>
						
						 
						 <br />
							<div class='table-responsive' id='hidetable' style="display:none;">
							<table  class="table table-striped" style="border-collapse:collapse;width:100%;">
								<tr class="styletr" style="font-weight:bold;">
									<th>&nbsp;&nbsp;View&nbsp;&nbsp; Edit </th>
									
									<th>GRN No.</th>
									<th>Date</th>
									<th>Items</th>
									<th>Department</th>
									
									
								</tr>
								<tr class="styletr"   ng-repeat="grnData in data">
									<td> 
										<a href="#">
          <span style="font-size:20px;" class="glyphicon glyphicon-eye-open" title="View"></span>
        </a> &nbsp;&nbsp; &nbsp; <a  href="#/store/transaction/GRN"  >
										 &nbsp; &nbsp;<img src="../icons/edit.png" title="Edit" style="height:25px;" ng-click="getUpdatedGRNfromGRNS(grnData.grnNo);" />
										</a>
									</td>
									<td style="display:none;"><input type="hidden" name="status{{$index}}" ng-model="grnData.status"  readonly /></td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="GRN No." name="grnNo{{$index}}" ng-model="grnData.grnNo"  readonly>
									</div>
									</td>
									
									<td>
									<div class="form-group">
									<input type="text" class="form-control1 datepicker" placeholder="Date of Request" maxlength="10" name="dateOfRequest{{$index}}" ng-model="grnData.grnDate"  ng-click="ShowDatePicker();" readonly>
									</div>
									</td>
									<td>
									<div class="form-group" >
									<input type="text" class="form-control1" placeholder="Requested Items" readonly maxlength="20" name="requestItems{{$index}}" ng-model="grnData.Items">									
															
									</div>
									</td>
									<td>
									<div class="form-group">
									<input type="text" class="form-control1" placeholder="Department" maxlength="30" name="department{{$index}}" ng-model="grnData.departmentName" ng-required="true" readonly>									
															
									</div>
									</td>
									
									
									
									
									
								</tr>
								
								
							</table>
							
							</div>
							
						 <br>
						 					
										
						</form>
									 							 
						
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
