	<script type="text/javascript">	
		$('.autofocus').focus();	
	</script>
<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
		 $('.Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="IndentController" ng-init="addFormField();loadseasonNames();loadVarietyNames();loadFieldAssistants();loadFieldOfficerDropdown();GetLoginUserStatus();" >   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Common Indents</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="IndentForm" novalidate ng-submit="IndentsSubmit(AddedIndent,IndentForm)">
					 		<div class="row">
								<div class="col-sm-4">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : IndentForm.season.$invalid && (IndentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100"  tabindex="1"  data-ng-model='AddedIndent.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10">
															<option value="">Season</option>
						        			            </select>	
				        		        	        </div>
													<p ng-show="IndentForm.season.$error.required && (IndentForm.season.$dirty || submitted)" class="help-block">Season</p>													
												</div>
					                    	</div>
										</div>
								<div class="col-sm-8">
									<div class="row">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">&nbsp; &nbsp; &nbsp;Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourSpplr" value="0" ng-model="AddedIndent.status"  >
				    				            	  <i class="input-helper"></i>Requested 
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedIndent.status">
			    						            <i class="input-helper"></i>Approved
					  				              </label>
												  <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="2"  ng-model="AddedIndent.status">
			    						            <i class="input-helper"></i>Pending
					  				              </label>
												  <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="3"  ng-model="AddedIndent.status">
			    						            <i class="input-helper"></i>All
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
								</div>
							</div>
							
							<div class="row">
										<div class="col-sm-3">
											
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : IndentForm.date.$invalid && (IndentForm.date.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">From Date</label>
														<input type="text" class="form-control date autofocus" placeholder='From Date ' id="date" with-floating-label  name="date" ng-model="AddedIndent.fromDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}"  maxlength="12"/>	
				        		        	        </div>
													<p ng-show="IndentForm.date.$error.required && (IndentForm.date.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										
										</div>
										<div class="col-sm-3">
											
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : IndentForm.toDate.$invalid && (IndentForm.toDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="todate">To Date</label>
														<input type="text" class="form-control date" placeholder='To Date ' id="todate" with-floating-label  name="toDate" ng-model="AddedIndent.toDate" readonly="readonly"  tabindex="3" data-input-mask="{mask: '00-00-0000'}"  maxlength="12" ng-blur="getTrayfillingList(AddedIndent.season,AddedIndent.date,AddedIndent.shift);"/>	
				        		        	        </div>
													<p ng-show="IndentForm.toDate.$error.required && (IndentForm.toDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										
										</div>
										<div class="col-sm-3">
									<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : IndentForm.fieldMan.$invalid && (IndentForm.fieldMan.$dirty || submitted)}">
						        	                <div class="fg-line">
													<select  chosen class="w-100" name="village" ng-model="AddedIndent.fieldMan" tabindex="4" ng-required="true" ng-options="fieldMan.id as fieldMan.fieldassistant for fieldMan in AssistantNames | orderBy:'fieldassistant':true">
													<option value="">Field Assistant</option>
												</select>
													</div>
													<p ng-show="IndentForm.fieldMan.$error.required && (IndentForm.fieldMan.$dirty || submitted)" class="help-block">Select Field Assistant</p>
													</div>
													
													</div>
									</div>
									<div class="col-sm-3">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : IndentForm.varietyOfCane.$invalid && (IndentForm.varietyOfCane.$dirty || Filtersubmitted)}">	
			        	                <div class="fg-line">
            					          <div class="select">
                				    		<select chosen class="w-100"   data-placeholder="Select Variety" name="varietyOfCane" data-ng-model='AddedIndent.varietyOfCane' ng-options="varietyOfCane.id as varietyOfCane.variety for varietyOfCane in varietyNames | orderBy:'-variety':true" tabindex="5">
												<option value="">Variety</option>
						                       											
				        		            </select>
                	            		  </div>
			                	        </div>
										<p ng-show="IndentForm.varietyOfCane.$error.required && (IndentForm.varietyOfCane.$dirty || Filtersubmitted)" class="help-block">Select Variety </p>
										</div>
										
			                    	</div>			                    
				                  </div>
							</div>
							
							
					</form>		
							<div style="text-align:center;"><button type="button" class="btn btn-primary" ng-click="loadIndents(AddedIndent,IndentForm);">Load Indents</button></div>
							<br /><hr />	
											 		
						 
							
							<!----------table grid design--------->
					
											
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
										<table class="table table-striped table-vmiddle">
											<thead>
												<tr>
													
													<th>Select</th>
													<th>Indent</th>
													<th>Date Of Indent</th>
													<th>Ryot Code </th>
													<th>Field Officer</th>
													<th>Modify Indent</th>
													<th>Authorization</th>
													
														
													
													
												</tr>											
											</thead>
											<tbody>
												<tr ng-repeat="BudData in data">
													
													<td>
													<div class="checkbox">
															<label class="checkbox checkbox-inline m-r-20">
																<input type="checkbox" class="checkbox" name="hrvFlag"  ng-model="BudData.hrvFlag" ng-change="optionToggled()"><i class="input-helper"></i>
															</label>
														</div>
													</td>
																							
													<td>
		<div class="form-group" ng-class="{ 'has-error' : IndentForm.indent{{BudData.id}}.$invalid && (IndentForm.indent{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
													<input type="text" placeholder="Indent" id="indent"  class="form-control1"  name="indent{{BudData.id}}" placeholder ="Batch No" ng-model="BudData.indent" tabindex="6"> 
											
		
		<p ng-show="IndentForm.indent{{BudData.id}}.$error.required && (IndentForm.indent{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="IndentForm.indent{{BudData.id}}.$error.pattern && (IndentForm.indent{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
												
		</div>	
		
		
								</td>
								<td>
												<div class="form-group" ng-class="{ 'has-error' : IndentForm.dateofIndent{{$index}}.$invalid && (IndentForm.dateofIndent{{$index}}.$dirty || submitted)}">										
												<input type="text" class="form-control1 datepicker" placeholder='Date of Indent' maxlength="10" tabindex="7" name="dateofIndent{{BudData.id}}" ng-model="BudData.dateofIndent" data-input-mask="{mask: '00-00-0000'}" ng-mouseover="ShowDatePicker();"  ng-required="true"  data-ng-pattern="/(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/"/>
												<p ng-show="IndentForm.dateofIndent{{$index}}.$error.required && (IndentForm.dateofIndent{{$index}}.$dirty || submitted)" class="help-block">Required.</p>									
												<p ng-show="IndentForm.dateofIndent{{$index}}.$error.pattern && (IndentForm.dateofIndent{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
											</div>
											</td>
								
													
												<td>
								
		<div class="form-group" ng-class="{ 'has-error' : IndentForm.ryotCode{{BudData.id}}.$invalid && (IndentForm.ryotCode{{BudData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
		<!--<select class="form-control1"   data-placeholder="Select Variety" name="varietyOfCane{{BudData.id}}" data-ng-model='BudData.varietyOfCane' ng-options="varietyOfCane.id as varietyOfCane.variety for varietyOfCane in varietyNames | orderBy:'-variety':true" tabindex="8">
												<option value="">Variety</option>
						                       											
				        		            </select>-->
										<input type='text' class="form-control1"   placeholder="Ryot Code" name="ryotCode{{BudData.id}}" data-ng-model='BudData.ryotCode' readonly>	
															
		<p ng-show="IndentForm.ryotCode{{BudData.id}}.$error.required && (IndentForm.ryotCode{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="IndentForm.ryotCode{{BudData.id}}.$error.pattern && (IndentForm.ryotCode{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
		</div>												
													</td>    
													
													
													
													<td>
		<div class="form-group" ng-class="{ 'has-error' : IndentForm.fieldOfficer{{BudData.id}}.$invalid && (IndentForm.fieldOfficer{{BudData.id}}.$dirty || Addsubmitted)}">										
													<!--<select  class="form-control1"   data-placeholder="Field Officer" name="fieldOfficer{{$index}}" data-ng-model='BudData.fieldOfficer' ng-options="fieldOfficer.id as fieldOfficer.employeename for fieldOfficer in FieldOffNames | orderBy:'-employeename':true" ng-change="fieldOff(BudData.fieldOfficer);" tabindex="9">
												<option value="">Field Officer</option>
						                       											
				        		            </select>-->
											<input type="text" class="form-control1" name="fieldOfficer{{$index}}" ng-model="BudData.fieldOfficer" placeholder="Field Officer" />
		<p ng-show="IndentForm.variety{{BudData.id}}.$error.required && (IndentForm.variety{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
		<p ng-show="IndentForm.variety{{BudData.id}}.$error.pattern && (IndentForm.variety{{BudData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>											
		</div>											
												</td> 
												<td>
												<div class="form-group" >	
												<span class="btn btn" style="color:#FFFFFF;" ><a  href="#/seedling/transaction/seedlingIndent" ng-click="modifyIndent(BudData.indent);"  >Modify</a></span>

												</div>
												</td>
												<td>
												<div class="form-group" >	
												<span class="btn btn" style="color:#FFFFFF;" ><a  href="#/seedling/transaction/AuthorisationFertilizers" ng-click="authorizationIndent(AddedIndent.season,BudData.ryotCode);"  >Authorization</a></span>

												</div>
												</td>  
												<td>
												<div class="form-group" >	
											<!--	<span class="btn btn" style="color:#FFFFFF;" ng-click="rejectIndent(AddedIndent.season,BudData.ryotCode,BudData.indent);">Reject</span>-->
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide " ng-click="rejectIndent(AddedIndent.season,BudData.ryotCode,BudData.indent);">Reject</button>
												</div>
												</td>  
												
												
													</tr>
													
											</tbody>
										</table>
									</div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
