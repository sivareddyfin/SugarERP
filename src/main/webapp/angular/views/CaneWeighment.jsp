	<script>
	
	//  $(function() 
//	  {
//	    // $( "#date" ).datepicker({
////    		  changeMonth: true,
////		      changeYear: true,
////			  dateFormat: 'dd-mm-yy'
////	     });		 	
//		 $('#date').focus();	 		 
//		 //$('#Time').timepicker({ 'scrollDefault': 'now' });
//	  });
(function () {
    function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
        document.getElementById('time1').value = h + ":" + m + ":" + s;
		//$scope.AddCaneWeight.caneWeighmentTime = h + ":" + m + ":" + s;
		//alert( h + ":" + m + ":" + s);
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
})();
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CaneWeighment" ng-init="loadServerDate();loadCaneWeightSeasonNames();loadWeighBridgeNames();loadUCNames();loadServerTime();loadCaneWeighMaxId();loadShiftBridgeDetails();getLoginUser();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Cane Weighment</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>	
								 	
					 	<form name="CaneWeighmentForm" novalidate ng-submit="SaveCaneWeighment(AddCaneWeight,CaneWeighmentForm);">
							  <input type="hidden" name="shiftTime" ng-model="AddCaneWeight.shiftTime" />
							  <div class="row">
							  	<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CaneWeighmentForm.season.$invalid && (CaneWeighmentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select class="w-100" chosen name="season" ng-model="AddCaneWeight.season" tabindex="1" ng-required="true" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true">
															<option value="">Select Season</option>
														</select>
				                	    		    </div>
													<p ng-show="CaneWeighmentForm.season.$error.required && (CaneWeighmentForm.season.$dirty || submitted)" class="help-block">Season is Required.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : CaneWeighmentForm.caneWeighmentDate.$invalid && (CaneWeighmentForm.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="caneWeighmentDate" ng-model="AddCaneWeight.caneWeighmentDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-change="getShiftId(AddCaneWeight.caneWeighmentDate);"/>	
				        		        	        </div>
													<p ng-show="CaneWeighmentForm.caneWeighmentDate.$error.required && (CaneWeighmentForm.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : CaneWeighmentForm.caneWeighmentTime.$invalid && (CaneWeighmentForm.caneWeighmentTime.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="time1">Time</label>
														<!--<input type="text" class="form-control" placeholder='Time' id="time1" with-floating-label name="caneWeighmentTime" ng-model="AddCaneWeight.caneWeighmentTime"  tabindex="3" ng-required="true" data-input-mask="{mask: '00:00:00'}" maxlength="10" readonly="readonly"/>-->
														 <input id="time1" class="form-control" placeholder='Time'  with-floating-label name="caneWeighmentTime" ng-model="AddCaneWeight.caneWeighmentTime"  tabindex="3" ng-required="true" data-input-mask="{mask: '00:00:00'}" maxlength="10" readonly="readonly">		
				                			        </div>
													<p ng-show="CaneWeighmentForm.caneWeighmentTime.$error.required && (CaneWeighmentForm.caneWeighmentTime.$dirty || submitted)" class="help-block">Time is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							  <div class="row">
							  	<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Shift">Shift</label>
														<input type="text" class="form-control" placeholder='Shift' id="Shift"  with-floating-label name="shiftId" ng-model="AddCaneWeight.shiftId" readonly />	
				            		    	        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				           		 	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneWeighmentForm.weighBridgeId.$invalid && (CaneWeighmentForm.weighBridgeId.$dirty || submitted)}">
						        	                <div class="fg-line">
														<!--<label for="WeighBridge">Weigh Bridge</label>-->
														<!--<input type="text" class="form-control" placeholder='Weigh Bridge' id="WeighBridge" readonly with-floating-label name="weighBridge" ng-model="AddCaneWeight.weighBridge"/>-->
														<select chosen class="w-100" name="weighBridgeId" ng-model="AddCaneWeight.weighBridgeId" ng-required="true" ng-options="weighBridgeId.id as weighBridgeId.bridgename for weighBridgeId in WeighBridgeNames">
															<option value="">Select Bridge</option>
														</select>
				                			        </div>
													<p ng-show="CaneWeighmentForm.weighBridgeId.$error.required && (CaneWeighmentForm.weighBridgeId.$dirty || submitted)" class="help-block">Weigh Bridge is Required.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							  <div class="row">
							  	<div class="col-sm-2">
								
									<div class="row applybgColorRed" >
										<div class="col-sm-12">
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  ">
						        	                <p style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif;"><span >Shift A</span></p>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row applybgColorBlue">
										<div class="col-sm-12">
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  ">
						        	             <p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif;"><span style=''>Shift B</span></p>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row applybgColorGreen">
										<div class="col-sm-12">
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group ">
						        	                  <p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif;"><span>Shift C</span></p>
												</div>
					                    	</div>
										</div>
									</div>									
								</div>
							  	<div class="col-sm-3 ">
								
									<div class="row  ">
										<div class="col-sm-12">
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts1wt">Receipts 1wt</label>
														<input type="text" class="form-control" placeholder='Receipts 1wt' id="Receipts1wt" readonly with-floating-label name="recieptWeightOneForShiftOne" ng-model="AddCaneWeight.recieptWeightOneForShiftOne"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts12wt">Receipts 1wt</label>
														<input type="text" class="form-control" placeholder='Receipts 1wt' id="Receipts12wt" readonly with-floating-label name="recieptWeightOneForShiftTwo" ng-model="AddCaneWeight.recieptWeightOneForShiftTwo"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-control floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts3wt">Receipts 1wt</label>
														<input type="text" class="form-control" placeholder='Receipts 1wt' id="Receipts3wt" readonly with-floating-label name="recieptWeightOneForShiftThree" ng-model="AddCaneWeight.recieptWeightOneForShiftThree"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts2wt">Receipts 2wt</label>
														<input type="text" class="form-control" placeholder='Receipts 2wt' id="Receipts2wt" readonly with-floating-label name="recieptWeightTwoForShiftOne" ng-model="AddCaneWeight.recieptWeightTwoForShiftOne"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
    	        				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts22wt">Receipts 2wt</label>
														<input type="text" class="form-control" placeholder='Receipts 2wt' id="Receipts22wt" readonly with-floating-label name="recieptWeightTwoForShiftTwo" ng-model="AddCaneWeight.recieptWeightTwoForShiftTwo"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Receipts23wt">Receipts 2wt</label>
														<input type="text" class="form-control" placeholder='Receipts 2wt' id="Receipts23wt" readonly with-floating-label name="recieptWeightTwoForShiftThree" ng-model="AddCaneWeight.recieptWeightTwoForShiftThree"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
				        	                		<div class="fg-line">
														<label for="Quantity1">Quantity</label>
														<input type="text" class="form-control" placeholder='Quantity' id="Quantity1" readonly with-floating-label name="totalWeightForShiftOne" ng-model="AddCaneWeight.totalWeightForShiftOne"/>
				                	        		</div>
												</div>
			                    			</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
				        	                		<div class="fg-line">
														<label for="Quantity2">Quantity</label>
														<input type="text" class="form-control" placeholder='Quantity' id="Quantity2" readonly with-floating-label name="totalWeightForShiftTwo" ng-model="AddCaneWeight.totalWeightForShiftTwo"/>
				                	        		</div>
												</div>
			                    			</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
				        	                		<div class="fg-line">
														<label for="Quantity3">Quantity</label>
														<input type="text" class="form-control" placeholder='Quantity' id="Quantity3" readonly with-floating-label name="totalWeightForShiftThree" ng-model="AddCaneWeight.totalWeightForShiftThree"/>
				                	        		</div>
												</div>
			                    			</div>
										</div>
									</div>								
								</div>
							  </div>
							 
							  <div class="row">
							  	<div class="col-sm-3">									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Weighment :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="weighmentorder" value="0" ng-model="AddCaneWeight.weighmentorder" ng-change="changeSerialNumStat(AddCaneWeight.weighmentorder);">
				    				            	  <i class="input-helper"></i>1
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="weighmentorder" value="1" ng-model="AddCaneWeight.weighmentorder"  ng-change="changeSerialNumStat(AddCaneWeight.weighmentorder);">
			    						            <i class="input-helper"></i>2
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floaing-label-wrapper" ng-class="{ 'has-error' : CaneWeighmentForm.serialNumber.$invalid && (CaneWeighmentForm.serialNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="SerialNumber">Serial Number</label>
														<input type="text" class="form-control" placeholder='Serial Number' id="SerialNumber" maxlength="10" with-floating-label name="serialNumber" ng-model="AddCaneWeight.serialNumber" tabindex="4" maxlength="10" data-ng-pattern="/^[0-9]+$/" ng-blur="serialValidation(AddCaneWeight.serialNumber);"  ng-required="weightStatus" ng-readonly="readonlyStatus"/> 
				                			        </div>
<p ng-show="CaneWeighmentForm.serialNumber.$error.required && (CaneWeighmentForm.serialNumber.$dirty || submitted)" class="help-block">Serial Number is Required.</p>												
<p ng-show="CaneWeighmentForm.serialNumber.$error.pattern && (CaneWeighmentForm.serialNumber.$dirty || submitted)" class="help-block">Enter Valid Serial Number.</p>
<p id="validateSerialError" style="display:none; color:#FF0000;">Permit is already closed for the given serial number. Please enter valid serial number and try again</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CaneWeighmentForm.grossTare.$invalid && (CaneWeighmentForm.grossTare.$dirty || submitted)}">
						        	                <div class="fg-line">
														<!--<label for="GrossTare">Gross Tare</label>-->
														<!--<input type="text" class="form-control" placeholder='Gross Tare' id="GrossTare" with-floating-label name="grossTare" ng-model="AddCaneWeight.grossTare" tabindex="5" ng-required="true"/>-->
														<select chosen class="w-100" name="grossTare" ng-model="AddCaneWeight.grossTare" tabindex="5" ng-required="true" ng-options="grossTare.id as grossTare.grossTare for grossTare in GrossTareData| orderBy:'-grossTare':true" ng-disabled="AddCaneWeight.weighmentorder!=''" >
															<option value="">Select Gross Tare</option>
														</select>
				                			        </div>
<p ng-show="CaneWeighmentForm.grossTare.$error.required && (CaneWeighmentForm.grossTare.$dirty || submitted)" class="help-block">Gross Tare is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">											
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneWeighmentForm.permitNumber.$invalid && (CaneWeighmentForm.permitNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="PermitNumber">Permit Number</label>
														<input type="text" class="form-control" placeholder='Permit Number' autofocus id="PermitNumber" maxlength="10" with-floating-label name="permitNumber" ng-model="AddCaneWeight.permitNumber" ng-blur="validatePermitNumber(AddCaneWeight.permitNumber);" tabindex="6" ng-required="true" data-ng-pattern="/^[0-9]+$/" ng-readonly="readonlyStatusforPermit"/>
														
				                			        </div>
<p ng-show="CaneWeighmentForm.permitNumber.$error.required && (CaneWeighmentForm.permitNumber.$dirty || submitted)" class="help-block">Permit Number is Required.</p>
<p ng-show="CaneWeighmentForm.permitNumber.$error.pattern && (CaneWeighmentForm.permitNumber.$dirty || submitted)" class="help-block">Enter Valid Permit Number.</p>													

												</div>																																																		
					                    	</div>
										</div>
									</div>
								</div>
							  </div><br />							
							  
							 <input type="hidden" name="extentSize" ng-model="AddCaneWeight.extentSize" />							
							 <input type="hidden" name="varietyCode" ng-model="AddCaneWeight.varietyCode" />							
							 <input type="hidden" name="villageCode" ng-model="AddCaneWeight.villageCode" />							
							 <input type="hidden" name="circleCode" ng-model="AddCaneWeight.circleCode" />		
							 <input type="hidden" name="agreementNumber" ng-model="AddCaneWeight.agreementNumber" />
							 <input type="hidden" name="plantOrRatoon" ng-model="AddCaneWeight.plantOrRatoon" />
							 <input type="hidden" name="plotNumber" ng-model="AddCaneWeight.plotNumber" />		
							 <input type="hidden" name="zoneCode" ng-model="AddCaneWeight.zoneCode" />
							 <input type="hidden" name="programNumber" ng-model="AddCaneWeight.programNumber" />							 					 	
							 						
							  <div class="row">
							  	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="RyotCode">Ryot Code</label>
														<input type="text" class="form-control" placeholder='Ryot Code' id="RyotCode" readonly with-floating-label name="ryotCode" ng-model="AddCaneWeight.ryotCode"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="PlantRatoon">Plant / Ratoon</label>
														<input type="text" class="form-control" placeholder='Plant / Ratoon' id="PlantRatoon" readonly with-floating-label name="plant" ng-model="AddCaneWeight.plant"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="RyotName">Ryot Name</label>
														<input type="text" class="form-control" placeholder='Ryot Name' id="RyotName" readonly with-floating-label name="ryotName" ng-model="AddCaneWeight.ryotName"/>														
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="CaneVariety">Cane Variety</label>
														<input type="text" class="form-control" placeholder='Cane Variety' id="CaneVariety" readonly with-floating-label name="variety" ng-model="AddCaneWeight.variety"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="FHGName">F_H_G Name</label>
														<input type="text" class="form-control" placeholder='F_H_G Name' id="FHGName" readonly with-floating-label name="relativeName" ng-model="AddCaneWeight.relativeName"/>
				                	    		    </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Circle">Circle</label>
														<input type="text" class="form-control" placeholder='Circle' id="Circle" readonly with-floating-label name="circle" ng-model="AddCaneWeight.circle"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Village">Village</label>
														<input type="text" class="form-control" placeholder='Village' id="Village" readonly with-floating-label name="village" ng-model="AddCaneWeight.village"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="LVCode">LV Code</label>
														<input type="text" class="form-control" placeholder='LV Code' id="LVCode" readonly with-floating-label name="landVillageCode" ng-model="AddCaneWeight.landVillageCode"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
							  </div>
							
							 <div class="row">
							 	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				           		 	<span class="input-group-addon">Burnt Cane :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
										            <input type="radio" name="burntCane" value="0" ng-model="AddCaneWeight.burntCane" ng-disabled="readonlyStatusSecond">
			    					            	<i class="input-helper"></i>Yes
										          </label>
				 			            		<label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="burntCane" value="1" ng-model="AddCaneWeight.burntCane" ng-disabled="readonlyStatusSecond">
					    				            <i class="input-helper"></i>No
							  		              </label>
			            		    	        </div>
			                    			</div>
										</div>
									</div>
								</div>
								<div class="col-sm-9">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Vehicle :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					        <label class="radio radio-inline m-r-5px" >
										            <input type="radio" name="vehicle" value="0" ng-model="AddCaneWeight.vehicle" ng-change="loadUCC(AddCaneWeight.vehicle);"  >
			    					            	<i class="input-helper"></i>Tractor
										          </label>
				 			            		<label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="vehicle" value="1" ng-model="AddCaneWeight.vehicle" ng-change="loadUCC(AddCaneWeight.vehicle);">
					    				            <i class="input-helper"></i>Small Lorry
							  		              </label>
				 					             <label class="radio radio-inline m-r-5px" >
				            					    <input type="radio" name="vehicle" value="2" ng-model="AddCaneWeight.vehicle" ng-change="loadUCC(AddCaneWeight.vehicle);">
			    				        		    <i class="input-helper"></i>Big Lorry
							  		              </label>								  
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-5" style="display:none;">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Vehicle Type :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					        <label class="radio radio-inline m-r-5px" >
										            <input type="radio" name="vehicleType" value="0" ng-model="AddCaneWeight.vehicleType" ng-disabled="readonlyStatusSecond">
			    					            	<i class="input-helper"></i>Tipper
										          </label>
				 			            		<label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="vehicleType" value="1" ng-model="AddCaneWeight.vehicleType" ng-disabled="readonlyStatusSecond">
					    				            <i class="input-helper"></i>10 Tyre
							  		              </label>
						 			             <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="vehicleType" value="2" ng-model="AddCaneWeight.vehicleType" ng-disabled="readonlyStatusSecond">
			    						            <i class="input-helper"></i>12 Tyre
							  		              </label>										  
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
							 </div><br />
							 
							 <div class="row">
							 	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneWeighmentForm.vehicleNumber.$invalid && (CaneWeighmentForm.vehicleNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="VehicleNumber">Vehicle Number</label>
														<input type="text" class="form-control" placeholder='Vehicle Number' id="VehicleNumber" maxlength="12" with-floating-label name="vehicleNumber" ng-model="AddCaneWeight.vehicleNumber" tabindex="7" ng-required="true" data-ng-pattern="/^[a-zA-Z0-9 ]+$/" ng-readonly="readonlyStatusforPermit"/>
				                			        </div>
<p ng-show="CaneWeighmentForm.vehicleNumber.$error.required && (CaneWeighmentForm.vehicleNumber.$dirty || submitted)" class="help-block">Vehicle Number is Required.</p>
<p ng-show="CaneWeighmentForm.vehicleNumber.$error.pattern && (CaneWeighmentForm.vehicleNumber.$dirty || submitted)" class="help-block">Enter Valid Vehicle Number.</p>													
													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Part Load :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
															<label class="radio radio-inline m-r-5px" >
										            <input type="radio" name="partLoad" value="0" ng-model="AddCaneWeight.partLoad" ng-change="changePartloadStatus(AddCaneWeight.partLoad);" ng-disabled="readonlyStatusSecond">
			    					            	<i class="input-helper"></i>Yes
										          </label>
											<label class="radio radio-inline m-r-5px" >			
										 <input type="radio" name="partLoad" value="1" ng-model="AddCaneWeight.partLoad"  ng-change="changePartloadStatus(AddCaneWeight.partLoad);" ng-disabled="readonlyStatusSecond">
					    				            <i class="input-helper"></i>No
							  		              </label>
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : CaneWeighmentForm.secondPermitNumber.$invalid && (CaneWeighmentForm.secondPermitNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="SecondPermitNum">Second Permit Number</label>
														<input type="text" class="form-control" placeholder='Second Permit Number' id="SecondPermitNum" maxlength="10" with-floating-label name="secondPermitNumber" ng-model="AddCaneWeight.secondPermitNumber" tabindex="8" data-ng-pattern="/^[0-9]+$/" ng-required="partloadStatus" ng-readonly="readonlyStatusSecond" ng-disabled="AddCaneWeight.partLoad==1"/>
				                			        </div>
													<p ng-show="CaneWeighmentForm.secondPermitNumber.$error.required && (CaneWeighmentForm.secondPermitNumber.$dirty || submitted)" class="help-block">Second Permit Number is Required.</p>
													<p ng-show="CaneWeighmentForm.secondPermitNumber.$error.pattern && (CaneWeighmentForm.secondPermitNumber.$dirty || submitted)" class="help-block">Enter Valid Second Permit Number.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CaneWeighmentForm.unloadingContractor.$invalid && (CaneWeighmentForm.unloadingContractor.$dirty || submitted)}">
					        	                <div class="fg-line">
														<select chosen class="w-100" name="unloadingContractor" ng-model="AddCaneWeight.unloadingContractor" tabindex="9" ng-options="unloadingContractor.id as unloadingContractor.name for unloadingContractor in UCNames | orderBy:'-name':true" ng-disabled="readonlyStatusSecond" ng-change="setContractorCode(AddCaneWeight.unloadingContractor);">
															<option value="">Select Unloading Contractor</option>
														</select>
					                	        </div>
												<p ng-show="CaneWeighmentForm.unloadingContractor.$error.required && (CaneWeighmentForm.unloadingContractor.$dirty || submitted)" class="help-block">Unloading Contractor is Required.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							 </div><br />
							
							 <div class="row">
							 	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Particulars">Particulars</label>
														<input type="text" class="form-control" placeholder='Unloading Contractor Code' id="Particulars" readonly with-floating-label name="particulars" ng-model="AddCaneWeight.particulars"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="OperatorName">Operator Name</label>
														<input type="text" class="form-control" placeholder='Operator Name' id="OperatorName" readonly with-floating-label name="operatorName" ng-model="AddCaneWeight.operatorName"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : CaneWeighmentForm.totalWeight.$invalid && (CaneWeighmentForm.totalWeight.$dirty || submitted)}">
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="TotalWeight">Total Wt.</label>
														
														<input type="text" readonly class="form-control" placeholder='Total Wt.' id="TotalWeight"  with-floating-label name="totalWeight" ng-model="AddCaneWeight.totalWeight" style="font-size:20px; font-weight:lighter;" data-ng-required="true" />
				                			        </div>
												</div>
												
												<p ng-show="CaneWeighmentForm.totalWeight.$error.required && (CaneWeighmentForm.totalWeight.$dirty || submitted)" class="help-block">Total Weight is required.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
													<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="GetCaneWeightbyServlet()">Get CaneWeight</button>
														
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							 </div><br />								  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(CaneWeighmentForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
