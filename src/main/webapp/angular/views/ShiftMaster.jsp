
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok' data-ng-controller="ShiftMaster" ng-init="addShiftField();loadAllAddedShifts();">    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
	 <form name="ShiftMasterForm" novalidate ng-submit="SaveShiftMAster(totalHrs,ShiftMasterForm)">
    	<section id="content">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Shift Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					<div class="row">
						  <div class="col-md-1"></div>
						  	<div class="col-md-10">
						 	 	<div class="table-responsive">
							 	 	<section class="asdok">
										<div class="container1">
							   				<table class="table table-striped table-vmiddle">
							 	  		<thead>
								     <tr>
                		    			<th><span>Actions</span></th>
										<th><span>Shift Name</span></th>
							            <th><span>From Time</span></th>
		                    			<th><span>To Time</span></th>
										<th><span>No.of Hrs</span></th>											
            						  </tr>											
										</thead>
									<tbody>
									  <tr ng-repeat="ShiftData in data">
										<td>
											<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addShiftField();" ng-if="ShiftData.shiftId=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
											<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeShiftRow(ShiftData.shiftId);" ng-if="ShiftData.shiftId!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>	
																				
										</td>
										<td>
<div class="form-group" ng-class="{ 'has-error' : ShiftMasterForm.shiftName{{ShiftData.shiftId}}.$invalid && (ShiftMasterForm.shiftName{{ShiftData.shiftId}}.$dirty || submitted)}">										
											<input type="text" class="form-control1" placeholder="Shift Name" maxlength='5' autofocus name="shiftName{{ShiftData.shiftId}}" ng-model='ShiftData.shiftName' data-ng-required='true' ng-keyUp="setValidationForName(ShiftData.shiftId);" id="name{{ShiftData.shiftId}}"/>
<p ng-show="ShiftMasterForm.shiftName{{ShiftData.shiftId}}.$error.required && (ShiftMasterForm.shiftName{{ShiftData.shiftId}}.$dirty || submitted)" class="help-block">Required</p>
<p class="help-block Errorname" style="color:#FF0000; display:none;" id="errorName{{ShiftData.shiftId}}">Invalid</p>											
</div>											
										</td>    
										<td>
<div class="form-group" ng-class="{ 'has-error' : ShiftMasterForm.fromTime{{ShiftData.shiftId}}.$invalid && (ShiftMasterForm.fromTime{{ShiftData.shiftId}}.$dirty || submitted)}">										
											<input type="text" class="form-control1 time" placeholder="From Time" maxlength='11' name="fromTime{{ShiftData.shiftId}}" ng-model='ShiftData.fromTime' ng-mouseover="displayTime(ShiftData.shiftId,'from');" data-ng-required='true' id="from{{ShiftData.shiftId}}" ng-focus="displayTime(ShiftData.shiftId,'from');" ng-change="CalculateShiftHrs();" ng-blur="setValidation(ShiftData.shiftId);"/>
<p ng-show="ShiftMasterForm.fromTime{{ShiftData.shiftId}}.$error.required && (ShiftMasterForm.fromTime{{ShiftData.shiftId}}.$dirty || submitted)" class="help-block">Required</p>
<p class="help-block Errorfrom" style="color:#FF0000; display:none;" id="error{{ShiftData.shiftId}}">Invalid</p>											
</div>											
										</td>  
										<td>
<div class="form-group" ng-class="{ 'has-error' : ShiftMasterForm.toTime{{ShiftData.shiftId}}.$invalid && (ShiftMasterForm.toTime{{ShiftData.shiftId}}.$dirty || submitted)}">										
											<input type="text" class="form-control1 time" placeholder="To Time" maxlength='11' name="toTime{{ShiftData.shiftId}}" ng-model='ShiftData.toTime' ng-mouseover="displayTime(ShiftData.shiftId,'to');"  data-ng-required='true' id="to{{ShiftData.shiftId}}" ng-focus="displayTime(ShiftData.shiftId,'to');" ng-change="CalculateShiftHrs();" ng-blur="setValidation(ShiftData.shiftId);"/>
<p ng-show="ShiftMasterForm.toTime{{ShiftData.shiftId}}.$error.required && (ShiftMasterForm.toTime{{ShiftData.shiftId}}.$dirty || submitted)" class="help-block">Required</p>	
<p class="help-block Errorto" style="color:#FF0000; display:none;" id="errorto{{ShiftData.shiftId}}">Invalid</p>										
</div>											
										</td>
										<td>
<div class="form-group" ng-class="{ 'has-error' : ShiftMasterForm.shiftHrs{{ShiftData.shiftId}}.$invalid && (ShiftMasterForm.toTime{{ShiftData.shiftId}}.$dirty || submitted)}">										
											<input type="text" class="form-control1"  placeholder="No.of Hrs" maxlength='10' name="shiftHrs{{ShiftData.shiftId}}"  readonly  data-ng-required='true' ng-value='ShiftData.fromTime' ng-model='ShiftData.shiftHrs' id="hrs{{ShiftData.shiftId}}"/>
<p ng-show="ShiftMasterForm.shiftHrs{{ShiftData.shiftId}}.$error.required && (ShiftMasterForm.shiftHrs{{ShiftData.shiftId}}.$dirty || submitted)" class="help-block">Required</p>											
</div>										
<input type='hidden' name="totalHrs" ng-model='ShiftData.totalHrs'>	
										</td>
									 </tr>
									</tbody>
									
									 
									
							 	 </table>
								
							  	</div>
							  </section>
							  <table align="right">
								 <tr>
										<td colspan='4' style='text-align:right;'><b>Total Hrs</b></td>
										<td>
<div class="form-group" ng-class="{ 'has-error' : ShiftMasterForm.totalHrs.$invalid && (ShiftMasterForm.totalHrs.$dirty || submitted)}">																				
											<input type='text' class='form-control1' placeholder="Total Hrs" maxlength="10" name="totalHrs" ng-model='totalHrs' readonly>
											<p class="help-block" style="color:#FF0000;" ng-if="totalHrs!='24:00:00'">Total shift hrs are not matching to the total hrs of the day. Please check once.</p>	
										</div>
																			
										</td>
									 </tr>
							</table>
						   </div>	  
						</div>
						<div class="col-md-1"></div>
					  </div>
					  <div class="row" align="center">            			
						<div class="input-group">
							<div class="fg-line">
								<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" id="saveButton" ng-disabled="ShiftMasterForm.$invalid || totalHrs!='24:00:00'">Save</button>
								<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="loadAllAddedShifts();">Reset</button>
							</div>
						</div>						 	
					 </div>	
				   </form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				 </div>
			   </div>					
		   </div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
