
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="UpdateSoilTestResults" ng-init='loadRyotExtentNames();'>      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Soil Test Results</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="recommendationName" novalidate>
					 <!--<dialog id="window">  -->
			    				<div class="card" id="popup_box">
							        <div class="card-body card-padding">
										<div class="row">
											<div class="col-sm-4">
							                    <div class="input-group">
            							            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        				                <div class="fg-line">
													   <input type="text" class="form-control" placeholder='Ryot Code' readonly name="rCode" ng-model='Recommendation.rCode'/>
						                	        </div>
						                    	</div>			                    
				        			          </div>
											  <div class="col-sm-4">
							                    <div class="input-group">
            							            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        				                <div class="fg-line">
													   <input type="text" class="form-control" placeholder='Ryot Name' readonly name="rName" ng-model='Recommendation.rName'/>
						                	        </div>
						                    	</div>			                    
							                  </div> 
											  <div class="col-sm-4">
							                    <div class="input-group">
			            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						        	                <div class="fg-line">
														<input type="text" class="form-control" placeholder='Survey Number' readonly name="surveyNo" ng-model='Recommendation.surveyNo'/>
			            			    	        </div>
			                    				</div>			                    
							                  </div>							
										</div><br /><hr />							
										<div class="row">
											<div class="col-sm-2"></div>
											<div class="col-sm-6">
							                <div class="input-group">
			            				     <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : recommendationName.recommendation.$invalid && (recommendationName.recommendation.$dirty || Resubmitted)}">											 
							        	         <div class="fg-line">							
													<textarea class="form-control" rows="10" cols="30" autofocus placeholder='Recommendations' name="recommendation" ng-model='Recommendation.recommendation' ng-required='true' id="recom"></textarea>													
												</div>	
<p ng-show="recommendationName.recommendation.$error.required && (recommendationName.recommendation.$dirty || Resubmitted)" class="help-block">Required.</p>												
</div>												
											  </div>
											</div>
											<div class="col-sm-2"><input type="hidden" name="plotNo" ng-model='Recommendation.plotNo'/></div>
										</div>
										<div align="center">											
											<button type="button" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click='SaveRecommendations(Recommendation);'>Save</button>
											<button type="button" ng-click='CloseDialog();' class="btn btn-primary btn-sm m-t-10">Close Dialog</button>	
										</div>														
									</div>
								</div>		
							<!--</dialog>-->
														
						</form>
					 	<form name="UpdateSoilTestForm" novalidate ng-submit='saveMainRecommendationsForm(ryotCode,UpdateSoilTestForm,enteredBy);'>
					 		<div class="row">								
				                <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : UpdateSoilTestForm.ryotCode.$invalid && (UpdateSoilTestForm.ryotCode.$dirty || submitted)}">																				
			        	                <div class="fg-line">
										  	<select chosen class="w-100" name="ryotCode" ng-model='ryotCode' ng-options="ryotCode.id as ryotCode.id for ryotCode in ryotCodeFilterData | orderBy:'-id':true" ng-required='true' ng-change="getRyotDetailsUpdate(ryotCode);getRyotUpdateDetails(ryotCode)">
												<option value="">Select Ryot Code</option>
											</select>
			                	        </div>
<p ng-show="UpdateSoilTestForm.ryotCode.$error.required && (UpdateSoilTestForm.ryotCode.$dirty || submitted)" class="help-block">Select Ryot Code</p>																			
</div>										
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-5">
								  	<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper">
				        	                <div class="fg-line">
												<label for="RyotName">Ryot Name</label>
												<input type="text" name="ryotName" ng-model="ryotName" readonly class="form-control" placeholder="Ryot Name" with-floating-label id="RyotName"/>
				                	        </div>
										</div>
			                    	</div>	
								  </div>
								  <div class="col-sm-2"><button type="button" class="btn btn-primary btn-sm m-t-10" ng-click='loadExtentDetails(ryotCode);'>Load Details</button></div>
    	        			 </div>
							 <div class="row">
							 	<div class="col-sm-5">
								  	<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group  floating-label-wrapper" ng-class="{ 'has-error' : UpdateSoilTestForm.enteredBy.$invalid && (UpdateSoilTestForm.enteredBy.$dirty || submitted)}">
				        	                <div class="fg-line">
												<label for="Tested By">Tested By</label>
												<input type="text" name="enteredBy" ng-model="enteredBy"  class="form-control" data-ng-required="true" placeholder="Tested By" with-floating-label  readonly id="Tested By" data-ng-pattern="/^[a-z A-Z.\s]*$/"/>
				                	        </div>
											<p ng-show="UpdateSoilTestForm.enteredBy.$error.required && (UpdateSoilTestForm.enteredBy.$dirty || submitted)" class="help-block"> Tested By is Required.</p>	
											<p ng-show="UpdateSoilTestForm.enteredBy.$error.pattern && (UpdateSoilTestForm.enteredBy.$dirty || submitted)" class="help-block">Enter Valid Tested By</p>																	
										</div>
			                    	</div>	
								  </div>
								  
								
							 </div>
							 
							 <br />	

							<input type="hidden" ng-model="soilTestStatus" />							 
							 <hr />
							 
							 
							
							 <div class="block-header"><b><u>Extent Details</u> :</b></div>
							 		<table class="table table-striped"> 
							 			<thead>
								        	<tr>												
    	            		    				<th>Plot Sl. No.</th>
								                <th>Variety</th>
												<th>Extent</th>
												<th>Survey No.</th>
												<th>Cultivate</th>
												<th>Recomendations</th>
            								</tr>											
										</thead>
										<tbody>
											<tr ng-repeat='UpdateExtentData in data'>
												<td>
													<input type="text" class="form-control1" placeholder="Plot Sl. No." readonly name="plotSlNumber" data-ng-model='UpdateExtentData.plotSlNumber'/>
												</td>    
												<td>
													<input type="text" class="form-control1" placeholder="Variety" readonly name="varietyCode" data-ng-model='UpdateExtentData.varietyCode'/>
												</td>  
												<td>
													<input type="text" class="form-control1" placeholder="Extent" readonly name="extentSize" data-ng-model='UpdateExtentData.extentSize'/>
												</td>  
												<td>
													<input type="text" class="form-control1"  placeholder="Survey No" readonly name="surveyNo" data-ng-model='UpdateExtentData.surveyNo'/>
												</td>
												<td>
<!--ng-options='suitableForCultivation.id as suitableForCultivation.name for suitableForCultivation in CultivationsDrop'												-->
<div class="form-group" ng-class="{ 'has-error' : UpdateSoilTestForm.suitableForCultivation{{UpdateExtentData.id}}.$invalid && (UpdateSoilTestForm.suitableForCultivation{{UpdateExtentData.id}}.$dirty || Addsubmitted)}">												
														<select  class="form-control1" name="suitableForCultivation{{UpdateExtentData.id}}" data-ng-model='UpdateExtentData.suitableForCultivation' class="cultivateyes">
															<option value="" ng-selected="UpdateExtentData.suitableForCultivation==''">Select</option>
															<option value="0" ng-selected="UpdateExtentData.suitableForCultivation=='0'">Yes</option>
															<option value="1" ng-selected="UpdateExtentData.suitableForCultivation=='1'">Yes with Enrichment</option>
															<option value="2"  ng-selected="UpdateExtentData.suitableForCultivation=='2'">No</option>
														</select>
<p ng-show="UpdateSoilTestForm.suitableForCultivation{{UpdateExtentData.id}}.$error.required && (UpdateSoilTestForm.suitableForCultivation{{UpdateExtentData.id}}.$dirty || Addsubmitted)" class="help-block">Required.</p>														
</div>														

												</td>
											 <td>
											 	<textarea rows="1" cols="18" style="border-radius:5px; height:35px;border: solid 1px #CCCCCC;" placeholder='Recomendations' ng-click="showDialog(UpdateExtentData.plotSlNumber,UpdateExtentData.surveyNo,ryotCode);" ng-focus="showDialog(UpdateExtentData.plotSlNumber,UpdateExtentData.surveyNo,ryotCode);"></textarea>
											</td>
											</tr>
										</tbody>		
								 </table>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="reset(UpdateSoilTestForm);">Reset</button>
									</div>
								</div>						 	
							 </div>									 			
						</form>	 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
