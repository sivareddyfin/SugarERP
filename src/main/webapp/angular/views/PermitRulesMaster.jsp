	<script type="text/javascript">	
		$('#autofocus').focus();	
	</script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="PermitRulesMaster" ng-init='loadAddedPermits();'>      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Permit Rules</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					 										
						<form name="PermitRulesForm" novalidate ng-submit="AddPermitRules(AddPermit,PermitRulesForm);">
						
						<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
									    		<span class="input-group-addon">Permit Based on</span>
					        	                <div class="fg-line" style="margin-top:5px;">
												  	<label class="radio radio-inline m-r-20">
							<input type="radio" name="permitType"  value="0" data-ng-model='AddPermit.permitType' ng-change="changeStatus(AddPermit.permitType);">
			    					    				<i class="input-helper"></i>No. of Tons 
							 						</label>
				 			 						<label class="radio radio-inline m-r-20">
				        <input type="radio" name="permitType" value="1"   data-ng-model='AddPermit.permitType'  ng-change="changeStatus(AddPermit.permitType);">
					    								<i class="input-helper"></i>Per Acre
							  		 				</label>						 							 
												</div>
											</div>
										</div>
									</div>
							 
							 <div class="table-responsive">
							 	<table class="table table-striped table-vmiddle" style="background-color:#FFFFFF;">
									<tr>
										<div></div>
										<th style="text-align:center;" ng-show="AddPermit.permitType!='1'" >No.Of Tons Per Acre</th>
										<th  style="text-align:center;"  ng-show="AddPermit.permitType!='1'">No.Of Tons Per Each Permit</th>
									
										<th  style="text-align:center;" ng-show="AddPermit.permitType=='1'">No of Permits per acre</th>
										
									</tr>
									<tr>
										<td ng-show="AddPermit.permitType!='1'" >
											<div class="input-group" ng-show="AddPermit.permitType!='1'">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group" ng-class="{ 'has-error' : PermitRulesForm.noOfTonsPerAcre.$invalid && (PermitRulesForm.noOfTonsPerAcre.$dirty || submitted)}">																																																																																													
							        	                <div class="fg-line">
        		    							          <input type="text" class="form-control" placeholder="No.Of Tons Per Acre" maxlength="5" name="noOfTonsPerAcre" ng-model="AddPermit.noOfTonsPerAcre" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  ng-show="AddPermit.permitType!='1'">
			    		        		    	        </div>
<p ng-show="PermitRulesForm.noOfTonsPerAcre.$error.required && (PermitRulesForm.noOfTonsPerAcre.$dirty || submitted)" class="help-block">Required.</p>
<p ng-show="PermitRulesForm.noOfTonsPerAcre.$error.pattern && (PermitRulesForm.noOfTonsPerAcre.$dirty || submitted)" class="help-block">Enter Valid Tons.</p>														
													</div>
			                    			</div>
										</td>
										<td ng-show="AddPermit.permitType!='1'">
											<div class="input-group" ng-show="AddPermit.permitType!='1'">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PermitRulesForm.noOfTonsPerEachPermit.$invalid && (PermitRulesForm.noOfTonsPerEachPermit.$dirty || submitted)}">																																																																																																									
					        	                <div class="fg-line">
        		    					          <input type="text" class="form-control" placeholder="No.Of Tons Per Each Permit"  maxlength="10" name="noOfTonsPerEachPermit" ng-model="AddPermit.noOfTonsPerEachPermit" data-ng-required='true' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-show="AddPermit.permitType!='1'">
			    			           	        </div>
<p ng-show="PermitRulesForm.noOfTonsPerEachPermit.$error.required && (PermitRulesForm.noOfTonsPerEachPermit.$dirty || submitted)" class="help-block">Required.</p>												
<p ng-show="PermitRulesForm.noOfTonsPerEachPermit.$error.pattern && (PermitRulesForm.noOfTonsPerEachPermit.$dirty || submitted)" class="help-block">Enter Valid Tons.</p>												
										       </div>
			                		    	</div>
											
										</td>
										<td ng-show="AddPermit.permitType=='1'">
										<div ng-show="AddPermit.permitType=='1'">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : PermitRulesForm.noOfPermitPerAcre.$invalid && (PermitRulesForm.noOfPermitPerAcre.$dirty || submitted)}">																																																																																																									
					        	                <div class="fg-line">
        		    					          <input type="text" class="form-control" placeholder="No of Permits per acre"  maxlength="10" name="noOfPermitPerAcre" ng-model="AddPermit.noOfPermitPerAcre" data-ng-required='validationRequired' ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-show="AddPermit.permitType=='1'">
			    			           	        </div>
<p ng-show="PermitRulesForm.noOfPermitPerAcre.$error.required && (PermitRulesForm.noOfPermitPerAcre.$dirty || submitted)" class="help-block">Required.</p>												
<p ng-show="PermitRulesForm.noOfPermitPerAcre.$error.pattern && (PermitRulesForm.noOfPermitPerAcre.$dirty || submitted)" class="help-block">Enter Valid Permits.</p>												
										       </div>
			                		    	</div>
										</div>
											<input type="hidden" class="form-control" name="id" ng-model="AddPermit.id">
										
										</td>										
									</tr>
								</table>
							 </div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Add</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(PermitRulesForm)">Reset</button>
									</div>
								</div>						 	
							 </div>	 			           
						</form>
							 						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
