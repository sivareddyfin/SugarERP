	<style type="text/css">

		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		input
		{
		color:#000000;
		font-size:18px;
		font-weight:bold;
		}
	</style>

	<script type="text/javascript">	
		$('#autofocus').focus();	
		var data = [{bname:'SBI',bcode:'SBI1',branch:'VSEZ',ifsc:'SBI0002',micr:'SBI0001',address:'Vizag',city:'Vizag',district:'E.G.Dt',state:'A.P'},{bname:'Axis',bcode:'AXS1',branch:'VSEZ',ifsc:'AXS0002',micr:'AXS0001',address:'Vizag',city:'Vizag',district:'E.G.Dt',state:'A.P'},{bname:'HDFC',bcode:'HDFC1',branch:'VSEZ',ifsc:'HDFC0002',micr:'HDFC0001',address:'Vizag',city:'Vizag',district:'E.G.Dt',state:'A.P'}];
		sessionStorage.setItem('data1',JSON.stringify(data));		
	</script>
	<script type="text/javascript">
	var rowCount = 0;
	function addMoreRows(frm) 
	{
		rowCount ++;
		$('#tab_logic').append('<tr id="addr'+rowCount+'"></tr>');
		$('#addr'+rowCount).html('<td width="100px;"><button type="button" id="right_All_1" class="btn btn-primary" onclick="removeRow('+rowCount+');"><i class="glyphicon glyphicon-remove-sign"></i></button></td><td width="200px;"><input type="text" class="form-control1" placeholder="From Amount" maxlength="10"/></td><td width="200px;"><input type="text" class="form-control1" placeholder="To Amount" maxlength="10"/></td><td width="200px;"><input type="text" class="form-control1"  placeholder="% of Interest" maxlength="3"/></td>');
	}

	function removeRow(removeNum) 
	{	
		jQuery('#addr'+removeNum).remove();
	}	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Company Advance Interest Rate</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 		<div class="row">
								
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_advances">
												<option value="0">Select Added Advances</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          
											<input type='text' class='form-control' placeholder='Calculate Interest Till'>
										  
			                	        </div>
			                    	</div>			                    
									
								  </div>
								  								
    	        			 </div><br /><br />
					 		<div class="row">
							  <div class="col-md-1"></div>
							  <div class="col-md-10">		
						 		  <div class="table-responsive">
							 <table class="table table-striped table-vmiddle" id="tab_logic">
							 		<thead>
								        <tr style="background-color:#FFFFFF;">
                		    				<th width="100px;">Actions</th>
							                <th width="200px;">From Amount</th>
		                    				<th width="200px;">To Amount</th>
											<th width="200px;">% of Interest</th>
            							</tr>											
									</thead>
									<tbody>
										<tr id="addr0">
											<td width="100px;">
												<button type="button" id="right_All_1" class="btn btn-primary" onclick="addMoreRows(this.form);"><i class="glyphicon glyphicon-plus-sign"></i></button>
											</td>  
											<td width="200px;"><input type="text" class="form-control1" placeholder="From Amount" maxlength="10" id="autofocus"/></td>  
											<td width="200px;"><input type="text" class="form-control1" placeholder="To Amount" maxlength="10"/></td>
											<td width="200px;"><input type="text" class="form-control1"  placeholder="% of Interest" maxlength="3"/></td>
										</tr>
									</tbody>		
							 </table>
						</div>	  
						</div>
						<div class="col-md-1"></div>
						</div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/CompanyAdvanceInterest" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/CompanyAdvanceInterest" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>							 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<!--<div class="block-header"><h2><b>Added Banks</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle" data-ng-init="names=['Jani']">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Bank Code'">
                		    					<span ng-if="!w.$edit">{{w.bname}}</span>
					    		                <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.bname" /></div>
					            		      </td>
		                    				  <td data-title="'Bank Name'">
							                     <span ng-if="!w.$edit">{{w.bcode}}</span>
							                     <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.bcode" /></div>
							                  </td>
							                  <td data-title="'Branch Name'">
							                      <span ng-if="!w.$edit">{{w.branch}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.branch" /></div>
							                  </td>
							                  <td data-title="'IFSC Code'">
							                      <span ng-if="!w.$edit">{{w.ifsc}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.ifsc" /></div>
							                  </td>											  
							                  <td data-title="'MICR Code'">
							                      <span ng-if="!w.$edit">{{w.micr}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.micr" /></div>
							                  </td>											  											  
							                  <td data-title="'Address'">
							                      <span ng-if="!w.$edit">{{w.address}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.address" /></div>
							                  </td>											  											  
							                  <td data-title="'City/Village'">
							                      <span ng-if="!w.$edit">{{w.city}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.city" /></div>
							                  </td>											  											  
							                  <td data-title="'District'">
							                      <span ng-if="!w.$edit">{{w.district}}</span>
        		            					  <div ng-if="w.$edit">
												  	<div class="select">
	                				    		<select chosen class="w-100" id="input_statename">
													<option value="0">Select State</option>
							                        <option value="United States">United States</option>
            	    						        <option value="United Kingdom">United Kingdom</option>
					        		                <option value="Afghanistan">Afghanistan</option>
				    	            		        <option value="Aland Islands">Aland Islands</option>
		                					        <option value="Albania">Albania</option>
						    	                    <option value="Algeria">Algeria</option>
                							        <option value="American Samoa">American Samoa</option>
				        		    	        </select>
                	            			  </div>
												  </div>
							                  </td>											  											  
							                  <td data-title="'State'">
							                      <span ng-if="!w.$edit">{{w.state}}</span>
        		            					  <div ng-if="w.$edit">
												  	<div class="select">
	                				    		<select chosen class="w-100" id="input_statename">
													<option value="0">Select State</option>
							                        <option value="United States">United States</option>
            	    						        <option value="United Kingdom">United Kingdom</option>
					        		                <option value="Afghanistan">Afghanistan</option>
				    	            		        <option value="Aland Islands">Aland Islands</option>
		                					        <option value="Albania">Albania</option>
						    	                    <option value="Algeria">Algeria</option>
                							        <option value="American Samoa">American Samoa</option>
				        		    	        </select>
                	            			  </div>
												  </div>
							                  </td>											  											  
											  
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>-->
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
