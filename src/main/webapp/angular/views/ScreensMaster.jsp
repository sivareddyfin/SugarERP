	<script type="text/javascript">		
		$(document).ready(function()
		{		
			$(".autofocus").focus();		
				
		});
	</script>
	
	
	



	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'> 
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="screensmaster" ng-init="loadScreensmaster();getAllScreensMaster();"> 
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Screen Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="ScreenForm" ng-submit='AddScreensSubmit(AddedScreens,ScreenForm);' novalidate>		
            			<div class="row">
			                <div class="col-sm-6">
								<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' :ScreenForm.screenName.$invalid && (ScreenForm.screenName.$dirty || Addsubmitted)}">
							        	            <div class="fg-line">
													<label for="screenname">Screen Name</label>
													
													 <input type="hidden"   name="screenCode"  ng-value="" data-ng-model="AddedScreens.screenCode"/>
    	        			 <input type="text" class="form-control autofocus" placeholder="Screen Name" maxlength="50"  name="screenName" data-ng-model="AddedScreens.screenName" autofocus  tabindex="1" data-ng-required='true' ng-pattern="/^[a-z A-Z.&-]*$/" id="screenname" with-floating-label  ng-blur="spacebtw('screenName');validateDup(AddedScreens.screenName);" />
							 
				                				    </div>
						<p ng-show="ScreenForm.screenName.$error.required && (ScreenForm.screenName.$dirty || Addsubmitted)" class="help-block">Screen Name is required</p>
						<p ng-show="ScreenForm.screenName.$error.pattern  && (ScreenForm.screenName.$dirty || Addsubmitted)" class="help-block">Enter Valid Screen Name</p>	
						<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddedScreens.screenName!=null">Screen Name Already Exits</p>													
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <div class="checkbox">	
										<div class="fg-line">
											<label ><b>Possibilities :</b></label>
											
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="canAdd" tabindex="3" data-ng-model="AddedScreens.canAdd"><i class="input-helper"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add
												
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="canModify" tabindex="4" data-ng-model="AddedScreens.canModify"><i class="input-helper"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modify
												
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="canRead" tabindex="5" data-ng-model="AddedScreens.canRead" selected="selected"><i class="input-helper"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View
								            </label>										
											<label class="checkbox checkbox-inline m-r-20">
								                <input type="checkbox" class="checkbox" name="canDelete" tabindex="6" data-ng-model="AddedScreens.canDelete"><i class="input-helper"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Delete
								            </label>																					
										</div>
                			       	</div>
										</div>
									</div>
									</div>
									<input type="hidden"  name="canAdd" data-ng-model="AddedScreens.canAdd" />
									<input type="hidden"  name="canModify" data-ng-model="AddedScreens.canModify" />
									<input type="hidden"  name="canRead" data-ng-model="AddedScreens.canRead" />
									<input type="hidden"  name="canDelete" data-ng-model="AddedScreens.canDelete"  />
									<input type="hidden"  name="modifyFlag" data-ng-model="AddedScreens.modifyFlag"  />
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
			        	                <div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Status :</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="status" value="0" data-ng-model="AddedScreens.status">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="status" value="1" data-ng-model="AddedScreens.status">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>			                    
										</div>
									</div>
									
									</div>
									 <div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
							        	            <div class="fg-line">
													<label for="decsription">Description</label>
    	  <input type="text" class="form-control" placeholder="Description(Optional)" name="description"  tabindex="2" data-ng-model="AddedScreens.description" maxlength="50" id="decsription" with-floating-label  ng-blur="spacebtw('description');" />
				                				    </div>
								<!--<p ng-show="CompanyForm.website.$error.required && (CompanyForm.website.$dirty || Addsubmitted)" class="help-block"> Website is required.</p>
								<p ng-show="CompanyForm.website.$error.pattern  && (CompanyForm.website.$dirty || Addsubmitted)" class="help-block"> Valid Website is required.</p>-->													
												</div>
												</div>
					                    	</div>
										</div>
									
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
			        	                		<div class="fg-line" style="margin-top:5px;">
										  <label class="radio radio-inline m-r-20"><b>Audit Trail Required :</b></label>	
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="isAuditTrailRequired" value="0" data-ng-model="AddedScreens.isAuditTrailRequired">
			    			            	<i class="input-helper"></i>Yes
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="isAuditTrailRequired" value="1" data-ng-model="AddedScreens.isAuditTrailRequired">
			    				            <i class="input-helper"></i>No
					  		              </label>
			                	        		</div>
			                    			</div>			                    
										</div>
									</div>
								</div>
							</div>
						<br />
										
						 <div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="submit" class="btn btn-primary btn-hide btn-sm m-t-10">save</button>									
									<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(ScreenForm)">Reset</button>
								</div>
							</div>						 	
						 </div>
						 </form>
			        
			    </div>															
				</div>
				<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Screens</b></h2></div>
				<div class="card">							
					<!--table-vmiddle-->
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
										<div class="container1">
								<form name="editScreenForm" novalidate> 						
							        <table ng-table="screensmaster.tableEdit" class="table table-striped table-vmiddle">
									
										<thead>
										<tr>
											<th><span>Action</span></th>
											<th><span>Screen Name</span></th>
											<th><span>Description</span></th>
											<th><span>Add</span></th>
											<th><span>Modify</span></th>
											<th><span>View</span></th>
											<th><span>Delete</span></th>
											<th><span>Audit</span></th>	
											<th><span>Status</span></th>	
										</tr>
										</thead>
										<tbody>
								        <tr ng-repeat="Screen in ScreenData"  ng-class="{ 'active': Screen.$edit }">
                		    				<td>
												
												
					    		    		   <button type="button" class="btn btn-default" ng-if="!Screen.$edit" ng-click="Screen.$edit = true;Screen.modifyFlag= 'Yes'"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="Screen.$edit" data-ng-click='updateScreenMaster(Screen,$index);Screen.$edit = isEdit;'><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden" name="modifyFlag{{$index}}" data-ng-model="Screen.modifyFlag">
		                    				 </td>
							                 <td>
                		    					<span ng-if="!Screen.$edit">{{Screen.screenName}}</span> 
					    		                <div ng-if="Screen.$edit"> 
												<div class="form-group" ng-class="{ 'has-error' :editScreenForm.screenName{{$index}}.$invalid && (editScreenForm.screenName{{$index}}.$dirty || submitted)}">
												<input type="text" class="form-control" placeholder="Screen Name" maxlength="35"  name="screenName{{$index}}" data-ng-model="Screen.screenName"  data-ng-required='true' data-ng-pattern="/^[a-z A-Z.&-\s]*$/" ng-blur="validateDuplicate(Screen.screenName,$index);"/>
				<p ng-show="editScreenForm.screenName{{$index}}.$error.required && (editScreenForm.screenName{{$index}}.$dirty || submitted)" class="help-block">Required</p>
				<p ng-show="editScreenForm.screenName{{$index}}.$error.pattern  && (editScreenForm.screenName{{$index}}.$dirty || submitted)" class="help-block">Invalid</p>
				<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="Screen.screenName!=null">Screen Name Already Exist.</p>
												
												</div>																								
					            		      </td>
		                    				  <td>
							                     <span ng-if="!Screen.$edit">{{Screen.description}}</span>
							                     <div ng-if="Screen.$edit">
												 	<div class="form-group">													
													  <input type="text" class="form-control" placeholder="Description(Optional)" name="description{{$index}}"  data-ng-model="Screen.description" maxlength="50">
													</div>
												 </div>
							                  </td>
							                  <td>
							                      <span ng-if="!Screen.$edit">
												  <span ng-if="Screen.canAdd==true">Yes</span>
													<span ng-if="Screen.canAdd==false">No</span>
												  </span>
        		            					  <div ng-if="Screen.$edit">
												  	<label class="checkbox" style="margin-top:-20px;">
													  	<input type="checkbox"    name="canAdd" data-ng-model="Screen.canAdd"><i class="input-helper"></i>
													</label>
												  </div>
							                  </td>
							                  <td>
                    							   <span ng-if="!Screen.$edit">
												    <span ng-if="Screen.canModify==true">Yes</span>
													<span ng-if="Screen.canModify==false">No</span>
												   </span>
					            		           <div ng-if="Screen.$edit">
													  	<label class="checkbox" style="margin-top:-20px;">
														  	<input type="checkbox"   name="canModify" data-ng-model="Screen.canModify" ><i class="input-helper"></i>
														</label>
													</div>
							                  </td>
					    		              <td>
                    							   <span ng-if="!Screen.$edit">
												    <span ng-if="Screen.canRead==true">Yes</span>
													<span ng-if="Screen.canRead==false">No</span>
												   </span>
					                    		   <div ng-if="Screen.$edit">
											  			<label class="checkbox" style="margin-top:-20px;">
														  	<input type="checkbox"    name="canRead" data-ng-model="Screen.canRead" ><i class="input-helper"></i>
														</label>
												    </div>
							                  </td>					
							                  <td>
                    							    <span ng-if="!Screen.$edit">
													 <span ng-if="Screen.canDelete==true">Yes</span>
													<span ng-if="Screen.canDelete==false">No</span>
													</span>
					            		            <div ng-if="Screen.$edit">
													  	<label class="checkbox" style="margin-top:-20px;">
														  	<input type="checkbox"    name="canDelete" data-ng-model="Screen.canDelete" ><i class="input-helper"></i>
														</label>
													</div>
					            		      </td>			
							                  <td>
                    							    <span ng-if="!Screen.$edit">
													<span ng-if="Screen.isAuditTrailRequired=='0'">Yes</span>
													<span ng-if="Screen.isAuditTrailRequired=='1'">No</span>
													</span>
					            		            <div ng-if="Screen.$edit">
															<label class="radio radio-inline m-r-20"> 
														<input type="radio" name="isAuditTrailRequired{{$index}}" value="0" data-ng-model='Screen.isAuditTrailRequired'>
			    					    				<i class="input-helper"></i>Yes
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-1px;">
				            							<input type="radio" name="isAuditTrailRequired{{$index}}" value="1" data-ng-model='Screen.isAuditTrailRequired'>
			    										<i class="input-helper"></i>No
							  		 				</label>						 			
													</div>
					            		      </td>
							                  <td>
                    							    <span ng-if="!Screen.$edit">
													<span ng-if="Screen.status=='0'">Active</span>
													<span ng-if="Screen.status=='1'">Inactive</span>
													</span>
					            		            <div ng-if="Screen.$edit">
															<label class="radio radio-inline m-r-20">
														<input type="radio" name="status{{$index}}" value="0" data-ng-model='Screen.status'>
			    					    				<i class="input-helper"></i>Active
									 				</label>
				 			 						<label class="radio radio-inline m-r-20" style="margin-left:-1px;">
				            							<input type="radio" name="status{{$index}}" value="1" data-ng-model='Screen.status'>
			    										<i class="input-helper"></i>Inactive
							  		 				</label>						 			
													</div>
					            		      	</td>
											  </tr>
											</tbody>
								     	</table>
								 	</form>
							 	</div>
							</section>
						</div>
					</div>
				</div>
				<!----------Table Design-------->
									
  
					
				
				<!----------Table Design-------->
			</div>
	    </section>   
	</section>  

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
