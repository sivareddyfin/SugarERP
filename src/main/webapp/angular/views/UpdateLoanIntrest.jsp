	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
	</style>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Loan Interest</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>					 		
					 		<div class="row">
								<div class="col-sm-2"></div>
				                <div class="col-sm-5">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
            					          <div class="select">
										  	<select chosen class="w-100" id="input_updzone">
												<option value="0">Select Bank</option>
												<option value="0">Advance 1</option>
												<option value="0">Advance 2</option>
												<option value="0">Advance 3</option>
											</select>
										  </div>
			                	        </div>
			                    	</div>			                    
				                  </div>
								  <div class="col-sm-5">
								  	<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/UpdateLoanInterest" class="btn btn-primary btn-sm m-t-10">Get Details</a>
										&nbsp;<a href="#/admin/admindata/masterdata/UpdateLoanInterest" class="btn btn-primary btn-sm m-t-10">Calculate Interest</a>
									</div>
								</div>
								  </div>
    	        			 </div>							 							
							 <hr />	
							 <div  class="table-responsive">
							 	<table class="table table-striped">
							 		<thead>
								        <tr>
                		    				<th>Ryot Code</th>
											<th>Ryot Name</th>
							                <th>Loan Sl.No.</th>
											<th>Loan A/C No</th>											
		                    				<th>Disbursed Date</th>
											<th>Principle Amount</th>											
											<th>Interest Rate</th>
											<th>Subvented Interest Rate</th>
											<th>Net Interest Rate</th>
											<th>Interest Amount</th>
											<th>Total Amount</th>
											
            							</tr>											
									</thead>
									<tbody>
										<tr>
											<td>1234</td> 
											<td>Ramarao</td>  
											<td>81</td>  
											<td>13456</td>  
											<td>02-01-2016</td>
											<td>100000</td>
											<td>12</td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Subvented Amount"/></td>
											<td>12</td>										
											
											<td><input type="text" class="form-control1"  size="5" placeholder="Interest Amount"/></td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Total Amount"/></td>
										</tr>
										<tr>
											<td>2345</td>  
											<td>Raju</td>  
											<td>82</td>  
											<td>25645</td>  
											 <td>02-01-2016</td>	 
											<td>100000</td>
											<td>12</td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Subvented Amount"/></td>
											<td>12</td>										
											
											<td><input type="text" class="form-control1"  size="5" placeholder="Interest Amount"/></td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Total Amount"/></td>
										</tr>
										<tr>
											<td>1323</td>  
											<td>Raghu</td>  
											<td>83</td>  
											<td>25789</td>  
											<td>02-01-2016</td>  
											<td>100000</td>
											<td>12</td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Subvented Amount"/></td>
											<td>12</td>										
											
											<td><input type="text" class="form-control1"  size="5" placeholder="Interest Amount"/></td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Total Amount"/></td>
										</tr>
										<tr>
											<td>1324</td>  
											<td>Ramu</td>  
											<td>84</td>  
											<td>25897</td>  
											<td>02-01-2016</td>
											<td>100000</td>																						
											<td>12</td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Subvented Amount"/></td>
											<td>12</td>										
											
											<td><input type="text" class="form-control1"  size="5" placeholder="Interest Amount"/></td>
											<td><input type="text" class="form-control1"  size="5" placeholder="Total Amount"/></td>
										</tr>
									</tbody>		
							   </table>
							 </div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/UpdateLoanInterest" class="btn btn-primary btn-sm m-t-10">Update</a>
										<a href="#/admin/admindata/masterdata/UpdateLoanInterest" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					           			
				        </div>
			    	</div>
					
													
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
