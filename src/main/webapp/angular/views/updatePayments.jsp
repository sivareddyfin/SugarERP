<style>
.styletr {border-bottom: 1px solid grey;}


</style>
	<script type="text/javascript">		
		$('#autofocus').focus();				
	</script>
	<script>
	  $(function() 
	  {
	     $( ".date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });	
		  $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" ng-controller="updatePaymentFormController" ng-init="loadBranchNames();loadCaneWeightSeasonNames();addFormFieldGrid();loadAccountedRyot();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Payments</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					 
					<form name="updatePaymentForm" novalidate ng-submit="AddedUpdatePaymentSubmit(updatePaymentForm);">
						<div class="row">
						  <div class="col-sm-3">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : updatePaymentForm.season.$invalid && (updatePaymentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select class="w-100" chosen name="season" ng-model="AddupdatePayment.season" tabindex="1" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true" ng-change="loadAccountingDates();">
															<option value="">Season</option>
														</select>
				                	    		    </div>
													<p ng-show="updatePaymentForm.season.$error.required && (updatePaymentForm.season.$dirty || submitted)" class="help-block">Season is Required.</p>
												</div>
					                    	</div>
										</div>
								
								<div class="col-sm-3">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : updatePaymentForm.foCode.$invalid && (updatePaymentForm.foCode.$dirty || Addsubmitted)}">
					        	                <div class="fg-line">
    		        					          <div class="select">
												  
												  <select chosen class="w-100" name="foCode" data-ng-model='AddupdatePayment.foCode'  ng-options="foCode.id as foCode.foCode for foCode in FieldOffNames " ng-change="getAccountedDate(AddupdatePayment.season,AddupdatePayment.foCode);">
														<option value="">Cane Accounting period</option>
				        				            </select>
												  </div>
			                	    		    </div>
						<p ng-show="updatePaymentForm.foCode.$error.required && (updatePaymentForm.foCode.$dirty || Addsubmitted)" class="help-block">Cane Accounting Period is Required.</p>												
											</div>
				                    	 </div>
										</div>
								<div class="col-sm-3">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : updatePaymentForm.branch.$invalid && (updatePaymentForm.branch.$dirty || Addsubmitted)}">
					        	                <div class="fg-line">
    		        					          <div class="select">
												  
												  <select chosen class="w-100" name="branch" data-ng-model='AddupdatePayment.branch'  ng-options="branch.id as branch.branchname for branch in BankNames  | orderBy:'-id':true">
														<option value="">Bank Branches</option>
				        				            </select>
												  </div>
			                	    		    </div>
						<p ng-show="updatePaymentForm.branch.$error.required && (updatePaymentForm.branch.$dirty || Addsubmitted)" class="help-block">Branch is Required.</p>												
											</div>
				                    	 </div>
										</div>
								<div ng-if="AddupdatePayment.bankPayment=='1'">		
								<div class="col-sm-3">
											<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group" ng-class="{ 'has-error' : updatePaymentForm.repaymentDate.$invalid && (updatePaymentForm.repaymentDate.$dirty || Addsubmitted)}">
					        	                <div class="fg-line">
    		        					         
												  
												  <input type="text"  class="form-control" name="repaymentDate" placeholder="Repayment Date" data-ng-model='AddupdatePayment.repaymentDate' readonly>
												
			                	    		    </div>
														
											</div>
				                    	 </div>
										</div>		
								</div>		
			                </div>
						<div class="row">
							<div class="col-sm-5">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />	
									 					 <label class="radio radio-inline m-r-20"><b>Payment to be done</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="sbac" value='0'  data-ng-model="AddupdatePayment.bankPayment" ng-change="accountedAompletedRyots(AddupdatePayment.season,AddupdatePayment.foCode,AddupdatePayment.bankPayment);" >
			    			            					<i class="input-helper"></i>SB A/C
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="sbac" value='1'   data-ng-model='AddupdatePayment.bankPayment' ng-change="accountedAompletedRyots(AddupdatePayment.season,AddupdatePayment.foCode,AddupdatePayment.bankPayment);">
			    				            			<i class="input-helper"></i>Loan Amount
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
								<div class="col-sm-5">
			                    			<div class="input-group">
												<div class="form-group" >
			                        				<div class="fg-line nulvalidate">
									<br />	
									 					 <label class="radio radio-inline m-r-20"><b>Accounted Ryots</b></label>	
            					         				 <label class="radio radio-inline m-r-20">
								           				 	<input type="radio" name="unpaid" value="0"   data-ng-model="AddupdatePayment.paymentToRyot" >
			    			            					<i class="input-helper"></i>Unpaid
								         			 	</label>
				 			              				<label class="radio radio-inline m-r-20">
				            			    			<input type="radio" name="unpaid" value="1"   data-ng-model='AddupdatePayment.paymentToRyot'>
			    				            			<i class="input-helper"></i>All
					  		              				</label>
															
            					         				
									
			                        				</div>
											</div>
			                    	</div>			                    
			                	</div>
								 <div class="col-sm-1">
				            			   	<input type="hidden" name="supply" data-ng-model='AddupdatePayment.supplyType'>
								 </div>
									<div class="col-sm-1">
											<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="getPaymentDetails(AddupdatePayment,updatePaymentForm)">Get Details</button>
									</div>
							</div>
							
							<div >
								<div class="row" id='hideloan' style='display:none;'>
								<div class="col-sm-3">
								<div class="input-group">
            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
											<div class="form-group">
					        	                <div class="fg-line">
													<label for="toRank">System Date</label>

									<input type="text" class="form-control date" ng-model="AddupdatePayment.systemDate"  id="toRank" with-floating-label placeholder="System Date" />
									</div>
									</div>
									</div>
								</div>
								
								<div class="col-sm-3">
									<button type="button" class="btn btn-primary btn-sm m-t-10" ng-click="CalculateAdditionalInterest();" >Calculate Additional Interest</button>
								</div>
							</div>
							</div>
					<hr />
			        	<div id="loadSbselectAll" style="display:none;">
						&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
						<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="toggleAllSb()" ng-model="isAllSelectedSb" >&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Select All<i class="input-helper"></i>
								</label>
							</div>
							<div id="sbgridDetails" style="display:none;">
							<table class="table table-stripped">
							<thead>
								        <tr>
											<th>Select</th>
											<th>Ryot Code</th>
							                <th>Bank Branch</th>
											<th>SB Account</th>
											<th>Paid Amount</th>
											
											<th>Paid From</th>
											<th>Is Direct</th>
											<th>Amount</th>
											<th>Paid Amount</th>
											<th>Main Ryot</th>
										
											
            							</tr>
										<tr ng-repeat="updatesbacData in updatesbacPaymentArray">
										<td><div class="checkbox">
															<label class="checkbox checkbox-inline m-r-20">
																<input type="checkbox" class="checkbox" name="chkFlag{{$index}}"  ng-model="updatesbacData.chkFlag" ng-change="optionToggledSb()"><i class="input-helper"></i>
															</label>
														</div></td>
										<td>{{updatesbacData.ryotCode}}</td>
										<td>{{updatesbacData.branchcode}}</td>
										<td>{{updatesbacData.accountNum}}</td>
										<td>{{updatesbacData.loanamt}}</td>
										<td><select class="form-control1" ng-model="updatesbacData.paidFrom" name="paidFrom{{$index}}" ng-options="paidFrom.paidFromId as paidFrom.paidFrom for paidFrom in paidFrom" >
											<option value="">Paid From</option>
											</select> 
										</td>
										<td><select class="form-control1" ng-model="updatesbacData.isDirect" name="isDirect{{$index}}" ng-options="isDirect.isDirectId as isDirect.isDirect for isDirect in isDirect"  >
											<option value="">No</option>
											</select> 
										</td>
										 
										<td><input type="text" class="form-control1"  ng-model="updatesbacData.paidtoRyot" name="paidtoRyot{{$index}}" /></td>
										
										<!--<td ng-if="updatesbacData.isDirect=='Yes'"><input type="text" class="form-control1" ng-repeat="duplicateName in updatesbacPaymentArraysb" ng-model="duplicateName.bifAmt" ng-if="duplicateName.ryotCode==updatesbacData.ryotCode"></td>-->
										
										<td><input type="text" class="form-control1" name="bifAmt{{$index}}" ng-model="updatesbacData.bifAmt"  /></td>
										
										<td><input type="text" class="form-control1"  ng-model="updatesbacData.mainRyot" name="mainRyot{{$index}}"  /></td>
										
										<!--<td><a href="" id="getPopupSbContent" ng-click="getPopupSbContent(updatesbacData.isDirect,updatesbacData.chkFlag,updatesbacData.ryotCode,updatesbacData.branchcode,updatesbacData.loanamt,updatesbacData.accountNum,updatesbacData.paidtoRyot,updatesbacData.paidFrom,updatesbacData.isDirect,$index);" ng-if="updatesbacData.isDirect=='Yes'"><b>Get Details</b></a></td>-->
										
										</tr>
										
																				
									</thead>
							</table>
							</div>
							</div>
						<div  ng-show="AddupdatePayment.bankPayment=='1'">
						<div id="hideloanselectall">
						<div class="checkbox">
								<label class="checkbox checkbox-inline m-r-20">
									<input type="checkbox" class="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" >&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Select All<i class="input-helper"></i>
								</label>
							</div>
						</div>
							<div id="loangridDetails" style="display:none;">
							<table style="width:100%;"  style="border-collapse:collapse;">
							<thead>
								        <tr class="styletr" style="font-weight:bold;">
											<th>Select</th>
											<th>Ryot Code</th>
							                <th>Bank Code</th>
											<th>Account No.</th>
											<th>Int. <br />Rate</th>
											<th>Principle</th>
											<th>Add.Int <br />Amt</th>
											<th>Total <br />amount</th>
											<th>Paid from</th>
											<th>Is Direct</th>
											<th>Amount</th>
											<th>Paid Amt</th>
											<th>To be Paid Amt</th>
											<th>Main Ryot</th>
										
											
            							</tr>
										<tr class="styletr"  ng-repeat="updateloanData in updateloanAmtPaymentArray" style="height:50px;">
										
										<td>
										<div class="checkbox">
															<label class="checkbox checkbox-inline m-r-20">
																<input type="checkbox" class="checkbox" name="chkFlag{{$index}}"  ng-model="updateloanData.chkFlag" ng-change="optionToggled()"><i class="input-helper"></i>
															</label>
														</div> 
										</td>
										<td>{{updateloanData.ryotCode}}</td>
										<td>{{updateloanData.branchcode}}</td>
										<td>{{updateloanData.accountNum}}
										</td>
										<td>{{updateloanData.intrestRate}}</td>
										<td>{{updateloanData.principle}} </td>
										<td><b>{{updateloanData.additionalInt}}</b> </td>
										<td>&nbsp;{{updateloanData.totalAmount}}</td>
										<td><select class="form-control1" ng-model="updateloanData.paidFrom" name="paidFrom{{$index}}" ng-options="paidFrom.paidFromId as paidFrom.paidFrom for paidFrom in paidFrom" style="width:120px;">
											<option value="">Paid From</option>
											</select> 
										</td>
										<td><select class="form-control1" ng-model="updateloanData.isDirect" name="isDirect{{$index}}" ng-options="isDirect.isDirectId as isDirect.isDirect for isDirect in isDirect" style="width:100px;" >
											<option value="">Is Direct</option>
											</select> 
										</td>
										<td><div><input type="text" class="form-control1"  ng-model="updateloanData.amount" name="amount{{$index}}" style="width:100px;" /></div></td>
										<!--<td><a href="" id="getPopupContent" ng-click="getPopupContent();">Ryot Details</a></td>-->
										
										<!--<td><span ng-repeat="duplicateNameLoan in updateloanAmtPaymentArrayLoan" ng-if="duplicateNameLoan.ryotCode==updateloanData.ryotCode">{{duplicateNameLoan.bifAmt}}</span></td>-->
										
										<td><input type="text" class="form-control1"  ng-model="updateloanData.bifAmt" name="bifAmt{{$index}}" style="width:100px;" /></td>
										
										<td><input type="text" class="form-control1"  ng-model="updateloanData.tobePaidAmt" name="tobePaidAmt{{$index}}" style="width:100px;" /></td>
										
										<td><input type="text" class="form-control1"  ng-model="updateloanData.mainRyot" name="mainRyot{{$index}}" style="width:100px;" /></td>
										
										
										<!--<td><span ng-repeat="duplicateLoanName in updateloanAmtPaymentArrayLoan" ng-if="duplicateLoanName.ryotCode==updateloanData.ryotCode">{{duplicateLoanName.bifAmt}}</span></td>-->
										<!--<td><a href="" id="getPopupSbContent" ng-click="getPopupLoanContent(updateloanData.ryotCode,updateloanData.branchcode,updateloanData.accountNum,updateloanData.intrestRate,updateloanData.principle,updateloanData.additionalInt,updateloanData.totalAmount,updateloanData.paidFrom,updateloanData.isDirect,updateloanData.amount,$index);" ng-if="updateloanData.isDirect=='Yes'"><b>Get Details</b></a></td>--></tr>
										
										
														
									</thead>
							</table>
							</div>
							</div>
						<br />
						<div  ng-show="AddupdatePayment.bankPayment=='0'">
						
							<div class="row" align="center">            			
									<div class="input-group">
										<div class="fg-line">
											<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="SBpopupSave(AddupdatePayment,updatePaymentForm);">Save</button>
											<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="resetSB(updatePaymentForm)">Reset</button>
											<br /><br />
										</div>
									</div>						 	
								 </div>
							 
							 </div>
						<div  ng-show="AddupdatePayment.bankPayment=='1'">
							 	
									<div class="row" align="center">            			
											<div class="input-group">
												<div class="fg-line">
													<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide" ng-click="LoanpopupSave();">Save</button>
													<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="resetLoan(updatePaymentForm)">Reset</button>
													<br /><br />
												</div>
											</div>						 	
										 </div>
									 
							 </div>
					</form>	
						  
							 							 
						  
						  
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
				
	    </section> 
	</section>
	
	
	
	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
