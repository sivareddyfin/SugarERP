
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="programDetailsController" ng-init="loadProgram();loadSeasonNames();loadZoneNames();loadCircleNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Program Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
								<form name="ProgramDetailSearch">
 								<div class="row">
									<div class="col-sm-2">
									</div>
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : season.$invalid && (season.$dirty)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="season" data-ng-model="season" ng-options="season.season as season.season for season in SeasonNames | orderBy:'-season':true" ng-required='true'>
<option value="">Select Season</option>

                    </select>
                	            	   </div>
                	        </div>
							
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-3">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : programNumber.$invalid && (programNumber.$dirty)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="programNumber" data-ng-model="programNumber" ng-options="programNumber.programnumber as programNumber.programnumber for programNumber in programNames | orderBy:'-programnumber':true" ng-required='true'>
<option value="">Select Program</option>

                    </select>
                	            	   </div>
                	        </div>
							
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-3">
  									<div class="input-group">
										<div class="fg-line">
												<button type="button" class="btn btn-primary" ng-click="getDetails();">Get Details</button>

													</div>
												</div>
  										</div>
									</div>
									<div class="row">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : zonecode.$invalid && (zonecode.$dirty)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="zonecode" data-ng-model="zonecode" ng-options="zonecode.id as zonecode.zone for zonecode in ZoneData | orderBy:'-zone':true" ng-required='true'>
<option value="">Select Zone</option>

                    </select>
                	            	   </div>
                	        </div>
							
							</div>
                    	</div>
					</div>
				</div>	
			</div>
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
            	            						 <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
															<div class="form-group" ng-class="{ 'has-error' : circlecode.$invalid && (circlecode.$dirty)}">
        	                									<div class="fg-line">
            	           											<div class="select">
                	     	 									<select chosen class="w-100" name="circlecode" data-ng-model="circlecode" ng-options="circlecode.id as circlecode.circle for circlecode in CircleData | orderBy:'-circle':true" ng-required='true'>
<option value="">Select Circle</option>

                    </select>
                	            	   </div>
                	        </div>
							
							</div>
                    	</div>
					</div>
				</div>	
			</div>
								</div>
								</form>	
									<form name="ProgramDetailsForm" ng-submit="ProgramDetail(AddedprogramDetail)" >
								<div class="row">
									<div class="col-sm-4">
										<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="yearname">Year</label>
											<input type="text" class="form-control" placeholder="Year" maxlength="50" ng-model='AddedprogramDetail.yearOfPlanting' name="year"   readonly="readonly" id="yearname" with-floating-label  />
			                	        </div>
			                    	</div>
									</div>
								</div>
									</div>
									<div class="col-sm-4">
										<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="monthname">Month</label>
											<input type="text" class="form-control" placeholder="Month" maxlength="50" ng-model='AddedprogramDetail.monthId' name="month"   readonly="readonly" id="monthname" with-floating-label  />
			                	        </div>
			                    	</div>
									</div>
								</div>
									</div>
									<div class="col-sm-4">
										<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="fortnight">Extent Type</label>
											<input type="text" class="form-control" placeholder="Fort Night" maxlength="50" ng-model='AddedprogramDetail.periodId' name="fortNight"  tabindex="2" readonly="readonly"  id="fortnight" with-floating-label  />
			                	        </div>
			                    	</div>
									</div>
								</div>
									</div>
									
									
								</div>	<br />
								<div class="row">
									<div class="col-sm-4">
										<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
										<label for="extenttype">Extent Type</label>
											<input type="text" class="form-control" placeholder="Extent Type" maxlength="50" ng-model='AddedprogramDetail.typeOfExtent' name="extentType"  tabindex="2" readonly="readonly" id="extenttype" with-floating-label  />
			                	        </div>
			                    	</div>
									</div>
								</div>
									</div>
									<div class="col-sm-4">
										<div class="row">
									<div class="col-sm-12">
										<div class="input-group">									  
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<label for="varietyname">Variety</label>
											<input type="text" class="form-control" placeholder="Variety" maxlength="50" ng-model='AddedprogramDetail.varietyOfCane' name="varietyCode"  tabindex="2" readonly="readonly" id="varietyname" with-floating-label  />
			                	        </div>
			                    	</div>
									</div>
								</div>
									</div>
								</div>
								
 								</form>
							 <hr />	
							 
						<div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
														
							        <table ng-table="programDetailsController.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>SL NO.</span></th>
													<th><span>Ryot Code</span></th>
													<th><span>Ryot Name</span></th>
													<th><span>Plot No.</span></th>
													<th><span>Agreement No.</span></th>
													<th><span>Date of<br />Plantation</span></th>
													<th><span>Extent Size (Acres)</span></th>
													<!--<th><span>Extent Variety</span></th>
													<th><span>Zone</span></th>
  													<th><span>Circle</span></th>-->
													
												</tr>
											</thead>
										<tbody>
								        <!--<tr ng-repeat="programDetails in programData "  ng-class="{ 'active': programDetails.$edit }">-->
										 <!--<tr ng-repeat="programDetails in programData "  ng-class="{ 'active': programDetails.$edit }">-->
										 <tr ng-repeat="programDetails in programData | filter:{zonecode:zonecode,circlecode: circlecode} "  ng-class="{ 'active': programDetails.$edit }">
                		    				<td>
											{{$index+1}}
                		    					
					    		               
					            		      </td>
		                    				  
							                  <td>
							                     {{programDetails.ryotcode}}
        		            					 
							                  </td>
											  <td>
							                      {{programDetails.ryotname}}
        		            					 
							                  </td>
											  <td>{{programDetails.plotseqno}}</td>
											  <td>
							                     {{programDetails.agreementnumber}}
        		            					 
												 
							                  </td>
											  <td>
							                      {{programDetails.cropdate}}
        		            					
							                  </td>
											   <td>
							                     {{programDetails.extentsize}}
        		            					
							                  </td>											  
											 <!-- <td>
							                     {{programDetails.varietycode}}
        		            					
							                  </td>
											   <td> {{programDetails.zonecode}} </td>
  												<td> {{programDetails.circlecode}}</td>-->
        		            					
											   									  
							           </tr>
									</tbody>
								 </table>
											
										 </div>
										</section> 
																 
							     </div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
							 	 						           			
				        </div>
			    	</div>
					
													
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
