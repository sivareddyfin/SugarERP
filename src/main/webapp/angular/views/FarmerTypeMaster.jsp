	<style type="text/css">
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
</style>

	<script type="text/javascript">	
		$('#autofocus').focus();	
		var data = [{fid:'SBI',fname:'SBI1',fdesc:'VSEZ'},{fid:'SBI',fname:'SBI1',fdesc:'VSEZ'},{fid:'SBI',fname:'SBI1',fdesc:'VSEZ'}];
		sessionStorage.setItem('data1',JSON.stringify(data));		
	</script>
	<script type="text/javascript">
	var rowCount = 0;
	function addMoreRows(frm) 
	{
		rowCount ++;
		$('#tab_logic').append('<tr id="addr'+rowCount+'"></tr>');
		$('#addr'+rowCount).html('<td width="100px;"><button type="button" id="right_All_1" class="btn btn-primary" onclick="removeRow('+rowCount+');"><i class="glyphicon glyphicon-remove-sign"></i></button></td><td width="150px;"><input type="text" class="form-control1" placeholder="Plot No"/></td><td width="150px;"><input type="text" class="form-control1" placeholder="Variety"/></td><td width="200px;"><input type="text" class="form-control1" placeholder="Plant/Ratoon"/></td><td width="200px;"><input type="text" class="form-control1" placeholder="Extent"/></td><td width="100px;"><input type="text" class="form-control1" placeholder="Avg Qty" /></td><td width="150px;"><input type="text" class="form-control1"  placeholder="Survey No"/></td>');
	}

	function removeRow(removeNum) 
	{	
		jQuery('#addr'+removeNum).remove();
	}	
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content">      
        	<div class="container" data-ng-controller="tableCtrl as tctrl">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Farmer Type Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
							<div class="row">
								<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Farmer Type ID" id="input_farmertypeid" readonly/>
			                	        </div>
			                    	</div>			                    
				                  </div>
				                <div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Farmer Type" id="autofocus" />
			                	        </div>
			                    	</div>			                    
				                  </div>								  
    	        			 </div><br /> 					 		
						     <div class="row">
							 	<div class="col-sm-6">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<input type="text" class="form-control" placeholder="Description" id="input_description" />
			                	        </div>
			                    	</div>			                    
				                  </div>
								<div class="col-sm-6">
				                    <div class="input-group" style="margin-top:10px;">
            				            <span class="input-group-addon">Status :</span>
			        	                <div class="fg-line" style="margin-top:5px;">
            					          <label class="radio radio-inline m-r-20">
								            <input type="radio" name="inlineRadioOptions" value="option1" checked="checked">
			    			            	<i class="input-helper"></i>Active
								          </label>
				 			              <label class="radio radio-inline m-r-20">
				            			    <input type="radio" name="inlineRadioOptions" value="option2">
			    				            <i class="input-helper"></i>Inactive
					  		              </label>
			                	        </div>
			                    	</div>			                    
				                  </div>  
							 </div>
							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<a href="#/admin/admindata/masterdata/FarmerType" class="btn btn-primary btn-sm m-t-10">Add</a>
										<a href="#/admin/admindata/masterdata/FarmerType" class="btn btn-primary btn-sm m-t-10">Reset</a>
									</div>
								</div>						 	
							 </div>					
							 
							 
							 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Farmer Types</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">						
							        <table ng-table="tctrl.tableEdit" class="table table-striped table-vmiddle">
										
								        <tr ng-repeat="w in $data"  ng-class="{ 'active': w.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!w.$edit" ng-click="w.$edit = true"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="button" class="btn btn-success" ng-if="w.$edit" ng-click="w.$edit = false" onclick="editsuccess();"><i class="zmdi zmdi-check"></i></button>
		                    				 </td>
							                 <td data-title="'Farmer Type ID'">
                		    					<span ng-if="!w.$edit">{{w.fid}}</span>
					    		                <div ng-if="w.$edit"><input class="form-control" type="text" ng-model="w.fid" /></div>
					            		      </td>
		                    				  <td data-title="'Farmer Type'">
							                     <span ng-if="!w.$edit">{{w.fname}}</span>
							                     <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.fname" /></div>
							                  </td>
							                  <td data-title="'Description'">
							                      <span ng-if="!w.$edit">{{w.fdesc}}</span>
        		            					  <div ng-if="w.$edit"><input class="form-control" type="email" ng-model="w.fdesc" /></div>
							                  </td>
											  <td data-title="'Status'">
							                      <span ng-if="!w.$edit">Active</span>
        		            					  <div ng-if="w.$edit">
												  	<select chosen class="w-100">
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
													</select>
												  </div>
							                  </td>							                  
							             </tr>
								         </table>							 
							     </div>
							</div>
						</div>
					<!----------end----------------------->										
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
