
<script>
//$(document).ready(function(){

var renderTable = function (chart, containerId) {
		
            // After the chart is rendered we export the data as CSV, parse it and then create a markup
            // equivalent to a table by parsing the exported CSV.

            var data = chart.getDataAsCSV(),
                rows,
                row,
                i,
               length,
                tableBody = '',
				tableBody1 = '',
                tableHeader = '';
				tableHeader1 = '';
     
            // Get all the rows by splitting data with '\n' seperator
            rows = data.replace(/"/g, '').split('\n');
   var row1 = "";
            // Retrieve the data from the rows and compute body string from the data rows
            for (i = 1, length = rows.length; i < length; i++) {
                row = rows[i].split(',');
				
				row1 = Math.round(row[1] * 100) / 100;
				
				//var row[1] = Math.round(num * 100) / 100;
               tableBody += '<td>' + row[0] + '</td>' ;
				tableBody1 += '<td>' +  row1 + '</td>' ;
            }
     
           // Compute header string from first row
            row = rows[0].split(',');
            tableHeader = '<th>' + row[0] + '</th>' ;
			tableHeader1 = '<th>' + row[1] + '</th>' ;
    
            // Create the table string and append it to the table container
           document.getElementById(containerId).innerHTML = '<table width ="100%;" border="1" style="border-collapse:collapse;"><tbody><tr>'+tableHeader+tableBody+'</tr><tr>'+tableHeader1+tableBody1+'</tr></tbody</table>';
     
       };
	   
	   var ajaxLink = "/SugarERP/crushedWeightByZone.html";
	  //alert(ajaxLink);
	  loadChart(ajaxLink);
	  function loadChart(ajaxLink)
	  {
	   $.getJSON(ajaxLink, function(json) 
		{
			//alert('inside');
			 FusionCharts.ready(function () 
			 {
           
		   var caption = "Area Wise Crushing";
			var chartData = {
						"chart": {
							"caption": caption,
							"showborder": "0",
							"exportenabled": "1",
							"exportatclient": "0",
							"exporthandler": "http://export.api3.fusioncharts.com",
							"html5exporthandler": "http://export.api3.fusioncharts.com",								
							"xAxisNameFont": "Helvetica",
							"xAxisName": "Zone",
               	            "yAxisName": "Net Weight",
							"xAxisNameFontSize": "12",
							"xAxisNameFontBold":"1",
							"yAxisNameFontBold":"1",
							"yAxisNameFontSize": "12",
							"yAxisNameFont": "Helvetica",
							"yFormatNumberScale":"1",
							"numberScaleValue": "1000,1000,1000",
							"numberScaleUnit": "K,M,B",
							"bgColor": "#ffffff",
							"decimals":'1',
							"baseFont":"Helvetica",		
							"baseFontSize":"12",
							"captionFontSize":"18",
							"showlabels": "1",
							"showlegend": "1",
							"legendBgColor": "#ffffff",
							"legendBorderColor":"#000000",
							"legendBorder":"1",
							"legendBorderAlpha": "1",
							"legendShadow": "1",
							"legendValue":"1",
							"legendPosition":"bottom",
							"showCanvasBorder": "0",
							"enablemultislicing": "0",
							"slicingdistance": "20",
							"showpercentvalues": "1",
							"showpercentintooltip": "1",
							"showvalues":"1",
							"plottooltext": "$label  : $datavalue $percentvalue "
						},
					"data":  json['data']
					};
					
 		 var revenueChart = new FusionCharts({
		
        type: 'pie2d',
	    renderAt: 'container',
        width: '100%',
        height: '400',
	        dataFormat: 'json',
	        dataSource: chartData,
			events: {
					dataPlotClick: function (event,dataObj) 
					{	
						
						 sessionStorage.setItem("category",dataObj.categoryLabel);
						sessionStorage.setItem("code",json['data'][dataObj.dataIndex].faid);
//																				
						drillDown(json['data'][dataObj.dataIndex].faid,dataObj.categoryLabel);
						
					 }
				 }

	    });

		 revenueChart.addEventListener('renderComplete', function (e, a) {
         
            renderTable(revenueChart, 'table-container');
			});
		revenueChart.render();
 
 })
 
		})
	}	
		function drillDown(fieldofficerId,category)
		
		{
		var fieldofficerid = fieldofficerId.toString();
		
		
			 $.getJSON("/SugarERP/circleWiseCrushedWeightByZones.html?foid="+fieldofficerid, function(json) 
			{
			
			
			 FusionCharts.ready(function () 
			 {
			 	var caption2 = category;
				var chartData2 = {
									"chart": {
										"caption": caption2,
										"showborder": "0",
										"exportenabled": "1",
										"exportatclient": "0",
										"exporthandler": "http://export.api3.fusioncharts.com",
										"html5exporthandler": "http://export.api3.fusioncharts.com",
										"xAxisName": "Circle",
               	  						"yAxisName": "Net Weight",
																		
										"xAxisNameFont": "Helvetica",
										"xAxisNameFontSize": "12",
										"xAxisNameFontBold":"1",
										"yAxisNameFontBold":"1",
										"yAxisNameFontSize": "12",
										"yAxisNameFont": "Helvetica",
										"yFormatNumberScale":"1",
										"numberScaleValue": "1000,1000,1000",
										"numberScaleUnit": "K,M,B",
										"bgColor": "#ffffff",
										"decimals":'1',
										"baseFont":"Helvetica",		
										"baseFontSize":"12",
										"captionFontSize":"18",
										"showlabels": "1",
										"showlegend": "1",
										"legendBgColor": "#ffffff",
										"legendBorderColor":"#000000",
										"legendBorder":"1",
										"legendBorderAlpha": "1",
										"legendShadow": "1",
										"legendValue":"1",
										"legendPosition":"bottom",
										"showCanvasBorder": "0",
										"enablemultislicing": "0",
										"slicingdistance": "20",
										"showpercentvalues": "1",
										"showpercentintooltip": "1",
										"showvalues":"1",
										"plottooltext": "$label  : $datavalue $percentvalue "
									},
								"data": json['data']
								//alert(JSON.stringify(data));
							};
							var datalength = json['data'].length;
							var netWeightTotal=0;
							for(var t=0;t<(datalength);t++)
							{
							 netWeightTotal +=  json['data'][t].value;
							}
							var ntWtTotal  = netWeightTotal.toFixed(2);
							//alert(ntWtTotal);
							
		 var revenueChart = new FusionCharts({
		
        type: 'pie2d',
	    renderAt: 'container',
        width: '100%',
        height: '400',
	        dataFormat: 'json',
	        dataSource: chartData2,
			events: {
					dataPlotClick: function (event,dataObj) 
					{	
						
					 }
				 }

	    });
		 revenueChart.addEventListener('renderComplete', function (e, a) {
            // In the renderComplete event create the table, even when the data is updated by calling setChartData method, this event will be fired and the updated data will reflect in the table too, since renderTable will be called again.
//            // Creating the table on 'render' event will not update the table everytime, it will only update when render() method is called.
            renderTable(revenueChart, 'table-container');
			});
		revenueChart.render();
		
			$("#goback1").show();
			//$("#disptotal").show();
			 
			 })
		})
			//alert(fieldofficerId);
		}
		
		$(document).ready(function()
		{
			$(".goback").click(function()
			{
		
					loadChart(ajaxLink);
					$("#goback1").hide();
					
			});
			
		});

					
</script>

 </head>
 <body>
 
 <div style="float: right; margin-right:20px;"><p></span> <input type="button" id="goback1" class="goback" value="< Go Back" style=" border:none; display:none;"/></p></div>
<div class="row">
  	<div class="col-sm-12">
    	 <div id='container'></div><br/>
		 <div id="table-container"  id='exportpdf'>The table corresponding to the chart will render here></div>
		 <span id="disptotal" style="display:none;">
	</div>
	
</div>
 


