	<script type="text/javascript">		
		$('.autofocus').focus();		
	</script>
	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="CircleMaster" ng-init='loandMaxCircleCode();loadFieldAssistants();loadVillageNames();loadAddedCircles();loadZoneNames();'>      
        	<div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Circle Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					     <form name="CircleMasterForm" novalidate ng-submit='AddCircles(AddCircle,CircleMasterForm);'>
						 <input type="hidden" name="screenName" ng-model="AddCircle.screenName" />
						 	<div class="row">
								<div class="col-sm-6">								
									
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="circleCode">Circle Code</label>
            							          <input type="text" class="form-control" placeholder="Circle Code" maxlength="3" readonly data-ng-model='AddCircle.circleCode' id="circleCode" with-floating-label>
			            		    	        </div>
			                    			</div>		
											</div>	
									<div class="col-sm-6">	
									
											<div class="input-group">
	            				            	<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : CircleMasterForm.circle.$invalid && (CircleMasterForm.circle.$dirty || Addsubmitted)}">																																															
					        	                <div class="fg-line">
													<label for="circleName">Circle Name</label>
    	    	    					          <input type="text" class="form-control autofocus" placeholder="Circle Name"  maxlength="25" name="circle" data-ng-model='AddCircle.circle' data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" ng-blur="spacebtw('circle');validateDup();" id="circleName" with-floating-label>
					                	        </div>
	<p ng-show="CircleMasterForm.circle.$error.required && (CircleMasterForm.circle.$dirty || Addsubmitted)" class="help-block">Circle is required</p>												
	<p ng-show="CircleMasterForm.circle.$error.pattern  && (CircleMasterForm.circle.$dirty || Addsubmitted)" class="help-block">Enter a valid Circle.</p>	
	<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddCircle.circle!=null">Circle Name Already Exist</p>																																									
</div>												
				    		                  </div>		
										  </div>
										  </div>
										  <div class="row">
										<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : CircleMasterForm.fieldAssistantId.$invalid && (CircleMasterForm.fieldAssistantId.$dirty || Addsubmitted)}">																																			
					        	                <div class="fg-line">
                						    		<select chosen class="w-100" name="fieldAssistantId" data-ng-required='true' data-ng-model='AddCircle.fieldAssistantId' ng-options="fieldAssistantId.id as fieldAssistantId.fieldassistant for fieldAssistantId in FieldAssistantData  | orderBy:'-fieldassistant':true">
														<option value="">Select Field Assistant</option>
				        		        		    </select>
					                	        </div>
	<p ng-show="CircleMasterForm.fieldAssistantId.$error.required && (CircleMasterForm.fieldAssistantId.$dirty || Addsubmitted)" class="help-block">Select Field Assistant</p>																								
	  </div>												
			 	                    	   </div>		
										</div>										
									<div class="col-sm-6">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : CircleMasterForm.villageCode.$invalid && (CircleMasterForm.villageCode.$dirty || Addsubmitted)}">																																																											
					        	                <div class="fg-line">
                						    		<select chosen class="w-100" name="villageCode" data-ng-model='AddCircle.villageCode' data-ng-required='true' ng-options="villageCode.id as villageCode.village for villageCode in VillagesNamesData | orderBy:'-village':true" ng-change="getZonesByVillage(AddCircle.villageCode);">
														<option value="">Select Village</option>
				        		        		    </select>
					                	        </div>
	<p ng-show="CircleMasterForm.villageCode.$error.required && (CircleMasterForm.villageCode.$dirty || Addsubmitted)" class="help-block">Select Village</p>																								
</div>												
					                    	</div>		
										</div>
									</div>
								 <div class="row">
										<div class="col-sm-5">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Status :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        			   					          <label class="radio radio-inline m-r-20">
										              <input type="radio" name="inlineRadioOptions" value="0" checked="checked" data-ng-model='AddCircle.status'>
 			    			           				  <i class="input-helper"></i>Active
										          </label>
				 					              <label class="radio radio-inline m-r-20">
						            			      <input type="radio" name="inlineRadioOptions" value="1" data-ng-model='AddCircle.status'>
			    				                      <i class="input-helper"></i>Inactive
					  		                      </label>
			                	               </div>
			                    	       </div>
										</div>
								
									<div class="col-sm-3">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="form-group" ng-class="{ 'has-error' : CircleMasterForm.zone.$invalid && (CircleMasterForm.zone.$dirty || Addsubmitted)}">																																																											
												<div class="fg-line">
												<select  chosen class="w-100" name='zone' ng-model="AddCircle.zone" data-ng-required="true"  ng-options="zone.id as zone.zone for zone in zoneNames | orderBy:'-zone':true">
													<option value="">Zone</option>
												</select>
												</div>
													<p ng-show="CircleMasterForm.zone.$error.required && (CircleMasterForm.zone.$dirty || Addsubmitted)" class="help-block">Select Zone</p>																								
												</div>
											</div>
										</div>
										
										
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="Description">Description</label>
            							          <input type="text" class="form-control" placeholder="Description" maxlength="50"  data-ng-model='AddCircle.description' ng-blur="spacebtw('description')"  id="Description" with-floating-label>
			            		    	        </div>
			                    			</div>
										</div>
										
								</div>
															
							</div>
							<input type="hidden"  name="modifyFlag" data-ng-model="AddCircle.modifyFlag"  />
							<div class="row">
								<div class="col-sm-12" align="center">
									<div class="input-group">
												<div class="fg-line">
													<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
													<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(CircleMasterForm)">Reset</button>
												</div>
											</div>
								</div>
							</div>
						 </form>				
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					<!----------table grid design--------->
					<div class="block-header" style="margin-bottom:0px; position:static; margin-top:-30px; padding:8px;"><h2><b>Added Circles</b></h2></div>
					<div class="card">							
					        <div class="card-body card-padding">							
								<div class="table-responsive">
									<section class="asdok">
									<div class="container1">
								  <form name="UpdateCircleForm" novalidate>						
							        <table ng-table="CircleMaster.tableEdit" class="table table-striped table-vmiddle">
									<thead>
															<tr>
																<th><span>Action</span></th>
																<th><span>Circle Code</span></th>
																<th><span>Circle Name</span></th>
																<th><span>Field Assistant</span></th>
																<th><span>Village Name</span></th>
																<th><span>Zone Name</span></th>
																<th><span>Description</span></th>
																<th><span>Status</span></th>
																</tr>
														</thead>
														
										<tbody>
								        <tr ng-repeat="UpdateCircle in AddedCircles"  ng-class="{ 'active': UpdateCircle.$edit }">
                		    				<td data-title="'Actions'">
					    		               <button type="button" class="btn btn-default" ng-if="!UpdateCircle.$edit" ng-click="UpdateCircle.$edit = true;UpdateCircle.modifyFlag='Yes';UpdateCircle.screenName='Circle Master';"><i class="zmdi zmdi-edit"></i></button>
					            		       <button type="submit" class="btn btn-success btn-hideg" ng-if="UpdateCircle.$edit" ng-click="UpdateAddedCircles(UpdateCircle,$index);UpdateCircle.$edit = isEdit;"><i class="zmdi zmdi-check"></i></button>
											   <input type="hidden"  name="modifyFlag{{$index}}" data-ng-model="UpdateCircle.modifyFlag"  />
											   <input type="hidden" name="screenName" ng-model="UpdateCircle.screenName" />
		                    				 </td>
							                 <td>
                		    					<span ng-if="!UpdateCircle.$edit">{{UpdateCircle.circleCode}}</span>
					    		                <div ng-if="UpdateCircle.$edit">
													<div class="form-group">
														<input class="form-control" type="text" ng-model="UpdateCircle.circleCode" placeholder='Circle Code' maxlength="3" readonly/>
													</div>
												</div>
					            		      </td>
		                    				  <td>
							                     <span ng-if="!UpdateCircle.$edit">{{UpdateCircle.circle}}</span>
							                     <div ng-if="UpdateCircle.$edit">
<div class="form-group" ng-class="{ 'has-error' : UpdateCircleForm.circle{{$index}}.$invalid && (UpdateCircleForm.circle{{$index}}.$dirty || submitted)}">											
												 	<input class="form-control" type="text" data-ng-model="UpdateCircle.circle" maxlength="25" placeholder='Circle Name' data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" name="circle{{$index}}" ng-blur="spacebtwgrid('circle',$index);validateDuplicate(UpdateCircle.circle,$index);"/>
	<p ng-show="UpdateCircleForm.circle{{$index}}.$error.required && (UpdateCircleForm.circle{{$index}}.$dirty || submitted)" class="help-block">Circle is required</p>												
	<p ng-show="UpdateCircleForm.circle{{$index}}.$error.pattern  && (UpdateCircleForm.circle{{$index}}.$dirty || submitted)" class="help-block">Enter a valid Circle.</p>
	<p class="help-block duplicate{{$index}}" style="color:#FF0000; display:none;" ng-if="UpdateCircle.circle!=null">Circle Name Already Exist.</p>																																										
													
</div>													
												 </div>
							                  </td>
							                  <td>
							                      <!--<span ng-if="!UpdateCircle.$edit" ng-repeat="AssistantNames in FieldAssistantData">
												  	<span ng-if="AssistantNames.id==UpdateCircle.fieldAssistantId">{{AssistantNames.fieldassistant}}</span>
												  </span>-->
												    <span ng-if="!UpdateCircle.$edit"><span>{{UpdateCircle.fieldAssistant}}</span></span>
        		            					  <div ng-if="UpdateCircle.$edit">
												  	<div class="form-group">
<select class='form-control1' name="fieldAssistantId"  data-ng-model='UpdateCircle.fieldAssistantId' ng-options="fieldAssistantId.id as fieldAssistantId.fieldassistant for fieldAssistantId in FieldAssistantData | orderBy:'-fieldassistant':true">
				        		           			 </select>
													 </div>
												  </div>
							                  </td>
							                  <td>
							                      <!--<span ng-if="!UpdateCircle.$edit" ng-repeat="VillageName in VillagesNamesData">
												  	<span ng-if="VillageName.id==UpdateCircle.villageCode">{{VillageName.village}}</span>
												  </span>-->
												  <span ng-if="!UpdateCircle.$edit" ><span>{{UpdateCircle.village}}</span></span>
        		            					  <div ng-if="UpdateCircle.$edit">
												  	<div class="form-group">
<select class='form-control1' name="villageCode" data-ng-model='UpdateCircle.villageCode'  ng-options="villagecode.id as villagecode.village for villagecode in VillagesNamesData | orderBy:'-village':true"></select>
													</div>
												  </div>
							                  </td>	
											  <!--
											  <td>
							                      <span ng-if="!UpdateMandal.$edit" ng-repeat='ZoneName in ZoneData'>
												  	<span ng-if="ZoneName.id==UpdateMandal.zoneCode">{{ZoneName.zone}}</span>
												  </span>
        		            					  <div ng-if="UpdateMandal.$edit">
												  	<div class="form-group" ng-class="{ 'has-error' : UpdateMandalMasterForm.zoneCode{{$index}}.$invalid && (UpdateMandalMasterForm.zoneCode{{$index}}.$dirty || submitted)}">
	        		        				    		<select class='form-control1' name="zoneCode" data-ng-model='UpdateMandal.zoneCode' data-ng-required='true' ng-options="ZoneCode.id as ZoneCode.zone for ZoneCode in ZoneData  | orderBy:'-zone':true" ng-required="true">
															<option value="">Select Zone</option>
				        				            	</select>
														<p ng-show="UpdateMandalMasterForm.zoneCode{{$index}}.$error.required && (UpdateMandalMasterForm.zoneCode{{$index}}.$dirty || submitted)" class="help-block">Zone is required.</p>
													</div>
												  </div>
							                  </td>
											  -->
											  
											  
												<td>
							                     
												  <span ng-if="!UpdateCircle.$edit" ><span>{{UpdateCircle.zoneName}}</span></span>
        		            					  <div ng-if="UpdateCircle.$edit">
												  	<div class="form-group">
<select class='form-control1' name="zone" data-ng-model='UpdateCircle.zone'  ng-options="zone.id as zone.zone for zone in zoneNames | orderBy:'-zone':true"></select>
													</div>
												  </div>
							                  </td>												  
							                  <td>
							                      <span ng-if="!UpdateCircle.$edit">{{UpdateCircle.description}}</span>
        		            					  <div ng-if="UpdateCircle.$edit">
												  	<div class="form-group">
														<input class="form-control" type="text" data-ng-model="UpdateCircle.description" maxlength="25" placeholder='Description' ng-blur="spacebtwgrid('description',$index)"/>												
													</div>
												  </div>
							                  </td>											  											  
											  
							                  <td>
							                      <span ng-if="!UpdateCircle.$edit"> 
													<span ng-if="UpdateCircle.status=='0'">Active</span>
													<span ng-if="UpdateCircle.status=='1'">Inactive</span>
												  </span>
        		            					  <div ng-if="UpdateCircle.$edit">
												  	<div class="form-group">
													  	<select class='form-control1' data-ng-model='UpdateCircle.status'>														
									                        <option value="0" ng-if="UpdateCircle.status=='0'" selected="selected">Active</option>
															<option value="1">Inactive</option>																												
            	    								        <option value="1" ng-if="UpdateCircle.status=='1'" selected="selected">Inactive</option>
															<option value="0">Active</option>
							        		            </select>
													</div>
												  </div>
							                  </td>											  											  
							             </tr>
										 </tbody>
								         </table>
								    </form>
									</div>
									</section>							 
							     </div>
							</div>
						</div>
					<!----------end----------------------->
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
