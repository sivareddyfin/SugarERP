

	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="UpdateEstimatedQtyController" ng-init="loadSeasonNames();loadCircleNames();loadFormData();">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Update Estimated Quantity</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<!--------Form Start----->
					 <!-------body start------>
					 <form name="UpdateEstimatedQty">
					 		<div class="row">
										
										  <div class="col-sm-5">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : UpdateEstimatedQty.season.$invalid && (UpdateEstimatedQty.season.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedEstimatedQty.season' name="season" ng-options="season.season as season.season for season in SeasonData">
															<option value="">Select Season</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="UpdateEstimatedQty.season.$error.required && (UpdateEstimatedQty.season.$dirty || Filtersubmitted)" class="help-block">Select Season</p>		 
											  </div>
			                    	      </div>
										</div>
										  <div class="col-sm-5">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : UpdateEstimatedQty.circle.$invalid && (UpdateEstimatedQty.circle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedEstimatedQty.circle' name="circle" ng-options="circle.id as circle.circle for circle in circleNames">
															<option value="">Select Circle</option>
						        			            </select>
		                	            			  </div>
					<p ng-show="UpdateEstimatedQty.circle.$error.required && (UpdateEstimatedQty.circle.$dirty || Filtersubmitted)" class="help-block">Select Circle</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-primary" ng-click="getEstimatedQtyDetails(UpdateEstimatedQty);">Load Details</button>
										</div>	
																				
							</div>
							
					</form>		
							
							<br /><hr />	
											 		
						 
							<input type="hidden"  data-ng-model="flag" />
							<!----------table grid design--------->
					
											
					        <div class="card-body card-padding"> 							
								<div class="table-responsive">
								<section class="asdok">
									<div class="container1"> 
										<form name="UpdateEstimatedQtyForm" novalidate >				
							        <table ng-table="UpdateEstimatedQty.tableEdit" class="table table-striped table-vmiddle">
										<thead>
												<tr>
													<th><span>Ryot<br /> Code</span></th>
													<th><span>Ryot<br /> Name</span></th>
													<th><span>F_G_H <br />Name</span></th>
													<th><span>Agreement<br /> No.</span></th>
													<th><span>Agreement<br /> Dt.</span></th>
													<th><span>Plot<br /> No.<br /></span></th>
													<th><span>Survey<br /> No.</span></th>
													<th><span>Extent<br /> Area<br />(Acres)</span></th>
													<th><span>Agreed<br /> Qty<br />(Tons)</span></th>
													<th><span>Estimated<br /> Qty<br />(Tons)</span></th>
												</tr>
												<br>
											</thead>
										<tbody>
								        <tr ng-repeat="UpdateEstimate in EstimateQtyData"  ng-class="{ 'active': UpdateEstimate.$edit }">
                		    				<td>
                		    					{{UpdateEstimate.ryotCode}}
					            		      </td>
		                    				  
							                  <td>
							                     {{UpdateEstimate.ryotName}}
							                  </td>
											  <td>
							                     {{UpdateEstimate.fghName}}
							                  </td>
											  <td>
							                     {{UpdateEstimate.agreementNumber}}
							                  </td>
											  <td>
							                      {{UpdateEstimate.agreementDate}}
							                  </td>
											   <td>
							                      {{UpdateEstimate.plotNumber}}
							                  </td>
											   <td>
							                     {{UpdateEstimate.surveyNumber}}
        		            					
							                  </td>
											   <td>
							                      {{UpdateEstimate.extentArea}}
							                  </td>
											  <td>
							                      {{UpdateEstimate.agreedQty}}
							                  </td>
											  	<td style="width:1000px;">
											
                   									<input type="text" class="form-control"  size="15" placeholder="Estimated Qty"  ng-model="UpdateEstimate.estimateQty" placeholder='Estimated Quantity' maxlength="50" name="estimateQty"/> 
								                   
													</td>
													<td>
													 <span class="input-group-addon" style="color: #0099FF; font-size:20px; cursor: pointer;">
								                        <i class="glyphicon glyphicon-ok-sign" ng-click='updateEstimate(UpdateEstimate,$index);'></i>
								                    </span>
								              											
											</td>
											  											  
							             </tr>
										 </tbody>
								         </table>
										 </form>	
										 </div>
										</section>  
										<div align="center"><button type="button" class="btn btn-primary btn-hide" ng-click="saveEstimatedQtyDetails(UpdateEstimatedQtyForm);">Save</button></div>						 
							     </div>
							
						
					<!----------end----------------------->	
						  
		
				 </div>
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
