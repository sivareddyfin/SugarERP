
<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		// $('#date').focus();	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	  <section id="content" data-ng-controller="BudRemovedController" ng-init="loadAddedDates();">   
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Bud Removed Cane</b></h2></div>
			    <div class="card">
				
				
			        <div class="card-body card-padding"> 
					<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
					<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group">
										<div class="form-group">
						        	        <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" ng-model="addedDates" ng-options="addedDates.addedDates as addedDates.addedDates for addedDates in addedDateNames" ng-change="modifyDate(addedDates);">
															<option value="">Added Dates</option>
													</select>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					<!--------Form Start----->
					 
					<!-------body start------>
					 
					 <form name="BudRemovedCane" novalidate ng-submit="budRemovedCaneSubmit(RemovedBudCane,BudRemovedCane)">
					 		<div class="row">
										
										  <input type="hidden" class="form-control " ng-required="true"  name="modifyFlag" ng-model="RemovedBudCane.modifyFlag" />
										  <div class="col-sm-4">
											<div class="input-group">
		            				           <span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.date.$invalid && (BudRemovedCane.date.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control date" ng-required="true" placeholder="Date" name="date" id="date" ng-model="RemovedBudCane.date" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.date.$error.required && (BudRemovedCane.date.$dirty || Filtersubmitted)" class="help-block">Select Date</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.time.$invalid && (BudRemovedCane.time.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control Time" ng-required="true" placeholder="Time" name="time" id="Time" ng-model="RemovedBudCane.time" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.time.$error.required && (BudRemovedCane.time.$dirty || Filtersubmitted)" class="help-block">Select Time</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.weight.$invalid && (BudRemovedCane.weight.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    			<input type="text" class="form-control" ng-required="true" placeholder="Weight" name="weight"  ng-model="RemovedBudCane.weight"/>
		                	            			  </div>
					<p ng-show="BudRemovedCane.weight.$error.required && (BudRemovedCane.weight.$dirty || Filtersubmitted)" class="help-block">Weight is required</p>		 
											  </div>
			                    	      </div>
										</div>										
							</div>
							<div class="row">
										
										  
										  <div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.sentByVehicle.$invalid && (BudRemovedCane.sentByVehicle.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control" ng-required="true" placeholder="Sent by Vehicle" name="sentByVehicle" ng-model="RemovedBudCane.sentByVehicle" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.sentByVehicle.$error.required && (BudRemovedCane.sentByVehicle.$dirty || Filtersubmitted)" class="help-block">Sent By Vehicle is required</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.driverMobileNumber.$invalid && (BudRemovedCane.driverMobileNumber.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control" ng-required="true" placeholder="Driver Mobile Number" name="driverMobileNumber" ng-model="RemovedBudCane.driverMobileNumber" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.driverMobileNumber.$error.required && (BudRemovedCane.driverMobileNumber.$dirty || Filtersubmitted)" class="help-block">Driver mobile Number is required</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.destination.$invalid && (BudRemovedCane.destination.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    			<input type="text" class="form-control" ng-required="true"  placeholder="Destination" name="destination" ng-model="RemovedBudCane.destination" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.destination.$error.required && (BudRemovedCane.destination.$dirty || Filtersubmitted)" class="help-block">Destination is required</p>		 
											  </div>
			                    	      </div>
										</div>										
							</div>
							<div class="row">
										
										  
										  <div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.vehicleNo.$invalid && (BudRemovedCane.vehicleNo.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    		<input type="text" class="form-control" ng-required="true"  placeholder="Vehicle No." name="vehicleNo" ng-model="RemovedBudCane.vehicleNumber" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.vehicleNo.$error.required && (BudRemovedCane.vehicleNo.$dirty || Filtersubmitted)" class="help-block">Vehicle No. is required</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.driverName.$invalid && (BudRemovedCane.driverName.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line"> 
    	            						    		<input type="text" class="form-control" ng-required="true"  placeholder="Driver Name" name="driverName" ng-model="RemovedBudCane.driverName"  />
		                	            			  </div>
					<p ng-show="BudRemovedCane.driverName.$error.required && (BudRemovedCane.driverName.$dirty || Filtersubmitted)" class="help-block">Driver Name is required</p>		 
											  </div>
			                    	      </div>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : BudRemovedCane.sentTo.$invalid && (BudRemovedCane.sentTo.$dirty || Filtersubmitted)}">												
					        	                	<div class="fg-line">
    	            						    			<input type="text" class="form-control" ng-required="true"  placeholder="Sent to" ng-model="RemovedBudCane.sentTo" name="sentTo" />
		                	            			  </div>
					<p ng-show="BudRemovedCane.sentTo.$error.required && (BudRemovedCane.sentTo.$dirty || Filtersubmitted)" class="help-block">Sent to is required</p>		 
											  </div>
			                    	      </div>
										</div>										
							</div>
							
						<div align="center"><button type="submit" class="btn btn-primary btn-hide">Save</button>
						 &nbsp;<button type="button" class="btn btn-primary btn-hide" ng-click="reset(BudRemovedCane);">Reset</button>
						 </div>	
					</form>		
							
							
											 		
						 
							
							<!----------table grid design--------->
					
											
					        
			    	</div>
					
					
				</div>
		</section>     
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
