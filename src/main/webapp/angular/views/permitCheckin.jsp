	<script>
	
	
(function () {
    function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
        document.getElementById('time1').value = h + ":" + m + ":" + s;
		//$scope.AddPermitCheckin.caneWeighmentTime = h + ":" + m + ":" + s;
		//alert( h + ":" + m + ":" + s);
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
})();
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="PermitCheckinController" ng-init="loadVehicleMaxId();loadServerDate();loadCaneWeightSeasonNames();loadWeighBridgeNames();loadServerTime();loadCaneWeighMaxId();loadVehicleDetailsByShifts();getLoginUser();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Permit Checkin</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>	
								 	
					 	<form name="Permitcheckinform" novalidate ng-submit="SavepermitCheckin(AddPermitCheckin,Permitcheckinform);">
							  <input type="hidden" name="shiftTime" ng-model="AddPermitCheckin.shiftTime" />
							  <div class="row">
							  	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group" ng-class="{ 'has-error' : Permitcheckinform.season.$invalid && (Permitcheckinform.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select class="w-100" chosen name="season" ng-model="AddPermitCheckin.season" tabindex="1" ng-required="true" ng-options="season.season as season.season for season in seasonNames | orderBy:'-season':true">
															<option value="">Select Season</option>
														</select>
				                	    		    </div>
													<p ng-show="Permitcheckinform.season.$error.required && (Permitcheckinform.season.$dirty || submitted)" class="help-block">Season is Required.</p>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : Permitcheckinform.caneWeighmentDate.$invalid && (Permitcheckinform.caneWeighmentDate.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="caneWeighmentDate" ng-model="AddPermitCheckin.caneWeighmentDate" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-change="getShiftId(AddPermitCheckin.caneWeighmentDate);"/>	
				        		        	        </div>
													<p ng-show="Permitcheckinform.caneWeighmentDate.$error.required && (Permitcheckinform.caneWeighmentDate.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : Permitcheckinform.caneWeighmentTime.$invalid && (Permitcheckinform.caneWeighmentTime.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="time1">Time</label>
													
														 <input id="time1" class="form-control" placeholder='Time'  with-floating-label name="caneWeighmentTime" ng-model="AddPermitCheckin.caneWeighmentTime"  tabindex="3" ng-required="true" data-input-mask="{mask: '00:00:00'}" maxlength="10" readonly="readonly">		
				                			        </div>
													<p ng-show="Permitcheckinform.caneWeighmentTime.$error.required && (Permitcheckinform.caneWeighmentTime.$dirty || submitted)" class="help-block">Time is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Shift">Shift</label>
														<input type="text" class="form-control" placeholder='Shift' id="Shift"  with-floating-label name="shiftId" ng-model="AddPermitCheckin.shiftId" readonly />	
				            		    	        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							
							
							
							
								
							<div class="row" >
								<div class="col-sm-3">
								
								<div class="row">
										<div class="col-sm-12">
										
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:; margin-bottom:-30px;"><span> Shifts</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:; margin-bottom:-40px;"><span> All Vehicles</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<br />
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:;margin-bottom:-30px;"><span> Waiting for weighment</span>	</p>
										</div>
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-2 applybgColorRed ">
									<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:;margin-bottom:-30px;"><span> A</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="shiftAVehicle">Shift A Vehicle</label>-->
														<input type="text" class="form-control" placeholder='Shift A Vehicle' autofocus id="shiftAVehicle"   name="shiftAVehicle"  ng-model="AddPermitCheckin.shiftAVehicle"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="weightintons">Shift A Vehicle Weight</label>-->
														<input type="text" class="form-control" placeholder='Weight in tons' autofocus id="weightintons"   name="shiftAVehicle"  ng-model="AddPermitCheckin.shiftAVehiclewaiting"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-2 applybgColorBlue">
								<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:;margin-bottom:-30px;"><span>B</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										
										
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="shiftBVehicle">Shift B Vehicle</label>-->
														<input type="text" class="form-control" placeholder='Shift B Vehicle' autofocus id="shiftBVehicle"   name="shiftBVehicle"  ng-model="AddPermitCheckin.shiftBVehicle"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="weightintonsB">Shift B Vehicle Weight</label>-->
														<input type="text" class="form-control" placeholder='Weight in tons' autofocus id="weightintonsB"   name="shiftBVehicleweight"  ng-model="AddPermitCheckin.shiftBVehiclewaiting"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-2 applybgColorGreen">
								<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:;margin-bottom:-30px;"><span> C</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
									
											
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
													<!--	<label for="shiftCVehicle">Shift C Vehicle</label>-->
														<input type="text" class="form-control" placeholder='Shift C Vehicle' autofocus id="shiftCVehicle"   name="shiftCVehicle"  ng-model="AddPermitCheckin.shiftCVehicle"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="weightintonsC">Shift B Vehicle Weight</label>-->
														<input type="text" class="form-control" placeholder='Weight in tons' autofocus id="weightintonsC"   name="shiftBVehicleweight"  ng-model="AddPermitCheckin.shiftCVehiclewaiting"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-2">
								<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<p  style="font-size:16px;text-align:left;font-family:Geneva, Arial, Helvetica, sans-serif; font-weight:;margin-bottom:-30px;"><span> Total</span>	</p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="shiftATotalVehicle">Shift A Total Vehicles</label>-->
														<input type="text" class="form-control" placeholder='Total' autofocus id="shiftATotalVehicle"   name="shiftATotalVehicle" readonly ng-model="AddPermitCheckin.AllshiftTotalVehicle"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" >
						        	                <div class="fg-line">
														<!--<label for="shiftweightTotal">Total</label>-->
														<input type="text" class="form-control" placeholder='Weight in tons' autofocus id="shiftweightTotal"   name="shiftweightTotal" readonly ng-model="AddPermitCheckin.AllshiftwaitingTotal"/>
														
				                			        </div>
													

												</div>																																																		
					                    	</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-3">
									
								</div>
								</div>	
								
								<div class="row">
								<div class="col-sm-3">
									
								</div>
								
							
								</div>
								
							 <br />
							 
							  <div class="row">
							  	<div class="col-sm-6">
								<div class="row">
								<div class="col-sm-12">											
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Permitcheckinform.permitNumber.$invalid && (Permitcheckinform.permitNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="PermitNumber">Permit Number</label>
														<input type="text" class="form-control" placeholder='Permit Number' autofocus id="PermitNumber" maxlength="10" with-floating-label name="permitNumber" ng-model="AddPermitCheckin.permitNumber" ng-blur="validatePermitNumber(AddPermitCheckin.permitNumber);" tabindex="6" ng-required="true" data-ng-pattern="/^[0-9]+$/" ng-readonly="readonlyStatusforPermit"/>
														
				                			        </div>
<p ng-show="Permitcheckinform.permitNumber.$error.required && (Permitcheckinform.permitNumber.$dirty || submitted)" class="help-block">Permit Number is Required.</p>
<p ng-show="Permitcheckinform.permitNumber.$error.pattern && (Permitcheckinform.permitNumber.$dirty || submitted)" class="help-block">Enter Valid Permit Number.</p>													

												</div>																																																		
					                    	</div>
										</div>
									</div>
								</div>
								
								<div class=col-sm-6>
								<div class="row">
									<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floaing-label-wrapper" ng-class="{ 'has-error' : Permitcheckinform.serialNumber.$invalid && (Permitcheckinform.serialNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="SerialNumber">Admission No.</label>
														<input type="text" class="form-control" placeholder='Admission No.' id="SerialNumber" maxlength="10" with-floating-label name="serialNumber" ng-model="AddPermitCheckin.serialNumber" tabindex="4" maxlength="10" data-ng-pattern="/^[0-9]+$/"  ng-required="weightStatus" ng-readonly="readonlyStatus" /> 
				                			        </div>
<p ng-show="Permitcheckinform.serialNumber.$error.required && (Permitcheckinform.serialNumber.$dirty || submitted)" class="help-block">Admission Number is Required.</p>												
<p ng-show="Permitcheckinform.serialNumber.$error.pattern && (Permitcheckinform.serialNumber.$dirty || submitted)" class="help-block">Enter Valid Admission Number.</p>
<p id="validateSerialError" style="display:none; color:#FF0000;">Permit is already closed for the given serial number. Please enter valid Admission number and try again</p>													
												</div>
					                    	</div>
										</div>
										</div>
							  </div>							
							  </div>
							 <input type="hidden" name="extentSize" ng-model="AddPermitCheckin.extentSize" />							
							 <input type="hidden" name="varietyCode" ng-model="AddPermitCheckin.varietyCode" />							
							 <input type="hidden" name="villageCode" ng-model="AddPermitCheckin.villageCode" />							
							 <input type="hidden" name="circleCode" ng-model="AddPermitCheckin.circleCode" />		
							 <input type="hidden" name="agreementNumber" ng-model="AddPermitCheckin.agreementNumber" />
							 <input type="hidden" name="plantOrRatoon" ng-model="AddPermitCheckin.plantOrRatoon" />
							 <input type="hidden" name="plotNumber" ng-model="AddPermitCheckin.plotNumber" />		
							 <input type="hidden" name="zoneCode" ng-model="AddPermitCheckin.zoneCode" />
							 <input type="hidden" name="programNumber" ng-model="AddPermitCheckin.programNumber" />							 					 	
							 						
							  <div class="row">
							  	<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="RyotCode">Ryot Code</label>
														<input type="text" class="form-control" placeholder='Ryot Code' id="RyotCode" readonly with-floating-label name="ryotCode" ng-model="AddPermitCheckin.ryotCode"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="PlantRatoon">Plant / Ratoon</label>
														<input type="text" class="form-control" placeholder='Plant / Ratoon' id="PlantRatoon" readonly with-floating-label name="plant" ng-model="AddPermitCheckin.plant"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="RyotName">Ryot Name</label>
														<input type="text" class="form-control" placeholder='Ryot Name' id="RyotName" readonly with-floating-label name="ryotName" ng-model="AddPermitCheckin.ryotName"/>														
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="CaneVariety">Cane Variety</label>
														<input type="text" class="form-control" placeholder='Cane Variety' id="CaneVariety" readonly with-floating-label name="variety" ng-model="AddPermitCheckin.variety"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="FHGName">F_H_G Name</label>
														<input type="text" class="form-control" placeholder='F_H_G Name' id="FHGName" readonly with-floating-label name="relativeName" ng-model="AddPermitCheckin.relativeName"/>
				                	    		    </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Circle">Circle</label>
														<input type="text" class="form-control" placeholder='Circle' id="Circle" readonly with-floating-label name="circle" ng-model="AddPermitCheckin.circle"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="Village">Village</label>
														<input type="text" class="form-control" placeholder='Village' id="Village" readonly with-floating-label name="village" ng-model="AddPermitCheckin.village"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="LVCode">LV Code</label>
														<input type="text" class="form-control" placeholder='LV Code' id="LVCode" readonly with-floating-label name="landVillageCode" ng-model="AddPermitCheckin.landVillageCode"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
							  </div>
							
							 <div class="row">
							 <div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Permitcheckinform.vehicleregno.$invalid && (Permitcheckinform.vehicleregno.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="vehicleregno">Vehicle Reg. Number</label>
														<input type="text" class="form-control" placeholder='Vehicle Reg.Number' id="vehicleregno" maxlength="12" with-floating-label name="vehicleregno" ng-model="AddPermitCheckin.vehicleregno" tabindex="7" ng-required="true" data-ng-pattern="/^[a-zA-Z0-9 ]+$/" ng-readonly="readonlyStatusforPermit"/>
				                			        </div>
<p ng-show="Permitcheckinform.vehicleregno.$error.required && (Permitcheckinform.vehicleregno.$dirty || submitted)" class="help-block">Vehicle Reg. Number is Required.</p>
<p ng-show="Permitcheckinform.vehicleregno.$error.pattern && (Permitcheckinform.vehicleregno.$dirty || submitted)" class="help-block">Enter Reg. Valid Vehicle Number.</p>													
													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							 	<div class="col-sm-2">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : Permitcheckinform.vehicleNumber.$invalid && (Permitcheckinform.vehicleNumber.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="VehicleNumber">Vehicle Number</label>
														<input type="text" class="form-control" placeholder='Vehicle Number' id="VehicleNumber" maxlength="12" with-floating-label name="vehicleNumber" ng-model="AddPermitCheckin.vehicleNumber" tabindex="7" ng-required="true" data-ng-pattern="/^[a-zA-Z0-9 ]+$/" ng-readonly="readonlyStatusforPermit"/>
				                			        </div>
<p ng-show="Permitcheckinform.vehicleNumber.$error.required && (Permitcheckinform.vehicleNumber.$dirty || submitted)" class="help-block">Vehicle Number is Required.</p>
<p ng-show="Permitcheckinform.vehicleNumber.$error.pattern && (Permitcheckinform.vehicleNumber.$dirty || submitted)" class="help-block">Enter Valid Vehicle Number.</p>													
													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
						        	                <div class="fg-line">
														<label for="OperatorName">Operator Name</label>
														<input type="text" class="form-control" placeholder='Operator Name' id="OperatorName" readonly with-floating-label name="operatorName" ng-model="AddPermitCheckin.operatorName"/>
				                			        </div>
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Vehicle :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					        <label class="radio radio-inline m-r-5px" >
										            <input type="radio" name="vehicle" value="0" ng-model="AddPermitCheckin.vehicle"/>
			    					            	<i class="input-helper"></i>Tractor
										          </label>
				 			            		<label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="vehicle" value="1" ng-model="AddPermitCheckin.vehicle"/>
					    				            <i class="input-helper"></i>Small Lorry
							  		              </label>
				 					             <label class="radio radio-inline m-r-5px" >
				            					    <input type="radio" name="vehicle" value="2" ng-model="AddPermitCheckin.vehicle"/>
			    				        		    <i class="input-helper"></i>Big Lorry
							  		              </label>								  
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								
								
								
							 </div><br />
							 
						
							
							 								  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(Permitcheckinform)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
