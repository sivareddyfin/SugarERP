	<script type="text/javascript">		
		$('#autofocus').focus();			
	</script>

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="BankMaster" ng-init="loandMaxBankCode();loadAddedBanks();addFormField();loadStateDropDown();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Bank Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					   <div class="row">
									<div class="col-sm-12">
					                    <div class="input-group">
        	    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
				        	                <div class="fg-line">
            						          <div class="select">
	                				    		<select chosen class="w-100" name="AddedBankName" ng-model='AddBankDrop.AddedBankName' ng-options="AddedBankName.id as AddedBankName.bankname for AddedBankName in AddedBankDropdown | orderBy:'-bankname':true" ng-change="loadAddedBanksonChange();">
													<option value="">Added Banks</option>
				        		    	        </select>
                	            			  </div>
			        	        	        </div>
			            	        	</div>			                    
				            	      </div>								
    	        			 </div><br /> 					 
					   <form name="BankMasterForm" novalidate ng-submit='AddBanks(AddBank,BankMasterForm);'>
							 <div class="row">
							 	<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
					        	                <div class="fg-line">
												<label for="bankCode">Bank Code</label>
        		    					          <input type="text" class="form-control" placeholder="Bank Code"  maxlength="15" readonly data-ng-model='AddBank.bankCode' id="bankCode" with-floating-label> 
			    		            	        </div>
			            		        	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.bankName.$invalid && (BankMasterForm.bankName.$dirty || submitted)}">																																															
					        	                <div class="fg-line">
												<label for="bankName">Bank Name</label>
        		    					          <input type="text" class="form-control" placeholder="Bank Name" maxlength="50" name="bankName" data-ng-model='AddBank.bankName' data-ng-required='true' data-ng-pattern="/^[a-z A-Z.-\s]*$/" autofocus ng-blur="spacebtw('bankName');validateDup(AddBank.bankCode);" id="bankName" with-floating-label>
			    		            	        </div>
	<p ng-show="BankMasterForm.bankName.$error.required && (BankMasterForm.bankName.$dirty || submitted)" class="help-block">Bank Name is required.</p>												
	<p ng-show="BankMasterForm.bankName.$error.pattern  && (BankMasterForm.bankName.$dirty || submitted)" class="help-block">Enter a valid Bank Name.</p>
	<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddBank.bankName!=null">Bank Name Already Exist.</p>																																									
												
</div>												
			            		        	</div>
										</div>
									</div>
								</div>
							 </div><br />
							 <input type="hidden"  name="modifyFlag" data-ng-model="AddBank.modifyFlag"  />
							 	<p><b><u>Branch Details</u> :</b></p>				
								<!--<div class="table-responsive">
								<section class="asdok">
										<div class="container1">
									<table class="table table-striped table-vmiddle">
										<thead>
											<tr>
												<th><span>Action</span></th>
												<th width="150"><span>Branch<br> Code</span></th>
												<th width="150"><span>Branch<br> Name</span></th>
												<th width="150"><span>IFSC <br>Code</span></th>
												<th width="150"><span>MICR <br>Code</span></th>
												<th width="150"><span>GL <br>Code</span></th>
												<th width="150"><span>Contact <br> Person</span></th>
												<th width="150"><span>Contact<br> No.</span></th>
												<th width="150"><span>E-Mail</span></th>
												<th width="150"><span>Address</span></th>
												<th width="150"><span>City/<br>Village</span></th>
												<th width="150"><span>State</span></th>												
												<th width="150"><span title="District">Dist.</span></th>
												<th width="150"><span>Status</span></th>
											</tr>
										</thead>										
										<tbody>
											<tr ng-repeat='BranchData in data'>
												<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="BranchData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="BranchData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="BranchData.branchCode"/>
												</td>
												<td>
		<input type="text" class="form-control1" placeholder="Branch Code" name="branchCode" readonly data-ng-model="BranchData.branchCode" style="margin-top:-10px;" tooltip-placement="bottom"  uib-tooltip="{{BranchData.branchCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.branchCode"/>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.branchName{{BranchData.id}}.$invalid && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)}">																																																											
<input type="text" class="form-control1" placeholder="Branch Name" name="branchName{{BranchData.id}}"  data-ng-model="BranchData.branchName" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" ng-blur="validateDuplicate(BranchData.branchName,BranchData.id);" tooltip-placement="bottom"  uib-tooltip="{{BranchData.branchName}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.branchName"/>
	<p ng-show="BankMasterForm.branchName{{BranchData.id}}.$error.required && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="BankMasterForm.branchName{{BranchData.id}}.$error.pattern  && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>	
	<p class="help-block duplicate{{BranchData.id}}" style="color:#FF0000; display:none;" ng-if="BranchData.branchName!=null">Branch Name Already Exist.</p>																																																				 
</div>												 

												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.ifscCode{{BranchData.id}}.$invalid && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="IFSC Code" name="ifscCode{{BranchData.id}}" data-ng-model="BranchData.ifscCode" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" uib-tooltip="{{BranchData.ifscCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.ifscCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.ifscCode{{BranchData.id}}.$error.required && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.ifscCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.micrCode{{BranchData.id}}.$invalid && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)}">																																																																																			
<input type="text" class="form-control1" placeholder="MICR Code" name="micrCode{{BranchData.id}}" data-ng-model="BranchData.micrCode" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" uib-tooltip="{{BranchData.micrCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.micrCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.micrCode{{BranchData.id}}.$error.required && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.micrCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div>												  
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.glCode{{BranchData.id}}.$invalid && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)}">																																																																																															
<input type="text" class="form-control1" placeholder="GL Code"  name="glCode{{BranchData.id}}" data-ng-model="BranchData.glCode" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" uib-tooltip="{{BranchData.glCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.glCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.glCode{{BranchData.id}}.$error.required && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.glCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.contactPerson{{BranchData.id}}.$invalid && (BankMasterForm.contactPerson{{BranchData.id}}.$dirty || submitted)}">																																																																																																																							
<input type="text" class="form-control1" placeholder="Contact Person" name="contactPerson{{BranchData.id}}" data-ng-model="BranchData.contactPerson" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" uib-tooltip="{{BranchData.contactPerson}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.contactPerson" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.contactPerson{{BranchData.id}}.$error.required && (BankMasterForm.contactPerson{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.contactPerson{{BranchData.id}}.$error.pattern  && (BankMasterForm.contactPerson{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div>											
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.contactNo{{BranchData.id}}.$invalid && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																			
												  <input type="text" class="form-control1" placeholder="Contact No." name="contactNo{{BranchData.id}}" data-ng-model="BranchData.contactNo" maxlength="10" data-ng-minlength="10" data-ng-required="true" data-ng-pattern='/^[7890]\d{9,10}$/'  uib-tooltip="{{BranchData.contactNo}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.contactNo" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.required && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.pattern  && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Should Start with 7,8,9 Series</p>
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.minlength  && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Too Short</p>																																																																
												  
</div>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.email{{BranchData.id}}.$invalid && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																															
					<input type="email" class="form-control1" placeholder="E-mail" name="email{{BranchData.id}}" data-ng-model="BranchData.email" data-ng-required='true' uib-tooltip="{{BranchData.email}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.email" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.email{{BranchData.id}}.$error.required && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.email{{BranchData.id}}.$error.email  && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																																	 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.address{{BranchData.id}}.$invalid && (BankMasterForm.address{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																											
	<input type="text" class="form-control1" placeholder="Address" name="address{{BranchData.id}}" data-ng-model="BranchData.address" data-ng-required='true' uib-tooltip="{{BranchData.address}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.address" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.address{{BranchData.id}}.$error.required && (BankMasterForm.address{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.city{{BranchData.id}}.$invalid && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="City/Village" name="city{{BranchData.id}}" data-ng-model="BranchData.city" data-ng-pattern="/^[a-z A-Z.\s]*$/" data-ng-required='true' style="width:100px" uib-tooltip="{{BranchData.city}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.city" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.required && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.pattern  && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.stateCode{{BranchData.id}}.$invalid && (BankMasterForm.stateCode{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																															
		<select class="form-control1" name="stateCode{{BranchData.id}}" data-ng-model="BranchData.stateCode"  data-ng-required='true' 
		ng-options="stateCode.id as stateCode.state for stateCode in AddStates  | orderBy:'-state':true"
		ng-change="loadDistrictDropDown(BranchData.stateCode);" uib-tooltip="{{stateCodeTooltip}}" 
		tooltip-trigger="mouseenter" tooltip-enable="BranchData.stateCode" tooltip-placement="bottom">
			<option value="">State</option>
		</select>
	<p ng-show="BankMasterForm.stateCode{{BranchData.id}}.$error.required && (BankMasterForm.stateCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>																
</div>												 
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.districtCode{{BranchData.id}}.$invalid && (BankMasterForm.districtCode{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																			
		<select class="form-control1" name="districtCode{{BranchData.id}}" data-ng-model="BranchData.districtCode" data-ng-required='true'
		ng-options="districtCode.id as districtCode.district for districtCode in AddDistricts | orderBy:'-district':true" 
		ng-change="loadDistrictsDropDown(BranchData.districtCode);"
		uib-tooltip="{{districtCodeTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.districtCode" tooltip-placement="bottom">
			<option value="">District</option>
		</select>
	<p ng-show="BankMasterForm.districtCode{{BranchData.id}}.$error.required && (BankMasterForm.districtCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>														
</div>											
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.status{{BranchData.id}}.$invalid && (BankMasterForm.status{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																																											
		<select class="form-control1" name="status{{BranchData.id}}" data-ng-model="BranchData.status" data-ng-required='true' uib-tooltip="{{statusTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.status" tooltip-placement="bottom" 
		ng-change="setTooltipStatus(BranchData.status);">
			<option value="">Status</option><option value="0">Active</option><option value="1">Inactive</option>
		</select>
<p ng-show="BankMasterForm.status{{BranchData.id}}.$error.required && (BankMasterForm.status{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>																		
</div>												 
												</td>
											</tr>

										</tbody>
									</table>
									</div>
									</section>
								</div>-->
								
								
								<tabset justified="true">
			                <tab  heading="Branch Information">
								<div class="table-responsive">
								  						
									<section class="asdok">
										<div class="container1">																											
										        <table class="table table-striped" style="width:100%;">								                  
													<thead>
														<tr>
															<th><span>Action</span></th>
															<th><span>Branch Code</span></th>
															<th><span>Branch Name</span></th>
															<th><span>IFSC Code</span></th>
															<th><span>MICR Code</span></th>
															<th><span>GL Code</span></th>
														
														</tr>
													</thead>
													<tbody>
														<!--<tr ng-repeat="AdvanceData in AdvanceTab">-->
															<tr ng-repeat='BranchData in data'>
															<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="BranchData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="BranchData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="BranchData.branchCode"/>


<!--<input type="text" class="form-control1" placeholder="Branch Code" name="branchCode" readonly data-ng-model="BranchData.branchCode" style="margin-top:-10px;" tooltip-placement="bottom"  uib-tooltip="{{BranchData.branchCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.branchCode"/>-->
												</td>
												<td>
												<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.branchCode{{BranchData.id}}.$invalid && (BankMasterForm.branchCode{{BranchData.id}}.$dirty || submitted)}">																																																											
<input type="text" class="form-control1" placeholder="Branch Code" name="branchCode{{BranchData.id}}"  data-ng-model="BranchData.branchCode"  data-ng-pattern="/^[0-9.\s]*$/"]  tooltip-placement="bottom"  uib-tooltip="{{BranchData.branchCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.branchCode" />
	<p ng-show="BankMasterForm.branchCode{{BranchData.id}}.$error.required && (BankMasterForm.branchCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="BankMasterForm.branchCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.branchCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>	
	<!--<p class="help-block duplicate{{BranchData.id}}" style="color:#FF0000; display:none;" ng-if="BranchData.branchCode!=null">Branch Name Already Exist.</p>-->																																																				 
</div>
		
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.branchName{{BranchData.id}}.$invalid && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)}">																																																											
<input type="text" class="form-control1" placeholder="Branch Name" name="branchName{{BranchData.id}}"  data-ng-model="BranchData.branchName" data-ng-required='true'  data-ng-pattern="/^[a-z A-Z.\s]*$/"  tooltip-placement="bottom"  uib-tooltip="{{BranchData.branchName}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.branchName"/>
	<p ng-show="BankMasterForm.branchName{{BranchData.id}}.$error.required && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)" class="help-block">Required.</p>												
	<p ng-show="BankMasterForm.branchName{{BranchData.id}}.$error.pattern  && (BankMasterForm.branchName{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>	
																																																				 
</div>												 

												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.ifscCode{{BranchData.id}}.$invalid && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)}">																																																																							
<input type="text" class="form-control1" placeholder="IFSC Code" name="ifscCode{{BranchData.id}}" data-ng-model="BranchData.ifscCode" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" uib-tooltip="{{BranchData.ifscCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.ifscCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.ifscCode{{BranchData.id}}.$error.required && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.ifscCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.ifscCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.micrCode{{BranchData.id}}.$invalid && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)}">																																																																																			
<input type="text" class="form-control1" placeholder="MICR Code" name="micrCode{{BranchData.id}}" data-ng-model="BranchData.micrCode" data-ng-required='true' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" uib-tooltip="{{BranchData.micrCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.micrCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.micrCode{{BranchData.id}}.$error.required && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.micrCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.micrCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div>												  
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.glCode{{BranchData.id}}.$invalid && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)}">																																																																																															
<input type="text" class="form-control1" placeholder="GL Code"  name="glCode{{BranchData.id}}" data-ng-model="BranchData.glCode" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" uib-tooltip="{{BranchData.glCode}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.glCode" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.glCode{{BranchData.id}}.$error.required && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.glCode{{BranchData.id}}.$error.pattern  && (BankMasterForm.glCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div>
												</td>
														</tr>											
													</tbody>
												</table>									  												
										</div>
									</section>
																				
									
								</div>
    			            </tab>
                			<tab  heading="Contact Information">
								<div class="table-responsive">
								
									<section class="asdok">
										<div class="container1">																			
							                  <table class="table table-striped" style="width:100%;">								                  
												 <thead>
													<tr>
													
														<th><span>Action</span></th>
														<th><span>Contact Person</span></th>
														<th><span>Contact No.</span></th>
														<th><span>Email</span></th>
														<th><span>Address</span></th>
														
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat='BranchData in data'>
													<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="BranchData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="BranchData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="BranchData.branchCode"/>
												</td>
													<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.contactPerson{{BranchData.id}}.$invalid && (BankMasterForm.contactPerson{{BranchData.id}}.$dirty || submitted)}">																																																																																																																							
<input type="text" class="form-control1" placeholder="Contact Person" name="contactPerson{{BranchData.id}}" data-ng-model="BranchData.contactPerson"   uib-tooltip="{{BranchData.contactPerson}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.contactPerson" tooltip-placement="bottom"/>
																																																																	

</div>											
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.contactNo{{BranchData.id}}.$invalid && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																			
												  <input type="text" class="form-control1" placeholder="Contact No." name="contactNo{{BranchData.id}}" data-ng-model="BranchData.contactNo" maxlength="10" data-ng-minlength="10" data-ng-required="true" data-ng-pattern='/^[7890]\d{9,10}$/'  uib-tooltip="{{BranchData.contactNo}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.contactNo" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.required && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.pattern  && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Should Start with 7,8,9 Series</p>
	<p ng-show="BankMasterForm.contactNo{{BranchData.id}}.$error.minlength  && (BankMasterForm.contactNo{{BranchData.id}}.$dirty || submitted)" class="help-block">Too Short</p>																																																																
												  
</div>
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.email{{BranchData.id}}.$invalid && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																															
					<input type="email" class="form-control1" placeholder="E-mail" name="email{{BranchData.id}}" data-ng-model="BranchData.email" data-ng-required='true' uib-tooltip="{{BranchData.email}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.email" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.email{{BranchData.id}}.$error.required && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.email{{BranchData.id}}.$error.email  && (BankMasterForm.email{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																																	 
</div>												 
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.address{{BranchData.id}}.$invalid && (BankMasterForm.address{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																											
	<input type="text" class="form-control1" placeholder="Address" name="address{{BranchData.id}}" data-ng-model="BranchData.address" data-ng-required='true' uib-tooltip="{{BranchData.address}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.address" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.address{{BranchData.id}}.$error.required && (BankMasterForm.address{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
</div>												 
												</td>
													</tr>										
												</tbody>
											</table>											
									</div>
								 </section>
								 									
								 
							  </div>
            			    </tab>
			                
            			    <tab  heading="Additional Information">
								<div class="table-responsive">
								
									<section class="asdok">
										<div class="container1">																			
							                  <table class="table table-striped" style="width:100%;">								                  
												 <thead>
													<tr>
													
														<th><span>Action</span></th>
														<th><span>City/Village</span></th>
														<th><span>State</span></th>
														<th><span>District</span></th>
														<th><span>Status</span></th>
														
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat='BranchData in data'>
													<td>
<button type="button" id="right_All_1" class="btn btn-primary" ng-click='addFormField();' ng-if="BranchData.id == '1'"><i class="glyphicon glyphicon-plus-sign"></i></button>												
<span ng-if="updateFlag!='ok'"><button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id != '1'"><i class="glyphicon glyphicon-remove-sign"></i></button></span>
<span ng-if="updateFlag=='ok'">
<span ng-if="BranchData.id!= '1'">
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id<=updateLength" ng-disabled='true'><i class="glyphicon glyphicon-remove-sign"></i></button>
	<button type="button" id="right_All_1" class="btn btn-primary" ng-click='removeRow(BranchData.id);' ng-if="BranchData.id>updateLength" ng-disabled='false'><i class="glyphicon glyphicon-remove-sign"></i></button>
	
	</span>
</span>				<input type="hidden" class="form-control1"  name="branchCode" readonly data-ng-model="BranchData.branchCode"/>
												</td>
													<!--<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.city{{BranchData.id}}.$invalid && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																							
	<input type="text" class="form-control1" placeholder="City/Village" name="city{{BranchData.id}}" data-ng-model="BranchData.city" data-ng-pattern="/^[a-z A-Z.\s]*$/" data-ng-required='true' style="width:100px" uib-tooltip="{{BranchData.city}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.city" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.required && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.pattern  && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																																		
</div>												 
												</td>-->
												
												<td><div class="form-group" ng-class="{ 'has-error' : BankMasterForm.city{{BranchData.id}}.$invalid && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)}">																																																																																																																							
<input type="text" class="form-control1" placeholder="City/Village" name="contactPerson{{BranchData.id}}" data-ng-model="BranchData.city" data-ng-required='true' data-ng-pattern="/^[a-z A-Z.\s]*$/" uib-tooltip="{{BranchData.contactPerson}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.city" tooltip-placement="bottom"/>
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.required && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>												
	<p ng-show="BankMasterForm.city{{BranchData.id}}.$error.pattern  && (BankMasterForm.city{{BranchData.id}}.$dirty || submitted)" class="help-block">Invalid</p>																																																																

</div></td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.stateCode{{BranchData.id}}.$invalid && (BankMasterForm.stateCode{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																															
		<select class="form-control1" name="stateCode{{BranchData.id}}" data-ng-model="BranchData.stateCode"  data-ng-required='true' 
		ng-options="stateCode.id as stateCode.state for stateCode in AddStates  | orderBy:'-state':true"
		ng-change="loadDistrictDropDown(BranchData.stateCode);" uib-tooltip="{{stateCodeTooltip}}" 
		tooltip-trigger="mouseenter" tooltip-enable="BranchData.stateCode" tooltip-placement="bottom">
			<option value="">State</option>
		</select>
	<p ng-show="BankMasterForm.stateCode{{BranchData.id}}.$error.required && (BankMasterForm.stateCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>																
</div>												 
												</td>
												
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.districtCode{{BranchData.id}}.$invalid && (BankMasterForm.districtCode{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																			
		<select class="form-control1" name="districtCode{{BranchData.id}}" data-ng-model="BranchData.districtCode" data-ng-required='true'
		ng-options="districtCode.id as districtCode.district for districtCode in AddDistricts | orderBy:'-district':true" 
		ng-change="loadDistrictsDropDown(BranchData.districtCode);"
		uib-tooltip="{{districtCodeTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.districtCode" tooltip-placement="bottom">
			<option value="">District</option>
		</select>
	<p ng-show="BankMasterForm.districtCode{{BranchData.id}}.$error.required && (BankMasterForm.districtCode{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>														
</div>											
												</td>
												<td>
<div class="form-group" ng-class="{ 'has-error' : BankMasterForm.status{{BranchData.id}}.$invalid && (BankMasterForm.status{{BranchData.id}}.$dirty || submitted)}">																																																																																																																																																																																																											
		<select class="form-control1" name="status{{BranchData.id}}" data-ng-model="BranchData.status" data-ng-required='true' uib-tooltip="{{statusTooltip}}" tooltip-trigger="mouseenter" tooltip-enable="BranchData.status" tooltip-placement="bottom" 
		ng-change="setTooltipStatus(BranchData.status);">
			<option value="">Status</option><option value="0">Active</option><option value="1">Inactive</option>
		</select>
<p ng-show="BankMasterForm.status{{BranchData.id}}.$error.required && (BankMasterForm.status{{BranchData.id}}.$dirty || submitted)" class="help-block">Required</p>																		
</div>												 
												</td>
													</tr>										
												</tbody>
											</table>											
									</div>
								 </section>
								 									
								 
							  </div>
            			    </tab>
            			    				
			            </tabset>

							 <div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(BankMasterForm,AddBankDrop.AddedBankName)" ng-if="AddBankDrop.AddedBankName==null">Reset</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(BankMasterForm,AddBankDrop.AddedBankName)" ng-if="AddBankDrop.AddedBankName!=null">Reset</button>										
									</div>
								</div>						 	
							 </div>	
						</form>						 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
													
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
