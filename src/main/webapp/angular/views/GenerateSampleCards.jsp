	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
		<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="generateSampleCards" ng-init="loadSeason();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Generate Sample Cards</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>
					       <form name="generatesample" ng-submit="GenerateSampleSubmit(generatesample)">
					 		<div class="row">  
								<div class="col-sm-2"></div>
								
									<div class="col-sm-3">
										<div class="input-group">
										  <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										  <div class="form-group" ng-class="{ 'has-error' : generatesample.season.$invalid && (generatesample.season.$dirty || Filtersubmitted)}">
											  <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" name="season" ng-model="GenerateCard.season" ng-options="season.season as season.season for season in SeasonNames   | orderBy:'-season':true" data-ng-required="true" ng-change="loadProgram(GenerateCard.season);">
														<option value="">Select Season</option>
													</select>
												</div>
											 </div>
								<p ng-show="generatesample.season.$error.required && (generatesample.season.$dirty || Addsubmitted)" class="help-block">Select Season</p>										
										   </div>
									   </div>
										
									</div>
								<div class="col-sm-3">
									<div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
										<div class="form-group" ng-class="{ 'has-error' : generatesample.program.$invalid && (generatesample.program.$dirty || Filtersubmitted)}">
						        	        <div class="fg-line">
												<div class="select">
													<select chosen class="w-100" name="program" ng-model="GenerateCard.program" ng-options="program.programnumber as program.programnumber for program in ProgramNames | orderBy:'-programnumber':true">
														<option value="">Select Program</option>
													</select>													  
												</div>
			    		            		</div>
											<p ng-show="generatesample.program.$error.required && (generatesample.program.$dirty || Filtersubmitted)" class="help-block">Select Program</p>										
										</div>
			            		    </div>									
								</div>
								
								
								  <div class="col-sm-4">
								  		<div class="input-group">
										<div class="fg-line">
											<button type="button" class="btn btn-primary" ng-click="getSampleCardsDetails(generatesample);">Get Details</button>
										</div>
									</div>
								  </div>						
    	        		 </div>
						 	<hr />
						 							 
						<div class="card-body card-padding">  	
							<div  class="table-responsive">
							  <section class="asdok">
								 <div class="container1">
									 <table ng-table="generateSampleCards.tableEdit" class="table table-striped table-vmiddle">							 		
							               <thead>
										        <tr>
													<!--<th><span>SL NO.</span></th>-->
													<th><span>RYOT CODE</span></th>
													<th><span>NAME</span></th>
													<th><span>AGREEMENT<br />NO.</span></th>
													<th><span>PLOT NO</span></th>
													<th><span>PLANT/RATOON<br />IN ACRES</span></th>
													<th><span>VARIETY</span></th>
													<th><span>DATE OF <br />PLANT/RATOON</span></th>
													<th><span>SAMPLE<br />CARDS</span></th>
													
												</tr>
											</thead>
											<tbody>
										        <tr ng-repeat="generate in generateData"  ng-class="{ 'active': generate.$edit }">											
												<!--<td>{{$index+1}}</td>-->											  
											  	<td>{{generate.ryotCode}}</td>											  
											  	<td>{{generate.ryotName}}</td>											  
											  	<td>{{generate.agreementNumber}}</td>											  
											  	<td>{{generate.plotNumber}}</td>											  
											  	<td>{{generate.plantOrRatooninAcres}}<input type="hidden" name="programNumber{{index}}" ng-model="generate.programNumber" /></td>										
											  	<td>{{generate.variety}}<input type="hidden" name="season{{index}}" ng-model="generate.season" /></td>											
											  	<td>{{generate.dateOfPlantorRatoon}}<input type="hidden" name="sampleCard{{index}}" ng-model="generate.sampleCard" /></td>
											  	<td>
													<div class="input-group input-group-unstyled">
														<div class="form-group" ng-class="{ 'has-error' : generatesample.sampleCardsId{{$index}}.$invalid && (generatesample.sampleCardsId{{$index}}.$dirty || Addsubmitted)}">													
                   									  		<input type="text" class="form-control1" placeholder='Sample Cards' name="sampleCardsId{{$index}}" data-ng-model="generate.sampleCardsId" readonly data-ng-required="true" readonly/ tooltip-placement="bottom"  uib-tooltip="{{generate.sampleCardsId}}" tooltip-trigger="mouseenter" tooltip-enable="generate.sampleCardsId">
															<p ng-show="generatesample.sampleCardsId{{$index}}.$error.required && (generatesample.sampleCardsId{{$index}}.$dirty || Addsubmitted)" class="help-block">Required.</p>										
									                	</div>												
													</div>
											 	</td>											  											  
											  </tr>
											</tbody>
										 </table>
									 	
								 </div>
							</section> 														 
						</div>
						<hr />								 								 
						<div class="row" align="center">            			
							<div class="input-group">
								<div class="fg-line">
									<button type="button" class="btn btn-primary btn-hide" ng-click="assignSampleCards();">Assign Sample Cards</button>
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>						 	
						</div>	
					</form>						 							 
					</div>
			    </div>
			</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>															