<style>
.styletr {border-bottom: 1px solid grey;}


</style>
<script type="text/javascript">	
		$('.autofocus').focus();
	</script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.js'></script>
	
	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>   
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>
		<section id="content" data-ng-controller="bankLoanDetailsController" ng-init="loadSeason(); loadLoanDetails(); summation();">
		  <div class="container">
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b> Bank Loan Details</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->
					 <!-------body start------>	
					     <form name="BankLoanDetailsForm" novalidate ng-submit="BankLoanDetailsSubmit(AddedBankLoanDetails,BankLoanDetailsForm)" >
                            <input type="hidden" ng-model="AddedBankLoanDetails.modifyFlag" name="modifyFlag"  />
						 <div class="row">
							 <div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						<div class="form-group " ng-class="{'has-error':BankLoanDetailsForm.season.$invalid && (BankLoanDetailsForm.season.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedBankLoanDetails.season' name="season" ng-options="season.season as season.season for season in SeasonData" ng-required="true">
															<option value="">Select Season</option>
						        			            </select>
									</div>
								<p ng-show="BankLoanDetailsForm.season.$error.required && (BankLoanDetailsForm.season.$dirty || Addsubmitted)" class="help-block">season is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-3">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="form-group " ng-class="{'has-error':BankLoanDetailsForm.loanDetails.$invalid && (BankLoanDetailsForm.loanDetails.$dirty || Addsubmitted)}">												
									<div class="fg-line">
									
										<select chosen class="w-100 form-control " tabindex="2" data-ng-model='AddedBankLoanDetails.loanDetails' name="loanDetails"    ng-options="loanDetails.loanDetails as loanDetails.loanDetails for loanDetails in loanDetailss" maxlength="20" ng-required="true"  >
														<option value=""> Loan Details</option>
						        			            </select>
									</div>
							<p ng-show="BankLoanDetailsForm.loanDetails.$error.required && (BankLoanDetailsForm.loanDetails.$dirty || Addsubmitted)" class="help-block">loanDetails is required</p>		 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							<div class="col-sm-5">
							 <div class="row">
								<div class="col-sm-12">
								<div class="input-group">
								<span class="input-group-addon"></span>
											 	<div class="fg-line">
												
												 <button type="Button"   name="get"  class="btn btn-primary btn-sm m-t-10" ng-click="updateData(AddedBankLoanDetails);">Get details</button>
<button type="Button"   name="get"  class="btn btn-primary btn-sm m-t-10" ng-click="exportData();">Export to excel</button>													 
												
								 
								</div>
								</div>			                    
								</div>
							</div>	
							</div>
							
							
							
							</div>
							<br/>
							
							<div class="table-responsive" id='hidetable' style="display:none;">
							<table  style="border-collapse:collapse;width:100%;" border="0">
								<tr class="styletr" style="font-weight:bold;height:50px;">
					             <th> SlNo</th>
								 <th> Ryot Code</th>
								 <th> Name & Village </th>
								 <th> Supply Quantity</th>
								 <th> Loan No.</th>
								 <th> Loan Ref No.</th>
								 <th>  Total Amount</th>
								 <th>  Intrest</th>
								 <th> Recovery Amount</th>
								 <th>  Balance</th>
								 </tr>
								<br>
								 	<tr class="styletr"  ng-repeat="BankLoanDetailsdata in bankLoanData"  >
									<td>
									<div class="form-group">
									     {{$index+1}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.ryotCode}}
									</div>
									</td>
									
									<td>
									<div class="form-group" >
									
		 <span style="width:100%;">{{BankLoanDetailsdata.ryotName}}</span><br>{{BankLoanDetailsdata.village}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.Quantity}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.loanNo}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.loanREfNo}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.TotalAmount}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.intrestAmt}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.TotalRecovery}}
									</div>
									</td>
									
									<td>
									<div class="form-group">
									     {{BankLoanDetailsdata.balance}}
									</div>
									</td>
									
									
									
									
									
									
									</tr>
									<tr>
									  <td colspan="6"></td><td><b>{{totalAmount}}</b></td><td><b>{{amountRecovered}}</b></td><td><b>{{totalBalance}}</b></td>
									</tr>
									
									</table>
					 </div>
				</div>
		</div>
		</section>
	</section>				 