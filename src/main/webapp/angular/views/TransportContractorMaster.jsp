	<script type="text/javascript">		
		$('.autofocus').focus();
	</script>


	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content"  data-ng-controller="TransportContractorMaster" ng-init="loadTransporterCode();loadTransporterBanks();loadTransporterBranchs();LoadAddedTransportersDropDown();addFormField();loadAllAddedTransportersData();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Transport Contractor Master</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>
					 <div class="row">
				                <div class="col-sm-12">
				                    <div class="input-group">
            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
			        	                <div class="fg-line">
											<select chosen class='w-100' name="AddedTransporters" ng-model='AddedTransporters' ng-change='loadAllAddedTransportersData(AddedTransporters);' ng-options="transporter.id as transporter.transporter for transporter in GetAllAddedTransporters | orderBy:'-transporter':true">
												<option value=''>Added Transport Contractor</option>
											</select>
			                	        </div>
			                    	</div>			                    
				                  </div>
						</div></br>
						<form name="TransportContractorForm" novalidate ng-submit="SaveTransportContractor(AddTransporter,TransportContractorForm);">	
							<input type="hidden"  name="modifyFlag" data-ng-model="AddTransporter.modifyFlag"  />						
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
						        	                <div class="fg-line">
													<label for="transporterCode">Transporter Code</label>
														<input type="text" class="form-control" placeholder="Transporter Code" maxlength="10" readonly name="transporterCode" ng-model="AddTransporter.transporterCode"  id="transporterCode" with-floating-label/>
				            		    	        </div>
			                    			</div>
										</div>
									</div><br />
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.contactNumber.$invalid && (TransportContractorForm.contactNumber.$dirty || Addsubmitted)}">												
						        	                <div class="fg-line">
													<label for="contactNumber">Contact No.</label>
														<input type="text" class="form-control" placeholder="Contact No." maxlength="10" tabindex="3" name="contactNumber" ng-model="AddTransporter.contactNumber" data-ng-pattern='/^[7890]\d{9,10}$/' data-ng-required='true' data-ng-minlength="10"  id="contactNumber" with-floating-label/>
				            		    	        </div>
<p ng-show="TransportContractorForm.contactNumber.$error.required && (TransportContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Contact Number is required</p>																									
<p ng-show="TransportContractorForm.contactNumber.$error.pattern && (TransportContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Contact Number starting with 7,8,9 series</p>																									
<p ng-show="TransportContractorForm.contactNumber.$error.minlength && (TransportContractorForm.contactNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																									
</div>													
			                    			</div>						                    			                    											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																								
					        	                <div class="fg-line">
												<label for="ifsc">IFSC Code</label>
													<input type="text" class="form-control" placeholder="IFSC Code"  maxlength="10" tabindex="6" name="ifscCode" ng-model="AddTransporter.ifscCode" readonly  id="ifsc" with-floating-label/>
			    		            	        </div>
<p></p>												
</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.panNumber.$invalid && (TransportContractorForm.panNumber.$dirty || Addsubmitted)}">																																				
					        	                <div class="fg-line">
													<label for="panNumber">PAN No.</label>
													<input type="text" class="form-control" placeholder="PAN No."  maxlength="10" tabindex="9" name="panNumber" ng-model="AddTransporter.panNumber" data-ng-required='true' data-ng-minlength="10" data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/" id="panNumber" with-floating-label/>
			            		    	        </div>
<p ng-show="TransportContractorForm.panNumber.$error.required && (TransportContractorForm.panNumber.$dirty || Addsubmitted)" class="help-block">PAN Number is required</p>												
<p ng-show="TransportContractorForm.panNumber.$error.pattern && (TransportContractorForm.panNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid PAN Number</p>
<p ng-show="TransportContractorForm.panNumber.$error.minlength && (TransportContractorForm.panNumber.$dirty || Addsubmitted)" class="help-block">PAN Number is required 10-digits</p>												
												
</div>												 
			                    			</div>																					
										</div>
									</div>
									
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
		            				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.transporter.$invalid && (TransportContractorForm.transporter.$dirty || Addsubmitted)}">																																																
					        	                <div class="fg-line">
												<label for="transporter">Transporter Name</label>
													<input type="text" class="form-control autofocus" placeholder="Transporter Name" maxlength="25" tabindex="1" name="transporter" ng-model="AddTransporter.transporter" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/" ng-blur="spacebtw('transporter');validateDup();" id="transporter" with-floating-label>
			            		    	        </div>
<p ng-show="TransportContractorForm.transporter.$error.required && (TransportContractorForm.transporter.$dirty || Addsubmitted)" class="help-block">Transporter Name is required</p>																			
<p ng-show="TransportContractorForm.transporter.$error.pattern && (TransportContractorForm.transporter.$dirty || Addsubmitted)" class="help-block">Enter Valid Transporter Name</p>	
<p class="help-block duplicate" style="color:#FF0000; display:none;" ng-if="AddTransporter.transporter!=null">Transporter Name Already Exist.</p>																		
</div>												
			                    			</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.bankCode.$invalid && (TransportContractorForm.bankCode.$dirty || Addsubmitted)}">																																																												
			    		    	                <div class="fg-line">
												
													<select chosen class="w-100" tabindex="4" name="bankCode" ng-model="AddTransporter.bankCode" data-ng-required='true' ng-options="bank.id as bank.bankname for bank in GetBankDropdownData | orderBy:'-bankname':true" ng-change="getBankbranchdata(AddTransporter.bankCode);">
														<option value="">Select Bank</option>
													</select>
			                			        </div>
<p ng-show="TransportContractorForm.bankCode.$error.required && (TransportContractorForm.bankCode.$dirty || Addsubmitted)" class="help-block">Select Bank</p>																															
</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.accountNumber.$invalid && (TransportContractorForm.accountNumber.$dirty || Addsubmitted)}">																																																																								
					        	                <div class="fg-line">
												<label for="accountNumber">Account No.</label>
													<input type="text" class="form-control" placeholder="Account No."  maxlength="16" tabindex="7" name="accountNumber" ng-model="AddTransporter.accountNumber" data-ng-required='true' data-ng-pattern="/^[0-9\s]*$/" ng-minlength='7'  id="accountNumber" with-floating-label/>
			    		            	        </div>
<p ng-show="TransportContractorForm.accountNumber.$error.required && (TransportContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Account Number is required</p>
<p ng-show="TransportContractorForm.accountNumber.$error.pattern && (TransportContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Account Number</p>																																											
<p ng-show="TransportContractorForm.accountNumber.$error.minlength && (TransportContractorForm.accountNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											

</div>												
			            		        	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Status : </span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					          <label class="radio radio-inline m-r-20">
										            <input type="radio" name="status" value="0" ng-model='AddTransporter.status'>
			    					            	<i class="input-helper"></i>Active
										          </label>
				 			            		  <label class="radio radio-inline m-r-20">
						            			    <input type="radio" name="status" value="1" ng-model='AddTransporter.status'>
					    				            <i class="input-helper"></i>Inactive
							  		              </label>										
					                	        </div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        	    				          		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.address.$invalid && (TransportContractorForm.address.$dirty || Addsubmitted)}">																																																																																				
					        	                <div class="fg-line">
												<label for="address">Address</label>
													<input type="text" class="form-control" placeholder="Address" maxlength="50" tabindex="2" name="address" ng-model="AddTransporter.address" data-ng-required='true'  ng-blur="spacebtw('address');" id="address" with-floating-label/>
					                	        </div>
<p ng-show="TransportContractorForm.address.$error.required && (TransportContractorForm.address.$dirty || Addsubmitted)" class="help-block">Address is required</p>												
</div>												
					                    	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
        		    				            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group">																																																																																																
			    		    	                <div class="fg-line">
													<select chosen class="w-100" tabindex="5" name="branchCode" ng-model="AddTransporter.branchCode"  ng-options="branchCode.id as branchCode.branchname for branchCode in GetBranchDropdownData | orderBy:'-branchname':true" ng-change="setTransporterifscCode(AddTransporter.branchCode);">
														<option value="">Select Branch</option>
													</select>
			                			        </div>
</div>												
					                    	</div>											
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.aadhaarNumber.$invalid && (TransportContractorForm.aadhaarNumber.$dirty || Addsubmitted)}">																																																																																																												
					        	                <div class="fg-line">
												<label for="aadhaarNumber">Aadhar No.</label>
													<input type="text" class="form-control" placeholder="Aadhar No." maxlength="14" tabindex="8" name="aadhaarNumber" ng-model="AddTransporter.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}" data-ng-pattern="/^[0-9\s]*$/" id="aadhaarNumber" with-floating-label />
			    		            	        </div>
<p ng-show="TransportContractorForm.aadhaarNumber.$error.required && (TransportContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Aadhar Number is required</p>
<p ng-show="TransportContractorForm.aadhaarNumber.$error.pattern && (TransportContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Enter Valid Aadhar Number</p>																																											
<p ng-show="TransportContractorForm.aadhaarNumber.$error.minlength && (TransportContractorForm.aadhaarNumber.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																											
												
</div>												
			            		        	</div>											
										</div>
									</div>									
								</div>
							</div><br />

							<p><b>Surety Persons Details :</b></p>
							<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Actions</th>
											<th>Name</th>
							                <th>Contact No.</th>
		                    				<th>Aadhar No.</th>
											<th>PAN No.</th>
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="TransporterData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="TransporterData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(TransporterData.id);" ng-if="TransporterData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.surety{{TransporterData.id}}.$invalid && (TransportContractorForm.surety{{TransporterData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Name" maxlength='8' name="surety{{TransporterData.id}}" ng-model="TransporterData.surety" data-ng-required='true' data-ng-pattern="/^[a-z A-Z\s]*$/"/>
<p ng-show="TransportContractorForm.surety{{TransporterData.id}}.$error.required && (TransportContractorForm.surety{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="TransportContractorForm.surety{{TransporterData.id}}.$error.pattern && (TransportContractorForm.surety{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.contactNo{{TransporterData.id}}.$invalid && (TransportContractorForm.contactNo{{TransporterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Contact No." maxlength="10" name="contactNo{{TransporterData.id}}" data-ng-model='TransporterData.contactNo' data-ng-required='true' data-ng-minlength="10" data-ng-pattern='/^[7890]\d{9,10}$/' />
<p ng-show="TransportContractorForm.contactNo{{TransporterData.id}}.$error.required && (TransportContractorForm.contactNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="TransportContractorForm.contactNo{{TransporterData.id}}.$error.pattern && (TransportContractorForm.contactNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Should Start with 7,8,9 series</p>												
<p ng-show="TransportContractorForm.contactNo{{TransporterData.id}}.$error.minlength && (TransportContractorForm.contactNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																								
</div>												
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$invalid && (TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Aadhar No." maxlength='14' name="aadhaarNumber{{TransporterData.id}}" ng-model="TransporterData.aadhaarNumber" data-ng-required='true' ng-minlength='14' data-input-mask="{mask: '0000 0000 0000'}" data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$error.required && (TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$error.pattern && (TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$error.minlength && (TransportContractorForm.aadhaarNumber{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : TransportContractorForm.panNo{{TransporterData.id}}.$invalid && (TransportContractorForm.panNo{{TransporterData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="PAN No." maxlength='10' name="panNo{{TransporterData.id}}" ng-model="TransporterData.panNo" data-ng-required='true' ng-minlength='10' data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/>
<p ng-show="TransportContractorForm.panNo{{TransporterData.id}}.$error.required && (TransportContractorForm.panNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="TransportContractorForm.panNo{{TransporterData.id}}.$error.pattern && (TransportContractorForm.panNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
<p ng-show="TransportContractorForm.panNo{{TransporterData.id}}.$error.minlength && (TransportContractorForm.panNo{{TransporterData.id}}.$dirty || Addsubmitted)" class="help-block">Too Short</p>																																				
												
</div>												
											</td>
											</tr>
									</tbody>
							 	</table>
							</div>							 
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(TransportContractorForm,AddedTransporters);">Reset</button>
									</div>
								</div>						 	
							</div>																	 							 
						</form>	 		 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
									
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
