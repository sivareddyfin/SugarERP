	<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
			 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="ChemicalTreatmentController" ng-init="addFormField();addFormFieldBud();loadShift();loadVarietyNames();loadVillageNames();loadseasonNames();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Chemical Treatment</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="chemicalTreatmentForm" novalidate ng-submit="SaveChemicalTreatment(AddedChemicalTreatment,chemicalTreatmentForm);">
							  <input type="hidden" name="modifyFlag" ng-model="AddedChemicalTreatment.modifyFlag" />
							  <div class="row">
							 <div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : chemicalTreatmentForm.season.$invalid && (chemicalTreatmentForm.season.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedChemicalTreatment.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change ="getChemicalTreatmentList(AddedChemicalTreatment.season,AddedChemicalTreatment.date,AddedChemicalTreatment.shift);">
															<option value="">Season</option>
						        			            </select>	
				        		        	        </div>
													<p ng-show="chemicalTreatmentForm.season.$error.required && (chemicalTreatmentForm.season.$dirty || submitted)" class="help-block">Season</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : chemicalTreatmentForm.date.$invalid && (chemicalTreatmentForm.date.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="date" ng-model="AddedChemicalTreatment.date" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-blur ="getChemicalTreatmentList(AddedChemicalTreatment.season,AddedChemicalTreatment.date,AddedChemicalTreatment.shift);" />	
				        		        	        </div>
													<p ng-show="chemicalTreatmentForm.date.$error.required && (chemicalTreatmentForm.date.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group floating-label-wrapper"  ng-class="{ 'has-error' : chemicalTreatmentForm.shift.$invalid && (chemicalTreatmentForm.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														<select chosen class="w-100" name="shift" ng-required="true"  ng-model="AddedChemicalTreatment.shift" ng-required="true" ng-options="shift.id as shift.shiftName for shift in WeighBridgeNames | orderBy:'-shiftName':true" ng-change ="getChemicalTreatmentList(AddedChemicalTreatment.season,AddedChemicalTreatment.date,AddedChemicalTreatment.shift);">
															<option value="">Shift</option>
															
														</select>
				                			        </div>
													<p ng-show="chemicalTreatmentForm.shift.$error.required && (chemicalTreatmentForm.shift.$dirty || submitted)" class="help-block">Shift is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
									
								</div>
							  </div>
							
							  
							
							  <div class="row">
							  
							  	
								
								
								
							  </div>
							 <div class="row">
							 <div class="col-sm-4">
								<div class="row">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;	Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourcharges" value="0" ng-model="AddedChemicalTreatment.labourSpplr" ng-click="getLabourContractorDetails();">
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedChemicalTreatment.labourSpplr" ng-click="getLabourContractorDetails();">
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>


										</div>

								</div>

								<div class='col-sm-4'>
								<div class="row">
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
														<select chosen class="w-100" name='lc'  ng-model="AddedChemicalTreatment.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							</div>
</div>


							 </div>
								<div ng-show="AddedChemicalTreatment.labourSpplr=='0'">						
							  <div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
									</div>
								</div>
								
							 	<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Men</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.noOfMen.$invalid && (chemicalTreatmentForm.noOfMen.$dirty || submitted)}">
												 <div class="fg-line">
												 	<label for="noOfMen">No. of Men</label>
<input type="text" class="form-control" placeholder="No. of People" name="noOfMen" ng-model="AddedChemicalTreatment.noOfMen" ng-keyup="LoadManTotal(AddedChemicalTreatment.noOfMen,AddedChemicalTreatment.manCost);getGrandTotalforCT(AddedChemicalTreatment.menTotal,AddedChemicalTreatment.womenTotal,AddedChemicalTreatment.totalLabourCost,AddedChemicalTreatment.labourSpplr);" data-ng-pattern="/^[0-9\s]*$/" id="noOfMen" with-floating-label />
												</div>
												
											<p ng-show="chemicalTreatmentForm.noOfMen.$error.pattern && (chemicalTreatmentForm.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>									
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.manCost.$invalid && (chemicalTreatmentForm.manCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="manCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="manCost" ng-model="AddedChemicalTreatment.manCost" ng-keyup="LoadManTotal(AddedChemicalTreatment.noOfMen,AddedChemicalTreatment.manCost);getGrandTotalforCT(AddedChemicalTreatment.menTotal,AddedChemicalTreatment.womenTotal,AddedChemicalTreatment.totalLabourCost,AddedChemicalTreatment.labourSpplr);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="manCost" with-floating-label   />
												</div>
												<p ng-show="chemicalTreatmentForm.manCost.$error.pattern && (chemicalTreatmentForm.manCost.$dirty || submitted)" class="help-block">Invalid</p>									
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="menTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="menTotal" ng-model="AddedChemicalTreatment.menTotal" readonly="readonly" id="menTotal" with-floating-label />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.noOfWomen.$invalid && (chemicalTreatmentForm.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="noOfWomen">No. of Women</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfWomen" ng-model="AddedChemicalTreatment.noOfWomen" ng-keyup="LoadWoManTotal(AddedChemicalTreatment.noOfWomen,AddedChemicalTreatment.womanCost);getGrandTotalforCT(AddedChemicalTreatment.menTotal,AddedChemicalTreatment.womenTotal,AddedChemicalTreatment.totalLabourCost,AddedChemicalTreatment.labourSpplr);" data-ng-pattern="/^[0-9\s]*$/" id="noOfWomen" with-floating-label />
												</div>
										<p ng-show="chemicalTreatmentForm.noOfWomen.$error.pattern && (chemicalTreatmentForm.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>											
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.womanCost.$invalid && (chemicalTreatmentForm.womanCost.$dirty || submitted)}">
												 <div class="fg-line">
												 <label for="womanCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="womanCost" ng-model="AddedChemicalTreatment.womanCost" ng-keyup="LoadWoManTotal(AddedChemicalTreatment.noOfWomen,AddedChemicalTreatment.womanCost);getGrandTotalforCT(AddedChemicalTreatment.menTotal,AddedChemicalTreatment.womenTotal,AddedChemicalTreatment.totalLabourCost,AddedChemicalTreatment.labourSpplr);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="womanCost" with-floating-label />
												</div>
												<p ng-show="chemicalTreatmentForm.womanCost.$error.pattern && (chemicalTreatmentForm.womanCost.$dirty || submitted)" class="help-block">Invalid</p>								
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												  <label for="womenTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" ng-model="AddedChemicalTreatment.womenTotal" readonly="readonly" id="womenTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalLabourCost">Grand Total</label>
														<input type="text" class="form-control" placeholder="Grand Total" name="totalLabourCost" ng-model="AddedChemicalTreatment.totalLabourCost" readonly="readonly" id="totalLabourCost" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
									
								</div>
								
								</div>
								<div  ng-show="AddedChemicalTreatment.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.costPerTon.$invalid && (chemicalTreatmentForm.costPerTon.$dirty || submitted)}">
							<div class="fg-line">
							<label for="costPerTon">Cost Per Ton</label>
							<input type="number" placeholder="Cost Per Ton" ng-model="AddedChemicalTreatment.costPerTon" name="costPerTon" class="form-control" ng-keyup="getContractTotalCost(AddedChemicalTreatment.costPerTon,AddedChemicalTreatment.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="costPerTon" with-floating-label  />
													
							</div>
							<p ng-show="chemicalTreatmentForm.costPerTon.$error.pattern && (chemicalTreatmentForm.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : chemicalTreatmentForm.totalTons.$invalid && (chemicalTreatmentForm.totalTons.$dirty || submitted)}">
							<div class="fg-line">
							<label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons" ng-model="AddedChemicalTreatment.totalTons" class="form-control" name="totalTons" ng-keyup="getContractTotalCost(AddedChemicalTreatment.costPerTon,AddedChemicalTreatment.totalTons);" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" id="totalTons" with-floating-label  />
							</div>
							
							<p ng-show="chemicalTreatmentForm.totalTons.$error.pattern && (chemicalTreatmentForm.totalTons.$dirty || submitted)" class="help-block">Invalid</p>	
							</div>
							</div>
							</div>
									<div class="col-sm-3">
									<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="fg-line">
									<label for="totalContractAmt">Grand Total</label>
									<input type="text" placeholder="Grand Total" ng-model="AddedChemicalTreatment.totalContractAmt" class="form-control"  name="totalContractAmt" readonly="readonly"  id="totalContractAmt" with-floating-label  />
									</div>
									</div>
									</div>
								</div>
								</div>	
													 					 	
							 		
									
									
										
							<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Actions</th>
											<th>Product Code</th>
							                <th>Product Name</th>
		                    				<th>Quantity</th>
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="chemicalTreatmentData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="chemicalTreatmentData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(chemicalTreatmentData.id);" ng-if="chemicalTreatmentData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$invalid && (chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input type="text" class="form-control1" placeholder="Product Code" name="productCode{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.productCode"  data-ng-pattern="/^[a-z 0-9 A-Z.\s]*$/" />
<p ng-show="chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$error.required && (chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$error.pattern && (chemicalTreatmentForm.productCode{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.pName{{chemicalTreatmentData.id}}.$invalid && (chemicalTreatmentForm.pName{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Product Name"  name="pName{{chemicalTreatmentData.id}}" data-ng-model='chemicalTreatmentData.productName'/>
<p ng-show="chemicalTreatmentForm.pName{{chemicalTreatmentData.id}}.$error.required && (chemicalTreatmentForm.pName{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
																								
</div>												
											</td>  
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$invalid && (chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)}">																																																																																																																																													
												<input type="text" class="form-control1" placeholder="Quantity" maxlength='14' name="qty{{chemicalTreatmentData.id}}" ng-model="chemicalTreatmentData.productQuantity"    data-ng-pattern="/^[0-9\s]*$/"/>
<p ng-show="chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$error.required && (chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$error.pattern && (chemicalTreatmentForm.qty{{chemicalTreatmentData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											
											
											</tr>
									</tbody>
							 	</table>
							</div>		
							 
							
								<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Batch Series</th>
							                <th>Variety</th>
		                    				<th>Supplier</th>
											<th>Village</th>
											<th>Tub weight with Buds</th>
											<th>No. of Tubs</th>
											
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in datagrid">
											<td>
											
											
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormFieldBud();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRowBud(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											
											<td style="display:none;">
											<input type="hidden" ng-model="batchData.id" name="batchid{{batchData.id}}" />
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.batchSeries{{batchData.id}}.$invalid && (chemicalTreatmentForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
											<input list="stateList" placeholder="Batch Series" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{batchData.id}}" placeholder ="Batch No" ng-model="batchData.batchSeries" ng-change="loadBatchDetails(batchData.batchSeries,batchData.id);"> 
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
       						    </select>    
   									</datalist>

<p ng-show="chemicalTreatmentForm.batchSeries{{batchData.id}}.$error.required && (chemicalTreatmentForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.batchSeries{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
										
</div>	


						</td>
						<td style='display:none;'>
						
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.variety{{batchData.id}}.$invalid && (chemicalTreatmentForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="variety{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.variety' name="variety" readonly >
													
<p ng-show="chemicalTreatmentForm.variety{{batchData.id}}.$error.required && (chemicalTreatmentForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.variety{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.seedSupr{{batchData.id}}.$invalid && (chemicalTreatmentForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="seedSupr{{batchData.id}}" ng-model="batchData.seedSupr"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="chemicalTreatmentForm.seedSupr{{batchData.id}}.$error.required && (chemicalTreatmentForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.seedSupr{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.landVillageCode{{batchData.id}}.$invalid && (chemicalTreatmentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="landVillageCode{{batchData.id}}" placeholder="Village" data-ng-model='batchData.landVillageCode' name="landVillageCode" readonly />
<p ng-show="chemicalTreatmentForm.landVillageCode{{batchData.id}}.$error.required && (chemicalTreatmentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.landVillageCode{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
											<td>
						
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.varietyName{{batchData.id}}.$invalid && (chemicalTreatmentForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
<input type='text' class="form-control1" name="varietyName{{batchData.id}}" placeholder="Variety" data-ng-model='batchData.varietyName' name="varietyName" readonly >
													
<p ng-show="chemicalTreatmentForm.varietyName{{batchData.id}}.$error.required && (chemicalTreatmentForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.varietyName{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.ryotName{{batchData.id}}.$invalid && (chemicalTreatmentForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier"  name="ryotName{{batchData.id}}" ng-model="batchData.ryotName"   data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly>
<p ng-show="chemicalTreatmentForm.ryotName{{batchData.id}}.$error.required && (chemicalTreatmentForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.ryotName{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.lvName{{batchData.id}}.$invalid && (chemicalTreatmentForm.lvName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																	<input type="text" class="form-control1" name="lvName{{batchData.id}}" placeholder="Village" data-ng-model='batchData.lvName'  readonly />
<p ng-show="chemicalTreatmentForm.lvName{{batchData.id}}.$error.required && (chemicalTreatmentForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.lvName{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>
						
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.tubWt{{batchData.id}}.$invalid && (chemicalTreatmentForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Tub Weight with buds" maxlength='10' name="tubWt{{batchData.id}}" ng-model="batchData.tubWt" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/"/>
<p ng-show="chemicalTreatmentForm.tubWt{{batchData.id}}.$error.required && (chemicalTreatmentForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.tubWt{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : chemicalTreatmentForm.noOfTubs{{batchData.id}}.$invalid && (chemicalTreatmentForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Weight in tons witout trash" maxlength='10' name="noOfTubs{{batchData.id}}" ng-model="batchData.noOfTubs" data-ng-required='true'  data-ng-pattern="/^[0-9.\s]*$/" ng-keyup="getotalCost(batchData.id,batchData.noOfTubs);" />
<p ng-show="chemicalTreatmentForm.noOfTubs{{batchData.id}}.$error.required && (chemicalTreatmentForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="chemicalTreatmentForm.noOfTubs{{batchData.id}}.$error.pattern && (chemicalTreatmentForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											
											</tr>
									</tbody>
							 	</table>
							</div>
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(chemicalTreatmentForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
