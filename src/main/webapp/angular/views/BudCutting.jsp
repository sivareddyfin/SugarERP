		<script>
	  $(function() 
	  {
	     $( "#date" ).datepicker({
    		  changeMonth: true,
		      changeYear: true,
			  dateFormat: 'dd-mm-yy'
	     });		 	
		 	 		 
		 $('#Time').timepicker({ 'scrollDefault': 'now' });
	  });
    </script>	

	<header id="header" data-current-skin={{mactrl.currentSkin}} data-ng-include="'template/header.jsp'" data-ng-controller="headerCtrl as hctrl"></header>
	<section id="main" class='bannerok'>    
	    <aside id="sidebar" data-ng-include="'template/sidebar-left.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>
	    <aside id="chat" data-ng-include="'template/chat.jsp'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.right === true }"></aside>

    	<section id="content" data-ng-controller="BudCuttingController" ng-init="loadShift();loadVarietyNames();addFormField();loadVillageNames();loadseasonNames();loadMachine();">      
        	<div class="container">				
    			<div class="block-header" style="margin-bottom:4px; position:static; margin-top:-15px; padding:8px;"><h2><b>Bud Cutting</b></h2></div>
			    <div class="card">
			        <div class="card-body card-padding">
					<!--------Form Start----->					
					 <!-------body start------>						 	
					 	<form name="BudCuttingForm" novalidate ng-submit="BudCuttingSubmit(AddedBudCutting,BudCuttingForm);" >
							  <input type="hidden" class="form-control" placeholder='ModifyFlag'   name="modifyFlag" ng-model="AddedBudCutting.modifyFlag" />
							  <div class="row">
								 <div class="col-sm-6">
										<div class="row">
											<div class="col-sm-12">
												<div class="input-group">
													<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
													<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : BudCuttingForm.season.$invalid && (BudCuttingForm.season.$dirty || submitted)}">
														<div class="fg-line">
															<select chosen class="w-100"  tabindex="1" data-ng-required='true' data-ng-model='AddedBudCutting.season' name="season" ng-options="season.season as season.season for season in seasonNames" maxlength="10" ng-change="getAddedBudCuttingList(AddedBudCutting.season,AddedBudCutting.date,AddedBudCutting.shift);">
																<option value="">Season</option>
															</select>	
														</div>
														<p ng-show="BudCuttingForm.season.$error.required && (BudCuttingForm.season.$dirty || submitted)" class="help-block">Season</p>													
													</div>
												</div>
											</div>
										</div>
									</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-calendar ma-icon"></i></span>
												<div class="form-group  floating-label-wrapper"  ng-class="{ 'has-error' : BudCuttingForm.date.$invalid && (BudCuttingForm.date.$dirty || submitted)}">
						        	                <div class="fg-line">
														<label for="date">Date</label>
														<input type="text" class="form-control" placeholder='Date' id="date" with-floating-label  name="date" ng-model="AddedBudCutting.date" readonly="readonly"  tabindex="2" data-input-mask="{mask: '00-00-0000'}" ng-required="true" maxlength="12" ng-blur="getAddedBudCuttingList(AddedBudCutting.season,AddedBudCutting.date,AddedBudCutting.shift);" />	
				        		        	        </div>
													<p ng-show="BudCuttingForm.date.$error.required && (BudCuttingForm.date.$dirty || submitted)" class="help-block">Date is Required.</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-time ma-icon"></i></span>
												<div class="form-group"  ng-class="{ 'has-error' : BudCuttingForm.shift.$invalid && (BudCuttingForm.shift.$dirty || submitted)}">
						        	                <div class="fg-line">
														
														<select chosen class="w-100" name="shift" ng-required="true" data-placeholder="Shift" ng-model="AddedBudCutting.shift" ng-required="true" ng-options="shiftId.id as shiftId.shiftName for shiftId in WeighBridgeNames | orderBy:'-shiftName':true" ng-change="getAddedBudCuttingList(AddedBudCutting.season,AddedBudCutting.date,AddedBudCutting.shift);">
															<option value="">Select Shift</option>
															
														</select >
				                			        </div>
													<p ng-show="BudCuttingForm.shift.$error.required && (BudCuttingForm.shift.$dirty || submitted)" class="help-block">Select Shift</p>													
												</div>
					                    	</div>
										</div>
									</div>
								</div>
							  </div>
							
							  
							
							
							
							  
							  
							 <div class="row">
							 <div class="col-sm-4">
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Company or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="labourcharges" value="0" ng-model="AddedBudCutting.labourSpplr" ng-click="getLabourContractorDetails();" >
				    				            	  <i class="input-helper"></i>Company
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="labourcharges" value="1"  ng-model="AddedBudCutting.labourSpplr" ng-click="getLabourContractorDetails();">
			    						            <i class="input-helper"></i>Contract
					  				              </label>
			                			        </div>
					                    	</div>
										</div>
									 <div class="col-sm-4" ng-show="AddedBudCutting.labourSpplr=='0'">
									
											<div class="input-group" style="margin-top:10px;">
            				            		<span class="input-group-addon">Machine or Not :</span>
					        	                <div class="fg-line" style="margin-top:5px;">
        		    					         <label class="radio radio-inline m-r-5px" >
									            	  <input type="radio" name="machine" value="0" ng-model="AddedBudCutting.machineOrMen" >
				    				            	  <i class="input-helper"></i>Machine
									          	  </label>
						 			            <label class="radio radio-inline m-r-5px" >
						            			    <input type="radio" name="machine" value="1"  ng-model="AddedBudCutting.machineOrMen">
			    						            <i class="input-helper"></i>Manual
					  				              </label>
			                			        </div>
					                    	</div>
										
								</div>
									<div class='col-sm-4'>
								<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group">
						        	                <div class="fg-line">

													
														<select chosen class="w-100" name='lc'  ng-model="AddedBudCutting.lc" ng-options="lc.lcCode as lc.lcName for lc in lcNames">
															<option value=''>Contractor </option>
														</select>
													</div>
												</div>

							</div>
										
							 </div>
							 
							
							
							  	<div ng-show="AddedBudCutting.labourSpplr=='0' && AddedBudCutting.machineOrMen=='1'">						
							  <div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Labour Charges</u></h4>
									</div>
								</div>
								
							 	<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Men</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.noOfMen.$invalid && (BudCuttingForm.noOfMen.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfMen">No. of Men</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfMen" ng-model="AddedBudCutting.noOfMen" ng-keyup="loadmenTotal(AddedBudCutting.labourSpplr,AddedBudCutting.noOfMen,AddedBudCutting.manCost);getGrandTotal(AddedBudCutting.labourSpplr,AddedBudCutting.menTotal,AddedBudCutting.womenTotal);" tabindex="3" data-ng-pattern="/^[0-9\s]*$/" id="noOfMen" with-floating-label  />
												</div>
												<p ng-show="BudCuttingForm.noOfMen.$error.pattern && (BudCuttingForm.noOfMen.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.manCost.$invalid && (BudCuttingForm.manCost.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="manCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="manCost" ng-model="AddedBudCutting.manCost" ng-keyup="loadmenTotal(AddedBudCutting.labourSpplr,AddedBudCutting.noOfMen,AddedBudCutting.manCost);getGrandTotal(AddedBudCutting.labourSpplr,AddedBudCutting.menTotal,AddedBudCutting.womenTotal);"  tabindex="4" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" id="manCost" with-floating-label />
												</div>
												<p ng-show="BudCuttingForm.manCost.$error.pattern && (BudCuttingForm.manCost.$dirty || submitted)" class="help-block">Invalid</p>
												
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												   <label for="menTotal">Total</label>
														<input type="text" class="form-control" placeholder="Total" name="menTotal" ng-model="AddedBudCutting.menTotal" tabindex="5" readonly  id="menTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Women</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.noOfWomen.$invalid && (BudCuttingForm.noOfWomen.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfWomen">No. of Women</label>
														<input type="text" class="form-control" placeholder="No. of People" name="noOfWomen" ng-model="AddedBudCutting.noOfWomen" ng-keyup="loadWomenTotal(AddedBudCutting.labourSpplr,AddedBudCutting.noOfWomen,AddedBudCutting.womanCost);getGrandTotal(AddedBudCutting.labourSpplr,AddedBudCutting.menTotal,AddedBudCutting.womenTotal);" tabindex="6"  data-ng-pattern="/^[0-9\s]*$/" id="noOfWomen" with-floating-label />
												</div>
												<p ng-show="BudCuttingForm.noOfWomen.$error.pattern && (BudCuttingForm.noOfWomen.$dirty || submitted)" class="help-block">Invalid</p>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.womanCost.$invalid && (BudCuttingForm.womanCost.$dirty || submitted)}">
												 <div class="fg-line">
												   <label for="womanCost">Cost</label>
														<input type="text" class="form-control" placeholder="Cost" name="womanCost" ng-model="AddedBudCutting.womanCost" ng-keyup="loadWomenTotal(AddedBudCutting.labourSpplr,AddedBudCutting.noOfWomen,AddedBudCutting.womanCost);getGrandTotal(AddedBudCutting.labourSpplr,AddedBudCutting.menTotal,AddedBudCutting.womenTotal);" tabindex="7" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  id="womanCost" with-floating-label />
												</div>
												
										<p ng-show="BudCuttingForm.womanCost.$error.pattern && (BudCuttingForm.womanCost.$dirty || submitted)" class="help-block">Invalid</p>		
												</div>
												</div>
									</div>
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="womenTotal">Cost</label>
														<input type="text" class="form-control" placeholder="Total" name="womenTotal" ng-model="AddedBudCutting.womenTotal" tabindex="8" readonly  id="womenTotal" with-floating-label  />
												</div>
												</div>
												</div>
									</div>
								
								
								</div>
								
								</div>
								
								<div  ng-show="AddedBudCutting.machineOrMen=='0' && AddedBudCutting.labourSpplr=='0'">
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Machines</u></h4>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">No. of Machines</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-9">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.noOfMachines.$invalid && (BudCuttingForm.noOfMachines.$dirty || submitted)}">
												 <div class="fg-line">
												  <label for="noOfMachines">No. of Machines</label>
														<input type="text" class="form-control" placeholder="No. of Machines" name="noOfMachines" data-ng-model="AddedBudCutting.noOfMachines" data-ng-pattern="/^[0-9\s]*$/" id="noOfMachines" with-floating-label  />
												</div>
												<p ng-show="BudCuttingForm.noOfMachines.$error.pattern && (BudCuttingForm.noOfMachines.$dirty || submitted)" class="help-block">Invalid</p>
												
												
												</div>
												</div>
									</div>
							   </div>
									
								</div>
								
								
								<div class="row" ng-show="AddedBudCutting.machineOrMen=='1' && AddedBudCutting.labourSpplr=='0'">
								<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									
									</div>
									<div class="col-sm-4">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
												 <label for="totalLabourCost">Grand Total</label>
														<input type="text"  placeholder="Grand Total" ng-model="AddedBudCutting.totalLabourCost" name="totalLabourCost" class="form-control" readonly tabindex="12" id="totalLabourCost" with-floating-label />
												</div>
												</div>
												</div>
									</div>
									
								</div>
								
								
								<div  ng-show="AddedBudCutting.labourSpplr=='1'"> 
								<div class="row">
									<div class="col-sm-12">
										<h4 style="font-weight:normal;font-family:'Times New Roman', Times, serif;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>Contract Charges</u></h4>
									</div>
								</div>
								<div class="row">
								
									<div class="col-sm-3">
									<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper">
												 <div class="fg-line">
														<h5 style="font-weight:normal;">Contract Charges</h5>
												</div>
												</div>
												</div>
									</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.costPerTon.$invalid && (BudCuttingForm.costPerTon.$dirty || submitted)}">
							<div class="fg-line">
							 <label for="costPerTon">Cost Per Ton</label>
							<input type="number" placeholder="Cost Per Ton"  class="form-control" name="costPerTon" ng-model="AddedBudCutting.costPerTon" ng-keyup="loadContractTotal(AddedBudCutting.labourSpplr,AddedBudCutting.costPerTon,AddedBudCutting.totalTons);" tabindex="9" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"  id="costPerTon" with-floating-label />
							</div>
							<p ng-show="BudCuttingForm.costPerTon.$error.pattern && (BudCuttingForm.costPerTon.$dirty || submitted)" class="help-block">Invalid</p>
							
							</div>
							</div>
							</div>
									<div class="col-sm-3">
							<div class="input-group">
            				            		<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
												<div class="form-group floating-label-wrapper" ng-class="{ 'has-error' : BudCuttingForm.totalTons.$invalid && (BudCuttingForm.totalTons.$dirty || submitted)}">
							<div class="fg-line">
							 <label for="totalTons">Total Tons</label>
							<input type="number" placeholder="Total Tons"  class="form-control" name="totalTons" ng-model="AddedBudCutting.totalTons" ng-keyup="loadContractTotal(AddedBudCutting.labourSpplr,AddedBudCutting.costPerTon,AddedBudCutting.totalTons);" tabindex="10" ng-pattern="/^[0-9]+(\.[0-9]{1,3})?$/" id="totalTons" with-floating-label  />
							</div>
							
							<p ng-show="BudCuttingForm.totalTons.$error.pattern && (BudCuttingForm.totalTons.$dirty || submitted)" class="help-block">Invalid</p>
							</div>
							</div>
							</div>
									<div class="col-sm-3">
									<div class="input-group">
														<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
									<div class="fg-line">
									<label for="totalNoOfBuds">Total Cost</label>
									<input type="text" class="form-control" placeholder="Total Cost" name="totalNoOfBuds" ng-model="AddedBudCutting.totalNoOfBuds"  readonly tabindex="11" id="totalNoOfBuds" with-floating-label />
									</div>
									</div>
									</div>
								</div>
								</div>	
													 					 	
							 	
								<div class="table-responsive">
							 	<table class="table table-striped table-vmiddle">
							 		<thead>
								        <tr>
                		    				<th>Action</th>
											<th>Machine</th>
											<th>Batch Series</th>
											<th>Variety</th>
							                <th>Supplier</th>
		                    				<th>Village</th>
											<th>Buds/Kg</th>
											<th>Tub Weight with Buds(kgs.)</th>
											<th>No.of Tubs</th>
											<th>Total No. of Buds</th>
											
            							</tr>											
									</thead>
									<tbody>
										<tr ng-repeat="batchData in data">
											<td>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="addFormField();" ng-if="batchData.id=='1'"><i class="glyphicon glyphicon-plus-sign"></i></button>
												<button type="button" id="right_All_1" class="btn btn-primary" ng-click="removeRow(batchData.id);" ng-if="batchData.id!='1'"><i class="glyphicon glyphicon-remove-sign"></i></button>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.machine{{batchData.id}}.$invalid && (BudCuttingForm.machine{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<select class="form-control1" name="machine{{batchData.id}}" ng-model="batchData.machine" tabindex="13"  ng-options ="machine.id as machine.machineNumber for machine in machine">
													<option value="">Machine</option>
												</select>
<p ng-show="BudCuttingForm.machine{{batchData.id}}.$error.required && (BudCuttingForm.machine{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
												
																								
</div>												
											</td>
											<td style="display:none;">
											<input type="hidden" ng-model="batchData.id" name="batchid{{batchData.id}}" />
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.batchSeries{{batchData.id}}.$invalid && (BudCuttingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																							
											<input list="stateList" placeholder="Batch Series" id="ryotcodeorname"  class="form-control1"  name="batchSeries{{batchData.id}}" placeholder ="Batch No" ng-model="batchData.batchSeries" ng-change="loadBatchDetails(batchData.batchSeries,batchData.id);" tabindex="14">
    								<datalist id="stateList">
        							<select class="form-control1">
           						 <option ng-repeat="batch in addedbatchSeries" value="{{batch.batchseries}}"></option>
       						    </select>    
   									</datalist>

<p ng-show="BudCuttingForm.batchSeries{{batchData.id}}.$error.required && (BudCuttingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.batchSeries{{batchData.id}}.$error.pattern && (BudCuttingForm.batchSeries{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>			
										
</div>	


						</td>
											
											
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.variety{{batchData.id}}.$invalid && (BudCuttingForm.variety{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input class="form-control1" placeholder="Variety" name="variety{{batchData.id}}" data-ng-model='batchData.variety' name="VarietyCode" readonly="readonly"  tabindex="15">
														
<p ng-show="BudCuttingForm.variety{{batchData.id}}.$error.required && (BudCuttingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.variety{{batchData.id}}.$error.pattern && (BudCuttingForm.variety{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.seedSupr{{batchData.id}}.$invalid && (BudCuttingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier" maxlength='10' name="seedSupr{{batchData.id}}" ng-model="batchData.seedSupr"  data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly tabindex="16">
<p ng-show="BudCuttingForm.seedSupr{{batchData.id}}.$error.required && (BudCuttingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.seedSupr{{batchData.id}}.$error.pattern && (BudCuttingForm.seedSupr{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											<td style='display:none;'>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.landVillageCode{{batchData.id}}.$invalid && (BudCuttingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Village" name="landVillageCode{{batchData.id}}" ng-model="batchData.landVillageCode" tabindex="17"  readonly >
													
<p ng-show="BudCuttingForm.landVillageCode{{batchData.id}}.$error.required && (BudCuttingForm.landVillageCode{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
																								
</div>												
											</td>

											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.varietyName{{batchData.id}}.$invalid && (BudCuttingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																							
												<input class="form-control1" placeholder="Variety" name="varietyName{{batchData.id}}" data-ng-model='batchData.varietyName' name="VarietyCode" readonly="readonly"  tabindex="15">
														
<p ng-show="BudCuttingForm.varietyName{{batchData.id}}.$error.required && (BudCuttingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.varietyName{{batchData.id}}.$error.pattern && (BudCuttingForm.varietyName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
</div>												
											</td>    
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.ryotName{{batchData.id}}.$invalid && (BudCuttingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Supplier" name="ryotName{{batchData.id}}" ng-model="batchData.ryotName"  data-ng-pattern="/^[a-z 0-9 A-Z\s]*$/"/ readonly tabindex="16">
<p ng-show="BudCuttingForm.ryotName{{batchData.id}}.$error.required && (BudCuttingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.ryotName{{batchData.id}}.$error.pattern && (BudCuttingForm.ryotName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
																																				
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.lvName{{batchData.id}}.$invalid && (BudCuttingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																		
												<input type="text" class="form-control1" placeholder="Village" name="lvName{{batchData.id}}" ng-model="batchData.lvName" tabindex="17"  readonly >
													
<p ng-show="BudCuttingForm.lvName{{batchData.id}}.$error.required && (BudCuttingForm.lvName{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
																								
</div>												
											</td>
											
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.budsPerKG{{batchData.id}}.$invalid && (BudCuttingForm.budsPerKG{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Buds per kg" maxlength='10' name="budsPerKG{{batchData.id}}" ng-model="batchData.budsPerKG"   data-ng-pattern="/^[0-9.\s]*$/" tabindex="18" ng-keyup="getTotalBuds(batchData.budsPerKG,batchData.noOfTubs,batchData.tubWt,batchData.id);"/>
<p ng-show="BudCuttingForm.budsPerKG{{batchData.id}}.$error.required && (BudCuttingForm.budsPerKG{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.budsPerKG{{batchData.id}}.$error.pattern && (BudCuttingForm.budsPerKG{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.tubWt{{batchData.id}}.$invalid && (BudCuttingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Tub Weight with kgs"  name="tubWt{{batchData.id}}" ng-model="batchData.tubWt"   data-ng-pattern="/^[0-9.\s]*$/" tabindex="20" ng-keyup="getTotalBuds(batchData.budsPerKG,batchData.noOfTubs,batchData.tubWt,batchData.id);"/>
<p ng-show="BudCuttingForm.tubWt{{batchData.id}}.$error.required && (BudCuttingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.tubWt{{batchData.id}}.$error.pattern && (BudCuttingForm.tubWt{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.noOfTubs{{batchData.id}}.$invalid && (BudCuttingForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="No. of Tubs" maxlength='10' name="noOfTubs{{batchData.id}}" ng-model="batchData.noOfTubs"  data-ng-pattern="/^[0-9.\s]*$/" tabindex="19" ng-keyup="getTotalBuds(batchData.budsPerKG,batchData.noOfTubs,batchData.tubWt,batchData.id);"/>
<p ng-show="BudCuttingForm.noOfTubs{{batchData.id}}.$error.required && (BudCuttingForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.noOfTubs{{batchData.id}}.$error.pattern && (BudCuttingForm.noOfTubs{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											
											<td>
<div class="form-group" ng-class="{ 'has-error' : BudCuttingForm.totalBuds{{batchData.id}}.$invalid && (BudCuttingForm.totalBuds{{batchData.id}}.$dirty || Addsubmitted)}">																																																																																																																																																								
												<input type="text" class="form-control1"  placeholder="Total No.of Buds"  name="totalBuds{{batchData.id}}" ng-model="batchData.totalBuds"  data-ng-pattern="/^[0-9.\s]*$/" tabindex="21" readonly/>
<p ng-show="BudCuttingForm.totalBuds{{batchData.id}}.$error.required && (BudCuttingForm.totalBuds{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Required</p>												
<p ng-show="BudCuttingForm.totalBuds{{batchData.id}}.$error.pattern && (BudCuttingForm.totalBuds{{batchData.id}}.$dirty || Addsubmitted)" class="help-block">Invalid</p>												
												
</div>												
											</td>
											</tr>
											<tr>
											<td colspan="7"></td><td colspan="3" align="right"><div class="form-group" ><input type="text" class="form-control1" placeholder="Buds Grand Total" name="budGrandTotal"  ng-model="AddedBudCutting.budGrandTotal" tabindex="22" readonly /></div></td>
											</tr>
									</tbody>
							 	</table>
							</div>
								
										
							  
							
															  	  
							<div class="row" align="center">            			
								<div class="input-group">
									<div class="fg-line">
										<button type="submit" class="btn btn-primary btn-sm m-t-10 btn-hide">Save</button>
										<button type="reset" class="btn btn-primary btn-sm m-t-10" ng-click="reset(BudCuttingForm)">Reset</button>
									</div>
								</div>						 	
							 </div> 	
						  </form>	 							 
					<!------------------form end---------->							 
					<!-------from-end--------------------->		 						           			
				        </div>
			    	</div>
					
					
				</div>
	    </section> 
	</section>

	<footer id="footer" data-ng-include="'template/footer.jsp'"></footer>
