<!DOCTYPE html>

    <!--[if IE 9 ]><html class="ie9" data-ng-app="materialAdmin" data-ng-controller="materialadminCtrl as mactrl"><![endif]-->
    <![if IE 9 ]><html data-ng-app="materialAdmin" data-ng-controller="materialadminCtrl as mactrl"><![endif]>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SRI SARVARAYA SUGARS LTD.</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="vendors/bower_components/angular-loading-bar/src/loading-bar.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

        <!-- CSS -->		
        <link href="css/app.min.1.css" rel="stylesheet" id="app-level">		
	    <link href="css/app.min.2.css" rel="stylesheet" id="app-level">
		<link id="beyond-link" href="css/beyond.min.css" rel="stylesheet" type="text/css" />
		<link href="css/font-awesome.min.css" rel="stylesheet" />
        <!--<link href="css/demo.css" rel="stylesheet">-->
		<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
		<link href="css/jquery-ui.css" rel="stylesheet">
		<link href="css/jquery.timepicker.css" rel="stylesheet">
			
		<!--<link href="css/bootstrap-material-datetimepicker.css" rel="stylesheet">-->
		
		
		
		
				
	<style type="text/css">
		.container
		{
			margin-left:30px;
			width:95%;
		}
		.form-control1 
		{
		  display: block;
		  width: 100%;
		  height: 34px;
		  padding: 6px 12px;
		  font-size: 14px;
		  line-height: 1.42857143;
		  color: #555;
		  background-color: #fff;
		  background-image: none;
		  border: 1px solid #ccc;
		  border-radius: 4px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
		  -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
		}
		.form-control1:focus 
		{
		  border-color: #66afe9;
		  outline: 0;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
		}
		/* popup_box DIV-Styles*/
		#popup_box 
		{ 
			display:none; /* Hide the DIV */
			position:fixed;  
			_position:absolute; /* hack for internet explorer 6 */  
			height:450px;  
			width:80%;  
			background:#FFFFFF;  
			/*left: 300px;*/
			top: 150px;
			z-index:10; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
			/*margin-left: 15px;  */
			
			/* additional features, can be omitted */
			border:2px solid #CCCCCC;  	
			padding:15px;  
			-moz-box-shadow: 0 0 5px #CCCCCC;
			-webkit-box-shadow: 0 0 5px #CCCCCC;
			box-shadow: 0 0 5px #CCCCCC;			
			
		}

		#gridpopup_box 
		{ 
			display:none; /* Hide the DIV */
			position:fixed;  
			_position:absolute; /* hack for internet explorer 6 */ 
			height:450px;
			overflow-y:auto;  
			width:80%;  
			background:#FFFFFF;  
			/*left: 300px;*/
			top: 150px;
			z-index:10; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
			/*margin-left: 15px;  */

			/* additional features, can be omitted */
			border:2px solid #CCCCCC;  	
			padding:15px;  
			-moz-box-shadow: 0 0 5px #CCCCCC;
			-webkit-box-shadow: 0 0 5px #CCCCCC;
			box-shadow: 0 0 5px #CCCCCC;	

		}
		
		.finalPopupBox 
		{ 
			display:none; /* Hide the DIV */
			position:fixed;  
			_position:absolute; /* hack for internet explorer 6 */  
			height:450px;  
			width:80%;  
			background:#FFFFFF;  
			/*left: 300px;*/
			top: 150px;
			z-index:10; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
			/*margin-left: 15px;  */
			
			/* additional features, can be omitted */
			border:2px solid #CCCCCC;  	
			padding:15px;  
			-moz-box-shadow: 0 0 5px #CCCCCC;
			-webkit-box-shadow: 0 0 5px #CCCCCC;
			box-shadow: 0 0 5px #CCCCCC;			
			
		}

		.floating-label-wrapper 
		{
		    position: relative;
		}	

		.floating-label 
		{

		    color: #888 !important;
		    font-size: 12px;
		    position: absolute;
		    pointer-events: none;
		    left: 5px;
		    top: 7px;
		    transition: 0.2s ease all;
		    opacity: 0;
			z-index:2;
		}
		.form-control:focus ~ .floating-label,.form-control:not(.empty) ~ .floating-label 
		{
		    top: -15px;
		    left: 0;
		    font-size: 13px;
		    opacity: 1;
		}

		.form-control.empty ~ .floating-label  
		{
		    opacity: 1;
		}
		textarea 
		{ 
			resize: none; 
		}
		textarea ~ .form-control-highlight 
		{
		    margin-top: -11px;
		}		

		.asdok 
		{
			  position: relative;
			  padding-top: 25px;
			  background-color:#ffffff;
			  color:#333333;
		}
		.asdok.positioned 
		{
			  position: absolute;
			  width:800px;
			  box-shadow: 0 0 15px #333;
		}
		.container1 
		{
			  overflow-y: auto;
			  height: 300px;
		}
		.table 
		{
			  border-spacing: 0;
			  width:100%;
		}
		.table td + .table td 
		{
			  border-left:1px solid #eee;
			  text-align:left;
		}
		.table td, .table th 
		{
			  border-bottom:1px solid #eee;
			  color: #000;
			  width:10%;
		}
		.table th span
		{
			  position: absolute;
			  background: transparent;
			  color: #333333;
			  top: 0;
		}
		.table th:first-child div
		{
			  border: none;
		}
		/*form styles*/
		#msform 
		{
			width: 100%;
			margin: 50px auto;
			text-align: center;
			position: relative;
		}
		#msform #fieldset 
		{
			background: white;
			border: 0 none;
			border-radius: 3px;
			box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
			box-sizing: border-box;
			/*width: 100%;*/
			/*margin: 0 10%;*/
			/*stacking fieldsets above each other*/
			position: absolute;
			z-index:2;
			margin-bottom:20px;
			
		}
		/*Hide all except first fieldset*/
		#msform #fieldset:not(:first-of-type) 
		{
			display: none;
		}
		/*inputs*/
		/*buttons*/
		#msform .action-button 
		{
			width: 100px;
			/*background: #2196f3;*/
			background-color: #2196f3;
			font-weight: bold;
			color: white;
			border: 0 none;
			border-radius: 1px;
			cursor: pointer;
			padding: 10px 5px;
			margin: 10px 5px;
		}
		#msform .action-button:hover, #msform .action-button:focus 
		{
			box-shadow: 0 0 0 2px white, 0 0 0 3px #2196f3;
		}
		/*headings*/
		.fs-title 
		{
			font-size: 15px;
			text-transform: uppercase;
			color: #2C3E50;
			margin-bottom: 10px;
		}
		.fs-subtitle 
		{
			font-weight: normal;
			font-size: 13px;
			color: #666;
			margin-bottom: 20px;
		}
		/*progressbar*/
		#progressbar 
		{
			margin-top:-40px;
			margin-bottom: 30px;
			overflow: hidden;
			/*CSS counters to number the steps*/
			counter-reset: step;
		}
		#progressbar li 
		{
			list-style-type: none;
			color: #000000;
			text-transform: uppercase;
			font-size: 11px;
			font-weight:bold;
			width: 25%;
			float: left;
			position: relative;
		}
		#progressbar li:before 
		{
			content: counter(step);
			counter-increment: step;
			width: 30px;
			line-height: 30px;
			display: block;
			font-size: 10px;
			color: #333;			
			background: white;
			border-radius: 15px;
			margin: 0 auto 5px auto;
		}
		/*progressbar connectors*/
		#progressbar li:after 
		{
			content: '';
			width: 100%;
			height: 4px;
			background: white;
			position: absolute;
			left: -50%;
			top: 13px;
			z-index: -1; /*put it behind the numbers*/
		}
		#progressbar li:first-child:after 
		{
			/*connector not needed before the first step*/
			content: none;
		}
		/*marking active/completed steps green*/
		/*The number of the step and the connector before it = green*/
		#progressbar li.active:before, #progressbar li.active:after 
		{
			background: #2196f3;
			color: white;
		}		
		.bannerok
		{
			margin-top:-60px;
		}
		 .footerDiv{	
		margin-right:25%;

		}

</style>
    </head>

    <body data-ng-class="{ 'sw-toggled': mactrl.layoutType === '1'}">
		<script type="text/javascript">
			//if(sessionStorage.getItem('username')==null || sessionStorage.getItem('username')=="null")
			//{
				//window.location.assign('/SugarERP');
			//}
		</script>
    <!-- Navbar -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
				<div class="row">
					<div class="col-sm-1">
						<div class="sidebar-collapse pull-left" id="sidebar-collapse" style="margin-top:13px; margin-left:35px;">
        		           <i class="collapse-icon glyphicon glyphicon-th"></i>						</div>						
					</div>
					<div class="col-sm-4">
						<div style=" position:fixed; top:3%;">							
							<a data-ui-sref="home" data-ng-click="mactrl.sidebarStat($event)" id="heading" style="font-size:20px; color:#FFFFFF;text-shadow: 0px 4px 3px rgba(0,0,0,0.4),0px 8px 13px rgba(0,0,0,0.1),0px 18px 23px rgba(0,0,0,0.1);">
								SRI SARVARAYA SUGARS LTD.							</a>						</div>									
					</div>
					<div class="col-sm-4">
						<div style=" position:fixed; top:25px; left:93%;" >
							<ul>
								<li class="dropdown hovereft" uib-dropdown style="list-style-type: none; margin-right:30px;">
        		        			<a uib-dropdown-toggle href="">
		        		            	<i class="collapse-icon zmdi zmdi-more-vert" style="font-size:26px; color:#FFFFFF;"></i>									</a>
			            	    	<ul class="dropdown-menu dm-icon pull-right" style=" position:fixed; top:70px;">
					                    <li class="divider hidden-xs"></li>
										<li>
                        					<a href="#"><i class="zmdi zmdi-settings"></i> {{UserName}}</a>										</li>
										<li>
                        					<a href="#" data-ng-click="mactrl.Logout();"><i class="zmdi zmdi-settings"></i> Logout</a>										</li>
		            			    </ul>
					            </li>
							</ul>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar1" style="margin-top:27px;">
                <!-- Sidebar Menu -->
                <ul class="nav sidebar-menu">
                    <!--Dashboard-->
                
							
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts') }" data-ui-sref-active="active" class="caneReceipt" style="display:">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Dashboards </span> 
							<i class="menu-expand"></i>						</a>
						<!------Masters---->					
						<ul class="submenu">
						<!----Masters---->
                           <li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.masters') }" data-ui-sref-active="active" class="recptmaster">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">Reports in Graphs</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
                                <li>
                                   <a  data-ui-sref="DashboardExtend" data-ng-click="mactrl.sidebarStat($event)">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Total Extent Size </span>						</a>							</li>
                                <li>
                                  <a  data-ui-sref="DashboardCurrentLoans" data-ng-click="mactrl.sidebarStat($event)">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Current Outstanding Loans </span>						</a>								</li>
                                <li>
                                   <a  data-ui-sref="DashboardCrushedCane" data-ng-click="mactrl.sidebarStat($event)">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Total Crushed Cane </span>						</a>								</li>
							
							<li>
                                   <a  data-ui-sref="ZoneWiseCircleNetWeight" data-ng-click="mactrl.sidebarStat($event)">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Zone Wise Circle Net Weight  </span>						</a>
							</li>
							
							
							
							  </ul>								  						  				
						   </li>						
						<!------Transaction-->
                           
						   
						<!-----MIS Reports--->
						</ul>						
					</li>
											
                    <!--Administration and Security-->
                    <li data-ng-class="{ 'active toggled': mactrl.$state.includes('admin') }" data-ui-sref-active="active" class="adminsec">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon zmdi zmdi-account-circle"></i>
                            <span class="menu-text"> Administration and Security </span>
                            <i class="menu-expand"></i>						</a>
                        <ul class="submenu">
							<!----Masters---->
                            <li data-ng-class="{ 'active toggled': mactrl.$state.includes('admin.security') }" data-ui-sref-active="active" class="admaster">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-lock-open"></i>
                                    <span class="menu-text">Masters</span>
                                    <i class="menu-expand"></i>								</a>
                                <ul class="submenu">
                                    <li>
                                        <a data-ui-sref-active="active" data-ui-sref="admin.security.CompanyMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Company Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active" data-ui-sref="admin.security.ScreenMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Screens Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active" data-ui-sref="admin.security.RoleMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text"> Roles Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active" data-ui-sref="admin.security.Designation" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Designation Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.DepartmentMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Department Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.LoginUserMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">User Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.SetupMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Setup Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.ScreenFields" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Screen Fields Master</span>										</a>									</li>
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.Season" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Season Master</span>										</a>									</li>									
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.MigrationMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Migration Master</span>										</a>									</li>									
                                
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.resetPassword" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Reset Password</span></a>							
									</li>	
									
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="admin.security.changePassword" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Change Password</span></a>							
									</li>	
								
								
								</ul>
                            </li>
							<!---MIS Reports-->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('admin.reports') }" data-ui-sref-active="active" class="adminmis">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">MIS Reports</span>
                                    <i class="menu-expand"></i>								</a>
                                <ul class="submenu">
                                    <li>
                                        <a data-ui-sref-active="active" data-ui-sref="admin.reports.AuditTrail" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Audit Trail</span>										</a>									</li>								
                                </ul>
                            </li>                            
                        </ul>
                    </li>	
					<!----------Agriculture and Harvesting--------->									
                    <li data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting') }" data-ui-sref-active="active" class="harvestmodl">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Agriculture and Harvesting </span> 
							<i class="menu-expand"></i>						</a>
						<ul class="submenu">
							<!-----Masters----->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.masters') }" data-ui-sref-active="active" class="harmaster">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Masters</span>
                                    <i class="menu-expand"></i>								</a>
                                <ul class="submenu">
                                    <li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.LandType" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Land Type Master</span>										</a>									</li>
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.CaneManager" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Cane Manager Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.FieldOfficer" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Field Officer Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.FieldAssistant" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Field Assistant Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Region" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Region Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Zone" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Zone Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.Mandal" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Mandal Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.Village" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Village Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Circle" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Circle Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Bank" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Bank Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Variety" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Variety Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.Advance" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Company Advance Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.AdvanceOrder" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Order Advances</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.AdvanceCategory" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Advance Category Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.AdvanceSubCategory" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Advance Subcategory Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.RyotMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Ryot Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.FamilyGroup" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Family Group Master</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.SeedlingTray" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Seedling Tray</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="harvesting.masters.SampleRules" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Sample Rules</span>										</a>									</li>								
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.PermitRules" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Permit Rules</span>										</a>									</li>																										
                                
									<li>
                                        <a data-ui-sref-active="active" data-ui-sref="harvesting.masters.AccountingClouser" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Accounting Clouser</span>										</a>									
									</li>
								
								</ul>
                            </li>
							<!---Transaction Screens--->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.transaction') }" data-ui-sref-active="active" class="hartrans">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Transaction Screens</span>
                                    <i class="menu-expand"></i>								</a>
								<ul class="submenu">
                                  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.RyotExtentDetails" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Extent Details for Soil Test</span>									 </a>								   </li>
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.soilTestResults" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Update Soil Test Results</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.OfferMaster" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Generate Agreement</span>									 </a>								   </li>															
								  
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.CompanyAdvancestoRyot" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Advances to Ryots</span>									 </a>								   </li>															
								  
								  <!--<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.SeedlingTrayCharges" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Seedling Tray Charges</span>									 </a>								   
									</li>		--->	
									
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.PrepareLoan" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Prepare Tieup Loans</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.RyotLoans" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Search Ryot Loans by Ryot</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.UpdateEstimatedQty" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Update Estimated Quantity</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GeneratePrograms" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Generate Programs</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GenerateSampleCards" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Generate Sample Cards</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.LabCCSRatings" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Update Lab CCS Rating</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.AvgCCSRatings" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Update Average CCS Rating</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GenerateRanking" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Generate Ranking</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.GeneratePermit" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Generate Permits</span>									 </a>								   </li>
										
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.SchedulePermit" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Schedule Permit</span>									 </a>							
									</li>	
											
										
																									
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.RegeneratePermit" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Re-generate Permit</span>									 </a>								   </li>
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.ReprintPermit" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Reprint Permits</span>									 </a>								   </li>

									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.specialPermit" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Special Permit</span>									 </a>	
									</li>
									
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="harvesting.transaction.ryotPermitDetails" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Ryot Permit Details</span>									 </a>	
									</li>


									
								 </ul>
							</li>
						    <!-----------MIS Reports------------>	
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('harvesting.Reports') }" data-ui-sref-active="active" class="harreport">
		                              <a href="#" class="menu-dropdown">
										<i class="menu-icon zmdi zmdi-account-circle"></i>
	                                    <span class="menu-text">MIS Reports</span>
                                    	<i class="menu-expand"></i>									  </a>	
									  <ul class="submenu">							   
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.ProgramDetails" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">Program Details</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">AgreementCheckList</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementCircleWiseCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">CircleWiseCheckList</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgreementMasterListRyotWiseInVillage" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">CheckListRyotWiseInVillage</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.GrowersRegisterRyotwiseReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">GrowersRegister Ryot wise</span>									 		</a>								   		
												</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.GrowerListByVillageMandalSummary" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">GrowerList Village & Mandal</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.BankOrder" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">Bank Order</span>									 		</a>								   		</li>
									  	<li>
                                     		<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.LoanMasterListReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	   		<i class="menu-icon zmdi zmdi-home"></i>
                                        		<span class="menu-text">Loans Master List Report</span>									 		</a>								   		</li>	
																				
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AgricultureLoanAppReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Agriculture Loan AppReport</span>								  	 </a>								  </li>										
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.RyotLoanFarwardingReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Ryot Loan Forwarding Report</span>								  	 </a>								  </li>										
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.SeedSeedlingLoansReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Seed Seedling Loans Report</span>								  	 </a>								  </li>										
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.VarietywiseSummaryReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Group Wise Crop Summary Report</span>								  	 </a>								  </li>										
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.LoginUserReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Login User</span>								  	 </a>								  </li>		
								 <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.CirclPlantRatoonWiseRyotReport" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">CirclePlantRatoonWiseRyotReport</span>								  	 </a>								  </li>	
										
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.CaneSamplesCheckList" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">Cane Samples CheckList</span>								  	 </a>								 
										</li>
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.RankingStatementReport" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">Ranking Statement Report</span>								  	 </a>								 
										</li>
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.CaneGrowersListCircleProgramWise" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">CaneGrowersCircleProgramWise</span>								  	 </a>								 
										</li>
										
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.InactiveModifiedAgreementsList" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">InactiveModifiedAgreementsList</span>								  	 </a>								 
										</li>
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.VerifyingLoansAsPerBranch" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">Loan Check List Report</span>								  	 </a>								 
										</li>
										
										 <li>
											<a data-ui-sref-active="active"  data-ui-sref="harvesting.Reports.AdvanceCheckListReport" data-ng-click="mactrl.sidebarStat($event)">
											<i class="menu-icon zmdi zmdi-home"></i>
											<span class="menu-text">Advance Check List Report</span>								  	 </a>								 
										</li>
										
										
		
										
										
								
									</ul>
							</li>
							

						</ul>
					</li>
					<!---------Cane Receipts------>					
                    <li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts') }" data-ui-sref-active="active" class="caneReceipt" style="display:">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Cane Receipts </span> 
							<i class="menu-expand"></i>						</a>
						<!------Masters---->					
						<ul class="submenu">
						<!----Masters---->
                           <li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.masters') }" data-ui-sref-active="active" class="recptmaster">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">Masters</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.masters.CaneWeightMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Weight Master</span>								  </a>								</li>
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.masters.ShiftMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Shift Master</span>								  </a>								</li>
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.masters.WeightBridgemaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Weigh Bridge Master</span>								  </a>								</li>
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.masters.UnloadingContractorMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Unloading Contractor Master</span>								  </a>								</li>																
							  </ul>								  						  				
						   </li>						
						<!------Transaction-->
                           <li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.transaction') }" data-ui-sref-active="active" class="recpttrans">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">Transaction Screens</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
								
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.permitCheckin" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Permit Checkin</span>								  </a>								</li>
							  


							  <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.CaneWeighment" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Weighment</span>								  </a>								</li>
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.TestWeighment" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Test Weighment</span>								  </a>								</li>
									 
									 <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.reprintReceipt" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Reprint Receipt</span>									 </a>			
									</li>
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="receipts.transaction.permitCheckinReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Permit CheckIn Report</span>									 </a>			
									</li>


						
							 </ul>							  							  						  				
						   </li>
						   <li data-ng-class="{ 'active toggled': mactrl.$state.includes('receipts.reports') }" data-ui-sref-active="active" class="recpttrans">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">MIS Reports</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.cummulativecanecrush" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Area Wise Cumulative</span>								  </a>	
								</li>
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VarietyWiseCaneCrushingCumulativeReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Variety Wise Cumulative</span>								  </a>		
								</li>
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VehicleTypeWiseCaneCrushingCumulative" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Vehicle Wise Cumulative</span>								  </a>						
								</li>
								
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.canecrushreport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Area Wise Daily</span>								  </a>	
								</li>
								
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VarietyWiseCaneCrushingReportDatewise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Variety Wise Daily</span>								  </a>				
								</li>
								
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VarietyWiseCaneCrushingShiftsWise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Variety Wise Daily By Shift</span>								  </a>						
								</li>																
																					
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VehicleTypeWiseCaneCrushingShiftsWise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Vehicle Wise Daily By Shift</span>								  </a>					
								</li>
								
								
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.DailyLogSheet" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Daily Log Sheet</span>								  </a>						
								</li>
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="receipts.reports.GrowerWiseHourlyCrushingReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Ryot Wise Hourly Crushing</span>								  </a>							
								</li>
								
								
								 
								  <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.BruntCaneReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Brunt Cane</span>								  </a>		
								 </li>
								 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.CaneSuppliersReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Suppliers Report</span>								  </a>		
								 </li>
								 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.zonewiseWeighment" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Weighment ZoneWise</span>								  </a>		
								 </li> 
								 
								
								<li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.VehicleNumWiseTrptDtls" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Vehicle NumberWise Trnspt Dtls</span>								  </a>		
								 </li>
									 
								<li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.ShiftWiseCaneCrushed" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">ShiftWiseCaneCrushed</span>								  </a>		
								 </li>
								 
								 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.HarvestContractorWiseRyot" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">HarvestContractorWiseRyot</span>								  </a>		
								 </li>
								 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.AreaWisePendingReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">AreaWisePendingReport</span>								  </a>		
								 </li>
								 
								 
								  <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.TransportContractorWiseRyot" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">TransportContractorWiseRyot</span>								  </a>		
								 </li>
								 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.TransportContractorRyotCumulative" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">TransportContractorRyotCumulative</span>								  </a>		
								 </li>
								 
								  <li>
									<a data-ui-sref-active="active"  data-ui-sref="receipts.reports.HarvestContractorWiseRyotCumulative" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">HarvestContractorRyotCumulative</span>								  </a>		
								 </li>
								 
								 
								 
							
							 </ul>							  							  						  				
						   </li>
						   
						   
						   
						  
						<!-----MIS Reports--->
						</ul>						
					</li>
					<!----Cane Accounting----->
                    <li data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting') }" data-ui-sref-active="active" class="caneAccounting" style="display:">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Cane Accounting </span> 
							<i class="menu-expand"></i>						</a>
						<ul class="submenu">
						<!-----Masters----->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.masters') }" data-ui-sref-active="active" class="accountmaster">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">Masters</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.Deductions" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Standard Deductions</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.DeductionsOrder" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Order Deductions</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.TieupLoanInterest" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Tieup Loan Interest </span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.AccountGroupMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Account Group</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.SubGroupMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Account Subgroup</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.HarvestContractorMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Harvesting Contractor</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.TransportContractorMaster" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Transport Contractor</span>								  </a>								</li>	
							  	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.masters.CaneAccountingRules" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Accounting Rules </span>								  </a>								
									 </li>	
									 
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.purchaseTaxTransfer" data-ng-click="mactrl.sidebarStat($event)">
									<i class="menu-icon zmdi zmdi-home"></i>
									<span class="menu-text">Purchase Tax Transfer </span>								  </a>							
								</li>
								 <li>
									<a data-ui-sref-active="active"  data-ui-sref="accounting.masters.accountingClosure" data-ng-click="mactrl.sidebarStat($event)">
									<i class="menu-icon zmdi zmdi-home"></i>
									<span class="menu-text">Accounting Closure </span>								  </a>							
								</li>
									 
									 
									 
									 
							  </ul>							  							  						  				
						   </li>	
						<!----Transactions-->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.transaction') }" data-ui-sref-active="active" class="accounttransaction">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">Transaction Screens</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
							  	  
								  
								 <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateLoans" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Loans</span>											</a>								 
									</li>
							  	  
								  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateHarvestCharges" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Harvest Rates</span>								  	 </a>								  </li>
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateTransportCharges" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Transport Rates</span>								  	 </a>								  </li>
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.PerformCaneAccountingCalculation" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Cane Accounting Calculation</span>								  	 </a>				
									</li>
								  
								<li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.updatepayments" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Payments</span>								  	 </a>								
										</li>
								  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.PostSeasonCalculation" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Accrued & Due</span>								  	 </a>								  </li>
							  	  <li>
							  	
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.UpdateRyotAccounts" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Ryot Accounts</span>								  	 </a>								  </li>
							  	  
								    <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.seedAccountings" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Update Seed Accounting</span>								  	 </a>								  </li>
								  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.SearchRyotLoansbySurvey" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Ryot Loans by Survey No</span>								  	 </a>								  </li>
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.RyotLedger" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">General Ledger</span>								  	 </a>								  </li>
							  	  <li>
                                  	 <a data-ui-sref-active="active"  data-ui-sref="accounting.transaction.TrailBalance" data-ng-click="mactrl.sidebarStat($event)">
                                     	<i class="menu-icon zmdi zmdi-home"></i>
                                     	<span class="menu-text">Trail Balance</span>								  	 </a>								  </li>
							  </ul>							  							  						  				
						   </li>	
						<!---MIS Reports---->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('accounting.reports') }" data-ui-sref-active="active" class="seedreports">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">MIS Reports</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
							
							
							 
							<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.DeductionCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">DeductionCheckList</span>								  </a>						
							   </li>
							   
							     <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.LoanRecoveryReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Loan Statement Detailed</span>								  </a>						
							   </li>
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CanePricePaymentsStatement" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">SB Payment Detailed</span>								  </a>						
							   </li>
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CanePaymentVoucher" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CanePaymentVoucher</span>								  </a>						
							   </li>
							    

							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.BankWiseGrowersLoanCollectionForCaneAC" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Loan Account Summary</span>								  </a>						
							   </li> 
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.BankWisePaymentToRyotSBACsForCaneAC" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">SB Account Summary</span>								  </a>						
							   </li>
							   <li>
								<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneAccountCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CaneAccountCheckList</span>								  </a>						
							   </li>
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CanePaymentReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Payment Report</span>								  </a>						
							   </li>
							   
							  <li>
								<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneSupplyPaymentStatementForCaneAC" data-ng-click="mactrl.sidebarStat($event)">
                                  <i class="menu-icon zmdi zmdi-home"></i>
                                  <span class="menu-text">CaneSupplyPaymentStatement</span>								  </a>						
								</li>

							
							
							
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.HarvestingDeductionsForCaneACPeriod" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">HarvestingDeductionsForCaneAC</span>								  </a>						
							   </li> 
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.HarvestRecoverySummaryForCaneAC" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">HarvestRecoverySummaryCaneAC</span>								  </a>						
							   </li> 


							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.TransportContractorServiceProviderRates" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">TransportContractorServiceRates</span>								  </a>						
							   </li> 

                              	<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.TransportDeductionsCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Transport Deductions CheckList</span>								  </a>							    </li>
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.TransportDeductionsForCane" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Transport Deductions For Cane</span>								  </a>							    </li>
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.TransportRecoverySummaryForCane" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Transport Recovery Summary</span>								  </a>				</li>




			
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.VillageWiseTransportSubsidyList" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">VillageWiseTransportSubsidyList</span>								  </a>							    </li> 
								
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.TransportSubsidyListForTheCrushing" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">TransportSubsidyForTheCrushing</span>								  </a>							  
								</li>	
						   
							   
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CheckListForSBAccountPortions" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CheckListForSBAccountPortions</span>								  </a>						
								</li>


														   


								 <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.VillageWiseIncentiveCanePricePaymentSummary" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">VillageWiseIncentiveCanePrice</span>								  </a>						
							   </li> 
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.IncentiveCanePricePaymentStatement" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">IncentiveCanePricePayment</span>								  </a>						
							   </li> 
							   
								 
							   
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CanToFinancialAccounting" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CaneToFinancialAccounting</span>								  </a>						
							   </li>
							   
							   
								<li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneAccountCheckListSample" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CaneAccountCheckListSample</span>								  </a>						
							   </li>
							  
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.PaymentCheckList" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">PaymentCheckList</span>								  </a>						
							   </li>
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.RyotWiseWeighmentDetail" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">RyotWiseWeighmentDetail</span>								  </a>						
							   </li>
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.HarvestingContractorServiceProviderRates" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">HarvestingContractorServiceRates</span>								  </a>						
							   </li> 
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneSupplyCheckListForWeek" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Cane Supply Statement</span>								  </a>						
							   </li>
							   
							    <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneLedgerReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CaneLedgerReport</span>								  </a>						
							   </li>
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneToFAReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CaneToFAReport</span>								  </a>						
							   </li>
							   
							   
							   <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="accounting.reports.CaneSupplyPaymentStatement" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">CummulativeCaneSupplyPayment</span>								  </a>						
							   </li>
							   
							   
							   <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.bankLoanDetails" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Bank Loan Details</span>
									</span>									 </a>								
									</li>
							   <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.BankLoanDataPdfFormate" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Bank Loan Details pdf Formate</span>
									</span>									 </a>								
								</li>
							    <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.YearlyLedgerRyotsReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">YearlyLedger Report</span>
																	 </a>								
								</li>
							     <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.UnloadContractorUnloadingReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">UnloadContractorUnloadingReport</span>
																	 </a>								
								</li>
								<!--
								 <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.PendingLoansAndAdvancesReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">PendingLoansAndAdvancesReport</span>
																	 </a>								
								</li>
							   -->
							    <li>
										<a data-ui-sref-active="active"  data-ui-sref="accounting.reports.UpdatePaymentsReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">UpdatePaymentsReport</span>
																	 </a>								
								</li>
							 </ul>							  							  						  				
						   </li>										
						</ul>
					</li>
					
					<!----Seedling Nursery----->
					<li data-ng-class="{ 'active toggled': mactrl.$state.includes('seedling') }" data-ui-sref-active="active" class="seedlingmodl">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Seeedling Nursery </span> 
							<i class="menu-expand"></i>						</a>
						<ul class="submenu">
							<!-----Masters----->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('seedling.masters') }" data-ui-sref-active="active" class="seedmaster">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Masters</span>
                                    <i class="menu-expand"></i>								</a>
                                <ul class="submenu">
								
                                    <li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.SubCategory" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">SubCategory</span>										</a>									</li>
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.categorymaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Category</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.unitmaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Package</span>										</a>									</li>
											
											<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.machine" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Machine</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.Manufacturing" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Manufacturing</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.productmaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Product</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.stockIndent" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Stock Indent</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active" data-ui-sref="seedling.masters.stockIndents" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Stock Indents</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active" data-ui-sref="seedling.masters.stockissue" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Stock Issue</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.stockledger" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Stock Ledger</span>										</a>									</li>								
									<li style="display:none;">
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.closingStockStatements" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Closing Stock Statements</span>	
											</a>									</li>
											
											<li>
                                        <a data-ui-sref-active="active"  data-ui-sref="seedling.masters.kindMaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Kind</span>										</a>									</li>
								
                                </ul>
                            </li>
							<!---Transaction Screens--->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('seedling.transaction') }" data-ui-sref-active="active" class="seedtrans">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Transaction Screens</span>
                                    <i class="menu-expand"></i>								</a>
								<ul class="submenu">
                                  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.seedlingEstimate" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Seedling Estimate</span>									 </a>								   </li>
								  <li style="display:none;">
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.seedlingEstimateReport" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Seedling Estimate Report</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.SourceofSeed" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Source of Seed</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Detrashing" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Detrashing</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.CaneShifting" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Transfer of Cane</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.BudCutting" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Bud Cutting</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.ChemicalTreatment" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Chemical Treatment</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.grading" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Sprouting/Grading</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.trayfilling" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Tray Filling</span>									 </a>								   </li>
										
										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.greenHouse" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Green House</span>									 </a>								   </li>
										
								
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Hardening" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Hardening</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Trimming" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Trimming</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.seedlingIndent" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Common Indent</span>									 </a>								   </li>
										 <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Indents" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Common Indents</span>									 </a>								   </li>															
								  <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.dispatchForm" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Dispatch Advise</span>									 </a>								   </li>															
								 
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Acknowledgement" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Dispatch Acknowledgement</span>									 </a>								   </li>
										
										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.seedlingDispatchAcknowledgement" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Reprint Dispatch</span>									 </a>								   </li>
										
										
										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.reprintAuthorisation" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Reprint Authorsation</span>									 </a>								   </li>	
										
										
										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.Authorisationfertilizers" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Authorisation for fertilizers</span>									 </a>								   </li>															
								  
									 <li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.seedlingTray" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">SeedlingTray</span>									 </a>								   </li>
										
									<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.BudRemovedCane" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Bud Removed Cane</span>									 </a>								   </li>	
										
										
										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.plantingUpdate" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Planting Update</span>									 </a>								   </li>	
										

										
										<li style="display:none;">
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.ReprintFertilizer" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Reprint Fertilizer</span>									 </a>								   </li>

										<li>
                                     <a data-ui-sref-active="active"  data-ui-sref="seedling.transaction.AuthorisationAck" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Authorisation Acknowledgement</span>									 </a>								   </li>															
								  
									 <li>										
								 </ul>
							</li>
						    <!-----------MIS Reports------------>
							<li style="display:;" data-ng-class="{ 'active toggled': mactrl.$state.includes('seedling.reports') }" data-ui-sref-active="active" class="seedreports">
                              <a href="#" class="menu-dropdown">
								<i class="menu-icon zmdi zmdi-account-circle"></i>
                                <span class="menu-text">MIS Reports</span>
                                <i class="menu-expand"></i>							  </a>		
                              <ul class="submenu">
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.weighmentDetails" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Weighment Details</span>								  </a>								</li>
                                <li>
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.indentDetails" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Indent Details</span>								  </a>								</li>
									  <li style="display:none">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.VarietyWiseCaneCrushingShiftsWise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Bud Cutting</span>								  </a>							    </li>
								 	<li style="display:none">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.VarietyWiseCaneCrushingReportDatewise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Chemical Treatment</span>								  </a>							    </li>
								
								<li style="display:none">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.VarietyWiseCaneCrushingCumulativeReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Sprouting</span>								  </a>							    </li>
									  </li>
									  
									  
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.AuthorisationStatusReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Authorisation Status Report</span>								  </a>							   
								</li>	
									<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.AuthorisationAckDetailsReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">AuthorisationAckDetailsReport</span>								  </a>							   
								</li>
								
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.ProcurementReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seed Procurement Report</span>								  </a>							   
								</li>	
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedlingProcessReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seedling Process Report</span>								  </a>							   
								</li>	
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedDistrubution" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seedlings Distrubution Report</span>								  </a>							   
								</li>
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedProductionReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seedlings Production Report</span>								  </a>							   
								</li>	
								
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedUtilizationReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seed Utilization Report</span>								  </a>							   
								</li>	
									
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedDevlopmentReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seedlings Devlopment Report</span>								  </a>							   
								</li>
								<li style="display:;">
                                  <a data-ui-sref-active="active"  data-ui-sref="seedling.reports.VegetableFrootsProductionReport" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Vegetable Fruits Production Report</span>								  </a>							   
								</li>
									
								<li style="display:;">
									<a data-ui-sref-active="active"  data-ui-sref="seedling.reports.SeedUtilizationReportZoneWise" data-ng-click="mactrl.sidebarStat($event)">
                                     <i class="menu-icon zmdi zmdi-home"></i>
                                     <span class="menu-text">Seed Utilization Report Zone Wise</span>								  </a>							   
								</li>	
									 </ul>							  							  						  				
						   </li>	
						</ul>
					</li>
					<!--- Store------>
                   		<li data-ng-class="{ 'active toggled': mactrl.$state.includes('store') }" data-ui-sref-active="active" class="storemodl">
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon glyphicon glyphicon-tasks"></i>
                            <span class="menu-text"> Store </span> 
							<i class="menu-expand"></i>						</a>
						<ul class="submenu">
							<!-----Masters----->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('store.masters') }" data-ui-sref-active="active" class="storemaster">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Masters</span>
                                    <i class="menu-expand"></i>								</a>
                                <ul class="submenu">
                                   							
											<li style="display:;">
                                        <a data-ui-sref-active="active"  data-ui-sref="store.masters.productmaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Product</span>										</a>									</li>
											<li style="display:;">
                                        <a data-ui-sref-active="active"  data-ui-sref="store.masters.productGroupmaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Product group</span>										</a>									</li>
											<li style="display:;">
                                        <a data-ui-sref-active="active"  data-ui-sref="store.masters.productsubGroupmaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">Product Subgroup</span>										</a>									</li>
											<li style="display:;">
                                        <a data-ui-sref-active="active"  data-ui-sref="store.masters.uommaster" data-ng-click="mactrl.sidebarStat($event)">
                                            <i class="menu-icon zmdi zmdi-home"></i>
                                            <span class="menu-text">UOM</span>										</a>									</li>
									  </ul>		
							<!---Transaction Screens--->
							<li data-ng-class="{ 'active toggled': mactrl.$state.includes('store.transaction') }" data-ui-sref-active="active" class="storetrans">
                                <a href="#" class="menu-dropdown">
									<i class="menu-icon zmdi zmdi-account-circle"></i>
                                    <span class="menu-text">Transaction Screens</span>
                                    <i class="menu-expand"></i>								</a>
								<ul class="submenu">
                                  
									<li >
                                     <a data-ui-sref-active="active"  data-ui-sref="store.transaction.ProductList" data-ng-click="mactrl.sidebarStat($event)">
                                        <i class="menu-icon zmdi zmdi-home"></i>
                                        <span class="menu-text">Product List</span>																			 </a>								
									</li>
									
								
									 							
							 </ul>							  							  						  				
						   </li>	
						</ul>
					</li>
                    <!--<li class="active">
                        <a  data-ng-click="mactrl.Logout();" style="">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Logout </span>			
						</a>					
					</li>-->					
              
                <!-- /Sidebar Menu -->
            </div>
            <!-- Page Content -->
            <div class="page-content">
                <div class="page-body">
					<data ui-view></data>
                </div>
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>
		
        







        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->


        <!-- Core -->
		<!--<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>-->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="js/jquery-1.10.2.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/beyond.js"></script>
		<script src="js/sweetalert-dev.js"></script>
	   <script src="js/highcharts/highcharts.js"></script> 
	    <script src="js/highcharts/modulesdata.js"></script> 		
		<script src="js/highcharts/modulesdrildown.js"></script>
		<script src="js/highcharts/cdnrawjitxlsx.full.min.js"></script>		
		<!-- <script type="text/javascript" src="js/googleplaces.js"></script>-->
		  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr0UTxeiLfIZu80falT09gdsAQNn1qcE4&sensor=false" ></script>
		

		
		<!--<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>-->
		<script src="js/moment-with-locales.min.js"></script>		
		<script src="js/jquery.timepicker.js"></script>		

		<!--<script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>-->


        <!-- Angular -->
		<!--<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>-->
        <script src="vendors/bower_components/angular/angular.min.js"></script>
        <script src="vendors/bower_components/angular-animate/angular-animate.min.js"></script>
        <script src="vendors/bower_components/angular-resource/angular-resource.min.js"></script>
        
        <!-- Angular Modules -->
        <script src="vendors/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script src="vendors/bower_components/angular-loading-bar/src/loading-bar.js"></script>
        <script src="vendors/bower_components/oclazyload/dist/ocLazyLoad.min.js"></script>
        <script src="vendors/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

        <!-- Common Vendors -->
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="vendors/bower_components/ng-table/dist/ng-table.min.js"></script>
		 <script src="js/highcharts/cdnrawjitxlsx.full.min.js"></script>
    	<script src="js/highcharts/rawgitods.js"></script>
      

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <!-- Using below vendors in order to avoid misloading on resolve -->
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="vendors/bower_components/angular-nouislider/src/nouislider.min.js"></script>
        
        
        <!-- App level -->
        <script src="js/app.js"></script>
        <script src="js/config.js"></script>
        <script src="js/controllers/main.js"></script>
		<script src="js/controllers/main_extend.js"></script>
		<script src="js/controllers/main_seedlingNursery.js"></script>
		<script src="js/controllers/main_store.js"></script>
        <script src="js/services.js"></script>
        <script src="js/templates.js"></script>
        <script src="js/controllers/ui-bootstrap.js"></script>
        <script src="js/controllers/table.js"></script>
		<script src="js/controllers/tableok.js"></script>
		<script src="js/alasql.min.js"></script>
		<script src="js/xlsx.core.min.js"></script>
		


        <!-- Template Modules -->
        <script src="js/modules/template.js"></script>
        <script src="js/modules/ui.js"></script>
        <script src="js/modules/charts/flot.js"></script>
        <script src="js/modules/charts/other-charts.js"></script>
        <script src="js/modules/form.js"></script>
        <script src="js/modules/media.js"></script>
        <!--<script src="js/modules/components.js"></script>-->
        <script src="js/modules/calendar.js"></script>
        <script src="js/modules/demo.js"></script>
		<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>-->
		<!--<script src="js/jquery.geocomplete.js"></script>-->
		<script src="js/logger.js"></script>
		<link href="css/sweetalert.css" rel="stylesheet">
    </body>
</html>

